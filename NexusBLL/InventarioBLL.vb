﻿Imports NexusDLL
Imports NexusELL.TableEntities
Public Class InventarioBLL
    Dim dl As InventarioDLL
    Dim Tablas As TableData
    Public Sub New(ByVal StringConexion As String)
        dl = New InventarioDLL(StringConexion)
        Tablas = New TableData(StringConexion)
    End Sub

    Public Sub inv_SubGruposInsert(ByVal entidad As inv_SubGrupos)
        dl.inv_SubGruposInsert(entidad)
    End Sub
    Public Function inv_ConsultaExportableExistenciasYprecios(ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal Fecha As DateTime) As DataTable
        Return dl.inv_ConsultaExportableExistenciasYprecios(IdGrupo, IdSubGrupo, Fecha)
    End Function
    Public Function inv_ExistenciasBodegasProductos(ByVal Fecha As DateTime, ByVal IdGrupo As String, ByVal IdSubGrupo As Integer) As DataTable
        Return dl.inv_ExistenciasBodegasProductos(Fecha, IdGrupo, IdSubGrupo)
    End Function
    Public Function inv_ConsultaEntradas(ByVal Usuario As String) As DataSet
        Return dl.inv_ConsultaEntradas(Usuario)
    End Function
    Public Function inv_AprobarTraslado(ByVal IdDoc As Integer, ByVal AprobarReprobar As Integer, ByVal Comentario As String, ByVal AprobadoPor As String) As String
        Return dl.inv_AprobarTraslado(IdDoc, AprobarReprobar, Comentario, AprobadoPor)
    End Function
    Public Function ConsultaTraslados(ByVal Desde As Date, ByVal Hasta As Date, ByVal Usuario As String) As DataTable
        Return dl.ConsultaTraslados(Desde, Hasta, Usuario)
    End Function
    Public Function inv_AplicarTrasladoInventarioIngreso(ByVal Id As Integer) As String
        Return dl.inv_AplicarTrasladoInventarioIngreso(Id)
    End Function
    Public Function inv_RevierteTrasladoIngresado(ByVal Id As Integer) As String
        Return dl.inv_RevierteTrasladoIngresado(Id)
    End Function
    Public Function inv_ConsultaSalidas(ByVal Usuario As String) As DataSet
        Return dl.inv_ConsultaSalidas(Usuario)
    End Function
    Public Function inv_ConsultaTraslados(ByVal Usuario As String) As DataSet
        Return dl.inv_ConsultaTraslados(Usuario)
    End Function
    Public Function inv_ConsultaProducciones(ByVal Usuario As String) As DataSet
        Return dl.inv_ConsultaProducciones(Usuario)
    End Function
    Public Function inv_ActualizarVineta(ByRef Detalle As List(Of inv_Vinetas)) As String
        Return dl.inv_ActualizarVineta(Detalle)
    End Function
    Public Function inv_ObtenieCodigoDeProducto(ByVal IdGrupo As Integer) As String
        Return dl.inv_ObtieneCodigoProducto(IdGrupo)
    End Function
    Public Function inv_ObtenerListaProductos() As DataTable
        Return dl.inv_ObtenerListaProductos()
    End Function
    Public Function inv_ObtenerProductosEtiquetas() As DataTable
        Return dl.inv_ObtenerProductosEtiquetas()
    End Function
    Public Function inv_ObtenerSucursalesCategorias(ByVal IdCategoria As Integer) As DataTable
        Return dl.inv_ObtenerSucursalesCategorias(IdCategoria)
    End Function
    Public Function inv_InsertarFormulas(ByRef Header As inv_Formulas, ByRef Detalle As List(Of inv_FormulasDetalle)) As String
        Return dl.inv_InsertarFormulas(Header, Detalle)
    End Function
    Public Function inv_ObtenerGastosProduccion(ByVal Id As Integer) As DataTable
        Return dl.inv_ObtenerGastosProduccion(Id)
    End Function
    Public Function inv_ActualizarFormulas(ByRef Header As inv_Formulas, ByRef Detalle As List(Of inv_FormulasDetalle)) As String
        Return dl.inv_ActualizarFormulas(Header, Detalle)
    End Function
    Public Function inv_VerificaProductoFormula(ByVal IdProducto As String, ByVal Tipo As Integer) As Integer
        Return dl.inv_VerificaProductoFormula(IdProducto, Tipo)
    End Function
    Public Function inv_ObtenerDetalleDocumentoFormula(ByVal IdComprobante As Integer, ByVal Tabla As String) As DataTable
        Return dl.inv_ObtenerDetalleDocumentoFormula(IdComprobante, Tabla)
    End Function
    Public Function inv_BuscaProductosByLike(ByVal Codigo As String, ByVal Nombre As String) As DataTable
        Return dl.inv_BuscaProductosByLike(Codigo, Nombre)
    End Function
    Public Function inv_ObtienePrecioCosto(ByVal IdProducto As String, ByVal Fecha As DateTime) As Decimal
        Return dl.inv_ObtienePrecioCosto(IdProducto, Fecha)
    End Function
    Public Function inv_ObtieneTipoProducto(ByVal IdProducto As String) As Integer
        Return dl.inv_ObtieneTipoProducto(IdProducto)
    End Function
    Public Function inv_ObtenerGastosProduccionProd(ByVal Id As Integer, ByVal IdProducto As String) As DataTable
        Return dl.inv_ObtenerGastosProduccionProd(Id, IdProducto)
    End Function
    Public Function inv_ProductoEsCompuesto(ByVal IdProducto As String) As Boolean
        Return dl.inv_ProductoEsCompuesto(IdProducto)
    End Function
    Public Function inv_ReplicaProductos(ByVal entProducto As inv_Productos, ByVal Operacion As String, ByVal IdProducto As String) As String
        Return dl.inv_ReplicaProductos(entProducto, Operacion, IdProducto)
    End Function
    Public Sub inv_ActualizaUltimoCorrelativoProducto(ByVal IdGrupo As Integer)
        dl.inv_ActualizaUltimoCorrelativoProducto(IdGrupo)
    End Sub
    Public Function inv_GeneralesProducto(ByVal IdProducto As String) As DataTable
        Return dl.inv_GeneralesProducto(IdProducto)
    End Function
    Public Function inv_ListaDeProductos(ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer) As DataTable
        Return dl.inv_ListaDeProductos(IdGrupo, IdSubGrupo)
    End Function
    Public Function inv_ListaDeProductosCategorias(ByVal IdCategoria As Integer, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal IdBodega As Integer) As DataTable
        Return dl.inv_ListaDeProductosCategorias(IdCategoria, IdGrupo, IdSubGrupo, IdBodega)
    End Function
    Public Function inv_ListaDeProductosMaximosMinimos(ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal Hasta As Date, ByVal TipoReporte As Integer, ByVal SoloAplican As Boolean) As DataTable
        Return dl.inv_ListaDeProductosMaximosMinimos(IdGrupo, IdSubGrupo, Hasta, TipoReporte, SoloAplican)
    End Function
    Public Function inv_ActualizaPrecios(ByVal IdProducto As String, ByVal Detalle As List(Of inv_ProductosPrecios)) As String
        Return dl.inv_ActualizarPrecios(IdProducto, Detalle)
    End Function
    Public Function inv_VerificaCodigoProducto(ByVal IdProducto As String) As Boolean
        Return dl.inv_VerificaCodigoProducto(IdProducto)
    End Function
    Public Function inv_ObtieneExistenciaProducto(ByVal IdProducto As String, ByVal IdBodega As Integer, ByVal Fecha As Date) As DataTable
        Return dl.inv_ObtieneExistenciaProducto(IdProducto, IdBodega, Fecha)
    End Function
    Public Function inv_ObtieneExistenciaProductoTraslado(ByVal IdProducto As String, ByVal IdBodega As Integer, ByVal Fecha As Date) As DataTable
        Return dl.inv_ObtieneExistenciaProductoTraslado(IdProducto, IdBodega, Fecha)
    End Function
    Public Function inv_ObtieneExistencia(ByVal IdProducto As String, ByVal Fecha As DateTime, ByVal IdBodega As Integer) As Decimal
        Return dl.inv_ObtieneExistencia(IdProducto, Fecha, IdBodega)
    End Function
    Public Function inv_ObtieneTomaFisica(ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal IdBodega As Integer, ByVal Fecha As DateTime) As DataTable
        Return dl.inv_ObtieneTomaFisica(IdGrupo, IdSubGrupo, IdBodega, Fecha)
    End Function
    Public Function inv_ObtienePrecioProducto(ByVal IdProducto As String, ByVal IdPrecio As Integer) As Decimal
        Return dl.inv_ObtienePrecioProducto(IdProducto, IdPrecio)
    End Function

    Public Function inv_ObtieneDescuentoMargen(ByVal Cantidad As Decimal) As Decimal
        Return dl.inv_ObtieneDescuentoMargen(Cantidad)
    End Function


    Public Function inv_ObtenerEsComponente(ByVal IdProducto As String) As DataTable
        Return dl.inv_ObtenerEsComponente(IdProducto)
    End Function
    Public Function inv_ObtenerComisionProducto(ByVal IdProducto As String) As Decimal
        Return dl.inv_ObtenerComisionProducto(IdProducto)
    End Function
    Public Function inv_ReporteKardex(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProducto As String, ByVal IdBodega As Integer, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer) As DataTable
        Return dl.inv_ReporteKardex(Desde, Hasta, IdProducto, IdBodega, IdGrupo, IdSubGrupo)
    End Function
    Public Function inv_KardexBodega(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProducto As String, ByVal IdBodega As Integer) As DataTable
        Return dl.inv_KardexBodega(Desde, Hasta, IdProducto, IdBodega)
    End Function
    Public Function inv_ObtienePrecios(ByVal IdProducto As String, ByVal Usuario As String, pnIVA As Decimal) As DataTable
        Return dl.inv_ObtienePrecios(IdProducto, Usuario, pnIVA)
    End Function
    Public Function inv_VerificaPrecioUsuario(ByVal IdUsuario As String, ByVal IdPrecio As Integer) As Boolean
        Return dl.inv_VerificaPrecioUsuario(IdUsuario, IdPrecio)
    End Function
    Public Function inv_ObtenerProductosKit(ByVal IdProducto As String) As DataTable
        Return dl.inv_ObtenerProductosKit(IdProducto)
    End Function
    Public Function inv_InsertarProducciones(ByRef Header As inv_Producciones, ByRef Detalle As List(Of inv_ProduccionesDetalle) _
 , ByRef DetGastos As List(Of inv_ProduccionesGastosDetalle), ByRef DetGastosProd As List(Of inv_ProduccionesGastosProductos)) As String
        Return dl.inv_InsertarProducciones(Header, Detalle, DetGastos, DetGastosProd)
    End Function
    Public Function inv_ActualizarProducciones(ByRef Header As inv_Producciones, ByRef Detalle As List(Of inv_ProduccionesDetalle) _
 , ByRef DetGastos As List(Of inv_ProduccionesGastosDetalle), ByRef DetGastosProd As List(Of inv_ProduccionesGastosProductos)) As String
        Return dl.inv_ActualizarProducciones(Header, Detalle, DetGastos, DetGastosProd)
    End Function
    Public Function inv_InsertaProductoKit(ByVal IdProducto As String, ByRef ProductoKit As List(Of inv_ProductosKit)) As String
        Return dl.inv_InsertaProductoKit(IdProducto, ProductoKit)
    End Function
    Public Function inv_ObtieneExistenciaProductoFormulas(ByVal IdProducto As String, ByVal IdBodega As Integer, ByVal Fecha As DateTime) As DataTable
        Return dl.inv_ObtieneExistenciaProductoFormulas(IdProducto, IdBodega, Fecha)
    End Function
    Public Function inv_ObtieneExistenciaKit(ByVal IdProducto As String, ByVal IdBodega As Integer, ByVal Fecha As DateTime) As DataTable
        Return dl.inv_ObtieneExistenciaKit(IdProducto, IdBodega, Fecha)
    End Function
    Public Function inv_ObtieneExistenciaFormulas(ByVal IdProducto As String, ByVal IdBodega As Integer, ByVal Fecha As DateTime) As DataTable
        Return dl.inv_ObtieneExistenciaFormulas(IdProducto, IdBodega, Fecha)
    End Function
    Public Function inv_ObtienePrecioVenta(ByVal IdProducto As String, ByVal IdPrecio As Integer) As Decimal
        Return dl.inv_ObtienePrecioVenta(IdProducto, IdPrecio)
    End Function
    Public Function inv_ObtenerGastosProduccionProdEstructura() As DataTable
        Return dl.inv_ObtenerGastosProduccionProdEstructura()
    End Function
    'Public Function inv_SaldosBodega(ByVal IdBodega As Integer, ByVal IdProducto As String) As DataTable
    '    Return dl.inv_SaldosBodega(IdBodega, IdProducto)
    'End Function
    Public Function inv_ObtieneSaldosPorProducto(ByVal IdBodega As Integer, ByVal IdProducto As String) As DataTable
        Return dl.inv_ObtieneSaldosPorProducto(IdBodega, IdProducto)
    End Function
    Public Function inv_InventarioValuado(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdSucursal As Integer, ByVal IdSubGrupo As Integer, ByVal Archivo As Boolean, ByVal IdCategoria As Integer) As DataTable
        Return dl.inv_InventarioValuado(Desde, Hasta, IdBodega, IdSucursal, IdSubGrupo, Archivo, IdCategoria)
    End Function
    Public Function inv_InventarioValuadoProveedor(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdSucursal As Integer, ByVal IdSubGrupo As Integer, ByVal Archivo As Boolean, ByVal IdCategoria As Integer, ByVal IdProveedor As String) As DataTable
        Return dl.inv_InventarioValuadoProveedor(Desde, Hasta, IdBodega, IdSucursal, IdSubGrupo, Archivo, IdCategoria, IdProveedor)
    End Function


    Public Function inv_InventarioSinMovimiento(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProducto As String, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer) As DataTable
        Return dl.inv_InventarioSinMovimiento(Desde, Hasta, IdProducto, IdGrupo, IdSubGrupo)
    End Function
    Public Function inv_InventarioUnidades(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal IdCategoria As Integer) As DataTable
        Return dl.inv_InventarioUnidades(Desde, Hasta, IdBodega, IdGrupo, IdSubGrupo, IdCategoria)
    End Function
    Public Function inv_InventarioTomaFisica(ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer) As DataTable
        Return dl.inv_InventarioTomaFisica(Hasta, IdBodega, IdGrupo, IdSubGrupo)
    End Function
    Public Function inv_InventarioTomaFisica(ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdProveedor As Integer) As DataTable
        Return dl.inv_InventarioTomaFisica(Hasta, IdBodega, IdProveedor)
    End Function
    Public Function inv_UtilidadesVentas(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer) As DataTable
        Return dl.inv_UtilidadesVentas(Desde, Hasta, IdBodega)
    End Function
    Public Function inv_spVentasCostosPorDoc(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer) As DataTable
        Return dl.inv_spVentasCostosPorDoc(Desde, Hasta, IdBodega)
    End Function
    Public Function inv_spRptGiroProducto(ByVal IdProducto As String, ByVal Annio As Integer, ByVal Mes As Integer) As DataTable
        Return dl.inv_spRptGiroProducto(IdProducto, Annio, Mes)
    End Function
    Public Function inv_UtilidadesVentasdetalle(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdCliente As String, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal PorGrupo As Boolean, ByVal Utilidad As Decimal) As DataTable
        Return dl.inv_UtilidadesVentasdetalle(Desde, Hasta, IdBodega, IdCliente, IdGrupo, IdSubGrupo, PorGrupo, Utilidad)
    End Function
    Public Function inv_CargosAbonos(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer) As DataTable
        Return dl.inv_CargosAbonos(Desde, Hasta, IdBodega)
    End Function
    Public Function inv_InsertarCategoria(ByRef entCategoria As inv_Categorias, ByRef Detalle As List(Of inv_CategoriasDetalle)) As String
        Return dl.inv_InsertarCategorias(entCategoria, Detalle)
    End Function
    Public Function inv_ActualizarCategoria(ByRef entCategoria As inv_Categorias, ByRef Detalle As List(Of inv_CategoriasDetalle)) As String
        Return dl.inv_ActualizarCategorias(entCategoria, Detalle)
    End Function
    Public Function inv_VerificaProductoExiste(ByRef IdProducto As String) As String
        Return dl.inv_VerificaProductoExiste(IdProducto)
    End Function
    Public Function inv_InsertaProductosMasivos(ByRef DetalleProductos As List(Of inv_Productos), ByRef DetalleProductosPrecios As List(Of inv_ProductosPrecios)) As String
        Return dl.inv_InsertaProductosMasivos(DetalleProductos, DetalleProductosPrecios)
    End Function
    'ENTRADAS DE INVENTARIO
    Public Function inv_InsertarEntrada(ByRef Header As inv_Entradas, ByRef Detalle As List(Of inv_EntradasDetalle)) As String
        Return dl.inv_InsertarEntrada(Header, Detalle)
    End Function
    Public Function inv_ActualizarEntrada(ByRef Header As inv_Entradas, ByRef Detalle As List(Of inv_EntradasDetalle)) As String
        Return dl.inv_ActualizarEntrada(Header, Detalle)
    End Function
    Public Function inv_ObtenerIdDocumento(ByVal Id As Integer, ByVal TipoAvance As Integer, ByVal Tabla As String) As Integer
        Return dl.inv_ObtenerIdDocumento(Id, TipoAvance, Tabla)
    End Function
    'Public Function inv_ObtenerIdEntrada(ByVal Id As Integer, ByVal TipoAvance As Integer) As Integer
    '    Return dl.inv_ObtenerIdEntrada(Id, TipoAvance)
    'End Function
    'Public Function inv_ObtenerDetalleEntrada(ByVal IdComprobante As Integer) As DataTable
    '    Return dl.inv_ObtenerDetalleEntrada(IdComprobante)
    'End Function

    'SALIDAS DE INVENTARIO
    Public Function inv_InsertarSalida(ByRef Header As inv_Salidas, ByRef Detalle As List(Of inv_SalidasDetalle), ByVal AutorizoReversion As String) As String
        Return dl.inv_InsertarSalida(Header, Detalle, AutorizoReversion)
    End Function
    Public Function inv_ActualizarSalida(ByRef Header As inv_Salidas, ByRef Detalle As List(Of inv_SalidasDetalle), ByVal AutorizoReversion As String) As String
        Return dl.inv_ActualizarSalida(Header, Detalle, AutorizoReversion)
    End Function
    'Public Function inv_ObtenerIdSalida(ByVal Id As Integer, ByVal TipoAvance As Integer) As Integer
    '    Return dl.inv_ObtenerIdSalida(Id, TipoAvance)
    'End Function
    Public Function inv_ObtenerDetalleDocumento(ByVal IdComprobante As Integer, ByVal Tabla As String) As DataTable
        Return dl.inv_ObtenerDetalleDocumento(IdComprobante, Tabla)
    End Function
    Public Function inv_ProrrateoProduccion(ByVal IdComprobante As Integer) As DataTable
        Return dl.inv_ProrrateoProduccion(IdComprobante)
    End Function
    'TRASLADOS DE INVENTARIO
    Public Function inv_InsertarTraslado(ByRef Header As inv_Traslados, ByRef Detalle As List(Of inv_TrasladosDetalle)) As String
        Return dl.inv_InsertarTraslado(Header, Detalle)
    End Function
    Public Function inv_ActualizaPedidoTraslado(ByVal Idtraslado As Integer, ByVal IdPedido As Integer, ByVal Aplicacion As Integer) As String
        Return dl.inv_ActualizaPedidoTraslado(Idtraslado, IdPedido, Aplicacion)
    End Function
    Public Function inv_ActualizarTraslado(ByRef Header As inv_Traslados, ByRef Detalle As List(Of inv_TrasladosDetalle)) As String
        Return dl.inv_ActualizarTraslado(Header, Detalle)
    End Function
    Public Function inv_ObtenerIdTraslado(ByVal Id As Integer, ByVal TipoAvance As Integer, ByVal Tabla As String) As Integer
        Return dl.inv_ObtenerIdDocumento(Id, TipoAvance, Tabla)
    End Function

    Public Function inv_ObtenerDataInventario(ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Return dl.inv_ObtenerDataInventario(Desde, Hasta)
    End Function
    Public Function inv_ObtenerMovimientos(ByVal Desde As Date, ByVal Hasta As Date, ByVal Tipo As Integer, ByVal IdCento As String) As DataTable
        Return dl.inv_ObtenerMovimientos(Desde, Hasta, Tipo, IdCento)
    End Function
    Public Function inv_AutorizoReversionInventario(ByVal IdUsuario As String, ByVal Tipo As String, ByVal Numero As String, ByVal Id As Integer) As String
        Return dl.inv_AutorizoReversionInventario(IdUsuario, Tipo, Numero, Id)
    End Function

    Public Function inv_AplicarTransaccionInventario(ByVal Id As Integer, ByVal Modulo As String, ByVal Tipo As String, ByVal IdTipoComprobante As Integer, ByVal TipoTraslado As Integer) As String
        Return dl.inv_AplicarTransaccionInventario(Id, Modulo, Tipo, IdTipoComprobante, TipoTraslado)
    End Function

    Public Function inv_ActualizaPreciosVentaImportacion_CambieNombre(ByVal Id As Integer) As String
        Return dl.inv_ActualizaPreciosVentaImportacion(Id)
    End Function

    Public Function ExisteNumeroTraslado(ByVal Numero As String) As Integer
        Return dl.ExisteNumeroTraslado(Numero)
    End Function
    Public Function inv_ObtieneTomaFisicaEstructura() As DataTable
        Return dl.inv_ObtieneTomaFisicaEstructura()
    End Function
    Public Function inv_ObtieneCreaProdcutosEstructura() As DataTable
        Return dl.inv_ObtieneCreaProdcutosEstructura()
    End Function
    Public Function inv_ObtieneExistenciaCostoProducto(ByVal IdProducto As String, ByVal IdBodega As Integer) As DataTable
        Return dl.inv_ObtieneExistenciaCostoProducto(IdProducto, IdBodega)
    End Function

    Public Function inv_Contabilizar(ByVal Desde As Date, ByVal Hasta As Date, ByVal TipoPartida As String, ByVal IdSucursal As Integer, ByVal Usuario As String, ByVal Concepto As String) As String
        Return dl.inv_contabilizar(Desde, Hasta, TipoPartida, IdSucursal, Usuario, Concepto)
    End Function

    Public Function inv_RecalculaCostosExistencias(IdBodega As Integer, Hasta As Date, Tipo As Int16) As String
        Return dl.inv_RecalculaCostosExistencias(IdBodega, Hasta, Tipo)
    End Function
#Region "Produccion"
    Public Function GetOrdenManoObra(ByVal IdComprobante As Integer) As DataTable
        Return dl.GetOrdenManoObra(IdComprobante)
    End Function
    Public Function pro_ValorManoObra(ByVal Fecha1 As DateTime, ByVal Fecha2 As DateTime, ByVal IdEmpleado As Integer) As Decimal
        Return dl.pro_ValorManoObra(Fecha1, Fecha2, IdEmpleado)
    End Function
    Public Function pro_InsertarOrdenesProduccion(ByRef entOrden As pro_OrdenesProduccion, ByRef entDetalle As List(Of pro_OrdenesProduccionDetalleManoObra), ByRef entDet1 As List(Of pro_OrdenesGasDirectos), ByRef entDet2 As List(Of pro_OrdenesGasIndirectos)) As String
        Return dl.pro_InsertarOrdenesProduccion(entOrden, entDetalle, entDet1, entDet2)
    End Function
    Public Function pro_ActualizarOrdenesProduccion(ByRef entOrden As pro_OrdenesProduccion, ByRef entDetalle As List(Of pro_OrdenesProduccionDetalleManoObra), ByRef entDet1 As List(Of pro_OrdenesGasDirectos), ByRef entDet2 As List(Of pro_OrdenesGasIndirectos)) As String
        Return dl.pro_ActualizarOrdenProduccion(entOrden, entDetalle, entDet1, entDet2)
    End Function
    Public Function pro_ConsultaOrdenes() As DataSet
        Return dl.pro_ConsultaOrdenes()
    End Function
    Public Function pro_ConsultaSalidas() As DataSet
        Return dl.pro_ConsultaSalidas()
    End Function
    Public Function pro_ConsultaEntradas() As DataSet
        Return dl.pro_ConsultaEntradas()
    End Function
    Public Function pro_ConsultaOrdenEmpaque() As DataSet
        Return dl.pro_ConsultaOrdenEmpaque()
    End Function
    Public Function pro_InsertarSalida(ByRef Header As pro_SalidasProduccion, ByRef Detalle As List(Of pro_SalidasProduccionDetalle)) As String
        Return dl.pro_InsertarSalida(Header, Detalle)
    End Function
    Public Function pro_ActualizarSalida(ByRef Header As pro_SalidasProduccion, ByRef Detalle As List(Of pro_SalidasProduccionDetalle)) As String
        Return dl.pro_ActualizarSalida(Header, Detalle)
    End Function
    Public Function pro_InsertarEntrada(ByRef Header As pro_EntradasProduccion, ByRef Detalle As List(Of pro_EntradasProduccionDetalle)) As String
        Return dl.pro_InsertarEntrada(Header, Detalle)
    End Function
    Public Function pro_ActualizarEntrada(ByRef Header As pro_EntradasProduccion, ByRef Detalle As List(Of pro_EntradasProduccionDetalle)) As String
        Return dl.pro_ActualizarEntrada(Header, Detalle)
    End Function
    Public Function pro_ObtenerDetalleSalidas(ByVal IdComprobante As Integer) As DataTable
        Return dl.pro_ObtenerDetalleSalidas(IdComprobante)
    End Function
    Public Function pro_InsertarOrdenesEmpaque(ByRef Header As pro_OrdenesEmpaque, ByRef Detalle1 As List(Of pro_OrdenesEmpaqueDetalleSalida), ByRef Detalle2 As List(Of pro_OrdenesEmpaqueDetalleEntrada)) As String
        Return dl.pro_InsertarOrdenesEmpaque(Header, Detalle1, Detalle2)
    End Function
    Public Function pro_ActualizarOrdenesEmpaque(ByRef Header As pro_OrdenesEmpaque, ByRef Detalle1 As List(Of pro_OrdenesEmpaqueDetalleSalida), ByRef Detalle2 As List(Of pro_OrdenesEmpaqueDetalleEntrada)) As String
        Return dl.pro_ActualizarOrdenesEmpaque(Header, Detalle1, Detalle2)
    End Function
#End Region
End Class
