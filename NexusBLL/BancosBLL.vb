﻿Imports NexusDLL
Imports NexusELL.TableEntities

Public Class BancosBLL
    Dim dl As BancosDLL
    Public Sub New(ByVal sConnectionString As String)
        dl = New BancosDLL(sConnectionString)
    End Sub
    Public Function ConsultaBancos() As DataSet
        Return dl.ConsultaBancos()
    End Function
    Public Function ConsultaTransacciones(ByVal usuario As String, ByVal Fecha As String) As DataSet
        Return dl.ConsultaTransacciones(usuario, Fecha)
    End Function
    Public Function ConsultaCheques(ByVal Usuario As String, ByVal Fecha As String) As DataSet
        Return dl.ConsultaCheques(Usuario, Fecha)
    End Function
    Public Function ActualizaDetalleConciliacion(ByVal DetalleConcilia As List(Of ban_ConciliacionesDetalle), ByVal IdCuentaBancaria As Integer, ByVal IdSucursal As Integer, ByVal Mes As Integer, ByVal Anio As Integer, ByVal Fecha As Date, ByVal Cheques As Boolean, ByVal CreadoPor As String) As String
        Return dl.ActualizaDetalleConciliacion(DetalleConcilia, IdCuentaBancaria, IdSucursal, Mes, Anio, Fecha, Cheques, CreadoPor)
    End Function
    Public Function ActualizaCompraLiquidada(ByVal IdComprobante As Integer, ByVal IdCheque As Integer) As Integer
        Return dl.ActualizaCompraLiquidada(IdComprobante, IdCheque)
    End Function
    Public Function GetConciliaRegistrosBancos(ByVal IdCuentaBancaria As Integer, ByVal fecha As Date, ByVal Todos As Boolean, ByVal IdSucursal As Integer) As DataSet
        Return dl.GetConciliaRegistrosBancos(IdCuentaBancaria, fecha, Todos, IdSucursal)
    End Function
    Public Function AnulacionChequeCerrado(ByVal iIdCheque As Integer, ByVal iIdTransaccion As Integer, ByVal iIdTipoTrans As Integer, ByVal sIdTipoPartida As String, ByVal iIdPartida As Integer, ByVal dFecha As Date, ByVal sMotivo As String, ByVal sUsuario As String) As Integer
        Return dl.AnulacionChequeCerrado(iIdCheque, iIdTransaccion, iIdTipoTrans, sIdTipoPartida, iIdPartida, dFecha, sMotivo, sUsuario)
    End Function
    Public Function AnulacionChequeAbierto(ByVal iIdCheque As Integer, ByVal sUsuario As String, ByVal dFecha As Date, ByVal sMotivo As String) As Integer
        Return dl.AnulacionChequeAbierto(iIdCheque, sUsuario, dFecha, sMotivo)
    End Function
    Public Function GetCuentaCbleBanco(ByVal IdCuentaBancaria As Integer) As String
        Return dl.GetCuentaCbleBanco(IdCuentaBancaria)
    End Function
    Public Function GetBloqueCheques(ByVal DesdeCheque As Integer, ByVal HastaCheque As Integer, ByVal IdCuenta As String) As DataTable
        Return dl.GetBloqueCheques(DesdeCheque, HastaCheque, IdCuenta)
    End Function
    Public Function GetCuentaBancaria(ByVal IdCuentaBancaria As Integer) As DataTable
        Return dl.GetCuentaBancaria(IdCuentaBancaria)
    End Function
    Public Function GetSaldoCuentaBanco(ByVal sIdCuenta As String, ByVal fecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return dl.GetSaldoCuentaBanco(sIdCuenta, fecha, IdSucursal)
    End Function
    Public Function GetCargosPendientes(ByVal cuenta_contable As String, ByVal fecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return dl.GetCargosPendientes(cuenta_contable, fecha, IdSucursal)
    End Function
    Public Function GetCargosNoContab(ByVal cuenta_contable As String, ByVal fecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return dl.GetCargosNoContab(cuenta_contable, fecha, IdSucursal)
    End Function
    Public Function GetDepositosTransito(ByVal cuenta_contable As String, ByVal fecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return dl.GetDepositosTransito(cuenta_contable, fecha, IdSucursal)
    End Function
    Public Function GetChequesPendientes(ByVal cuenta_contable As String, ByVal fecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return dl.GetChequesPendientes(cuenta_contable, fecha, IdSucursal)
    End Function
    Public Function GetDepositosNoContab(ByVal cuenta_contable As String, ByVal fecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return dl.GetDepositosNoContab(cuenta_contable, fecha, idsucursal)
    End Function
    Public Sub ActualizaMarcaProcesado(ByVal IdTransaccion As Integer, ByVal Procesado As Integer)
        dl.ActualizaMarcaProcesado(IdTransaccion, Procesado)
    End Sub
    Public Function GetConciliaCheques(ByVal IdCuentaBancaria As Integer, ByVal fecha As Date, ByVal Todos As Boolean, ByVal IdSucursal As Integer) As DataTable
        Return dl.GetConciliaCheques(IdCuentaBancaria, fecha, Todos, IdSucursal)
    End Function
    Public Function GetChequesAutoriza(ByVal IdCuentaBancaria As Integer, ByVal fecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.GetChequesAutoriza(IdCuentaBancaria, fecha, IdSucursal)
    End Function
    Public Function GetConciliaTrans(ByVal IdCuentaBancaria As Integer, ByVal fecha As Date, ByVal Todos As Boolean, ByVal IdSucursal As Integer) As DataTable
        Return dl.GetConciliaTrans(IdCuentaBancaria, fecha, Todos, IdSucursal)
    End Function
    Public Sub ActualizaMarcaConciliado(ByVal IdCheque As Integer, ByVal Conciliado As Integer)
        dl.ActualizaMarcaConciliado(IdCheque, Conciliado)
    End Sub
    Public Sub AutorizaCheques(ByVal IdCheque As Integer, ByVal AutorizadoPor As String)
        dl.AutorizaCheques(IdCheque, AutorizadoPor)
    End Sub
    Public Function GetCuentasBanco(ByVal IdBanco As Integer) As DataTable
        Return dl.GetCuentasBanco(IdBanco)
    End Function
    Public Sub GeneraConciliacion(ByVal sCtaBanco, ByVal sCtaContable, ByVal dtFecha _
                              , ByVal dSaldoContab, ByVal dCargosNoCon _
                              , ByVal dDepositosNC, ByVal dChequesPend _
                              , ByVal dCargosPend, ByVal dDepositosTr, ByVal deSaldoBanco, ByVal IdSucursal)
        dl.GeneraConciliacion(sCtaBanco, sCtaContable, dtFecha, dSaldoContab, dCargosNoCon _
                              , dDepositosNC, dChequesPend, dCargosPend, dDepositosTr _
                              , deSaldoBanco, IdSucursal)
    End Sub
    Public Function ExisteCheque(ByVal IdCuentaBancaria As Integer, ByVal sNumero As String) As Integer
        Return dl.existeCheque(IdCuentaBancaria, sNumero)
    End Function
    Public Function ban_InsertarCheque(ByVal Chq As ban_Cheques, ByVal Det As List(Of ban_ChequesDetalle), EnLinea As Boolean) As String
        Dim Par As New con_Partidas
        With Par
            .IdPartida = IIf(EnLinea, Chq.IdPartida, -1)  'SI EL MODULO ESTÁ DESCONECTADO DE LA CONTABILIDAD ENTONCES, LE SETEAMOS -1
            .IdTipo = Chq.IdTipoPartida
            .IdSucursal = Chq.IdSucursal
            .Numero = Chq.NumeroPartida
            .Fecha = Chq.Fecha
            .Concepto = Chq.Concepto
            .Actualizada = 0
            .IdModuloOrigen = 2
            .CreadoPor = Chq.CreadoPor
            .FechaHoraCreacion = Chq.FechaHoraCreacion
            .ModificadoPor = Chq.ModificadoPor
            .FechaHoraModificacion = Chq.FechaHoraModificacion
        End With
        Dim i As Integer = 1
        Dim ParDet As New List(Of con_PartidasDetalle)
        For Each detalleCheque As ban_ChequesDetalle In Det
            Dim entPartidaDetalle As New con_PartidasDetalle
            With entPartidaDetalle
                .IdPartida = Chq.IdPartida
                .IdDetalle = i
                .IdCuenta = detalleCheque.IdCuenta
                .Referencia = detalleCheque.Referencia
                .Concepto = detalleCheque.Concepto
                .Debe = detalleCheque.Debe
                .Haber = detalleCheque.Haber
                .IdCentro = detalleCheque.IdCentro
                .CreadoPor = detalleCheque.CreadoPor
                .FechaHoraCreacion = detalleCheque.FechaHoraCreacion
            End With
            ParDet.Add(entPartidaDetalle)
            i += 1
        Next

        Return dl.ban_InsertarCheque(Chq, Det, Par, ParDet)
    End Function
    'Public Function ban_ActualizarCheque(ByVal entCheque As ban_Cheques, ByVal detalle As List(Of ban_ChequesDetalle)) As String
    '    Dim entPartida As New con_Partidas
    '    With entPartida
    '        .IdPartida = entCheque.IdPartida
    '        .IdTipo = entCheque.IdTipoPartida
    '        .IdSucursal = entCheque.IdSucursal
    '        .Numero = entCheque.NumeroPartida
    '        .Fecha = entCheque.Fecha
    '        .Concepto = entCheque.Concepto
    '        .Actualizada = 0
    '        .IdModuloOrigen = 2
    '        .CreadoPor = entCheque.CreadoPor
    '        .FechaHoraCreacion = entCheque.FechaHoraCreacion
    '        .ModificadoPor = entCheque.ModificadoPor
    '        .FechaHoraModificacion = entCheque.FechaHoraModificacion
    '    End With
    '    Dim i As Integer = 1
    '    Dim DetallePartidas As New List(Of con_PartidasDetalle)
    '    For Each detalleCheque As ban_ChequesDetalle In detalle
    '        Dim entPartidaDetalle As New con_PartidasDetalle
    '        With entPartidaDetalle
    '            .IdPartida = entCheque.IdPartida
    '            .IdDetalle = i
    '            .IdCuenta = detalleCheque.IdCuenta
    '            .Referencia = detalleCheque.Referencia
    '            .Concepto = detalleCheque.Concepto
    '            .Debe = detalleCheque.Debe
    '            .Haber = detalleCheque.Haber
    '            .IdCentro = detalleCheque.IdCentro
    '            .CreadoPor = detalleCheque.CreadoPor
    '            .FechaHoraCreacion = detalleCheque.FechaHoraCreacion
    '        End With
    '        DetallePartidas.Add(entPartidaDetalle)
    '        i += 1
    '    Next

    '    Return dl.ActualizarCheque(entCheque, detalle, entPartida, DetallePartidas)
    'End Function
    Public Function GetChequesDetalle(ByVal IdCheque As Integer) As DataTable
        Return dl.GetChequesDetalle(IdCheque)
    End Function
    Public Function GetTransaccionDetalle(ByVal idTrans As Integer) As DataTable
        Return dl.GetTransaccionDetalle(idTrans)
    End Function
    Public Function GetNumeroCheque(ByVal IdCuentaBancaria As Integer) As String
        Return dl.GetNumeroCheque(IdCuentaBancaria)
    End Function
    Public Function GetIdFormatoCheque(ByVal IdCuentaBancaria As Integer) As Integer
        Return dl.GetIdFormatoCheque(IdCuentaBancaria)
    End Function
    Public Function ActualizarTransaccion(ByVal entTrans As ban_Transacciones, ByVal detalle As List(Of ban_TransaccionesDetalle), EnLinea As Boolean) As String

        entTrans.Contabilizar = EnLinea  'SE SETEA SI EL MODULO ESTÁ EN LÍNEA

        Dim entPartida As New con_Partidas
        With entPartida
            .IdPartida = entTrans.IdPartida
            .IdTipo = entTrans.IdTipoPartida
            .IdSucursal = entTrans.IdSucursal
            .Numero = entTrans.NumeroPartida
            .Fecha = entTrans.FechaContable  'entTrans.Fecha
            .Concepto = entTrans.Concepto
            .Actualizada = 0
            .IdModuloOrigen = 2
            .CreadoPor = entTrans.CreadoPor
            .FechaHoraCreacion = entTrans.FechaHoraCreacion
            .ModificadoPor = entTrans.ModificadoPor
            .FechaHoraModificacion = entTrans.FechaHoraModificacion
        End With
        Dim i As Integer = 1
        Dim DetallePartidas As New List(Of con_PartidasDetalle)
        For Each detalleTrans As ban_TransaccionesDetalle In detalle
            Dim entPartidaDetalle As New con_PartidasDetalle
            With entPartidaDetalle
                .IdPartida = entTrans.IdPartida
                .IdDetalle = i
                .IdCuenta = detalleTrans.IdCuenta
                .Referencia = detalleTrans.Referencia
                .Concepto = detalleTrans.Concepto
                .Debe = detalleTrans.Debe
                .Haber = detalleTrans.Haber
                .CreadoPor = detalleTrans.CreadoPor
                .IdCentro = detalleTrans.IdCentro
                .FechaHoraCreacion = detalleTrans.FechaHoraCreacion
            End With
            DetallePartidas.Add(entPartidaDetalle)
            i += 1
        Next

        Return dl.ActualizarTransaccion(entTrans, detalle, entPartida, DetallePartidas)
    End Function
    Public Function InsertarTransaccion(ByVal entTrans As ban_Transacciones, ByVal detalle As List(Of ban_TransaccionesDetalle), EnLinea As Boolean) As String
        entTrans.Contabilizar = EnLinea  'SE SETEA SI EL MODULO ESTÁ EN LÍNEA
        Dim entPartida As New con_Partidas
        With entPartida
            .IdPartida = 0
            .IdTipo = entTrans.IdTipoPartida
            .IdSucursal = entTrans.IdSucursal
            .Numero = ""
            .Fecha = entTrans.FechaContable ' cambie la fecha por fecha contable
            .Concepto = entTrans.Concepto
            .Actualizada = 0
            .IdModuloOrigen = 2
            .CreadoPor = entTrans.CreadoPor
            .FechaHoraCreacion = entTrans.FechaHoraCreacion
            .ModificadoPor = entTrans.ModificadoPor
            .FechaHoraModificacion = entTrans.FechaHoraModificacion
        End With
        Dim i As Integer = 1
        Dim DetallePartidas As New List(Of con_PartidasDetalle)
        For Each detalleTrans As ban_TransaccionesDetalle In detalle
            Dim entPartidaDetalle As New con_PartidasDetalle
            With entPartidaDetalle
                .IdPartida = 0
                .IdDetalle = i
                .IdCuenta = detalleTrans.IdCuenta
                .Referencia = detalleTrans.Referencia
                .Concepto = detalleTrans.Concepto
                .Debe = detalleTrans.Debe
                .Haber = detalleTrans.Haber
                .IdCentro = detalleTrans.IdCentro
                .CreadoPor = detalleTrans.CreadoPor
                .FechaHoraCreacion = detalleTrans.FechaHoraCreacion
            End With
            DetallePartidas.Add(entPartidaDetalle)
            i += 1
        Next
        Return dl.InsertarTransaccion(entTrans, detalle, entPartida, DetallePartidas)
    End Function
    Public Function ConsultaTransacciones() As DataSet
        Return dl.ConsultaTransacciones()
    End Function
    Function GetChequesPorConciliar(ByVal IdCuentaBancaria As Integer) As DataTable
        Return dl.GetChequesPorConciliar(IdCuentaBancaria)
    End Function
    Public Sub ActualizaChequeConciliado(ByVal IdCheque As Integer)
        dl.ActualizaChequeConciliado(IdCheque)
    End Sub
    Function ban_ObtenerIdCuentaBancaria(ByVal IdCta As Integer, ByVal Tipo As Integer) As Integer
        Return dl.ban_ObtenerIdCuentaBancaria(IdCta, Tipo)
    End Function
    Function ObtieneIdCheque(ByVal iIdCheque As Integer, ByVal iTipo As Integer) As Integer
        Return dl.GetIdCheque(iIdCheque, iTipo)
    End Function
    Function ObtieneIdTransac(ByVal iIdTransac As Integer, ByVal iTipo As Integer) As Integer
        Return dl.GetIdTransac(iIdTransac, iTipo)
    End Function
    Public Function ObtieneTransaccionByPk(ByVal IdTransaccion As Integer)
        Return dl.ObtieneTransaccionByPk(IdTransaccion)
    End Function
    Function ValidaCentroCuenta(ByVal IdCuenta As String, ByVal IdCentro As String) As Integer
        Return dl.ValidaCentroCuenta(IdCuenta, IdCentro)
    End Function
    Public Function ObtienePartidaDesembolso(ByVal IdPrestamo As Integer, ByVal TipoMovimiento As Integer, ByVal IdAsociado As Integer, ByVal IdCuentaBanco As Integer, ByVal NumCheque As Integer, ByVal CreadoPor As String) As DataTable
        Return dl.ObtienePartidaDesembolso(IdPrestamo, TipoMovimiento, IdAsociado, IdCuentaBanco, NumCheque, CreadoPor)
    End Function

    Public Function ban_ContabilizarRegistros(ByVal Desde As Date, ByVal Hasta As Date, ByVal TipoPartida As String, ByVal IdSucursal As Integer, ByVal CreadoPor As String) As String
        Return dl.ban_ContabilizarRegistros(Desde, Hasta, TipoPartida, IdSucursal, CreadoPor)
    End Function


#Region "Reportes"
    Public Function ReporteCheques(ByVal IdCheque As Integer, ByVal Tipo As Integer) As DataTable
        Return dl.ReporteCheques(IdCheque, Tipo)
    End Function
    Public Function GetLibroBancos(ByVal FechaI As DateTime, ByVal FechaF As DateTime, ByVal IdCuenta As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.GetLibroBancos(FechaI, FechaF, IdCuenta, IdSucursal)
    End Function
    Public Function GetListadoCheques(ByVal FechaI As DateTime, ByVal FechaF As DateTime, ByVal IdCta As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.GetListadoCheques(FechaI, FechaF, IdCta, IdSucursal)
    End Function
    Public Function GetListadoTrans(ByVal FechaI As DateTime, ByVal FechaF As DateTime, ByVal IdCuentaBancaria As String, ByVal iIdTipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.GetListadoTrans(FechaI, FechaF, IdCuentaBancaria, iIdTipo, IdSucursal)
    End Function
    Public Function ObtieneConciliacion(ByVal iCtaBanco, ByVal dtFecha, ByVal IdSucursal) As DataTable
        Return dl.Obtieneconciliacion(iCtaBanco, dtFecha, IdSucursal)
    End Function
    Public Function ObtieneDisponibilidad(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ObtieneDisponibilidad(dFechaI, dFechaF, IdSucursal)
    End Function
    Public Function ObtieneDisponibilidadDet(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ObtieneDisponibilidadDet(dFechaI, dFechaF, IdSucursal)
    End Function
    Public Function ban_ObtieneDataBancos(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ban_ObtieneDataBancos(Desde, Hasta, IdSucursal)
    End Function
#End Region
End Class
