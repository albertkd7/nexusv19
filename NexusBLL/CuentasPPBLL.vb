﻿Imports NexusDLL
Imports NexusELL.TableEntities
Public Class CuentasPPBLL
    Dim dl As CuentasPPDLL
    Public Sub New(ByVal dateBaseName As String)
        dl = New CuentasPPDLL(dateBaseName)
    End Sub

    Public Function ObtenerSaldosCompras(ByVal IdProveedor As String, ByVal Fecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ObtenerSaldosCompras(IdProveedor, Fecha, IdSucursal)
    End Function

    Public Function cpp_GetDetalleAbono(ByVal IdComprobante As Integer) As DataTable
        Return dl.cpp_GetDetalleAbono(IdComprobante)
    End Function
    Public Function ExisteNumeroQuedan(ByVal Numero As String) As Integer
        Return dl.ExisteNumeroQuedan(Numero)
    End Function
    Public Function GetQuedan() As DataTable
        Return dl.GetQuedan()
    End Function
   
    Public Function cpp_EstadoCuenta(ByVal IdSucursal As Integer, ByVal Hasta As Date, ByVal DesdeProveedor As String, ByVal HastaProveedor As String) As DataTable
        Return dl.cpp_EstadoCuenta(IdSucursal, Hasta, DesdeProveedor, HastaProveedor)
    End Function
    Public Function AnulaQuedan(ByVal IdComprobante As Integer, ByRef UsuarioAnula As String) As String
        Return dl.AnulaQuedan(IdComprobante, UsuarioAnula)
    End Function
    Public Function GetSaldoProveedor(ByVal IdProveedor As String, ByVal HastaFecha As DateTime, ByVal IdSucursal As Integer) As Decimal
        Return dl.GetSaldoProveedor(IdProveedor, HastaFecha, IdSucursal)
    End Function
    Public Function cpp_DocumentosAnulados(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProveedor As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.cpp_DocumentosAnulados(Desde, Hasta, IdProveedor, IdSucursal)
    End Function

    Public Function GetSaldos_Notas(ByVal IdProveedor As String, ByVal Fecha As Date, ByVal IdNota As Integer) As DataTable
        Return dl.GetSaldos_Notas(IdProveedor, Fecha, IdNota)
    End Function

    Public Function cpp_ConsultaAbonosProveedor(ByVal IdProveedor As String, ByVal Desde As Date, ByVal Hasta As Date) As DataSet
        Return dl.cpp_ConsultaAbonosProveedor(IdProveedor, Desde, Hasta)
    End Function
    Public Function cpp_AplicarAbono(ByRef AbonoHeader As cpp_Abonos, ByRef AbonoDetalle As List(Of cpp_AbonosDetalle)) As String
        Return dl.cpp_AplicarAbono(AbonoHeader, AbonoDetalle)
    End Function
    Public Function cpp_AplicarTraslado(ByRef AbonoHeader As cpp_Abonos, ByRef AbonoDetalle As List(Of cpp_AbonosDetalle), ByVal TrasHeader As cpp_Traslado, ByRef TrasDetalle As List(Of cpp_TrasladoDetalle), ByRef DetalleCompra As List(Of com_Compras)) As String
        Return dl.cpp_AplicarTraslado(AbonoHeader, AbonoDetalle, TrasHeader, TrasDetalle, DetalleCompra)
    End Function
    Public Function cpp_AplicarNotaCredito(ByRef NotasDetalle As List(Of com_NotasCredito)) As String
        Return dl.cpp_AplicarNotaCredito(NotasDetalle)
    End Function


    Public Function cpp_ObtenerAbonosPendientes(ByVal IdSucursal As Integer) As DataTable
        Return dl.cpp_ObtenerAbonosPendientes(IdSucursal)
    End Function
    Public Function cpp_ObtenerEstruturaPendientes() As DataTable
        Return dl.cpp_ObtenerEstruturaPendientes()
    End Function

    Public Function cpp_InsertarQuedan(ByVal entQuedan As cpp_Quedan, ByVal Detalle As List(Of cpp_QuedanDetalle)) As String
        Return dl.cpp_InsertarQuedan(entQuedan, Detalle)
    End Function
    Public Function cpp_ActualizarQuedan(ByVal entQuedan As cpp_Quedan, ByVal Detalle As List(Of cpp_QuedanDetalle)) As String
        Return dl.cpp_ActualizarQuedan(entQuedan, Detalle)
    End Function

    Public Function cpp_EliminaNotaCredito(ByVal IdNota As Integer) As String
        Return dl.cpp_EliminaNotaCredido(IdNota)
    End Function

    Function GetQuedanDetalle(ByVal IdQuedan As Integer) As DataTable
        Return dl.GetQuedanDetalle(IdQuedan)
    End Function
    Function GetQuedanDetalleStore(ByVal IdQuedan As Integer) As DataTable
        Return dl.GetQuedanDetalleStore(IdQuedan)
    End Function

    Public Function cpp_ObtenerQuedanPendientes(ByVal IdSucursal As Integer) As DataTable
        Return dl.cpp_ObtenerQuedanPendientes(IdSucursal)
    End Function
    Public Function cpp_CancelarQuedan(ByVal IdQuedan As Integer, ByVal Fecha As DateTime) As Integer
        Return dl.cpp_CancelarQuedan(IdQuedan, Fecha)
    End Function
    Function cpp_ListadoAbonosPorProveedor(ByVal dDesde As Date, ByVal dHasta As Date, ByVal IdProveedor As String, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.getList_Abonos(dDesde, dHasta, IdProveedor, Tipo, IdSucursal)
    End Function
    Function getList_Quedan(ByVal iTipo_Fecha As Integer, ByVal iTipo_Reporte As Integer, ByVal dDesde As Date, ByVal dHasta As Date) As DataTable
        Return dl.getList_Quedan(iTipo_Fecha, iTipo_Reporte, dDesde, dHasta)
    End Function
    Function getAnalisis(ByVal sHastaProv As String, ByVal dHastaFecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.getAnalisis(sHastaProv, dHastaFecha, IdSucursal)
    End Function
    Function cpp_ResumenSaldosProveedor(IdSucursal As Integer, ByVal Hasta As Date) As DataTable
        Return dl.cpp_ResumenSaldosProveedor(IdSucursal, Hasta)
    End Function
    Function getAnalisisDetalle(ByVal sDesdeProv As String, ByVal sHastaProv As String, ByVal dHastaFecha As Date) As DataTable
        Return dl.getAnalisisDetalle(sDesdeProv, sHastaProv, dHastaFecha)
    End Function
    Function getAuxiliar(ByVal sHastaProv As String, ByVal dHastaFecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.getAuxiliar(sHastaProv, dHastaFecha, IdSucursal)
    End Function
    Function cpp_MovimientosProveedor(ByVal IdProveedor As String, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.cpp_MovimientosProveedor(IdProveedor, Desde, Hasta, IdSucursal)
    End Function
    Function cpp_ObtenerIdQuedan(ByVal IdQuedan As Integer, ByVal TipoAvance As Integer) As Integer
        Return dl.cpp_ObtenerIdQuedan(IdQuedan, TipoAvance)
    End Function
    Function cpp_ObtenerUltAbono(ByVal IdProveedor As String) As Integer
        Return dl.cpp_ObtenerUltAbono(IdProveedor)
    End Function
    Function cpp_AnulaAbono(ByVal IdComprobante As Integer, ByVal IdCheque As Integer, ByVal IdTransaccion As Integer, ByVal AnuladoPor As String, ByVal Eliminar As Integer) As String
        Return dl.cpp_AnulaAbono(IdComprobante, IdCheque, IdTransaccion, AnuladoPor, Eliminar)
    End Function
    Function cpp_AutorizaAbonosCheque(ByVal IdComprobante As Integer, ByVal IdCheque As Integer, ByVal Usuario As String) As Integer
        Return dl.cpp_AutorizaAbonosCheque(IdComprobante, IdCheque, Usuario)
    End Function

    Function cpp_AutorizaAbonosTransaccion(ByVal IdComprobante As Integer, ByVal IdTransaccion As Integer, ByVal Usuario As String) As Integer
        Return dl.cpp_AutorizaAbonosTransaccion(IdComprobante, IdTransaccion, Usuario)
    End Function

    Function cpp_AutorizaAbonosSinTransaccion(ByVal IdComprobante As Integer, ByVal Usuario As String) As Integer
        Return dl.cpp_AutorizaAbonosSinTransaccion(IdComprobante, Usuario)
    End Function
    Function cpp_InsertaPartida(ByRef entPartida As con_Partidas, ByRef entDetalle As List(Of con_PartidasDetalle)) As String
        Return dl.cpp_InsertaPartida(entPartida, entDetalle)
    End Function

    Function cpp_ObtenerDocumentosAbonados(ByVal sIdProveedor As String, ByVal IdComprobante As Integer) As String
        Return dl.cpp_ObtenerDocumentosAbonados(sIdProveedor, IdComprobante)
    End Function
    Function cpp_ObtenerAbonosPorProveedor(ByVal dDesde As Date, ByVal dHasta As Date, ByVal IdProveedor As String) As DataTable
        Return dl.cpp_ObtenerAbonosPorProveedor(dDesde, dHasta, IdProveedor)
    End Function
End Class
