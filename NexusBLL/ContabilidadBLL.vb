﻿Imports NexusDLL
Imports NexusELL.TableEntities
Public Class ContabilidadBLL
    Dim dl As ContabilidadDLL
    Public Sub New(ByVal dateBaseName As String)
        dl = New ContabilidadDLL(dateBaseName)
    End Sub

    Public Function rptIndicador(ByVal IdFormula As Integer, ByVal mes As Integer, ByVal Anio As Integer, ByVal Tipo As Integer) As Decimal
		Return dl.rptIndicador(IdFormula, mes, Anio, Tipo)
	End Function

	Public Function Get_FormulasFinancieras(ByVal IdFormula As Integer) As DataTable
		Return dl.Get_FormulasFinancieras(IdFormula)
	End Function
	Public Function InsertaFormulaFinanciera(ByRef entFormulaFinanciera As List(Of con_FormulasFinancieras), ByVal IdFormula As Integer) As String
		Return dl.InsertaFormulaFinanciera(entFormulaFinanciera, IdFormula)
	End Function
	Public Function EliminaFormulaFinanciera(ByVal IdFormula As Integer) As String
		Return dl.EliminaFormulaFinanciera(IdFormula)
	End Function
	Public Function rptPartidaReporteByPk(ByVal IdPartida As Integer) As DataTable
        Return dl.rptPartidaReporteByPk(IdPartida)
    End Function
    Public Function rptPartidaReporteByFecha(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sTipoPar As String, ByVal Desde As String, ByVal Hasta As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptPartidaReporteByFecha(dFechaIni, dFechaFin, sTipoPar, Desde, Hasta, IdSucursal)
    End Function
    Public Function rptPartidasConcentradas(ByVal TipoPartida As String, ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal desde As String, ByVal hasta As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptPartidasConcentradas(TipoPartida, dFechaI, dFechaF, desde, hasta, IdSucursal)
    End Function
    Public Function rptTotalesPartida(ByVal dtFechaIni As Date, ByVal dtFechaFin As Date, ByVal TipoPartida As String, ByVal EnDescuadre As Boolean, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptTotalesPartida(dtFechaIni, dtFechaFin, TipoPartida, EnDescuadre, IdSucursal)
    End Function
    Public Function rptConcentracionMayor(ByVal FechaIni As Date, ByVal FechaFin As Date, ByVal IdTipoPartida As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptConcentracionMayor(FechaIni, FechaFin, IdTipoPartida, IdSucursal)
    End Function
    Public Function pre_PresupuestosSelectRepeated(ByVal pAnio As Integer) As Integer
        Return dl.pre_PresupuestosSelectRepeated(pAnio)
    End Function
    Public Function pre_ObtenerDetallePresupuesto(ByVal Id As Integer) As DataTable
        Return dl.pre_ObtenerDetallePresupuesto(Id)
    End Function
    Public Function InsertaDetallePresupuesto(ByRef list As List(Of con_PresupuestosDetalle)) As String
        Return dl.InsertaDetallePresupuesto(list)
    End Function
    Public Function ModificaDetallePresupuesto(ByVal IdPresupuesto As Integer, ByRef list As List(Of con_PresupuestosDetalle)) As String
        Return dl.ModificaDetallePresupuesto(IdPresupuesto, list)
    End Function
    Public Function pre_ObtenerPresupuestosCTA() As DataTable
        Return dl.pre_ObtenerPresupuestosCTA()
    End Function
    Public Function con_ReportePresupuesto(ByVal Ejercicio As Integer, ByVal Mes As Integer) As DataTable
        Return dl.con_ReportePresupuesto(Ejercicio, Mes)
    End Function
    Public Function rptCatalogoConSaldos(ByVal Mes As Integer, ByVal Ejercicio As Integer) As DataTable
        Return dl.rptCatalogoConSaldos(mes, ejercicio)
    End Function
    Public Function rptMovimientosCuenta(ByVal FechaInic As Date, ByVal FechaFin As Date, ByVal IdCuenta As String, ByVal IdCentro As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptMovimientosCuenta(FechaInic, FechaFin, IdCuenta, IdCentro, IdSucursal)
    End Function
    
    Public Function rptLibroDiario(ByVal IdSucursal As Integer, ByVal Desde As Date, ByVal Hasta As Date, Tipo As Integer) As DataTable
        Return dl.rptLibroDiario(IdSucursal, Desde, Hasta, Tipo)
    End Function
    Public Function rptLibroMayor(ByVal dtFechaIni As Date, ByVal dtFechaFin As Date, ByVal IdSucursal As Integer, Tipo As Integer) As DataTable
        Return dl.rptLibroMayor(dtFechaIni, dtFechaFin, IdSucursal, Tipo)
    End Function
    Public Function ConsultaPartidas(ByVal Usuario As String, ByVal Fecha As String) As DataSet
        Return dl.ConsultaPartidas(Usuario, Fecha)
    End Function
    Public Function rptLibroDiarioMayorDetalle(ByVal dtFechaIni As Date, ByVal dtFechaFin As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptLibroDiarioMayorDetalle(dtFechaIni, dtFechaFin, IdSucursal)
    End Function
    Public Function rptLibroDiarioMayor(ByVal dtFechaIni As Date, ByVal dtFechaFin As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptLibroDiarioMayor(dtFechaIni, dtFechaFin, IdSucursal)
    End Function
    Public Function rptLibroAuxiliarDetalle(ByVal dtFechaIni As Date, ByVal dtFechaFin As Date, ByVal sIdCtaDesde As String, ByVal sIdCtaHasta As String) As DataTable
        Return dl.rptLibroAuxiliarDetalle(dtFechaIni, dtFechaFin, sIdCtaDesde, sIdCtaHasta)
    End Function
    Public Function rptLibroAuxiliarMayor(ByVal dtFechaIni As Date, ByVal dtFechaFin As Date, ByVal sIdCtaDesde As String, ByVal sIdCtaHasta As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptLibroAuxiliarMayor(dtFechaIni, dtFechaFin, sIdCtaDesde, sIdCtaHasta, IdSucursal)
    End Function
    Public Function ObtenerCuentasExcluidas(ByVal IdUsuario As String) As DataTable
        Return dl.ObtenerCuentasExcluidas(IdUsuario)
    End Function
    Public Function ObtenerCuentasAExcluir(ByVal DesdeCuenta As String, ByVal HastaCuenta As String) As DataTable
        Return dl.ObtenerCuentasAExcluir(DesdeCuenta, HastaCuenta)
    End Function
    Public Sub InsertarCuentaExcluida(ByVal IdUsuario As String, ByVal Cuenta As String, ByVal Motivo As String, ByVal CreadoPor As String, ByVal Excluir As Boolean)
        dl.InsertarCuentaExcluida(IdUsuario, Cuenta, Motivo, CreadoPor, Excluir)
    End Sub
    Public Function VerificaAccesoCuentas(ByVal IdUsuario As String, ByVal DesdeCuenta As String, ByVal HastaCuenta As String) As Boolean
        Return dl.VerificaAccesoCuentas(IdUsuario, DesdeCuenta, HastaCuenta)
    End Function
    Public Function rptCuentasLibroAuxiliarMayorExcel(ByVal HastaCuenta As String, ByVal DesdeCuenta As String, ByVal DesdeFecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptCuentasLibroAuxiliarMayorExcel(DesdeCuenta, HastaCuenta, DesdeFecha, IdSucursal)
    End Function
    Public Function rptDetalleCuentasMayorLibroAuxiliarExcel(ByVal IdCuenta As String, ByVal IdCuentaMayor As String) As DataTable
        Return dl.rptDetalleCuentasMayorLibroAuxiliarExcel(IdCuenta, IdCuentaMayor)
    End Function
    Public Function rptPartidasLibroAuxiliarMayorExcel(ByVal IdCuenta As String, ByVal DesdeFecha As Date, ByVal HastaFecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptPartidasLibroAuxiliarMayorExcel(IdCuenta, DesdeFecha, HastaFecha, IdSucursal)
    End Function
    Public Function rptComparacion2Meses(ByVal iMes1 As Integer, ByVal iAnio1 As Integer, ByVal iMes2 As Integer, ByVal iAnio2 As Integer, ByVal iNivel As Integer, ByVal iTipoSaldos As Integer, ByVal sDesdeCta As String, ByVal sHastaCta As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptComparacion2Meses(iMes1, iAnio1, iMes2, iAnio2, iNivel, iTipoSaldos, sDesdeCta, sHastaCta, IdSucursal)
    End Function
    Public Function rptComparacionAnual(ByVal iEjercicio As Integer, ByVal iNivel As Integer, ByVal iTipoSaldos As Integer, ByVal sDesdeCta As String, ByVal sHastaCta As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptComparacionAnual(iEjercicio, iNivel, iTipoSaldos, sDesdeCta, sHastaCta, IdSucursal)
    End Function

#Region "Presupuestos"
    Public Function pre_PresupuestoPorSucursal(ByVal pAnio As Integer) As DataTable
        Return dl.pre_PresupuestosPorSucursal(pAnio)
    End Function

    Public Function pre_PresupuestoPorSucursal(ByVal pAnio As Integer, ByVal pMes As Integer) As DataTable
        Return dl.pre_PresupuestosPorSucursal(pAnio, pMes)
    End Function

    Public Function pre_PresupuestoAnualDepto(ByVal pAnio As Integer, ByVal pDepto As Integer) As DataTable
        Return dl.pre_PresupuestoAnualDepto(pAnio, pDepto)
    End Function

    Public Function pre_PresupuestoAnualSuc(ByVal pAnio As Integer, ByVal pSuc As Integer) As DataTable
        Return dl.pre_PresupuestoAnualSuc(pAnio, pSuc)
    End Function

    Public Function pre_PresupuestoCabecera(ByVal pAnio As Integer, ByVal pSuc As Integer, ByVal pDepto As Integer, ByVal pCentroCosto As Integer) As DataTable
        Return dl.pre_PresupuestoCabecera(pAnio, pSuc, pDepto, pCentroCosto)
    End Function

    Public Function pre_PresupuestoCabeceraMes(ByVal pAnio As Integer, ByVal pSuc As Integer, ByVal pDepto As Integer, ByVal pCentroCosto As Integer, ByVal pMes As Integer) As DataTable
        Return dl.pre_PresupuestoCabeceraMes(pAnio, pSuc, pDepto, pCentroCosto, pMes)
    End Function

    Public Function pre_PresupuestoCuentaConSaldos(ByVal pAnio As Integer, ByVal pIdPresupuesto As Integer) As DataTable
        Return dl.pre_PresupuestoCuentaConSaldos(pAnio, pIdPresupuesto)
    End Function

    Public Function pre_PresupuestoCuentaConSaldosMes(ByVal pAnio As Integer, ByVal pMes As Integer, ByVal pIdPresupuesto As Integer) As DataTable
        Return dl.pre_PresupuestoCuentaConSaldosMes(pAnio, pMes, pIdPresupuesto)
    End Function

    Public Function pre_PresupuestosVsCuentas(ByVal pAnio As Integer, ByVal pMes As Integer, ByVal IdSucursal As Integer, ByVal IdDepto As Integer, ByVal IdCentro As String) As DataTable
        Return dl.pre_PresupuestosVsCuentas(pAnio, pMes, IdSucursal, IdDepto, IdCentro)
    End Function
    Public Function pre_ObtenerPresupuestos() As DataTable
        Return dl.pre_ObtenerPresupuestos()
    End Function
#End Region

	'ESTADOS FINANCIEROS
	Public Function con_rptRazonesFinancieras(ByVal mes As Integer, ByVal ejercicio As Integer, ByVal IdSucursal As Integer) As DataTable
		Return dl.con_rptRazonesFinancieras(mes, ejercicio, IdSucursal)
	End Function
	Public Function con_rptBalanceGeneralResumen(ByVal mes As Integer, ByVal ejercicio As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.con_rptBalanceGeneralResumen(mes, ejercicio, Tipo, IdSucursal)
    End Function
    Public Function con_rptBalanceGeneralDetalle(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.con_rptBalanceGeneralDetalle(Mes, Ejercicio, Tipo, IdSucursal)
    End Function
    Public Function con_rptBalanceComprobacionReporte(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal Nivel As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.con_rptBalanceComprobacionReporte(Mes, Ejercicio, Nivel, Tipo, IdSucursal)
    End Function
    Public Function con_rptBalanceComprobacionResumen(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.con_rptBalanceComprobacionResumen(Mes, Ejercicio, Tipo, IdSucursal)
    End Function
    Public Function con_rptBalanceComprobacionDetalle(ByVal mes As Integer, ByVal ejercicio As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.con_rptBalanceComprobacionDetalle(mes, ejercicio, Tipo, IdSucursal)
    End Function
    Public Function con_rptEstadoResultados(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal Nivel As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.con_rptEstadoResultados(Mes, Ejercicio, Nivel, Tipo, IdSucursal)
    End Function
    Public Function con_rptAnexos(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.con_rptAnexos(Mes, Ejercicio, Tipo, IdSucursal)
    End Function
    Public Function con_rptBalanceGeneralNIIF(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.con_rptBalanceGeneralNIIF(Mes, Ejercicio, Tipo, IdSucursal)
    End Function
    
    Public Function rptEstadoResultadoNIIF(ByVal mes As Integer, ByVal ejercicio As Integer, ByVal Tipo As Integer, ByVal Nivel As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptEstadoResultadoNIIF(mes, ejercicio, Tipo, Nivel, IdSucursal)
    End Function

    Public Function rptFlujoEfectivoNIIF(ByVal mes As Integer, ByVal ejercicio As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptFlujoEfectivoNIIF(mes, ejercicio, IdSucursal)
    End Function
    Public Function rptCambioPatrimonio(ByVal ejercicio As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.rptCambioPatrimonio(ejercicio, IdSucursal)
    End Function


    Public Function con_ObtenerDetalleReporteNIIF(ByVal IdReporte As Integer) As DataTable
        Return dl.con_ObtenerDetalleReporteNIIF(IdReporte)
    End Function

    Public Function con_ObtenerUltimoIdDetalleNIIF(ByVal IdReporte As Integer) As Integer
        Return dl.con_ObtenerUltimoIdDetalleNIIF(IdReporte)
    End Function
    Public Function con_CargarCuentasDeMayor() As DataTable
        Return dl.con_CargarCuentasDeMayor()
    End Function
    Public Function con_CargarActividadesFlujoEfectivo() As DataTable
        Return dl.con_CargarActividadesFlujoEfectivo()
    End Function
    Public Function con_AniosCerradosFlujoEfectivo(ByVal Ejercicio As Integer) As DataTable
        Return dl.con_AniosCerradosFlujoEfectivo(Ejercicio)
    End Function
    
    'OTROS PROCESOS
    Public Function ObtenerSaldosCentros(ByVal DesdeFecha As Date, ByVal HastaFecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ObtenerSaldosCentros(DesdeFecha, HastaFecha, IdSucursal)
    End Function
    Public Function ObtenerSaldosCentrosMayor(ByVal DesdeFecha As Date, ByVal HastaFecha As Date, ByVal DesdeCentro As String, ByVal HastaCentro As String) As DataTable
        Return dl.ObtenerSaldosCentrosMayor(DesdeFecha, HastaFecha, DesdeCentro, HastaCentro)
    End Function
    Public Function rep_EstadoResultadosCC(ByVal Desde As Date, ByVal Hasta As Date, ByVal DesdeCentro As String, ByVal HastaCentro As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.rep_EstadoResultadosCC(Desde, Hasta, DesdeCentro, HastaCentro, IdSucursal)
    End Function
    Public Function pro_ObtenerDatosICV(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataSet
        Return dl.pro_ObtenerDatosICV(Desde, Hasta, IdSucursal)
    End Function
    Public Sub pro_PreCierreContable(ByVal Ejercicio As Integer, ByVal Mes As Integer, ByVal CreadoPor As String)
        dl.pro_PreCierreMensual(Ejercicio, Mes, CreadoPor)
    End Sub
    Public Function con_ObtenerTransaccionesCaja(ByVal IdSucursal As Integer, ByVal Fecha As DateTime) As DataTable
        Return dl.con_ObtenerTransaccionesCaja(IdSucursal, Fecha)
    End Function
    Public Function con_ObtenerDatoCierre(ByVal Fecha As DateTime) As DataTable
        Return dl.con_ObtenerDatoCierre(Fecha)
    End Function
    Public Function con_ObtenerLiqCaja(ByVal Fecha As DateTime, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdUsuario As String, ByVal Cierre As Boolean) As DataTable
        Return dl.con_ObtenerLiqCaja(Fecha, IdSucursal, IdPunto, IdUsuario, Cierre)
    End Function
    Public Sub ObtienePeriodoContable(ByRef Mes As Integer, ByRef Ejercicio As Integer, Optional ByRef MinFecha As Date = Nothing)
        dl.ObtienePeriodoContable(Mes, Ejercicio, MinFecha)
    End Sub
    Public Function ObtieneDataConta(ByVal FechaI As Date, ByVal FechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ObtieneDataConta(FechaI, FechaF, IdSucursal)
    End Function
    Public Function ObtenerCuentaLiquidadora() As String
        Return dl.ObtenerCuentaLiquidadora()
    End Function
    Public Function GetCuentasContables() As DataTable
        Return dl.GetCuentasContables
    End Function
    Public Function IsCuentaTransac(ByVal IdCuenta As String) As Boolean
        Return dl.IsCuentaTransac(IdCuenta)
    End Function

    Public Function IsCuentaContable(ByVal IdCuenta As String) As Boolean
        Return dl.IsCuentaContable(IdCuenta)
    End Function

    Public Function InsertaPartidas(ByRef entPartidas As con_Partidas, ByRef listPartidasDetalle As List(Of con_PartidasDetalle)) As String
        Return dl.InsertaPartidas(entPartidas, listPartidasDetalle)
    End Function

    Public Function UpdatePartidas(ByRef entPartidas As con_Partidas, ByRef listPartidasDetalle As List(Of con_PartidasDetalle)) As String
        Return dl.UpdatePartidas(entPartidas, listPartidasDetalle)
    End Function

    Public Function ConsultaPartidas() As DataSet
        Return dl.ConsultaPartidas()
    End Function

    Public Function pro_ObtenerPartidasDetalle(ByVal IdPartida As Integer) As DataTable
        Return dl.pro_ObtenerPartidasDetalle(IdPartida)
    End Function

    Public Function IsCuentaConSaldo(ByVal IdCuenta As String) As Boolean
        Return dl.IsCuentaConSaldo(IdCuenta)
    End Function
    Public Function pro_VerificaIntegridadCierreMensual(ByVal ejercicio As Integer, ByVal num_mes As Integer) As DataSet
        Return dl.pro_VerificaIntegridadCierreMensual(ejercicio, num_mes)
    End Function

    Public Function pro_CierreMensual(ByVal ejercicio As Integer, ByVal num_mes As Integer, ByVal CreadoPor As String) As Boolean
        Return dl.pro_CierreMensual(ejercicio, num_mes, CreadoPor)
    End Function

    Public Function CountPartidasCierre()
        Return dl.CountPartidasCierre()
    End Function

    Public Function pro_CierreTemporal(ByVal ejercicio As Integer, ByVal num_mes As Integer, ByVal CreadoPor As String) As Boolean
        Return dl.pro_CierreTemporal(ejercicio, num_mes, CreadoPor)
    End Function

    Public Function pro_RevierteCierreMensual(ByVal ejercicio As Integer, ByVal num_mes As Integer) As Boolean
        Return dl.pro_RevierteCierreMensual(ejercicio, num_mes)
    End Function
    Public Function pro_RenumeraPartidas(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal TipoPar As String) As Integer
        Return dl.pro_RenumeraPartidas(Mes, Ejercicio, TipoPar)
    End Function
    Function pro_CrearCierre(ByVal iEjercicio As Integer, ByVal sTipoPar As String, ByVal dFecha As Date, ByVal sConcepto As String, ByVal sUser As String, ByVal sIdCuenta As String, ByVal sIdCuentaTipo As String, ByVal IdSucursal As Integer) As Integer
        Return dl.pro_CrearCierre(iEjercicio, sTipoPar, dFecha, sConcepto, sUser, sIdCuenta, sIdCuentaTipo, IdSucursal)
    End Function
    Function ObtieneIdPartida(ByVal iIdPartida As Integer, ByVal iTipo As Integer) As Integer
        Return dl.ObtieneIdPartida(iIdPartida, iTipo)
    End Function
    Public Sub ChequeaCuentas()
        dl.ChequeaCuentas()
    End Sub
    Public Sub EliminarCatalgoParaCarga()
        dl.EliminarCatalgoParaCarga()
    End Sub
    Public Sub OrdenaCatalagoConDelimitador(ByVal delimitador As String)
        dl.OrdenaCatalagoConDelimitador(delimitador)
    End Sub
    Public Function ObtenerCuentasMayor(ByVal IdCentro As String) As DataTable
        Return dl.ObtenerCuentasMayor(IdCentro)
    End Function
    Public Function ObtenerCuentasPatrimonio() As DataTable
        Return dl.ObtenerCuentasPatrimonio()
    End Function
    Public Sub EliminaCuentasMayor(ByVal IdCentro As String)
        dl.EliminaCuentasMayor(IdCentro)
    End Sub
    Public Sub InsertarCuentaMayor(ByVal IdCentro As String, ByVal IdCuenta As String, ByVal CreadoPor As String)
        dl.InsertarCuentaMayor(IdCentro, IdCuenta, CreadoPor)
    End Sub
    Public Function rep_AuxiliarCentrosCosto(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdCentro As String, ByVal DesdeCuenta As String, ByVal HastaCuenta As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.rep_AuxiliarCentrosCosto(Desde, Hasta, IdCentro, DesdeCuenta, HastaCuenta, IdSucursal)
    End Function

    Public Function pro_ValidaCentroCuenta(ByVal IdCuenta As String, ByVal IdCentro As String) As Integer
        Return dl.pro_ValidaCentroCuenta(IdCuenta, IdCentro)
    End Function

    Public Function pro_EliminaPartidasPorFecha(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipo As String, ByVal IdSucursal As Integer) As String
        Return dl.pro_EliminaPartidasPorFecha(Desde, Hasta, IdTipo, IdSucursal)
    End Function

    Public Function con_EliminaPartidas(ByVal IdModulo As Integer, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipoPartida As String, ByVal IdSucursal As Integer) As Integer
        Return dl.con_EliminaPartidas(IdModulo, Desde, Hasta, IdTipoPartida, IdSucursal)
    End Function
    Public Function con_ValidaContabilizacion(ByVal IdSucursal As Integer, ByVal IdModulo As Integer, ByVal Desde As Date, ByVal Hasta As Date) As Integer
        Return dl.con_ValidaContabilizacion(IdSucursal, IdModulo, Desde, Hasta)
    End Function

    Public Function con_ObtenerPeriodoContabilizado(ByVal IdModulo As Integer, ByVal IdSucursal As Integer, ByVal IdUsuario As String, Optional ByVal CajaChica As Boolean = False) As DataTable
        Return dl.con_ObtenerPeriodoContabilizado(IdModulo, IdSucursal, IdUsuario, CajaChica)
    End Function

    Public Function InsertaPartidasImport(ByRef entPartidas As con_Partidas, ByRef listPartidasDetalle As List(Of con_PartidasDetalle)) As String
        Return dl.InsertaPartidasImport(entPartidas, listPartidasDetalle)
    End Function

End Class

