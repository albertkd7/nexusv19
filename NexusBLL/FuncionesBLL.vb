﻿Imports NexusDLL
Public Class FuncionesBLL
    Dim dl As FuncionesDLL

    Public Sub New(ByVal StringConexion As String)
        dl = New FuncionesDLL(StringConexion)
    End Sub
    Public Function ObtenerUltimoId(ByVal Tabla As String, ByVal PrimaryKey As String) As Integer
        Return dl.ObtenerUltimoId(Tabla, PrimaryKey)
    End Function
    Public Function ActualizaAbono(ByVal Tabla As String, ByVal Id As Integer) As Integer
        Return dl.ActualizaAbono(Tabla, Id)
    End Function
    Public Function ObtenerNewId() As String
        Return dl.ObtenerNewId()
    End Function
    Public Function ObtenerIdToken(ByVal Token As String) As Integer
        Return dl.ObtenerIdToken(Token)
    End Function
    Public Function getDepartamento(ByVal IdMunicipio As String) As String
        Return dl.getDepartamento(IdMunicipio)
    End Function
    Public Function EsNulo(ByVal Value As Object, ByVal ValueNoNull As Object) As Object
        If Value Is Nothing Then Return Nothing
        If IsDBNull(Value) Then Return ValueNoNull
        Return value
    End Function
    Public Function GetFechaContable(ByVal IdSucursal As Integer) As Date
        Return dl.GetFechaContable(IdSucursal)
    End Function
    Public Function GetFechaContableCaja(ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As Date
        Return dl.GetFechaContableCaja(IdSucursal, IdPunto)
    End Function
    Public Function ObtenerCorrelativoManual(ByVal NombreCorrelativo As String) As Integer
        Return dl.ObtenerCorrelativoManual(NombreCorrelativo)
    End Function
    Public Function ObtenerUltimoIdAppointment(ByVal nombretabla As String) As Integer
        Return dl.ObtenerUltimoIdAppointment(nombretabla)
    End Function
    Public Function GetFechaUltAbonoEventoCliente(ByVal IdCliente As String, ByVal IdEvento As Integer) As Date
        Return dl.GetFechaUltAbonoEventoCliente(IdCliente, IdEvento)
    End Function
    Public Function CorrelativosInv(ByVal NombreCorrelativo As String) As Integer
        Return dl.CorrelativosInv(NombreCorrelativo)
    End Function
    Public Function ObtenerFormularioUnico(ByVal IdSucursal As Integer) As Integer
        Return dl.ObtenerFormularioUnico(IdSucursal)
    End Function
End Class
