﻿Imports NexusDLL
Public Class CombosBLL

    Dim dl As CombosDLL
    Public Sub New(ByVal dateBaseName As String)
        dl = New CombosDLL(dateBaseName)
    End Sub
    Public Sub adm_Modulos(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "adm_MOdulos", "IdModulo", "Nombre", "", TipoSelect)
    End Sub
    Public Sub adm_SujetoAlerta(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='SUJETO_ALERTA'", "")
    End Sub
    Public Sub admDepartamentos(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "adm_Departamentos", "IdDepartamento", "Nombre", "", TipoSelect)
    End Sub
    Public Sub admMunicipios(ByVal combo As Object, ByVal IdDepartamento As String, Optional ByVal TipoSelect As String = "")
        Dim sCondic As String = "IdDepartamento='" & IdDepartamento & "'"
        dl.CargaCombo(combo, "adm_Municipios", "IdMunicipio", "Nombre", sCondic, TipoSelect)
    End Sub
    Public Sub cpp_TiposAbono(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='CPP_ABONOS'")
    End Sub
    Public Sub adm_Usuarios(ByVal leEdit As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaLookUpEdit(leEdit, "adm_Usuarios", "IdUsuario", "Nombre", "", TipoSelect)
    End Sub
    Public Sub fac_RubrosActividades(ByVal combo As Object, ByVal sTipoSelect As String)
        dl.CargaCombo(combo, "fac_RubrosActividades", "IdRubro", "Nombre", "", sTipoSelect)
    End Sub

#Region "Compras"
    Public Sub com_Aduana(ByVal combo As Object)
        dl.CargaCombo(combo, "com_Aduana", "IdAduana", "Nombre", "")
    End Sub
    Public Sub com_Proveedores(ByVal combo As Object)
        dl.CargaCombo(combo, "com_Proveedores", "IdProveedor", "Nombre, Nrc, Nit", "")
    End Sub
    Public Sub com_GastosImportaciones(ByVal combo As Object)
        dl.CargaCombo(combo, "com_GastosImportaciones", "IdGasto", "Nombre", "")
    End Sub
    Public Sub com_TiposCompra(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='COM_TIPO_COMPRA'")
    End Sub
    Public Sub com_ClaseDocumento(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='COM_CLASEDOCUMENTO'")
    End Sub
    Public Sub com_ClaseDocumentoImp(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='COM_CLASEDOCUMENTO_IMP'")
    End Sub
    Public Sub com_TiposCajas(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='COM_TIPOS_CAJACHICA'")
    End Sub
    Public Sub com_OrigenesCompra(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='COM_ORIGEN_COMPRA'")
    End Sub
    Public Sub com_OrigenesImportacion(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='COM_ORIGEN_COMPRA' and Id>1")
    End Sub
    Public Sub com_OrigenProveedor(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='PROVEEDORES_ORIGEN'")
    End Sub
    Public Sub adm_pais(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Pais", "IdPais", "Nombre", "")
    End Sub
    Public Sub com_CargoAbono(ByVal Combo As Object)
        dl.CargaCombo(Combo, "adm_Listas", "Id", "Nombre", "Modulo = 'COM'")
    End Sub
    Public Sub Com_CajasChicaCompras(ByVal Combo As Object, ByVal sTipoSelect As String)
        dl.CargaCombo(Combo, "com_CajasChica ", "IdCaja", "Nombre", "IdModuloAplica in (1,3) or IdCaja=1", sTipoSelect)
    End Sub
    Public Sub Com_CajasChicaComprasContabilizar(ByVal Combo As Object, ByVal sTipoSelect As String)
        dl.CargaCombo(Combo, "com_CajasChica ", "IdCaja", "Nombre", "IdCaja >1", sTipoSelect)
    End Sub
    Public Sub coo_TipoCalculoImpCompras(ByVal combo As Object, ByVal sTipoSelect As String)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo = 'TIPO_CALCULO'", sTipoSelect)
    End Sub
#End Region
#Region "Facturación"
    Public Sub fac_TiposDocumento(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='FAC_TIPOS_DOCUMENTOS'")
    End Sub
    Public Sub fac_Actividades(ByVal combo As Object, ByVal sTipoSelect As String)
        dl.CargaCombo(combo, "fac_Actividades", "IdActividad", "Nombre", sTipoSelect)
    End Sub
    Public Sub fac_Profesiones(ByVal combo As Object)
        dl.CargaCombo(combo, "fac_Profesiones", "IdProfesion", "Nombre", "")
    End Sub
    Public Sub fac_RubroActividades(ByVal combo As Object)
        dl.CargaCombo(combo, "fac_RubrosActividades", "IdRubro", "Nombre", "")
    End Sub

    Public Sub adm_Sucursales(ByVal combo As Object, ByVal Usuario As String, ByVal sTipoSelect As String)
        Dim Condicion As String = "IdUsuario= '" & Usuario & "'"
        dl.CargaComboSucursales(combo, Condicion, sTipoSelect)
    End Sub
    ' solo para la sucursal de envio por remision
    Public Sub adm_SucursalesFac(ByVal combo As Object, ByVal sTipoSelect As String)
        dl.CargaCombo(combo, "adm_sucursales", "IdSucursal", "Nombre", "", sTipoSelect)
    End Sub

    Public Sub adm_TiposComprobante(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "", sTipoSelect)
    End Sub
    Public Sub adm_TipoLibroAplica(ByVal combo As Object)
        'COMBO ESPECIAL PARA MOSTRAR EL TIPO DE LIBRO AL QUE APLICA EL DOCUMENTO
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='LIBROS'")
    End Sub
    Public Sub adm_TipoModuloAplicaComprobante(ByVal combo As Object)
        'COMBO ESPECIAL PARA MOSTRAR EL MÓDULO AL QUE APLICA EL DOCUMENTO
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='MODULO_APLICA'")
    End Sub
    Public Sub com_TiposComprobante(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        'COMBO ESPECIAL PARA MOSTRAR LOS TIPOS DE COMPROBANTE QUE APLICAN PARA EL MODULO DE COMPRAS
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "IdModuloAplica = 1", sTipoSelect)
    End Sub
    Public Sub fac_TiposComprobante(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        'COMBO ESPECIAL PARA MOSTRAR LOS TIPOS DE COMPROBANTE QUE APLICAN PARA EL MODULO DE FACTURACION Y VENTAS
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "IdModuloAplica = 2", sTipoSelect)
    End Sub
    Public Sub fac_Clientes(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        'COMBO ESPECIAL PARA MOSTRAR LOS TIPOS DE COMPROBANTE QUE APLICAN PARA EL MODULO DE FACTURACION Y VENTAS
        dl.CargaCombo(combo, "fac_clientes", "IdCliente", "Nombre", "", sTipoSelect)
    End Sub
    Public Sub fac_TiposComprobanteBatarse(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        'COMBO ESPECIAL PARA MOSTRAR LOS TIPOS DE COMPROBANTE QUE APLICAN PARA EL MODULO DE FACTURACION Y VENTAS
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "IdModuloAplica = 2 and IdTipoComprobante not in (9,10,11)", sTipoSelect)
    End Sub
    Public Sub imp_TiposComprobante(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        'COMBO ESPECIAL PARA MOSTRAR LOS TIPOS DE COMPROBANTE QUE APLICAN PARA LAS IMPORTACIONES
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "IdModuloAplica = 3", sTipoSelect)
    End Sub
    Public Sub inv_TiposEntrada(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        'COMBO ESPECIAL PARA MOSTRAR LOS TIPOS DE COMPROBANTE QUE APLICAN PARA ENTRADAS EN INVENTARIO
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "IdModuloAplica = 4", sTipoSelect)
    End Sub

    Public Sub inv_TiposSalida(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        'COMBO ESPECIAL PARA MOSTRAR LOS TIPOS DE COMPROBANTE QUE APLICAN PARA SALIDAS EN INVENTARIO
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "IdModuloAplica = 5", sTipoSelect)
    End Sub
    Public Sub inv_TiposTraslado(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        'COMBO ESPECIAL PARA MOSTRAR LOS TIPOS DE COMPROBANTE QUE APLICAN PARA TRASLADOS EN INVENTARIO
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "IdModuloAplica = 6", sTipoSelect)
    End Sub
    Public Sub inv_TiposProduccion(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "IdModuloAplica = 7", sTipoSelect)
    End Sub

    Public Sub fac_PuntosVenta(ByVal combo As Object, ByVal IdSucursal As Integer, Optional ByVal sTipoSelect As String = "")
        Dim sCondic = ""
        If IdSucursal > -1 Then
            sCondic = "IdSucursal=" & IdSucursal
        End If
        dl.CargaCombo(combo, "Fac_PuntosVenta", "IdPunto", "Nombre", sCondic, sTipoSelect)
    End Sub
    Public Sub fac_Rutas(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "fac_Rutas", "IdRuta", "Nombre", "", TipoSelect)
    End Sub
    Public Sub fac_TipoReporteVentas(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='TIPO_REPORTEVENTAS'")
    End Sub
    Public Sub fac_FormasPago(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "fac_FormasPago", "IdFormaPago", "Nombre", "", sTipoSelect)
    End Sub
    Public Sub fac_VendedoresFacturacion(ByVal combo As Object, ByVal IdSucursal As Integer)
        dl.CargaCombo(combo, "fac_Vendedores", "IdVendedor", "Nombre", "Activo=1 and IdSucursal=" & IdSucursal, "")
    End Sub
    Public Sub fac_FormasPagoBatarse(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "fac_FormasPago", "IdFormaPago", "Nombre", "IdFormaPago in(1,2,3)", sTipoSelect)
    End Sub
    Public Sub fac_Vendedores(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "fac_Vendedores", "IdVendedor", "Nombre", "Activo=1", sTipoSelect)
    End Sub
    Public Sub fac_TiposVenta(ByVal combo As Object)
        dl.CargaCombo(combo, "fac_TiposVenta", "IdTipo", "Nombre", "")
    End Sub
    Public Sub adm_TipoAplicacionComprobante(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='TIPO_APLICACION'")
    End Sub
    Public Sub fac_TiposImpuesto(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='TIPO_IMPUESTO'")
    End Sub
    Public Sub fac_FormaPrecios(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='TIPO_PRECIOS'")
    End Sub
#End Region
#Region "Inventarios"
    Public Sub invGrupos(ByVal combo As Object, ByVal IdDepartamento As Integer, Optional ByVal sTipoSelect As String = "")
        Dim sCondic = ""
        If IdDepartamento > 0 Then
            sCondic = "IdCategoria=" & IdDepartamento
        End If
        dl.CargaCombo(combo, "inv_Grupos", "IdGrupo", "Nombre", sCondic, sTipoSelect)
    End Sub
    Public Sub invGrupos(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "inv_Grupos", "IdGrupo", "Nombre", "", sTipoSelect)
    End Sub
    Public Sub invSubGrupos(ByVal combo As Object, ByVal IdGrupo As Integer, Optional ByVal sTipoSelect As String = "")
        Dim sCondic = ""
        If IdGrupo > 0 Then   'para un grupo en particular
            sCondic = "IdGrupo=" & IdGrupo
        End If
        dl.CargaCombo(combo, "inv_SubGrupos", "IdSubGrupo", "Nombre", sCondic, sTipoSelect)
    End Sub
    Public Sub invSubGruposTodos(ByVal combo As Object)
        dl.CargaCombo(combo, "inv_SubGrupos", "IdSubGrupo", "Nombre", "", "")
    End Sub
    Public Sub inv_Bodegas(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "inv_Bodegas", "IdBodega", "Nombre", "", sTipoSelect)
    End Sub
    Public Sub invUnidadesMedida(ByVal combo As Object)
        dl.CargaCombo(combo, "inv_UnidadesMedida", "IdUnidad", "Nombre", "")
    End Sub
    Public Sub invMarcas(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "inv_Marcas", "IdMarca", "Nombre", "", sTipoSelect)
    End Sub
    Public Sub inv_Precios(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "inv_Precios", "IdPrecio", "Nombre", "", TipoSelect)
    End Sub
    Public Sub inv_GastosProduccion(ByVal combo As Object)
        dl.CargaCombo(combo, "inv_GastosProduccion", "IdGasto", "Nombre", "")
    End Sub
    Public Sub invUbicaciones(ByVal combo As Object)
        dl.CargaCombo(combo, "inv_Ubicaciones", "IdUbicacion", "Nombre", "")
    End Sub

    Public Sub invTiposInventario(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='INV_TIPOS'", sTipoSelect)
    End Sub
    Public Sub invEstadosInventario(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='INV_ESTADOS'", sTipoSelect)
    End Sub
    Public Sub invMaximosMinimos(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='INV_MAXIMOSMINIMOS'", sTipoSelect)
    End Sub
    Public Sub invCategoriaProductof983(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='INV_CATEGORIA_PRODUCTO'", sTipoSelect)
    End Sub
    Public Sub invCategoriaProducto(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "inv_categorias", "IdCategoria", "Nombre", "", sTipoSelect)
    End Sub
#End Region
#Region "Contabilidad"
    Public Sub conTiposPartida(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaLookUpEdit(combo, "con_TiposPartida", "IdTipo", "Nombre", "", sTipoSelect)
    End Sub
    Public Sub conTiposCuentaCierre(ByVal combo As Object, ByVal Tipo As String)
        dl.CargaCombo(combo, "con_Cuentas", "IdCuenta", "Nombre", "Nivel=2 and Naturaleza in (4,5)", Tipo)
    End Sub
    
    Public Sub con_CentrosCosto(ByVal combo As Object, ByVal sCondicion As String, Optional ByVal sTipoSelect As String = "")
        'If sCondicion <> "" Then
        '    sCondicion = "IdCuentaMayor= " & Left(sCondicion, 3)
        'End If
        dl.CargaLookUpEdit(combo, "con_CentrosCosto", "IdCentro", "Nombre", sCondicion, sTipoSelect, "S")
    End Sub
    Public Sub con_RubrosPatrimonio(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='CON_PATRI'", sTipoSelect)
    End Sub
    Public Sub con_CargarCentrosCostoCuentaMayor(ByVal combo As Object, ByVal IdCuenta As String)
        dl.con_CargarCentrosCostoCuentaMayor(combo, IdCuenta)
    End Sub
    Public Sub con_CargarEstadosFinancierosNIIF(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='CON_EF'", sTipoSelect)
    End Sub
    Public Sub con_CargarRubroFlujoEfectivo(ByVal combo As Object)
        dl.CargaCombo(combo, "con_RubroFlujo", "IdRubro", "Nombre", "")
    End Sub
    Public Sub con_Conceptos(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "con_ConceptosCaja", "IdConcepto", "Nombre", "", sTipoSelect)
    End Sub
#End Region
#Region "Presupuesto"

    Public Sub pre_Departamentos(ByVal pCombo As Object, ByVal pIdSucursal As String, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(pCombo, "pre_DepartamentosSucursal", "IdDepartamentoSucursalPk", "Nombre", IIf(pIdSucursal > 0, "IdSucursalFk = " + pIdSucursal, ""), sTipoSelect)
    End Sub
#End Region
#Region "Bancos"
    Public Sub banCuentasBancarias(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        Dim sCondic = ""
        sCondic = "CuentaInactiva=0"
        dl.CargaComboCuenta(combo, "ban_CuentasBancarias", "IdCuentaBancaria", "RTRIM(Nombre)+' - '+NumeroCuenta Nombre", sCondic, sTipoSelect)
    End Sub
    Public Sub banTiposTransaccion(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "ban_TiposTransaccion", "IdTipo", "Nombre", "", sTipoSelect)
    End Sub
    Public Sub Ban_CargoAbono(ByVal Combo As Object)
        dl.CargaCombo(Combo, "adm_Listas", "Id", "Nombre", "Modulo = 'BAN_CARGO_ABONO'")
    End Sub
    Public Sub ban_TipoImpresion(ByVal Combo As Object)
        dl.CargaCombo(Combo, "adm_Listas", "Id", "Nombre", "Modulo = 'BAN_TIPO_IMPRESION'")
    End Sub
    Public Sub ban_TipoBanco(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='TIPO_BANCO'")
    End Sub
#End Region
#Region "CtasCobrar"
    Public Sub cpcCobradores(ByVal combo As Object)
        dl.CargaCombo(combo, "cpc_Cobradores", "IdCobrador", "Nombre", "")
    End Sub
    Public Sub cpcTiposMov(ByVal combo As Object, Optional ByVal sTipoApl As String = "")
        dl.CargaCombo(combo, "cpc_TiposMov", "IdTipo", "Nombre", sTipoApl)
    End Sub

#End Region
#Region "ActivoFijo"
    Public Sub acf_Clases(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "acf_Clases", "IdClase", "Nombre", "", TipoSelect)
    End Sub
    Public Sub acf_Motivos(ByVal combo As Object, ByVal sTipoSelect As String)
        dl.CargaCombo(combo, "acf_MotivosBaja", "IdMotivo", "Nombre", "", sTipoSelect)
    End Sub
    Public Sub acf_Fallas(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "acf_Fallas", "IdFalla", "Nombre", "", TipoSelect)
    End Sub
    Public Sub acf_Tecnicos(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "acf_Tecnicos", "IdTecnico", "Nombre", "", TipoSelect)
    End Sub

    Public Sub acf_TiposDepreciacion(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "acf_TiposDepreciacion", "IdTipo", "Nombre", "", TipoSelect)
    End Sub
    Public Sub acf_Ubicacion(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "acf_Ubicaciones", "IdUbicacion", "Nombre", "", TipoSelect)
    End Sub
    Public Sub acf_Estados(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "acf_Estados", "IdEstado", "Nombre", "", TipoSelect)
    End Sub
    Public Sub acf_Marcas(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "acf_Marcas", "IdMarca", "Nombre", "", TipoSelect)
    End Sub
    Public Sub acf_Estilos(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "acf_Estilos", "IdEstilo", "Nombre", "", TipoSelect)
    End Sub
    Public Sub acf_Modelos(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "acf_Modelos", "IdModelo", "Nombre", "", TipoSelect)
    End Sub
    Public Sub acf_TipoAplicacionAjuste(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='ACF_AJUSTEAPLICACION'")
    End Sub
    Public Sub acf_CuentaGasto(ByVal combo As Object, ByVal sTipoSelect As String)
        dl.CargaComboCuentaGasto(combo, "Id", "Nombre", sTipoSelect)
    End Sub

    Public Sub acf_ListGrupoGeneral(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='AGRUPO_ACTIVO'")
    End Sub
#End Region
#Region "Produccion"
    Public Sub pro_Actividades(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "pro_Actividades", "IdActividad", "Nombre", "", sTipoSelect)
    End Sub
    Public Sub PRO_TiposSalida(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        'COMBO ESPECIAL PARA MOSTRAR LOS TIPOS DE COMPROBANTE QUE APLICAN PARA SALIDAS EN INVENTARIO
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "IdModuloAplica = 7", sTipoSelect)
    End Sub
    Public Sub pro_TiposEntrada(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        'COMBO ESPECIAL PARA MOSTRAR LOS TIPOS DE COMPROBANTE QUE APLICAN PARA SALIDAS EN INVENTARIO
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "IdModuloAplica = 8", sTipoSelect)
    End Sub
    Public Sub pro_TiposEntradaEmpaque(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        'COMBO ESPECIAL PARA MOSTRAR LOS TIPOS DE COMPROBANTE QUE APLICAN PARA SALIDAS EN INVENTARIO
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "IdModuloAplica = 9", sTipoSelect)
    End Sub
    Public Sub pro_TiposSalidaEmpaque(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        'COMBO ESPECIAL PARA MOSTRAR LOS TIPOS DE COMPROBANTE QUE APLICAN PARA SALIDAS EN INVENTARIO
        dl.CargaCombo(combo, "adm_TiposComprobante", "IdTipoComprobante", "Nombre", "IdModuloAplica = 10", sTipoSelect)
    End Sub
#End Region
#Region "Planillas"
    Public Sub pla_Empleados(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "pla_Empleados", "IdEmpleado", "Nombres as Nombre", "", TipoSelect)
    End Sub
#End Region

#Region "Eventos"
    Public Sub eve_Seminarios(ByVal combo As Object, Optional ByVal TipoSelect As String = "")
        dl.CargaCombo(combo, "eve_Seminarios", "IdSeminario", "Nombre", "", TipoSelect)
    End Sub


    Public Sub eve_EfectoCentroCosto(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='EFECTO_CENTRO_COSTOS'")
    End Sub
    Public Sub eve_TipoFacturacion(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='EVE_TIPOPAGO'")
    End Sub
    Public Sub eve_TipoVentas(ByVal combo As Object)
        dl.CargaCombo(combo, "adm_Listas", "Id", "Nombre", "Modulo='EVE_TIPOFACTURACION'")
    End Sub

    Public Sub eve_CentroCosto(ByVal combo As Object, Optional ByVal sTipoSelect As String = "")
        dl.CargaCombo(combo, "eve_CentrosCostos", "IdCentro", "Nombre", "", sTipoSelect)
    End Sub
#End Region
End Class
