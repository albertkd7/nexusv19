﻿Imports NexusDLL
Imports NexusELL.TableEntities

Public Class ConsultasBLL
    Dim dl As ConsultasDLL
    Dim bl As TableData
    Public Sub New(ByVal StringConexion As String)
        dl = New ConsultasDLL(StringConexion)
        bl = New TableData(StringConexion)
    End Sub


    Public Function ConsultaGEstionesProgramadas(ByVal forma As Object, ByVal IdVendedor As Integer) As String
        dl.ConsultaGEstionesProgramadas(forma, IdVendedor)
        forma.text = "Consulta de Gestiones Programadas Pendientes"
        forma.showdialog()
        Return forma.ValCodigo
    End Function

#Region "ConsultasCatalogos"
    Public Function ConsultaProductos(ByRef Forma As Object, ByVal Condicion As String, ByVal IdProducto As String) As String
        dl.ConsultaProductos(Forma, Condicion)
        Forma.ShowDialog()
        Return Forma.IdProducto
    End Function
    Public Function cnsSeries(ByRef forma As Object, ByVal IdTipoComprobante As Integer) As String
        Dim sCondic As String = ""
        sCondic = " IdTipoComprobante=" & IdTipoComprobante

        dl.CargaCns(forma, "adm_SeriesDocumentos", sCondic, "Serie", "CreadoPor", "FechaAsignacion", "EsActivo")
        forma.Text = "Consulta de Series"
        forma.showdialog()
        Return forma.ValCodigo
    End Function
    Public Function ConsultaProduccionesInv(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaProduccionesInv
        forma.ds = ds
        forma.text = "Consulta de Producciones de  Inventario"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaEmpleados(ByVal forma As Object) As String
        dl.ConsultaEmpleados(forma)
        'forma.ds = ds
        forma.text = "Consulta de Empleados"
        forma.showdialog()
        Return forma.ValCodigo
    End Function
    Public Function ConsultaOrdenProduccion(ByVal forma As Object) As String
        dl.ConsultaOrdenProduccion(forma)
        forma.text = "Consulta de Ordenes Producción"
        forma.showdialog()
        Return forma.ValCodigo
    End Function
    Public Function ConsultaGastosProduccion(ByVal forma As Object) As String
        dl.ConsultaGastosProduccion(forma)
        forma.text = "Consulta de Gastos de Producción"
        forma.showdialog()
        Return forma.ValCodigo
    End Function
    Public Function ConsultaRetencion(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaRetencion
        forma.ds = ds
        forma.PrimaryKey = "IdComprobante"
        forma.text = "Consulta de Comprobantes de Retención"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaProductos2(ByRef Forma As Object, ByVal sCondic As String, ByVal IdProducto As String) As inv_Productos

        Dim ent As New inv_Productos

        'Al llenar la entidad ya no se llama la consulta
        If IdProducto <> "" Then
            ent = bl.inv_ProductosSelectByPK(IdProducto)
        End If

        If ent.IdProducto = "" Then
            dl.ConsultaProductos(Forma, "")
            Forma.ShowDialog()
            If Forma.IdProducto <> "" Then
                ent = bl.inv_ProductosSelectByPK(Forma.IdProducto)
            End If
        End If
        Return ent
    End Function
    Public Function cnsCuentas(ByRef forma As Object, ByVal IdCuenta As String) As con_Cuentas
        Dim entCuenta As New con_Cuentas

        'If IdCuenta = "" Or IdCuenta Is Nothing Then
        '    Return entCuenta
        'End If

        'con esto se comprueba si existe la cuenta cuando se le pasa un parámetro diferente de -1
        If Not IdCuenta = "" And Not IdCuenta Is Nothing Then
            entCuenta = bl.con_CuentasSelectByPK(IdCuenta)
        End If
        If entCuenta.IdCuenta = "" Then  'es porque no existe la cuenta
            dl.CargaCns(forma, "con_cuentas", "", "IdCuenta", "Nombre", "IdCuentaMayor")
            forma.Text = "Consulta de cuentas"
            forma.showdialog()
            If Not forma.IdCuenta = "" Then
                entCuenta = bl.con_CuentasSelectByPK(forma.IdCuenta)
            End If
        End If
        Return entCuenta

    End Function
	Public Function cnsProveedores(ByRef forma As Object, ByVal IdProveedor As String) As com_Proveedores
		Dim entProveedor As New com_Proveedores

		'Al llenar la entidad ya no se llama la consulta
		If Not IdProveedor = "" And Not IdProveedor Is Nothing Then
			entProveedor = bl.com_ProveedoresSelectByPK(IdProveedor)
		End If

		If entProveedor.IdProveedor = "" Then
			dl.CargaCns(forma, "com_Proveedores", "", "IdProveedor", "Nombre", "Nrc", "Telefonos")
			forma.Text = "Consulta de proveedores"
			forma.showdialog()
			If Not forma.ValCodigo = "" Then
				entProveedor = bl.com_ProveedoresSelectByPK(forma.ValCodigo)
			End If

		End If
		Return entProveedor
	End Function
	Public Function cnsProspectosClientes(ByRef forma As Object, ByVal IdProspecto As Integer) As fac_ProspectacionClientes
		Dim entProspecto As New fac_ProspectacionClientes

		'Al llenar la entidad ya no se llama la consulta
		If IdProspecto > 0 Then
			entProspecto = bl.fac_ProspectacionClientesSelectByPK(IdProspecto)
		End If

		If entProspecto.IdComprobante = 0 Then
			dl.CargaCns(forma, "fac_ProspectacionClientes", "", "IdComprobante", "NombreCliente", "Nrc", "Telefonos", "FechaContacto")
			forma.Text = "Consulta de prospectos de clientes"
			forma.showdialog()

			Try
				If Convert.ToInt32(forma.ValCodigo) > 0 Then
					entProspecto = bl.fac_ProspectacionClientesSelectByPK(Convert.ToInt32(forma.ValCodigo))
				End If
			Catch ex As Exception

			End Try


		End If
		Return entProspecto
	End Function
	Public Function cnsVales(ByRef forma As Object, ByVal IdVale As Integer) As fac_Vales
        Dim entVale As New fac_Vales

        'Al llenar la entidad ya no se llama la consulta
        If Not IdVale = 0 Then
            entVale = bl.fac_ValesSelectByPK(IdVale)
        End If

        If entVale.IdComprobante = 0 Then
            dl.CargaCns(forma, "fac_Vales", "", "IdComprobante", "NumeroComprobante", "Fecha", "Nombre", "Valor")
            forma.Text = "Consulta de Vales"
            forma.showdialog()
            If Not CInt(forma.ValCodigo) = 0 Then
                entVale = bl.fac_ValesSelectByPK(CInt(forma.ValCodigo))
            End If

        End If
        Return entVale
    End Function
    Public Function cnsClientes(ByVal forma As Object, ByVal Codigo As String, Optional ByVal Condicion As String = "") As fac_Clientes
        Dim entCliente As New fac_Clientes
        'Al llenar la entidad ya no se llama la consulta
        If Not Codigo = "" And Not Codigo Is Nothing Then
            entCliente = bl.fac_ClientesSelectByPK(Codigo)
        End If
        If entCliente.IdCliente = "" Then
            dl.CargaCns(forma, "fac_Clientes", Condicion, "IdCliente", "Nombre", "Nrc", "Nit", "Direccion", "Telefonos")

            forma.Text = "Consulta de clientes"
            forma.showdialog()
            If Not forma.IdCliente = "" Then
                entCliente = bl.fac_ClientesSelectByPK(forma.IdCliente)
            End If
        End If
        Return entCliente
    End Function
    Public Function cnsEventos(ByVal forma As Object, ByVal Codigo As Integer, ByVal IdSucursal As Integer) As eve_Eventos
        Dim entEventos As New eve_Eventos
        'Al llenar la entidad ya no se llama la consulta
        If Not Codigo = 0 Then
            entEventos = bl.eve_EventosSelectByPK(Codigo)
        End If
        Dim Condicion As String = ""
        If IdSucursal <> 1 Then
            Condicion = "IdSucursal =" & IdSucursal
        End If
        If entEventos.IdEvento = 0 Then
            dl.CargaCns(forma, "eve_Eventos", Condicion, "IdEvento", "DescripcionEvento", "CodEvento", "Grupo", "FechaInicio", "FechaFin")

            forma.Text = "Consulta de Eventos"
            forma.showdialog()
            If Not forma.IdEvento = 0 Then
                entEventos = bl.eve_EventosSelectByPK(forma.IdEvento)
            End If
        End If
        Return entEventos
    End Function
    Public Function cnsParticipantes(ByVal forma As Object, ByVal Codigo As String) As String
        Dim entParticipantes As New eve_Participantes
        'Al llenar la entidad ya no se llama la consulta
        If Not Codigo = "" And Not Codigo Is Nothing Then
            entParticipantes = bl.eve_ParticipantesSelectByPK(Codigo)
        End If
        If entParticipantes.NumInscripcion = "" Then
            dl.CargaCns(forma, "eve_Participantes", "", "NumInscripcion", "Nombre")

            forma.Text = "Consulta de Participantes"
            forma.showdialog()
            'If Not forma.NumInscripcion = "" Then
            '    entParticipantes = bl.eve_ParticipantesSelectByPK(forma.NumInscripcion)
            'End If
        End If
        Return forma.NumInscripcion
    End Function
    Public Function cnsClientesPedidos(ByVal forma As Object, ByVal Codigo As Integer) As fac_Pedidos
        Dim entPedBusca As New fac_Pedidos

        dl.CargaCns(forma, "fac_Pedidos", "", "IdComprobante", "IdCliente", "Nombre", "Nrc", "Nit", "OtroDocumento=''", "Telefonos=Telefono")

        forma.Text = "Consulta de clientes Por Pedidos"
        forma.showdialog()
        If Not forma.IdComprobante = 0 Then
            entPedBusca = bl.fac_PedidosSelectByPK(forma.IdComprobante)
        End If

        Return entPedBusca
    End Function

    Public Function cnsEmpleados(ByVal forma As Object) As String
        dl.ConsultaEmpleados(forma)
        forma.text = "Consulta de Empleados"
        forma.showdialog()
        Return forma.valcodigo
    End Function

    Public Function cnsAbonosCPP(ByVal forma As Object, ByVal IdSucursal As Integer) As String
        dl.ConsultaAbonosCPP(forma, IdSucursal)
        forma.text = "Consulta de Abonos de Cuenta por Pagar"
        forma.showdialog()
        Return forma.valcodigo
    End Function
    Public Function cnsGrupos(ByVal forma As Object) As String
        dl.CargaCns(forma, "inv_Grupos", "", "IdGrupo", "Nombre")
        forma.text = "Consulta de grupos"
        forma.showdialog()
        Return forma.ValCodigo
    End Function
    Public Function cnsSubGrupos(ByVal forma As Object, ByVal IdGrupo As String) As String
        Dim sCondic As String = "IdGrupo='" & IdGrupo & "'"
        dl.CargaCns(forma, "inv_SubGrupos", sCondic, "IdSubGrupo", "Nombre")
        forma.text = "Consulta de sub-grupos"
        forma.showdialog()
        Return forma.ValCodigo
    End Function
    Public Function cnsActivos(ByVal forma As Object, ByVal IdActivo As Integer) As acf_Activos
        Dim entActivo As New acf_Activos

        If IdActivo < 0 Then
            Return entActivo
        End If

        If IdActivo > 0 Then
            entActivo = bl.acf_ActivosSelectByPK(IdActivo)
        End If

        If entActivo.IdActivo = 0 Then
            dl.CargaCns(forma, "acf_Activos", "", "IdActivo", "Codigo", "Nombre", "ValorAdquisicion")
            forma.Text = "Consulta de Activos"
            forma.showdialog()
            If Not forma.ValCodigo = "" Then
                entActivo = bl.acf_ActivosSelectByPK(CInt(forma.ValCodigo))
            End If

        End If
        Return entActivo
    End Function

    'Public Function cnsUbicaciones(ByVal forma As Object) As String
    '    dl.CargaCns(forma, "acf_Ubicaciones", "", "IdUbicacion", "Nombre", "Nombre")
    '    forma.text = "Consulta de Ubicaciones de Activo Fijo"
    '    forma.showdialog()
    '    Return forma.ValCodigo
    'End Function
    Public Function cnsLiquidacion(ByRef forma As Object, ByVal IdComprobante As String) As com_Liquidacion
        Dim entLiquidacion As New com_Liquidacion

        'Al llenar la entidad ya no se llama la consulta
        If Not IdComprobante = 0 And Not IdComprobante Is Nothing Then
            entLiquidacion = bl.com_LiquidacionSelectByPK(IdComprobante)
        End If

        If entLiquidacion.IdComprobante = 0 Then
            dl.CargaCns(forma, "com_Liquidacion", "", "IdComprobante", "Numero", "FechaContable", "Fecha", "Nombre", "Nrc", "Nit")
            forma.Text = "Consulta de Documentos de Liquidación"
            forma.showdialog()
            If Not forma.ValCodigo = "" Then
                entLiquidacion = bl.com_LiquidacionSelectByPK(forma.ValCodigo)
            End If

        End If
        Return entLiquidacion
    End Function
    Public Function ConsultaPedidosClientes(ByRef Forma As Object, ByVal Condicion As String) As String
        dl.ConsultaPedidosClientes(Forma, Condicion)
        Forma.ShowDialog()
        Return Forma.ValCodigo
    End Function
    Public Function ConsultaActivoFijo(ByRef Forma As Object, ByVal IdSucursal As Integer) As String
        dl.ConsultaActivoFijo(Forma, IdSucursal)
        Forma.ShowDialog()
        Return Forma.ValCodigo
    End Function
    Public Function ConsultaTrasladosActivoFijo(ByRef Forma As Object) As String
        dl.ConsultaTrasladosActivoFijo(Forma, "")
        Forma.ShowDialog()
        Return Forma.ValCodigo
    End Function
    Public Function cnsMantenimientos(ByVal forma As Object, ByVal Id As Integer) As acf_Mantenimientos
        Dim entMantenimiento As New acf_Mantenimientos

        If Id < 0 Then
            Return entMantenimiento
        End If

        If Id > 0 Then
            entMantenimiento = bl.acf_MantenimientosSelectByPK(Id)
        End If

        If entMantenimiento.Id = 0 Then
            dl.CargaCns(forma, "acf_Mantenimientos", "", "Id", "IdActivo", "Fecha", "IdFalla", "Valor", "DetalleProblema")
            forma.Text = "Consulta de Mantenimientos"
            forma.showdialog()
            If Not forma.ValCodigo = "" Then
                entMantenimiento = bl.acf_MantenimientosSelectByPK(forma.ValCodigo)
            End If
        End If
        Return entMantenimiento
    End Function

    Public Function banCuentasBancarias(ByVal forma As Object) As String
        dl.CargaCns(forma, "ban_CuentasBancarias", "", "IdCuentaBancaria", "Nombre", "NumeroCuenta")
        forma.text = "Consulta de cuentas bancarias"
        forma.showdialog()

        Return forma.ValCodigo
    End Function
    Public Function cnsConsultaDesembolsos(ByRef Forma As Object) As Object

        dl.CargaDesembolsos(forma)
        Forma.Text = "Consulta de Desembolsos"
        Forma.showdialog()
        Return Forma
    End Function
#End Region

#Region "ConsultasMaestroDetalle"
    Public Function ConsultaFormulasInv(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaFormulasInv
        forma.ds = ds
        forma.text = "Consulta de Formulas de Produccion de  Inventario"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaFactura(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaFactura
        forma.ds = ds
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaCheques(ByVal forma As Object, ByVal Fecha As String) As String
        Dim ds As DataSet = dl.ConsultaCheques(Fecha)
        forma.ds = ds
        forma.PrimaryKey = "IdCheque"
        forma.text = "Consulta de cheques"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaCotizaciones(ByVal forma As Object, ByVal IdVendedor As Integer) As Integer
        Dim ds As DataSet = dl.ConsultaCotizaciones(IdVendedor)
        If ds.Tables(0).Rows.Count <= 0 Then
            Return 0
        End If
        forma.ds = ds
        forma.PrimaryKey = "IdComprobante"
        forma.text = "Consulta de cotizaciones"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaQuedan(ByVal forma As Object) As Integer
        Dim ds As DataSet = dl.ConsultaQuedan
        If ds.Tables(0).Rows.Count <= 0 Then
            Return 0
        End If
        forma.ds = ds
        forma.PrimaryKey = "IdQuedan"
        forma.text = "Consulta de Quedan"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaTransacciones(ByVal forma As Object, ByVal Fecha As String) As String
        Dim ds As DataSet = dl.ConsultaTransacciones(Fecha)
        forma.ds = ds
        forma.PrimaryKey = "IdTransaccion"
        forma.text = "Consulta de transacciones bancarias"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaPartidas(ByVal forma As Object, ByVal Fecha As String) As String
        Dim ds As DataSet = dl.ConsultaPartidas(Fecha)
        forma.ds = ds
        forma.PrimaryKey = "IdPartida"
        forma.text = "Consulta de partidas"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaRemision(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaRemision
        forma.ds = ds
        forma.PrimaryKey = "IdComprobante"
        forma.text = "Consulta de notas de remisión"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function com_ConsultaCompras(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaCompras
        forma.ds = ds
        forma.Text = "Consulta de compras"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function com_ConsultaComprasQuedan(ByVal forma As Object, ByVal IdProveedor As String) As String
        Dim ds As DataSet = dl.ConsultaComprasQuedan(IdProveedor)
        forma.cnsDataTable = ds.Tables(0)
        forma.Text = "Consulta de compras"
        forma.showdialog()
        Return forma.ValCodigo
    End Function
    Public Function com_ConsultaOrdenesCompra(ByVal forma As Object) As String
        Dim ds As DataSet = dl.com_ConsultaOrdenesCompra
        forma.ds = ds
        forma.Text = "Consulta de Ordenes de compra"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function com_ConsultaRequisiciones(ByVal forma As Object) As String
        Dim ds As DataSet = dl.com_ConsultaRequisiciones
        forma.ds = ds
        forma.Text = "Consulta de Requisiciones para Ordenes de compra"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaImportaciones(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaImportaciones
        forma.ds = ds
        forma.text = "Consulta de importaciones"
        forma.showdialog()
        Return forma.IdCodigo
    End Function

    Public Function ConsultaPedidos(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaPedidos
        forma.ds = ds
        forma.text = "Consulta de Pedidos/Ordenes de Compra"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaEntradasInv(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaEntradasInv
        forma.ds = ds
        forma.text = "Consulta de Entradas al Inventario"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaSalidasInv(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaSalidasInv
        forma.ds = ds
        forma.text = "Consulta de Salidas del Inventario"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaTrasladosInv(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaTrasladosInv
        forma.ds = ds
        forma.text = "Consulta de Traslados de Inventario"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
    Public Function ConsultaEventos(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaEventos
        forma.ds = ds
        forma.text = "Consulta de Eventos"
        forma.showdialog()
        Return forma.IdCodigo
    End Function

    Public Function ConsultaInscripciones(ByVal forma As Object) As String
        Dim ds As DataSet = dl.ConsultaInscripciones
        forma.ds = ds
        forma.text = "Consulta de Inscripciones a Eventos"
        forma.showdialog()
        Return forma.IdCodigo
    End Function
#End Region
End Class
