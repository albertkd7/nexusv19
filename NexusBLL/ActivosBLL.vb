﻿Imports NexusDLL
Imports NexusELL.TableEntities

Public Class ActivosBusiness
    Dim dl As ActivosDLL
    Public Sub New(ByVal sStringName As String)
        dl = New ActivosDLL(sStringName)
    End Sub
    Public Function ConsultaActivos(ByVal IdSucursal As Integer) As DataTable
        Return dl.ConsultaActivos(IdSucursal)
    End Function
    Public Function GetActivos(ByVal IdClase As Integer, ByVal FechaRetiro As DateTime, ByVal IdSucursal As Integer) As DataTable
        Return dl.GetActivos(IdClase, FechaRetiro, IdSucursal)
    End Function
    Public Function ObtenerIdDocumento(ByVal Id As Integer, ByVal TipoAvance As Integer, ByVal Tabla As String) As Integer
        Return dl.ObtenerIdDocumento(Id, TipoAvance, Tabla)
    End Function
    Public Sub ActualizarRetiro(ByVal IdActivo As Integer, ByVal FechaRetiro As Date, ByVal IdMotivo As Integer, ByVal IdEstado As Integer, ByVal ValorRetiro As Decimal)
        dl.ActualizarRetiro(IdActivo, FechaRetiro, IdMotivo, IdEstado, ValorRetiro)
    End Sub
    Public Function SiExisteActivo(ByVal sCodActivo As String) As Integer
        Return dl.SiExisteActivo(sCodActivo)
    End Function
    Public Function ListCuadroDiferencias(ByVal Ejercicio As Integer, ByVal Mes As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.ListCuadroDiferencias(Ejercicio, Mes, IdSucursal)
    End Function
    Public Function ObtenerDetalleDocumento(ByVal IdComprobante As Integer, ByVal Tabla As String) As DataTable
        Return dl.ObtenerDetalleDocumento(IdComprobante, Tabla)
    End Function
    Public Function ImpresionTraslado(ByVal IdComprobante As Integer) As DataTable
        Return dl.ImpresionTraslado(IdComprobante)
    End Function
    Public Function HistorialTraslado(ByVal IdActivo As Integer) As DataTable
        Return dl.HistorialTraslado(IdActivo)
    End Function
    Public Function ListMantenimientos(ByVal IdActivo As Integer, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ListMantenimientos(IdActivo, Desde, Hasta, IdSucursal)
    End Function
    Function GuardarActivo(ByRef entActivo As acf_Activos, _
                           ByRef entDetalle As List(Of acf_DepreciacionesAjustes), ByVal EsNuevo As Boolean) As String
        Return dl.GuardarActivo(entActivo, entDetalle, EsNuevo)
    End Function
    Function GuardarTaslado(ByRef entTraslado As acf_Traslados, _
                       ByRef entDetalle As List(Of acf_TrasladosDetalle), ByVal EsNuevo As Boolean) As String
        Return dl.GuardarTaslado(entTraslado, entDetalle, EsNuevo)
    End Function
    Public Function ListActivosVendidos(ByVal IdClase As Integer, ByVal FechaI As DateTime, ByVal FechaF As DateTime, ByVal IdSucursal As Integer) As DataTable
        Return dl.ListActivosVendidos(IdClase, FechaI, FechaF, IdSucursal)
    End Function
    Public Function ListActivosRetirados(ByVal IdClase As Integer, ByVal FechaI As DateTime, ByVal FechaF As DateTime, ByVal IdSucursal As Integer) As DataTable
        Return dl.ListActivosRetirados(IdClase, FechaI, FechaF, IdSucursal)
    End Function

    Public Function rptComprobanteMantenimiento(ByVal IdMantenimiento As Integer) As DataTable
        Return dl.rptComprobanteMantenimiento(IdMantenimiento)
    End Function

    Public Function ImprimeActivoFijo(ByVal IdActivo As Integer) As DataTable
        Return dl.ImprimeActivoFijo(IdActivo)
    End Function

    Public Function ObtienePeriodoCerrado(ByVal IdSucursal As Integer) As DataTable
        Return dl.ObtienePeriodoCerrado(IdSucursal)
    End Function

    Public Function ContabilizarActivoFijo(ByVal Ejercicio As Integer, ByVal Mes As Integer, ByVal IdTipoPartida As String, ByVal Concepto As String, ByVal CreadoPor As String, ByVal IdSucursal As Integer) As String
        Return dl.ContabilizarActivoFijo(Ejercicio, Mes, IdTipoPartida, Concepto, CreadoPor, IdSucursal)
    End Function

    Public Function ExistePartidaActivo(ByVal Fecha As DateTime, ByVal IdSucursal As Integer) As Integer
        Return dl.ExistePartidaActivo(Fecha, IdSucursal)
    End Function

    Public Function RevertirContabilizarActivoFijo(ByVal ejercicio As Integer, ByVal num_mes As Integer, ByVal IdSucursal As Integer) As String
        Return dl.RevertirContabilizarActivoFijo(ejercicio, num_mes, IdSucursal)
    End Function

    Public Function ObtenerIdMantenimiento(ByVal IdMantenimiento As Integer, ByVal TipoAvance As Integer) As Integer
        Return dl.ObtenerIdMantenimiento(IdMantenimiento, TipoAvance)
    End Function


    Public Function DatosGeneralesActivoFijo(ByVal NumActivo As String) As DataTable
        Return dl.DatosGeneralesActivoFijo(NumActivo)
    End Function

    Public Function ListActivos(ByVal iIdClase As Integer, ByVal iIdUbica As Integer, ByVal iIdTipo As Integer, ByVal IdEstado As Integer, ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ListActivos(iIdClase, iIdUbica, iIdTipo, IdEstado, dFechaI, dFechaF, IdSucursal)
    End Function

    Public Function TodosActivos(ByVal IdSucursal As Integer) As DataTable
        Return dl.TodosActivos(IdSucursal)
    End Function
    Public Function ListActivosUbicaciones(ByVal iIdUbica As Integer, ByVal iIdEmpleado As Integer, ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ListActivosUbicaciones(iIdUbica, iIdEmpleado, dFechaI, dFechaF, IdSucursal)
    End Function

    Public Function ListActivosMarca(ByVal iIdMarca As Integer, ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ListActivosMarcas(iIdMarca, dFechaI, dFechaF, IdSucursal)
    End Function
    Public Function ListActivosCuentas(ByVal iIdCuenta As String, ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ListActivosCuentas(iIdCuenta, dFechaI, dFechaF, IdSucursal)
    End Function

    Public Function ListActivosGeneral(ByVal IdClase As Integer, ByVal IdUbicacion As Integer, ByVal iIdCuenta As String, ByVal TipoGrupo As Integer, ByVal Depreciable As Boolean, ByVal Depreciado As Boolean, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ListActivosGeneral(IdClase, IdUbicacion, iIdCuenta, TipoGrupo, Depreciable, Depreciado, dFechaF, IdSucursal)
    End Function
    Public Function ListActivosDepreciados(ByVal IdClase As Integer, ByVal IdUbicacion As Integer, ByVal iIdCuenta As String, ByVal TipoGrupo As Integer, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ListActivosDepreciados(IdClase, IdUbicacion, iIdCuenta, TipoGrupo, dFechaF, IdSucursal)
    End Function

    Public Function ListActivosEstado(ByVal iIdEstado As String, ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.ListActivosEstado(iIdEstado, dFechaI, dFechaF, IdSucursal)
    End Function

    Public Function ListDepreciacionIndividual(ByVal iIdActivo As Integer, ByVal IdTipo As Integer) As DataTable
        Return dl.ListDepreciacionIndividual(iIdActivo, IdTipo)
    End Function

End Class
