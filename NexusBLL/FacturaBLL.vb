﻿Imports NexusDLL
Imports NexusELL.TableEntities
Imports System.Windows.Forms
Public Class FacturaBLL
    Dim dl As FacturaDLL, dlData As TableData

    Public Sub New(ByVal StringConexion As String)
        dl = New FacturaDLL(StringConexion)
        dlData = New TableData(StringConexion)
    End Sub
    Public Function GetIdSucursal() As Integer
        Return dl.GetIdSucursal
    End Function
    Public Function fac_DocumentosAsignados() As DataTable
        Return dl.fac_DocumentosAsignados()
    End Function
    Public Function fac_EliminaKardexPedido(ByVal IdPedido As Integer) As String
        Return dl.fac_EliminaKardexPedido(IdPedido)
    End Function
    Public Function fac_InsertarKardexPedido(ByVal IdPedido As Integer) As String
        Return dl.fac_InsertarKardexPedido(IdPedido)
    End Function
    Public Function fac_InformeComprobantesRetencion(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipo As Integer, ByVal IdGerente As Integer, ByVal IdCliente As String, ByVal Enviar As Integer) As DataTable
        Return dl.fac_InformeComprobantesRetencion(Desde, Hasta, IdTipo, IdGerente, IdCliente, Enviar)
    End Function
    Public Function fac_ObtenerFormasPagoDetalle(ByVal IdComprobante As Integer) As DataTable
        Return dl.fac_ObtenerFormasPagoDetalle(IdComprobante)
    End Function
    Public Function GetRetencionesDetalle(ByVal IdComprobante As Integer) As DataTable
        Return dl.GetRetencionesDetalle(IdComprobante)
    End Function
    Public Function com_ObtenerRetencion(ByVal IdComprobante As Integer) As DataTable
        Return dl.com_ObtenerRetencion(IdComprobante)
    End Function
    Public Function fac_ConsultaFacturasCredito(ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Return dl.fac_ConsultaFacturasCredito(Desde, Hasta)
    End Function
    Public Function fac_AprobarFacturaCredito(ByVal IdDoc As Integer, ByVal AprobarReprobar As Integer, ByVal Fecha As DateTime, ByVal AprobadoPor As String) As String
        Return dl.fac_AprobarFacturaCredito(IdDoc, AprobarReprobar, FEcha, AprobadoPor)
    End Function
    Public Function fac_DocumentosLegales(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_DocumentosLegales(Desde, Hasta, IdSucursal, IdPunto)
    End Function
    Public Function fac_ConsultaSaldoCliente(ByVal idcliente As String, ByVal Dias As Integer) As DataTable
        Return dl.fac_ConsultaSaldoCliente(idcliente, Dias)
    End Function
    Public Function fac_ConsultaCotizaciones(ByVal Usuario As String, Optional IdVendedor As Integer = 0) As DataSet
        Return dl.fac_ConsultaCotizaciones(Usuario, IdVendedor)
    End Function
    Public Function fac_RptConsusltaFormasPago(ByVal DesdeFecha As DateTime, ByVal HastaFecha As DateTime, ByVal IdSucursal As Integer) As DataTable
        Return dl.fac_RptConsusltaFormasPago(DesdeFecha, HastaFecha, IdSucursal)
    End Function
    Public Function fac_spRptVentasRutas(ByVal DesdeFecha As DateTime, ByVal HastaFecha As DateTime, ByVal IdTipoComprobante As Integer, ByVal IdFormaPago As Integer, ByVal IdRuta As Integer) As DataTable
        Return dl.fac_spRptVentasRutas(DesdeFecha, HastaFecha, IdTipoComprobante, IdFormaPago, IdRuta)
    End Function
    Public Function fac_ValidaToken(ByVal Token As String, ByVal Descuento As Decimal, ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal IdProducto As String) As String
        Return dl.fac_ValidaToken(Token, Descuento, Fecha, IdSucursal, IdProducto)
    End Function
    Public Function fac_ListadoAutorizacionesPorTOKEN(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdUsuario As String) As DataTable
        Return dl.fac_ListadoAutorizacionesPorTOKEN(Desde, Hasta, IdSucursal, IdUsuario)
    End Function
    Public Function fac_DescuentoToken(ByVal Token As String) As Decimal
        Return dl.fac_DescuentoToken(Token)
    End Function
    Public Function fac_VentasSucursal(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.fac_VentasSucursal(Desde, Hasta, IdSucursal)
    End Function
    Public Function fac_VentasSucursalSubGrupos(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.fac_VentasSucursalSubGrupos(Desde, Hasta, IdSucursal)
    End Function
    Public Function fac_VentasSucursalMarca(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.fac_VentasSucursalMarca(Desde, Hasta, IdSucursal)
    End Function
    Public Function fac_VentasSucursalGrupos(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.fac_VentasSucursalGrupos(Desde, Hasta, IdSucursal)
    End Function
    Public Function fac_ConsultaVales(ByVal Usuario As String) As DataTable
        Return dl.fac_ConsultaVales(Usuario)
    End Function
    Public Function fac_ObtenerEstructuraPago() As DataTable
        Return dl.fac_ObtenerEstructuraPago()
    End Function
	Public Function fac_GestionesbyCliente(ByVal IdCliente As String, ByVal IdVendedor As Integer) As DataTable
		Return dl.fac_GestionesbyCliente(IdCliente, IdVendedor)
	End Function
	Public Function fac_GestionesbyIdProspecto(ByVal IdComprobante As String, ByVal IdVendedor As Integer) As DataTable
		Return dl.fac_GestionesbyIdProspecto(IdComprobante, IdVendedor)
	End Function
    Public Function fac_ClientesInfoAnexos(ByVal IdCliente As String) As DataTable
        Return dl.fac_ClientesInfoAnexos(IdCliente)
    End Function
    Public Function fac_ClientesObtieneNombre(ByVal IdCliente As String) As String
        Return dl.fac_ClientesObtieneNombre(IdCliente)
    End Function
    'Public Function fac_PedidosParaTraslados(ByVal IdBodega As Integer) As DataTable
    '    Return dl.fac_PedidosParaTraslados(IdBodega)
    'End Function
    Public Function rptDatosDeclaracionJuradaCliente(ByVal IdCliente As String) As DataTable
        Return dl.rptDatosDeclaracionJuradaCliente(IdCliente)
    End Function
    Public Function fac_PedidosParaTraslados(ByVal IdSucursal As Integer) As DataTable
        Return dl.fac_PedidosParaTraslados(IdSucursal)
    End Function
    Public Function fac_ConsultaAperturasCaja(ByVal Usuario As String) As DataTable
        Return dl.fac_ConsultaAperturasCaja(Usuario)
    End Function
    Public Function fac_ConsultaRetenciones(ByVal Usuario As String) As DataSet
        Return dl.fac_ConsultaRetenciones(Usuario)
    End Function
    Public Function fac_ExisteAperturaCaja(ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As Integer
        Return dl.fac_ExisteAperturaCaja(Fecha, IdSucursal, IdPunto)
    End Function
    Public Function fac_ObtenerSeriesDocumentos(ByVal IdUsuario As String) As DataTable
        Return dl.fac_ObtenerSeriesDocumentos(IdUsuario)
    End Function
    Public Function InsertRemision(ByRef RemisionHeader As fac_NotasRemision, ByRef RemisionDetalle As List(Of fac_NotasRemisionDetalle)) As String
        Return dl.InsertRemision(RemisionHeader, RemisionDetalle)
    End Function
    Public Function InsertRetencion(ByRef RetencionHeader As fac_Retencion, ByRef RetencionDetalle As List(Of fac_RetencionDetalle)) As String
        Return dl.InsertRetencion(RetencionHeader, RetencionDetalle)
    End Function
    Public Function InsertVendedores(ByRef Vendedores As fac_Vendedores, ByRef DetalleComision As List(Of fac_VendedoresDetalle), ByRef DetalleClientesVendedor As List(Of fac_VendedoresClientes)) As String
        Return dl.InsertVendedores(Vendedores, DetalleComision, DetalleClientesVendedor)
    End Function
    Public Function inv_ObtieneProductoAlias(ByVal Codigo As String) As String
        Return dl.inv_ObtieneProductoAlias(Codigo)
    End Function
    Public Function UpdateVendedores(ByRef Vendedores As fac_Vendedores, ByRef DetalleComision As List(Of fac_VendedoresDetalle), ByRef DetalleClientesVendedor As List(Of fac_VendedoresClientes)) As String
        Return dl.UpdateVendedores(Vendedores, DetalleComision, DetalleClientesVendedor)
    End Function
    Public Function UpdateRemision(ByRef RemisionHeader As fac_NotasRemision, ByRef RemisionDetalle As List(Of fac_NotasRemisionDetalle)) As String
        Return dl.UpdateRemision(RemisionHeader, RemisionDetalle)
    End Function
    Public Function UpdateRetencion(ByRef RetencionHeader As fac_Retencion, ByRef RetecionDetalle As List(Of fac_RetencionDetalle)) As String
        Return dl.UpdateRetencion(RetencionHeader, RetecionDetalle)
    End Function
    Public Function fac_ObtenerPedidoPreHoja(ByVal IdPedido As Integer, ByVal IncluirCaracteristas As Boolean) As DataTable
        Return dl.fac_ObtenerPedidoPreHoja(IdPedido, IncluirCaracteristas)
    End Function
    Public Function fac_ConsultaGerencial(ByVal Desde As Date, ByVal Hasta As Date, ByVal TipoDoc As Integer) As DataTable
        Return dl.fac_ConsultaGerencial(Desde, Hasta, TipoDoc)
    End Function
    Public Function fac_ConsultaHistorialPorCodigoProducto(ByVal Codigo As String) As DataSet
        Return dl.fac_ConsultaHistorialPorCodigoProducto(Codigo)
    End Function
    Public Function fac_VerificaFacturadoPedido(ByVal IdPedido As Integer) As Integer
        Return dl.fac_VerificaFacturadoPedido(IdPedido)
    End Function
    Public Function GetObtieneCorrelativoFacturacion(ByVal IdPunto As Integer, ByVal IdTipoComprobante As Integer) As Integer
        Return dl.GetObtenerCorrelativoFacturacion(IdPunto, IdTipoComprobante)
    End Function
    Public Function GetObtieneCorrelativoFacturacionUnico(ByVal IdPunto As Integer, ByVal IdTipoComprobante As Integer) As Integer
        Return dl.GetObtenerCorrelativoFacturacionUnico(IdPunto, IdTipoComprobante)
    End Function
    Public Function GetObtenerSerieDocumento(ByVal IdPunto As Integer, ByVal IdTipoComprobante As Integer) As String
        Return dl.GetObtenerSerieDocumento(IdPunto, IdTipoComprobante)
    End Function

    Public Function inv_GeneralesProducto(ByVal IdProducto As String) As DataTable
        Return dl.inv_GeneralesProducto(IdProducto)
    End Function
    Public Function fac_ConsultaPedidosDetalleGerencial(ByVal IdComprobante As Integer) As DataTable
        Return dl.fac_ConsultaPedidosDetalleGerencial(IdComprobante)
    End Function
    Public Function fac_FechaServer() As Date
        Return dl.fac_FechaServer()
    End Function
    Public Function fac_VerificaExistenciasPorIdPedido(ByVal IdPedido As Integer, ByVal IdBodega As Integer, ByVal Tipo As Boolean) As DataTable
        Return dl.fac_VerificaExistenciasPorIdPedido(IdPedido, IdBodega, Tipo)
    End Function
    Public Function fac_ObtenerIdNotaRemision(ByVal Id As Integer, ByVal TipoAvance As Integer) As Integer
        Return dl.fac_ObtenerIdNotaRemision(Id, TipoAvance)
    End Function
    Public Function fac_ObtenerIdVale(ByVal Id As Integer, ByVal TipoAvance As Integer) As Integer
        Return dl.fac_ObtenerIdVale(Id, TipoAvance)
    End Function
    Public Function fac_ObtenerIdRetencion(ByVal Id As Integer, ByVal TipoAvance As Integer) As Integer
        Return dl.fac_ObtenerIdRetencion(Id, TipoAvance)
    End Function
    Public Function fac_ObtenerClientesPrecios(ByVal IdCliente As String) As DataTable
        Return dl.fac_ObtenerClientesPrecios(IdCliente)
    End Function
    Public Function fac_ObtenerHorasClientes(ByVal Desde As DateTime, ByVal Hasta As DateTime, ByVal IdCliente As String) As DataTable
        Return dl.fac_ObtenerHorasClientes(Desde, Hasta, IdCliente)
    End Function
    Public Function fac_InsertarCotizacion(ByRef entCotizacion As fac_Cotizaciones, ByRef entDetalle As List(Of fac_CotizacionesDetalle)) As String
        Return dl.fac_InsertarCotizacion(entCotizacion, entDetalle)
    End Function
    Public Function fac_ConsultaProductos(ByVal Condicion As String) As DataTable
        Return dl.fac_ConsultaProductos(Condicion)
    End Function
    Public Function fac_ConsultaProductosPrecios(ByVal Fecha As DateTime, ByVal IdGrupo As Integer) As DataTable
        Return dl.fac_ConsultaProductosPrecios(Fecha, IdGrupo)
    End Function
    Public Function fac_ActualizarCotizacion(ByRef entCotizacion As fac_Cotizaciones, ByRef entDetalle As List(Of fac_CotizacionesDetalle)) As String
        Return dl.fac_ActualizarCotizacion(entCotizacion, entDetalle)
    End Function
    Public Function fac_ObtenerDetalleDocumento(ByVal Tabla As String, ByVal IdComprobante As Integer) As DataTable
        Return dl.fac_ObtenerDetalleDocumento(Tabla, IdComprobante)
    End Function
    Public Function fac_ObtenerDetalleDocumentoVentaPedido(ByVal IdComprobante As Integer) As DataTable
        Return dl.fac_ObtenerDetalleDocumentoVentaPedido(IdComprobante)
    End Function
    Public Function fac_ObtenerDetalleCotizacion(ByVal IdComprobante As Integer) As DataTable
        Return dl.fac_ObtenerDetalleCotizacion(IdComprobante)
    End Function
    Public Function fac_ObtenerDetalleVendedores(ByVal IdVendedor As Integer) As DataTable
        Return dl.fac_ObtenerDetalleVendedores(IdVendedor)
    End Function
    Public Function fac_ObtenerClientesVendedor(ByVal IdVendedor As Integer) As DataTable
        Return dl.fac_ObtenerClientesVendedor(IdVendedor)
    End Function
    Public Function fac_ExisteRCV() As Boolean
        Return dl.fac_ExisteRCV()
    End Function
    Public Function fac_rpt_RCV(ByVal IdVendedor As Integer) As DataTable
        Return dl.fac_rpt_RCV(IdVendedor)
    End Function
    Public Function fac_GetVendedores() As DataTable
        Return dl.fac_GetVendedores()
    End Function
	Public Function fac_GestionesPeriodos(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdCliente As String, ByVal IdVendedor As Integer, ByVal IdCotizacion As Integer) As DataTable
		Return dl.fac_GestionesPeriodos(Desde, Hasta, IdCliente, IdVendedor, IdCotizacion)
	End Function
	Public Function fac_GestionesProspectosPeriodos(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProspecto As Integer, ByVal IdVendedor As Integer, ByVal RepGestiones As Boolean) As DataTable
		Return dl.fac_GestionesProspectosPeriodos(Desde, Hasta, IdProspecto, IdVendedor, RepGestiones)
	End Function
	Public Function fac_GetCitasAll(ByVal Fecha As Date) As DataTable
        Return dl.fac_GetCitasAll(Fecha)
    End Function
    Public Function fac_ObtenerCotizacionPedido(ByVal IdComprobante As Integer, Tipo As Integer) As DataTable
        Return dl.fac_ObtenerCotizacionPedido(IdComprobante, Tipo)
    End Function
    Public Function fac_ObtenerAfectaNC(ByVal IdComprobante As Integer) As DataTable
        Return dl.fac_ObtenerAfectaNC(IdComprobante)
    End Function
    Public Function fac_InsertaFacturaImportacion(ByRef FacturaHeader As fac_Ventas, ByRef FacturaDetalle As List(Of fac_VentasDetalle)) As String
        Return dl.fac_InsertaFacturaImportacion(FacturaHeader, FacturaDetalle)
    End Function
    'PEDIDOS DE MERCADERÍA
    Public Function fac_InsertarPedido(ByRef PedidoHeader As fac_Pedidos, ByRef PedidoDetalle As List(Of fac_PedidosDetalle)) As String
        Return dl.fac_InsertarPedido(PedidoHeader, PedidoDetalle)
    End Function
    Public Function fac_ActualizaPedido(ByRef PedidoHeader As fac_Pedidos, ByRef PedidoDetalle As List(Of fac_PedidosDetalle)) As String
        Return dl.fac_ActualizaPedido(PedidoHeader, PedidoDetalle)
    End Function
    Public Function fac_ObtenerIdPedidoPorNumero(ByVal Numero As String) As Integer
        Return dl.fac_ObtenerIdPedidoPorNumero(Numero)
    End Function
    Public Function fac_ObtenerPedidoDetalle(ByVal IdComprobante As Integer, ByVal Impuesto As Decimal, ByRef DocumentoDetallaIva As Boolean, ByVal TipoVenta As Integer) As DataTable
        'fac_ObtenerPedidoDetalle(ByVal IdComprobante As Integer, ByVal Impuesto As Decimal, ByRef DocumentoDetallaIva As Boolean, ByVal TipoVenta As Integer) As DataTable
        Return dl.fac_ObtenerPedidoDetalle(IdComprobante, Impuesto, DocumentoDetallaIva, TipoVenta)
    End Function

    Public Function fac_ObtenerContrasenaDescuento(ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal Descuento As Decimal, ByVal Usuario As String, ByVal IdProducto As String) As String
        Return dl.fac_ObtenerContrasenaDescuento(Fecha, IdSucursal, Descuento, Usuario, IdProducto)
    End Function
    Public Function fac_ObtienePrecioCliente(ByVal IdProducto As String, ByVal IdCliente As String) As Decimal
        Return dl.fac_ObtienePrecioCliente(IdProducto, IdCliente)
    End Function
    Public Function fac_ObtenerIdPedido(ByVal IdComprobante As Integer, ByVal TipoAvance As Integer) As Integer
        Return dl.fac_ObtenerIdPedido(IdComprobante, TipoAvance)
    End Function
    Public Function fac_ObtenerIdDocumento(ByVal Tabla As String, ByVal IdComprobante As Integer, ByVal TipoAvance As Integer) As Integer
        Return dl.fac_ObtenerIdDocumento(Tabla, IdComprobante, TipoAvance)
    End Function
    Public Function fac_VerificaCodigoCliente(ByVal IdCliente As String) As Boolean
        Return dl.fac_VerificaCodigoCliente(IdCliente)
    End Function
    Public Function fac_ConsultaPedidos(ByVal IdUsusario As String) As DataTable
        Return dl.fac_ConsultaPedidos(IdUsusario)
    End Function
    Public Function fac_ConsultaPedidos(ByVal IdUsusario As String, ByVal Desde As Date, ByVal Hasta As Date, ByVal Pendiente As Boolean) As DataTable
        Return dl.fac_ConsultaPedidos(IdUsusario, Desde, Hasta, Pendiente)
    End Function
    Public Function fac_ConsultaFacturacion(ByVal Desde As Date, ByVal Hasta As Date, ByVal TipoDoc As Integer, ByVal IdUsuario As String) As DataSet
        Return dl.fac_ConsultaFacturacion(Desde, Hasta, TipoDoc, IdUsuario)
    End Function
    Public Function fac_PedidosPendientes(ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Return dl.fac_PedidosPendientes(Desde, Hasta)
    End Function
    Public Function fac_DetalleVale(ByVal ID As Integer) As DataTable
        Return dl.fac_DetalleVale(ID)
    End Function
    Public Function fac_InsertaFactura(ByRef FacturaHeader As fac_Ventas, ByRef FacturaDetalle As List(Of fac_VentasDetalle), ByRef FacturaDetallePago As List(Of fac_VentasDetallePago), ByVal AsignarNumero As Boolean) As String
        Return dl.fac_InsertaFactura(FacturaHeader, FacturaDetalle, FacturaDetallePago, "VENTA DIRECTA", AsignarNumero)
    End Function
    Public Function fac_ObtenerPedido(ByVal IdComprobante As Integer) As DataTable
        Return dl.fac_ObtenerPedido(IdComprobante)
    End Function
    Public Function fac_ObtenerNotaRemision(ByVal IdComprobante As Integer) As DataTable
        Return dl.fac_ObtenerNotaRemision(IdComprobante)
    End Function
    Public Function fac_ObtenerFormasPago(ByVal IdFormaPago As Integer, ByVal TotalComprobante As Decimal) As DataTable
        Return dl.fac_ObtenerFormasPago(IdFormaPago, TotalComprobante)
    End Function
    Public Function getIdComprobante(ByVal iIdSucursal As Integer, ByVal iIdPunto As Integer, ByVal iIdTipoComprobante As Integer, ByVal sSerie As String, ByVal sNumero As String) As Integer
        Return dl.getIdComprobante(iIdSucursal, iIdPunto, iIdTipoComprobante, sSerie, sNumero)
    End Function
    Public Function fac_SaldoComprobante(ByVal IdCliente As String, ByVal Fecha As Date, ByVal IdComprobante As Integer) As DataTable
        Return dl.fac_SaldoComprobante(IdCliente, Fecha, IdComprobante)
    End Function
    Public Function Verificar_Nota(ByVal iIdComprobante As Integer) As String
        Return dl.Verificar_Nota(iIdComprobante)
    End Function
    Public Function Verificar_Abonos(ByVal iIdComprobante As Integer) As String
        Return dl.Verificar_Abonos(iIdComprobante)
    End Function
    Public Function Verificar_AbonosEventos(ByVal iIdComprobante As Integer) As String
        Return dl.Verificar_AbonosEventos(iIdComprobante)
    End Function
    Public Function fac_ObtenerSerieDocumento(ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdTipoDoc As Integer) As String
        Return dl.fac_ObtenerSerieDocumento(IdSucursal, IdPunto, IdTipoDoc)
    End Function
    Public Function fac_AnulaDocumentoVenta(ByVal IdComprobante As Integer, ByVal MotivoAnulacion As String, ByVal FechaAnulacion As Date, ByVal AnuladoPor As String, ByVal eliminar As Integer, ByVal CreaDocumento As Integer, ByRef entFactura As fac_Ventas) As String
        Return dl.fac_AnulaDocumentoVenta(IdComprobante, MotivoAnulacion, FechaAnulacion, AnuladoPor, eliminar, CreaDocumento, entFactura)
    End Function

    Public Sub fac_TipoAplicacionComprobante(ByVal IdComprobante As Integer, ByRef DocumentoDetallaIva As Boolean, ByRef TipoAplicacion As Integer, ByRef LimiteLineas As Integer)
        dl.fac_TipoAplicacionComprobante(IdComprobante, DocumentoDetallaIva, TipoAplicacion, LimiteLineas)
    End Sub
    Public Sub fac_ObtenerDatosEncabezadoPieTicket(ByRef DatosEncabezado As String, ByRef DatosPie As String, IdPunto As Integer)
        dl.fac_ObtenerDatosEncabezadoPieTicket(DatosEncabezado, DatosPie, IdPunto)
    End Sub
    Public Function ContabilizarVentas(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sTipoPartida As String, ByVal sUserId As String, ByVal sConcepto As String, ByVal IdSucursal As Integer) As String
        Return dl.ContabilizarVentas(dFechaIni, dFechaFin, sTipoPartida, sUserId, sConcepto, IdSucursal)
    End Function
    Public Function fac_GeneraFacturaPedido(ByVal IdPedido As Integer, ByVal Usuario As String, ByVal Iva As Decimal) As String
        Dim FacturaHeader As New fac_Ventas
        Dim PedidoHeader As fac_Pedidos = dlData.fac_PedidosSelectByPK(IdPedido)

        With FacturaHeader
            .IdSucursal = PedidoHeader.IdSucursal
            .IdPunto = 1
            .IdTipoComprobante = PedidoHeader.IdTipoComprobante
            .Fecha = Today
            .IdCliente = PedidoHeader.IdCliente
            .Nombre = PedidoHeader.Nombre
            .Nrc = PedidoHeader.Nrc
            .Nit = PedidoHeader.Nit
            .Giro = PedidoHeader.Giro
            .Direccion = PedidoHeader.Direccion
            .Telefono = PedidoHeader.Telefono
            .VentaAcuentaDe = PedidoHeader.VentaAcuentaDe
            .IdMunicipio = PedidoHeader.IdMunicipio
            .IdDepartamento = PedidoHeader.IdDepartamento
            .IdFormaPago = PedidoHeader.IdFormaPago
            .DiasCredito = PedidoHeader.DiasCredito
            .IdVendedor = PedidoHeader.IdVendedor
            .IdComprobanteNota = 0
            .OrdenCompra = PedidoHeader.Numero
            .IdBodega = PedidoHeader.IdBodega
            .TipoVenta = PedidoHeader.TipoVenta
            .TipoImpuesto = PedidoHeader.TipoImpuesto

            .MotivoAnulacion = ""
            .AnuladoPor = ""
            .CreadoPor = Usuario
            .FechaHoraCreacion = Now
            .ModificadoPor = ""
            .FechaHoraModificacion = Nothing
        End With

        FacturaHeader.TotalNeto = 0.0

        Dim dt As DataTable = dl.fac_ObtenerDetalleDocumento("fac_pedidos", PedidoHeader.IdComprobante)
        Dim detFactura As New List(Of fac_VentasDetalle)
        Dim detFormaPago As New List(Of fac_VentasDetallePago)
        Dim i As Integer = 1
        Dim TotalIva As Decimal = 0.0, TotalDoc As Decimal = 0.0
        For Each fila As DataRow In dt.Rows
            Dim FacturaDetalle As New fac_VentasDetalle
            With FacturaDetalle
                .IdComprobante = 0
                .IdDetalle = i
                .IdProducto = fila.Item("IdProducto")
                .IdPrecio = fila.Item("IdPrecio")
                .Cantidad = fila.Item("Cantidad")
                .Descripcion = fila.Item("Descripcion")
                .PrecioVenta = fila.Item("PrecioVenta")
                .PorcDescuento = fila.Item("PorcDescuento")
                .ValorDescuento = fila.Item("ValorDescuento")
                .PrecioUnitario = fila.Item("PrecioUnitario")
                If FacturaHeader.TipoVenta = 2 Then
                    .VentaAfecta = 0
                    .VentaExenta = .PrecioUnitario * .Cantidad
                Else
                    .VentaAfecta = .PrecioUnitario * .Cantidad
                    .VentaExenta = 0
                End If

                .TipoImpuesto = PedidoHeader.TipoVenta

                'cuando el iva esta incluído en la facturacion
                .VentaNeta = Decimal.Round(.VentaAfecta / (Iva + 1), 2)
                .ValorIVA = Decimal.Round(.VentaAfecta - .VentaNeta, 2)
                .VentaNeta = .VentaNeta + .VentaExenta

                .CreadoPor = Usuario
                .FechaHoraCreacion = Now
                FacturaHeader.TotalDescto = FacturaHeader.TotalDescto + .ValorDescuento
                FacturaHeader.TotalNeto = (FacturaHeader.TotalNeto + .VentaNeta)
                FacturaHeader.TotalComprobante = FacturaHeader.TotalComprobante + (.VentaNoSujeta + .VentaExenta + .VentaAfecta)
                FacturaHeader.TotalAfecto = FacturaHeader.TotalAfecto + .VentaAfecta
                FacturaHeader.TotalExento = FacturaHeader.TotalExento + .VentaExenta
                FacturaHeader.TotalIva = FacturaHeader.TotalIva + .ValorIVA
                i = i + 1
            End With
            detFactura.Add(FacturaDetalle)
        Next
        FacturaHeader.PorcDescto = 0.0
        FacturaHeader.TotalImpuesto1 = 0.0
        FacturaHeader.TotalImpuesto2 = 0.0
        FacturaHeader.SaldoActual = FacturaHeader.TotalComprobante

        Return dl.fac_InsertaFactura(FacturaHeader, detFactura, detFormaPago, "PEDIDO", True)
    End Function
    Public Function fac_ObtenerDetallePedidoParaArchivo(ByVal IdComprobante As Integer) As DataTable
        Return dl.fac_ObtenerDetallePedidoParaArchivo(IdComprobante)
    End Function
    Public Function fac_ObtenerVentasPedido(ByVal IdComprobante As Integer) As DataTable
        Return dl.fac_ObtenerVentasPedido(IdComprobante)
    End Function
    Public Function fac_ActualizaFormaPago(ByVal IdComprobante As Integer, ByVal IdPedio As Integer, ByVal IdFormaPago As Integer) As String
        Return dl.fac_ActualizaFormaPago(IdComprobante, IdPedio, IdFormaPago)
    End Function
    Public Function fac_ExtraePedidoAnular(ByVal IdComprobante As Integer) As DataTable
        Return dl.fac_ExtraePedidoAnular(IdComprobante)
    End Function
    Public Function fac_GenerarFacturaPedido(ByVal IdPedido As Integer, ByVal Nombre As String, ByVal Nrc As String, ByVal Nit As String, ByVal Usuario As String, ByVal PorcIva As Decimal, ByVal Descto As Decimal, ByVal EsGrande As Boolean, ByRef nf As Integer, ByVal TipoComprobante As Integer, ByVal IdFormaPago As Integer, ByVal FechaActiva As Date, ByVal RecalcularIva As Boolean) As String
        Dim Factura As New fac_Ventas
        Dim FacturaDetalle As New List(Of fac_VentasDetalle)
        Dim PedidoHeader As fac_Pedidos = dlData.fac_PedidosSelectByPK(IdPedido)
        Dim entCliente As fac_Clientes = dlData.fac_ClientesSelectByPK(PedidoHeader.IdCliente)
        Dim dtTipo As adm_TiposComprobante = dlData.adm_TiposComprobanteSelectByPK(TipoComprobante) 'PedidoHeader.IdTipoComprobante 
        Dim dtFact As DataTable = dl.fac_GenerarFacturaPedido(PedidoHeader.IdComprobante, Descto)
        Dim TipoAplicacion As Integer = dtTipo.TipoAplicacion
        Dim FacturaDetallePago As New List(Of fac_VentasDetallePago)

        If dtTipo.TipoAplicacion = 2 Or PedidoHeader.Devolucion Then
            TipoAplicacion = -1
        End If

        Dim i As Integer = 1, msj As String = "", TipoImpuestoRetencionPercepcion As Integer = 0, ItemsPreHoja As Integer = 0
        If Not EsGrande Then 'pendiente de verificar el tipo de contribuyente y si es agente de percepción
            TipoImpuestoRetencionPercepcion = 1
        End If
        PedidoHeader.Nombre = Nombre
        PedidoHeader.Nrc = Nrc
        PedidoHeader.Nit = Nit
        PedidoHeader.IdTipoComprobante = TipoComprobante
        PedidoHeader.IdFormaPago = IdFormaPago
        PedidoHeader.CreadoPor = Usuario
        PedidoHeader.Fecha = FechaActiva

        ItemsPreHoja = dtFact.Rows.Count
        For Each fila As DataRow In dtFact.Rows
            Dim Detalle As New fac_VentasDetalle
            With Detalle
                .IdComprobante = 0
                .IdDetalle = i
                .IdProducto = fila.Item("IdProducto")
                .IdPrecio = fila.Item("IdPrecio")
                .Cantidad = fila.Item("Cantidad")
                .Descripcion = fila.Item("Descripcion")
                .PrecioVenta = fila.Item("PrecioVenta")
                .PorcDescuento = fila.Item("PorcDescuento")
                .ValorDescuento = fila.Item("ValorDescuento")
                .PrecioUnitario = fila.Item("PrecioUnitario")
                'SI EL DOCUMENTO DETALLA EL IVA Y EL TIPO DE VENTA ES GRAVADO
                If dtTipo.DetallaIVA And PedidoHeader.TipoVenta = 1 Then
                    .PrecioUnitario = Decimal.Round(.PrecioUnitario / (PorcIva + 1), 6)
                End If
                'LA VENTA ES GRAVADA
                If PedidoHeader.TipoVenta = 1 Then
                    .VentaAfecta = fila.Item("PrecioTotal")
                    .VentaNeta = Decimal.Round(CDec(fila.Item("PrecioTotal")) / (PorcIva + 1), 2, MidpointRounding.AwayFromZero)
                    .ValorIVA = .VentaAfecta - .VentaNeta
                    If dtTipo.DetallaIVA Then 'PARA EL CASO DEL CCF Y NC
                        .VentaAfecta = .VentaNeta
                    End If
                    .VentaExenta = 0
                Else
                    .VentaAfecta = 0
                    .ValorIVA = 0
                    .VentaExenta = fila.Item("PrecioTotal")
                    .VentaNeta = .VentaExenta
                End If

                .TipoImpuesto = PedidoHeader.TipoVenta
                .PrecioCosto = fila.Item("PrecioCosto")
                .CreadoPor = Usuario
                .FechaHoraCreacion = Now

                If i = 1 Then  'en la primer fila se llena la entidad de encabezado
                    LlenarEntidadFactura(PedidoHeader, Factura, TipoAplicacion)
                End If
                Factura.TotalDescto += .ValorDescuento
                Factura.TotalNeto += .VentaNeta
                Factura.TotalAfecto += .VentaAfecta
                Factura.TotalExento += .VentaExenta
                Factura.TotalIva += .ValorIVA
                Factura.DiasCredito = entCliente.DiasCredito
                'La columna SaldoActual se está utilizando para guardar el total del pedido con precio de venta
                Factura.SaldoActual += Decimal.Round(.PrecioVenta * .Cantidad, 2, MidpointRounding.AwayFromZero)
            End With
            FacturaDetalle.Add(Detalle)

            'cuando se alcanza el limite de líneas por documento se procede a generar la factura
            If i = dtTipo.NumeroLineas Then 'se generará la factura cuando se llega al número de filas por documento

                'SE EVALUA SI SE RECALCULA EL IVA, CUANDO ES CRÉDITO FISCAL, DESACTIVADO EL PROCESO
                'If RecalcularIva Then
                If dtTipo.DetallaIVA Then 'CCF, NCI, SE DETERMINA EL IMPUESTO EN BASE AL TOTAL AFECTO
                    Factura.TotalIva = Decimal.Round(Factura.TotalAfecto + Factura.TotalIva, 2)
                    Factura.TotalNeto = Decimal.Round(Factura.TotalIva / (PorcIva + 1), 2)
                    Factura.TotalAfecto = Factura.TotalNeto
                    Factura.TotalIva = Decimal.Round(Factura.TotalIva - Factura.TotalAfecto, 2)
                Else
                    Factura.TotalNeto = Decimal.Round(Factura.TotalAfecto / (PorcIva + 1), 2)
                    Factura.TotalIva = Decimal.Round(Factura.TotalAfecto - Factura.TotalNeto, 2)
                End If

                'TipoImpuestoRetencionPercepcion= 0, no aplica ninguno, 1= es retención, 2= percepción
                Dim totalParaRetencionPercepcion As Decimal = Factura.TotalAfecto
                If Not dtTipo.DetallaIVA Then  'PARA FACTURA DE CONSUMIDOR FNAL
                    totalParaRetencionPercepcion = Decimal.Round(Factura.TotalAfecto / (PorcIva), 2)
                End If
                If TipoImpuestoRetencionPercepcion > 0 And totalParaRetencionPercepcion >= 100 And PedidoHeader.AplicaRetencionPercepcion = True Then
                    Factura.TotalImpuesto1 = Decimal.Round(CDec(totalParaRetencionPercepcion * 0.01), 2)
                End If

                Factura.SaldoActual += Factura.TotalImpuesto1
                Factura.TotalComprobante = Factura.TotalNoSujeto + Factura.TotalExento + Factura.TotalAfecto

                If dtTipo.DetallaIVA Then  'se está emitiendo crédito fiscal, por lo tanto el IVA se suma
                    Factura.TotalComprobante += Factura.TotalIva
                End If
                'ALMACENANDO EL TOTAL FINAL DEL DOCUMENTO

                If TipoImpuestoRetencionPercepcion = 1 Then  ' es una retención la que se le está aplicando, por lo tanto se resta
                    Factura.TotalComprobante -= Factura.TotalImpuesto1
                Else 'es la percepción la que se le aplica, por lo tanto se suma
                    Factura.TotalComprobante += Factura.TotalImpuesto1
                End If

                Factura.PorcDescto = Descto
                msj = dl.fac_InsertaFactura(Factura, FacturaDetalle, FacturaDetallePago, "PEDIDO", True) 'se inserta la factura

                ' se crea una nueva entidad para la nueva factura
                Factura = New fac_Ventas
                FacturaDetalle = New List(Of fac_VentasDetalle)
                nf += 1

                ItemsPreHoja -= i 'resto las líneas que se han facturado
                i = 0  'se setea a cero para que vuelva a iniciar
            End If
            i += 1
        Next
        'si no generó ningún error previo, se inserta la última factura o la única sino sobrepasa del número de líneas
        If msj = "" And ItemsPreHoja > 0 Then

            'SE RECALCULA EL IVA EN BASE AL TOTAL, CUANDO ES CRÉDITO FISCAL
            If dtTipo.DetallaIVA Then 'CCF, NCI, SE DETERMINA EL IMPUESTO EN BASE AL TOTAL AFECTO
                Factura.TotalIva = Decimal.Round(Factura.TotalAfecto + Factura.TotalIva, 2)
                Factura.TotalNeto = Decimal.Round(Factura.TotalIva / (PorcIva + 1), 2)
                Factura.TotalAfecto = Factura.TotalNeto
                Factura.TotalIva = Decimal.Round(Factura.TotalIva - Factura.TotalAfecto, 2)
            Else
                Factura.TotalNeto = Decimal.Round(Factura.TotalAfecto / (PorcIva + 1), 2)
                Factura.TotalIva = Decimal.Round(Factura.TotalAfecto - Factura.TotalNeto, 2)
            End If

            'TipoImpuestoRetencionPercepcion= 0, no aplica ninguno, 1= es retención, 2= percepción
            Dim totalParaRetencionPercepcion As Decimal = Factura.TotalAfecto
            If Not dtTipo.DetallaIVA Then
                totalParaRetencionPercepcion = Decimal.Round(Factura.TotalAfecto / (PorcIva + 1), 2)
            End If
            If TipoImpuestoRetencionPercepcion > 0 And totalParaRetencionPercepcion >= 100 And PedidoHeader.AplicaRetencionPercepcion = True Then
                Factura.TotalImpuesto1 = Decimal.Round(CDec(totalParaRetencionPercepcion * 0.01), 2)
            End If

            Factura.SaldoActual += Factura.TotalImpuesto1
            Factura.TotalComprobante = Factura.TotalNoSujeto + Factura.TotalExento + Factura.TotalAfecto

            If dtTipo.DetallaIVA Then  'se está emitiendo crédito fiscal, por lo tanto el IVA se suma
                Factura.TotalComprobante += Factura.TotalIva
            End If
            'ALMACENANDO EL TOTAL FINAL DEL DOCUMENTO

            If TipoImpuestoRetencionPercepcion = 1 Then  ' es una retención la que se le está aplicando, por lo tanto se resta
                Factura.TotalComprobante -= Factura.TotalImpuesto1
            Else 'es la percepción la que se le aplica, por lo tanto se suma
                Factura.TotalComprobante += Factura.TotalImpuesto1
            End If

            Factura.PorcDescto = Descto
            Return dl.fac_InsertaFactura(Factura, FacturaDetalle, FacturaDetallePago, "PEDIDO", True)
        Else
            Return msj
        End If
    End Function
    Private Sub LlenarEntidadFactura(ByVal Pedido As fac_Pedidos, ByRef Factura As fac_Ventas, ByVal TipoAplicacion As Integer)

        With Factura
            .IdSucursal = Pedido.IdSucursal
            .IdPunto = 1
            .IdTipoComprobante = Pedido.IdTipoComprobante
            .Fecha = Pedido.Fecha
            .IdCliente = Pedido.IdCliente
            .Nombre = Pedido.Nombre
            .Nrc = Pedido.Nrc
            .Nit = Pedido.Nit
            .Giro = Pedido.Giro
            .Direccion = Pedido.Direccion
            .Telefono = Pedido.Telefono
            .VentaAcuentaDe = Pedido.VentaAcuentaDe
            .IdMunicipio = Pedido.IdMunicipio
            .IdDepartamento = Pedido.IdDepartamento
            .IdFormaPago = Pedido.IdFormaPago
            .DiasCredito = Pedido.DiasCredito
            .IdVendedor = Pedido.IdVendedor
            .IdComprobanteNota = Pedido.IdComprobante 'este campo se utiliza temporalmente para guardar el Id del pedido y pasarlo a la capa de datos, luego se re-setea
            .OrdenCompra = Pedido.Numero  'éste número se cambió para que se genere en la capa de datos
            .IdBodega = Pedido.IdBodega
            .TipoVenta = Pedido.TipoVenta
            .TipoImpuesto = Pedido.TipoImpuesto
            .MotivoAnulacion = ""
            .AnuladoPor = ""
            .TotalNeto = 0.0
            .PorcDescto = 0.0
            .TotalNoSujeto = 0.0
            .TotalAfecto = 0.0
            .TotalExento = 0.0
            .TotalImpuesto1 = 0.0
            .TotalImpuesto2 = 0.0
            .TotalComprobante = 0.0
            .SaldoActual = 0.0
            .Comentario = Pedido.Notas
            .CreadoPor = Pedido.CreadoPor
            .FechaHoraCreacion = Now
            .ModificadoPor = ""
            .FechaHoraModificacion = Nothing

            If TipoAplicacion <> 1 Then  'cuando es nota de crédito, fac. de devolucion y nota de envio de devolucion
                .EsDevolucion = True
            End If
        End With
    End Sub
    Public Function fac_InsertarCliente(ByRef Header As fac_Clientes, ByRef Detalle As List(Of fac_ClientesPrecios), ByRef DetalleInfoAdicional As fac_ClientesAnexo) As String
        Return dl.fac_InsertarCliente(Header, Detalle, DetalleInfoAdicional)
    End Function
    Public Function fac_ActualizarCliente(ByRef Header As fac_Clientes, ByRef Detalle As List(Of fac_ClientesPrecios), ByRef DetalleInfoAdicional As fac_ClientesAnexo) As String
        Return dl.fac_ActualizarCliente(Header, Detalle, DetalleInfoAdicional)
    End Function
    Public Function fac_ObtenerDataVentas(ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Return dl.fac_ObtenerDataVentas(Desde, Hasta)
    End Function

    Public Function fac_ObtenerDatosEncabezadoPieTicket(ByVal IdPunto As Integer) As DataTable
        Return dl.fac_ObtenerDatosEncabezadoPieTicket(IdPunto)
    End Function

    Public Function fac_ObtenerTicketsParaCinta(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_ObtenerTicketsParaCinta(Desde, Hasta, IdPunto)
    End Function


    'Estas son las funciones para invocar los reportes
#Region "Reportes de Facturacion"
    Public Function fac_VentasPeriodo(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal Idcentro As String) As DataTable
        Return dl.fac_VentasPeriodo(Desde, Hasta, IdSucursal, IdPunto, Idcentro)
    End Function
    Public Function fac_VentasProductoCliente(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_VentasProductoCliente(Desde, Hasta, IdProducto, IdSucursal, IdPunto)
    End Function
    Public Function fac_VentasProductoConsolidado(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_VentasProductoConsolidado(Desde, Hasta, IdProducto, IdSucursal, IdPunto)
    End Function
    Public Function fac_VentasProductosAnual(ByVal Anio As Integer, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdGrupo As Integer, ByVal TipoReporte As Integer, ByVal IdBodega As Integer) As DataTable
        Return dl.fac_VentasProductosAnual(Anio, IdProducto, IdPunto, IdGrupo, TipoReporte, IdBodega)
    End Function
    Public Function fac_VentasProductosAnual2(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdGrupo As Integer) As DataTable
        Return dl.fac_VentasProductosAnual2(Desde, Hasta, IdProducto, idsucursal, IdPunto, IdGrupo)
    End Function
    Public Function fac_VentasProductosComparacionAnual(ByVal Anio1 As Integer, ByVal Anio2 As Integer, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_VentasProductosComparacionAnual(Anio1, Anio2, IdProducto, IdSucursal, IdPunto)
    End Function
    Public Function fac_VentasProductosComparacionMensual(ByVal Anio1 As Integer, ByVal Mes1 As Integer, ByVal Anio2 As Integer, ByVal Mes2 As Integer, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_VentasProductosComparacionMensual(Anio1, Mes1, Anio2, Mes2, IdProducto, IdSucursal, IdPunto)
    End Function
    Public Function fac_VentasTrimestrales(ByVal Mes1 As Integer, ByVal Anio1 As Integer, ByVal Mes2 As Integer, ByVal Anio2 As Integer _
       , ByVal Mes3 As Integer, ByVal Anio3 As Integer, ByVal Mes4 As Integer, ByVal Anio4 As Integer _
       , ByVal Mes5 As Integer, ByVal Anio5 As Integer, ByVal Mes6 As Integer, ByVal Anio6 As Integer) As DataTable
        Return dl.fac_VentasTrimestrales(Mes1, Anio1, Mes2, Anio2, Mes3, Anio3, Mes4, Anio4, Mes5, Anio5, Mes6, Anio6)
    End Function
    Public Function fac_VentasClienteDocumento(ByVal Desde As Date, ByVal Hasta As Date, ByVal ClienteDesde As String, ByVal ClienteHasta As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_VentasClienteDocumento(Desde, Hasta, ClienteDesde, ClienteHasta, IdSucursal, IdPunto)
    End Function
    Public Function fac_rptCotizaciones(ByVal Desde As Date, ByVal Hasta As Date)
        Return dl.fac_rptCotizaciones(Desde, Hasta)
    End Function
    Public Function fac_spRptRetenciones(ByVal Desde As Date, ByVal Hasta As Date)
        Return dl.fac_spRptRetenciones(Desde, Hasta)
    End Function
    Public Function fac_VerificaCorteCajaActivo(ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As Integer
        Return dl.fac_VerificaCorteCajaActivo(Fecha, IdSucursal, IdPunto)
    End Function
    Public Function fac_VerificaVentaAprobada(ByVal IdComprobante As Integer) As Integer
        Return dl.fac_VerificaVentaAprobada(IdComprobante)
    End Function
    Public Function GetArqueoCaja(ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal Fecha As Date) As DataSet
        Return dl.GetArqueoCaja(IdSucursal, IdPunto, Fecha)
    End Function

    Public Function fac_CorteCaja(ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdVendedor As Integer, ByVal TotalArqueo As Decimal, ByVal TotalCheques As Decimal, ByVal TotalRemesas As Decimal, ByVal TotalVaucher As Decimal) As DataSet
        Return dl.fac_CorteCaja(Fecha, IdSucursal, IdPunto, IdVendedor, TotalArqueo, TotalCheques, TotalRemesas, TotalVaucher)
    End Function
    Public Function fac_CierraCorteCaja(ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal Saldo As Decimal, ByVal CreadoPor As String) As Integer
        Return dl.fac_CierraCorteCaja(Fecha, IdSucursal, IdPunto, Saldo, CreadoPor)
    End Function
    Public Function fac_VentasClienteProducto(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdCliente As String, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_VentasClienteProducto(Desde, Hasta, IdCliente, IdProducto, IdSucursal, IdPunto)
    End Function
    Public Function fac_ProductoDespachado(ByVal IdRuta As Integer, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_ProductoDespachado(IdRuta, Desde, Hasta, IdSucursal, IdPunto)
    End Function
    Public Function fac_VentasComprobante(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipoComprobante As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_VentasComprobante(Desde, Hasta, IdTipoComprobante, IdSucursal, IdPunto)
    End Function
    Public Function fac_RepVales(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_RepVales(Desde, Hasta, IdSucursal, IdPunto)
    End Function
    Public Function fac_VentasFormaPago(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdFormaPago As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdVendedor As Integer) As DataTable
        Return dl.fac_VentasFormaPago(Desde, Hasta, IdFormaPago, IdSucursal, IdPunto, IdVendedor)
    End Function
    Public Function fac_VentasFormaPagoZonas(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdFormaPago As Integer, ByVal IdSucursal As Integer _
                                             , ByVal IdPunto As Integer, ByVal IdVendedor As Integer, ByVal IdDepartamento As String, ByVal IdMunicipio As String) As DataTable
        Return dl.fac_VentasFormaPagoZonas(Desde, Hasta, IdFormaPago, IdSucursal, IdPunto, IdVendedor, IdDepartamento, IdMunicipio)
    End Function
    Public Function fac_VentasDevoluciones(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_VentasDevoluciones(Desde, Hasta, IdPunto)
    End Function
    Public Function fac_VentasVendedor(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdVendedor As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal Detallado As Boolean) As DataTable
        Return dl.fac_VentasVendedor(Desde, Hasta, IdVendedor, IdSucursal, IdPunto, Detallado)
    End Function
    Public Function fac_VentasProductoConsolidado(ByVal fecDesde As Date, ByVal fecHasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal snTop As Boolean) As DataTable
        Return dl.fac_VentasProductoConsolidado(fecDesde, fecHasta, IdSucursal, IdPunto, snTop)
    End Function
    Public Function fac_VentasClienteConsolidado(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal snTop As Boolean) As DataTable
        Return dl.fac_VentasClienteConsolidado(Desde, Hasta, IdSucursal, IdPunto, snTop)
    End Function
    Public Function fac_PedidosPeriodo(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_PedidosPeriodo(Desde, Hasta, IdSucursal, IdPunto)
    End Function
    Public Function getLibroContribuyentes(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal IdSucursal As Integer) As DataSet
        Return dl.getLibroContribuyentes(Mes, Ejercicio, IdSucursal)
    End Function
    Public Function getLiquidacionImpuestos(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal IdSucursal As Integer) As DataSet
        Return dl.getLiquidacionImpuestos(Mes, Ejercicio, IdSucursal)
    End Function

    Public Function getLibroConsumidor(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.getLibroConsumidor(Mes, Ejercicio, IdSucursal)
    End Function
    Public Function getLibroConsumidorDetalle(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.getLibroConsumidorDetalle(Mes, Ejercicio, IdSucursal)
    End Function
    Public Function fac_DocumentosAnulados(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipo As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_DocumentosAnulados(Desde, Hasta, IdTipo, IdSucursal, IdPunto)
    End Function
    Public Function fac_CorteX(ByVal Fecha As DateTime, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_CorteX(Fecha, IdSucursal, IdPunto)
    End Function
    Public Function fac_CorteZ(ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_CorteZ(Fecha, IdSucursal, IdPunto)
    End Function
    Public Function fac_CorteGranTotalZ(ByVal Mes As Integer, ByVal Anio As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return dl.fac_CorteGranTotalZ(Mes, Anio, IdSucursal, IdPunto)
    End Function
    Public Function fac_VentasVendedorUtilidad(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdVendedor As Integer, ByVal Tipo As Integer) As DataTable
        Return dl.fac_ventasVendedorUtilidad(Desde, Hasta, IdBodega, IdSucursal, IdPunto, IdVendedor, Tipo)
    End Function
    Public Function fac_VentasClientesUtilidad(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdCliente As String) As DataTable
        Return dl.fac_VentasClientesUtilidad(Desde, Hasta, IdBodega, IdSucursal, IdPunto, IdCliente)
    End Function

#End Region

    'FUNCIONES PARA COPA AIRLINES
    Public Function fac_InsertaFacturaBoletos(ByRef FacturaHeader As fac_Ventas, ByRef FacturaDetalle As List(Of fac_VentasDetalle), ByVal AsignarNumero As Boolean) As String
        Return dl.fac_InsertaFacturaBoletos(FacturaHeader, FacturaDetalle, AsignarNumero) ', FacturaDetallePago, "VENTA DIRECTA", AsignarNumero)
    End Function

    Public Function fac_ConsultaFacturacionBoletos(ByVal Numero As String, ByRef Fecha As Date) As DataTable
        Return dl.fac_ConsultaFacturacionBoletos(Numero, Fecha)
    End Function

End Class