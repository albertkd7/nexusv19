﻿Imports NexusDLL
Imports NexusELL.TableEntities
Public Class CuentasPCBLL
    Dim dl As CuentasPCDLL
    Dim dlBank As BancosDLL
    Public Sub New(ByVal strConexion As String)
        dl = New CuentasPCDLL(strConexion)
        dlBank = New BancosDLL(strConexion)
    End Sub

    Public Function GetSaldosFacturas(ByVal IdCliente As String, ByVal Fecha As Date, ByVal FechaUltPago As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.GetSaldosFacturas(IdCliente, Fecha, FechaUltPago, IdSucursal)
    End Function
    Public Function ObtenerDocumentosCobroDetalle(ByVal IdComprobante As Integer) As DataTable
        Return dl.ObtenerDocumentosCobroDetalle(IdComprobante)
    End Function
    Public Function ConsultaCargos(ByVal sIdCliente As String) As DataTable
        Return dl.ConsultaCargos(sIdCliente)
    End Function
    Public Function getAbonoDetalle(ByVal IdComprobante As Integer) As DataTable
        Return dl.getAbonoDetalle(IdComprobante)
    End Function
    Public Function ContabilizarComprobantesRetencion(ByVal Desde As DateTime, ByVal Hasta As DateTime, ByVal TipoPartida As String, ByVal CreadoPor As String, ByVal Concepto As String, ByVal Idsucursal As Integer) As String
        Return dl.ContabilizarComprobantesRetencion(Desde, Hasta, TipoPartida, CreadoPor, Concepto, Idsucursal)
    End Function
    Public Function ObtenerAbono(ByVal IdComprobante As Integer, ByVal Fecha As DateTime) As DataTable
        Return dl.ObtenerAbono(IdComprobante, Fecha)
    End Function
    Public Function InsertarDocumentosCobro(ByRef PedidoHeader As cpc_DocumentosCobro, ByRef PedidoDetalle As List(Of cpc_DocumentosCobroDetalle)) As String
        Return dl.InsertarDocumentosCobro(PedidoHeader, PedidoDetalle)
    End Function
    Public Function ActualizaDocumentosCobro(ByRef PedidoHeader As cpc_DocumentosCobro, ByRef PedidoDetalle As List(Of cpc_DocumentosCobroDetalle)) As String
        Return dl.ActualizaDocumentosCobro(PedidoHeader, PedidoDetalle)
    End Function
    Public Function ContabilizarAbonos(ByVal Desde As DateTime, ByVal Hasta As DateTime, ByVal TipoPartida As String, ByVal CreadoPor As String, ByVal Concepto As String, ByVal IdSucursal As Integer) As String
        Return dl.ContabilizarAbonos(Desde, Hasta, TipoPartida, CreadoPor, Concepto, IdSucursal)
    End Function
    Public Function InsertaAbono(ByRef AbonoHeader As cpc_Abonos _
         , ByRef AbonoDetalle As List(Of cpc_AbonosDetalle) _
         , ByVal InsertarPartida As Boolean _
        , ByVal IdTipo As String _
        , ByVal IdCuentaMov As String _
        , ByVal IdModuloOrigen As Integer _
        , ByVal IdCtaBanco As Integer, ByVal IdCtaCliente As String, ByVal AfectarBanco As Boolean _
        , ByVal IdTipoTransaccion As Integer, ByVal sConcepto As String) As String

        Dim IdCuentaContableBanco As String = dlBank.GetCuentaCbleBanco(IdCtaBanco)
        Dim entTrans As New ban_Transacciones
        Dim entTransDetalle As ban_TransaccionesDetalle
        Dim DetalleTrans As New List(Of ban_TransaccionesDetalle)
        Dim TotalAbono As Decimal = 0.0
        ' se determina si la aplicacion pasa de una vez a bancos
        If AfectarBanco Then
            With entTrans
                .Concepto = AbonoHeader.Concepto & " " & sConcepto
                .Contabilizar = True
                .IdSucursal = AbonoHeader.IdSucursal
                .CreadoPor = AbonoHeader.CreadoPor
                .Fecha = AbonoHeader.Fecha
                .FechaContable = AbonoHeader.Fecha
                .FechaHoraCreacion = AbonoHeader.FechaHoraCreacion
                .FechaHoraModificacion = AbonoHeader.FechaHoraModificacion
                .IdCuentaBancaria = IdCtaBanco
                .IdPartida = 0
                .IdTipo = 2
                .IdTipoPartida = IdTipo
                .IdTransaccion = 0
                .IncluirConciliacion = 1
                .ModificadoPor = AbonoHeader.ModificadoPor
                .NumeroPartida = ""
                .ProcesadaBanco = 1
                .Valor = TotalAbono
            End With


            'aplico la cuenta por cobrar del cliente a modo de disminuir su saldo contable
            Dim i As Integer = 1
            For Each detalleAbono As cpc_AbonosDetalle In AbonoDetalle
                entTransDetalle = New ban_TransaccionesDetalle
                With entTransDetalle
                    .IdTransaccion = 0
                    .IdDetalle = i
                    .IdCuenta = IdCtaCliente
                    .Referencia = AbonoHeader.NumeroComprobante
                    .Concepto = detalleAbono.Concepto
                    .Debe = 0
                    .Haber = detalleAbono.MontoAbonado
                    .CreadoPor = detalleAbono.CreadoPor
                    .FechaHoraCreacion = Now
                End With
                TotalAbono += detalleAbono.MontoAbonado
                DetalleTrans.Add(entTransDetalle)
                i += 1
            Next
            'se Agrega el debe o deposito a la transaccion en bancos
            entTransDetalle = New ban_TransaccionesDetalle
            With entTransDetalle
                .IdTransaccion = 0
                .IdDetalle = i + 1
                .IdCuenta = IdCuentaContableBanco
                .Referencia = AbonoHeader.NumeroComprobante
                .Concepto = AbonoHeader.Concepto & " " & sConcepto
                .Debe = TotalAbono
                .Haber = 0.0
                .CreadoPor = AbonoHeader.CreadoPor
                .FechaHoraCreacion = Now
            End With
            DetalleTrans.Add(entTransDetalle)

            entTrans.Valor = TotalAbono
        End If

        Dim entPartidaDetalle As con_PartidasDetalle
        Dim entPartida As New con_Partidas
        Dim DetallePartidas As New List(Of con_PartidasDetalle)

        If InsertarPartida Then
            With entPartida
                .IdPartida = 0
                .IdTipo = IdTipo
                .IdSucursal = AbonoHeader.IdSucursal
                .Numero = ""
                .Fecha = AbonoHeader.Fecha
                .Concepto = AbonoHeader.Concepto & " " & sConcepto
                .Actualizada = 1
                If AfectarBanco Then
                    .IdModuloOrigen = IdModuloOrigen
                Else
                    .IdModuloOrigen = 0
                End If

                .CreadoPor = AbonoHeader.CreadoPor
                .FechaHoraCreacion = AbonoHeader.FechaHoraCreacion
                .ModificadoPor = AbonoHeader.ModificadoPor
                .FechaHoraModificacion = AbonoHeader.FechaHoraModificacion
            End With


            Dim i As Integer = 1
            TotalAbono = 0.0
            'se agregan todos los haberes aplicados al cliente
            For Each detalleAbono As cpc_AbonosDetalle In AbonoDetalle
                entPartidaDetalle = New con_PartidasDetalle
                With entPartidaDetalle
                    .IdPartida = 0
                    .IdDetalle = i
                    .IdCuenta = IdCtaCliente
                    .Referencia = AbonoHeader.NumeroComprobante
                    .Concepto = detalleAbono.Concepto
                    .Debe = 0
                    .Haber = detalleAbono.MontoAbonado
                    .CreadoPor = detalleAbono.CreadoPor
                    .FechaHoraCreacion = Now
                End With
                TotalAbono += detalleAbono.MontoAbonado
                DetallePartidas.Add(entPartidaDetalle)
                i += 1
            Next

            'Agrega el debe de la partida
            entPartidaDetalle = New con_PartidasDetalle
            With entPartidaDetalle
                .IdPartida = 0
                .IdDetalle = i + 1
                .IdCuenta = IIf(AfectarBanco, IdCuentaContableBanco, IdCuentaMov)
                .Referencia = AbonoHeader.NumeroComprobante
                .Concepto = AbonoHeader.Concepto & " " & sConcepto
                .Debe = TotalAbono
                .Haber = 0
                .CreadoPor = AbonoHeader.CreadoPor
                .FechaHoraCreacion = Now
            End With
            DetallePartidas.Add(entPartidaDetalle)
        End If

        Return dl.InsertaAbono(AbonoHeader, AbonoDetalle, entPartida, DetallePartidas, entTrans, DetalleTrans)
    End Function
    Public Function ConsultaAbonos(ByVal sIdCliente As String) As DataTable
        Return dl.ConsultaAbonos(sIdCliente)
    End Function
    Sub ActualizaFechaCliente(ByVal sIdCliente As String, ByVal dFecha As Date)
        dl.ActualizaFechaCliente(sIdCliente, dFecha)
    End Sub

    Public Function getAbonosCliente(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sIdCliente As String, ByVal IdRutas As Integer, ByVal IdSucursal As Integer, ByVal IdVendedor As Integer) As DataTable
        Return dl.getAbonosCliente(dFechaIni, dFechaFin, sIdCliente, IdRutas, IdSucursal, IdVendedor)
    End Function
    Public Function cpc_ConsultaAbonosClientes(ByVal IdCliente As String, ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Return dl.cpc_ConsultaAbonosClientes(IdCliente, Desde, Hasta)
    End Function
    Public Function getAbonosClienteConsolidado(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sIdCliente As String, ByVal IdRuta As Integer, ByVal IdSucursal As Integer, ByVal IdVendedor As Integer) As DataTable
        Return dl.getAbonosClienteConsolidado(dFechaIni, dFechaFin, sIdCliente, IdRuta, IdSucursal, IdVendedor)
    End Function

    Public Function getCargosCliente(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sIdCliente As String) As DataTable
        Return dl.getCargosCliente(dFechaIni, dFechaFin, sIdCliente)
    End Function
    Public Function getDocsCredito(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sIdCliente As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.getDocsCredito(dFechaIni, dFechaFin, sIdCliente, IdSucursal)
    End Function
    Public Function getDocsCancelados(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sIdCliente As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.getDocsCancelados(dFechaIni, dFechaFin, sIdCliente, IdSucursal)
    End Function
    Public Function cpc_EstadoCuenta(ByVal HastaFecha As Date, ByVal DesdeCliente As String, ByVal HastaCliente As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.cpc_EstadoCuenta(HastaFecha, DesdeCliente, HastaCliente, IdSucursal)
    End Function
    Public Function cpc_MovimientoHistoricoPorCliente(ByVal IdCliente As String, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.cpc_MovimientoHistoricoPorCliente(IdCliente, Desde, Hasta, IdSucursal)
    End Function
    Public Function getAnalisis(ByVal HastaFecha As Date, ByVal IdSucursal As Integer, ByVal IdVendedor As Integer) As DataTable
        Return dl.getAnalisis(HastaFecha, IdSucursal, IdVendedor)
    End Function
    Public Function getAnalisisDetalleExcel(ByVal HastaFecha As Date, ByVal IdSucursal As Integer, ByVal IdVendedor As Integer) As DataTable
        Return dl.getAnalisisDetalleExcel(HastaFecha, IdSucursal, IdVendedor)
    End Function
    Public Function getAnalisisExcel(ByVal HastaFecha As Date, ByVal IdSucursal As Integer, ByVal IdVendedor As Integer) As DataTable
        Return dl.getAnalisisExcel(HastaFecha, IdSucursal, IdVendedor)
    End Function
    Public Function getAnalisisDetalle(ByVal HastaFecha As Date, ByVal IdSucursal As Integer, ByVal IdVendedor As Integer) As DataTable
        Return dl.getAnalisisDetalle(HastaFecha, IdSucursal, IdVendedor)
    End Function
    Public Function getAuxiliar(ByVal Codigo As String, ByVal HastaFecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.getAuxiliar(Codigo, HastaFecha, IdSucursal)
    End Function

    Public Function SaldoCliente(ByVal IdCliente As String, ByVal dHastaFecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return dl.SaldoCliente(IdCliente, dHastaFecha, IdSucursal)
    End Function
    Public Function DocumentosVencidos(ByVal IdCliente As String, ByVal dHastaFecha As Date) As Integer
        Return dl.DocumentosVencidos(IdCliente, dHastaFecha)
    End Function
    Public Function cpc_ConsultaCobros(ByVal IdUsuario) As DataTable
        Return dl.cpc_ConsultaCobros(IdUsuario)
    End Function
    Function cpc_ObtenerUltAbono(ByVal IdCliente As String) As Integer
        Return dl.cpc_ObtenerUltAbono(IdCliente)
    End Function
    Function cpc_ActualizaFechaCancelacion(ByVal IdComprobante As Integer) As String
        Return dl.cpc_ActualizaFechaCancelacion(IdComprobante)
    End Function
    Function cpc_AnulaAbono(ByVal IdComprobante As Integer, ByVal AnuladoPor As String) As String
        Return dl.cpc_AnulaAbono(IdComprobante, AnuladoPor)
    End Function
    Public Function cpc_GetDetalleAbono(ByVal IdComprobante As Integer) As DataTable
        Return dl.cpc_GetDetalleAbono(IdComprobante)
    End Function
    Public Function cpc_DocumentosAnulados(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdCliente As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.cpc_DocumentosAnulados(Desde, Hasta, IdCliente, IdSucursal)
    End Function
    Public Function cpc_AplicarNotaCredito(ByRef NotasDetalle As List(Of fac_NotasCredito), ByVal Fecha As DateTime) As String
        Return dl.cpc_AplicarNotaCredito(NotasDetalle, Fecha)
    End Function

End Class
