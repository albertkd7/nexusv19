﻿Imports NexusDLL
Imports NexusELL.TableEntities
Public Class ComprasBLL
    Dim dl As ComprasDLL
    Public Sub New(ByVal dateBaseName As String)
        dl = New ComprasDLL(dateBaseName)
    End Sub
    Public Function com_ObtenerListaProveedores() As DataTable
        Return dl.com_ObtenerListaProveedores()
    End Function
    Public Function com_ConsultaCompras(ByVal Usuario As String) As DataSet
        Return dl.com_ConsultaCompras(Usuario)
    End Function
    Public Function com_ConsultaImportaciones(ByVal Usuario As String) As DataSet
        Return dl.com_ConsultaImportaciones(Usuario)
    End Function
    Public Function com_ObtenerPeriodoContabilizado(ByVal IdModulo As Integer) As DataTable
        Return dl.com_ObtenerPeriodoContabilizado(IdModulo)
    End Function
    Public Function com_ValidaContabilizacion(ByVal IdModulo As Integer, ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Return dl.com_ValidaContabilizacion(IdModulo, Desde, Hasta)
    End Function
    Public Function com_ObtieneCreaLiquidacionEstructura() As DataTable
        Return dl.com_ObtieneCreaLiquidacionEstructura()
    End Function
    Public Function com_LiquidaCajaChica(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdCaja As Integer, ByVal esFHC As Boolean) As DataTable
        Return dl.com_LiquidaCajaChica(Desde, Hasta, IdCaja, esFHC)
    End Function
    Public Function com_CompraConAbono(ByVal IdComprobante As Integer, ByVal Tipo As Integer) As Integer
        Return dl.com_CompraConAbono(IdComprobante, Tipo)
    End Function
    Public Function com_ConsultaDocsLiquidacion(ByVal Usuario As String) As DataTable
        Return dl.com_ConsultaDocsLiquidacion(Usuario)
    End Function
    Public Function com_ObtenerEstructuraIdCompra() As DataTable
        Return dl.com_ObtenerEstructuraIdCompra()
    End Function
    Public Function com_ValidaContabilizacion(ByVal IdSucursal As Integer, ByVal IdModulo As Integer, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdCaja As Integer) As DataTable
        Return dl.com_ValidaContabilizacion(IdSucursal, IdModulo, Desde, Hasta, IdCaja)
    End Function
    Public Function com_ObtenerDatosf930(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.com_ObtenerDatosF930(Desde, Hasta, IdSucursal)
    End Function
    Public Function com_EliminaPartidas(ByVal IdModulo As Integer, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipoPartida As String, ByVal IdSucursal As Integer, ByVal IdCaja As Integer, ByVal IdPartidas As String) As Integer
        Return dl.com_EliminaPartidas(IdModulo, Desde, Hasta, IdTipoPartida, IdSucursal, IdCaja, IdPartidas)
    End Function
    Public Function com_ConsultaOrdenesCompra(ByVal Usuario As String) As DataSet
        Return dl.com_ConsultaOrdenesCompra(Usuario)
    End Function
    Public Function com_ConsultaRequisiciones(ByVal Usuario As String) As DataSet
        Return dl.com_ConsultaRequisiciones(Usuario)
    End Function
    Public Function com_ConsultaRetenciones(ByVal Usuario As String) As DataSet
        Return dl.com_ConsultaRetenciones(Usuario)
    End Function
    Public Function com_InsertaRequisiciones(ByRef Header As com_Requisiciones, ByRef Detalle As List(Of com_RequisicionesDetalle)) As String
        Return dl.com_InsertaRequisiciones(Header, Detalle)
    End Function
    Public Function com_ContabilizarComprasCajaChica(ByVal Desde As Date, ByVal Hasta As Date, ByVal TipoPart As String, ByVal IdSucursal As Integer, ByVal CreadoPor As String, ByVal IdCaja As Integer) As String
        Return dl.com_ContabilizarComprasCajaChica(Desde, Hasta, TipoPart, IdSucursal, CreadoPor, IdCaja)
    End Function
    Public Function com_InsertaOrdenCompra(ByRef Header As com_OrdenesCompra, ByRef Detalle As List(Of com_OrdenesCompraDetalle), ByRef DetallePago As List(Of com_OrdenesCompraDetallePago)) As Boolean
        Return dl.com_InsertaOrdenCompra(Header, Detalle, DetallePago)
    End Function
    Public Function com_ActualizaOrdenCompra(ByRef Header As com_OrdenesCompra, ByRef Detalle As List(Of com_OrdenesCompraDetalle), ByRef DetallePago As List(Of com_OrdenesCompraDetallePago)) As Boolean
        Return dl.com_ActualizaOrdenCompra(Header, Detalle, DetallePago)
    End Function
    Public Function com_ConsultaOrdenCompra(ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Return dl.com_ConsultaOrdenCompra(Desde, Hasta)
    End Function
    Public Function com_AprobarOrdenCompra(ByVal IdDoc As Integer, ByVal AprobarReprobar As Integer, ByVal Comentario As String, ByVal AprobadoPor As String) As String
        Return dl.com_AprobarOrdenCompra(IdDoc, AprobarReprobar, Comentario, AprobadoPor)
    End Function
    Public Function ConteoOrdenesCompra(ByVal IdProveedor As String) As Integer
        Return dl.ConteoOrdenesCompra(IdProveedor)
    End Function
    Public Function GetRetencionesDetalle(ByVal IdComprobante As Integer) As DataTable
        Return dl.GetRetencionesDetalle(IdComprobante)
    End Function
    Public Function InsertRetencion(ByRef RetencionHeader As com_Retenciones, ByRef RetencionDetalle As List(Of com_RetencionesDetalle)) As String
        Return dl.InsertRetencion(RetencionHeader, RetencionDetalle)
    End Function
    Public Function com_ObtenerRetencion(ByVal IdComprobante As Integer) As DataTable
        Return dl.com_ObtenerRetencion(IdComprobante)
    End Function
    Public Function com_ObtenerIdRetencion(ByVal Id As Integer, ByVal TipoAvance As Integer) As Integer
        Return dl.com_ObtenerIdRetencion(Id, TipoAvance)
    End Function
    Public Function UpdateRetencion(ByRef RetencionHeader As com_Retenciones, ByRef RetencionDetalle As List(Of com_RetencionesDetalle)) As String
        Return dl.UpdateRetencion(RetencionHeader, RetencionDetalle)
    End Function
    Public Function com_ObtenerOrdenCompraDetalle(ByVal IdOrden As Integer, Optional NumOrden As String = "{N/A}") As DataTable
        Return dl.com_ObtenerOrdenCompraDetalle(IdOrden, NumOrden)
    End Function
    Public Function com_ObtenerOrdenCompraSugerida(ByVal IdProveedor As String) As DataTable
        Return dl.com_ObtenerOrdenCompraSugerida(IdProveedor)
    End Function
    Public Function com_ObtenerOrdenCompraFormaPago(ByVal IdOrden As Integer) As DataTable
        Return dl.com_ObtenerOrdenCompraFormaPago(IdOrden)
    End Function
    Public Function InsertaLiquidacion(ByRef LiquidacionHeader As com_Liquidacion) As String
        Return dl.com_InsertaLiquidacion(LiquidacionHeader)
    End Function
    Public Function com_ActualizaLiquidacion(ByRef LiquidacionHeader As com_Liquidacion) As String
        Return dl.com_ActualizaLiquidacion(LiquidacionHeader)
    End Function
    Public Function com_ObtenerIdOrdenCompra(ByVal Numero As String) As Integer
        Return dl.com_ObtenerIdOrdenCompra(Numero)
    End Function
    Public Function com_ObtenerIdOrdenCompra(ByVal IdOrden As Integer, ByVal TipoAvance As Integer) As Integer
        Return dl.com_ObtenerIdOrdenCompra(IdOrden, TipoAvance)
    End Function
    Public Function com_GuardarImportacion(ByRef ImportacionHeader As com_Importaciones _
   , ByRef ImportacionDetalle As List(Of com_ImportacionesDetalle) _
    , ByRef ImportacionesGastosDet As List(Of com_ImportacionesGastosDetalle) _
    , ByRef ImportacionesProveedores As List(Of com_ImportacionesProveedores), ByVal ImportacionesGastosProd As List(Of com_ImportacionesGastosProductos)) As String

        Return dl.com_GuardarImportacion(ImportacionHeader, ImportacionDetalle, ImportacionesGastosDet _
        , ImportacionesProveedores, ImportacionesGastosProd)
    End Function
    Public Function com_ObtenerIdRequisicion(ByVal Id As Integer, ByVal TipoAvance As Integer, ByVal Tabla As String) As Integer
        Return dl.com_ObtenerIdRequisicion(Id, TipoAvance, Tabla)
    End Function

    Public Function com_ObtenerImportacionDetalle(ByVal IdComprobante As Integer) As DataTable
        Return dl.com_ObtenerImportacionDetalle(IdComprobante)
    End Function
    Public Function com_HabilitarCheckGastoImportacion() As Boolean
        Return dl.com_HabilitarCheckGastoImportacion()
    End Function
    Public Function com_ObtenerProrrateoCalculado(ByVal IdComprobante As Integer) As DataSet
        Return dl.com_ObtenerProrrateoCalculado(IdComprobante)
    End Function
    Public Function com_ObtenerListadoImportaciones(ByVal IdSucursal As Integer, desde As Date, hasta As Date) As DataTable
        Return dl.com_ObtenerListadoImportaciones(IdSucursal, desde, hasta)
    End Function
    'Public Function com_ActualizaCompra(ByRef CompraHeader As com_Compras, ByRef CompraDetalle As List(Of com_ComprasDetalle), ByRef ImpuestosCompras As List(Of com_ComprasDetalleImpuestos)) As String
    '    Return dl.com_ActualizaCompra(CompraHeader, CompraDetalle, ImpuestosCompras)
    'End Function
    Public Function com_ObtenerDetalleRequisicion(ByVal IdComprobante As Integer, ByVal Tabla As String) As DataTable
        Return dl.com_ObtenerDetalleRequisicion(IdComprobante, Tabla)
    End Function
    Public Function com_ActualizaRequisiciones(ByRef Header As com_Requisiciones, ByRef Detalle As List(Of com_RequisicionesDetalle)) As String
        Return dl.com_ActualizaRequisiciones(Header, Detalle)
    End Function
    Public Function com_GuardaCompra(ByRef CompraHeader As com_Compras, ByRef CompraDetalle As List(Of com_ComprasDetalle), ByRef DetalleImpuestos As List(Of com_ComprasDetalleImpuestos), EsNuevo As Boolean) As String
        Return dl.com_GuardaCompra(CompraHeader, CompraDetalle, DetalleImpuestos, EsNuevo)
    End Function
    Public Function com_ObtenerCompraDetalle(ByVal IdCompra As Integer) As DataTable
        Return dl.com_ObtenerCompraDetalle(IdCompra)
    End Function
    Public Function com_ObtenerDocumentoCompra(ByVal IdCompra As Integer) As DataTable
        Return dl.com_ObtenerDocumentoCompra(IdCompra)
    End Function
   
    Public Function com_ContabilizarLiquidacion(ByVal Desde As Date, ByVal Hasta As Date, ByVal Tipo As Integer, ByVal IdSucursal As Integer, ByVal CreadoPor As String) As String
        Return dl.com_ContabilizarLiquidacion(Desde, Hasta, Tipo, IdSucursal, CreadoPor)
    End Function
    Public Function com_ObtenerIdCompra(ByVal IdCompra As Integer, ByVal TipoAvance As Integer) As Integer
        Return dl.com_ObtenerIdCompra(IdCompra, TipoAvance)
    End Function
    Public Function com_ObtenerRequisicionesNoAutorizadas(ByVal HastaFecha As DateTime) As DataTable
        Return dl.com_ObtenerRequisicionesNoAutorizadas(HastaFecha)
    End Function
    Public Function com_ObtenerIdImportacion(ByVal IdComprobante As Integer, ByVal TipoAvance As Integer) As Integer
        Return dl.com_ObtenerIdImportacion(IdComprobante, TipoAvance)
    End Function
    Public Function com_ObtenerDataCompras(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return dl.com_ObtenerDataCompras(Desde, Hasta, IdSucursal)
    End Function
    Public Function com_MarcarRequisiciones(ByRef Detalle As List(Of com_RequisicionesDetalle)) As String
        Return dl.com_MarcarRequisiciones(Detalle)
    End Function
    Public Function com_ObtenerOrdenCompra(ByVal IdComprobante As Integer, ByVal Tipo As Integer) As DataTable
        Return dl.com_ObtenerOrdenCompra(IdComprobante, Tipo)
    End Function
    Public Function com_ObtenerGastosImportacion(ByVal IdImportacion As Integer) As DataTable
        Return dl.com_ObtenerGastosImportacion(IdImportacion)
    End Function
    Public Function com_ObtenerImpuestos(ByVal IdCompra As Integer, ByVal TotalAfecto As Decimal) As DataTable
        Return dl.com_ObtenerImpuestos(IdCompra, TotalAfecto)
    End Function
    Public Function com_ObtenerGastosImportacionProd(ByVal IdImportacion As Integer, ByVal Referencia As String, ByVal IdProducto As String) As DataTable
        Return dl.com_ObtenerGastosImportacionProd(IdImportacion, Referencia, IdProducto)
    End Function
    Public Function com_ObtenerGastosImportacionProdEstructura() As DataTable
        Return dl.com_ObtenerGastosImportacionProdEstructura()
    End Function
    Public Function com_ObtenerProveedoresImportacion(ByVal IdImportacion As Integer) As DataTable
        Return dl.com_ObtenerProveedoresImportacion(IdImportacion)
    End Function


    Public Function com_ContabilizarCompras(ByVal Desde As Date, ByVal Hasta As Date, ByVal TipoPart As String, ByVal IdSucursal As Integer, ByVal CreadoPor As String, ByVal IdComprobante As Integer, ByVal TipoContabilizacion As Integer) As String
        Return dl.com_ContabilizarCompras(Desde, Hasta, TipoPart, IdSucursal, CreadoPor, IdComprobante, TipoContabilizacion)
    End Function

    Public Function com_EliminaPartidas(ByVal IdModulo As Integer, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipoPartida As String, ByVal IdSucursal As Integer) As Integer
        Return dl.com_EliminaPartidas(IdModulo, Desde, Hasta, IdTipoPartida, IdSucursal)
    End Function

#Region "Reportes"
    Public Function com_ComprasRetencion(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdProveedor As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.com_ComprasRetencion(dFechaI, dFechaF, IdProveedor, IdSucursal)
    End Function


    Public Function GetLibroCompras(ByVal iMes As Integer, ByVal iAnio As Integer, ByVal IdBodega As Integer) As DataTable
        Return dl.GetLibroCompras(iMes, iAnio, IdBodega)
    End Function
    Public Function com_ComprasPeriodoProveedor(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdProveedor As String, ByVal IdSucursal As Integer, ByVal IdMarca As Integer, ByVal IdCentro As String) As DataTable
        Return dl.com_ComprasPeriodoProveedor(dFechaI, dFechaF, IdBodega, IdProveedor, IdSucursal, IdMarca, IdCentro)
    End Function
    Public Function com_ComprasExcluidos(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdProveedor As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.com_ComprasExcluidos(dFechaI, dFechaF, IdBodega, IdProveedor, IdSucursal)
    End Function
    Public Function com_ComprasCajaChica(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdCaja As Integer, ByVal IdSucursal As Integer, ByVal TipoConsulta As Integer) As DataTable
        Return dl.com_ComprasCajaChica(dFechaI, dFechaF, IdCaja, IdSucursal, TipoConsulta)
    End Function
    Public Function com_ComprasPeriodoProducto(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdCentro As String) As DataTable
        Return dl.com_ComprasPeriodoProducto(dFechaI, dFechaF, IdBodega, IdProducto, IdSucursal, IdCentro)
    End Function
    Public Function com_ComprasProveedorProducto(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdProveedor As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.com_ComprasProveedorProducto(dFechaI, dFechaF, IdBodega, IdProveedor, IdSucursal)
    End Function
    Public Function com_ComprasProductoProveedor(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdProducto As String, ByVal IdSucursal As Integer) As DataTable
        Return dl.com_ComprasProductoProveedor(dFechaI, dFechaF, IdBodega, IdProducto, IdSucursal)
    End Function
    Public Function GetComprasTotalesProveedor(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdSucursal As Integer, ByVal IdMarca As Integer) As DataTable
        Return dl.GetComprasTotalesProveedor(dFechaI, dFechaF, IdBodega, IdSucursal, IdMarca)
    End Function
    Public Function GetComprasTotalesProducto(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdSucursal As Integer) As DataTable
        Return dl.GetComprasTotalesProducto(dFechaI, dFechaF, IdBodega, IdSucursal)
    End Function
#End Region
End Class
