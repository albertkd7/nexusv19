﻿Imports NexusELL.TableEntities
Imports NexusDLL
Public Class AdmonBLL
    Dim dl As NexusDLL.AdmonDLL

    Public Sub New(ByVal dateBaseName As String)
        dl = New AdmonDLL(dateBaseName)
    End Sub
    Public Function ValidarUsuario(ByVal IdUsuario As String, ByVal Password As String) As String
        Return dl.ValidarUsuario(IdUsuario, Password)
    End Function
    Public Function ExtraeUsuario(ByVal IdUsuario As String) As DataTable
        Return dl.ExtraeUsuario(IdUsuario)
    End Function

    Public Function GestionesProgramadas(ByVal FechaDesde As Date, ByVal FechaHasta As Date, ByVal IdVendedor As Integer) As DataTable
        Return dl.GestionesProgramadas(FechaDesde, FechaHasta, IdVendedor)
    End Function
    Public Sub IngresoFallido(ByVal IdUsuario As String, ByVal Pc As String, ByVal NumAcceso As Integer)
        dl.IngresoFallido(IdUsuario, Pc, NumAcceso)
    End Sub
    Public Sub HistorialClaves(ByVal IdUsuario As String, ByVal Password As String)
        dl.HistorialClaves(IdUsuario, Password)
    End Sub
    Public Function adm_rptBitacora(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdModulo As Integer, ByVal IdUsuario As String) As DataTable
        Return dl.adm_rptBitacora(Desde, Hasta, IdModulo, IdUsuario)
    End Function
    Public Function GetPermisosUsuario(ByVal sIdUsuario As String, ByVal sModulo As String, ByVal sOptionId As String) As DataTable
        Return dl.GetPermisosUsuario(sIdUsuario, sModulo, sOptionId)
    End Function
    Public Function adm_CierreSucursal(ByVal IdSucursal As Integer, ByVal Fecha As Date _
                                       , ByRef entArqueo As List(Of fac_CorteCajaArqueo) _
                                       , ByRef entArqueoCheques As List(Of fac_CorteCajaCheques) _
                                       , ByRef entArqueoRemesas As List(Of fac_CorteCajaRemesas) _
                                       , ByRef entArqueoPOS As List(Of fac_CorteCajaPOS) _
                                       , ByVal IdUsuario As String, ByVal IdPunto As Integer) As String
        Return dl.adm_CierreSucursal(IdSucursal, Fecha, entArqueo, entArqueoCheques, entArqueoRemesas, entArqueoPOS, IdUsuario, IdPunto)
    End Function
    Public Function adm_RevierteCierreSucursal(ByVal IdSucursal As Integer, ByVal Fecha As Date, ByVal IdUsuario As String, ByVal Idpunto As Integer) As String
        Return dl.adm_RevierteCierreSucursal(IdSucursal, Fecha, IdUsuario, Idpunto)
    End Function
    Public Function ObtenerOpcionesUsuario(ByVal Modulo As String, ByVal IdUsuario As String) As DataTable
        Return dl.ObtenerOpcionesUsuario(Modulo, IdUsuario)
    End Function
    Public Sub EliminaOpcionesUsuario(ByVal sIdUsuario As String, ByVal sModulo As String)
        dl.EliminaOpcionesUsuario(sIdUsuario, sModulo)
    End Sub
    Public Sub InsertaOpcionUsuario(ByVal sIdUsuario As String, ByVal sModulo As String, ByVal sOptionId As String, ByVal boolean1 As Boolean, ByVal boolean2 As Boolean, ByVal boolean3 As Boolean)
        dl.InsertaOpcionUsuario(sIdUsuario, sModulo, sOptionId, boolean1, boolean2, boolean3)
    End Sub
    Public Sub InsertarPrecioUsuario(ByVal IdUsuario As String, ByVal IdPrecio As Integer, ByVal CreadoPor As String)
        dl.InsertarPrecioUsuario(IdUsuario, IdPrecio, CreadoPor)
    End Sub
    Public Sub InsertarSucursalUsuario(ByVal IdUsuario As String, ByVal IdSucursal As Integer, ByVal CreadoPor As String)
        dl.InsertarSucursalUsuario(IdUsuario, IdSucursal, CreadoPor)
    End Sub
    Public Sub AsignaPermisosDeUsuarioAsuario(ByVal IdUsuario As String, ByVal IdUsuarioCopiar As String)
        dl.AsignaPermisosDeUsuarioAsuario(IdUsuario, IdUsuarioCopiar)
    End Sub
    Public Sub EliminarPreciosUsuario(ByVal IdUsuario As String)
        dl.EliminarPreciosUsuario(IdUsuario)
    End Sub
    Public Sub EliminarSucursalesUsuario(ByVal IdUsuario As String)
        dl.EliminarSucursalesUsuario(IdUsuario)
    End Sub
    Public Function ObtenerPreciosUsuario(ByVal IdUsuario As String) As DataTable
        Return dl.ObtenerPreciosUsuario(IdUsuario)
    End Function
    Public Function ObtenerSucursalesUsuario(ByVal IdUsuario As String) As DataTable
        Return dl.ObtenerSucursalesUsuario(IdUsuario)
    End Function
    Public Function ObtenerUltimasClaves(ByVal IdUsuario As String) As DataTable
        Return dl.ObtenerUltimasClaves(IdUsuario)
    End Function

    Function GetModulos() As DataTable
        Return dl.GetModulos()
    End Function
    Function GetUsuarios() As DataTable
        Return dl.GetUsuarios()
    End Function
    Function ObtieneParametros() As DataTable
        Return dl.ObtieneParametros
    End Function
    Function ObtieneCorrelativos(ByVal NombreTabla As String) As Integer
        Return dl.ObtieneCorrelativos(NombreTabla)
    End Function
    Public Function ObtieneFechaCierre() As DataTable
        Return dl.ObtieneFechaCierre()
    End Function
    Public Function ObtieneFacturasInterface(ByVal IdSucursal As Integer, ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Return dl.ObtieneFacturasInterface(IdSucursal, Desde, Hasta)
    End Function
    Sub adm_ParametrosUpdate(ByVal entidad As adm_Parametros, ByVal CorrelativoQuedan As Integer _
    , ByVal CorrelativoSujetoExcluido As Integer, ByVal CorrelEntradas As Integer, ByVal CorrelSalidas As Integer _
    , ByVal CorrelTraslados As Integer, ByVal CorrelNA As Integer, ByVal CorrelVale As Integer, ByVal CorrelOC As Integer)
        dl.adm_ParametrosUpdate(entidad, CorrelativoQuedan, CorrelativoSujetoExcluido, CorrelEntradas _
                                , CorrelSalidas, CorrelTraslados, CorrelNA, CorrelVale, CorrelOC)
    End Sub
    Function GetSchemasDB() As DataTable
        Return dl.GetSchemasDB()
    End Function
    Public Function EliminaEmpresa_Schema(ByVal NameSchema As String) As String
        Return dl.EliminaEmpresa_Schema(NameSchema)
    End Function
    Public Sub EliminaEmpresa_Lista(ByVal cns As String)
        dl.EliminaEmpresa_Lista(cns)
    End Sub
    Public Function GetNameSchemaByUser(ByVal UserDB As String)
        Return dl.GetNameSchemaByUser(UserDB)
    End Function
End Class
