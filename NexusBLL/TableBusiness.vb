﻿Imports NexusDLL
Imports NexusELL.TableEntities

Public Class TableBusiness
    Dim dl As TableData
    Public Sub New(ByVal StringConexion As String)
        dl = New TableData(StringConexion)
    End Sub


    Public Function AppointmentsSelectAll() As DataTable
        Return dl.AppointmentsSelectAll()
    End Function

    Public Function AppointmentsSelectByPK _
            (ByVal UniqueID As System.Int32 _
            ) As Appointments

        Return dl.AppointmentsSelectByPK( _
            UniqueID _
            )
    End Function
    Public Function fac_VentasRetencionSelectAll() As DataTable
        Return dl.fac_VentasRetencionSelectAll()
    End Function

    Public Function fac_VentasRetencionSelectByPK _
      (ByVal IdComprobante As System.Int32
      ) As fac_VentasRetencion

        Return dl.fac_VentasRetencionSelectByPK(
         IdComprobante
         )
    End Function

    Public Sub fac_VentasRetencionDeleteByPK _
      (ByVal IdComprobante As System.Int32
      )

        dl.fac_VentasRetencionDeleteByPK(
         IdComprobante
         )
    End Sub

    Public Sub fac_VentasRetencionInsert _
    (ByVal entidad As fac_VentasRetencion)

        dl.fac_VentasRetencionInsert(entidad)
    End Sub

    Public Sub fac_VentasRetencionUpdate _
    (ByVal entidad As fac_VentasRetencion)

        dl.fac_VentasRetencionUpdate(entidad)
    End Sub

    Public Sub AppointmentsDeleteByPK _
            (ByVal UniqueID As System.Int32 _
            )

        dl.AppointmentsDeleteByPK( _
            UniqueID _
            )
    End Sub

    Public Sub AppointmentsInsert _
    (ByVal entidad As Appointments)

        dl.AppointmentsInsert(entidad)
    End Sub

    Public Sub AppointmentsUpdate _
    (ByVal entidad As Appointments)

        dl.AppointmentsUpdate(entidad)
    End Sub

    Public Function con_PresupuestosSelectAll() As DataTable
        Return dl.con_PresupuestosSelectAll()
    End Function

    Public Function con_PresupuestosSelectByPK _
        (ByVal IdPresupuesto As System.Int32
        ) As con_Presupuestos

        Return dl.con_PresupuestosSelectByPK(
        IdPresupuesto
        )
    End Function

    Public Sub con_PresupuestosDeleteByPK _
        (ByVal IdPresupuesto As System.Int32
        )

        dl.con_PresupuestosDeleteByPK(
        IdPresupuesto
        )
    End Sub

    Public Sub con_PresupuestosInsert _
(ByVal entidad As con_Presupuestos)

        dl.con_PresupuestosInsert(entidad)
    End Sub

    Public Sub con_PresupuestosUpdate _
(ByVal entidad As con_Presupuestos)

        dl.con_PresupuestosUpdate(entidad)
    End Sub

    Public Function con_PresupuestosDetalleSelectAll() As DataTable
        Return dl.con_PresupuestosDetalleSelectAll()
    End Function

    Public Function con_PresupuestosDetalleSelectByPK _
        (ByVal IdPresupuesto As System.Int32 _
        , ByVal IdCuenta As System.String
        ) As con_PresupuestosDetalle

        Return dl.con_PresupuestosDetalleSelectByPK(
        IdPresupuesto _
        , IdCuenta
        )
    End Function

    Public Sub con_PresupuestosDetalleDeleteByPK _
        (ByVal IdPresupuesto As System.Int32 _
        , ByVal IdCuenta As System.String
        )

        dl.con_PresupuestosDetalleDeleteByPK(
        IdPresupuesto _
        , IdCuenta
        )
    End Sub

    Public Sub con_PresupuestosDetalleInsert _
(ByVal entidad As con_PresupuestosDetalle)

        dl.con_PresupuestosDetalleInsert(entidad)
    End Sub

    Public Sub con_PresupuestosDetalleUpdate _
(ByVal entidad As con_PresupuestosDetalle)

        dl.con_PresupuestosDetalleUpdate(entidad)
    End Sub


    Public Function fac_TokenSelectAll() As DataTable
        Return dl.fac_TokenSelectAll()
    End Function

    Public Function fac_TokenSelectByPK _
            (ByVal IdToken As System.Int32 _
            ) As fac_Token

        Return dl.fac_TokenSelectByPK( _
            IdToken _
            )
    End Function

    Public Sub fac_TokenDeleteByPK _
            (ByVal IdToken As System.Int32 _
            )

        dl.fac_TokenDeleteByPK( _
            IdToken _
            )
    End Sub

    Public Sub fac_TokenInsert _
    (ByVal entidad As fac_Token)

        dl.fac_TokenInsert(entidad)
    End Sub

    Public Sub fac_TokenUpdate _
    (ByVal entidad As fac_Token)

        dl.fac_TokenUpdate(entidad)
    End Sub


#Region "Admon"
    Public Function adm_UsuariosSelectAll() As DataTable
        Return dl.adm_UsuariosSelectAll()
    End Function

    Public Function adm_UsuariosSelectByPK(ByVal IdUsuario As String) As adm_Usuarios
        Return dl.adm_UsuariosSelectByPK(IdUsuario)
    End Function

    Public Sub adm_UsuariosDeleteByPK(ByVal IdUsuario As String)

        dl.adm_UsuariosDeleteByPK(IdUsuario)
    End Sub

    Public Sub adm_UsuariosInsert(ByVal entidad As adm_Usuarios)

        dl.adm_UsuariosInsert(entidad)
    End Sub

    Public Sub adm_UsuariosUpdate(ByVal entidad As adm_Usuarios)

        dl.adm_UsuariosUpdate(entidad)
    End Sub
    Public Function adm_LibrosSelectAll() As DataTable
        Return dl.adm_LibrosSelectAll()
    End Function

    Public Function adm_LibrosSelectByPK(ByVal IdLibro As System.Int32) As adm_Libros

        Return dl.adm_LibrosSelectByPK( _
         IdLibro _
         )
    End Function

    Public Sub adm_LibrosDeleteByPK _
      (ByVal IdLibro As System.Int32 _
      )

        dl.adm_LibrosDeleteByPK( _
         IdLibro _
         )
    End Sub

    Public Sub adm_LibrosInsert _
    (ByVal entidad As adm_Libros)

        dl.adm_LibrosInsert(entidad)
    End Sub

    Public Sub adm_LibrosUpdate _
    (ByVal entidad As adm_Libros)

        dl.adm_LibrosUpdate(entidad)
    End Sub

    Public Function adm_SucursalesSelectAll() As DataTable
        Return dl.adm_SucursalesSelectAll()
    End Function

    Public Function adm_SucursalesSelectByPK(ByVal IdSucursal As System.Int32) As adm_Sucursales

        Return dl.adm_SucursalesSelectByPK(IdSucursal)
    End Function

    Public Sub adm_SucursalesDeleteByPK(ByVal IdSucursal As System.Int32)

        dl.adm_SucursalesDeleteByPK(IdSucursal)
    End Sub

    Public Sub adm_SucursalesInsert(ByVal entidad As adm_Sucursales)
        dl.adm_SucursalesInsert(entidad)
    End Sub

    Public Sub adm_SucursalesUpdate(ByVal entidad As adm_Sucursales)
        dl.adm_SucursalesUpdate(entidad)
    End Sub
    Public Function adm_TiposComprobanteSelectAll() As DataTable
        Return dl.adm_TiposComprobanteSelectAll()
    End Function

    Public Function adm_TiposComprobanteSelectByPK(ByVal IdTipoComprobante As System.Int32) As adm_TiposComprobante

        Return dl.adm_TiposComprobanteSelectByPK(IdTipoComprobante)
    End Function

    Public Sub adm_TiposComprobanteDeleteByPK(ByVal IdTipoComprobante As Int32)

        dl.adm_TiposComprobanteDeleteByPK(IdTipoComprobante)
    End Sub

    Public Sub adm_TiposComprobanteInsert(ByVal entidad As adm_TiposComprobante)

        dl.adm_TiposComprobanteInsert(entidad)
    End Sub

    Public Sub adm_TiposComprobanteUpdate(ByVal entidad As adm_TiposComprobante)

        dl.adm_TiposComprobanteUpdate(entidad)
    End Sub

    Public Function adm_SeriesDocumentosSelectAll() As DataTable
        Return dl.adm_SeriesDocumentosSelectAll()
    End Function

    Public Function adm_SeriesDocumentosSelectByPK _
      (ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdTipoComprobante As Integer, ByVal Serie As String, ByVal Resolucion As String) As adm_SeriesDocumentos

        Return dl.adm_SeriesDocumentosSelectByPK(
         IdSucursal _
         , IdPunto _
         , IdTipoComprobante _
         , Serie _
         , Resolucion
         )
    End Function

    Public Sub adm_SeriesDocumentosDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal IdTipoComprobante As System.Int32 _
      , ByVal Serie As System.String _
      , ByVal Resolucion As System.String
      )

        dl.adm_SeriesDocumentosDeleteByPK(
         IdSucursal _
         , IdPunto _
         , IdTipoComprobante _
         , Serie _
         , Resolucion
         )
    End Sub

    Public Sub adm_SeriesDocumentosInsert _
    (ByVal entidad As adm_SeriesDocumentos)

        dl.adm_SeriesDocumentosInsert(entidad)
    End Sub

    Public Sub adm_SeriesDocumentosUpdate _
    (ByVal entidad As adm_SeriesDocumentos)

        dl.adm_SeriesDocumentosUpdate(entidad)
    End Sub

    Public Function adm_DepartamentosSelectAll() As DataTable
        Return dl.adm_DepartamentosSelectAll()
    End Function

    Public Function adm_DepartamentosSelectByPK _
      (ByVal IdDepartamento As System.String _
      ) As adm_Departamentos

        Return dl.adm_DepartamentosSelectByPK( _
         IdDepartamento _
         )
    End Function

    Public Sub adm_DepartamentosDeleteByPK _
      (ByVal IdDepartamento As System.String _
      )

        dl.adm_DepartamentosDeleteByPK( _
         IdDepartamento _
         )
    End Sub

    Public Sub adm_DepartamentosInsert _
    (ByVal entidad As adm_Departamentos)

        dl.adm_DepartamentosInsert(entidad)
    End Sub

    Public Sub adm_DepartamentosUpdate _
    (ByVal entidad As adm_Departamentos)

        dl.adm_DepartamentosUpdate(entidad)
    End Sub

    Public Function adm_MunicipiosSelectAll() As DataTable
        Return dl.adm_MunicipiosSelectAll()
    End Function

    Public Function adm_MunicipiosSelectByPK _
      (ByVal IdMunicipio As System.String _
      ) As adm_Municipios

        Return dl.adm_MunicipiosSelectByPK( _
         IdMunicipio _
         )
    End Function

    Public Sub adm_MunicipiosDeleteByPK _
      (ByVal IdMunicipio As System.String _
      )

        dl.adm_MunicipiosDeleteByPK( _
         IdMunicipio _
         )
    End Sub

    Public Sub adm_MunicipiosInsert _
    (ByVal entidad As adm_Municipios)

        dl.adm_MunicipiosInsert(entidad)
    End Sub

    Public Sub adm_MunicipiosUpdate _
    (ByVal entidad As adm_Municipios)

        dl.adm_MunicipiosUpdate(entidad)
    End Sub


#End Region

#Region "Bancos"
    Public Function ban_CuentasBancariasSelectAll() As DataTable
        Return dl.ban_CuentasBancariasSelectAll()
    End Function

    Public Function ban_CuentasBancariasSelectByPK _
      (ByVal IdCuentaBancaria As System.Int32 _
      ) As ban_CuentasBancarias

        Return dl.ban_CuentasBancariasSelectByPK( _
         IdCuentaBancaria _
         )
    End Function

    Public Sub ban_CuentasBancariasDeleteByPK _
      (ByVal IdCuentaBancaria As System.Int32 _
      )

        dl.ban_CuentasBancariasDeleteByPK( _
         IdCuentaBancaria _
         )
    End Sub

    Public Sub ban_CuentasBancariasInsert(ByVal entidad As ban_CuentasBancarias)

        dl.ban_CuentasBancariasInsert(entidad)
    End Sub

    Public Sub ban_CuentasBancariasUpdate(ByVal entidad As ban_CuentasBancarias)

        dl.ban_CuentasBancariasUpdate(entidad)
    End Sub


    Public Function ban_ChequesSelectAll() As DataTable
        Return dl.ban_ChequesSelectAll()
    End Function
    Public Function ban_ChequesSelectByPK(ByVal IdCheque As System.Int32) As ban_Cheques
        Return dl.ban_ChequesSelectByPK(IdCheque)
    End Function
    Public Sub ban_ChequesDeleteByPK _
      (ByVal IdCheque As System.Int32 _
      )

        dl.ban_ChequesDeleteByPK( _
         IdCheque _
         )
    End Sub
    Public Sub ban_ChequesInsert _
    (ByVal entidad As ban_Cheques)

        dl.ban_ChequesInsert(entidad)
    End Sub
    Public Sub ban_ChequesUpdate _
    (ByVal entidad As ban_Cheques)

        dl.ban_ChequesUpdate(entidad)
    End Sub


    Public Function ban_ChequesDetalleSelectAll() As DataTable
        Return dl.ban_ChequesDetalleSelectAll()
    End Function

    Public Function ban_ChequesDetalleSelectByPK _
      (ByVal IdCheque As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As ban_ChequesDetalle

        Return dl.ban_ChequesDetalleSelectByPK( _
         IdCheque _
         , IdDetalle _
         )
    End Function

    Public Sub ban_ChequesDetalleDeleteByPK _
      (ByVal IdCheque As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.ban_ChequesDetalleDeleteByPK( _
         IdCheque _
         , IdDetalle _
         )
    End Sub

    Public Sub ban_ChequesDetalleInsert _
    (ByVal entidad As ban_ChequesDetalle)

        dl.ban_ChequesDetalleInsert(entidad)
    End Sub

    Public Sub ban_ChequesDetalleUpdate _
    (ByVal entidad As ban_ChequesDetalle)

        dl.ban_ChequesDetalleUpdate(entidad)
    End Sub

    Public Function ban_TiposTransaccionSelectAll() As DataTable
        Return dl.ban_TiposTransaccionSelectAll()
    End Function

    Public Function ban_TiposTransaccionSelectByPK _
      (ByVal IdTipo As System.Byte _
      ) As ban_TiposTransaccion

        Return dl.ban_TiposTransaccionSelectByPK( _
         IdTipo _
         )
    End Function

    Public Sub ban_TiposTransaccionDeleteByPK _
      (ByVal IdTipo As System.Byte _
      )

        dl.ban_TiposTransaccionDeleteByPK( _
         IdTipo _
         )
    End Sub

    Public Sub ban_TiposTransaccionInsert _
    (ByVal entidad As ban_TiposTransaccion)

        dl.ban_TiposTransaccionInsert(entidad)
    End Sub

    Public Sub ban_TiposTransaccionUpdate _
    (ByVal entidad As ban_TiposTransaccion)

        dl.ban_TiposTransaccionUpdate(entidad)
    End Sub

    Public Function ban_TransaccionesSelectAll() As DataTable
        Return dl.ban_TransaccionesSelectAll()
    End Function

    Public Function ban_TransaccionesSelectByPK _
      (ByVal IdTransaccion As System.Int32 _
      ) As ban_Transacciones

        Return dl.ban_TransaccionesSelectByPK( _
         IdTransaccion _
         )
    End Function

    Public Sub ban_TransaccionesDeleteByPK _
      (ByVal IdTransaccion As System.Int32 _
      )

        dl.ban_TransaccionesDeleteByPK( _
         IdTransaccion _
         )
    End Sub

    Public Sub ban_TransaccionesInsert _
    (ByVal entidad As ban_Transacciones)

        dl.ban_TransaccionesInsert(entidad)
    End Sub

    Public Sub ban_TransaccionesUpdate _
    (ByVal entidad As ban_Transacciones)

        dl.ban_TransaccionesUpdate(entidad)
    End Sub

    Public Function ban_TransaccionesDetalleSelectAll() As DataTable
        Return dl.ban_TransaccionesDetalleSelectAll()
    End Function

    Public Function ban_TransaccionesDetalleSelectByPK _
      (ByVal IdTransaccion As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As ban_TransaccionesDetalle

        Return dl.ban_TransaccionesDetalleSelectByPK( _
         IdTransaccion _
         , IdDetalle _
         )
    End Function

    Public Sub ban_TransaccionesDetalleDeleteByPK(ByVal IdTransaccion As System.Int32, ByVal IdDetalle As System.Int32)

        dl.ban_TransaccionesDetalleDeleteByPK(IdTransaccion, IdDetalle)
    End Sub

    Public Sub ban_TransaccionesDetalleInsert(ByVal entidad As ban_TransaccionesDetalle)

        dl.ban_TransaccionesDetalleInsert(entidad)
    End Sub

    Public Sub ban_TransaccionesDetalleUpdate(ByVal entidad As ban_TransaccionesDetalle)

        dl.ban_TransaccionesDetalleUpdate(entidad)
    End Sub

#End Region

#Region "Compras"
    Public Function com_ProveedoresSelectAll() As DataTable
        Return dl.com_ProveedoresSelectAll()
    End Function

    Public Function com_ProveedoresSelectByPK(ByVal id_proveedor As System.String) As com_Proveedores
        Return dl.com_ProveedoresSelectByPK(id_proveedor)
    End Function

    Public Sub com_ProveedoresDeleteByPK(ByVal IdProveedor As System.String)
        dl.com_ProveedoresDeleteByPK(IdProveedor)
    End Sub

    Public Sub com_ProveedoresInsert(ByVal entidad As com_Proveedores)
        dl.com_ProveedoresInsert(entidad)
    End Sub

    Public Sub com_ProveedoresUpdate(ByVal entidad As com_Proveedores)
        dl.com_ProveedoresUpdate(entidad)
    End Sub
    Public Function com_GastosImportacionesSelectAll() As DataTable
        Return dl.com_GastosImportacionesSelectAll()
    End Function

    Public Function com_GastosImportacionesSelectByPK _
      (ByVal IdGasto As System.Int32 _
      ) As com_GastosImportaciones

        Return dl.com_GastosImportacionesSelectByPK( _
         IdGasto _
         )
    End Function

    Public Sub com_GastosImportacionesDeleteByPK _
      (ByVal IdGasto As System.Int32 _
      )

        dl.com_GastosImportacionesDeleteByPK( _
         IdGasto _
         )
    End Sub

    Public Sub com_GastosImportacionesInsert _
    (ByVal entidad As com_GastosImportaciones)

        dl.com_GastosImportacionesInsert(entidad)
    End Sub

    Public Sub com_GastosImportacionesUpdate _
    (ByVal entidad As com_GastosImportaciones)

        dl.com_GastosImportacionesUpdate(entidad)
    End Sub
    Public Function com_CajasChicaSelectAll() As DataTable
        Return dl.com_CajasChicaSelectAll()
    End Function

    Public Function com_CajasChicaSelectByPK _
      (ByVal IdCaja As System.Int32 _
      ) As com_CajasChica

        Return dl.com_CajasChicaSelectByPK( _
         IdCaja _
         )
    End Function

    Public Sub com_CajasChicaDeleteByPK _
      (ByVal IdCaja As System.Int32 _
      )

        dl.com_CajasChicaDeleteByPK( _
         IdCaja _
         )
    End Sub

    Public Sub com_CajasChicaInsert _
    (ByVal entidad As com_CajasChica)

        dl.com_CajasChicaInsert(entidad)
    End Sub

    Public Sub com_CajasChicaUpdate _
    (ByVal entidad As com_CajasChica)

        dl.com_CajasChicaUpdate(entidad)
    End Sub


    Public Function com_ImportacionesProveedoresSelectAll() As DataTable
        Return dl.com_ImportacionesProveedoresSelectAll()
    End Function

    Public Function com_ImportacionesProveedoresSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As com_ImportacionesProveedores

        Return dl.com_ImportacionesProveedoresSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub com_ImportacionesProveedoresDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.com_ImportacionesProveedoresDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub com_ImportacionesProveedoresInsert _
    (ByVal entidad As com_ImportacionesProveedores)

        dl.com_ImportacionesProveedoresInsert(entidad)
    End Sub

    Public Sub com_ImportacionesProveedoresUpdate _
    (ByVal entidad As com_ImportacionesProveedores)

        dl.com_ImportacionesProveedoresUpdate(entidad)
    End Sub
    Public Function com_ImportacionesGastosProductosSelectAll() As DataTable
        Return dl.com_ImportacionesGastosProductosSelectAll()
    End Function

    Public Function com_ImportacionesGastosProductosSelectByPK _
      (ByVal IdImportacion As System.Int32 _
      , ByVal Referencia As System.String _
      , ByVal IdProducto As System.String _
      , ByVal IdGasto As System.Int32 _
      ) As com_ImportacionesGastosProductos

        Return dl.com_ImportacionesGastosProductosSelectByPK( _
         IdImportacion _
         , Referencia _
         , IdProducto _
         , IdGasto _
         )
    End Function

    Public Sub com_ImportacionesGastosProductosDeleteByPK _
      (ByVal IdImportacion As System.Int32 _
      , ByVal Referencia As System.String _
      , ByVal IdProducto As System.String _
      , ByVal IdGasto As System.Int32 _
      )

        dl.com_ImportacionesGastosProductosDeleteByPK( _
         IdImportacion _
         , Referencia _
         , IdProducto _
         , IdGasto _
         )
    End Sub

    Public Sub com_ImportacionesGastosProductosInsert _
    (ByVal entidad As com_ImportacionesGastosProductos)

        dl.com_ImportacionesGastosProductosInsert(entidad)
    End Sub

    Public Sub com_ImportacionesGastosProductosUpdate _
    (ByVal entidad As com_ImportacionesGastosProductos)

        dl.com_ImportacionesGastosProductosUpdate(entidad)
    End Sub





    Public Function com_LiquidacionSelectAll() As DataTable
        Return dl.com_LiquidacionSelectAll()
    End Function

    Public Function com_LiquidacionSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As com_Liquidacion

        Return dl.com_LiquidacionSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub com_LiquidacionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.com_LiquidacionDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub com_LiquidacionInsert _
    (ByVal entidad As com_Liquidacion)

        dl.com_LiquidacionInsert(entidad)
    End Sub

    Public Sub com_LiquidacionUpdate _
    (ByVal entidad As com_Liquidacion)

        dl.com_LiquidacionUpdate(entidad)
    End Sub



    Public Function com_RequisicionesSelectAll() As DataTable
        Return dl.com_RequisicionesSelectAll()
    End Function

    Public Function com_RequisicionesSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As com_Requisiciones

        Return dl.com_RequisicionesSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub com_RequisicionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.com_RequisicionesDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub com_RequisicionesInsert _
    (ByVal entidad As com_Requisiciones)

        dl.com_RequisicionesInsert(entidad)
    End Sub

    Public Sub com_RequisicionesUpdate _
    (ByVal entidad As com_Requisiciones)

        dl.com_RequisicionesUpdate(entidad)
    End Sub
    Public Function com_RequisicionesDetalleSelectAll() As DataTable
        Return dl.com_RequisicionesDetalleSelectAll()
    End Function

    Public Function com_RequisicionesDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_RequisicionesDetalle

        Return dl.com_RequisicionesDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub com_RequisicionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.com_RequisicionesDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_RequisicionesDetalleInsert _
    (ByVal entidad As com_RequisicionesDetalle)

        dl.com_RequisicionesDetalleInsert(entidad)
    End Sub

    Public Sub com_RequisicionesDetalleUpdate _
    (ByVal entidad As com_RequisicionesDetalle)

        dl.com_RequisicionesDetalleUpdate(entidad)
    End Sub




    Public Function com_OrdenesCompraSelectAll() As DataTable
        Return dl.com_OrdenesCompraSelectAll()
    End Function

    Public Function com_OrdenesCompraSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As com_OrdenesCompra

        Return dl.com_OrdenesCompraSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub com_OrdenesCompraDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.com_OrdenesCompraDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub com_OrdenesCompraInsert _
    (ByVal entidad As com_OrdenesCompra)

        dl.com_OrdenesCompraInsert(entidad)
    End Sub

    Public Sub com_OrdenesCompraUpdate _
    (ByVal entidad As com_OrdenesCompra)

        dl.com_OrdenesCompraUpdate(entidad)
    End Sub

    Public Function com_OrdenesCompraDetalleSelectAll() As DataTable
        Return dl.com_OrdenesCompraDetalleSelectAll()
    End Function

    Public Function com_OrdenesCompraDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_OrdenesCompraDetalle

        Return dl.com_OrdenesCompraDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub com_OrdenesCompraDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.com_OrdenesCompraDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_OrdenesCompraDetalleInsert _
    (ByVal entidad As com_OrdenesCompraDetalle)

        dl.com_OrdenesCompraDetalleInsert(entidad)
    End Sub

    Public Function com_ComprasSelectAll() As DataTable
        Return dl.com_ComprasSelectAll()
    End Function

    Public Function com_ComprasSelectByPK(ByVal IdComprobante As System.Int32) As com_Compras
        Return dl.com_ComprasSelectByPK(IdComprobante)
    End Function

    Public Sub com_ComprasDeleteByPK(ByVal IdComprobante As System.Int32)
        dl.com_ComprasDeleteByPK(IdComprobante)
    End Sub

    Public Sub com_ComprasInsert(ByVal entidad As com_Compras)
        dl.com_ComprasInsert(entidad)
    End Sub

    Public Sub com_ComprasUpdate(ByVal entidad As com_Compras)
        dl.com_ComprasUpdate(entidad)
    End Sub

    Public Function com_ComprasDetalleSelectAll() As DataTable
        Return dl.com_ComprasDetalleSelectAll()
    End Function

    Public Function com_ComprasDetalleSelectByPK(ByVal IdComprobante As System.Int32, ByVal IdDetalle As System.Int32) As com_ComprasDetalle

        Return dl.com_ComprasDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub com_ComprasDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.com_ComprasDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_ComprasDetalleInsert(ByVal entidad As com_ComprasDetalle)

        dl.com_ComprasDetalleInsert(entidad)
    End Sub
    Public Function com_ImportacionesSelectAll() As DataTable
        Return dl.com_ImportacionesSelectAll()
    End Function

    Public Function com_ImportacionesSelectByPK(ByVal IdComprobante As Int32) As com_Importaciones
        Return dl.com_ImportacionesSelectByPK(IdComprobante)
    End Function

    Public Sub com_ImportacionesDeleteByPK(ByVal IdComprobante As Int32)

        dl.com_ImportacionesDeleteByPK(IdComprobante)
    End Sub

    Public Sub com_ImportacionesUpdate(ByVal entidad As com_Importaciones)

        dl.com_ImportacionesUpdate(entidad)
    End Sub

    Public Function com_ImportacionesDetalleSelectAll() As DataTable
        Return dl.com_ImportacionesDetalleSelectAll()
    End Function

    Public Function com_ImportacionesDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_ImportacionesDetalle

        Return dl.com_ImportacionesDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub com_ImportacionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.com_ImportacionesDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_ImportacionesDetalleInsert _
    (ByVal entidad As com_ImportacionesDetalle)

        dl.com_ImportacionesDetalleInsert(entidad)
    End Sub

    Public Sub com_ImportacionesDetalleUpdate _
    (ByVal entidad As com_ImportacionesDetalle)

        dl.com_ImportacionesDetalleUpdate(entidad)
    End Sub
    Public Function com_ImportacionesGastosDetalleSelectAll() As DataTable
        Return dl.com_ImportacionesGastosDetalleSelectAll()
    End Function

    Public Function com_ImportacionesGastosDetalleSelectByPK _
      (ByVal IdImportacion As System.Int32 _
      , ByVal IdGasto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_ImportacionesGastosDetalle

        Return dl.com_ImportacionesGastosDetalleSelectByPK( _
         IdImportacion _
         , IdGasto _
         , IdDetalle _
         )
    End Function

    Public Sub com_ImportacionesGastosDetalleDeleteByPK _
      (ByVal IdImportacion As System.Int32 _
      , ByVal IdGasto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.com_ImportacionesGastosDetalleDeleteByPK( _
         IdImportacion _
         , IdGasto _
         , IdDetalle _
         )
    End Sub

    Public Sub com_ImportacionesGastosDetalleInsert _
    (ByVal entidad As com_ImportacionesGastosDetalle)

        dl.com_ImportacionesGastosDetalleInsert(entidad)
    End Sub

    Public Sub com_ImportacionesGastosDetalleUpdate _
    (ByVal entidad As com_ImportacionesGastosDetalle)

        dl.com_ImportacionesGastosDetalleUpdate(entidad)
    End Sub


    Public Function com_NotasCreditoSelectAll() As DataTable
        Return dl.com_NotasCreditoSelectAll()
    End Function

    Public Function com_NotasCreditoSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_NotasCredito

        Return dl.com_NotasCreditoSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub com_NotasCreditoDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.com_NotasCreditoDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_NotasCreditoInsert _
    (ByVal entidad As com_NotasCredito)

        dl.com_NotasCreditoInsert(entidad)
    End Sub

    Public Sub com_NotasCreditoUpdate _
    (ByVal entidad As com_NotasCredito)

        dl.com_NotasCreditoUpdate(entidad)
    End Sub

    Public Function com_RetencionesSelectAll() As DataTable
        Return dl.com_RetencionesSelectAll()
    End Function

    Public Function com_RetencionesSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As com_Retenciones

        Return dl.com_RetencionesSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub com_RetencionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.com_RetencionesDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub com_RetencionesInsert _
    (ByVal entidad As com_Retenciones)

        dl.com_RetencionesInsert(entidad)
    End Sub

    Public Sub com_RetencionesUpdate _
    (ByVal entidad As com_Retenciones)

        dl.com_RetencionesUpdate(entidad)
    End Sub

    Public Function com_RetencionesDetalleSelectAll() As DataTable
        Return dl.com_RetencionesDetalleSelectAll()
    End Function

    Public Function com_RetencionesDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_RetencionesDetalle

        Return dl.com_RetencionesDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub com_RetencionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.com_RetencionesDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_RetencionesDetalleInsert _
    (ByVal entidad As com_RetencionesDetalle)

        dl.com_RetencionesDetalleInsert(entidad)
    End Sub

    Public Sub com_RetencionesDetalleUpdate _
    (ByVal entidad As com_RetencionesDetalle)

        dl.com_RetencionesDetalleUpdate(entidad)
    End Sub

    Public Function com_ImpuestosSelectAll() As DataTable
        Return dl.com_ImpuestosSelectAll()
    End Function

    Public Function com_ImpuestosSelectByPK _
      (ByVal IdImpuesto As System.Int32 _
      ) As com_Impuestos

        Return dl.com_ImpuestosSelectByPK( _
         IdImpuesto _
         )
    End Function

    Public Sub com_ImpuestosDeleteByPK _
      (ByVal IdImpuesto As System.Int32 _
      )

        dl.com_ImpuestosDeleteByPK( _
         IdImpuesto _
         )
    End Sub

    Public Sub com_ImpuestosInsert _
    (ByVal entidad As com_Impuestos)

        dl.com_ImpuestosInsert(entidad)
    End Sub

    Public Sub com_ImpuestosUpdate _
    (ByVal entidad As com_Impuestos)

        dl.com_ImpuestosUpdate(entidad)
    End Sub

    Public Function com_ComprasDetalleImpuestosSelectAll() As DataTable
        Return dl.com_ComprasDetalleImpuestosSelectAll()
    End Function

    Public Function com_ComprasDetalleImpuestosSelectByPK _
      (ByVal IdCompra As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_ComprasDetalleImpuestos

        Return dl.com_ComprasDetalleImpuestosSelectByPK( _
         IdCompra _
         , IdDetalle _
         )
    End Function

    Public Sub com_ComprasDetalleImpuestosDeleteByPK _
      (ByVal IdCompra As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.com_ComprasDetalleImpuestosDeleteByPK( _
         IdCompra _
         , IdDetalle _
         )
    End Sub

    Public Sub com_ComprasDetalleImpuestosInsert _
    (ByVal entidad As com_ComprasDetalleImpuestos)

        dl.com_ComprasDetalleImpuestosInsert(entidad)
    End Sub

    Public Sub com_ComprasDetalleImpuestosUpdate _
    (ByVal entidad As com_ComprasDetalleImpuestos)

        dl.com_ComprasDetalleImpuestosUpdate(entidad)
    End Sub

    Public Function com_OrdenesCompraDetallePagoSelectAll() As DataTable
        Return dl.com_OrdenesCompraDetallePagoSelectAll()
    End Function

    Public Function com_OrdenesCompraDetallePagoSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_OrdenesCompraDetallePago

        Return dl.com_OrdenesCompraDetallePagoSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub com_OrdenesCompraDetallePagoDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.com_OrdenesCompraDetallePagoDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_OrdenesCompraDetallePagoInsert _
    (ByVal entidad As com_OrdenesCompraDetallePago)

        dl.com_OrdenesCompraDetallePagoInsert(entidad)
    End Sub

    Public Sub com_OrdenesCompraDetallePagoUpdate _
    (ByVal entidad As com_OrdenesCompraDetallePago)

        dl.com_OrdenesCompraDetallePagoUpdate(entidad)
    End Sub





#End Region

#Region "Contabilidad"

	Public Function con_FormulasFinancierasSelectAll() As DataTable
		Return dl.con_FormulasFinancierasSelectAll()
	End Function

	Public Function con_FormulasFinancierasSelectByPK _
		(ByVal Id As System.Int32
		) As con_FormulasFinancieras

		Return dl.con_FormulasFinancierasSelectByPK(
		Id
		)
	End Function

	Public Sub con_FormulasFinancierasDeleteByPK _
		(ByVal Id As System.Int32
		)

		dl.con_FormulasFinancierasDeleteByPK(
		Id
		)
	End Sub

	Public Sub con_FormulasFinancierasInsert _
(ByVal entidad As con_FormulasFinancieras)

		dl.con_FormulasFinancierasInsert(entidad)
	End Sub

	Public Sub con_FormulasFinancierasUpdate _
(ByVal entidad As con_FormulasFinancieras)

		dl.con_FormulasFinancierasUpdate(entidad)
	End Sub





	Public Function con_CuentasSearchByPk(ByVal pIdCuenta As String, ByVal pIdPresupuesto As Integer) As DataTable
        Return dl.con_CuentasSearchByPk(pIdCuenta, pIdPresupuesto)
    End Function
    Public Function con_CuentasSelectAll() As DataTable
        Return dl.con_CuentasSelectAll()
    End Function
    Public Function con_CuentasSelectParaArbol() As DataTable
        Return dl.con_CuentasSelectParaArbol()
    End Function
    Public Function con_CuentasSelectByPK _
      (ByVal IdCuenta As System.String _
      ) As con_Cuentas

        Return dl.con_CuentasSelectByPK( _
         IdCuenta _
         )
    End Function

    Public Sub con_CuentasDeleteByPK _
      (ByVal IdCuenta As System.String
      )

        dl.con_CuentasDeleteByPK(
         IdCuenta
         )
    End Sub

    Public Sub con_CuentasUpdateNaturaleza(ByVal IdCuenta As String, ByVal Naturaleza As Integer, Optional UpdateSubCuentas As Boolean = False)
        dl.con_CuentasUpdateNaturaleza(IdCuenta, Naturaleza, UpdateSubCuentas)
    End Sub

    Public Sub con_CuentasInsert _
    (ByVal entidad As con_Cuentas)

        dl.con_CuentasInsert(entidad)
    End Sub

    Public Sub con_CuentasUpdate _
    (ByVal entidad As con_Cuentas)

        dl.con_CuentasUpdate(entidad)
    End Sub

    Public Function con_PartidasSelectAll() As DataTable
        Return dl.con_PartidasSelectAll()
    End Function

    Public Function con_PartidasSelectByPK _
      (ByVal IdPartida As System.Int32 _
      ) As con_Partidas

        Return dl.con_PartidasSelectByPK( _
         IdPartida _
         )
    End Function

    Public Sub con_PartidasDeleteByPK _
      (ByVal IdPartida As System.Int32 _
      )

        dl.con_PartidasDeleteByPK( _
         IdPartida _
         )
    End Sub

    Public Sub con_PartidasInsert _
    (ByVal entidad As con_Partidas)

        dl.con_PartidasInsert(entidad)
    End Sub

    Public Sub con_PartidasUpdate _
    (ByVal entidad As con_Partidas)

        dl.con_PartidasUpdate(entidad)
    End Sub

    Public Function con_PartidasDetalleSelectAll() As DataTable
        Return dl.con_PartidasDetalleSelectAll()
    End Function

    Public Function con_PartidasDetalleSelectByPK _
      (ByVal IdPartida As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As con_PartidasDetalle

        Return dl.con_PartidasDetalleSelectByPK( _
         IdPartida _
         , IdDetalle _
         )
    End Function

    Public Sub con_PartidasDetalleDeleteByPK _
      (ByVal IdPartida As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.con_PartidasDetalleDeleteByPK( _
         IdPartida _
         , IdDetalle _
         )
    End Sub

    Public Sub con_PartidasDetalleInsert _
    (ByVal entidad As con_PartidasDetalle)

        dl.con_PartidasDetalleInsert(entidad)
    End Sub

    Public Sub con_PartidasDetalleUpdate _
    (ByVal entidad As con_PartidasDetalle)

        dl.con_PartidasDetalleUpdate(entidad)
    End Sub

    Public Function con_TiposPartidaSelectAll() As DataTable
        Return dl.con_TiposPartidaSelectAll()
    End Function

    Public Function con_TiposPartidaSelectByPK _
      (ByVal IdTipo As System.String _
      ) As con_TiposPartida

        Return dl.con_TiposPartidaSelectByPK( _
         IdTipo _
         )
    End Function

    Public Sub con_TiposPartidaDeleteByPK _
      (ByVal IdTipo As System.String _
      )

        dl.con_TiposPartidaDeleteByPK( _
         IdTipo _
         )
    End Sub

    Public Sub con_TiposPartidaInsert _
    (ByVal entidad As con_TiposPartida)

        dl.con_TiposPartidaInsert(entidad)
    End Sub

    Public Sub con_TiposPartidaUpdate _
    (ByVal entidad As con_TiposPartida)

        dl.con_TiposPartidaUpdate(entidad)
    End Sub
    Public Function con_CentrosCostoSelectAll() As DataTable
        Return dl.con_CentrosCostoSelectAll()
    End Function

    Public Function con_CentrosCostoSelectByPK _
      (ByVal IdCentro As System.String _
      ) As con_CentrosCosto

        Return dl.con_CentrosCostoSelectByPK( _
         IdCentro _
         )
    End Function

    Public Sub con_CentrosCostoDeleteByPK _
      (ByVal IdCentro As System.String _
      )

        dl.con_CentrosCostoDeleteByPK( _
         IdCentro _
         )
    End Sub

    Public Sub con_CentrosCostoInsert _
    (ByVal entidad As con_CentrosCosto)

        dl.con_CentrosCostoInsert(entidad)
    End Sub

    Public Sub con_CentrosCostoUpdate _
    (ByVal entidad As con_CentrosCosto)

        dl.con_CentrosCostoUpdate(entidad)
    End Sub

    Public Function con_CentrosMayorSelectAll() As DataTable
        Return dl.con_CentrosMayorSelectAll()
    End Function

    Public Function con_CentrosMayorSelectByPK _
      (ByVal IdCuentaMayor As System.String _
      , ByVal IdCentro As System.String _
      ) As con_CentrosMayor

        Return dl.con_CentrosMayorSelectByPK( _
         IdCuentaMayor _
         , IdCentro _
         )
    End Function

    Public Sub con_CentrosMayorDeleteByPK _
      (ByVal IdCuentaMayor As System.String _
      , ByVal IdCentro As System.String _
      )

        dl.con_CentrosMayorDeleteByPK( _
         IdCuentaMayor _
         , IdCentro _
         )
    End Sub

    Public Sub con_CentrosMayorInsert _
    (ByVal entidad As con_CentrosMayor)

        dl.con_CentrosMayorInsert(entidad)
    End Sub

    Public Sub con_CentrosMayorUpdate _
    (ByVal entidad As con_CentrosMayor)

        dl.con_CentrosMayorUpdate(entidad)
    End Sub
    Public Function con_CuentasPatrimonioSelectAll() As DataTable
        Return dl.con_CuentasPatrimonioSelectAll()
    End Function

    Public Function con_CuentasPatrimonioSelectByPK _
      (ByVal IdCuenta As System.String _
      ) As con_CuentasPatrimonio

        Return dl.con_CuentasPatrimonioSelectByPK( _
         IdCuenta _
         )
    End Function

    Public Sub con_CuentasPatrimonioDeleteByPK _
      (ByVal IdCuenta As System.String _
      )

        dl.con_CuentasPatrimonioDeleteByPK( _
         IdCuenta _
         )
    End Sub

    Public Sub con_CuentasPatrimonioInsert _
    (ByVal entidad As con_CuentasPatrimonio)

        dl.con_CuentasPatrimonioInsert(entidad)
    End Sub

    Public Sub con_CuentasPatrimonioUpdate _
    (ByVal entidad As con_CuentasPatrimonio)

        dl.con_CuentasPatrimonioUpdate(entidad)
    End Sub


    Public Function con_NIIFConceptosSelectAll() As DataTable
        Return dl.con_NIIFConceptosSelectAll()
    End Function

    Public Function con_NIIFConceptosSelectByPK(ByVal IdReporte As System.Int32, ByVal IdDetalle As System.Int32) As con_NIIFConceptos

        Return dl.con_NIIFConceptosSelectByPK(IdReporte, IdDetalle)
    End Function

    Public Sub con_NIIFConceptosDeleteByPK(ByVal IdReporte As System.Int32, ByVal IdDetalle As System.Int32 _
      )

        dl.con_NIIFConceptosDeleteByPK(IdReporte _
         , IdDetalle _
         )
    End Sub

    Public Sub con_NIIFConceptosInsert(ByVal entidad As con_NIIFConceptos)

        dl.con_NIIFConceptosInsert(entidad)
    End Sub

    Public Sub con_NIIFConceptosUpdate(ByVal entidad As con_NIIFConceptos)

        dl.con_NIIFConceptosUpdate(entidad)
    End Sub
    Public Function con_RubroFlujoSelectAll() As DataTable
        Return dl.con_RubroFlujoSelectAll()
    End Function

    Public Function con_RubroFlujoSelectByPK _
      (ByVal IdRubro As System.Int32 _
      ) As con_RubroFlujo

        Return dl.con_RubroFlujoSelectByPK( _
         IdRubro _
         )
    End Function

    Public Sub con_RubroFlujoDeleteByPK _
      (ByVal IdRubro As System.Int32 _
      )

        dl.con_RubroFlujoDeleteByPK( _
         IdRubro _
         )
    End Sub

    Public Sub con_RubroFlujoInsert _
    (ByVal entidad As con_RubroFlujo)

        dl.con_RubroFlujoInsert(entidad)
    End Sub

    Public Sub con_RubroFlujoUpdate _
    (ByVal entidad As con_RubroFlujo)

        dl.con_RubroFlujoUpdate(entidad)
    End Sub

    Public Function con_RubroFlujoDetalleSelectAll() As DataTable
        Return dl.con_RubroFlujoDetalleSelectAll()
    End Function

    Public Function con_RubroFlujoDetalleSelectByPK _
      (ByVal IdCuentaMayor As System.String _
      ) As con_RubroFlujoDetalle

        Return dl.con_RubroFlujoDetalleSelectByPK( _
         IdCuentaMayor _
         )
    End Function

    Public Sub con_RubroFlujoDetalleDeleteByPK _
      (ByVal IdCuentaMayor As System.String _
      )

        dl.con_RubroFlujoDetalleDeleteByPK( _
         IdCuentaMayor _
         )
    End Sub

    Public Sub con_RubroFlujoDetalleInsert _
    (ByVal entidad As con_RubroFlujoDetalle)

        dl.con_RubroFlujoDetalleInsert(entidad)
    End Sub

    Public Sub con_RubroFlujoDetalleUpdate _
    (ByVal entidad As con_RubroFlujoDetalle)

        dl.con_RubroFlujoDetalleUpdate(entidad)
    End Sub

    Public Function con_ConceptosCajaSelectAll() As DataTable
        Return dl.con_ConceptosCajaSelectAll()
    End Function

    Public Function con_ConceptosCajaSelectByPK _
      (ByVal IdConcepto As System.Int32 _
      ) As con_ConceptosCaja

        Return dl.con_ConceptosCajaSelectByPK( _
         IdConcepto _
         )
    End Function

    Public Sub con_ConceptosCajaDeleteByPK _
      (ByVal IdConcepto As System.Int32 _
      )

        dl.con_ConceptosCajaDeleteByPK( _
         IdConcepto _
         )
    End Sub

    Public Sub con_ConceptosCajaInsert _
    (ByVal entidad As con_ConceptosCaja)

        dl.con_ConceptosCajaInsert(entidad)
    End Sub

    Public Sub con_ConceptosCajaUpdate _
    (ByVal entidad As con_ConceptosCaja)

        dl.con_ConceptosCajaUpdate(entidad)
    End Sub

    Public Function con_TransaccionesSelectAll() As DataTable
        Return dl.con_TransaccionesSelectAll()
    End Function

    Public Function con_TransaccionesSelectByPK _
      (ByVal IdComprobante As System.Guid _
      ) As con_Transacciones

        Return dl.con_TransaccionesSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub con_TransaccionesDeleteByPK _
      (ByVal IdComprobante As System.Guid _
      )

        dl.con_TransaccionesDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub con_TransaccionesInsert _
    (ByVal entidad As con_Transacciones)

        dl.con_TransaccionesInsert(entidad)
    End Sub

    Public Sub con_TransaccionesUpdate _
    (ByVal entidad As con_Transacciones)

        dl.con_TransaccionesUpdate(entidad)
    End Sub


#End Region
#Region "Presupuestos"


    Public Function pre_DepartamentosSucursalSelectAll() As DataTable
        Return dl.pre_DepartamentosSucursalSelectAll()
    End Function

    Public Function pre_DepartamentosSucursalSelectByPK _
      (ByVal IdDepartamentoSucursalPk As System.Int32 _
      ) As pre_DepartamentosSucursal

        Return dl.pre_DepartamentosSucursalSelectByPK( _
         IdDepartamentoSucursalPk _
         )
    End Function

    Public Sub pre_DepartamentosSucursalDeleteByPK _
      (ByVal IdDepartamentoSucursalPk As System.Int32 _
      )

        dl.pre_DepartamentosSucursalDeleteByPK( _
         IdDepartamentoSucursalPk _
         )
    End Sub

    Public Sub pre_DepartamentosSucursalInsert _
    (ByVal entidad As pre_DepartamentosSucursal)

        dl.pre_DepartamentosSucursalInsert(entidad)
    End Sub

    Public Sub pre_DepartamentosSucursalUpdate _
    (ByVal entidad As pre_DepartamentosSucursal)

        dl.pre_DepartamentosSucursalUpdate(entidad)
    End Sub

    Public Function pre_DetallesPresupuestoSelectAll() As DataTable
        Return dl.pre_DetallesPresupuestoSelectAll()
    End Function

    Public Function pre_DetallesPresupuestoSelectByCuenta(ByVal pIdPreCuenta As Integer, ByVal pMes As pre_DetallesPresupuesto.Meses) As DataTable
        Return dl.pre_DetallesPresupuestoSelectByCuenta(pIdPreCuenta, pMes)
    End Function

    Public Function pre_ObtenerTotalPresupuestoMes(ByVal pMes As Integer, ByVal pIdPresupuesto As Integer) As Decimal
        Return dl.pre_ObtenerTotalPresupuestoMes(pMes, pIdPresupuesto)
    End Function

    Public Function pre_PresupuestosSelectRepeated(ByVal pIdSucursal As Integer, ByVal pIdDepto As Integer, ByVal pIdCetoCosto As Integer, ByVal pAnio As Integer) As Integer
        Return dl.pre_PresupuestosSelectRepeated(pIdSucursal, pIdDepto, pIdCetoCosto, pAnio)
    End Function

    Public Function pre_ObtenerNumeroEdicionesMes(ByVal pMes As Integer, ByVal pIdPreCuenta As Integer) As Decimal
        Dim cantidad As Integer = dl.pre_ObtenerNumeroEdicionesMes(pMes, pIdPreCuenta)
        If cantidad > 0 Then
            cantidad = cantidad - 1
        End If
        Return cantidad
    End Function

    Public Function pre_ObtenerTotalPresupuesto(ByVal pIdPreCuenta As Integer) As Decimal
        Return dl.pre_ObtenerTotalPresupuesto(pIdPreCuenta)
    End Function

    Public Function pre_DetallesPresupuestoSelectByPK _
      (ByVal IdDetallePresupuestoPk As System.Int32 _
      ) As pre_DetallesPresupuesto

        Return dl.pre_DetallesPresupuestoSelectByPK( _
         IdDetallePresupuestoPk _
         )
    End Function

    Public Sub pre_DetallesPresupuestoDeleteByPK _
      (ByVal IdDetallePresupuestoPk As System.Int32 _
      )

        dl.pre_DetallesPresupuestoDeleteByPK( _
         IdDetallePresupuestoPk _
         )
    End Sub

    Public Sub pre_DetallesPresupuestoInsert _
    (ByVal entidad As pre_DetallesPresupuesto)

        dl.pre_DetallesPresupuestoInsert(entidad)
    End Sub

    Public Sub pre_DetallesPresupuestoUpdate _
    (ByVal entidad As pre_DetallesPresupuesto)

        dl.pre_DetallesPresupuestoUpdate(entidad)
    End Sub

    Public Function pre_PresupuestoCuentaSelectAll() As DataTable
        Return dl.pre_PresupuestoCuentaSelectAll()
    End Function

    Public Function pre_PresupuestoCuentaSelectByPresupuesto(ByVal pIdPresupuesto As Integer) As DataTable
        Return dl.pre_PresupuestoCuentaSelectByPresupuesto(pIdPresupuesto)
    End Function

    Public Function pre_PresupuestoCuentaSelectByPK _
      (ByVal IdPresupuestoCuentaPk As System.Int32 _
      ) As pre_PresupuestoCuenta

        Return dl.pre_PresupuestoCuentaSelectByPK( _
         IdPresupuestoCuentaPk _
         )
    End Function

    Public Sub pre_PresupuestoCuentaDeleteByPK _
      (ByVal IdPresupuestoCuentaPk As System.Int32 _
      )

        dl.pre_PresupuestoCuentaDeleteByPK( _
         IdPresupuestoCuentaPk _
         )
    End Sub

    Public Sub pre_PresupuestoCuentaInsert _
    (ByVal entidad As pre_PresupuestoCuenta)

        dl.pre_PresupuestoCuentaInsert(entidad)
    End Sub

    Public Sub pre_PresupuestoCuentaUpdate _
    (ByVal entidad As pre_PresupuestoCuenta)

        dl.pre_PresupuestoCuentaUpdate(entidad)
    End Sub

    Public Function pre_PresupuestosSelectAll() As DataTable
        Return dl.pre_PresupuestosSelectAll()
    End Function

    Public Function pre_PresupuestosSelectByPK _
      (ByVal IdPresupuestoPk As System.Int32 _
      ) As pre_Presupuestos

        Return dl.pre_PresupuestosSelectByPK( _
         IdPresupuestoPk _
         )
    End Function

    Public Sub pre_PresupuestosDeleteByPK _
      (ByVal IdPresupuestoPk As System.Int32 _
      )

        dl.pre_PresupuestosDeleteByPK( _
         IdPresupuestoPk _
         )
    End Sub

    Public Sub pre_PresupuestosInsert _
    (ByVal entidad As pre_Presupuestos)

        dl.pre_PresupuestosInsert(entidad)
    End Sub

    Public Sub pre_PresupuestosUpdate _
    (ByVal entidad As pre_Presupuestos)

        dl.pre_PresupuestosUpdate(entidad)
    End Sub

#End Region

#Region "Facturacion"
	Public Function fac_ProspectacionClientesSelectAll() As DataTable
		Return dl.fac_ProspectacionClientesSelectAll()
	End Function

	Public Function fac_ProspectacionClientesSelectByPK _
		(ByVal IdComprobante As System.Int32
		) As fac_ProspectacionClientes

		Return dl.fac_ProspectacionClientesSelectByPK(
		IdComprobante
		)
	End Function

	Public Sub fac_ProspectacionClientesDeleteByPK _
		(ByVal IdComprobante As System.Int32
		)

		dl.fac_ProspectacionClientesDeleteByPK(
		IdComprobante
		)
	End Sub

	Public Sub fac_ProspectacionClientesInsert _
(ByVal entidad As fac_ProspectacionClientes)

		dl.fac_ProspectacionClientesInsert(entidad)
	End Sub

	Public Sub fac_ProspectacionClientesUpdate _
(ByVal entidad As fac_ProspectacionClientes)

		dl.fac_ProspectacionClientesUpdate(entidad)
	End Sub




	Public Function fac_BitacoraAlertasSelectAll() As DataTable
        Return dl.fac_BitacoraAlertasSelectAll()
    End Function

    Public Function fac_BitacoraAlertasSelectByPK _
            (ByVal IdBitacora As System.Int32 _
            ) As fac_BitacoraAlertas

        Return dl.fac_BitacoraAlertasSelectByPK( _
            IdBitacora _
            )
    End Function

    Public Sub fac_BitacoraAlertasDeleteByPK _
            (ByVal IdBitacora As System.Int32 _
            )

        dl.fac_BitacoraAlertasDeleteByPK( _
            IdBitacora _
            )
    End Sub

    Public Sub fac_BitacoraAlertasInsert _
    (ByVal entidad As fac_BitacoraAlertas)

        dl.fac_BitacoraAlertasInsert(entidad)
    End Sub

    Public Sub fac_BitacoraAlertasUpdate _
    (ByVal entidad As fac_BitacoraAlertas)

        dl.fac_BitacoraAlertasUpdate(entidad)
    End Sub




    Public Function fac_CorreosAlertaSelectAll() As DataTable
        Return dl.fac_CorreosAlertaSelectAll()
    End Function

    Public Function fac_CorreosAlertaSelectByPK _
            (ByVal IdCorreo As System.Int32 _
            ) As fac_CorreosAlerta

        Return dl.fac_CorreosAlertaSelectByPK( _
            IdCorreo _
            )
    End Function

    Public Sub fac_CorreosAlertaDeleteByPK _
            (ByVal IdCorreo As System.Int32 _
            )

        dl.fac_CorreosAlertaDeleteByPK( _
            IdCorreo _
            )
    End Sub

    Public Sub fac_CorreosAlertaInsert _
    (ByVal entidad As fac_CorreosAlerta)

        dl.fac_CorreosAlertaInsert(entidad)
    End Sub

    Public Sub fac_CorreosAlertaUpdate _
    (ByVal entidad As fac_CorreosAlerta)

        dl.fac_CorreosAlertaUpdate(entidad)
    End Sub




    Public Function fac_RubrosActividadesSelectAll() As DataTable
        Return dl.fac_RubrosActividadesSelectAll()
    End Function

    Public Function fac_RubrosActividadesSelectByPK _
            (ByVal IdRubro As System.Int16 _
            ) As fac_RubrosActividades

        Return dl.fac_RubrosActividadesSelectByPK( _
            IdRubro _
            )
    End Function

    Public Sub fac_RubrosActividadesDeleteByPK _
            (ByVal IdRubro As System.Int16 _
            )

        dl.fac_RubrosActividadesDeleteByPK( _
            IdRubro _
            )
    End Sub

    Public Sub fac_RubrosActividadesInsert _
    (ByVal entidad As fac_RubrosActividades)

        dl.fac_RubrosActividadesInsert(entidad)
    End Sub

    Public Sub fac_RubrosActividadesUpdate _
    (ByVal entidad As fac_RubrosActividades)

        dl.fac_RubrosActividadesUpdate(entidad)
    End Sub



    Public Function fac_ActividadesSelectAll() As DataTable
        Return dl.fac_ActividadesSelectAll()
    End Function

    Public Function fac_ActividadesSelectByPK _
            (ByVal IdActividad As System.Int32 _
            ) As fac_Actividades

        Return dl.fac_ActividadesSelectByPK( _
            IdActividad _
            )
    End Function

    Public Sub fac_ActividadesDeleteByPK _
            (ByVal IdActividad As System.Int32 _
            )

        dl.fac_ActividadesDeleteByPK( _
            IdActividad _
            )
    End Sub

    Public Sub fac_ActividadesInsert _
    (ByVal entidad As fac_Actividades)

        dl.fac_ActividadesInsert(entidad)
    End Sub

    Public Sub fac_ActividadesUpdate _
    (ByVal entidad As fac_Actividades)

        dl.fac_ActividadesUpdate(entidad)
    End Sub

    Public Function fac_ClientesAnexoSelectAll() As DataTable
        Return dl.fac_ClientesAnexoSelectAll()
    End Function

    Public Function fac_ClientesAnexoSelectByPK _
            (ByVal IdCliente As System.String _
            ) As fac_ClientesAnexo

        Return dl.fac_ClientesAnexoSelectByPK( _
            IdCliente _
            )
    End Function

    Public Sub fac_ClientesAnexoDeleteByPK _
            (ByVal IdCliente As System.String _
            )

        dl.fac_ClientesAnexoDeleteByPK( _
            IdCliente _
            )
    End Sub

    Public Sub fac_ClientesAnexoInsert _
    (ByVal entidad As fac_ClientesAnexo)

        dl.fac_ClientesAnexoInsert(entidad)
    End Sub

    Public Sub fac_ClientesAnexoUpdate _
    (ByVal entidad As fac_ClientesAnexo)

        dl.fac_ClientesAnexoUpdate(entidad)
    End Sub

    Public Function fac_ProfesionesSelectAll() As DataTable
        Return dl.fac_ProfesionesSelectAll()
    End Function

    Public Function fac_ProfesionesSelectByPK _
            (ByVal IdProfesion As System.Int16 _
            ) As fac_Profesiones

        Return dl.fac_ProfesionesSelectByPK( _
            IdProfesion _
            )
    End Function

    Public Sub fac_ProfesionesDeleteByPK _
            (ByVal IdProfesion As System.Int16 _
            )

        dl.fac_ProfesionesDeleteByPK( _
            IdProfesion _
            )
    End Sub

    Public Sub fac_ProfesionesInsert _
    (ByVal entidad As fac_Profesiones)

        dl.fac_ProfesionesInsert(entidad)
    End Sub

    Public Sub fac_ProfesionesUpdate _
    (ByVal entidad As fac_Profesiones)

        dl.fac_ProfesionesUpdate(entidad)
    End Sub




    Public Function fac_GestionesSelectAll() As DataTable
        Return dl.fac_GestionesSelectAll()
    End Function

    Public Function fac_GestionesSelectByPK _
            (ByVal IdGestion As System.Int32 _
            ) As fac_Gestiones

        Return dl.fac_GestionesSelectByPK( _
            IdGestion _
            )
    End Function

    Public Sub fac_GestionesDeleteByPK _
            (ByVal IdGestion As System.Int32 _
            )

        dl.fac_GestionesDeleteByPK( _
            IdGestion _
            )
    End Sub

    Public Sub fac_GestionesInsert _
    (ByVal entidad As fac_Gestiones)

        dl.fac_GestionesInsert(entidad)
    End Sub

    Public Sub fac_GestionesUpdate _
    (ByVal entidad As fac_Gestiones)

        dl.fac_GestionesUpdate(entidad)
    End Sub



    Public Function fac_CajasSelectAll() As DataTable
        Return dl.fac_CajasSelectAll()
    End Function

    Public Function fac_CajasSelectByPK(ByVal NumeroCaja As System.String) As fac_Cajas
        Return dl.fac_CajasSelectByPK(NumeroCaja)
    End Function

    Public Sub fac_CajasDeleteByPK(ByVal NumeroCaja As System.String)
        dl.fac_CajasDeleteByPK(NumeroCaja)
    End Sub

    Public Sub fac_CajasInsert(ByVal entidad As fac_Cajas)
        dl.fac_CajasInsert(entidad)
    End Sub

    Public Sub fac_CajasUpdate(ByVal entidad As fac_Cajas)
        dl.fac_CajasUpdate(entidad)
    End Sub

    Public Function fac_ClientesSelectAll() As DataTable
        Return dl.fac_ClientesSelectAll()
    End Function

    Public Function fac_ClientesSelectByPK(ByVal IdCliente As System.String) As fac_Clientes
        Return dl.fac_ClientesSelectByPK(IdCliente)
    End Function

    Public Sub fac_ClientesDeleteByPK(ByVal IdCliente As System.String)
        dl.fac_ClientesDeleteByPK(IdCliente)
    End Sub

    Public Sub fac_ClientesInsert(ByVal entidad As fac_Clientes)
        dl.fac_ClientesInsert(entidad)
    End Sub

    Public Sub fac_ClientesUpdate(ByVal entidad As fac_Clientes)
        dl.fac_ClientesUpdate(entidad)
    End Sub
    Public Function fac_ClientesPreciosSelectAll() As DataTable
        Return dl.fac_ClientesPreciosSelectAll()
    End Function

    Public Function fac_ClientesPreciosSelectByPK _
      (ByVal IdCliente As System.String _
      , ByVal IdProducto As System.String _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_ClientesPrecios

        Return dl.fac_ClientesPreciosSelectByPK( _
         IdCliente _
         , IdProducto _
         , IdDetalle _
         )
    End Function

    Public Sub fac_ClientesPreciosDeleteByPK _
      (ByVal IdCliente As System.String _
      , ByVal IdProducto As System.String _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.fac_ClientesPreciosDeleteByPK( _
         IdCliente _
         , IdProducto _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_ClientesPreciosInsert _
    (ByVal entidad As fac_ClientesPrecios)

        dl.fac_ClientesPreciosInsert(entidad)
    End Sub

    Public Sub fac_ClientesPreciosUpdate _
    (ByVal entidad As fac_ClientesPrecios)

        dl.fac_ClientesPreciosUpdate(entidad)
    End Sub

    Public Function fac_CotizacionesSelectAll() As DataTable
        Return dl.fac_CotizacionesSelectAll()
    End Function

    Public Function fac_CotizacionesSelectByPK(ByVal IdComprobante As Int32) As fac_Cotizaciones

        Return dl.fac_CotizacionesSelectByPK(IdComprobante)
    End Function

    Public Sub fac_CotizacionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.fac_CotizacionesDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub fac_CotizacionesInsert _
    (ByVal entidad As fac_Cotizaciones)

        dl.fac_CotizacionesInsert(entidad)
    End Sub

    Public Sub fac_CotizacionesUpdate _
    (ByVal entidad As fac_Cotizaciones)

        dl.fac_CotizacionesUpdate(entidad)
    End Sub



    Public Function fac_CotizacionesDetalleSelectAll() As DataTable
        Return dl.fac_CotizacionesDetalleSelectAll()
    End Function

    Public Function fac_CotizacionesDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_CotizacionesDetalle

        Return dl.fac_CotizacionesDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub fac_CotizacionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.fac_CotizacionesDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_CotizacionesDetalleInsert _
    (ByVal entidad As fac_CotizacionesDetalle)

        dl.fac_CotizacionesDetalleInsert(entidad)
    End Sub

    Public Sub fac_CotizacionesDetalleUpdate _
    (ByVal entidad As fac_CotizacionesDetalle)

        dl.fac_CotizacionesDetalleUpdate(entidad)
    End Sub

    Public Function fac_VentasSelectAll() As DataTable
        Return dl.fac_VentasSelectAll()
    End Function

    Public Function fac_VentasSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As fac_Ventas

        Return dl.fac_VentasSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub fac_VentasDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.fac_VentasDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub fac_VentasInsert _
    (ByVal entidad As fac_Ventas)

        dl.fac_VentasInsert(entidad)
    End Sub

    Public Sub fac_VentasUpdate _
    (ByVal entidad As fac_Ventas)

        dl.fac_VentasUpdate(entidad)
    End Sub



    Public Function fac_VentasDetalleSelectAll() As DataTable
        Return dl.fac_VentasDetalleSelectAll()
    End Function

    Public Function fac_VentasDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32) As fac_VentasDetalle

        Return dl.fac_VentasDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub fac_VentasDetalleDeleteByPK(ByVal IdComprobante As System.Int32, ByVal IdDetalle As System.Int32)

        dl.fac_VentasDetalleDeleteByPK(IdComprobante, IdDetalle)
    End Sub
    Public Sub fac_VentasDetalleInsert(ByVal entidad As fac_VentasDetalle)
        dl.fac_VentasDetalleInsert(entidad)
    End Sub
    Public Sub fac_VentasDetalleUpdate(ByVal entidad As fac_VentasDetalle)
        dl.fac_VentasDetalleUpdate(entidad)
    End Sub

    Public Function fac_NotasRemisionSelectAll() As DataTable
        Return dl.fac_NotasRemisionSelectAll()
    End Function

    Public Function fac_NotasRemisionSelectByPK(ByVal IdComprobante As System.Int32) As fac_NotasRemision

        Return dl.fac_NotasRemisionSelectByPK(IdComprobante)
    End Function

    Public Sub fac_NotasRemisionDeleteByPK(ByVal IdComprobante As System.Int32)

        dl.fac_NotasRemisionDeleteByPK(IdComprobante)
    End Sub

    Public Sub fac_NotasRemisionInsert(ByVal entidad As fac_NotasRemision)

        dl.fac_NotasRemisionInsert(entidad)
    End Sub

    Public Sub fac_NotasRemisionUpdate _
    (ByVal entidad As fac_NotasRemision)

        dl.fac_NotasRemisionUpdate(entidad)
    End Sub

    Public Function fac_NotasRemisionDetalleSelectAll() As DataTable
        Return dl.fac_NotasRemisionDetalleSelectAll()
    End Function

    Public Function fac_NotasRemisionDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_NotasRemisionDetalle

        Return dl.fac_NotasRemisionDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub fac_NotasRemisionDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.fac_NotasRemisionDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_NotasRemisionDetalleInsert _
    (ByVal entidad As fac_NotasRemisionDetalle)

        dl.fac_NotasRemisionDetalleInsert(entidad)
    End Sub

    Public Sub fac_NotasRemisionDetalleUpdate _
    (ByVal entidad As fac_NotasRemisionDetalle)

        dl.fac_NotasRemisionDetalleUpdate(entidad)
    End Sub

    Public Function fac_PedidosSelectAll() As DataTable
        Return dl.fac_PedidosSelectAll()
    End Function

    Public Function fac_PedidosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As fac_Pedidos

        Return dl.fac_PedidosSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub fac_PedidosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.fac_PedidosDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub fac_PedidosInsert _
    (ByVal entidad As fac_Pedidos)

        dl.fac_PedidosInsert(entidad)
    End Sub

    Public Sub fac_PedidosUpdate(ByVal entidad As fac_Pedidos)

        dl.fac_PedidosUpdate(entidad)
    End Sub

    Public Function fac_PedidosDetalleSelectAll() As DataTable
        Return dl.fac_PedidosDetalleSelectAll()
    End Function

    Public Sub fac_PedidosDetalleDeleteByPK(ByVal IdComprobante As System.Int32, ByVal IdDetalle As System.Int32)

        dl.fac_PedidosDetalleDeleteByPK(IdComprobante, IdDetalle)
    End Sub

    Public Sub fac_PedidosDetalleInsert(ByVal entidad As fac_PedidosDetalle)

        dl.fac_PedidosDetalleInsert(entidad)
    End Sub

    Public Sub fac_PedidosDetalleUpdate(ByVal entidad As fac_PedidosDetalle)

        dl.fac_PedidosDetalleUpdate(entidad)
    End Sub

    Public Function fac_PuntosVentaSelectAll() As DataTable
        Return dl.fac_PuntosVentaSelectAll()
    End Function

    Public Function fac_PuntosVentaSelectByPK(ByVal IdSucursal As System.Int32, ByVal IdPunto As System.Int32) As fac_PuntosVenta

        Return dl.fac_PuntosVentaSelectByPK(IdSucursal, IdPunto)
    End Function

    Public Sub fac_PuntosVentaDeleteByPK(ByVal IdSucursal As System.Int32, ByVal IdPunto As System.Int32)

        dl.fac_PuntosVentaDeleteByPK(IdSucursal, IdPunto)
    End Sub

    Public Sub fac_PuntosVentaInsert _
    (ByVal entidad As fac_PuntosVenta)

        dl.fac_PuntosVentaInsert(entidad)
    End Sub

    Public Sub fac_PuntosVentaUpdate _
    (ByVal entidad As fac_PuntosVenta)

        dl.fac_PuntosVentaUpdate(entidad)
    End Sub

    Public Function fac_SeriesDocumentosSelectAll() As DataTable
        Return dl.fac_SeriesDocumentosSelectAll()
    End Function

    Public Function fac_SeriesDocumentosSelectByPK _
      (ByVal IdPunto As System.Int32 _
      , ByVal IdComprobante As System.Int32 _
      , ByVal Serie As System.String _
      ) As fac_SeriesDocumentos

        Return dl.fac_SeriesDocumentosSelectByPK( _
         IdPunto _
         , IdComprobante _
         , Serie _
         )
    End Function

    Public Sub fac_SeriesDocumentosDeleteByPK _
      (ByVal IdPunto As System.Int32 _
      , ByVal IdComprobante As System.Int32 _
      , ByVal Serie As System.String _
      )

        dl.fac_SeriesDocumentosDeleteByPK( _
         IdPunto _
         , IdComprobante _
         , Serie _
         )
    End Sub

    Public Sub fac_SeriesDocumentosInsert(ByVal entidad As fac_SeriesDocumentos)
        dl.fac_SeriesDocumentosInsert(entidad)
    End Sub

    Public Sub fac_SeriesDocumentosUpdate(ByVal entidad As fac_SeriesDocumentos)
        dl.fac_SeriesDocumentosUpdate(entidad)
    End Sub


    Public Function fac_VendedoresSelectAll() As DataTable
        Return dl.fac_VendedoresSelectAll()
    End Function

    Public Function fac_VendedoresSelectByPK _
      (ByVal IdVendedor As System.String _
      ) As fac_Vendedores

        Return dl.fac_VendedoresSelectByPK( _
         IdVendedor _
         )
    End Function

    Public Sub fac_VendedoresDeleteByPK _
      (ByVal IdVendedor As System.String)

        dl.fac_VendedoresDeleteByPK( _
         IdVendedor _
         )
    End Sub

    Public Sub fac_VendedoresInsert _
    (ByVal entidad As fac_Vendedores)

        dl.fac_VendedoresInsert(entidad)
    End Sub

    Public Sub fac_VendedoresUpdate _
    (ByVal entidad As fac_Vendedores)

        dl.fac_VendedoresUpdate(entidad)
    End Sub
    Public Function fac_VendedoresDetalleSelectAll() As DataTable
        Return dl.fac_VendedoresDetalleSelectAll()
    End Function

    Public Function fac_VendedoresDetalleSelectByPK _
      (ByVal IdVendedor As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_VendedoresDetalle

        Return dl.fac_VendedoresDetalleSelectByPK( _
         IdVendedor _
         , IdDetalle _
         )
    End Function

    Public Sub fac_VendedoresDetalleDeleteByPK _
      (ByVal IdVendedor As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.fac_VendedoresDetalleDeleteByPK( _
         IdVendedor _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_VendedoresDetalleInsert _
    (ByVal entidad As fac_VendedoresDetalle)

        dl.fac_VendedoresDetalleInsert(entidad)
    End Sub

    Public Sub fac_VendedoresDetalleUpdate _
    (ByVal entidad As fac_VendedoresDetalle)

        dl.fac_VendedoresDetalleUpdate(entidad)
    End Sub

    Public Function fac_FormasPagoSelectAll() As DataTable
        Return dl.fac_FormasPagoSelectAll()
    End Function

    Public Function fac_FormasPagoSelectByPK _
      (ByVal IdFormaPago As System.Int32 _
      ) As fac_FormasPago

        Return dl.fac_FormasPagoSelectByPK( _
         IdFormaPago _
         )
    End Function

    Public Sub fac_FormasPagoDeleteByPK _
      (ByVal IdFormaPago As System.Int32 _
      )

        dl.fac_FormasPagoDeleteByPK( _
         IdFormaPago _
         )
    End Sub

    Public Sub fac_FormasPagoInsert _
    (ByVal entidad As fac_FormasPago)

        dl.fac_FormasPagoInsert(entidad)
    End Sub

    Public Sub fac_FormasPagoUpdate _
    (ByVal entidad As fac_FormasPago)

        dl.fac_FormasPagoUpdate(entidad)
    End Sub



    Public Function fac_RutasSelectAll() As DataTable
        Return dl.fac_RutasSelectAll()
    End Function

    Public Function fac_RutasSelectByPK _
      (ByVal IdRuta As System.Int32 _
      ) As fac_Rutas

        Return dl.fac_RutasSelectByPK( _
         IdRuta _
         )
    End Function

    Public Sub fac_RutasDeleteByPK _
      (ByVal IdRuta As System.Int32 _
      )

        dl.fac_RutasDeleteByPK( _
         IdRuta _
         )
    End Sub

    Public Sub fac_RutasInsert _
    (ByVal entidad As fac_Rutas)

        dl.fac_RutasInsert(entidad)
    End Sub

    Public Sub fac_RutasUpdate _
    (ByVal entidad As fac_Rutas)

        dl.fac_RutasUpdate(entidad)
    End Sub

    Public Function fac_NotasCreditoSelectAll() As DataTable
        Return dl.fac_NotasCreditoSelectAll()
    End Function

    Public Function fac_NotasCreditoSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_NotasCredito

        Return dl.fac_NotasCreditoSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub fac_NotasCreditoDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.fac_NotasCreditoDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_NotasCreditoInsert _
    (ByVal entidad As fac_NotasCredito)

        dl.fac_NotasCreditoInsert(entidad)
    End Sub

    Public Sub fac_NotasCreditoUpdate _
    (ByVal entidad As fac_NotasCredito)

        dl.fac_NotasCreditoUpdate(entidad)
    End Sub

    Public Function fac_RetencionSelectAll() As DataTable
        Return dl.fac_RetencionSelectAll()
    End Function

    Public Function fac_RetencionSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As fac_Retencion

        Return dl.fac_RetencionSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub fac_RetencionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.fac_RetencionDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub fac_RetencionInsert _
    (ByVal entidad As fac_Retencion)

        dl.fac_RetencionInsert(entidad)
    End Sub

    Public Sub fac_RetencionUpdate _
    (ByVal entidad As fac_Retencion)

        dl.fac_RetencionUpdate(entidad)
    End Sub

    Public Function fac_RetencionDetalleSelectAll() As DataTable
        Return dl.fac_RetencionDetalleSelectAll()
    End Function

    Public Function fac_RetencionDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_RetencionDetalle

        Return dl.fac_RetencionDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub fac_RetencionDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.fac_RetencionDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_RetencionDetalleInsert _
    (ByVal entidad As fac_RetencionDetalle)

        dl.fac_RetencionDetalleInsert(entidad)
    End Sub

    Public Sub fac_RetencionDetalleUpdate _
    (ByVal entidad As fac_RetencionDetalle)

        dl.fac_RetencionDetalleUpdate(entidad)
    End Sub

    Public Function fac_ValesSelectAll() As DataTable
        Return dl.fac_ValesSelectAll()
    End Function

    Public Function fac_ValesSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As fac_Vales

        Return dl.fac_ValesSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub fac_ValesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.fac_ValesDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub fac_ValesInsert _
    (ByVal entidad As fac_Vales)

        dl.fac_ValesInsert(entidad)
    End Sub

    Public Sub fac_ValesUpdate _
    (ByVal entidad As fac_Vales)

        dl.fac_ValesUpdate(entidad)
    End Sub

    Public Function fac_AperturaCajaSelectAll() As DataTable
        Return dl.fac_AperturaCajaSelectAll()
    End Function

    Public Function fac_AperturaCajaSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As fac_AperturaCaja

        Return dl.fac_AperturaCajaSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub fac_AperturaCajaDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.fac_AperturaCajaDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub fac_AperturaCajaInsert _
    (ByVal entidad As fac_AperturaCaja)

        dl.fac_AperturaCajaInsert(entidad)
    End Sub

    Public Sub fac_AperturaCajaUpdate _
    (ByVal entidad As fac_AperturaCaja)

        dl.fac_AperturaCajaUpdate(entidad)
    End Sub


    Public Function fac_VentasDetallePagoSelectAll() As DataTable
        Return dl.fac_VentasDetallePagoSelectAll()
    End Function

    Public Function fac_VentasDetallePagoSelectByPK _
      (ByVal IdFormaPago As System.Int32 _
      , ByVal IdComprobVenta As System.Int32 _
      ) As fac_VentasDetallePago

        Return dl.fac_VentasDetallePagoSelectByPK( _
         IdFormaPago _
         , IdComprobVenta _
         )
    End Function

    Public Sub fac_VentasDetallePagoDeleteByPK _
      (ByVal IdFormaPago As System.Int32 _
      , ByVal IdComprobVenta As System.Int32 _
      )

        dl.fac_VentasDetallePagoDeleteByPK( _
         IdFormaPago _
         , IdComprobVenta _
         )
    End Sub

    Public Sub fac_VentasDetallePagoInsert _
    (ByVal entidad As fac_VentasDetallePago)

        dl.fac_VentasDetallePagoInsert(entidad)
    End Sub

    Public Sub fac_VentasDetallePagoUpdate _
    (ByVal entidad As fac_VentasDetallePago)

        dl.fac_VentasDetallePagoUpdate(entidad)
    End Sub

    Public Function fac_CorteCajaArqueoSelectAll() As DataTable
        Return dl.fac_CorteCajaArqueoSelectAll()
    End Function

    Public Function fac_CorteCajaArqueoSelectByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal Fecha As System.DateTime _
      , ByVal IdDenominacion As System.Int32 _
      ) As fac_CorteCajaArqueo

        Return dl.fac_CorteCajaArqueoSelectByPK( _
         IdSucursal _
         , IdPunto _
         , Fecha _
         , IdDenominacion _
         )
    End Function

    Public Sub fac_CorteCajaArqueoDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal Fecha As System.DateTime _
      , ByVal IdDenominacion As System.Int32 _
      )

        dl.fac_CorteCajaArqueoDeleteByPK( _
         IdSucursal _
         , IdPunto _
         , Fecha _
         , IdDenominacion _
         )
    End Sub

    Public Sub fac_CorteCajaArqueoInsert _
    (ByVal entidad As fac_CorteCajaArqueo)

        dl.fac_CorteCajaArqueoInsert(entidad)
    End Sub

    Public Sub fac_CorteCajaArqueoUpdate _
    (ByVal entidad As fac_CorteCajaArqueo)

        dl.fac_CorteCajaArqueoUpdate(entidad)
    End Sub

    Public Function fac_CorteCajaChequesSelectAll() As DataTable
        Return dl.fac_CorteCajaChequesSelectAll()
    End Function

    Public Function fac_CorteCajaChequesSelectByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal Fecha As System.DateTime _
      , ByVal NumCheque As System.String _
      , ByVal IdBanco As System.Int32 _
      ) As fac_CorteCajaCheques

        Return dl.fac_CorteCajaChequesSelectByPK( _
         IdSucursal _
         , IdPunto _
         , Fecha _
         , NumCheque _
         , IdBanco _
         )
    End Function

    Public Sub fac_CorteCajaChequesDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal Fecha As System.DateTime _
      , ByVal NumCheque As System.String _
      , ByVal IdBanco As System.Int32 _
      )

        dl.fac_CorteCajaChequesDeleteByPK( _
         IdSucursal _
         , IdPunto _
         , Fecha _
         , NumCheque _
         , IdBanco _
         )
    End Sub

    Public Sub fac_CorteCajaChequesInsert _
    (ByVal entidad As fac_CorteCajaCheques)

        dl.fac_CorteCajaChequesInsert(entidad)
    End Sub

    Public Sub fac_CorteCajaChequesUpdate _
    (ByVal entidad As fac_CorteCajaCheques)

        dl.fac_CorteCajaChequesUpdate(entidad)
    End Sub



#End Region
#Region "Inventario"
    Public Function inv_CategoriasSelectAll() As DataTable
        Return dl.inv_CategoriasSelectAll()
    End Function

    Public Function inv_CategoriasSelectByPK _
      (ByVal IdCategoria As System.Int32 _
      ) As inv_Categorias

        Return dl.inv_CategoriasSelectByPK( _
         IdCategoria _
         )
    End Function

    Public Sub inv_CategoriasDeleteByPK _
      (ByVal IdCategoria As System.Int32 _
      )

        dl.inv_CategoriasDeleteByPK( _
         IdCategoria _
         )
    End Sub

    Public Sub inv_CategoriasInsert _
    (ByVal entidad As inv_Categorias)

        dl.inv_CategoriasInsert(entidad)
    End Sub

    Public Sub inv_CategoriasUpdate _
    (ByVal entidad As inv_Categorias)

        dl.inv_CategoriasUpdate(entidad)
    End Sub


    Public Function inv_BodegasSelectAll() As DataTable
        Return dl.inv_BodegasSelectAll()
    End Function

    Public Function inv_BodegasSelectByPK _
      (ByVal IdBodega As System.Int32 _
      ) As inv_Bodegas

        Return dl.inv_BodegasSelectByPK( _
         IdBodega _
         )
    End Function

    Public Sub inv_BodegasDeleteByPK _
      (ByVal IdBodega As System.Int32 _
      )

        dl.inv_BodegasDeleteByPK( _
         IdBodega _
         )
    End Sub

    Public Sub inv_BodegasInsert _
    (ByVal entidad As inv_Bodegas)

        dl.inv_BodegasInsert(entidad)
    End Sub

    Public Sub inv_BodegasUpdate _
    (ByVal entidad As inv_Bodegas)

        dl.inv_BodegasUpdate(entidad)
    End Sub

    Public Function inv_ExistenciasSelectAll() As DataTable
        Return dl.inv_ExistenciasSelectAll()
    End Function

    Public Function inv_ExistenciasSelectByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdBodega As System.Int32 _
      ) As inv_Existencias

        Return dl.inv_ExistenciasSelectByPK( _
         IdProducto _
         , IdBodega _
         )
    End Function

    Public Sub inv_ExistenciasDeleteByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdBodega As System.Int32 _
      )

        dl.inv_ExistenciasDeleteByPK( _
         IdProducto _
         , IdBodega _
         )
    End Sub

    Public Sub inv_ExistenciasInsert _
    (ByVal entidad As inv_Existencias)

        dl.inv_ExistenciasInsert(entidad)
    End Sub

    Public Sub inv_ExistenciasUpdate _
    (ByVal entidad As inv_Existencias)

        dl.inv_ExistenciasUpdate(entidad)
    End Sub

    Public Function inv_GruposSelectAll() As DataTable
        Return dl.inv_GruposSelectAll()
    End Function

    Public Function inv_GruposSelectByPK _
      (ByVal IdGrupo As System.Int32 _
      ) As inv_Grupos

        Return dl.inv_GruposSelectByPK( _
         IdGrupo _
         )
    End Function

    Public Sub inv_GruposDeleteByPK _
      (ByVal IdGrupo As System.Int32 _
      )

        dl.inv_GruposDeleteByPK( _
         IdGrupo _
         )
    End Sub

    Public Sub inv_GruposInsert _
    (ByVal entidad As inv_Grupos)

        dl.inv_GruposInsert(entidad)
    End Sub

    Public Sub inv_GruposUpdate _
    (ByVal entidad As inv_Grupos)

        dl.inv_GruposUpdate(entidad)
    End Sub

    Public Function inv_KardexSelectAll() As DataTable
        Return dl.inv_KardexSelectAll()
    End Function

    Public Function inv_KardexSelectByPK _
      (ByVal IdMovimiento As System.Int32 _
      ) As inv_Kardex

        Return dl.inv_KardexSelectByPK( _
         IdMovimiento _
         )
    End Function

    Public Sub inv_KardexDeleteByPK _
      (ByVal IdMovimiento As System.Int32 _
      )

        dl.inv_KardexDeleteByPK( _
         IdMovimiento _
         )
    End Sub

    Public Sub inv_KardexInsert _
    (ByVal entidad As inv_Kardex)

        dl.inv_KardexInsert(entidad)
    End Sub

    Public Sub inv_KardexUpdate _
    (ByVal entidad As inv_Kardex)

        dl.inv_KardexUpdate(entidad)
    End Sub

    Public Function inv_MarcasSelectAll() As DataTable
        Return dl.inv_MarcasSelectAll()
    End Function

    Public Function inv_MarcasSelectByPK _
      (ByVal IdMarca As System.Int32 _
      ) As inv_Marcas

        Return dl.inv_MarcasSelectByPK( _
         IdMarca _
         )
    End Function

    Public Sub inv_MarcasDeleteByPK _
      (ByVal IdMarca As System.Int32 _
      )

        dl.inv_MarcasDeleteByPK( _
         IdMarca _
         )
    End Sub

    Public Sub inv_MarcasInsert _
    (ByVal entidad As inv_Marcas)

        dl.inv_MarcasInsert(entidad)
    End Sub

    Public Sub inv_MarcasUpdate _
    (ByVal entidad As inv_Marcas)

        dl.inv_MarcasUpdate(entidad)
    End Sub

    Public Function inv_EntradasSelectAll() As DataTable
        Return dl.inv_EntradasSelectAll()
    End Function

    Public Function inv_EntradasSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As inv_Entradas

        Return dl.inv_EntradasSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub inv_EntradasDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.inv_EntradasDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub inv_EntradasInsert _
    (ByVal entidad As inv_Entradas)

        dl.inv_EntradasInsert(entidad)
    End Sub

    Public Sub inv_EntradasUpdate _
    (ByVal entidad As inv_Entradas)

        dl.inv_EntradasUpdate(entidad)
    End Sub

    Public Function inv_EntradasDetalleSelectAll() As DataTable
        Return dl.inv_EntradasDetalleSelectAll()
    End Function

    Public Function inv_EntradasDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_EntradasDetalle

        Return dl.inv_EntradasDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub inv_EntradasDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.inv_EntradasDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_EntradasDetalleInsert _
    (ByVal entidad As inv_EntradasDetalle)

        dl.inv_EntradasDetalleInsert(entidad)
    End Sub

    Public Sub inv_EntradasDetalleUpdate _
    (ByVal entidad As inv_EntradasDetalle)

        dl.inv_EntradasDetalleUpdate(entidad)
    End Sub

    Public Function inv_SalidasSelectAll() As DataTable
        Return dl.inv_SalidasSelectAll()
    End Function

    Public Function inv_SalidasSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As inv_Salidas

        Return dl.inv_SalidasSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub inv_SalidasDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.inv_SalidasDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub inv_SalidasInsert _
    (ByVal entidad As inv_Salidas)

        dl.inv_SalidasInsert(entidad)
    End Sub

    Public Sub inv_SalidasUpdate _
    (ByVal entidad As inv_Salidas)

        dl.inv_SalidasUpdate(entidad)
    End Sub

    Public Function inv_SalidasDetalleSelectAll() As DataTable
        Return dl.inv_SalidasDetalleSelectAll()
    End Function

    Public Function inv_SalidasDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_SalidasDetalle

        Return dl.inv_SalidasDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub inv_SalidasDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.inv_SalidasDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_SalidasDetalleInsert _
    (ByVal entidad As inv_SalidasDetalle)

        dl.inv_SalidasDetalleInsert(entidad)
    End Sub

    Public Sub inv_SalidasDetalleUpdate _
    (ByVal entidad As inv_SalidasDetalle)

        dl.inv_SalidasDetalleUpdate(entidad)
    End Sub

    Public Function inv_TrasladosSelectAll() As DataTable
        Return dl.inv_TrasladosSelectAll()
    End Function

    Public Function inv_TrasladosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As inv_Traslados

        Return dl.inv_TrasladosSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub inv_TrasladosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.inv_TrasladosDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub inv_TrasladosInsert _
    (ByVal entidad As inv_Traslados)

        dl.inv_TrasladosInsert(entidad)
    End Sub

    Public Sub inv_TrasladosUpdate _
    (ByVal entidad As inv_Traslados)

        dl.inv_TrasladosUpdate(entidad)
    End Sub

    Public Function inv_TrasladosDetalleSelectAll() As DataTable
        Return dl.inv_TrasladosDetalleSelectAll()
    End Function

    Public Function inv_TrasladosDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_TrasladosDetalle

        Return dl.inv_TrasladosDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub inv_TrasladosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.inv_TrasladosDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_TrasladosDetalleInsert _
    (ByVal entidad As inv_TrasladosDetalle)

        dl.inv_TrasladosDetalleInsert(entidad)
    End Sub

    Public Sub inv_TrasladosDetalleUpdate _
    (ByVal entidad As inv_TrasladosDetalle)

        dl.inv_TrasladosDetalleUpdate(entidad)
    End Sub


    Public Function inv_PreciosSelectAll() As DataTable
        Return dl.inv_PreciosSelectAll()
    End Function

    Public Function inv_PreciosSelectByPK _
      (ByVal IdPrecio As System.Int32 _
      ) As inv_Precios

        Return dl.inv_PreciosSelectByPK( _
         IdPrecio _
         )
    End Function

    Public Sub inv_PreciosDeleteByPK _
      (ByVal IdPrecio As System.Int32 _
      )

        dl.inv_PreciosDeleteByPK( _
         IdPrecio _
         )
    End Sub

    Public Sub inv_PreciosInsert _
    (ByVal entidad As inv_Precios)

        dl.inv_PreciosInsert(entidad)
    End Sub

    Public Sub inv_PreciosUpdate _
    (ByVal entidad As inv_Precios)

        dl.inv_PreciosUpdate(entidad)
    End Sub

    Public Function inv_ProductosSelectAll() As DataTable
        Return dl.inv_ProductosSelectAll()
    End Function

    Public Function inv_ProductosSelectByPK(ByVal IdProducto As String) As inv_Productos
        Return dl.inv_ProductosSelectByPK(IdProducto)
    End Function

    Public Sub inv_ProductosDeleteByPK(ByVal IdProducto As String)
        dl.inv_ProductosDeleteByPK(IdProducto)
    End Sub

    Public Sub inv_ProductosInsert(ByVal entidad As inv_Productos)
        dl.inv_ProductosInsert(entidad)
    End Sub

    Public Sub inv_ProductosUpdate(ByVal entidad As inv_Productos)
        dl.inv_ProductosUpdate(entidad)
    End Sub

    Public Function inv_ProductosDetalleSelectAll() As DataTable
        Return dl.inv_ProductosDetalleSelectAll()
    End Function

    Public Function inv_ProductosDetalleSelectByPK(ByVal IdProducto As System.String, ByVal IdDetalle As System.Int32) As inv_ProductosDetalle

        Return dl.inv_ProductosDetalleSelectByPK(IdProducto, IdDetalle)
    End Function

    Public Sub inv_ProductosDetalleDeleteByPK _
      (ByVal IdProducto As System.String _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.inv_ProductosDetalleDeleteByPK( _
         IdProducto _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_ProductosDetalleInsert _
    (ByVal entidad As inv_ProductosDetalle)

        dl.inv_ProductosDetalleInsert(entidad)
    End Sub

    Public Sub inv_ProductosDetalleUpdate _
    (ByVal entidad As inv_ProductosDetalle)

        dl.inv_ProductosDetalleUpdate(entidad)
    End Sub

    Public Function inv_ProductosPreciosSelectAll() As DataTable
        Return dl.inv_ProductosPreciosSelectAll()
    End Function

    Public Function inv_ProductosPreciosSelectByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdPrecio As System.Int32 _
      ) As inv_ProductosPrecios

        Return dl.inv_ProductosPreciosSelectByPK( _
         IdProducto _
         , IdPrecio _
         )
    End Function

    Public Sub inv_ProductosPreciosDeleteByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdPrecio As System.Int32 _
      )

        dl.inv_ProductosPreciosDeleteByPK( _
         IdProducto _
         , IdPrecio _
         )
    End Sub

    Public Sub inv_ProductosPreciosInsert _
    (ByVal entidad As inv_ProductosPrecios)

        dl.inv_ProductosPreciosInsert(entidad)
    End Sub

    Public Sub inv_ProductosPreciosUpdate _
    (ByVal entidad As inv_ProductosPrecios)

        dl.inv_ProductosPreciosUpdate(entidad)
    End Sub

    Public Function inv_SubGruposSelectAll() As DataTable
        Return dl.inv_SubGruposSelectAll()
    End Function

    Public Function inv_SubGruposSelectByPK(ByVal IdSubGrupo As System.Int32) As inv_SubGrupos
        Return dl.inv_SubGruposSelectByPK(IdSubGrupo)
    End Function

    Public Sub inv_SubGruposDeleteByPK(ByVal IdSubGrupo As System.Int32)
        dl.inv_SubGruposDeleteByPK(IdSubGrupo)
    End Sub

    Public Sub inv_SubGruposInsert(ByVal entidad As inv_SubGrupos)
        dl.inv_SubGruposInsert(entidad)
    End Sub

    Public Sub inv_SubGruposUpdate(ByVal entidad As inv_SubGrupos)

        dl.inv_SubGruposUpdate(entidad)
    End Sub

    Public Function inv_UbicacionesSelectAll() As DataTable
        Return dl.inv_UbicacionesSelectAll()
    End Function

    Public Function inv_UbicacionesSelectByPK _
      (ByVal IdUbicacion As System.Int32 _
      ) As inv_Ubicaciones

        Return dl.inv_UbicacionesSelectByPK( _
         IdUbicacion _
         )
    End Function

    Public Sub inv_UbicacionesDeleteByPK _
      (ByVal IdUbicacion As System.Int32 _
      )

        dl.inv_UbicacionesDeleteByPK( _
         IdUbicacion _
         )
    End Sub

    Public Sub inv_UbicacionesInsert _
    (ByVal entidad As inv_Ubicaciones)

        dl.inv_UbicacionesInsert(entidad)
    End Sub

    Public Sub inv_UbicacionesUpdate _
    (ByVal entidad As inv_Ubicaciones)

        dl.inv_UbicacionesUpdate(entidad)
    End Sub

    Public Function inv_UnidadesMedidaSelectAll() As DataTable
        Return dl.inv_UnidadesMedidaSelectAll()
    End Function

    Public Function inv_UnidadesMedidaSelectByPK _
      (ByVal IdUnidad As System.Int32 _
      ) As inv_UnidadesMedida

        Return dl.inv_UnidadesMedidaSelectByPK( _
         IdUnidad _
         )
    End Function

    Public Sub inv_UnidadesMedidaDeleteByPK _
      (ByVal IdUnidad As System.Int32 _
      )

        dl.inv_UnidadesMedidaDeleteByPK( _
         IdUnidad _
         )
    End Sub

    Public Sub inv_UnidadesMedidaInsert _
    (ByVal entidad As inv_UnidadesMedida)

        dl.inv_UnidadesMedidaInsert(entidad)
    End Sub

    Public Sub inv_UnidadesMedidaUpdate _
    (ByVal entidad As inv_UnidadesMedida)

        dl.inv_UnidadesMedidaUpdate(entidad)
    End Sub
    Public Function inv_FormulasSelectAll() As DataTable
        Return dl.inv_FormulasSelectAll()
    End Function

    Public Function inv_FormulasSelectByPK _
      (ByVal IdFormula As System.Int32 _
      ) As inv_Formulas

        Return dl.inv_FormulasSelectByPK( _
         IdFormula _
         )
    End Function

    Public Sub inv_FormulasDeleteByPK _
      (ByVal IdFormula As System.Int32 _
      )

        dl.inv_FormulasDeleteByPK( _
         IdFormula _
         )
    End Sub

    Public Sub inv_FormulasInsert _
    (ByVal entidad As inv_Formulas)

        dl.inv_FormulasInsert(entidad)
    End Sub

    Public Sub inv_FormulasUpdate _
    (ByVal entidad As inv_Formulas)

        dl.inv_FormulasUpdate(entidad)
    End Sub

    Public Function inv_FormulasDetalleSelectAll() As DataTable
        Return dl.inv_FormulasDetalleSelectAll()
    End Function

    Public Function inv_FormulasDetalleSelectByPK _
      (ByVal IdFormula As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_FormulasDetalle

        Return dl.inv_FormulasDetalleSelectByPK( _
         IdFormula _
         , IdDetalle _
         )
    End Function

    Public Sub inv_FormulasDetalleDeleteByPK _
      (ByVal IdFormula As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.inv_FormulasDetalleDeleteByPK( _
         IdFormula _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_FormulasDetalleInsert _
    (ByVal entidad As inv_FormulasDetalle)

        dl.inv_FormulasDetalleInsert(entidad)
    End Sub

    Public Sub inv_FormulasDetalleUpdate _
    (ByVal entidad As inv_FormulasDetalle)

        dl.inv_FormulasDetalleUpdate(entidad)
    End Sub



    Public Function inv_ProduccionesSelectAll() As DataTable
        Return dl.inv_ProduccionesSelectAll()
    End Function

    Public Function inv_ProduccionesSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As inv_Producciones

        Return dl.inv_ProduccionesSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub inv_ProduccionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.inv_ProduccionesDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub inv_ProduccionesInsert _
    (ByVal entidad As inv_Producciones)

        dl.inv_ProduccionesInsert(entidad)
    End Sub

    Public Sub inv_ProduccionesUpdate _
    (ByVal entidad As inv_Producciones)

        dl.inv_ProduccionesUpdate(entidad)
    End Sub

    Public Function inv_ProduccionesDetalleSelectAll() As DataTable
        Return dl.inv_ProduccionesDetalleSelectAll()
    End Function

    Public Function inv_ProduccionesDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_ProduccionesDetalle

        Return dl.inv_ProduccionesDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub inv_ProduccionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.inv_ProduccionesDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_ProduccionesDetalleInsert _
    (ByVal entidad As inv_ProduccionesDetalle)

        dl.inv_ProduccionesDetalleInsert(entidad)
    End Sub

    Public Sub inv_ProduccionesDetalleUpdate _
    (ByVal entidad As inv_ProduccionesDetalle)

        dl.inv_ProduccionesDetalleUpdate(entidad)
    End Sub

    Public Function inv_ProductosKitSelectAll() As DataTable
        Return dl.inv_ProductosKitSelectAll()
    End Function

    Public Function inv_ProductosKitSelectByPK _
      (ByVal IdKit As System.String _
      , ByVal IdDetalle As System.Int32 _
      , ByVal IdProducto As System.String _
      ) As inv_ProductosKit

        Return dl.inv_ProductosKitSelectByPK( _
         IdKit _
         , IdDetalle _
         , IdProducto _
         )
    End Function

    Public Sub inv_ProductosKitDeleteByPK _
      (ByVal IdKit As System.String _
      , ByVal IdDetalle As System.Int32 _
      , ByVal IdProducto As System.String _
      )

        dl.inv_ProductosKitDeleteByPK( _
         IdKit _
         , IdDetalle _
         , IdProducto _
         )
    End Sub

    Public Sub inv_ProductosKitInsert _
    (ByVal entidad As inv_ProductosKit)

        dl.inv_ProductosKitInsert(entidad)
    End Sub

    Public Sub inv_ProductosKitUpdate _
    (ByVal entidad As inv_ProductosKit)

        dl.inv_ProductosKitUpdate(entidad)
    End Sub


    Public Function inv_TomaFisicaSelectAll() As DataTable
        Return dl.inv_TomaFisicaSelectAll()
    End Function

    Public Function inv_TomaFisicaSelectByPK _
      (ByVal IdProducto As System.String _
      , ByVal Fecha As System.DateTime _
      , ByVal IdBodega As System.Int32 _
      ) As inv_TomaFisica

        Return dl.inv_TomaFisicaSelectByPK( _
         IdProducto _
         , Fecha _
         , IdBodega _
         )
    End Function

    Public Sub inv_TomaFisicaDeleteByPK _
      (ByVal IdProducto As System.String _
      , ByVal Fecha As System.DateTime _
      , ByVal IdBodega As System.Int32 _
      )

        dl.inv_TomaFisicaDeleteByPK( _
         IdProducto _
         , Fecha _
         , IdBodega _
         )
    End Sub

    Public Sub inv_TomaFisicaInsert _
    (ByVal entidad As inv_TomaFisica)

        dl.inv_TomaFisicaInsert(entidad)
    End Sub

    Public Sub inv_TomaFisicaUpdate _
    (ByVal entidad As inv_TomaFisica)

        dl.inv_TomaFisicaUpdate(entidad)
    End Sub
    Public Function inv_GastosProduccionSelectAll() As DataTable
        Return dl.inv_GastosProduccionSelectAll()
    End Function

    Public Function inv_GastosProduccionSelectByPK _
      (ByVal IdGasto As System.Int32 _
      ) As inv_GastosProduccion

        Return dl.inv_GastosProduccionSelectByPK( _
         IdGasto _
         )
    End Function

    Public Sub inv_GastosProduccionDeleteByPK _
      (ByVal IdGasto As System.Int32 _
      )

        dl.inv_GastosProduccionDeleteByPK( _
         IdGasto _
         )
    End Sub

    Public Sub inv_GastosProduccionInsert _
    (ByVal entidad As inv_GastosProduccion)

        dl.inv_GastosProduccionInsert(entidad)
    End Sub

    Public Sub inv_GastosProduccionUpdate _
    (ByVal entidad As inv_GastosProduccion)

        dl.inv_GastosProduccionUpdate(entidad)
    End Sub

    Public Function inv_ProduccionesGastosDetalleSelectAll() As DataTable
        Return dl.inv_ProduccionesGastosDetalleSelectAll()
    End Function

    Public Function inv_ProduccionesGastosDetalleSelectByPK _
      (ByVal IdProduccion As System.Int32 _
      , ByVal IdGasto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_ProduccionesGastosDetalle

        Return dl.inv_ProduccionesGastosDetalleSelectByPK( _
         IdProduccion _
         , IdGasto _
         , IdDetalle _
         )
    End Function

    Public Sub inv_ProduccionesGastosDetalleDeleteByPK _
      (ByVal IdProduccion As System.Int32 _
      , ByVal IdGasto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.inv_ProduccionesGastosDetalleDeleteByPK( _
         IdProduccion _
         , IdGasto _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_ProduccionesGastosDetalleInsert _
    (ByVal entidad As inv_ProduccionesGastosDetalle)

        dl.inv_ProduccionesGastosDetalleInsert(entidad)
    End Sub

    Public Sub inv_ProduccionesGastosDetalleUpdate _
    (ByVal entidad As inv_ProduccionesGastosDetalle)

        dl.inv_ProduccionesGastosDetalleUpdate(entidad)
    End Sub


    Public Function inv_ProduccionesGastosProductosSelectAll() As DataTable
        Return dl.inv_ProduccionesGastosProductosSelectAll()
    End Function

    Public Function inv_ProduccionesGastosProductosSelectByPK _
      (ByVal IdProduccion As System.Int32 _
      , ByVal IdProducto As System.String _
      , ByVal IdGasto As System.Int32 _
      ) As inv_ProduccionesGastosProductos

        Return dl.inv_ProduccionesGastosProductosSelectByPK( _
         IdProduccion _
         , IdProducto _
         , IdGasto _
         )
    End Function

    Public Sub inv_ProduccionesGastosProductosDeleteByPK _
      (ByVal IdProduccion As System.Int32 _
      , ByVal IdProducto As System.String _
      , ByVal IdGasto As System.Int32 _
      )

        dl.inv_ProduccionesGastosProductosDeleteByPK( _
         IdProduccion _
         , IdProducto _
         , IdGasto _
         )
    End Sub

    Public Sub inv_ProduccionesGastosProductosInsert _
    (ByVal entidad As inv_ProduccionesGastosProductos)

        dl.inv_ProduccionesGastosProductosInsert(entidad)
    End Sub

    Public Sub inv_ProduccionesGastosProductosUpdate _
    (ByVal entidad As inv_ProduccionesGastosProductos)

        dl.inv_ProduccionesGastosProductosUpdate(entidad)
    End Sub

    Public Function inv_ProductosMargenesSelectAll() As DataTable
        Return dl.inv_ProductosMargenesSelectAll()
    End Function

    Public Function inv_ProductosMargenesSelectByPK _
      (ByVal IdMargen As System.Int32 _
      ) As inv_ProductosMargenes

        Return dl.inv_ProductosMargenesSelectByPK( _
         IdMargen _
         )
    End Function

    Public Sub inv_ProductosMargenesDeleteByPK _
      (ByVal IdMargen As System.Int32 _
      )

        dl.inv_ProductosMargenesDeleteByPK( _
         IdMargen _
         )
    End Sub

    Public Sub inv_ProductosMargenesInsert _
    (ByVal entidad As inv_ProductosMargenes)

        dl.inv_ProductosMargenesInsert(entidad)
    End Sub

    Public Sub inv_ProductosMargenesUpdate _
    (ByVal entidad As inv_ProductosMargenes)

        dl.inv_ProductosMargenesUpdate(entidad)
    End Sub

    Public Function inv_CategoriasDetalleSelectAll() As DataTable
        Return dl.inv_CategoriasDetalleSelectAll()
    End Function

    Public Function inv_CategoriasDetalleSelectByPK _
      (ByVal IdCategoria As System.Int32 _
      , ByVal IdSucursal As System.Int32 _
      ) As inv_CategoriasDetalle

        Return dl.inv_CategoriasDetalleSelectByPK( _
         IdCategoria _
         , IdSucursal _
         )
    End Function

    Public Sub inv_CategoriasDetalleDeleteByPK _
      (ByVal IdCategoria As System.Int32 _
      , ByVal IdSucursal As System.Int32 _
      )

        dl.inv_CategoriasDetalleDeleteByPK( _
         IdCategoria _
         , IdSucursal _
         )
    End Sub

    Public Sub inv_CategoriasDetalleInsert _
    (ByVal entidad As inv_CategoriasDetalle)

        dl.inv_CategoriasDetalleInsert(entidad)
    End Sub

    Public Sub inv_CategoriasDetalleUpdate _
    (ByVal entidad As inv_CategoriasDetalle)

        dl.inv_CategoriasDetalleUpdate(entidad)
    End Sub



#End Region
#Region "CpC y CpP"
    Public Function cpc_DocumentosCobroSelectAll() As DataTable
        Return dl.cpc_DocumentosCobroSelectAll()
    End Function

    Public Function cpc_DocumentosCobroSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As cpc_DocumentosCobro

        Return dl.cpc_DocumentosCobroSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub cpc_DocumentosCobroDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.cpc_DocumentosCobroDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub cpc_DocumentosCobroInsert _
    (ByVal entidad As cpc_DocumentosCobro)

        dl.cpc_DocumentosCobroInsert(entidad)
    End Sub

    Public Sub cpc_DocumentosCobroUpdate _
    (ByVal entidad As cpc_DocumentosCobro)

        dl.cpc_DocumentosCobroUpdate(entidad)
    End Sub

    Public Function cpc_DocumentosCobroDetalleSelectAll() As DataTable
        Return dl.cpc_DocumentosCobroDetalleSelectAll()
    End Function

    Public Function cpc_DocumentosCobroDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As cpc_DocumentosCobroDetalle

        Return dl.cpc_DocumentosCobroDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub cpc_DocumentosCobroDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.cpc_DocumentosCobroDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub cpc_DocumentosCobroDetalleInsert _
    (ByVal entidad As cpc_DocumentosCobroDetalle)

        dl.cpc_DocumentosCobroDetalleInsert(entidad)
    End Sub

    Public Sub cpc_DocumentosCobroDetalleUpdate _
    (ByVal entidad As cpc_DocumentosCobroDetalle)

        dl.cpc_DocumentosCobroDetalleUpdate(entidad)
    End Sub


    Public Function cpc_AbonosSelectAll() As DataTable
        Return dl.cpc_AbonosSelectAll()
    End Function

    Public Function cpc_AbonosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As cpc_Abonos

        Return dl.cpc_AbonosSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub cpc_AbonosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.cpc_AbonosDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub cpc_AbonosInsert _
    (ByVal entidad As cpc_Abonos)

        dl.cpc_AbonosInsert(entidad)
    End Sub

    Public Sub cpc_AbonosUpdate _
    (ByVal entidad As cpc_Abonos)

        dl.cpc_AbonosUpdate(entidad)
    End Sub

    Public Function cpc_AbonosDetalleSelectAll() As DataTable
        Return dl.cpc_AbonosDetalleSelectAll()
    End Function

    Public Function cpc_AbonosDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As cpc_AbonosDetalle

        Return dl.cpc_AbonosDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub cpc_AbonosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.cpc_AbonosDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub cpc_AbonosDetalleInsert _
    (ByVal entidad As cpc_AbonosDetalle)

        dl.cpc_AbonosDetalleInsert(entidad)
    End Sub

    Public Sub cpc_AbonosDetalleUpdate _
    (ByVal entidad As cpc_AbonosDetalle)

        dl.cpc_AbonosDetalleUpdate(entidad)
    End Sub

    Public Function cpc_CargosSelectAll() As DataTable
        Return dl.cpc_CargosSelectAll()
    End Function

    Public Function cpc_CargosSelectByPK _
      (ByVal Iden As System.Int32 _
      ) As cpc_Cargos

        Return dl.cpc_CargosSelectByPK( _
         Iden _
         )
    End Function

    Public Sub cpc_CargosDeleteByPK _
      (ByVal Iden As System.Int32 _
      )

        dl.cpc_CargosDeleteByPK( _
         Iden _
         )
    End Sub

    Public Sub cpc_CargosInsert _
    (ByVal entidad As cpc_Cargos)

        dl.cpc_CargosInsert(entidad)
    End Sub

    Public Sub cpc_CargosUpdate _
    (ByVal entidad As cpc_Cargos)

        dl.cpc_CargosUpdate(entidad)
    End Sub

    Public Function cpc_CargosDetalleSelectAll() As DataTable
        Return dl.cpc_CargosDetalleSelectAll()
    End Function

    Public Function cpc_CargosDetalleSelectByPK _
      (ByVal Iden As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As cpc_CargosDetalle

        Return dl.cpc_CargosDetalleSelectByPK( _
         Iden _
         , IdDetalle _
         )
    End Function

    Public Sub cpc_CargosDetalleDeleteByPK _
      (ByVal Iden As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.cpc_CargosDetalleDeleteByPK( _
         Iden _
         , IdDetalle _
         )
    End Sub

    Public Sub cpc_CargosDetalleInsert _
    (ByVal entidad As cpc_CargosDetalle)

        dl.cpc_CargosDetalleInsert(entidad)
    End Sub

    Public Sub cpc_CargosDetalleUpdate _
    (ByVal entidad As cpc_CargosDetalle)

        dl.cpc_CargosDetalleUpdate(entidad)
    End Sub

    Public Function cpc_CobradoresSelectAll() As DataTable
        Return dl.cpc_CobradoresSelectAll()
    End Function

    Public Function cpc_CobradoresSelectByPK _
      (ByVal IdCobrador As System.Int32 _
      ) As cpc_Cobradores

        Return dl.cpc_CobradoresSelectByPK( _
         IdCobrador _
         )
    End Function

    Public Sub cpc_CobradoresDeleteByPK _
      (ByVal IdCobrador As System.Int32 _
      )

        dl.cpc_CobradoresDeleteByPK( _
         IdCobrador _
         )
    End Sub

    Public Sub cpc_CobradoresInsert _
    (ByVal entidad As cpc_Cobradores)

        dl.cpc_CobradoresInsert(entidad)
    End Sub

    Public Sub cpc_CobradoresUpdate _
    (ByVal entidad As cpc_Cobradores)

        dl.cpc_CobradoresUpdate(entidad)
    End Sub

    Public Function cpc_PoliticaAntiguedadSelectAll() As DataTable
        Return dl.cpc_PoliticaAntiguedadSelectAll()
    End Function

    Public Function cpc_PoliticaAntiguedadSelectByPK _
      (ByVal Id As System.Int16 _
      ) As cpc_PoliticaAntiguedad

        Return dl.cpc_PoliticaAntiguedadSelectByPK( _
         Id _
         )
    End Function

    Public Sub cpc_PoliticaAntiguedadDeleteByPK _
      (ByVal Id As System.Int16 _
      )

        dl.cpc_PoliticaAntiguedadDeleteByPK( _
         Id _
         )
    End Sub

    Public Sub cpc_PoliticaAntiguedadInsert _
    (ByVal entidad As cpc_PoliticaAntiguedad)

        dl.cpc_PoliticaAntiguedadInsert(entidad)
    End Sub

    Public Sub cpc_PoliticaAntiguedadUpdate _
    (ByVal entidad As cpc_PoliticaAntiguedad)

        dl.cpc_PoliticaAntiguedadUpdate(entidad)
    End Sub

    Public Function cpc_TiposMovSelectAll() As DataTable
        Return dl.cpc_TiposMovSelectAll()
    End Function

    Public Function cpc_TiposMovSelectByPK _
      (ByVal IdTipo As System.Int32 _
      ) As cpc_TiposMov

        Return dl.cpc_TiposMovSelectByPK( _
         IdTipo _
         )
    End Function

    Public Sub cpc_TiposMovDeleteByPK _
      (ByVal IdTipo As System.Int32 _
      )

        dl.cpc_TiposMovDeleteByPK( _
         IdTipo _
         )
    End Sub

    Public Sub cpc_TiposMovInsert _
    (ByVal entidad As cpc_TiposMov)

        dl.cpc_TiposMovInsert(entidad)
    End Sub

    Public Sub cpc_TiposMovUpdate _
    (ByVal entidad As cpc_TiposMov)

        dl.cpc_TiposMovUpdate(entidad)
    End Sub



    'cuentas por pagar
    Public Function cpp_AbonosSelectAll() As DataTable
        Return dl.cpp_AbonosSelectAll()
    End Function

    Public Function cpp_AbonosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As cpp_Abonos

        Return dl.cpp_AbonosSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub cpp_AbonosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.cpp_AbonosDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub cpp_AbonosInsert(ByVal entidad As cpp_Abonos)

        dl.cpp_AbonosInsert(entidad)
    End Sub

    Public Sub cpp_AbonosUpdate(ByVal entidad As cpp_Abonos)

        dl.cpp_AbonosUpdate(entidad)
    End Sub
    Public Function cpp_AbonosDetalleSelectAll() As DataTable
        Return dl.cpp_AbonosDetalleSelectAll()
    End Function

    Public Function cpp_AbonosDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As cpp_AbonosDetalle

        Return dl.cpp_AbonosDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub cpp_AbonosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.cpp_AbonosDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub cpp_AbonosDetalleInsert _
    (ByVal entidad As cpp_AbonosDetalle)

        dl.cpp_AbonosDetalleInsert(entidad)
    End Sub

    Public Sub cpp_AbonosDetalleUpdate _
    (ByVal entidad As cpp_AbonosDetalle)

        dl.cpp_AbonosDetalleUpdate(entidad)
    End Sub


    Public Function cpp_QuedanSelectAll() As DataTable
        Return dl.cpp_QuedanSelectAll()
    End Function

    Public Function cpp_QuedanSelectByPK(ByVal IdQuedan As System.Int32) As cpp_Quedan

        Return dl.cpp_QuedanSelectByPK(IdQuedan)
    End Function

    Public Sub cpp_QuedanDeleteByPK _
      (ByVal IdQuedan As System.Int32 _
      )

        dl.cpp_QuedanDeleteByPK( _
         IdQuedan _
         )
    End Sub

    Public Sub cpp_QuedanInsert(ByVal entidad As cpp_Quedan)

        dl.cpp_QuedanInsert(entidad)
    End Sub

    Public Sub cpp_QuedanUpdate(ByVal entidad As cpp_Quedan)

        dl.cpp_QuedanUpdate(entidad)
    End Sub

    Public Function cpp_QuedanDetalleSelectAll() As DataTable
        Return dl.cpp_QuedanDetalleSelectAll()
    End Function

    Public Function cpp_QuedanDetalleSelectByPK _
      (ByVal id_quedan As System.Int32, ByVal id_detalle As System.Int32) As cpp_QuedanDetalle

        Return dl.cpp_QuedanDetalleSelectByPK(id_quedan, id_detalle)
    End Function

    Public Sub cpp_QuedanDetalleDeleteByPK _
      (ByVal idQuedan As System.Int32, ByVal idDetalle As System.Int32)

        dl.cpp_QuedanDetalleDeleteByPK(idQuedan, idDetalle)
    End Sub

    Public Sub cpp_QuedanDetalleInsert _
    (ByVal entidad As cpp_QuedanDetalle)

        dl.cpp_QuedanDetalleInsert(entidad)
    End Sub

    Public Sub cpp_QuedanDetalleUpdate _
    (ByVal entidad As cpp_QuedanDetalle)

        dl.cpp_QuedanDetalleUpdate(entidad)
    End Sub


#End Region

#Region "ActivoFijo"
    Public Function acf_ActivosSelectAll() As DataTable
        Return dl.acf_ActivosSelectAll()
    End Function

    Public Function acf_ActivosSelectByPK _
      (ByVal IdActivo As System.Int32 _
      ) As acf_Activos

        Return dl.acf_ActivosSelectByPK( _
         IdActivo _
         )
    End Function

    Public Sub acf_ActivosDeleteByPK _
      (ByVal IdActivo As System.Int32 _
      )

        dl.acf_ActivosDeleteByPK( _
         IdActivo _
         )
    End Sub

    Public Sub acf_ActivosInsert _
    (ByVal entidad As acf_Activos)

        dl.acf_ActivosInsert(entidad)
    End Sub

    Public Sub acf_ActivosUpdate _
    (ByVal entidad As acf_Activos)

        dl.acf_ActivosUpdate(entidad)
    End Sub

    Public Function acf_ClasesSelectAll() As DataTable
        Return dl.acf_ClasesSelectAll()
    End Function

    Public Function acf_ClasesSelectByPK _
      (ByVal IdClase As System.Int16 _
      ) As acf_Clases

        Return dl.acf_ClasesSelectByPK( _
         IdClase _
         )
    End Function

    Public Sub acf_ClasesDeleteByPK _
      (ByVal IdClase As System.Int16 _
      )

        dl.acf_ClasesDeleteByPK( _
         IdClase _
         )
    End Sub

    Public Sub acf_ClasesInsert _
    (ByVal entidad As acf_Clases)

        dl.acf_ClasesInsert(entidad)
    End Sub

    Public Sub acf_ClasesUpdate _
    (ByVal entidad As acf_Clases)

        dl.acf_ClasesUpdate(entidad)
    End Sub

    Public Function acf_DepreciacionesAjustesSelectAll() As DataTable
        Return dl.acf_DepreciacionesAjustesSelectAll()
    End Function

    Public Function acf_DepreciacionesAjustesSelectByPK _
      (ByVal IdActivo As System.Int32 _
      , ByVal NumCuota As System.Int16 _
      ) As acf_DepreciacionesAjustes

        Return dl.acf_DepreciacionesAjustesSelectByPK( _
         IdActivo _
         , NumCuota _
         )
    End Function

    Public Sub acf_DepreciacionesAjustesDeleteByPK _
      (ByVal IdActivo As System.Int32 _
      , ByVal NumCuota As System.Int16 _
      )

        dl.acf_DepreciacionesAjustesDeleteByPK( _
         IdActivo _
         , NumCuota _
         )
    End Sub

    Public Sub acf_DepreciacionesAjustesInsert _
    (ByVal entidad As acf_DepreciacionesAjustes)

        dl.acf_DepreciacionesAjustesInsert(entidad)
    End Sub

    Public Sub acf_DepreciacionesAjustesUpdate _
    (ByVal entidad As acf_DepreciacionesAjustes)

        dl.acf_DepreciacionesAjustesUpdate(entidad)
    End Sub

    Public Function acf_EstadosSelectAll() As DataTable
        Return dl.acf_EstadosSelectAll()
    End Function

    Public Function acf_EstadosSelectByPK _
      (ByVal IdEstado As System.Byte _
      ) As acf_Estados

        Return dl.acf_EstadosSelectByPK( _
         IdEstado _
         )
    End Function

    Public Sub acf_EstadosDeleteByPK _
      (ByVal IdEstado As System.Byte _
      )

        dl.acf_EstadosDeleteByPK( _
         IdEstado _
         )
    End Sub

    Public Sub acf_EstadosInsert _
    (ByVal entidad As acf_Estados)

        dl.acf_EstadosInsert(entidad)
    End Sub

    Public Sub acf_EstadosUpdate _
    (ByVal entidad As acf_Estados)

        dl.acf_EstadosUpdate(entidad)
    End Sub

    Public Function acf_EstilosSelectAll() As DataTable
        Return dl.acf_EstilosSelectAll()
    End Function

    Public Function acf_EstilosSelectByPK _
      (ByVal IdEstilo As System.Int32 _
      ) As acf_Estilos

        Return dl.acf_EstilosSelectByPK( _
         IdEstilo _
         )
    End Function

    Public Sub acf_EstilosDeleteByPK _
      (ByVal IdEstilo As System.Int32 _
      )

        dl.acf_EstilosDeleteByPK( _
         IdEstilo _
         )
    End Sub

    Public Sub acf_EstilosInsert _
    (ByVal entidad As acf_Estilos)

        dl.acf_EstilosInsert(entidad)
    End Sub

    Public Sub acf_EstilosUpdate _
    (ByVal entidad As acf_Estilos)

        dl.acf_EstilosUpdate(entidad)
    End Sub

    Public Function acf_FallasSelectAll() As DataTable
        Return dl.acf_FallasSelectAll()
    End Function

    Public Function acf_FallasSelectByPK _
      (ByVal IdFalla As System.Int32 _
      ) As acf_Fallas

        Return dl.acf_FallasSelectByPK( _
         IdFalla _
         )
    End Function

    Public Sub acf_FallasDeleteByPK _
      (ByVal IdFalla As System.Int32 _
      )

        dl.acf_FallasDeleteByPK( _
         IdFalla _
         )
    End Sub

    Public Sub acf_FallasInsert _
    (ByVal entidad As acf_Fallas)

        dl.acf_FallasInsert(entidad)
    End Sub

    Public Sub acf_FallasUpdate _
    (ByVal entidad As acf_Fallas)

        dl.acf_FallasUpdate(entidad)
    End Sub

    Public Function acf_MantenimientosSelectAll() As DataTable
        Return dl.acf_MantenimientosSelectAll()
    End Function

    Public Function acf_MantenimientosSelectByPK _
      (ByVal Id As System.Int32 _
      ) As acf_Mantenimientos

        Return dl.acf_MantenimientosSelectByPK( _
         Id _
         )
    End Function

    Public Sub acf_MantenimientosDeleteByPK _
      (ByVal Id As System.Int32 _
      )

        dl.acf_MantenimientosDeleteByPK( _
         Id _
         )
    End Sub

    Public Sub acf_MantenimientosInsert _
    (ByVal entidad As acf_Mantenimientos)

        dl.acf_MantenimientosInsert(entidad)
    End Sub

    Public Sub acf_MantenimientosUpdate _
    (ByVal entidad As acf_Mantenimientos)

        dl.acf_MantenimientosUpdate(entidad)
    End Sub

    Public Function acf_MarcasSelectAll() As DataTable
        Return dl.acf_MarcasSelectAll()
    End Function

    Public Function acf_MarcasSelectByPK _
      (ByVal IdMarca As System.Int32 _
      ) As acf_Marcas

        Return dl.acf_MarcasSelectByPK( _
         IdMarca _
         )
    End Function

    Public Sub acf_MarcasDeleteByPK _
      (ByVal IdMarca As System.Int32 _
      )

        dl.acf_MarcasDeleteByPK( _
         IdMarca _
         )
    End Sub

    Public Sub acf_MarcasInsert _
    (ByVal entidad As acf_Marcas)

        dl.acf_MarcasInsert(entidad)
    End Sub

    Public Sub acf_MarcasUpdate _
    (ByVal entidad As acf_Marcas)

        dl.acf_MarcasUpdate(entidad)
    End Sub

    Public Function acf_ModelosSelectAll() As DataTable
        Return dl.acf_ModelosSelectAll()
    End Function

    Public Function acf_ModelosSelectByPK _
      (ByVal IdModelo As System.Int32 _
      ) As acf_Modelos

        Return dl.acf_ModelosSelectByPK( _
         IdModelo _
         )
    End Function

    Public Sub acf_ModelosDeleteByPK _
      (ByVal IdModelo As System.Int32 _
      )

        dl.acf_ModelosDeleteByPK( _
         IdModelo _
         )
    End Sub

    Public Sub acf_ModelosInsert _
    (ByVal entidad As acf_Modelos)

        dl.acf_ModelosInsert(entidad)
    End Sub

    Public Sub acf_ModelosUpdate _
    (ByVal entidad As acf_Modelos)

        dl.acf_ModelosUpdate(entidad)
    End Sub

    Public Function acf_MotivosBajaSelectAll() As DataTable
        Return dl.acf_MotivosBajaSelectAll()
    End Function

    Public Function acf_MotivosBajaSelectByPK _
      (ByVal IdMotivo As System.Int32 _
      ) As acf_MotivosBaja

        Return dl.acf_MotivosBajaSelectByPK( _
         IdMotivo _
         )
    End Function

    Public Sub acf_MotivosBajaDeleteByPK _
      (ByVal IdMotivo As System.Int32 _
      )

        dl.acf_MotivosBajaDeleteByPK( _
         IdMotivo _
         )
    End Sub

    Public Sub acf_MotivosBajaInsert _
    (ByVal entidad As acf_MotivosBaja)

        dl.acf_MotivosBajaInsert(entidad)
    End Sub

    Public Sub acf_MotivosBajaUpdate _
    (ByVal entidad As acf_MotivosBaja)

        dl.acf_MotivosBajaUpdate(entidad)
    End Sub

    Public Function acf_TecnicosSelectAll() As DataTable
        Return dl.acf_TecnicosSelectAll()
    End Function

    Public Function acf_TecnicosSelectByPK _
      (ByVal IdTecnico As System.Int32 _
      ) As acf_Tecnicos

        Return dl.acf_TecnicosSelectByPK( _
         IdTecnico _
         )
    End Function

    Public Sub acf_TecnicosDeleteByPK _
      (ByVal IdTecnico As System.Int32 _
      )

        dl.acf_TecnicosDeleteByPK( _
         IdTecnico _
         )
    End Sub

    Public Sub acf_TecnicosInsert _
    (ByVal entidad As acf_Tecnicos)

        dl.acf_TecnicosInsert(entidad)
    End Sub

    Public Sub acf_TecnicosUpdate _
    (ByVal entidad As acf_Tecnicos)

        dl.acf_TecnicosUpdate(entidad)
    End Sub

    Public Function acf_TiposAjusteSelectAll() As DataTable
        Return dl.acf_TiposAjusteSelectAll()
    End Function

    Public Function acf_TiposAjusteSelectByPK _
      (ByVal IdTipo As System.Byte _
      ) As acf_TiposAjuste

        Return dl.acf_TiposAjusteSelectByPK( _
         IdTipo _
         )
    End Function

    Public Sub acf_TiposAjusteDeleteByPK _
      (ByVal IdTipo As System.Byte _
      )

        dl.acf_TiposAjusteDeleteByPK( _
         IdTipo _
         )
    End Sub

    Public Sub acf_TiposAjusteInsert _
    (ByVal entidad As acf_TiposAjuste)

        dl.acf_TiposAjusteInsert(entidad)
    End Sub

    Public Sub acf_TiposAjusteUpdate _
    (ByVal entidad As acf_TiposAjuste)

        dl.acf_TiposAjusteUpdate(entidad)
    End Sub

    Public Function acf_TiposDepreciacionSelectAll() As DataTable
        Return dl.acf_TiposDepreciacionSelectAll()
    End Function

    Public Function acf_TiposDepreciacionSelectByPK _
      (ByVal IdTipo As System.Byte _
      ) As acf_TiposDepreciacion

        Return dl.acf_TiposDepreciacionSelectByPK( _
         IdTipo _
         )
    End Function

    Public Sub acf_TiposDepreciacionDeleteByPK _
      (ByVal IdTipo As System.Byte _
      )

        dl.acf_TiposDepreciacionDeleteByPK( _
         IdTipo _
         )
    End Sub

    Public Sub acf_TiposDepreciacionInsert _
    (ByVal entidad As acf_TiposDepreciacion)

        dl.acf_TiposDepreciacionInsert(entidad)
    End Sub

    Public Sub acf_TiposDepreciacionUpdate _
    (ByVal entidad As acf_TiposDepreciacion)

        dl.acf_TiposDepreciacionUpdate(entidad)
    End Sub

    Public Function acf_UbicacionesSelectAll() As DataTable
        Return dl.acf_UbicacionesSelectAll()
    End Function

    Public Function acf_UbicacionesSelectByPK _
      (ByVal IdUbicacion As System.Int16 _
      ) As acf_Ubicaciones

        Return dl.acf_UbicacionesSelectByPK( _
         IdUbicacion _
         )
    End Function

    Public Sub acf_UbicacionesDeleteByPK _
      (ByVal IdUbicacion As System.Int16 _
      )

        dl.acf_UbicacionesDeleteByPK( _
         IdUbicacion _
         )
    End Sub

    Public Sub acf_UbicacionesInsert _
    (ByVal entidad As acf_Ubicaciones)

        dl.acf_UbicacionesInsert(entidad)
    End Sub

    Public Sub acf_UbicacionesUpdate _
    (ByVal entidad As acf_Ubicaciones)

        dl.acf_UbicacionesUpdate(entidad)
    End Sub
    Public Function acf_TrasladosSelectAll() As DataTable
        Return dl.acf_TrasladosSelectAll()
    End Function

    Public Function acf_TrasladosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As acf_Traslados

        Return dl.acf_TrasladosSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub acf_TrasladosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.acf_TrasladosDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub acf_TrasladosInsert _
    (ByVal entidad As acf_Traslados)

        dl.acf_TrasladosInsert(entidad)
    End Sub

    Public Sub acf_TrasladosUpdate _
    (ByVal entidad As acf_Traslados)

        dl.acf_TrasladosUpdate(entidad)
    End Sub

    Public Function acf_TrasladosDetalleSelectAll() As DataTable
        Return dl.acf_TrasladosDetalleSelectAll()
    End Function

    Public Function acf_TrasladosDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As acf_TrasladosDetalle

        Return dl.acf_TrasladosDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub acf_TrasladosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.acf_TrasladosDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub acf_TrasladosDetalleInsert _
    (ByVal entidad As acf_TrasladosDetalle)

        dl.acf_TrasladosDetalleInsert(entidad)
    End Sub

    Public Sub acf_TrasladosDetalleUpdate _
    (ByVal entidad As acf_TrasladosDetalle)

        dl.acf_TrasladosDetalleUpdate(entidad)
    End Sub


#End Region

#Region "Planillas"

    Public Function pla_EmpleadosSelectByPK(ByVal IdEmpleado As Int32) As pla_Empleados

        Return dl.pla_EmpleadosSelectByPK(IdEmpleado)
    End Function

#End Region

#Region "Produccion"
    Public Function pro_ActividadesSelectAll() As DataTable
        Return dl.pro_ActividadesSelectAll()
    End Function

    Public Function pro_ActividadesSelectByPK _
      (ByVal IdActividad As System.Int32 _
      ) As pro_Actividades

        Return dl.pro_ActividadesSelectByPK( _
         IdActividad _
         )
    End Function

    Public Sub pro_ActividadesDeleteByPK _
      (ByVal IdActividad As System.Int32 _
      )

        dl.pro_ActividadesDeleteByPK( _
         IdActividad _
         )
    End Sub

    Public Sub pro_ActividadesInsert _
    (ByVal entidad As pro_Actividades)

        dl.pro_ActividadesInsert(entidad)
    End Sub

    Public Sub pro_ActividadesUpdate _
    (ByVal entidad As pro_Actividades)

        dl.pro_ActividadesUpdate(entidad)
    End Sub


    Public Function pro_OrdenesProduccionSelectAll() As DataTable
        Return dl.pro_OrdenesProduccionSelectAll()
    End Function

    Public Function pro_OrdenesProduccionSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As pro_OrdenesProduccion

        Return dl.pro_OrdenesProduccionSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub pro_OrdenesProduccionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.pro_OrdenesProduccionDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub pro_OrdenesProduccionInsert _
    (ByVal entidad As pro_OrdenesProduccion)

        dl.pro_OrdenesProduccionInsert(entidad)
    End Sub

    Public Sub pro_OrdenesProduccionUpdate _
    (ByVal entidad As pro_OrdenesProduccion)

        dl.pro_OrdenesProduccionUpdate(entidad)
    End Sub
    Public Function pro_OrdenesProduccionDetalleManoObraSelectAll() As DataTable
        Return dl.pro_OrdenesProduccionDetalleManoObraSelectAll()
    End Function

    Public Function pro_OrdenesProduccionDetalleManoObraSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_OrdenesProduccionDetalleManoObra

        Return dl.pro_OrdenesProduccionDetalleManoObraSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub pro_OrdenesProduccionDetalleManoObraDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.pro_OrdenesProduccionDetalleManoObraDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesProduccionDetalleManoObraInsert _
    (ByVal entidad As pro_OrdenesProduccionDetalleManoObra)

        dl.pro_OrdenesProduccionDetalleManoObraInsert(entidad)
    End Sub

    Public Sub pro_OrdenesProduccionDetalleManoObraUpdate _
    (ByVal entidad As pro_OrdenesProduccionDetalleManoObra)

        dl.pro_OrdenesProduccionDetalleManoObraUpdate(entidad)
    End Sub

    Public Function pro_EntradasProduccionSelectAll() As DataTable
        Return dl.pro_EntradasProduccionSelectAll()
    End Function

    Public Function pro_EntradasProduccionSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As pro_EntradasProduccion

        Return dl.pro_EntradasProduccionSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub pro_EntradasProduccionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.pro_EntradasProduccionDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub pro_EntradasProduccionInsert _
    (ByVal entidad As pro_EntradasProduccion)

        dl.pro_EntradasProduccionInsert(entidad)
    End Sub

    Public Sub pro_EntradasProduccionUpdate _
    (ByVal entidad As pro_EntradasProduccion)

        dl.pro_EntradasProduccionUpdate(entidad)
    End Sub

    Public Function pro_EntradasProduccionDetalleSelectAll() As DataTable
        Return dl.pro_EntradasProduccionDetalleSelectAll()
    End Function

    Public Function pro_EntradasProduccionDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_EntradasProduccionDetalle

        Return dl.pro_EntradasProduccionDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub pro_EntradasProduccionDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.pro_EntradasProduccionDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_EntradasProduccionDetalleInsert _
    (ByVal entidad As pro_EntradasProduccionDetalle)

        dl.pro_EntradasProduccionDetalleInsert(entidad)
    End Sub

    Public Sub pro_EntradasProduccionDetalleUpdate _
    (ByVal entidad As pro_EntradasProduccionDetalle)

        dl.pro_EntradasProduccionDetalleUpdate(entidad)
    End Sub

    Public Function pro_SalidasProduccionSelectAll() As DataTable
        Return dl.pro_SalidasProduccionSelectAll()
    End Function

    Public Function pro_SalidasProduccionSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As pro_SalidasProduccion

        Return dl.pro_SalidasProduccionSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub pro_SalidasProduccionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.pro_SalidasProduccionDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub pro_SalidasProduccionInsert _
    (ByVal entidad As pro_SalidasProduccion)

        dl.pro_SalidasProduccionInsert(entidad)
    End Sub

    Public Sub pro_SalidasProduccionUpdate _
    (ByVal entidad As pro_SalidasProduccion)

        dl.pro_SalidasProduccionUpdate(entidad)
    End Sub

    Public Function pro_SalidasProduccionDetalleSelectAll() As DataTable
        Return dl.pro_SalidasProduccionDetalleSelectAll()
    End Function

    Public Function pro_SalidasProduccionDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_SalidasProduccionDetalle

        Return dl.pro_SalidasProduccionDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub pro_SalidasProduccionDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.pro_SalidasProduccionDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_SalidasProduccionDetalleInsert _
    (ByVal entidad As pro_SalidasProduccionDetalle)

        dl.pro_SalidasProduccionDetalleInsert(entidad)
    End Sub

    Public Sub pro_SalidasProduccionDetalleUpdate _
    (ByVal entidad As pro_SalidasProduccionDetalle)

        dl.pro_SalidasProduccionDetalleUpdate(entidad)
    End Sub

    Public Function pro_OrdenesGasDirectosSelectAll() As DataTable
        Return dl.pro_OrdenesGasDirectosSelectAll()
    End Function

    Public Function pro_OrdenesGasDirectosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_OrdenesGasDirectos

        Return dl.pro_OrdenesGasDirectosSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub pro_OrdenesGasDirectosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.pro_OrdenesGasDirectosDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesGasDirectosInsert _
    (ByVal entidad As pro_OrdenesGasDirectos)

        dl.pro_OrdenesGasDirectosInsert(entidad)
    End Sub

    Public Sub pro_OrdenesGasDirectosUpdate _
    (ByVal entidad As pro_OrdenesGasDirectos)

        dl.pro_OrdenesGasDirectosUpdate(entidad)
    End Sub

    Public Function pro_OrdenesGasIndirectosSelectAll() As DataTable
        Return dl.pro_OrdenesGasIndirectosSelectAll()
    End Function

    Public Function pro_OrdenesGasIndirectosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_OrdenesGasIndirectos

        Return dl.pro_OrdenesGasIndirectosSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub pro_OrdenesGasIndirectosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.pro_OrdenesGasIndirectosDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesGasIndirectosInsert _
    (ByVal entidad As pro_OrdenesGasIndirectos)

        dl.pro_OrdenesGasIndirectosInsert(entidad)
    End Sub

    Public Sub pro_OrdenesGasIndirectosUpdate _
    (ByVal entidad As pro_OrdenesGasIndirectos)

        dl.pro_OrdenesGasIndirectosUpdate(entidad)
    End Sub

    Public Function pro_OrdenesEmpaqueSelectAll() As DataTable
        Return dl.pro_OrdenesEmpaqueSelectAll()
    End Function

    Public Function pro_OrdenesEmpaqueSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As pro_OrdenesEmpaque

        Return dl.pro_OrdenesEmpaqueSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub pro_OrdenesEmpaqueDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.pro_OrdenesEmpaqueDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueInsert _
    (ByVal entidad As pro_OrdenesEmpaque)

        dl.pro_OrdenesEmpaqueInsert(entidad)
    End Sub

    Public Sub pro_OrdenesEmpaqueUpdate _
    (ByVal entidad As pro_OrdenesEmpaque)

        dl.pro_OrdenesEmpaqueUpdate(entidad)
    End Sub

    Public Function pro_OrdenesEmpaqueDetalleEntradaSelectAll() As DataTable
        Return dl.pro_OrdenesEmpaqueDetalleEntradaSelectAll()
    End Function

    Public Function pro_OrdenesEmpaqueDetalleEntradaSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_OrdenesEmpaqueDetalleEntrada

        Return dl.pro_OrdenesEmpaqueDetalleEntradaSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub pro_OrdenesEmpaqueDetalleEntradaDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.pro_OrdenesEmpaqueDetalleEntradaDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleEntradaInsert _
    (ByVal entidad As pro_OrdenesEmpaqueDetalleEntrada)

        dl.pro_OrdenesEmpaqueDetalleEntradaInsert(entidad)
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleEntradaUpdate _
    (ByVal entidad As pro_OrdenesEmpaqueDetalleEntrada)

        dl.pro_OrdenesEmpaqueDetalleEntradaUpdate(entidad)
    End Sub

    Public Function pro_OrdenesEmpaqueDetalleSalidaSelectAll() As DataTable
        Return dl.pro_OrdenesEmpaqueDetalleSalidaSelectAll()
    End Function

    Public Function pro_OrdenesEmpaqueDetalleSalidaSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_OrdenesEmpaqueDetalleSalida

        Return dl.pro_OrdenesEmpaqueDetalleSalidaSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub pro_OrdenesEmpaqueDetalleSalidaDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.pro_OrdenesEmpaqueDetalleSalidaDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleSalidaInsert _
    (ByVal entidad As pro_OrdenesEmpaqueDetalleSalida)

        dl.pro_OrdenesEmpaqueDetalleSalidaInsert(entidad)
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleSalidaUpdate _
    (ByVal entidad As pro_OrdenesEmpaqueDetalleSalida)

        dl.pro_OrdenesEmpaqueDetalleSalidaUpdate(entidad)
    End Sub



#End Region


#Region "Eventos"
    Public Function eve_EventosSelectAll() As DataTable
        Return dl.eve_EventosSelectAll()
    End Function

    Public Function eve_EventosSelectByPK _
      (ByVal IdEvento As System.Int32 _
      ) As eve_Eventos

        Return dl.eve_EventosSelectByPK( _
         IdEvento _
         )
    End Function

    Public Sub eve_EventosDeleteByPK _
      (ByVal IdEvento As System.Int32 _
      )

        dl.eve_EventosDeleteByPK( _
         IdEvento _
         )
    End Sub

    Public Sub eve_EventosInsert _
    (ByVal entidad As eve_Eventos)

        dl.eve_EventosInsert(entidad)
    End Sub

    Public Sub eve_EventosUpdate _
    (ByVal entidad As eve_Eventos)

        dl.eve_EventosUpdate(entidad)
    End Sub

    Public Function eve_EventosDetalleSelectAll() As DataTable
        Return dl.eve_EventosDetalleSelectAll()
    End Function

    Public Function eve_EventosDetalleSelectByPK _
      (ByVal IdEvento As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As eve_EventosDetalle

        Return dl.eve_EventosDetalleSelectByPK( _
         IdEvento _
         , IdDetalle _
         )
    End Function

    Public Sub eve_EventosDetalleDeleteByPK _
      (ByVal IdEvento As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.eve_EventosDetalleDeleteByPK( _
         IdEvento _
         , IdDetalle _
         )
    End Sub

    Public Sub eve_EventosDetalleInsert _
    (ByVal entidad As eve_EventosDetalle)

        dl.eve_EventosDetalleInsert(entidad)
    End Sub

    Public Sub eve_EventosDetalleUpdate _
    (ByVal entidad As eve_EventosDetalle)

        dl.eve_EventosDetalleUpdate(entidad)
    End Sub

    Public Function eve_SeminariosSelectAll() As DataTable
        Return dl.eve_SeminariosSelectAll()
    End Function

    Public Function eve_SeminariosSelectByPK _
      (ByVal IdSeminario As System.Int32 _
      ) As eve_Seminarios

        Return dl.eve_SeminariosSelectByPK( _
         IdSeminario _
         )
    End Function

    Public Sub eve_SeminariosDeleteByPK _
      (ByVal IdSeminario As System.Int32 _
      )

        dl.eve_SeminariosDeleteByPK( _
         IdSeminario _
         )
    End Sub

    Public Sub eve_SeminariosInsert _
    (ByVal entidad As eve_Seminarios)

        dl.eve_SeminariosInsert(entidad)
    End Sub

    Public Sub eve_SeminariosUpdate _
    (ByVal entidad As eve_Seminarios)

        dl.eve_SeminariosUpdate(entidad)
    End Sub
    Public Function eve_InscripcionesSelectAll() As DataTable
        Return dl.eve_InscripcionesSelectAll()
    End Function

    Public Function eve_InscripcionesSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As eve_Inscripciones

        Return dl.eve_InscripcionesSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub eve_InscripcionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.eve_InscripcionesDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub eve_InscripcionesInsert _
    (ByVal entidad As eve_Inscripciones)

        dl.eve_InscripcionesInsert(entidad)
    End Sub

    Public Sub eve_InscripcionesUpdate _
    (ByVal entidad As eve_Inscripciones)

        dl.eve_InscripcionesUpdate(entidad)
    End Sub

    Public Function eve_InscripcionesDetalleSelectAll() As DataTable
        Return dl.eve_InscripcionesDetalleSelectAll()
    End Function

    Public Function eve_InscripcionesDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As eve_InscripcionesDetalle

        Return dl.eve_InscripcionesDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub eve_InscripcionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.eve_InscripcionesDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub eve_InscripcionesDetalleInsert _
    (ByVal entidad As eve_InscripcionesDetalle)

        dl.eve_InscripcionesDetalleInsert(entidad)
    End Sub

    Public Sub eve_InscripcionesDetalleUpdate _
    (ByVal entidad As eve_InscripcionesDetalle)

        dl.eve_InscripcionesDetalleUpdate(entidad)
    End Sub

    Public Function eve_EventosDetallePagoSelectAll() As DataTable
        Return dl.eve_EventosDetallePagoSelectAll()
    End Function

    Public Function eve_EventosDetallePagoSelectByPK _
      (ByVal IdEvento As System.Int32 _
      , ByVal NumCuota As System.Int32 _
      ) As eve_EventosDetallePago

        Return dl.eve_EventosDetallePagoSelectByPK( _
         IdEvento _
         , NumCuota _
         )
    End Function

    Public Sub eve_EventosDetallePagoDeleteByPK _
      (ByVal IdEvento As System.Int32 _
      , ByVal NumCuota As System.Int32 _
      )

        dl.eve_EventosDetallePagoDeleteByPK( _
         IdEvento _
         , NumCuota _
         )
    End Sub

    Public Sub eve_EventosDetallePagoInsert _
    (ByVal entidad As eve_EventosDetallePago)

        dl.eve_EventosDetallePagoInsert(entidad)
    End Sub

    Public Sub eve_EventosDetallePagoUpdate _
    (ByVal entidad As eve_EventosDetallePago)

        dl.eve_EventosDetallePagoUpdate(entidad)
    End Sub

    Public Function eve_CentrosCostosSelectAll() As DataTable
        Return dl.eve_CentrosCostosSelectAll()
    End Function

    Public Function eve_CentrosCostosSelectByPK _
      (ByVal IdCentro As System.Int32 _
      ) As eve_CentrosCostos

        Return dl.eve_CentrosCostosSelectByPK( _
         IdCentro _
         )
    End Function

    Public Sub eve_CentrosCostosDeleteByPK _
      (ByVal IdCentro As System.Int32 _
      )

        dl.eve_CentrosCostosDeleteByPK( _
         IdCentro _
         )
    End Sub

    Public Sub eve_CentrosCostosInsert _
    (ByVal entidad As eve_CentrosCostos)

        dl.eve_CentrosCostosInsert(entidad)
    End Sub

    Public Sub eve_CentrosCostosUpdate _
    (ByVal entidad As eve_CentrosCostos)

        dl.eve_CentrosCostosUpdate(entidad)
    End Sub


    Public Function eve_AbonosSelectAll() As DataTable
        Return dl.eve_AbonosSelectAll()
    End Function

    Public Function eve_AbonosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As eve_Abonos

        Return dl.eve_AbonosSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub eve_AbonosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.eve_AbonosDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub eve_AbonosInsert _
    (ByVal entidad As eve_Abonos)

        dl.eve_AbonosInsert(entidad)
    End Sub

    Public Sub eve_AbonosUpdate _
    (ByVal entidad As eve_Abonos)

        dl.eve_AbonosUpdate(entidad)
    End Sub

    Public Function eve_AbonosDetalleSelectAll() As DataTable
        Return dl.eve_AbonosDetalleSelectAll()
    End Function

    Public Function eve_AbonosDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As eve_AbonosDetalle

        Return dl.eve_AbonosDetalleSelectByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Function

    Public Sub eve_AbonosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        dl.eve_AbonosDetalleDeleteByPK( _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub eve_AbonosDetalleInsert _
    (ByVal entidad As eve_AbonosDetalle)

        dl.eve_AbonosDetalleInsert(entidad)
    End Sub

    Public Sub eve_AbonosDetalleUpdate _
    (ByVal entidad As eve_AbonosDetalle)

        dl.eve_AbonosDetalleUpdate(entidad)
    End Sub

    Public Function eve_ParticipantesSelectAll() As DataTable
        Return dl.eve_ParticipantesSelectAll()
    End Function

    Public Function eve_ParticipantesSelectByPK _
      (ByVal NumInscripcion As System.String _
      ) As eve_Participantes

        Return dl.eve_ParticipantesSelectByPK( _
         NumInscripcion _
         )
    End Function

    Public Sub eve_ParticipantesDeleteByPK _
      (ByVal NumInscripcion As System.String _
      )

        dl.eve_ParticipantesDeleteByPK( _
         NumInscripcion _
         )
    End Sub

    Public Sub eve_ParticipantesInsert _
    (ByVal entidad As eve_Participantes)

        dl.eve_ParticipantesInsert(entidad)
    End Sub

    Public Sub eve_ParticipantesUpdate _
    (ByVal entidad As eve_Participantes)

        dl.eve_ParticipantesUpdate(entidad)
    End Sub


    Public Function eve_TransaccionesEventosSelectAll() As DataTable
        Return dl.eve_TransaccionesEventosSelectAll()
    End Function

    Public Function eve_TransaccionesEventosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As eve_TransaccionesEventos

        Return dl.eve_TransaccionesEventosSelectByPK( _
         IdComprobante _
         )
    End Function

    Public Sub eve_TransaccionesEventosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        dl.eve_TransaccionesEventosDeleteByPK( _
         IdComprobante _
         )
    End Sub

    Public Sub eve_TransaccionesEventosInsert _
    (ByVal entidad As eve_TransaccionesEventos)

        dl.eve_TransaccionesEventosInsert(entidad)
    End Sub

    Public Sub eve_TransaccionesEventosUpdate _
    (ByVal entidad As eve_TransaccionesEventos)

        dl.eve_TransaccionesEventosUpdate(entidad)
    End Sub

    Public Function eve_InscripcionesParticipantesSelectAll() As DataTable
        Return dl.eve_InscripcionesParticipantesSelectAll()
    End Function

    Public Function eve_InscripcionesParticipantesSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Participante As System.String _
      , ByVal IdSeminario As System.Int32 _
      ) As eve_InscripcionesParticipantes

        Return dl.eve_InscripcionesParticipantesSelectByPK( _
         IdComprobante _
         , Participante _
         , IdSeminario _
         )
    End Function

    Public Sub eve_InscripcionesParticipantesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Participante As System.String _
      , ByVal IdSeminario As System.Int32 _
      )

        dl.eve_InscripcionesParticipantesDeleteByPK( _
         IdComprobante _
         , Participante _
         , IdSeminario _
         )
    End Sub

    Public Sub eve_InscripcionesParticipantesInsert _
    (ByVal entidad As eve_InscripcionesParticipantes)

        dl.eve_InscripcionesParticipantesInsert(entidad)
    End Sub

    Public Sub eve_InscripcionesParticipantesUpdate _
    (ByVal entidad As eve_InscripcionesParticipantes)

        dl.eve_InscripcionesParticipantesUpdate(entidad)
    End Sub



#End Region
End Class
