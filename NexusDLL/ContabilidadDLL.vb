﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common
Imports NexusELL.TableEntities

Public Class ContabilidadDLL
    Dim db As Database
    Dim fd As FuncionesDLL
    Dim objTabla As TableData


    Public Sub New(ByVal strConexion As String)

        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db
        fd = New FuncionesDLL(strConexion)
        objTabla = New TableData(strConexion)
    End Sub

    Public Function Get_FormulasFinancieras(ByVal IdFormula As Integer) As DataTable
		Dim sSQL As String = "select * from con_FormulasFinancieras "
		If IdFormula = -999 Then
			sSQL &= "where 1=2"
		Else
			sSQL &= "where IdFormula=" & IdFormula & " ORDER BY IdFormula,Id "
		End If
		Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
	End Function
	Public Function rptIndicador(ByVal IdFormula As Integer, ByVal mes As Integer, ByVal Anio As Integer, ByVal Tipo As Integer) As Decimal
		Return db.ExecuteScalar("con_spIndicador", IdFormula, mes, Anio, Tipo)
	End Function
	Public Function rptPartidaReporteByPk(ByVal IdPartida As Integer) As DataTable
        Return db.ExecuteDataSet("con_spPartidaReporteByPk", IdPartida).Tables(0)
    End Function
    Public Function rptPartidaReporteByFecha(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sTipoPar As String, ByVal Desde As String, ByVal Hasta As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spPartidaReporteByFecha", dFechaIni, dFechaFin, sTipoPar, Desde, Hasta, IdSucursal).Tables(0)
    End Function
    Public Function rptPartidasConcentradas(ByVal TipoPartida As String, ByVal DesdeFecha As Date, ByVal HastaFecha As Date, ByVal desde As String, ByVal hasta As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spConcentrarPartidas", TipoPartida, DesdeFecha, HastaFecha, desde, hasta, IdSucursal).Tables(0)
    End Function
    Public Function rptMovimientosCuenta(ByVal dtFechaIni As Date, ByVal dtFechaFin As Date, ByVal IdCuenta As String, ByVal IdCentro As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spMovimientosCuenta", dtFechaIni, dtFechaFin, IdCuenta, IdCentro, IdSucursal).Tables(0)
    End Function
    Public Function pre_PresupuestosSelectRepeated(ByVal pAnio As Integer) As Integer
        Dim sql As String = "Select count(*) from con_Presupuestos where periodo=" & pAnio
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function pre_ObtenerDetallePresupuesto(ByVal Id As Integer) As DataTable
        Dim sql As String = "select cc.IdCuenta, cc.Nombre, Mes01= isnull(d.mes01,0), Mes02= isnull(d.mes02,0), Mes03= isnull(d.mes03,0), Mes04= isnull(d.mes04,0), Mes05= isnull(d.mes05,0)"
        sql += " , Mes06= isnull(d.mes06,0), Mes07= isnull(d.mes07,0), Mes08= isnull(d.mes08,0), Mes09= isnull(d.mes09,0), Mes10= isnull(d.mes10,0), Mes11= isnull(d.mes11,0), Mes12= isnull(d.mes12,0) "
        sql += " from con_cuentas cc left join con_Presupuestosdetalle d on cc.idcuenta = d.idcuenta and d.idpresupuesto=" & Id
        sql += " where cc.estransaccional=1 "
        sql += " order by cc.idcuenta "

        If Id = -1 Then
            sql = " select * from con_Presupuestosdetalle where idpresupuesto= -111111"
        End If
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function InsertaDetallePresupuesto(ByRef list As List(Of con_PresupuestosDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""
        Try
            For Each entDetalle In list
                objTabla.con_PresupuestosDetalleInsert(entDetalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            cn.Close()
            msj = ex.Message()
        Finally
            cn.Close()
        End Try

        Return msj
    End Function
    Public Function ModificaDetallePresupuesto(ByVal IdPresupuesto As Integer, ByRef list As List(Of con_PresupuestosDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""
        Try
            Dim sql As String = "delete con_PresupuestosDetalle where idpresupuesto = " & IdPresupuesto
            db.ExecuteNonQuery(tran, CommandType.Text, sql)

            For Each entDetalle In list
                objTabla.con_PresupuestosDetalleInsert(entDetalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            cn.Close()
            msj = ex.Message()
        Finally
            cn.Close()
        End Try

        Return msj
    End Function

    Public Function pre_ObtenerPresupuestosCTA() As DataTable
        Dim sSQL As String = "	select pre.IdPresupuesto"
        sSQL += ",pre.Periodo,pre.Comentario,  pre.CreadoPor, pre.FechaHoraCreacion"
        sSQL += "	from con_Presupuestos pre "
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function con_ReportePresupuesto(ByVal Ejercicio As Integer, ByVal Mes As Integer) As DataTable
        Return db.ExecuteDataSet("pre_PresupuestosVsCuentasCTA", Ejercicio, Mes).Tables(0)
    End Function
    Public Function rptTotalesPartida(ByVal dtFechaInic As Date, ByVal dtFechaFin As Date, ByVal TipoPartida As String, ByVal EnDescuadre As Boolean, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spTotalesPartida", dtFechaInic, dtFechaFin, TipoPartida, EnDescuadre, IdSucursal).Tables(0)
    End Function
    
    Public Function rptLibroDiario(IdSucursal As Integer, ByVal Desde As Date, ByVal Hasta As Date, Tipo As Integer) As DataTable
        If Tipo = 0 Then
            Return db.ExecuteDataSet("con_spLibroDiario", Desde, Hasta, IdSucursal).Tables(0)
        Else
            Return db.ExecuteDataSet("con_spLibroMayorFecha", IdSucursal, Desde, Hasta).Tables(0)
        End If

    End Function
    Public Function rptLibroDiarioMayorDetalle(ByVal dtFechaIni As Date, ByVal dtFechaFin As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSetWait("con_spLibroDiarioMayorDetalle", dtFechaIni, dtFechaFin, IdSucursal).Tables(0)
    End Function
    Public Function rptLibroDiarioMayor(ByVal DesdeFecha As Date, ByVal HastaFecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSetWait("con_spLibroDiarioMayor", DesdeFecha, HastaFecha, IdSucursal).Tables(0)
    End Function
    Public Function rptLibroMayor(ByVal dtFechaIni As Date, ByVal dtFechaFin As Date, ByVal IdSucursal As Integer, Tipo As Integer) As DataTable
        If Tipo = 0 Then
            Return db.ExecuteDataSetWait("con_spLibroMayor", dtFechaIni, dtFechaFin, IdSucursal).Tables(0)
        Else
            Return db.ExecuteDataSetWait("con_spLibroMayorConsolidado", IdSucursal, dtFechaIni, dtFechaFin).Tables(0)
        End If

    End Function
    
    Public Function rptLibroAuxiliarDetalle(ByVal dtFechaIni As Date, ByVal dtFechaFin As Date, ByVal sIdCtaDesde As String, ByVal sIdCtaHasta As String) As DataTable
        Return db.ExecuteDataSet("con_spLibroAuxiliarDetalle", dtFechaIni, dtFechaFin, sIdCtaDesde, sIdCtaHasta).Tables(0)
    End Function
    Public Function rptLibroAuxiliarMayor(ByVal dtFechaIni As Date, ByVal dtFechaFin As Date, ByVal sIdCtaDesde As String, ByVal sIdCtaHasta As String, ByVal IdSucursal As Integer) As DataTable

        Dim dcCommand3 As System.Data.Common.DbCommand
        dcCommand3 = db.GetStoredProcCommand("con_spLibroAuxiliarMayor", dtFechaIni, dtFechaFin, sIdCtaDesde, sIdCtaHasta, IdSucursal)
        dcCommand3.CommandTimeout = 60000

        Return db.ExecuteDataSet(dcCommand3).Tables(0)
    End Function
    Public Function VerificaAccesoCuentas(ByVal IdUsuario As String, ByVal DesdeCuenta As String, ByVal HastaCuenta As String) As Boolean
        Return db.ExecuteScalar("con_spVerificaAccesoCuentas", IdUsuario, DesdeCuenta, HastaCuenta)
    End Function
    Public Function ObtenerCuentasExcluidas(ByVal IdUsuario As String) As DataTable
        Dim sql As String = "select ce.IdCuenta, Nombre, Motivo, Excluir = convert(bit,1) from con_CuentasExcluidas ce inner join Con_Cuentas cc on ce.IdCuenta = cc.IdCuenta "
        sql &= "Where IdUsuario = '" & IdUsuario & "' order by ce.IdCuenta"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function ObtenerCuentasAExcluir(ByVal DesdeCuenta As String, ByVal HastaCuenta As String) As DataTable
        Dim sql As String = "select IdCuenta, Nombre, Motivo = convert(varchar(150),''), Excluir = convert(bit,0) from con_Cuentas Where IdCuenta >='" & DesdeCuenta & "' and IdCuenta <= '" & HastaCuenta & "' order by IdCuenta"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Sub InsertarCuentaExcluida(ByVal IdUsuario As String, ByVal Cuenta As String, ByVal Motivo As String, ByVal CreadoPor As String, ByVal Excluir As Boolean)
        Dim sql As String = "delete con_CuentasExcluidas where IdUsuario = '" & IdUsuario & "' And IdCuenta = '" & Cuenta & "'"
        db.ExecuteNonQuery(CommandType.Text, sql)
        If Excluir Then
            sql = "insert con_CuentasExcluidas (IdUsuario, IdCuenta, Motivo, CreadoPor) values('" & IdUsuario & "', '" & Cuenta & "', '" & Motivo & "', '" & CreadoPor & "')"
            db.ExecuteNonQuery(CommandType.Text, sql)
        End If
    End Sub
    Public Function ConsultaPartidas(ByVal Usuario As String, ByVal Fecha As String) As DataSet

        Dim sql As String = "SELECT P.IdPartida, P.IdTipo, P.Numero, P.Fecha,p.IdSucursal, left(P.Concepto,100) Concepto, P.CreadoPor, P.FechaHoraCreacion FROM con_Partidas P inner join adm_UsuariosSucursales s on p.idsucursal=s.idsucursal"
        sql &= String.Format(" Where p.Fecha >='{0}'", Fecha)
        sql &= String.Format(" and s.IdUsuario ='{0}'", Usuario)

        sql &= String.Format(" Order By p.IdPartida DESC")
        sql &= String.Format(" ; SELECT pd.* FROM con_PartidasDetalle pd INNER JOIN con_Partidas cp ON pd.IdPartida = cp.IdPartida inner join adm_UsuariosSucursales s on cp.idsucursal=s.idsucursal Where cp.Fecha >= '{0}'", Fecha)
        sql &= String.Format(" and s.IdUsuario ='{0}'", Usuario)

        Dim dsPa As DataSet = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("Partidas_Detalle" _
                                   , dsPa.Tables(0).Columns("IdPartida") _
                                   , dsPa.Tables(1).Columns("IdPartida"))
        dsPa.Relations.Add(dr)
        Return dsPa
    End Function
    Public Function rptCuentasLibroAuxiliarMayorExcel(ByVal HastaCuenta As String, ByVal DesdeCuenta As String, ByVal DesdeFecha As Date, ByVal IdSucursal As Integer) As DataTable

        Dim dcCommand3 As System.Data.Common.DbCommand
        dcCommand3 = db.GetStoredProcCommand("con_spCuentasLibroAuxiliarMayorExcel", DesdeCuenta, HastaCuenta, DesdeFecha, IdSucursal)
        dcCommand3.CommandTimeout = 60000

        Return db.ExecuteDataSet(dcCommand3).Tables(0)
    End Function
    Public Function rptDetalleCuentasMayorLibroAuxiliarExcel(ByVal IdCuenta As String, ByVal IdCuentaMayor As String) As DataTable

        Dim dcCommand3 As System.Data.Common.DbCommand
        dcCommand3 = db.GetStoredProcCommand("con_spObtieneCuentasMayor", IdCuenta, IdCuentaMayor)
        dcCommand3.CommandTimeout = 60000


        Return db.ExecuteDataSet(dcCommand3).Tables(0)
    End Function

    Public Function rptPartidasLibroAuxiliarMayorExcel(ByVal IdCuenta As String, ByVal DesdeFecha As Date, ByVal HastaFecha As Date, ByVal IdSucursal As Integer) As DataTable

        Dim dcCommand3 As System.Data.Common.DbCommand
        dcCommand3 = db.GetStoredProcCommand("con_spPartidasLibroAuxiliarMayorExcel", IdCuenta, DesdeFecha, HastaFecha, IdSucursal)
        dcCommand3.CommandTimeout = 60000

        Return db.ExecuteDataSet(dcCommand3).Tables(0)
    End Function
    Public Function rptConcentracionMayor(ByVal FechaIni As Date, ByVal FechaFin As Date, ByVal IdTipoPartida As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spConcentracionMayor", FechaIni, FechaFin, IdTipoPartida, IdSucursal).Tables(0)
    End Function
    Public Function rptCatalogoConSaldos(ByVal mes As Integer, ByVal ejercicio As Integer) As DataTable
        Return db.ExecuteDataSet("con_spRepCatalogoConSaldos", mes, ejercicio).Tables(0)
    End Function
    Public Function rptComparacion2Meses(ByVal iMes1 As Integer, ByVal iAnio1 As Integer, ByVal iMes2 As Integer, ByVal iAnio2 As Integer, ByVal iNivel As Integer, ByVal iTipoSaldos As Integer, ByVal sDesdeCta As String, ByVal sHastaCta As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spComparacion2Meses", iMes1, iAnio1, iMes2, iAnio2, iNivel, iTipoSaldos, sDesdeCta, sHastaCta, IdSucursal).Tables(0)
    End Function
    Public Function rptComparacionAnual(ByVal iEjercicio As Integer, ByVal iNivel As Integer, ByVal iTipoSaldos As Integer, ByVal sDesdeCta As String, ByVal sHastaCta As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spComparacionAnual", iEjercicio, iNivel, iTipoSaldos, sDesdeCta, sHastaCta, IdSucursal).Tables(0)
    End Function
    Public Function con_ObtenerTransaccionesCaja(ByVal IdSucursal As Integer, ByVal Fecha As DateTime) As DataTable
        Dim sql As String
        sql = "Select * from con_Transacciones where IdSucursal=" + IdSucursal.ToString + " and Fecha=" + Chr(39) + Fecha.Year.ToString + Fecha.Month.ToString.PadLeft(2, "0") + Fecha.Day.ToString.PadLeft(2, "0") + Chr(39) + " order by FechaHoraCreacion"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function con_ObtenerDatoCierre(ByVal Fecha As DateTime) As DataTable
        Dim sql As String
        sql = "Select * from con_CierreCaja where Fecha=" + Chr(39) + Fecha.Year.ToString + Fecha.Month.ToString.PadLeft(2, "0") + Fecha.Day.ToString.PadLeft(2, "0") + Chr(39)
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function con_ObtenerLiqCaja(ByVal Fecha As DateTime, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdUsuario As String, ByVal Cierre As Boolean) As DataTable
        Return db.ExecuteDataSet("con_spLiquiCaja", Fecha, IdSucursal, IdPunto, IdUsuario, Cierre).Tables(0)
    End Function

#Region "Presupestos"
    Public Function pre_PresupuestosPorSucursal(ByVal pAnio As Integer) As DataTable
        Return db.ExecuteDataSet("pre_PresupuestoPorSucursal", pAnio).Tables(0)
    End Function

    Public Function pre_PresupuestosPorSucursal(ByVal pAnio As Integer, ByVal pMes As Integer) As DataTable
        Return db.ExecuteDataSet("pre_PresupuestoPorSucursalMes", pAnio, pMes).Tables(0)
    End Function

    Public Function pre_PresupuestoAnualDepto(ByVal pAnio As Integer, ByVal pDepto As Integer) As DataTable
        Return db.ExecuteDataSet("pre_PresupuestoAnualDepto", pAnio, pDepto).Tables(0)
    End Function

    Public Function pre_PresupuestoAnualSuc(ByVal pAnio As Integer, ByVal pSuc As Integer) As DataTable
        Return db.ExecuteDataSet("pre_PresupuestoAnualSuc", pAnio, pSuc).Tables(0)
    End Function

    Public Function pre_PresupuestoCabecera(ByVal pAnio As Integer, ByVal pSuc As Integer, ByVal pDepto As Integer, ByVal pCentroCosto As Integer) As DataTable
        Return db.ExecuteDataSet("pre_PresupuestoCabecera", pAnio, pSuc, pDepto, pCentroCosto).Tables(0)
    End Function

    Public Function pre_PresupuestoCabeceraMes(ByVal pAnio As Integer, ByVal pSuc As Integer, ByVal pDepto As Integer, ByVal pCentroCosto As Integer, ByVal pMes As Integer) As DataTable
        Return db.ExecuteDataSet("pre_PresupuestoCabeceraMes", pAnio, pSuc, pDepto, pCentroCosto, pMes).Tables(0)
    End Function

    Public Function pre_PresupuestoCuentaConSaldos(ByVal pAnio As Integer, ByVal pIdPresupuesto As Integer) As DataTable
        Return db.ExecuteDataSet("pre_PresupuestoCuentaConSaldos", pIdPresupuesto, pAnio).Tables(0)
    End Function

    Public Function pre_PresupuestoCuentaConSaldosMes(ByVal pAnio As Integer, ByVal pMes As Integer, ByVal pIdPresupuesto As Integer) As DataTable
        Return db.ExecuteDataSet("pre_PresupuestoCuentaConSaldosMes", pIdPresupuesto, pAnio, pMes).Tables(0)
    End Function

    Public Function pre_PresupuestosVsCuentas(ByVal pAnio As Integer, ByVal pMes As Integer, ByVal IdSucursal As Integer, ByVal IdDepto As Integer, ByVal IdCentro As String) As DataTable
        Return db.ExecuteDataSet("pre_PresupuestosVsCuentas", pAnio, pMes, IdSucursal, IdDepto, IdCentro).Tables(0)
    End Function
    Public Function pre_ObtenerPresupuestos() As DataTable
        Dim sSQL As String = "	select pre.IdPresupuestoPk,pre.IdSucursalFk,s.Nombre as Sucursal,pre.IdDepartamentoFk"
        sSQL += ",d.Nombre as Departamento,pre.IdCentroCostosFk,cc.Nombre as CentroCostos,pre.Anio,pre.Comentario,pre.FechaCreacion"
        sSQL += ",(select count(IdPresupuestoCuentaPk) from [pre_PresupuestoCuenta] where IdPresupuestoFk = pre.IdPresupuestoPk) AS Cuentas"
        sSQL += "	from pre_Presupuestos pre INNER JOIN [adm_Sucursales] s ON s.IdSucursal = pre.IdSucursalFk "
        sSQL += " INNER JOIN [pre_DepartamentosSucursal] d ON d.IdDepartamentoSucursalPk = pre.IdDepartamentoFk"
        sSQL += " INNER JOIN [con_CentrosCosto] cc ON cc.IdCentro = pre.IdCentroCostosFk	"
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
#End Region

	'REPORTES FINANCIEROS
	Public Function con_rptRazonesFinancieras(ByVal mes As Integer, ByVal ejercicio As Integer, ByVal IdSucursal As Integer) As DataTable
		Return db.ExecuteDataSet("con_spIndicadorMensual", mes, ejercicio, IdSucursal).Tables(0)
	End Function

	Public Function con_rptBalanceComprobacionResumen(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spBalanceComprobCuentaRes", Mes, Ejercicio, Tipo, IdSucursal).Tables(0)
    End Function
    Public Function con_rptBalanceComprobacionDetalle(ByVal mes As Integer, ByVal ejercicio As Integer, ByVal tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spBalanceComprobCuentaDet", mes, ejercicio, tipo, IdSucursal).Tables(0)
    End Function
    Public Function con_rptBalanceComprobacionReporte(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal Nivel As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spBalanceComprobReporte", Mes, Ejercicio, Nivel, Tipo, IdSucursal).Tables(0)
    End Function
    Public Function con_rptEstadoResultados(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal Nivel As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spEstadoResultados", Mes, Ejercicio, Nivel, Tipo, IdSucursal).Tables(0)
    End Function
    Public Function con_rptBalanceGeneralResumen(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spBalanceGeneralCuentaRes", Mes, Ejercicio, Tipo, IdSucursal).Tables(0)
    End Function
    Public Function con_rptBalanceGeneralDetalle(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spBalanceGeneralCuentaDet", Mes, Ejercicio, Tipo, IdSucursal).Tables(0)
    End Function
    Public Function con_rptAnexos(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spAnexos", Mes, Ejercicio, Tipo, IdSucursal).Tables(0)
    End Function
    Public Function con_rptBalanceGeneralNIIF(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spBalanceGeneralNIIF", Mes, Ejercicio, tipo, IdSucursal).Tables(0)
    End Function
    Public Function rptEstadoResultadoNIIF(ByVal mes As Integer, ByVal ejercicio As Integer, ByVal tipo As Integer, ByVal Nivel As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spEstadoResutaldoNIIF", mes, ejercicio, tipo, Nivel, IdSucursal).Tables(0)
    End Function

    Public Function rptFlujoEfectivoNIIF(ByVal mes As Integer, ByVal ejercicio As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spEstadoFlujoEfectivoNIIF", mes, ejercicio, IdSucursal).Tables(0)
    End Function
    Public Function rptCambioPatrimonio(ByVal ejercicio As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spCambioPatrimonio", ejercicio, IdSucursal).Tables(0)
    End Function


    Public Function con_CargarCuentasDeMayor() As DataTable
        Return db.ExecuteDataSet("con_spCuentaMayorFlujoEfectivo").Tables(0)
    End Function
    Public Function con_CargarActividadesFlujoEfectivo() As DataTable
        Dim sSQL As String
        sSQL = "select Id Id,Nombre from adm_Listas  where Modulo='CON_EF' order by Id"
        Dim cmd As DbCommand = db.GetSqlStringCommand(sSQL)
        Return db.ExecuteDataSet(cmd).Tables(0)
    End Function
    Public Function con_AniosCerradosFlujoEfectivo(ByVal Ejercicio As Integer) As DataTable
        Dim sSQL As String
        sSQL = "SELECT Mes,Ejercicio FROM con_Cierres WHERE Mes = 12 AND Ejercicio <= " + Ejercicio.ToString
        Dim cmd As DbCommand = db.GetSqlStringCommand(sSQL)
        Return db.ExecuteDataSet(cmd).Tables(0)
    End Function
    Public Function con_ObtenerDetalleReporteNIIF(ByVal IdReporte As Integer) As DataTable
        Dim sql As String = "SELECT * FROM con_NIIFConceptos WHERE IdReporte = " & IdReporte.ToString + " ORDER BY NumeroLinea "
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Function con_ObtenerUltimoIdDetalleNIIF(ByVal IdReporte As Integer) As Integer
        Dim sql As String = "SELECT max(IdDetalle) FROM con_NIIFConceptos WHERE IdReporte = " & IdReporte.ToString
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    'OTROS PROCESOS
    Public Function GetCuentasContables() As DataTable
        Dim sSQl As String
        sSQl = "select IdCuenta, Nombre from con_cuentas order by IdCuenta"
        Return db.ExecuteDataSet(CommandType.Text, sSQl).Tables(0)
    End Function
    Public Function ObtenerCuentaLiquidadora() As String
        Dim sql As String = "select top 1 IdCuenta, Nombre from con_cuentas where Naturaleza = 6 and EsTransaccional = 1"
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function ObtieneDataConta(ByVal FechaI As Date, ByVal FechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Dim sSQL As String = ""
        sSQL &= "select con.IdPartida, con.IdTipo [Tipo de Partida], con.Numero [Numero de Partida], con.Fecha, con.Concepto [Concepto de partida], Det.IdCuenta, Cue.Nombre [Nombre de Cuenta], "
        sSQL &= "Det.Referencia, Det.Concepto, Det.Debe, Det.Haber, Che.Numero [Numero de Cheque], Che.AnombreDe [Cheque a favor de], "
        sSQL &= "Che.Valor, Det.CreadoPor, Det.FechaHoraCreacion, Con.ModificadoPor, Con.FechaHoraModificacion "
        sSQL &= "from con_partidas con "
        sSQL &= "inner join con_partidasdetalle det on con.idpartida = det.idpartida "
        sSQL &= "inner join con_cuentas cue on Det.IdCuenta = Cue.IdCuenta "
        sSQL &= "left join ban_cheques che on con.idpartida = che.idpartida "
        sSQL &= "where con.Fecha >= @FechaI and con.Fecha < dateadd(dd,1,@FechaF) "
        If IdSucursal > -1 Then
            sSQL &= " and con.IdSucursal= @IdSucursal "
        End If
        Dim cmd As DbCommand = db.GetSqlStringCommand(sSQL)
        db.AddInParameter(cmd, "@FechaI", DbType.Date, FechaI)
        db.AddInParameter(cmd, "@FechaF", DbType.Date, FechaF)
        db.AddInParameter(cmd, "@IdSucursal", DbType.Int32, IdSucursal)

        Return db.ExecuteDataSet(cmd).Tables(0)
    End Function
    Public Sub ObtienePeriodoContable(ByRef Mes As Integer, ByRef Ejercicio As Integer, Optional ByRef MinFecha As Date = Nothing)
        Dim dt As DataTable = db.ExecuteDataSet(CommandType.Text, "select top 1 Mes, Ejercicio from con_cierres order by Ejercicio desc, Mes desc").Tables(0)
        If dt.Rows.Count > 0 Then
            Mes = dt.Rows(0).Item("Mes")
            Ejercicio = dt.Rows(0).Item("Ejercicio")
        Else
            Mes = 0
            Ejercicio = 0
        End If
        If MinFecha <> Nothing Then
            MinFecha = db.ExecuteScalar(CommandType.Text, "SELECT isnull(min(Fecha), getdate()) FROM con_Partidas")
        End If
    End Sub
    Public Function IsCuentaContable(ByVal IdCuenta As String) As Boolean
        Dim sql As String = "select 1 from con_cuentas where IdCuenta = @IdCuenta"

        Dim cmd As DbCommand = db.GetSqlStringCommand(sql)
        db.AddInParameter(cmd, "IdCuenta", DbType.String, IdCuenta)
        Dim num_reg As Integer = db.ExecuteScalar(cmd)
        If num_reg = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function InsertaPartidas(ByRef entPartidas As con_Partidas, ByRef ListPartidasDetalle As List(Of con_PartidasDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""
        Try
            TranInsertaPartidas(entPartidas, ListPartidasDetalle, tran)
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            cn.Close()
            msj = ex.Message()
        Finally
            cn.Close()
        End Try

        Return msj

    End Function

    Sub ChequeaCuentas()
        Dim sSQL = "update con_cuentas set EsTransaccional = 1; "
        sSQL &= "UPDATE con_Cuentas SET EsTransaccional = 0 WHERE IdCuenta IN (SELECT IdCuentaMayor FROM con_cuentas);"
        db.ExecuteNonQuery(CommandType.Text, sSQL)

    End Sub

    Sub EliminarCatalgoParaCarga()
        Dim sSQL = "DELETE con_Cuentas WHERE IdCuenta NOT IN(SELECT IdCuenta FROM con_PartidasDetalle GROUP BY IdCuenta)"
        db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Sub

    Sub OrdenaCatalagoConDelimitador(ByVal delimitador As String)
        Dim sSQL = "" &
            "UPDATE con_Cuentas SET Nivel = (len(IdCuenta) - DATALENGTH(replace(IdCuenta, '" & delimitador & "', ''))) / len('" & delimitador & "') " &
            "UPDATE con_Cuentas SET IdCuentaMayor = substring(IdCuenta, 1, (len(IdCuenta) - patindex('%" & delimitador & "%', reverse(IdCuenta)))) WHERE len(IdCuenta) > 1" &
            "UPDATE con_Cuentas SET IdCuentaMayor = '' WHERE len(IdCuenta) = 1"
        db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Sub

    Public Sub TranInsertaPartidas(ByRef entPartidas As con_Partidas, ByRef ListPartidasDetalle As List(Of con_PartidasDetalle), ByVal tran As DbTransaction)

        'Obtiene id de Partida
        entPartidas.IdPartida = fd.ObtenerUltimoId("CON_PARTIDAS", "IdPartida") + 1

        'obtiene el numumero de partida
        entPartidas.Numero = fd.GetNumPartida(entPartidas.IdTipo, entPartidas.Fecha, tran)
        objTabla.con_PartidasInsert(entPartidas, tran)

        For Each entDetalle In ListPartidasDetalle
            entDetalle.IdPartida = entPartidas.IdPartida
            objTabla.con_PartidasDetalleInsert(entDetalle, tran)
        Next

    End Sub

    Public Function UpdatePartidas(ByRef entPartidas As con_Partidas, ByRef ListPartidasDetalle As List(Of con_PartidasDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""
        Try
            objTabla.con_PartidasUpdate(entPartidas, tran)
            Dim sql As String = "delete con_PartidasDetalle where IdPartida= " & entPartidas.IdPartida
            
            db.ExecuteNonQuery(tran, CommandType.Text, sql)

            For Each entDetalle In ListPartidasDetalle
                objTabla.con_PartidasDetalleInsert(entDetalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            cn.Close()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj
    End Function

    Public Function ConsultaPartidas() As DataSet

        Dim ds As New DataSet
        Dim sSQL As String = "select * from con_partidas; select * from con_partidasDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sSQL)

        Dim dr As New DataRelation _
            ("MaestroDetalle" _
             , ds.Tables(0).Columns("IdPartida") _
             , ds.Tables(1).Columns("IdPartida"))
        ds.Relations.Add(dr)

        Return ds
    End Function

    Public Function pro_ObtenerPartidasDetalle(ByVal IdPartida As Integer) As DataTable
        Dim sql As String = "select * from con_partidasDetalle where IdPartida= " & IdPartida
        
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)

    End Function

    Public Function pro_BorrarDetallePartida(ByVal IdPartida As Integer)
        Dim sql As String = "delete con_PartidasDetalle where IdPartida= " & IdPartida
        Return db.ExecuteNonQuery(CommandType.Text, sql)
    End Function
    Public Function IsCuentaTransac(ByVal IdCuenta As String) As Boolean
        Dim sql As String = "select 1 from con_cuentas where IdCuenta = @IdCuenta"
        sql &= " and EsTransaccional=1"

        Dim cmd As DbCommand = db.GetSqlStringCommand(sql)
        db.AddInParameter(cmd, "IdCuenta", DbType.String, IdCuenta)
        Dim num_reg As Integer = db.ExecuteScalar(cmd)
        If num_reg = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function IsCuentaConSaldo(ByVal IdCuenta As String) As Boolean
        Dim sSQL As String
        sSQL = "select 1 from con_partidasDetalle where IdCuenta=@IdCuenta"
        Dim cmd As DbCommand = db.GetSqlStringCommand(sSQL)
        db.AddInParameter(cmd, "IdCuenta", DbType.String, IdCuenta)
        If db.ExecuteScalar(cmd) Is Nothing Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function pro_VerificaIntegridadCierreMensual(ByVal Ejercicio As Integer, ByVal Mes As Integer) As DataSet
        Return db.ExecuteDataSet("con_spIntegridadCierreMes", Ejercicio, Mes)

    End Function
    Public Function pro_CierreMensual(ByVal Ejercicio As Integer, ByVal Mes As Integer, ByVal CreadoPor As String) As Boolean

        Dim dcCommand As DbCommand = db.GetStoredProcCommand("con_spCierreMesConsolidado", Ejercicio, Mes, CreadoPor)
        dcCommand.CommandTimeout = 5000000 '5 MINUTES

        Dim i As Integer = db.ExecuteScalar(dcCommand) 'db.ExecuteScalar("con_spCierreMesConsolidado", Ejercicio, Mes, CreadoPor)
        If i = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function CountPartidasCierre() As Integer
        Return db.ExecuteScalar(CommandType.Text, "SELECT count(*) FROM con_Partidas")
    End Function
    Public Function pro_CierreTemporal(ByVal Ejercicio As Integer, ByVal Mes As Integer, ByVal CreadoPor As String) As Boolean
        Dim i As Integer = db.ExecuteScalar("con_spCierreTemporal", Ejercicio, Mes, CreadoPor)
        If i = 0 Then
            Return False
        Else
            Return True
        End If
    End Function


    Public Function pro_RevierteCierreMensual(ByVal Ejercicio As Integer, ByVal Mes As Integer) As Boolean
        'Dim i As Integer = db.ExecuteScalar("con_cierre_mes", Ejercicio, Mes)
        Dim sql As String = "delete con_saldos where mes=@Mes and Ejercicio=@Ejercicio"

        'elimino el mes que se está tratando de revertir
        Dim cmd As DbCommand = db.GetSqlStringCommand(sql)
        db.AddInParameter(cmd, "@Mes", DbType.Int32, Mes)
        db.AddInParameter(cmd, "@Ejercicio", DbType.Int32, Ejercicio)
        db.ExecuteScalar(cmd)

        sql = "update con_partidas set actualizada=0 where month(fecha)=@Mes and year(fecha)=@Ejercicio "
        cmd = db.GetSqlStringCommand(sql)
        db.AddInParameter(cmd, "@Mes", DbType.Int32, Mes)
        db.AddInParameter(cmd, "@Ejercicio", DbType.Int32, Ejercicio)
        db.ExecuteScalar(cmd)


        sql = "delete con_cierres where mes=@Mes and Ejercicio=@Ejercicio"
        cmd = db.GetSqlStringCommand(sql)
        db.AddInParameter(cmd, "@Mes", DbType.Int32, Mes)
        db.AddInParameter(cmd, "@Ejercicio", DbType.Int32, Ejercicio)
        db.ExecuteScalar(cmd)

        ' se setea la fecha límite para realizar transacciones
        Dim Fecha As String = String.Format("{0}{1}01", Ejercicio.ToString.PadLeft(4, "0"), Mes.ToString.PadLeft(2, "0"))
        sql = String.Format("update adm_parametros set FechaCierre = '{0}'", Fecha)
        db.ExecuteNonQuery(CommandType.Text, sql)
        Return True

    End Function

    Public Function pro_RenumeraPartidas(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal TipoPart As String) As Integer
        Return db.ExecuteScalar("con_spRenumeraPartidas", Mes, Ejercicio, TipoPart)
    End Function
    Public Function pro_CrearCierre(ByVal iEjercicio As Integer, ByVal sTipoPar As String, ByVal dFecha As Date, ByVal sConcepto As String, ByVal sUser As String, ByVal sIdCuenta As String, ByVal sIdCuentaTipo As String, ByVal IdSucursal As Integer) As Integer
        Dim i As Integer = db.ExecuteScalar("con_spPartidaCierre", iEjercicio, sTipoPar, dFecha, sConcepto, sUser, sIdCuenta, sIdCuentaTipo, IdSucursal)
        Return i
    End Function
    Public Function pro_EliminaPartidasPorFecha(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipo As String, ByVal IdSucursal As Integer) As String

        Dim Condicion As String = " and 1=1 "
        If IdSucursal <> 1 And IdSucursal <> -1 Then
            Condicion = " and IdSucursal= " & IdSucursal
        End If

        Dim sql As String = ""
        sql &= "delete con_partidas where IdModuloOrigen < 2 and Fecha >= '" & Format(Desde, "yyyyMMdd")
        sql &= "' and Fecha < dateadd(dd,1,'" & Format(Hasta, "yyyyMMdd") & "') and IdTipo = @IdTipo"
        sql &= Condicion

        Dim cmd As DbCommand = db.GetSqlStringCommand(sql)
        db.AddInParameter(cmd, "@IdTipo", DbType.String, IdTipo)
        Return db.ExecuteScalar(cmd)
    End Function
    Public Function pro_ObtenerDatosICV(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataSet
        'con_spGenerarArchivoICV
        Return db.ExecuteDataSet("con_spGenerarArchivoICV2", Desde, Hasta, IdSucursal)
    End Function
    Public Sub pro_PreCierreMensual(ByVal Ejercicio As Integer, ByVal Mes As Integer, ByVal CreadoPor As String)
        db.ExecuteNonQuery("con_spPreCierreContable", Mes, Ejercicio)
    End Sub
    Public Function ObtieneIdPartida(ByVal IdPartida As Integer, ByVal TipoAvance As Integer) As Integer
        If TipoAvance = 0 Then
            Return IdPartida
        End If
        Dim sSQL As String = ""

        If TipoAvance = 1 Then
            sSQL = "select top 1 IdPartida from con_partidas where IdPartida > " & IdPartida
        Else
            sSQL = "select top 1 IdPartida from con_partidas where IdPartida < " & IdPartida & " order by IdPartida desc"
        End If
        Return db.ExecuteScalar(CommandType.Text, sSQL)
    End Function
    Public Sub EliminaCuentasMayor(ByVal IdCentro As String)
        Dim sSQL As String = "delete con_CentrosMayor Where IdCentro=@IdCentro"
        Dim cmd As DbCommand = db.GetSqlStringCommand(sSQL)

        db.AddInParameter(cmd, "@IdCentro", DbType.String, IdCentro)
        db.ExecuteNonQuery(cmd)
    End Sub
    Public Sub InsertarCuentaMayor(ByVal IdCentro As String, ByVal IdCuenta As String, ByVal CreadoPor As String)
        Dim sSQL = "Insert into con_CentrosMayor (IdCentro, IdCuentaMayor, CreadoPor) values (@IdCentro, @IdCuenta, @CreadoPor)"
        Dim cmd As DbCommand = db.GetSqlStringCommand(sSQL)
        db.AddInParameter(cmd, "@IdCentro", DbType.String, IdCentro)
        db.AddInParameter(cmd, "@IdCuenta", DbType.String, IdCuenta)
        db.AddInParameter(cmd, "@CreadoPor", DbType.String, CreadoPor)
        db.ExecuteNonQuery(cmd)
    End Sub
    Public Function ObtenerCuentasMayor(ByVal IdCentro As String) As DataTable
        Return db.ExecuteDataSet("con_spCentrosMayor", IdCentro).Tables(0)
    End Function
    Public Function ObtenerCuentasPatrimonio() As DataTable
        Return db.ExecuteDataSet("con_spCuentasPatrimonio").Tables(0)
    End Function

    Public Function rep_AuxiliarCentrosCosto(ByVal DesdeFecha As Date, ByVal HastaFecha As Date, ByVal IdCentro As String, ByVal DesdeCuenta As String, ByVal HastaCuenta As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spLibroAuxiliarMayorCentro", DesdeFecha, HastaFecha, IdCentro, DesdeCuenta, HastaCuenta, IdSucursal).Tables(0)
    End Function

    Function pro_ValidaCentroCuenta(ByVal IdCuenta As String, ByVal IdCentro As String) As Integer
        Return db.ExecuteScalar("con_spValidaCentroCosto", IdCuenta, IdCentro)
    End Function
    Public Function ObtenerSaldosCentros(ByVal DesdeFecha As Date, ByVal HastaFecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spSaldosCentro", DesdeFecha, HastaFecha, IdSucursal).Tables(0)
    End Function
    Public Function ObtenerSaldosCentrosMayor(ByVal DesdeFecha As Date, ByVal HastaFecha As Date, ByVal DesdeCentro As String, ByVal HastaCentro As String) As DataTable
        Return db.ExecuteDataSet("con_spSaldosCentrosMayor", DesdeFecha, HastaFecha, DesdeCentro, HastaCentro).Tables(0)
    End Function
    Public Function rep_EstadoResultadosCC(ByVal Desde As Date, ByVal Hasta As Date, ByVal DesdeCentro As String, ByVal HastaCentro As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("con_spEstadoResultadosCC", Desde, Hasta, DesdeCentro, HastaCentro, IdSucursal).Tables(0)
    End Function

    'elimina partidas generadas por los modulos
    Public Function con_EliminaPartidas(ByVal IdModulo As Integer, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipoPartida As String, ByVal IdSucursal As Integer) As Integer
        Dim sql As String = "delete con_partidas where Fecha between '" & Format(Desde, "yyyyMMdd") & "'"
        sql += "  and '" & Format(Hasta, "yyyyMMdd") & "'"
        sql += "  and IdTipo= '" & IdTipoPartida & "'"
        sql += "  and IdModuloOrigen= " & IdModulo
        sql += "  and IdSucursal =" & IdSucursal
        sql += "  ; delete adm_periodoscontabilizados where desdefecha = '" & Format(Desde, "yyyyMMdd") & "'"
        sql += "  and hastafecha = '" & Format(Hasta, "yyyyMMdd") & "'"
        sql += "  and IdTipoPartida= '" & IdTipoPartida & "'"
        sql += "  and IdModulo= " & IdModulo
        sql += "  and IdSucursal =" & IdSucursal

        Return db.ExecuteNonQuery(CommandType.Text, sql)
    End Function

    Public Function con_ValidaContabilizacion(ByVal IdSucursal As Integer, ByVal IdModulo As Integer, ByVal Fecha As Date, ByVal Hasta As Date) As Integer
        Dim sql As String = "select 1 from adm_periodosContabilizados where '" & Format(Fecha, "yyyyMMdd") & "' between DesdeFecha and HastaFecha"
        sql &= " and IdModulo=" & IdModulo & " and IdSucursal = " & IdSucursal & " and IdCaja <=0"
        sql &= " union select 2 from adm_periodosContabilizados where '" & Format(Hasta, "yyyyMMdd") & "' between DesdeFecha and HastaFecha"
        sql &= " and IdModulo=" & IdModulo & " and IdSucursal = " & IdSucursal & " and IdCaja <=0"
        Dim dt As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)

        Return dt.Rows.Count
    End Function
    Public Function con_ObtenerPeriodoContabilizado(ByVal IdModulo As Integer, ByVal IdSucursal As Integer, ByVal IdUsuario As String, Optional ByVal CajaChica As Boolean = False) As DataTable
        Dim sql As String = "select p.* from adm_PeriodosContabilizados p inner join adm_UsuariosSucursales s on p.idsucursal=s.idsucursal where p.IdModulo = " & IdModulo
        sql += " and s.IdUsuario= '" & IdUsuario & "'"
        If CajaChica Then
            sql += " and p.IdCaja > 0"
        Else
            sql += " and p.IdCaja <= 0"
        End If
        sql += " order by p.FechaHoraCreacion desc"

        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Function InsertaPartidasImport(ByRef entPartidas As con_Partidas, ByRef ListPartidasDetalle As List(Of con_PartidasDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""
        Try
            entPartidas.IdPartida = fd.ObtenerUltimoId("con_Partidas", "IdPartida") + 1
            TranInsertaPartidasImport(entPartidas, ListPartidasDetalle, tran)
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            cn.Close()
            msj = ex.Message()
        Finally
            cn.Close()
        End Try

        Return msj

    End Function
	Public Sub TranInsertaPartidasImport(ByRef entPartidas As con_Partidas, ByRef ListPartidasDetalle As List(Of con_PartidasDetalle), ByVal tran As DbTransaction)

		'Obtiene id de Partida
		'entPartidas.IdPartida = fd.ObtenerUltimoId("CON_PARTIDAS", "IdPartida") + 1

		'obtiene el numumero de partida
		'entPartidas.Numero = fd.GetNumPartida(entPartidas.IdTipo, entPartidas.Fecha, tran)
		objTabla.con_PartidasInsert(entPartidas, tran)

		For Each entDetalle In ListPartidasDetalle
			entDetalle.IdPartida = entPartidas.IdPartida
			objTabla.con_PartidasDetalleInsert(entDetalle, tran)
		Next

	End Sub

	Public Function InsertaFormulaFinanciera _
(ByRef entFormulaFinanciera As List(Of con_FormulasFinancieras), ByVal IdFormula As Integer) As String
		Dim cn As DbConnection = db.CreateConnection
		cn.Open()
		Dim tran As DbTransaction = cn.BeginTransaction()
		Try
			Dim Id As Integer

			Id = fd.ObtenerUltimoId("Con_FormulasFinancieras", "Id") + 1

			Dim sSQL As String = "delete con_FormulasFinancieras where IdFormula=" & IdFormula
			db.ExecuteNonQuery(tran, CommandType.Text, sSQL)

			For Each detalle As con_FormulasFinancieras In entFormulaFinanciera
				detalle.Id = Id
				objTabla.con_FormulasFinancierasInsert(detalle, tran)
				Id = Id + 1
			Next
		Catch ex As Exception
			tran.Rollback()
			Return ex.Message()
		End Try

		tran.Commit()
		cn.Close()
		Return ""

	End Function

	Public Function EliminaFormulaFinanciera _
(ByVal IdFormula As Integer) As String
		Dim cn As DbConnection = db.CreateConnection
		cn.Open()
		Dim tran As DbTransaction = cn.BeginTransaction()
		Try
			Dim sSQL As String = "delete con_FormulasFinancieras where IdFormula=" & IdFormula
			db.ExecuteNonQuery(tran, CommandType.Text, sSQL)

		Catch ex As Exception
			tran.Rollback()
			Return ex.Message()
		End Try

		tran.Commit()
		cn.Close()
		Return ""
	End Function

End Class

