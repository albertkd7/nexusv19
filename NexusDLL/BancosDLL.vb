﻿Imports System.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports System.Data.Common
Imports System.Data
Imports NexusELL.TableEntities

Public Class BancosDLL
    Dim fd As FuncionesDLL
    Dim db As Database
    Dim objTabla As TableData
    Dim dlConta As NexusDLL.ContabilidadDLL
    'En esta clase se encuentra toda la codificación que se necesita interacción con la base de datos

    Public Sub New(ByVal strConexion As String)

        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db
        fd = New FuncionesDLL(strConexion)
        dlConta = New NexusDLL.ContabilidadDLL(strConexion)
        objTabla = New TableData(strConexion)
    End Sub
    Public Function AnulacionChequeCerrado(ByVal iIdCheque As Integer, ByVal iIdTransaccion As Integer, ByVal iIdTipoTrans As Integer, ByVal sIdTipoPartida As String, ByVal iIdPartida As Integer, ByVal dFecha As Date, ByVal sMotivo As String, ByVal sUsuario As String) As Integer
        Return db.ExecuteScalar("ban_spAnulacionChequeMesCerrado", iIdCheque, iIdTransaccion, iIdTipoTrans, sIdTipoPartida, iIdPartida, dFecha, sMotivo, sUsuario)
    End Function
    Public Function AnulacionChequeAbierto(ByVal iIdCheque, ByVal sUsuario, ByVal dFecha, ByVal sMotivo) As Integer
        Return db.ExecuteScalar("ban_spAnulacionChequeMesAbierto", iIdCheque, sUsuario, dFecha, sMotivo)
    End Function
    Public Function GetConciliaRegistrosBancos(ByVal IdCuentaBancaria As Integer, ByVal fecha As Date, ByVal Todos As Boolean, ByVal IdSucursal As Integer) As DataSet
        Return db.ExecuteDataSet("ban_spObtenerDocumentosParaConciliacion", IdCuentaBancaria, fecha, Todos, IdSucursal)

    End Function
    Public Function ActualizaCompraLiquidada(ByVal IdComprobante As Integer, ByVal IdCheque As Integer) As Integer
        Dim sSql As String = "update com_compras set IdCheque=" & IdCheque
        sSql &= " where IdComprobante =" & IdComprobante
        Return db.ExecuteNonQuery(CommandType.Text, sSql)
    End Function
    Public Function ConsultaBancos() As DataSet

        Dim ds As New DataSet
        Dim sSQL As String = "select * from ban_bancos;select * from ban_cuentasbancarias "

        ds = db.ExecuteDataSet(CommandType.Text, sSQL)
        Dim dr As New DataRelation _
            ("MaestroDetalle", ds.Tables(0).Columns("idbanco"), ds.Tables(1).Columns("idbanco"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function ActualizaDetalleConciliacion(ByVal DetalleConcilia As List(Of ban_ConciliacionesDetalle), ByVal IdCuentaBancaria As Integer, ByVal IdSucursal As Integer, ByVal Mes As Integer, ByVal Anio As Integer, ByVal Fecha As Date, ByVal Cheques As Boolean, ByVal CreadoPor As String) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try

            'borro la conciliacion del mes
            Dim sSQL As String = "delete ban_conciliacionesdetalle where IdCuentaBancaria=" & IdCuentaBancaria
            sSQL += " and Idsucursal= " & IdSucursal
            sSQL += " and MesConcilia=" & Mes
            sSQL += " and EjercicioConcilia=" & Anio
            db.ExecuteNonQuery(CommandType.Text, sSQL)

            For Each det As ban_ConciliacionesDetalle In DetalleConcilia
                objTabla.ban_ConciliacionesDetalleInsert(det, tran)
            Next


        Catch ex As Exception
            tran.Rollback()
            cn.Close()
            Return ex.Message()
        End Try
        tran.Commit()
        cn.Close()
        Return ""

    End Function
    Public Function ConsultaCheques(ByVal usuario As String, ByVal Fecha As String) As DataSet

        Dim Condicion As String = " and 1=1 "
        Dim sql As String = "SELECT Che.IdCheque, Che.IdTipoPartida, Che.NumeroPartida, Ban.NumeroCuenta, Numero, Fecha,che.IdSucursal, AnombreDe, Left(Concepto,100) Concepto, Valor, che.Anulado, Che.CreadoPor, "
        sql &= "Che.FechaHoraCreacion from ban_Cheques Che inner join ban_cuentasbancarias ban on che.idcuentabancaria = ban.idcuentabancaria"
        sql &= " inner join adm_UsuariosSucursales s on che.idsucursal= s.idsucursal "
        sql &= String.Format(" where Fecha >= '{0}'", Fecha)
        sql &= String.Format(" and s.idusuario= '{0}'", usuario)
        sql &= Condicion
        sql &= String.Format("order by che.idcheque desc")
        sql &= String.Format(" select * from ban_ChequesDetalle cd inner join ban_Cheques ch on cd.IdCheque = ch.IdCheque inner join adm_UsuariosSucursales s on ch.idsucursal= s.idsucursal where Fecha >='{0}'", Fecha)
        sql &= String.Format(" and s.idusuario= '{0}'", usuario)
        sql &= Condicion

        Dim dsCh As DataSet = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("Cheques_Detalle" _
                                   , dsCh.Tables(0).Columns("IdCheque") _
                                   , dsCh.Tables(1).Columns("IdCheque"))
        dsCh.Relations.Add(dr)
        Return dsCh
    End Function
    Public Function ConsultaTransacciones(ByVal Usuario As String, ByVal Fecha As String) As DataSet

        Dim Condicion As String = " and 1=1 "


        Dim sql As String = "SELECT Tra.IdTransaccion, Tra.IdTipoPartida, Tra.NumeroPartida, Ban.NumeroCuenta, Tra.Fecha, Tra.IdSucursal, Left(Concepto,100) Concepto, Tra.Valor, Tra.CreadoPor,"
        sql &= " Tra.FechaHoraCreacion, Tra.ModificadoPor from ban_Transacciones Tra"
        sql &= " inner join Ban_CuentasBancarias Ban ON Tra.IdCuentaBancaria = Ban.IdCuentaBancaria"
        sql &= " inner join adm_UsuariosSucursales s on tra.idsucursal= s.idsucursal "
        sql &= String.Format(" where Fecha >= '{0}'", Fecha)
        sql &= String.Format(" and s.idusuario= '{0}'", usuario)
        sql &= Condicion
        sql &= String.Format(" order by tra.idtransaccion desc")
        sql &= String.Format(" ; select * from ban_TransaccionesDetalle td inner join Ban_Transacciones bt on td.IdTransaccion = bt.IdTransaccion inner join adm_UsuariosSucursales s on bt.idsucursal= s.idsucursal where Fecha > ='{0}'", Fecha)
        sql &= String.Format(" and s.idusuario= '{0}'", usuario)
        sql &= Condicion

        Dim dsTr As DataSet = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("TransDetalle" _
                                   , dsTr.Tables(0).Columns("IdTransaccion") _
                                   , dsTr.Tables(1).Columns("IdTransaccion"))
        dsTr.Relations.Add(dr)
        Return dsTr
    End Function
    Public Function GetLibroBancos(ByVal FechaI As DateTime, ByVal FechaF As DateTime, ByVal IdCuenta As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("ban_spLibroBancos", FechaI, FechaF, IdCuenta, IdSucursal).Tables(0)
    End Function
    Public Function GetCuentaCbleBanco(ByVal IdCuentaBancaria As Integer) As String
        Dim sSQL As String = "select IdCuentaContable from ban_CuentasBancarias"
        sSQL &= " where IdCuentaBancaria=" & IdCuentaBancaria
        Return db.ExecuteScalar(CommandType.Text, sSQL)
    End Function

    Public Function GetBloqueCheques(ByVal DesdeCheque As Integer, ByVal HastaCheque As Integer, ByVal IdCuenta As String) As DataTable
        Return db.ExecuteDataSet("ban_spBloqueCheques", DesdeCheque, HastaCheque, IdCuenta).Tables(0)
    End Function
    Public Sub ActualizaMarcaConciliado(ByVal IdCheque As Integer, ByVal Conciliado As Integer)
        Dim sSQL As String = "update ban_cheques set conciliado= " & Conciliado
        sSQL &= " where IdCheque=" & IdCheque

        db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Sub
    Public Sub AutorizaCheques(ByVal IdCheque As Integer, ByVal AutorizadoPor As String)
        Dim sSQL As String = " insert into ban_chequesautorizados values(" & IdCheque & ",'" & AutorizadoPor & "', getdate())"
        sSQL += "; update ban_cheques set impreso=1 where idcheque = " & IdCheque
        db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Sub

    Public Sub ActualizaMarcaProcesado(ByVal IdTransaccion As Integer, ByVal Procesado As Integer)
        Dim sSQL As String = "update ban_transacciones set procesadaBanco= " & Procesado
        sSQL &= " where IdTransaccion=" & IdTransaccion
        db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Sub
    Public Function GetConciliaCheques(ByVal IdCuentaBancaria As Integer, ByVal fecha As Date, ByVal Todos As Boolean, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("ban_spConciliaCheques", IdCuentaBancaria, fecha, Todos, IdSucursal).Tables(0)
    End Function
    Public Function GetChequesAutoriza(ByVal IdCuentaBancaria As Integer, ByVal fecha As Date, ByVal IdSucursal As Integer) As DataTable
        Dim sSQL As String = "Select IdCheque, Impreso, Numero, Fecha, AnombreDe, Concepto, Valor, CreadoPor, FechaHoraCreacion "
        sSQL += " from ban_cheques where idcuentabancaria= " & IdCuentaBancaria
        sSQL += " and impreso=0 and  idsucursal=" & IdSucursal
        sSQL += " and fecha <='" & Format(fecha, "yyyyMMdd") & "'"
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function

    Public Function GetConciliaTrans(ByVal IdCuentaBancaria As Integer, ByVal fecha As Date, ByVal Todos As Boolean, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("ban_spConciliaTransacciones", IdCuentaBancaria, fecha, Todos, IdSucursal).Tables(0)
    End Function
    Public Function GetCargosPendientes(ByVal cuenta_contable As String, ByVal fecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return fd.SiEsNulo(db.ExecuteScalar("ban_spCargosPendientes", cuenta_contable, fecha, IdSucursal), 0)
    End Function
    Public Function GetSaldoCuentaBanco(ByVal cuenta_contable As String, ByVal fecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return fd.SiEsNulo(db.ExecuteScalar("ban_spSaldoCuentaBanco", cuenta_contable, fecha, IdSucursal), 0)
    End Function
    Public Function GetCargosNoContab(ByVal sCtaContable As String, ByVal fecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return fd.SiEsNulo(db.ExecuteScalar("ban_spCargosNoContabilizados", sCtaContable, fecha, IdSucursal), 0)
    End Function
    Public Function GetDepositosTransito(ByVal cuenta_contable As String, ByVal fecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return fd.SiEsNulo(db.ExecuteScalar("ban_spDepositosTransito", cuenta_contable, fecha, IdSucursal), 0)
    End Function
    Public Function GetChequesPendientes(ByVal cuenta_contable As String, ByVal fecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return fd.SiEsNulo(db.ExecuteScalar("ban_spChequesPendientes", cuenta_contable, fecha, IdSucursal), 0)
    End Function
    Public Function GetDepositosNoContab(ByVal cuenta_contable As String, ByVal fecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return fd.SiEsNulo(db.ExecuteScalar("ban_spDepositosNoContabilizados", cuenta_contable, fecha, IdSucursal), 0)
    End Function
    Public Sub GeneraConciliacion(ByVal sCtaBanco, ByVal sCtaContable, ByVal dtFecha _
                              , ByVal dSaldoContab, ByVal dCargosNoCon _
                              , ByVal dDepositosNC, ByVal dChequesPend _
                              , ByVal dCargosPend, ByVal dDepositosTr _
                              , ByVal deSaldoBanco, ByVal IdSucursal)

        db.ExecuteNonQuery("ban_spGeneraConciliacion", sCtaBanco, sCtaContable, dtFecha _
                              , dSaldoContab, dCargosNoCon _
                              , dDepositosNC, dChequesPend _
                              , dCargosPend, dDepositosTr, deSaldoBanco, IdSucursal)
    End Sub
    Public Function GetCuentasBanco(ByVal IdCta As Integer) As DataTable
        Dim sSQL As String = "select * from ban_cuentasBancarias where IdCuentaBancaria=" & IdCta
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function GetCuentaBancaria(ByVal IdCta As Integer) As DataTable
        Dim sSQL As String = "select * from ban_cuentasBancarias where IdCuentaBancaria=" & IdCta
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function GetChequesDetalle(ByVal IdCheque As Integer) As DataTable
        Dim sSQL As String = "select * from ban_ChequesDetalle where IdCheque=" & IdCheque

        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function GetTransaccionDetalle(ByVal idTrans As Integer) As DataTable
        Dim sSQL As String = "select * from ban_transaccionesDetalle where idTransaccion=" & idTrans

        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function ban_InsertarCheque(ByVal Che As ban_Cheques, ByVal CheDet As List(Of ban_ChequesDetalle), ByVal Par As con_Partidas, _
        ByVal ParDet As List(Of con_PartidasDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Dim sql As String, msj As String = "Ok", PartidaNueva As Boolean
        Try
            'obtiene el id de partida
            If Par.IdPartida = -1 Then   'está desconectado el modulo
                Che.NumeroPartida = "000000"
                Che.IdPartida = -1
            Else
                If Che.IdPartida = 0 Then  'es una partida nueva
                    Par.IdPartida = fd.ObtenerUltimoId("CON_PARTIDAS", "IdPartida") + 1
                    Par.Numero = fd.GetNumPartida(Che.IdTipoPartida, Che.Fecha, tran)
                    PartidaNueva = True
                    Che.NumeroPartida = Par.Numero
                    Che.IdPartida = Par.IdPartida
                End If
            End If

            'obtiene el IdCheque y numero de cheque
            If Che.IdCheque = 0 Then  'es cheque nuevo
                Che.IdCheque = fd.ObtenerUltimoId("BAN_CHEQUES", "IdCheque") + 1
                Dim UltimoNumeroChq As Integer = GetNumeroCheque(Che.IdCuentaBancaria)

                If UltimoNumeroChq = Val(Che.Numero) Then
                    'Actualiza el ultimo numero de cheque
                    sql = "update ban_cuentasBancarias set ultNumeroCheque =" & UltimoNumeroChq
                    sql &= " where idCuentaBancaria=" & Che.IdCuentaBancaria
                    db.ExecuteNonQuery(tran, CommandType.Text, sql)
                End If

                objTabla.ban_ChequesInsert(Che, tran)
            Else
                objTabla.ban_ChequesUpdate(Che, tran)
                sql = "delete ban_chequesDetalle where idCheque=" & Che.IdCheque
                db.ExecuteNonQuery(tran, CommandType.Text, sql)
            End If

            For Each entDetalle As ban_ChequesDetalle In CheDet
                entDetalle.IdCheque = Che.IdCheque
                objTabla.ban_ChequesDetalleInsert(entDetalle, tran)
            Next

            'Guarda la partida si está en línea
            If Par.IdPartida > 0 Then
                If PartidaNueva Then
                    objTabla.con_PartidasInsert(Par, tran)
                Else
                    objTabla.con_PartidasUpdate(Par, tran)
                    sql = "delete con_PartidasDetalle where IdPartida=" & Par.IdPartida
                    db.ExecuteNonQuery(tran, CommandType.Text, sql)
                End If

                For Each entDetallePartida As con_PartidasDetalle In ParDet
                    entDetallePartida.IdPartida = Par.IdPartida
                    objTabla.con_PartidasDetalleInsert(entDetallePartida, tran)
                Next
            End If
            
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj

    End Function
  
    Public Function existeCheque(ByVal IdCuentaBancaria As Integer, ByVal sNumero As String) As Integer
        Dim sSql As String = "select IdCheque from ban_cheques where IdCuentaBancaria=" & IdCuentaBancaria
        sSql &= " and Numero ='" & sNumero & "'"
        Dim IdCheque As Integer = fd.SiEsNulo(db.ExecuteScalar(CommandType.Text, sSql), 0)
        Return IdCheque
    End Function
    Public Function InsertarTransaccion(ByVal entTrans As ban_Transacciones, ByVal detalle As List(Of ban_TransaccionesDetalle), ByVal entPartida As con_Partidas, ByVal DetallePartidas As List(Of con_PartidasDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj = "Ok"
        Try
            TransInsertarTransaccion(entTrans, detalle, entPartida, DetallePartidas, tran)
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj
    End Function
    Public Sub TransInsertarTransaccion _
            (ByVal entTrans As ban_Transacciones _
             , ByVal detalle As List(Of ban_TransaccionesDetalle) _
             , ByVal entPartida As con_Partidas _
             , ByVal DetallePartidas As List(Of con_PartidasDetalle) _
             , ByVal tran As DbTransaction)

        'obtiene el id de transaccion
        entTrans.IdTransaccion = fd.ObtenerUltimoId("BAN_TRANSACCIONES", "IdTransaccion") + 1

        'Crea la partida
        If entTrans.Contabilizar Then    'si se contabiliza
            dlConta.TranInsertaPartidas(entPartida, DetallePartidas, tran)
            entTrans.IdPartida = entPartida.IdPartida
            entTrans.NumeroPartida = entPartida.Numero
        End If

        'Guarda la transacción
        objTabla.ban_TransaccionesInsert(entTrans, tran)

        For Each entDetalle As ban_TransaccionesDetalle In detalle
            entDetalle.IdTransaccion = entTrans.IdTransaccion
            objTabla.ban_TransaccionesDetalleInsert(entDetalle, tran)
        Next
    End Sub
    Public Function ActualizarTransaccion(ByVal entTrans As ban_Transacciones, _
          ByVal detalle As List(Of ban_TransaccionesDetalle), ByVal entPartida As con_Partidas, ByVal DetallePartidas As List(Of con_PartidasDetalle)) As String


        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Dim bEsPartidaNueva As Boolean = False
        Dim msj As String = "Ok"
        Try
            If entTrans.IdPartida = 0 And entTrans.Contabilizar Then

                'obtiene el id de partida
                bEsPartidaNueva = True
                entPartida.IdPartida = fd.ObtenerUltimoId("CON_PARTIDAS", "IdPartida") + 1
                entTrans.IdPartida = entPartida.IdPartida

                'obtiene el numero de partida
                entTrans.NumeroPartida = fd.GetNumPartida(entTrans.IdTipoPartida, entTrans.Fecha, tran)
                entPartida.Numero = entTrans.NumeroPartida
            End If
            If Not entTrans.Contabilizar Then
                db.ExecuteNonQuery(tran, "con_PartidasDeleteBypk", entTrans.IdPartida)
                entTrans.IdPartida = 0
                entTrans.NumeroPartida = ""
            End If

            objTabla.ban_TransaccionesUpdate(entTrans, tran)

            'Borra el detalle del transaccion y lo vuelve a insertar
            Dim sql As String = "delete ban_TransaccionesDetalle where IdTransaccion=" & entTrans.IdTransaccion

            db.ExecuteNonQuery(tran, CommandType.Text, sql)

            For Each entDetalle As ban_TransaccionesDetalle In detalle
                objTabla.ban_TransaccionesDetalleInsert(entDetalle, tran)
            Next

            If entTrans.Contabilizar Then    'si se contabiliza
                If bEsPartidaNueva Then
                    'Guarda por primera vez la partida
                    objTabla.con_PartidasInsert(entPartida, tran)
                    'For Each entDetallePartida As con_PartidasDetalle In DetallePartidas
                    '    entDetallePartida.IdPartida = entPartida.IdPartida
                    '    objTabla.con_PartidasDetalleInsert(entDetallePartida, tran)
                    'Next
                Else
                    'Actualiza partida
                    objTabla.con_PartidasUpdate(entPartida, tran)
                    'Elimina el detalle de partida para luego volverlo a crear
                    sql = "delete con_PartidasDetalle where IdPartida=" & entPartida.IdPartida
                    db.ExecuteNonQuery(tran, CommandType.Text, sql)
                End If
                For Each entDetallePartida As con_PartidasDetalle In DetallePartidas
                    entDetallePartida.IdPartida = entPartida.IdPartida
                    objTabla.con_PartidasDetalleInsert(entDetallePartida, tran)
                Next
            End If

            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try
        Return msj
    End Function
    Public Function GetNumeroCheque(ByVal IdCuentaBancaria As Integer) As Integer
        Dim sql As String = "select UltNumeroCheque from ban_cuentasBancarias where IdCuentaBancaria =" & IdCuentaBancaria
        Dim UltimoValor As Integer = fd.SiEsNulo(db.ExecuteScalar(CommandType.Text, sql), 0)

        UltimoValor += 1
        Return UltimoValor

    End Function
    Public Function GetIdFormatoCheque(ByVal IdCuentaBancaria As Integer) As Integer
        Dim sSQL As String = "select IdFormatoVaucher from ban_cuentasBancarias"
        sSQL &= " where IdCuentaBancaria =" & IdCuentaBancaria

        Return fd.SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), 0)

    End Function
  
    Public Function ConsultaTransacciones() As DataSet
        Dim ds As New DataSet
        Dim sSQL As String = "select * from ban_transacciones; select * from ban_TransaccionesDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sSQL)
        Dim dr As New DataRelation _
            ("TransaccionesDetalle" _
             , ds.Tables(0).Columns("idTransaccion") _
             , ds.Tables(1).Columns("idTransaccion"))
        ds.Relations.Add(dr)

        Return ds

    End Function
    Public Function GetChequesPorConciliar(ByVal IdCuentaBancaria As Integer) As DataTable
        Dim sSQL As String = "select idCheque, conciliado, numero, AnombreDe, fecha, valor" & _
        " from ban_cheques where conciliado = 0 and IdCuentaBancaria= " & IdCuentaBancaria

        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)

    End Function
    Public Function ActualizaChequeConciliado(ByVal IdCheque As Integer) As Boolean
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            Dim sSQL As String = "update ban_cheques set conciliado=1"
            sSQL &= " where idCheque=" & IdCheque

            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)


        Catch ex As Exception
            tran.Rollback()
            cn.Close()
            Return False
        End Try
        tran.Commit()
        cn.Close()
        Return (True)
    End Function
    Function ban_ObtenerIdCuentaBancaria(ByVal IdCta As Integer, ByVal TipoAvance As Integer) As Integer
        If TipoAvance = 0 Then
            Return IdCta
        End If
        Dim sql As String = "select top 1 IdCuentaBancaria from ban_cuentasBancarias where IdCuentaBancaria > " & IdCta

        If TipoAvance <> 1 Then
            sql = "select top 1 IdCuentaBancaria from ban_cuentasBancarias where IdCuentaBancaria < " & IdCta & " order by IdCuentaBancaria desc"
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Function GetIdCheque(ByVal IdCheque As Integer, ByVal TipoAvance As Integer) As Integer
        Dim sql As String = "select isnull(min(IdCheque),0) from ban_cheques where IdCheque> " & IdCheque
        If TipoAvance = -1 Then  'para obtener el registro anterior
            sql = "select isnull(max(IdCheque),0) from ban_cheques where IdCheque < " & IdCheque
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)

    End Function
    Function GetIdTransac(ByVal IdTransac As Integer, ByVal TipoAvance As Integer) As Integer
        Dim sql As String = "select isnull(min(IdTransaccion),0) from ban_transacciones where IdTransaccion > " & IdTransac
        If TipoAvance = -1 Then  'para obtener el registro anterior
            sql = "select isnull(max(IdTransaccion),0) from ban_transacciones where Idtransaccion < " & IdTransac
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Function ObtieneTransaccionByPk(ByVal IdTransaccion As Integer) As DataTable
        Return db.ExecuteDataSet("ban_spObtieneTransaccionByPk", IdTransaccion).Tables(0)
    End Function

#Region "Reportes"
    Public Function ReporteCheques(ByVal IdCheque As Integer, ByVal Tipo As Integer) As DataTable
        Return db.ExecuteDataSet("ban_GetCheque", IdCheque, Tipo).Tables(0)
    End Function
    Public Function GetListadoCheques(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdCta As Integer, ByVal IdSucursal As Integer) As DataTable

        Dim Condicion As String = " and 1=1 "
        If IdSucursal <> 1 And IdSucursal <> -1 Then
            Condicion = " and bc.IdSucursal= " & IdSucursal
        End If

        Dim sql = ""
        sql &= "SELECT Banco=bcb.Nombre+' / '+bcb.NumeroCuenta, Fecha, Numero, AnombreDe, Valor, Concepto FROM ban_Cheques bc "
        sql &= "inner join ban_CuentasBancarias bcb on bc.IdCuentaBancaria = bcb.IdCuentaBancaria "
        sql &= "WHERE Fecha >= '" & Format(Desde, "yyyyMMdd") & "' and Fecha < dateadd(dd, 1, '" & Format(Hasta, "yyyyMMdd") & "')"
        sql &= Condicion

        If IdCta = -1 Then
            sql &= " order by bc.IdCuentaBancaria, Convert(numeric, Numero)"
        Else
            sql &= " and bc.IdCuentaBancaria = " & IdCta & " order by bc.IdCuentaBancaria, convert(numeric,Numero)"
        End If

        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function GetListadoTrans(ByVal FechaI As DateTime, ByVal FechaF As DateTime, ByVal IdCuentaBancaria As String, ByVal iIdTipo As Integer, ByVal IdSucursal As Integer) As DataTable

        Dim Condicion As String = " and 1=1 "
        If IdSucursal <> 1 And IdSucursal <> -1 Then
            Condicion = " and tra.IdSucursal= " & IdSucursal
        End If

        Dim sSQL = ""
        sSQL &= "SELECT Det.IdCuenta, Det.Referencia, Det.Concepto, Det.Debe, Det.Haber, Tra.IdTransaccion, Tra.Fecha, Tra.Concepto, Tra.NumeroPartida FROM ban_Transacciones Tra"
        sSQL &= " INNER JOIN Ban_TransaccionesDetalle Det ON Det.IdTransaccion = Tra.IdTransaccion WHERE Fecha >= '" & Format(FechaI, "yyyyMMdd") & "' and Fecha < dateadd(dd, 1, '" & Format(FechaF, "yyyyMMdd") & "')"
        sSQL &= " AND Tra.IdCuentaBancaria = " & IdCuentaBancaria
        sSQL &= Condicion

        If iIdTipo > 0 Then
            sSQL &= " AND Tra.IdTipo = " & iIdTipo
        End If
        sSQL &= " Order by Tra.IdTransaccion"
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function Obtieneconciliacion(ByVal idCta, ByVal dtFecha, ByVal IdSucursal) As DataTable
        Return db.ExecuteDataSet("ban_spObtieneConciliacion", idCta, dtFecha, IdSucursal).Tables(0)
    End Function
    Public Function ObtieneDisponibilidad(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("ban_spDisponibilidad", dFechaI, dFechaF, IdSucursal).Tables(0)
    End Function
    Public Function ObtieneDisponibilidadDet(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("ban_spDisponibilidadDetalle", dFechaI, dFechaF, IdSucursal).Tables(0)
    End Function
    Public Function ban_ObtieneDataBancos(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("ban_spObtieneDataBancos", Desde, Hasta, IdSucursal).Tables(0)
    End Function
    Function ValidaCentroCuenta(ByVal IdCuenta As String, ByVal IdCentro As String) As Integer
        Return db.ExecuteScalar("con_spValidaCentroCosto", IdCuenta, IdCentro)
    End Function

    Public Function ObtienePartidaDesembolso(ByVal Id As Integer, ByVal TipoMovimiento As Integer, ByVal IdAsociado As Integer, ByVal IdCuentaBanco As Integer, ByVal NumCheque As Integer, ByVal CreadoPor As String) As DataTable
        Dim Store As String = ""

        If TipoMovimiento = 1 Then
            Store = "ban_spChequeDesembolso"
            Return db.ExecuteDataSet(Store, Id, IdCuentaBanco, NumCheque, CreadoPor).Tables(0)
        Else
            Store = "ban_spChequeOtrosDBCoop"
            Return db.ExecuteDataSet(Store, Id, TipoMovimiento, IdAsociado, IdCuentaBanco, NumCheque, CreadoPor).Tables(0)
        End If
    End Function
    Public Function ban_ContabilizarRegistros(ByVal Desde As Date, ByVal Hasta As Date, ByVal TipoPartida As String, ByVal IdSucursal As Integer, ByVal CreadoPor As String) As String
        Return db.ExecuteScalar("ban_spContabilizarRegistros", Desde, Hasta, TipoPartida, IdSucursal, CreadoPor)
    End Function
#End Region
End Class
