﻿Imports System.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports System.Data.Common
Imports System.Data
Imports NexusELL.TableEntities
Public Class CuentasPCDLL
    Dim db As Database
    Dim fd As FuncionesDLL
    Dim objTablas As TableData
    Dim dlConta As ContabilidadDLL
    Dim dlBancos As BancosDLL

    Public Sub New(ByVal strConexion As String)

        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db
        dlConta = New ContabilidadDLL(strConexion)
        dlBancos = New BancosDLL(strConexion)
        fd = New FuncionesDLL(strConexion)
        objTablas = New TableData(strConexion)
    End Sub
    Public Function cpc_ConsultaCobros(ByVal IdUsusario As String) As DataTable

        Dim sSQL As String


        sSQL = "SELECT c.IdComprobante, c.Fecha, Vendedor=ven.Nombre, c.IdSucursal, c.CreadoPor, c.FechaHoraCreacion"
        sSQL &= " FROM cpc_DocumentosCobro c inner join adm_UsuariosSucursales s on c.idsucursal= s.idsucursal "
        sSQL &= " inner join fac_vendedores ven on c.idvendedor = ven.idvendedor "
        sSQL &= " where s.idusuario ='" & IdUsusario & "'"
        sSQL &= " order by c.IdComprobante desc"

        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function ContabilizarComprobantesRetencion(ByVal Desde As DateTime, ByVal Hasta As DateTime, ByVal TipoPartida As String, ByVal CreadoPor As String, ByVal Concepto As String, ByVal Idsucursal As Integer) As String
        Return db.ExecuteScalar("cpc_spContabilizarComprobantesRetencion", Desde, Hasta, TipoPartida, CreadoPor, Concepto, Idsucursal)
    End Function
    Public Function ConsultaCargos(ByVal sIdCliente As String) As DataTable
        Return db.ExecuteDataSet("cpc_spConsultaCargos", sIdCliente).Tables(0)
    End Function
    Public Function getAbonoDetalle(ByVal IdComprobante As Integer) As DataTable
        Dim sql = "select * from cpc_AbonosDetalle where IdComprobante = " & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function ObtenerAbono(ByVal IdComprobante As Integer, ByVal Fecha As DateTime) As DataTable
        Return db.ExecuteDataSet("cpc_spAbono", IdComprobante, Fecha).Tables(0)
    End Function
    Public Function InsertarDocumentosCobro(ByRef PedidoHeader As cpc_DocumentosCobro, ByRef PedidoDetalle As List(Of cpc_DocumentosCobroDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""
        Try
            PedidoHeader.IdComprobante = fd.ObtenerUltimoId("cpc_DocumentosCobrodetalle", "IdComprobante") + 1
            objTablas.cpc_DocumentosCobroInsert(PedidoHeader, tran)

            For Each detalle As cpc_DocumentosCobroDetalle In PedidoDetalle
                detalle.IdComprobante = PedidoHeader.IdComprobante
                objTablas.cpc_DocumentosCobroDetalleInsert(detalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try
        Return msj


        Return ""
    End Function
    Public Function ActualizaDocumentosCobro(ByRef PedidoHeader As cpc_DocumentosCobro, ByRef PedidoDetalle As List(Of cpc_DocumentosCobroDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""
        Try
            objTablas.cpc_DocumentosCobroUpdate(PedidoHeader, tran)

            Dim sql As String = "delete cpc_DocumentosCobroDetalle where IdComprobante=" & PedidoHeader.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sql)

            For Each detalle As cpc_DocumentosCobroDetalle In PedidoDetalle
                detalle.IdComprobante = PedidoHeader.IdComprobante
                objTablas.cpc_DocumentosCobroDetalleInsert(detalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj
    End Function
    Public Function ContabilizarAbonos(ByVal Desde As DateTime, ByVal Hasta As DateTime, ByVal TipoPartida As String, ByVal CreadoPor As String, ByVal Concepto As String, ByVal IdSucursal As Integer) As String
        Return db.ExecuteScalar("cpc_spContabilizar", Desde, Hasta, TipoPartida, CreadoPor, Concepto, IdSucursal)
    End Function

    Public Function GetSaldosFacturas(ByVal IdCliente As String, ByVal Fecha As Date, ByVal FechaUltPago As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spSaldosAbonos", IdCliente, Fecha, FechaUltPago, IdSucursal).Tables(0)
    End Function
    Public Function ObtenerDocumentosCobroDetalle(ByVal IdComprobante As Integer) As DataTable
        Dim sql As String

        sql = "select d.*, v.IdTipoComprobante,v.IdCliente,v.Fecha, Numero=t.Abreviatura+'-'+ v.Numero, v.TotalComprobante, v.Nombre  from cpc_DocumentosCobrodetalle d"
        sql += " inner join fac_ventas v on d.idcomprobventa = v.idcomprobante"
        sql += " inner join adm_TiposComprobante t on v.idtipocomprobante = t.idtipocomprobante"
        sql += " where d.IdComprobante = " & IdComprobante

        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Function InsertaAbono _
        (ByRef AbonoHeader As cpc_Abonos _
        , ByRef AbonoDetalle As List(Of cpc_AbonosDetalle) _
        , ByVal PartidaHeader As con_Partidas _
        , ByVal PartidaDetalle As List(Of con_PartidasDetalle) _
        , ByVal TransHeader As ban_Transacciones _
        , ByVal TransDetalle As List(Of ban_TransaccionesDetalle)) As String


        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj = ""
        Try


            If TransHeader.IdTipoPartida <> "" Then
                dlBancos.TransInsertarTransaccion(TransHeader, TransDetalle, PartidaHeader, PartidaDetalle, tran)
                AbonoHeader.IdTransaccion = TransHeader.IdTransaccion
                AbonoHeader.IdPartida = TransHeader.IdPartida

            ElseIf PartidaHeader.IdTipo <> "" Then
                dlConta.TranInsertaPartidas(PartidaHeader, PartidaDetalle, tran)
                AbonoHeader.IdPartida = PartidaHeader.IdPartida
            End If

            'obtengo el primary key de abonos
            AbonoHeader.IdComprobante = fd.ObtenerUltimoId("CPC_ABONOS", "IdComprobante") + 1
            objTablas.cpc_AbonosInsert(AbonoHeader, tran)

            For Each detalle As cpc_AbonosDetalle In AbonoDetalle
                detalle.IdComprobante = AbonoHeader.IdComprobante
                objTablas.cpc_AbonosDetalleInsert(detalle, tran)
                If detalle.SaldoComprobante = detalle.MontoAbonado Then
                    Dim sSQL2 = "update fac_Ventas set FechaCancelacion = '" & String.Format("{0:yyyyMMdd}", AbonoHeader.Fecha)
                    sSQL2 &= "' where IdComprobante = '" & detalle.IdComprobVenta & "'"
                    db.ExecuteNonQuery(CommandType.Text, sSQL2)
                End If
            Next
            'actualizo la fecha del ultimo pago del cliente
            Dim sSQL = "update fac_clientes set FechaUltPago = '" & String.Format("{0:yyyyMMdd}", AbonoHeader.Fecha)
            sSQL &= "' where IdCliente = '" & AbonoHeader.IdCliente & "'"

            db.ExecuteNonQuery(CommandType.Text, sSQL)

            Dim sSQL_A = "update adm_correlativos set UltimoValor = UltimoValor + 1 where correlativo='ABONOS_CPC'"
            db.ExecuteNonQuery(CommandType.Text, sSQL_A)

            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.ToString()
        Finally
            cn.Close()
        End Try

        Return msj
    End Function
    'Public Function ConsultaAbonos(ByVal sIdCliente As String) As DataSet
    '    Dim ds As New DataSet
    '    Dim sSQL = "select IdComprobante, NumeroComprobante, Fecha, Concepto, CreadoPor FROM cpc_Abonos "
    '    sSQL &= " where IdCliente = '" & sIdCliente
    '    sSQL &= "' ; SELECT Numero=ve.Numero, ve.Fecha, ve.TotalComprobante, de.SaldoComprobante, "
    '    sSQL &= "de.Concepto, de.IdComprobante from cpc_AbonosDetalle de inner join fac_ventas ve "
    '    sSQL &= "on de.IdComprobVenta = ve.IdComprobante and ve.IdCliente = '" & sIdCliente & "'"
    '    ds = db.ExecuteDataSet(CommandType.Text, sSQL)

    '    Dim dr As New DataRelation("Abonos_Detalle" _
    '                               , ds.Tables(0).Columns("IdComprobante") _
    '                               , ds.Tables(1).Columns("IdComprobante"))
    '    ds.Relations.Add(dr)
    '    Return ds
    'End Function
    Public Function ConsultaAbonos(ByVal IdCliente As String) As DataTable
        Dim sql As String = "select NumeroComprobante,FechaComprobante=Fecha,TotalComprobante=sum(det.MontoAbonado),Saldo=0,MontoAbonado=sum(det.MontoAbonado),ven.IdComprobante from cpc_Abonos ven"
        sql += " Inner Join cpc_AbonosDetalle det on det.IdComprobante=ven.IdComprobante where ven.IdCliente='" & IdCliente & "'"
        sql += "GROUP BY NumeroComprobante,ven.Fecha,ven.IdComprobante  "
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Sub ActualizaFechaCliente(ByVal sIdCliente As String, ByVal dFecha As Date)
        Dim sql = ""
        If dFecha = Nothing Then
            sql &= "update fac_clientes set FechaUltPago = '19991231"
        Else
            sql &= "update fac_clientes set FechaUltPago = '" & String.Format("{0:yyyyMMdd}", dFecha)
        End If

        sql &= "' Where IdCliente = '" & sIdCliente & "'"
        db.ExecuteNonQuery(CommandType.Text, sql)
    End Sub
    Public Function getClientes(ByVal sFiltro As String) As DataTable
        Return db.ExecuteDataSet("cpc_spList_Clientes", sFiltro).Tables(0)
    End Function
    Public Function getDocsCredito(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sIdCliente As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spDocs_Credito", dFechaIni, dFechaFin, sIdCliente, IdSucursal).Tables(0)
    End Function
    Public Function getAbonosCliente(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sIdCliente As String, ByVal IdRutas As Integer, ByVal IdSucursal As Integer, ByVal IdVendedor As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spAbonos_Cliente", dFechaIni, dFechaFin, sIdCliente, IdRutas, IdSucursal, IdVendedor).Tables(0)
    End Function

    Public Function getAbonosClienteConsolidado(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sIdCliente As String, ByVal IdRuta As Integer, ByVal IdSucursal As Integer, ByVal IdVendedor As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spAbonosClienteConsolidado", dFechaIni, dFechaFin, sIdCliente, IdRuta, IdSucursal, IdVendedor).Tables(0)
    End Function

    Public Function getCargosCliente(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sIdCliente As String) As DataTable
        Return db.ExecuteDataSet("cpc_spCargos_Cliente", dFechaIni, dFechaFin, sIdCliente).Tables(0)
    End Function
    Public Function cpc_ConsultaAbonosClientes(ByVal IdCliente As String, ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Return db.ExecuteDataSet("cpc_spConsultaAbonosClientes", IdCliente, Desde, Hasta).Tables(0)
    End Function
    Public Function getDocsCancelados(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sIdCliente As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spDocs_Cancelados", dFechaIni, dFechaFin, sIdCliente, IdSucursal).Tables(0)
    End Function
    Public Function cpc_EstadoCuenta(ByVal HastaFecha As Date, ByVal sIdCliente As String, ByVal sIdCliente2 As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spEstadoCuenta", HastaFecha, sIdCliente, sIdCliente2, IdSucursal).Tables(0)
    End Function
    Public Function cpc_MovimientoHistoricoPorCliente(ByVal IdCliente As String, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spMovimientoHistorico", IdCliente, Desde, Hasta, IdSucursal).Tables(0)
    End Function
    Public Function getDocsVencer(ByVal dDesdeFecha As Date, ByVal dHastaFecha As Date, ByVal sIdCliente As String) As DataTable
        Return db.ExecuteDataSet("cpc_spMov_Historico", dDesdeFecha, dHastaFecha, sIdCliente).Tables(0)
    End Function
    Public Function getAnalisis(ByVal HastaFecha As Date, ByVal IdSucursal As Integer, ByVal Idvendedor As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spAnalisis", HastaFecha, IdSucursal, Idvendedor).Tables(0)
    End Function
    Public Function getAnalisisDetalleExcel(ByVal HastaFecha As Date, ByVal IdSucursal As Integer, ByVal IdVendedor As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spAnalisisDetalleExcel", HastaFecha, IdSucursal, IdVendedor).Tables(0)
    End Function
    Public Function getAnalisisExcel(ByVal HastaFecha As Date, ByVal IdSucursal As Integer, ByVal IdVendedor As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spAnalisisExcel", HastaFecha, IdSucursal, IdVendedor).Tables(0)
    End Function
    Public Function getAnalisisDetalle(ByVal HastaFecha As Date, ByVal IdSucursal As Integer, ByVal Idvendedor As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spAnalisisDetalle", HastaFecha, IdSucursal, Idvendedor).Tables(0)
    End Function
    Public Function getAuxiliar(ByVal Codigo As String, ByVal HastaFecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spAuxiliar", Codigo, HastaFecha, IdSucursal).Tables(0)
    End Function

    Public Function SaldoCliente(ByVal IdCliente As String, ByVal HastaFecha As Date, ByVal IdSucursal As Integer) As Decimal
        Return db.ExecuteScalar("cpc_spSaldoCliente", IdCliente, HastaFecha, IdSucursal)
    End Function
    Public Function DocumentosVencidos(ByVal IdCliente As String, ByVal HastaFecha As Date) As Integer
        Return db.ExecuteScalar("cpc_spDocumentosVencidos", IdCliente, HastaFecha)
    End Function

    Function cpc_ObtenerUltAbono(ByVal IdCliente As String) As Integer
        Dim sql As String = "select isnull(max(IdComprobante),0) from cpc_Abonos where concepto NOT like '%ABONO ANULADO%' and  IdCliente='" & IdCliente & "'"
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function

    Function cpc_ActualizaFechaCancelacion(ByVal IdComprobante As Integer) As String
        Dim sql As String = "update fac_ventas set fechacancelacion=null from fac_ventas   fa inner join cpc_AbonosDetalle  n"
        sql += " on fa.idcomprobante = n.IdComprobVenta  where n.IdComprobante =  " & IdComprobante

        Return db.ExecuteNonQuery(CommandType.Text, sql)
    End Function
    Function cpc_AnulaAbono(ByVal IdComprobante As Integer, ByVal AnuladoPor As String) As String
        Dim sql As String = "update cpc_abonos set fechahoramodificacion =getdate(), ModificadoPor='" & AnuladoPor
        sql += "', concepto='<<< ABONO ANULADO >>> '+concepto  where IdComprobante =  " & IdComprobante
        sql += "; update cpc_abonosdetalle set montoabonado=0.0, saldocomprobante=0.0 where idcomprobante = " & IdComprobante
        Return db.ExecuteNonQuery(CommandType.Text, sql)
    End Function

    Function cpc_GetDetalleAbono(ByVal IdComprobante As Integer) As DataTable
        Dim sql As String = "select NumeroComprobante=ven.Numero, FechaComprobante=ven.Fecha,"
        sql += "TotalComprobante=ven.TotalComprobante,"
        sql += "Saldo=det.SaldoComprobante, MontoAbonado=det.MontoAbonado,det.IdComprobante "
        sql += " from cpc_AbonosDetalle det Inner Join fac_ventas ven on det.IdComprobVenta=ven.IdComprobante where det.IdComprobante = " & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function cpc_DocumentosAnulados(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdCliente As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spDocumentosAnulados", Desde, Hasta, IdCliente, IdSucursal).Tables(0)
    End Function
    Public Function cpc_AplicarNotaCredito(ByRef NotaDetalle As List(Of fac_NotasCredito), ByVal Fecha As DateTime) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Try
            For Each detalle As fac_NotasCredito In NotaDetalle
                objTablas.fac_NotasCreditoInsert(detalle, tran)
                If detalle.SaldoComprobante = detalle.MontoAbonado Then
                    Dim sSQL2 = "update fac_Ventas set FechaCancelacion = '" & String.Format("{0:yyyyMMdd}", Fecha)
                    sSQL2 &= "' where IdComprobante = '" & detalle.IdComprobVenta & "'"
                    db.ExecuteNonQuery(CommandType.Text, sSQL2)
                End If
            Next
        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try
        tran.Commit()
        cn.Close()
        Return ""
    End Function

End Class
