﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports System.Data.Common
Imports System.Data
Imports NexusELL.TableEntities

Public Class InventarioDLL
    Dim db As Database
    Dim dbAux As Database
    Dim fd As FuncionesDLL
    Dim Tablas As TableData
    Dim TablasAux As TableData

    Public Sub New(ByVal strConexion As String)

        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db
        fd = New FuncionesDLL(strConexion)
        Tablas = New TableData(strConexion)
    End Sub

    Public Function GetIdSubGrupo(ByVal IdGrupo As Integer, ByVal tran As DbTransaction)
        Dim ssQL As String = "select max(IdSubGrupo) from inv_subgrupos "
        'ssQL &= " where IdGrupo=" & IdGrupo
        Dim IdSubGrupo = db.ExecuteScalar(tran, CommandType.Text, ssQL)
        If IsDBNull(IdSubGrupo) Then IdSubGrupo = 0
        Return IdSubGrupo
    End Function
    Public Function inv_BuscaProductosByLike(ByVal Codigo As String, ByVal Nombre As String) As DataTable
        Return db.ExecuteDataSet("inv_spBuscaProductosByLike", Codigo, Nombre).Tables(0)
    End Function
    Public Function inv_ObtienePrecioCosto(ByVal IdProducto As String, ByVal Fecha As DateTime) As Decimal
        Return db.ExecuteScalar("inv_spObtienePrecioCosto", IdProducto, Fecha)
    End Function

    Public Function inv_ObtieneTipoProducto(ByVal IdProducto As String) As Integer
        Dim sSQL As String = "Select TipoProducto from inv_productos where idproducto = '" & IdProducto & "'"
        Return fd.SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), 2)
    End Function
    Public Function inv_ProductoEsCompuesto(ByVal IdProducto As String) As Boolean
        Dim sSQL As String = "Select Compuesto from inv_productos where idproducto = '" & IdProducto & "'"
        Return fd.SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), False)
    End Function
    Public Function inv_ObtenerGastosProduccionProdEstructura() As DataTable
        Dim sSQL = "Select IdProduccion =convert(int,0), IdGasto, IdProducto=convert(varchar(25),''), Descripcion=convert(varchar(90),''), AplicaGasto=convert(BIT,0)"
        sSQL += " from inv_gastosproduccion where idgasto=-999"

        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function inv_ObtenerGastosProduccion(ByVal Id As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spGastosProduccion", Id).Tables(0)
    End Function
    Public Function inv_ObtenerGastosProduccionProd(ByVal Id As Integer, ByVal IdProducto As String) As DataTable
        Return db.ExecuteDataSet("inv_spGastosProduccionProductos", Id, IdProducto).Tables(0)
    End Function

    Public Function inv_ObtenerEsComponente(ByVal IdProducto As String) As DataTable
        Dim sql As String = "select IdProducto from inv_ProductosKit where IdProducto='" & IdProducto & "'"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function inv_ObtenerComisionProducto(ByVal IdProducto As String) As Decimal
        Dim sql As String = "select PorcComision from inv_Productos where IdProducto='" & IdProducto & "'"
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function

    Public Function inv_ReplicaProductos(ByVal entProducto As inv_Productos, ByVal Operacion As String, ByVal IdProducto As String) As String

        Dim sSQl As String = "Select IpServidor, InstanciaSql= InstaciaSql from Trans_transferenciasdetalle group by IpServidor, InstaciaSql"
        Dim dt As DataTable = db.ExecuteDataSet(CommandType.Text, sSQl).Tables(0)
        Dim Respuesta As String = ""

        For i = 3 To 10
            'Dim Cadena As String = "Data Source=" + dt.Rows(i).Item("IpServidor")
            'If dt.Rows(i).Item("InstanciaSql") <> "" Then
            '    Cadena += "\" + dt.Rows(i).Item("InstanciaSql") + ";Initial Catalog=Nexus000;User ID=sa;Password=Itosa08$"
            'Else
            '    Cadena += ";Initial Catalog=Nexus000;User ID=sa;Password=Itosa08$"
            'End If


            Dim Cadena As String = "cns" + i.ToString.PadLeft(2, "0")
            Try
                dbAux = DatabaseFactory.CreateDatabase(Cadena)
                Dim cn As DbConnection = dbAux.CreateConnection
                'cn.Open()
                'Dim tran As DbTransaction = cn.BeginTransaction()

                TablasAux = New TableData(Cadena)
                If Operacion = "I" Then
                    Dim sSQL1 As String = "Select count(*) from inv_productos where idproducto ='" & entProducto.IdProducto & "'"
                    Dim Existe As Integer = fd.SiEsNulo(dbAux.ExecuteScalar(CommandType.Text, sSQL1), 0)
                    If Existe = 0 Then
                        TablasAux.inv_ProductosInsert(entProducto)
                    End If
                End If
                If Operacion = "U" Then
                    TablasAux.inv_ProductosUpdate(entProducto)
                End If
                If Operacion = "E" Then
                    TablasAux.inv_ProductosDeleteByPK(IdProducto)
                End If

                'cn.Close()
            Catch ex As Exception
                'tran.Rollback()
                Respuesta += "Conexión: " + Cadena + " " + ex.Message + Chr(13)
            End Try

            ' tran.Commit()
        Next

        Return Respuesta
    End Function

    'PRODUCCIONES DE INVENTARIO
    Public Function inv_InsertarProducciones(ByRef Header As inv_Producciones, ByRef Detalle As List(Of inv_ProduccionesDetalle) _
    , ByRef DetGastos As List(Of inv_ProduccionesGastosDetalle), ByRef DetGastosProd As List(Of inv_ProduccionesGastosProductos)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            Header.IdComprobante = fd.ObtenerUltimoId("INV_PRODUCCIONES", "IdComprobante") + 1

            Tablas.inv_ProduccionesInsert(Header, tran)

            For Each det As inv_ProduccionesDetalle In Detalle
                det.IdComprobante = Header.IdComprobante
                Tablas.inv_ProduccionesDetalleInsert(det, tran)
            Next

            For Each det2 As inv_ProduccionesGastosDetalle In DetGastos
                det2.IdProduccion = Header.IdComprobante
                Tablas.inv_ProduccionesGastosDetalleInsert(det2, tran)
            Next

            For Each det3 As inv_ProduccionesGastosProductos In DetGastosProd
                det3.IdProduccion = Header.IdComprobante
                Tablas.inv_ProduccionesGastosProductosInsert(det3, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function

    Public Function inv_ActualizarProducciones(ByRef Header As inv_Producciones, ByRef Detalle As List(Of inv_ProduccionesDetalle) _
    , ByRef DetGastos As List(Of inv_ProduccionesGastosDetalle), ByRef DetGastosProd As List(Of inv_ProduccionesGastosProductos)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Try
            Tablas.inv_ProduccionesUpdate(Header, tran)
            Dim sSQL As String = "delete inv_ProduccionesDetalle where IdComprobante=" & Header.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)
            For Each det As inv_ProduccionesDetalle In Detalle
                Tablas.inv_ProduccionesDetalleInsert(det, tran)
            Next

            Dim sSQL1 As String = "delete inv_ProduccionesGastosDetalle where IdProduccion=" & Header.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL1)
            For Each det As inv_ProduccionesGastosDetalle In DetGastos
                Tablas.inv_ProduccionesGastosDetalleInsert(det, tran)
            Next

            Dim sSQL2 As String = "delete inv_ProduccionesGastosProductos where IdProduccion=" & Header.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL2)
            For Each det As inv_ProduccionesGastosProductos In DetGastosProd
                Tablas.inv_ProduccionesGastosProductosInsert(det, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message()
        End Try
        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function inv_ActualizarVineta(ByRef Detalle As List(Of inv_Vinetas)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            Dim sSQL As String = "delete inv_vinetas"

            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)
            For Each det As inv_Vinetas In Detalle
                Tablas.inv_VinetasInsert(det, tran)
            Next
        Catch ex As Exception
            tran.Rollback()
            Return ex.Message()
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function inv_ObtenerProductosKit(ByVal IdProducto As String) As DataTable
        Dim sql As String = "select k.IdKit,k.IdProducto,p.Nombre,k.Cantidad from inv_ProductosKit k inner join inv_Productos p on k.IdProducto=p.IdProducto where IdKit='" & IdProducto & "'"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function inv_InsertaProductoKit(ByVal IdProducto As String, ByRef ProductoKit As List(Of inv_ProductosKit)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Try
            Dim sSQL As String = "delete inv_ProductosKit where IdKit='" & IdProducto & "'"
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)
            For Each detalle As inv_ProductosKit In ProductoKit
                Tablas.inv_ProductosKitInsert(detalle, tran)
            Next
        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try
        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function inv_ObtenerProductosEtiquetas() As DataTable
        Return db.ExecuteDataSet("inv_spGenerarCodigosEtiquetas").Tables(0)
    End Function
    Public Function inv_ObtieneExistenciaFormulas(ByVal IdProducto As String, ByVal IdBodega As Integer, ByVal Fecha As DateTime) As DataTable
        Return db.ExecuteDataSet("inv_spVerificaSaldoFormulas", IdProducto, IdBodega, Fecha).Tables(0)
    End Function
    Public Function inv_ObtieneExistenciaProductoFormulas(ByVal IdProducto As String, ByVal IdBodega As Integer, ByVal Fecha As DateTime) As DataTable
        Return db.ExecuteDataSet("inv_spVerificaProductoFormulas", IdProducto, IdBodega, Fecha).Tables(0)
    End Function
    Public Function inv_ObtieneExistenciaKit(ByVal IdProducto As String, ByVal IdBodega As Integer, ByVal Fecha As DateTime) As DataTable
        Return db.ExecuteDataSet("inv_spVerificaSaldoKit", IdProducto, IdBodega, Fecha).Tables(0)
    End Function
    Public Sub inv_SubGruposInsert(ByVal entidad As inv_SubGrupos)

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Try
            entidad.IdSubGrupo = GetIdSubGrupo(entidad.IdGrupo, tran) + 1
            db.ExecuteNonQuery(tran, "inv_SubGruposInsert", _
             entidad.IdSubGrupo, entidad.IdGrupo, entidad.Nombre)
        Catch ex As Exception
            tran.Rollback()
            cn.Close()
            Exit Sub
        End Try
        tran.Commit()
        cn.Close()
    End Sub
    Public Function inv_ConsultaExportableExistenciasYprecios(ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal Fecha As DateTime) As DataTable
        Return db.ExecuteDataSet("inv_PreciosYExistenciasExpotables", IdGrupo, IdSubGrupo, Fecha).Tables(0)
    End Function
    Public Function inv_ExistenciasBodegasProductos(ByVal Fecha As DateTime, ByVal IdGrupo As String, ByVal IdSubGrupo As Integer) As DataTable
        'Return db.ExecuteDataSet("inv_spExistenciasBodegas", Fecha, IdGrupo, IdSubGrupo).Tables(0)
        Dim dt As DataTable = New DataTable()
        Using dbConn As New SqlClient.SqlConnection(db.ConnectionString)
            Using SQLComm As New SqlClient.SqlCommand("inv_spExistenciasBodegas", dbConn)
                SQLComm.CommandType = CommandType.StoredProcedure
                SQLComm.Parameters.Add("@HastaFecha", SqlDbType.DateTime).Value = Fecha
                SQLComm.Parameters.Add("@IdGrupo", SqlDbType.Int).Value = IdGrupo
                SQLComm.Parameters.Add("@IdSubGrupo", SqlDbType.Int).Value = IdSubGrupo
                SQLComm.CommandTimeout = 0
                Try
                    dbConn.Open()

                    'SQLComm.ExecuteNonQuery()
                    Dim da As New SqlClient.SqlDataAdapter(SQLComm)
                    da.Fill(dt)

                    dbConn.Close()
                    da.Dispose()
                Catch ex As Exception
                    'Return Nothing
                End Try
            End Using
        End Using
        Return dt
    End Function
    Public Function inv_ConsultaEntradas(ByVal Usuario As String) As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select e.IdComprobante, e.Numero, e.Fecha, e.IdSucursal, Left(e.Concepto,50) Concepto, e.AplicadaInventario, e.CreadoPor, e.FechaHoraCreacion "
        sql &= " from inv_Entradas e inner join adm_UsuariosSucursales  s on e.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"
        sql &= " order by e.idcomprobante desc ; select d.* from inv_EntradasDetalle d inner join inv_entradas e on d.idcomprobante = e.idcomprobante inner join adm_UsuariosSucursales  s on e.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"

        ds = db.ExecuteDataSet(CommandType.Text, sql)
        Dim dr As New DataRelation("EntradasDetalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function inv_RevierteTrasladoIngresado(ByVal Id As Integer) As String
        Return db.ExecuteScalar("inv_spRevierteTrasladoIngresado", Id)
    End Function
    Public Function inv_AplicarTrasladoInventarioIngreso(ByVal Id As Integer) As String
        Return db.ExecuteScalar("inv_spInsertKardexTrasladoIngreso", Id)
    End Function
    Public Function ConsultaTraslados(ByVal Desde As Date, ByVal Hasta As Date, ByVal USuario As String) As DataTable
        Return db.ExecuteDataSet("inv_spConsultaTraslados", Desde, Hasta, USuario).Tables(0)
    End Function
    Public Function inv_AprobarTraslado(ByVal IdDoc As Integer, ByVal AprobarReprobar As Integer, ByVal Comentario As String, ByVal AprobadoPor As String) As String
        Dim sSQL As String = ""
        If AprobarReprobar = 1 Then
            sSQL = "update inv_traslados set Aprobada =1,"
            sSQL += " AprobadaPor='" & AprobadoPor + "'"
            sSQL += " where IdComprobante = " & IdDoc
        Else
            sSQL = " update inv_Traslados set Aprobada =0, "
            sSQL += " ComentarioAprobacion= ' " & Comentario + "'"
            sSQL += " ,AprobadaPor='" & AprobadoPor + "'"
            sSQL += " where IdComprobante = " & IdDoc
        End If
        Return db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Function
    Public Function inv_ConsultaSalidas(ByVal Usuario As String) As DataSet

        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select e.IdComprobante, e.Numero, e.Fecha, e.IdSucursal, Left(e.Concepto,50) Concepto, e.AplicadaInventario, e.CreadoPor, e.FechaHoraCreacion "
        sql &= " from inv_Salidas e inner join adm_UsuariosSucursales  s on e.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"
        sql &= " order by e.idcomprobante desc ; select d.* from inv_SalidasDetalle d inner join inv_Salidas e on d.idcomprobante = e.idcomprobante inner join adm_UsuariosSucursales  s on e.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("SalidasDetalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function inv_ConsultaTraslados(ByVal Usuario As String) As DataSet


        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select e.IdComprobante, e.Numero, e.Fecha, e.IdSucursal,e.IdSucursalRecibe, Left(e.Concepto,50) Concepto, AplicadaInventario,Aprobada,e.ComentarioAprobacion,e.AprobadaPor,e.CreadoPor, e.FechaHoraCreacion "
        sql &= " from inv_Traslados e inner join adm_UsuariosSucursales  s on e.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"
        sql &= " order by e.idcomprobante desc ; select d.* from inv_TrasladosDetalle d inner join inv_Traslados e on d.idcomprobante = e.idcomprobante inner join adm_UsuariosSucursales  s on e.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("TrasladosDetalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function inv_ConsultaProducciones(ByVal Usuario As String) As DataSet

        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select p.IdComprobante, p.Numero, p.Fecha, p.IdSucursal, Left(p.Concepto,50) Concepto, p.CreadoPor, p.FechaHoraCreacion "
        sql &= " from inv_Producciones p inner join adm_UsuariosSucursales  s on p.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"
        sql &= " order by p.idcomprobante; select d.* from inv_ProduccionesDetalle d inner join inv_Producciones p on d.idcomprobante = d.idcomprobante inner join adm_UsuariosSucursales  s on p.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("ProduccionesDetalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function inv_ActualizarPrecios(ByVal IdProducto As String, ByVal Detalle As List(Of inv_ProductosPrecios)) As String

        db.ExecuteNonQuery("inv_spProductosPreciosDelete", IdProducto)

        For Each entDetalle As inv_ProductosPrecios In Detalle
            db.ExecuteNonQuery("inv_ProductosPreciosInsert", entDetalle.IdProducto, entDetalle.IdPrecio, entDetalle.Precio, _
            entDetalle.Descuento, entDetalle.Margen, entDetalle.CreadoPor, entDetalle.FechaHoraCreacion)
        Next


        Return ""
    End Function
    Public Function inv_ObtenerSucursalesCategorias(ByVal IdCategoria As Integer) As DataTable
        Dim sSQL As String = "select s.IdSucursal, IdCuentaIngreso=isnull(d.IdCuentaIngreso,'') from adm_Sucursales s"
        sSQL += " left join inv_CategoriasDetalle d on s.IdSucursal = d.idsucursal and d.idCategoria=" & IdCategoria
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function

    Public Function inv_ObtenerListaProductos() As DataTable
        Dim sql As String = "select IdProducto from inv_productos order by FechaHoraCreacion"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function inv_VerificaProductoFormula(ByVal IdProducto As String, ByVal Tipo As Integer) As Integer
        Dim sql As String = ""

        If Tipo = 1 Then
            sql = "select count(*) from inv_Formulas where IdProducto='" & IdProducto & "'"
        Else
            sql = "select count(*) from inv_Formulas where IdProducto='" & IdProducto & "'"
        End If

        Return db.ExecuteScalar(CommandType.Text, fd.SiEsNulo(sql, 0))
    End Function
    Public Function inv_ObtenerDetalleDocumentoFormula(ByVal IdComprobante As Integer, ByVal Tabla As String) As DataTable
        Dim sql As String = "select * from " & Tabla & " where IdFormula=" & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function inv_GeneralesProducto(ByVal IdProducto) As DataTable
        Return db.ExecuteDataSet("inv_spGeneralesProducto", IdProducto).Tables(0)
    End Function
    Public Function inv_ListaDeProductos(ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spListaPrecios", IdGrupo, IdSubGrupo).Tables(0)
    End Function
    Public Function inv_ListaDeProductosCategorias(ByVal IdCategoria As Integer, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal IdBodega As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spListaProductosCategorias", IdCategoria, IdGrupo, IdSubGrupo, IdBodega).Tables(0)
    End Function
    Public Function inv_ListaDeProductosMaximosMinimos(ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal Hasta As Date, ByVal TipoReporte As Integer, ByVal SoloAplican As Boolean) As DataTable
        Return db.ExecuteDataSet("inv_spMaximosMinimos", IdGrupo, IdSubGrupo, Hasta, TipoReporte, SoloAplican).Tables(0)
    End Function

    Public Function inv_VerificaCodigoProducto(ByVal IdProducto As String) As Boolean
        Return db.ExecuteScalar(CommandType.Text, "select PermiteFacturar from inv_productos where IdProducto = '" & IdProducto & "'") = True
    End Function
    Public Function inv_ObtieneExistenciaProducto(ByVal IdProducto As String, ByVal IdBodega As Integer, ByVal Fecha As Date) As DataTable
        Return db.ExecuteDataSet("inv_spVerificaSaldo", IdProducto, IdBodega, Fecha).Tables(0)
    End Function
    Public Function inv_ObtieneExistenciaProductoTraslado(ByVal IdProducto As String, ByVal IdBodega As Integer, ByVal Fecha As Date) As DataTable
        Return db.ExecuteDataSet("inv_spVerificaSaldoTraslado", IdProducto, IdBodega, Fecha).Tables(0)
    End Function
    Public Function inv_ObtieneTomaFisica(ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal IdBodega As Integer, ByVal Fecha As DateTime) As DataTable
        Return db.ExecuteDataSet("inv_spObtieneTomaFisica", IdGrupo, IdSubGrupo, IdBodega, Fecha).Tables(0)
    End Function
    Public Function inv_ObtieneExistencia(ByVal IdProducto As String, ByVal Fecha As DateTime, ByVal IdBodega As Integer) As Decimal
        Return db.ExecuteScalar("inv_spObtieneExistencia", IdProducto, Fecha, IdBodega)
    End Function
    Public Function inv_ObtienePrecioProducto(ByVal IdProducto As String, ByVal IdPrecio As Integer) As Decimal
        Dim sql As String = "select Precio from inv_ProductosPrecios where IdProducto = '" & IdProducto & "' and IdPrecio="
        sql &= IdPrecio.ToString
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function

    Public Function inv_ObtieneDescuentoMargen(ByVal Cantidad As Decimal) As Decimal
        Dim sql As String = "SELECT Valor from inv_ProductosMargenes where " & Cantidad & ">=Desde AND " & Cantidad & "<(Hasta+1)"
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function

    Public Function inv_ObtieneCodigoProducto(ByVal IdGrupo As Integer) As String
        If IdGrupo < 1 Then
            Return ""
        Else
            Dim sql As String = "select UltCodProducto+1 from inv_grupos where IdGrupo = " & IdGrupo
            Dim IdProduc As String = db.ExecuteScalar(CommandType.Text, sql).ToString()
            IdProduc = IdGrupo.ToString().PadLeft(3, "0") & IdProduc.PadLeft(3, "0")
            Return IdProduc
        End If

    End Function
    Public Sub inv_ActualizaUltimoCorrelativoProducto(ByVal IdGrupo As Integer)
        Dim sql As String = "update inv_grupos set UltCodProducto = UltCodProducto +1 where IdGrupo = " & IdGrupo
        db.ExecuteNonQuery(CommandType.Text, sql)
    End Sub
    Public Function inv_InsertarFormulas(ByRef Header As inv_Formulas, ByRef Detalle As List(Of inv_FormulasDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            Header.IdFormula = fd.ObtenerUltimoId("INV_FORMULAS", "IdFormula") + 1

            Tablas.inv_FormulasInsert(Header, tran)

            For Each det As inv_FormulasDetalle In Detalle
                det.IdFormula = Header.IdFormula
                Tablas.inv_FormulasDetalleInsert(det, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function

    Public Function inv_ActualizarFormulas(ByRef Header As inv_Formulas, ByRef Detalle As List(Of inv_FormulasDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Try
            Tablas.inv_FormulasUpdate(Header, tran)
            Dim sSQL As String = "delete inv_FormulasDetalle where IdFormula=" & Header.IdFormula
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)
            For Each det As inv_FormulasDetalle In Detalle
                Tablas.inv_FormulasDetalleInsert(det, tran)
            Next
        Catch ex As Exception
            tran.Rollback()
            Return ex.Message()
        End Try
        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function inv_InsertarCategorias(ByRef entCategoria As inv_Categorias, ByRef Detalle As List(Of inv_CategoriasDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim IdProd As String = ""
        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            entCategoria.IdCategoria = fd.ObtenerUltimoId("INV_CATEGORIAS", "IdCategoria") + 1
            Tablas.inv_CategoriasInsert(entCategoria, tran)

            For Each det As inv_CategoriasDetalle In Detalle
                det.IdCategoria = entCategoria.IdCategoria
                Tablas.inv_CategoriasDetalleInsert(det, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function inv_VerificaProductoExiste(ByRef IdProducto As String) As String
        Dim sql
        sql = "select isnull(IdProducto,'') from inv_productos  where IdProducto ='" & IdProducto & "'"
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function inv_InsertaProductosMasivos(ByRef DetalleProductos As List(Of inv_Productos), ByRef DetalleProductosPrecios As List(Of inv_ProductosPrecios)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Try
            For Each det As inv_Productos In DetalleProductos
                Tablas.inv_ProductosInsert(det, tran)
            Next
            For Each det1 As inv_ProductosPrecios In DetalleProductosPrecios
                Tablas.inv_ProductosPreciosInsert(det1, tran)
            Next
        Catch ex As Exception
            tran.Rollback()
            Return ex.Message()
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function inv_ActualizarCategorias(ByRef entCategoria As inv_Categorias, ByRef Detalle As List(Of inv_CategoriasDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            Tablas.inv_CategoriasUpdate(entCategoria, tran)
            Dim sSQL As String = "delete inv_CategoriasDetalle where Idcategoria=" & entCategoria.IdCategoria
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)
            For Each det As inv_CategoriasDetalle In Detalle
                Tablas.inv_CategoriasDetalleInsert(det, tran)
            Next
        Catch ex As Exception
            tran.Rollback()
            Return ex.Message()
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    'ENTRADAS DE INVENTARIO
    Public Function inv_InsertarEntrada(ByRef Header As inv_Entradas, ByRef Detalle As List(Of inv_EntradasDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim IdProd As String = ""
        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            Header.IdComprobante = fd.ObtenerUltimoId("INV_ENTRADAS", "IdComprobante") + 1

            If Header.Numero = "" Then ' solo si no le colocan el correlativo, lo generara automaticamente
                Dim UltimoNumero As Integer = 0
                UltimoNumero = fd.CorrelativosInv("ENTRADAS_INV")
                Header.Numero = UltimoNumero.ToString.PadLeft(6, "0")
            End If


            Tablas.inv_EntradasInsert(Header, tran)

            For Each det As inv_EntradasDetalle In Detalle
                det.IdComprobante = Header.IdComprobante
                IdProd = det.IdProducto
                Tablas.inv_EntradasDetalleInsert(det, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message & ", " & IdProd
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function inv_ActualizarEntrada(ByRef Header As inv_Entradas, ByRef Detalle As List(Of inv_EntradasDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            Tablas.inv_EntradasUpdate(Header, tran)
            Dim sSQL As String = "delete inv_EntradasDetalle where IdComprobante=" & Header.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)
            For Each det As inv_EntradasDetalle In Detalle
                Tablas.inv_EntradasDetalleInsert(det, tran)
            Next
        Catch ex As Exception
            tran.Rollback()
            Return ex.Message()
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function inv_ObtenerIdDocumento(ByVal Id As Integer, ByVal TipoAvance As Integer, ByVal Tabla As String) As Integer
        Dim sql As String = "select isnull(min(IdComprobante),0) from " & Tabla & " where IdComprobante > " & Id
        If TipoAvance = -1 Then  'para obtener el registro anterior
            sql = "select isnull(max(IdComprobante),0) from " & Tabla & " where IdComprobante < " & Id
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function

    'SALIDAS DE INVENTARIO
    Public Function inv_InsertarSalida(ByRef Header As inv_Salidas, ByRef Detalle As List(Of inv_SalidasDetalle), ByVal AutorizoReversion As String) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            Header.IdComprobante = fd.ObtenerUltimoId("INV_SALIDAS", "IdComprobante") + 1
            If Header.Numero = "" Then ' solo si no le colocan el correlativo, lo generara automaticamente
                Dim UltimoNumero As Integer = 0
                UltimoNumero = fd.CorrelativosInv("SALIDAS_INV")
                Header.Numero = UltimoNumero.ToString.PadLeft(6, "0")
            End If

            Tablas.inv_SalidasInsert(Header, tran)

            For Each det As inv_SalidasDetalle In Detalle
                det.IdComprobante = Header.IdComprobante
                Tablas.inv_SalidasDetalleInsert(det, tran)
            Next

            If AutorizoReversion <> "" Then
                Dim sSQL As String = "insert into adm_bitacorasucesos values(5, " & Header.IdComprobante
                sSQL += ", 'Autorizo Salida Inventario Negativo'," & Header.Numero & ",'" & AutorizoReversion & "',getdate(),"
                sSQL += " '', NULL, '',NULL, '',NULL) "
                db.ExecuteNonQuery(CommandType.Text, sSQL)
            End If

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function inv_ActualizarSalida(ByRef Header As inv_Salidas, ByRef Detalle As List(Of inv_SalidasDetalle), ByVal AutorizoReversion As String) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            Tablas.inv_SalidasUpdate(Header, tran)
            Dim sSQL As String = "delete inv_SalidasDetalle where IdComprobante=" & Header.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)
            For Each det As inv_SalidasDetalle In Detalle
                Tablas.inv_SalidasDetalleInsert(det, tran)
            Next
            If AutorizoReversion <> "" Then
                Dim sSQL1 As String = "insert into adm_bitacorasucesos values(5, " & Header.IdComprobante
                sSQL1 += ", 'Autorizo Salida Inventario Negativo'," & Header.Numero & ",'' , NULL, '"
                sSQL1 += AutorizoReversion & "', getdate(), '',NULL, '',NULL) "
                db.ExecuteNonQuery(CommandType.Text, sSQL1)
            End If
        Catch ex As Exception
            tran.Rollback()
            Return ex.Message()
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    'Public Function inv_ObtenerIdSalida(ByVal Id As Integer, ByVal TipoAvance As Integer) As Integer
    '    Dim sql As String = "select isnull(min(IdComprobante),0) from inv_Salidas where IdComprobante > " & Id
    '    If TipoAvance = -1 Then  'para obtener el registro anterior
    '        sql = "select isnull(max(IdComprobante),0) from inv_Salidas where IdComprobante < " & Id
    '    End If
    '    Return db.ExecuteScalar(CommandType.Text, sql)
    'End Function
    Public Function inv_ObtenerDetalleDocumento(ByVal IdComprobante As Integer, ByVal Tabla As String) As DataTable
        Dim sql As String = "select * from " & Tabla & " where IdComprobante=" & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Function inv_ProrrateoProduccion(ByVal IdComprobante As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spProrrateoProduccion", IdComprobante, 0).Tables(0)
    End Function
    Public Function inv_ActualizaPedidoTraslado(ByVal Idtraslado As Integer, ByVal IdPedido As Integer, ByVal Aplicacion As Integer) As String

        Dim sql As String = "update fac_pedidos set facturado=" & Aplicacion & " , IdComprobanteFactura=" & IIf(Aplicacion > 0, Idtraslado, Aplicacion)
        sql += " where IdComprobante= " & IdPedido
        Return db.ExecuteNonQuery(CommandType.Text, sql)

    End Function
    'TRASLADOS DE INVENTARIO
    Public Function inv_InsertarTraslado(ByRef Header As inv_Traslados, ByRef Detalle As List(Of inv_TrasladosDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            Header.IdComprobante = fd.ObtenerUltimoId("INV_TRASLADOS", "IdComprobante") + 1
            'If Header.Numero = "" Then ' solo si no le colocan el correlativo, lo generara automaticamente
            Dim UltimoNumero As Integer = 0
            UltimoNumero = fd.CorrelativosInv("TRASLADOS_INV")
            'Header.Numero = UltimoNumero.ToString.PadLeft(6, "0")
            'End If

            Tablas.inv_TrasladosInsert(Header, tran)

            For Each det As inv_TrasladosDetalle In Detalle
                det.IdComprobante = Header.IdComprobante
                Tablas.inv_TrasladosDetalleInsert(det, tran)
            Next


        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function inv_ActualizarTraslado(ByRef Header As inv_Traslados, ByRef Detalle As List(Of inv_TrasladosDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            Tablas.inv_TrasladosUpdate(Header, tran)
            Dim sSQL As String = "delete inv_TrasladosDetalle where IdComprobante=" & Header.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)
            For Each det As inv_TrasladosDetalle In Detalle
                Tablas.inv_TrasladosDetalleInsert(det, tran)
            Next
        Catch ex As Exception
            tran.Rollback()
            Return ex.Message()
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function inv_ObtieneCreaProdcutosEstructura() As DataTable
        Dim sSQL As String = "Select IdProducto, Nombre , UnidadMedida=convert(int,1),Existe=Convert(int,0), PrecioCosto, IdSubGrupo=convert(int,1),PrecioFinal=convert(decimal(18,2),0),PrecioMayoreo=convert(decimal(18,2),0),Precio1=convert(decimal(18,2),0),IdGrupo=convert(int,1) from inv_productos where idproducto ='-656566'"
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function inv_ObtieneTomaFisicaEstructura() As DataTable
        Dim sSQL As String = "Select IdProducto, Nombre , UnidadMedida=convert(varchar(50),''), PrecioCosto, Conteo=convert(decimal(18,6),0), Acumulado=convert(decimal(18,6),0), Existencias=convert(decimal(18,6),0), Diferencia=convert(decimal(18,6),0), Grupo=convert(varchar(50),'') from inv_productos where idproducto ='-656566'"
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function ExisteNumeroTraslado(ByVal Numero As String) As Integer
        Dim sql As String = String.Format("select IdComprobante from inv_Traslados where Numero= '{0}'", Numero)
        Dim i As Integer = SiEsNulo(db.ExecuteScalar(CommandType.Text, sql), 0)
        Return i
    End Function

    Public Function SiEsNulo(ByVal value As Object, ByVal ValueNoNull As Object) As Object
        If value Is Nothing Then Return Nothing
        If IsDBNull(value) Then Return ValueNoNull
        Return value
    End Function
    Public Function inv_ActualizaPreciosVentaImportacion(ByVal Id As Integer) As String
        Dim sql As String = "update inv_ProductosPrecios set precio=n.precioventanuevo from inv_ProductosPrecios   fa inner join com_ImportacionesDetalle  n"
        sql += " on fa.idproducto = n.Idproducto where  fa.IdPrecio =1 and  n.IdComprobante = " & Id

        sql += "; update inv_Productos set porcComision=n.porcComision from inv_Productos  pro inner join com_ImportacionesDetalle  n"
        sql += " on pro.idproducto = n.Idproducto where n.IdComprobante = " & Id
        Return db.ExecuteScalar(CommandType.Text, sql)

    End Function


    Public Function inv_ObtenerDataInventario(ByVal Desde As Date, ByVal Hasta As Date)
        Dim sql As String = ""
        sql += "select ik.IdComprobante, ik.Numero, ik.Fecha, ik.Numero, ik.IdProducto, ip.Nombre, "
        sql += "UnidadMedida = iu.Nombre, Marca = im.Nombre, "
        sql += "Entrada=case when TipoAplicacion=1 then Cantidad else 0 end, "
        sql += "Salida=case when TipoAplicacion=1 then 0 else Cantidad end, "
        sql += "ik.PrecioUnitario, ik.precioCosto, ik.SaldoExistencias "
        sql += "from inv_kardex ik "
        sql += "inner join inv_productos ip on ik.IdProducto = ip.IdProducto "
        sql += "inner join inv_UnidadesMedida iu on ip.IdUnidad = iu.IdUnidad "
        sql += "inner join inv_Marcas im on ip.IdMarca = im.IdMarca "
        sql += "where ik.Fecha >= @Desde and ik.Fecha <= dateadd(dd,1,@Hasta)"

        Dim cmd As DbCommand = db.GetSqlStringCommand(sql)
        db.AddInParameter(cmd, "@Desde", DbType.Date, Desde)
        db.AddInParameter(cmd, "@Hasta", DbType.Date, Hasta)

        Return db.ExecuteDataSet(cmd).Tables(0)
    End Function
    Public Function inv_AplicarTransaccionInventario(ByVal Id As Integer, ByVal Modulo As String, ByVal Tipo As String, ByVal IdTipoComprobante As Integer, ByVal TipoTraslado As Integer) As String
        Dim store As String = ""
        Select Case Modulo
            Case "Compra"
                If Tipo = "I" Then
                    store = "inv_spInsertKardexCompras"
                Else
                    store = "inv_spDeleteKardexCompras"
                End If
            Case "Importacion"
                If Tipo = "I" Then
                    store = "inv_spInsertKardexImportacion"
                Else
                    store = "inv_spDeleteKardexImportacion"
                End If
            Case "Entrada"
                If Tipo = "I" Then
                    store = "inv_spInsertKardexEntradas"
                Else
                    store = "inv_spDeleteKardexEntradas"
                End If
            Case "EntradaProduccion"
                If Tipo = "I" Then
                    store = "inv_spInsertKardexEntradasProduccion"
                Else
                    store = "inv_spDeleteKardexEntradasProduccion"
                End If

            Case "Salida"
                If Tipo = "I" Then
                    store = "inv_spInsertKardexSalidas"
                Else
                    store = "inv_spDeleteKardexSalidas"
                End If
            Case "SalidaProduccion"
                If Tipo = "I" Then
                    store = "inv_spInsertKardexSalidasProduccion"
                Else
                    store = "inv_spDeleteKardexSalidasProduccion"
                End If
            Case "Ventas"
                If Tipo = "I" Then
                    store = "inv_spInsertKardexVentas"
                Else
                    store = "inv_spDeleteKardexVentas"
                End If
            Case "Traslado"
                If Tipo = "I" Then
                    store = "inv_spInsertKardexTraslado"
                Else
                    store = "inv_spDeleteKardexTraslado"
                End If
            Case "Produccion"
                If Tipo = "I" Then
                    store = "inv_spInsertKardexProduccion"
                Else
                    store = "inv_spDeleteKardexProduccion"
                End If
            Case "OrdenProduccion"
                If Tipo = "I" Then
                    store = "inv_spInsertKardexOrdenProduccion"
                Else
                    store = "inv_spDeleteKardexOrdenProduccion"
                End If
            Case "OrdenesEmpaque"
                If Tipo = "I" Then
                    store = "inv_spInsertKardexOrdenEmpaque"
                Else
                    store = "inv_spDeleteKardexOrdenEmpaque"
                End If
        End Select

        Dim dcCommand As System.Data.Common.DbCommand
        If Modulo = "Traslado" Then
            dcCommand = db.GetStoredProcCommand(store, Id, TipoTraslado)
        Else
            dcCommand = db.GetStoredProcCommand(store, Id)
        End If
        dcCommand.CommandTimeout = 60000

        Dim dcCommand2 As System.Data.Common.DbCommand
        dcCommand2 = db.GetStoredProcCommand("inv_spRenumerarMovimientos_Existencia", Id, IdTipoComprobante)
        dcCommand2.CommandTimeout = 60000

        Dim dcCommand3 As System.Data.Common.DbCommand
        dcCommand3 = db.GetStoredProcCommand("inv_spRecalculoCosto", Id, IdTipoComprobante)
        dcCommand3.CommandTimeout = 60000

        Dim msj As String = ""

        msj = db.ExecuteScalar(dcCommand) 'ejecuta el stored de insert o eliminacion al kardex
        If Tipo = "I" Then
            If msj = "Ok" Then ' SI INSERTO CORRECTAMENTE, EJECUTO LA RENUMERCACION DE MOVIMIENTO Y EXISTENCIAS
                msj = db.ExecuteScalar(dcCommand2)

                If msj = "Ok" Then ' SE RENUMERA EL IDMOVPRODUCTO Y EL COSTO
                    msj = db.ExecuteScalar(dcCommand3)
                End If
            End If
        End If

        Return msj

    End Function

    Public Function inv_ObtieneExistenciaCostoProducto(ByVal IdProducto As String, ByVal IdBodega As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spVerificaSaldoCosto", IdProducto, IdBodega).Tables(0)
    End Function
    Public Function inv_contabilizar(ByVal Desde As Date, ByVal Hasta As Date, ByVal TipoPartida As String, ByVal IdSucursal As Integer, ByVal Usuario As String, ByVal Concepto As String) As String
        Return db.ExecuteScalar("inv_spContabilizar", Desde, Hasta, TipoPartida, IdSucursal, Usuario, Concepto)
    End Function

    Public Function inv_RecalculaCostosExistencias(IdBodega As Integer, Hasta As Date, Tipo As Int16) As String
        Dim msj As String = "Ok"
        Try
            Dim dcCommand As Data.Common.DbCommand
            If Tipo = 0 Then 'COSTO PROMEDIO ESTANDARD
                dcCommand = db.GetStoredProcCommand("inv_spRecalculoCosto", -1)
                dcCommand.CommandTimeout = 100000 '10 minutos
                db.ExecuteScalar(dcCommand)
            End If
            If Tipo = 1 Then 'COSTO PROMEDIO Aligacion Directa
                dcCommand = db.GetStoredProcCommand("inv_spCalcularCostoPorAligacionDirecta", IdBodega, Hasta)
                dcCommand.CommandTimeout = 100000 ' 10 minutos
                db.ExecuteScalar(dcCommand)
            End If
            If Tipo = 2 Then 'las Existencias
                dcCommand = db.GetStoredProcCommand("inv_spRecalculoExistencias", IdBodega, Hasta)
                dcCommand.CommandTimeout = 60000 ' 6 minutos
                db.ExecuteScalar(dcCommand)
            End If

        Catch ex As Exception
            msj = ex.Message
        End Try
        Return msj
    End Function
     

#Region "Reportes"
    Public Function inv_ReporteKardex(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProducto As String, ByVal IdBodega As Integer, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer)
        Return db.ExecuteDataSet("inv_spKardex", Desde, Hasta, IdProducto, IdBodega, IdGrupo, IdSubGrupo).Tables(0)
    End Function
    Public Function inv_KardexBodega(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProducto As String, ByVal IdBodega As Integer)
        Return db.ExecuteDataSet("inv_spKardexBodega", Desde, Hasta, IdProducto, IdBodega, 1).Tables(0)
    End Function
    Public Function inv_ObtienePrecios(ByVal IdProducto As String, ByVal Usuario As String, pnIVA As Decimal) As DataTable
        ' Dim factiva As Decimal = db.ExecuteScalar(CommandType.Text, "select top 1 (1+(PorcIva/100)) from adm_parametros")
        Dim sIVA As String = pnIVA + 1.ToString()
        Dim sql As String = "select ip.IdPrecio, Precio=isnull(pp.Precio,0.0), Descuento=isnull(pp.Descuento,0.0), Margen=isnull(pp.Margen,0.0), PrecioNeto = isnull((pp.Precio/" & sIVA & "), 0.0) from inv_Precios ip left join inv_ProductosPrecios pp on ip.IdPrecio = pp.IdPrecio and pp.IdProducto = '"
        sql &= IdProducto & "'"
        sql &= " inner join adm_UsuariosPrecios usu ON ip.IdPrecio = usu.IdPrecio AND usu.IdUsuario ='" & Usuario & "'"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function inv_VerificaPrecioUsuario(ByVal IdUsuario As String, ByVal IdPrecio As Integer) As Boolean
        Dim sql As String = "select 1 from adm_UsuariosPrecios where IdUsuario = '" & IdUsuario & "' and IdPrecio = " & IdPrecio
        Dim Res As Boolean = db.ExecuteScalar(CommandType.Text, sql) = 0
        Return Res
    End Function
    Public Function inv_ObtienePrecioVenta(ByVal IdProducto As String, ByVal IdPrecio As Integer) As Decimal
        Dim sql As String = "select Precio from inv_ProductosPrecios where IdPrecio = "
        sql &= IdPrecio & " and IdProducto = '" & IdProducto & "'"
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function inv_ObtieneSaldosPorProducto(ByVal IdBodega As Integer, ByVal IdProducto As String)
        Return db.ExecuteDataSet("inv_spObtieneSaldosPorProducto", IdBodega, IdProducto).Tables(0)
    End Function
    'Public Function inv_SaldosBodega(ByVal IdBodega As Integer, ByVal IdProducto As String)
    '    Return db.ExecuteDataSet("inv_spSaldosBodega", IdBodega, IdProducto).Tables(0)
    'End Function
    Public Function inv_InventarioValuado(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal Archivo As Boolean, ByVal IdCategoria As Integer) As DataTable
        Dim dcCommand As Data.Common.DbCommand
        dcCommand = db.GetStoredProcCommand("inv_spInventarioValuado", Desde, Hasta, IdBodega, IdGrupo, IdSubGrupo, Archivo, IdCategoria)
        dcCommand.CommandTimeout = 60000 ' 6 minutos
        Return db.ExecuteDataSet(dcCommand).Tables(0)

        'Return db.ExecuteDataSet("inv_spInventarioValuado", Desde, Hasta, IdBodega, IdGrupo, IdSubGrupo, Archivo, IdCategoria).Tables(0)
    End Function

    Public Function inv_InventarioValuadoProveedor(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer _
                                                   , ByVal Archivo As Boolean, ByVal IdCategoria As Integer, ByVal IdProveedor As String) As DataTable

        Dim dcCommand As Data.Common.DbCommand
        dcCommand = db.GetStoredProcCommand("inv_spInventarioValuadoProveedor", Desde, Hasta, IdBodega, IdGrupo, IdSubGrupo, Archivo, IdCategoria, IdProveedor)
        dcCommand.CommandTimeout = 60000 ' 6 minutos
        Return db.ExecuteDataSet(dcCommand).Tables(0)

        'Return db.ExecuteDataSet("inv_spInventarioValuadoProveedor", Desde, Hasta, IdBodega, IdGrupo, IdSubGrupo, Archivo, IdCategoria, IdProveedor).Tables(0)
    End Function

    Public Function inv_InventarioSinMovimiento(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProducto As String, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spInventarioSinMovimientos", Desde, Hasta, IdProducto, IdGrupo, IdSubGrupo).Tables(0)
    End Function

    Public Function inv_InventarioUnidades(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal IdCategoria As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spInventarioUnidades", Desde, Hasta, IdBodega, IdGrupo, IdSubGrupo, IdCategoria).Tables(0)
    End Function
    Public Function inv_InventarioTomaFisica(ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spInventarioTomaFisica", Hasta, IdBodega, IdGrupo, IdSubGrupo).Tables(0)
    End Function
    Public Function inv_InventarioTomaFisica(ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdProveedor As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spInventarioTomaFisicaProveedor", Hasta, IdBodega, IdProveedor).Tables(0)
    End Function
    Public Function inv_UtilidadesVentas(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spVentasCostos", Desde, Hasta, IdBodega).Tables(0)
    End Function
    Public Function inv_spVentasCostosPorDoc(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spVentasCostosPorDoc", Desde, Hasta, IdBodega).Tables(0)
    End Function
    Public Function inv_UtilidadesVentasdetalle(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdCliente As String, ByVal IdGrupo As Integer, ByVal IdSubGrupo As Integer, ByVal PorGrupo As Boolean, ByVal Utilidad As Decimal) As DataTable
        Return db.ExecuteDataSet("inv_spVentasCostosDetalle", Desde, Hasta, IdBodega, IdCliente, IdGrupo, IdSubGrupo, PorGrupo, Utilidad).Tables(0)
    End Function
    Public Function inv_CargosAbonos(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spCargosAbonos", Desde, Hasta, IdBodega).Tables(0)
    End Function
    Public Function inv_spRptGiroProducto(ByVal IdProducto As String, ByVal Annio As Integer, ByVal Mes As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spRptGiroProducto", IdProducto, Annio, Mes).Tables(0)
    End Function


    Public Function inv_ObtenerMovimientos(ByVal Desde As Date, ByVal Hasta As Date, ByVal Tipo As Integer, ByVal IdCentro As String) As DataTable
        Return db.ExecuteDataSet("inv_spListMovimientos", Desde, Hasta, Tipo, IdCentro).Tables(0)
    End Function
    Public Function inv_AutorizoReversionInventario(ByVal IdUsuario As String, ByVal Tipo As String, ByVal Numero As String, ByVal Id As Integer) As String
        Dim sSQL1 As String = "insert into adm_bitacorasucesos values(5, " & Id
        sSQL1 += ", 'Autorizo " & Tipo & " Inventario Negativo'," & Numero & ",'' , NULL, '"
        sSQL1 += IdUsuario & "', getdate(), '',NULL, '',NULL) "
        Return db.ExecuteNonQuery(CommandType.Text, sSQL1)
    End Function

#End Region

#Region "Produccion"
    Public Function GetOrdenManoObra(ByVal IdComprobante As Integer) As DataTable
        Dim sSQL As String = "select * from pro_OrdenesProduccionDetalleManoObra "
        sSQL &= "where IdComprobante=" & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function pro_ValorManoObra(ByVal Fecha1 As DateTime, ByVal Fecha2 As DateTime, ByVal IdEmpleado As Integer) As Decimal
        Return db.ExecuteScalar("pro_spValorManoObra", Fecha1, Fecha2, IdEmpleado)
    End Function

    Public Function pro_ConsultaEntradas() As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select e.IdComprobante, e.Numero, e.Fecha, e.IdOrden, NumeroOrden=isnull(o.NumeroOrden,''),  Left(e.Concepto,50) Concepto, e.CreadoPor, e.FechaHoraCreacion "
        sql &= " from pro_EntradasProduccion e left join pro_OrdenesProduccion o on e.idorden= o.idcomprobante  "
        sql &= " order by e.Fecha desc ; select d.* from pro_EntradasProduccionDetalle d inner join pro_EntradasProduccion e on d.idcomprobante = e.idcomprobante"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("EntradasDetalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function pro_ConsultaOrdenEmpaque() As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select e.IdComprobante, NumeroOrden=e.Numero, e.Fecha,  Left(e.Concepto,50) Concepto, e.CreadoPor, e.FechaHoraCreacion "
        sql &= " from pro_OrdenesEmpaque e "
        sql &= " order by e.Fecha desc ; select d.* from pro_OrdenesEmpaqueDetalleSalida d inner join pro_OrdenesEmpaque e on d.idcomprobante = e.idcomprobante"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("EntradasDetalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function

    Public Function pro_ConsultaSalidas() As DataSet


        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select e.IdComprobante, e.Numero, e.Fecha, e.IdOrden, NumeroOrden=isnull(o.NumeroOrden,''),  Left(e.Concepto,50) Concepto, e.CreadoPor, e.FechaHoraCreacion "
        sql &= " from pro_SalidasProduccion e left join pro_OrdenesProduccion o on e.idorden= o.idcomprobante  "
        sql &= " order by e.Fecha desc ; select d.* from pro_SalidasProduccionDetalle d inner join pro_SalidasProduccion e on d.idcomprobante = e.idcomprobante"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("SalidasDetalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function pro_ObtenerDetalleSalidas(ByVal IdComprobante As Integer) As DataTable
        Dim sql As String = "select * from pro_salidasproducciondetalle where IdComprobante in (select idcomprobante from pro_salidasproduccion where idcomprobante=" & IdComprobante
        sql += " )"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function pro_ConsultaOrdenes() As DataSet


        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select e.IdComprobante, e.NumeroOrden, FechaOrden=e.fechaorden,e.FechaEntrega,e.Nombre, e.CreadoPor, e.FechaHoraCreacion "
        sql &= " from pro_OrdenesProduccion e "
        sql &= " order by e.idcomprobante desc "
        'sql &= " order by e.Fecha desc ; select d.* from pro_requisicion d inner join pro_OrdenesProduccion e on e.idcomprobante = d.idOrden"


        ds = db.ExecuteDataSet(CommandType.Text, sql)
        Return ds
    End Function
    Public Function pro_InsertarOrdenesProduccion(ByRef entOrden As pro_OrdenesProduccion, ByRef entManoObra As List(Of pro_OrdenesProduccionDetalleManoObra), ByRef entDet1 As List(Of pro_OrdenesGasDirectos), ByRef entDet2 As List(Of pro_OrdenesGasIndirectos)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            entOrden.IdComprobante = fd.ObtenerUltimoId("PRO_ORDENESPRODUCCION", "IdComprobante") + 1
            Tablas.pro_OrdenesProduccionInsert(entOrden, tran)
            For Each detalle As pro_OrdenesProduccionDetalleManoObra In entManoObra
                detalle.IdComprobante = entOrden.IdComprobante
                Tablas.pro_OrdenesProduccionDetalleManoObraInsert(detalle, tran)
            Next
            For Each detalle As pro_OrdenesGasDirectos In entDet1
                detalle.IdComprobante = entOrden.IdComprobante
                Tablas.pro_OrdenesGasDirectosInsert(detalle, tran)
            Next
            For Each detalle As pro_OrdenesGasIndirectos In entDet2
                detalle.IdComprobante = entOrden.IdComprobante
                Tablas.pro_OrdenesGasIndirectosInsert(detalle, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function pro_ActualizarOrdenProduccion(ByRef entOrden As pro_OrdenesProduccion, ByRef ManoObra As List(Of pro_OrdenesProduccionDetalleManoObra), ByRef entDet1 As List(Of pro_OrdenesGasDirectos), ByRef entDet2 As List(Of pro_OrdenesGasIndirectos)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            Tablas.pro_OrdenesProduccionUpdate(entOrden, tran)

            Dim sSQL As String = "delete pro_OrdenesProduccionDetalleManoObra where IdComprobante=" & entOrden.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)

            sSQL = "delete pro_OrdenesGasDirectos where IdComprobante=" & entOrden.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)

            sSQL = "delete pro_OrdenesGasinDirectos where IdComprobante=" & entOrden.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)

            For Each detalle As pro_OrdenesProduccionDetalleManoObra In ManoObra
                detalle.IdComprobante = entOrden.IdComprobante
                Tablas.pro_OrdenesProduccionDetalleManoObraInsert(detalle, tran)
            Next
            For Each detalle As pro_OrdenesGasDirectos In entDet1
                detalle.IdComprobante = entOrden.IdComprobante
                Tablas.pro_OrdenesGasDirectosInsert(detalle, tran)
            Next
            For Each detalle As pro_OrdenesGasIndirectos In entDet2
                detalle.IdComprobante = entOrden.IdComprobante
                Tablas.pro_OrdenesGasIndirectosInsert(detalle, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function pro_InsertarSalida(ByRef Header As pro_SalidasProduccion, ByRef Detalle As List(Of pro_SalidasProduccionDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            Header.IdComprobante = fd.ObtenerUltimoId("PRO_SALIDASPRODUCCION", "IdComprobante") + 1
            If Header.Numero = "" Then ' solo si no le colocan el correlativo, lo generara automaticamente
                Dim UltimoNumero As Integer = 0
                UltimoNumero = fd.CorrelativosInv("SALIDAS_PRODUCCION")
                Header.Numero = UltimoNumero.ToString.PadLeft(6, "0")
            End If

            Tablas.pro_SalidasProduccionInsert(Header, tran)

            For Each det As pro_SalidasProduccionDetalle In Detalle
                det.IdComprobante = Header.IdComprobante
                Tablas.pro_SalidasProduccionDetalleInsert(det, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function pro_ActualizarSalida(ByRef Header As pro_SalidasProduccion, ByRef Detalle As List(Of pro_SalidasProduccionDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            Tablas.pro_SalidasProduccionUpdate(Header, tran)
            Dim sSQL As String = "delete pro_SalidasProduccionDetalle where IdComprobante=" & Header.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)
            For Each det As pro_SalidasProduccionDetalle In Detalle
                Tablas.pro_SalidasProduccionDetalleInsert(det, tran)
            Next
        Catch ex As Exception
            tran.Rollback()
            Return ex.Message()
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function

    Public Function pro_InsertarEntrada(ByRef Header As pro_EntradasProduccion, ByRef Detalle As List(Of pro_EntradasProduccionDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            Header.IdComprobante = fd.ObtenerUltimoId("PRO_ENTRADASPRODUCCION", "IdComprobante") + 1
            If Header.Numero = "" Then ' solo si no le colocan el correlativo, lo generara automaticamente
                Dim UltimoNumero As Integer = 0
                UltimoNumero = fd.CorrelativosInv("ENTRADAS_PRODUCCION")
                Header.Numero = UltimoNumero.ToString.PadLeft(6, "0")
            End If

            Tablas.pro_EntradasProduccionInsert(Header, tran)

            For Each det As pro_EntradasProduccionDetalle In Detalle
                det.IdComprobante = Header.IdComprobante
                Tablas.pro_EntradasProduccionDetalleInsert(det, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function pro_ActualizarEntrada(ByRef Header As pro_EntradasProduccion, ByRef Detalle As List(Of pro_EntradasProduccionDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            Tablas.pro_EntradasProduccionUpdate(Header, tran)
            Dim sSQL As String = "delete pro_EntradasProduccionDetalle where IdComprobante=" & Header.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)
            For Each det As pro_EntradasProduccionDetalle In Detalle
                Tablas.pro_EntradasProduccionDetalleInsert(det, tran)
            Next
        Catch ex As Exception
            tran.Rollback()
            Return ex.Message()
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function

    Public Function pro_InsertarOrdenesEmpaque(ByRef Header As pro_OrdenesEmpaque, ByRef Detalle1 As List(Of pro_OrdenesEmpaqueDetalleSalida), ByRef Detalle2 As List(Of pro_OrdenesEmpaqueDetalleEntrada)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            Header.IdComprobante = fd.ObtenerUltimoId("PRO_ORDENESEMPAQUE", "IdComprobante") + 1
            If Header.Numero = "" Then ' solo si no le colocan el correlativo, lo generara automaticamente
                Dim UltimoNumero As Integer = 0
                UltimoNumero = fd.CorrelativosInv("ORDEN_EMPAQUE")
                Header.Numero = UltimoNumero.ToString.PadLeft(6, "0")
            End If

            Tablas.pro_OrdenesEmpaqueInsert(Header, tran)

            For Each det As pro_OrdenesEmpaqueDetalleSalida In Detalle1
                det.IdComprobante = Header.IdComprobante
                Tablas.pro_OrdenesEmpaqueDetalleSalidaInsert(det, tran)
            Next
            For Each det As pro_OrdenesEmpaqueDetalleEntrada In Detalle2
                det.IdComprobante = Header.IdComprobante
                Tablas.pro_OrdenesEmpaqueDetalleEntradaInsert(det, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function pro_ActualizarOrdenesEmpaque(ByRef Header As pro_OrdenesEmpaque, ByRef Detalle1 As List(Of pro_OrdenesEmpaqueDetalleSalida), ByRef Detalle2 As List(Of pro_OrdenesEmpaqueDetalleEntrada)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            Tablas.pro_OrdenesEmpaqueUpdate(Header, tran)
            Dim sSQL As String = "delete pro_OrdenesEmpaqueDetalleSalida where IdComprobante=" & Header.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)

            sSQL = "delete pro_OrdenesEmpaqueDetalleEntrada where IdComprobante=" & Header.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)

            For Each det As pro_OrdenesEmpaqueDetalleSalida In Detalle1
                Tablas.pro_OrdenesEmpaqueDetalleSalidaInsert(det, tran)
            Next

            For Each det As pro_OrdenesEmpaqueDetalleEntrada In Detalle2
                Tablas.pro_OrdenesEmpaqueDetalleEntradaInsert(det, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message()
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
#End Region

End Class
