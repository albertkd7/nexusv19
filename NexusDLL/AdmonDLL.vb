﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports NexusELL.TableEntities
Imports System.Data.Common

Public Class AdmonDLL
    Dim db As Database
    Dim dbImport As Database
    Dim objTabla As TableData

    Public Sub New(ByVal strConexion As String)

        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db

        objTabla = New TableData(strConexion)
    End Sub
    Public Function ObtieneParametros() As DataTable
        Return db.ExecuteDataSet(CommandType.Text, "select * from adm_parametros").Tables(0)
    End Function
    Sub adm_ParametrosUpdate(ByVal Ent As adm_Parametros, ByVal CorrelQuedan As Integer _
        , ByVal CorrelSujetoExcluido As Integer, ByVal CorrelEntradas As Integer _
        , ByVal CorrelSalidas As Integer, ByVal CorrelTraslados As Integer, ByVal CorrelNA As Integer _
        , ByVal CorrelVale As Integer, ByVal CorrelOC As Integer)
        db.ExecuteNonQuery("adm_ParametrosUpdate" _
        , Ent.NrcEmpresa, Ent.NitEmpresa, Ent.TipoContribuyente, Ent.EsRetenedor, Ent.Direccion, Ent.Domicilio _
        , Ent.Municipio, Ent.Departamento, Ent.NumeroPatronal, Ent.ActividadEconomica _
        , Ent.NombreFirmante1, Ent.CargoFirmante1 _
        , Ent.NombreFirmante2, Ent.CargoFirmante2 _
        , Ent.NombreFirmante3, Ent.CargoFirmante3 _
        , Ent.NombreFirmante4, Ent.CargoFirmante4 _
        , Ent.NombreFirmante5, Ent.CargoFirmante5 _
        , Ent.NombreFirmante6, Ent.CargoFirmante6, Ent.DescMoneda, Ent.Ejercicio _
        , Ent.FechaCierre, Ent.FechaLimite, Ent.PorcIVA, Ent.PorcReten, Ent.PorcPercep _
        , Ent.IdSucursal, Ent.IdPunto, Ent.IdBodega _
        , Ent.TipoNumeracionPartidas, Ent.GuardarDesbalance _
        , Ent.IdCuentaCaja, Ent.IdCuentaCredito1, Ent.IdCuentaCredito2, Ent.IdCuentaDebito1, Ent.IdCuentaDebito2 _
        , Ent.IdCuentaRetencion1, Ent.IdCuentaRetencion2, Ent.IdCuentaPercepcion _
        , Ent.IdCuentaRetencionVentas, Ent.IdCuentaPercepcionVentas, Ent.IdCuentaImportacion, Ent.IdCuentaComisionLiquidacion _
        , Ent.IdTipoPrecio, Ent.TipoPrecio, Ent.IdTipoPartidaCompras, Ent.ContabilizarLineaCompras _
        , Ent.ValidarExistencias, Ent.AlertaMinimos, Ent.AutorizarCheques, Ent.VistaPreviaFacturar, Ent.SeccionarFormaPago _
        , Ent.UtilizarFormularioUnico, Ent.BloquearFechaFacturar, Ent.LogoEmpresa, Ent.Telefonos, Ent.IdCuentaPorCobrar _
        , Ent.FacturarMenosCosto, Ent.MesCierreAnual, Ent.LimiteMontoDirectoEfectivo, Ent.LimiteMontoAcumuladoEfectivo, Ent.LimiteMontoDirectoOtros, Ent.LimiteMontoAcumuladoOtros, Ent.DiasMaximoCredito)

        Dim sql As String = " delete adm_correlativos where correlativo IN('QUEDAN','SUJETO_EXCLUIDO','ENTRADAS_INV','SALIDAS_INV','TRASLADOS_INV','ABONOS_CPC','FAC_VALES','ORDEN_COMPRA') ;"

        sql &= "insert into adm_correlativos values('QUEDAN', " & CorrelQuedan & " ) ;"
        sql &= "insert into adm_correlativos values('SUJETO_EXCLUIDO', " & CorrelSujetoExcluido & " ) ;"
        sql &= "insert into adm_correlativos values('ENTRADAS_INV', " & CorrelEntradas & " ) ;"
        sql &= "insert into adm_correlativos values('SALIDAS_INV', " & CorrelSalidas & " ) ;"
        sql &= "insert into adm_correlativos values('TRASLADOS_INV', " & CorrelTraslados & " ) ;"
        sql &= "insert into adm_correlativos values('ABONOS_CPC', " & CorrelNA & " ) ;"
        sql &= "insert into adm_correlativos values('FAC_VALES', " & CorrelVale & " ) ;"
        sql &= "insert into adm_correlativos values('ORDEN_COMPRA', " & CorrelOC & " ) ;"

        db.ExecuteNonQuery(CommandType.Text, sql)

    End Sub
    
    Public Function ValidarUsuario(ByVal UserId As String, ByVal txt_password As String) As String
        'este codigo es el que desencripta la base de datos.
        'Try
        '    db.ExecuteNonQuery(CommandType.Text, "open symmetric key dbdx decryption by password = '##Itosa081945343$$';exec dbd_unlock_for_host @proc=1")
        'Catch ex As Exception

        'End Try

        Dim msj As String = "", EsCorrecto As Boolean = False
        Try
            EsCorrecto = db.ExecuteScalar("adm_ValidarUsuario", UserId, txt_password) = 1
            If EsCorrecto Then
                msj = "Ok"
            Else
                msj = "No"
            End If
        Catch ex As Exception
            msj = ex.Message
        End Try
        Return msj
        
    End Function
    Public Function ExtraeUsuario(ByVal UserId As String) As DataTable
        Dim sSQL As String = "Select * from adm_usuarios where IdUsuario ='" & UserId & "'"
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function

    Public Function GestionesProgramadas(ByVal FechaDesde As Date, ByVal FechaHasta As Date, ByVal IdVendedor As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spRptGestionesProgramadas", FechaDesde, FechaHasta, IdVendedor).Tables(0)
    End Function
    Public Sub IngresoFallido(ByVal IdUsuario As String, ByVal Pc As String, ByVal NumAcceso As String)
        Dim sSQL As String = "insert into adm_AccesosFallidos values ('" & IdUsuario & "'"
        sSQL += ", '" & Pc & "'"
        sSQL += ", " & NumAcceso
        sSQL += ", getdate() )"
        db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Sub

    Public Sub HistorialClaves(ByVal IdUsuario As String, ByVal Password As String)
        Dim sSQL As String = "insert into adm_HistorialUsuarios values ('" & IdUsuario & "'"
        sSQL += ", '" & Password & "'"
        sSQL += ", getdate() )"
        db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Sub
    Public Function adm_rptBitacora(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdModulo As Integer, ByVal IdUsuario As String) As DataTable
        Return db.ExecuteDataSet("adm_spBitacora", Desde, Hasta, IdModulo, IdUsuario).Tables(0)
    End Function

    Public Sub BloqueaUsuario(ByVal cod_usuario As String)
        db.ExecuteScalar("adm_BloqueaUsuario", cod_usuario)
    End Sub
    Public Function GetPermisosUsuario(ByVal sIdUsuario As String, ByVal sModulo As String, ByVal sOptionId As String) As DataTable
        Return db.ExecuteDataSet("adm_GetPermisosUsuario", sIdUsuario, sModulo, sOptionId).Tables(0)
    End Function
    Public Function ObtenerOpcionesUsuario(ByVal Modulo As String, ByVal IdUsuario As String) As DataTable
        Return db.ExecuteDataSet("adm_GetOpciones", Modulo, IdUsuario).Tables(0)
    End Function
    Public Sub EliminaOpcionesUsuario(ByVal sIdUsuario As String, ByVal sModulo As String)
        db.ExecuteNonQuery("adm_Opciones_UsuariosDelete", sIdUsuario, sModulo)
    End Sub
    Public Sub InsertaOpcionUsuario(ByVal sIdUsuario As String, ByVal sModulo As String, ByVal sOptionId As String, ByVal boolean1 As Boolean, ByVal boolean2 As Boolean, ByVal boolean3 As Boolean)
        db.ExecuteNonQuery("adm_Opciones_UsuariosInsert", sIdUsuario, sModulo, sOptionId, boolean1, boolean2, boolean3)
    End Sub
    Public Function ObtenerPreciosUsuario(ByVal IdUsuario As String) As DataTable
        Dim sql = "select ip.IdPrecio, Nombre, Acceso = convert(bit,case when up.IdPrecio is null then 0 else 1 end) "
        sql &= "from inv_Precios ip left join adm_usuariosPrecios up on ip.IdPrecio = up.IdPrecio "
        sql &= "and IdUsuario = '" & IdUsuario & "'"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function ObtenerSucursalesUsuario(ByVal IdUsuario As String) As DataTable
        Dim sql = "select ip.IdSucursal, Nombre, Acceso = convert(bit,case when up.IdSucursal is null then 0 else 1 end) "
        sql &= "from adm_sucursales ip left join adm_UsuariosSucursales up on ip.Idsucursal = up.Idsucursal "
        sql &= "and IdUsuario = '" & IdUsuario & "'"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Function ObtenerUltimasClaves(ByVal IdUsuario As String) As DataTable
        Dim sql = "select top 6 Clave from adm_historialusuarios where IdUsuario='" & IdUsuario & "'"
        sql &= " ORDER BY FechaHoraCreacion DESC"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Sub EliminarPreciosUsuario(ByVal IdUsuario As String)
        Dim sql = "delete adm_usuariosPrecios where IdUsuario = '" & IdUsuario & "'"
        db.ExecuteNonQuery(CommandType.Text, sql)
    End Sub
    Public Sub EliminarSucursalesUsuario(ByVal IdUsuario As String)
        Dim sql = "delete  adm_usuariossucursales where IdUsuario = '" & IdUsuario & "'"
        db.ExecuteNonQuery(CommandType.Text, sql)
    End Sub
    Public Sub InsertarPrecioUsuario(ByVal IdUsuario As String, ByVal IdPrecio As Integer, ByVal CreadoPor As String)
        Dim sql = "insert into adm_usuariosPrecios values('" & IdUsuario & "', " & IdPrecio
        sql &= ", '" & CreadoPor & "', getdate() )"
        db.ExecuteNonQuery(CommandType.Text, sql)
    End Sub
    Public Sub InsertarSucursalUsuario(ByVal IdUsuario As String, ByVal IdSucursal As Integer, ByVal CreadoPor As String)
        Dim sql = "insert into adm_usuariossucursales values('" & IdUsuario & "', " & IdSucursal
        sql &= ", '" & CreadoPor & "', getdate() )"
        db.ExecuteNonQuery(CommandType.Text, sql)
    End Sub
    Public Sub AsignaPermisosDeUsuarioAsuario(ByVal IdUsuario As String, ByVal IdUsuarioCopiar As String)
        Dim sql = "delete adm_OpcionesUsuarios WHERE IdUsuario= '" & IdUsuario & "'"
        db.ExecuteNonQuery(CommandType.Text, sql)

        Dim sql1 = "INSERT INTO adm_OpcionesUsuarios SELECT IdUsuario ='" & IdUsuario & "' , NombreModulo , IdOpcion , snInsert "
        sql1 &= ", snDelete ,  snEdit FROM adm_OpcionesUsuarios WHERE IdUsuario='" & IdUsuarioCopiar & "' "
        db.ExecuteNonQuery(CommandType.Text, sql1)
    End Sub
    Function GetModulos() As DataTable
        Return db.ExecuteDataSet(CommandType.Text, "Select * from adm_Modulos").Tables(0)
    End Function
    Public Function adm_CierreSucursal(ByVal IdSucursal As Integer, ByVal Fecha As Date, ByRef entArqueo As List(Of fac_CorteCajaArqueo) _
                                       , ByRef entArqueoCheques As List(Of fac_CorteCajaCheques) _
                                       , ByRef entArqueoRemesas As List(Of fac_CorteCajaRemesas) _
                                        , ByRef entArqueoPOS As List(Of fac_CorteCajaPOS) _
                                       , ByVal IdUsuario As String, ByVal IdPunto As Integer) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = "Ok"
        Try


            msj = db.ExecuteScalar("adm_spCierreSucursal", IdSucursal, Fecha, IdUsuario, IdPunto)

            Dim sSQL As String = "delete fac_CorteCajaArqueo where IdSucursal=" & IdSucursal & " and IdPunto= " & IdPunto
            sSQL += " and Fecha='" + Format(Fecha, "yyyyMMdd") + "'"
            db.ExecuteNonQuery(CommandType.Text, sSQL)

            sSQL = "delete fac_CorteCajaCheques where IdSucursal=" & IdSucursal & " and IdPunto= " & IdPunto
            sSQL += " and Fecha='" + Format(Fecha, "yyyyMMdd") + "'"
            db.ExecuteNonQuery(CommandType.Text, sSQL)

            sSQL = "delete fac_CorteCajaRemesas where IdSucursal=" & IdSucursal & " and IdPunto= " & IdPunto
            sSQL += " and Fecha='" + Format(Fecha, "yyyyMMdd") + "'"
            db.ExecuteNonQuery(CommandType.Text, sSQL)

            sSQL = "delete fac_CorteCajaPOS where IdSucursal=" & IdSucursal & " and IdPunto= " & IdPunto
            sSQL += " and Fecha='" + Format(Fecha, "yyyyMMdd") + "'"
            db.ExecuteNonQuery(CommandType.Text, sSQL)
            'Caja Arqueo 
            For Each detalle As fac_CorteCajaArqueo In entArqueo
                objTabla.fac_CorteCajaArqueoInsert(detalle, tran)
            Next
            'Cheques 
            For Each detalle As fac_CorteCajaCheques In entArqueoCheques
                objTabla.fac_CorteCajaChequesInsert(detalle, tran)
            Next
            'Remesas
            For Each detalle As fac_CorteCajaRemesas In entArqueoRemesas
                objTabla.fac_CorteCajaRemesasInsert(detalle, tran)
            Next
            'Pos
            For Each detalle As fac_CorteCajaPOS In entArqueoPOS
                objTabla.fac_CorteCajaPOSInsert(detalle, tran)
            Next

            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message()
        Finally
            cn.Close()
        End Try
        Return msj
    End Function

    Public Function adm_RevierteCierreSucursal(ByVal IdSucursal As Integer, ByVal Fecha As Date, ByVal IdUsuario As String, ByVal IdPunto As Integer) As String
        Dim sql As String = "update fac_calendario set activo=0  where fecha = '" + Format(Fecha, "yyyyMMdd") + "'"
        sql += " and IdSucursal= " & IdSucursal & " and IdPunto = " & IdPunto
        Return db.ExecuteNonQuery(CommandType.Text, sql)
    End Function

    Public Function GetUsuarios() As DataTable
        Return db.ExecuteDataSet(CommandType.Text, "Select * from adm_Usuarios").Tables(0)
    End Function
    
    Public Function ObtieneCorrelativos(ByVal NombreCorrelativo As String) As Integer

        Dim sSQL As String = "select isnull(UltimoValor,1) from adm_correlativos where Correlativo='" & NombreCorrelativo & "'"
        Dim sSQL2 As String = "select correlativo from adm_correlativos where Correlativo='" & NombreCorrelativo & "'"
        Dim NuevoCorrelativo As Integer = SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), 1)
        Dim Existe As String = SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL2), "")

        If Existe = "" Or Existe = Nothing Then 'NuevoCorrelativo = 1 Then 'Crea un nuevo registro
            sSQL = "insert adm_correlativos (correlativo, UltimoValor) values ('" & NombreCorrelativo & "',1)"
        End If

        db.ExecuteNonQuery(CommandType.Text, sSQL)

        Return NuevoCorrelativo
    End Function

    Public Function ObtieneFechaCierre() As DataTable
        Return db.ExecuteDataSet(CommandType.Text, "select FechaCierre,FechaLimite from adm_parametros").Tables(0)
    End Function

    Public Function ObtieneFacturasInterface(ByVal IdSucursal As Integer, ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Dim Conexion As String = "cns" + IdSucursal.ToString
        dbImport = DatabaseFactory.CreateDatabase(Conexion)


        Dim sSql As String = "SELECT NumTransaccion=TransactionNumber, Fecha= TIME , FechaHora = TIME "
        sSql += " ,IdTipoComprobante =CASE WHEN StoreId IN (5037,5032) THEN 7 ELSE  CASE WHEN substring(ReferenceNumber,1,2)='cf' OR substring(ReferenceNumber,1,2)='ccf' "
        sSql += " OR substring(ReferenceNumber,1,2)='cr' OR substring(ReferenceNumber,1,2)='CR'  THEN 5 ELSE 6 end END"
        sSql += " ,Numero=ReferenceNumber, NombreCliente='',Nrc='', Nit='',Total=convert(DECIMAL(18,2),Total ) "
        sSql += " FROM [TRANSACTION ] WHERE convert(varchar(10),TIME,112) >='" & Format(Desde, "yyyyMMdd") & "'"
        sSql += " AND convert(varchar(10),TIME,112) < dateadd(dd,1,'" & Format(Hasta, "yyyyMMdd") & "')"
        sSql += " and StoreId= " & IdSucursal
        sSql += " AND Total <> 0.00 ORDER BY TIME, ReferenceNumber "

        Return dbImport.ExecuteDataSet(CommandType.Text, sSql).Tables(0)
    End Function

    Public Function SiEsNulo(ByVal value As Object, ByVal ValueNoNull As Object) As Object
        If value Is Nothing Then Return Nothing
        If IsDBNull(value) Then Return ValueNoNull
        Return value
    End Function

    Public Function GetSchemasDB() As DataTable
        Dim sql As String = ""
        sql += "      IF OBJECT_ID('dbo.adm_empresas', 'U') IS NOT NULL "
        sql += "  BEGIN "
        sql += "         SELECT "
        sql += "Esquema = Cns "
        sql += "           ,Nombre "
        sql += "         ,Nrc = '00000-0' "
        sql += "         ,UserDB = Usuario "
        sql += "          ,PassDB = Clave "
        sql += "          ,IdDB = Correlativo "
        sql += "      FROM dbo.adm_empresas; "
        sql += "  End "
        sql += "     ELSE "
        sql += " BEGIN "
        sql += "       DECLARE @query VARCHAR(MAX) = ( "
        sql += "     SELECT REPLACE( "
        sql += "       STUFF(( "
        sql += "           SELECT TOP 999 "
        sql += "';' + CONCAT('SELECT Esquema=',char(39), sh.[name], char(39) "
        sql += " 			            ,',Nombre=NombreEmpresa,Nrc=NrcEmpresa' "
        sql += "		            ,',UserDB=',char(39), dbp.[name], char(39) "
        sql += "		            ,',PassDB=',char(39), 'Itosa08$', char(39) "
        sql += "		            ,',IdDB=', sh.[schema_id] "
        sql += "		            ,' FROM ', sh.[name], '.adm_Parametros' "
        sql += "		            ) "
        sql += "        FROM sys.schemas sh "
        sql += "         INNER JOIN sys.database_principals dbp ON sh.[name] = dbp.default_schema_name AND dbp.[Type] IN('S', 'U') "
        sql += "         WHERE sh.[name] LIKE 'Nexus%' OR sh.[name] = 'dbo' "
        sql += "         FOR XML PATH('') "
        sql += "     ), 1, 1, '') "
        sql += "     , ';', ' UNION ALL ') "
        sql += "       ); "
        sql += "       EXEC(@query); "
        sql += "   End "
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Function EliminaEmpresa_Schema(ByVal NameSchema As String) As String
        Return CType(db.ExecuteScalarWait("EXEC dbo.DropLogicDB64 '" & NameSchema & "'"), String)
    End Function

    Public Sub EliminaEmpresa_Lista(ByVal cns As String)
        db.ExecuteScalarWait("delete from dbo.adm_empresas where cns = '" & cns & "'")
    End Sub

    Public Function GetNameSchemaByUser(ByVal UserDB As String) As String
        Return db.ExecuteScalar(CommandType.Text, "SELECT default_schema_name FROM sys.database_principals WHERE [name] = '" & UserDB & "'")
    End Function
End Class
