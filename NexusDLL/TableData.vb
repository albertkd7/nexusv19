﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports System.Data.Common
Imports System.Data
Imports NexusELL.TableEntities



Public Class TableData
    Dim db As Database
    Dim fd As FuncionesDLL
    Public Sub New(ByVal strConexion As String)
        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db

        fd = New FuncionesDLL(strConexion)
    End Sub



#Region "Appointments"
    Public Function AppointmentsSelectAll() As DataTable
        Return db.ExecuteDataSet("AppointmentsSelectAll").Tables(0)
    End Function

    Public Function AppointmentsSelectByPK _
            (ByVal UniqueID As System.Int32 _
            ) As Appointments

        Dim dt As datatable
        dt = db.ExecuteDataSet("AppointmentsSelectByPK", _
            UniqueID _
            ).tables(0)

        Dim Entidad As New Appointments
        If dt.Rows.Count > 0 Then
            entidad.UniqueID = dt.rows(0).item("UniqueID")
            entidad.Type = dt.rows(0).item("Type")
            entidad.StartDate = dt.rows(0).item("StartDate")
            entidad.EndDate = dt.rows(0).item("EndDate")
            entidad.AllDay = dt.rows(0).item("AllDay")
            entidad.Subject = dt.rows(0).item("Subject")
            entidad.Location = dt.rows(0).item("Location")
            entidad.Description = dt.rows(0).item("Description")
            entidad.Status = dt.rows(0).item("Status")
            entidad.Label = dt.rows(0).item("Label")
            entidad.ResourceID = dt.rows(0).item("ResourceID")
            Entidad.ResourceIDs = fd.SiEsNulo(dt.Rows(0).Item("ResourceIDs"), Nothing)
            entidad.ReminderInfo = dt.rows(0).item("ReminderInfo")
            Entidad.RecurrenceInfo = fd.SiEsNulo(dt.Rows(0).Item("RecurrenceInfo"), Nothing)
            entidad.TimeZoneId = dt.rows(0).item("TimeZoneId")
            entidad.CustomField1 = dt.rows(0).item("CustomField1")
            entidad.CustomField2 = dt.rows(0).item("CustomField2")

        End If
        Return Entidad
    End Function

    Public Sub AppointmentsDeleteByPK _
            (ByVal UniqueID As System.Int32 _
            )

        db.ExecuteNonQuery("AppointmentsDeleteByPK", _
            UniqueID _
            )
    End Sub

    Public Sub AppointmentsDeleteByPK _
            (ByVal UniqueID As System.Int32 _
            , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "AppointmentsDeleteByPK", _
            UniqueID _
            )
    End Sub

    Public Sub AppointmentsInsert _
    (ByVal entidad As Appointments)

        db.ExecuteNonQuery("AppointmentsInsert", _
            entidad.UniqueID _
            , entidad.Type _
            , entidad.StartDate _
            , entidad.EndDate _
            , entidad.AllDay _
            , entidad.Subject _
            , entidad.Location _
            , entidad.Description _
            , entidad.Status _
            , entidad.Label _
            , entidad.ResourceID _
            , entidad.ResourceIDs _
            , entidad.ReminderInfo _
            , entidad.RecurrenceInfo _
            , entidad.TimeZoneId _
            , entidad.CustomField1 _
            , entidad.CustomField2 _
            )
    End Sub

    Public Sub AppointmentsInsert _
    (ByVal entidad As Appointments, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "AppointmentsInsert", _
            entidad.UniqueID _
            , entidad.Type _
            , entidad.StartDate _
            , entidad.EndDate _
            , entidad.AllDay _
            , entidad.Subject _
            , entidad.Location _
            , entidad.Description _
            , entidad.Status _
            , entidad.Label _
            , entidad.ResourceID _
            , entidad.ResourceIDs _
            , entidad.ReminderInfo _
            , entidad.RecurrenceInfo _
            , entidad.TimeZoneId _
            , entidad.CustomField1 _
            , entidad.CustomField2 _
            )
    End Sub

    Public Sub AppointmentsUpdate _
    (ByVal entidad As Appointments)

        db.ExecuteNonQuery("AppointmentsUpdate", _
            entidad.UniqueID _
            , entidad.Type _
            , entidad.StartDate _
            , entidad.EndDate _
            , entidad.AllDay _
            , entidad.Subject _
            , entidad.Location _
            , entidad.Description _
            , entidad.Status _
            , entidad.Label _
            , entidad.ResourceID _
            , entidad.ResourceIDs _
            , entidad.ReminderInfo _
            , entidad.RecurrenceInfo _
            , entidad.TimeZoneId _
            , entidad.CustomField1 _
            , entidad.CustomField2 _
            )
    End Sub

    Public Sub AppointmentsUpdate _
    (ByVal entidad As Appointments, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "AppointmentsUpdate", _
            entidad.UniqueID _
            , entidad.Type _
            , entidad.StartDate _
            , entidad.EndDate _
            , entidad.AllDay _
            , entidad.Subject _
            , entidad.Location _
            , entidad.Description _
            , entidad.Status _
            , entidad.Label _
            , entidad.ResourceID _
            , entidad.ResourceIDs _
            , entidad.ReminderInfo _
            , entidad.RecurrenceInfo _
            , entidad.TimeZoneId _
            , entidad.CustomField1 _
            , entidad.CustomField2 _
            )
    End Sub

#End Region
#Region "fac_VentasRetencion"
    Public Function fac_VentasRetencionSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_VentasRetencionSelectAll").Tables(0)
    End Function

    Public Function fac_VentasRetencionSelectByPK _
            (ByVal IdComprobante As System.Int32
            ) As fac_VentasRetencion

        Dim dt As DataTable
        dt = db.ExecuteDataSet("fac_VentasRetencionSelectByPK",
            IdComprobante
            ).Tables(0)

        Dim Entidad As New fac_VentasRetencion
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.Numero = dt.Rows(0).Item("Numero")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.FechaContable = dt.Rows(0).Item("FechaContable")
            Entidad.Observacion = dt.Rows(0).Item("Observacion")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub fac_VentasRetencionDeleteByPK _
            (ByVal IdComprobante As System.Int32
            )

        db.ExecuteNonQuery("fac_VentasRetencionDeleteByPK",
            IdComprobante
            )
    End Sub

    Public Sub fac_VentasRetencionDeleteByPK _
            (ByVal IdComprobante As System.Int32 _
            , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VentasRetencionDeleteByPK",
            IdComprobante
            )
    End Sub

    Public Sub fac_VentasRetencionInsert _
    (ByVal entidad As fac_VentasRetencion)

        db.ExecuteNonQuery("fac_VentasRetencionInsert",
            entidad.IdComprobante _
            , entidad.Numero _
            , entidad.Fecha _
            , entidad.FechaContable _
            , entidad.Observacion _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion
            )
    End Sub

    Public Sub fac_VentasRetencionInsert _
    (ByVal entidad As fac_VentasRetencion, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VentasRetencionInsert",
            entidad.IdComprobante _
            , entidad.Numero _
            , entidad.Fecha _
            , entidad.FechaContable _
            , entidad.Observacion _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion
            )
    End Sub

    Public Sub fac_VentasRetencionUpdate _
    (ByVal entidad As fac_VentasRetencion)

        db.ExecuteNonQuery("fac_VentasRetencionUpdate",
            entidad.IdComprobante _
            , entidad.Numero _
            , entidad.Fecha _
            , entidad.FechaContable _
            , entidad.Observacion _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion
            )
    End Sub

    Public Sub fac_VentasRetencionUpdate _
    (ByVal entidad As fac_VentasRetencion, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VentasRetencionUpdate",
            entidad.IdComprobante _
            , entidad.Numero _
            , entidad.Fecha _
            , entidad.FechaContable _
            , entidad.Observacion _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion
            )
    End Sub

#End Region
#Region "adm_Usuarios"
    Public Function adm_UsuariosSelectAll() As DataTable
        Return db.ExecuteDataSet("adm_UsuariosSelectAll").Tables(0)
    End Function

    Public Function adm_UsuariosSelectByPK _
      (ByVal IdUsuario As System.String _
      ) As adm_Usuarios

        Dim dt As datatable
        dt = db.ExecuteDataSet("adm_UsuariosSelectByPK", _
         IdUsuario _
         ).tables(0)

        Dim Entidad As New adm_Usuarios
        If dt.Rows.Count > 0 Then
            entidad.IdUsuario = dt.rows(0).item("IdUsuario")
            entidad.Clave = dt.rows(0).item("Clave")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Apellidos = dt.rows(0).item("Apellidos")
            entidad.FechaCreacion = dt.rows(0).item("FechaCreacion")
            Entidad.FechaVencimiento = dt.Rows(0).Item("FechaVencimiento")
            Entidad.FechaUltCambioClave = fd.SiEsNulo(dt.Rows(0).Item("FechaUltCambioClave"), Nothing)
            Entidad.VigenciaClave = fd.SiEsNulo(dt.Rows(0).Item("VigenciaClave"), 0)
            Entidad.Estado = dt.rows(0).item("Estado")
            Entidad.IdSucursal = fd.SiEsNulo(dt.Rows(0).Item("IdSucursal"), 1)
            Entidad.IdBodega = fd.SiEsNulo(dt.Rows(0).Item("IdBodega"), 0)
            Entidad.IdTipoPrecio = fd.SiEsNulo(dt.Rows(0).Item("IdTipoPrecio"), 1)
            Entidad.CambiarPrecios = fd.SiEsNulo(dt.Rows(0).Item("CambiarPrecios"), Nothing)
            Entidad.AccesoProductos = fd.SiEsNulo(dt.Rows(0).Item("AccesoProductos"), Nothing)
            Entidad.AutorizaCredito = fd.SiEsNulo(dt.Rows(0).Item("AutorizaCredito"), 0)
            Entidad.AutorizarRevertir = fd.SiEsNulo(dt.Rows(0).Item("AutorizarRevertir"), 0)
            Entidad.BloquearBodega = fd.SiEsNulo(dt.Rows(0).Item("BloquearBodega"), 0)
            Entidad.TodasLasSucursales = fd.SiEsNulo(dt.Rows(0).Item("TodasLasSucursales"), False)
            Entidad.AutorizarDescuentos = fd.SiEsNulo(dt.Rows(0).Item("AutorizarDescuentos"), False)
            Entidad.DesdeDescuentos = dt.Rows(0).Item("DesdeDescuentos")
            Entidad.HastaDescuentos = dt.Rows(0).Item("HastaDescuentos")
            Entidad.IdVendedor = fd.SiEsNulo(dt.Rows(0).Item("IdVendedor"), 0)
            Entidad.IdPunto = fd.SiEsNulo(dt.Rows(0).Item("IdPunto"), 0)
        End If

        Return Entidad
    End Function

    Public Sub adm_UsuariosDeleteByPK _
      (ByVal IdUsuario As System.String _
      )

        db.ExecuteNonQuery("adm_UsuariosDeleteByPK", _
         IdUsuario _
         )
    End Sub

    Public Sub adm_UsuariosDeleteByPK _
      (ByVal IdUsuario As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_UsuariosDeleteByPK", _
         IdUsuario _
         )
    End Sub

    Public Sub adm_UsuariosInsert _
    (ByVal entidad As adm_Usuarios)

        db.ExecuteNonQuery("adm_UsuariosInsert", _
         entidad.IdUsuario _
         , entidad.Clave _
         , entidad.Nombre _
         , entidad.Apellidos _
         , entidad.FechaCreacion _
         , entidad.FechaVencimiento _
         , entidad.FechaUltCambioClave _
         , entidad.VigenciaClave _
         , entidad.Estado _
         , entidad.IdSucursal _
         , entidad.IdBodega _
         , entidad.IdTipoPrecio _
         , entidad.CambiarPrecios _
         , entidad.AccesoProductos _
         , entidad.AutorizaCredito _
         , entidad.AutorizarRevertir _
         , entidad.BloquearBodega _
         , entidad.TodasLasSucursales _
           , entidad.AutorizarDescuentos _
  , entidad.DesdeDescuentos _
  , entidad.HastaDescuentos _
  , entidad.IdVendedor _
  , entidad.IdPunto _
         )
    End Sub

    Public Sub adm_UsuariosInsert _
    (ByVal entidad As adm_Usuarios, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_UsuariosInsert", _
         entidad.IdUsuario _
         , entidad.Clave _
         , entidad.Nombre _
         , entidad.Apellidos _
         , entidad.FechaCreacion _
         , entidad.FechaVencimiento _
         , entidad.FechaUltCambioClave _
         , entidad.VigenciaClave _
         , entidad.Estado _
        , entidad.IdSucursal _
        , entidad.IdBodega _
         , entidad.IdTipoPrecio _
         , entidad.CambiarPrecios _
         , entidad.AccesoProductos _
          , entidad.AutorizaCredito _
          , entidad.AutorizarRevertir _
          , entidad.BloquearBodega _
          , entidad.TodasLasSucursales _
            , entidad.AutorizarDescuentos _
        , entidad.DesdeDescuentos _
        , entidad.HastaDescuentos _
, entidad.IdVendedor _
  , entidad.IdPunto _
         )
    End Sub

    Public Sub adm_UsuariosUpdate _
    (ByVal entidad As adm_Usuarios)

        db.ExecuteNonQuery("adm_UsuariosUpdate", _
         entidad.IdUsuario _
         , entidad.Clave _
         , entidad.Nombre _
         , entidad.Apellidos _
         , entidad.FechaCreacion _
         , entidad.FechaVencimiento _
         , entidad.FechaUltCambioClave _
         , entidad.VigenciaClave _
         , entidad.Estado _
        , entidad.IdSucursal _
                 , entidad.IdBodega _
         , entidad.IdTipoPrecio _
         , entidad.CambiarPrecios _
         , entidad.AccesoProductos _
          , entidad.AutorizaCredito _
          , entidad.AutorizarRevertir _
          , entidad.BloquearBodega _
          , entidad.TodasLasSucursales _
            , entidad.AutorizarDescuentos _
  , entidad.DesdeDescuentos _
  , entidad.HastaDescuentos _
, entidad.IdVendedor _
  , entidad.IdPunto _
         )
    End Sub

    Public Sub adm_UsuariosUpdate _
    (ByVal entidad As adm_Usuarios, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_UsuariosUpdate", _
         entidad.IdUsuario _
         , entidad.Clave _
         , entidad.Nombre _
         , entidad.Apellidos _
         , entidad.FechaCreacion _
         , entidad.FechaVencimiento _
         , entidad.FechaUltCambioClave _
         , entidad.VigenciaClave _
         , entidad.Estado _
        , entidad.IdSucursal _
                 , entidad.IdBodega _
         , entidad.IdTipoPrecio _
         , entidad.CambiarPrecios _
         , entidad.AccesoProductos _
          , entidad.AutorizaCredito _
          , entidad.AutorizarRevertir _
          , entidad.BloquearBodega _
          , entidad.TodasLasSucursales _
            , entidad.AutorizarDescuentos _
  , entidad.DesdeDescuentos _
  , entidad.HastaDescuentos _
, entidad.IdVendedor _
  , entidad.IdPunto _
         )
    End Sub

#End Region
#Region "adm_Libros"
    Public Function adm_LibrosSelectAll() As DataTable
        Return db.ExecuteDataSet("adm_LibrosSelectAll").Tables(0)
    End Function

    Public Function adm_LibrosSelectByPK _
      (ByVal IdLibro As System.Int32 _
      ) As adm_Libros

        Dim dt As datatable
        dt = db.ExecuteDataSet("adm_LibrosSelectByPK", _
         IdLibro _
         ).tables(0)

        Dim Entidad As New adm_Libros
        If dt.Rows.Count > 0 Then
            entidad.IdLibro = dt.rows(0).item("IdLibro")
            entidad.Nombre = dt.rows(0).item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub adm_LibrosDeleteByPK _
      (ByVal IdLibro As System.Int32 _
      )

        db.ExecuteNonQuery("adm_LibrosDeleteByPK", _
         IdLibro _
         )
    End Sub

    Public Sub adm_LibrosDeleteByPK _
      (ByVal IdLibro As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_LibrosDeleteByPK", _
         IdLibro _
         )
    End Sub

    Public Sub adm_LibrosInsert _
    (ByVal entidad As adm_Libros)

        db.ExecuteNonQuery("adm_LibrosInsert", _
         entidad.IdLibro _
         , entidad.Nombre _
         )
    End Sub

    Public Sub adm_LibrosInsert _
    (ByVal entidad As adm_Libros, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_LibrosInsert", _
         entidad.IdLibro _
         , entidad.Nombre _
         )
    End Sub

    Public Sub adm_LibrosUpdate _
    (ByVal entidad As adm_Libros)

        db.ExecuteNonQuery("adm_LibrosUpdate", _
         entidad.IdLibro _
         , entidad.Nombre _
         )
    End Sub

    Public Sub adm_LibrosUpdate _
    (ByVal entidad As adm_Libros, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_LibrosUpdate", _
         entidad.IdLibro _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "adm_SeriesDocumentos"
    Public Function adm_SeriesDocumentosSelectAll() As DataTable
        Return db.ExecuteDataSet("adm_SeriesDocumentosSelectAll").Tables(0)
    End Function

    Public Function adm_SeriesDocumentosSelectByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal IdTipoComprobante As System.Int32 _
      , ByVal Serie As System.String _
      , ByVal Resolucion As System.String
      ) As adm_SeriesDocumentos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("adm_SeriesDocumentosSelectByPK",
         IdSucursal _
         , IdPunto _
         , IdTipoComprobante _
         , Serie _
         , Resolucion
         ).Tables(0)

        Dim Entidad As New adm_SeriesDocumentos
        If dt.Rows.Count > 0 Then
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.IdPunto = dt.Rows(0).Item("IdPunto")
            Entidad.IdTipoComprobante = dt.Rows(0).Item("IdTipoComprobante")
            Entidad.Serie = dt.Rows(0).Item("Serie")
            Entidad.FechaAsignacion = dt.Rows(0).Item("FechaAsignacion")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.UltimoNumero = dt.Rows(0).Item("UltimoNumero")
            Entidad.DesdeNumero = dt.Rows(0).Item("DesdeNumero")
            Entidad.HastaNumero = dt.Rows(0).Item("HastaNumero")
            Entidad.EsActivo = dt.Rows(0).Item("EsActivo")
            Entidad.Resolucion = dt.Rows(0).Item("Resolucion")
        End If
        Return Entidad
    End Function

    Public Sub adm_SeriesDocumentosDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal IdTipoComprobante As System.Int32 _
      , ByVal Serie As System.String _
      , ByVal Resolucion As System.String
      )

        db.ExecuteNonQuery("adm_SeriesDocumentosDeleteByPK",
         IdSucursal _
         , IdPunto _
         , IdTipoComprobante _
         , Serie _
         , Resolucion
         )
    End Sub

    Public Sub adm_SeriesDocumentosDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal IdTipoComprobante As System.Int32 _
      , ByVal Serie As System.String _
      , ByVal Resolucion As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_SeriesDocumentosDeleteByPK",
         IdSucursal _
         , IdPunto _
         , IdTipoComprobante _
         , Serie _
         , Resolucion
           )
    End Sub

    Public Sub adm_SeriesDocumentosInsert _
    (ByVal entidad As adm_SeriesDocumentos)

        db.ExecuteNonQuery("adm_SeriesDocumentosInsert",
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.IdTipoComprobante _
         , entidad.Serie _
         , entidad.FechaAsignacion _
         , entidad.CreadoPor _
         , entidad.UltimoNumero _
         , entidad.DesdeNumero _
         , entidad.HastaNumero _
         , entidad.EsActivo _
         , entidad.Resolucion
         )
    End Sub

    Public Sub adm_SeriesDocumentosInsert _
    (ByVal entidad As adm_SeriesDocumentos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_SeriesDocumentosInsert", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.IdTipoComprobante _
         , entidad.Serie _
         , entidad.FechaAsignacion _
         , entidad.CreadoPor _
         , entidad.UltimoNumero _
         , entidad.DesdeNumero _
         , entidad.HastaNumero _
         , entidad.EsActivo _
         , entidad.Resolucion _
         )
    End Sub

    Public Sub adm_SeriesDocumentosUpdate _
    (ByVal entidad As adm_SeriesDocumentos)

        db.ExecuteNonQuery("adm_SeriesDocumentosUpdate", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.IdTipoComprobante _
         , entidad.Serie _
         , entidad.FechaAsignacion _
         , entidad.CreadoPor _
         , entidad.UltimoNumero _
         , entidad.DesdeNumero _
         , entidad.HastaNumero _
         , entidad.EsActivo _
         , entidad.Resolucion _
         )
    End Sub

    Public Sub adm_SeriesDocumentosUpdate _
    (ByVal entidad As adm_SeriesDocumentos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_SeriesDocumentosUpdate", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.IdTipoComprobante _
         , entidad.Serie _
         , entidad.FechaAsignacion _
         , entidad.CreadoPor _
         , entidad.UltimoNumero _
         , entidad.DesdeNumero _
         , entidad.HastaNumero _
         , entidad.EsActivo _
         , entidad.Resolucion _
         )
    End Sub

#End Region

#Region "adm_Sucursales"
    Public Function adm_SucursalesSelectAll() As DataTable
        Return db.ExecuteDataSet("adm_SucursalesSelectAll").Tables(0)
    End Function

    Public Function adm_SucursalesSelectByPK _
      (ByVal IdSucursal As System.Int32 _
      ) As adm_Sucursales

        Dim dt As datatable
        dt = db.ExecuteDataSet("adm_SucursalesSelectByPK", _
         IdSucursal _
         ).tables(0)

        Dim Entidad As New adm_Sucursales
        If dt.Rows.Count > 0 Then
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Responsable = dt.rows(0).item("Responsable")
            entidad.Telefonos = dt.rows(0).item("Telefonos")
            entidad.UltimoNumero = dt.rows(0).item("UltimoNumero")
            entidad.DesdeNumero = dt.rows(0).item("DesdeNumero")
            entidad.HastaNumero = dt.rows(0).item("HastaNumero")
            Entidad.IdCuentaCostos = dt.Rows(0).Item("IdCuentaCostos").ToString()
            Entidad.IdCuentaCaja = dt.Rows(0).Item("IdCuentaCaja").ToString()
        End If
        Return Entidad
    End Function

    Public Sub adm_SucursalesDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      )

        db.ExecuteNonQuery("adm_SucursalesDeleteByPK", _
         IdSucursal _
         )
    End Sub

    Public Sub adm_SucursalesDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_SucursalesDeleteByPK", _
         IdSucursal _
         )
    End Sub

    Public Sub adm_SucursalesInsert _
    (ByVal entidad As adm_Sucursales)

        db.ExecuteNonQuery("adm_SucursalesInsert",
         entidad.IdSucursal _
         , entidad.Nombre _
         , entidad.Responsable _
         , entidad.Telefonos _
         , entidad.UltimoNumero _
         , entidad.DesdeNumero _
         , entidad.HastaNumero _
         , entidad.IdCuentaCostos _
         , entidad.IdCuentaCaja
         )
    End Sub

    Public Sub adm_SucursalesInsert _
    (ByVal entidad As adm_Sucursales, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_SucursalesInsert",
         entidad.IdSucursal _
         , entidad.Nombre _
         , entidad.Responsable _
         , entidad.Telefonos _
         , entidad.UltimoNumero _
         , entidad.DesdeNumero _
         , entidad.HastaNumero _
         , entidad.IdCuentaCostos _
         , entidad.IdCuentaCaja
         )
    End Sub

    Public Sub adm_SucursalesUpdate _
    (ByVal entidad As adm_Sucursales)

        db.ExecuteNonQuery("adm_SucursalesUpdate",
         entidad.IdSucursal _
         , entidad.Nombre _
         , entidad.Responsable _
         , entidad.Telefonos _
         , entidad.UltimoNumero _
         , entidad.DesdeNumero _
         , entidad.HastaNumero _
         , entidad.IdCuentaCostos _
         , entidad.IdCuentaCaja
         )
    End Sub

    Public Sub adm_SucursalesUpdate _
    (ByVal entidad As adm_Sucursales, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_SucursalesUpdate",
         entidad.IdSucursal _
         , entidad.Nombre _
         , entidad.Responsable _
         , entidad.Telefonos _
         , entidad.UltimoNumero _
         , entidad.DesdeNumero _
         , entidad.HastaNumero _
         , entidad.IdCuentaCostos _
         , entidad.IdCuentaCaja
         )
    End Sub

#End Region

#Region "adm_TiposComprobante"
    Public Function adm_TiposComprobanteSelectAll() As DataTable
        Return db.ExecuteDataSet("adm_TiposComprobanteSelectAll").Tables(0)
    End Function

    Public Function adm_TiposComprobanteSelectByPK(ByVal IdTipoComprobante As Int32) As adm_TiposComprobante

        Dim dt As DataTable = db.ExecuteDataSet("adm_TiposComprobanteSelectByPK", IdTipoComprobante).Tables(0)

        Dim Entidad As New adm_TiposComprobante
        If dt.Rows.Count > 0 Then
            Entidad.IdTipoComprobante = dt.Rows(0).Item("IdTipoComprobante")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Abreviatura = dt.Rows(0).Item("Abreviatura")
            Entidad.TipoAplicacion = dt.Rows(0).Item("TipoAplicacion")
            Entidad.IdLibro = dt.Rows(0).Item("IdLibro")
            Entidad.DetallaIVA = dt.Rows(0).Item("DetallaIVA")
            Entidad.IdModuloAplica = dt.Rows(0).Item("IdModuloAplica")
            Entidad.NumeroLineas = dt.Rows(0).Item("NumeroLineas")
            Entidad.IdCuentaInv = dt.Rows(0).Item("IdCuentaInv")
            Entidad.NombreImpresor = dt.Rows(0).Item("NombreImpresor")
            Entidad.FormularioUnico = dt.Rows(0).Item("FormularioUnico")
        End If
        Return Entidad
    End Function

    Public Sub adm_TiposComprobanteDeleteByPK _
      (ByVal IdTipoComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("adm_TiposComprobanteDeleteByPK", _
         IdTipoComprobante _
         )
    End Sub

    Public Sub adm_TiposComprobanteDeleteByPK _
      (ByVal IdTipoComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_TiposComprobanteDeleteByPK", _
         IdTipoComprobante _
         )
    End Sub

    Public Sub adm_TiposComprobanteInsert _
    (ByVal entidad As adm_TiposComprobante)

        db.ExecuteNonQuery("adm_TiposComprobanteInsert", _
         entidad.IdTipoComprobante _
         , entidad.Nombre _
         , entidad.Abreviatura _
         , entidad.TipoAplicacion _
         , entidad.IdLibro _
         , entidad.DetallaIVA _
         , entidad.IdModuloAplica _
         , entidad.NumeroLineas _
         , entidad.IdCuentaInv _
         , entidad.NombreImpresor _
         , entidad.FormularioUnico _
         )
    End Sub

    Public Sub adm_TiposComprobanteInsert _
    (ByVal entidad As adm_TiposComprobante, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_TiposComprobanteInsert", _
         entidad.IdTipoComprobante _
         , entidad.Nombre _
         , entidad.Abreviatura _
         , entidad.TipoAplicacion _
         , entidad.IdLibro _
         , entidad.DetallaIVA _
         , entidad.IdModuloAplica _
         , entidad.NumeroLineas _
         , entidad.IdCuentaInv _
         , entidad.NombreImpresor _
         , entidad.FormularioUnico _
         )
    End Sub

    Public Sub adm_TiposComprobanteUpdate _
    (ByVal entidad As adm_TiposComprobante)

        db.ExecuteNonQuery("adm_TiposComprobanteUpdate", _
         entidad.IdTipoComprobante _
         , entidad.Nombre _
         , entidad.Abreviatura _
         , entidad.TipoAplicacion _
         , entidad.IdLibro _
         , entidad.DetallaIVA _
         , entidad.IdModuloAplica _
         , entidad.NumeroLineas _
         , entidad.IdCuentaInv _
         , entidad.NombreImpresor _
          , entidad.FormularioUnico _
         )
    End Sub

    Public Sub adm_TiposComprobanteUpdate _
    (ByVal entidad As adm_TiposComprobante, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_TiposComprobanteUpdate", _
         entidad.IdTipoComprobante _
         , entidad.Nombre _
         , entidad.Abreviatura _
         , entidad.TipoAplicacion _
         , entidad.IdLibro _
         , entidad.DetallaIVA _
         , entidad.IdModuloAplica _
         , entidad.NumeroLineas _
         , entidad.IdCuentaInv _
         , entidad.NombreImpresor _
         , entidad.FormularioUnico _
         )
    End Sub

#End Region

#Region "adm_Departamentos"
    Public Function adm_DepartamentosSelectAll() As DataTable
        Return db.ExecuteDataSet("adm_DepartamentosSelectAll").Tables(0)
    End Function

    Public Function adm_DepartamentosSelectByPK _
      (ByVal IdDepartamento As System.String _
      ) As adm_Departamentos

        Dim dt As datatable
        dt = db.ExecuteDataSet("adm_DepartamentosSelectByPK", _
         IdDepartamento _
         ).tables(0)

        Dim Entidad As New adm_Departamentos
        If dt.Rows.Count > 0 Then
            entidad.IdDepartamento = dt.rows(0).item("IdDepartamento")
            entidad.Nombre = dt.rows(0).item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub adm_DepartamentosDeleteByPK _
      (ByVal IdDepartamento As System.String _
      )

        db.ExecuteNonQuery("adm_DepartamentosDeleteByPK", _
         IdDepartamento _
         )
    End Sub

    Public Sub adm_DepartamentosDeleteByPK _
      (ByVal IdDepartamento As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_DepartamentosDeleteByPK", _
         IdDepartamento _
         )
    End Sub

    Public Sub adm_DepartamentosInsert _
    (ByVal entidad As adm_Departamentos)

        db.ExecuteNonQuery("adm_DepartamentosInsert", _
         entidad.IdDepartamento _
         , entidad.Nombre _
         )
    End Sub

    Public Sub adm_DepartamentosInsert _
    (ByVal entidad As adm_Departamentos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_DepartamentosInsert", _
         entidad.IdDepartamento _
         , entidad.Nombre _
         )
    End Sub

    Public Sub adm_DepartamentosUpdate _
    (ByVal entidad As adm_Departamentos)

        db.ExecuteNonQuery("adm_DepartamentosUpdate", _
         entidad.IdDepartamento _
         , entidad.Nombre _
         )
    End Sub

    Public Sub adm_DepartamentosUpdate _
    (ByVal entidad As adm_Departamentos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_DepartamentosUpdate", _
         entidad.IdDepartamento _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "adm_Municipios"
    Public Function adm_MunicipiosSelectAll() As DataTable
        Return db.ExecuteDataSet("adm_MunicipiosSelectAll").Tables(0)
    End Function

    Public Function adm_MunicipiosSelectByPK _
      (ByVal IdMunicipio As System.String _
      ) As adm_Municipios

        Dim dt As datatable
        dt = db.ExecuteDataSet("adm_MunicipiosSelectByPK", _
         IdMunicipio _
         ).tables(0)

        Dim Entidad As New adm_Municipios
        If dt.Rows.Count > 0 Then
            entidad.IdMunicipio = dt.rows(0).item("IdMunicipio")
            entidad.IdDepartamento = dt.rows(0).item("IdDepartamento")
            entidad.Nombre = dt.rows(0).item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub adm_MunicipiosDeleteByPK _
      (ByVal IdMunicipio As System.String _
      )

        db.ExecuteNonQuery("adm_MunicipiosDeleteByPK", _
         IdMunicipio _
         )
    End Sub

    Public Sub adm_MunicipiosDeleteByPK _
      (ByVal IdMunicipio As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_MunicipiosDeleteByPK", _
         IdMunicipio _
         )
    End Sub

    Public Sub adm_MunicipiosInsert _
    (ByVal entidad As adm_Municipios)

        db.ExecuteNonQuery("adm_MunicipiosInsert", _
         entidad.IdMunicipio _
         , entidad.IdDepartamento _
         , entidad.Nombre _
         )
    End Sub

    Public Sub adm_MunicipiosInsert _
    (ByVal entidad As adm_Municipios, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_MunicipiosInsert", _
         entidad.IdMunicipio _
         , entidad.IdDepartamento _
         , entidad.Nombre _
         )
    End Sub

    Public Sub adm_MunicipiosUpdate _
    (ByVal entidad As adm_Municipios)

        db.ExecuteNonQuery("adm_MunicipiosUpdate", _
         entidad.IdMunicipio _
         , entidad.IdDepartamento _
         , entidad.Nombre _
         )
    End Sub

    Public Sub adm_MunicipiosUpdate _
    (ByVal entidad As adm_Municipios, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "adm_MunicipiosUpdate", _
         entidad.IdMunicipio _
         , entidad.IdDepartamento _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "ban_ConciliacionesDetalle"
    Public Function ban_ConciliacionesDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("ban_ConciliacionesDetalleSelectAll").Tables(0)
    End Function



    Public Sub ban_ConciliacionesDetalleInsert _
(ByVal entidad As ban_ConciliacionesDetalle)

        db.ExecuteNonQuery("ban_ConciliacionesDetalleInsert", _
            entidad.IdSucursal _
            , entidad.IdCuentaBancaria _
            , entidad.IdComprobante _
            , entidad.Cheque _
            , entidad.IdCuentaContable _
            , entidad.ProcesadaBanco _
            , entidad.Contabilizar _
            , entidad.Referencia _
            , entidad.Concepto _
            , entidad.MesConcilia _
            , entidad.EjercicioConcilia _
            , entidad.Fecha _
            , entidad.Valor _
            , entidad.Debe _
            , entidad.Haber _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            )
    End Sub

    Public Sub ban_ConciliacionesDetalleInsert _
    (ByVal entidad As ban_ConciliacionesDetalle, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_ConciliacionesDetalleInsert", _
            entidad.IdSucursal _
            , entidad.IdCuentaBancaria _
            , entidad.IdComprobante _
            , entidad.Cheque _
            , entidad.IdCuentaContable _
            , entidad.ProcesadaBanco _
            , entidad.Contabilizar _
            , entidad.Referencia _
            , entidad.Concepto _
            , entidad.MesConcilia _
            , entidad.EjercicioConcilia _
            , entidad.Fecha _
            , entidad.Valor _
            , entidad.Debe _
            , entidad.Haber _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            )
    End Sub

#End Region

#Region "fac_Token"
    Public Function fac_TokenSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_TokenSelectAll").Tables(0)
    End Function

    Public Function fac_TokenSelectByPK _
            (ByVal IdToken As System.Int32 _
            ) As fac_Token

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_TokenSelectByPK", _
            IdToken _
            ).tables(0)

        Dim Entidad As New fac_Token
        If dt.Rows.Count > 0 Then
            entidad.IdToken = dt.rows(0).item("IdToken")
            entidad.Token = dt.rows(0).item("Token")
            entidad.DesdeFecha = dt.rows(0).item("DesdeFecha")
            entidad.HastaFecha = dt.rows(0).item("HastaFecha")

        End If
        Return Entidad
    End Function

    Public Sub fac_TokenDeleteByPK _
            (ByVal IdToken As System.Int32 _
            )

        db.ExecuteNonQuery("fac_TokenDeleteByPK", _
            IdToken _
            )
    End Sub

    Public Sub fac_TokenDeleteByPK _
            (ByVal IdToken As System.Int32 _
            , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_TokenDeleteByPK", _
            IdToken _
            )
    End Sub

    Public Sub fac_TokenInsert _
    (ByVal entidad As fac_Token)

        db.ExecuteNonQuery("fac_TokenInsert", _
            entidad.IdToken _
            , entidad.Token _
            , entidad.DesdeFecha _
            , entidad.HastaFecha _
            )
    End Sub

    Public Sub fac_TokenInsert _
    (ByVal entidad As fac_Token, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_TokenInsert", _
            entidad.IdToken _
            , entidad.Token _
            , entidad.DesdeFecha _
            , entidad.HastaFecha _
            )
    End Sub

    Public Sub fac_TokenUpdate _
    (ByVal entidad As fac_Token)

        db.ExecuteNonQuery("fac_TokenUpdate", _
            entidad.IdToken _
            , entidad.Token _
            , entidad.DesdeFecha _
            , entidad.HastaFecha _
            )
    End Sub

    Public Sub fac_TokenUpdate _
    (ByVal entidad As fac_Token, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_TokenUpdate", _
            entidad.IdToken _
            , entidad.Token _
            , entidad.DesdeFecha _
            , entidad.HastaFecha _
            )
    End Sub

#End Region

#Region "con_Cuentas"
    Public Function con_CuentasSearchByPk(ByVal pIdCuenta As String, ByVal pIdPresupuesto As Integer) As DataTable
        Return db.ExecuteDataSet("con_CuentasSearchByPk", pIdCuenta, pIdPresupuesto).Tables(0)
    End Function
    Public Function con_CuentasSelectAll() As DataTable
        Return db.ExecuteDataSet("con_CuentasSelectAll").Tables(0)
    End Function
    Public Function con_CuentasSelectParaArbol() As DataTable
        Dim sql As String = "select IdCuenta, Nombre "
        sql &= ",Tipo = case when tipo = 0 then 'Elemento' else case when Tipo = 1 then'Rubro' else case when Tipo = 2 then'Cta.de Mayor' else 'Sub-Cuenta' end end end"
        sql &= ",Nivel, Naturaleza= case when Naturaleza = 1 then 'Activo' else case when Naturaleza = 2 then 'Pasivo' else case when Naturaleza=3 then 'Patrimonio' else case when Naturaleza=4 then 'Res.Deudor' else case when Naturaleza=5 then 'Res.Acreedor' else case when Naturaleza=6 then 'Liquidadora' else case when Naturaleza=7 then'Orden Deudoras' else 'Orden Acreedoras' end end end end end end end"
        sql &= ",EsTransaccional, IdCuentaMayor from con_Cuentas"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function con_CuentasSelectByPK(ByVal IdCuenta As System.String) As con_Cuentas

        Dim dt As DataTable = db.ExecuteDataSet("con_CuentasSelectByPK", IdCuenta).Tables(0)

        Dim Entidad As New con_Cuentas
        If dt.Rows.Count > 0 Then
            Entidad.IdCuenta = dt.Rows(0).Item("IdCuenta")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.IdCuentaMayor = dt.Rows(0).Item("IdCuentaMayor")
            Entidad.Tipo = dt.Rows(0).Item("Tipo")
            Entidad.Nivel = dt.Rows(0).Item("Nivel")
            Entidad.Naturaleza = dt.Rows(0).Item("Naturaleza")
            Entidad.EsTransaccional = dt.Rows(0).Item("EsTransaccional")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
        End If
        Return Entidad
    End Function

    Public Sub con_CuentasDeleteByPK(ByVal IdCuenta As System.String)

        db.ExecuteNonQuery("con_CuentasDeleteByPK", IdCuenta)
    End Sub

    Public Sub con_CuentasDeleteByPK(ByVal IdCuenta As System.String, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_CuentasDeleteByPK", IdCuenta)
    End Sub

    Public Sub con_CuentasUpdateNaturaleza(ByVal IdCuenta As String, ByVal Naturaleza As Integer, Optional UpdateSubCuentas As Boolean = False)
        If UpdateSubCuentas Then
            db.ExecuteNonQuery(CommandType.Text, "UPDATE con_Cuentas SET Naturaleza = " & Naturaleza & " WHERE IdCuenta like '" & IdCuenta & "%'")
        Else
            db.ExecuteNonQuery(CommandType.Text, "UPDATE con_Cuentas SET Naturaleza = " & Naturaleza & " WHERE IdCuenta = '" & IdCuenta & "'")
        End If
    End Sub

    Public Sub con_CuentasInsert(ByVal entidad As con_Cuentas)

        db.ExecuteNonQuery("con_CuentasInsert", _
         entidad.IdCuenta _
         , entidad.Nombre _
         , entidad.IdCuentaMayor _
         , entidad.Tipo _
         , entidad.Nivel _
         , entidad.Naturaleza _
         , entidad.EsTransaccional _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion)
    End Sub

    Public Sub con_CuentasInsert(ByVal entidad As con_Cuentas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_CuentasInsert", _
         entidad.IdCuenta _
         , entidad.Nombre _
         , entidad.IdCuentaMayor _
         , entidad.Tipo _
         , entidad.Nivel _
         , entidad.Naturaleza _
         , entidad.EsTransaccional _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion)
    End Sub

    Public Sub con_CuentasUpdate(ByVal entidad As con_Cuentas)

        db.ExecuteNonQuery("con_CuentasUpdate", _
         entidad.IdCuenta _
         , entidad.Nombre _
         , entidad.IdCuentaMayor _
         , entidad.Tipo _
         , entidad.Nivel _
         , entidad.Naturaleza _
         , entidad.EsTransaccional _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion)
    End Sub

    Public Sub con_CuentasUpdate(ByVal entidad As con_Cuentas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_CuentasUpdate", _
         entidad.IdCuenta _
         , entidad.Nombre _
         , entidad.IdCuentaMayor _
         , entidad.Tipo _
         , entidad.Nivel _
         , entidad.Naturaleza _
         , entidad.EsTransaccional _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion)
    End Sub

#End Region

#Region "con_ConceptoCaja"
    Public Function con_ConceptosCajaSelectAll() As DataTable
        Return db.ExecuteDataSet("con_ConceptosCajaSelectAll").Tables(0)
    End Function

    Public Function con_ConceptosCajaSelectByPK _
      (ByVal IdConcepto As System.Int32 _
      ) As con_ConceptosCaja

        Dim dt As DataTable
        dt = db.ExecuteDataSet("con_ConceptosCajaSelectByPK", _
         IdConcepto _
         ).Tables(0)

        Dim Entidad As New con_ConceptosCaja
        If dt.Rows.Count > 0 Then
            Entidad.IdConcepto = dt.Rows(0).Item("IdConcepto")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.IngresoEgreso = dt.Rows(0).Item("IngresoEgreso")
            Entidad.CuentaContable = dt.Rows(0).Item("CuentaContable")
            Entidad.Orden = dt.Rows(0).Item("Orden")

        End If
        Return Entidad
    End Function

    Public Sub con_ConceptosCajaDeleteByPK _
      (ByVal IdConcepto As System.Int32 _
      )

        db.ExecuteNonQuery("con_ConceptosCajaDeleteByPK", _
         IdConcepto _
         )
    End Sub

    Public Sub con_ConceptosCajaDeleteByPK _
      (ByVal IdConcepto As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_ConceptosCajaDeleteByPK", _
         IdConcepto _
         )
    End Sub

    Public Sub con_ConceptosCajaInsert _
    (ByVal entidad As con_ConceptosCaja)

        db.ExecuteNonQuery("con_ConceptosCajaInsert", _
         entidad.IdConcepto _
         , entidad.Nombre _
         , entidad.IngresoEgreso _
         , entidad.CuentaContable _
         , entidad.Orden _
         )
    End Sub

    Public Sub con_ConceptoCajaInsert _
    (ByVal entidad As con_ConceptosCaja, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_ConceptosCajaInsert", _
         entidad.IdConcepto _
         , entidad.Nombre _
         , entidad.IngresoEgreso _
         , entidad.CuentaContable _
         , entidad.Orden _
         )
    End Sub

    Public Sub con_ConceptosCajaUpdate _
    (ByVal entidad As con_ConceptosCaja)

        db.ExecuteNonQuery("con_ConceptosCajaUpdate", _
         entidad.IdConcepto _
         , entidad.Nombre _
         , entidad.IngresoEgreso _
         , entidad.CuentaContable _
         , entidad.Orden _
         )
    End Sub

    Public Sub con_ConceptosCajaUpdate _
    (ByVal entidad As con_ConceptosCaja, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_ConceptosCajaUpdate", _
         entidad.IdConcepto _
         , entidad.Nombre _
         , entidad.IngresoEgreso _
         , entidad.CuentaContable _
         , entidad.Orden _
         )
    End Sub

#End Region
#Region "con_Transacciones"
    Public Function con_TransaccionesSelectAll() As DataTable
        Return db.ExecuteDataSet("con_TransaccionesSelectAll").Tables(0)
    End Function

    Public Function con_TransaccionesSelectByPK _
      (ByVal IdComprobante As System.Guid _
      ) As con_Transacciones

        Dim dt As datatable
        dt = db.ExecuteDataSet("con_TransaccionesSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New con_Transacciones
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.IdPunto = dt.Rows(0).Item("IdPunto")
            entidad.IdConcepto = dt.rows(0).item("IdConcepto")
            entidad.IdCuentaBancaria = dt.rows(0).item("IdCuentaBancaria")
            entidad.Valor = dt.rows(0).item("Valor")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub con_TransaccionesDeleteByPK _
      (ByVal IdComprobante As System.Guid _
      )

        db.ExecuteNonQuery("con_TransaccionesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub con_TransaccionesDeleteByPK _
      (ByVal IdComprobante As System.Guid _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_TransaccionesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub con_TransaccionesInsert _
    (ByVal entidad As con_Transacciones)

        db.ExecuteNonQuery("con_TransaccionesInsert", _
         entidad.IdComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.IdConcepto _
         , entidad.IdCuentaBancaria _
         , entidad.Valor _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub con_TransaccionesInsert _
    (ByVal entidad As con_Transacciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_TransaccionesInsert", _
         entidad.IdComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.IdConcepto _
         , entidad.IdCuentaBancaria _
         , entidad.Valor _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub con_TransaccionesUpdate _
    (ByVal entidad As con_Transacciones)

        db.ExecuteNonQuery("con_TransaccionesUpdate", _
         entidad.IdComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.IdConcepto _
         , entidad.IdCuentaBancaria _
         , entidad.Valor _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub con_TransaccionesUpdate _
    (ByVal entidad As con_Transacciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_TransaccionesUpdate", _
         entidad.IdComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.IdConcepto _
         , entidad.IdCuentaBancaria _
         , entidad.Valor _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "con_CuentasPatrimonio"
    Public Function con_CuentasPatrimonioSelectAll() As DataTable
        Return db.ExecuteDataSet("con_CuentasPatrimonioSelectAll").Tables(0)
    End Function

    Public Function con_CuentasPatrimonioSelectByPK _
      (ByVal IdCuenta As System.String _
      ) As con_CuentasPatrimonio

        Dim dt As datatable
        dt = db.ExecuteDataSet("con_CuentasPatrimonioSelectByPK", _
         IdCuenta _
         ).tables(0)

        Dim Entidad As New con_CuentasPatrimonio
        If dt.Rows.Count > 0 Then
            entidad.IdCuenta = dt.rows(0).item("IdCuenta")
            entidad.IdRubro = dt.rows(0).item("IdRubro")

        End If
        Return Entidad
    End Function

    Public Sub con_CuentasPatrimonioDeleteByPK _
      (ByVal IdCuenta As System.String _
      )

        db.ExecuteNonQuery("con_CuentasPatrimonioDeleteByPK", _
         IdCuenta _
         )
    End Sub

    Public Sub con_CuentasPatrimonioDeleteByPK _
      (ByVal IdCuenta As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_CuentasPatrimonioDeleteByPK", _
         IdCuenta _
         )
    End Sub

    Public Sub con_CuentasPatrimonioInsert _
    (ByVal entidad As con_CuentasPatrimonio)

        db.ExecuteNonQuery("con_CuentasPatrimonioInsert", _
         entidad.IdCuenta _
         , entidad.IdRubro _
         )
    End Sub

    Public Sub con_CuentasPatrimonioInsert _
    (ByVal entidad As con_CuentasPatrimonio, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_CuentasPatrimonioInsert", _
         entidad.IdCuenta _
         , entidad.IdRubro _
         )
    End Sub

    Public Sub con_CuentasPatrimonioUpdate _
    (ByVal entidad As con_CuentasPatrimonio)

        db.ExecuteNonQuery("con_CuentasPatrimonioUpdate", _
         entidad.IdCuenta _
         , entidad.IdRubro _
         )
    End Sub

    Public Sub con_CuentasPatrimonioUpdate _
    (ByVal entidad As con_CuentasPatrimonio, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_CuentasPatrimonioUpdate", _
         entidad.IdCuenta _
         , entidad.IdRubro _
         )
    End Sub

#End Region

#Region "con_Partidas"
    Public Function con_PartidasSelectAll() As DataTable
        Return db.ExecuteDataSet("con_PartidasSelectAll").Tables(0)
    End Function

    Public Function con_PartidasSelectByPK _
      (ByVal IdPartida As System.Int32 _
      ) As con_Partidas

        Dim dt As datatable
        dt = db.ExecuteDataSet("con_PartidasSelectByPK", _
         IdPartida _
         ).tables(0)

        Dim Entidad As New con_Partidas
        If dt.Rows.Count > 0 Then
            Entidad.IdPartida = dt.Rows(0).Item("IdPartida")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            entidad.IdTipo = dt.rows(0).item("IdTipo")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.Actualizada = dt.rows(0).item("Actualizada")
            entidad.IdModuloOrigen = dt.rows(0).item("IdModuloOrigen")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub con_PartidasDeleteByPK _
      (ByVal IdPartida As System.Int32 _
      )

        db.ExecuteNonQuery("con_PartidasDeleteByPK", _
         IdPartida _
         )
    End Sub

    Public Sub con_PartidasDeleteByPK _
      (ByVal IdPartida As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_PartidasDeleteByPK", _
         IdPartida _
         )
    End Sub

    Public Sub con_PartidasInsert _
    (ByVal entidad As con_Partidas)

        db.ExecuteNonQuery("con_PartidasInsert", _
         entidad.IdPartida _
         , entidad.IdSucursal _
         , entidad.IdTipo _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.Actualizada _
         , entidad.IdModuloOrigen _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub con_PartidasInsert _
    (ByVal entidad As con_Partidas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_PartidasInsert", _
         entidad.IdPartida _
         , entidad.IdSucursal _
         , entidad.IdTipo _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.Actualizada _
         , entidad.IdModuloOrigen _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub con_PartidasUpdate _
    (ByVal entidad As con_Partidas)

        db.ExecuteNonQuery("con_PartidasUpdate", _
         entidad.IdPartida _
         , entidad.IdSucursal _
         , entidad.IdTipo _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.Actualizada _
         , entidad.IdModuloOrigen _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub con_PartidasUpdate(ByVal entidad As con_Partidas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_PartidasUpdate", _
         entidad.IdPartida _
         , entidad.IdSucursal _
         , entidad.IdTipo _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.Actualizada _
         , entidad.IdModuloOrigen _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "con_PartidasDetalle"
    Public Function con_PartidasDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("con_PartidasDetalleSelectAll").Tables(0)
    End Function

    Public Function con_PartidasDetalleSelectByPK _
      (ByVal IdPartida As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As con_PartidasDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("con_PartidasDetalleSelectByPK", _
         IdPartida, IdDetalle).Tables(0)

        Dim Entidad As New con_PartidasDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdPartida = dt.rows(0).item("IdPartida")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdCuenta = dt.rows(0).item("IdCuenta")
            entidad.Referencia = dt.rows(0).item("Referencia")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.Debe = dt.rows(0).item("Debe")
            Entidad.Haber = dt.Rows(0).Item("Haber")
            Entidad.IdCentro = dt.Rows(0).Item("IdCentro")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub con_PartidasDetalleDeleteByPK _
      (ByVal IdPartida As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("con_PartidasDetalleDeleteByPK", _
         IdPartida _
         , IdDetalle _
         )
    End Sub

    Public Sub con_PartidasDetalleDeleteByPK _
      (ByVal IdPartida As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_PartidasDetalleDeleteByPK", _
         IdPartida _
         , IdDetalle _
         )
    End Sub

    Public Sub con_PartidasDetalleInsert _
    (ByVal entidad As con_PartidasDetalle)

        db.ExecuteNonQuery("con_PartidasDetalleInsert", _
         entidad.IdPartida _
         , entidad.IdDetalle _
         , entidad.IdCuenta _
         , entidad.Referencia _
         , entidad.Concepto _
         , entidad.Debe _
         , entidad.Haber _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub con_PartidasDetalleInsert _
    (ByVal entidad As con_PartidasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_PartidasDetalleInsert", _
         entidad.IdPartida _
         , entidad.IdDetalle _
         , entidad.IdCuenta _
         , entidad.Referencia _
         , entidad.Concepto _
         , entidad.Debe _
         , entidad.Haber _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub con_PartidasDetalleUpdate _
    (ByVal entidad As con_PartidasDetalle)

        db.ExecuteNonQuery("con_PartidasDetalleUpdate", _
         entidad.IdPartida _
         , entidad.IdDetalle _
         , entidad.IdCuenta _
         , entidad.Referencia _
         , entidad.Concepto _
         , entidad.Debe _
         , entidad.Haber _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub con_PartidasDetalleUpdate _
    (ByVal entidad As con_PartidasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_PartidasDetalleUpdate", _
         entidad.IdPartida _
         , entidad.IdDetalle _
         , entidad.IdCuenta _
         , entidad.Referencia _
         , entidad.Concepto _
         , entidad.Debe _
         , entidad.Haber _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "con_TiposPartida"
    Public Function con_TiposPartidaSelectAll() As DataTable
        Return db.ExecuteDataSet("con_TiposPartidaSelectAll").Tables(0)
    End Function

    Public Function con_TiposPartidaSelectByPK _
      (ByVal IdTipo As System.String _
      ) As con_TiposPartida

        Dim dt As datatable
        dt = db.ExecuteDataSet("con_TiposPartidaSelectByPK", _
         IdTipo _
         ).tables(0)

        Dim Entidad As New con_TiposPartida
        If dt.Rows.Count > 0 Then
            entidad.IdTipo = dt.rows(0).item("IdTipo")
            entidad.Nombre = dt.rows(0).item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub con_TiposPartidaDeleteByPK _
      (ByVal IdTipo As System.String _
      )

        db.ExecuteNonQuery("con_TiposPartidaDeleteByPK", _
         IdTipo _
         )
    End Sub

    Public Sub con_TiposPartidaDeleteByPK _
      (ByVal IdTipo As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_TiposPartidaDeleteByPK", _
         IdTipo _
         )
    End Sub

    Public Sub con_TiposPartidaInsert _
    (ByVal entidad As con_TiposPartida)

        db.ExecuteNonQuery("con_TiposPartidaInsert", _
         entidad.IdTipo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub con_TiposPartidaInsert _
    (ByVal entidad As con_TiposPartida, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_TiposPartidaInsert", _
         entidad.IdTipo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub con_TiposPartidaUpdate _
    (ByVal entidad As con_TiposPartida)

        db.ExecuteNonQuery("con_TiposPartidaUpdate", _
         entidad.IdTipo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub con_TiposPartidaUpdate _
    (ByVal entidad As con_TiposPartida, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_TiposPartidaUpdate", _
         entidad.IdTipo _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "con_CentrosCosto"
    Public Function con_CentrosCostoSelectAll() As DataTable
        Return db.ExecuteDataSet("con_CentrosCostoSelectAll").Tables(0)
    End Function

    Public Function con_CentrosCostoSelectByPK _
      (ByVal IdCentro As System.String _
      ) As con_CentrosCosto

        Dim dt As datatable
        dt = db.ExecuteDataSet("con_CentrosCostoSelectByPK", _
         IdCentro _
         ).tables(0)

        Dim Entidad As New con_CentrosCosto
        If dt.Rows.Count > 0 Then
            entidad.IdCentro = dt.rows(0).item("IdCentro")
            entidad.Nombre = dt.rows(0).item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub con_CentrosCostoDeleteByPK _
      (ByVal IdCentro As System.String _
      )

        db.ExecuteNonQuery("con_CentrosCostoDeleteByPK", _
         IdCentro _
         )
    End Sub

    Public Sub con_CentrosCostoDeleteByPK _
      (ByVal IdCentro As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_CentrosCostoDeleteByPK", _
         IdCentro _
         )
    End Sub

    Public Sub con_CentrosCostoInsert _
    (ByVal entidad As con_CentrosCosto)

        db.ExecuteNonQuery("con_CentrosCostoInsert", _
         entidad.IdCentro _
         , entidad.Nombre _
         )
    End Sub

    Public Sub con_CentrosCostoInsert _
    (ByVal entidad As con_CentrosCosto, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_CentrosCostoInsert", _
         entidad.IdCentro _
         , entidad.Nombre _
         )
    End Sub

    Public Sub con_CentrosCostoUpdate _
    (ByVal entidad As con_CentrosCosto)

        db.ExecuteNonQuery("con_CentrosCostoUpdate", _
         entidad.IdCentro _
         , entidad.Nombre _
         )
    End Sub

    Public Sub con_CentrosCostoUpdate _
    (ByVal entidad As con_CentrosCosto, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_CentrosCostoUpdate", _
         entidad.IdCentro _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "con_CentrosMayor"
    Public Function con_CentrosMayorSelectAll() As DataTable
        Return db.ExecuteDataSet("con_CentrosMayorSelectAll").Tables(0)
    End Function

    Public Function con_CentrosMayorSelectByPK _
      (ByVal IdCuentaMayor As System.String _
      , ByVal IdCentro As System.String _
      ) As con_CentrosMayor

        Dim dt As datatable
        dt = db.ExecuteDataSet("con_CentrosMayorSelectByPK", _
         IdCuentaMayor _
         , IdCentro _
         ).tables(0)

        Dim Entidad As New con_CentrosMayor
        If dt.Rows.Count > 0 Then
            entidad.IdCuentaMayor = dt.rows(0).item("IdCuentaMayor")
            entidad.IdCentro = dt.rows(0).item("IdCentro")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub con_CentrosMayorDeleteByPK _
      (ByVal IdCuentaMayor As System.String _
      , ByVal IdCentro As System.String _
      )

        db.ExecuteNonQuery("con_CentrosMayorDeleteByPK", _
         IdCuentaMayor _
         , IdCentro _
         )
    End Sub

    Public Sub con_CentrosMayorDeleteByPK _
      (ByVal IdCuentaMayor As System.String _
      , ByVal IdCentro As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_CentrosMayorDeleteByPK", _
         IdCuentaMayor _
         , IdCentro _
         )
    End Sub

    Public Sub con_CentrosMayorInsert _
    (ByVal entidad As con_CentrosMayor)

        db.ExecuteNonQuery("con_CentrosMayorInsert", _
         entidad.IdCuentaMayor _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub con_CentrosMayorInsert _
    (ByVal entidad As con_CentrosMayor, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_CentrosMayorInsert", _
         entidad.IdCuentaMayor _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub con_CentrosMayorUpdate _
    (ByVal entidad As con_CentrosMayor)

        db.ExecuteNonQuery("con_CentrosMayorUpdate", _
         entidad.IdCuentaMayor _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub con_CentrosMayorUpdate _
    (ByVal entidad As con_CentrosMayor, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_CentrosMayorUpdate", _
         entidad.IdCuentaMayor _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "con_NIIFConceptos"
    Public Function con_NIIFConceptosSelectAll() As DataTable
        Return db.ExecuteDataSet("con_NIIFConceptosSelectAll").Tables(0)
    End Function

    Public Function con_NIIFConceptosSelectByPK _
      (ByVal IdReporte As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As con_NIIFConceptos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("con_NIIFConceptosSelectByPK", _
         IdReporte _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New con_NIIFConceptos
        If dt.Rows.Count > 0 Then
            Entidad.IdReporte = dt.Rows(0).Item("IdReporte")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.NumeroLinea = dt.Rows(0).Item("NumeroLinea")
            Entidad.ConceptoNIIF = dt.Rows(0).Item("ConceptoNIIF")
            Entidad.Resta = dt.Rows(0).Item("Resta")
            Entidad.IdCuentaContable = dt.Rows(0).Item("IdCuentaContable")
            Entidad.CuentaPadre = dt.Rows(0).Item("CuentaPadre")
            Entidad.SubTotal = dt.Rows(0).Item("SubTotal")
            Entidad.ImprimirLinea = dt.Rows(0).Item("ImprimirLinea")
            Entidad.Valor1 = dt.Rows(0).Item("Valor1")
            Entidad.Valor2 = dt.Rows(0).Item("Valor2")
            Entidad.EsSubTitulo = dt.Rows(0).Item("EsSubTitulo")
            Entidad.ImprimirLineaBajo = dt.Rows(0).Item("ImprimirLineaBajo")

        End If
        Return Entidad
    End Function

    Public Sub con_NIIFConceptosDeleteByPK _
      (ByVal IdReporte As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("con_NIIFConceptosDeleteByPK", _
         IdReporte _
         , IdDetalle _
         )
    End Sub

    Public Sub con_NIIFConceptosDeleteByPK _
      (ByVal IdReporte As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_NIIFConceptosDeleteByPK", _
         IdReporte _
         , IdDetalle _
         )
    End Sub

    Public Sub con_NIIFConceptosInsert _
    (ByVal entidad As con_NIIFConceptos)

        db.ExecuteNonQuery("con_NIIFConceptosInsert", _
         entidad.IdReporte _
         , entidad.IdDetalle _
         , entidad.NumeroLinea _
         , entidad.ConceptoNIIF _
         , entidad.Resta _
         , entidad.IdCuentaContable _
         , entidad.CuentaPadre _
         , entidad.SubTotal _
         , entidad.ImprimirLinea _
         , entidad.Valor1 _
         , entidad.Valor2 _
         , entidad.EsSubTitulo _
         , entidad.ImprimirLineaBajo _
         )
    End Sub

    Public Sub con_NIIFConceptosInsert _
    (ByVal entidad As con_NIIFConceptos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_NIIFConceptosInsert", _
         entidad.IdReporte _
         , entidad.IdDetalle _
         , entidad.NumeroLinea _
         , entidad.ConceptoNIIF _
         , entidad.Resta _
         , entidad.IdCuentaContable _
         , entidad.CuentaPadre _
         , entidad.SubTotal _
         , entidad.ImprimirLinea _
         , entidad.Valor1 _
         , entidad.Valor2 _
         , entidad.EsSubTitulo _
         , entidad.ImprimirLineaBajo _
         )
    End Sub

    Public Sub con_NIIFConceptosUpdate _
    (ByVal entidad As con_NIIFConceptos)

        db.ExecuteNonQuery("con_NIIFConceptosUpdate", _
         entidad.IdReporte _
         , entidad.IdDetalle _
         , entidad.NumeroLinea _
         , entidad.ConceptoNIIF _
         , entidad.Resta _
         , entidad.IdCuentaContable _
         , entidad.CuentaPadre _
         , entidad.SubTotal _
         , entidad.ImprimirLinea _
         , entidad.Valor1 _
         , entidad.Valor2 _
         , entidad.EsSubTitulo _
         , entidad.ImprimirLineaBajo _
         )
    End Sub

    Public Sub con_NIIFConceptosUpdate _
    (ByVal entidad As con_NIIFConceptos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_NIIFConceptosUpdate", _
         entidad.IdReporte _
         , entidad.IdDetalle _
         , entidad.NumeroLinea _
         , entidad.ConceptoNIIF _
         , entidad.Resta _
         , entidad.IdCuentaContable _
         , entidad.CuentaPadre _
         , entidad.SubTotal _
         , entidad.ImprimirLinea _
         , entidad.Valor1 _
         , entidad.Valor2 _
         , entidad.EsSubTitulo _
         , entidad.ImprimirLineaBajo _
         )
    End Sub
#End Region
#Region "con_RubroFlujo"
    Public Function con_RubroFlujoSelectAll() As DataTable
        Return db.ExecuteDataSet("con_RubroFlujoSelectAll").Tables(0)
    End Function

    Public Function con_RubroFlujoSelectByPK _
      (ByVal IdRubro As System.Int32 _
      ) As con_RubroFlujo

        Dim dt As datatable
        dt = db.ExecuteDataSet("con_RubroFlujoSelectByPK", _
         IdRubro _
         ).tables(0)

        Dim Entidad As New con_RubroFlujo
        If dt.Rows.Count > 0 Then
            entidad.IdRubro = dt.rows(0).item("IdRubro")
            entidad.Nombre = dt.rows(0).item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub con_RubroFlujoDeleteByPK _
      (ByVal IdRubro As System.Int32 _
      )

        db.ExecuteNonQuery("con_RubroFlujoDeleteByPK", _
         IdRubro _
         )
    End Sub

    Public Sub con_RubroFlujoDeleteByPK _
      (ByVal IdRubro As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_RubroFlujoDeleteByPK", _
         IdRubro _
         )
    End Sub

    Public Sub con_RubroFlujoInsert _
    (ByVal entidad As con_RubroFlujo)

        db.ExecuteNonQuery("con_RubroFlujoInsert", _
         entidad.IdRubro _
         , entidad.Nombre _
         )
    End Sub

    Public Sub con_RubroFlujoInsert _
    (ByVal entidad As con_RubroFlujo, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_RubroFlujoInsert", _
         entidad.IdRubro _
         , entidad.Nombre _
         )
    End Sub

    Public Sub con_RubroFlujoUpdate _
    (ByVal entidad As con_RubroFlujo)

        db.ExecuteNonQuery("con_RubroFlujoUpdate", _
         entidad.IdRubro _
         , entidad.Nombre _
         )
    End Sub

    Public Sub con_RubroFlujoUpdate _
    (ByVal entidad As con_RubroFlujo, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_RubroFlujoUpdate", _
         entidad.IdRubro _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "con_RubroFlujoDetalle"
    Public Function con_RubroFlujoDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("con_RubroFlujoDetalleSelectAll").Tables(0)
    End Function

    Public Function con_RubroFlujoDetalleSelectByPK _
      (ByVal IdCuentaMayor As System.String _
      ) As con_RubroFlujoDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("con_RubroFlujoDetalleSelectByPK", _
         IdCuentaMayor _
         ).tables(0)

        Dim Entidad As New con_RubroFlujoDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdCuentaMayor = dt.rows(0).item("IdCuentaMayor")
            entidad.IdRubro = dt.rows(0).item("IdRubro")
            entidad.Concepto = dt.rows(0).item("Concepto")

        End If
        Return Entidad
    End Function

    Public Sub con_RubroFlujoDetalleDeleteByPK _
      (ByVal IdCuentaMayor As System.String _
      )

        db.ExecuteNonQuery("con_RubroFlujoDetalleDeleteByPK", _
         IdCuentaMayor _
         )
    End Sub

    Public Sub con_RubroFlujoDetalleDeleteByPK _
      (ByVal IdCuentaMayor As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_RubroFlujoDetalleDeleteByPK", _
         IdCuentaMayor _
         )
    End Sub

    Public Sub con_RubroFlujoDetalleInsert _
    (ByVal entidad As con_RubroFlujoDetalle)

        db.ExecuteNonQuery("con_RubroFlujoDetalleInsert", _
         entidad.IdCuentaMayor _
         , entidad.IdRubro _
         , entidad.Concepto _
         )
    End Sub

    Public Sub con_RubroFlujoDetalleInsert _
    (ByVal entidad As con_RubroFlujoDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_RubroFlujoDetalleInsert", _
         entidad.IdCuentaMayor _
         , entidad.IdRubro _
         , entidad.Concepto _
         )
    End Sub

    Public Sub con_RubroFlujoDetalleUpdate _
    (ByVal entidad As con_RubroFlujoDetalle)

        db.ExecuteNonQuery("con_RubroFlujoDetalleUpdate", _
         entidad.IdCuentaMayor _
         , entidad.IdRubro _
         , entidad.Concepto _
         )
    End Sub

    Public Sub con_RubroFlujoDetalleUpdate _
    (ByVal entidad As con_RubroFlujoDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_RubroFlujoDetalleUpdate", _
         entidad.IdCuentaMayor _
         , entidad.IdRubro _
         , entidad.Concepto _
         )
    End Sub

#End Region


#Region "con_FormulasFinancieras"
	Public Function con_FormulasFinancierasSelectAll() As DataTable
		Return db.ExecuteDataSet("con_FormulasFinancierasSelectAll").Tables(0)
	End Function

	Public Function con_FormulasFinancierasSelectByPK _
		(ByVal Id As System.Int32
		) As con_FormulasFinancieras

		Dim dt As DataTable
		dt = db.ExecuteDataSet("con_FormulasFinancierasSelectByPK",
		Id
		).Tables(0)

		Dim Entidad As New con_FormulasFinancieras
		If dt.Rows.Count > 0 Then
			Entidad.Id = dt.Rows(0).Item("Id")
			Entidad.IdFormula = dt.Rows(0).Item("IdFormula")
			Entidad.ValorFormula = dt.Rows(0).Item("ValorFormula")
			Entidad.Descripcion = dt.Rows(0).Item("Descripcion")
			Entidad.TipoValor = dt.Rows(0).Item("TipoValor")

		End If
		Return Entidad
	End Function



	Public Sub con_FormulasFinancierasDeleteByPK _
		(ByVal Id As System.Int32
		)

		db.ExecuteNonQuery("con_FormulasFinancierasDeleteByPK",
		Id
		)
	End Sub

	Public Sub con_FormulasFinancierasDeleteByPK _
		(ByVal Id As System.Int32 _
		, Transaccion As DbTransaction)

		db.ExecuteNonQuery(Transaccion, "con_FormulasFinancierasDeleteByPK",
		Id
		)
	End Sub

	Public Sub con_FormulasFinancierasInsert _
(ByVal entidad As con_FormulasFinancieras)

		db.ExecuteNonQuery("con_FormulasFinancierasInsert",
		entidad.Id _
		, entidad.IdFormula _
		, entidad.ValorFormula _
		, entidad.Descripcion _
		, entidad.TipoValor
		)
	End Sub

	Public Sub con_FormulasFinancierasInsert _
(ByVal entidad As con_FormulasFinancieras, Transaccion As DbTransaction)

		db.ExecuteNonQuery(Transaccion, "con_FormulasFinancierasInsert",
		entidad.Id _
		, entidad.IdFormula _
		, entidad.ValorFormula _
		, entidad.Descripcion _
		, entidad.TipoValor
		)
	End Sub

	Public Sub con_FormulasFinancierasUpdate _
(ByVal entidad As con_FormulasFinancieras)

		db.ExecuteNonQuery("con_FormulasFinancierasUpdate",
		entidad.Id _
		, entidad.IdFormula _
		, entidad.ValorFormula _
		, entidad.Descripcion _
		, entidad.TipoValor
		)
	End Sub

	Public Sub con_FormulasFinancierasUpdate _
(ByVal entidad As con_FormulasFinancieras, Transaccion As DbTransaction)

		db.ExecuteNonQuery(Transaccion, "con_FormulasFinancierasUpdate",
		entidad.Id _
		, entidad.IdFormula _
		, entidad.ValorFormula _
		, entidad.Descripcion _
		, entidad.TipoValor
		)
	End Sub

#End Region

	'PRESUPUESTOS
#Region "con_Presupuestos"
	Public Function con_PresupuestosSelectAll() As DataTable
        Return db.ExecuteDataSet("con_PresupuestosSelectAll").Tables(0)
    End Function

    Public Function con_PresupuestosSelectByPK _
        (ByVal IdPresupuesto As System.Int32
        ) As con_Presupuestos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("con_PresupuestosSelectByPK",
        IdPresupuesto
        ).Tables(0)

        Dim Entidad As New con_Presupuestos
        If dt.Rows.Count > 0 Then
            Entidad.IdPresupuesto = dt.Rows(0).Item("IdPresupuesto")
            Entidad.Periodo = dt.Rows(0).Item("Periodo")
            Entidad.Comentario = dt.Rows(0).Item("Comentario")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub con_PresupuestosDeleteByPK _
        (ByVal IdPresupuesto As System.Int32
        )

        db.ExecuteNonQuery("con_PresupuestosDeleteByPK",
        IdPresupuesto
        )
    End Sub

    Public Sub con_PresupuestosDeleteByPK _
        (ByVal IdPresupuesto As System.Int32 _
        , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_PresupuestosDeleteByPK",
        IdPresupuesto
        )
    End Sub

    Public Sub con_PresupuestosInsert _
(ByVal entidad As con_Presupuestos)

        db.ExecuteNonQuery("con_PresupuestosInsert",
        entidad.IdPresupuesto _
        , entidad.Periodo _
        , entidad.Comentario _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion
        )
    End Sub

    Public Sub con_PresupuestosInsert _
(ByVal entidad As con_Presupuestos, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_PresupuestosInsert",
        entidad.IdPresupuesto _
        , entidad.Periodo _
        , entidad.Comentario _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion
        )
    End Sub

    Public Sub con_PresupuestosUpdate _
(ByVal entidad As con_Presupuestos)

        db.ExecuteNonQuery("con_PresupuestosUpdate",
        entidad.IdPresupuesto _
        , entidad.Periodo _
        , entidad.Comentario _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion
        )
    End Sub

    Public Sub con_PresupuestosUpdate _
(ByVal entidad As con_Presupuestos, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_PresupuestosUpdate",
        entidad.IdPresupuesto _
        , entidad.Periodo _
        , entidad.Comentario _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion
        )
    End Sub

#End Region
#Region "con_PresupuestosDetalle"
    Public Function con_PresupuestosDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("con_PresupuestosDetalleSelectAll").Tables(0)
    End Function

    Public Function con_PresupuestosDetalleSelectByPK _
        (ByVal IdPresupuesto As System.Int32 _
        , ByVal IdCuenta As System.String
        ) As con_PresupuestosDetalle

        Dim dt As DataTable
        dt = db.ExecuteDataSet("con_PresupuestosDetalleSelectByPK",
        IdPresupuesto _
        , IdCuenta
        ).Tables(0)

        Dim Entidad As New con_PresupuestosDetalle
        If dt.Rows.Count > 0 Then
            Entidad.IdPresupuesto = dt.Rows(0).Item("IdPresupuesto")
            Entidad.IdCuenta = dt.Rows(0).Item("IdCuenta")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Mes01 = dt.Rows(0).Item("Mes01")
            Entidad.Mes02 = dt.Rows(0).Item("Mes02")
            Entidad.Mes03 = dt.Rows(0).Item("Mes03")
            Entidad.Mes04 = dt.Rows(0).Item("Mes04")
            Entidad.Mes05 = dt.Rows(0).Item("Mes05")
            Entidad.Mes06 = dt.Rows(0).Item("Mes06")
            Entidad.Mes07 = dt.Rows(0).Item("Mes07")
            Entidad.Mes08 = dt.Rows(0).Item("Mes08")
            Entidad.Mes09 = dt.Rows(0).Item("Mes09")
            Entidad.Mes10 = dt.Rows(0).Item("Mes10")
            Entidad.Mes11 = dt.Rows(0).Item("Mes11")
            Entidad.Mes12 = dt.Rows(0).Item("Mes12")

        End If
        Return Entidad
    End Function

    Public Sub con_PresupuestosDetalleDeleteByPK _
        (ByVal IdPresupuesto As System.Int32 _
        , ByVal IdCuenta As System.String
        )

        db.ExecuteNonQuery("con_PresupuestosDetalleDeleteByPK",
        IdPresupuesto _
        , IdCuenta
        )
    End Sub

    Public Sub con_PresupuestosDetalleDeleteByPK _
        (ByVal IdPresupuesto As System.Int32 _
        , ByVal IdCuenta As System.String _
        , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_PresupuestosDetalleDeleteByPK",
        IdPresupuesto _
        , IdCuenta
        )
    End Sub

    Public Sub con_PresupuestosDetalleInsert _
(ByVal entidad As con_PresupuestosDetalle)

        db.ExecuteNonQuery("con_PresupuestosDetalleInsert",
        entidad.IdPresupuesto _
        , entidad.IdCuenta _
        , entidad.Nombre _
        , entidad.Mes01 _
        , entidad.Mes02 _
        , entidad.Mes03 _
        , entidad.Mes04 _
        , entidad.Mes05 _
        , entidad.Mes06 _
        , entidad.Mes07 _
        , entidad.Mes08 _
        , entidad.Mes09 _
        , entidad.Mes10 _
        , entidad.Mes11 _
        , entidad.Mes12
        )
    End Sub

    Public Sub con_PresupuestosDetalleInsert _
(ByVal entidad As con_PresupuestosDetalle, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_PresupuestosDetalleInsert",
        entidad.IdPresupuesto _
        , entidad.IdCuenta _
        , entidad.Nombre _
        , entidad.Mes01 _
        , entidad.Mes02 _
        , entidad.Mes03 _
        , entidad.Mes04 _
        , entidad.Mes05 _
        , entidad.Mes06 _
        , entidad.Mes07 _
        , entidad.Mes08 _
        , entidad.Mes09 _
        , entidad.Mes10 _
        , entidad.Mes11 _
        , entidad.Mes12
        )
    End Sub

    Public Sub con_PresupuestosDetalleUpdate _
(ByVal entidad As con_PresupuestosDetalle)

        db.ExecuteNonQuery("con_PresupuestosDetalleUpdate",
        entidad.IdPresupuesto _
        , entidad.IdCuenta _
        , entidad.Nombre _
        , entidad.Mes01 _
        , entidad.Mes02 _
        , entidad.Mes03 _
        , entidad.Mes04 _
        , entidad.Mes05 _
        , entidad.Mes06 _
        , entidad.Mes07 _
        , entidad.Mes08 _
        , entidad.Mes09 _
        , entidad.Mes10 _
        , entidad.Mes11 _
        , entidad.Mes12
        )
    End Sub

    Public Sub con_PresupuestosDetalleUpdate _
(ByVal entidad As con_PresupuestosDetalle, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "con_PresupuestosDetalleUpdate",
        entidad.IdPresupuesto _
        , entidad.IdCuenta _
        , entidad.Nombre _
        , entidad.Mes01 _
        , entidad.Mes02 _
        , entidad.Mes03 _
        , entidad.Mes04 _
        , entidad.Mes05 _
        , entidad.Mes06 _
        , entidad.Mes07 _
        , entidad.Mes08 _
        , entidad.Mes09 _
        , entidad.Mes10 _
        , entidad.Mes11 _
        , entidad.Mes12
        )
    End Sub

#End Region

#Region "pre_DepartamentosSucursal"
    Public Function pre_DepartamentosSucursalSelectAll() As DataTable
        Return db.ExecuteDataSet("pre_DepartamentosSucursalSelectAll").Tables(0)
    End Function

    Public Function pre_DepartamentosSucursalSelectByPK _
      (ByVal IdDepartamentoSucursalPk As Int32 _
      ) As pre_DepartamentosSucursal

        Dim dt As DataTable
        dt = db.ExecuteDataSet("pre_DepartamentosSucursalSelectByPK", _
         IdDepartamentoSucursalPk _
         ).Tables(0)

        Dim Entidad As New pre_DepartamentosSucursal
        If dt.Rows.Count > 0 Then
            Entidad.IdDepartamentoSucursalPk = dt.Rows(0).Item("IdDepartamentoSucursalPk")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Descripcion = dt.Rows(0).Item("Descripcion")
            Entidad.Encargado = dt.Rows(0).Item("Encargado")
            Entidad.IdSucursalFk = dt.Rows(0).Item("IdSucursalFk")

        End If
        Return Entidad
    End Function

    Public Sub pre_DepartamentosSucursalDeleteByPK _
      (ByVal IdDepartamentoSucursalPk As Int32 _
      )

        db.ExecuteNonQuery("pre_DepartamentosSucursalDeleteByPK", _
         IdDepartamentoSucursalPk _
         )
    End Sub

    Public Sub pre_DepartamentosSucursalDeleteByPK _
      (ByVal IdDepartamentoSucursalPk As Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pre_DepartamentosSucursalDeleteByPK", _
         IdDepartamentoSucursalPk _
         )
    End Sub

    Public Sub pre_DepartamentosSucursalInsert _
    (ByVal entidad As pre_DepartamentosSucursal)

        db.ExecuteNonQuery("pre_DepartamentosSucursalInsert", _
         fd.ObtenerUltimoId("pre_DepartamentosSucursal", "IdDepartamentoSucursalPk") + 1 _
         , entidad.Nombre _
         , entidad.Descripcion _
         , entidad.Encargado _
         , entidad.IdSucursalFk _
         )
    End Sub

    Public Sub pre_DepartamentosSucursalInsert _
    (ByVal entidad As pre_DepartamentosSucursal, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pre_DepartamentosSucursalInsert", _
         fd.ObtenerUltimoId("pre_DepartamentosSucursal", "IdDepartamentoSucursalPk") + 1 _
         , entidad.Nombre _
         , entidad.Descripcion _
         , entidad.Encargado _
         , entidad.IdSucursalFk _
         )
    End Sub

    Public Sub pre_DepartamentosSucursalUpdate _
    (ByVal entidad As pre_DepartamentosSucursal)

        db.ExecuteNonQuery("pre_DepartamentosSucursalUpdate", _
         entidad.IdDepartamentoSucursalPk _
         , entidad.Nombre _
         , entidad.Descripcion _
         , entidad.Encargado _
         , entidad.IdSucursalFk _
         )
    End Sub

    Public Sub pre_DepartamentosSucursalUpdate _
    (ByVal entidad As pre_DepartamentosSucursal, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pre_DepartamentosSucursalUpdate", _
         entidad.IdDepartamentoSucursalPk _
         , entidad.Nombre _
         , entidad.Descripcion _
         , entidad.Encargado _
         , entidad.IdSucursalFk _
         )
    End Sub

#End Region

#Region "pre_DetallesPresupuesto"

    Public Function pre_DetallesPresupuestoSelectAll() As DataTable
        Return db.ExecuteDataSet("pre_DetallesPresupuestoSelectAll").Tables(0)
    End Function

    Public Function pre_DetallesPresupuestoSelectByCuenta(ByVal pIdPreCuenta As Integer, ByVal pMes As pre_DetallesPresupuesto.Meses) As DataTable
        Return db.ExecuteDataSet("pre_DetallesPresupuestoSelectByCuenta", pIdPreCuenta, pMes).Tables(0)
    End Function

    Public Function pre_ObtenerTotalPresupuestoMes(ByVal pMes As Integer, ByVal pIdPreCuenta As Integer) As Decimal
        Dim total = fd.SiEsNulo(db.ExecuteScalar("pre_ObtenerPresupuestoMes", pMes, pIdPreCuenta), 0)
        Return CType(total, Decimal)
    End Function

    Public Function pre_ObtenerNumeroEdicionesMes(ByVal pMes As Integer, ByVal pIdPreCuenta As Integer) As Integer
        Dim cantidad = fd.SiEsNulo(db.ExecuteScalar("pre_ObtenerNumeroEdiciones", pIdPreCuenta, pMes), 0)
        Return CType(cantidad, Integer)
    End Function

    Public Function pre_PresupuestosSelectRepeated(ByVal pIdSucursal As Integer, ByVal pIdDepto As Integer, ByVal pIdCetoCosto As Integer, ByVal pAnio As Integer) As Integer
        Dim cantidad = fd.SiEsNulo(db.ExecuteScalar("pre_PresupuestosSelectRepeated", pIdSucursal, pIdDepto, pIdCetoCosto, pAnio), 0)
        Return CType(cantidad, Integer)
    End Function

    Public Function pre_ObtenerTotalPresupuesto(ByVal pIdPreCuenta As Integer) As Decimal
        Dim total = fd.SiEsNulo(db.ExecuteScalar("pre_ObtenerTotalPresupuesto", pIdPreCuenta), 0)
        Return CType(total, Decimal)
    End Function

    Public Function pre_DetallesPresupuestoSelectByPK _
      (ByVal IdDetallePresupuestoPk As Int32 _
      ) As pre_DetallesPresupuesto

        Dim dt As DataTable
        dt = db.ExecuteDataSet("pre_DetallesPresupuestoSelectByPK", _
         IdDetallePresupuestoPk _
         ).Tables(0)

        Dim Entidad As New pre_DetallesPresupuesto
        If dt.Rows.Count > 0 Then
            Entidad.IdDetallePresupuestoPk = dt.Rows(0).Item("IdDetallePresupuestoPk")
            Entidad.IdPresupuestoFk = dt.Rows(0).Item("IdPresupuestoCuentaFk")
            Entidad.Mes = dt.Rows(0).Item("Mes")
            Entidad.Valor = dt.Rows(0).Item("Valor")
            Entidad.Comentario = dt.Rows(0).Item("Comentario")
            Entidad.FechaCreacion = dt.Rows(0).Item("FechaCreacion")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")

        End If
        Return Entidad
    End Function

    Public Sub pre_DetallesPresupuestoDeleteByPK _
      (ByVal IdDetallePresupuestoPk As Int32 _
      )

        db.ExecuteNonQuery("pre_DetallesPresupuestoDeleteByPK", _
         IdDetallePresupuestoPk _
         )
    End Sub

    Public Sub pre_DetallesPresupuestoDeleteByPK _
      (ByVal IdDetallePresupuestoPk As Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pre_DetallesPresupuestoDeleteByPK", _
         IdDetallePresupuestoPk _
         )
    End Sub

    Public Sub pre_DetallesPresupuestoInsert _
    (ByVal entidad As pre_DetallesPresupuesto)

        db.ExecuteNonQuery("pre_DetallesPresupuestoInsert", _
         fd.ObtenerUltimoId("pre_DetallesPresupuesto", "IdDetallePresupuestoPk") + 1 _
         , entidad.IdPresupuestoFk _
         , entidad.Mes _
         , entidad.Valor _
         , entidad.Comentario _
         , entidad.FechaCreacion _
         , entidad.CreadoPor _
         )
    End Sub

    Public Sub pre_DetallesPresupuestoInsert _
    (ByVal entidad As pre_DetallesPresupuesto, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pre_DetallesPresupuestoInsert", _
         fd.ObtenerUltimoId("pre_DetallesPresupuesto", "IdDetallePresupuestoPk") + 1 _
         , entidad.IdPresupuestoFk _
         , entidad.Mes _
         , entidad.Valor _
         , entidad.Comentario _
         , entidad.FechaCreacion _
         , entidad.CreadoPor _
         )
    End Sub

    Public Sub pre_DetallesPresupuestoUpdate _
    (ByVal entidad As pre_DetallesPresupuesto)

        db.ExecuteNonQuery("pre_DetallesPresupuestoUpdate", _
         entidad.IdDetallePresupuestoPk _
         , entidad.IdPresupuestoFk _
         , entidad.Mes _
         , entidad.Valor _
         , entidad.Comentario _
         , entidad.FechaCreacion _
         , entidad.CreadoPor _
         )
    End Sub

    Public Sub pre_DetallesPresupuestoUpdate _
    (ByVal entidad As pre_DetallesPresupuesto, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pre_DetallesPresupuestoUpdate", _
         entidad.IdDetallePresupuestoPk _
         , entidad.IdPresupuestoFk _
         , entidad.Mes _
         , entidad.Valor _
         , entidad.Comentario _
         , entidad.FechaCreacion _
         , entidad.CreadoPor _
         )
    End Sub

#End Region

#Region "pre_PresupuestoCuenta"

    Public Function pre_PresupuestoCuentaSelectAll() As DataTable
        Return db.ExecuteDataSet("pre_PresupuestoCuentaSelectAll").Tables(0)
    End Function

    Public Function pre_PresupuestoCuentaSelectByPresupuesto(ByVal pIdPresupuesto As Integer) As DataTable
        Return db.ExecuteDataSet("pre_PresupuestoCuentaSelectByPresupuesto", pIdPresupuesto).Tables(0)
    End Function

    Public Function pre_PresupuestoCuentaSelectByPK _
      (ByVal IdPresupuestoCuentaPk As Int32 _
      ) As pre_PresupuestoCuenta

        Dim dt As DataTable
        dt = db.ExecuteDataSet("pre_PresupuestoCuentaSelectByPK", _
         IdPresupuestoCuentaPk _
         ).Tables(0)

        Dim Entidad As New pre_PresupuestoCuenta
        If dt.Rows.Count > 0 Then
            Entidad.IdPresupuestoCuentaPk = dt.Rows(0).Item("IdPresupuestoCuentaPk")
            Entidad.IdCuentaFk = dt.Rows(0).Item("IdCuentaFk")
            Entidad.IdPresupuestoFk = dt.Rows(0).Item("IdPresupuestoFk")

        End If
        Return Entidad
    End Function

    Public Sub pre_PresupuestoCuentaDeleteByPK _
      (ByVal IdPresupuestoCuentaPk As Int32 _
      )

        db.ExecuteNonQuery("pre_PresupuestoCuentaDeleteByPK", _
         IdPresupuestoCuentaPk _
         )
    End Sub

    Public Sub pre_PresupuestoCuentaDeleteByPK _
      (ByVal IdPresupuestoCuentaPk As Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pre_PresupuestoCuentaDeleteByPK", _
         IdPresupuestoCuentaPk _
         )
    End Sub

    Public Sub pre_PresupuestoCuentaInsert _
    (ByVal entidad As pre_PresupuestoCuenta)

        db.ExecuteNonQuery("pre_PresupuestoCuentaInsert", _
         fd.ObtenerUltimoId("pre_PresupuestoCuenta", "IdPresupuestoCuentaPk") + 1 _
         , entidad.IdCuentaFk _
         , entidad.IdPresupuestoFk _
         )
    End Sub

    Public Sub pre_PresupuestoCuentaInsert _
    (ByVal entidad As pre_PresupuestoCuenta, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pre_PresupuestoCuentaInsert", _
         fd.ObtenerUltimoId("pre_PresupuestoCuenta", "IdPresupuestoCuentaPk") + 1 _
         , entidad.IdCuentaFk _
         , entidad.IdPresupuestoFk _
         )
    End Sub

    Public Sub pre_PresupuestoCuentaUpdate _
    (ByVal entidad As pre_PresupuestoCuenta)

        db.ExecuteNonQuery("pre_PresupuestoCuentaUpdate", _
         entidad.IdPresupuestoCuentaPk _
         , entidad.IdCuentaFk _
         , entidad.IdPresupuestoFk _
         )
    End Sub

    Public Sub pre_PresupuestoCuentaUpdate _
    (ByVal entidad As pre_PresupuestoCuenta, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pre_PresupuestoCuentaUpdate", _
         entidad.IdPresupuestoCuentaPk _
         , entidad.IdCuentaFk _
         , entidad.IdPresupuestoFk _
         )
    End Sub

#End Region

#Region "pre_Presupuestos"
    Public Function pre_PresupuestosSelectAll() As DataTable
        Return db.ExecuteDataSet("pre_PresupuestosSelectAll").Tables(0)
    End Function

    Public Function pre_PresupuestosSelectByPK _
      (ByVal IdPresupuestoPk As Int32 _
      ) As pre_Presupuestos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("pre_PresupuestosSelectByPK", _
         IdPresupuestoPk _
         ).Tables(0)

        Dim Entidad As New pre_Presupuestos
        If dt.Rows.Count > 0 Then
            Entidad.IdPresupuestoPk = dt.Rows(0).Item("IdPresupuestoPk")
            Entidad.IdSucursalFk = dt.Rows(0).Item("IdSucursalFk")
            Entidad.IdDepartamentoFk = dt.Rows(0).Item("IdDepartamentoFk")
            Entidad.IdCentroCostosFk = dt.Rows(0).Item("IdCentroCostosFk")
            Entidad.Anio = dt.Rows(0).Item("Anio")
            Entidad.Comentario = dt.Rows(0).Item("Comentario")
            Entidad.FechaCreacion = dt.Rows(0).Item("FechaCreacion")

        End If
        Return Entidad
    End Function

    Public Sub pre_PresupuestosDeleteByPK _
      (ByVal IdPresupuestoPk As Int32 _
      )

        db.ExecuteNonQuery("pre_PresupuestosDeleteByPK", _
         IdPresupuestoPk _
         )
    End Sub

    Public Sub pre_PresupuestosDeleteByPK _
      (ByVal IdPresupuestoPk As Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pre_PresupuestosDeleteByPK", _
         IdPresupuestoPk _
         )
    End Sub

    Public Sub pre_PresupuestosInsert _
    (ByVal entidad As pre_Presupuestos)

        db.ExecuteNonQuery("pre_PresupuestosInsert", _
         fd.ObtenerUltimoId("pre_Presupuestos", "IdPresupuestoPk") + 1 _
         , entidad.IdSucursalFk _
         , entidad.IdDepartamentoFk _
         , entidad.IdCentroCostosFk _
         , entidad.Anio _
         , entidad.Comentario _
         , entidad.FechaCreacion _
         )
    End Sub

    Public Sub pre_PresupuestosInsert _
    (ByVal entidad As pre_Presupuestos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pre_PresupuestosInsert", _
         fd.ObtenerUltimoId("pre_Presupuestos", "IdPresupuestoPk") + 1 _
         , entidad.IdSucursalFk _
         , entidad.IdDepartamentoFk _
         , entidad.IdCentroCostosFk _
         , entidad.Anio _
         , entidad.Comentario _
         , entidad.FechaCreacion _
         )
    End Sub

    Public Sub pre_PresupuestosUpdate _
    (ByVal entidad As pre_Presupuestos)

        db.ExecuteNonQuery("pre_PresupuestosUpdate", _
         entidad.IdPresupuestoPk _
         , entidad.IdSucursalFk _
         , entidad.IdDepartamentoFk _
         , entidad.IdCentroCostosFk _
         , entidad.Anio _
         , entidad.Comentario _
         , entidad.FechaCreacion _
         )
    End Sub

    Public Sub pre_PresupuestosUpdate _
    (ByVal entidad As pre_Presupuestos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pre_PresupuestosUpdate", _
         entidad.IdPresupuestoPk _
         , entidad.IdSucursalFk _
         , entidad.IdDepartamentoFk _
         , entidad.IdCentroCostosFk _
         , entidad.Anio _
         , entidad.Comentario _
         , entidad.FechaCreacion _
         )
    End Sub

#End Region

#Region "ban_CuentasBancarias"
    Public Function ban_CuentasBancariasSelectAll() As DataTable
        Return db.ExecuteDataSet("ban_CuentasBancariasSelectAll").Tables(0)
    End Function

    Public Function ban_CuentasBancariasSelectByPK _
      (ByVal IdCuentaBancaria As System.Int32
      ) As ban_CuentasBancarias

        Dim dt As DataTable
        dt = db.ExecuteDataSet("ban_CuentasBancariasSelectByPK",
         IdCuentaBancaria
         ).Tables(0)

        Dim Entidad As New ban_CuentasBancarias
        If dt.Rows.Count > 0 Then
            Entidad.IdCuentaBancaria = dt.Rows(0).Item("IdCuentaBancaria")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.NumeroCuenta = dt.Rows(0).Item("NumeroCuenta")
            Entidad.IdCuentaContable = dt.Rows(0).Item("IdCuentaContable")
            Entidad.NombreDigitador = dt.Rows(0).Item("NombreDigitador")
            Entidad.CargoDigitador = dt.Rows(0).Item("CargoDigitador")
            Entidad.NombreRevisa = dt.Rows(0).Item("NombreRevisa")
            Entidad.CargoRevisa = dt.Rows(0).Item("CargoRevisa")
            Entidad.NombreAutoriza = dt.Rows(0).Item("NombreAutoriza")
            Entidad.CargoAutoriza = dt.Rows(0).Item("CargoAutoriza")
            Entidad.UltNumeroCheque = dt.Rows(0).Item("UltNumeroCheque")
            Entidad.IdTipoPartida = dt.Rows(0).Item("IdTipoPartida")
            Entidad.IdFormatoVaucher = dt.Rows(0).Item("IdFormatoVaucher")
            Entidad.EstadoCuenta = dt.Rows(0).Item("EstadoCuenta")
            Entidad.TipoImpresion = dt.Rows(0).Item("TipoImpresion")
            Entidad.CuentaInactiva = dt.Rows(0).Item("CuentaInactiva")
            Entidad.CodigoEmpresa = dt.Rows(0).Item("CodigoEmpresa")
            Entidad.IdBanco = dt.Rows(0).Item("IdBanco")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.IdCuentaContablePos = dt.Rows(0).Item("IdCuentaContablePos")
        End If
        Return Entidad
    End Function

    Public Sub ban_CuentasBancariasDeleteByPK _
      (ByVal IdCuentaBancaria As System.Int32
      )

        db.ExecuteNonQuery("ban_CuentasBancariasDeleteByPK",
         IdCuentaBancaria
         )
    End Sub

    Public Sub ban_CuentasBancariasDeleteByPK _
      (ByVal IdCuentaBancaria As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_CuentasBancariasDeleteByPK",
         IdCuentaBancaria
         )
    End Sub

    Public Sub ban_CuentasBancariasInsert _
    (ByVal entidad As ban_CuentasBancarias)

        db.ExecuteNonQuery("ban_CuentasBancariasInsert",
         entidad.IdCuentaBancaria _
         , entidad.Nombre _
         , entidad.NumeroCuenta _
         , entidad.IdCuentaContable _
         , entidad.NombreDigitador _
         , entidad.CargoDigitador _
         , entidad.NombreRevisa _
         , entidad.CargoRevisa _
         , entidad.NombreAutoriza _
         , entidad.CargoAutoriza _
         , entidad.UltNumeroCheque _
         , entidad.IdTipoPartida _
         , entidad.IdFormatoVaucher _
         , entidad.EstadoCuenta _
         , entidad.TipoImpresion _
         , entidad.CuentaInactiva _
         , entidad.CodigoEmpresa _
         , entidad.IdBanco _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.IdCuentaContablePos
         )
    End Sub

    Public Sub ban_CuentasBancariasInsert _
    (ByVal entidad As ban_CuentasBancarias, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_CuentasBancariasInsert",
         entidad.IdCuentaBancaria _
         , entidad.Nombre _
         , entidad.NumeroCuenta _
         , entidad.IdCuentaContable _
         , entidad.NombreDigitador _
         , entidad.CargoDigitador _
         , entidad.NombreRevisa _
         , entidad.CargoRevisa _
         , entidad.NombreAutoriza _
         , entidad.CargoAutoriza _
         , entidad.UltNumeroCheque _
         , entidad.IdTipoPartida _
         , entidad.IdFormatoVaucher _
         , entidad.EstadoCuenta _
         , entidad.TipoImpresion _
         , entidad.CuentaInactiva _
         , entidad.CodigoEmpresa _
         , entidad.IdBanco _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
          , entidad.IdCuentaContablePos
         )
    End Sub

    Public Sub ban_CuentasBancariasUpdate _
    (ByVal entidad As ban_CuentasBancarias)

        db.ExecuteNonQuery("ban_CuentasBancariasUpdate",
         entidad.IdCuentaBancaria _
         , entidad.Nombre _
         , entidad.NumeroCuenta _
         , entidad.IdCuentaContable _
         , entidad.NombreDigitador _
         , entidad.CargoDigitador _
         , entidad.NombreRevisa _
         , entidad.CargoRevisa _
         , entidad.NombreAutoriza _
         , entidad.CargoAutoriza _
         , entidad.UltNumeroCheque _
         , entidad.IdTipoPartida _
         , entidad.IdFormatoVaucher _
         , entidad.EstadoCuenta _
         , entidad.TipoImpresion _
         , entidad.CuentaInactiva _
         , entidad.CodigoEmpresa _
         , entidad.IdBanco _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
          , entidad.IdCuentaContablePos
         )
    End Sub

    Public Sub ban_CuentasBancariasUpdate _
    (ByVal entidad As ban_CuentasBancarias, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_CuentasBancariasUpdate",
         entidad.IdCuentaBancaria _
         , entidad.Nombre _
         , entidad.NumeroCuenta _
         , entidad.IdCuentaContable _
         , entidad.NombreDigitador _
         , entidad.CargoDigitador _
         , entidad.NombreRevisa _
         , entidad.CargoRevisa _
         , entidad.NombreAutoriza _
         , entidad.CargoAutoriza _
         , entidad.UltNumeroCheque _
         , entidad.IdTipoPartida _
         , entidad.IdFormatoVaucher _
         , entidad.EstadoCuenta _
         , entidad.TipoImpresion _
         , entidad.CuentaInactiva _
         , entidad.CodigoEmpresa _
         , entidad.IdBanco _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
          , entidad.IdCuentaContablePos
         )
    End Sub

#End Region

#Region "ban_Cheques"
    Public Function ban_ChequesSelectAll() As DataTable
        Return db.ExecuteDataSet("ban_ChequesSelectAll").Tables(0)
    End Function

    Public Function ban_ChequesSelectByPK(ByVal IdCheque As System.Int32) As ban_Cheques

        Dim dt As DataTable = db.ExecuteDataSet("ban_ChequesSelectByPK", IdCheque).Tables(0)

        Dim Entidad As New ban_Cheques
        If dt.Rows.Count > 0 Then
            Entidad.IdCheque = dt.Rows(0).Item("IdCheque")
            Entidad.IdCuentaBancaria = dt.Rows(0).Item("IdCuentaBancaria")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.Numero = dt.Rows(0).Item("Numero")
            Entidad.Sufijo = dt.Rows(0).Item("Sufijo")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.Valor = dt.Rows(0).Item("Valor")
            Entidad.IdProveedor = dt.Rows(0).Item("IdProveedor")
            Entidad.AnombreDe = dt.Rows(0).Item("AnombreDe")
            Entidad.Concepto = dt.Rows(0).Item("Concepto")
            Entidad.IdPartida = dt.Rows(0).Item("IdPartida")
            Entidad.IdTipoPartida = dt.Rows(0).Item("IdTipoPartida")
            Entidad.NumeroPartida = dt.Rows(0).Item("NumeroPartida")
            Entidad.Impreso = dt.Rows(0).Item("Impreso")
            Entidad.Conciliado = dt.Rows(0).Item("Conciliado")
            Entidad.PagadoBanco = dt.Rows(0).Item("PagadoBanco")
            Entidad.Anulado = dt.Rows(0).Item("Anulado")
            Entidad.FechaAnulacion = fd.SiEsNulo(dt.Rows(0).Item("FechaAnulacion"), Nothing)
            Entidad.MotivoAnulacion = dt.Rows(0).Item("MotivoAnulacion")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
        End If
        Return Entidad
    End Function

    Public Sub ban_ChequesDeleteByPK(ByVal IdCheque As System.Int32)

        db.ExecuteNonQuery("ban_ChequesDeleteByPK", IdCheque)
    End Sub

    Public Sub ban_ChequesDeleteByPK _
      (ByVal IdCheque As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_ChequesDeleteByPK", _
         IdCheque _
         )
    End Sub

    Public Sub ban_ChequesInsert(ByVal entidad As ban_Cheques)

        db.ExecuteNonQuery("ban_ChequesInsert", _
         entidad.IdCheque _
         , entidad.IdCuentaBancaria _
         , entidad.IdSucursal _
         , entidad.Numero _
         , entidad.Sufijo _
         , entidad.Fecha _
         , entidad.Valor _
         , entidad.IdProveedor _
         , entidad.AnombreDe _
         , entidad.Concepto _
         , entidad.IdPartida _
         , entidad.IdTipoPartida _
         , entidad.NumeroPartida _
         , entidad.Impreso _
         , entidad.Conciliado _
         , entidad.PagadoBanco _
         , entidad.Anulado _
         , entidad.FechaAnulacion _
         , entidad.MotivoAnulacion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub ban_ChequesInsert(ByVal entidad As ban_Cheques, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_ChequesInsert", _
         entidad.IdCheque _
         , entidad.IdCuentaBancaria _
         , entidad.IdSucursal _
         , entidad.Numero _
         , entidad.Sufijo _
         , entidad.Fecha _
         , entidad.Valor _
         , entidad.IdProveedor _
         , entidad.AnombreDe _
         , entidad.Concepto _
         , entidad.IdPartida _
         , entidad.IdTipoPartida _
         , entidad.NumeroPartida _
         , entidad.Impreso _
         , entidad.Conciliado _
         , entidad.PagadoBanco _
         , entidad.Anulado _
         , entidad.FechaAnulacion _
         , entidad.MotivoAnulacion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub ban_ChequesUpdate(ByVal entidad As ban_Cheques)

        db.ExecuteNonQuery("ban_ChequesUpdate", _
         entidad.IdCheque _
         , entidad.IdCuentaBancaria _
         , entidad.IdSucursal _
         , entidad.Numero _
         , entidad.Sufijo _
         , entidad.Fecha _
         , entidad.Valor _
         , entidad.IdProveedor _
         , entidad.AnombreDe _
         , entidad.Concepto _
         , entidad.IdPartida _
         , entidad.IdTipoPartida _
         , entidad.NumeroPartida _
         , entidad.Impreso _
         , entidad.Conciliado _
         , entidad.PagadoBanco _
         , entidad.Anulado _
         , entidad.FechaAnulacion _
         , entidad.MotivoAnulacion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub ban_ChequesUpdate(ByVal entidad As ban_Cheques, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_ChequesUpdate", _
         entidad.IdCheque _
         , entidad.IdCuentaBancaria _
         , entidad.IdSucursal _
         , entidad.Numero _
         , entidad.Sufijo _
         , entidad.Fecha _
         , entidad.Valor _
         , entidad.IdProveedor _
         , entidad.AnombreDe _
         , entidad.Concepto _
         , entidad.IdPartida _
         , entidad.IdTipoPartida _
         , entidad.NumeroPartida _
         , entidad.Impreso _
         , entidad.Conciliado _
         , entidad.PagadoBanco _
         , entidad.Anulado _
         , entidad.FechaAnulacion _
         , entidad.MotivoAnulacion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "ban_ChequesDetalle"
    Public Function ban_ChequesDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("ban_ChequesDetalleSelectAll").Tables(0)
    End Function

    Public Function ban_ChequesDetalleSelectByPK _
      (ByVal IdCheque As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As ban_ChequesDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("ban_ChequesDetalleSelectByPK", _
         IdCheque _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New ban_ChequesDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdCheque = dt.rows(0).item("IdCheque")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdCuenta = dt.rows(0).item("IdCuenta")
            entidad.Referencia = dt.rows(0).item("Referencia")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.Debe = dt.rows(0).item("Debe")
            entidad.Haber = dt.rows(0).item("Haber")
            entidad.IdCentro = dt.rows(0).item("IdCentro")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
        End If
        Return Entidad
    End Function

    Public Sub ban_ChequesDetalleDeleteByPK _
      (ByVal IdCheque As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("ban_ChequesDetalleDeleteByPK", _
         IdCheque _
         , IdDetalle _
         )
    End Sub

    Public Sub ban_ChequesDetalleDeleteByPK _
      (ByVal IdCheque As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_ChequesDetalleDeleteByPK", _
         IdCheque _
         , IdDetalle _
         )
    End Sub

    Public Sub ban_ChequesDetalleInsert _
    (ByVal entidad As ban_ChequesDetalle)

        db.ExecuteNonQuery("ban_ChequesDetalleInsert", _
         entidad.IdCheque _
         , entidad.IdDetalle _
         , entidad.IdCuenta _
         , entidad.Referencia _
         , entidad.Concepto _
         , entidad.Debe _
         , entidad.Haber _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub ban_ChequesDetalleInsert _
    (ByVal entidad As ban_ChequesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_ChequesDetalleInsert", _
         entidad.IdCheque _
         , entidad.IdDetalle _
         , entidad.IdCuenta _
         , entidad.Referencia _
         , entidad.Concepto _
         , entidad.Debe _
         , entidad.Haber _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub ban_ChequesDetalleUpdate _
    (ByVal entidad As ban_ChequesDetalle)

        db.ExecuteNonQuery("ban_ChequesDetalleUpdate", _
         entidad.IdCheque _
         , entidad.IdDetalle _
         , entidad.IdCuenta _
         , entidad.Referencia _
         , entidad.Concepto _
         , entidad.Debe _
         , entidad.Haber _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub ban_ChequesDetalleUpdate _
    (ByVal entidad As ban_ChequesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_ChequesDetalleUpdate", _
         entidad.IdCheque _
         , entidad.IdDetalle _
         , entidad.IdCuenta _
         , entidad.Referencia _
         , entidad.Concepto _
         , entidad.Debe _
         , entidad.Haber _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "ban_TiposTransaccion"
    Public Function ban_TiposTransaccionSelectAll() As DataTable
        Return db.ExecuteDataSet("ban_TiposTransaccionSelectAll").Tables(0)
    End Function

    Public Function ban_TiposTransaccionSelectByPK _
      (ByVal IdTipo As System.Byte _
      ) As ban_TiposTransaccion

        Dim dt As datatable
        dt = db.ExecuteDataSet("ban_TiposTransaccionSelectByPK", _
         IdTipo _
         ).tables(0)

        Dim Entidad As New ban_TiposTransaccion
        If dt.Rows.Count > 0 Then
            entidad.IdTipo = dt.rows(0).item("IdTipo")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.IdTipoPartida = dt.rows(0).item("IdTipoPartida")
            entidad.TipoTransaccion = dt.rows(0).item("TipoTransaccion")

        End If
        Return Entidad
    End Function

    Public Sub ban_TiposTransaccionDeleteByPK _
      (ByVal IdTipo As System.Byte _
      )

        db.ExecuteNonQuery("ban_TiposTransaccionDeleteByPK", _
         IdTipo _
         )
    End Sub

    Public Sub ban_TiposTransaccionDeleteByPK _
      (ByVal IdTipo As System.Byte _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_TiposTransaccionDeleteByPK", _
         IdTipo _
         )
    End Sub

    Public Sub ban_TiposTransaccionInsert _
    (ByVal entidad As ban_TiposTransaccion)

        db.ExecuteNonQuery("ban_TiposTransaccionInsert", _
         entidad.IdTipo _
         , entidad.Nombre _
         , entidad.IdTipoPartida _
         , entidad.TipoTransaccion _
         )
    End Sub

    Public Sub ban_TiposTransaccionInsert _
    (ByVal entidad As ban_TiposTransaccion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_TiposTransaccionInsert", _
         entidad.IdTipo _
         , entidad.Nombre _
         , entidad.IdTipoPartida _
         , entidad.TipoTransaccion _
         )
    End Sub

    Public Sub ban_TiposTransaccionUpdate _
    (ByVal entidad As ban_TiposTransaccion)

        db.ExecuteNonQuery("ban_TiposTransaccionUpdate", _
         entidad.IdTipo _
         , entidad.Nombre _
         , entidad.IdTipoPartida _
         , entidad.TipoTransaccion _
         )
    End Sub

    Public Sub ban_TiposTransaccionUpdate _
    (ByVal entidad As ban_TiposTransaccion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_TiposTransaccionUpdate", _
         entidad.IdTipo _
         , entidad.Nombre _
         , entidad.IdTipoPartida _
         , entidad.TipoTransaccion _
         )
    End Sub

#End Region
#Region "ban_Transacciones"
    Public Function ban_TransaccionesSelectAll() As DataTable
        Return db.ExecuteDataSet("ban_TransaccionesSelectAll").Tables(0)
    End Function

    Public Function ban_TransaccionesSelectByPK(ByVal IdTransaccion As Int32) As ban_Transacciones

        Dim dt As DataTable = db.ExecuteDataSet("ban_TransaccionesSelectByPK", IdTransaccion).Tables(0)
        Dim Entidad As New ban_Transacciones
        If dt.Rows.Count > 0 Then
            Entidad.IdTransaccion = dt.Rows(0).Item("IdTransaccion")
            Entidad.IdCuentaBancaria = dt.Rows(0).Item("IdCuentaBancaria")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.FechaContable = fd.SiEsNulo(dt.Rows(0).Item("FechaContable"), Nothing)
            Entidad.Concepto = dt.Rows(0).Item("Concepto")
            Entidad.Valor = dt.Rows(0).Item("Valor")
            Entidad.IdTipo = dt.Rows(0).Item("IdTipo")
            Entidad.IncluirConciliacion = dt.Rows(0).Item("IncluirConciliacion")
            Entidad.Contabilizar = dt.Rows(0).Item("Contabilizar")
            Entidad.ProcesadaBanco = dt.Rows(0).Item("ProcesadaBanco")
            Entidad.IdTipoPartida = dt.Rows(0).Item("IdTipoPartida")
            Entidad.IdPartida = dt.Rows(0).Item("IdPartida")
            Entidad.NumeroPartida = dt.Rows(0).Item("NumeroPartida")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
        End If
        Return Entidad
    End Function

    Public Sub ban_TransaccionesDeleteByPK _
      (ByVal IdTransaccion As System.Int32 _
      )

        db.ExecuteNonQuery("ban_TransaccionesDeleteByPK", _
         IdTransaccion _
         )
    End Sub

    Public Sub ban_TransaccionesDeleteByPK _
      (ByVal IdTransaccion As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_TransaccionesDeleteByPK", _
         IdTransaccion _
         )
    End Sub

    Public Sub ban_TransaccionesInsert _
    (ByVal entidad As ban_Transacciones)

        db.ExecuteNonQuery("ban_TransaccionesInsert", _
         entidad.IdTransaccion _
         , entidad.IdCuentaBancaria _
         , entidad.IdSucursal _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.Concepto _
         , entidad.Valor _
         , entidad.IdTipo _
         , entidad.IncluirConciliacion _
         , entidad.Contabilizar _
         , entidad.ProcesadaBanco _
         , entidad.IdTipoPartida _
         , entidad.IdPartida _
         , entidad.NumeroPartida _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub ban_TransaccionesInsert _
    (ByVal entidad As ban_Transacciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_TransaccionesInsert", _
         entidad.IdTransaccion _
         , entidad.IdCuentaBancaria _
          , entidad.IdSucursal _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.Concepto _
         , entidad.Valor _
         , entidad.IdTipo _
         , entidad.IncluirConciliacion _
         , entidad.Contabilizar _
         , entidad.ProcesadaBanco _
         , entidad.IdTipoPartida _
         , entidad.IdPartida _
         , entidad.NumeroPartida _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub ban_TransaccionesUpdate _
    (ByVal entidad As ban_Transacciones)

        db.ExecuteNonQuery("ban_TransaccionesUpdate", _
         entidad.IdTransaccion _
         , entidad.IdCuentaBancaria _
          , entidad.IdSucursal _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.Concepto _
         , entidad.Valor _
         , entidad.IdTipo _
         , entidad.IncluirConciliacion _
         , entidad.Contabilizar _
         , entidad.ProcesadaBanco _
         , entidad.IdTipoPartida _
         , entidad.IdPartida _
         , entidad.NumeroPartida _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub ban_TransaccionesUpdate _
    (ByVal entidad As ban_Transacciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_TransaccionesUpdate", _
         entidad.IdTransaccion _
         , entidad.IdCuentaBancaria _
          , entidad.IdSucursal _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.Concepto _
         , entidad.Valor _
         , entidad.IdTipo _
         , entidad.IncluirConciliacion _
         , entidad.Contabilizar _
         , entidad.ProcesadaBanco _
         , entidad.IdTipoPartida _
         , entidad.IdPartida _
         , entidad.NumeroPartida _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "ban_TransaccionesDetalle"
    Public Function ban_TransaccionesDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("ban_TransaccionesDetalleSelectAll").Tables(0)
    End Function

    Public Function ban_TransaccionesDetalleSelectByPK _
      (ByVal IdTransaccion As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As ban_TransaccionesDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("ban_TransaccionesDetalleSelectByPK", _
         IdTransaccion _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New ban_TransaccionesDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdTransaccion = dt.rows(0).item("IdTransaccion")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdCuenta = dt.rows(0).item("IdCuenta")
            entidad.Referencia = dt.rows(0).item("Referencia")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.Debe = dt.rows(0).item("Debe")
            entidad.Haber = dt.rows(0).item("Haber")
            entidad.IdCentro = dt.rows(0).item("IdCentro")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub ban_TransaccionesDetalleDeleteByPK _
      (ByVal IdTransaccion As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("ban_TransaccionesDetalleDeleteByPK", _
         IdTransaccion _
         , IdDetalle _
         )
    End Sub

    Public Sub ban_TransaccionesDetalleDeleteByPK _
      (ByVal IdTransaccion As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_TransaccionesDetalleDeleteByPK", _
         IdTransaccion _
         , IdDetalle _
         )
    End Sub

    Public Sub ban_TransaccionesDetalleInsert _
    (ByVal entidad As ban_TransaccionesDetalle)

        db.ExecuteNonQuery("ban_TransaccionesDetalleInsert", _
         entidad.IdTransaccion _
         , entidad.IdDetalle _
         , entidad.IdCuenta _
         , entidad.Referencia _
         , entidad.Concepto _
         , entidad.Debe _
         , entidad.Haber _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub ban_TransaccionesDetalleInsert _
    (ByVal entidad As ban_TransaccionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_TransaccionesDetalleInsert", _
         entidad.IdTransaccion _
         , entidad.IdDetalle _
         , entidad.IdCuenta _
         , entidad.Referencia _
         , entidad.Concepto _
         , entidad.Debe _
         , entidad.Haber _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub ban_TransaccionesDetalleUpdate _
    (ByVal entidad As ban_TransaccionesDetalle)

        db.ExecuteNonQuery("ban_TransaccionesDetalleUpdate", _
         entidad.IdTransaccion _
         , entidad.IdDetalle _
         , entidad.IdCuenta _
         , entidad.Referencia _
         , entidad.Concepto _
         , entidad.Debe _
         , entidad.Haber _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub ban_TransaccionesDetalleUpdate _
    (ByVal entidad As ban_TransaccionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "ban_TransaccionesDetalleUpdate", _
         entidad.IdTransaccion _
         , entidad.IdDetalle _
         , entidad.IdCuenta _
         , entidad.Referencia _
         , entidad.Concepto _
         , entidad.Debe _
         , entidad.Haber _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub
#End Region

#Region "fac_Clientes"
    Public Function fac_ClientesSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_ClientesSelectAll").Tables(0)
    End Function

    Public Function fac_ClientesSelectByPK(ByVal IdCliente As System.String) As fac_Clientes

        Dim dt As DataTable = db.ExecuteDataSet("fac_ClientesSelectByPK", IdCliente).Tables(0)

        Dim Entidad As New fac_Clientes
        If dt.Rows.Count > 0 Then
            Entidad.IdCliente = dt.Rows(0).Item("IdCliente")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.RazonSocial = dt.Rows(0).Item("RazonSocial")
            Entidad.Nrc = dt.Rows(0).Item("Nrc")
            Entidad.Nit = dt.Rows(0).Item("Nit")
            Entidad.OtroDocumento = fd.SiEsNulo(dt.Rows(0).Item("OtroDocumento"), Nothing)
            Entidad.Giro = dt.Rows(0).Item("Giro")
            Entidad.IdDepartamento = dt.Rows(0).Item("IdDepartamento")
            Entidad.IdMunicipio = dt.Rows(0).Item("IdMunicipio")
            Entidad.Direccion = dt.Rows(0).Item("Direccion")
            Entidad.Telefonos = dt.Rows(0).Item("Telefonos")
            Entidad.Fax = fd.SiEsNulo(dt.Rows(0).Item("Fax"), Nothing)
            Entidad.CorreoElectronico = dt.Rows(0).Item("CorreoElectronico")
            Entidad.IdCuentaContable = dt.Rows(0).Item("IdCuentaContable")
            Entidad.DiasCredito = dt.Rows(0).Item("DiasCredito")
            Entidad.IdVendedor = dt.Rows(0).Item("IdVendedor")
            Entidad.IdFormaPago = dt.Rows(0).Item("IdFormaPago")
            Entidad.AplicaRetencion = dt.Rows(0).Item("AplicaRetencion")
            Entidad.LimiteCredito = dt.Rows(0).Item("LimiteCredito")
            Entidad.SaldoActual = dt.Rows(0).Item("SaldoActual")
            Entidad.FechaUltPago = dt.Rows(0).Item("FechaUltPago")
            Entidad.IdPrecio = dt.Rows(0).Item("IdPrecio")
            Entidad.Contacto1 = dt.Rows(0).Item("Contacto1")
            Entidad.InfoContacto1 = dt.Rows(0).Item("InfoContacto1")
            Entidad.Contacto2 = dt.Rows(0).Item("Contacto2")
            Entidad.InfoContacto2 = dt.Rows(0).Item("InfoContacto2")
            Entidad.IdTipoComprobante = dt.Rows(0).Item("IdTipoComprobante")
            Entidad.IdTipoImpuesto = dt.Rows(0).Item("IdTipoImpuesto")
            Entidad.PorcDescuento = dt.Rows(0).Item("PorcDescuento")
            Entidad.IdRuta = dt.Rows(0).Item("IdRuta")
            Entidad.BloquearFacturacion = dt.Rows(0).Item("BloquearFacturacion")
            Entidad.TipoImpuestoAdicional = fd.SiEsNulo(dt.Rows(0).Item("TipoImpuestoAdicional"), Nothing)
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
        End If
        Return Entidad
    End Function

    Public Sub fac_ClientesDeleteByPK _
      (ByVal IdCliente As System.String _
      )

        db.ExecuteNonQuery("fac_ClientesDeleteByPK", _
         IdCliente _
         )
    End Sub

    Public Sub fac_ClientesDeleteByPK _
      (ByVal IdCliente As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ClientesDeleteByPK", _
         IdCliente _
         )
    End Sub

    Public Sub fac_ClientesInsert _
    (ByVal entidad As fac_Clientes)

        db.ExecuteNonQuery("fac_ClientesInsert", _
         entidad.IdCliente _
         , entidad.Nombre _
         , entidad.RazonSocial _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.OtroDocumento _
         , entidad.Giro _
         , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.Direccion _
         , entidad.Telefonos _
         , entidad.Fax _
         , entidad.CorreoElectronico _
         , entidad.IdCuentaContable _
         , entidad.DiasCredito _
         , entidad.IdVendedor _
         , entidad.IdFormaPago _
         , entidad.AplicaRetencion _
         , entidad.LimiteCredito _
         , entidad.SaldoActual _
         , entidad.FechaUltPago _
         , entidad.IdPrecio _
         , entidad.Contacto1 _
         , entidad.InfoContacto1 _
         , entidad.Contacto2 _
         , entidad.InfoContacto2 _
         , entidad.IdTipoComprobante _
         , entidad.IdTipoImpuesto _
         , entidad.PorcDescuento _
         , entidad.IdRuta _
         , entidad.BloquearFacturacion _
          , entidad.TipoImpuestoAdicional _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_ClientesInsert _
    (ByVal entidad As fac_Clientes, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ClientesInsert", _
         entidad.IdCliente _
         , entidad.Nombre _
         , entidad.RazonSocial _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.OtroDocumento _
         , entidad.Giro _
         , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.Direccion _
         , entidad.Telefonos _
         , entidad.Fax _
         , entidad.CorreoElectronico _
         , entidad.IdCuentaContable _
         , entidad.DiasCredito _
         , entidad.IdVendedor _
         , entidad.IdFormaPago _
         , entidad.AplicaRetencion _
         , entidad.LimiteCredito _
         , entidad.SaldoActual _
         , entidad.FechaUltPago _
         , entidad.IdPrecio _
         , entidad.Contacto1 _
         , entidad.InfoContacto1 _
         , entidad.Contacto2 _
         , entidad.InfoContacto2 _
         , entidad.IdTipoComprobante _
         , entidad.IdTipoImpuesto _
         , entidad.PorcDescuento _
         , entidad.IdRuta _
         , entidad.BloquearFacturacion _
          , entidad.TipoImpuestoAdicional _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_ClientesUpdate _
    (ByVal entidad As fac_Clientes)

        db.ExecuteNonQuery("fac_ClientesUpdate", _
         entidad.IdCliente _
         , entidad.Nombre _
         , entidad.RazonSocial _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.OtroDocumento _
         , entidad.Giro _
         , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.Direccion _
         , entidad.Telefonos _
         , entidad.Fax _
         , entidad.CorreoElectronico _
         , entidad.IdCuentaContable _
         , entidad.DiasCredito _
         , entidad.IdVendedor _
         , entidad.IdFormaPago _
         , entidad.AplicaRetencion _
         , entidad.LimiteCredito _
         , entidad.SaldoActual _
         , entidad.FechaUltPago _
         , entidad.IdPrecio _
         , entidad.Contacto1 _
         , entidad.InfoContacto1 _
         , entidad.Contacto2 _
         , entidad.InfoContacto2 _
         , entidad.IdTipoComprobante _
         , entidad.IdTipoImpuesto _
         , entidad.PorcDescuento _
         , entidad.IdRuta _
         , entidad.BloquearFacturacion _
         , entidad.TipoImpuestoAdicional _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_ClientesUpdate _
    (ByVal entidad As fac_Clientes, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ClientesUpdate", _
         entidad.IdCliente _
         , entidad.Nombre _
         , entidad.RazonSocial _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.OtroDocumento _
         , entidad.Giro _
         , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.Direccion _
         , entidad.Telefonos _
         , entidad.Fax _
         , entidad.CorreoElectronico _
         , entidad.IdCuentaContable _
         , entidad.DiasCredito _
         , entidad.IdVendedor _
         , entidad.IdFormaPago _
         , entidad.AplicaRetencion _
         , entidad.LimiteCredito _
         , entidad.SaldoActual _
         , entidad.FechaUltPago _
         , entidad.IdPrecio _
         , entidad.Contacto1 _
         , entidad.InfoContacto1 _
         , entidad.Contacto2 _
         , entidad.InfoContacto2 _
         , entidad.IdTipoComprobante _
         , entidad.IdTipoImpuesto _
         , entidad.PorcDescuento _
         , entidad.IdRuta _
         , entidad.BloquearFacturacion _
         , entidad.TipoImpuestoAdicional _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "fac_RubrosActividades"
    Public Function fac_RubrosActividadesSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_RubrosActividadesSelectAll").Tables(0)
    End Function

    Public Function fac_RubrosActividadesSelectByPK _
            (ByVal IdRubro As System.Int16 _
            ) As fac_RubrosActividades

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_RubrosActividadesSelectByPK", _
            IdRubro _
            ).tables(0)

        Dim Entidad As New fac_RubrosActividades
        If dt.Rows.Count > 0 Then
            entidad.IdRubro = dt.rows(0).item("IdRubro")
            entidad.Nombre = dt.rows(0).item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub fac_RubrosActividadesDeleteByPK _
            (ByVal IdRubro As System.Int16 _
            )

        db.ExecuteNonQuery("fac_RubrosActividadesDeleteByPK", _
            IdRubro _
            )
    End Sub

    Public Sub fac_RubrosActividadesDeleteByPK _
            (ByVal IdRubro As System.Int16 _
            , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_RubrosActividadesDeleteByPK", _
            IdRubro _
            )
    End Sub

    Public Sub fac_RubrosActividadesInsert _
    (ByVal entidad As fac_RubrosActividades)

        db.ExecuteNonQuery("fac_RubrosActividadesInsert", _
            entidad.IdRubro _
            , entidad.Nombre _
            )
    End Sub

    Public Sub fac_RubrosActividadesInsert _
    (ByVal entidad As fac_RubrosActividades, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_RubrosActividadesInsert", _
            entidad.IdRubro _
            , entidad.Nombre _
            )
    End Sub

    Public Sub fac_RubrosActividadesUpdate _
    (ByVal entidad As fac_RubrosActividades)

        db.ExecuteNonQuery("fac_RubrosActividadesUpdate", _
            entidad.IdRubro _
            , entidad.Nombre _
            )
    End Sub

    Public Sub fac_RubrosActividadesUpdate _
    (ByVal entidad As fac_RubrosActividades, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_RubrosActividadesUpdate", _
            entidad.IdRubro _
            , entidad.Nombre _
            )
    End Sub

#End Region

#Region "fac_ClientesPrecios"
    Public Function fac_ClientesPreciosSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_ClientesPreciosSelectAll").Tables(0)
    End Function

    Public Function fac_ClientesPreciosSelectByPK _
      (ByVal IdCliente As System.String _
      , ByVal IdProducto As System.String _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_ClientesPrecios

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_ClientesPreciosSelectByPK", _
         IdCliente _
         , IdProducto _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New fac_ClientesPrecios
        If dt.Rows.Count > 0 Then
            entidad.IdCliente = dt.rows(0).item("IdCliente")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            Entidad.Descripcion = dt.Rows(0).Item("Descripcion")
            entidad.PrecioUnitario = dt.rows(0).item("PrecioUnitario")

        End If
        Return Entidad
    End Function

    Public Sub fac_ClientesPreciosDeleteByPK _
      (ByVal IdCliente As System.String _
      , ByVal IdProducto As System.String _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("fac_ClientesPreciosDeleteByPK", _
         IdCliente _
         , IdProducto _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_ClientesPreciosDeleteByPK _
      (ByVal IdCliente As System.String _
      , ByVal IdProducto As System.String _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ClientesPreciosDeleteByPK", _
         IdCliente _
         , IdProducto _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_ClientesPreciosInsert _
    (ByVal entidad As fac_ClientesPrecios)

        db.ExecuteNonQuery("fac_ClientesPreciosInsert", _
         entidad.IdCliente _
         , entidad.IdProducto _
         , entidad.IdDetalle _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         )
    End Sub

    Public Sub fac_ClientesPreciosInsert(ByVal entidad As fac_ClientesPrecios, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ClientesPreciosInsert", _
         entidad.IdCliente _
         , entidad.IdProducto _
         , entidad.IdDetalle _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         )
    End Sub

    Public Sub fac_ClientesPreciosUpdate _
    (ByVal entidad As fac_ClientesPrecios)

        db.ExecuteNonQuery("fac_ClientesPreciosUpdate", _
         entidad.IdCliente _
         , entidad.IdProducto _
         , entidad.IdDetalle _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         )
    End Sub

    Public Sub fac_ClientesPreciosUpdate _
    (ByVal entidad As fac_ClientesPrecios, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ClientesPreciosUpdate", _
         entidad.IdCliente _
         , entidad.IdProducto _
         , entidad.IdDetalle _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         )
    End Sub

#End Region

#Region "fac_Rutas"
    Public Function fac_RutasSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_RutasSelectAll").Tables(0)
    End Function

    Public Function fac_RutasSelectByPK _
      (ByVal IdRuta As System.Int32 _
      ) As fac_Rutas

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_RutasSelectByPK", _
         IdRuta _
         ).tables(0)

        Dim Entidad As New fac_Rutas
        If dt.Rows.Count > 0 Then
            entidad.IdRuta = dt.rows(0).item("IdRuta")
            entidad.Nombre = dt.rows(0).item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub fac_RutasDeleteByPK _
      (ByVal IdRuta As System.Int32 _
      )

        db.ExecuteNonQuery("fac_RutasDeleteByPK", _
         IdRuta _
         )
    End Sub

    Public Sub fac_RutasDeleteByPK _
      (ByVal IdRuta As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_RutasDeleteByPK", _
         IdRuta _
         )
    End Sub

    Public Sub fac_RutasInsert _
    (ByVal entidad As fac_Rutas)

        db.ExecuteNonQuery("fac_RutasInsert", _
         entidad.IdRuta _
         , entidad.Nombre _
         )
    End Sub

    Public Sub fac_RutasInsert _
    (ByVal entidad As fac_Rutas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_RutasInsert", _
         entidad.IdRuta _
         , entidad.Nombre _
         )
    End Sub

    Public Sub fac_RutasUpdate _
    (ByVal entidad As fac_Rutas)

        db.ExecuteNonQuery("fac_RutasUpdate", _
         entidad.IdRuta _
         , entidad.Nombre _
         )
    End Sub

    Public Sub fac_RutasUpdate _
    (ByVal entidad As fac_Rutas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_RutasUpdate", _
         entidad.IdRuta _
         , entidad.Nombre _
         )
    End Sub

#End Region

#Region "fac_NotasRemision"
    Public Function fac_NotasRemisionSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_NotasRemisionSelectAll").Tables(0)
    End Function

    Public Function fac_NotasRemisionSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As fac_NotasRemision

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_NotasRemisionSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New fac_NotasRemision
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdCliente = dt.rows(0).item("IdCliente")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Nit = dt.rows(0).item("Nit")
            entidad.Nrc = dt.rows(0).item("Nrc")
            entidad.Giro = dt.rows(0).item("Giro")
            entidad.Direccion = dt.rows(0).item("Direccion")
            entidad.IdDepartamento = dt.rows(0).item("IdDepartamento")
            entidad.IdMunicipio = dt.rows(0).item("IdMunicipio")
            entidad.Telefonos = dt.rows(0).item("Telefonos")
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.IdVendedor = dt.rows(0).item("IdVendedor")
            entidad.IdBodega = dt.rows(0).item("IdBodega")
            entidad.IdPunto = dt.rows(0).item("IdPunto")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub fac_NotasRemisionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("fac_NotasRemisionDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_NotasRemisionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_NotasRemisionDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_NotasRemisionInsert _
    (ByVal entidad As fac_NotasRemision)

        db.ExecuteNonQuery("fac_NotasRemisionInsert", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nit _
         , entidad.Nrc _
         , entidad.Giro _
         , entidad.Direccion _
         , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.Telefonos _
         , entidad.IdSucursal _
         , entidad.IdVendedor _
         , entidad.IdBodega _
         , entidad.IdPunto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_NotasRemisionInsert _
    (ByVal entidad As fac_NotasRemision, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_NotasRemisionInsert", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nit _
         , entidad.Nrc _
         , entidad.Giro _
         , entidad.Direccion _
         , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.Telefonos _
         , entidad.IdSucursal _
         , entidad.IdVendedor _
         , entidad.IdBodega _
         , entidad.IdPunto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_NotasRemisionUpdate _
    (ByVal entidad As fac_NotasRemision)

        db.ExecuteNonQuery("fac_NotasRemisionUpdate", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nit _
         , entidad.Nrc _
         , entidad.Giro _
         , entidad.Direccion _
         , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.Telefonos _
         , entidad.IdSucursal _
         , entidad.IdVendedor _
         , entidad.IdBodega _
         , entidad.IdPunto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_NotasRemisionUpdate _
    (ByVal entidad As fac_NotasRemision, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_NotasRemisionUpdate", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nit _
         , entidad.Nrc _
         , entidad.Giro _
         , entidad.Direccion _
         , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.Telefonos _
         , entidad.IdSucursal _
         , entidad.IdVendedor _
         , entidad.IdBodega _
         , entidad.IdPunto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "fac_NotasRemisionDetalle"
    Public Function fac_NotasRemisionDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_NotasRemisionDetalleSelectAll").Tables(0)
    End Function

    Public Function fac_NotasRemisionDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_NotasRemisionDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_NotasRemisionDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        If dt.Rows.Count = 0 Then
            Return Nothing
        Else
            Dim Entidad As New fac_NotasRemisionDetalle
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.PrecioUnitario = dt.rows(0).item("PrecioUnitario")
            entidad.PjeDescuento = dt.rows(0).item("PjeDescuento")
            entidad.PrecioTotal = dt.rows(0).item("PrecioTotal")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

            Return Entidad
        End If
    End Function

    Public Sub fac_NotasRemisionDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("fac_NotasRemisionDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_NotasRemisionDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_NotasRemisionDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_NotasRemisionDetalleInsert _
    (ByVal entidad As fac_NotasRemisionDetalle)

        db.ExecuteNonQuery("fac_NotasRemisionDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.IdPrecio _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PjeDescuento _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub fac_NotasRemisionDetalleInsert _
    (ByVal entidad As fac_NotasRemisionDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_NotasRemisionDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.IdPrecio _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PjeDescuento _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub fac_NotasRemisionDetalleUpdate _
    (ByVal entidad As fac_NotasRemisionDetalle)

        db.ExecuteNonQuery("fac_NotasRemisionDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.IdPrecio _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PjeDescuento _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub fac_NotasRemisionDetalleUpdate _
    (ByVal entidad As fac_NotasRemisionDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_NotasRemisionDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.IdPrecio _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PjeDescuento _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "fac_Cotizaciones"
    Public Function fac_CotizacionesSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_CotizacionesSelectAll").Tables(0)
    End Function

    Public Function fac_CotizacionesSelectByPK(ByVal IdComprobante As Int32) As fac_Cotizaciones

        Dim dt As DataTable = db.ExecuteDataSet("fac_CotizacionesSelectByPK", IdComprobante).Tables(0)

        Dim Entidad As New fac_Cotizaciones
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.Numero = dt.Rows(0).Item("Numero")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.IdCliente = dt.Rows(0).Item("IdCliente")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Nrc = dt.Rows(0).Item("Nrc")
            Entidad.Direccion = dt.Rows(0).Item("Direccion")
            Entidad.Nit = dt.Rows(0).Item("Nit")
            Entidad.AtencionA = dt.Rows(0).Item("AtencionA")
            Entidad.IdVendedor = dt.Rows(0).Item("IdVendedor")
            Entidad.IdFormaPago = dt.Rows(0).Item("IdFormaPago")
            Entidad.DiasCredito = dt.Rows(0).Item("DiasCredito")
            Entidad.TiempoEntrega = dt.Rows(0).Item("TiempoEntrega")
            Entidad.Proyecto = dt.Rows(0).Item("Proyecto")
            Entidad.Garantia = dt.Rows(0).Item("Garantia")
            Entidad.Concepto = dt.Rows(0).Item("Concepto")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.ImprimirPrecios = dt.Rows(0).Item("ImprimirPrecios")
            Entidad.TipoVenta = dt.Rows(0).Item("TipoVenta")
            Entidad.IdTipoComprobante = dt.Rows(0).Item("IdTipoComprobante")
            Entidad.AplicaRetencionPercepcion = dt.Rows(0).Item("AplicaRetencionPercepcion")
            Entidad.TotalAfecto = dt.Rows(0).Item("TotalAfecto")
            Entidad.TotalIva = dt.Rows(0).Item("TotalIva")
            Entidad.TotalImpuesto1 = dt.Rows(0).Item("TotalImpuesto1")
            Entidad.TotalComprobante = dt.Rows(0).Item("TotalComprobante")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub fac_CotizacionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("fac_CotizacionesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_CotizacionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CotizacionesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_CotizacionesInsert _
    (ByVal entidad As fac_Cotizaciones)

        db.ExecuteNonQuery("fac_CotizacionesInsert", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nrc _
         , entidad.Direccion _
         , entidad.Nit _
         , entidad.AtencionA _
         , entidad.IdVendedor _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.TiempoEntrega _
         , entidad.Garantia _
          , entidad.Concepto _
         , entidad.IdSucursal _
         , entidad.ImprimirPrecios _
         , entidad.TipoVenta _
         , entidad.IdTipoComprobante _
         , entidad.AplicaRetencionPercepcion _
         , entidad.TotalAfecto _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalComprobante _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_CotizacionesInsert(ByVal entidad As fac_Cotizaciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CotizacionesInsert", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nrc _
         , entidad.Direccion _
         , entidad.Nit _
         , entidad.AtencionA _
         , entidad.IdVendedor _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.TiempoEntrega _
         , entidad.Proyecto _
         , entidad.Garantia _
         , entidad.Concepto _
         , entidad.IdSucursal _
         , entidad.ImprimirPrecios _
         , entidad.TipoVenta _
         , entidad.IdTipoComprobante _
          , entidad.AplicaRetencionPercepcion _
                   , entidad.TotalAfecto _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalComprobante _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_CotizacionesUpdate _
    (ByVal entidad As fac_Cotizaciones)

        db.ExecuteNonQuery("fac_CotizacionesUpdate", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nrc _
         , entidad.Direccion _
         , entidad.Nit _
         , entidad.AtencionA _
         , entidad.IdVendedor _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.TiempoEntrega _
         , entidad.Proyecto _
         , entidad.Garantia _
         , entidad.Concepto _
         , entidad.IdSucursal _
         , entidad.ImprimirPrecios _
         , entidad.TipoVenta _
         , entidad.IdTipoComprobante _
         , entidad.AplicaRetencionPercepcion _
         , entidad.TotalAfecto _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalComprobante _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_CotizacionesUpdate(ByVal entidad As fac_Cotizaciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CotizacionesUpdate", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nrc _
         , entidad.Direccion _
         , entidad.Nit _
         , entidad.AtencionA _
         , entidad.IdVendedor _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.TiempoEntrega _
         , entidad.Proyecto _
         , entidad.Garantia _
         , entidad.Concepto _
         , entidad.IdSucursal _
         , entidad.ImprimirPrecios _
         , entidad.TipoVenta _
         , entidad.IdTipoComprobante _
         , entidad.AplicaRetencionPercepcion _
         , entidad.TotalAfecto _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalComprobante _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region

#Region "fac_CotizacionesDetalle"
    Public Function fac_CotizacionesDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_CotizacionesDetalleSelectAll").Tables(0)
    End Function

    Public Function fac_CotizacionesDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_CotizacionesDetalle

        Dim dt As DataTable
        dt = db.ExecuteDataSet("fac_CotizacionesDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).Tables(0)

        Dim Entidad As New fac_CotizacionesDetalle
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.IdProducto = dt.Rows(0).Item("IdProducto")
            Entidad.CodigoPublico = dt.Rows(0).Item("CodigoPublico")
            Entidad.IdPrecio = dt.Rows(0).Item("IdPrecio")
            Entidad.Cantidad = dt.Rows(0).Item("Cantidad")
            Entidad.Descripcion = dt.Rows(0).Item("Descripcion")
            Entidad.Especificaciones = dt.Rows(0).Item("Especificaciones")
            Entidad.ArchivoImagen = dt.Rows(0).Item("ArchivoImagen")
            Entidad.PrecioVenta = dt.Rows(0).Item("PrecioVenta")
            Entidad.PorcDescuento = dt.Rows(0).Item("PorcDescuento")
            Entidad.PrecioUnitario = dt.Rows(0).Item("PrecioUnitario")
            Entidad.PrecioTotal = dt.Rows(0).Item("PrecioTotal")
            Entidad.VentaNeta = dt.Rows(0).Item("VentaNeta")
            Entidad.ValorIva = dt.Rows(0).Item("ValorIva")
            Entidad.DescuentoAutorizadoPor = dt.Rows(0).Item("DescuentoAutorizadoPor")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub fac_CotizacionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("fac_CotizacionesDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_CotizacionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CotizacionesDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_CotizacionesDetalleInsert _
    (ByVal entidad As fac_CotizacionesDetalle)

        db.ExecuteNonQuery("fac_CotizacionesDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.CodigoPublico _
         , entidad.IdPrecio _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.Especificaciones _
         , entidad.ArchivoImagen _
         , entidad.PrecioVenta _
         , entidad.PorcDescuento _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         , entidad.VentaNeta _
         , entidad.ValorIva _
         , entidad.DescuentoAutorizadoPor _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub fac_CotizacionesDetalleInsert _
    (ByVal entidad As fac_CotizacionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CotizacionesDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.CodigoPublico _
         , entidad.IdPrecio _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.Especificaciones _
         , entidad.ArchivoImagen _
         , entidad.PrecioVenta _
         , entidad.PorcDescuento _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         , entidad.VentaNeta _
         , entidad.ValorIva _
         , entidad.DescuentoAutorizadoPor _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub fac_CotizacionesDetalleUpdate _
    (ByVal entidad As fac_CotizacionesDetalle)

        db.ExecuteNonQuery("fac_CotizacionesDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.CodigoPublico _
         , entidad.IdPrecio _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.Especificaciones _
         , entidad.ArchivoImagen _
         , entidad.PrecioVenta _
         , entidad.PorcDescuento _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         , entidad.VentaNeta _
         , entidad.ValorIva _
         , entidad.DescuentoAutorizadoPor _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub fac_CotizacionesDetalleUpdate _
    (ByVal entidad As fac_CotizacionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CotizacionesDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.CodigoPublico _
         , entidad.IdPrecio _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.Especificaciones _
         , entidad.ArchivoImagen _
         , entidad.PrecioVenta _
         , entidad.PorcDescuento _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         , entidad.VentaNeta _
         , entidad.ValorIva _
         , entidad.DescuentoAutorizadoPor _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "fac_Pedidos"
    Public Function fac_PedidosSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_PedidosSelectAll").Tables(0)
    End Function

    Public Function fac_PedidosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As fac_Pedidos

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_PedidosSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New fac_Pedidos
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdCliente = dt.rows(0).item("IdCliente")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Nrc = dt.rows(0).item("Nrc")
            entidad.Nit = dt.rows(0).item("Nit")
            entidad.Giro = dt.rows(0).item("Giro")
            entidad.Direccion = dt.rows(0).item("Direccion")
            entidad.Telefono = dt.rows(0).item("Telefono")
            entidad.IdMunicipio = dt.rows(0).item("IdMunicipio")
            entidad.IdDepartamento = dt.rows(0).item("IdDepartamento")
            entidad.VentaAcuentaDe = dt.rows(0).item("VentaAcuentaDe")
            entidad.IdTipoComprobante = dt.rows(0).item("IdTipoComprobante")
            entidad.TipoVenta = dt.rows(0).item("TipoVenta")
            entidad.TipoImpuesto = dt.rows(0).item("TipoImpuesto")
            entidad.IdFormaPago = dt.rows(0).item("IdFormaPago")
            entidad.DiasCredito = dt.rows(0).item("DiasCredito")
            entidad.IdVendedor = dt.rows(0).item("IdVendedor")
            entidad.IdBodega = dt.rows(0).item("IdBodega")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.Devolucion = dt.Rows(0).Item("Devolucion")
            entidad.Facturado = dt.rows(0).item("Facturado")
            Entidad.Notas = dt.Rows(0).Item("Notas")
            Entidad.Anticipo = dt.Rows(0).Item("Anticipo")
            Entidad.TotalAfecto = dt.Rows(0).Item("TotalAfecto")
            Entidad.TotalIva = dt.Rows(0).Item("TotalIva")
            Entidad.TotalImpuesto1 = dt.Rows(0).Item("TotalImpuesto1")
            Entidad.TotalComprobante = dt.Rows(0).Item("TotalComprobante")
            Entidad.PorcDescuento = dt.Rows(0).Item("PorcDescuento")
            Entidad.IdComprobanteFactura = dt.Rows(0).Item("IdComprobanteFactura")
            Entidad.AplicaRetencionPercepcion = dt.Rows(0).Item("AplicaRetencionPercepcion")
            Entidad.Reservado = dt.Rows(0).Item("Reservado")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
        End If
        Return Entidad
    End Function

    Public Sub fac_PedidosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("fac_PedidosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_PedidosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_PedidosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_PedidosInsert _
    (ByVal entidad As fac_Pedidos)

        db.ExecuteNonQuery("fac_PedidosInsert", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.Giro _
         , entidad.Direccion _
         , entidad.Telefono _
        , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.VentaAcuentaDe _
         , entidad.IdTipoComprobante _
         , entidad.TipoVenta _
         , entidad.TipoImpuesto _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.IdVendedor _
         , entidad.IdBodega _
         , entidad.IdSucursal _
          , entidad.Devolucion _
         , entidad.Facturado _
         , entidad.Notas _
         , entidad.Anticipo _
         , entidad.TotalAfecto _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalComprobante _
         , entidad.PorcDescuento _
         , entidad.IdComprobanteFactura _
         , entidad.AplicaRetencionPercepcion _
         , entidad.Reservado _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_PedidosInsert _
    (ByVal entidad As fac_Pedidos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_PedidosInsert", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.Giro _
         , entidad.Direccion _
         , entidad.Telefono _
        , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.VentaAcuentaDe _
         , entidad.IdTipoComprobante _
         , entidad.TipoVenta _
         , entidad.TipoImpuesto _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.IdVendedor _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.Devolucion _
         , entidad.Facturado _
         , entidad.Notas _
         , entidad.Anticipo _
                  , entidad.TotalAfecto _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalComprobante _
         , entidad.PorcDescuento _
         , entidad.IdComprobanteFactura _
         , entidad.AplicaRetencionPercepcion _
         , entidad.Reservado _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_PedidosUpdate _
    (ByVal entidad As fac_Pedidos)

        db.ExecuteNonQuery("fac_PedidosUpdate", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.Giro _
         , entidad.Direccion _
         , entidad.Telefono _
        , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.VentaAcuentaDe _
         , entidad.IdTipoComprobante _
         , entidad.TipoVenta _
         , entidad.TipoImpuesto _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.IdVendedor _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.Devolucion _
         , entidad.Facturado _
         , entidad.Notas _
         , entidad.Anticipo _
                  , entidad.TotalAfecto _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalComprobante _
         , entidad.PorcDescuento _
         , entidad.IdComprobanteFactura _
         , entidad.AplicaRetencionPercepcion _
         , entidad.Reservado _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_PedidosUpdate _
    (ByVal entidad As fac_Pedidos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_PedidosUpdate", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.Giro _
         , entidad.Direccion _
         , entidad.Telefono _
        , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.VentaAcuentaDe _
         , entidad.IdTipoComprobante _
         , entidad.TipoVenta _
         , entidad.TipoImpuesto _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.IdVendedor _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.Devolucion _
         , entidad.Facturado _
         , entidad.Notas _
         , entidad.Anticipo _
                  , entidad.TotalAfecto _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalComprobante _
         , entidad.PorcDescuento _
         , entidad.IdComprobanteFactura _
         , entidad.AplicaRetencionPercepcion _
         , entidad.Reservado _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "fac_PedidosDetalle"
    Public Function fac_PedidosDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_PedidosDetalleSelectAll").Tables(0)
    End Function

    Public Sub fac_PedidosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("fac_PedidosDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_PedidosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_PedidosDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_PedidosDetalleInsert(ByVal entidad As fac_PedidosDetalle)

        db.ExecuteNonQuery("fac_PedidosDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.IdPrecio _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioVenta _
         , entidad.PorcDescuento _
         , entidad.ValorDescuento _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         , entidad.VentaNeta _
         , entidad.ValorIva _
         , entidad.PrecioCosto _
         , entidad.TipoProducto _
         , entidad.EsEspecial _
         , entidad.EsCompuesto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub fac_PedidosDetalleInsert(ByVal entidad As fac_PedidosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_PedidosDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.IdPrecio _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioVenta _
         , entidad.PorcDescuento _
         , entidad.ValorDescuento _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         , entidad.VentaNeta _
         , entidad.ValorIva _
         , entidad.PrecioCosto _
         , entidad.TipoProducto _
         , entidad.EsEspecial _
         , entidad.EsCompuesto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub fac_PedidosDetalleUpdate _
    (ByVal entidad As fac_PedidosDetalle)

        db.ExecuteNonQuery("fac_PedidosDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.IdPrecio _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioVenta _
         , entidad.PorcDescuento _
         , entidad.ValorDescuento _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
                  , entidad.VentaNeta _
         , entidad.ValorIva _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub fac_PedidosDetalleUpdate _
    (ByVal entidad As fac_PedidosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_PedidosDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.IdPrecio _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioVenta _
         , entidad.PorcDescuento _
         , entidad.ValorDescuento _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
                  , entidad.VentaNeta _
         , entidad.ValorIva _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "fac_Cajas"
    Public Function fac_CajasSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_CajasSelectAll").Tables(0)
    End Function

    Public Function fac_CajasSelectByPK(ByVal NumeroCaja As System.String) As fac_Cajas

        Dim dt As DataTable
        dt = db.ExecuteDataSet("fac_CajasSelectByPK", _
         NumeroCaja).Tables(0)

        Dim Entidad As New fac_Cajas
        If dt.Rows.Count > 0 Then
            Entidad.NumeroCaja = dt.Rows(0).Item("NumeroCaja")
            Entidad.NombreCaja = dt.Rows(0).Item("NombreCaja")
            Entidad.Responsable = dt.Rows(0).Item("Responsable")
            Entidad.UltimoCorte = dt.Rows(0).Item("UltimoCorte")
            Entidad.FechaUltimoCorte = dt.Rows(0).Item("FechaUltimoCorte")
        End If
        Return Entidad
    End Function

    Public Sub fac_CajasDeleteByPK(ByVal NumeroCaja As System.String)
        db.ExecuteNonQuery("fac_CajasDeleteByPK", NumeroCaja)
    End Sub

    Public Sub fac_CajasDeleteByPK(ByVal NumeroCaja As System.String, ByVal Transaccion As DbTransaction)
        db.ExecuteNonQuery(Transaccion, "fac_CajasDeleteByPK", NumeroCaja)
    End Sub

    Public Sub fac_CajasInsert _
    (ByVal entidad As fac_Cajas)

        db.ExecuteNonQuery("fac_CajasInsert", _
         entidad.NumeroCaja _
         , entidad.NombreCaja _
         , entidad.Responsable _
         , entidad.UltimoCorte _
        , entidad.FechaUltimoCorte)
    End Sub

    Public Sub fac_CajasInsert(ByVal entidad As fac_Cajas, ByVal Transaccion As DbTransaction)
        db.ExecuteNonQuery(Transaccion, "fac_CajasInsert", _
        entidad.NumeroCaja, entidad.NombreCaja, entidad.UltimoCorte, entidad.FechaUltimoCorte, entidad.Responsable)
    End Sub

    Public Sub fac_CajasUpdate(ByVal entidad As fac_Cajas)

        db.ExecuteNonQuery("fac_CajasUpdate", _
        entidad.NumeroCaja, entidad.NombreCaja, entidad.Responsable, entidad.UltimoCorte, entidad.FechaUltimoCorte)
    End Sub

    Public Sub fac_CajasUpdate(ByVal entidad As fac_Cajas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CajasUpdate", _
         entidad.NumeroCaja _
         , entidad.NombreCaja _
         , entidad.Responsable _
         , entidad.UltimoCorte _
         , entidad.FechaUltimoCorte _
         )
    End Sub

#End Region
#Region "fac_Ventas"
    Public Function fac_VentasSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_VentasSelectAll").Tables(0)
    End Function

    Public Function fac_VentasSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As fac_Ventas

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_VentasSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New fac_Ventas
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdTipoComprobante = dt.rows(0).item("IdTipoComprobante")
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.IdPunto = dt.rows(0).item("IdPunto")
            entidad.Serie = dt.rows(0).item("Serie")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.NumFormularioUnico = dt.rows(0).item("NumFormularioUnico")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdCliente = dt.rows(0).item("IdCliente")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Nrc = dt.rows(0).item("Nrc")
            entidad.Nit = dt.rows(0).item("Nit")
            entidad.Giro = dt.rows(0).item("Giro")
            entidad.Direccion = dt.rows(0).item("Direccion")
            entidad.Telefono = dt.rows(0).item("Telefono")
            entidad.VentaAcuentaDe = dt.rows(0).item("VentaAcuentaDe")
            entidad.IdDepartamento = dt.rows(0).item("IdDepartamento")
            entidad.IdMunicipio = dt.rows(0).item("IdMunicipio")
            entidad.IdFormaPago = dt.rows(0).item("IdFormaPago")
            entidad.DiasCredito = dt.rows(0).item("DiasCredito")
            entidad.OrdenCompra = dt.rows(0).item("OrdenCompra")
            entidad.IdVendedor = dt.rows(0).item("IdVendedor")
            entidad.TipoVenta = dt.rows(0).item("TipoVenta")
            entidad.TipoImpuesto = dt.rows(0).item("TipoImpuesto")
            entidad.PorcDescto = dt.rows(0).item("PorcDescto")
            entidad.TotalDescto = dt.rows(0).item("TotalDescto")
            entidad.TotalComprobante = dt.rows(0).item("TotalComprobante")
            entidad.TotalIva = dt.rows(0).item("TotalIva")
            entidad.TotalImpuesto1 = dt.rows(0).item("TotalImpuesto1")
            entidad.TotalImpuesto2 = dt.rows(0).item("TotalImpuesto2")
            entidad.TotalAfecto = dt.rows(0).item("TotalAfecto")
            entidad.TotalExento = dt.rows(0).item("TotalExento")
            entidad.TotalNoSujeto = dt.rows(0).item("TotalNoSujeto")
            entidad.TotalNeto = dt.rows(0).item("TotalNeto")
            entidad.TotalPagado = dt.rows(0).item("TotalPagado")
            entidad.SaldoActual = dt.rows(0).item("SaldoActual")
            Entidad.FechaCancelacion = fd.SiEsNulo(dt.Rows(0).Item("FechaCancelacion"), Nothing)
            entidad.Anulado = dt.rows(0).item("Anulado")
            entidad.MotivoAnulacion = dt.rows(0).item("MotivoAnulacion")
            entidad.AnuladoPor = dt.rows(0).item("AnuladoPor")
            Entidad.FechaAnulacion = fd.SiEsNulo(dt.Rows(0).Item("FechaAnulacion"), Nothing)
            entidad.IdComprobanteNota = dt.rows(0).item("IdComprobanteNota")
            entidad.IdBodega = dt.rows(0).item("IdBodega")
            entidad.EstadoFactura = dt.rows(0).item("EstadoFactura")
            Entidad.EsDevolucion = dt.Rows(0).Item("EsDevolucion")
            Entidad.AutorizoDescuento = dt.Rows(0).Item("AutorizoDescuento")
            Entidad.IdSucursalEnvio = dt.Rows(0).Item("IdSucursalEnvio")
            Entidad.Comentario = dt.Rows(0).Item("Comentario")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub fac_VentasDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("fac_VentasDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_VentasDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VentasDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_VentasInsert _
    (ByVal entidad As fac_Ventas)

        db.ExecuteNonQuery("fac_VentasInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Serie _
         , entidad.Numero _
         , entidad.NumFormularioUnico _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.Giro _
         , entidad.Direccion _
         , entidad.Telefono _
         , entidad.VentaAcuentaDe _
         , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.OrdenCompra _
         , entidad.IdVendedor _
         , entidad.TipoVenta _
         , entidad.TipoImpuesto _
         , entidad.PorcDescto _
         , entidad.TotalDescto _
         , entidad.TotalComprobante _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.TotalAfecto _
         , entidad.TotalExento _
         , entidad.TotalNoSujeto _
         , entidad.TotalNeto _
         , entidad.TotalPagado _
         , entidad.SaldoActual _
         , entidad.FechaCancelacion _
         , entidad.Anulado _
         , entidad.MotivoAnulacion _
         , entidad.AnuladoPor _
         , entidad.FechaAnulacion _
         , entidad.IdComprobanteNota _
         , entidad.IdBodega _
         , entidad.EstadoFactura _
         , entidad.EsDevolucion _
         , entidad.AutorizoDescuento _
         , entidad.IdSucursalEnvio _
         , entidad.Comentario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_VentasInsert _
    (ByVal entidad As fac_Ventas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VentasInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Serie _
         , entidad.Numero _
         , entidad.NumFormularioUnico _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.Giro _
         , entidad.Direccion _
         , entidad.Telefono _
         , entidad.VentaAcuentaDe _
         , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.OrdenCompra _
         , entidad.IdVendedor _
         , entidad.TipoVenta _
         , entidad.TipoImpuesto _
         , entidad.PorcDescto _
         , entidad.TotalDescto _
         , entidad.TotalComprobante _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.TotalAfecto _
         , entidad.TotalExento _
         , entidad.TotalNoSujeto _
         , entidad.TotalNeto _
         , entidad.TotalPagado _
         , entidad.SaldoActual _
         , entidad.FechaCancelacion _
         , entidad.Anulado _
         , entidad.MotivoAnulacion _
         , entidad.AnuladoPor _
         , entidad.FechaAnulacion _
         , entidad.IdComprobanteNota _
         , entidad.IdBodega _
         , entidad.EstadoFactura _
         , entidad.EsDevolucion _
         , entidad.AutorizoDescuento _
         , entidad.IdSucursalEnvio _
         , entidad.Comentario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_VentasUpdate _
    (ByVal entidad As fac_Ventas)

        db.ExecuteNonQuery("fac_VentasUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Serie _
         , entidad.Numero _
         , entidad.NumFormularioUnico _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.Giro _
         , entidad.Direccion _
         , entidad.Telefono _
         , entidad.VentaAcuentaDe _
         , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.OrdenCompra _
         , entidad.IdVendedor _
         , entidad.TipoVenta _
         , entidad.TipoImpuesto _
         , entidad.PorcDescto _
         , entidad.TotalDescto _
         , entidad.TotalComprobante _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.TotalAfecto _
         , entidad.TotalExento _
         , entidad.TotalNoSujeto _
         , entidad.TotalNeto _
         , entidad.TotalPagado _
         , entidad.SaldoActual _
         , entidad.FechaCancelacion _
         , entidad.Anulado _
         , entidad.MotivoAnulacion _
         , entidad.AnuladoPor _
         , entidad.FechaAnulacion _
         , entidad.IdComprobanteNota _
         , entidad.IdBodega _
         , entidad.EstadoFactura _
         , entidad.EsDevolucion _
         , entidad.AutorizoDescuento _
         , entidad.IdSucursalEnvio _
         , entidad.Comentario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_VentasUpdate _
    (ByVal entidad As fac_Ventas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VentasUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Serie _
         , entidad.Numero _
         , entidad.NumFormularioUnico _
         , entidad.Fecha _
         , entidad.IdCliente _
         , entidad.Nombre _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.Giro _
         , entidad.Direccion _
         , entidad.Telefono _
         , entidad.VentaAcuentaDe _
         , entidad.IdDepartamento _
         , entidad.IdMunicipio _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.OrdenCompra _
         , entidad.IdVendedor _
         , entidad.TipoVenta _
         , entidad.TipoImpuesto _
         , entidad.PorcDescto _
         , entidad.TotalDescto _
         , entidad.TotalComprobante _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.TotalAfecto _
         , entidad.TotalExento _
         , entidad.TotalNoSujeto _
         , entidad.TotalNeto _
         , entidad.TotalPagado _
         , entidad.SaldoActual _
         , entidad.FechaCancelacion _
         , entidad.Anulado _
         , entidad.MotivoAnulacion _
         , entidad.AnuladoPor _
         , entidad.FechaAnulacion _
         , entidad.IdComprobanteNota _
         , entidad.IdBodega _
         , entidad.EstadoFactura _
         , entidad.EsDevolucion _
         , entidad.AutorizoDescuento _
         , entidad.IdSucursalEnvio _
         , entidad.Comentario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "fac_Vales"
    Public Function fac_ValesSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_ValesSelectAll").Tables(0)
    End Function

    Public Function fac_ValesSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As fac_Vales

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_ValesSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New fac_Vales
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.IdPunto = dt.Rows(0).Item("IdPunto")
            entidad.NumeroComprobante = dt.rows(0).item("NumeroComprobante")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Valor = dt.rows(0).item("Valor")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub fac_ValesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("fac_ValesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_ValesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ValesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_ValesInsert _
    (ByVal entidad As fac_Vales)

        db.ExecuteNonQuery("fac_ValesInsert", _
         entidad.IdComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.Nombre _
         , entidad.Valor _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_ValesInsert _
    (ByVal entidad As fac_Vales, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ValesInsert", _
         entidad.IdComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.Nombre _
         , entidad.Valor _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_ValesUpdate _
    (ByVal entidad As fac_Vales)

        db.ExecuteNonQuery("fac_ValesUpdate", _
         entidad.IdComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.Nombre _
         , entidad.Valor _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_ValesUpdate _
    (ByVal entidad As fac_Vales, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ValesUpdate", _
         entidad.IdComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.Nombre _
         , entidad.Valor _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "fac_AperturaCaja"
    Public Function fac_AperturaCajaSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_AperturaCajaSelectAll").Tables(0)
    End Function

    Public Function fac_AperturaCajaSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As fac_AperturaCaja

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_AperturaCajaSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New fac_AperturaCaja
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.IdPunto = dt.rows(0).item("IdPunto")
            entidad.FondoApertura = dt.rows(0).item("FondoApertura")
            entidad.Responsable = dt.rows(0).item("Responsable")
            entidad.Comentario = dt.rows(0).item("Comentario")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub fac_AperturaCajaDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("fac_AperturaCajaDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_AperturaCajaDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_AperturaCajaDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_AperturaCajaInsert _
    (ByVal entidad As fac_AperturaCaja)

        db.ExecuteNonQuery("fac_AperturaCajaInsert", _
         entidad.IdComprobante _
         , entidad.Fecha _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.FondoApertura _
         , entidad.Responsable _
         , entidad.Comentario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_AperturaCajaInsert _
    (ByVal entidad As fac_AperturaCaja, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_AperturaCajaInsert", _
         entidad.IdComprobante _
         , entidad.Fecha _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.FondoApertura _
         , entidad.Responsable _
         , entidad.Comentario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_AperturaCajaUpdate _
    (ByVal entidad As fac_AperturaCaja)

        db.ExecuteNonQuery("fac_AperturaCajaUpdate", _
         entidad.IdComprobante _
         , entidad.Fecha _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.FondoApertura _
         , entidad.Responsable _
         , entidad.Comentario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub fac_AperturaCajaUpdate _
    (ByVal entidad As fac_AperturaCaja, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_AperturaCajaUpdate", _
         entidad.IdComprobante _
         , entidad.Fecha _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.FondoApertura _
         , entidad.Responsable _
         , entidad.Comentario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "fac_CorteCajaArqueo"
    Public Function fac_CorteCajaArqueoSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_CorteCajaArqueoSelectAll").Tables(0)
    End Function

    Public Function fac_CorteCajaArqueoSelectByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal Fecha As System.DateTime _
      , ByVal IdDenominacion As System.Int32 _
      ) As fac_CorteCajaArqueo

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_CorteCajaArqueoSelectByPK", _
         IdSucursal _
         , IdPunto _
         , Fecha _
         , IdDenominacion _
         ).tables(0)

        Dim Entidad As New fac_CorteCajaArqueo
        If dt.Rows.Count > 0 Then
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.IdPunto = dt.rows(0).item("IdPunto")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdDenominacion = dt.rows(0).item("IdDenominacion")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.Total = dt.rows(0).item("Total")

        End If
        Return Entidad
    End Function

    Public Sub fac_CorteCajaArqueoDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal Fecha As System.DateTime _
      , ByVal IdDenominacion As System.Int32 _
      )

        db.ExecuteNonQuery("fac_CorteCajaArqueoDeleteByPK", _
         IdSucursal _
         , IdPunto _
         , Fecha _
         , IdDenominacion _
         )
    End Sub

    Public Sub fac_CorteCajaArqueoDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal Fecha As System.DateTime _
      , ByVal IdDenominacion As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CorteCajaArqueoDeleteByPK", _
         IdSucursal _
         , IdPunto _
         , Fecha _
         , IdDenominacion _
         )
    End Sub

    Public Sub fac_CorteCajaArqueoInsert _
    (ByVal entidad As fac_CorteCajaArqueo)

        db.ExecuteNonQuery("fac_CorteCajaArqueoInsert", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.IdDenominacion _
         , entidad.Cantidad _
         , entidad.Total _
         )
    End Sub

    Public Sub fac_CorteCajaArqueoInsert _
    (ByVal entidad As fac_CorteCajaArqueo, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CorteCajaArqueoInsert", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.IdDenominacion _
         , entidad.Cantidad _
         , entidad.Total _
         )
    End Sub

    Public Sub fac_CorteCajaArqueoUpdate _
    (ByVal entidad As fac_CorteCajaArqueo)

        db.ExecuteNonQuery("fac_CorteCajaArqueoUpdate", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.IdDenominacion _
         , entidad.Cantidad _
         , entidad.Total _
         )
    End Sub

    Public Sub fac_CorteCajaArqueoUpdate _
    (ByVal entidad As fac_CorteCajaArqueo, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CorteCajaArqueoUpdate", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.IdDenominacion _
         , entidad.Cantidad _
         , entidad.Total _
         )
    End Sub

#End Region
#Region "fac_CorteCajaCheques"
    Public Function fac_CorteCajaChequesSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_CorteCajaChequesSelectAll").Tables(0)
    End Function

    Public Function fac_CorteCajaChequesSelectByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal Fecha As System.DateTime _
      , ByVal NumCheque As System.String _
      , ByVal IdBanco As System.Int32 _
      ) As fac_CorteCajaCheques

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_CorteCajaChequesSelectByPK", _
         IdSucursal _
         , IdPunto _
         , Fecha _
         , NumCheque _
         , IdBanco _
         ).tables(0)

        Dim Entidad As New fac_CorteCajaCheques
        If dt.Rows.Count > 0 Then
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.IdPunto = dt.rows(0).item("IdPunto")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.NumCheque = dt.rows(0).item("NumCheque")
            Entidad.IdBanco = dt.Rows(0).Item("IdBanco")
            Entidad.Cliente = dt.Rows(0).Item("Cliente")
            entidad.Valor = dt.rows(0).item("Valor")
            entidad.NumReserva = dt.rows(0).item("NumReserva")
            entidad.IdBancoRemesa = dt.rows(0).item("IdBancoRemesa")

        End If
        Return Entidad
    End Function

    Public Sub fac_CorteCajaChequesDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal Fecha As System.DateTime _
      , ByVal NumCheque As System.String _
      , ByVal IdBanco As System.Int32 _
      )

        db.ExecuteNonQuery("fac_CorteCajaChequesDeleteByPK", _
         IdSucursal _
         , IdPunto _
         , Fecha _
         , NumCheque _
         , IdBanco _
         )
    End Sub

    Public Sub fac_CorteCajaChequesDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal Fecha As System.DateTime _
      , ByVal NumCheque As System.String _
      , ByVal IdBanco As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CorteCajaChequesDeleteByPK", _
         IdSucursal _
         , IdPunto _
         , Fecha _
         , NumCheque _
         , IdBanco _
         )
    End Sub

    Public Sub fac_CorteCajaChequesInsert _
    (ByVal entidad As fac_CorteCajaCheques)

        db.ExecuteNonQuery("fac_CorteCajaChequesInsert", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.NumCheque _
         , entidad.IdBanco _
         , entidad.Cliente _
         , entidad.Valor _
         , entidad.NumReserva _
         , entidad.IdBancoRemesa _
         )
    End Sub

    Public Sub fac_CorteCajaChequesInsert _
    (ByVal entidad As fac_CorteCajaCheques, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CorteCajaChequesInsert", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.NumCheque _
         , entidad.IdBanco _
         , entidad.Cliente _
         , entidad.Valor _
         , entidad.NumReserva _
         , entidad.IdBancoRemesa _
         )
    End Sub

    Public Sub fac_CorteCajaChequesUpdate _
    (ByVal entidad As fac_CorteCajaCheques)

        db.ExecuteNonQuery("fac_CorteCajaChequesUpdate", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.NumCheque _
         , entidad.IdBanco _
         , entidad.Cliente _
         , entidad.Valor _
         , entidad.NumReserva _
         , entidad.IdBancoRemesa _
         )
    End Sub

    Public Sub fac_CorteCajaChequesUpdate _
    (ByVal entidad As fac_CorteCajaCheques, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CorteCajaChequesUpdate", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.NumCheque _
         , entidad.IdBanco _
         , entidad.Cliente _
         , entidad.Valor _
         , entidad.NumReserva _
         , entidad.IdBancoRemesa _
         )
    End Sub

#End Region

#Region "fac_CorteCajaRemesas"
    Public Sub fac_CorteCajaRemesasInsert _
(ByVal entidad As fac_CorteCajaRemesas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CorteCajaRemesasInsert",
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.Numero _
         , entidad.IdBanco _
         , entidad.Valor _
         , entidad.FechaRemesar
         )
    End Sub

    Public Sub fac_CorteCajaRemesasUpdate _
    (ByVal entidad As fac_CorteCajaRemesas)

        db.ExecuteNonQuery("fac_CorteCajaRemesasUpdate",
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.Numero _
         , entidad.IdBanco _
         , entidad.Valor _
          , entidad.FechaRemesar
         )
    End Sub
#End Region
#Region "fac_CorteCajaPOS"

    Public Sub fac_CorteCajaPOSInsert _
    (ByVal entidad As fac_CorteCajaPOS)

        db.ExecuteNonQuery("fac_CorteCajaPOSInsert", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.Numero _
         , entidad.IdBanco _
         , entidad.Valor _
         )
    End Sub

    Public Sub fac_CorteCajaPOSInsert _
    (ByVal entidad As fac_CorteCajaPOS, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CorteCajaPOSInsert", _
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.Numero _
         , entidad.IdBanco _
         , entidad.Valor _
         )
    End Sub


#End Region

#Region "fac_VentasDetallePago"
    Public Function fac_VentasDetallePagoSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_VentasDetallePagoSelectAll").Tables(0)
    End Function

    Public Function fac_VentasDetallePagoSelectByPK _
      (ByVal IdFormaPago As System.Int32 _
      , ByVal IdComprobVenta As System.Int32 _
      ) As fac_VentasDetallePago

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_VentasDetallePagoSelectByPK", _
         IdFormaPago _
         , IdComprobVenta _
         ).tables(0)

        Dim Entidad As New fac_VentasDetallePago
        If dt.Rows.Count > 0 Then
            entidad.IdFormaPago = dt.rows(0).item("IdFormaPago")
            entidad.IdComprobVenta = dt.rows(0).item("IdComprobVenta")
            entidad.Total = dt.rows(0).item("Total")

        End If
        Return Entidad
    End Function

    Public Sub fac_VentasDetallePagoDeleteByPK _
      (ByVal IdFormaPago As System.Int32 _
      , ByVal IdComprobVenta As System.Int32 _
)
        db.ExecuteNonQuery("fac_VentasDetallePagoDeleteByPK", IdComprobVenta)
    End Sub

    Public Sub fac_VentasDetallePagoDeleteByPK _
      (ByVal IdFormaPago As System.Int32 _
      , ByVal IdComprobVenta As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VentasDetallePagoDeleteByPK", _
         IdFormaPago _
         , IdComprobVenta _
         )
    End Sub

    Public Sub fac_VentasDetallePagoInsert _
    (ByVal entidad As fac_VentasDetallePago)

        db.ExecuteNonQuery("fac_VentasDetallePagoInsert", _
         entidad.IdFormaPago _
         , entidad.IdComprobVenta _
         , entidad.Total _
         )
    End Sub

    Public Sub fac_VentasDetallePagoInsert _
    (ByVal entidad As fac_VentasDetallePago, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VentasDetallePagoInsert", _
         entidad.IdFormaPago _
         , entidad.IdComprobVenta _
         , entidad.Total _
         )
    End Sub

    Public Sub fac_VentasDetallePagoUpdate _
    (ByVal entidad As fac_VentasDetallePago)

        db.ExecuteNonQuery("fac_VentasDetallePagoUpdate", _
         entidad.IdFormaPago _
         , entidad.IdComprobVenta _
         , entidad.Total _
         )
    End Sub

    Public Sub fac_VentasDetallePagoUpdate _
    (ByVal entidad As fac_VentasDetallePago, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VentasDetallePagoUpdate", _
         entidad.IdFormaPago _
         , entidad.IdComprobVenta _
         , entidad.Total _
         )
    End Sub

#End Region
#Region "fac_Actividades"
    Public Function fac_ActividadesSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_ActividadesSelectAll").Tables(0)
    End Function

    Public Function fac_ActividadesSelectByPK _
            (ByVal IdActividad As System.Int32 _
            ) As fac_Actividades

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_ActividadesSelectByPK", _
            IdActividad _
            ).tables(0)

        Dim Entidad As New fac_Actividades
        If dt.Rows.Count > 0 Then
            entidad.IdRubro = dt.rows(0).item("IdRubro")
            entidad.IdActividad = dt.rows(0).item("IdActividad")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.EmiteAlerta = dt.rows(0).item("EmiteAlerta")
            entidad.Peso = dt.rows(0).item("Peso")
            entidad.Apnfd = dt.rows(0).item("Apnfd")

        End If
        Return Entidad
    End Function

    Public Sub fac_ActividadesDeleteByPK _
            (ByVal IdActividad As System.Int32 _
            )

        db.ExecuteNonQuery("fac_ActividadesDeleteByPK", _
            IdActividad _
            )
    End Sub

    Public Sub fac_ActividadesDeleteByPK _
            (ByVal IdActividad As System.Int32 _
            , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ActividadesDeleteByPK", _
            IdActividad _
            )
    End Sub

    Public Sub fac_ActividadesInsert _
    (ByVal entidad As fac_Actividades)

        db.ExecuteNonQuery("fac_ActividadesInsert", _
            entidad.IdRubro _
            , entidad.IdActividad _
            , entidad.Nombre _
            , entidad.EmiteAlerta _
            , entidad.Peso _
            , entidad.Apnfd _
            )
    End Sub

    Public Sub fac_ActividadesInsert _
    (ByVal entidad As fac_Actividades, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ActividadesInsert", _
            entidad.IdRubro _
            , entidad.IdActividad _
            , entidad.Nombre _
            , entidad.EmiteAlerta _
            , entidad.Peso _
            , entidad.Apnfd _
            )
    End Sub

    Public Sub fac_ActividadesUpdate _
    (ByVal entidad As fac_Actividades)

        db.ExecuteNonQuery("fac_ActividadesUpdate", _
            entidad.IdRubro _
            , entidad.IdActividad _
            , entidad.Nombre _
            , entidad.EmiteAlerta _
            , entidad.Peso _
            , entidad.Apnfd _
            )
    End Sub

    Public Sub fac_ActividadesUpdate _
    (ByVal entidad As fac_Actividades, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ActividadesUpdate", _
            entidad.IdRubro _
            , entidad.IdActividad _
            , entidad.Nombre _
            , entidad.EmiteAlerta _
            , entidad.Peso _
            , entidad.Apnfd _
            )
    End Sub

#End Region
#Region "fac_ClientesAnexo"
    Public Function fac_ClientesAnexoSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_ClientesAnexoSelectAll").Tables(0)
    End Function

    Public Function fac_ClientesAnexoSelectByPK _
            (ByVal IdCliente As System.String _
            ) As fac_ClientesAnexo

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_ClientesAnexoSelectByPK", _
            IdCliente _
            ).tables(0)

        Dim Entidad As New fac_ClientesAnexo
        If dt.Rows.Count > 0 Then
            entidad.IdCliente = dt.rows(0).item("IdCliente")
            entidad.IdProfesion = dt.rows(0).item("IdProfesion")
            entidad.IdRubro = dt.rows(0).item("IdRubro")
            entidad.IdActividad = dt.rows(0).item("IdActividad")
            entidad.IngresosBruto = dt.rows(0).item("IngresosBruto")
            entidad.APNFD = dt.rows(0).item("APNFD")
            entidad.Comentario = dt.rows(0).item("Comentario")
            entidad.MontoEstimadoCompras = dt.rows(0).item("MontoEstimadoCompras")
            entidad.PersonaJuridica = dt.rows(0).item("PersonaJuridica")
            entidad.RepresentanteLegal = dt.rows(0).item("RepresentanteLegal")
            entidad.TipoDocumento = dt.rows(0).item("TipoDocumento")
            entidad.NumDocumentoRepLegal = dt.rows(0).item("NumDocumentoRepLegal")
            entidad.NITRepLegal = dt.rows(0).item("NITRepLegal")
            entidad.IdDepartamentoRepLegal = dt.rows(0).item("IdDepartamentoRepLegal")
            entidad.IdMunicipioRepLegal = dt.rows(0).item("IdMunicipioRepLegal")
            entidad.IdProfesionRepLegal = dt.rows(0).item("IdProfesionRepLegal")
            entidad.DireccionRepLegal = dt.rows(0).item("DireccionRepLegal")
            entidad.TelMovilRepLegal = dt.rows(0).item("TelMovilRepLegal")
            entidad.TelefonosRepLegal = dt.rows(0).item("TelefonosRepLegal")
            entidad.CorreoElectronicoRepLegal = dt.rows(0).item("CorreoElectronicoRepLegal")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub fac_ClientesAnexoDeleteByPK _
            (ByVal IdCliente As System.String _
            )

        db.ExecuteNonQuery("fac_ClientesAnexoDeleteByPK", _
            IdCliente _
            )
    End Sub

    Public Sub fac_ClientesAnexoDeleteByPK _
            (ByVal IdCliente As System.String _
            , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ClientesAnexoDeleteByPK", _
            IdCliente _
            )
    End Sub

    Public Sub fac_ClientesAnexoInsert _
    (ByVal entidad As fac_ClientesAnexo)

        db.ExecuteNonQuery("fac_ClientesAnexoInsert", _
            entidad.IdCliente _
            , entidad.IdProfesion _
            , entidad.IdRubro _
            , entidad.IdActividad _
            , entidad.IngresosBruto _
            , entidad.APNFD _
            , entidad.Comentario _
            , entidad.MontoEstimadoCompras _
            , entidad.PersonaJuridica _
            , entidad.RepresentanteLegal _
            , entidad.TipoDocumento _
            , entidad.NumDocumentoRepLegal _
            , entidad.NITRepLegal _
            , entidad.IdDepartamentoRepLegal _
            , entidad.IdMunicipioRepLegal _
            , entidad.IdProfesionRepLegal _
            , entidad.DireccionRepLegal _
            , entidad.TelMovilRepLegal _
            , entidad.TelefonosRepLegal _
            , entidad.CorreoElectronicoRepLegal _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            )
    End Sub

    Public Sub fac_ClientesAnexoInsert _
    (ByVal entidad As fac_ClientesAnexo, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ClientesAnexoInsert", _
            entidad.IdCliente _
            , entidad.IdProfesion _
            , entidad.IdRubro _
            , entidad.IdActividad _
            , entidad.IngresosBruto _
            , entidad.APNFD _
            , entidad.Comentario _
            , entidad.MontoEstimadoCompras _
            , entidad.PersonaJuridica _
            , entidad.RepresentanteLegal _
            , entidad.TipoDocumento _
            , entidad.NumDocumentoRepLegal _
            , entidad.NITRepLegal _
            , entidad.IdDepartamentoRepLegal _
            , entidad.IdMunicipioRepLegal _
            , entidad.IdProfesionRepLegal _
            , entidad.DireccionRepLegal _
            , entidad.TelMovilRepLegal _
            , entidad.TelefonosRepLegal _
            , entidad.CorreoElectronicoRepLegal _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            )
    End Sub

    Public Sub fac_ClientesAnexoUpdate _
    (ByVal entidad As fac_ClientesAnexo)

        db.ExecuteNonQuery("fac_ClientesAnexoUpdate", _
            entidad.IdCliente _
            , entidad.IdProfesion _
            , entidad.IdRubro _
            , entidad.IdActividad _
            , entidad.IngresosBruto _
            , entidad.APNFD _
            , entidad.Comentario _
            , entidad.MontoEstimadoCompras _
            , entidad.PersonaJuridica _
            , entidad.RepresentanteLegal _
            , entidad.TipoDocumento _
            , entidad.NumDocumentoRepLegal _
            , entidad.NITRepLegal _
            , entidad.IdDepartamentoRepLegal _
            , entidad.IdMunicipioRepLegal _
            , entidad.IdProfesionRepLegal _
            , entidad.DireccionRepLegal _
            , entidad.TelMovilRepLegal _
            , entidad.TelefonosRepLegal _
            , entidad.CorreoElectronicoRepLegal _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            )
    End Sub

    Public Sub fac_ClientesAnexoUpdate _
    (ByVal entidad As fac_ClientesAnexo, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ClientesAnexoUpdate", _
            entidad.IdCliente _
            , entidad.IdProfesion _
            , entidad.IdRubro _
            , entidad.IdActividad _
            , entidad.IngresosBruto _
            , entidad.APNFD _
            , entidad.Comentario _
            , entidad.MontoEstimadoCompras _
            , entidad.PersonaJuridica _
            , entidad.RepresentanteLegal _
            , entidad.TipoDocumento _
            , entidad.NumDocumentoRepLegal _
            , entidad.NITRepLegal _
            , entidad.IdDepartamentoRepLegal _
            , entidad.IdMunicipioRepLegal _
            , entidad.IdProfesionRepLegal _
            , entidad.DireccionRepLegal _
            , entidad.TelMovilRepLegal _
            , entidad.TelefonosRepLegal _
            , entidad.CorreoElectronicoRepLegal _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            )
    End Sub

#End Region
#Region "fac_ProspectacionClientes"
	Public Function fac_ProspectacionClientesSelectAll() As DataTable
		Return db.ExecuteDataSet("fac_ProspectacionClientesSelectAll").Tables(0)
	End Function

	Public Function fac_ProspectacionClientesSelectByPK _
		(ByVal IdComprobante As System.Int32
		) As fac_ProspectacionClientes

		Dim dt As DataTable
		dt = db.ExecuteDataSet("fac_ProspectacionClientesSelectByPK",
		IdComprobante
		).Tables(0)

		Dim Entidad As New fac_ProspectacionClientes
		If dt.Rows.Count > 0 Then
			Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
			Entidad.NombreCliente = dt.Rows(0).Item("NombreCliente")
			Entidad.RazonSocial = dt.Rows(0).Item("RazonSocial")
			Entidad.Direccion = dt.Rows(0).Item("Direccion")
			Entidad.IdDepartamento = dt.Rows(0).Item("IdDepartamento")
			Entidad.IdMunicipio = dt.Rows(0).Item("IdMunicipio")
			Entidad.Telefonos = dt.Rows(0).Item("Telefonos")
			Entidad.Nit = dt.Rows(0).Item("Nit")
			Entidad.Dui = dt.Rows(0).Item("Dui")
			Entidad.Nrc = dt.Rows(0).Item("Nrc")
			Entidad.Giro = dt.Rows(0).Item("Giro")
			Entidad.Email = dt.Rows(0).Item("Email")
			Entidad.ReferidoPor = dt.Rows(0).Item("ReferidoPor")
			Entidad.FechaContacto = dt.Rows(0).Item("FechaContacto")
			Entidad.Asunto = dt.Rows(0).Item("Asunto")
			Entidad.LugarContacto = dt.Rows(0).Item("LugarContacto")
			Entidad.IdVendedor = dt.Rows(0).Item("IdVendedor")
			Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
			Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
			Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
			Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

		End If
		Return Entidad
	End Function

	Public Sub fac_ProspectacionClientesDeleteByPK _
		(ByVal IdComprobante As System.Int32
		)

		db.ExecuteNonQuery("fac_ProspectacionClientesDeleteByPK",
		IdComprobante
		)
	End Sub

	Public Sub fac_ProspectacionClientesDeleteByPK _
		(ByVal IdComprobante As System.Int32 _
		, Transaccion As DbTransaction)

		db.ExecuteNonQuery(Transaccion, "fac_ProspectacionClientesDeleteByPK",
		IdComprobante
		)
	End Sub

	Public Sub fac_ProspectacionClientesInsert _
(ByVal entidad As fac_ProspectacionClientes)

		db.ExecuteNonQuery("fac_ProspectacionClientesInsert",
		entidad.IdComprobante _
		, entidad.NombreCliente _
		, entidad.RazonSocial _
		, entidad.Direccion _
		, entidad.IdDepartamento _
		, entidad.IdMunicipio _
		, entidad.Telefonos _
		, entidad.Nit _
		, entidad.Dui _
		, entidad.Nrc _
		, entidad.Giro _
		, entidad.Email _
		, entidad.ReferidoPor _
		, entidad.FechaContacto _
		, entidad.Asunto _
		, entidad.LugarContacto _
		, entidad.IdVendedor _
		, entidad.CreadoPor _
		, entidad.FechaHoraCreacion _
		, entidad.ModificadoPor _
		, entidad.FechaHoraModificacion
		)
	End Sub

	Public Sub fac_ProspectacionClientesInsert _
(ByVal entidad As fac_ProspectacionClientes, Transaccion As DbTransaction)

		db.ExecuteNonQuery(Transaccion, "fac_ProspectacionClientesInsert",
		entidad.IdComprobante _
		, entidad.NombreCliente _
		, entidad.RazonSocial _
		, entidad.Direccion _
		, entidad.IdDepartamento _
		, entidad.IdMunicipio _
		, entidad.Telefonos _
		, entidad.Nit _
		, entidad.Dui _
		, entidad.Nrc _
		, entidad.Giro _
		, entidad.Email _
		, entidad.ReferidoPor _
		, entidad.FechaContacto _
		, entidad.Asunto _
		, entidad.LugarContacto _
		, entidad.IdVendedor _
		, entidad.CreadoPor _
		, entidad.FechaHoraCreacion _
		, entidad.ModificadoPor _
		, entidad.FechaHoraModificacion
		)
	End Sub

	Public Sub fac_ProspectacionClientesUpdate _
(ByVal entidad As fac_ProspectacionClientes)

		db.ExecuteNonQuery("fac_ProspectacionClientesUpdate",
		entidad.IdComprobante _
		, entidad.NombreCliente _
		, entidad.RazonSocial _
		, entidad.Direccion _
		, entidad.IdDepartamento _
		, entidad.IdMunicipio _
		, entidad.Telefonos _
		, entidad.Nit _
		, entidad.Dui _
		, entidad.Nrc _
		, entidad.Giro _
		, entidad.Email _
		, entidad.ReferidoPor _
		, entidad.FechaContacto _
		, entidad.Asunto _
		, entidad.LugarContacto _
		, entidad.IdVendedor _
		, entidad.CreadoPor _
		, entidad.FechaHoraCreacion _
		, entidad.ModificadoPor _
		, entidad.FechaHoraModificacion
		)
	End Sub

	Public Sub fac_ProspectacionClientesUpdate _
(ByVal entidad As fac_ProspectacionClientes, Transaccion As DbTransaction)

		db.ExecuteNonQuery(Transaccion, "fac_ProspectacionClientesUpdate",
		entidad.IdComprobante _
		, entidad.NombreCliente _
		, entidad.RazonSocial _
		, entidad.Direccion _
		, entidad.IdDepartamento _
		, entidad.IdMunicipio _
		, entidad.Telefonos _
		, entidad.Nit _
		, entidad.Dui _
		, entidad.Nrc _
		, entidad.Giro _
		, entidad.Email _
		, entidad.ReferidoPor _
		, entidad.FechaContacto _
		, entidad.Asunto _
		, entidad.LugarContacto _
		, entidad.IdVendedor _
		, entidad.CreadoPor _
		, entidad.FechaHoraCreacion _
		, entidad.ModificadoPor _
		, entidad.FechaHoraModificacion
		)
	End Sub

#End Region




#Region "fac_Profesiones"
	Public Function fac_ProfesionesSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_ProfesionesSelectAll").Tables(0)
    End Function

    Public Function fac_ProfesionesSelectByPK _
            (ByVal IdProfesion As System.Int16 _
            ) As fac_Profesiones

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_ProfesionesSelectByPK", _
            IdProfesion _
            ).tables(0)

        Dim Entidad As New fac_Profesiones
        If dt.Rows.Count > 0 Then
            entidad.IdProfesion = dt.rows(0).item("IdProfesion")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Asegurado = dt.rows(0).item("Asegurado")
            entidad.Peso = dt.rows(0).item("Peso")

        End If
        Return Entidad
    End Function

    Public Sub fac_ProfesionesDeleteByPK _
            (ByVal IdProfesion As System.Int16 _
            )

        db.ExecuteNonQuery("fac_ProfesionesDeleteByPK", _
            IdProfesion _
            )
    End Sub

    Public Sub fac_ProfesionesDeleteByPK _
            (ByVal IdProfesion As System.Int16 _
            , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ProfesionesDeleteByPK", _
            IdProfesion _
            )
    End Sub

    Public Sub fac_ProfesionesInsert _
    (ByVal entidad As fac_Profesiones)

        db.ExecuteNonQuery("fac_ProfesionesInsert", _
            entidad.IdProfesion _
            , entidad.Nombre _
            , entidad.Asegurado _
            , entidad.Peso _
            )
    End Sub

    Public Sub fac_ProfesionesInsert _
    (ByVal entidad As fac_Profesiones, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ProfesionesInsert", _
            entidad.IdProfesion _
            , entidad.Nombre _
            , entidad.Asegurado _
            , entidad.Peso _
            )
    End Sub

    Public Sub fac_ProfesionesUpdate _
    (ByVal entidad As fac_Profesiones)

        db.ExecuteNonQuery("fac_ProfesionesUpdate", _
            entidad.IdProfesion _
            , entidad.Nombre _
            , entidad.Asegurado _
            , entidad.Peso _
            )
    End Sub

    Public Sub fac_ProfesionesUpdate _
    (ByVal entidad As fac_Profesiones, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_ProfesionesUpdate", _
            entidad.IdProfesion _
            , entidad.Nombre _
            , entidad.Asegurado _
            , entidad.Peso _
            )
    End Sub

#End Region
#Region "fac_CorreosAlerta"
    Public Function fac_CorreosAlertaSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_CorreosAlertaSelectAll").Tables(0)
    End Function

    Public Function fac_CorreosAlertaSelectByPK _
            (ByVal IdCorreo As System.Int32 _
            ) As fac_CorreosAlerta

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_CorreosAlertaSelectByPK", _
            IdCorreo _
            ).tables(0)

        Dim Entidad As New fac_CorreosAlerta
        If dt.Rows.Count > 0 Then
            entidad.IdCorreo = dt.rows(0).item("IdCorreo")
            entidad.Correo = dt.rows(0).item("Correo")
            entidad.Contacto = dt.rows(0).item("Contacto")
            entidad.Cargo = dt.rows(0).item("Cargo")
            entidad.Tipo = dt.rows(0).item("Tipo")

        End If
        Return Entidad
    End Function

    Public Sub fac_CorreosAlertaDeleteByPK _
            (ByVal IdCorreo As System.Int32 _
            )

        db.ExecuteNonQuery("fac_CorreosAlertaDeleteByPK", _
            IdCorreo _
            )
    End Sub

    Public Sub fac_CorreosAlertaDeleteByPK _
            (ByVal IdCorreo As System.Int32 _
            , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CorreosAlertaDeleteByPK", _
            IdCorreo _
            )
    End Sub

    Public Sub fac_CorreosAlertaInsert _
    (ByVal entidad As fac_CorreosAlerta)

        db.ExecuteNonQuery("fac_CorreosAlertaInsert", _
            entidad.IdCorreo _
            , entidad.Correo _
            , entidad.Contacto _
            , entidad.Cargo _
            , entidad.Tipo _
            )
    End Sub

    Public Sub fac_CorreosAlertaInsert _
    (ByVal entidad As fac_CorreosAlerta, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CorreosAlertaInsert", _
            entidad.IdCorreo _
            , entidad.Correo _
            , entidad.Contacto _
            , entidad.Cargo _
            , entidad.Tipo _
            )
    End Sub

    Public Sub fac_CorreosAlertaUpdate _
    (ByVal entidad As fac_CorreosAlerta)

        db.ExecuteNonQuery("fac_CorreosAlertaUpdate", _
            entidad.IdCorreo _
            , entidad.Correo _
            , entidad.Contacto _
            , entidad.Cargo _
            , entidad.Tipo _
            )
    End Sub

    Public Sub fac_CorreosAlertaUpdate _
    (ByVal entidad As fac_CorreosAlerta, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_CorreosAlertaUpdate", _
            entidad.IdCorreo _
            , entidad.Correo _
            , entidad.Contacto _
            , entidad.Cargo _
            , entidad.Tipo _
            )
    End Sub

#End Region
#Region "fac_BitacoraAlertas"
    Public Function fac_BitacoraAlertasSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_BitacoraAlertasSelectAll").Tables(0)
    End Function

    Public Function fac_BitacoraAlertasSelectByPK _
            (ByVal IdBitacora As System.Int32 _
            ) As fac_BitacoraAlertas

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_BitacoraAlertasSelectByPK", _
            IdBitacora _
            ).tables(0)

        Dim Entidad As New fac_BitacoraAlertas
        If dt.Rows.Count > 0 Then
            entidad.IdBitacora = dt.rows(0).item("IdBitacora")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdCliente = dt.rows(0).item("IdCliente")
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.PesoRiesgo = dt.rows(0).item("PesoRiesgo")
            entidad.IdAlerta = dt.rows(0).item("IdAlerta")
            entidad.NumeroReferencia = dt.rows(0).item("NumeroReferencia")
            entidad.TipoTransaccion = dt.rows(0).item("TipoTransaccion")
            entidad.MontoEstimadoCompra = dt.rows(0).item("MontoEstimadoCompra")
            entidad.MontoTransaccion = dt.rows(0).item("MontoTransaccion")
            entidad.DiferenciaPerfilMensual = dt.rows(0).item("DiferenciaPerfilMensual")
            entidad.DiferenciaPorcentaje = dt.rows(0).item("DiferenciaPorcentaje")
            entidad.IdFormaPago = dt.rows(0).item("IdFormaPago")
            entidad.NombreTercero = dt.rows(0).item("NombreTercero")
            entidad.Parentesco = dt.rows(0).item("Parentesco")
            entidad.IdActividad = dt.rows(0).item("IdActividad")
            entidad.Profesion = dt.rows(0).item("Profesion")
            entidad.IngresosDeclarados = dt.rows(0).item("IngresosDeclarados")
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.Revisada = dt.rows(0).item("Revisada")
            entidad.FechaRevisada = dt.rows(0).item("FechaRevisada")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.Comentario = dt.rows(0).item("Comentario")

        End If
        Return Entidad
    End Function

    Public Sub fac_BitacoraAlertasDeleteByPK _
            (ByVal IdBitacora As System.Int32 _
            )

        db.ExecuteNonQuery("fac_BitacoraAlertasDeleteByPK", _
            IdBitacora _
            )
    End Sub

    Public Sub fac_BitacoraAlertasDeleteByPK _
            (ByVal IdBitacora As System.Int32 _
            , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_BitacoraAlertasDeleteByPK", _
            IdBitacora _
            )
    End Sub

    Public Sub fac_BitacoraAlertasInsert _
    (ByVal entidad As fac_BitacoraAlertas)

        db.ExecuteNonQuery("fac_BitacoraAlertasInsert", _
            entidad.IdBitacora _
            , entidad.Fecha _
            , entidad.IdCliente _
            , entidad.IdComprobante _
            , entidad.PesoRiesgo _
            , entidad.IdAlerta _
            , entidad.NumeroReferencia _
            , entidad.TipoTransaccion _
            , entidad.MontoEstimadoCompra _
            , entidad.MontoTransaccion _
            , entidad.DiferenciaPerfilMensual _
            , entidad.DiferenciaPorcentaje _
            , entidad.IdFormaPago _
            , entidad.NombreTercero _
            , entidad.Parentesco _
            , entidad.IdActividad _
            , entidad.Profesion _
            , entidad.IngresosDeclarados _
            , entidad.IdSucursal _
            , entidad.Revisada _
            , entidad.FechaRevisada _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            , entidad.Comentario _
            )
    End Sub

    Public Sub fac_BitacoraAlertasInsert _
    (ByVal entidad As fac_BitacoraAlertas, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_BitacoraAlertasInsert", _
            entidad.IdBitacora _
            , entidad.Fecha _
            , entidad.IdCliente _
            , entidad.IdComprobante _
            , entidad.PesoRiesgo _
            , entidad.IdAlerta _
            , entidad.NumeroReferencia _
            , entidad.TipoTransaccion _
            , entidad.MontoEstimadoCompra _
            , entidad.MontoTransaccion _
            , entidad.DiferenciaPerfilMensual _
            , entidad.DiferenciaPorcentaje _
            , entidad.IdFormaPago _
            , entidad.NombreTercero _
            , entidad.Parentesco _
            , entidad.IdActividad _
            , entidad.Profesion _
            , entidad.IngresosDeclarados _
            , entidad.IdSucursal _
            , entidad.Revisada _
            , entidad.FechaRevisada _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            , entidad.Comentario _
            )
    End Sub

    Public Sub fac_BitacoraAlertasUpdate _
    (ByVal entidad As fac_BitacoraAlertas)

        db.ExecuteNonQuery("fac_BitacoraAlertasUpdate", _
            entidad.IdBitacora _
            , entidad.Fecha _
            , entidad.IdCliente _
            , entidad.IdComprobante _
            , entidad.PesoRiesgo _
            , entidad.IdAlerta _
            , entidad.NumeroReferencia _
            , entidad.TipoTransaccion _
            , entidad.MontoEstimadoCompra _
            , entidad.MontoTransaccion _
            , entidad.DiferenciaPerfilMensual _
            , entidad.DiferenciaPorcentaje _
            , entidad.IdFormaPago _
            , entidad.NombreTercero _
            , entidad.Parentesco _
            , entidad.IdActividad _
            , entidad.Profesion _
            , entidad.IngresosDeclarados _
            , entidad.IdSucursal _
            , entidad.Revisada _
            , entidad.FechaRevisada _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            , entidad.Comentario _
            )
    End Sub

    Public Sub fac_BitacoraAlertasUpdate _
    (ByVal entidad As fac_BitacoraAlertas, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_BitacoraAlertasUpdate", _
            entidad.IdBitacora _
            , entidad.Fecha _
            , entidad.IdCliente _
            , entidad.IdComprobante _
            , entidad.PesoRiesgo _
            , entidad.IdAlerta _
            , entidad.NumeroReferencia _
            , entidad.TipoTransaccion _
            , entidad.MontoEstimadoCompra _
            , entidad.MontoTransaccion _
            , entidad.DiferenciaPerfilMensual _
            , entidad.DiferenciaPorcentaje _
            , entidad.IdFormaPago _
            , entidad.NombreTercero _
            , entidad.Parentesco _
            , entidad.IdActividad _
            , entidad.Profesion _
            , entidad.IngresosDeclarados _
            , entidad.IdSucursal _
            , entidad.Revisada _
            , entidad.FechaRevisada _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            , entidad.Comentario _
            )
    End Sub

#End Region


#Region "fac_VentasDetalle"
    Public Function fac_VentasDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_VentasDetalleSelectAll").Tables(0)
    End Function

    Public Function fac_VentasDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_VentasDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_VentasDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New fac_VentasDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            Entidad.Cantidad = dt.Rows(0).Item("Cantidad")
            Entidad.IdPrecio = dt.Rows(0).Item("IdPrecio")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.PrecioVenta = dt.rows(0).item("PrecioVenta")
            entidad.PorcDescuento = dt.rows(0).item("PorcDescuento")
            entidad.ValorDescuento = dt.rows(0).item("ValorDescuento")
            Entidad.PrecioUnitario = dt.Rows(0).Item("PrecioUnitario")
            Entidad.VentaNoSujeta = dt.Rows(0).Item("VentaNoSujeta")
            Entidad.VentaExenta = dt.Rows(0).Item("VentaExenta")
            Entidad.VentaAfecta = dt.Rows(0).Item("VentaAfecta")
            Entidad.VentaNeta = dt.Rows(0).Item("VentaNeta")
            Entidad.TipoImpuesto = dt.Rows(0).Item("TipoImpuesto")
            Entidad.ValorIVA = dt.Rows(0).Item("ValorIVA")
            Entidad.PrecioCosto = dt.Rows(0).Item("PrecioCosto")
            Entidad.PorcComision = dt.Rows(0).Item("PorcComision")
            Entidad.DescuentoAutorizadoPor = dt.Rows(0).Item("DescuentoAutorizadoPor")
            Entidad.IdCentro = dt.Rows(0).Item("IdCentro")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub fac_VentasDetalleDeleteByPK(ByVal IdComprobante As System.Int32, ByVal IdDetalle As System.Int32)

        db.ExecuteNonQuery("fac_VentasDetalleDeleteByPK", _
         IdComprobante, IdDetalle)
    End Sub

    Public Sub fac_VentasDetalleDeleteByPK(ByVal IdComprobante As System.Int32, ByVal IdDetalle As System.Int32, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VentasDetalleDeleteByPK", IdComprobante, IdDetalle)
    End Sub

    Public Sub fac_VentasDetalleInsert(ByVal entidad As fac_VentasDetalle)

        db.ExecuteNonQuery("fac_VentasDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.IdPrecio _
         , entidad.Descripcion _
         , entidad.PrecioVenta _
         , entidad.PorcDescuento _
         , entidad.ValorDescuento _
         , entidad.PrecioUnitario _
         , entidad.VentaExenta _
         , entidad.VentaAfecta _
         , entidad.VentaNeta _
         , entidad.TipoImpuesto _
         , entidad.ValorIVA _
          , entidad.PrecioCosto _
          , entidad.PorcComision _
          , entidad.DescuentoAutorizadoPor _
          , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub fac_VentasDetalleInsert _
    (ByVal entidad As fac_VentasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VentasDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.IdPrecio _
         , entidad.Descripcion _
         , entidad.PrecioVenta _
         , entidad.PorcDescuento _
         , entidad.ValorDescuento _
         , entidad.PrecioUnitario _
         , entidad.VentaNoSujeta _
         , entidad.VentaExenta _
         , entidad.VentaAfecta _
         , entidad.VentaNeta _
         , entidad.TipoImpuesto _
         , entidad.ValorIVA _
         , entidad.PrecioCosto _
         , entidad.PorcComision _
         , entidad.DescuentoAutorizadoPor _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub fac_VentasDetalleUpdate _
    (ByVal entidad As fac_VentasDetalle)

        db.ExecuteNonQuery("fac_VentasDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.IdPrecio _
         , entidad.Descripcion _
         , entidad.PrecioVenta _
         , entidad.PorcDescuento _
         , entidad.ValorDescuento _
         , entidad.PrecioUnitario _
         , entidad.VentaNoSujeta _
         , entidad.VentaExenta _
         , entidad.VentaAfecta _
         , entidad.VentaNeta _
         , entidad.TipoImpuesto _
         , entidad.ValorIVA _
         , entidad.PrecioCosto _
         , entidad.PorcComision _
         , entidad.DescuentoAutorizadoPor _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub fac_VentasDetalleUpdate _
    (ByVal entidad As fac_VentasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VentasDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.IdPrecio _
         , entidad.Descripcion _
         , entidad.PrecioVenta _
         , entidad.PorcDescuento _
         , entidad.ValorDescuento _
         , entidad.PrecioUnitario _
         , entidad.VentaNoSujeta _
         , entidad.VentaExenta _
         , entidad.VentaAfecta _
         , entidad.VentaNeta _
         , entidad.TipoImpuesto _
         , entidad.ValorIVA _
         , entidad.PrecioCosto _
         , entidad.PorcComision _
         , entidad.DescuentoAutorizadoPor _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region

#Region "fac_PuntosVenta"
    Public Function fac_PuntosVentaSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_PuntosVentaSelectAll").Tables(0)
    End Function
    Public Function fac_PuntosVentaSelectByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32
      ) As fac_PuntosVenta

        Dim dt As DataTable
        dt = db.ExecuteDataSet("fac_PuntosVentaSelectByPK",
         IdSucursal _
         , IdPunto
         ).Tables(0)

        Dim Entidad As New fac_PuntosVenta
        If dt.Rows.Count > 0 Then
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.IdPunto = dt.Rows(0).Item("IdPunto")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.IdCuentaBanco = dt.Rows(0).Item("IdCuentaBanco")
            Entidad.IdCuentaCajaChica = dt.Rows(0).Item("IdCuentaCajaChica")
            Entidad.VentaMensual = dt.Rows(0).Item("VentaMensual")
        End If
        Return Entidad
    End Function

    Public Sub fac_PuntosVentaDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32
      )

        db.ExecuteNonQuery("fac_PuntosVentaDeleteByPK",
         IdSucursal _
         , IdPunto
         )
    End Sub

    Public Sub fac_PuntosVentaDeleteByPK _
      (ByVal IdSucursal As System.Int32 _
      , ByVal IdPunto As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_PuntosVentaDeleteByPK",
         IdSucursal _
         , IdPunto
         )
    End Sub

    Public Sub fac_PuntosVentaInsert _
    (ByVal entidad As fac_PuntosVenta)

        db.ExecuteNonQuery("fac_PuntosVentaInsert",
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Nombre _
         , entidad.IdCuentaBanco _
         , entidad.IdCuentaCajaChica _
         , entidad.VentaMensual
         )
    End Sub

    Public Sub fac_PuntosVentaInsert _
    (ByVal entidad As fac_PuntosVenta, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_PuntosVentaInsert",
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Nombre _
        , entidad.IdCuentaBanco _
         , entidad.IdCuentaCajaChica _
         , entidad.VentaMensual
         )
    End Sub

    Public Sub fac_PuntosVentaUpdate _
    (ByVal entidad As fac_PuntosVenta)

        db.ExecuteNonQuery("fac_PuntosVentaUpdate",
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Nombre _
        , entidad.IdCuentaBanco _
         , entidad.IdCuentaCajaChica _
         , entidad.VentaMensual
         )
    End Sub

    Public Sub fac_PuntosVentaUpdate _
    (ByVal entidad As fac_PuntosVenta, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_PuntosVentaUpdate",
         entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Nombre _
        , entidad.IdCuentaBanco _
         , entidad.IdCuentaCajaChica _
         , entidad.VentaMensual
         )
    End Sub

#End Region
#Region "fac_SeriesDocumentos"
    Public Function fac_SeriesDocumentosSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_SeriesDocumentosSelectAll").Tables(0)
    End Function

    Public Function fac_SeriesDocumentosSelectByPK _
      (ByVal IdPunto As System.Int32 _
      , ByVal IdComprobante As System.Int32 _
      , ByVal Serie As System.String _
      ) As fac_SeriesDocumentos

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_SeriesDocumentosSelectByPK", _
         IdPunto _
         , IdComprobante _
         , Serie _
         ).tables(0)

        Dim Entidad As New fac_SeriesDocumentos
        If dt.Rows.Count > 0 Then
            entidad.IdPunto = dt.rows(0).item("IdPunto")
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.Serie = dt.rows(0).item("Serie")
            entidad.FechaAsignacion = dt.rows(0).item("FechaAsignacion")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.UltimoNumero = dt.rows(0).item("UltimoNumero")
            entidad.DesdeNumero = dt.rows(0).item("DesdeNumero")
            entidad.HastaNumero = dt.rows(0).item("HastaNumero")
            entidad.EsActivo = dt.rows(0).item("EsActivo")

        End If
        Return Entidad
    End Function

    Public Sub fac_SeriesDocumentosDeleteByPK _
      (ByVal IdPunto As System.Int32 _
      , ByVal IdComprobante As System.Int32 _
      , ByVal Serie As System.String _
      )

        db.ExecuteNonQuery("fac_SeriesDocumentosDeleteByPK", _
         IdPunto _
         , IdComprobante _
         , Serie _
         )
    End Sub

    Public Sub fac_SeriesDocumentosDeleteByPK _
      (ByVal IdPunto As System.Int32 _
      , ByVal IdComprobante As System.Int32 _
      , ByVal Serie As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_SeriesDocumentosDeleteByPK", _
         IdPunto _
         , IdComprobante _
         , Serie _
         )
    End Sub

    Public Sub fac_SeriesDocumentosInsert _
    (ByVal entidad As fac_SeriesDocumentos)

        db.ExecuteNonQuery("fac_SeriesDocumentosInsert", _
         entidad.IdPunto _
         , entidad.IdComprobante _
         , entidad.Serie _
         , entidad.FechaAsignacion _
         , entidad.CreadoPor _
         , entidad.UltimoNumero _
         , entidad.DesdeNumero _
         , entidad.HastaNumero _
         , entidad.EsActivo _
         )
    End Sub

    Public Sub fac_SeriesDocumentosInsert _
    (ByVal entidad As fac_SeriesDocumentos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_SeriesDocumentosInsert", _
         entidad.IdPunto _
         , entidad.IdComprobante _
         , entidad.Serie _
         , entidad.FechaAsignacion _
         , entidad.CreadoPor _
         , entidad.UltimoNumero _
         , entidad.DesdeNumero _
         , entidad.HastaNumero _
         , entidad.EsActivo _
         )
    End Sub

    Public Sub fac_SeriesDocumentosUpdate _
    (ByVal entidad As fac_SeriesDocumentos)

        db.ExecuteNonQuery("fac_SeriesDocumentosUpdate", _
         entidad.IdPunto _
         , entidad.IdComprobante _
         , entidad.Serie _
         , entidad.FechaAsignacion _
         , entidad.CreadoPor _
         , entidad.UltimoNumero _
         , entidad.DesdeNumero _
         , entidad.HastaNumero _
         , entidad.EsActivo _
         )
    End Sub

    Public Sub fac_SeriesDocumentosUpdate _
    (ByVal entidad As fac_SeriesDocumentos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_SeriesDocumentosUpdate", _
         entidad.IdPunto _
         , entidad.IdComprobante _
         , entidad.Serie _
         , entidad.FechaAsignacion _
         , entidad.CreadoPor _
         , entidad.UltimoNumero _
         , entidad.DesdeNumero _
         , entidad.HastaNumero _
         , entidad.EsActivo _
         )
    End Sub

#End Region
#Region "fac_Vendedores"
    Public Function fac_VendedoresSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_VendedoresSelectAll").Tables(0)
    End Function

    Public Function fac_VendedoresSelectByPK(ByVal IdVendedor As System.String) As fac_Vendedores

        Dim dt As DataTable
        dt = db.ExecuteDataSet("fac_VendedoresSelectByPK", IdVendedor).Tables(0)

        Dim Entidad As New fac_Vendedores
        If dt.Rows.Count > 0 Then
            Entidad.IdVendedor = dt.Rows(0).Item("IdVendedor")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Activo = dt.Rows(0).Item("Activo")
            Entidad.Telefonos = dt.Rows(0).Item("Telefonos")
            Entidad.Direccion = dt.Rows(0).Item("Direccion")
            Entidad.LimiteVenta = dt.Rows(0).Item("LimiteVenta")
            Entidad.PorcComision = dt.Rows(0).Item("PorcComision")
            Entidad.IdEmpleado = dt.Rows(0).Item("IdEmpleado")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.AllRCV = dt.Rows(0).Item("AllRCV")
        End If
        Return Entidad
    End Function

    Public Sub fac_VendedoresDeleteByPK(ByVal IdVendedor As System.String)

        db.ExecuteNonQuery("fac_VendedoresDeleteByPK", IdVendedor)
    End Sub

    Public Sub fac_VendedoresDeleteByPK(ByVal IdVendedor As System.String, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VendedoresDeleteByPK", IdVendedor)
    End Sub

    Public Sub fac_VendedoresInsert _
    (ByVal entidad As fac_Vendedores)

        db.ExecuteNonQuery("fac_VendedoresInsert",
         entidad.IdVendedor _
         , entidad.Nombre _
         , entidad.Activo _
         , entidad.Telefonos _
         , entidad.Direccion _
         , entidad.LimiteVenta _
         , entidad.PorcComision _
         , entidad.IdEmpleado _
         , entidad.IdSucursal _
         , entidad.AllRCV)
    End Sub

    Public Sub fac_VendedoresInsert _
    (ByVal entidad As fac_Vendedores, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VendedoresInsert",
         entidad.IdVendedor _
         , entidad.Nombre _
         , entidad.Activo _
         , entidad.Telefonos _
         , entidad.Direccion _
         , entidad.LimiteVenta _
         , entidad.PorcComision _
         , entidad.IdEmpleado _
         , entidad.IdSucursal _
         , entidad.AllRCV)
    End Sub

    Public Sub fac_VendedoresUpdate _
    (ByVal entidad As fac_Vendedores)

        db.ExecuteNonQuery("fac_VendedoresUpdate",
         entidad.IdVendedor _
         , entidad.Nombre _
         , entidad.Activo _
         , entidad.Telefonos _
         , entidad.Direccion _
         , entidad.LimiteVenta _
         , entidad.PorcComision _
         , entidad.IdEmpleado _
         , entidad.IdSucursal _
         , entidad.AllRCV)
    End Sub

    Public Sub fac_VendedoresUpdate _
    (ByVal entidad As fac_Vendedores, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VendedoresUpdate",
         entidad.IdVendedor _
         , entidad.Nombre _
         , entidad.Activo _
         , entidad.Telefonos _
         , entidad.Direccion _
         , entidad.LimiteVenta _
         , entidad.PorcComision _
         , entidad.IdEmpleado _
         , entidad.IdSucursal _
         , entidad.AllRCV)
    End Sub

#End Region
#Region "fac_VendedoresDetalle"
    Public Function fac_VendedoresDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_VendedoresDetalleSelectAll").Tables(0)
    End Function

    Public Function fac_VendedoresDetalleSelectByPK _
      (ByVal IdVendedor As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_VendedoresDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_VendedoresDetalleSelectByPK", _
         IdVendedor _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New fac_VendedoresDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdVendedor = dt.rows(0).item("IdVendedor")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.RangoDesde = dt.rows(0).item("RangoDesde")
            entidad.RangoHasta = dt.rows(0).item("RangoHasta")
            entidad.Porcentaje = dt.rows(0).item("Porcentaje")

        End If
        Return Entidad
    End Function

    Public Sub fac_VendedoresDetalleDeleteByPK _
      (ByVal IdVendedor As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("fac_VendedoresDetalleDeleteByPK", _
         IdVendedor _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_VendedoresDetalleDeleteByPK _
      (ByVal IdVendedor As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VendedoresDetalleDeleteByPK", _
         IdVendedor _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_VendedoresDetalleInsert _
    (ByVal entidad As fac_VendedoresDetalle)

        db.ExecuteNonQuery("fac_VendedoresDetalleInsert", _
         entidad.IdVendedor _
         , entidad.IdDetalle _
         , entidad.RangoDesde _
         , entidad.RangoHasta _
         , entidad.Porcentaje _
         )
    End Sub

    Public Sub fac_VendedoresDetalleInsert _
    (ByVal entidad As fac_VendedoresDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VendedoresDetalleInsert",
         entidad.IdVendedor _
         , entidad.IdDetalle _
         , entidad.RangoDesde _
         , entidad.RangoHasta _
         , entidad.Porcentaje
         )
    End Sub

    Public Sub fac_VendedoresDetalleUpdate _
    (ByVal entidad As fac_VendedoresDetalle)

        db.ExecuteNonQuery("fac_VendedoresDetalleUpdate", _
         entidad.IdVendedor _
         , entidad.IdDetalle _
         , entidad.RangoDesde _
         , entidad.RangoHasta _
         , entidad.Porcentaje _
         )
    End Sub

    Public Sub fac_VendedoresDetalleUpdate _
    (ByVal entidad As fac_VendedoresDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_VendedoresDetalleUpdate",
         entidad.IdVendedor _
         , entidad.IdDetalle _
         , entidad.RangoDesde _
         , entidad.RangoHasta _
         , entidad.Porcentaje
         )
    End Sub
#End Region
#Region "fac_VendedoresClientes"
    Public Sub fac_VendedoresClientesInsert _
        (ByVal entidad As fac_VendedoresClientes, ByVal Transaccion As DbTransaction)
        db.ExecuteNonQuery(Transaccion, CommandType.Text,
                           " INSERT INTO fac_VendedoresClientes " _
                          & " SELECT " & entidad.IdVendedor & " = " & entidad.IdVendedor _
                          & " , " & entidad.IdCliente & " = '" & entidad.IdCliente & "'")
    End Sub
    Public Sub fac_VendedoresClientesDelete _
        (ByVal entidad As fac_VendedoresClientes, ByVal Transaccion As DbTransaction)
        db.ExecuteNonQuery(Transaccion, CommandType.Text,
                           " DELETE FROM fac_VendedoresClientes " _
                          & " WHERE " & entidad.IdVendedor & " = " & entidad.IdVendedor _
                         & " AND " & entidad.IdCliente & " = '" & entidad.IdCliente & "'")
    End Sub
#End Region
#Region "fac_FormasPago"
    Public Function fac_FormasPagoSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_FormasPagoSelectAll").Tables(0)
    End Function

    Public Function fac_FormasPagoSelectByPK _
      (ByVal IdFormaPago As System.Int32 _
      ) As fac_FormasPago

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_FormasPagoSelectByPK", _
         IdFormaPago _
         ).tables(0)

        Dim Entidad As New fac_FormasPago
        If dt.Rows.Count > 0 Then
            entidad.IdFormaPago = dt.rows(0).item("IdFormaPago")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.IdCuentaContable = dt.rows(0).item("IdCuentaContable")
            entidad.DiasCredito = dt.rows(0).item("DiasCredito")

        End If
        Return Entidad
    End Function

    Public Sub fac_FormasPagoDeleteByPK _
      (ByVal IdFormaPago As System.Int32 _
      )

        db.ExecuteNonQuery("fac_FormasPagoDeleteByPK", _
         IdFormaPago _
         )
    End Sub

    Public Sub fac_FormasPagoDeleteByPK _
      (ByVal IdFormaPago As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_FormasPagoDeleteByPK", _
         IdFormaPago _
         )
    End Sub

    Public Sub fac_FormasPagoInsert _
    (ByVal entidad As fac_FormasPago)

        db.ExecuteNonQuery("fac_FormasPagoInsert", _
         entidad.IdFormaPago _
         , entidad.Nombre _
         , entidad.IdCuentaContable _
         , entidad.DiasCredito _
         )
    End Sub

    Public Sub fac_FormasPagoInsert _
    (ByVal entidad As fac_FormasPago, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_FormasPagoInsert", _
         entidad.IdFormaPago _
         , entidad.Nombre _
         , entidad.IdCuentaContable _
         , entidad.DiasCredito _
         )
    End Sub

    Public Sub fac_FormasPagoUpdate _
    (ByVal entidad As fac_FormasPago)

        db.ExecuteNonQuery("fac_FormasPagoUpdate", _
         entidad.IdFormaPago _
         , entidad.Nombre _
         , entidad.IdCuentaContable _
         , entidad.DiasCredito _
         )
    End Sub

    Public Sub fac_FormasPagoUpdate _
    (ByVal entidad As fac_FormasPago, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_FormasPagoUpdate", _
         entidad.IdFormaPago _
         , entidad.Nombre _
         , entidad.IdCuentaContable _
         , entidad.DiasCredito _
         )
    End Sub

#End Region

#Region "fac_NotasCredito"
    Public Function fac_NotasCreditoSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_NotasCreditoSelectAll").Tables(0)
    End Function

    Public Function fac_NotasCreditoSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_NotasCredito

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_NotasCreditoSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New fac_NotasCredito
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdComprobVenta = dt.rows(0).item("IdComprobVenta")
            entidad.MontoAbonado = dt.rows(0).item("MontoAbonado")
            entidad.SaldoComprobante = dt.rows(0).item("SaldoComprobante")
            entidad.NumComprobanteVenta = dt.rows(0).item("NumComprobanteVenta")

        End If
        Return Entidad
    End Function

    Public Sub fac_NotasCreditoDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("fac_NotasCreditoDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_NotasCreditoDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_NotasCreditoDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_NotasCreditoInsert _
    (ByVal entidad As fac_NotasCredito)

        db.ExecuteNonQuery("fac_NotasCreditoInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         , entidad.MontoAbonado _
         , entidad.SaldoComprobante _
         , entidad.NumComprobanteVenta _
         )
    End Sub

    Public Sub fac_NotasCreditoInsert _
    (ByVal entidad As fac_NotasCredito, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_NotasCreditoInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         , entidad.MontoAbonado _
         , entidad.SaldoComprobante _
         , entidad.NumComprobanteVenta _
         )
    End Sub

    Public Sub fac_NotasCreditoUpdate _
    (ByVal entidad As fac_NotasCredito)

        db.ExecuteNonQuery("fac_NotasCreditoUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         , entidad.MontoAbonado _
         , entidad.SaldoComprobante _
         , entidad.NumComprobanteVenta _
         )
    End Sub

    Public Sub fac_NotasCreditoUpdate _
    (ByVal entidad As fac_NotasCredito, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_NotasCreditoUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         , entidad.MontoAbonado _
         , entidad.SaldoComprobante _
         , entidad.NumComprobanteVenta _
         )
    End Sub

#End Region
#Region "fac_Retencion"
    Public Function fac_RetencionSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_RetencionSelectAll").Tables(0)
    End Function

    Public Function fac_RetencionSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As fac_Retencion

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_RetencionSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New fac_Retencion
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.Serie = dt.rows(0).item("Serie")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdClienteProveedor = dt.rows(0).item("IdClienteProveedor")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Direccion = dt.rows(0).item("Direccion")
            entidad.Giro = dt.rows(0).item("Giro")
            entidad.Nrc = dt.rows(0).item("Nrc")
            entidad.Nit = dt.rows(0).item("Nit")
            entidad.Observaciones = dt.rows(0).item("Observaciones")
            entidad.TotalIva = dt.rows(0).item("TotalIva")
            entidad.TotalImpuesto1 = dt.rows(0).item("TotalImpuesto1")
            entidad.TotalImpuesto2 = dt.rows(0).item("TotalImpuesto2")
            entidad.TotalComprobante = dt.rows(0).item("TotalComprobante")
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
            Entidad.FechaContable = fd.SiEsNulo(dt.Rows(0).Item("FechaContable"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub fac_RetencionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("fac_RetencionDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_RetencionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_RetencionDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub fac_RetencionInsert _
    (ByVal entidad As fac_Retencion)

        db.ExecuteNonQuery("fac_RetencionInsert",
         entidad.IdComprobante _
         , entidad.Serie _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdClienteProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.Giro _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.Observaciones _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.TotalComprobante _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         , entidad.FechaContable
         )
    End Sub

    Public Sub fac_RetencionInsert _
    (ByVal entidad As fac_Retencion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_RetencionInsert",
         entidad.IdComprobante _
         , entidad.Serie _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdClienteProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.Giro _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.Observaciones _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.TotalComprobante _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         , entidad.FechaContable
         )
    End Sub

    Public Sub fac_RetencionUpdate _
    (ByVal entidad As fac_Retencion)

        db.ExecuteNonQuery("fac_RetencionUpdate",
         entidad.IdComprobante _
         , entidad.Serie _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdClienteProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.Giro _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.Observaciones _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.TotalComprobante _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         , entidad.FechaContable
         )
    End Sub

    Public Sub fac_RetencionUpdate _
    (ByVal entidad As fac_Retencion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_RetencionUpdate",
         entidad.IdComprobante _
         , entidad.Serie _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdClienteProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.Giro _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.Observaciones _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.TotalComprobante _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         , entidad.FechaContable
         )
    End Sub

#End Region
#Region "fac_RetencionDetalle"
    Public Function fac_RetencionDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_RetencionDetalleSelectAll").Tables(0)
    End Function

    Public Function fac_RetencionDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As fac_RetencionDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_RetencionDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New fac_RetencionDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.PrecioUnitario = dt.rows(0).item("PrecioUnitario")
            entidad.PrecioTotal = dt.rows(0).item("PrecioTotal")

        End If
        Return Entidad
    End Function

    Public Sub fac_RetencionDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("fac_RetencionDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_RetencionDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_RetencionDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub fac_RetencionDetalleInsert _
    (ByVal entidad As fac_RetencionDetalle)

        db.ExecuteNonQuery("fac_RetencionDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         )
    End Sub

    Public Sub fac_RetencionDetalleInsert _
    (ByVal entidad As fac_RetencionDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_RetencionDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         )
    End Sub

    Public Sub fac_RetencionDetalleUpdate _
    (ByVal entidad As fac_RetencionDetalle)

        db.ExecuteNonQuery("fac_RetencionDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         )
    End Sub

    Public Sub fac_RetencionDetalleUpdate _
    (ByVal entidad As fac_RetencionDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_RetencionDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         )
    End Sub

#End Region
#Region "fac_Gestiones"
    Public Function fac_GestionesSelectAll() As DataTable
        Return db.ExecuteDataSet("fac_GestionesSelectAll").Tables(0)
    End Function

    Public Function fac_GestionesSelectByPK _
            (ByVal IdGestion As System.Int32 _
            ) As fac_Gestiones

        Dim dt As datatable
        dt = db.ExecuteDataSet("fac_GestionesSelectByPK", _
            IdGestion _
            ).tables(0)

        Dim Entidad As New fac_Gestiones
        If dt.Rows.Count > 0 Then
            Entidad.IdGestion = dt.Rows(0).Item("IdGestion")
            Entidad.IdCliente = dt.Rows(0).Item("IdCliente")
            Entidad.NombreCliente = dt.Rows(0).Item("NombreCliente")
            Entidad.Motivo = dt.Rows(0).Item("Motivo")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.HoraLlamada = dt.Rows(0).Item("HoraLlamada")
            Entidad.ComentarioGeneral = dt.Rows(0).Item("ComentarioGeneral")
            Entidad.ComentarioCliente = dt.Rows(0).Item("ComentarioCliente")
            Entidad.ProximaGestion = fd.SiEsNulo(dt.Rows(0).Item("ProximaGestion"), Nothing)
            Entidad.FechaVisita = fd.SiEsNulo(dt.Rows(0).Item("FechaVisita"), Nothing)
            Entidad.HoraVisita = fd.SiEsNulo(dt.Rows(0).Item("HoraVisita"), Nothing)
            Entidad.IdOrden = dt.Rows(0).Item("IdOrden")
            Entidad.IdVendedor = dt.Rows(0).Item("IdVendedor")
            Entidad.IdGestionProgramada = dt.Rows(0).Item("IdGestionProgramada")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.IdCotizacion = dt.Rows(0).Item("IdCotizacion")
        End If
        Return Entidad
    End Function

    Public Sub fac_GestionesDeleteByPK _
            (ByVal IdGestion As System.Int32 _
            )

        db.ExecuteNonQuery("fac_GestionesDeleteByPK", _
            IdGestion _
            )
    End Sub

    Public Sub fac_GestionesDeleteByPK _
            (ByVal IdGestion As System.Int32 _
            , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_GestionesDeleteByPK", _
            IdGestion _
            )
    End Sub

    Public Sub fac_GestionesInsert _
    (ByVal entidad As fac_Gestiones)

        db.ExecuteNonQuery("fac_GestionesInsert", _
            entidad.IdGestion _
            , entidad.IdCliente _
            , entidad.NombreCliente _
            , entidad.Motivo _
            , entidad.Fecha _
            , entidad.HoraLlamada _
            , entidad.ComentarioGeneral _
            , entidad.ComentarioCliente _
            , entidad.ProximaGestion _
            , entidad.FechaVisita _
            , entidad.HoraVisita _
            , entidad.IdOrden _
            , entidad.IdVendedor _
            , entidad.IdGestionProgramada _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            , entidad.IdCotizacion _
            )
    End Sub

    Public Sub fac_GestionesInsert _
    (ByVal entidad As fac_Gestiones, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_GestionesInsert", _
            entidad.IdGestion _
            , entidad.IdCliente _
            , entidad.NombreCliente _
            , entidad.Motivo _
            , entidad.Fecha _
            , entidad.HoraLlamada _
            , entidad.ComentarioGeneral _
            , entidad.ComentarioCliente _
            , entidad.ProximaGestion _
            , entidad.FechaVisita _
            , entidad.HoraVisita _
            , entidad.IdOrden _
            , entidad.IdVendedor _
            , entidad.IdGestionProgramada _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            , entidad.IdCotizacion _
            )
    End Sub

    Public Sub fac_GestionesUpdate _
    (ByVal entidad As fac_Gestiones)

        db.ExecuteNonQuery("fac_GestionesUpdate", _
            entidad.IdGestion _
            , entidad.IdCliente _
            , entidad.NombreCliente _
            , entidad.Motivo _
            , entidad.Fecha _
            , entidad.HoraLlamada _
            , entidad.ComentarioGeneral _
            , entidad.ComentarioCliente _
            , entidad.ProximaGestion _
            , entidad.FechaVisita _
            , entidad.HoraVisita _
            , entidad.IdOrden _
            , entidad.IdVendedor _
            , entidad.IdGestionProgramada _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            , entidad.IdCotizacion _
            )
    End Sub

    Public Sub fac_GestionesUpdate _
    (ByVal entidad As fac_Gestiones, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "fac_GestionesUpdate", _
            entidad.IdGestion _
            , entidad.IdCliente _
            , entidad.NombreCliente _
            , entidad.Motivo _
            , entidad.Fecha _
            , entidad.HoraLlamada _
            , entidad.ComentarioGeneral _
            , entidad.ComentarioCliente _
            , entidad.ProximaGestion _
            , entidad.FechaVisita _
            , entidad.HoraVisita _
            , entidad.IdOrden _
            , entidad.IdVendedor _
            , entidad.IdGestionProgramada _
            , entidad.CreadoPor _
            , entidad.FechaHoraCreacion _
            , entidad.IdCotizacion _
            )
    End Sub

#End Region
#Region "com_Proveedores"
    Public Function com_ProveedoresSelectAll() As DataTable
        Return db.ExecuteDataSet("com_ProveedoresSelectAll").Tables(0)
    End Function

    Public Function com_ProveedoresSelectByPK _
        (ByVal IdProveedor As System.String
        ) As com_Proveedores

        Dim dt As DataTable
        dt = db.ExecuteDataSet("com_ProveedoresSelectByPK",
        IdProveedor
        ).Tables(0)

        Dim Entidad As New com_Proveedores
        If dt.Rows.Count > 0 Then
            Entidad.IdProveedor = dt.Rows(0).Item("IdProveedor")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Nrc = dt.Rows(0).Item("Nrc")
            Entidad.Nit = dt.Rows(0).Item("Nit")
            Entidad.Giro = dt.Rows(0).Item("Giro")
            Entidad.Direccion = dt.Rows(0).Item("Direccion")
            Entidad.IdDepartamento = dt.Rows(0).Item("IdDepartamento")
            Entidad.IdMunicipio = dt.Rows(0).Item("IdMunicipio")
            Entidad.Nacionalidad = dt.Rows(0).Item("Nacionalidad")
            Entidad.Telefonos = dt.Rows(0).Item("Telefonos")
            Entidad.NumeroFax = dt.Rows(0).Item("NumeroFax")
            Entidad.CorreoElectronico = dt.Rows(0).Item("CorreoElectronico")
            Entidad.Contacto1 = dt.Rows(0).Item("Contacto1")
            Entidad.Contacto2 = dt.Rows(0).Item("Contacto2")
            Entidad.Contacto3 = dt.Rows(0).Item("Contacto3")
            Entidad.IdCuentaContable = dt.Rows(0).Item("IdCuentaContable")
            Entidad.LimiteCredito = dt.Rows(0).Item("LimiteCredito")
            Entidad.DiasCredito = dt.Rows(0).Item("DiasCredito")
            Entidad.Tipo = dt.Rows(0).Item("Tipo")
            Entidad.AplicaRetencion = dt.Rows(0).Item("AplicaRetencion")
            Entidad.IdOrigen = dt.Rows(0).Item("IdOrigen")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
            Entidad.NumCasa = dt.Rows(0).Item("NumCasa")
            Entidad.ApartamentoLocal = dt.Rows(0).Item("ApartamentoLocal")
            Entidad.OtrosDomicilio = dt.Rows(0).Item("OtrosDomicilio")
            Entidad.ColoniaBarrio = dt.Rows(0).Item("ColoniaBarrio")
            Entidad.NumeroExcluido = dt.Rows(0).Item("NumeroExcluido")
            Entidad.CodPais = dt.Rows(0).Item("CodPais")
            Entidad.RepresentanteLegal = dt.Rows(0).Item("RepresentanteLegal")

        End If
        Return Entidad
    End Function

    Public Sub com_ProveedoresDeleteByPK _
        (ByVal IdProveedor As System.String
        )

        db.ExecuteNonQuery("com_ProveedoresDeleteByPK",
        IdProveedor
        )
    End Sub

    Public Sub com_ProveedoresDeleteByPK _
        (ByVal IdProveedor As System.String _
        , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ProveedoresDeleteByPK",
        IdProveedor
        )
    End Sub

    Public Sub com_ProveedoresInsert _
(ByVal entidad As com_Proveedores)

        db.ExecuteNonQuery("com_ProveedoresInsert",
        entidad.IdProveedor _
        , entidad.Nombre _
        , entidad.Nrc _
        , entidad.Nit _
        , entidad.Giro _
        , entidad.Direccion _
        , entidad.IdDepartamento _
        , entidad.IdMunicipio _
        , entidad.Nacionalidad _
        , entidad.Telefonos _
        , entidad.NumeroFax _
        , entidad.CorreoElectronico _
        , entidad.Contacto1 _
        , entidad.Contacto2 _
        , entidad.Contacto3 _
        , entidad.IdCuentaContable _
        , entidad.LimiteCredito _
        , entidad.DiasCredito _
        , entidad.Tipo _
        , entidad.AplicaRetencion _
        , entidad.IdOrigen _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion _
        , entidad.NumCasa _
        , entidad.ApartamentoLocal _
        , entidad.OtrosDomicilio _
        , entidad.ColoniaBarrio _
        , entidad.NumeroExcluido _
        , entidad.CodPais _
        , entidad.RepresentanteLegal
        )
    End Sub

    Public Sub com_ProveedoresInsert _
(ByVal entidad As com_Proveedores, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ProveedoresInsert",
        entidad.IdProveedor _
        , entidad.Nombre _
        , entidad.Nrc _
        , entidad.Nit _
        , entidad.Giro _
        , entidad.Direccion _
        , entidad.IdDepartamento _
        , entidad.IdMunicipio _
        , entidad.Nacionalidad _
        , entidad.Telefonos _
        , entidad.NumeroFax _
        , entidad.CorreoElectronico _
        , entidad.Contacto1 _
        , entidad.Contacto2 _
        , entidad.Contacto3 _
        , entidad.IdCuentaContable _
        , entidad.LimiteCredito _
        , entidad.DiasCredito _
        , entidad.Tipo _
        , entidad.AplicaRetencion _
        , entidad.IdOrigen _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion _
        , entidad.NumCasa _
        , entidad.ApartamentoLocal _
        , entidad.OtrosDomicilio _
        , entidad.ColoniaBarrio _
        , entidad.NumeroExcluido _
        , entidad.CodPais _
        , entidad.RepresentanteLegal
        )
    End Sub

    Public Sub com_ProveedoresUpdate _
(ByVal entidad As com_Proveedores)

        db.ExecuteNonQuery("com_ProveedoresUpdate",
        entidad.IdProveedor _
        , entidad.Nombre _
        , entidad.Nrc _
        , entidad.Nit _
        , entidad.Giro _
        , entidad.Direccion _
        , entidad.IdDepartamento _
        , entidad.IdMunicipio _
        , entidad.Nacionalidad _
        , entidad.Telefonos _
        , entidad.NumeroFax _
        , entidad.CorreoElectronico _
        , entidad.Contacto1 _
        , entidad.Contacto2 _
        , entidad.Contacto3 _
        , entidad.IdCuentaContable _
        , entidad.LimiteCredito _
        , entidad.DiasCredito _
        , entidad.Tipo _
        , entidad.AplicaRetencion _
        , entidad.IdOrigen _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion _
        , entidad.NumCasa _
        , entidad.ApartamentoLocal _
        , entidad.OtrosDomicilio _
        , entidad.ColoniaBarrio _
        , entidad.NumeroExcluido _
        , entidad.CodPais _
        , entidad.RepresentanteLegal
        )
    End Sub

    Public Sub com_ProveedoresUpdate _
(ByVal entidad As com_Proveedores, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ProveedoresUpdate",
        entidad.IdProveedor _
        , entidad.Nombre _
        , entidad.Nrc _
        , entidad.Nit _
        , entidad.Giro _
        , entidad.Direccion _
        , entidad.IdDepartamento _
        , entidad.IdMunicipio _
        , entidad.Nacionalidad _
        , entidad.Telefonos _
        , entidad.NumeroFax _
        , entidad.CorreoElectronico _
        , entidad.Contacto1 _
        , entidad.Contacto2 _
        , entidad.Contacto3 _
        , entidad.IdCuentaContable _
        , entidad.LimiteCredito _
        , entidad.DiasCredito _
        , entidad.Tipo _
        , entidad.AplicaRetencion _
        , entidad.IdOrigen _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion _
        , entidad.NumCasa _
        , entidad.ApartamentoLocal _
        , entidad.OtrosDomicilio _
        , entidad.ColoniaBarrio _
        , entidad.NumeroExcluido _
        , entidad.CodPais _
        , entidad.RepresentanteLegal
        )
    End Sub

#End Region

#Region "com_GastosImportaciones"
    Public Function com_GastosImportacionesSelectAll() As DataTable
        Return db.ExecuteDataSet("com_GastosImportacionesSelectAll").Tables(0)
    End Function

    Public Function com_GastosImportacionesSelectByPK _
      (ByVal IdGasto As System.Int32 _
      ) As com_GastosImportaciones

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_GastosImportacionesSelectByPK", _
         IdGasto _
         ).tables(0)

        Dim Entidad As New com_GastosImportaciones
        If dt.Rows.Count > 0 Then
            entidad.IdGasto = dt.rows(0).item("IdGasto")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.IdCuenta = dt.rows(0).item("IdCuenta")

        End If
        Return Entidad
    End Function

    Public Sub com_GastosImportacionesDeleteByPK _
      (ByVal IdGasto As System.Int32 _
      )

        db.ExecuteNonQuery("com_GastosImportacionesDeleteByPK", _
         IdGasto _
         )
    End Sub

    Public Sub com_GastosImportacionesDeleteByPK _
      (ByVal IdGasto As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_GastosImportacionesDeleteByPK", _
         IdGasto _
         )
    End Sub

    Public Sub com_GastosImportacionesInsert _
    (ByVal entidad As com_GastosImportaciones)

        db.ExecuteNonQuery("com_GastosImportacionesInsert", _
         entidad.IdGasto _
         , entidad.Nombre _
         , entidad.IdCuenta _
         )
    End Sub

    Public Sub com_GastosImportacionesInsert _
    (ByVal entidad As com_GastosImportaciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_GastosImportacionesInsert", _
         entidad.IdGasto _
         , entidad.Nombre _
         , entidad.IdCuenta _
         )
    End Sub

    Public Sub com_GastosImportacionesUpdate _
    (ByVal entidad As com_GastosImportaciones)

        db.ExecuteNonQuery("com_GastosImportacionesUpdate", _
         entidad.IdGasto _
         , entidad.Nombre _
         , entidad.IdCuenta _
         )
    End Sub

    Public Sub com_GastosImportacionesUpdate _
    (ByVal entidad As com_GastosImportaciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_GastosImportacionesUpdate", _
         entidad.IdGasto _
         , entidad.Nombre _
         , entidad.IdCuenta _
         )
    End Sub

#End Region
#Region "com_CajasChica"
    Public Function com_CajasChicaSelectAll() As DataTable
        Return db.ExecuteDataSet("com_CajasChicaSelectAll").Tables(0)
    End Function

    Public Function com_CajasChicaSelectByPK _
      (ByVal IdCaja As System.Int32 _
      ) As com_CajasChica

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_CajasChicaSelectByPK", _
         IdCaja _
         ).tables(0)

        Dim Entidad As New com_CajasChica
        If dt.Rows.Count > 0 Then
            entidad.IdCaja = dt.rows(0).item("IdCaja")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.IdModuloAplica = dt.rows(0).item("IdModuloAplica")
            entidad.ElaboradoPor = dt.rows(0).item("ElaboradoPor")
            Entidad.AutorizadoPor1 = dt.Rows(0).Item("AutorizadoPor1")
            Entidad.AutorizadoPor2 = dt.Rows(0).Item("AutorizadoPor2")
            Entidad.IdCuentaContable = dt.Rows(0).Item("IdCuentaContable")

        End If
        Return Entidad
    End Function

    Public Sub com_CajasChicaDeleteByPK _
      (ByVal IdCaja As System.Int32 _
      )

        db.ExecuteNonQuery("com_CajasChicaDeleteByPK", _
         IdCaja _
         )
    End Sub

    Public Sub com_CajasChicaDeleteByPK _
      (ByVal IdCaja As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_CajasChicaDeleteByPK", _
         IdCaja _
         )
    End Sub

    Public Sub com_CajasChicaInsert _
    (ByVal entidad As com_CajasChica)

        db.ExecuteNonQuery("com_CajasChicaInsert", _
         entidad.IdCaja _
         , entidad.Nombre _
         , entidad.IdModuloAplica _
         , entidad.ElaboradoPor _
         , entidad.AutorizadoPor1 _
         , entidad.AutorizadoPor2 _
         , entidad.IdCuentaContable _
         )
    End Sub

    Public Sub com_CajasChicaInsert _
    (ByVal entidad As com_CajasChica, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_CajasChicaInsert", _
         entidad.IdCaja _
         , entidad.Nombre _
         , entidad.IdModuloAplica _
         , entidad.ElaboradoPor _
         , entidad.AutorizadoPor1 _
         , entidad.AutorizadoPor2 _
         , entidad.IdCuentaContable _
         )
    End Sub

    Public Sub com_CajasChicaUpdate _
    (ByVal entidad As com_CajasChica)

        db.ExecuteNonQuery("com_CajasChicaUpdate", _
         entidad.IdCaja _
         , entidad.Nombre _
         , entidad.IdModuloAplica _
         , entidad.ElaboradoPor _
         , entidad.AutorizadoPor1 _
         , entidad.AutorizadoPor2 _
         , entidad.IdCuentaContable _
         )
    End Sub

    Public Sub com_CajasChicaUpdate _
    (ByVal entidad As com_CajasChica, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_CajasChicaUpdate", _
         entidad.IdCaja _
         , entidad.Nombre _
         , entidad.IdModuloAplica _
         , entidad.ElaboradoPor _
         , entidad.AutorizadoPor1 _
         , entidad.AutorizadoPor2 _
         , entidad.IdCuentaContable _
         )
    End Sub

#End Region
#Region "com_Impuestos"
    Public Function com_ImpuestosSelectAll() As DataTable
        Return db.ExecuteDataSet("com_ImpuestosSelectAll").Tables(0)
    End Function

    Public Function com_ImpuestosSelectByPK _
      (ByVal IdImpuesto As System.Int32 _
      ) As com_Impuestos

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_ImpuestosSelectByPK", _
         IdImpuesto _
         ).tables(0)

        Dim Entidad As New com_Impuestos
        If dt.Rows.Count > 0 Then
            entidad.IdImpuesto = dt.rows(0).item("IdImpuesto")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.IdCuentaContable = dt.rows(0).item("IdCuentaContable")
            entidad.TipoCalculo = dt.rows(0).item("TipoCalculo")
            entidad.Valor = dt.rows(0).item("Valor")

        End If
        Return Entidad
    End Function

    Public Sub com_ImpuestosDeleteByPK _
      (ByVal IdImpuesto As System.Int32 _
      )

        db.ExecuteNonQuery("com_ImpuestosDeleteByPK", _
         IdImpuesto _
         )
    End Sub

    Public Sub com_ImpuestosDeleteByPK _
      (ByVal IdImpuesto As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImpuestosDeleteByPK", _
         IdImpuesto _
         )
    End Sub

    Public Sub com_ImpuestosInsert _
    (ByVal entidad As com_Impuestos)

        db.ExecuteNonQuery("com_ImpuestosInsert", _
         entidad.IdImpuesto _
         , entidad.Nombre _
         , entidad.IdCuentaContable _
         , entidad.TipoCalculo _
         , entidad.Valor _
         )
    End Sub

    Public Sub com_ImpuestosInsert _
    (ByVal entidad As com_Impuestos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImpuestosInsert", _
         entidad.IdImpuesto _
         , entidad.Nombre _
         , entidad.IdCuentaContable _
         , entidad.TipoCalculo _
         , entidad.Valor _
         )
    End Sub

    Public Sub com_ImpuestosUpdate _
    (ByVal entidad As com_Impuestos)

        db.ExecuteNonQuery("com_ImpuestosUpdate", _
         entidad.IdImpuesto _
         , entidad.Nombre _
         , entidad.IdCuentaContable _
         , entidad.TipoCalculo _
         , entidad.Valor _
         )
    End Sub

    Public Sub com_ImpuestosUpdate _
    (ByVal entidad As com_Impuestos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImpuestosUpdate", _
         entidad.IdImpuesto _
         , entidad.Nombre _
         , entidad.IdCuentaContable _
         , entidad.TipoCalculo _
         , entidad.Valor _
         )
    End Sub

#End Region
#Region "com_ComprasDetalleImpuestos"
    Public Function com_ComprasDetalleImpuestosSelectAll() As DataTable
        Return db.ExecuteDataSet("com_ComprasDetalleImpuestosSelectAll").Tables(0)
    End Function

    Public Function com_ComprasDetalleImpuestosSelectByPK _
      (ByVal IdCompra As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_ComprasDetalleImpuestos

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_ComprasDetalleImpuestosSelectByPK", _
         IdCompra _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New com_ComprasDetalleImpuestos
        If dt.Rows.Count > 0 Then
            entidad.IdCompra = dt.rows(0).item("IdCompra")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdImpuesto = dt.rows(0).item("IdImpuesto")
            entidad.Valor = dt.rows(0).item("Valor")

        End If
        Return Entidad
    End Function

    Public Sub com_ComprasDetalleImpuestosDeleteByPK _
      (ByVal IdCompra As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("com_ComprasDetalleImpuestosDeleteByPK", _
         IdCompra _
         , IdDetalle _
         )
    End Sub

    Public Sub com_ComprasDetalleImpuestosDeleteByPK _
      (ByVal IdCompra As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ComprasDetalleImpuestosDeleteByPK", _
         IdCompra _
         , IdDetalle _
         )
    End Sub

    Public Sub com_ComprasDetalleImpuestosInsert _
    (ByVal entidad As com_ComprasDetalleImpuestos)

        db.ExecuteNonQuery("com_ComprasDetalleImpuestosInsert", _
         entidad.IdCompra _
         , entidad.IdDetalle _
         , entidad.IdImpuesto _
         , entidad.Valor _
         )
    End Sub

    Public Sub com_ComprasDetalleImpuestosInsert _
    (ByVal entidad As com_ComprasDetalleImpuestos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ComprasDetalleImpuestosInsert", _
         entidad.IdCompra _
         , entidad.IdDetalle _
         , entidad.IdImpuesto _
         , entidad.Valor _
         )
    End Sub

    Public Sub com_ComprasDetalleImpuestosUpdate _
    (ByVal entidad As com_ComprasDetalleImpuestos)

        db.ExecuteNonQuery("com_ComprasDetalleImpuestosUpdate", _
         entidad.IdCompra _
         , entidad.IdDetalle _
         , entidad.IdImpuesto _
         , entidad.Valor _
         )
    End Sub

    Public Sub com_ComprasDetalleImpuestosUpdate _
    (ByVal entidad As com_ComprasDetalleImpuestos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ComprasDetalleImpuestosUpdate", _
         entidad.IdCompra _
         , entidad.IdDetalle _
         , entidad.IdImpuesto _
         , entidad.Valor _
         )
    End Sub

#End Region

#Region "com_Requisiciones"
    Public Function com_RequisicionesSelectAll() As DataTable
        Return db.ExecuteDataSet("com_RequisicionesSelectAll").Tables(0)
    End Function

    Public Function com_RequisicionesSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As com_Requisiciones

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_RequisicionesSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New com_Requisiciones
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub com_RequisicionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("com_RequisicionesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub com_RequisicionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_RequisicionesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub com_RequisicionesInsert _
    (ByVal entidad As com_Requisiciones)

        db.ExecuteNonQuery("com_RequisicionesInsert", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.IdSucursal _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub com_RequisicionesInsert _
    (ByVal entidad As com_Requisiciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_RequisicionesInsert", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.IdSucursal _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub com_RequisicionesUpdate _
    (ByVal entidad As com_Requisiciones)

        db.ExecuteNonQuery("com_RequisicionesUpdate", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.IdSucursal _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub com_RequisicionesUpdate _
    (ByVal entidad As com_Requisiciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_RequisicionesUpdate", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.IdSucursal _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "com_RequisicionesDetalle"
    Public Function com_RequisicionesDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("com_RequisicionesDetalleSelectAll").Tables(0)
    End Function

    Public Function com_RequisicionesDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_RequisicionesDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_RequisicionesDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New com_RequisicionesDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.CantidadAutorizada = dt.rows(0).item("CantidadAutorizada")
            entidad.Consolidado = dt.rows(0).item("Consolidado")
            entidad.UsuarioConsolido = dt.rows(0).item("UsuarioConsolido")
            Entidad.FechaConsolido = fd.SiEsNulo(dt.Rows(0).Item("FechaConsolido"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub com_RequisicionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("com_RequisicionesDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_RequisicionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_RequisicionesDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_RequisicionesDetalleInsert _
    (ByVal entidad As com_RequisicionesDetalle)

        db.ExecuteNonQuery("com_RequisicionesDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.CantidadAutorizada _
         , entidad.Consolidado _
         , entidad.UsuarioConsolido _
         , entidad.FechaConsolido _
         )
    End Sub

    Public Sub com_RequisicionesDetalleInsert _
    (ByVal entidad As com_RequisicionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_RequisicionesDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.CantidadAutorizada _
         , entidad.Consolidado _
         , entidad.UsuarioConsolido _
         , entidad.FechaConsolido _
         )
    End Sub

    Public Sub com_RequisicionesDetalleUpdate _
    (ByVal entidad As com_RequisicionesDetalle)

        db.ExecuteNonQuery("com_RequisicionesDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.CantidadAutorizada _
         , entidad.Consolidado _
         , entidad.UsuarioConsolido _
         , entidad.FechaConsolido _
         )
    End Sub

    Public Sub com_RequisicionesDetalleUpdate _
    (ByVal entidad As com_RequisicionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_RequisicionesDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.CantidadAutorizada _
         , entidad.Consolidado _
         , entidad.UsuarioConsolido _
         , entidad.FechaConsolido _
         )
    End Sub

#End Region

#Region "com_OrdenesCompra"
    Public Function com_OrdenesCompraSelectAll() As DataTable
        Return db.ExecuteDataSet("com_OrdenesCompraSelectAll").Tables(0)
    End Function

    Public Function com_OrdenesCompraSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As com_OrdenesCompra

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_OrdenesCompraSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New com_OrdenesCompra
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdProveedor = dt.rows(0).item("IdProveedor")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Direccion = dt.rows(0).item("Direccion")
            entidad.Giro = dt.rows(0).item("Giro")
            entidad.Nit = dt.rows(0).item("Nit")
            entidad.Nrc = dt.rows(0).item("Nrc")
            entidad.IdBodega = dt.rows(0).item("IdBodega")
            entidad.IdFormaPago = dt.rows(0).item("IdFormaPago")
            entidad.DiasCredito = dt.rows(0).item("DiasCredito")
            entidad.IdOrigen = dt.rows(0).item("IdOrigen")
            entidad.IdTipoCompra = dt.rows(0).item("IdTipoCompra")
            entidad.EnviarFacturaA = dt.rows(0).item("EnviarFacturaA")
            Entidad.DireccionEnvioFactura = dt.Rows(0).Item("DireccionEnvioFactura")
            Entidad.EmailEnvioFactura = dt.Rows(0).Item("EmailEnvioFactura")
            entidad.SolicitadoPor = dt.rows(0).item("SolicitadoPor")
            entidad.LugarEntrega = dt.rows(0).item("LugarEntrega")
            entidad.DireccionEntrega = dt.rows(0).item("DireccionEntrega")
            Entidad.Iva = dt.Rows(0).Item("Iva")
            Entidad.TotalImpuesto1 = dt.Rows(0).Item("TotalImpuesto1")
            Entidad.TotalImpuesto2 = dt.Rows(0).Item("TotalImpuesto2")
            entidad.Total = dt.rows(0).item("Total")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.Aprobada = dt.Rows(0).Item("Aprobada")
            Entidad.AprobadaPor = dt.Rows(0).Item("AprobadaPor")
            Entidad.ComentarioAprobacion = dt.Rows(0).Item("ComentarioAprobacion")
            Entidad.FacturaAduana = dt.Rows(0).Item("FacturaAduana")
            Entidad.CartaAduana = dt.Rows(0).Item("CartaAduana")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub com_OrdenesCompraDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("com_OrdenesCompraDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub com_OrdenesCompraDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_OrdenesCompraDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub com_OrdenesCompraInsert _
    (ByVal entidad As com_OrdenesCompra)

        db.ExecuteNonQuery("com_OrdenesCompraInsert", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.Giro _
         , entidad.Nit _
         , entidad.Nrc _
         , entidad.IdBodega _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.IdOrigen _
         , entidad.IdTipoCompra _
         , entidad.EnviarFacturaA _
         , entidad.DireccionEnvioFactura _
          , entidad.EmailEnvioFactura _
         , entidad.SolicitadoPor _
         , entidad.LugarEntrega _
         , entidad.DireccionEntrega _
         , entidad.Iva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.Total _
         , entidad.IdSucursal _
           , entidad.Aprobada _
  , entidad.AprobadaPor _
  , entidad.ComentarioAprobacion _
  , entidad.FacturaAduana _
  , entidad.CartaAduana _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub com_OrdenesCompraInsert _
    (ByVal entidad As com_OrdenesCompra, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_OrdenesCompraInsert", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.Giro _
         , entidad.Nit _
         , entidad.Nrc _
         , entidad.IdBodega _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.IdOrigen _
         , entidad.IdTipoCompra _
         , entidad.EnviarFacturaA _
         , entidad.DireccionEnvioFactura _
          , entidad.EmailEnvioFactura _
         , entidad.SolicitadoPor _
         , entidad.LugarEntrega _
         , entidad.DireccionEntrega _
         , entidad.Iva _
                  , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.Total _
         , entidad.IdSucursal _
           , entidad.Aprobada _
  , entidad.AprobadaPor _
  , entidad.ComentarioAprobacion _
    , entidad.FacturaAduana _
  , entidad.CartaAduana _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub com_OrdenesCompraUpdate _
    (ByVal entidad As com_OrdenesCompra)

        db.ExecuteNonQuery("com_OrdenesCompraUpdate", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.Giro _
         , entidad.Nit _
         , entidad.Nrc _
         , entidad.IdBodega _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.IdOrigen _
         , entidad.IdTipoCompra _
         , entidad.EnviarFacturaA _
         , entidad.DireccionEnvioFactura _
          , entidad.EmailEnvioFactura _
         , entidad.SolicitadoPor _
         , entidad.LugarEntrega _
         , entidad.DireccionEntrega _
         , entidad.Iva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.Total _
         , entidad.IdSucursal _
           , entidad.Aprobada _
  , entidad.AprobadaPor _
  , entidad.ComentarioAprobacion _
    , entidad.FacturaAduana _
  , entidad.CartaAduana _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub com_OrdenesCompraUpdate _
    (ByVal entidad As com_OrdenesCompra, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_OrdenesCompraUpdate", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.Giro _
         , entidad.Nit _
         , entidad.Nrc _
         , entidad.IdBodega _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.IdOrigen _
         , entidad.IdTipoCompra _
         , entidad.EnviarFacturaA _
         , entidad.DireccionEnvioFactura _
          , entidad.EmailEnvioFactura _
         , entidad.SolicitadoPor _
         , entidad.LugarEntrega _
         , entidad.DireccionEntrega _
         , entidad.Iva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.Total _
         , entidad.IdSucursal _
           , entidad.Aprobada _
  , entidad.AprobadaPor _
  , entidad.ComentarioAprobacion _
    , entidad.FacturaAduana _
  , entidad.CartaAduana _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region

#Region "com_OrdenesCompraDetalle"
    Public Function com_OrdenesCompraDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("com_OrdenesCompraDetalleSelectAll").Tables(0)
    End Function

    Public Function com_OrdenesCompraDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_OrdenesCompraDetalle

        Dim dt As DataTable = db.ExecuteDataSet("com_OrdenesCompraDetalleSelectByPK", IdComprobante, IdDetalle).Tables(0)

        Dim Entidad As New com_OrdenesCompraDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            Entidad.PrecioReferencia = dt.Rows(0).Item("PrecioReferencia")
            Entidad.PorcDescto = dt.Rows(0).Item("PorcDescto")
            Entidad.ValorDescto = dt.Rows(0).Item("ValorDescto")
            Entidad.PrecioUnitario = dt.Rows(0).Item("PrecioUnitario")
            Entidad.ValorExento = dt.Rows(0).Item("ValorExento")
            entidad.ValorAfecto = dt.rows(0).item("ValorAfecto")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub com_OrdenesCompraDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("com_OrdenesCompraDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_OrdenesCompraDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_OrdenesCompraDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_OrdenesCompraDetalleInsert(ByVal entidad As com_OrdenesCompraDetalle)

        db.ExecuteNonQuery("com_OrdenesCompraDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioReferencia _
         , entidad.PorcDescto _
         , entidad.ValorDescto _
         , entidad.ValorExento _
         , entidad.PrecioUnitario _
         , entidad.ValorAfecto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub com_OrdenesCompraDetalleInsert _
    (ByVal entidad As com_OrdenesCompraDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_OrdenesCompraDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioReferencia _
         , entidad.PorcDescto _
         , entidad.ValorDescto _
         , entidad.PrecioUnitario _
         , entidad.ValorExento _
         , entidad.ValorAfecto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "com_Compras"
    Public Function com_ComprasSelectAll() As DataTable
        Return db.ExecuteDataSet("com_ComprasSelectAll").Tables(0)
    End Function

    Public Function com_ComprasSelectByPK _
        (ByVal IdComprobante As System.Int32
        ) As com_Compras

        Dim dt As DataTable
        dt = db.ExecuteDataSet("com_ComprasSelectByPK",
        IdComprobante
        ).Tables(0)

        Dim Entidad As New com_Compras
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdTipoComprobante = dt.Rows(0).Item("IdTipoComprobante")
            Entidad.Correlativo = dt.Rows(0).Item("Correlativo")
            Entidad.Serie = dt.Rows(0).Item("Serie")
            Entidad.Numero = dt.Rows(0).Item("Numero")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.FechaContable = dt.Rows(0).Item("FechaContable")
            Entidad.IdProveedor = dt.Rows(0).Item("IdProveedor")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Nrc = dt.Rows(0).Item("Nrc")
            Entidad.Nit = dt.Rows(0).Item("Nit")
            Entidad.Direccion = dt.Rows(0).Item("Direccion")
            Entidad.Giro = dt.Rows(0).Item("Giro")
            Entidad.IdFormaPago = dt.Rows(0).Item("IdFormaPago")
            Entidad.NumOrdenCompra = dt.Rows(0).Item("NumOrdenCompra")
            Entidad.DiasCredito = dt.Rows(0).Item("DiasCredito")
            Entidad.FechaVencimiento = dt.Rows(0).Item("FechaVencimiento")
            Entidad.IdTipoCompra = dt.Rows(0).Item("IdTipoCompra")
            Entidad.IdOrigenCompra = dt.Rows(0).Item("IdOrigenCompra")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.IdBodega = dt.Rows(0).Item("IdBodega")
            Entidad.IdCaja = dt.Rows(0).Item("IdCaja")
            Entidad.CompraExcluido = dt.Rows(0).Item("CompraExcluido")
            Entidad.NumDocExcluido = dt.Rows(0).Item("NumDocExcluido")
            Entidad.ExcluirLibro = dt.Rows(0).Item("ExcluirLibro")
            Entidad.TotalExento = dt.Rows(0).Item("TotalExento")
            Entidad.TotalAfecto = dt.Rows(0).Item("TotalAfecto")
            Entidad.TotalIva = dt.Rows(0).Item("TotalIva")
            Entidad.TipoImpuestoAdicional = dt.Rows(0).Item("TipoImpuestoAdicional")
            Entidad.TotalImpuesto1 = dt.Rows(0).Item("TotalImpuesto1")
            Entidad.TotalImpuesto2 = dt.Rows(0).Item("TotalImpuesto2")
            Entidad.TotalComprobante = dt.Rows(0).Item("TotalComprobante")
            Entidad.AplicadaInventario = dt.Rows(0).Item("AplicadaInventario")
            Entidad.Contabilizada = dt.Rows(0).Item("Contabilizada")
            Entidad.AfectaCosto = dt.Rows(0).Item("AfectaCosto")
            Entidad.IdCheque = dt.Rows(0).Item("IdCheque")
            Entidad.IdPartida = dt.Rows(0).Item("IdPartida")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub com_ComprasDeleteByPK _
        (ByVal IdComprobante As System.Int32
        )

        db.ExecuteNonQuery("com_ComprasDeleteByPK",
        IdComprobante
        )
    End Sub

    Public Sub com_ComprasDeleteByPK _
        (ByVal IdComprobante As System.Int32 _
        , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ComprasDeleteByPK",
        IdComprobante
        )
    End Sub

    Public Sub com_ComprasInsert _
(ByVal entidad As com_Compras)

        db.ExecuteNonQuery("com_ComprasInsert",
        entidad.IdComprobante _
        , entidad.IdTipoComprobante _
        , entidad.Correlativo _
        , entidad.Serie _
        , entidad.Numero _
        , entidad.Fecha _
        , entidad.FechaContable _
        , entidad.IdProveedor _
        , entidad.Nombre _
        , entidad.Nrc _
        , entidad.Nit _
        , entidad.Direccion _
        , entidad.Giro _
        , entidad.IdFormaPago _
        , entidad.NumOrdenCompra _
        , entidad.DiasCredito _
        , entidad.FechaVencimiento _
        , entidad.IdTipoCompra _
        , entidad.IdOrigenCompra _
        , entidad.IdSucursal _
        , entidad.IdBodega _
        , entidad.IdCaja _
        , entidad.CompraExcluido _
        , entidad.NumDocExcluido _
        , entidad.ExcluirLibro _
        , entidad.TotalExento _
        , entidad.TotalAfecto _
        , entidad.TotalIva _
        , entidad.TipoImpuestoAdicional _
        , entidad.TotalImpuesto1 _
        , entidad.TotalImpuesto2 _
        , entidad.TotalComprobante _
        , entidad.AplicadaInventario _
        , entidad.Contabilizada _
        , entidad.AfectaCosto _
        , entidad.IdCheque _
        , entidad.IdPartida _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion
        )
    End Sub

    Public Sub com_ComprasInsert _
(ByVal entidad As com_Compras, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ComprasInsert",
        entidad.IdComprobante _
        , entidad.IdTipoComprobante _
        , entidad.Correlativo _
        , entidad.Serie _
        , entidad.Numero _
        , entidad.Fecha _
        , entidad.FechaContable _
        , entidad.IdProveedor _
        , entidad.Nombre _
        , entidad.Nrc _
        , entidad.Nit _
        , entidad.Direccion _
        , entidad.Giro _
        , entidad.IdFormaPago _
        , entidad.NumOrdenCompra _
        , entidad.DiasCredito _
        , entidad.FechaVencimiento _
        , entidad.IdTipoCompra _
        , entidad.IdOrigenCompra _
        , entidad.IdSucursal _
        , entidad.IdBodega _
        , entidad.IdCaja _
        , entidad.CompraExcluido _
        , entidad.NumDocExcluido _
        , entidad.ExcluirLibro _
        , entidad.TotalExento _
        , entidad.TotalAfecto _
        , entidad.TotalIva _
        , entidad.TipoImpuestoAdicional _
        , entidad.TotalImpuesto1 _
        , entidad.TotalImpuesto2 _
        , entidad.TotalComprobante _
        , entidad.AplicadaInventario _
        , entidad.Contabilizada _
        , entidad.AfectaCosto _
        , entidad.IdCheque _
        , entidad.IdPartida _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion
        )
    End Sub

    Public Sub com_ComprasUpdate _
(ByVal entidad As com_Compras)

        db.ExecuteNonQuery("com_ComprasUpdate",
        entidad.IdComprobante _
        , entidad.IdTipoComprobante _
        , entidad.Correlativo _
        , entidad.Serie _
        , entidad.Numero _
        , entidad.Fecha _
        , entidad.FechaContable _
        , entidad.IdProveedor _
        , entidad.Nombre _
        , entidad.Nrc _
        , entidad.Nit _
        , entidad.Direccion _
        , entidad.Giro _
        , entidad.IdFormaPago _
        , entidad.NumOrdenCompra _
        , entidad.DiasCredito _
        , entidad.FechaVencimiento _
        , entidad.IdTipoCompra _
        , entidad.IdOrigenCompra _
        , entidad.IdSucursal _
        , entidad.IdBodega _
        , entidad.IdCaja _
        , entidad.CompraExcluido _
        , entidad.NumDocExcluido _
        , entidad.ExcluirLibro _
        , entidad.TotalExento _
        , entidad.TotalAfecto _
        , entidad.TotalIva _
        , entidad.TipoImpuestoAdicional _
        , entidad.TotalImpuesto1 _
        , entidad.TotalImpuesto2 _
        , entidad.TotalComprobante _
        , entidad.AplicadaInventario _
        , entidad.Contabilizada _
        , entidad.AfectaCosto _
        , entidad.IdCheque _
        , entidad.IdPartida _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion
        )
    End Sub

    Public Sub com_ComprasUpdate _
(ByVal entidad As com_Compras, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ComprasUpdate",
        entidad.IdComprobante _
        , entidad.IdTipoComprobante _
        , entidad.Correlativo _
        , entidad.Serie _
        , entidad.Numero _
        , entidad.Fecha _
        , entidad.FechaContable _
        , entidad.IdProveedor _
        , entidad.Nombre _
        , entidad.Nrc _
        , entidad.Nit _
        , entidad.Direccion _
        , entidad.Giro _
        , entidad.IdFormaPago _
        , entidad.NumOrdenCompra _
        , entidad.DiasCredito _
        , entidad.FechaVencimiento _
        , entidad.IdTipoCompra _
        , entidad.IdOrigenCompra _
        , entidad.IdSucursal _
        , entidad.IdBodega _
        , entidad.IdCaja _
        , entidad.CompraExcluido _
        , entidad.NumDocExcluido _
        , entidad.ExcluirLibro _
        , entidad.TotalExento _
        , entidad.TotalAfecto _
        , entidad.TotalIva _
        , entidad.TipoImpuestoAdicional _
        , entidad.TotalImpuesto1 _
        , entidad.TotalImpuesto2 _
        , entidad.TotalComprobante _
        , entidad.AplicadaInventario _
        , entidad.Contabilizada _
        , entidad.AfectaCosto _
        , entidad.IdCheque _
        , entidad.IdPartida _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion
        )
    End Sub

#End Region

#Region "com_ComprasDetalle"
    Public Function com_ComprasDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("com_ComprasDetalleSelectAll").Tables(0)
    End Function

    Public Function com_ComprasDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_ComprasDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_ComprasDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New com_ComprasDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            Entidad.Cantidad = dt.Rows(0).Item("Cantidad")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            Entidad.PrecioReferencia = dt.Rows(0).Item("PrecioReferencia")
            entidad.PorcDescto = dt.rows(0).item("PorcDescto")
            Entidad.ValorDescto = dt.Rows(0).Item("ValorDescto")
            Entidad.PrecioUnitario = dt.Rows(0).Item("PrecioUnitario")
            entidad.ValorExento = dt.rows(0).item("ValorExento")
            Entidad.ValorAfecto = dt.Rows(0).Item("ValorAfecto")
            Entidad.IdCentro = dt.Rows(0).Item("IdCentro")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub com_ComprasDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("com_ComprasDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_ComprasDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ComprasDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_ComprasDetalleInsert(ByVal entidad As com_ComprasDetalle)

        db.ExecuteNonQuery("com_ComprasDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioReferencia _
         , entidad.PorcDescto _
         , entidad.ValorDescto _
        , entidad.PrecioUnitario _
         , entidad.ValorExento _
         , entidad.ValorAfecto _
                  , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub com_ComprasDetalleInsert(ByVal entidad As com_ComprasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ComprasDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.TipoImpuesto _
         , entidad.PrecioReferencia _
         , entidad.PorcDescto _
         , entidad.ValorDescto _
         , entidad.PrecioUnitario _
         , entidad.ValorExento _
         , entidad.ValorAfecto _
         , entidad.IdCuenta _
                  , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "com_Importaciones"
    Public Function com_ImportacionesSelectAll() As DataTable
        Return db.ExecuteDataSet("com_ImportacionesSelectAll").Tables(0)
    End Function

    Public Function com_ImportacionesSelectByPK(ByVal IdComprobante As Int32) As com_Importaciones

        Dim dt As DataTable = db.ExecuteDataSet("com_ImportacionesSelectByPK", IdComprobante).Tables(0)

        Dim Entidad As New com_Importaciones
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.NumeroFactura = dt.Rows(0).Item("NumeroFactura")
            Entidad.NumeroPoliza = dt.Rows(0).Item("NumeroPoliza")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.FechaContable = dt.Rows(0).Item("FechaContable")
            Entidad.IdProveedor = dt.Rows(0).Item("IdProveedor")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Direccion = dt.Rows(0).Item("Direccion")
            Entidad.DaiCalculado = dt.Rows(0).Item("DaiCalculado")
            Entidad.ValorDai = dt.Rows(0).Item("ValorDai")
            Entidad.ValorSeguros = dt.Rows(0).Item("ValorSeguros")
            Entidad.ValorTransporte = dt.Rows(0).Item("ValorTransporte")
            Entidad.ValorGastosImportacion = dt.Rows(0).Item("ValorGastosImportacion")
            Entidad.ValorGastosExternos = dt.Rows(0).Item("ValorGastosExternos")
            Entidad.ValorGastosInternos = dt.Rows(0).Item("ValorGastosInternos")
            Entidad.ValorOtrosGastos = dt.Rows(0).Item("ValorOtrosGastos")
            Entidad.ValorSujetoDeclaracion = dt.Rows(0).Item("ValorSujetoDeclaracion")
            Entidad.ValorIva = dt.Rows(0).Item("ValorIva")
            Entidad.ValorExento = dt.Rows(0).Item("ValorExento")
            Entidad.ValorImpuesto1 = dt.Rows(0).Item("ValorImpuesto1")
            Entidad.ValorImpuesto2 = dt.Rows(0).Item("ValorImpuesto2")
            Entidad.PorcDescuento = dt.Rows(0).Item("PorcDescuento")
            Entidad.TotalPoliza = dt.Rows(0).Item("TotalPoliza")
            Entidad.TotalPorPagar = dt.Rows(0).Item("TotalPorPagar")
            Entidad.IdBodega = dt.Rows(0).Item("IdBodega")
            Entidad.TipoImportacion = dt.Rows(0).Item("TipoImportacion")
            Entidad.DiasCredito = dt.Rows(0).Item("DiasCredito")
            Entidad.AplicadaInventario = fd.SiEsNulo(dt.Rows(0).Item("AplicadaInventario"), Nothing)
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            'Entidad.IdPartida = dt.Rows(0).Item("IdPartida")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
            Entidad.IdAduana = fd.SiEsNulo(dt.Rows(0).Item("IdAduana"), 1)
            Entidad.TipoDocumento = fd.SiEsNulo(dt.Rows(0).Item("TipoDocumento"), 1)
        End If
        Return Entidad
    End Function

    Public Sub com_ImportacionesDeleteByPK(ByVal IdComprobante As Int32)

        db.ExecuteNonQuery("com_ImportacionesDeleteByPK", IdComprobante)
    End Sub

    Public Sub com_ImportacionesDeleteByPK(ByVal IdComprobante As Int32, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesDeleteByPK", IdComprobante)
    End Sub

    Public Sub com_ImportacionesUpdate(ByVal entidad As com_Importaciones)

        db.ExecuteNonQuery("com_ImportacionesUpdate",
         entidad.IdComprobante _
         , entidad.NumeroFactura _
         , entidad.NumeroPoliza _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.IdProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.DaiCalculado _
         , entidad.ValorDai _
         , entidad.ValorSeguros _
         , entidad.ValorTransporte _
         , entidad.ValorGastosImportacion _
         , entidad.ValorGastosExternos _
         , entidad.ValorGastosInternos _
         , entidad.ValorOtrosGastos _
         , entidad.ValorSujetoDeclaracion _
         , entidad.ValorIva _
         , entidad.ValorExento _
         , entidad.ValorImpuesto1 _
         , entidad.ValorImpuesto2 _
         , entidad.PorcDescuento _
         , entidad.TotalPoliza _
         , entidad.TotalPorPagar _
         , entidad.IdBodega _
         , entidad.TipoImportacion _
         , entidad.DiasCredito _
         , entidad.AplicadaInventario _
         , entidad.IdSucursal _
         , entidad.IdPartida _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         , entidad.IdAduana _
         , entidad.TipoDocumento
         )
    End Sub

    Public Sub com_ImportacionesInsert(ByVal entidad As com_Importaciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesInsert",
         entidad.IdComprobante _
         , entidad.NumeroFactura _
         , entidad.NumeroPoliza _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.IdProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.DaiCalculado _
         , entidad.ValorDai _
         , entidad.ValorSeguros _
         , entidad.ValorTransporte _
         , entidad.ValorGastosImportacion _
         , entidad.ValorGastosExternos _
         , entidad.ValorGastosInternos _
         , entidad.ValorOtrosGastos _
         , entidad.ValorSujetoDeclaracion _
         , entidad.ValorIva _
        , entidad.ValorExento _
        , entidad.ValorImpuesto1 _
         , entidad.ValorImpuesto2 _
         , entidad.PorcDescuento _
         , entidad.TotalPoliza _
         , entidad.TotalPorPagar _
         , entidad.IdBodega _
         , entidad.TipoImportacion _
         , entidad.DiasCredito _
         , entidad.AplicadaInventario _
         , entidad.IdSucursal _
         , entidad.IdPartida _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         , entidad.IdAduana _
         , entidad.TipoDocumento
         )
    End Sub

    Public Sub com_ImportacionesUpdate(ByVal entidad As com_Importaciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesUpdate",
         entidad.IdComprobante _
         , entidad.NumeroFactura _
         , entidad.NumeroPoliza _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.IdProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.DaiCalculado _
         , entidad.ValorDai _
         , entidad.ValorSeguros _
         , entidad.ValorTransporte _
         , entidad.ValorGastosImportacion _
         , entidad.ValorGastosExternos _
         , entidad.ValorGastosInternos _
         , entidad.ValorOtrosGastos _
         , entidad.ValorSujetoDeclaracion _
         , entidad.ValorIva _
         , entidad.ValorExento _
         , entidad.ValorImpuesto1 _
         , entidad.ValorImpuesto2 _
         , entidad.PorcDescuento _
         , entidad.TotalPoliza _
         , entidad.TotalPorPagar _
         , entidad.IdBodega _
         , entidad.TipoImportacion _
         , entidad.DiasCredito _
         , entidad.AplicadaInventario _
         , entidad.IdSucursal _
         , entidad.IdPartida _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         , entidad.IdAduana _
         , entidad.TipoDocumento
         )
    End Sub

#End Region

#Region "com_Liquidacion"
    Public Function com_LiquidacionSelectAll() As DataTable
        Return db.ExecuteDataSet("com_LiquidacionSelectAll").Tables(0)
    End Function

    Public Function com_LiquidacionSelectByPK _
        (ByVal IdComprobante As System.Int32
        ) As com_Liquidacion

        Dim dt As DataTable
        dt = db.ExecuteDataSet("com_LiquidacionSelectByPK",
        IdComprobante
        ).Tables(0)

        Dim Entidad As New com_Liquidacion
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.Serie = dt.Rows(0).Item("Serie")
            Entidad.Numero = dt.Rows(0).Item("Numero")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.FechaContable = dt.Rows(0).Item("FechaContable")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.IdProveedor = dt.Rows(0).Item("IdProveedor")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Nrc = dt.Rows(0).Item("Nrc")
            Entidad.Nit = dt.Rows(0).Item("Nit")
            Entidad.Direccion = dt.Rows(0).Item("Direccion")
            Entidad.TotalIva = dt.Rows(0).Item("TotalIva")
            Entidad.TotalComprobante = dt.Rows(0).Item("TotalComprobante")
            Entidad.Comision = dt.Rows(0).Item("Comision")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
            Entidad.IdCuentaBancaria = dt.Rows(0).Item("IdCuentaBancaria")
            Entidad.TotalExento = dt.Rows(0).Item("TotalExento")
            Entidad.NumeroCF = dt.Rows(0).Item("NumeroCF")
            Entidad.SerieCF = dt.Rows(0).Item("SerieCF")
            Entidad.Resolucion = dt.Rows(0).Item("Resolucion")
            Entidad.NumControlInterno = dt.Rows(0).Item("NumControlInterno")
            Entidad.ClaseDocumento = dt.Rows(0).Item("ClaseDocumento")
            Entidad.ResolucionCF = dt.Rows(0).Item("ResolucionCF")
            Entidad.NumControlInternoCF = dt.Rows(0).Item("NumControlInternoCF")
            Entidad.ClaseDocumentoCF = dt.Rows(0).Item("ClaseDocumentoCF")

        End If
        Return Entidad
    End Function

    Public Sub com_LiquidacionDeleteByPK _
        (ByVal IdComprobante As System.Int32
        )

        db.ExecuteNonQuery("com_LiquidacionDeleteByPK",
        IdComprobante
        )
    End Sub

    Public Sub com_LiquidacionDeleteByPK _
        (ByVal IdComprobante As System.Int32 _
        , Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_LiquidacionDeleteByPK",
        IdComprobante
        )
    End Sub

    Public Sub com_LiquidacionInsert _
(ByVal entidad As com_Liquidacion)

        db.ExecuteNonQuery("com_LiquidacionInsert",
        entidad.IdComprobante _
        , entidad.Serie _
        , entidad.Numero _
        , entidad.Fecha _
        , entidad.FechaContable _
        , entidad.IdSucursal _
        , entidad.IdProveedor _
        , entidad.Nombre _
        , entidad.Nrc _
        , entidad.Nit _
        , entidad.Direccion _
        , entidad.TotalIva _
        , entidad.TotalComprobante _
        , entidad.Comision _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion _
        , entidad.IdCuentaBancaria _
        , entidad.TotalExento _
        , entidad.NumeroCF _
        , entidad.SerieCF _
        , entidad.Resolucion _
        , entidad.NumControlInterno _
        , entidad.ClaseDocumento _
        , entidad.ResolucionCF _
        , entidad.NumControlInternoCF _
        , entidad.ClaseDocumentoCF
        )
    End Sub

    Public Sub com_LiquidacionInsert _
(ByVal entidad As com_Liquidacion, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_LiquidacionInsert",
        entidad.IdComprobante _
        , entidad.Serie _
        , entidad.Numero _
        , entidad.Fecha _
        , entidad.FechaContable _
        , entidad.IdSucursal _
        , entidad.IdProveedor _
        , entidad.Nombre _
        , entidad.Nrc _
        , entidad.Nit _
        , entidad.Direccion _
        , entidad.TotalIva _
        , entidad.TotalComprobante _
        , entidad.Comision _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion _
        , entidad.IdCuentaBancaria _
        , entidad.TotalExento _
        , entidad.NumeroCF _
        , entidad.SerieCF _
        , entidad.Resolucion _
        , entidad.NumControlInterno _
        , entidad.ClaseDocumento _
        , entidad.ResolucionCF _
        , entidad.NumControlInternoCF _
        , entidad.ClaseDocumentoCF
        )
    End Sub

    Public Sub com_LiquidacionUpdate _
(ByVal entidad As com_Liquidacion)

        db.ExecuteNonQuery("com_LiquidacionUpdate",
        entidad.IdComprobante _
        , entidad.Serie _
        , entidad.Numero _
        , entidad.Fecha _
        , entidad.FechaContable _
        , entidad.IdSucursal _
        , entidad.IdProveedor _
        , entidad.Nombre _
        , entidad.Nrc _
        , entidad.Nit _
        , entidad.Direccion _
        , entidad.TotalIva _
        , entidad.TotalComprobante _
        , entidad.Comision _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion _
        , entidad.IdCuentaBancaria _
        , entidad.TotalExento _
        , entidad.NumeroCF _
        , entidad.SerieCF _
        , entidad.Resolucion _
        , entidad.NumControlInterno _
        , entidad.ClaseDocumento _
        , entidad.ResolucionCF _
        , entidad.NumControlInternoCF _
        , entidad.ClaseDocumentoCF
        )
    End Sub

    Public Sub com_LiquidacionUpdate _
(ByVal entidad As com_Liquidacion, Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_LiquidacionUpdate",
        entidad.IdComprobante _
        , entidad.Serie _
        , entidad.Numero _
        , entidad.Fecha _
        , entidad.FechaContable _
        , entidad.IdSucursal _
        , entidad.IdProveedor _
        , entidad.Nombre _
        , entidad.Nrc _
        , entidad.Nit _
        , entidad.Direccion _
        , entidad.TotalIva _
        , entidad.TotalComprobante _
        , entidad.Comision _
        , entidad.CreadoPor _
        , entidad.FechaHoraCreacion _
        , entidad.ModificadoPor _
        , entidad.FechaHoraModificacion _
        , entidad.IdCuentaBancaria _
        , entidad.TotalExento _
        , entidad.NumeroCF _
        , entidad.SerieCF _
        , entidad.Resolucion _
        , entidad.NumControlInterno _
        , entidad.ClaseDocumento _
        , entidad.ResolucionCF _
        , entidad.NumControlInternoCF _
        , entidad.ClaseDocumentoCF
        )
    End Sub

#End Region


#Region "com_Retenciones"
    Public Function com_RetencionesSelectAll() As DataTable
        Return db.ExecuteDataSet("com_RetencionesSelectAll").Tables(0)
    End Function

    Public Function com_RetencionesSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As com_Retenciones

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_RetencionesSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New com_Retenciones
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.Serie = dt.rows(0).item("Serie")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.FechaContable = dt.rows(0).item("FechaContable")
            entidad.IdClienteProveedor = dt.rows(0).item("IdClienteProveedor")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Direccion = dt.rows(0).item("Direccion")
            entidad.Nrc = dt.rows(0).item("Nrc")
            entidad.Nit = dt.rows(0).item("Nit")
            entidad.PorcentajeIva = dt.rows(0).item("PorcentajeIva")
            entidad.PorcentajeRet = dt.rows(0).item("PorcentajeRet")
            entidad.TotalIva = dt.rows(0).item("TotalIva")
            entidad.TotalImpuesto1 = dt.rows(0).item("TotalImpuesto1")
            entidad.TotalImpuesto2 = dt.rows(0).item("TotalImpuesto2")
            entidad.TotalComprobante = dt.rows(0).item("TotalComprobante")
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub com_RetencionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("com_RetencionesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub com_RetencionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_RetencionesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub com_RetencionesInsert _
    (ByVal entidad As com_Retenciones)

        db.ExecuteNonQuery("com_RetencionesInsert", _
         entidad.IdComprobante _
         , entidad.Serie _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.IdClienteProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.PorcentajeIva _
         , entidad.PorcentajeRet _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.TotalComprobante _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub com_RetencionesInsert _
    (ByVal entidad As com_Retenciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_RetencionesInsert", _
         entidad.IdComprobante _
         , entidad.Serie _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.IdClienteProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.PorcentajeIva _
         , entidad.PorcentajeRet _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.TotalComprobante _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub com_RetencionesUpdate _
    (ByVal entidad As com_Retenciones)

        db.ExecuteNonQuery("com_RetencionesUpdate", _
         entidad.IdComprobante _
         , entidad.Serie _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.IdClienteProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.PorcentajeIva _
         , entidad.PorcentajeRet _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.TotalComprobante _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub com_RetencionesUpdate _
    (ByVal entidad As com_Retenciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_RetencionesUpdate", _
         entidad.IdComprobante _
         , entidad.Serie _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.IdClienteProveedor _
         , entidad.Nombre _
         , entidad.Direccion _
         , entidad.Nrc _
         , entidad.Nit _
         , entidad.PorcentajeIva _
         , entidad.PorcentajeRet _
         , entidad.TotalIva _
         , entidad.TotalImpuesto1 _
         , entidad.TotalImpuesto2 _
         , entidad.TotalComprobante _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "com_RetencionesDetalle"
    Public Function com_RetencionesDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("com_RetencionesDetalleSelectAll").Tables(0)
    End Function

    Public Function com_RetencionesDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_RetencionesDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_RetencionesDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New com_RetencionesDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.PrecioUnitario = dt.rows(0).item("PrecioUnitario")
            entidad.PrecioTotal = dt.rows(0).item("PrecioTotal")

        End If
        Return Entidad
    End Function

    Public Sub com_RetencionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("com_RetencionesDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_RetencionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_RetencionesDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_RetencionesDetalleInsert _
    (ByVal entidad As com_RetencionesDetalle)

        db.ExecuteNonQuery("com_RetencionesDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         )
    End Sub

    Public Sub com_RetencionesDetalleInsert _
    (ByVal entidad As com_RetencionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_RetencionesDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         )
    End Sub

    Public Sub com_RetencionesDetalleUpdate _
    (ByVal entidad As com_RetencionesDetalle)

        db.ExecuteNonQuery("com_RetencionesDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         )
    End Sub

    Public Sub com_RetencionesDetalleUpdate _
    (ByVal entidad As com_RetencionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_RetencionesDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         )
    End Sub

#End Region
#Region "com_OrdenesCompraDetallePago"
    Public Function com_OrdenesCompraDetallePagoSelectAll() As DataTable
        Return db.ExecuteDataSet("com_OrdenesCompraDetallePagoSelectAll").Tables(0)
    End Function

    Public Function com_OrdenesCompraDetallePagoSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_OrdenesCompraDetallePago

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_OrdenesCompraDetallePagoSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New com_OrdenesCompraDetallePago
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.Valor = dt.rows(0).item("Valor")

        End If
        Return Entidad
    End Function

    Public Sub com_OrdenesCompraDetallePagoDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("com_OrdenesCompraDetallePagoDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_OrdenesCompraDetallePagoDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_OrdenesCompraDetallePagoDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_OrdenesCompraDetallePagoInsert _
    (ByVal entidad As com_OrdenesCompraDetallePago)

        db.ExecuteNonQuery("com_OrdenesCompraDetallePagoInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.Descripcion _
         , entidad.Valor _
         )
    End Sub

    Public Sub com_OrdenesCompraDetallePagoInsert _
    (ByVal entidad As com_OrdenesCompraDetallePago, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_OrdenesCompraDetallePagoInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.Descripcion _
         , entidad.Valor _
         )
    End Sub

    Public Sub com_OrdenesCompraDetallePagoUpdate _
    (ByVal entidad As com_OrdenesCompraDetallePago)

        db.ExecuteNonQuery("com_OrdenesCompraDetallePagoUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.Descripcion _
         , entidad.Valor _
         )
    End Sub

    Public Sub com_OrdenesCompraDetallePagoUpdate _
    (ByVal entidad As com_OrdenesCompraDetallePago, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_OrdenesCompraDetallePagoUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.Descripcion _
         , entidad.Valor _
         )
    End Sub

#End Region

#Region "com_ImportacionesDetalle"
    Public Function com_ImportacionesDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("com_ImportacionesDetalleSelectAll").Tables(0)
    End Function

    Public Function com_ImportacionesDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_ImportacionesDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_ImportacionesDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        If dt.Rows.Count = 0 Then
            Return Nothing
        Else
            Dim Entidad As New com_ImportacionesDetalle
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.Referencia = dt.rows(0).item("Referencia")
            entidad.ValorDai = dt.rows(0).item("ValorDai")
            Entidad.PrecioUnitario = dt.Rows(0).Item("PrecioUnitario")
            Entidad.PrecioVentaAct = dt.Rows(0).Item("PrecioVentaAct")
            Entidad.PrecioVentaCal = dt.Rows(0).Item("PrecioVentaCal")
            Entidad.PrecioVentaNuevo = dt.Rows(0).Item("PrecioVentaNuevo")
            Entidad.PorcComision = dt.Rows(0).Item("PorcComision")
            Entidad.PrecioTotal = dt.Rows(0).Item("PrecioTotal")
            Entidad.IdCentro = dt.Rows(0).Item("IdCentro")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

            Return Entidad
        End If
    End Function

    Public Sub com_ImportacionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("com_ImportacionesDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_ImportacionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_ImportacionesDetalleInsert _
    (ByVal entidad As com_ImportacionesDetalle)

        db.ExecuteNonQuery("com_ImportacionesDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.Referencia _
         , entidad.ValorDai _
         , entidad.PrecioUnitario _
           , entidad.PrecioVentaAct _
  , entidad.PrecioVentaCal _
  , entidad.PrecioVentaNuevo _
  , entidad.PorcComision _
         , entidad.PrecioTotal _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub com_ImportacionesDetalleInsert(ByVal entidad As com_ImportacionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.Referencia _
         , entidad.ValorDai _
         , entidad.PrecioUnitario _
         , entidad.PrecioVentaAct _
         , entidad.PrecioVentaCal _
         , entidad.PrecioVentaNuevo _
         , entidad.PorcComision _
         , entidad.PrecioTotal _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub com_ImportacionesDetalleUpdate _
    (ByVal entidad As com_ImportacionesDetalle)

        db.ExecuteNonQuery("com_ImportacionesDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.Referencia _
         , entidad.ValorDai _
         , entidad.PrecioUnitario _
           , entidad.PrecioVentaAct _
  , entidad.PrecioVentaCal _
  , entidad.PrecioVentaNuevo _
  , entidad.PorcComision _
         , entidad.PrecioTotal _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub com_ImportacionesDetalleUpdate _
    (ByVal entidad As com_ImportacionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.Referencia _
         , entidad.ValorDai _
         , entidad.PrecioUnitario _
           , entidad.PrecioVentaAct _
  , entidad.PrecioVentaCal _
  , entidad.PrecioVentaNuevo _
  , entidad.PorcComision _
         , entidad.PrecioTotal _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "com_ImportacionesGastosDetalle"
    Public Function com_ImportacionesGastosDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("com_ImportacionesGastosDetalleSelectAll").Tables(0)
    End Function

    Public Function com_ImportacionesGastosDetalleSelectByPK _
      (ByVal IdImportacion As System.Int32 _
      , ByVal IdGasto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_ImportacionesGastosDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_ImportacionesGastosDetalleSelectByPK", _
         IdImportacion _
         , IdGasto _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New com_ImportacionesGastosDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdImportacion = dt.rows(0).item("IdImportacion")
            entidad.IdGasto = dt.rows(0).item("IdGasto")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.Valor = dt.rows(0).item("Valor")
            Entidad.Contabilizar = dt.Rows(0).Item("Contabilizar")
        End If
        Return Entidad
    End Function

    Public Sub com_ImportacionesGastosDetalleDeleteByPK _
      (ByVal IdImportacion As System.Int32 _
      , ByVal IdGasto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("com_ImportacionesGastosDetalleDeleteByPK", _
         IdImportacion _
         , IdGasto _
         , IdDetalle _
         )
    End Sub

    Public Sub com_ImportacionesGastosDetalleDeleteByPK _
      (ByVal IdImportacion As System.Int32 _
      , ByVal IdGasto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesGastosDetalleDeleteByPK", _
         IdImportacion _
         , IdGasto _
         , IdDetalle _
         )
    End Sub

    Public Sub com_ImportacionesGastosDetalleInsert _
    (ByVal entidad As com_ImportacionesGastosDetalle)

        db.ExecuteNonQuery("com_ImportacionesGastosDetalleInsert", _
         entidad.IdImportacion _
         , entidad.IdGasto _
         , entidad.IdDetalle _
         , entidad.Concepto _
         , entidad.Valor _
         , entidad.Contabilizar _
         )
    End Sub

    Public Sub com_ImportacionesGastosDetalleInsert _
    (ByVal entidad As com_ImportacionesGastosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesGastosDetalleInsert", _
         entidad.IdImportacion _
         , entidad.IdGasto _
         , entidad.IdDetalle _
         , entidad.Concepto _
         , entidad.Valor _
          , entidad.Contabilizar _
         )
    End Sub

    Public Sub com_ImportacionesGastosDetalleUpdate _
    (ByVal entidad As com_ImportacionesGastosDetalle)

        db.ExecuteNonQuery("com_ImportacionesGastosDetalleUpdate", _
         entidad.IdImportacion _
         , entidad.IdGasto _
         , entidad.IdDetalle _
         , entidad.Concepto _
         , entidad.Valor _
          , entidad.Contabilizar _
         )
    End Sub

    Public Sub com_ImportacionesGastosDetalleUpdate _
    (ByVal entidad As com_ImportacionesGastosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesGastosDetalleUpdate", _
         entidad.IdImportacion _
         , entidad.IdGasto _
         , entidad.IdDetalle _
         , entidad.Concepto _
         , entidad.Valor _
          , entidad.Contabilizar _
         )
    End Sub

#End Region
#Region "com_ImportacionesProveedores"
    Public Function com_ImportacionesProveedoresSelectAll() As DataTable
        Return db.ExecuteDataSet("com_ImportacionesProveedoresSelectAll").Tables(0)
    End Function

    Public Function com_ImportacionesProveedoresSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As com_ImportacionesProveedores

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_ImportacionesProveedoresSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New com_ImportacionesProveedores
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdImportacion = dt.rows(0).item("IdImportacion")
            entidad.IdProveedor = dt.rows(0).item("IdProveedor")
            entidad.NumeroComprobante = dt.rows(0).item("NumeroComprobante")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.FechaContable = dt.rows(0).item("FechaContable")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdFormaPago = dt.rows(0).item("IdFormaPago")
            entidad.DiasCredito = dt.rows(0).item("DiasCredito")
            entidad.Valor = dt.rows(0).item("Valor")

        End If
        Return Entidad
    End Function

    Public Sub com_ImportacionesProveedoresDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("com_ImportacionesProveedoresDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub com_ImportacionesProveedoresDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesProveedoresDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub com_ImportacionesProveedoresInsert _
    (ByVal entidad As com_ImportacionesProveedores)

        db.ExecuteNonQuery("com_ImportacionesProveedoresInsert", _
         entidad.IdComprobante _
         , entidad.IdImportacion _
         , entidad.IdProveedor _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.IdDetalle _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.Valor _
         )
    End Sub

    Public Sub com_ImportacionesProveedoresInsert(ByVal entidad As com_ImportacionesProveedores, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesProveedoresInsert",
         entidad.IdComprobante _
         , entidad.IdImportacion _
         , entidad.IdProveedor _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.IdDetalle _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.Valor _
         , entidad.Contabilizar)
    End Sub

    Public Sub com_ImportacionesProveedoresUpdate _
    (ByVal entidad As com_ImportacionesProveedores)

        db.ExecuteNonQuery("com_ImportacionesProveedoresUpdate", _
         entidad.IdComprobante _
         , entidad.IdImportacion _
         , entidad.IdProveedor _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.IdDetalle _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.Valor _
         )
    End Sub

    Public Sub com_ImportacionesProveedoresUpdate _
    (ByVal entidad As com_ImportacionesProveedores, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesProveedoresUpdate", _
         entidad.IdComprobante _
         , entidad.IdImportacion _
         , entidad.IdProveedor _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.FechaContable _
         , entidad.IdDetalle _
         , entidad.IdFormaPago _
         , entidad.DiasCredito _
         , entidad.Valor _
         )
    End Sub

#End Region
#Region "com_ImportacionesGastosProductos"
    Public Function com_ImportacionesGastosProductosSelectAll() As DataTable
        Return db.ExecuteDataSet("com_ImportacionesGastosProductosSelectAll").Tables(0)
    End Function

    Public Function com_ImportacionesGastosProductosSelectByPK _
      (ByVal IdImportacion As System.Int32 _
      , ByVal Referencia As System.String _
      , ByVal IdProducto As System.String _
      , ByVal IdGasto As System.Int32 _
      ) As com_ImportacionesGastosProductos

        Dim dt As datatable
        dt = db.ExecuteDataSet("com_ImportacionesGastosProductosSelectByPK", _
         IdImportacion _
         , Referencia _
         , IdProducto _
         , IdGasto _
         ).tables(0)

        Dim Entidad As New com_ImportacionesGastosProductos
        If dt.Rows.Count > 0 Then
            entidad.IdImportacion = dt.rows(0).item("IdImportacion")
            entidad.Referencia = dt.rows(0).item("Referencia")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.IdGasto = dt.rows(0).item("IdGasto")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.AplicaGasto = dt.rows(0).item("AplicaGasto")

        End If
        Return Entidad
    End Function

    Public Sub com_ImportacionesGastosProductosDeleteByPK _
      (ByVal IdImportacion As System.Int32 _
      , ByVal Referencia As System.String _
      , ByVal IdProducto As System.String _
      , ByVal IdGasto As System.Int32 _
      )

        db.ExecuteNonQuery("com_ImportacionesGastosProductosDeleteByPK", _
         IdImportacion _
         , Referencia _
         , IdProducto _
         , IdGasto _
         )
    End Sub

    Public Sub com_ImportacionesGastosProductosDeleteByPK _
      (ByVal IdImportacion As System.Int32 _
      , ByVal Referencia As System.String _
      , ByVal IdProducto As System.String _
      , ByVal IdGasto As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesGastosProductosDeleteByPK", _
         IdImportacion _
         , Referencia _
         , IdProducto _
         , IdGasto _
         )
    End Sub

    Public Sub com_ImportacionesGastosProductosInsert _
    (ByVal entidad As com_ImportacionesGastosProductos)

        db.ExecuteNonQuery("com_ImportacionesGastosProductosInsert", _
         entidad.IdImportacion _
         , entidad.Referencia _
         , entidad.IdProducto _
         , entidad.IdGasto _
         , entidad.Descripcion _
         , entidad.AplicaGasto _
         )
    End Sub

    Public Sub com_ImportacionesGastosProductosInsert _
    (ByVal entidad As com_ImportacionesGastosProductos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesGastosProductosInsert", _
         entidad.IdImportacion _
         , entidad.Referencia _
         , entidad.IdProducto _
         , entidad.IdGasto _
         , entidad.Descripcion _
         , entidad.AplicaGasto _
         )
    End Sub

    Public Sub com_ImportacionesGastosProductosUpdate _
    (ByVal entidad As com_ImportacionesGastosProductos)

        db.ExecuteNonQuery("com_ImportacionesGastosProductosUpdate", _
         entidad.IdImportacion _
         , entidad.Referencia _
         , entidad.IdProducto _
         , entidad.IdGasto _
         , entidad.Descripcion _
         , entidad.AplicaGasto _
         )
    End Sub

    Public Sub com_ImportacionesGastosProductosUpdate _
    (ByVal entidad As com_ImportacionesGastosProductos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_ImportacionesGastosProductosUpdate", _
         entidad.IdImportacion _
         , entidad.Referencia _
         , entidad.IdProducto _
         , entidad.IdGasto _
         , entidad.Descripcion _
         , entidad.AplicaGasto _
         )
    End Sub

#End Region

#Region "com_NotasCredito"
    Public Function com_NotasCreditoSelectAll() As DataTable
        Return db.ExecuteDataSet("com_NotasCreditoSelectAll").Tables(0)
    End Function

    Public Function com_NotasCreditoSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As com_NotasCredito

        Dim dt As DataTable
        dt = db.ExecuteDataSet("com_NotasCreditoSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).Tables(0)

        Dim Entidad As New com_NotasCredito
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.NumComprobanteCompra = dt.Rows(0).Item("NumComprobanteCompra")
            Entidad.MontoAbonado = dt.Rows(0).Item("MontoAbonado")
            Entidad.SaldoActual = dt.Rows(0).Item("SaldoActual")
            Entidad.IdComprobanteCompra = dt.Rows(0).Item("IdComprobanteCompra")

        End If
        Return Entidad
    End Function

    Public Sub com_NotasCreditoDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("com_NotasCreditoDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_NotasCreditoDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_NotasCreditoDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub com_NotasCreditoInsert _
    (ByVal entidad As com_NotasCredito)

        db.ExecuteNonQuery("com_NotasCreditoInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.NumComprobanteCompra _
         , entidad.MontoAbonado _
         , entidad.SaldoActual _
         , entidad.IdComprobanteCompra _
         )
    End Sub

    Public Sub com_NotasCreditoInsert _
    (ByVal entidad As com_NotasCredito, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_NotasCreditoInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.NumComprobanteCompra _
         , entidad.MontoAbonado _
         , entidad.SaldoActual _
         , entidad.IdComprobanteCompra _
         )
    End Sub

    Public Sub com_NotasCreditoUpdate _
    (ByVal entidad As com_NotasCredito)

        db.ExecuteNonQuery("com_NotasCreditoUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.NumComprobanteCompra _
         , entidad.MontoAbonado _
         , entidad.SaldoActual _
         , entidad.IdComprobanteCompra _
         )
    End Sub

    Public Sub com_NotasCreditoUpdate _
    (ByVal entidad As com_NotasCredito, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "com_NotasCreditoUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.NumComprobanteCompra _
         , entidad.MontoAbonado _
         , entidad.SaldoActual _
         , entidad.IdComprobanteCompra _
         )
    End Sub

#End Region

#Region "inv_Categorias"
    Public Function inv_CategoriasSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_CategoriasSelectAll").Tables(0)
    End Function

    Public Function inv_CategoriasSelectByPK _
      (ByVal IdCategoria As System.Int32 _
      ) As inv_Categorias

        Dim dt As datatable
        dt = db.ExecuteDataSet("inv_CategoriasSelectByPK", _
         IdCategoria _
         ).tables(0)

        Dim Entidad As New inv_Categorias
        If dt.Rows.Count > 0 Then
            entidad.IdCategoria = dt.rows(0).item("IdCategoria")
            entidad.Nombre = dt.rows(0).item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub inv_CategoriasDeleteByPK _
      (ByVal IdCategoria As System.Int32 _
      )

        db.ExecuteNonQuery("inv_CategoriasDeleteByPK", _
         IdCategoria _
         )
    End Sub

    Public Sub inv_CategoriasDeleteByPK _
      (ByVal IdCategoria As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_CategoriasDeleteByPK", _
         IdCategoria _
         )
    End Sub

    Public Sub inv_CategoriasInsert _
    (ByVal entidad As inv_Categorias)

        db.ExecuteNonQuery("inv_CategoriasInsert", _
         entidad.IdCategoria _
         , entidad.Nombre _
         )
    End Sub

    Public Sub inv_CategoriasInsert _
    (ByVal entidad As inv_Categorias, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_CategoriasInsert", _
         entidad.IdCategoria _
         , entidad.Nombre _
         )
    End Sub

    Public Sub inv_CategoriasUpdate _
    (ByVal entidad As inv_Categorias)

        db.ExecuteNonQuery("inv_CategoriasUpdate", _
         entidad.IdCategoria _
         , entidad.Nombre _
         )
    End Sub

    Public Sub inv_CategoriasUpdate _
    (ByVal entidad As inv_Categorias, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_CategoriasUpdate", _
         entidad.IdCategoria _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "inv_CategoriasDetalle"
    Public Function inv_CategoriasDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_CategoriasDetalleSelectAll").Tables(0)
    End Function

    Public Function inv_CategoriasDetalleSelectByPK _
      (ByVal IdCategoria As System.Int32 _
      , ByVal IdSucursal As System.Int32 _
      ) As inv_CategoriasDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("inv_CategoriasDetalleSelectByPK", _
         IdCategoria _
         , IdSucursal _
         ).tables(0)

        Dim Entidad As New inv_CategoriasDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdCategoria = dt.rows(0).item("IdCategoria")
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.IdCuentaIngreso = dt.rows(0).item("IdCuentaIngreso")

        End If
        Return Entidad
    End Function

    Public Sub inv_CategoriasDetalleDeleteByPK _
      (ByVal IdCategoria As System.Int32 _
      , ByVal IdSucursal As System.Int32 _
      )

        db.ExecuteNonQuery("inv_CategoriasDetalleDeleteByPK", _
         IdCategoria _
         , IdSucursal _
         )
    End Sub

    Public Sub inv_CategoriasDetalleDeleteByPK _
      (ByVal IdCategoria As System.Int32 _
      , ByVal IdSucursal As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_CategoriasDetalleDeleteByPK", _
         IdCategoria _
         , IdSucursal _
         )
    End Sub

    Public Sub inv_CategoriasDetalleInsert _
    (ByVal entidad As inv_CategoriasDetalle)

        db.ExecuteNonQuery("inv_CategoriasDetalleInsert", _
         entidad.IdCategoria _
         , entidad.IdSucursal _
         , entidad.IdCuentaIngreso _
         )
    End Sub

    Public Sub inv_CategoriasDetalleInsert _
    (ByVal entidad As inv_CategoriasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_CategoriasDetalleInsert", _
         entidad.IdCategoria _
         , entidad.IdSucursal _
         , entidad.IdCuentaIngreso _
         )
    End Sub

    Public Sub inv_CategoriasDetalleUpdate _
    (ByVal entidad As inv_CategoriasDetalle)

        db.ExecuteNonQuery("inv_CategoriasDetalleUpdate", _
         entidad.IdCategoria _
         , entidad.IdSucursal _
         , entidad.IdCuentaIngreso _
         )
    End Sub

    Public Sub inv_CategoriasDetalleUpdate _
    (ByVal entidad As inv_CategoriasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_CategoriasDetalleUpdate", _
         entidad.IdCategoria _
         , entidad.IdSucursal _
         , entidad.IdCuentaIngreso _
         )
    End Sub

#End Region

#Region "inv_Bodegas"
    Public Function inv_BodegasSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_BodegasSelectAll").Tables(0)
    End Function

    Public Function inv_BodegasSelectByPK _
      (ByVal IdBodega As System.Int32 _
      ) As inv_Bodegas

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_BodegasSelectByPK", _
         IdBodega _
         ).Tables(0)

        Dim Entidad As New inv_Bodegas
        If dt.Rows.Count > 0 Then
            Entidad.IdBodega = dt.Rows(0).Item("IdBodega")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Responsable = dt.Rows(0).Item("Responsable")
            Entidad.Direccion = dt.Rows(0).Item("Direccion")
            Entidad.Telefono = dt.Rows(0).Item("Telefono")

        End If
        Return Entidad
    End Function

    Public Sub inv_BodegasDeleteByPK _
      (ByVal IdBodega As System.Int32 _
      )

        db.ExecuteNonQuery("inv_BodegasDeleteByPK", _
         IdBodega _
         )
    End Sub

    Public Sub inv_BodegasDeleteByPK _
      (ByVal IdBodega As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_BodegasDeleteByPK", _
         IdBodega _
         )
    End Sub

    Public Sub inv_BodegasInsert _
    (ByVal entidad As inv_Bodegas)

        db.ExecuteNonQuery("inv_BodegasInsert", _
         entidad.IdBodega _
         , entidad.Nombre _
         , entidad.Responsable _
         , entidad.Direccion _
         , entidad.Telefono _
         )
    End Sub

    Public Sub inv_BodegasInsert _
    (ByVal entidad As inv_Bodegas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_BodegasInsert", _
         entidad.IdBodega _
         , entidad.Nombre _
         , entidad.Responsable _
         , entidad.Direccion _
         , entidad.Telefono _
         )
    End Sub

    Public Sub inv_BodegasUpdate _
    (ByVal entidad As inv_Bodegas)

        db.ExecuteNonQuery("inv_BodegasUpdate", _
         entidad.IdBodega _
         , entidad.Nombre _
         , entidad.Responsable _
         , entidad.Direccion _
         , entidad.Telefono _
         )
    End Sub

    Public Sub inv_BodegasUpdate _
    (ByVal entidad As inv_Bodegas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_BodegasUpdate", _
         entidad.IdBodega _
         , entidad.Nombre _
         , entidad.Responsable _
         , entidad.Direccion _
         , entidad.Telefono _
         )
    End Sub

#End Region
#Region "inv_Existencias"
    Public Function inv_ExistenciasSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_ExistenciasSelectAll").Tables(0)
    End Function

    Public Function inv_ExistenciasSelectByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdBodega As System.Int32 _
      ) As inv_Existencias

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_ExistenciasSelectByPK", _
         IdProducto _
         , IdBodega _
         ).Tables(0)

        Dim Entidad As New inv_Existencias
        If dt.Rows.Count > 0 Then
            Entidad.IdProducto = dt.Rows(0).Item("IdProducto")
            Entidad.IdBodega = dt.Rows(0).Item("IdBodega")
            Entidad.Entrada = dt.Rows(0).Item("Entrada")
            Entidad.Salida = dt.Rows(0).Item("Salida")
            Entidad.Saldo = dt.Rows(0).Item("Saldo")

        End If
        Return Entidad
    End Function

    Public Sub inv_ExistenciasDeleteByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdBodega As System.Int32 _
      )

        db.ExecuteNonQuery("inv_ExistenciasDeleteByPK", _
         IdProducto _
         , IdBodega _
         )
    End Sub

    Public Sub inv_ExistenciasDeleteByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdBodega As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ExistenciasDeleteByPK", _
         IdProducto _
         , IdBodega _
         )
    End Sub

    Public Sub inv_ExistenciasInsert _
    (ByVal entidad As inv_Existencias)

        db.ExecuteNonQuery("inv_ExistenciasInsert", _
         entidad.IdProducto _
         , entidad.IdBodega _
         , entidad.Entrada _
         , entidad.Salida _
         , entidad.Saldo _
         )
    End Sub

    Public Sub inv_ExistenciasInsert _
    (ByVal entidad As inv_Existencias, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ExistenciasInsert", _
         entidad.IdProducto _
         , entidad.IdBodega _
         , entidad.Entrada _
         , entidad.Salida _
         , entidad.Saldo _
         )
    End Sub

    Public Sub inv_ExistenciasUpdate _
    (ByVal entidad As inv_Existencias)

        db.ExecuteNonQuery("inv_ExistenciasUpdate", _
         entidad.IdProducto _
         , entidad.IdBodega _
         , entidad.Entrada _
         , entidad.Salida _
         , entidad.Saldo _
         )
    End Sub

    Public Sub inv_ExistenciasUpdate _
    (ByVal entidad As inv_Existencias, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ExistenciasUpdate", _
         entidad.IdProducto _
         , entidad.IdBodega _
         , entidad.Entrada _
         , entidad.Salida _
         , entidad.Saldo _
         )
    End Sub

#End Region
#Region "inv_Grupos"
    Public Function inv_GruposSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_GruposSelectAll").Tables(0)
    End Function

    Public Function inv_GruposSelectByPK _
      (ByVal IdGrupo As System.Int32 _
      ) As inv_Grupos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_GruposSelectByPK", _
         IdGrupo _
         ).Tables(0)

        Dim Entidad As New inv_Grupos
        If dt.Rows.Count > 0 Then
            Entidad.IdGrupo = dt.Rows(0).Item("IdGrupo")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.IdCategoria = dt.Rows(0).Item("IdCategoria")
            Entidad.IdCuentaInventario = dt.Rows(0).Item("IdCuentaInventario")
            Entidad.IdCuentaIngreso = dt.Rows(0).Item("IdCuentaIngreso")
            Entidad.IdCuentaCostos = dt.Rows(0).Item("IdCuentaCostos")
            Entidad.IdCuentaRebajas = dt.Rows(0).Item("IdCuentaRebajas")
            Entidad.IdCuentaDevolucion = dt.Rows(0).Item("IdCuentaDevolucion")
            Entidad.UltCodProducto = dt.Rows(0).Item("UltCodProducto")
            Entidad.pje1 = dt.Rows(0).Item("pje1")
            Entidad.pje2 = dt.Rows(0).Item("pje2")
            Entidad.pje3 = dt.Rows(0).Item("pje3")
            Entidad.pje4 = dt.Rows(0).Item("pje4")

        End If
        Return Entidad
    End Function

    Public Sub inv_GruposDeleteByPK _
      (ByVal IdGrupo As System.Int32 _
      )

        db.ExecuteNonQuery("inv_GruposDeleteByPK", _
         IdGrupo _
         )
    End Sub

    Public Sub inv_GruposDeleteByPK _
      (ByVal IdGrupo As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_GruposDeleteByPK", _
         IdGrupo _
         )
    End Sub

    Public Sub inv_GruposInsert _
    (ByVal entidad As inv_Grupos)

        db.ExecuteNonQuery("inv_GruposInsert", _
         entidad.IdGrupo _
         , entidad.Nombre _
         , entidad.IdCategoria _
         , entidad.IdCuentaInventario _
         , entidad.IdCuentaIngreso _
         , entidad.IdCuentaCostos _
         , entidad.IdCuentaRebajas _
         , entidad.IdCuentaDevolucion _
         , entidad.UltCodProducto _
         , entidad.pje1 _
         , entidad.pje2 _
         , entidad.pje3 _
         , entidad.pje4 _
         )
    End Sub

    Public Sub inv_GruposInsert _
    (ByVal entidad As inv_Grupos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_GruposInsert", _
         entidad.IdGrupo _
         , entidad.Nombre _
         , entidad.IdCategoria _
         , entidad.IdCuentaInventario _
         , entidad.IdCuentaIngreso _
         , entidad.IdCuentaCostos _
         , entidad.IdCuentaRebajas _
         , entidad.IdCuentaDevolucion _
         , entidad.UltCodProducto _
         , entidad.pje1 _
         , entidad.pje2 _
         , entidad.pje3 _
         , entidad.pje4 _
         )
    End Sub

    Public Sub inv_GruposUpdate _
    (ByVal entidad As inv_Grupos)

        db.ExecuteNonQuery("inv_GruposUpdate", _
         entidad.IdGrupo _
         , entidad.Nombre _
         , entidad.IdCategoria _
         , entidad.IdCuentaInventario _
         , entidad.IdCuentaIngreso _
         , entidad.IdCuentaCostos _
         , entidad.IdCuentaRebajas _
         , entidad.IdCuentaDevolucion _
         , entidad.UltCodProducto _
         , entidad.pje1 _
         , entidad.pje2 _
         , entidad.pje3 _
         , entidad.pje4 _
         )
    End Sub

    Public Sub inv_GruposUpdate _
    (ByVal entidad As inv_Grupos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_GruposUpdate", _
         entidad.IdGrupo _
         , entidad.Nombre _
         , entidad.IdCategoria _
         , entidad.IdCuentaInventario _
         , entidad.IdCuentaIngreso _
         , entidad.IdCuentaCostos _
         , entidad.IdCuentaRebajas _
         , entidad.IdCuentaDevolucion _
         , entidad.UltCodProducto _
         , entidad.pje1 _
         , entidad.pje2 _
         , entidad.pje3 _
         , entidad.pje4 _
         )
    End Sub

#End Region
#Region "inv_Kardex"
    Public Function inv_KardexSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_KardexSelectAll").Tables(0)
    End Function

    Public Function inv_KardexSelectByPK _
      (ByVal IdMovimiento As System.Int32 _
      ) As inv_Kardex

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_KardexSelectByPK", _
         IdMovimiento _
         ).Tables(0)

        Dim Entidad As New inv_Kardex
        If dt.Rows.Count > 0 Then
            Entidad.IdMovimiento = dt.Rows(0).Item("IdMovimiento")
            Entidad.IdTipoComprobante = dt.Rows(0).Item("IdTipoComprobante")
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.Numero = dt.Rows(0).Item("Numero")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.IdBodega = dt.Rows(0).Item("IdBodega")
            Entidad.IdProducto = dt.Rows(0).Item("IdProducto")
            Entidad.TipoAplicacion = dt.Rows(0).Item("TipoAplicacion")
            Entidad.Cantidad = dt.Rows(0).Item("Cantidad")
            Entidad.PrecioUnitario = dt.Rows(0).Item("PrecioUnitario")
            Entidad.PrecioCosto = dt.Rows(0).Item("PrecioCosto")
            Entidad.SaldoExistencias = dt.Rows(0).Item("SaldoExistencias")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub inv_KardexDeleteByPK _
      (ByVal IdMovimiento As System.Int32 _
      )

        db.ExecuteNonQuery("inv_KardexDeleteByPK", _
         IdMovimiento _
         )
    End Sub

    Public Sub inv_KardexDeleteByPK _
      (ByVal IdMovimiento As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_KardexDeleteByPK", _
         IdMovimiento _
         )
    End Sub

    Public Sub inv_KardexInsert _
    (ByVal entidad As inv_Kardex)

        db.ExecuteNonQuery("inv_KardexInsert", _
         entidad.IdMovimiento _
         , entidad.IdTipoComprobante _
         , entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdBodega _
         , entidad.IdProducto _
         , entidad.TipoAplicacion _
         , entidad.Cantidad _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.SaldoExistencias _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_KardexInsert _
    (ByVal entidad As inv_Kardex, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_KardexInsert", _
         entidad.IdMovimiento _
         , entidad.IdTipoComprobante _
         , entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdBodega _
         , entidad.IdProducto _
         , entidad.TipoAplicacion _
         , entidad.Cantidad _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.SaldoExistencias _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_KardexUpdate _
    (ByVal entidad As inv_Kardex)

        db.ExecuteNonQuery("inv_KardexUpdate", _
         entidad.IdMovimiento _
         , entidad.IdTipoComprobante _
         , entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdBodega _
         , entidad.IdProducto _
         , entidad.TipoAplicacion _
         , entidad.Cantidad _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.SaldoExistencias _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_KardexUpdate _
    (ByVal entidad As inv_Kardex, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_KardexUpdate", _
         entidad.IdMovimiento _
         , entidad.IdTipoComprobante _
         , entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdBodega _
         , entidad.IdProducto _
         , entidad.TipoAplicacion _
         , entidad.Cantidad _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.SaldoExistencias _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "inv_Marcas"
    Public Function inv_MarcasSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_MarcasSelectAll").Tables(0)
    End Function

    Public Function inv_MarcasSelectByPK _
      (ByVal IdMarca As System.Int32 _
      ) As inv_Marcas

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_MarcasSelectByPK", _
         IdMarca _
         ).Tables(0)

        Dim Entidad As New inv_Marcas
        If dt.Rows.Count > 0 Then
            Entidad.IdMarca = dt.Rows(0).Item("IdMarca")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub inv_MarcasDeleteByPK _
      (ByVal IdMarca As System.Int32 _
      )

        db.ExecuteNonQuery("inv_MarcasDeleteByPK", _
         IdMarca _
         )
    End Sub

    Public Sub inv_MarcasDeleteByPK _
      (ByVal IdMarca As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_MarcasDeleteByPK", _
         IdMarca _
         )
    End Sub

    Public Sub inv_MarcasInsert _
    (ByVal entidad As inv_Marcas)

        db.ExecuteNonQuery("inv_MarcasInsert", _
         entidad.IdMarca _
         , entidad.Nombre _
         )
    End Sub

    Public Sub inv_MarcasInsert _
    (ByVal entidad As inv_Marcas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_MarcasInsert", _
         entidad.IdMarca _
         , entidad.Nombre _
         )
    End Sub

    Public Sub inv_MarcasUpdate _
    (ByVal entidad As inv_Marcas)

        db.ExecuteNonQuery("inv_MarcasUpdate", _
         entidad.IdMarca _
         , entidad.Nombre _
         )
    End Sub

    Public Sub inv_MarcasUpdate _
    (ByVal entidad As inv_Marcas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_MarcasUpdate", _
         entidad.IdMarca _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "inv_Entradas"
    Public Function inv_EntradasSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_EntradasSelectAll").Tables(0)
    End Function

    Public Function inv_EntradasSelectByPK(ByVal IdComprobante As Integer) As inv_Entradas

        Dim dt As DataTable = db.ExecuteDataSet("inv_EntradasSelectByPK", IdComprobante).Tables(0)

        Dim Entidad As New inv_Entradas
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdTipoComprobante = dt.Rows(0).Item("IdTipoComprobante")
            Entidad.Numero = dt.Rows(0).Item("Numero")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.Concepto = dt.Rows(0).Item("Concepto")
            Entidad.IdBodega = dt.Rows(0).Item("IdBodega")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.AplicadaInventario = dt.Rows(0).Item("AplicadaInventario")
            Entidad.IdComprobVenta = dt.Rows(0).Item("IdComprobVenta")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
        End If
        Return Entidad
    End Function

    Public Sub inv_EntradasDeleteByPK(ByVal IdComprobante As Integer)

        db.ExecuteNonQuery("inv_EntradasDeleteByPK", IdComprobante)
    End Sub

    Public Sub inv_EntradasDeleteByPK(ByVal IdComprobante As Integer, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_EntradasDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub inv_EntradasInsert _
    (ByVal entidad As inv_Entradas)

        db.ExecuteNonQuery("inv_EntradasInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.IdComprobVenta _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub inv_EntradasInsert _
    (ByVal entidad As inv_Entradas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_EntradasInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.IdComprobVenta _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub inv_EntradasUpdate _
    (ByVal entidad As inv_Entradas)

        db.ExecuteNonQuery("inv_EntradasUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.IdComprobVenta _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub inv_EntradasUpdate _
    (ByVal entidad As inv_Entradas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_EntradasUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.IdComprobVenta _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "inv_EntradasDetalle"
    Public Function inv_EntradasDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_EntradasDetalleSelectAll").Tables(0)
    End Function

    Public Function inv_EntradasDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_EntradasDetalle

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_EntradasDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).Tables(0)

        Dim Entidad As New inv_EntradasDetalle
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.IdProducto = dt.Rows(0).Item("IdProducto")
            Entidad.Cantidad = dt.Rows(0).Item("Cantidad")
            Entidad.Descripcion = dt.Rows(0).Item("Descripcion")
            Entidad.PrecioUnitario = dt.Rows(0).Item("PrecioUnitario")
            Entidad.PrecioCosto = dt.Rows(0).Item("PrecioCosto")
            Entidad.PrecioTotal = dt.Rows(0).Item("PrecioTotal")
            Entidad.IdCentro = dt.Rows(0).Item("IdCentro")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub inv_EntradasDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("inv_EntradasDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_EntradasDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_EntradasDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_EntradasDetalleInsert _
    (ByVal entidad As inv_EntradasDetalle)

        db.ExecuteNonQuery("inv_EntradasDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_EntradasDetalleInsert _
    (ByVal entidad As inv_EntradasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_EntradasDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_EntradasDetalleUpdate _
    (ByVal entidad As inv_EntradasDetalle)

        db.ExecuteNonQuery("inv_EntradasDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_EntradasDetalleUpdate _
    (ByVal entidad As inv_EntradasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_EntradasDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "inv_Salidas"
    Public Function inv_SalidasSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_SalidasSelectAll").Tables(0)
    End Function

    Public Function inv_SalidasSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As inv_Salidas

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_SalidasSelectByPK", _
         IdComprobante _
         ).Tables(0)

        Dim Entidad As New inv_Salidas
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdTipoComprobante = dt.Rows(0).Item("IdTipoComprobante")
            Entidad.Numero = dt.Rows(0).Item("Numero")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.Concepto = dt.Rows(0).Item("Concepto")
            Entidad.IdBodega = dt.Rows(0).Item("IdBodega")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.AplicadaInventario = dt.Rows(0).Item("AplicadaInventario")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
        End If
        Return Entidad
    End Function

    Public Sub inv_SalidasDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("inv_SalidasDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub inv_SalidasDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_SalidasDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub inv_SalidasInsert _
    (ByVal entidad As inv_Salidas)

        db.ExecuteNonQuery("inv_SalidasInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub inv_SalidasInsert _
    (ByVal entidad As inv_Salidas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_SalidasInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub inv_SalidasUpdate _
    (ByVal entidad As inv_Salidas)

        db.ExecuteNonQuery("inv_SalidasUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub inv_SalidasUpdate _
    (ByVal entidad As inv_Salidas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_SalidasUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "inv_SalidasDetalle"
    Public Function inv_SalidasDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_SalidasDetalleSelectAll").Tables(0)
    End Function

    Public Function inv_SalidasDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_SalidasDetalle

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_SalidasDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).Tables(0)

        Dim Entidad As New inv_SalidasDetalle
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.IdProducto = dt.Rows(0).Item("IdProducto")
            Entidad.Cantidad = dt.Rows(0).Item("Cantidad")
            Entidad.Descripcion = dt.Rows(0).Item("Descripcion")
            Entidad.PrecioUnitario = dt.Rows(0).Item("PrecioUnitario")
            Entidad.PrecioCosto = dt.Rows(0).Item("PrecioCosto")
            Entidad.PrecioTotal = dt.Rows(0).Item("PrecioTotal")
            Entidad.IdCentro = dt.Rows(0).Item("IdCentro")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub inv_SalidasDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("inv_SalidasDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_SalidasDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_SalidasDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_SalidasDetalleInsert _
    (ByVal entidad As inv_SalidasDetalle)

        db.ExecuteNonQuery("inv_SalidasDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_SalidasDetalleInsert _
    (ByVal entidad As inv_SalidasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_SalidasDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_SalidasDetalleUpdate _
    (ByVal entidad As inv_SalidasDetalle)

        db.ExecuteNonQuery("inv_SalidasDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_SalidasDetalleUpdate _
    (ByVal entidad As inv_SalidasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_SalidasDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.IdCentro _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region

#Region "inv_Traslados"
    Public Function inv_TrasladosSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_TrasladosSelectAll").Tables(0)
    End Function

    Public Function inv_TrasladosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As inv_Traslados

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_TrasladosSelectByPK", _
         IdComprobante _
         ).Tables(0)

        Dim Entidad As New inv_Traslados
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdTipoComprobante = dt.Rows(0).Item("IdTipoComprobante")
            Entidad.Numero = dt.Rows(0).Item("Numero")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.Concepto = dt.Rows(0).Item("Concepto")
            Entidad.IdBodegaSalida = dt.Rows(0).Item("IdBodegaSalida")
            Entidad.IdBodegaIngreso = dt.Rows(0).Item("IdBodegaIngreso")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.AplicadaInventario = fd.SiEsNulo(dt.Rows(0).Item("AplicadaInventario"), Nothing)
            Entidad.ImprimirValores = fd.SiEsNulo(dt.Rows(0).Item("ImprimirValores"), Nothing)
            Entidad.IdPedido = fd.SiEsNulo(dt.Rows(0).Item("IdPedido"), 0)
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = fd.SiEsNulo(dt.Rows(0).Item("ModificadoPor"), Nothing)
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
            Entidad.IdSucursalRecibe = dt.Rows(0).Item("IdSucursalRecibe")
        End If
        Return Entidad
    End Function

    Public Sub inv_TrasladosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("inv_TrasladosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub inv_TrasladosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_TrasladosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub inv_TrasladosInsert _
    (ByVal entidad As inv_Traslados)

        db.ExecuteNonQuery("inv_TrasladosInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodegaSalida _
         , entidad.IdBodegaIngreso _
         , entidad.IdSucursal _
         , entidad.AplicadaInventario _
         , entidad.ImprimirValores _
         , entidad.IdPedido _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         , entidad.IdSucursalRecibe _
         )
    End Sub

    Public Sub inv_TrasladosInsert _
    (ByVal entidad As inv_Traslados, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_TrasladosInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodegaSalida _
         , entidad.IdBodegaIngreso _
         , entidad.IdSucursal _
         , entidad.AplicadaInventario _
         , entidad.ImprimirValores _
         , entidad.IdPedido _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         , entidad.IdSucursalRecibe _
         )
    End Sub

    Public Sub inv_TrasladosUpdate _
    (ByVal entidad As inv_Traslados)

        db.ExecuteNonQuery("inv_TrasladosUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodegaSalida _
         , entidad.IdBodegaIngreso _
         , entidad.IdSucursal _
         , entidad.AplicadaInventario _
         , entidad.ImprimirValores _
         , entidad.IdPedido _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         , entidad.IdSucursalRecibe _
         )
    End Sub

    Public Sub inv_TrasladosUpdate _
    (ByVal entidad As inv_Traslados, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_TrasladosUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodegaSalida _
         , entidad.IdBodegaIngreso _
         , entidad.IdSucursal _
         , entidad.AplicadaInventario _
         , entidad.ImprimirValores _
         , entidad.IdPedido _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         , entidad.IdSucursalRecibe _
         )
    End Sub

#End Region

#Region "inv_Producciones"
    Public Function inv_ProduccionesSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_ProduccionesSelectAll").Tables(0)
    End Function

    Public Function inv_ProduccionesSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As inv_Producciones

        Dim dt As datatable
        dt = db.ExecuteDataSet("inv_ProduccionesSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New inv_Producciones
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdTipoComprobante = dt.Rows(0).Item("IdTipoComprobante")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.IdBodega = dt.rows(0).item("IdBodega")
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.AplicadaInventario = dt.rows(0).item("AplicadaInventario")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub inv_ProduccionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("inv_ProduccionesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub inv_ProduccionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProduccionesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub inv_ProduccionesInsert _
    (ByVal entidad As inv_Producciones)

        db.ExecuteNonQuery("inv_ProduccionesInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub inv_ProduccionesInsert _
    (ByVal entidad As inv_Producciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProduccionesInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub inv_ProduccionesUpdate _
    (ByVal entidad As inv_Producciones)

        db.ExecuteNonQuery("inv_ProduccionesUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub inv_ProduccionesUpdate _
    (ByVal entidad As inv_Producciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProduccionesUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdSucursal _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "inv_ProduccionesDetalle"
    Public Function inv_ProduccionesDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_ProduccionesDetalleSelectAll").Tables(0)
    End Function

    Public Function inv_ProduccionesDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_ProduccionesDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("inv_ProduccionesDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New inv_ProduccionesDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.PrecioUnitario = dt.rows(0).item("PrecioUnitario")
            entidad.PrecioCosto = dt.rows(0).item("PrecioCosto")
            entidad.PrecioTotal = dt.rows(0).item("PrecioTotal")
            entidad.CantidadPosible = dt.rows(0).item("CantidadPosible")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub inv_ProduccionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("inv_ProduccionesDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_ProduccionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProduccionesDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_ProduccionesDetalleInsert _
    (ByVal entidad As inv_ProduccionesDetalle)

        db.ExecuteNonQuery("inv_ProduccionesDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CantidadPosible _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_ProduccionesDetalleInsert _
    (ByVal entidad As inv_ProduccionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProduccionesDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CantidadPosible _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_ProduccionesDetalleUpdate _
    (ByVal entidad As inv_ProduccionesDetalle)

        db.ExecuteNonQuery("inv_ProduccionesDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CantidadPosible _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_ProduccionesDetalleUpdate _
    (ByVal entidad As inv_ProduccionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProduccionesDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CantidadPosible _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "inv_ProduccionesGastosProductos"
    Public Function inv_ProduccionesGastosProductosSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_ProduccionesGastosProductosSelectAll").Tables(0)
    End Function

    Public Function inv_ProduccionesGastosProductosSelectByPK _
      (ByVal IdProduccion As System.Int32 _
      , ByVal IdProducto As System.String _
      , ByVal IdGasto As System.Int32 _
      ) As inv_ProduccionesGastosProductos

        Dim dt As datatable
        dt = db.ExecuteDataSet("inv_ProduccionesGastosProductosSelectByPK", _
         IdProduccion _
         , IdProducto _
         , IdGasto _
         ).tables(0)

        Dim Entidad As New inv_ProduccionesGastosProductos
        If dt.Rows.Count > 0 Then
            entidad.IdProduccion = dt.rows(0).item("IdProduccion")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.IdGasto = dt.rows(0).item("IdGasto")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.AplicaGasto = dt.rows(0).item("AplicaGasto")

        End If
        Return Entidad
    End Function

    Public Sub inv_ProduccionesGastosProductosDeleteByPK _
      (ByVal IdProduccion As System.Int32 _
      , ByVal IdProducto As System.String _
      , ByVal IdGasto As System.Int32 _
      )

        db.ExecuteNonQuery("inv_ProduccionesGastosProductosDeleteByPK", _
         IdProduccion _
         , IdProducto _
         , IdGasto _
         )
    End Sub

    Public Sub inv_ProduccionesGastosProductosDeleteByPK _
      (ByVal IdProduccion As System.Int32 _
      , ByVal IdProducto As System.String _
      , ByVal IdGasto As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProduccionesGastosProductosDeleteByPK", _
         IdProduccion _
         , IdProducto _
         , IdGasto _
         )
    End Sub

    Public Sub inv_ProduccionesGastosProductosInsert _
    (ByVal entidad As inv_ProduccionesGastosProductos)

        db.ExecuteNonQuery("inv_ProduccionesGastosProductosInsert", _
         entidad.IdProduccion _
         , entidad.IdProducto _
         , entidad.IdGasto _
         , entidad.Descripcion _
         , entidad.AplicaGasto _
         )
    End Sub

    Public Sub inv_ProduccionesGastosProductosInsert _
    (ByVal entidad As inv_ProduccionesGastosProductos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProduccionesGastosProductosInsert", _
         entidad.IdProduccion _
         , entidad.IdProducto _
         , entidad.IdGasto _
         , entidad.Descripcion _
         , entidad.AplicaGasto _
         )
    End Sub

    Public Sub inv_ProduccionesGastosProductosUpdate _
    (ByVal entidad As inv_ProduccionesGastosProductos)

        db.ExecuteNonQuery("inv_ProduccionesGastosProductosUpdate", _
         entidad.IdProduccion _
         , entidad.IdProducto _
         , entidad.IdGasto _
         , entidad.Descripcion _
         , entidad.AplicaGasto _
         )
    End Sub

    Public Sub inv_ProduccionesGastosProductosUpdate _
    (ByVal entidad As inv_ProduccionesGastosProductos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProduccionesGastosProductosUpdate", _
         entidad.IdProduccion _
         , entidad.IdProducto _
         , entidad.IdGasto _
         , entidad.Descripcion _
         , entidad.AplicaGasto _
         )
    End Sub

#End Region

#Region "inv_ProductosKit"
    Public Function inv_ProductosKitSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_ProductosKitSelectAll").Tables(0)
    End Function

    Public Function inv_ProductosKitSelectByPK _
      (ByVal IdKit As System.String _
      , ByVal IdDetalle As System.Int32 _
      , ByVal IdProducto As System.String _
      ) As inv_ProductosKit

        Dim dt As datatable
        dt = db.ExecuteDataSet("inv_ProductosKitSelectByPK", _
         IdKit _
         , IdDetalle _
         , IdProducto _
         ).tables(0)

        Dim Entidad As New inv_ProductosKit
        If dt.Rows.Count > 0 Then
            entidad.IdKit = dt.rows(0).item("IdKit")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub inv_ProductosKitDeleteByPK _
      (ByVal IdKit As System.String _
      , ByVal IdDetalle As System.Int32 _
      , ByVal IdProducto As System.String _
      )

        db.ExecuteNonQuery("inv_ProductosKitDeleteByPK", _
         IdKit _
         , IdDetalle _
         , IdProducto _
         )
    End Sub

    Public Sub inv_ProductosKitDeleteByPK _
      (ByVal IdKit As System.String _
      , ByVal IdDetalle As System.Int32 _
      , ByVal IdProducto As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosKitDeleteByPK", _
         IdKit _
         , IdDetalle _
         , IdProducto _
         )
    End Sub

    Public Sub inv_ProductosKitInsert _
    (ByVal entidad As inv_ProductosKit)

        db.ExecuteNonQuery("inv_ProductosKitInsert", _
         entidad.IdKit _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_ProductosKitInsert _
    (ByVal entidad As inv_ProductosKit, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosKitInsert", _
         entidad.IdKit _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_ProductosKitUpdate _
    (ByVal entidad As inv_ProductosKit)

        db.ExecuteNonQuery("inv_ProductosKitUpdate", _
         entidad.IdKit _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_ProductosKitUpdate _
    (ByVal entidad As inv_ProductosKit, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosKitUpdate", _
         entidad.IdKit _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "inv_TomaFisica"
    Public Function inv_TomaFisicaSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_TomaFisicaSelectAll").Tables(0)
    End Function

    Public Function inv_TomaFisicaSelectByPK _
      (ByVal IdProducto As System.String _
      , ByVal Fecha As System.DateTime _
      , ByVal IdBodega As System.Int32 _
      ) As inv_TomaFisica

        Dim dt As datatable
        dt = db.ExecuteDataSet("inv_TomaFisicaSelectByPK", _
         IdProducto _
         , Fecha _
         , IdBodega _
         ).tables(0)

        Dim Entidad As New inv_TomaFisica
        If dt.Rows.Count > 0 Then
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdBodega = dt.rows(0).item("IdBodega")
            entidad.ConteoFisico = dt.rows(0).item("ConteoFisico")
            entidad.ConteoAcumulado = dt.rows(0).item("ConteoAcumulado")
            entidad.ExistenciaActual = dt.rows(0).item("ExistenciaActual")
            entidad.PrecioCosto = dt.rows(0).item("PrecioCosto")
            entidad.IdGrupo = dt.rows(0).item("IdGrupo")
            entidad.IdSubGrupo = dt.rows(0).item("IdSubGrupo")
            entidad.Revisado = dt.rows(0).item("Revisado")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.HechaHoraCreacion = dt.rows(0).item("HechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub inv_TomaFisicaDeleteByPK _
      (ByVal IdProducto As System.String _
      , ByVal Fecha As System.DateTime _
      , ByVal IdBodega As System.Int32 _
      )

        db.ExecuteNonQuery("inv_TomaFisicaDeleteByPK", _
         IdProducto _
         , Fecha _
         , IdBodega _
         )
    End Sub

    Public Sub inv_TomaFisicaDeleteByPK _
      (ByVal IdProducto As System.String _
      , ByVal Fecha As System.DateTime _
      , ByVal IdBodega As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_TomaFisicaDeleteByPK", _
         IdProducto _
         , Fecha _
         , IdBodega _
         )
    End Sub

    Public Sub inv_TomaFisicaInsert _
    (ByVal entidad As inv_TomaFisica)

        db.ExecuteNonQuery("inv_TomaFisicaInsert", _
         entidad.IdProducto _
         , entidad.Fecha _
         , entidad.IdBodega _
         , entidad.ConteoFisico _
         , entidad.ConteoAcumulado _
         , entidad.ExistenciaActual _
         , entidad.PrecioCosto _
         , entidad.IdGrupo _
         , entidad.IdSubGrupo _
         , entidad.Revisado _
         , entidad.CreadoPor _
         , entidad.HechaHoraCreacion _
         )
    End Sub

    Public Sub inv_TomaFisicaInsert _
    (ByVal entidad As inv_TomaFisica, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_TomaFisicaInsert", _
         entidad.IdProducto _
         , entidad.Fecha _
         , entidad.IdBodega _
         , entidad.ConteoFisico _
         , entidad.ConteoAcumulado _
         , entidad.ExistenciaActual _
         , entidad.PrecioCosto _
         , entidad.IdGrupo _
         , entidad.IdSubGrupo _
         , entidad.Revisado _
         , entidad.CreadoPor _
         , entidad.HechaHoraCreacion _
         )
    End Sub

    Public Sub inv_TomaFisicaUpdate _
    (ByVal entidad As inv_TomaFisica)

        db.ExecuteNonQuery("inv_TomaFisicaUpdate", _
         entidad.IdProducto _
         , entidad.Fecha _
         , entidad.IdBodega _
         , entidad.ConteoFisico _
         , entidad.ConteoAcumulado _
         , entidad.ExistenciaActual _
         , entidad.PrecioCosto _
         , entidad.IdGrupo _
         , entidad.IdSubGrupo _
         , entidad.Revisado _
         , entidad.CreadoPor _
         , entidad.HechaHoraCreacion _
         )
    End Sub

    Public Sub inv_TomaFisicaUpdate _
    (ByVal entidad As inv_TomaFisica, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_TomaFisicaUpdate", _
         entidad.IdProducto _
         , entidad.Fecha _
         , entidad.IdBodega _
         , entidad.ConteoFisico _
         , entidad.ConteoAcumulado _
         , entidad.ExistenciaActual _
         , entidad.PrecioCosto _
         , entidad.IdGrupo _
         , entidad.IdSubGrupo _
         , entidad.Revisado _
         , entidad.CreadoPor _
         , entidad.HechaHoraCreacion _
         )
    End Sub

#End Region

#Region "Vinetas"
    Public Sub inv_VinetasInsert _
(ByVal entidad As inv_Vinetas)

        db.ExecuteNonQuery("inv_VinetasInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.IdMarca _
         , entidad.IdUnidad _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_VinetasInsert _
    (ByVal entidad As inv_Vinetas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_VinetasInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.IdMarca _
         , entidad.IdUnidad _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region

#Region "inv_TrasladosDetalle"
    Public Function inv_TrasladosDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_TrasladosDetalleSelectAll").Tables(0)
    End Function

    Public Function inv_TrasladosDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_TrasladosDetalle

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_TrasladosDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).Tables(0)

        Dim Entidad As New inv_TrasladosDetalle
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.IdProducto = dt.Rows(0).Item("IdProducto")
            Entidad.Cantidad = dt.Rows(0).Item("Cantidad")
            Entidad.Descripcion = dt.Rows(0).Item("Descripcion")
            Entidad.PrecioUnitario = dt.Rows(0).Item("PrecioUnitario")
            Entidad.PrecioTotal = dt.Rows(0).Item("PrecioTotal")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub inv_TrasladosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("inv_TrasladosDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_TrasladosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_TrasladosDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_TrasladosDetalleInsert _
    (ByVal entidad As inv_TrasladosDetalle)

        db.ExecuteNonQuery("inv_TrasladosDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_TrasladosDetalleInsert _
    (ByVal entidad As inv_TrasladosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_TrasladosDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_TrasladosDetalleUpdate _
    (ByVal entidad As inv_TrasladosDetalle)

        db.ExecuteNonQuery("inv_TrasladosDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_TrasladosDetalleUpdate _
    (ByVal entidad As inv_TrasladosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_TrasladosDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "inv_Precios"
    Public Function inv_PreciosSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_PreciosSelectAll").Tables(0)
    End Function

    Public Function inv_PreciosSelectByPK _
      (ByVal IdPrecio As System.Int32 _
      ) As inv_Precios

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_PreciosSelectByPK", _
         IdPrecio _
         ).Tables(0)

        Dim Entidad As New inv_Precios
        If dt.Rows.Count > 0 Then
            Entidad.IdPrecio = dt.Rows(0).Item("IdPrecio")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.RequiereAutorizacion = dt.Rows(0).Item("RequiereAutorizacion")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
        End If
        Return Entidad
    End Function

    Public Sub inv_PreciosDeleteByPK(ByVal IdPrecio As System.Int32)

        db.ExecuteNonQuery("inv_PreciosDeleteByPK", IdPrecio)
    End Sub

    Public Sub inv_PreciosDeleteByPK(ByVal IdPrecio As System.Int32, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_PreciosDeleteByPK", IdPrecio)
    End Sub

    Public Sub inv_PreciosInsert _
    (ByVal entidad As inv_Precios)

        db.ExecuteNonQuery("inv_PreciosInsert", _
         entidad.IdPrecio _
         , entidad.Nombre _
         , entidad.RequiereAutorizacion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_PreciosInsert _
    (ByVal entidad As inv_Precios, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_PreciosInsert", _
         entidad.IdPrecio _
         , entidad.Nombre _
         , entidad.RequiereAutorizacion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_PreciosUpdate _
    (ByVal entidad As inv_Precios)

        db.ExecuteNonQuery("inv_PreciosUpdate", _
         entidad.IdPrecio _
         , entidad.Nombre _
         , entidad.RequiereAutorizacion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_PreciosUpdate _
    (ByVal entidad As inv_Precios, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_PreciosUpdate", _
         entidad.IdPrecio _
         , entidad.Nombre _
         , entidad.RequiereAutorizacion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "inv_Productos"
    Public Function inv_ProductosSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_ProductosSelectAll").Tables(0)
    End Function

    Public Function inv_ProductosSelectByPK _
      (ByVal IdProducto As System.String _
      ) As inv_Productos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_ProductosSelectByPK", _
         IdProducto _
         ).Tables(0)

        Dim Entidad As New inv_Productos
        If dt.Rows.Count > 0 Then
            Entidad.IdProducto = dt.Rows(0).Item("IdProducto")
            Entidad.CodBarra = dt.Rows(0).Item("Codbarra")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.IdGrupo = dt.Rows(0).Item("IdGrupo")
            Entidad.IdSubGrupo = dt.Rows(0).Item("IdSubGrupo")
            Entidad.IdUnidad = dt.Rows(0).Item("IdUnidad")
            Entidad.IdMarca = dt.Rows(0).Item("IdMarca")
            Entidad.IdProveedor = dt.Rows(0).Item("IdProveedor")
            Entidad.IdUbicacion = dt.Rows(0).Item("IdUbicacion")
            Entidad.IdCategoria = dt.Rows(0).Item("IdCategoria")
            Entidad.Talla = dt.Rows(0).Item("Talla")
            Entidad.Color = dt.Rows(0).Item("Color")
            Entidad.Estilo = dt.Rows(0).Item("Estilo")
            Entidad.UnidadesPresentacion = dt.Rows(0).Item("UnidadesPresentacion")
            Entidad.ExistenciaMaxima = dt.Rows(0).Item("ExistenciaMaxima")
            Entidad.ExistenciaMinima = dt.Rows(0).Item("ExistenciaMinima")
            Entidad.TipoProducto = dt.Rows(0).Item("TipoProducto")
            Entidad.EsExento = dt.Rows(0).Item("EsExento")
            Entidad.EsEspecial = dt.Rows(0).Item("EsEspecial")
            Entidad.PermiteFacturar = dt.Rows(0).Item("PermiteFacturar")
            Entidad.FechaIngreso = dt.Rows(0).Item("FechaIngreso")
            Entidad.PrecioUltCosto = dt.Rows(0).Item("PrecioUltCosto")
            Entidad.PrecioCosto = dt.Rows(0).Item("PrecioCosto")
            Entidad.InformacionAdicional = dt.Rows(0).Item("InformacionAdicional")
            Entidad.IdCuentaIng = dt.Rows(0).Item("IdCuentaIng")
            Entidad.IdCuentaInv = dt.Rows(0).Item("IdCuentaInv")
            Entidad.IdCuentaCos = dt.Rows(0).Item("IdCuentaCos")
            Entidad.ArchivoImagen = dt.Rows(0).Item("ArchivoImagen")
            Entidad.EstadoProducto = dt.Rows(0).Item("EstadoProducto")
            Entidad.Compuesto = dt.Rows(0).Item("Compuesto")
            Entidad.PorcComision = dt.Rows(0).Item("PorcComision")
            Entidad.IdClasificacion = dt.Rows(0).Item("IdClasificacion")
            Entidad.IdCentro = dt.Rows(0).Item("IdCentro")
            Entidad.Traduccion = dt.Rows(0).Item("Traduccion")
            Entidad.Origen = dt.Rows(0).Item("Origen")
            Entidad.Cuantia = dt.Rows(0).Item("Cuantia")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
        End If
        Return Entidad
    End Function

    Public Sub inv_ProductosDeleteByPK _
      (ByVal IdProducto As System.String _
      )

        db.ExecuteNonQuery("inv_ProductosDeleteByPK", _
         IdProducto _
         )
    End Sub

    Public Sub inv_ProductosDeleteByPK _
      (ByVal IdProducto As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosDeleteByPK", _
         IdProducto _
         )
    End Sub

    Public Sub inv_ProductosInsert _
    (ByVal entidad As inv_Productos)

        db.ExecuteNonQuery("inv_ProductosInsert", _
         entidad.IdProducto _
         , entidad.CodBarra _
         , entidad.Nombre _
         , entidad.IdGrupo _
         , entidad.IdSubGrupo _
         , entidad.IdUnidad _
         , entidad.IdMarca _
         , entidad.IdProveedor _
         , entidad.IdUbicacion _
         , entidad.IdCategoria _
         , entidad.Talla _
         , entidad.Color _
         , entidad.Estilo _
         , entidad.UnidadesPresentacion _
         , entidad.ExistenciaMaxima _
         , entidad.ExistenciaMinima _
         , entidad.TipoProducto _
         , entidad.EsExento _
         , entidad.EsEspecial _
         , entidad.PermiteFacturar _
         , entidad.FechaIngreso _
         , entidad.PrecioUltCosto _
         , entidad.PrecioCosto _
         , entidad.InformacionAdicional _
         , entidad.IdCuentaIng _
         , entidad.IdCuentaInv _
         , entidad.IdCuentaCos _
         , entidad.ArchivoImagen _
         , entidad.EstadoProducto _
         , entidad.Compuesto _
         , entidad.PorcComision _
         , entidad.IdClasificacion _
         , entidad.IdCentro _
         , entidad.Traduccion _
         , entidad.Origen _
         , entidad.Cuantia _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub inv_ProductosInsert(ByVal entidad As inv_Productos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosInsert", _
         entidad.IdProducto _
         , entidad.CodBarra _
         , entidad.Nombre _
         , entidad.IdGrupo _
         , entidad.IdSubGrupo _
         , entidad.IdUnidad _
         , entidad.IdMarca _
         , entidad.IdProveedor _
         , entidad.IdUbicacion _
         , entidad.IdCategoria _
         , entidad.Talla _
         , entidad.Color _
         , entidad.Estilo _
         , entidad.UnidadesPresentacion _
         , entidad.ExistenciaMaxima _
         , entidad.ExistenciaMinima _
         , entidad.TipoProducto _
         , entidad.EsExento _
         , entidad.EsEspecial _
         , entidad.PermiteFacturar _
         , entidad.FechaIngreso _
         , entidad.PrecioUltCosto _
         , entidad.PrecioCosto _
         , entidad.InformacionAdicional _
         , entidad.IdCuentaIng _
         , entidad.IdCuentaInv _
         , entidad.IdCuentaCos _
         , entidad.ArchivoImagen _
         , entidad.EstadoProducto _
         , entidad.Compuesto _
         , entidad.PorcComision _
         , entidad.IdClasificacion _
         , entidad.IdCentro _
                  , entidad.Traduccion _
         , entidad.Origen _
         , entidad.Cuantia _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub inv_ProductosUpdate(ByVal entidad As inv_Productos)

        db.ExecuteNonQuery("inv_ProductosUpdate", _
         entidad.IdProducto _
         , entidad.CodBarra _
         , entidad.Nombre _
         , entidad.IdGrupo _
         , entidad.IdSubGrupo _
         , entidad.IdUnidad _
         , entidad.IdMarca _
         , entidad.IdProveedor _
         , entidad.IdUbicacion _
         , entidad.IdCategoria _
         , entidad.Talla _
         , entidad.Color _
         , entidad.Estilo _
         , entidad.UnidadesPresentacion _
         , entidad.ExistenciaMaxima _
         , entidad.ExistenciaMinima _
         , entidad.TipoProducto _
         , entidad.EsExento _
         , entidad.EsEspecial _
         , entidad.PermiteFacturar _
         , entidad.FechaIngreso _
         , entidad.PrecioUltCosto _
         , entidad.PrecioCosto _
         , entidad.InformacionAdicional _
         , entidad.IdCuentaIng _
         , entidad.IdCuentaInv _
         , entidad.IdCuentaCos _
         , entidad.ArchivoImagen _
         , entidad.EstadoProducto _
         , entidad.Compuesto _
         , entidad.PorcComision _
         , entidad.IdClasificacion _
         , entidad.IdCentro _
                  , entidad.Traduccion _
         , entidad.Origen _
         , entidad.Cuantia _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub inv_ProductosUpdate(ByVal entidad As inv_Productos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosUpdate", _
         entidad.IdProducto _
         , entidad.CodBarra _
         , entidad.Nombre _
         , entidad.IdGrupo _
         , entidad.IdSubGrupo _
         , entidad.IdUnidad _
         , entidad.IdMarca _
         , entidad.IdProveedor _
         , entidad.IdUbicacion _
         , entidad.IdCategoria _
         , entidad.Talla _
         , entidad.Color _
         , entidad.Estilo _
         , entidad.UnidadesPresentacion _
         , entidad.ExistenciaMaxima _
         , entidad.ExistenciaMinima _
         , entidad.TipoProducto _
         , entidad.EsExento _
         , entidad.EsEspecial _
         , entidad.PermiteFacturar _
         , entidad.FechaIngreso _
         , entidad.PrecioUltCosto _
         , entidad.PrecioCosto _
         , entidad.InformacionAdicional _
         , entidad.IdCuentaIng _
         , entidad.IdCuentaInv _
         , entidad.IdCuentaCos _
         , entidad.ArchivoImagen _
         , entidad.EstadoProducto _
         , entidad.Compuesto _
         , entidad.PorcComision _
         , entidad.IdClasificacion _
         , entidad.IdCentro _
                  , entidad.Traduccion _
         , entidad.Origen _
         , entidad.Cuantia _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "inv_ProductosDetalle"
    Public Function inv_ProductosDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_ProductosDetalleSelectAll").Tables(0)
    End Function

    Public Function inv_ProductosDetalleSelectByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_ProductosDetalle

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_ProductosDetalleSelectByPK", _
         IdProducto _
         , IdDetalle _
         ).Tables(0)

        Dim Entidad As New inv_ProductosDetalle
        If dt.Rows.Count > 0 Then
            Entidad.IdProducto = dt.Rows(0).Item("IdProducto")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Valor = dt.Rows(0).Item("Valor")

        End If
        Return Entidad
    End Function

    Public Sub inv_ProductosDetalleDeleteByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("inv_ProductosDetalleDeleteByPK", _
         IdProducto _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_ProductosDetalleDeleteByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosDetalleDeleteByPK", _
         IdProducto _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_ProductosDetalleInsert _
    (ByVal entidad As inv_ProductosDetalle)

        db.ExecuteNonQuery("inv_ProductosDetalleInsert", _
         entidad.IdProducto _
         , entidad.IdDetalle _
         , entidad.Nombre _
         , entidad.Valor _
         )
    End Sub

    Public Sub inv_ProductosDetalleInsert _
    (ByVal entidad As inv_ProductosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosDetalleInsert", _
         entidad.IdProducto _
         , entidad.IdDetalle _
         , entidad.Nombre _
         , entidad.Valor _
         )
    End Sub

    Public Sub inv_ProductosDetalleUpdate _
    (ByVal entidad As inv_ProductosDetalle)

        db.ExecuteNonQuery("inv_ProductosDetalleUpdate", _
         entidad.IdProducto _
         , entidad.IdDetalle _
         , entidad.Nombre _
         , entidad.Valor _
         )
    End Sub

    Public Sub inv_ProductosDetalleUpdate _
    (ByVal entidad As inv_ProductosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosDetalleUpdate", _
         entidad.IdProducto _
         , entidad.IdDetalle _
         , entidad.Nombre _
         , entidad.Valor _
         )
    End Sub

#End Region
#Region "inv_ProductosPrecios"
    Public Function inv_ProductosPreciosSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_ProductosPreciosSelectAll").Tables(0)
    End Function

    Public Function inv_ProductosPreciosSelectByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdPrecio As System.Int32 _
      ) As inv_ProductosPrecios

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_ProductosPreciosSelectByPK", _
         IdProducto _
         , IdPrecio _
         ).Tables(0)

        Dim Entidad As New inv_ProductosPrecios
        If dt.Rows.Count > 0 Then
            Entidad.IdProducto = dt.Rows(0).Item("IdProducto")
            Entidad.IdPrecio = dt.Rows(0).Item("IdPrecio")
            Entidad.Precio = dt.Rows(0).Item("Precio")
            Entidad.Descuento = dt.Rows(0).Item("Descuento")
            Entidad.Margen = dt.Rows(0).Item("Margen")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub inv_ProductosPreciosDeleteByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdPrecio As System.Int32 _
      )

        db.ExecuteNonQuery("inv_ProductosPreciosDeleteByPK", _
         IdProducto _
         , IdPrecio _
         )
    End Sub

    Public Sub inv_ProductosPreciosDeleteByPK _
      (ByVal IdProducto As System.Int32 _
      , ByVal IdPrecio As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosPreciosDeleteByPK", _
         IdProducto _
         , IdPrecio _
         )
    End Sub

    Public Sub inv_ProductosPreciosInsert _
    (ByVal entidad As inv_ProductosPrecios)

        db.ExecuteNonQuery("inv_ProductosPreciosInsert", _
         entidad.IdProducto _
         , entidad.IdPrecio _
         , entidad.Precio _
          , entidad.Descuento _
           , entidad.Margen _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_ProductosPreciosInsert _
    (ByVal entidad As inv_ProductosPrecios, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosPreciosInsert", _
         entidad.IdProducto _
         , entidad.IdPrecio _
         , entidad.Precio _
                   , entidad.Descuento _
           , entidad.Margen _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_ProductosPreciosUpdate _
    (ByVal entidad As inv_ProductosPrecios)

        db.ExecuteNonQuery("inv_ProductosPreciosUpdate", _
         entidad.IdProducto _
         , entidad.IdPrecio _
         , entidad.Precio _
                   , entidad.Descuento _
           , entidad.Margen _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_ProductosPreciosUpdate _
    (ByVal entidad As inv_ProductosPrecios, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosPreciosUpdate", _
         entidad.IdProducto _
         , entidad.IdPrecio _
         , entidad.Precio _
                   , entidad.Descuento _
           , entidad.Margen _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion)
    End Sub

#End Region
#Region "inv_SubGrupos"
    Public Function inv_SubGruposSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_SubGruposSelectAll").Tables(0)
    End Function

    Public Function inv_SubGruposSelectByPK(ByVal IdSubGrupo As System.Int32) As inv_SubGrupos

        Dim dt As DataTable = db.ExecuteDataSet("inv_SubGruposSelectByPK", IdSubGrupo).Tables(0)

        Dim Entidad As New inv_SubGrupos
        If dt.Rows.Count > 0 Then
            Entidad.IdSubGrupo = dt.Rows(0).Item("IdSubGrupo")
            Entidad.IdGrupo = dt.Rows(0).Item("IdGrupo")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
        End If
        Return Entidad
    End Function

    Public Sub inv_SubGruposDeleteByPK(ByVal IdSubGrupo As System.Int32)
        db.ExecuteNonQuery("inv_SubGruposDeleteByPK", IdSubGrupo)
    End Sub

    Public Sub inv_SubGruposDeleteByPK _
      (ByVal IdSubGrupo As System.Int32, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_SubGruposDeleteByPK", IdSubGrupo)
    End Sub

    Public Sub inv_SubGruposInsert(ByVal entidad As inv_SubGrupos)

        db.ExecuteNonQuery("inv_SubGruposInsert", _
                           entidad.IdSubGrupo, entidad.IdGrupo, entidad.Nombre)
    End Sub

    Public Sub inv_SubGruposInsert _
    (ByVal entidad As inv_SubGrupos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_SubGruposInsert", entidad.IdSubGrupo, entidad.IdGrupo, entidad.Nombre)
    End Sub

    Public Sub inv_SubGruposUpdate _
    (ByVal entidad As inv_SubGrupos)

        db.ExecuteNonQuery("inv_SubGruposUpdate", _
         entidad.IdGrupo, entidad.IdSubGrupo _
         , entidad.Nombre)
    End Sub

    Public Sub inv_SubGruposUpdate _
    (ByVal entidad As inv_SubGrupos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_SubGruposUpdate", entidad.IdSubGrupo, entidad.IdGrupo, entidad.Nombre)
    End Sub

#End Region
#Region "inv_Ubicaciones"
    Public Function inv_UbicacionesSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_UbicacionesSelectAll").Tables(0)
    End Function

    Public Function inv_UbicacionesSelectByPK _
      (ByVal IdUbicacion As System.Int32 _
      ) As inv_Ubicaciones

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_UbicacionesSelectByPK", _
         IdUbicacion _
         ).Tables(0)

        Dim Entidad As New inv_Ubicaciones
        If dt.Rows.Count > 0 Then
            Entidad.IdUbicacion = dt.Rows(0).Item("IdUbicacion")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub inv_UbicacionesDeleteByPK _
      (ByVal IdUbicacion As System.Int32 _
      )

        db.ExecuteNonQuery("inv_UbicacionesDeleteByPK", _
         IdUbicacion _
         )
    End Sub

    Public Sub inv_UbicacionesDeleteByPK _
      (ByVal IdUbicacion As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_UbicacionesDeleteByPK", _
         IdUbicacion _
         )
    End Sub

    Public Sub inv_UbicacionesInsert _
    (ByVal entidad As inv_Ubicaciones)

        db.ExecuteNonQuery("inv_UbicacionesInsert", _
         entidad.IdUbicacion _
         , entidad.Nombre _
         )
    End Sub

    Public Sub inv_UbicacionesInsert _
    (ByVal entidad As inv_Ubicaciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_UbicacionesInsert", _
         entidad.IdUbicacion _
         , entidad.Nombre _
         )
    End Sub

    Public Sub inv_UbicacionesUpdate _
    (ByVal entidad As inv_Ubicaciones)

        db.ExecuteNonQuery("inv_UbicacionesUpdate", _
         entidad.IdUbicacion _
         , entidad.Nombre _
         )
    End Sub

    Public Sub inv_UbicacionesUpdate _
    (ByVal entidad As inv_Ubicaciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_UbicacionesUpdate", _
         entidad.IdUbicacion _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "inv_UnidadesMedida"
    Public Function inv_UnidadesMedidaSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_UnidadesMedidaSelectAll").Tables(0)
    End Function

    Public Function inv_UnidadesMedidaSelectByPK _
      (ByVal IdUnidad As System.Int32 _
      ) As inv_UnidadesMedida

        Dim dt As DataTable
        dt = db.ExecuteDataSet("inv_UnidadesMedidaSelectByPK", _
         IdUnidad _
         ).Tables(0)

        Dim Entidad As New inv_UnidadesMedida
        If dt.Rows.Count > 0 Then
            Entidad.IdUnidad = dt.Rows(0).Item("IdUnidad")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub inv_UnidadesMedidaDeleteByPK _
      (ByVal IdUnidad As System.Int32 _
      )

        db.ExecuteNonQuery("inv_UnidadesMedidaDeleteByPK", _
         IdUnidad _
         )
    End Sub

    Public Sub inv_UnidadesMedidaDeleteByPK _
      (ByVal IdUnidad As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_UnidadesMedidaDeleteByPK", _
         IdUnidad _
         )
    End Sub

    Public Sub inv_UnidadesMedidaInsert _
    (ByVal entidad As inv_UnidadesMedida)

        db.ExecuteNonQuery("inv_UnidadesMedidaInsert", _
         entidad.IdUnidad _
         , entidad.Nombre _
         )
    End Sub

    Public Sub inv_UnidadesMedidaInsert _
    (ByVal entidad As inv_UnidadesMedida, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_UnidadesMedidaInsert", _
         entidad.IdUnidad _
         , entidad.Nombre _
         )
    End Sub

    Public Sub inv_UnidadesMedidaUpdate _
    (ByVal entidad As inv_UnidadesMedida)

        db.ExecuteNonQuery("inv_UnidadesMedidaUpdate", _
         entidad.IdUnidad _
         , entidad.Nombre _
         )
    End Sub

    Public Sub inv_UnidadesMedidaUpdate _
    (ByVal entidad As inv_UnidadesMedida, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_UnidadesMedidaUpdate", _
         entidad.IdUnidad _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "inv_Formulas"
    Public Function inv_FormulasSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_FormulasSelectAll").Tables(0)
    End Function

    Public Function inv_FormulasSelectByPK _
      (ByVal IdFormula As System.Int32 _
      ) As inv_Formulas

        Dim dt As datatable
        dt = db.ExecuteDataSet("inv_FormulasSelectByPK", _
         IdFormula _
         ).tables(0)

        Dim Entidad As New inv_Formulas
        If dt.Rows.Count > 0 Then
            entidad.IdFormula = dt.rows(0).item("IdFormula")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub inv_FormulasDeleteByPK _
      (ByVal IdFormula As System.Int32 _
      )

        db.ExecuteNonQuery("inv_FormulasDeleteByPK", _
         IdFormula _
         )
    End Sub

    Public Sub inv_FormulasDeleteByPK _
      (ByVal IdFormula As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_FormulasDeleteByPK", _
         IdFormula _
         )
    End Sub

    Public Sub inv_FormulasInsert _
    (ByVal entidad As inv_Formulas)

        db.ExecuteNonQuery("inv_FormulasInsert", _
         entidad.IdFormula _
         , entidad.IdProducto _
         , entidad.Nombre _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_FormulasInsert _
    (ByVal entidad As inv_Formulas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_FormulasInsert", _
         entidad.IdFormula _
         , entidad.IdProducto _
         , entidad.Nombre _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_FormulasUpdate _
    (ByVal entidad As inv_Formulas)

        db.ExecuteNonQuery("inv_FormulasUpdate", _
         entidad.IdFormula _
         , entidad.IdProducto _
         , entidad.Nombre _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_FormulasUpdate _
    (ByVal entidad As inv_Formulas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_FormulasUpdate", _
         entidad.IdFormula _
         , entidad.IdProducto _
         , entidad.Nombre _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "inv_FormulasDetalle"
    Public Function inv_FormulasDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_FormulasDetalleSelectAll").Tables(0)
    End Function

    Public Function inv_FormulasDetalleSelectByPK _
      (ByVal IdFormula As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_FormulasDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("inv_FormulasDetalleSelectByPK", _
         IdFormula _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New inv_FormulasDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdFormula = dt.rows(0).item("IdFormula")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.PrecioCosto = dt.rows(0).item("PrecioCosto")
            entidad.PrecioTotal = dt.rows(0).item("PrecioTotal")

        End If
        Return Entidad
    End Function

    Public Sub inv_FormulasDetalleDeleteByPK _
      (ByVal IdFormula As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("inv_FormulasDetalleDeleteByPK", _
         IdFormula _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_FormulasDetalleDeleteByPK _
      (ByVal IdFormula As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_FormulasDetalleDeleteByPK", _
         IdFormula _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_FormulasDetalleInsert _
    (ByVal entidad As inv_FormulasDetalle)

        db.ExecuteNonQuery("inv_FormulasDetalleInsert", _
         entidad.IdFormula _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Nombre _
         , entidad.Cantidad _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         )
    End Sub

    Public Sub inv_FormulasDetalleInsert _
    (ByVal entidad As inv_FormulasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_FormulasDetalleInsert", _
         entidad.IdFormula _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Nombre _
         , entidad.Cantidad _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         )
    End Sub

    Public Sub inv_FormulasDetalleUpdate _
    (ByVal entidad As inv_FormulasDetalle)

        db.ExecuteNonQuery("inv_FormulasDetalleUpdate", _
         entidad.IdFormula _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Nombre _
         , entidad.Cantidad _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         )
    End Sub

    Public Sub inv_FormulasDetalleUpdate _
    (ByVal entidad As inv_FormulasDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_FormulasDetalleUpdate", _
         entidad.IdFormula _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Nombre _
         , entidad.Cantidad _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         )
    End Sub

#End Region
#Region "inv_GastosProduccion"
    Public Function inv_GastosProduccionSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_GastosProduccionSelectAll").Tables(0)
    End Function

    Public Function inv_GastosProduccionSelectByPK _
      (ByVal IdGasto As System.Int32 _
      ) As inv_GastosProduccion

        Dim dt As datatable
        dt = db.ExecuteDataSet("inv_GastosProduccionSelectByPK", _
         IdGasto _
         ).tables(0)

        Dim Entidad As New inv_GastosProduccion
        If dt.Rows.Count > 0 Then
            entidad.IdGasto = dt.rows(0).item("IdGasto")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.IdCuenta = dt.rows(0).item("IdCuenta")

        End If
        Return Entidad
    End Function

    Public Sub inv_GastosProduccionDeleteByPK _
      (ByVal IdGasto As System.Int32 _
      )

        db.ExecuteNonQuery("inv_GastosProduccionDeleteByPK", _
         IdGasto _
         )
    End Sub

    Public Sub inv_GastosProduccionDeleteByPK _
      (ByVal IdGasto As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_GastosProduccionDeleteByPK", _
         IdGasto _
         )
    End Sub

    Public Sub inv_GastosProduccionInsert _
    (ByVal entidad As inv_GastosProduccion)

        db.ExecuteNonQuery("inv_GastosProduccionInsert", _
         entidad.IdGasto _
         , entidad.Nombre _
         , entidad.IdCuenta _
         )
    End Sub

    Public Sub inv_GastosProduccionInsert _
    (ByVal entidad As inv_GastosProduccion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_GastosProduccionInsert", _
         entidad.IdGasto _
         , entidad.Nombre _
         , entidad.IdCuenta _
         )
    End Sub

    Public Sub inv_GastosProduccionUpdate _
    (ByVal entidad As inv_GastosProduccion)

        db.ExecuteNonQuery("inv_GastosProduccionUpdate", _
         entidad.IdGasto _
         , entidad.Nombre _
         , entidad.IdCuenta _
         )
    End Sub

    Public Sub inv_GastosProduccionUpdate _
    (ByVal entidad As inv_GastosProduccion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_GastosProduccionUpdate", _
         entidad.IdGasto _
         , entidad.Nombre _
         , entidad.IdCuenta _
         )
    End Sub

#End Region
#Region "inv_ProduccionesGastosDetalle"
    Public Function inv_ProduccionesGastosDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_ProduccionesGastosDetalleSelectAll").Tables(0)
    End Function

    Public Function inv_ProduccionesGastosDetalleSelectByPK _
      (ByVal IdProduccion As System.Int32 _
      , ByVal IdGasto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As inv_ProduccionesGastosDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("inv_ProduccionesGastosDetalleSelectByPK", _
         IdProduccion _
         , IdGasto _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New inv_ProduccionesGastosDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdProduccion = dt.rows(0).item("IdProduccion")
            entidad.IdGasto = dt.rows(0).item("IdGasto")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.Valor = dt.rows(0).item("Valor")
            entidad.Contabilizar = dt.rows(0).item("Contabilizar")

        End If
        Return Entidad
    End Function

    Public Sub inv_ProduccionesGastosDetalleDeleteByPK _
      (ByVal IdProduccion As System.Int32 _
      , ByVal IdGasto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("inv_ProduccionesGastosDetalleDeleteByPK", _
         IdProduccion _
         , IdGasto _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_ProduccionesGastosDetalleDeleteByPK _
      (ByVal IdProduccion As System.Int32 _
      , ByVal IdGasto As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProduccionesGastosDetalleDeleteByPK", _
         IdProduccion _
         , IdGasto _
         , IdDetalle _
         )
    End Sub

    Public Sub inv_ProduccionesGastosDetalleInsert _
    (ByVal entidad As inv_ProduccionesGastosDetalle)

        db.ExecuteNonQuery("inv_ProduccionesGastosDetalleInsert", _
         entidad.IdProduccion _
         , entidad.IdGasto _
         , entidad.IdDetalle _
         , entidad.Concepto _
         , entidad.Valor _
         , entidad.Contabilizar _
         )
    End Sub

    Public Sub inv_ProduccionesGastosDetalleInsert _
    (ByVal entidad As inv_ProduccionesGastosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProduccionesGastosDetalleInsert", _
         entidad.IdProduccion _
         , entidad.IdGasto _
         , entidad.IdDetalle _
         , entidad.Concepto _
         , entidad.Valor _
         , entidad.Contabilizar _
         )
    End Sub

    Public Sub inv_ProduccionesGastosDetalleUpdate _
    (ByVal entidad As inv_ProduccionesGastosDetalle)

        db.ExecuteNonQuery("inv_ProduccionesGastosDetalleUpdate", _
         entidad.IdProduccion _
         , entidad.IdGasto _
         , entidad.IdDetalle _
         , entidad.Concepto _
         , entidad.Valor _
         , entidad.Contabilizar _
         )
    End Sub

    Public Sub inv_ProduccionesGastosDetalleUpdate _
    (ByVal entidad As inv_ProduccionesGastosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProduccionesGastosDetalleUpdate", _
         entidad.IdProduccion _
         , entidad.IdGasto _
         , entidad.IdDetalle _
         , entidad.Concepto _
         , entidad.Valor _
         , entidad.Contabilizar _
         )
    End Sub

#End Region

#Region "inv_ProductosMargenes"
    Public Function inv_ProductosMargenesSelectAll() As DataTable
        Return db.ExecuteDataSet("inv_ProductosMargenesSelectAll").Tables(0)
    End Function

    Public Function inv_ProductosMargenesSelectByPK _
      (ByVal IdMargen As System.Int32 _
      ) As inv_ProductosMargenes

        Dim dt As datatable
        dt = db.ExecuteDataSet("inv_ProductosMargenesSelectByPK", _
         IdMargen _
         ).tables(0)

        Dim Entidad As New inv_ProductosMargenes
        If dt.Rows.Count > 0 Then
            entidad.IdMargen = dt.rows(0).item("IdMargen")
            entidad.Desde = dt.rows(0).item("Desde")
            entidad.Hasta = dt.rows(0).item("Hasta")
            entidad.Valor = dt.rows(0).item("Valor")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub inv_ProductosMargenesDeleteByPK _
      (ByVal IdMargen As System.Int32 _
      )

        db.ExecuteNonQuery("inv_ProductosMargenesDeleteByPK", _
         IdMargen _
         )
    End Sub

    Public Sub inv_ProductosMargenesDeleteByPK _
      (ByVal IdMargen As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosMargenesDeleteByPK", _
         IdMargen _
         )
    End Sub

    Public Sub inv_ProductosMargenesInsert _
    (ByVal entidad As inv_ProductosMargenes)

        db.ExecuteNonQuery("inv_ProductosMargenesInsert", _
         entidad.IdMargen _
         , entidad.Desde _
         , entidad.Hasta _
         , entidad.Valor _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_ProductosMargenesInsert _
    (ByVal entidad As inv_ProductosMargenes, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosMargenesInsert", _
         entidad.IdMargen _
         , entidad.Desde _
         , entidad.Hasta _
         , entidad.Valor _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_ProductosMargenesUpdate _
    (ByVal entidad As inv_ProductosMargenes)

        db.ExecuteNonQuery("inv_ProductosMargenesUpdate", _
         entidad.IdMargen _
         , entidad.Desde _
         , entidad.Hasta _
         , entidad.Valor _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub inv_ProductosMargenesUpdate _
    (ByVal entidad As inv_ProductosMargenes, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "inv_ProductosMargenesUpdate", _
         entidad.IdMargen _
         , entidad.Desde _
         , entidad.Hasta _
         , entidad.Valor _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region

#Region "pro_Actividades"
    Public Function pro_ActividadesSelectAll() As DataTable
        Return db.ExecuteDataSet("pro_ActividadesSelectAll").Tables(0)
    End Function

    Public Function pro_ActividadesSelectByPK _
      (ByVal IdActividad As System.Int32 _
      ) As pro_Actividades

        Dim dt As datatable
        dt = db.ExecuteDataSet("pro_ActividadesSelectByPK", _
         IdActividad _
         ).tables(0)

        Dim Entidad As New pro_Actividades
        If dt.Rows.Count > 0 Then
            entidad.IdActividad = dt.rows(0).item("IdActividad")
            entidad.Nombre = dt.rows(0).item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub pro_ActividadesDeleteByPK _
      (ByVal IdActividad As System.Int32 _
      )

        db.ExecuteNonQuery("pro_ActividadesDeleteByPK", _
         IdActividad _
         )
    End Sub

    Public Sub pro_ActividadesDeleteByPK _
      (ByVal IdActividad As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_ActividadesDeleteByPK", _
         IdActividad _
         )
    End Sub

    Public Sub pro_ActividadesInsert _
    (ByVal entidad As pro_Actividades)

        db.ExecuteNonQuery("pro_ActividadesInsert", _
         entidad.IdActividad _
         , entidad.Nombre _
         )
    End Sub

    Public Sub pro_ActividadesInsert _
    (ByVal entidad As pro_Actividades, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_ActividadesInsert", _
         entidad.IdActividad _
         , entidad.Nombre _
         )
    End Sub

    Public Sub pro_ActividadesUpdate _
    (ByVal entidad As pro_Actividades)

        db.ExecuteNonQuery("pro_ActividadesUpdate", _
         entidad.IdActividad _
         , entidad.Nombre _
         )
    End Sub

    Public Sub pro_ActividadesUpdate _
    (ByVal entidad As pro_Actividades, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_ActividadesUpdate", _
         entidad.IdActividad _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "pro_OrdenesProduccion"
    Public Function pro_OrdenesProduccionSelectAll() As DataTable
        Return db.ExecuteDataSet("pro_OrdenesProduccionSelectAll").Tables(0)
    End Function

    Public Function pro_OrdenesProduccionSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As pro_OrdenesProduccion

        Dim dt As datatable
        dt = db.ExecuteDataSet("pro_OrdenesProduccionSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New pro_OrdenesProduccion
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.FechaOrden = dt.rows(0).item("FechaOrden")
            entidad.FechaEntrega = dt.rows(0).item("FechaEntrega")
            entidad.NumeroOrden = dt.rows(0).item("NumeroOrden")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Cantidad = dt.Rows(0).Item("Cantidad")
            Entidad.CantidadUG = dt.Rows(0).Item("CantidadUG")
            entidad.IdEmpleado = dt.rows(0).item("IdEmpleado")
            entidad.IdEmpleadoAutoriza = dt.rows(0).item("IdEmpleadoAutoriza")
            Entidad.IdActividad = dt.Rows(0).Item("IdActividad")
            Entidad.IdBodega = dt.Rows(0).Item("IdBodega")
            Entidad.Aplicada = dt.Rows(0).Item("Aplicada")
            entidad.Observacion = dt.rows(0).item("Observacion")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")

        End If
        Return Entidad
    End Function

    Public Sub pro_OrdenesProduccionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("pro_OrdenesProduccionDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub pro_OrdenesProduccionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesProduccionDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub pro_OrdenesProduccionInsert _
    (ByVal entidad As pro_OrdenesProduccion)

        db.ExecuteNonQuery("pro_OrdenesProduccionInsert", _
         entidad.IdComprobante _
         , entidad.FechaOrden _
         , entidad.FechaEntrega _
         , entidad.NumeroOrden _
         , entidad.IdProducto _
         , entidad.Nombre _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.IdEmpleado _
         , entidad.IdEmpleadoAutoriza _
         , entidad.IdActividad _
         , entidad.IdBodega _
         , entidad.Aplicada _
         , entidad.Observacion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.FechaHoraModificacion _
         , entidad.ModificadoPor _
         )
    End Sub

    Public Sub pro_OrdenesProduccionInsert _
    (ByVal entidad As pro_OrdenesProduccion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesProduccionInsert", _
         entidad.IdComprobante _
         , entidad.FechaOrden _
         , entidad.FechaEntrega _
         , entidad.NumeroOrden _
         , entidad.IdProducto _
         , entidad.Nombre _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.IdEmpleado _
         , entidad.IdEmpleadoAutoriza _
         , entidad.IdActividad _
                  , entidad.IdBodega _
         , entidad.Aplicada _
         , entidad.Observacion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.FechaHoraModificacion _
         , entidad.ModificadoPor _
         )
    End Sub

    Public Sub pro_OrdenesProduccionUpdate _
    (ByVal entidad As pro_OrdenesProduccion)

        db.ExecuteNonQuery("pro_OrdenesProduccionUpdate", _
         entidad.IdComprobante _
         , entidad.FechaOrden _
         , entidad.FechaEntrega _
         , entidad.NumeroOrden _
         , entidad.IdProducto _
         , entidad.Nombre _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.IdEmpleado _
         , entidad.IdEmpleadoAutoriza _
         , entidad.IdActividad _
                  , entidad.IdBodega _
         , entidad.Aplicada _
         , entidad.Observacion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.FechaHoraModificacion _
         , entidad.ModificadoPor _
         )
    End Sub

    Public Sub pro_OrdenesProduccionUpdate _
    (ByVal entidad As pro_OrdenesProduccion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesProduccionUpdate", _
         entidad.IdComprobante _
         , entidad.FechaOrden _
         , entidad.FechaEntrega _
         , entidad.NumeroOrden _
         , entidad.IdProducto _
         , entidad.Nombre _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.IdEmpleado _
         , entidad.IdEmpleadoAutoriza _
         , entidad.IdActividad _
                  , entidad.IdBodega _
         , entidad.Aplicada _
         , entidad.Observacion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.FechaHoraModificacion _
         , entidad.ModificadoPor _
         )
    End Sub

#End Region
#Region "pro_OrdenesProduccionDetalleManoObra"
    Public Function pro_OrdenesProduccionDetalleManoObraSelectAll() As DataTable
        Return db.ExecuteDataSet("pro_OrdenesProduccionDetalleManoObraSelectAll").Tables(0)
    End Function

    Public Function pro_OrdenesProduccionDetalleManoObraSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_OrdenesProduccionDetalleManoObra

        Dim dt As datatable
        dt = db.ExecuteDataSet("pro_OrdenesProduccionDetalleManoObraSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New pro_OrdenesProduccionDetalleManoObra
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdEmpleado = dt.rows(0).item("IdEmpleado")
            entidad.NombreEmpleado = dt.rows(0).item("NombreEmpleado")
            entidad.HoraInicio = dt.rows(0).item("HoraInicio")
            entidad.HoraFin = dt.rows(0).item("HoraFin")
            entidad.IdActividad = dt.rows(0).item("IdActividad")
            entidad.Total = dt.rows(0).item("Total")

        End If
        Return Entidad
    End Function

    Public Sub pro_OrdenesProduccionDetalleManoObraDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("pro_OrdenesProduccionDetalleManoObraDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesProduccionDetalleManoObraDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesProduccionDetalleManoObraDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesProduccionDetalleManoObraInsert _
    (ByVal entidad As pro_OrdenesProduccionDetalleManoObra)

        db.ExecuteNonQuery("pro_OrdenesProduccionDetalleManoObraInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.IdEmpleado _
         , entidad.NombreEmpleado _
         , entidad.HoraInicio _
         , entidad.HoraFin _
         , entidad.IdActividad _
         , entidad.Total _
         )
    End Sub

    Public Sub pro_OrdenesProduccionDetalleManoObraInsert _
    (ByVal entidad As pro_OrdenesProduccionDetalleManoObra, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesProduccionDetalleManoObraInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.IdEmpleado _
         , entidad.NombreEmpleado _
         , entidad.HoraInicio _
         , entidad.HoraFin _
         , entidad.IdActividad _
         , entidad.Total _
         )
    End Sub

    Public Sub pro_OrdenesProduccionDetalleManoObraUpdate _
    (ByVal entidad As pro_OrdenesProduccionDetalleManoObra)

        db.ExecuteNonQuery("pro_OrdenesProduccionDetalleManoObraUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.IdEmpleado _
         , entidad.NombreEmpleado _
         , entidad.HoraInicio _
         , entidad.HoraFin _
         , entidad.IdActividad _
         , entidad.Total _
         )
    End Sub

    Public Sub pro_OrdenesProduccionDetalleManoObraUpdate _
    (ByVal entidad As pro_OrdenesProduccionDetalleManoObra, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesProduccionDetalleManoObraUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.IdEmpleado _
         , entidad.NombreEmpleado _
         , entidad.HoraInicio _
         , entidad.HoraFin _
         , entidad.IdActividad _
         , entidad.Total _
         )
    End Sub

#End Region
#Region "pro_EntradasProduccion"
    Public Function pro_EntradasProduccionSelectAll() As DataTable
        Return db.ExecuteDataSet("pro_EntradasProduccionSelectAll").Tables(0)
    End Function

    Public Function pro_EntradasProduccionSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As pro_EntradasProduccion

        Dim dt As datatable
        dt = db.ExecuteDataSet("pro_EntradasProduccionSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New pro_EntradasProduccion
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdTipoComprobante = dt.rows(0).item("IdTipoComprobante")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.IdBodega = dt.rows(0).item("IdBodega")
            entidad.IdOrden = dt.rows(0).item("IdOrden")
            entidad.AplicadaInventario = dt.rows(0).item("AplicadaInventario")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub pro_EntradasProduccionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("pro_EntradasProduccionDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub pro_EntradasProduccionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_EntradasProduccionDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub pro_EntradasProduccionInsert _
    (ByVal entidad As pro_EntradasProduccion)

        db.ExecuteNonQuery("pro_EntradasProduccionInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdOrden _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub pro_EntradasProduccionInsert _
    (ByVal entidad As pro_EntradasProduccion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_EntradasProduccionInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdOrden _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub pro_EntradasProduccionUpdate _
    (ByVal entidad As pro_EntradasProduccion)

        db.ExecuteNonQuery("pro_EntradasProduccionUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdOrden _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub pro_EntradasProduccionUpdate _
    (ByVal entidad As pro_EntradasProduccion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_EntradasProduccionUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdOrden _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "pro_EntradasProduccionDetalle"
    Public Function pro_EntradasProduccionDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("pro_EntradasProduccionDetalleSelectAll").Tables(0)
    End Function

    Public Function pro_EntradasProduccionDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_EntradasProduccionDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("pro_EntradasProduccionDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New pro_EntradasProduccionDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.CantidadUG = dt.rows(0).item("CantidadUG")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.PrecioUnitario = dt.rows(0).item("PrecioUnitario")
            entidad.PrecioCosto = dt.rows(0).item("PrecioCosto")
            entidad.PrecioTotal = dt.rows(0).item("PrecioTotal")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub pro_EntradasProduccionDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("pro_EntradasProduccionDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_EntradasProduccionDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_EntradasProduccionDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_EntradasProduccionDetalleInsert _
    (ByVal entidad As pro_EntradasProduccionDetalle)

        db.ExecuteNonQuery("pro_EntradasProduccionDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub pro_EntradasProduccionDetalleInsert _
    (ByVal entidad As pro_EntradasProduccionDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_EntradasProduccionDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub pro_EntradasProduccionDetalleUpdate _
    (ByVal entidad As pro_EntradasProduccionDetalle)

        db.ExecuteNonQuery("pro_EntradasProduccionDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub pro_EntradasProduccionDetalleUpdate _
    (ByVal entidad As pro_EntradasProduccionDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_EntradasProduccionDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "pro_SalidasProduccion"
    Public Function pro_SalidasProduccionSelectAll() As DataTable
        Return db.ExecuteDataSet("pro_SalidasProduccionSelectAll").Tables(0)
    End Function

    Public Function pro_SalidasProduccionSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As pro_SalidasProduccion

        Dim dt As datatable
        dt = db.ExecuteDataSet("pro_SalidasProduccionSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New pro_SalidasProduccion
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdTipoComprobante = dt.rows(0).item("IdTipoComprobante")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.IdBodega = dt.rows(0).item("IdBodega")
            entidad.IdOrden = dt.rows(0).item("IdOrden")
            entidad.AplicadaInventario = dt.rows(0).item("AplicadaInventario")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub pro_SalidasProduccionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("pro_SalidasProduccionDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub pro_SalidasProduccionDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_SalidasProduccionDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub pro_SalidasProduccionInsert _
    (ByVal entidad As pro_SalidasProduccion)

        db.ExecuteNonQuery("pro_SalidasProduccionInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdOrden _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub pro_SalidasProduccionInsert _
    (ByVal entidad As pro_SalidasProduccion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_SalidasProduccionInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdOrden _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub pro_SalidasProduccionUpdate _
    (ByVal entidad As pro_SalidasProduccion)

        db.ExecuteNonQuery("pro_SalidasProduccionUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdOrden _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub pro_SalidasProduccionUpdate _
    (ByVal entidad As pro_SalidasProduccion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_SalidasProduccionUpdate", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodega _
         , entidad.IdOrden _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "pro_SalidasProduccionDetalle"
    Public Function pro_SalidasProduccionDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("pro_SalidasProduccionDetalleSelectAll").Tables(0)
    End Function

    Public Function pro_SalidasProduccionDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_SalidasProduccionDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("pro_SalidasProduccionDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New pro_SalidasProduccionDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.CantidadUG = dt.rows(0).item("CantidadUG")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.PrecioUnitario = dt.rows(0).item("PrecioUnitario")
            entidad.PrecioCosto = dt.rows(0).item("PrecioCosto")
            entidad.PrecioTotal = dt.rows(0).item("PrecioTotal")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub pro_SalidasProduccionDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("pro_SalidasProduccionDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_SalidasProduccionDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_SalidasProduccionDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_SalidasProduccionDetalleInsert _
    (ByVal entidad As pro_SalidasProduccionDetalle)

        db.ExecuteNonQuery("pro_SalidasProduccionDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub pro_SalidasProduccionDetalleInsert _
    (ByVal entidad As pro_SalidasProduccionDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_SalidasProduccionDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub pro_SalidasProduccionDetalleUpdate _
    (ByVal entidad As pro_SalidasProduccionDetalle)

        db.ExecuteNonQuery("pro_SalidasProduccionDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub pro_SalidasProduccionDetalleUpdate _
    (ByVal entidad As pro_SalidasProduccionDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_SalidasProduccionDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "pro_OrdenesGasDirectos"
    Public Function pro_OrdenesGasDirectosSelectAll() As DataTable
        Return db.ExecuteDataSet("pro_OrdenesGasDirectosSelectAll").Tables(0)
    End Function

    Public Function pro_OrdenesGasDirectosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_OrdenesGasDirectos

        Dim dt As datatable
        dt = db.ExecuteDataSet("pro_OrdenesGasDirectosSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New pro_OrdenesGasDirectos
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            entidad.IdGasto = dt.rows(0).item("IdGasto")
            entidad.Gasto = dt.rows(0).item("Gasto")
            entidad.Importe = dt.rows(0).item("Importe")

        End If
        Return Entidad
    End Function

    Public Sub pro_OrdenesGasDirectosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("pro_OrdenesGasDirectosDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesGasDirectosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesGasDirectosDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesGasDirectosInsert _
    (ByVal entidad As pro_OrdenesGasDirectos)

        db.ExecuteNonQuery("pro_OrdenesGasDirectosInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.IdGasto _
         , entidad.Gasto _
         , entidad.Importe _
         )
    End Sub

    Public Sub pro_OrdenesGasDirectosInsert _
    (ByVal entidad As pro_OrdenesGasDirectos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesGasDirectosInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.IdGasto _
         , entidad.Gasto _
         , entidad.Importe _
         )
    End Sub

    Public Sub pro_OrdenesGasDirectosUpdate _
    (ByVal entidad As pro_OrdenesGasDirectos)

        db.ExecuteNonQuery("pro_OrdenesGasDirectosUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.IdGasto _
         , entidad.Gasto _
         , entidad.Importe _
         )
    End Sub

    Public Sub pro_OrdenesGasDirectosUpdate _
    (ByVal entidad As pro_OrdenesGasDirectos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesGasDirectosUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdGasto _
         , entidad.IdGasto _
         , entidad.Gasto _
         , entidad.Importe _
         )
    End Sub

#End Region
#Region "pro_OrdenesGasIndirectos"
    Public Function pro_OrdenesGasIndirectosSelectAll() As DataTable
        Return db.ExecuteDataSet("pro_OrdenesGasIndirectosSelectAll").Tables(0)
    End Function

    Public Function pro_OrdenesGasIndirectosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_OrdenesGasIndirectos

        Dim dt As datatable
        dt = db.ExecuteDataSet("pro_OrdenesGasIndirectosSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New pro_OrdenesGasIndirectos
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            entidad.IdGasto = dt.rows(0).item("IdGasto")
            entidad.Gasto = dt.rows(0).item("Gasto")
            entidad.Importe = dt.rows(0).item("Importe")

        End If
        Return Entidad
    End Function

    Public Sub pro_OrdenesGasIndirectosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("pro_OrdenesGasIndirectosDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesGasIndirectosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesGasIndirectosDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesGasIndirectosInsert _
    (ByVal entidad As pro_OrdenesGasIndirectos)

        db.ExecuteNonQuery("pro_OrdenesGasIndirectosInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.IdGasto _
         , entidad.Gasto _
         , entidad.Importe _
         )
    End Sub

    Public Sub pro_OrdenesGasIndirectosInsert _
    (ByVal entidad As pro_OrdenesGasIndirectos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesGasIndirectosInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.IdGasto _
         , entidad.Gasto _
         , entidad.Importe _
         )
    End Sub

    Public Sub pro_OrdenesGasIndirectosUpdate _
    (ByVal entidad As pro_OrdenesGasIndirectos)

        db.ExecuteNonQuery("pro_OrdenesGasIndirectosUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.IdGasto _
         , entidad.Gasto _
         , entidad.Importe _
         )
    End Sub

    Public Sub pro_OrdenesGasIndirectosUpdate _
    (ByVal entidad As pro_OrdenesGasIndirectos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesGasIndirectosUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.Fecha _
         , entidad.IdGasto _
         , entidad.Gasto _
         , entidad.Importe _
         )
    End Sub

#End Region
#Region "pro_OrdenesEmpaque"
    Public Function pro_OrdenesEmpaqueSelectAll() As DataTable
        Return db.ExecuteDataSet("pro_OrdenesEmpaqueSelectAll").Tables(0)
    End Function

    Public Function pro_OrdenesEmpaqueSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As pro_OrdenesEmpaque

        Dim dt As datatable
        dt = db.ExecuteDataSet("pro_OrdenesEmpaqueSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New pro_OrdenesEmpaque
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            Entidad.IdTipoComprobanteSal = dt.Rows(0).Item("IdTipoComprobanteSal")
            Entidad.IdTipoComprobanteEnt = dt.Rows(0).Item("IdTipoComprobanteEnt")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.Concepto = dt.rows(0).item("Concepto")
            Entidad.IdBodegaSal = dt.Rows(0).Item("IdBodegaSal")
            Entidad.IdBodegaIng = dt.Rows(0).Item("IdBodegaIng")
            Entidad.IdBodegaIngTerminado = dt.Rows(0).Item("IdBodegaIngTerminado")
            entidad.AplicadaInventario = dt.rows(0).item("AplicadaInventario")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub pro_OrdenesEmpaqueDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("pro_OrdenesEmpaqueDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesEmpaqueDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueInsert _
    (ByVal entidad As pro_OrdenesEmpaque)

        db.ExecuteNonQuery("pro_OrdenesEmpaqueInsert", _
         entidad.IdComprobante _
         , entidad.IdTipoComprobanteSal _
         , entidad.IdTipoComprobanteEnt _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodegaSal _
         , entidad.IdBodegaIng _
         , entidad.IdBodegaIngTerminado _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueInsert _
    (ByVal entidad As pro_OrdenesEmpaque, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesEmpaqueInsert", _
         entidad.IdComprobante _
                  , entidad.IdTipoComprobanteSal _
         , entidad.IdTipoComprobanteEnt _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodegaSal _
         , entidad.IdBodegaIng _
         , entidad.IdBodegaIngTerminado _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueUpdate _
    (ByVal entidad As pro_OrdenesEmpaque)

        db.ExecuteNonQuery("pro_OrdenesEmpaqueUpdate", _
         entidad.IdComprobante _
                  , entidad.IdTipoComprobanteSal _
         , entidad.IdTipoComprobanteEnt _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodegaSal _
         , entidad.IdBodegaIng _
         , entidad.IdBodegaIngTerminado _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueUpdate _
    (ByVal entidad As pro_OrdenesEmpaque, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesEmpaqueUpdate", _
         entidad.IdComprobante _
                  , entidad.IdTipoComprobanteSal _
         , entidad.IdTipoComprobanteEnt _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdBodegaSal _
         , entidad.IdBodegaIng _
         , entidad.IdBodegaIngTerminado _
         , entidad.AplicadaInventario _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "pro_OrdenesEmpaqueDetalleEntrada"
    Public Function pro_OrdenesEmpaqueDetalleEntradaSelectAll() As DataTable
        Return db.ExecuteDataSet("pro_OrdenesEmpaqueDetalleEntradaSelectAll").Tables(0)
    End Function

    Public Function pro_OrdenesEmpaqueDetalleEntradaSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_OrdenesEmpaqueDetalleEntrada

        Dim dt As datatable
        dt = db.ExecuteDataSet("pro_OrdenesEmpaqueDetalleEntradaSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New pro_OrdenesEmpaqueDetalleEntrada
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.CantidadUG = dt.rows(0).item("CantidadUG")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.PrecioUnitario = dt.rows(0).item("PrecioUnitario")
            entidad.PrecioCosto = dt.rows(0).item("PrecioCosto")
            entidad.PrecioTotal = dt.rows(0).item("PrecioTotal")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub pro_OrdenesEmpaqueDetalleEntradaDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("pro_OrdenesEmpaqueDetalleEntradaDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleEntradaDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesEmpaqueDetalleEntradaDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleEntradaInsert _
    (ByVal entidad As pro_OrdenesEmpaqueDetalleEntrada)

        db.ExecuteNonQuery("pro_OrdenesEmpaqueDetalleEntradaInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleEntradaInsert _
    (ByVal entidad As pro_OrdenesEmpaqueDetalleEntrada, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesEmpaqueDetalleEntradaInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleEntradaUpdate _
    (ByVal entidad As pro_OrdenesEmpaqueDetalleEntrada)

        db.ExecuteNonQuery("pro_OrdenesEmpaqueDetalleEntradaUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleEntradaUpdate _
    (ByVal entidad As pro_OrdenesEmpaqueDetalleEntrada, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesEmpaqueDetalleEntradaUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "pro_OrdenesEmpaqueDetalleSalida"
    Public Function pro_OrdenesEmpaqueDetalleSalidaSelectAll() As DataTable
        Return db.ExecuteDataSet("pro_OrdenesEmpaqueDetalleSalidaSelectAll").Tables(0)
    End Function

    Public Function pro_OrdenesEmpaqueDetalleSalidaSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As pro_OrdenesEmpaqueDetalleSalida

        Dim dt As datatable
        dt = db.ExecuteDataSet("pro_OrdenesEmpaqueDetalleSalidaSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New pro_OrdenesEmpaqueDetalleSalida
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdProducto = dt.rows(0).item("IdProducto")
            entidad.Cantidad = dt.rows(0).item("Cantidad")
            entidad.CantidadUG = dt.rows(0).item("CantidadUG")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.PrecioUnitario = dt.rows(0).item("PrecioUnitario")
            entidad.PrecioCosto = dt.rows(0).item("PrecioCosto")
            entidad.PrecioTotal = dt.rows(0).item("PrecioTotal")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub pro_OrdenesEmpaqueDetalleSalidaDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("pro_OrdenesEmpaqueDetalleSalidaDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleSalidaDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesEmpaqueDetalleSalidaDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleSalidaInsert _
    (ByVal entidad As pro_OrdenesEmpaqueDetalleSalida)

        db.ExecuteNonQuery("pro_OrdenesEmpaqueDetalleSalidaInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleSalidaInsert _
    (ByVal entidad As pro_OrdenesEmpaqueDetalleSalida, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesEmpaqueDetalleSalidaInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleSalidaUpdate _
    (ByVal entidad As pro_OrdenesEmpaqueDetalleSalida)

        db.ExecuteNonQuery("pro_OrdenesEmpaqueDetalleSalidaUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub pro_OrdenesEmpaqueDetalleSalidaUpdate _
    (ByVal entidad As pro_OrdenesEmpaqueDetalleSalida, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "pro_OrdenesEmpaqueDetalleSalidaUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdProducto _
         , entidad.Cantidad _
         , entidad.CantidadUG _
         , entidad.Descripcion _
         , entidad.PrecioUnitario _
         , entidad.PrecioCosto _
         , entidad.PrecioTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region


#Region "cpc_Abonos"
    Public Function cpc_AbonosSelectAll() As DataTable
        Return db.ExecuteDataSet("cpc_AbonosSelectAll").Tables(0)
    End Function

    Public Function cpc_AbonosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As cpc_Abonos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("cpc_AbonosSelectByPK", _
         IdComprobante _
         ).Tables(0)

        Dim Entidad As New cpc_Abonos
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.NumeroComprobante = dt.Rows(0).Item("NumeroComprobante")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.IdCuentaBancaria = dt.Rows(0).Item("IdCuentaBancaria")
            Entidad.IdCobrador = dt.Rows(0).Item("IdCobrador")
            Entidad.IdCliente = dt.Rows(0).Item("IdCliente")
            Entidad.IdTipoMov = dt.Rows(0).Item("IdTipoMov")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.Concepto = dt.Rows(0).Item("Concepto")
            Entidad.RecargoMora = dt.Rows(0).Item("RecargoMora")
            Entidad.PorcComision = dt.Rows(0).Item("PorcComision")
            Entidad.BancoProcedencia = dt.Rows(0).Item("BancoProcedencia")
            Entidad.NumCheque = dt.Rows(0).Item("NumCheque")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
            Entidad.IdPuntoVenta = fd.SiEsNulo(dt.Rows(0).Item("IdPunto"), Nothing)
            Entidad.IdTransaccion = fd.SiEsNulo(dt.Rows(0).Item("IdTransaccion"), 0)
            Entidad.IdPartida = fd.SiEsNulo(dt.Rows(0).Item("IdPartida"), 0)
        End If
        Return Entidad
    End Function

    Public Sub cpc_AbonosDeleteByPK(ByVal IdComprobante As Integer)

        db.ExecuteNonQuery("cpc_AbonosDeleteByPK", IdComprobante)
    End Sub

    Public Sub cpc_AbonosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_AbonosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub cpc_AbonosInsert _
    (ByVal entidad As cpc_Abonos)

        db.ExecuteNonQuery("cpc_AbonosInsert",
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdCuentaBancaria _
         , entidad.IdCobrador _
         , entidad.IdCliente _
         , entidad.IdTipoMov _
         , entidad.IdSucursal _
         , entidad.Concepto _
         , entidad.RecargoMora _
         , entidad.PorcComision _
         , entidad.BancoProcedencia _
         , entidad.NumCheque _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
           , entidad.IdPuntoVenta _
           , entidad.IdTransaccion _
           , entidad.IdPartida _
         )
    End Sub

    Public Sub cpc_AbonosInsert _
    (ByVal entidad As cpc_Abonos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_AbonosInsert",
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdCuentaBancaria _
         , entidad.IdCobrador _
         , entidad.IdCliente _
         , entidad.IdTipoMov _
         , entidad.IdSucursal _
         , entidad.Concepto _
         , entidad.RecargoMora _
         , entidad.PorcComision _
         , entidad.BancoProcedencia _
         , entidad.NumCheque _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
           , entidad.IdPuntoVenta _
           , entidad.IdTransaccion _
           , entidad.IdPartida _
         )
    End Sub

    Public Sub cpc_AbonosUpdate _
    (ByVal entidad As cpc_Abonos)

        db.ExecuteNonQuery("cpc_AbonosUpdate",
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdCuentaBancaria _
         , entidad.IdCobrador _
         , entidad.IdCliente _
         , entidad.IdTipoMov _
         , entidad.IdSucursal _
         , entidad.Concepto _
         , entidad.RecargoMora _
         , entidad.PorcComision _
                  , entidad.BancoProcedencia _
         , entidad.NumCheque _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
           , entidad.IdPuntoVenta _
           , entidad.IdTransaccion _
           , entidad.IdPartida _
         )
    End Sub

    Public Sub cpc_AbonosUpdate _
    (ByVal entidad As cpc_Abonos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_AbonosUpdate",
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdCuentaBancaria _
         , entidad.IdCobrador _
         , entidad.IdCliente _
         , entidad.IdTipoMov _
         , entidad.IdSucursal _
         , entidad.Concepto _
         , entidad.RecargoMora _
         , entidad.PorcComision _
                  , entidad.BancoProcedencia _
         , entidad.NumCheque _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
           , entidad.IdPuntoVenta _
           , entidad.IdTransaccion _
           , entidad.IdPartida _
         )
    End Sub

#End Region
#Region "cpc_AbonosDetalle"
    Public Function cpc_AbonosDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("cpc_AbonosDetalleSelectAll").Tables(0)
    End Function

    Public Function cpc_AbonosDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As cpc_AbonosDetalle

        Dim dt As DataTable
        dt = db.ExecuteDataSet("cpc_AbonosDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).Tables(0)

        Dim Entidad As New cpc_AbonosDetalle
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.IdComprobVenta = dt.Rows(0).Item("IdComprobVenta")
            Entidad.MontoAbonado = dt.Rows(0).Item("MontoAbonado")
            Entidad.SaldoComprobante = dt.Rows(0).Item("SaldoComprobante")
            Entidad.Concepto = dt.Rows(0).Item("Concepto")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub cpc_AbonosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("cpc_AbonosDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub cpc_AbonosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_AbonosDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub cpc_AbonosDetalleInsert _
    (ByVal entidad As cpc_AbonosDetalle)

        db.ExecuteNonQuery("cpc_AbonosDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         , entidad.MontoAbonado _
         , entidad.SaldoComprobante _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub cpc_AbonosDetalleInsert _
    (ByVal entidad As cpc_AbonosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_AbonosDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         , entidad.MontoAbonado _
         , entidad.SaldoComprobante _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub cpc_AbonosDetalleUpdate _
    (ByVal entidad As cpc_AbonosDetalle)

        db.ExecuteNonQuery("cpc_AbonosDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         , entidad.MontoAbonado _
         , entidad.SaldoComprobante _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub cpc_AbonosDetalleUpdate _
    (ByVal entidad As cpc_AbonosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_AbonosDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         , entidad.MontoAbonado _
         , entidad.SaldoComprobante _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "cpc_Cargos"
    Public Function cpc_CargosSelectAll() As DataTable
        Return db.ExecuteDataSet("cpc_CargosSelectAll").Tables(0)
    End Function

    Public Function cpc_CargosSelectByPK _
      (ByVal Iden As System.Int32 _
      ) As cpc_Cargos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("cpc_CargosSelectByPK", _
         Iden _
         ).Tables(0)

        Dim Entidad As New cpc_Cargos
        If dt.Rows.Count > 0 Then
            Entidad.Iden = dt.Rows(0).Item("Iden")
            Entidad.Numero = dt.Rows(0).Item("Numero")
            Entidad.IdCliente = dt.Rows(0).Item("IdCliente")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.Monto = dt.Rows(0).Item("Monto")
            Entidad.IdTipoMov = dt.Rows(0).Item("IdTipoMov")

        End If
        Return Entidad
    End Function

    Public Sub cpc_CargosDeleteByPK _
      (ByVal Iden As System.Int32 _
      )

        db.ExecuteNonQuery("cpc_CargosDeleteByPK", _
         Iden _
         )
    End Sub

    Public Sub cpc_CargosDeleteByPK _
      (ByVal Iden As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_CargosDeleteByPK", _
         Iden _
         )
    End Sub

    Public Sub cpc_CargosInsert _
    (ByVal entidad As cpc_Cargos)

        db.ExecuteNonQuery("cpc_CargosInsert", _
         entidad.Iden _
         , entidad.Numero _
         , entidad.IdCliente _
         , entidad.Fecha _
         , entidad.Monto _
         , entidad.IdTipoMov _
         )
    End Sub

    Public Sub cpc_CargosInsert _
    (ByVal entidad As cpc_Cargos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_CargosInsert", _
         entidad.Iden _
         , entidad.Numero _
         , entidad.IdCliente _
         , entidad.Fecha _
         , entidad.Monto _
         , entidad.IdTipoMov _
         )
    End Sub

    Public Sub cpc_CargosUpdate _
    (ByVal entidad As cpc_Cargos)

        db.ExecuteNonQuery("cpc_CargosUpdate", _
         entidad.Iden _
         , entidad.Numero _
         , entidad.IdCliente _
         , entidad.Fecha _
         , entidad.Monto _
         , entidad.IdTipoMov _
         )
    End Sub

    Public Sub cpc_CargosUpdate _
    (ByVal entidad As cpc_Cargos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_CargosUpdate", _
         entidad.Iden _
         , entidad.Numero _
         , entidad.IdCliente _
         , entidad.Fecha _
         , entidad.Monto _
         , entidad.IdTipoMov _
         )
    End Sub

#End Region
#Region "cpc_CargosDetalle"
    Public Function cpc_CargosDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("cpc_CargosDetalleSelectAll").Tables(0)
    End Function

    Public Function cpc_CargosDetalleSelectByPK _
      (ByVal Iden As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As cpc_CargosDetalle

        Dim dt As DataTable
        dt = db.ExecuteDataSet("cpc_CargosDetalleSelectByPK", _
         Iden _
         , IdDetalle _
         ).Tables(0)

        Dim Entidad As New cpc_CargosDetalle
        If dt.Rows.Count > 0 Then
            Entidad.Iden = dt.Rows(0).Item("Iden")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.Valor = dt.Rows(0).Item("Valor")
            Entidad.Concepto = dt.Rows(0).Item("Concepto")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub cpc_CargosDetalleDeleteByPK _
      (ByVal Iden As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("cpc_CargosDetalleDeleteByPK", _
         Iden _
         , IdDetalle _
         )
    End Sub

    Public Sub cpc_CargosDetalleDeleteByPK _
      (ByVal Iden As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_CargosDetalleDeleteByPK", _
         Iden _
         , IdDetalle _
         )
    End Sub

    Public Sub cpc_CargosDetalleInsert _
    (ByVal entidad As cpc_CargosDetalle)

        db.ExecuteNonQuery("cpc_CargosDetalleInsert", _
         entidad.Iden _
         , entidad.IdDetalle _
         , entidad.Valor _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub cpc_CargosDetalleInsert _
    (ByVal entidad As cpc_CargosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_CargosDetalleInsert", _
         entidad.Iden _
         , entidad.IdDetalle _
         , entidad.Valor _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub cpc_CargosDetalleUpdate _
    (ByVal entidad As cpc_CargosDetalle)

        db.ExecuteNonQuery("cpc_CargosDetalleUpdate", _
         entidad.Iden _
         , entidad.IdDetalle _
         , entidad.Valor _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub cpc_CargosDetalleUpdate _
    (ByVal entidad As cpc_CargosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_CargosDetalleUpdate", _
         entidad.Iden _
         , entidad.IdDetalle _
         , entidad.Valor _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "cpc_Cobradores"
    Public Function cpc_CobradoresSelectAll() As DataTable
        Return db.ExecuteDataSet("cpc_CobradoresSelectAll").Tables(0)
    End Function

    Public Function cpc_CobradoresSelectByPK _
      (ByVal IdCobrador As System.Int32 _
      ) As cpc_Cobradores

        Dim dt As DataTable
        dt = db.ExecuteDataSet("cpc_CobradoresSelectByPK", _
         IdCobrador _
         ).Tables(0)

        Dim Entidad As New cpc_Cobradores
        If dt.Rows.Count > 0 Then
            Entidad.IdCobrador = dt.Rows(0).Item("IdCobrador")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.PorcComision = dt.Rows(0).Item("PorcComision")

        End If
        Return Entidad
    End Function

    Public Sub cpc_CobradoresDeleteByPK _
      (ByVal IdCobrador As System.Int32 _
      )

        db.ExecuteNonQuery("cpc_CobradoresDeleteByPK", _
         IdCobrador _
         )
    End Sub

    Public Sub cpc_CobradoresDeleteByPK _
      (ByVal IdCobrador As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_CobradoresDeleteByPK", _
         IdCobrador _
         )
    End Sub

    Public Sub cpc_CobradoresInsert _
    (ByVal entidad As cpc_Cobradores)

        db.ExecuteNonQuery("cpc_CobradoresInsert", _
         entidad.IdCobrador _
         , entidad.Nombre _
         , entidad.PorcComision _
         )
    End Sub

    Public Sub cpc_CobradoresInsert _
    (ByVal entidad As cpc_Cobradores, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_CobradoresInsert", _
         entidad.IdCobrador _
         , entidad.Nombre _
         , entidad.PorcComision _
         )
    End Sub

    Public Sub cpc_CobradoresUpdate _
    (ByVal entidad As cpc_Cobradores)

        db.ExecuteNonQuery("cpc_CobradoresUpdate", _
         entidad.IdCobrador _
         , entidad.Nombre _
         , entidad.PorcComision _
         )
    End Sub

    Public Sub cpc_CobradoresUpdate _
    (ByVal entidad As cpc_Cobradores, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_CobradoresUpdate", _
         entidad.IdCobrador _
         , entidad.Nombre _
         , entidad.PorcComision _
         )
    End Sub

#End Region
#Region "cpc_PoliticaAntiguedad"
    Public Function cpc_PoliticaAntiguedadSelectAll() As DataTable
        Return db.ExecuteDataSet("cpc_PoliticaAntiguedadSelectAll").Tables(0)
    End Function

    Public Function cpc_PoliticaAntiguedadSelectByPK _
      (ByVal Id As System.Int16 _
      ) As cpc_PoliticaAntiguedad

        Dim dt As DataTable
        dt = db.ExecuteDataSet("cpc_PoliticaAntiguedadSelectByPK", _
         Id _
         ).Tables(0)

        Dim Entidad As New cpc_PoliticaAntiguedad
        If dt.Rows.Count > 0 Then
            Entidad.Id = dt.Rows(0).Item("Id")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Abreviatura = dt.Rows(0).Item("Abreviatura")
            Entidad.DesdeDias = dt.Rows(0).Item("DesdeDias")
            Entidad.HastaDias = dt.Rows(0).Item("HastaDias")

        End If
        Return Entidad
    End Function

    Public Sub cpc_PoliticaAntiguedadDeleteByPK _
      (ByVal Id As System.Int16 _
      )

        db.ExecuteNonQuery("cpc_PoliticaAntiguedadDeleteByPK", _
         Id _
         )
    End Sub

    Public Sub cpc_PoliticaAntiguedadDeleteByPK _
      (ByVal Id As System.Int16 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_PoliticaAntiguedadDeleteByPK", _
         Id _
         )
    End Sub

    Public Sub cpc_PoliticaAntiguedadInsert _
    (ByVal entidad As cpc_PoliticaAntiguedad)

        db.ExecuteNonQuery("cpc_PoliticaAntiguedadInsert", _
         entidad.Id _
         , entidad.Nombre _
         , entidad.Abreviatura _
         , entidad.DesdeDias _
         , entidad.HastaDias _
         )
    End Sub

    Public Sub cpc_PoliticaAntiguedadInsert _
    (ByVal entidad As cpc_PoliticaAntiguedad, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_PoliticaAntiguedadInsert", _
         entidad.Id _
         , entidad.Nombre _
         , entidad.Abreviatura _
         , entidad.DesdeDias _
         , entidad.HastaDias _
         )
    End Sub

    Public Sub cpc_PoliticaAntiguedadUpdate _
    (ByVal entidad As cpc_PoliticaAntiguedad)

        db.ExecuteNonQuery("cpc_PoliticaAntiguedadUpdate", _
         entidad.Id _
         , entidad.Nombre _
         , entidad.Abreviatura _
         , entidad.DesdeDias _
         , entidad.HastaDias _
         )
    End Sub

    Public Sub cpc_PoliticaAntiguedadUpdate _
    (ByVal entidad As cpc_PoliticaAntiguedad, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_PoliticaAntiguedadUpdate", _
         entidad.Id _
         , entidad.Nombre _
         , entidad.Abreviatura _
         , entidad.DesdeDias _
         , entidad.HastaDias _
         )
    End Sub

#End Region
#Region "cpc_TiposMov"
    Public Function cpc_TiposMovSelectAll() As DataTable
        Return db.ExecuteDataSet("cpc_TiposMovSelectAll").Tables(0)
    End Function

    Public Function cpc_TiposMovSelectByPK _
      (ByVal IdTipo As System.Int32 _
      ) As cpc_TiposMov

        Dim dt As DataTable
        dt = db.ExecuteDataSet("cpc_TiposMovSelectByPK", _
         IdTipo _
         ).Tables(0)

        Dim Entidad As New cpc_TiposMov
        If dt.Rows.Count > 0 Then
            Entidad.IdTipo = dt.Rows(0).Item("IdTipo")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.IdTipoTransaccion = dt.Rows(0).Item("IdTipoTransaccion")
            Entidad.IdTipoPartida = dt.Rows(0).Item("IdTipoPartida")
            Entidad.TipoAplicacion = dt.Rows(0).Item("TipoAplicacion")
            Entidad.IdCuentaContable = dt.Rows(0).Item("IdCuentaContable")
            Entidad.AfectaBancos = dt.Rows(0).Item("AfectaBancos")
        End If
        Return Entidad
    End Function

    Public Sub cpc_TiposMovDeleteByPK _
      (ByVal IdTipo As System.Int32 _
      )

        db.ExecuteNonQuery("cpc_TiposMovDeleteByPK", _
         IdTipo _
         )
    End Sub

    Public Sub cpc_TiposMovDeleteByPK _
      (ByVal IdTipo As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_TiposMovDeleteByPK", _
         IdTipo _
         )
    End Sub

    Public Sub cpc_TiposMovInsert _
    (ByVal entidad As cpc_TiposMov)

        db.ExecuteNonQuery("cpc_TiposMovInsert", _
         entidad.IdTipo _
         , entidad.Nombre _
         , entidad.IdTipoTransaccion _
         , entidad.IdTipoPartida _
         , entidad.TipoAplicacion _
         , entidad.IdCuentaContable _
         , entidad.AfectaBancos _
         )
    End Sub

    Public Sub cpc_TiposMovInsert _
    (ByVal entidad As cpc_TiposMov, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_TiposMovInsert", _
         entidad.IdTipo _
         , entidad.Nombre _
         , entidad.IdTipoTransaccion _
         , entidad.IdTipoPartida _
         , entidad.TipoAplicacion _
         , entidad.IdCuentaContable _
         , entidad.AfectaBancos _
         )
    End Sub

    Public Sub cpc_TiposMovUpdate _
    (ByVal entidad As cpc_TiposMov)

        db.ExecuteNonQuery("cpc_TiposMovUpdate", _
         entidad.IdTipo _
         , entidad.Nombre _
         , entidad.IdTipoTransaccion _
         , entidad.IdTipoPartida _
         , entidad.TipoAplicacion _
         , entidad.IdCuentaContable _
         , entidad.AfectaBancos _
         )
    End Sub

    Public Sub cpc_TiposMovUpdate _
    (ByVal entidad As cpc_TiposMov, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_TiposMovUpdate", _
         entidad.IdTipo _
         , entidad.Nombre _
         , entidad.IdTipoTransaccion _
         , entidad.IdTipoPartida _
         , entidad.TipoAplicacion _
         , entidad.IdCuentaContable _
         , entidad.AfectaBancos _
         )
    End Sub

#End Region
#Region "cpc_DocumentosCobro"
    Public Function cpc_DocumentosCobroSelectAll() As DataTable
        Return db.ExecuteDataSet("cpc_DocumentosCobroSelectAll").Tables(0)
    End Function

    Public Function cpc_DocumentosCobroSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As cpc_DocumentosCobro

        Dim dt As datatable
        dt = db.ExecuteDataSet("cpc_DocumentosCobroSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New cpc_DocumentosCobro
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.IdPunto = dt.rows(0).item("IdPunto")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdVendedor = dt.rows(0).item("IdVendedor")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub cpc_DocumentosCobroDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("cpc_DocumentosCobroDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub cpc_DocumentosCobroDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_DocumentosCobroDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub cpc_DocumentosCobroInsert _
    (ByVal entidad As cpc_DocumentosCobro)

        db.ExecuteNonQuery("cpc_DocumentosCobroInsert", _
         entidad.IdComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.IdVendedor _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub cpc_DocumentosCobroInsert _
    (ByVal entidad As cpc_DocumentosCobro, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_DocumentosCobroInsert", _
         entidad.IdComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.IdVendedor _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub cpc_DocumentosCobroUpdate _
    (ByVal entidad As cpc_DocumentosCobro)

        db.ExecuteNonQuery("cpc_DocumentosCobroUpdate", _
         entidad.IdComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.IdVendedor _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub cpc_DocumentosCobroUpdate _
    (ByVal entidad As cpc_DocumentosCobro, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_DocumentosCobroUpdate", _
         entidad.IdComprobante _
         , entidad.IdSucursal _
         , entidad.IdPunto _
         , entidad.Fecha _
         , entidad.IdVendedor _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "cpc_DocumentosCobroDetalle"
    Public Function cpc_DocumentosCobroDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("cpc_DocumentosCobroDetalleSelectAll").Tables(0)
    End Function

    Public Function cpc_DocumentosCobroDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As cpc_DocumentosCobroDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("cpc_DocumentosCobroDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New cpc_DocumentosCobroDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdComprobVenta = dt.rows(0).item("IdComprobVenta")

        End If
        Return Entidad
    End Function

    Public Sub cpc_DocumentosCobroDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("cpc_DocumentosCobroDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub cpc_DocumentosCobroDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_DocumentosCobroDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub cpc_DocumentosCobroDetalleInsert _
    (ByVal entidad As cpc_DocumentosCobroDetalle)

        db.ExecuteNonQuery("cpc_DocumentosCobroDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         )
    End Sub

    Public Sub cpc_DocumentosCobroDetalleInsert _
    (ByVal entidad As cpc_DocumentosCobroDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_DocumentosCobroDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         )
    End Sub

    Public Sub cpc_DocumentosCobroDetalleUpdate _
    (ByVal entidad As cpc_DocumentosCobroDetalle)

        db.ExecuteNonQuery("cpc_DocumentosCobroDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         )
    End Sub

    Public Sub cpc_DocumentosCobroDetalleUpdate _
    (ByVal entidad As cpc_DocumentosCobroDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpc_DocumentosCobroDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         )
    End Sub

#End Region


#Region "cpp_Abonos"
    Public Function cpp_AbonosSelectAll() As DataTable
        Return db.ExecuteDataSet("cpp_AbonosSelectAll").Tables(0)
    End Function

    Public Function cpp_AbonosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As cpp_Abonos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("cpp_AbonosSelectByPK", _
         IdComprobante _
         ).Tables(0)

        Dim Entidad As New cpp_Abonos
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.NumeroComprobante = dt.Rows(0).Item("NumeroComprobante")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.IdProveedor = dt.Rows(0).Item("IdProveedor")
            Entidad.IdTipo = dt.Rows(0).Item("IdTipo")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.Concepto = dt.Rows(0).Item("Concepto")
            Entidad.TotalAbono = dt.Rows(0).Item("TotalAbono")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.Cancelado = dt.Rows(0).Item("Cancelado")
            Entidad.IdCheque = dt.Rows(0).Item("IdCheque")
            Entidad.IdTransaccion = dt.Rows(0).Item("IdTransaccion")
            Entidad.IdCaja = dt.Rows(0).Item("IdCaja")
            Entidad.CanceladoPor = dt.Rows(0).Item("CanceladoPor")
            Entidad.FechaHoraCancelacion = dt.Rows(0).Item("FechaHoraCancelacion")

        End If
        Return Entidad
    End Function

    Public Sub cpp_AbonosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("cpp_AbonosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub cpp_AbonosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_AbonosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub cpp_AbonosInsert _
    (ByVal entidad As cpp_Abonos)

        db.ExecuteNonQuery("cpp_AbonosInsert", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdProveedor _
         , entidad.IdTipo _
         , entidad.IdSucursal _
         , entidad.Concepto _
         , entidad.TotalAbono _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.Cancelado _
         , entidad.IdCheque _
         , entidad.IdTransaccion _
         , entidad.IdCaja _
         , entidad.CanceladoPor _
         , entidad.FechaHoraCancelacion _
         )
    End Sub

    Public Sub cpp_AbonosInsert _
(ByVal entidad As cpp_Abonos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_AbonosInsert", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdProveedor _
         , entidad.IdTipo _
         , entidad.IdSucursal _
         , entidad.Concepto _
         , entidad.TotalAbono _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.Cancelado _
         , entidad.IdCheque _
         , entidad.IdTransaccion _
         , entidad.IdCaja _
         , entidad.CanceladoPor _
         , entidad.FechaHoraCancelacion _
         )
    End Sub
    Public Sub cpp_TrasladoInsert _
(ByVal entidad As cpp_Traslado, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_TrasladoInsert", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdProveedorOrigen _
         , entidad.IdProveedorDestino _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub
    Public Sub cpp_AbonosUpdate _
    (ByVal entidad As cpp_Abonos)

        db.ExecuteNonQuery("cpp_AbonosUpdate", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdProveedor _
         , entidad.IdTipo _
         , entidad.IdSucursal _
         , entidad.Concepto _
         , entidad.TotalAbono _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.Cancelado _
         , entidad.IdCheque _
         , entidad.IdTransaccion _
         , entidad.IdCaja _
         , entidad.CanceladoPor _
         , entidad.FechaHoraCancelacion _
         )
    End Sub

    Public Sub cpp_AbonosUpdate _
    (ByVal entidad As cpp_Abonos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_AbonosUpdate", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdProveedor _
         , entidad.IdTipo _
         , entidad.IdSucursal _
         , entidad.Concepto _
         , entidad.TotalAbono _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.Cancelado _
         , entidad.IdCheque _
         , entidad.IdTransaccion _
         , entidad.IdCaja _
         , entidad.CanceladoPor _
         , entidad.FechaHoraCancelacion _
         )
    End Sub

#End Region
#Region "cpp_AbonosDetalle"
    Public Function cpp_AbonosDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("cpp_AbonosDetalleSelectAll").Tables(0)
    End Function

    Public Function cpp_AbonosDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As cpp_AbonosDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("cpp_AbonosDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New cpp_AbonosDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.NumComprobanteCompra = dt.rows(0).item("NumComprobanteCompra")
            entidad.MontoAbonado = dt.rows(0).item("MontoAbonado")
            entidad.SaldoActual = dt.rows(0).item("SaldoActual")
            entidad.IdComprobanteCompra = dt.rows(0).item("IdComprobanteCompra")
            Entidad.TipoComprobanteCompra = dt.Rows(0).Item("TipoComprobanteCompra")

        End If
        Return Entidad
    End Function

    Public Sub cpp_AbonosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("cpp_AbonosDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub cpp_AbonosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_AbonosDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub cpp_AbonosDetalleInsert _
    (ByVal entidad As cpp_AbonosDetalle)

        db.ExecuteNonQuery("cpp_AbonosDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.NumComprobanteCompra _
         , entidad.MontoAbonado _
         , entidad.SaldoActual _
         , entidad.IdComprobanteCompra _
         , entidad.TipoComprobanteCompra _
         )
    End Sub
    Public Sub cpp_TrasladoDetalleInsert _
(ByVal entidad As cpp_TrasladoDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_TrasladoDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.NumComprobanteCompra _
         , entidad.SaldoActual _
         , entidad.IdComprobanteCompra _
         , entidad.TipoComprobanteCompra _
         )
    End Sub
    Public Sub cpp_AbonosDetalleInsert(ByVal entidad As cpp_AbonosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_AbonosDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.NumComprobanteCompra _
         , entidad.MontoAbonado _
         , entidad.SaldoActual _
         , entidad.IdComprobanteCompra _
          , entidad.TipoComprobanteCompra _
         )
    End Sub

    Public Sub cpp_AbonosDetalleUpdate _
    (ByVal entidad As cpp_AbonosDetalle)

        db.ExecuteNonQuery("cpp_AbonosDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.NumComprobanteCompra _
         , entidad.MontoAbonado _
         , entidad.SaldoActual _
         , entidad.IdComprobanteCompra _
          , entidad.TipoComprobanteCompra _
         )
    End Sub

    Public Sub cpp_AbonosDetalleUpdate _
    (ByVal entidad As cpp_AbonosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_AbonosDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.NumComprobanteCompra _
         , entidad.MontoAbonado _
         , entidad.SaldoActual _
         , entidad.IdComprobanteCompra _
          , entidad.TipoComprobanteCompra _
         )
    End Sub

#End Region

#Region "cpp_Quedan"
    Public Function cpp_QuedanSelectAll() As DataTable
        Return db.ExecuteDataSet("cpp_QuedanSelectAll").Tables(0)
    End Function

    Public Function cpp_QuedanSelectByPK(ByVal IdComprobante As System.Int32) As cpp_Quedan

        Dim dt As DataTable
        dt = db.ExecuteDataSet("cpp_QuedanSelectByPK", IdComprobante).Tables(0)

        Dim Entidad As New cpp_Quedan
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.Numero = dt.Rows(0).Item("Numero")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.IdProveedor = dt.Rows(0).Item("IdProveedor")
            Entidad.NombreProveedor = dt.Rows(0).Item("NombreProveedor")
            Entidad.NrcProveedor = dt.Rows(0).Item("NrcProveedor")
            Entidad.FechaPago = fd.SiEsNulo(dt.Rows(0).Item("FechaPago"), Nothing)
            Entidad.TotalPago = dt.Rows(0).Item("TotalPago")
            Entidad.Concepto = dt.Rows(0).Item("Concepto")
            Entidad.SaldoActual = dt.Rows(0).Item("SaldoActual")
            Entidad.Contabilizar = dt.Rows(0).Item("Contabilizar")
            Entidad.IdPartida = dt.Rows(0).Item("IdPartida")
            Entidad.IdTipoPartida = dt.Rows(0).Item("IdTipoPartida")
            Entidad.NumeroPartida = dt.Rows(0).Item("NumeroPartida")
            Entidad.Anulado = dt.Rows(0).Item("Anulado")
            Entidad.FechaCancelacion = fd.SiEsNulo(dt.Rows(0).Item("FechaCancelacion"), Nothing)
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
            Entidad.AnuladoPor = dt.Rows(0).Item("AnuladoPor")
            Entidad.FechaHoraAnulacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraAnulacion"), Nothing)
        End If
        Return Entidad
    End Function

    Public Sub cpp_QuedanDeleteByPK _
      (ByVal IdQuedan As System.Int32 _
      )

        db.ExecuteNonQuery("cpp_QuedanDeleteByPK", _
         IdQuedan _
         )
    End Sub

    Public Sub cpp_QuedanDeleteByPK _
      (ByVal IdQuedan As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_QuedanDeleteByPK", _
         IdQuedan _
         )
    End Sub

    Public Sub cpp_QuedanInsert _
    (ByVal entidad As cpp_Quedan)

        db.ExecuteNonQuery("cpp_QuedanInsert", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdProveedor _
         , entidad.NombreProveedor _
         , entidad.NrcProveedor _
         , entidad.FechaPago _
         , entidad.TotalPago _
         , entidad.Concepto _
         , entidad.SaldoActual _
         , entidad.Contabilizar _
         , entidad.IdPartida _
         , entidad.IdTipoPartida _
         , entidad.NumeroPartida _
                  , entidad.Anulado _
         , entidad.FechaCancelacion _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
                  , entidad.AnuladoPor _
         , entidad.FechaHoraAnulacion _
         )
    End Sub

    Public Sub cpp_QuedanInsert _
    (ByVal entidad As cpp_Quedan, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_QuedanInsert", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdProveedor _
         , entidad.NombreProveedor _
         , entidad.NrcProveedor _
         , entidad.FechaPago _
         , entidad.TotalPago _
         , entidad.Concepto _
         , entidad.SaldoActual _
         , entidad.Contabilizar _
         , entidad.IdPartida _
         , entidad.IdTipoPartida _
         , entidad.NumeroPartida _
                   , entidad.Anulado _
         , entidad.FechaCancelacion _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
                           , entidad.AnuladoPor _
         , entidad.FechaHoraAnulacion _
         )
    End Sub

    Public Sub cpp_QuedanUpdate _
    (ByVal entidad As cpp_Quedan)

        db.ExecuteNonQuery("cpp_QuedanUpdate", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdProveedor _
         , entidad.NombreProveedor _
         , entidad.NrcProveedor _
         , entidad.FechaPago _
         , entidad.TotalPago _
         , entidad.Concepto _
         , entidad.SaldoActual _
         , entidad.Contabilizar _
         , entidad.IdPartida _
         , entidad.IdTipoPartida _
         , entidad.NumeroPartida _
                   , entidad.Anulado _
         , entidad.FechaCancelacion _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
                           , entidad.AnuladoPor _
         , entidad.FechaHoraAnulacion _
         )
    End Sub

    Public Sub cpp_QuedanUpdate _
    (ByVal entidad As cpp_Quedan, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_QuedanUpdate", _
         entidad.IdComprobante _
         , entidad.Numero _
         , entidad.Fecha _
         , entidad.IdProveedor _
         , entidad.NombreProveedor _
         , entidad.NrcProveedor _
         , entidad.FechaPago _
         , entidad.TotalPago _
         , entidad.Concepto _
         , entidad.SaldoActual _
         , entidad.Contabilizar _
         , entidad.IdPartida _
         , entidad.IdTipoPartida _
         , entidad.NumeroPartida _
          , entidad.Anulado _
         , entidad.FechaCancelacion _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
        , entidad.AnuladoPor _
         , entidad.FechaHoraAnulacion _
         )
    End Sub

#End Region

#Region "cpp_QuedanDetalle"
    Public Function cpp_QuedanDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("cpp_QuedanDetalleSelectAll").Tables(0)
    End Function

    Public Function cpp_QuedanDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As cpp_QuedanDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("cpp_QuedanDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New cpp_QuedanDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdCompra = dt.rows(0).item("IdCompra")
            entidad.Numero = dt.rows(0).item("Numero")
            entidad.IdTipo = dt.rows(0).item("IdTipo")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.FechaVence = dt.rows(0).item("FechaVence")
            entidad.ValorGravado = dt.rows(0).item("ValorGravado")
            entidad.ValorExento = dt.rows(0).item("ValorExento")
            entidad.ValorIva = dt.rows(0).item("ValorIva")
            entidad.SubTotal = dt.rows(0).item("SubTotal")
            entidad.Retencion = dt.rows(0).item("Retencion")
            entidad.ValorTotal = dt.rows(0).item("ValorTotal")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub cpp_QuedanDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("cpp_QuedanDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub cpp_QuedanDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_QuedanDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub cpp_QuedanDetalleInsert _
    (ByVal entidad As cpp_QuedanDetalle)

        db.ExecuteNonQuery("cpp_QuedanDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdCompra _
         , entidad.Numero _
         , entidad.IdTipo _
         , entidad.Fecha _
         , entidad.FechaVence _
         , entidad.ValorGravado _
         , entidad.ValorExento _
         , entidad.ValorIva _
         , entidad.SubTotal _
         , entidad.Retencion _
         , entidad.ValorTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub cpp_QuedanDetalleInsert _
    (ByVal entidad As cpp_QuedanDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_QuedanDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdCompra _
         , entidad.Numero _
         , entidad.IdTipo _
         , entidad.Fecha _
         , entidad.FechaVence _
         , entidad.ValorGravado _
         , entidad.ValorExento _
         , entidad.ValorIva _
         , entidad.SubTotal _
         , entidad.Retencion _
         , entidad.ValorTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub cpp_QuedanDetalleUpdate _
    (ByVal entidad As cpp_QuedanDetalle)

        db.ExecuteNonQuery("cpp_QuedanDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdCompra _
         , entidad.Numero _
         , entidad.IdTipo _
         , entidad.Fecha _
         , entidad.FechaVence _
         , entidad.ValorGravado _
         , entidad.ValorExento _
         , entidad.ValorIva _
         , entidad.SubTotal _
         , entidad.Retencion _
         , entidad.ValorTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub cpp_QuedanDetalleUpdate _
    (ByVal entidad As cpp_QuedanDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "cpp_QuedanDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdCompra _
         , entidad.Numero _
         , entidad.IdTipo _
         , entidad.Fecha _
         , entidad.FechaVence _
         , entidad.ValorGravado _
         , entidad.ValorExento _
         , entidad.ValorIva _
         , entidad.SubTotal _
         , entidad.Retencion _
         , entidad.ValorTotal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region

#Region "ActivoFijo"
#Region "acf_Activos"
    Public Function acf_ActivosSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_ActivosSelectAll").Tables(0)
    End Function

    Public Function acf_ActivosSelectByPK _
      (ByVal IdActivo As System.Int32 _
      ) As acf_Activos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("acf_ActivosSelectByPK", _
         IdActivo _
         ).Tables(0)

        Dim Entidad As New acf_Activos
        If dt.Rows.Count > 0 Then
            Entidad.IdActivo = dt.Rows(0).Item("IdActivo")
            Entidad.Codigo = dt.Rows(0).Item("Codigo")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Descripcion = dt.Rows(0).Item("Descripcion")
            Entidad.FechaAdquisicion = dt.Rows(0).Item("FechaAdquisicion")
            Entidad.IdMarca = dt.Rows(0).Item("IdMarca")
            Entidad.IdModelo = dt.Rows(0).Item("IdModelo")
            Entidad.IdEstilo = dt.Rows(0).Item("IdEstilo")
            Entidad.Color = dt.Rows(0).Item("Color")
            Entidad.Serie = dt.Rows(0).Item("Serie")
            Entidad.Anio = dt.Rows(0).Item("Anio")
            Entidad.NumCompra = dt.Rows(0).Item("NumCompra")
            Entidad.NumCheque = dt.Rows(0).Item("NumCheque")
            Entidad.Depreciable = dt.Rows(0).Item("Depreciable")
            Entidad.ValorAdquisicion = dt.Rows(0).Item("ValorAdquisicion")
            Entidad.ValorDepreciar = dt.Rows(0).Item("ValorDepreciar")
            Entidad.ValorContable = dt.Rows(0).Item("ValorContable")
            Entidad.MesesDepreciacion = dt.Rows(0).Item("MesesDepreciacion")
            Entidad.CuotaMensual = dt.Rows(0).Item("CuotaMensual")
            Entidad.CuotaAnual = dt.Rows(0).Item("CuotaAnual")
            Entidad.FechaInicio = dt.Rows(0).Item("FechaInicio")
            Entidad.IdTipoDepreciacion = dt.Rows(0).Item("IdTipoDepreciacion")
            Entidad.ValorRetiro = dt.Rows(0).Item("ValorRetiro")
            Entidad.IdClase = dt.Rows(0).Item("IdClase")
            Entidad.IdProveedor = dt.Rows(0).Item("IdProveedor")
            Entidad.IdEmpleado = dt.Rows(0).Item("IdEmpleado")
            Entidad.IdUbicacion = dt.Rows(0).Item("IdUbicacion")
            Entidad.IdCuentaActivo = dt.Rows(0).Item("IdCuentaActivo")
            Entidad.IdCuentaDepreciacion = dt.Rows(0).Item("IdCuentaDepreciacion")
            Entidad.IdCuentaGasto = dt.Rows(0).Item("IdCuentaGasto")
            Entidad.IdEstado = dt.Rows(0).Item("IdEstado")
            Entidad.IdMotivoBaja = dt.Rows(0).Item("IdMotivoBaja")
            Entidad.FechaRetiro = fd.SiEsNulo(dt.Rows(0).Item("FechaRetiro"), Nothing)
            Entidad.VectoGarantia = fd.SiEsNulo(dt.Rows(0).Item("VectoGarantia"), Nothing)
            Entidad.InfoAdicional = dt.Rows(0).Item("InfoAdicional")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)
        End If
        Return Entidad
    End Function

    Public Sub acf_ActivosDeleteByPK _
      (ByVal IdActivo As System.Int32 _
      )

        db.ExecuteNonQuery("acf_ActivosDeleteByPK", _
         IdActivo _
         )
    End Sub

    Public Sub acf_ActivosDeleteByPK _
      (ByVal IdActivo As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_ActivosDeleteByPK", _
         IdActivo _
         )
    End Sub

    Public Sub acf_ActivosInsert _
    (ByVal entidad As acf_Activos)

        db.ExecuteNonQuery("acf_ActivosInsert", _
         entidad.IdActivo _
         , entidad.Codigo _
         , entidad.Nombre _
         , entidad.Descripcion _
         , entidad.FechaAdquisicion _
         , entidad.IdMarca _
         , entidad.IdModelo _
         , entidad.IdEstilo _
         , entidad.Color _
         , entidad.Serie _
         , entidad.Anio _
         , entidad.NumCompra _
         , entidad.NumCheque _
         , entidad.Depreciable _
         , entidad.ValorAdquisicion _
         , entidad.ValorDepreciar _
         , entidad.ValorContable _
         , entidad.MesesDepreciacion _
         , entidad.CuotaMensual _
         , entidad.CuotaAnual _
         , entidad.FechaInicio _
         , entidad.IdTipoDepreciacion _
         , entidad.ValorRetiro _
         , entidad.IdClase _
         , entidad.IdProveedor _
         , entidad.IdEmpleado _
         , entidad.IdUbicacion _
         , entidad.IdCuentaActivo _
         , entidad.IdCuentaDepreciacion _
         , entidad.IdCuentaGasto _
         , entidad.IdEstado _
         , entidad.IdMotivoBaja _
         , entidad.FechaRetiro _
         , entidad.VectoGarantia _
         , entidad.InfoAdicional _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub acf_ActivosInsert _
    (ByVal entidad As acf_Activos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_ActivosInsert", _
         entidad.IdActivo _
         , entidad.Codigo _
         , entidad.Nombre _
         , entidad.Descripcion _
         , entidad.FechaAdquisicion _
         , entidad.IdMarca _
         , entidad.IdModelo _
         , entidad.IdEstilo _
         , entidad.Color _
         , entidad.Serie _
         , entidad.Anio _
         , entidad.NumCompra _
         , entidad.NumCheque _
         , entidad.Depreciable _
         , entidad.ValorAdquisicion _
         , entidad.ValorDepreciar _
         , entidad.ValorContable _
         , entidad.MesesDepreciacion _
         , entidad.CuotaMensual _
         , entidad.CuotaAnual _
         , entidad.FechaInicio _
         , entidad.IdTipoDepreciacion _
         , entidad.ValorRetiro _
         , entidad.IdClase _
         , entidad.IdProveedor _
         , entidad.IdEmpleado _
         , entidad.IdUbicacion _
         , entidad.IdCuentaActivo _
         , entidad.IdCuentaDepreciacion _
         , entidad.IdCuentaGasto _
         , entidad.IdEstado _
         , entidad.IdMotivoBaja _
         , entidad.FechaRetiro _
         , entidad.VectoGarantia _
         , entidad.InfoAdicional _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub acf_ActivosUpdate _
    (ByVal entidad As acf_Activos)

        db.ExecuteNonQuery("acf_ActivosUpdate", _
         entidad.IdActivo _
         , entidad.Codigo _
         , entidad.Nombre _
         , entidad.Descripcion _
         , entidad.FechaAdquisicion _
         , entidad.IdMarca _
         , entidad.IdModelo _
         , entidad.IdEstilo _
         , entidad.Color _
         , entidad.Serie _
         , entidad.Anio _
         , entidad.NumCompra _
         , entidad.NumCheque _
         , entidad.Depreciable _
         , entidad.ValorAdquisicion _
         , entidad.ValorDepreciar _
         , entidad.ValorContable _
         , entidad.MesesDepreciacion _
         , entidad.CuotaMensual _
         , entidad.CuotaAnual _
         , entidad.FechaInicio _
         , entidad.IdTipoDepreciacion _
         , entidad.ValorRetiro _
         , entidad.IdClase _
         , entidad.IdProveedor _
         , entidad.IdEmpleado _
         , entidad.IdUbicacion _
         , entidad.IdCuentaActivo _
         , entidad.IdCuentaDepreciacion _
         , entidad.IdCuentaGasto _
         , entidad.IdEstado _
         , entidad.IdMotivoBaja _
         , entidad.FechaRetiro _
         , entidad.VectoGarantia _
         , entidad.InfoAdicional _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub acf_ActivosUpdate _
    (ByVal entidad As acf_Activos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_ActivosUpdate", _
         entidad.IdActivo _
         , entidad.Codigo _
         , entidad.Nombre _
         , entidad.Descripcion _
         , entidad.FechaAdquisicion _
         , entidad.IdMarca _
         , entidad.IdModelo _
         , entidad.IdEstilo _
         , entidad.Color _
         , entidad.Serie _
         , entidad.Anio _
         , entidad.NumCompra _
         , entidad.NumCheque _
         , entidad.Depreciable _
         , entidad.ValorAdquisicion _
         , entidad.ValorDepreciar _
         , entidad.ValorContable _
         , entidad.MesesDepreciacion _
         , entidad.CuotaMensual _
         , entidad.CuotaAnual _
         , entidad.FechaInicio _
         , entidad.IdTipoDepreciacion _
         , entidad.ValorRetiro _
         , entidad.IdClase _
         , entidad.IdProveedor _
         , entidad.IdEmpleado _
         , entidad.IdUbicacion _
         , entidad.IdCuentaActivo _
         , entidad.IdCuentaDepreciacion _
         , entidad.IdCuentaGasto _
         , entidad.IdEstado _
         , entidad.IdMotivoBaja _
         , entidad.FechaRetiro _
         , entidad.VectoGarantia _
         , entidad.InfoAdicional _
         , entidad.IdSucursal _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region

#Region "acf_Clases"
    Public Function acf_ClasesSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_ClasesSelectAll").Tables(0)
    End Function

    Public Function acf_ClasesSelectByPK(ByVal IdClase As Integer) As acf_Clases

        Dim dt As DataTable = db.ExecuteDataSet("acf_ClasesSelectByPK", IdClase).Tables(0)

        Dim Entidad As New acf_Clases
        If dt.Rows.Count > 0 Then
            Entidad.IdClase = dt.Rows(0).Item("IdClase")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Depreciable = dt.Rows(0).Item("Depreciable")
            Entidad.IdCtaActivo = dt.Rows(0).Item("IdCtaActivo")
            Entidad.IdCtaDepreciacion = dt.Rows(0).Item("IdCtaDepreciacion")
            Entidad.IdCtaGasto = dt.Rows(0).Item("IdCtaGasto")
        End If
        Return Entidad
    End Function

    Public Sub acf_ClasesDeleteByPK(ByVal IdClase As System.Int16)

        db.ExecuteNonQuery("acf_ClasesDeleteByPK", IdClase)
    End Sub

    Public Sub acf_ClasesDeleteByPK(ByVal IdClase As System.Int16, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_ClasesDeleteByPK", IdClase)
    End Sub

    Public Sub acf_ClasesInsert _
    (ByVal entidad As acf_Clases)

        db.ExecuteNonQuery("acf_ClasesInsert", _
         entidad.IdClase _
         , entidad.Nombre _
         , entidad.Depreciable _
         , entidad.IdCtaActivo _
         , entidad.IdCtaDepreciacion _
         , entidad.IdCtaGasto _
         )
    End Sub

    Public Sub acf_ClasesInsert _
    (ByVal entidad As acf_Clases, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_ClasesInsert", _
         entidad.IdClase _
         , entidad.Nombre _
         , entidad.Depreciable _
         , entidad.IdCtaActivo _
         , entidad.IdCtaDepreciacion _
         , entidad.IdCtaGasto _
         )
    End Sub

    Public Sub acf_ClasesUpdate _
    (ByVal entidad As acf_Clases)

        db.ExecuteNonQuery("acf_ClasesUpdate", _
         entidad.IdClase _
         , entidad.Nombre _
         , entidad.Depreciable _
         , entidad.IdCtaActivo _
         , entidad.IdCtaDepreciacion _
         , entidad.IdCtaGasto _
         )
    End Sub

    Public Sub acf_ClasesUpdate _
    (ByVal entidad As acf_Clases, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_ClasesUpdate", _
         entidad.IdClase _
         , entidad.Nombre _
         , entidad.Depreciable _
         , entidad.IdCtaActivo _
         , entidad.IdCtaDepreciacion _
         , entidad.IdCtaGasto _
         )
    End Sub

#End Region
#Region "acf_DepreciacionesAjustes"
    Public Function acf_DepreciacionesAjustesSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_DepreciacionesAjustesSelectAll").Tables(0)
    End Function

    Public Function acf_DepreciacionesAjustesSelectByPK _
      (ByVal IdActivo As System.Int32 _
      , ByVal NumCuota As System.Int16 _
      ) As acf_DepreciacionesAjustes

        Dim dt As DataTable
        dt = db.ExecuteDataSet("acf_DepreciacionesAjustesSelectByPK", _
         IdActivo _
         , NumCuota _
         ).Tables(0)

        Dim Entidad As New acf_DepreciacionesAjustes
        If dt.Rows.Count > 0 Then
            Entidad.IdActivo = dt.Rows(0).Item("IdActivo")
            Entidad.NumCuota = dt.Rows(0).Item("NumCuota")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.IdTipo = dt.Rows(0).Item("IdTipo")
            Entidad.Valor = dt.Rows(0).Item("Valor")

        End If
        Return Entidad
    End Function

    Public Sub acf_DepreciacionesAjustesDeleteByPK _
      (ByVal IdActivo As System.Int32 _
      , ByVal NumCuota As System.Int16 _
      )

        db.ExecuteNonQuery("acf_DepreciacionesAjustesDeleteByPK", _
         IdActivo _
         , NumCuota _
         )
    End Sub

    Public Sub acf_DepreciacionesAjustesDeleteByPK _
      (ByVal IdActivo As System.Int32 _
      , ByVal NumCuota As System.Int16 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_DepreciacionesAjustesDeleteByPK", _
         IdActivo _
         , NumCuota _
         )
    End Sub

    Public Sub acf_DepreciacionesAjustesInsert _
    (ByVal entidad As acf_DepreciacionesAjustes)

        db.ExecuteNonQuery("acf_DepreciacionesAjustesInsert", _
         entidad.IdActivo _
         , entidad.NumCuota _
         , entidad.Fecha _
         , entidad.IdTipo _
         , entidad.Valor _
         )
    End Sub

    Public Sub acf_DepreciacionesAjustesInsert _
    (ByVal entidad As acf_DepreciacionesAjustes, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_DepreciacionesAjustesInsert", _
         entidad.IdActivo _
         , entidad.NumCuota _
         , entidad.Fecha _
         , entidad.IdTipo _
         , entidad.Valor _
         )
    End Sub

    Public Sub acf_DepreciacionesAjustesUpdate _
    (ByVal entidad As acf_DepreciacionesAjustes)

        db.ExecuteNonQuery("acf_DepreciacionesAjustesUpdate", _
         entidad.IdActivo _
         , entidad.NumCuota _
         , entidad.Fecha _
         , entidad.IdTipo _
         , entidad.Valor _
         )
    End Sub

    Public Sub acf_DepreciacionesAjustesUpdate _
    (ByVal entidad As acf_DepreciacionesAjustes, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_DepreciacionesAjustesUpdate", _
         entidad.IdActivo _
         , entidad.NumCuota _
         , entidad.Fecha _
         , entidad.IdTipo _
         , entidad.Valor _
         )
    End Sub

#End Region
#Region "acf_Estados"
    Public Function acf_EstadosSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_EstadosSelectAll").Tables(0)
    End Function

    Public Function acf_EstadosSelectByPK _
      (ByVal IdEstado As System.Byte _
      ) As acf_Estados

        Dim dt As DataTable
        dt = db.ExecuteDataSet("acf_EstadosSelectByPK", _
         IdEstado _
         ).Tables(0)

        Dim Entidad As New acf_Estados
        If dt.Rows.Count > 0 Then
            Entidad.IdEstado = dt.Rows(0).Item("IdEstado")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub acf_EstadosDeleteByPK _
      (ByVal IdEstado As System.Byte _
      )

        db.ExecuteNonQuery("acf_EstadosDeleteByPK", _
         IdEstado _
         )
    End Sub

    Public Sub acf_EstadosDeleteByPK _
      (ByVal IdEstado As System.Byte _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_EstadosDeleteByPK", _
         IdEstado _
         )
    End Sub

    Public Sub acf_EstadosInsert _
    (ByVal entidad As acf_Estados)

        db.ExecuteNonQuery("acf_EstadosInsert", _
         entidad.IdEstado _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_EstadosInsert _
    (ByVal entidad As acf_Estados, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_EstadosInsert", _
         entidad.IdEstado _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_EstadosUpdate _
    (ByVal entidad As acf_Estados)

        db.ExecuteNonQuery("acf_EstadosUpdate", _
         entidad.IdEstado _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_EstadosUpdate _
    (ByVal entidad As acf_Estados, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_EstadosUpdate", _
         entidad.IdEstado _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "acf_Estilos"
    Public Function acf_EstilosSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_EstilosSelectAll").Tables(0)
    End Function

    Public Function acf_EstilosSelectByPK _
      (ByVal IdEstilo As System.Int32 _
      ) As acf_Estilos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("acf_EstilosSelectByPK", _
         IdEstilo _
         ).Tables(0)

        Dim Entidad As New acf_Estilos
        If dt.Rows.Count > 0 Then
            Entidad.IdEstilo = dt.Rows(0).Item("IdEstilo")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub acf_EstilosDeleteByPK _
      (ByVal IdEstilo As System.Int32 _
      )

        db.ExecuteNonQuery("acf_EstilosDeleteByPK", _
         IdEstilo _
         )
    End Sub

    Public Sub acf_EstilosDeleteByPK _
      (ByVal IdEstilo As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_EstilosDeleteByPK", _
         IdEstilo _
         )
    End Sub

    Public Sub acf_EstilosInsert _
    (ByVal entidad As acf_Estilos)

        db.ExecuteNonQuery("acf_EstilosInsert", _
         entidad.IdEstilo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_EstilosInsert _
    (ByVal entidad As acf_Estilos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_EstilosInsert", _
         entidad.IdEstilo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_EstilosUpdate _
    (ByVal entidad As acf_Estilos)

        db.ExecuteNonQuery("acf_EstilosUpdate", _
         entidad.IdEstilo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_EstilosUpdate _
    (ByVal entidad As acf_Estilos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_EstilosUpdate", _
         entidad.IdEstilo _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "acf_Fallas"
    Public Function acf_FallasSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_FallasSelectAll").Tables(0)
    End Function

    Public Function acf_FallasSelectByPK _
      (ByVal IdFalla As System.Int32 _
      ) As acf_Fallas

        Dim dt As DataTable
        dt = db.ExecuteDataSet("acf_FallasSelectByPK", _
         IdFalla _
         ).Tables(0)

        Dim Entidad As New acf_Fallas
        If dt.Rows.Count > 0 Then
            Entidad.IdFalla = dt.Rows(0).Item("IdFalla")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub acf_FallasDeleteByPK _
      (ByVal IdFalla As System.Int32 _
      )

        db.ExecuteNonQuery("acf_FallasDeleteByPK", _
         IdFalla _
         )
    End Sub

    Public Sub acf_FallasDeleteByPK _
      (ByVal IdFalla As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_FallasDeleteByPK", _
         IdFalla _
         )
    End Sub

    Public Sub acf_FallasInsert _
    (ByVal entidad As acf_Fallas)

        db.ExecuteNonQuery("acf_FallasInsert", _
         entidad.IdFalla _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_FallasInsert _
    (ByVal entidad As acf_Fallas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_FallasInsert", _
         entidad.IdFalla _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_FallasUpdate _
    (ByVal entidad As acf_Fallas)

        db.ExecuteNonQuery("acf_FallasUpdate", _
         entidad.IdFalla _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_FallasUpdate _
    (ByVal entidad As acf_Fallas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_FallasUpdate", _
         entidad.IdFalla _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "acf_Mantenimientos"
    Public Function acf_MantenimientosSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_MantenimientosSelectAll").Tables(0)
    End Function

    Public Function acf_MantenimientosSelectByPK _
      (ByVal Id As System.Int32 _
      ) As acf_Mantenimientos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("acf_MantenimientosSelectByPK", _
         Id _
         ).Tables(0)

        Dim Entidad As New acf_Mantenimientos
        If dt.Rows.Count > 0 Then
            Entidad.Id = dt.Rows(0).Item("Id")
            Entidad.IdActivo = dt.Rows(0).Item("IdActivo")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.Valor = dt.Rows(0).Item("Valor")
            Entidad.IdFalla = dt.Rows(0).Item("IdFalla")
            Entidad.IdTecnico = dt.Rows(0).Item("IdTecnico")
            Entidad.IncluyeGarantia = dt.Rows(0).Item("IncluyeGarantia")
            Entidad.FechaVenceGarantia = dt.Rows(0).Item("FechaVenceGarantia")
            Entidad.FechaProximoServicio = dt.Rows(0).Item("FechaProximoServicio")
            Entidad.DetalleProblema = dt.Rows(0).Item("DetalleProblema")
            Entidad.DetalleSolucion = dt.Rows(0).Item("DetalleSolucion")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub acf_MantenimientosDeleteByPK _
      (ByVal Id As System.Int32 _
      )

        db.ExecuteNonQuery("acf_MantenimientosDeleteByPK", _
         Id _
         )
    End Sub

    Public Sub acf_MantenimientosDeleteByPK _
      (ByVal Id As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_MantenimientosDeleteByPK", _
         Id _
         )
    End Sub

    Public Sub acf_MantenimientosInsert _
    (ByVal entidad As acf_Mantenimientos)

        db.ExecuteNonQuery("acf_MantenimientosInsert", _
         entidad.Id _
         , entidad.IdActivo _
         , entidad.Fecha _
         , entidad.Valor _
         , entidad.IdFalla _
         , entidad.IdTecnico _
         , entidad.IncluyeGarantia _
         , entidad.FechaVenceGarantia _
         , entidad.FechaProximoServicio _
         , entidad.DetalleProblema _
         , entidad.DetalleSolucion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub acf_MantenimientosInsert _
    (ByVal entidad As acf_Mantenimientos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_MantenimientosInsert", _
         entidad.Id _
         , entidad.IdActivo _
         , entidad.Fecha _
         , entidad.Valor _
         , entidad.IdFalla _
         , entidad.IdTecnico _
         , entidad.IncluyeGarantia _
         , entidad.FechaVenceGarantia _
         , entidad.FechaProximoServicio _
         , entidad.DetalleProblema _
         , entidad.DetalleSolucion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub acf_MantenimientosUpdate _
    (ByVal entidad As acf_Mantenimientos)

        db.ExecuteNonQuery("acf_MantenimientosUpdate", _
         entidad.Id _
         , entidad.IdActivo _
         , entidad.Fecha _
         , entidad.Valor _
         , entidad.IdFalla _
         , entidad.IdTecnico _
         , entidad.IncluyeGarantia _
         , entidad.FechaVenceGarantia _
         , entidad.FechaProximoServicio _
         , entidad.DetalleProblema _
         , entidad.DetalleSolucion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub acf_MantenimientosUpdate _
    (ByVal entidad As acf_Mantenimientos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_MantenimientosUpdate", _
         entidad.Id _
         , entidad.IdActivo _
         , entidad.Fecha _
         , entidad.Valor _
         , entidad.IdFalla _
         , entidad.IdTecnico _
         , entidad.IncluyeGarantia _
         , entidad.FechaVenceGarantia _
         , entidad.FechaProximoServicio _
         , entidad.DetalleProblema _
         , entidad.DetalleSolucion _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "acf_Marcas"
    Public Function acf_MarcasSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_MarcasSelectAll").Tables(0)
    End Function

    Public Function acf_MarcasSelectByPK(ByVal IdMarca As Int32) As acf_Marcas

        Dim dt As DataTable = db.ExecuteDataSet("acf_MarcasSelectByPK", IdMarca).Tables(0)

        Dim Entidad As New acf_Marcas
        If dt.Rows.Count > 0 Then
            Entidad.IdMarca = dt.Rows(0).Item("IdMarca")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
        End If
        Return Entidad
    End Function

    Public Sub acf_MarcasDeleteByPK _
      (ByVal IdMarca As Int32 _
      )

        db.ExecuteNonQuery("acf_MarcasDeleteByPK", _
         IdMarca _
         )
    End Sub

    Public Sub acf_MarcasDeleteByPK _
      (ByVal IdMarca As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_MarcasDeleteByPK", _
         IdMarca _
         )
    End Sub

    Public Sub acf_MarcasInsert _
    (ByVal entidad As acf_Marcas)

        db.ExecuteNonQuery("acf_MarcasInsert", _
         entidad.IdMarca _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_MarcasInsert _
    (ByVal entidad As acf_Marcas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_MarcasInsert", _
         entidad.IdMarca _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_MarcasUpdate _
    (ByVal entidad As acf_Marcas)

        db.ExecuteNonQuery("acf_MarcasUpdate", _
         entidad.IdMarca _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_MarcasUpdate _
    (ByVal entidad As acf_Marcas, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_MarcasUpdate", _
         entidad.IdMarca _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "acf_Modelos"
    Public Function acf_ModelosSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_ModelosSelectAll").Tables(0)
    End Function

    Public Function acf_ModelosSelectByPK _
      (ByVal IdModelo As System.Int32 _
      ) As acf_Modelos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("acf_ModelosSelectByPK", _
         IdModelo _
         ).Tables(0)

        Dim Entidad As New acf_Modelos
        If dt.Rows.Count > 0 Then
            Entidad.IdModelo = dt.Rows(0).Item("IdModelo")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub acf_ModelosDeleteByPK _
      (ByVal IdModelo As System.Int32 _
      )

        db.ExecuteNonQuery("acf_ModelosDeleteByPK", _
         IdModelo _
         )
    End Sub

    Public Sub acf_ModelosDeleteByPK _
      (ByVal IdModelo As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_ModelosDeleteByPK", _
         IdModelo _
         )
    End Sub

    Public Sub acf_ModelosInsert _
    (ByVal entidad As acf_Modelos)

        db.ExecuteNonQuery("acf_ModelosInsert", _
         entidad.IdModelo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_ModelosInsert _
    (ByVal entidad As acf_Modelos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_ModelosInsert", _
         entidad.IdModelo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_ModelosUpdate _
    (ByVal entidad As acf_Modelos)

        db.ExecuteNonQuery("acf_ModelosUpdate", _
         entidad.IdModelo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_ModelosUpdate _
    (ByVal entidad As acf_Modelos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_ModelosUpdate", _
         entidad.IdModelo _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "acf_MotivosBaja"
    Public Function acf_MotivosBajaSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_MotivosBajaSelectAll").Tables(0)
    End Function

    Public Function acf_MotivosBajaSelectByPK _
      (ByVal IdMotivo As System.Int32 _
      ) As acf_MotivosBaja

        Dim dt As DataTable
        dt = db.ExecuteDataSet("acf_MotivosBajaSelectByPK", _
         IdMotivo _
         ).Tables(0)

        Dim Entidad As New acf_MotivosBaja
        If dt.Rows.Count > 0 Then
            Entidad.IdMotivo = dt.Rows(0).Item("IdMotivo")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub acf_MotivosBajaDeleteByPK _
      (ByVal IdMotivo As System.Int32 _
      )

        db.ExecuteNonQuery("acf_MotivosBajaDeleteByPK", _
         IdMotivo _
         )
    End Sub

    Public Sub acf_MotivosBajaDeleteByPK _
      (ByVal IdMotivo As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_MotivosBajaDeleteByPK", _
         IdMotivo _
         )
    End Sub

    Public Sub acf_MotivosBajaInsert _
    (ByVal entidad As acf_MotivosBaja)

        db.ExecuteNonQuery("acf_MotivosBajaInsert", _
         entidad.IdMotivo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_MotivosBajaInsert _
    (ByVal entidad As acf_MotivosBaja, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_MotivosBajaInsert", _
         entidad.IdMotivo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_MotivosBajaUpdate _
    (ByVal entidad As acf_MotivosBaja)

        db.ExecuteNonQuery("acf_MotivosBajaUpdate", _
         entidad.IdMotivo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_MotivosBajaUpdate _
    (ByVal entidad As acf_MotivosBaja, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_MotivosBajaUpdate", _
         entidad.IdMotivo _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "acf_Tecnicos"
    Public Function acf_TecnicosSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_TecnicosSelectAll").Tables(0)
    End Function

    Public Function acf_TecnicosSelectByPK _
      (ByVal IdTecnico As System.Int32 _
      ) As acf_Tecnicos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("acf_TecnicosSelectByPK", _
         IdTecnico _
         ).Tables(0)

        Dim Entidad As New acf_Tecnicos
        If dt.Rows.Count > 0 Then
            Entidad.IdTecnico = dt.Rows(0).Item("IdTecnico")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.Empresa = dt.Rows(0).Item("Empresa")
            Entidad.Telefono = dt.Rows(0).Item("Telefono")
            Entidad.Email = dt.Rows(0).Item("Email")

        End If
        Return Entidad
    End Function

    Public Sub acf_TecnicosDeleteByPK _
      (ByVal IdTecnico As System.Int32 _
      )

        db.ExecuteNonQuery("acf_TecnicosDeleteByPK", _
         IdTecnico _
         )
    End Sub

    Public Sub acf_TecnicosDeleteByPK _
      (ByVal IdTecnico As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TecnicosDeleteByPK", _
         IdTecnico _
         )
    End Sub

    Public Sub acf_TecnicosInsert _
    (ByVal entidad As acf_Tecnicos)

        db.ExecuteNonQuery("acf_TecnicosInsert", _
         entidad.IdTecnico _
         , entidad.Nombre _
         , entidad.Empresa _
         , entidad.Telefono _
         , entidad.Email _
         )
    End Sub

    Public Sub acf_TecnicosInsert _
    (ByVal entidad As acf_Tecnicos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TecnicosInsert", _
         entidad.IdTecnico _
         , entidad.Nombre _
         , entidad.Empresa _
         , entidad.Telefono _
         , entidad.Email _
         )
    End Sub

    Public Sub acf_TecnicosUpdate _
    (ByVal entidad As acf_Tecnicos)

        db.ExecuteNonQuery("acf_TecnicosUpdate", _
         entidad.IdTecnico _
         , entidad.Nombre _
         , entidad.Empresa _
         , entidad.Telefono _
         , entidad.Email _
         )
    End Sub

    Public Sub acf_TecnicosUpdate _
    (ByVal entidad As acf_Tecnicos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TecnicosUpdate", _
         entidad.IdTecnico _
         , entidad.Nombre _
         , entidad.Empresa _
         , entidad.Telefono _
         , entidad.Email _
         )
    End Sub

#End Region
#Region "acf_TiposAjuste"
    Public Function acf_TiposAjusteSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_TiposAjusteSelectAll").Tables(0)
    End Function

    Public Function acf_TiposAjusteSelectByPK _
      (ByVal IdTipo As System.Byte _
      ) As acf_TiposAjuste

        Dim dt As DataTable
        dt = db.ExecuteDataSet("acf_TiposAjusteSelectByPK", _
         IdTipo _
         ).Tables(0)

        Dim Entidad As New acf_TiposAjuste
        If dt.Rows.Count > 0 Then
            Entidad.IdTipo = dt.Rows(0).Item("IdTipo")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")
            Entidad.TipoAplicacion = dt.Rows(0).Item("TipoAplicacion")

        End If
        Return Entidad
    End Function

    Public Sub acf_TiposAjusteDeleteByPK _
      (ByVal IdTipo As System.Byte _
      )

        db.ExecuteNonQuery("acf_TiposAjusteDeleteByPK", _
         IdTipo _
         )
    End Sub

    Public Sub acf_TiposAjusteDeleteByPK _
      (ByVal IdTipo As System.Byte _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TiposAjusteDeleteByPK", _
         IdTipo _
         )
    End Sub

    Public Sub acf_TiposAjusteInsert _
    (ByVal entidad As acf_TiposAjuste)

        db.ExecuteNonQuery("acf_TiposAjusteInsert", _
         entidad.IdTipo _
         , entidad.Nombre _
         , entidad.TipoAplicacion _
         )
    End Sub

    Public Sub acf_TiposAjusteInsert _
    (ByVal entidad As acf_TiposAjuste, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TiposAjusteInsert", _
         entidad.IdTipo _
         , entidad.Nombre _
         , entidad.TipoAplicacion _
         )
    End Sub

    Public Sub acf_TiposAjusteUpdate _
    (ByVal entidad As acf_TiposAjuste)

        db.ExecuteNonQuery("acf_TiposAjusteUpdate", _
         entidad.IdTipo _
         , entidad.Nombre _
         , entidad.TipoAplicacion _
         )
    End Sub

    Public Sub acf_TiposAjusteUpdate _
    (ByVal entidad As acf_TiposAjuste, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TiposAjusteUpdate", _
         entidad.IdTipo _
         , entidad.Nombre _
         , entidad.TipoAplicacion _
         )
    End Sub

#End Region
#Region "acf_TiposDepreciacion"
    Public Function acf_TiposDepreciacionSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_TiposDepreciacionSelectAll").Tables(0)
    End Function

    Public Function acf_TiposDepreciacionSelectByPK _
      (ByVal IdTipo As System.Byte _
      ) As acf_TiposDepreciacion

        Dim dt As DataTable
        dt = db.ExecuteDataSet("acf_TiposDepreciacionSelectByPK", _
         IdTipo _
         ).Tables(0)

        Dim Entidad As New acf_TiposDepreciacion
        If dt.Rows.Count > 0 Then
            Entidad.IdTipo = dt.Rows(0).Item("IdTipo")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub acf_TiposDepreciacionDeleteByPK _
      (ByVal IdTipo As System.Byte _
      )

        db.ExecuteNonQuery("acf_TiposDepreciacionDeleteByPK", _
         IdTipo _
         )
    End Sub

    Public Sub acf_TiposDepreciacionDeleteByPK _
      (ByVal IdTipo As System.Byte _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TiposDepreciacionDeleteByPK", _
         IdTipo _
         )
    End Sub

    Public Sub acf_TiposDepreciacionInsert _
    (ByVal entidad As acf_TiposDepreciacion)

        db.ExecuteNonQuery("acf_TiposDepreciacionInsert", _
         entidad.IdTipo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_TiposDepreciacionInsert _
    (ByVal entidad As acf_TiposDepreciacion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TiposDepreciacionInsert", _
         entidad.IdTipo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_TiposDepreciacionUpdate _
    (ByVal entidad As acf_TiposDepreciacion)

        db.ExecuteNonQuery("acf_TiposDepreciacionUpdate", _
         entidad.IdTipo _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_TiposDepreciacionUpdate _
    (ByVal entidad As acf_TiposDepreciacion, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TiposDepreciacionUpdate", _
         entidad.IdTipo _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "acf_Ubicaciones"
    Public Function acf_UbicacionesSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_UbicacionesSelectAll").Tables(0)
    End Function

    Public Function acf_UbicacionesSelectByPK _
      (ByVal IdUbicacion As System.Int16 _
      ) As acf_Ubicaciones

        Dim dt As DataTable
        dt = db.ExecuteDataSet("acf_UbicacionesSelectByPK", _
         IdUbicacion _
         ).Tables(0)

        Dim Entidad As New acf_Ubicaciones
        If dt.Rows.Count > 0 Then
            Entidad.IdUbicacion = dt.Rows(0).Item("IdUbicacion")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub acf_UbicacionesDeleteByPK _
      (ByVal IdUbicacion As System.Int16 _
      )

        db.ExecuteNonQuery("acf_UbicacionesDeleteByPK", _
         IdUbicacion _
         )
    End Sub

    Public Sub acf_UbicacionesDeleteByPK _
      (ByVal IdUbicacion As System.Int16 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_UbicacionesDeleteByPK", _
         IdUbicacion _
         )
    End Sub

    Public Sub acf_UbicacionesInsert _
    (ByVal entidad As acf_Ubicaciones)

        db.ExecuteNonQuery("acf_UbicacionesInsert", _
         entidad.IdUbicacion _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_UbicacionesInsert _
    (ByVal entidad As acf_Ubicaciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_UbicacionesInsert", _
         entidad.IdUbicacion _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_UbicacionesUpdate _
    (ByVal entidad As acf_Ubicaciones)

        db.ExecuteNonQuery("acf_UbicacionesUpdate", _
         entidad.IdUbicacion _
         , entidad.Nombre _
         )
    End Sub

    Public Sub acf_UbicacionesUpdate _
    (ByVal entidad As acf_Ubicaciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_UbicacionesUpdate", _
         entidad.IdUbicacion _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "acf_Traslados"
    Public Function acf_TrasladosSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_TrasladosSelectAll").Tables(0)
    End Function

    Public Function acf_TrasladosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As acf_Traslados

        Dim dt As datatable
        dt = db.ExecuteDataSet("acf_TrasladosSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New acf_Traslados
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.NumeroComprobante = dt.rows(0).item("NumeroComprobante")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.IdUbicacionAnt = dt.rows(0).item("IdUbicacionAnt")
            entidad.IdUbicacionAct = dt.rows(0).item("IdUbicacionAct")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub acf_TrasladosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("acf_TrasladosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub acf_TrasladosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TrasladosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub acf_TrasladosInsert _
    (ByVal entidad As acf_Traslados)

        db.ExecuteNonQuery("acf_TrasladosInsert", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdUbicacionAnt _
         , entidad.IdUbicacionAct _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub acf_TrasladosInsert _
    (ByVal entidad As acf_Traslados, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TrasladosInsert", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdUbicacionAnt _
         , entidad.IdUbicacionAct _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub acf_TrasladosUpdate _
    (ByVal entidad As acf_Traslados)

        db.ExecuteNonQuery("acf_TrasladosUpdate", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdUbicacionAnt _
         , entidad.IdUbicacionAct _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub acf_TrasladosUpdate _
    (ByVal entidad As acf_Traslados, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TrasladosUpdate", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.Concepto _
         , entidad.IdUbicacionAnt _
         , entidad.IdUbicacionAct _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "acf_TrasladosDetalle"
    Public Function acf_TrasladosDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("acf_TrasladosDetalleSelectAll").Tables(0)
    End Function

    Public Function acf_TrasladosDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As acf_TrasladosDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("acf_TrasladosDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New acf_TrasladosDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdActivo = dt.rows(0).item("IdActivo")
            entidad.Descripcion = dt.rows(0).item("Descripcion")
            entidad.IdResponsableAnt = dt.rows(0).item("IdResponsableAnt")
            entidad.IdResponsableAct = dt.rows(0).item("IdResponsableAct")

        End If
        Return Entidad
    End Function

    Public Sub acf_TrasladosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("acf_TrasladosDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub acf_TrasladosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TrasladosDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub acf_TrasladosDetalleInsert _
    (ByVal entidad As acf_TrasladosDetalle)

        db.ExecuteNonQuery("acf_TrasladosDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdActivo _
         , entidad.Descripcion _
         , entidad.IdResponsableAnt _
         , entidad.IdResponsableAct _
         )
    End Sub

    Public Sub acf_TrasladosDetalleInsert _
    (ByVal entidad As acf_TrasladosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TrasladosDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdActivo _
         , entidad.Descripcion _
         , entidad.IdResponsableAnt _
         , entidad.IdResponsableAct _
         )
    End Sub

    Public Sub acf_TrasladosDetalleUpdate _
    (ByVal entidad As acf_TrasladosDetalle)

        db.ExecuteNonQuery("acf_TrasladosDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdActivo _
         , entidad.Descripcion _
         , entidad.IdResponsableAnt _
         , entidad.IdResponsableAct _
         )
    End Sub

    Public Sub acf_TrasladosDetalleUpdate _
    (ByVal entidad As acf_TrasladosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "acf_TrasladosDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdActivo _
         , entidad.Descripcion _
         , entidad.IdResponsableAnt _
         , entidad.IdResponsableAct _
         )
    End Sub

#End Region

#End Region
#Region "pla_Empleados"
    Public Function pla_EmpleadosSelectAll() As DataTable
        Return db.ExecuteDataSet("pla_EmpleadosSelectAll").Tables(0)
    End Function

    Public Function pla_EmpleadosSelectByPK(ByVal IdEmpleado As Int32) As pla_Empleados

        Dim dt As DataTable = db.ExecuteDataSet("pla_EmpleadosSelectByPK", IdEmpleado).Tables(0)

        Dim Entidad As New pla_Empleados
        If dt.Rows.Count > 0 Then
            Entidad.IdEmpleado = dt.Rows(0).Item("IdEmpleado")
            Entidad.Codigo = dt.Rows(0).Item("Codigo")
            Entidad.Nombres = dt.Rows(0).Item("Nombres")
            Entidad.Apellidos = dt.Rows(0).Item("Apellidos")
            Entidad.IdSexo = dt.Rows(0).Item("IdSexo")
            Entidad.NombresAfiliacion = dt.Rows(0).Item("NombresAfiliacion")
            Entidad.ApellidosAfiliacion = dt.Rows(0).Item("ApellidosAfiliacion")
            Entidad.NombresAfp = dt.Rows(0).Item("NombresAfp")
            Entidad.ApellidosAfp = dt.Rows(0).Item("ApellidosAfp")
            Entidad.NombresNIT = dt.Rows(0).Item("NombresNIT")
            Entidad.ApellidosNIT = dt.Rows(0).Item("ApellidosNIT")
            Entidad.Direccion = dt.Rows(0).Item("Direccion")
            Entidad.IdDepto = dt.Rows(0).Item("IdDepto")
            Entidad.IdMunicipio = dt.Rows(0).Item("IdMunicipio")
            Entidad.Telefono1 = dt.Rows(0).Item("Telefono1")
            Entidad.Telefono2 = dt.Rows(0).Item("Telefono2")
            Entidad.TelefonoMovil = dt.Rows(0).Item("TelefonoMovil")
            Entidad.Email = dt.Rows(0).Item("Email")
            Entidad.NumeroDocIdentidad = dt.Rows(0).Item("NumeroDocIdentidad")
            
        End If
        Return Entidad
    End Function

#End Region

#Region "Eventos"
#Region "eve_Eventos"
    Public Function eve_EventosSelectAll() As DataTable
        Return db.ExecuteDataSet("eve_EventosSelectAll").Tables(0)
    End Function

    Public Function eve_EventosSelectByPK _
      (ByVal IdEvento As System.Int32 _
      ) As eve_Eventos

        Dim dt As DataTable
        dt = db.ExecuteDataSet("eve_EventosSelectByPK", _
         IdEvento _
         ).Tables(0)

        Dim Entidad As New eve_Eventos
        If dt.Rows.Count > 0 Then
            Entidad.IdEvento = dt.Rows(0).Item("IdEvento")
            Entidad.IdSucursal = dt.Rows(0).Item("IdSucursal")
            Entidad.CodEvento = dt.Rows(0).Item("CodEvento")
            Entidad.FechaInicio = dt.Rows(0).Item("FechaInicio")
            Entidad.FechaFin = dt.Rows(0).Item("FechaFin")
            Entidad.DescripcionEvento = dt.Rows(0).Item("DescripcionEvento")
            Entidad.Grupo = dt.Rows(0).Item("Grupo")
            Entidad.Cerrado = dt.Rows(0).Item("Cerrado")
            Entidad.IdCuentaContable = dt.Rows(0).Item("IdCuentaContable")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub eve_EventosDeleteByPK _
      (ByVal IdEvento As System.Int32 _
      )

        db.ExecuteNonQuery("eve_EventosDeleteByPK", _
         IdEvento _
         )
    End Sub

    Public Sub eve_EventosDeleteByPK _
      (ByVal IdEvento As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_EventosDeleteByPK", _
         IdEvento _
         )
    End Sub

    Public Sub eve_EventosInsert _
    (ByVal entidad As eve_Eventos)

        db.ExecuteNonQuery("eve_EventosInsert", _
         entidad.IdEvento _
         , entidad.IdSucursal _
         , entidad.CodEvento _
         , entidad.FechaInicio _
         , entidad.FechaFin _
         , entidad.DescripcionEvento _
         , entidad.Grupo _
         , entidad.Cerrado _
         , entidad.IdCuentaContable _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub eve_EventosInsert _
    (ByVal entidad As eve_Eventos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_EventosInsert", _
         entidad.IdEvento _
         , entidad.IdSucursal _
         , entidad.CodEvento _
         , entidad.FechaInicio _
         , entidad.FechaFin _
         , entidad.DescripcionEvento _
         , entidad.Grupo _
         , entidad.Cerrado _
          , entidad.IdCuentaContable _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub eve_EventosUpdate _
    (ByVal entidad As eve_Eventos)

        db.ExecuteNonQuery("eve_EventosUpdate", _
         entidad.IdEvento _
         , entidad.IdSucursal _
         , entidad.CodEvento _
         , entidad.FechaInicio _
         , entidad.FechaFin _
         , entidad.DescripcionEvento _
         , entidad.Grupo _
         , entidad.Cerrado _
          , entidad.IdCuentaContable _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub eve_EventosUpdate _
    (ByVal entidad As eve_Eventos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_EventosUpdate", _
         entidad.IdEvento _
         , entidad.IdSucursal _
         , entidad.CodEvento _
         , entidad.FechaInicio _
         , entidad.FechaFin _
         , entidad.DescripcionEvento _
         , entidad.Grupo _
         , entidad.Cerrado _
          , entidad.IdCuentaContable _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "eve_EventosDetalle"
    Public Function eve_EventosDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("eve_EventosDetalleSelectAll").Tables(0)
    End Function

    Public Function eve_EventosDetalleSelectByPK _
      (ByVal IdEvento As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As eve_EventosDetalle

        Dim dt As DataTable
        dt = db.ExecuteDataSet("eve_EventosDetalleSelectByPK", _
         IdEvento _
         , IdDetalle _
         ).Tables(0)

        Dim Entidad As New eve_EventosDetalle
        If dt.Rows.Count > 0 Then
            Entidad.IdEvento = dt.Rows(0).Item("IdEvento")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.IdSeminario = dt.Rows(0).Item("IdSeminario")
            Entidad.NumHoras = dt.Rows(0).Item("NumHoras")
            Entidad.PrecioSocio = dt.Rows(0).Item("PrecioSocio")
            Entidad.PrecioNoSocio = dt.Rows(0).Item("PrecioNoSocio")

        End If
        Return Entidad
    End Function

    Public Sub eve_EventosDetalleDeleteByPK _
      (ByVal IdEvento As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("eve_EventosDetalleDeleteByPK", _
         IdEvento _
         , IdDetalle _
         )
    End Sub

    Public Sub eve_EventosDetalleDeleteByPK _
      (ByVal IdEvento As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_EventosDetalleDeleteByPK", _
         IdEvento _
         , IdDetalle _
         )
    End Sub

    Public Sub eve_EventosDetalleInsert _
    (ByVal entidad As eve_EventosDetalle)

        db.ExecuteNonQuery("eve_EventosDetalleInsert", _
         entidad.IdEvento _
         , entidad.IdDetalle _
         , entidad.IdSeminario _
         , entidad.NumHoras _
         , entidad.PrecioSocio _
         , entidad.PrecioNoSocio _
         )
    End Sub

    Public Sub eve_EventosDetalleInsert _
    (ByVal entidad As eve_EventosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_EventosDetalleInsert", _
         entidad.IdEvento _
         , entidad.IdDetalle _
         , entidad.IdSeminario _
         , entidad.NumHoras _
         , entidad.PrecioSocio _
         , entidad.PrecioNoSocio _
         )
    End Sub

    Public Sub eve_EventosDetalleUpdate _
    (ByVal entidad As eve_EventosDetalle)

        db.ExecuteNonQuery("eve_EventosDetalleUpdate", _
         entidad.IdEvento _
         , entidad.IdDetalle _
         , entidad.IdSeminario _
         , entidad.NumHoras _
         , entidad.PrecioSocio _
         , entidad.PrecioNoSocio _
         )
    End Sub

    Public Sub eve_EventosDetalleUpdate _
    (ByVal entidad As eve_EventosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_EventosDetalleUpdate", _
         entidad.IdEvento _
         , entidad.IdDetalle _
         , entidad.IdSeminario _
         , entidad.NumHoras _
         , entidad.PrecioSocio _
         , entidad.PrecioNoSocio _
         )
    End Sub

#End Region
#Region "eve_EventosDetallePago"
    Public Function eve_EventosDetallePagoSelectAll() As DataTable
        Return db.ExecuteDataSet("eve_EventosDetallePagoSelectAll").Tables(0)
    End Function

    Public Function eve_EventosDetallePagoSelectByPK _
      (ByVal IdEvento As System.Int32 _
      , ByVal NumCuota As System.Int32 _
      ) As eve_EventosDetallePago

        Dim dt As datatable
        dt = db.ExecuteDataSet("eve_EventosDetallePagoSelectByPK", _
         IdEvento _
         , NumCuota _
         ).tables(0)

        Dim Entidad As New eve_EventosDetallePago
        If dt.Rows.Count > 0 Then
            entidad.IdEvento = dt.rows(0).item("IdEvento")
            entidad.NumCuota = dt.rows(0).item("NumCuota")
            entidad.PrecioSocio = dt.rows(0).item("PrecioSocio")
            entidad.PrecioNoSocio = dt.rows(0).item("PrecioNoSocio")
            entidad.Fecha = dt.rows(0).item("Fecha")

        End If
        Return Entidad
    End Function

    Public Sub eve_EventosDetallePagoDeleteByPK _
      (ByVal IdEvento As System.Int32 _
      , ByVal NumCuota As System.Int32 _
      )

        db.ExecuteNonQuery("eve_EventosDetallePagoDeleteByPK", _
         IdEvento _
         , NumCuota _
         )
    End Sub

    Public Sub eve_EventosDetallePagoDeleteByPK _
      (ByVal IdEvento As System.Int32 _
      , ByVal NumCuota As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_EventosDetallePagoDeleteByPK", _
         IdEvento _
         , NumCuota _
         )
    End Sub

    Public Sub eve_EventosDetallePagoInsert _
    (ByVal entidad As eve_EventosDetallePago)

        db.ExecuteNonQuery("eve_EventosDetallePagoInsert", _
         entidad.IdEvento _
         , entidad.NumCuota _
         , entidad.PrecioSocio _
         , entidad.PrecioNoSocio _
         , entidad.Fecha _
         )
    End Sub

    Public Sub eve_EventosDetallePagoInsert _
    (ByVal entidad As eve_EventosDetallePago, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_EventosDetallePagoInsert", _
         entidad.IdEvento _
         , entidad.NumCuota _
         , entidad.PrecioSocio _
         , entidad.PrecioNoSocio _
         , entidad.Fecha _
         )
    End Sub

    Public Sub eve_EventosDetallePagoUpdate _
    (ByVal entidad As eve_EventosDetallePago)

        db.ExecuteNonQuery("eve_EventosDetallePagoUpdate", _
         entidad.IdEvento _
         , entidad.NumCuota _
         , entidad.PrecioSocio _
         , entidad.PrecioNoSocio _
         , entidad.Fecha _
         )
    End Sub

    Public Sub eve_EventosDetallePagoUpdate _
    (ByVal entidad As eve_EventosDetallePago, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_EventosDetallePagoUpdate", _
         entidad.IdEvento _
         , entidad.NumCuota _
         , entidad.PrecioSocio _
         , entidad.PrecioNoSocio _
         , entidad.Fecha _
         )
    End Sub

#End Region
#Region "eve_CentrosCostos"
    Public Function eve_CentrosCostosSelectAll() As DataTable
        Return db.ExecuteDataSet("eve_CentrosCostosSelectAll").Tables(0)
    End Function

    Public Function eve_CentrosCostosSelectByPK _
      (ByVal IdCentro As System.Int32 _
      ) As eve_CentrosCostos

        Dim dt As datatable
        dt = db.ExecuteDataSet("eve_CentrosCostosSelectByPK", _
         IdCentro _
         ).tables(0)

        Dim Entidad As New eve_CentrosCostos
        If dt.Rows.Count > 0 Then
            entidad.IdCentro = dt.rows(0).item("IdCentro")
            entidad.Nombre = dt.rows(0).item("Nombre")
            entidad.Efecto = dt.rows(0).item("Efecto")

        End If
        Return Entidad
    End Function

    Public Sub eve_CentrosCostosDeleteByPK _
      (ByVal IdCentro As System.Int32 _
      )

        db.ExecuteNonQuery("eve_CentrosCostosDeleteByPK", _
         IdCentro _
         )
    End Sub

    Public Sub eve_CentrosCostosDeleteByPK _
      (ByVal IdCentro As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_CentrosCostosDeleteByPK", _
         IdCentro _
         )
    End Sub

    Public Sub eve_CentrosCostosInsert _
    (ByVal entidad As eve_CentrosCostos)

        db.ExecuteNonQuery("eve_CentrosCostosInsert", _
         entidad.IdCentro _
         , entidad.Nombre _
         , entidad.Efecto _
         )
    End Sub

    Public Sub eve_CentrosCostosInsert _
    (ByVal entidad As eve_CentrosCostos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_CentrosCostosInsert", _
         entidad.IdCentro _
         , entidad.Nombre _
         , entidad.Efecto _
         )
    End Sub

    Public Sub eve_CentrosCostosUpdate _
    (ByVal entidad As eve_CentrosCostos)

        db.ExecuteNonQuery("eve_CentrosCostosUpdate", _
         entidad.IdCentro _
         , entidad.Nombre _
         , entidad.Efecto _
         )
    End Sub

    Public Sub eve_CentrosCostosUpdate _
    (ByVal entidad As eve_CentrosCostos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_CentrosCostosUpdate", _
         entidad.IdCentro _
         , entidad.Nombre _
         , entidad.Efecto _
         )
    End Sub

#End Region

#Region "eve_Seminarios"
    Public Function eve_SeminariosSelectAll() As DataTable
        Return db.ExecuteDataSet("eve_SeminariosSelectAll").Tables(0)
    End Function

    Public Function eve_SeminariosSelectByPK _
      (ByVal IdSeminario As System.Int32 _
      ) As eve_Seminarios

        Dim dt As DataTable
        dt = db.ExecuteDataSet("eve_SeminariosSelectByPK", _
         IdSeminario _
         ).Tables(0)

        Dim Entidad As New eve_Seminarios
        If dt.Rows.Count > 0 Then
            Entidad.IdSeminario = dt.Rows(0).Item("IdSeminario")
            Entidad.Nombre = dt.Rows(0).Item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub eve_SeminariosDeleteByPK _
      (ByVal IdSeminario As System.Int32 _
      )

        db.ExecuteNonQuery("eve_SeminariosDeleteByPK", _
         IdSeminario _
         )
    End Sub

    Public Sub eve_SeminariosDeleteByPK _
      (ByVal IdSeminario As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_SeminariosDeleteByPK", _
         IdSeminario _
         )
    End Sub

    Public Sub eve_SeminariosInsert _
    (ByVal entidad As eve_Seminarios)

        db.ExecuteNonQuery("eve_SeminariosInsert", _
         entidad.IdSeminario _
         , entidad.Nombre _
         )
    End Sub

    Public Sub eve_SeminariosInsert _
    (ByVal entidad As eve_Seminarios, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_SeminariosInsert", _
         entidad.IdSeminario _
         , entidad.Nombre _
         )
    End Sub

    Public Sub eve_SeminariosUpdate _
    (ByVal entidad As eve_Seminarios)

        db.ExecuteNonQuery("eve_SeminariosUpdate", _
         entidad.IdSeminario _
         , entidad.Nombre _
         )
    End Sub

    Public Sub eve_SeminariosUpdate _
    (ByVal entidad As eve_Seminarios, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_SeminariosUpdate", _
         entidad.IdSeminario _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "eve_Inscripciones"
    Public Function eve_InscripcionesSelectAll() As DataTable
        Return db.ExecuteDataSet("eve_InscripcionesSelectAll").Tables(0)
    End Function

    Public Function eve_InscripcionesSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As eve_Inscripciones

        Dim dt As DataTable
        dt = db.ExecuteDataSet("eve_InscripcionesSelectByPK", _
         IdComprobante _
         ).Tables(0)

        Dim Entidad As New eve_Inscripciones
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdCliente = dt.Rows(0).Item("IdCliente")
            Entidad.Fecha = dt.Rows(0).Item("Fecha")
            Entidad.IdEvento = dt.Rows(0).Item("IdEvento")
            Entidad.IdTipoComprobante = dt.Rows(0).Item("IdTipoComprobante")
            Entidad.Responsable = dt.Rows(0).Item("Responsable")
            Entidad.AplicaRetencion = dt.Rows(0).Item("AplicaRetencion")
            Entidad.ResponsablePago = dt.Rows(0).Item("ResponsablePago")
            Entidad.CargoResponsablePago = dt.Rows(0).Item("CargoResponsablePago")
            Entidad.TelefonosResponsablePago = dt.Rows(0).Item("TelefonosResponsablePago")
            Entidad.DeptoResponsablePago = dt.Rows(0).Item("DeptoResponsablePago")
            Entidad.DetallarParticipantes = dt.Rows(0).Item("DetallarParticipantes")
            Entidad.IdTipoPago = dt.Rows(0).Item("IdTipoPago")
            Entidad.FechaUltAbono = dt.Rows(0).Item("FechaUltAbono")
            Entidad.FacturarPorGrupo = dt.Rows(0).Item("FacturarPorGrupo")
            Entidad.FacturarHoras = dt.Rows(0).Item("FacturarHoras")
            Entidad.CreadoPor = dt.Rows(0).Item("CreadoPor")
            Entidad.FechaHoraCreacion = dt.Rows(0).Item("FechaHoraCreacion")
            Entidad.ModificadoPor = dt.Rows(0).Item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub eve_InscripcionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("eve_InscripcionesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub eve_InscripcionesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_InscripcionesDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub eve_InscripcionesInsert _
    (ByVal entidad As eve_Inscripciones)

        db.ExecuteNonQuery("eve_InscripcionesInsert", _
         entidad.IdComprobante _
         , entidad.IdCliente _
         , entidad.Fecha _
         , entidad.IdEvento _
         , entidad.IdTipoComprobante _
         , entidad.Responsable _
         , entidad.AplicaRetencion _
         , entidad.ResponsablePago _
         , entidad.CargoResponsablePago _
         , entidad.TelefonosResponsablePago _
         , entidad.DeptoResponsablePago _
         , entidad.DetallarParticipantes _
         , entidad.IdTipoPago _
         , entidad.FechaUltAbono _
         , entidad.FacturarPorGrupo _
         , entidad.FacturarHoras _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub eve_InscripcionesInsert _
    (ByVal entidad As eve_Inscripciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_InscripcionesInsert", _
         entidad.IdComprobante _
         , entidad.IdCliente _
         , entidad.Fecha _
         , entidad.IdEvento _
         , entidad.IdTipoComprobante _
         , entidad.Responsable _
         , entidad.AplicaRetencion _
         , entidad.ResponsablePago _
         , entidad.CargoResponsablePago _
         , entidad.TelefonosResponsablePago _
         , entidad.DeptoResponsablePago _
         , entidad.DetallarParticipantes _
         , entidad.IdTipoPago _
         , entidad.FechaUltAbono _
          , entidad.FacturarPorGrupo _
          , entidad.FacturarHoras _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub eve_InscripcionesUpdate _
    (ByVal entidad As eve_Inscripciones)

        db.ExecuteNonQuery("eve_InscripcionesUpdate", _
         entidad.IdComprobante _
         , entidad.IdCliente _
         , entidad.Fecha _
         , entidad.IdEvento _
         , entidad.IdTipoComprobante _
         , entidad.Responsable _
         , entidad.AplicaRetencion _
         , entidad.ResponsablePago _
         , entidad.CargoResponsablePago _
         , entidad.TelefonosResponsablePago _
         , entidad.DeptoResponsablePago _
         , entidad.DetallarParticipantes _
         , entidad.IdTipoPago _
         , entidad.FechaUltAbono _
          , entidad.FacturarPorGrupo _
          , entidad.FacturarHoras _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub eve_InscripcionesUpdate _
    (ByVal entidad As eve_Inscripciones, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_InscripcionesUpdate", _
         entidad.IdComprobante _
         , entidad.IdCliente _
         , entidad.Fecha _
         , entidad.IdEvento _
         , entidad.IdTipoComprobante _
         , entidad.Responsable _
         , entidad.AplicaRetencion _
         , entidad.ResponsablePago _
         , entidad.CargoResponsablePago _
         , entidad.TelefonosResponsablePago _
         , entidad.DeptoResponsablePago _
         , entidad.DetallarParticipantes _
         , entidad.IdTipoPago _
         , entidad.FechaUltAbono _
          , entidad.FacturarPorGrupo _
          , entidad.FacturarHoras _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "eve_Abonos"
    Public Function eve_AbonosSelectAll() As DataTable
        Return db.ExecuteDataSet("eve_AbonosSelectAll").Tables(0)
    End Function

    Public Function eve_AbonosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As eve_Abonos

        Dim dt As datatable
        dt = db.ExecuteDataSet("eve_AbonosSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New eve_Abonos
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.NumeroComprobante = dt.rows(0).item("NumeroComprobante")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdCuentaBancaria = dt.rows(0).item("IdCuentaBancaria")
            entidad.IdCobrador = dt.rows(0).item("IdCobrador")
            entidad.IdCliente = dt.rows(0).item("IdCliente")
            entidad.IdTipoMov = dt.rows(0).item("IdTipoMov")
            entidad.IdSucursal = dt.rows(0).item("IdSucursal")
            entidad.IdEvento = dt.rows(0).item("IdEvento")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.BancoProcedencia = dt.rows(0).item("BancoProcedencia")
            entidad.NumCheque = dt.rows(0).item("NumCheque")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub eve_AbonosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("eve_AbonosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub eve_AbonosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_AbonosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub eve_AbonosInsert _
    (ByVal entidad As eve_Abonos)

        db.ExecuteNonQuery("eve_AbonosInsert", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdCuentaBancaria _
         , entidad.IdCobrador _
         , entidad.IdCliente _
         , entidad.IdTipoMov _
         , entidad.IdSucursal _
         , entidad.IdEvento _
         , entidad.Concepto _
         , entidad.BancoProcedencia _
         , entidad.NumCheque _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub eve_AbonosInsert _
    (ByVal entidad As eve_Abonos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_AbonosInsert", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdCuentaBancaria _
         , entidad.IdCobrador _
         , entidad.IdCliente _
         , entidad.IdTipoMov _
         , entidad.IdSucursal _
         , entidad.IdEvento _
         , entidad.Concepto _
         , entidad.BancoProcedencia _
         , entidad.NumCheque _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub eve_AbonosUpdate _
    (ByVal entidad As eve_Abonos)

        db.ExecuteNonQuery("eve_AbonosUpdate", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdCuentaBancaria _
         , entidad.IdCobrador _
         , entidad.IdCliente _
         , entidad.IdTipoMov _
         , entidad.IdSucursal _
         , entidad.IdEvento _
         , entidad.Concepto _
         , entidad.BancoProcedencia _
         , entidad.NumCheque _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub eve_AbonosUpdate _
    (ByVal entidad As eve_Abonos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_AbonosUpdate", _
         entidad.IdComprobante _
         , entidad.NumeroComprobante _
         , entidad.Fecha _
         , entidad.IdCuentaBancaria _
         , entidad.IdCobrador _
         , entidad.IdCliente _
         , entidad.IdTipoMov _
         , entidad.IdSucursal _
         , entidad.IdEvento _
         , entidad.Concepto _
         , entidad.BancoProcedencia _
         , entidad.NumCheque _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region
#Region "eve_AbonosDetalle"
    Public Function eve_AbonosDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("eve_AbonosDetalleSelectAll").Tables(0)
    End Function

    Public Function eve_AbonosDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As eve_AbonosDetalle

        Dim dt As datatable
        dt = db.ExecuteDataSet("eve_AbonosDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).tables(0)

        Dim Entidad As New eve_AbonosDetalle
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.IdDetalle = dt.rows(0).item("IdDetalle")
            entidad.IdComprobVenta = dt.rows(0).item("IdComprobVenta")
            entidad.IdParticipante = dt.rows(0).item("IdParticipante")
            entidad.MontoAbonado = dt.rows(0).item("MontoAbonado")
            entidad.SaldoComprobante = dt.rows(0).item("SaldoComprobante")
            entidad.Concepto = dt.rows(0).item("Concepto")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")

        End If
        Return Entidad
    End Function

    Public Sub eve_AbonosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("eve_AbonosDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub eve_AbonosDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_AbonosDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub eve_AbonosDetalleInsert _
    (ByVal entidad As eve_AbonosDetalle)

        db.ExecuteNonQuery("eve_AbonosDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         , entidad.IdParticipante _
         , entidad.MontoAbonado _
         , entidad.SaldoComprobante _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub eve_AbonosDetalleInsert _
    (ByVal entidad As eve_AbonosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_AbonosDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         , entidad.IdParticipante _
         , entidad.MontoAbonado _
         , entidad.SaldoComprobante _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub eve_AbonosDetalleUpdate _
    (ByVal entidad As eve_AbonosDetalle)

        db.ExecuteNonQuery("eve_AbonosDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         , entidad.IdParticipante _
         , entidad.MontoAbonado _
         , entidad.SaldoComprobante _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

    Public Sub eve_AbonosDetalleUpdate _
    (ByVal entidad As eve_AbonosDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_AbonosDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.IdComprobVenta _
         , entidad.IdParticipante _
         , entidad.MontoAbonado _
         , entidad.SaldoComprobante _
         , entidad.Concepto _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         )
    End Sub

#End Region
#Region "eve_InscripcionesDetalle"
    Public Function eve_InscripcionesDetalleSelectAll() As DataTable
        Return db.ExecuteDataSet("eve_InscripcionesDetalleSelectAll").Tables(0)
    End Function

    Public Function eve_InscripcionesDetalleSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      ) As eve_InscripcionesDetalle

        Dim dt As DataTable
        dt = db.ExecuteDataSet("eve_InscripcionesDetalleSelectByPK", _
         IdComprobante _
         , IdDetalle _
         ).Tables(0)

        Dim Entidad As New eve_InscripcionesDetalle
        If dt.Rows.Count > 0 Then
            Entidad.IdComprobante = dt.Rows(0).Item("IdComprobante")
            Entidad.IdDetalle = dt.Rows(0).Item("IdDetalle")
            Entidad.PorcDescto = dt.Rows(0).Item("PorcDescto")
            Entidad.NombreParticipantes = dt.Rows(0).Item("NombreParticipantes")
            Entidad.Socio = dt.Rows(0).Item("Socio")
            Entidad.RequiereHoras = dt.Rows(0).Item("RequiereHoras")
            Entidad.Cantidad = dt.Rows(0).Item("Cantidad")
            Entidad.NumInscripcion = dt.Rows(0).Item("NumInscripcion")
            Entidad.Cargo = dt.Rows(0).Item("Cargo")
            Entidad.CorreoElectronico = dt.Rows(0).Item("CorreoElectronico")

        End If
        Return Entidad
    End Function

    Public Sub eve_InscripcionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      )

        db.ExecuteNonQuery("eve_InscripcionesDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub eve_InscripcionesDetalleDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal IdDetalle As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_InscripcionesDetalleDeleteByPK", _
         IdComprobante _
         , IdDetalle _
         )
    End Sub

    Public Sub eve_InscripcionesDetalleInsert _
    (ByVal entidad As eve_InscripcionesDetalle)

        db.ExecuteNonQuery("eve_InscripcionesDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.PorcDescto _
         , entidad.NombreParticipantes _
         , entidad.Socio _
         , entidad.RequiereHoras _
         , entidad.Cantidad _
         , entidad.NumInscripcion _
         , entidad.Cargo _
         , entidad.CorreoElectronico _
         )
    End Sub

    Public Sub eve_InscripcionesDetalleInsert _
    (ByVal entidad As eve_InscripcionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_InscripcionesDetalleInsert", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.PorcDescto _
         , entidad.NombreParticipantes _
         , entidad.Socio _
         , entidad.RequiereHoras _
         , entidad.Cantidad _
         , entidad.NumInscripcion _
         , entidad.Cargo _
         , entidad.CorreoElectronico _
         )
    End Sub

    Public Sub eve_InscripcionesDetalleUpdate _
    (ByVal entidad As eve_InscripcionesDetalle)

        db.ExecuteNonQuery("eve_InscripcionesDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.PorcDescto _
         , entidad.NombreParticipantes _
         , entidad.Socio _
         , entidad.RequiereHoras _
         , entidad.NumInscripcion _
         , entidad.Cargo _
         , entidad.CorreoElectronico _
         )
    End Sub

    Public Sub eve_InscripcionesDetalleUpdate _
    (ByVal entidad As eve_InscripcionesDetalle, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_InscripcionesDetalleUpdate", _
         entidad.IdComprobante _
         , entidad.IdDetalle _
         , entidad.PorcDescto _
         , entidad.NombreParticipantes _
         , entidad.Socio _
         , entidad.RequiereHoras _
         , entidad.Cantidad _
         , entidad.NumInscripcion _
         , entidad.Cargo _
         , entidad.CorreoElectronico _
         )
    End Sub

#End Region
#Region "eve_InscripcionesParticipantes"
    Public Function eve_InscripcionesParticipantesSelectAll() As DataTable
        Return db.ExecuteDataSet("eve_InscripcionesParticipantesSelectAll").Tables(0)
    End Function

    Public Function eve_InscripcionesParticipantesSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Participante As System.String _
      , ByVal IdSeminario As System.Int32 _
      ) As eve_InscripcionesParticipantes

        Dim dt As datatable
        dt = db.ExecuteDataSet("eve_InscripcionesParticipantesSelectByPK", _
         IdComprobante _
         , Participante _
         , IdSeminario _
         ).tables(0)

        Dim Entidad As New eve_InscripcionesParticipantes
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.Participante = dt.rows(0).item("Participante")
            entidad.IdSeminario = dt.rows(0).item("IdSeminario")
            entidad.Aplica = dt.rows(0).item("Aplica")

        End If
        Return Entidad
    End Function

    Public Sub eve_InscripcionesParticipantesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Participante As System.String _
      , ByVal IdSeminario As System.Int32 _
      )

        db.ExecuteNonQuery("eve_InscripcionesParticipantesDeleteByPK", _
         IdComprobante _
         , Participante _
         , IdSeminario _
         )
    End Sub

    Public Sub eve_InscripcionesParticipantesDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Participante As System.String _
      , ByVal IdSeminario As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_InscripcionesParticipantesDeleteByPK", _
         IdComprobante _
         , Participante _
         , IdSeminario _
         )
    End Sub

    Public Sub eve_InscripcionesParticipantesInsert _
    (ByVal entidad As eve_InscripcionesParticipantes)

        db.ExecuteNonQuery("eve_InscripcionesParticipantesInsert", _
         entidad.IdComprobante _
         , entidad.Participante _
         , entidad.IdSeminario _
         , entidad.Aplica _
         )
    End Sub

    Public Sub eve_InscripcionesParticipantesInsert _
    (ByVal entidad As eve_InscripcionesParticipantes, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_InscripcionesParticipantesInsert", _
         entidad.IdComprobante _
         , entidad.Participante _
         , entidad.IdSeminario _
         , entidad.Aplica _
         )
    End Sub

    Public Sub eve_InscripcionesParticipantesUpdate _
    (ByVal entidad As eve_InscripcionesParticipantes)

        db.ExecuteNonQuery("eve_InscripcionesParticipantesUpdate", _
         entidad.IdComprobante _
         , entidad.Participante _
         , entidad.IdSeminario _
         , entidad.Aplica _
         )
    End Sub

    Public Sub eve_InscripcionesParticipantesUpdate _
    (ByVal entidad As eve_InscripcionesParticipantes, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_InscripcionesParticipantesUpdate", _
         entidad.IdComprobante _
         , entidad.Participante _
         , entidad.IdSeminario _
         , entidad.Aplica _
         )
    End Sub

#End Region

#Region "eve_Participantes"
    Public Function eve_ParticipantesSelectAll() As DataTable
        Return db.ExecuteDataSet("eve_ParticipantesSelectAll").Tables(0)
    End Function

    Public Function eve_ParticipantesSelectByPK _
      (ByVal NumInscripcion As System.String _
      ) As eve_Participantes

        Dim dt As datatable
        dt = db.ExecuteDataSet("eve_ParticipantesSelectByPK", _
         NumInscripcion _
         ).tables(0)

        Dim Entidad As New eve_Participantes
        If dt.Rows.Count > 0 Then
            entidad.NumInscripcion = dt.rows(0).item("NumInscripcion")
            entidad.Nombre = dt.rows(0).item("Nombre")

        End If
        Return Entidad
    End Function

    Public Sub eve_ParticipantesDeleteByPK _
      (ByVal NumInscripcion As System.String _
      )

        db.ExecuteNonQuery("eve_ParticipantesDeleteByPK", _
         NumInscripcion _
         )
    End Sub

    Public Sub eve_ParticipantesDeleteByPK _
      (ByVal NumInscripcion As System.String _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_ParticipantesDeleteByPK", _
         NumInscripcion _
         )
    End Sub

    Public Sub eve_ParticipantesInsert _
    (ByVal entidad As eve_Participantes)

        db.ExecuteNonQuery("eve_ParticipantesInsert", _
         entidad.NumInscripcion _
         , entidad.Nombre _
         )
    End Sub

    Public Sub eve_ParticipantesInsert _
    (ByVal entidad As eve_Participantes, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_ParticipantesInsert", _
         entidad.NumInscripcion _
         , entidad.Nombre _
         )
    End Sub

    Public Sub eve_ParticipantesUpdate _
    (ByVal entidad As eve_Participantes)

        db.ExecuteNonQuery("eve_ParticipantesUpdate", _
         entidad.NumInscripcion _
         , entidad.Nombre _
         )
    End Sub

    Public Sub eve_ParticipantesUpdate _
    (ByVal entidad As eve_Participantes, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_ParticipantesUpdate", _
         entidad.NumInscripcion _
         , entidad.Nombre _
         )
    End Sub

#End Region
#Region "eve_TransaccionesEventos"
    Public Function eve_TransaccionesEventosSelectAll() As DataTable
        Return db.ExecuteDataSet("eve_TransaccionesEventosSelectAll").Tables(0)
    End Function

    Public Function eve_TransaccionesEventosSelectByPK _
      (ByVal IdComprobante As System.Int32 _
      ) As eve_TransaccionesEventos

        Dim dt As datatable
        dt = db.ExecuteDataSet("eve_TransaccionesEventosSelectByPK", _
         IdComprobante _
         ).tables(0)

        Dim Entidad As New eve_TransaccionesEventos
        If dt.Rows.Count > 0 Then
            entidad.IdComprobante = dt.rows(0).item("IdComprobante")
            entidad.Fecha = dt.rows(0).item("Fecha")
            entidad.IdEvento = dt.rows(0).item("IdEvento")
            entidad.IdCentro = dt.rows(0).item("IdCentro")
            entidad.Valor = dt.rows(0).item("Valor")
            entidad.Observaciones = dt.rows(0).item("Observaciones")
            entidad.Autorizado = dt.rows(0).item("Autorizado")
            entidad.CreadoPor = dt.rows(0).item("CreadoPor")
            entidad.FechaHoraCreacion = dt.rows(0).item("FechaHoraCreacion")
            entidad.ModificadoPor = dt.rows(0).item("ModificadoPor")
            Entidad.FechaHoraModificacion = fd.SiEsNulo(dt.Rows(0).Item("FechaHoraModificacion"), Nothing)

        End If
        Return Entidad
    End Function

    Public Sub eve_TransaccionesEventosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      )

        db.ExecuteNonQuery("eve_TransaccionesEventosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub eve_TransaccionesEventosDeleteByPK _
      (ByVal IdComprobante As System.Int32 _
      , ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_TransaccionesEventosDeleteByPK", _
         IdComprobante _
         )
    End Sub

    Public Sub eve_TransaccionesEventosInsert _
    (ByVal entidad As eve_TransaccionesEventos)

        db.ExecuteNonQuery("eve_TransaccionesEventosInsert", _
         entidad.IdComprobante _
         , entidad.Fecha _
         , entidad.IdEvento _
         , entidad.IdCentro _
         , entidad.Valor _
         , entidad.Observaciones _
         , entidad.Autorizado _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub eve_TransaccionesEventosInsert _
    (ByVal entidad As eve_TransaccionesEventos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_TransaccionesEventosInsert", _
         entidad.IdComprobante _
         , entidad.Fecha _
         , entidad.IdEvento _
         , entidad.IdCentro _
         , entidad.Valor _
         , entidad.Observaciones _
         , entidad.Autorizado _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub eve_TransaccionesEventosUpdate _
    (ByVal entidad As eve_TransaccionesEventos)

        db.ExecuteNonQuery("eve_TransaccionesEventosUpdate", _
         entidad.IdComprobante _
         , entidad.Fecha _
         , entidad.IdEvento _
         , entidad.IdCentro _
         , entidad.Valor _
         , entidad.Observaciones _
         , entidad.Autorizado _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

    Public Sub eve_TransaccionesEventosUpdate _
    (ByVal entidad As eve_TransaccionesEventos, ByVal Transaccion As DbTransaction)

        db.ExecuteNonQuery(Transaccion, "eve_TransaccionesEventosUpdate", _
         entidad.IdComprobante _
         , entidad.Fecha _
         , entidad.IdEvento _
         , entidad.IdCentro _
         , entidad.Valor _
         , entidad.Observaciones _
         , entidad.Autorizado _
         , entidad.CreadoPor _
         , entidad.FechaHoraCreacion _
         , entidad.ModificadoPor _
         , entidad.FechaHoraModificacion _
         )
    End Sub

#End Region

#End Region




End Class
