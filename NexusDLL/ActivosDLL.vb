﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common
Imports NexusELL.TableEntities

Public Class ActivosDLL
    Dim fd As FuncionesDLL
    Dim db As Database
    Dim objTabla As TableData
    Dim dlConta As ContabilidadDLL
    'En esta clase se encuentra toda la codificación que se necesita para interacción con la base de datos

    Public Sub New(ByVal strConexion As String)

        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db
        fd = New FuncionesDLL(strConexion)
        dlConta = New ContabilidadDLL(strConexion)
        objTabla = New TableData(strConexion)
    End Sub

    Public Function SiExisteActivo(ByVal sCodActivo As String) As Integer
        Dim sSql As String = String.Format("select IdActivo from acf_Activos where Codigo='{0}'", sCodActivo)
        Dim IdActivo As Integer = SiEsNulo(db.ExecuteScalar(CommandType.Text, sSql), 0)
        Return IdActivo
    End Function

    Public Function ObtenerIdDocumento(ByVal Id As Integer, ByVal TipoAvance As Integer, ByVal Tabla As String) As Integer
        Dim sql As String = "select isnull(min(IdComprobante),0) from " & Tabla & " where IdComprobante > " & Id
        If TipoAvance = -1 Then  'para obtener el registro anterior
            sql = "select isnull(max(IdComprobante),0) from " & Tabla & " where IdComprobante < " & Id
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function ListMantenimientos(ByVal IdActivo As Integer, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spListMantenimientos", IdActivo, Desde, Hasta, IdSucursal).Tables(0)
    End Function
    Public Function ListCuadroDiferencias(ByVal Ejercicio As Integer, ByVal Mes As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spListCuadroDiferencias", Ejercicio, Mes, IdSucursal).Tables(0)
    End Function
    Public Function ObtenerDetalleDocumento(ByVal IdComprobante As Integer, ByVal Tabla As String) As DataTable
        Dim sql As String = "select * from " & Tabla & " where IdComprobante=" & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function ImpresionTraslado(ByVal IdComprobante As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spImpresionTraslado", IdComprobante).Tables(0)
    End Function
    Public Function HistorialTraslado(ByVal IdActivo As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spHistorialTraslados", IdActivo).Tables(0)
    End Function

    Public Function ListActivosVendidos(ByVal IdClase As Integer, ByVal FechaI As DateTime, ByVal FechaF As DateTime, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spListActivosVendidos", IdClase, FechaI, FechaF, IdSucursal).Tables(0)
    End Function

    Public Function ListActivosRetirados(ByVal IdClase As Integer, ByVal FechaI As DateTime, ByVal FechaF As DateTime, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spListActivosRetirados", IdClase, FechaI, FechaF, IdSucursal).Tables(0)
    End Function

    Public Function ConsultaActivos(ByVal IdSucursal As Integer) As DataTable

        Dim Condicion As String = " where 1=1 "
        If IdSucursal <> 1 Then
            Condicion = " where IdSucursal= " & IdSucursal
        End If

        Dim sSQL As String = "Select IdActivo, Codigo, IdSucursal, Nombre, ValorAdquisicion, ValorDepreciar, CreadoPor, FechaHoraCreacion from acf_Activos "
        sSQL &= Condicion

        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function

    Public Function GetActivos(ByVal IdClase As Integer, ByVal FechaRetiro As DateTime, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spGetActivos", IdClase, FechaRetiro, IdSucursal).Tables(0)
    End Function
    Public Sub ActualizarRetiro(ByVal IdActivo As Integer, ByVal FechaRetiro As Date, ByVal IdMotivo As Integer, ByVal IdEstado As Integer, ByVal Valoretiro As Decimal)
        Dim sSQL As String = "update acf_Activos set FechaRetiro ='" & Format(FechaRetiro, "yyyyMMdd")
        sSQL += "' , IdMotivoBaja=" & IdMotivo & " , IdEstado=" & IdEstado & " ,ValorRetiro= " & Valoretiro & "  WHERE IdActivo = " & IdActivo
        db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Sub
    Function GuardarActivo(ByRef entActivo As acf_Activos, _
                           ByRef entDetalle As List(Of acf_DepreciacionesAjustes), ByVal EsNuevo As Boolean) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""
        Try
            'todo calcula correlativos y los actualiza en las entidades
            If EsNuevo Then
                entActivo.IdActivo = fd.ObtenerUltimoId("ACF_ACTIVOS", "IdActivo") + 1
                objTabla.acf_ActivosInsert(entActivo, tran)
            Else
                objTabla.acf_ActivosUpdate(entActivo, tran)
                Dim sql = "delete acf_DepreciacionesAjustes where IdActivo = " & entActivo.IdActivo
                db.ExecuteNonQuery(CommandType.Text, sql)
            End If
            'actualizo el detalle de las depreciaciones
            For Each Det As acf_DepreciacionesAjustes In entDetalle
                Det.IdActivo = entActivo.IdActivo
                objTabla.acf_DepreciacionesAjustesInsert(Det, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message()
        Finally
            cn.Close()
        End Try

        Return msj
    End Function

    Function GuardarTaslado(ByRef entTraslado As acf_Traslados, _
                       ByRef entDetalle As List(Of acf_TrasladosDetalle), ByVal EsNuevo As Boolean) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""
        Try
            'todo calcula correlativos y los actualiza en las entidades
            If EsNuevo Then
                entTraslado.IdComprobante = fd.ObtenerUltimoId("ACF_TRASLADOS", "IdComprobante") + 1
                objTabla.acf_TrasladosInsert(entTraslado, tran)
            Else
                objTabla.acf_TrasladosUpdate(entTraslado, tran)
                Dim sql = "delete acf_TrasladosDetalle where IdComprobante = " & entTraslado.IdComprobante
                db.ExecuteNonQuery(CommandType.Text, sql)
            End If

            For Each Det As acf_TrasladosDetalle In entDetalle
                Det.IdComprobante = entTraslado.IdComprobante
                objTabla.acf_TrasladosDetalleInsert(Det, tran)

                Dim sSql As String = "Update acf_Activos  set IdEmpleado = " & Det.IdResponsableAct
                sSql += " where IdActivo = " & Det.IdActivo
                db.ExecuteNonQuery(CommandType.Text, sSql)
            Next

            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message()
        Finally
            cn.Close()
        End Try

        Return msj
    End Function



    Public Function ListActivos(ByVal iIdClase As Integer, ByVal iIdUbica As Integer, ByVal iIdTipo As Integer, ByVal iIdEstado As Integer, ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spList_Activos", iIdClase, iIdUbica, iIdTipo, iIdEstado, dFechaI, dFechaF, IdSucursal).Tables(0)
    End Function

    Public Function rptComprobanteMantenimiento(ByVal IdMantenimiento As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spImprimeMantenimiento", IdMantenimiento).Tables(0)
    End Function

    Public Function ImprimeActivoFijo(ByVal IdActivo As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spImprimeActivoFijo", IdActivo).Tables(0)
    End Function

    Public Function ObtienePeriodoCerrado(ByVal IdSucursal As Integer) As DataTable
        Dim sql As String = "select top 1 Mes, Ejercicio from acf_CierresActivoFijo"
        sql &= " where IdSucursal = " & IdSucursal
        sql &= " order by Ejercicio desc, Mes desc"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Function ContabilizarActivoFijo(ByVal Ejercicio As Integer, ByVal Mes As Integer, ByVal IdTipoPartida As String, ByVal Concepto As String, ByVal CreadoPor As String, ByVal IdSucursal As Integer) As String
        Dim msj As String = db.ExecuteScalar("acf_spContabilizarActivoFijo", Ejercicio, Mes, IdTipoPartida, Concepto, CreadoPor, IdSucursal)
        Dim sSQL As String = ""
        If msj <> "Ok" Then
            Return msj
        Else
            'elimino el mes que se está cerrando por cualquier cosa
            sSQL = "delete acf_CierresActivoFijo where mes=@Mes and Ejercicio=@Ejercicio and IdSucursal= @IdSucursal"
            Dim cmd As DbCommand = db.GetSqlStringCommand(sSQL)
            db.AddInParameter(cmd, "@Mes", DbType.Int32, Mes)
            db.AddInParameter(cmd, "@Ejercicio", DbType.Int32, Ejercicio)
            db.AddInParameter(cmd, "@IdSucursal", DbType.Int32, IdSucursal)
            db.ExecuteScalar(cmd)

            '-- inserto el mes cerrado
            Dim FechaCreacion As String = Format(Now, "yyyyMMdd")
            sSQL = "insert acf_CierresActivoFijo (Ejercicio,Mes, IdSucursal, CreadoPor,FechaHoraCreacion) values ('"
            sSQL &= Ejercicio & "','" & Mes & "','" & IdSucursal & "','" & CreadoPor & "','" & FechaCreacion & "')"
            db.ExecuteNonQuery(CommandType.Text, sSQL)
            Return msj
        End If
    End Function

    Public Function RevertirContabilizarActivoFijo(ByVal Ejercicio As Integer, ByVal Mes As Integer, ByVal IdSucursal As Integer) As String
        Dim msj As String = db.ExecuteScalar("acf_spRevertirContabilizacion", Ejercicio, Mes, IdSucursal)
        Return msj
    End Function

    Public Function ExistePartidaActivo(ByVal Fecha As Date, ByVal IdSucursal As Integer) As Integer
        Dim sql As String = "select max(IdPartida) from con_partidas where IdModuloOrigen=13 and Fecha='" & Format(Fecha, "yyyyMMdd") & "'"
        sql &= " and IdSucursal=" & IdSucursal
        Return SiEsNulo(db.ExecuteScalar(CommandType.Text, sql), 0)
    End Function

    Function ObtenerIdMantenimiento(ByVal IdMantenimiento As Integer, ByVal TipoAvance As Integer) As Integer
        If TipoAvance = 0 Then
            Return IdMantenimiento
        End If
        Dim sql As String = "select top 1 Id from acf_mantenimientos where Id < " & IdMantenimiento & " order by Id desc"
        If TipoAvance = 1 Then
            sql = "select top 1 Id from acf_Mantenimientos where Id > " & IdMantenimiento
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function


    Public Function DatosGeneralesActivoFijo(ByVal NumActivo As String) As DataTable
        Dim sql As String = "select codigo, Nombre,FechaAdquisicion from acf_Activos where Codigo = '" + NumActivo + "'"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function ListActivosUbicaciones(ByVal iIdUbica As Integer, ByVal iIdEmpleado As Integer, ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spListActivosUbicacion", iIdUbica, iIdEmpleado, dFechaI, dFechaF, IdSucursal).Tables(0)
    End Function

    Public Function ListActivosMarcas(ByVal iIdMarca As Integer, ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spListActivosMarca", iIdMarca, dFechaI, dFechaF, IdSucursal).Tables(0)
    End Function
    Public Function TodosActivos(ByVal IdSucursal As Integer) As DataTable

        Dim Condicion As String = " where 1=1 "
        If IdSucursal <> 1 And IdSucursal <> -1 Then
            Condicion = " where acf.IdSucursal= " & IdSucursal
        End If

        Dim sql As String = "select ClaseActivo=cla.Nombre,acf.Codigo, acf.IdSucursal,NombreActivo=acf.Nombre,acf.FechaAdquisicion,acf.ValorAdquisicion,acf.IdActivo from acf_Activos acf inner join acf_Clases cla on acf.IdClase=cla.IdClase "
        sql &= Condicion
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Function ListActivosCuentas(ByVal iIdCuenta As String, ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spListActivosCuentas", iIdCuenta, dFechaI, dFechaF, IdSucursal).Tables(0)
    End Function

    Public Function ListActivosGeneral(ByVal IdClase As Integer, ByVal IdUbicacion As Integer, ByVal iIdCuenta As String, ByVal TipoGrupo As Integer, ByVal Depreciable As Boolean, ByVal Depreciado As Boolean, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spListActivosGeneral", IdClase, IdUbicacion, iIdCuenta, TipoGrupo, Depreciable, Depreciado, dFechaF, IdSucursal).Tables(0)
    End Function
    Public Function ListActivosDepreciados(ByVal IdClase As Integer, ByVal IdUbicacion As Integer, ByVal iIdCuenta As String, ByVal TipoGrupo As Integer, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spListActivosDepreciados", IdClase, IdUbicacion, iIdCuenta, TipoGrupo, dFechaF, IdSucursal).Tables(0)
    End Function


    Public Function ListActivosEstado(ByVal iIdEstado As String, ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("acf_spListActivosEstado", iIdEstado, dFechaI, dFechaF, IdSucursal).Tables(0)
    End Function

    Public Function ListDepreciacionIndividual(ByVal iIdActivo As Integer, ByVal IdTipo As Integer) As DataTable
        If IdTipo = 1 Then
            Return db.ExecuteDataSet("acf_spListDepreciacionIndividual", iIdActivo).Tables(0)
        Else
            Return db.ExecuteDataSet("acf_spListDepreciacionIndividualAnual", iIdActivo).Tables(0)
        End If
    End Function
    Public Shared Function SiEsNulo(ByVal value As Object, ByVal ValueNoNull As Object) As Object
        If value Is Nothing Then Return Nothing
        If IsDBNull(value) Then Return ValueNoNull
        Return value
    End Function

End Class
