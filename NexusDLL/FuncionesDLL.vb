﻿Imports System.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports System.Data.Common
Imports System.Data

Public Class FuncionesDLL
    Private db As Database

    Public Sub New(ByVal strConexion As String)
        'db = DatabaseFactory.CreateDatabase(StringConexion)
        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db
    End Sub
    Public Function getDepartamento(ByVal IdMunicipio As String) As String
        Dim sSQL As String = "select IdDepartamento from adm_municipios where IdMunicipio = '"
        sSQL &= IdMunicipio & "'"
        Return SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), " ")
    End Function
    Public Function ObtenerIdToken(ByVal Token As String) As Integer
        Dim sSQL As String = "select IdToken INTO #Token from fac_Token where Token = '"
        sSQL &= Token & "'  IF @@ROWCOUNT = 0 INSERT INTO #Token SELECT 0 SELECT * FROM #Token DROP TABLE #Token "
        Return SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), " ")
    End Function
    Public Function GetFechaContable(ByVal IdSucursal As Integer) As Date
        Dim sSQL As String = "select max(fecha) from fac_Calendario where activo=1 and IdSucursal=" & IdSucursal
        Dim Fecha As Date = SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), Today)
        Return Fecha
    End Function
    Public Function GetFechaContableCaja(ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As Date
        Dim sSQL As String = "select max(fecha) from fac_Calendario where activo=1 and IdSucursal=" & IdSucursal & " and IdPunto = " & IdPunto
        Dim Fecha As Date = SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), Today)
        Return Fecha
    End Function
    Public Function GetFechaUltAbonoEventoCliente(ByVal IdCliente As String, ByVal IdEvento As Integer) As Date
        Dim sSQL As String = "select fechaultabono from eve_Inscripciones where IdEvento=" & IdEvento
        sSQL += " and IdCliente='" & IdCliente & "'"
        Dim Fecha As Date = db.ExecuteScalar(CommandType.Text, sSQL)
        Return Fecha
    End Function

    Public Function ObtenerUltimoIdAppointment(ByVal nombretable As String) As Integer
        'METER A TRANSACCION
        Dim sSQL As String = "SELECT IDENT_CURRENT ('" & nombretable & "') AS Current_Identity"
        Dim iMax As Integer = SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), 0)
        Return iMax
    End Function
    Public Function GetNumPartida(ByVal TipoPartida As String, ByVal Fecha As Date, ByVal Transac As DbTransaction) As String

        Dim sql As String = "select TipoNumeracionPartidas from adm_parametros"
        Dim TipoNumeracion As Integer = db.ExecuteScalar(CommandType.Text, sql)

        Dim str = ""
        Select Case TipoNumeracion
            Case 1 'Mensual con Tipo de Partida
                str = TipoPartida & Month(Fecha).ToString.PadLeft(2, "0") & Year(Fecha).ToString
            Case 2 'Anual con Tipo de Partida
                str = TipoPartida & Year(Fecha).ToString
            Case 3 'Mensual sin tipo de partida
                str = Month(Fecha).ToString.PadLeft(2, "0") & Year(Fecha).ToString
            Case 4 'Anual sin tipo de partida
                str = Year(Fecha).ToString
        End Select

        sql = "select UltimoValor from adm_correlativos where Correlativo='" & str & "'"
        Dim UltimoValor As Integer = SiEsNulo(db.ExecuteScalar(Transac, CommandType.Text, sql), 0)

        If UltimoValor = 0 Then 'Crea un nuevo registro
            sql = "insert adm_correlativos (correlativo, UltimoValor) values ('" & str & "',1)"
            UltimoValor = 1
        Else 'Actualiza el registro
            UltimoValor += 1
            sql = "update adm_correlativos set UltimoValor = UltimoValor + 1 where correlativo='" & str & "'"
        End If
        db.ExecuteNonQuery(Transac, CommandType.Text, sql)

        Select Case TipoNumeracion
            Case 1 'Mensual por tipo de partida
                Return Month(Fecha).ToString.PadLeft(2, "0") & UltimoValor.ToString.PadLeft(4, "0")
            Case 2 'Anual por tipo de partida
                Return UltimoValor.ToString.PadLeft(6, "0")
            Case 3 'Mensual sin tipo de partida
                Return UltimoValor.ToString.PadLeft(6, "0")
            Case 4 'Anual por Tipo de Partida
                Return UltimoValor.ToString.PadLeft(6, "0")
        End Select

        Return "000000"
    End Function

    Public Function GetNumeroCheque(ByVal IdBanco As Integer, ByVal IdCuentaBancaria As Integer) As Integer
        Dim sSQL As String = "select UltNumeroCheque from ban_CuentasBancarias"
        sSQL &= " where IdBanco= " & IdBanco & " and IdCuentaBancaria = " & IdCuentaBancaria
        Dim UltimoValor As Integer = SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), 0)

        UltimoValor += 1
        Return UltimoValor
    End Function

    Public Function ObtenerNewId() As String
        Dim NewId As String = db.ExecuteScalar(CommandType.Text, "select newid()").ToString()
        Return NewId
    End Function
    Public Function ObtenerUltimoId(ByVal Tabla As String, ByVal PrimaryKey As String) As Integer

        Dim sql As String = "select IsNull(max(" & PrimaryKey & "),0) from " & Tabla
        Dim iMax As Integer = db.ExecuteScalar(CommandType.Text, sql)
        Return iMax

    End Function
    Public Function ObtenerUltimoId(ByVal Tabla As String, ByVal PrimaryKey As String, ByVal Transac As DbTransaction) As Integer
        Dim sql As String = "select IsNull(max(" & PrimaryKey & "),0) from " & Tabla
        Dim iMax As Integer = db.ExecuteScalar(Transac, CommandType.Text, sql)
        Return iMax
    End Function
    Public Function ActualizaAbono(ByVal Tabla As String, ByVal Id As Integer) As Integer

        Dim sSQL As String

        If Tabla = "CHEQUES" Then
            sSQL = "Update cpp_abonos set cancelado=0, idcheque=0 where idcheque=" & Id
            sSQL = "; Update com_compras set idcheque=0 where idcheque=" & Id
        Else
            sSQL = "Update cpp_abonos set cancelado=0, idtransaccion=0 where idtransaccion=" & Id
        End If


        Return db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Function



    Public Function ObtenerCorrelativoManual(ByVal NombreCorrelativo As String, ByVal Transac As DbTransaction) As Integer
        Dim sSQL As String = "select UltimoValor from adm_correlativos where Correlativo='" & NombreCorrelativo & "'"
        Dim NuevoCorrelativo As Integer = SiEsNulo(db.ExecuteScalar(Transac, CommandType.Text, sSQL), 0) + 1

        If NuevoCorrelativo = 1 Then 'Crea un nuevo registro
            sSQL = "insert adm_correlativos (correlativo, UltimoValor) values ('" & NombreCorrelativo & "',1)"
        Else 'Actualiza el registro
            sSQL = "update adm_correlativos set UltimoValor = UltimoValor + 1 where correlativo='" & NombreCorrelativo & "'"
        End If
        db.ExecuteNonQuery(Transac, CommandType.Text, sSQL)

        Return NuevoCorrelativo

    End Function
    Public Function ObtenerCorrelativoManual(ByVal NombreCorrelativo As String) As Integer
        Dim sql As String = "select UltimoValor from adm_correlativos where Correlativo='" & NombreCorrelativo & "'"
        Dim NuevoCorrelativo As Integer = SiEsNulo(db.ExecuteScalar(CommandType.Text, sql), 0) + 1

        'If NuevoCorrelativo = 1 Then 'Crea un nuevo registro
        '    sSQL = "insert adm_correlativos (correlativo, UltimoValor) values ('" & NombreCorrelativo & "',1)"
        'Else 'Actualiza el registro
        '    sSQL = "update adm_correlativos set UltimoValor = UltimoValor + 1 where correlativo='" & NombreCorrelativo & "'"
        'End If
        'db.ExecuteNonQuery(CommandType.Text, sSQL)

        Return NuevoCorrelativo
    End Function
    'Public Function ObtenerCorrelativoQuedan(ByVal NombreCorrelativo As String) As Integer
    '    Dim sql As String = "select UltimoValor from adm_correlativos where Correlativo='" & NombreCorrelativo & "'"
    '    Dim NuevoCorrelativo As Integer = SiEsNulo(db.ExecuteScalar(CommandType.Text, sql), 0) + 1

    '    'If NuevoCorrelativo = 1 Then 'Crea un nuevo registro
    '    '    sql = "insert adm_correlativos (correlativo, UltimoValor) values ('" & NombreCorrelativo & "',1)"
    '    'End If
    '    'db.ExecuteNonQuery(CommandType.Text, sql)

    '    Return NuevoCorrelativo
    'End Function
    Public Function CorrelativosInv(ByVal NombreCorrelativo As String) As Integer

        Dim sSQL As String = "select isnull(UltimoValor,1) from adm_correlativos where Correlativo='" & NombreCorrelativo & "'"
        Dim sSQL2 As String = "select correlativo from adm_correlativos where Correlativo='" & NombreCorrelativo & "'"
        Dim NuevoCorrelativo As Integer = SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), 0) + 1
        Dim Existe As String = SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL2), "")

        If Existe = "" Or Existe = Nothing Then 'NuevoCorrelativo = 1 Then 'Crea un nuevo registro
            sSQL = "insert adm_correlativos (correlativo, UltimoValor) values ('" & NombreCorrelativo & "',1)"
        End If
        Dim sSQLActu = "update adm_correlativos set UltimoValor = " & NuevoCorrelativo
        sSQLActu += " where correlativo='" & NombreCorrelativo & "'"

        db.ExecuteNonQuery(CommandType.Text, sSQL)
        db.ExecuteNonQuery(CommandType.Text, sSQLActu)

        Return NuevoCorrelativo
    End Function
    Public Function ObtenerFormularioUnico(ByVal IdSucursal As Integer) As Integer
        'METER A TRANSACCION
        Dim sSQL As String = "select IsNull(max(NumFormularioUnico),0) from fac_ventas where IdSucursal= " & IdSucursal
        Dim iMax As Integer = db.ExecuteScalar(CommandType.Text, sSQL)
        Return iMax
    End Function
    Public Function SiEsNulo(ByVal value As Object, ByVal ValueNoNull As Object) As Object
        If value Is Nothing Then Return Nothing
        If IsDBNull(value) Then Return ValueNoNull
        Return value
    End Function
End Class
