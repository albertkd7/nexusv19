﻿Imports System.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports System.Data.Common
Imports System.Data


Public Class CombosDLL
    Dim db As Database
    Dim fd As FuncionesDLL

    Public Sub New(ByVal strConexion As String)

        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db
        fd = New FuncionesDLL(strConexion)
    End Sub

    Public Sub CargaCombo(ByRef Combo As Object _
                       , ByVal Tabla As String _
                      , ByVal ValueMember As String _
                      , ByVal DisplayMember As String _
                      , ByVal sCondic As String _
                      , Optional ByVal sTipoSelect As String = "")

        Dim sql As String = ""
        If sCondic <> "" Then
            sCondic = " where " & sCondic   'no incluir el and en la condicion en la primer condicion del where
        End If
        If sTipoSelect <> "" Then
            sql = "select -1 Id, '" & sTipoSelect & "' " & DisplayMember & " union all "

            If Tabla = "adm_Departamentos" Or Tabla = "adm_Municipios" Then
                sql = "select convert(varchar(3),-1) Id, '" & sTipoSelect & "' " & DisplayMember & " union all "
            End If

        End If
        sql &= String.Format("select {0} Id,{1} from {2} {3} order by Id", ValueMember, DisplayMember, Tabla, sCondic)
        Dim dl As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)

        With Combo.Properties
            .DisplayMember = "Nombre"
            .ValueMember = "Id"
            .datasource = dl
        End With
        Try
            Combo.ItemIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Public Sub CargaComboCuenta(ByRef Combo As Object _
                   , ByVal Tabla As String _
                  , ByVal ValueMember As String _
                  , ByVal DisplayMember As String _
                  , ByVal sCondic As String _
                  , Optional ByVal sTipoSelect As String = "")

        Dim sql As String = ""
        If sCondic <> "" Then
            sCondic = " where " & sCondic   'no incluir el and en la condicion en la primer condicion del where
        End If
        If sTipoSelect <> "" Then
            sql = "select -1 Id, '" & sTipoSelect & "' as Nombre  union all "
        End If
        sql &= String.Format("select {0} Id,{1} from {2} {3} order by Id", ValueMember, DisplayMember, Tabla, sCondic)
        Dim dl As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)

        With Combo.Properties
            .DisplayMember = "Nombre"
            .ValueMember = "Id"
            .datasource = dl
        End With
        Try
            Combo.ItemIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Public Sub CargaComboSucursales(ByRef Combo As Object _
                  , ByVal sCondic As String _
                  , Optional ByVal sTipoSelect As String = "")


        Dim sSQLUsuario As String = "Select TodasLasSucursales from adm_usuarios where " & sCondic
        Dim TodasLasSucursales As Boolean = fd.SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQLUsuario), False)
        Dim sql As String = ""
        If sCondic <> "" Then
            sCondic = " where " & sCondic   'no incluir el and en la condicion en la primer condicion del where
        End If
        If sTipoSelect <> "" Then
            If TodasLasSucursales Then
                sql = "select -1 Id, '" & sTipoSelect & "' Nombre union all "
            End If
        End If
        sql &= "select Id=s.IdSucursal,Nombre=s.Nombre from adm_usuariossucursales d inner join adm_sucursales s on d.idsucursal= s.idsucursal"
        sql &= sCondic
        'sql &= " order by s.IdSucursal"
        Dim dl As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)

        With Combo.Properties
            .DisplayMember = "Nombre"
            .ValueMember = "Id"
            .datasource = dl
        End With
        Try
            Combo.ItemIndex = 0
        Catch ex As Exception

        End Try

    End Sub

    Public Sub CargaLookUpEdit(ByRef combo As Object, ByVal Tabla As String, ByVal ValueMember As String _
                      , ByVal DisplayMember As String, ByVal sCondic As String _
                      , Optional ByVal sTipoSelect As String = "", Optional ByVal TipoDato As String = "S")

        Dim sql As String = ""
        If sCondic <> "" Then
            sCondic = " where " & sCondic   'no incluir el and en la condicion en la primer condicion del where
        End If
        If sTipoSelect <> "" And TipoDato = "S" Then
            sql = "select top 1 '-1' " & ValueMember & ", '" & sTipoSelect & "' " & DisplayMember & " from " & Tabla & " union all "
        End If
        If sTipoSelect <> "" And TipoDato = "I" Then
            sql = "SELECT TOP 1 -1 " & ValueMember & ", '" & sTipoSelect & "' " & DisplayMember & " from " & Tabla & " union all "
        End If
        sql &= String.Format("select {0},{1} from {2} {3} order by {0}", ValueMember, DisplayMember, Tabla, sCondic)
        Dim dl As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)

        With combo.Properties
            .DisplayMember = "Nombre"
            .ValueMember = ValueMember
            .datasource = dl
        End With
        Try
            combo.ItemIndex = 0
        Catch ex As Exception

        End Try

    End Sub
    Public Sub con_CargarCentrosCostoCuentaMayor(ByRef LookUpEdit As Object, ByVal IdCuenta As String)
        Dim sql As String = ""
        sql = "Select IdCentro, Nombre from con_CentrosCosto"
        'If IdCuenta = "" Then
        '    sql &= "Select IdCentro, Nombre from con_CentrosCosto"
        'Else
        '    sql &= "Select cc.IdCentro, cc.Nombre from con_CentrosCosto cc inner join con_CentrosMayor cm on cc.IdCentro = cm.IdCentro and '"
        '    sql &= IdCuenta & "' like  '%'+cm.IdCuentaMayor+'%'"
        'End If
        Dim dl As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)

        With LookUpEdit.Properties
            .DisplayMember = "Nombre"
            .ValueMember = "IdCentro"
            .datasource = dl
        End With
    End Sub
    Public Sub CargaComboCuentaGasto(ByRef Combo As Object, ByVal ValueMember As String _
        , ByVal DisplayMember As String, ByVal sTipoSelect As String)
        Dim sql As String = ""
        If sTipoSelect <> "" Then
            sql = "select '' Id, '" & sTipoSelect & "' " & DisplayMember & " union all "
        End If
        sql &= "select Id=acf.IdCuentaGasto, cue.Nombre from acf_Activos acf inner join con_cuentas cue on acf.IdCuentaGasto=cue.IdCuenta Group by acf.IdCuentaGasto,cue.Nombre "

        Dim dl As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        With Combo.Properties
            .DisplayMember = "Nombre"
            .ValueMember = "Id"
            .datasource = dl
        End With
        Try
            Combo.ItemIndex = 0
        Catch ex As Exception

        End Try
    End Sub
End Class
