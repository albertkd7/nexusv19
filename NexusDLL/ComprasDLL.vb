﻿Imports System.Configuration
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports System.Data.Common
Imports System.Data
Imports NexusELL.TableEntities
Public Class ComprasDLL
    Dim db As Database
    Dim fd As FuncionesDLL

    Dim objTablas As TableData
    Public Sub New(ByVal strConexion As String)

        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db
        fd = New FuncionesDLL(strConexion)
        objTablas = New TableData(strConexion)
    End Sub
    Public Function com_ObtieneCreaLiquidacionEstructura() As DataTable
        Dim sSQL As String = "SELECT Serie, Numero ,Resolucion, NumControlInterno, ClaseDocumento, Fecha, FechaContable, IdSucursal, IdProveedor, Nombre, Nrc, Nit, Direccion, TotalIva "
        sSQL += " , TotalComprobante, Comision, IdCuentaBancaria, TotalExento,NumeroCF,SerieCF,ResolucionCF, NumControlInternoCF, ClaseDocumentoCF FROM com_Liquidacion WHERE IdComprobante=-1"
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function com_ObtenerListaProveedores() As DataTable
        Dim sql As String = "select IdProveedor from com_Proveedores order by FechaHoraCreacion"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function com_ObtenerEstructuraIdCompra() As DataTable
        Dim sql As String = "select IdComprobante from com_compras  where IdComprobante = 999999999"

        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function com_CompraConAbono(ByVal IdComprobante As Integer, ByVal Tipo As Integer) As Integer
        Dim sSQL As String = "Select Cuantos=isnull(count(*),0) from cpp_AbonosDetalle where MontoAbonado >0.0 and  IdComprobanteCompra =" & IdComprobante
        sSQL += " and TipoComprobanteCompra= " & Tipo

        Return fd.SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), 0)
    End Function

    Public Function com_ValidaContabilizacion(ByVal IdSucursal As Integer, ByVal IdModulo As Integer, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdCaja As Integer) As DataTable
        Dim sql As String = "select desdefecha from adm_periodoscontabilizados where DesdeFecha between '" & Format(Desde, "yyyyMMdd") & "'"
        sql += " and '" & Format(Hasta, "yyyyMMdd") & "'"
        sql += " and IdModulo=" & IdModulo & " and IdCaja=" & IdCaja & " and IdSucursal= " & IdSucursal
        sql += " union all "
        sql += "select desdefecha from adm_periodoscontabilizados where HastaFecha between '" & Format(Desde, "yyyyMMdd") & "'"
        sql += " and '" & Format(Hasta, "yyyyMMdd") & "'"
        sql += " and IdModulo=" & IdModulo & " and IdCaja=" & IdCaja & " and IdSucursal= " & IdSucursal

        Return fd.SiEsNulo(db.ExecuteDataSet(CommandType.Text, sql).Tables(0), 0)
    End Function
    Public Function com_ObtenerDatosF930(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("com_spGenerarF930", Desde, Hasta, IdSucursal).Tables(0)
    End Function
    Public Function com_EliminaPartidas(ByVal IdModulo As Integer, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipoPartida As String, ByVal IdSucursal As Integer, ByVal IdCaja As Integer, ByVal Idpartidas As String) As Integer
        If Idpartidas = "" Then
            Dim sql As String
            If IdModulo = 22 Then
                sql = "DELETE ban_Transacciones WHERE IdPartida IN (Select IdPartida from  con_partidas where Fecha between '" & Format(Desde, "yyyyMMdd") & "'"
                sql += "  and '" & Format(Hasta, "yyyyMMdd") & "'"
                sql += "  and IdTipo= '" & IdTipoPartida & "'"
                sql += "  and IdModuloOrigen= " & IdModulo
                sql += "  and IdSucursal =" & IdSucursal & " )"
                db.ExecuteNonQuery(CommandType.Text, sql)
            End If
            sql = "delete con_partidas where Fecha between '" & Format(Desde, "yyyyMMdd") & "'"
            sql += "  and '" & Format(Hasta, "yyyyMMdd") & "'"
            sql += "  and IdTipo= '" & IdTipoPartida & "'"
            sql += "  and IdModuloOrigen= " & IdModulo
            sql += "  and IdSucursal =" & IdSucursal
            sql += "  ; delete adm_periodoscontabilizados where desdefecha = '" & Format(Desde, "yyyyMMdd") & "'"
            sql += "  and hastafecha = '" & Format(Hasta, "yyyyMMdd") & "'"
            sql += "  and IdTipoPartida= '" & IdTipoPartida & "'"
            sql += "  and IdModulo= " & IdModulo
            sql += "  and IdSucursal =" & IdSucursal
            Return db.ExecuteNonQuery(CommandType.Text, sql)
        Else
            Dim sql As String = "delete con_partidas where Fecha between '" & Format(Desde, "yyyyMMdd") & "'"
            sql += "  and '" & Format(Hasta, "yyyyMMdd") & "'"
            sql += "  and IdTipo= '" & IdTipoPartida & "'"
            sql += "  and IdModuloOrigen= " & IdModulo
            sql += "  and IdSucursal =" & IdSucursal & " and IdPartida in (" & Idpartidas & ")"
            sql += "  ; delete adm_periodoscontabilizados where desdefecha = '" & Format(Desde, "yyyyMMdd") & "'"
            sql += "  and hastafecha = '" & Format(Hasta, "yyyyMMdd") & "'"
            sql += "  and IdTipoPartida= '" & IdTipoPartida & "'"
            sql += "  and IdModulo= " & IdModulo
            sql += "  and IdSucursal =" & IdSucursal & " and IdCaja=" & IdCaja
            Return db.ExecuteNonQuery(CommandType.Text, sql)
        End If
    End Function
    Public Function com_ContabilizarComprasCajaChica(ByVal Desde As Date, ByVal Hasta As Date, ByVal TipoPart As String, ByVal IdSucursal As Integer, ByVal CreadoPor As String, ByVal IdCaja As Integer) As String
        Return db.ExecuteScalar("com_spContabilizarComprasCajaChica", Desde, Hasta, TipoPart, IdSucursal, CreadoPor, IdCaja)
    End Function
    Public Function com_LiquidaCajaChica(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdCaja As Integer, ByVal esFHC As Boolean) As DataTable

        Hasta = DateAdd(DateInterval.Day, 1, Hasta)
        Dim sSQL = "Select IdComprobante, Numero, Serie, Fecha, Nombre, TotalComprobante, Liquidar=convert(bit,1) from com_compras "
        If esFHC Then
            sSQL += " where fechahoracreacion > dateadd(day, -1,'" & Format(Desde, "yyyyMMdd") & "')"
            sSQL += " and fechahoracreacion < dateadd(day, 1,'" & Format(Hasta, "yyyyMMdd") & "')"
        Else
            sSQL += " where fecha >='" & Format(Desde, "yyyyMMdd") & "'"
            sSQL += " and fecha <'" & Format(Hasta, "yyyyMMdd") & "'"
        End If
        sSQL += " and IdCaja > 1 and IdCaja = " & IdCaja
        sSQL += " and IdCheque=0 order by Fecha"

        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function com_ConsultaCompras(ByVal Usuario As String) As DataSet
        Dim ds As New DataSet
        Dim sql As String

        sql = "SELECT c.IdComprobante, c.Serie, c.Numero, c.Giro, c.Fecha, c.IdSucursal, c.Nombre AS Proveedor, AplicadaInventario, "
        sql &= "c.CreadoPor, c.FechaHoraCreacion,c.TotalComprobante "
        sql &= "FROM com_Compras c inner join adm_UsuariosSucursales s on c.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"
        sql &= " order by c.idcomprobante desc ; select d.* from com_ComprasDetalle d inner join com_compras c on d.idcomprobante = c.idcomprobante inner join adm_UsuariosSucursales s on c.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("Compras_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function com_ValidaContabilizacion(ByVal IdModulo As Integer, ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Dim sql As String = "select desdefecha from adm_periodoscontabilizados where DesdeFecha between '" & Format(Desde, "yyyyMMdd") & "'"
        sql += " and '" & Format(Hasta, "yyyyMMdd") & "'"
        sql += " and IdModulo=" & IdModulo
        sql += " union all "
        sql += "select desdefecha from adm_periodoscontabilizados where HastaFecha between '" & Format(Desde, "yyyyMMdd") & "'"
        sql += " and '" & Format(Hasta, "yyyyMMdd") & "'"
        sql += " and IdModulo=" & IdModulo

        Return fd.SiEsNulo(db.ExecuteDataSet(CommandType.Text, sql).Tables(0), 0)
    End Function
    Public Function com_ObtenerPeriodoContabilizado(ByVal IdModulo As Integer) As DataTable
        Dim sql As String = "select * from adm_PeriodosContabilizados where IdModulo = " & IdModulo
        sql += " order by DesdeFecha"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function com_ConsultaImportaciones(ByVal Usuario As String) As DataSet
        Dim ds As New DataSet
        Dim sql As String

        sql = "select c.IdComprobante, c.NumeroPoliza, c.Fecha, c.FechaContable, c.IdSucursal, c.TotalPoliza, c.TotalPorPagar, c.AplicadaInventario, "
        sql &= " c.CreadoPor, c.FechaHoraCreacion from com_importaciones c inner join adm_UsuariosSucursales s on c.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"
        sql &= " order by c.idcomprobante desc ; select d.* from com_ImportacionesDetalle d inner join com_importaciones c on d.idcomprobante = c.idcomprobante inner join adm_UsuariosSucursales s on c.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("Importaciones_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function com_ConsultaDocsLiquidacion(ByVal Usuario As String) As DataTable
        Dim dt As New DataTable
        Dim sql As String

        sql = "Select c.IdComprobante, c.Numero, c.FechaContable, c.Fecha, c.Nombre, c.IdSucursal, c.Nrc, c.Nit , c.TotalIva, c.TotalComprobante, TotalComision=c.Comision, c.CreadoPor, c.FechaHoraCreacion from com_Liquidacion c"
        sql += " inner join adm_UsuariosSucursales s on c.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"
        sql &= " order by c.idcomprobante desc"


        dt = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        Return dt
    End Function

    Public Function com_ConsultaOrdenesCompra(ByVal Usuario As String) As DataSet



        Dim ds As New DataSet
        Dim sql As String = "SELECT o.IdComprobante, o.Numero, o.Fecha, o.Nombre AS Proveedor, o.IdSucursal, "
        sql &= "o.Iva, o.Total, o.CreadoPor, o.FechaHoraCreacion "
        sql &= " from com_ordenesCompra o inner join adm_UsuariosSucursales s on o.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"
        sql &= " order by o.idcomprobante desc; select d.* from com_OrdenesCompraDetalle d inner join com_ordenesCompra o on d.idcomprobante = o.idcomprobante inner join adm_UsuariosSucursales s on o.idsucursal= s.idsucursal "
        sql &= " where s.idusuario='" & Usuario & "'"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("ocRelation" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function com_ConsultaRequisiciones(ByVal Usuario As String) As DataSet


        Dim ds As New DataSet
        Dim sql As String = "SELECT r.IdComprobante, r.Numero, r.Concepto, r.Fecha,r.IdSucursal, r.CreadoPor, "
        sql &= "r.FechaHoraCreacion "
        sql &= "from com_Requisiciones r inner join adm_UsuariosSucursales s on r.idsucursal= s.idsucursal"
        sql &= " where s.idusuario='" & Usuario & "'"
        sql &= " order by r.idcomprobante desc; select d.* from com_RequisicionesDetalle d inner join com_Requisiciones r on d.idcomprobante = r.idcomprobante inner join adm_UsuariosSucursales s on r.idsucursal= s.idsucursal "
        sql &= " where s.idusuario='" & Usuario & "'"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("ocRelation" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function com_ConsultaRetenciones(ByVal Usuario As String) As DataSet
        Dim ds As New DataSet
        Dim sSQL As String


        sSQL = "SELECT r.IdComprobante, r.Numero, r.Fecha, r.Nombre, r.IdSucursal, r.TotalComprobante,  r.CreadoPor, r.FechaHoraCreacion from com_Retenciones r inner join adm_UsuariosSucursales s on r.idsucursal= s.idsucursal"
        sSQL &= " where s.idusuario='" & Usuario & "'"
        sSQL &= " order by r.Fecha desc ; select d.* from com_RetencionesDetalle d inner join com_Retenciones r on d.idcomprobante = r.idcomprobante inner join adm_UsuariosSucursales s on r.idsucursal= r.idsucursal"
        sSQL &= " where s.idusuario='" & Usuario & "'"

        ds = db.ExecuteDataSet(CommandType.Text, sSQL)

        Dim dr As New DataRelation("Notas_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function com_ObtenerIdRetencion(ByVal IdComprobante As Integer, ByVal TipoAvance As Integer) As Integer
        Dim sql As String = "select top 1 IdComprobante from com_retenciones where IdComprobante < " & IdComprobante & " order by IdComprobante desc"
        If TipoAvance = 1 Then  'para obtener el registro anterior
            sql = "select top 1 IdComprobante from com_Retenciones where IdComprobante > " & IdComprobante
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function com_ObtenerRetencion(ByVal IdComprobante As Integer) As DataTable
        Return db.ExecuteDataSet("com_spObtenerRetencion", IdComprobante).Tables(0)
    End Function
    Public Function com_ConsultaOrdenCompra(ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Return db.ExecuteDataSet("com_spConsultaOrdenCompra", Desde, Hasta).Tables(0)
    End Function
    Public Function com_AprobarOrdenCompra(ByVal IdDoc As Integer, ByVal AprobarReprobar As Integer, ByVal Comentario As String, ByVal AprobadoPor As String) As String
        Dim sSQL As String = ""

        If AprobarReprobar = 1 Then
            sSQL = "update com_OrdenesCompra set Aprobada =1,"
            sSQL += " AprobadaPor='" & AprobadoPor + "'"
            sSQL += " where IdComprobante = " & IdDoc
        Else
            sSQL = " update com_OrdenesCompra set Aprobada =0, "
            sSQL += " ComentarioAprobacion=ComentarioAprobacion + ' " & Comentario + "'"
            sSQL += " where IdComprobante = " & IdDoc
        End If

        Return db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Function
    Public Function GetRetencionesDetalle(ByVal IdComprobante As Integer) As DataTable
        Dim sql As String = "select * from com_RetencionesDetalle where IdComprobante=" & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function ConteoOrdenesCompra(ByVal IdProveedor As String) As Integer
        Dim sql As String = "select count(*) from com_OrdenesCompra where IdProveedor='" & IdProveedor & "'"
        Return Convert.ToInt32(db.ExecuteScalar(CommandType.Text, sql))
    End Function
    Public Function com_InsertaLiquidacion(ByRef LiquidacionHeader As com_Liquidacion) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            LiquidacionHeader.IdComprobante = fd.ObtenerUltimoId("COM_LIQUIDACION", "IdComprobante") + 1
            objTablas.com_LiquidacionInsert(LiquidacionHeader, tran)
        Catch ex As Exception
            tran.Rollback()
            cn.Close()
            Return ex.Message
        End Try
        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function InsertRetencion(ByRef RetencionHeader As com_Retenciones, ByRef RetencionDetalle As List(Of com_RetencionesDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj = ""
        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            RetencionHeader.IdComprobante = fd.ObtenerUltimoId("COM_RETENCIONES", "IdComprobante") + 1

            objTablas.com_RetencionesInsert(RetencionHeader, tran)

            For Each detalle As com_RetencionesDetalle In RetencionDetalle
                detalle.IdComprobante = RetencionHeader.IdComprobante
                objTablas.com_RetencionesDetalleInsert(detalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj
    End Function
    Public Function UpdateRetencion(ByRef RetencionHeader As com_Retenciones, ByRef RetencionDetalle As List(Of com_RetencionesDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj = ""
        Try
            objTablas.com_RetencionesUpdate(RetencionHeader, tran)
            Dim sql As String = "delete com_RetencionesDetalle where IdComprobante=" & RetencionHeader.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sql)
            For Each detalle As com_RetencionesDetalle In RetencionDetalle
                objTablas.com_RetencionesDetalleInsert(detalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try
        Return msj
    End Function
    Public Function com_ActualizaLiquidacion(ByRef LiquidacionHeader As com_Liquidacion) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            objTablas.com_LiquidacionUpdate(LiquidacionHeader, tran)

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""

    End Function
    Public Function com_ObtenerIdRequisicion(ByVal Id As Integer, ByVal TipoAvance As Integer, ByVal Tabla As String) As Integer
        Dim sql As String = "select isnull(min(IdComprobante),0) from " & Tabla & " where IdComprobante > " & Id
        If TipoAvance = -1 Then  'para obtener el registro anterior
            sql = "select isnull(max(IdComprobante),0) from " & Tabla & " where IdComprobante < " & Id
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function com_InsertaOrdenCompra _
        (ByRef Header As com_OrdenesCompra, ByRef Detalle As List(Of com_OrdenesCompraDetalle), ByRef DetallePago As List(Of com_OrdenesCompraDetallePago)) As Boolean

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            Header.IdComprobante = fd.ObtenerUltimoId("COM_ORDENESCOMPRA", "IdComprobante") + 1

            If Header.Numero = "" Then ' solo si no le colocan el correlativo, lo generara automaticamente
                Dim UltimoNumero As Integer = 0
                UltimoNumero = fd.CorrelativosInv("ORDEN_COMPRA")
                Header.Numero = UltimoNumero.ToString.PadLeft(6, "0")
            End If

            objTablas.com_OrdenesCompraInsert(Header, tran)

            For Each det As com_OrdenesCompraDetalle In Detalle
                det.IdComprobante = Header.IdComprobante
                objTablas.com_OrdenesCompraDetalleInsert(det, tran)
            Next

            For Each det As com_OrdenesCompraDetallePago In DetallePago
                det.IdComprobante = Header.IdComprobante
                objTablas.com_OrdenesCompraDetallePagoInsert(det, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return False
        End Try

        tran.Commit()
        cn.Close()
        Return True
    End Function
    Public Function com_ActualizaOrdenCompra(ByRef Header As com_OrdenesCompra, ByRef Detalle As List(Of com_OrdenesCompraDetalle), ByRef DetallePago As List(Of com_OrdenesCompraDetallePago)) As Boolean
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try

            objTablas.com_OrdenesCompraUpdate(Header, tran)

            Dim sSQL As String = "delete com_OrdenesCompraDetalle where IdComprobante=" & Header.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)

            Dim sSQL2 As String = "delete com_OrdenesCompraDetallePago where IdComprobante=" & Header.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL2)

            For Each det As com_OrdenesCompraDetalle In Detalle
                det.IdComprobante = Header.IdComprobante
                objTablas.com_OrdenesCompraDetalleInsert(det, tran)
            Next
            For Each det As com_OrdenesCompraDetallePago In DetallePago
                det.IdComprobante = Header.IdComprobante
                objTablas.com_OrdenesCompraDetallePagoInsert(det, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return False
        End Try

        tran.Commit()
        cn.Close()
        Return True

    End Function
    Public Function com_ObtenerOrdenCompraDetalle(ByVal IdOrden As Integer, Optional NumOrden As String = "{N/A}") As DataTable
        Dim sql As String = " " _
            & " SELECT d.IdComprobante, d.IdDetalle, d.IdProducto, Cantidad = (d.Cantidad - isnull(z.Recibido, 0.0000)), d.Descripcion, d.PrecioReferencia, d.PorcDescto, d.PrecioUnitario, d.ValorDescto, d.ValorExento, d.ValorAfecto, d.CreadoPor, d.FechaHoraCreacion, " _
            & " IdCuenta=p.IdCuentaInv, IdCentro=isnull(p.IdCentro,''),Referencia=convert(varchar(50),''), ValorDai=convert(decimal(18,2),0.0), PrecioTotal=PrecioUnitario " _
            & " INTO #queryOC " _
            & " FROM com_OrdenesCompraDetalle d " _
            & " INNER JOIN inv_productos p on d.idproducto= p.idproducto " _
            & " LEFT JOIN ( " _
             & "    SELECT a.IdProducto, Recibido = sum(a.Cantidad) " _
             & "    FROM com_ImportacionesDetalle a " _
             & "    INNER JOIN com_Importaciones b on b.IdComprobante = a.IdComprobante " _
             & "    where b.NumeroFactura = '" & NumOrden & "'" _
             & "    GROUP BY a.IdProducto " _
             & "    ) z on z.IdProducto = d.IdProducto " _
            & " WHERE d.IdComprobante = " & IdOrden _
            & " ; SELECT * FROM #queryOC WHERE Cantidad > 0; DROP TABLE #queryOC " 'Para solo mostrar solo los pendientes...
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function com_ObtenerOrdenCompraSugerida(IdProveedor As String) As DataTable
        'Dim sql As String = "select IdProducto, IdCuenta=p.IdCuentaInv from com_OrdenesCompraDetalle d inner join inv_productos p on d.idproducto= p.idproducto where d.IdComprobante=" & IdOrden
        Return db.ExecuteDataSet("com_spObtenerOrdenCompraSugerida", IdProveedor).Tables(0)
    End Function
    Public Function com_ObtenerOrdenCompraFormaPago(ByVal IdOrden As Integer) As DataTable
        Dim sql As String = "select * from com_OrdenesCompraDetallePago where IdComprobante=" & IdOrden
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Function com_ObtenerOrdenCompra(ByVal IdOrden As Integer, ByVal Tipo As Integer) As DataTable
        Return db.ExecuteDataSet("com_spOrdenCompra", IdOrden, Tipo).Tables(0)
    End Function

    Public Function com_ObtenerGastosImportacion(ByVal IdImportacion As Integer) As DataTable
        Return db.ExecuteDataSet("com_spGastosImportacion", IdImportacion).Tables(0)
    End Function
    Public Function com_ObtenerImpuestos(ByVal IdCompra As Integer, ByVal TotalAfecto As Decimal) As DataTable
        Return db.ExecuteDataSet("com_spImpuestos", IdCompra, TotalAfecto).Tables(0)
    End Function

    Public Function com_ObtenerGastosImportacionProd(ByVal IdImportacion As Integer, ByVal Referencia As String, ByVal IdProducto As String) As DataTable
        Return db.ExecuteDataSet("com_spGastosImportacionProductos", IdImportacion, Referencia, IdProducto).Tables(0)
    End Function

    Public Function com_ObtenerGastosImportacionProdEstructura() As DataTable
        Dim sSQL = "Select IdImportacion =convert(int,0), IdGasto,Referencia= convert(varchar(50),''), IdProducto=convert(varchar(25),''), Descripcion=convert(varchar(90),''), AplicaGasto=convert(BIT,0)"
        sSQL += " from com_GastosImportaciones where idgasto=-999"
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function

    Public Function com_ObtenerProveedoresImportacion(ByVal IdImportacion As Integer) As DataTable
        Dim sql As String = "Select * from com_ImportacionesProveedores where IdImportacion = " & IdImportacion
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Function com_InsertaRequisiciones(ByRef RequisicionHeader As com_Requisiciones, ByRef RequisicionDetalle As List(Of com_RequisicionesDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            RequisicionHeader.IdComprobante = fd.ObtenerUltimoId("COM_REQUISICIONES", "IdComprobante") + 1

            objTablas.com_RequisicionesInsert(RequisicionHeader, tran)

            For Each detalle As com_RequisicionesDetalle In RequisicionDetalle
                detalle.IdComprobante = RequisicionHeader.IdComprobante
                objTablas.com_RequisicionesDetalleInsert(detalle, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            cn.Close()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function com_GuardaCompra(ByRef CompraHeader As com_Compras _
    , ByRef CompraDetalle As List(Of com_ComprasDetalle), ByRef DetalleImpuestos As List(Of com_ComprasDetalleImpuestos), EsNuevo As Boolean) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = "Ok"
        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            If EsNuevo Then
                CompraHeader.IdComprobante = fd.ObtenerUltimoId("COM_COMPRAS", "IdComprobante") + 1
                objTablas.com_ComprasInsert(CompraHeader, tran)
            Else

                objTablas.com_ComprasUpdate(CompraHeader, tran)

                Dim sql As String = "delete com_ComprasDetalle where IdComprobante=" & CompraHeader.IdComprobante
                sql &= "; delete com_ComprasDetalleImpuestos where IdCompra=" & CompraHeader.IdComprobante

                db.ExecuteNonQuery(tran, CommandType.Text, sql)
            End If

            For Each detalle As com_ComprasDetalle In CompraDetalle
                detalle.IdComprobante = CompraHeader.IdComprobante
                objTablas.com_ComprasDetalleInsert(detalle, tran)
            Next
            For Each detalle As com_ComprasDetalleImpuestos In DetalleImpuestos
                detalle.IdCompra = CompraHeader.IdComprobante
                objTablas.com_ComprasDetalleImpuestosInsert(detalle, tran)
            Next

            If CompraHeader.CompraExcluido And EsNuevo Then
                Dim sql As String = "update adm_correlativos set ultimovalor = ultimovalor+1"
                sql += " where correlativo ='SUJETO_EXCLUIDO' "
                db.ExecuteNonQuery(tran, CommandType.Text, sql)
            End If
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message().ToString
        Finally
            cn.Close()
        End Try

        Return msj
    End Function
    
    Public Function com_MarcarRequisiciones(ByRef Detalle As List(Of com_RequisicionesDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            For Each det As com_RequisicionesDetalle In Detalle
                ' Tablas.inv_SalidasDetalleInsert(det, tran)
                Dim sSQL As String



                sSQL = "UPDATE com_RequisicionesDetalle SET CantidadAutorizada = " & det.CantidadAutorizada
                sSQL += ", FechaConsolido = '" & Format(det.FechaConsolido, "yyyyMMdd") & "'"
                sSQL += ", UsuarioConsolido = '" & det.UsuarioConsolido & "'"
                sSQL += ", Consolidado = '" & det.Consolidado & "'"
                sSQL += " where IdComprobante = " & det.IdComprobante
                sSQL += " and IdDetalle = " & det.IdDetalle



                db.ExecuteNonQuery(CommandType.Text, sSQL)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message()
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function

    Public Function com_ActualizaRequisiciones(ByRef RequisicionesHeader As com_Requisiciones, ByRef RequisicionesDetalle As List(Of com_RequisicionesDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            objTablas.com_RequisicionesUpdate(RequisicionesHeader, tran)

            Dim sSQL As String = "delete com_RequisicionesDetalle where IdComprobante=" & RequisicionesHeader.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)

            For Each detalle As com_RequisicionesDetalle In RequisicionesDetalle
                detalle.IdComprobante = RequisicionesHeader.IdComprobante
                objTablas.com_RequisicionesDetalleInsert(detalle, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""

    End Function

    Public Function com_ObtenerCompraDetalle(ByVal IdCompra As Integer) As DataTable
        Dim sql As String = "select IdProducto, Cantidad, TipoImpuesto, Descripcion, PrecioReferencia, PorcDescto, ValorDescto, "
        sql &= "PrecioUnitario, ValorExento, ValorAfecto, IdCuenta, IdCentro from com_ComprasDetalle where IdComprobante=" & IdCompra
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function com_ObtenerDocumentoCompra(ByVal IdCompra As Integer) As DataTable
        Try
            Return db.ExecuteDataSet("com_spObtenerCompra", IdCompra).Tables(0)
        Catch ex As Exception
            Dim sql As String = "select cc.*, FormaPago= fp.Nombre, cd.IdDetalle, cd.IdProducto, cd.Cantidad, cd.Descripcion, cd.PrecioUnitario, cd.PorcDescto, cd.ValorDescto, cd.ValorExento, cd.ValorAfecto from com_compras cc"
            sql += " inner join com_comprasDetalle cd on cc.IdComprobante = cd.IdComprobante"
            sql += " inner join fac_formaspago fp on cc.IdFormaPago = fp.IdFormaPago"
            sql += " where cc.IdComprobante = " & IdCompra
            Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        End Try
    End Function

    Public Function com_ObtenerIdOrdenCompra(ByVal Numero As String) As Integer
        Dim sql As String = "select Isnull(IdComprobante,0) from com_OrdenesCompra where Numero='" & Numero & "'"
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function com_ObtenerIdOrdenCompra(ByVal IdOrden As Integer, ByVal TipoAvance As Integer) As Integer
        Dim sql As String = "select top 1 IdComprobante from com_OrdenesCompra where IdComprobante < " & IdOrden & " order by IdComprobante desc"
        If TipoAvance = 1 Then  'para obtener el registro anterior
            sql = "select top 1 IdComprobante from com_OrdenesCompra where IdComprobante > " & IdOrden
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function com_ObtenerIdCompra(ByVal IdCompra As Integer, ByVal TipoAvance As Integer) As Integer
        Dim sSQL As String = "select top 1 IdComprobante from com_Compras where IdComprobante < " & IdCompra & " order by IdComprobante desc"
        If TipoAvance = 1 Then
            sSQL = "select top 1 IdComprobante from com_Compras where IdComprobante > " & IdCompra
        End If
        Return db.ExecuteScalar(CommandType.Text, sSQL)
    End Function
    Public Function com_ObtenerDetalleRequisicion(ByVal IdComprobante As Integer, ByVal Tabla As String) As DataTable
        Dim sql As String = "select * from " & Tabla & " where IdComprobante=" & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function com_ObtenerRequisicionesNoAutorizadas(ByVal HastaFecha As DateTime) As DataTable
        Dim sSQL As String
        sSQL = "SELECT r.IdComprobante,r.CreadoPor, rd.Descripcion,rd.Cantidad,rd.CantidadAutorizada, rd.IdProducto, r.Fecha, rd.IdDetalle , rd.Consolidado as Seleccionado  "
        sSQL += "FROM com_RequisicionesDetalle rd "
        sSQL += "INNER JOIN com_Requisiciones r ON rd.IdComprobante = r.IdComprobante "
        sSQL += "WHERE rd.Consolidado = 0 "
        sSQL += "and r.fecha <= '" & Format(HastaFecha, "yyyyMMdd") & "'"

        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function com_ObtenerImportacionDetalle(ByVal IdComprobante As Integer) As DataTable
        Dim sSQL As String = "select * from com_ImportacionesDetalle where IdComprobante=" & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function

    Public Function com_HabilitarCheckGastoImportacion() As Boolean
        Dim sSQL As String = "DECLARE @Existe INT = 0; " + Chr(13)
        sSQL += " SELECT @Existe = count(*) " + Chr(13)
        sSQL += " FROM INFORMATION_SCHEMA.COLUMNS " + Chr(13)
        sSQL += " WHERE COLUMN_NAME = 'HabilitarMarcarContabGastosImp' AND TABLE_NAME = 'adm_Parametros' " + Chr(13)
        sSQL += " IF @Existe = 0 " + Chr(13)
        sSQL += " BEGIN " + Chr(13)
        sSQL += "     ALTER TABLE adm_Parametros ADD HabilitarMarcarContabGastosImp BIT NULL " + Chr(13)
        sSQL += " END " + Chr(13)
        db.ExecuteScalar(CommandType.Text, sSQL)

        sSQL = " SELECT Habilitar = convert(bit, isnull(HabilitarMarcarContabGastosImp, 0)) FROM adm_Parametros "
        Return Convert.ToBoolean(db.ExecuteScalar(CommandType.Text, sSQL))
    End Function
    Public Function com_ObtenerProrrateoCalculado(ByVal IdComprobante As Integer) As DataSet
        Return db.ExecuteDataSetWait("com_spProrrateoImportacion", IdComprobante, 1)
    End Function
    Public Function com_ObtenerListadoImportaciones(ByVal IdSucursal As Integer, Desde As Date, Hasta As Date) As DataTable
        Return db.ExecuteDataSet("com_spListadoImportaciones", IdSucursal, Desde, Hasta).Tables(0)
    End Function

    Public Function com_GuardarImportacion(ByRef ImportacionHeader As com_Importaciones _
    , ByRef ImportacionDetalle As List(Of com_ImportacionesDetalle) _
    , ByRef ImportacionesGastosDet As List(Of com_ImportacionesGastosDetalle) _
    , ByRef ImportacionesProveedores As List(Of com_ImportacionesProveedores), ByVal ImportacionesGastosProd As List(Of com_ImportacionesGastosProductos)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = "Ok"
        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            If ImportacionHeader.IdComprobante = 0 Then
                ImportacionHeader.IdComprobante = fd.ObtenerUltimoId("COM_IMPORTACIONES", "IdComprobante", tran) + 1
                objTablas.com_ImportacionesInsert(ImportacionHeader, tran)
            Else
                Dim sql As String = "delete com_ImportacionesDetalle where IdComprobante=" & ImportacionHeader.IdComprobante
                sql &= "; delete com_ImportacionesGastosDetalle where IdImportacion=" & ImportacionHeader.IdComprobante
                sql &= "; delete com_ImportacionesProveedores where IdImportacion=" & ImportacionHeader.IdComprobante
                sql &= "; delete com_ImportacionesGastosProductos where IdImportacion=" & ImportacionHeader.IdComprobante

                db.ExecuteNonQuery(tran, CommandType.Text, sql)
                objTablas.com_ImportacionesUpdate(ImportacionHeader, tran)
            End If

            For Each detalle As com_ImportacionesDetalle In ImportacionDetalle
                detalle.IdComprobante = ImportacionHeader.IdComprobante
                objTablas.com_ImportacionesDetalleInsert(detalle, tran)
            Next

            If Not ImportacionesGastosDet Is Nothing Then
                For Each detalle2 As com_ImportacionesGastosDetalle In ImportacionesGastosDet
                    detalle2.IdImportacion = ImportacionHeader.IdComprobante
                    objTablas.com_ImportacionesGastosDetalleInsert(detalle2, tran)
                Next
            End If
            

            Dim UltIdComprobanteProv As Integer = fd.ObtenerUltimoId("COM_IMPORTACIONESPROVEEDORES", "IdComprobante", tran) + 1
            If Not ImportacionesProveedores Is Nothing Then
                For Each detalle3 As com_ImportacionesProveedores In ImportacionesProveedores
                    detalle3.IdImportacion = ImportacionHeader.IdComprobante
                    detalle3.IdComprobante = UltIdComprobanteProv
                    objTablas.com_ImportacionesProveedoresInsert(detalle3, tran)
                    UltIdComprobanteProv += 1
                Next
            End If
            
            If Not ImportacionesGastosProd Is Nothing Then
                For Each detalle4 As com_ImportacionesGastosProductos In ImportacionesGastosProd
                    detalle4.IdImportacion = ImportacionHeader.IdComprobante
                    objTablas.com_ImportacionesGastosProductosInsert(detalle4, tran)
                Next
            End If
            
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try
        Return msj
    End Function
    
    Public Function com_ObtenerIdImportacion(ByVal IdComprobante As Integer, ByVal TipoAvance As Integer) As Integer
        Dim sql As String = "select isnull(min(IdComprobante),0) from com_Importaciones where IdComprobante > " & IdComprobante
        If TipoAvance = -1 Then  'para obtener el registro anterior
            sql = "select isnull(max(IdComprobante),0) from com_Importaciones where IdComprobante < " & IdComprobante
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)

    End Function
#Region "Reportes"
    Public Function com_ComprasRetencion(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdProveedor As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("com_spRentencionCompras", dFechaI, dFechaF, IdProveedor, IdSucursal).Tables(0)
    End Function

    Public Function GetLibroCompras(ByVal iMes As Integer, ByVal iAnio As Integer, ByVal iIdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("com_spLibroCompras", iMes, iAnio, iIdSucursal).Tables(0)
    End Function
    Public Function com_ComprasPeriodoProveedor(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdProveedor As String, ByVal IdSucursal As Integer, ByVal IdMarca As Integer, ByVal IdCentro As String) As DataTable
        Return db.ExecuteDataSet("com_spComprasPeriodoProveedor", dFechaI, dFechaF, IdBodega, IdProveedor, IdSucursal, IdMarca, IdCentro).Tables(0)
    End Function
    Public Function com_ComprasExcluidos(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdProveedor As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("com_spComprasExcluidos", dFechaI, dFechaF, IdBodega, IdProveedor, IdSucursal).Tables(0)
    End Function
    Public Function com_ComprasCajaChica(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdCaja As Integer, ByVal IdSucursal As Integer, ByVal TipoConsulta As Integer) As DataTable
        Return db.ExecuteDataSet("com_spComprasCajaChica", dFechaI, dFechaF, IdCaja, IdSucursal, TipoConsulta).Tables(0)
    End Function

    Public Function com_ComprasPeriodoProducto(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdCentro As String) As DataTable
        Return db.ExecuteDataSet("com_spComprasPeriodoProducto", dFechaI, dFechaF, IdBodega, IdProducto, IdSucursal, IdCentro).Tables(0)
    End Function
    Public Function com_ComprasProductoProveedor(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdProducto As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("com_spComprasProductoProveedor", dFechaI, dFechaF, IdBodega, IdProducto, IdSucursal).Tables(0)
    End Function
    Public Function com_ComprasProveedorProducto(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdProveedor As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("com_spComprasProveedorProducto", dFechaI, dFechaF, IdBodega, IdProveedor, IdSucursal).Tables(0)
    End Function
    Public Function GetComprasTotalesProducto(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("com_spComprasTotalesProducto", dFechaI, dFechaF, IdBodega, IdSucursal).Tables(0)
    End Function
    Public Function GetComprasTotalesProveedor(ByVal dFechaI As Date, ByVal dFechaF As Date, ByVal IdBodega As Integer, ByVal IdSucursal As Integer, ByVal IdMarca As Integer) As DataTable
        Return db.ExecuteDataSet("com_spComprasTotalesProveedor", dFechaI, dFechaF, IdBodega, IdSucursal, IdMarca).Tables(0)
    End Function
#End Region
    Public Function com_ContabilizarLiquidacion(ByVal Desde As Date, ByVal Hasta As Date, ByVal Tipo As Integer, ByVal IdSucursal As Integer, ByVal CreadoPor As String) As String
        Return db.ExecuteScalar("com_spContabilizarDocsLiquidacion", Desde, Hasta, Tipo, IdSucursal, CreadoPor)
    End Function
    Public Function com_ObtenerDataCompras(ByVal FechaI As Date, ByVal FechaF As Date, ByVal IdSucursal As Integer) As DataTable
        Dim Condicion As String = " and 1=1 "
        If IdSucursal <> 1 Then
            Condicion = " and cc.IdSucursal= " & IdSucursal
        End If

        Dim sSQL As String = ""
        sSQL &= "select cc.*, "
        sSQL &= "cd.IdDetalle, cd.IdProducto, cd.Cantidad, cd.Descripcion, cd.PrecioUnitario, cd.PorcDescto, cd.ValorDescto, cd.ValorExento, cd.ValorAfecto "
        sSQL &= "from com_compras cc "
        sSQL &= "inner join com_ComprasDetalle cd on cc.IdComprobante = cd.idComprobante "
        sSQL &= "where cc.Fecha >= @FechaI and cc.Fecha < dateadd(dd,1,@FechaF) "
        sSQL &= Condicion

        Dim cmd As DbCommand = db.GetSqlStringCommand(sSQL)
        db.AddInParameter(cmd, "@FechaI", DbType.Date, FechaI)
        db.AddInParameter(cmd, "@FechaF", DbType.Date, FechaF)

        Return db.ExecuteDataSet(cmd).Tables(0)
    End Function
    Public Function com_ContabilizarCompras(ByVal Desde As Date, ByVal Hasta As Date, ByVal TipoPart As String, ByVal IdSucursal As Integer, ByVal CreadoPor As String, ByVal IdComprobante As Integer, ByVal TipoContabilizacion As Integer) As String
        Return db.ExecuteScalar("com_spContabilizarCompras", Desde, Hasta, TipoPart, IdSucursal, CreadoPor, IdComprobante, TipoContabilizacion)
    End Function
    Public Function com_EliminaPartidas(ByVal IdModulo As Integer, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipoPartida As String, ByVal IdSucursal As Integer) As Integer
        Dim sql As String = "delete con_partidas where Fecha between '" & Format(Desde, "yyyyMMdd") & "'"
        sql += "  and '" & Format(Hasta, "yyyyMMdd") & "'"
        sql += "  and IdTipo= '" & IdTipoPartida & "'"
        sql += "  and IdModuloOrigen= " & IdModulo
        sql += "  and IdSucursal =" & IdSucursal
        sql += "  ; delete adm_periodoscontabilizados where desdefecha = '" & Format(Desde, "yyyyMMdd") & "'"
        sql += "  and hastafecha = '" & Format(Hasta, "yyyyMMdd") & "'"
        sql += "  and IdTipoPartida= '" & IdTipoPartida & "'"
        sql += "  and IdModulo= " & IdModulo
        sql += "  and IdSucursal =" & IdSucursal

        Return db.ExecuteNonQuery(CommandType.Text, sql)
    End Function
End Class
