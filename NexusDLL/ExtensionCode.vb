﻿Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common

Public Class ExtensionCode

End Class

Public Module NxExtensions
    <System.Runtime.CompilerServices.Extension()>
    Public Function ExecuteDataSetWait(ByVal db As Database, ByVal StoreProcedureName As String, ParamArray parms As Object()) As DataSet
        Dim ds As DataSet = New DataSet()
        Dim cmds As DbCommand = db.GetStoredProcCommand(StoreProcedureName, parms)
        cmds.CommandTimeout = 0
        ds = db.ExecuteDataSet(cmds)
        Return ds
    End Function

    <System.Runtime.CompilerServices.Extension()>
    Public Function ExecuteScalarWait(ByVal db As Database, ByVal CmdSQL As String) As Object
        Dim cmds As DbCommand = db.GetSqlStringCommand(CmdSQL)
        cmds.CommandTimeout = 0
        Return db.ExecuteScalar(cmds)
    End Function
End Module