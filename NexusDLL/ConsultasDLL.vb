﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Public Class ConsultasDLL
    Dim db As Database

    Public Sub New(ByVal strConexion As String)

        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db
    End Sub

    Public Sub ConsultaGEstionesProgramadas(ByRef Forma As Object, ByVal IdVendedor As Integer)
        Dim sql As String = "Select IdComprobante=pro.UniqueID,Desde= pro.StartDate, Hasta= pro.enddate, Asunto= pro.subject, "
        sql &= " Descripcion= pro.description, IdCliente=pro.customfield1, Cliente = cli.nombre"
        sql &= " from Appointments pro inner join fac_Clientes cli on pro.CustomField1 = cli.IdCliente "
        sql &= " where pro.UniqueID not in (select idgestionprogramada from fac_Gestiones ) and pro.CustomField2 =" & IdVendedor

        Dim dtOden As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        Forma.cnsDatatable = dtOden
    End Sub

    Public Sub CargaCns(ByRef cns As Object, ByVal tabla As String, ByVal sCondic As String, ByVal ParamArray Parametros() As Object)

        Dim campos As String = Join(Parametros, ",")
        Dim sSQL As String

        Dim paramOrder As String = Parametros(0)
        Dim sWhere As String = "where 1=1"
        If sCondic <> "" Then sWhere &= " and " & sCondic

        sSQL = String.Format("select {0} from {1} {2} order by {3}", campos, tabla, sWhere, paramOrder)
        Dim dl As DataTable = db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)

        cns.cnsDatatable = dl

    End Sub
    Public Sub CargaDesembolsos(ByRef Forma As Object)
        Forma.cnsDatatable = db.ExecuteDataSet("coo_spDesembolsosParaContabilidad").Tables(0)
    End Sub
    Public Function ConsultaRetencion() As DataSet
        Dim ds As New DataSet
        Dim sSQL As String
        sSQL = "SELECT IdComprobante, Numero, Fecha, Nombre, CreadoPor, FechaHoraCreacion from com_Retenciones"
        sSQL &= " ; select * from com_RetencionesDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sSQL)

        Dim dr As New DataRelation("Notas_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function ConsultaProduccionesInv() As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select IdComprobante, Numero, Fecha, Left(Concepto,50) Concepto, CreadoPor, FechaHoraCreacion "
        sql &= " from inv_Producciones; select * from inv_ProduccionesDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("EntradasDetalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    'Public Function ConsultaEmpleados() As DataSet
    '    Dim ds As New DataSet

    '    Dim sql As String = "select IdEmpleado, Apellidos, Nombres, DUI=NumeroDocIdentidad, NIT, SalarioOrdinario, FechaIngreso, "
    '    sql &= " Cargo=pp.NombrePlaza, Departamento=pd.nombre"
    '    sql &= " from pla_empleados pe inner join pla_Plazas pp on pe.IdPlaza=pp.IdPlaza"
    '    sql &= " inner join pla_Departamentos pd on pd.IdDepartamento=pe.IdDepartamento"
    '    sql &= " where IdEstadoEmpleado in (Select IdEstado from pla_EstadoEmpleado where AplicaPlanilla=1)"

    '    ds = db.ExecuteDataSet(CommandType.Text, sql)
    '    Return ds
    'End Function
    Public Sub ConsultaProductos(ByRef Forma As Object, ByVal Condicion As String)
        Dim sql As String = "select IdProducto, Nombre from inv_productos where EstadoProducto = 1 " & Condicion
        Dim dtProd As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        Forma.cnsDatatable = dtProd
    End Sub
    Public Sub ConsultaEmpleados(ByRef Forma As Object)
        Dim sql As String = "select IdEmpleado,Codigo ,Apellidos, Nombres, "
        sql &= " Cargo=pp.NombrePlaza, Departamento=pd.nombre"
        sql &= " from pla_empleados pe inner join pla_Plazas pp on pe.IdPlaza=pp.IdPlaza"
        sql &= " inner join pla_Departamentos pd on pe.IdDepartamento=pd.IdDepartamento"
        sql &= " order by Apellidos, Nombres "

        Dim dtEmp As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        Forma.cnsDatatable = dtEmp
    End Sub
    Public Sub ConsultaOrdenProduccion(ByRef Forma As Object)
        Dim sql As String = "select IdComprobante,NumeroOrden, FechaOrden, FechaEntrega,CodigoProducto=IdProducto ,Producto=nombre "
        sql &= " from pro_OrdenesProduccion"
        sql &= " order by IdComprobante desc"

        Dim dtOden As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        Forma.cnsDatatable = dtOden
    End Sub
    Public Sub ConsultaGastosProduccion(ByRef Forma As Object)
        Dim sql As String = "select IdGasto,Nombre,Descripcion=nombre "
        sql &= " from inv_GastosProduccion "
        sql &= " order by Idgasto"

        Dim dtOden As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        Forma.cnsDatatable = dtOden
    End Sub
    Public Sub ConsultaAbonosCPP(ByRef Forma As Object, ByVal IdSucursal As Integer)
        Dim sql As String = "select a.IdComprobante, a.numerocomprobante NumeroRecibo, p.nombre NombreProveedor,a.fecha FechaAbono, a.TotalAbono MontoAbonado  from cpp_abonos a inner join com_proveedores p on a.idproveedor = p.idproveedor where Cancelado=0 and a.IdSucursal = " & IdSucursal
        Dim dtAbonos As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        Forma.cnsDatatable = dtAbonos
    End Sub
    Public Sub ConsultaActivoFijo(ByRef Forma As Object, ByVal IdSucursal As Integer)
        Dim Condicion As String = " where 1=1 "
        If IdSucursal <> 1 Then
            Condicion = " where IdSucursal= " & IdSucursal
        End If

        Dim sSQL As String = "select IdActivo,codigo,Nombre from acf_Activos "
        sSQL &= Condicion
        Dim dtProd As DataTable = db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
        Forma.cnsDatatable = dtProd
    End Sub
    Public Sub ConsultaPedidosClientes(ByRef Forma As Object, ByVal sCondic As String)
        Dim sSQL As String = "select p.IdComprobante, p.Numero as NumPedido, NumeroFactura=f.Numero, p.Fecha, p.IdCliente, p.Nombre from fac_pedidos p inner join fac_ventas f on p.idcomprobante = f.IdComprobanteNota"
        sSQL += " where f.IdTipoComprobante in (5,6,7,24) and f.totalcomprobante > 0.00 and f.IdCliente= '" & sCondic & "'"

        Dim dtProd As DataTable = db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
        Forma.cnsDatatable = dtProd
    End Sub

    Public Sub ConsultaTrasladosActivoFijo(ByRef Forma As Object, ByVal sCondic As String)
        Dim sSQL As String = "select IdComprobante, NumeroComprobante, Fecha, Concepto, CreadoPor from acf_Traslados "
        Dim dtProd As DataTable = db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
        Forma.cnsDatatable = dtProd
    End Sub


    Public Sub ConsultaCuentas(ByRef Forma As Object, ByVal sCondic As String)
        Dim sSQL As String = "select IdCuenta, Nombre, IdCuentaMayor from con_cuentas"
        Forma.cnsDatatable = db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Sub

#Region "ConsultasMaestrosDetalle"

    Public Function ConsultaPedidos() As DataSet
        Dim ds As New DataSet
        Dim sSQL As String
        sSQL = "SELECT fac_Pedidos.IdComprobante, fac_Pedidos.Numero, fac_Pedidos.Fecha, fac_Clientes.Nombre AS Cliente, Fac_Pedidos.CreadoPor, Fac_Pedidos.FechaHoraCreacion"
        sSQL &= " FROM fac_Pedidos INNER JOIN fac_Clientes on fac_Pedidos.IdCliente = fac_Clientes.IdCliente"
        sSQL &= " ; select * from fac_PedidosDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sSQL)

        Dim dr As New DataRelation("Pedidos_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function ConsultaRemision() As DataSet
        Dim ds As New DataSet
        Dim sSQL As String
        sSQL = "SELECT IdComprobante, Numero, Fecha, Nombre, CreadoPor, FechaHoraCreacion from fac_NotasRemision"
        sSQL &= " ; select * from fac_NotasRemisionDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sSQL)

        Dim dr As New DataRelation("Notas_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function ConsultaCotizaciones(ByVal IdVendedor As Integer) As DataSet
        Dim ds As New DataSet
        Dim sSQL As String
        sSQL = "SELECT IdComprobante, Numero, Fecha, Nombre, CreadoPor, FechaHoraCreacion"
        sSQL &= " FROM Fac_Cotizaciones"
        If IdVendedor > 0 Then
            sSQL &= " where IdVendedor=" & IdVendedor
        End If
        sSQL &= "; select d.* from fac_CotizacionesDetalle d inner join Fac_Cotizaciones c on d.idcomprobante = c.idcomprobante"
        If IdVendedor > 0 Then
            sSQL &= " where c.IdVendedor=" & IdVendedor
        End If
        ds = db.ExecuteDataSet(CommandType.Text, sSQL)

        Dim dr As New DataRelation("Cotiza_Detalle", ds.Tables(0).Columns("IdComprobante"), ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function ConsultaFormulasInv() As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select IdFormula, IdProducto, Fecha, Left(Concepto,50) Concepto, CreadoPor, FechaHoraCreacion "
        sql &= " from inv_Formulas; select * from inv_FormulasDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("EntradasDetalle" _
                                   , ds.Tables(0).Columns("IdFormula") _
                                   , ds.Tables(1).Columns("IdFormula"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function ConsultaFactura() As DataSet
        Dim ds As New DataSet
        Dim sSQL As String
        sSQL = "SELECT IdComprobante, Numero, Fecha, Nombre, TotalComprobante, CreadoPor, FechaHoraCreacion from fac_Ventas"
        sSQL &= " Order By IdComprobante DESC; SELECT * FROM fac_VentasDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sSQL)

        Dim dr As New DataRelation("Ventas_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function

    Public Function ConsultaCompras() As DataSet
        Dim ds As New DataSet
        Dim sql As String

        sql = "SELECT IdComprobante, Serie, Numero, Fecha, Nombre AS Proveedor, "
        sql &= "CreadoPor, FechaHoraCreacion "
        sql &= "FROM com_Compras "
        sql &= " ; select * from com_ComprasDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("Compras_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function ConsultaComprasQuedan(ByVal IdProveedor As String) As DataSet
        Dim ds As New DataSet
        Dim sql As String

        sql = "SELECT IdComprobante, Serie, Numero, Fecha, Nombre AS Proveedor, "
        sql &= "CreadoPor, FechaHoraCreacion "
        sql &= "FROM com_Compras where IdProveedor =  '" & IdProveedor
        sql &= "' ; select * from com_ComprasDetalle where IdComprobante in (select IdComprobante from Com_compras where IdProveedor =  '" & IdProveedor & "' )"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("Compras_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function com_ConsultaOrdenesCompra() As DataSet
        Dim ds As New DataSet
        Dim sql As String = "SELECT IdComprobante, Numero, Fecha, Nombre AS Proveedor, "
        sql &= "Iva, Total, CreadoPor, FechaHoraCreacion "
        sql &= "from com_ordenesCompra "
        sql &= "; select * from com_OrdenesCompraDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("ocRelation" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function com_ConsultaRequisiciones() As DataSet
        Dim ds As New DataSet
        Dim sql As String = "SELECT IdComprobante, Numero, Fecha, CreadoPor, "
        sql &= "FechaHoraCreacion "
        sql &= "from com_Requisiciones "
        sql &= "; select * from com_RequisicionesDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("ocRelation" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function ConsultaImportaciones() As DataSet
        Dim ds As New DataSet
        Dim sql As String
        sql = "select IdComprobante, NumeroPoliza, Fecha, FechaContable, TotalPoliza, TotalPorPagar "
        sql &= "from com_importaciones ; select * from com_ImportacionesDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("Importaciones_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function

    Public Function ConsultaQuedan() As DataSet
        Dim ds As New DataSet
        Dim sSQL As String
        sSQL = "SELECT Que.IdComprobante, Que.Numero, Que.IdProveedor, Pro.Nombre NombreProveedor, Que.Fecha, Que.FechaPago, Que.TotalPago "
        sSQL &= "FROM CPP_Quedan Que INNER JOIN Com_Proveedores Pro ON Que.IdProveedor = Pro.IdProveedor; SELECT * FROM CPP_QuedanDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sSQL)

        Dim dr As New DataRelation("Quedan_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function ConsultaCheques(ByVal Fecha As String) As DataSet

        Dim sql As String = "SELECT Che.IdCheque, Che.IdTipoPartida, Che.NumeroPartida, Ban.NumeroCuenta, Numero, Fecha, AnombreDe, Left(Concepto,100) Concepto, Valor, Che.CreadoPor, "
        sql &= "Che.FechaHoraCreacion from ban_Cheques Che inner join ban_cuentasbancarias ban on che.idcuentabancaria = ban.idcuentabancaria order by che.fecha desc"
        'sql &= String.Format(" where Fecha >= '{0}' order by Ban.NumeroCuenta, Numero desc", Fecha)
        'sql &= String.Format(" select * from ban_ChequesDetalle cd inner join ban_Cheques ch on cd.IdCheque = ch.IdCheque where Fecha >='{0}'", Fecha)
        sql &= " select * from ban_ChequesDetalle cd inner join ban_Cheques ch on cd.IdCheque = ch.IdCheque "

        Dim dsCh As DataSet = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("Cheques_Detalle" _
                                   , dsCh.Tables(0).Columns("IdCheque") _
                                   , dsCh.Tables(1).Columns("IdCheque"))
        dsCh.Relations.Add(dr)
        Return dsCh
    End Function
    Public Function ConsultaTransacciones(ByVal Fecha As String) As DataSet

        Dim sql As String = "SELECT Tra.IdTransaccion, Tra.IdTipoPartida, Tra.NumeroPartida, Ban.NumeroCuenta, Tra.Fecha, Left(Concepto,100) Concepto, Tra.Valor, Tra.CreadoPor,"
        sql &= " Tra.FechaHoraCreacion, Tra.ModificadoPor from ban_Transacciones Tra"
        sql &= " inner join Ban_CuentasBancarias Ban ON Tra.IdCuentaBancaria = Ban.IdCuentaBancaria"
        sql &= String.Format(" where Fecha >= '{0}' order by Ban.NumeroCuenta, Fecha desc", Fecha)
        sql &= String.Format(" ; select * from ban_TransaccionesDetalle td inner join Ban_Transacciones bt on td.IdTransaccion = bt.IdTransaccion where Fecha > ='{0}'", Fecha)

        Dim dsTr As DataSet = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("TransDetalle" _
                                   , dsTr.Tables(0).Columns("IdTransaccion") _
                                   , dsTr.Tables(1).Columns("IdTransaccion"))
        dsTr.Relations.Add(dr)
        Return dsTr
    End Function
    Public Function ConsultaPartidas(ByVal Fecha As String) As DataSet

        Dim sql As String = "SELECT IdPartida, IdTipo, Numero, Fecha, left(Concepto,100) Concepto, CreadoPor, FechaHoraCreacion FROM con_Partidas"
        sql &= String.Format(" Where Fecha >='{0}' Order By IdPartida DESC", Fecha)
        sql &= String.Format(" ; SELECT * FROM con_PartidasDetalle pd INNER JOIN con_Partidas cp ON pd.IdPartida = cp.IdPartida Where Fecha >= '{0}'", Fecha)

        Dim dsPa As DataSet = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("Partidas_Detalle" _
                                   , dsPa.Tables(0).Columns("IdPartida") _
                                   , dsPa.Tables(1).Columns("IdPartida"))
        dsPa.Relations.Add(dr)
        Return dsPa
    End Function

    Public Function ConsultaEntradasInv() As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select IdComprobante, Numero, Fecha, Left(Concepto,50) Concepto, CreadoPor, FechaHoraCreacion "
        sql &= " from inv_Entradas; select * from inv_EntradasDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("EntradasDetalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function ConsultaSalidasInv() As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select IdComprobante, Numero, Fecha, Left(Concepto,50) Concepto, CreadoPor, FechaHoraCreacion "
        sql &= " from inv_Salidas ; select * from inv_SalidasDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("SalidasDetalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function ConsultaTrasladosInv() As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select IdComprobante, Numero, Fecha, Left(Concepto,50) Concepto, CreadoPor, FechaHoraCreacion"
        sql &= " from inv_Traslados ; select * from inv_TrasladosDetalle"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("TrasladosDetalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function

    Public Function ConsultaEventos() As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select IdEvento, CodEvento, FechaInicio, FechaFin, Grupo, Left(DescripcionEvento,50) Concepto, CreadoPor, FechaHoraCreacion "
        sql &= " from eve_Eventos; select d.IdEvento,Seminario=Left(s.Nombre,50), d.NumHoras,d.PrecioSocio, d.PrecioNoSocio from eve_EventosDetalle d inner join eve_seminarios s on d.idseminario=s.idseminario"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("EntradasDetalle" _
                                   , ds.Tables(0).Columns("IdEvento") _
                                   , ds.Tables(1).Columns("IdEvento"))
        ds.Relations.Add(dr)
        Return ds
    End Function

    Public Function ConsultaInscripciones() As DataSet
        Dim ds As New DataSet
        Dim sql As String = ""
        sql &= "select i.IdComprobante, FechaInscripcion=i.fecha,i.IdCliente, Cliente=c.Nombre, Evento= e.descripcionevento , EmitirDocumento= t.nombre, i.CreadoPor, i.FechaHoraCreacion "
        sql &= " from eve_InsCripciones i inner join eve_eventos e on i.idevento = e.idevento  inner join adm_TiposComprobante t on i.idtipocomprobante = t.idtipocomprobante inner join fac_clientes c on i.idcliente = c.idcliente; select d.IdComprobante, d.NombreParticipantes, d.Socio, d.RequiereHoras,d.NumInscripcion, Descuento = d.PorcDescto, d.Cargo from eve_inscripcionesdetalle d inner join eve_inscripciones e on d.idcomprobante = e.idcomprobante  inner join fac_clientes c on e.idcliente = c.idcliente"

        ds = db.ExecuteDataSet(CommandType.Text, sql)

        Dim dr As New DataRelation("DetalleInscripcion" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function



#End Region
End Class
