﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common
Imports NexusELL.TableEntities
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports System.Text
Imports System.Security.Cryptography
Imports System.Web.Script.Serialization
Imports System.Configuration

Public Class DBconfig
    Dim _db As Database
    Dim NxSchemas As Boolean = IIf(ConfigurationManager.AppSettings("NxSchemas:Disabled") = "True", True, False)
    Dim ListEsquemas As New List(Of Esquemas)

    Public Property db() As Database
        Get
            Return _db
        End Get
        Set(ByVal value As Database)
            _db = value
        End Set
    End Property

    Public Sub New(ByVal ConnectionString As String)

        'Obtener el string encriptado.. segun cns01..02..
        Dim dbf As Database = Nothing

        If NxSchemas = True Then
            Dim settings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings
            If Not settings Is Nothing Then
                For Each cs As ConnectionStringSettings In settings
                    If cs.Name = "MultiEmpresa" Then
                        dbf = DatabaseFactory.CreateDatabase(cs.Name)
                    End If
                Next
            End If
        Else
            dbf = DatabaseFactory.CreateDatabase(ConnectionString)
        End If

        'Proceder a des-encriptar....
        Dim u As String = dbf.ConnectionString
        Dim o As String = Desencriptar(u)

        Dim jsonschemas = System.IO.Directory.GetCurrentDirectory() & "\Nexus.xgg"
        If System.IO.File.Exists(jsonschemas) Then
            Dim jss As New JavaScriptSerializer()
            Dim jsonencry = System.IO.File.ReadAllText(jsonschemas)
            ListEsquemas = jss.Deserialize(Of List(Of Esquemas))(Desencriptar(jsonencry))
        End If

        If NxSchemas = True And ListEsquemas.Any() Then
            Dim Esq = ListEsquemas.FirstOrDefault(Function(x) ("cns" & String.Format("{0:00}", x.IdDB)) = ConnectionString)
            Dim CadenaDB As String = IIf(o = "Data Source=n/a", u, o)

            If Not Esq Is Nothing Then
                CadenaDB = ChangeCredentials(CadenaDB, Esq.UserDB, Esq.PassDB)
            End If
            Dim dbs As SqlDatabase = New SqlDatabase(CadenaDB)
            db = dbs
        ElseIf o = "Data Source=n/a" Then
            If NxSchemas Then
                Dim dbs As SqlDatabase = New SqlDatabase(u)
                db = dbs
            Else
                db = DatabaseFactory.CreateDatabase(ConnectionString)
            End If
        Else
            'Genearar el objeto derivado con el string real, para la conexion con la DB...
            Dim dbs As SqlDatabase = New SqlDatabase(o)
            'Devolver el objeto derivado al Database Enterprise para no modificar muchas lineas de codigo ^_^
            db = dbs
        End If

    End Sub

    'Public Function Encriptar(ByVal Input As String) As String

    '    Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("NexusKey") 'La clave debe ser de 8 caracteres
    '    Dim EncryptionKey() As Byte = Convert.FromBase64String("rpaSPvIvVLlrcmtzPU9/c67Gkj7yL1S9") 'No se puede alterar la cantidad de caracteres pero si la clave
    '    Dim buffer() As Byte = Encoding.UTF8.GetBytes(Input)
    '    Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
    '    des.Key = EncryptionKey
    '    des.IV = IV

    '    Return Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length()))

    'End Function

    Public Function Desencriptar(ByVal Input As String) As String
        Dim cs As String = "Data Source=n/a"
        Try
            Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("NexusKey") 'La clave debe ser de 8 caracteres
            Dim EncryptionKey() As Byte = Convert.FromBase64String("rpaSPvIvVLlrcmtzPU9/c67Gkj7yL1S9") 'No se puede alterar la cantidad de caracteres pero si la clave
            Dim buffer() As Byte = Convert.FromBase64String(Input)
            Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
            des.Key = EncryptionKey
            des.IV = IV
            cs = Encoding.UTF8.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length()))

        Catch ex As Exception

        End Try
        Return cs
    End Function

    Public Function ChangeCredentials(ByVal StringConexion As String, ByVal _User As String, ByVal _Pass As String)
        '' - Data Source=tcp:jurosa.database.windows.net,1433;Initial Catalog=NEXUS_JURO;User ID=administrador@jurosa;Password=nexus123+
        Dim newcnx As String = String.Empty
        Dim arry1 = StringConexion.Split(";")
        newcnx &= arry1(0) & ";" & arry1(1) & ";"

        Dim arry2 = arry1(2).Split("=")
        newcnx &= arry2(0) & "=" & IIf(_User = "dbo", arry2(1), _User) & ";"

        Dim arry3 = arry1(3).Split("=")
        newcnx &= arry3(0) & "=" & IIf(_User = "dbo", arry3(1), _Pass)

        Return newcnx
    End Function
End Class


Public Class Esquemas
    Public Esquema As String
    Public Nombre As String
    Public Nrc As String
    Public UserDB As String
    Public PassDB As String
    Public IdDB As Integer
End Class