﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common
Imports NexusELL.TableEntities
Public Class CuentasPPDLL
    Dim db As Database
    Dim fd As FuncionesDLL
    Dim objTablas As TableData

    Public Sub New(ByVal strConexion As String)

        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db
        fd = New FuncionesDLL(strConexion)
        objTablas = New TableData(strConexion)
    End Sub

    Function GetQuedanDetalle(ByVal IdQuedan As Integer) As DataTable
        Dim sql As String = "select * from cpp_QuedanDetalle where IdComprobante = " & IdQuedan
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Function GetQuedanDetalleStore(ByVal IdQuedan As Integer) As DataTable
        Return db.ExecuteDataSet("cpp_spGetQuedanDetalle", IdQuedan).Tables(0)
    End Function

    Public Function GetSaldoProveedor(ByVal IdProveedor As String, ByVal HastaFecha As DateTime, ByVal IdSucursal As Integer) As Decimal
        Dim Saldo As Decimal = db.ExecuteScalar("cpp_spSaldoProveedor", IdProveedor, HastaFecha, IdSucursal)
        Return Saldo
    End Function
    Public Function cpp_DocumentosAnulados(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProveedor As String, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpp_spDocumentosAnulados", Desde, Hasta, IdProveedor, IdSucursal).Tables(0)
    End Function
    Public Function GetQuedan() As DataTable
        Return db.ExecuteDataSet("cpp_spExtraeQuedan").Tables(0)
    End Function
    Public Function ExisteNumeroQuedan(ByVal Numero As String) As Integer
        Dim sql As String = String.Format("select IdComprobante from cpp_quedan where Numero= '{0}'", Numero)
        Dim i As Integer = fd.SiEsNulo(db.ExecuteScalar(CommandType.Text, sql), 0)
        Return i
    End Function
    
    Public Function cpp_EstadoCuenta(ByVal IdSucursal As Integer, ByVal Fecha As Date, ByVal DesdeProveedor As String, ByVal HastaProveedor As String) As DataTable
        Return db.ExecuteDataSet("cpp_spEstadoCuenta", IdSucursal, Fecha, DesdeProveedor, HastaProveedor).Tables(0)
    End Function
    Public Function AnulaQuedan(ByVal IdCOmprobante As Integer, ByRef UsuarioAnula As String) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            db.ExecuteScalar("cpp_spAnulaQuedan", IdCOmprobante, UsuarioAnula)
        Catch ex As Exception
            tran.Rollback()
            cn.Close()

            Return ex.Message
        End Try
        tran.Commit()
        cn.Close()
        Return ""

    End Function
    Public Function ObtenerSaldosCompras(ByVal IdProveedor As String, ByVal Fecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpp_spSaldos_Compras", IdProveedor, Fecha, IdSucursal).Tables(0)
    End Function

    Public Function GetSaldos_Notas(ByVal IdProveedor As String, ByVal Fecha As Date, ByVal IdNota As Integer) As DataTable
        Return db.ExecuteDataSet("cpp_spObtenerSaldosNota", IdProveedor, Fecha, IdNota).Tables(0)
    End Function

    Public Function cpp_ObtenerAbonosPendientes(ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpp_spObtenerAbonosPendientes", IdSucursal).Tables(0)
    End Function

    Public Function cpp_ObtenerEstruturaPendientes() As DataTable
        Dim sql As String = "select IdCuentaBancaria=IdCuentaBancaria, Monto= convert(decimal(18,2),0.00), IdSucursal = convert(int,0) from ban_CuentasBancarias where IdCuentaBancaria=-999 "
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function cpp_ConsultaAbonosProveedor(ByVal IdProveedor As String, ByVal Desde As Date, ByVal Hasta As Date) As DataSet
        Dim ds As DataSet = db.ExecuteDataSet("cpp_spConsultaAbonosProveedor", IdProveedor, Desde, Hasta)
        Dim dr As New DataRelation("AbonosProveedor", ds.Tables(0).Columns("IdComprobante"), ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    
    Function cpp_GetDetalleAbono(ByVal IdComprobante As Integer) As DataTable
        Dim sql As String = "select NumComprobanteCompra=com.Numero,FechaCompra=Com.Fecha,"
        sql += "FechaVencto=com.FechaVencimiento,TotalCompra=com.TotalComprobante,"
        sql += "SaldoCompra=com.TotalComprobante-det.MontoAbonado,MontoAbonar=det.MontoAbonado,det.IdComprobante "
        sql += " from cpp_AbonosDetalle det Inner Join com_Compras com on det.IdComprobanteCompra=com.IdComprobante where det.TipoComprobanteCompra = 1 and det.IdComprobante = " & IdComprobante
        sql += "Union all select NumComprobanteCompra=com.NumeroComprobante,FechaCompra=Com.Fecha,"
        sql += "FechaVencto=dateadd(dd, com.DiasCredito , com.Fecha ),TotalCompra=com.Valor,"
        sql += "SaldoCompra=com.Valor-det.MontoAbonado,MontoAbonar=det.MontoAbonado,det.IdComprobante "
        sql += " from cpp_AbonosDetalle det Inner Join com_ImportacionesProveedores  com on det.IdComprobanteCompra=com.IdComprobante where det.TipoComprobanteCompra = 2 and det.IdComprobante = " & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Function cpp_ObtenerQuedanPendientes(ByVal IdSucursal As Integer) As DataTable
        Dim sql = "select IdComprobante Correlativo, Numero, Fecha, NombreProveedor, NrcProveedor, FechaPago, TotalPago Total, Concepto, Cancelar=convert(bit,0) "
        sql += " from cpp_Quedan where FechaCancelacion is null  and IdSucursal =" & IdSucursal
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function cpp_CancelarQuedan(ByVal IdQuedan As Integer, ByVal Fecha As DateTime) As Integer
        Dim sql = "update cpp_Quedan set FechaCancelacion=' " & Format(Fecha, "yyyyMMdd") & "'"
        sql += " where IdComprobante=" & IdQuedan
        Return db.ExecuteNonQuery(CommandType.Text, sql)
    End Function

    Function cpp_ObtenerIdQuedan(ByVal IdQuedan As Integer, ByVal TipoAvance As Integer) As Integer
        Dim sql As String = "select isnull(min(IdComprobante),0) from cpp_Quedan where IdComprobante > " & IdQuedan
        If TipoAvance = -1 Then  'para obtener el registro anterior
            sql = "select isnull(max(IdComprobante),0) from cpp_Quedan where IdComprobante < " & IdQuedan
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)

    End Function

    Function cpp_AutorizaAbonosCheque(ByVal IdComprobante As Integer, ByVal IdCheque As Integer, ByVal Usuario As String) As Integer
        Dim sql As String = "Update cpp_Abonos set Cancelado=1, IdCheque=" & IdCheque & ", CanceladoPor='" & Usuario & "' where IdComprobante = " & IdComprobante
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function

    Function cpp_AutorizaAbonosTransaccion(ByVal IdComprobante As Integer, ByVal IdTransaccion As Integer, ByVal Usuario As String) As Integer
        Dim sql As String = "Update cpp_Abonos set Cancelado=1 ,IdTransaccion=" & IdTransaccion & ", CanceladoPor='" & Usuario & "' where IdComprobante = " & IdComprobante
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Function cpp_AutorizaAbonosSinTransaccion(ByVal IdComprobante As Integer, ByVal Usuario As String) As Integer
        Dim sql As String = "Update cpp_Abonos set Cancelado=1, IdCheque=-1, IdTransaccion=-1, IdCaja=-1, CanceladoPor='" & Usuario & "' where IdComprobante = " & IdComprobante
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Function cpp_InsertaPartida(ByRef entPartida As con_Partidas, ByRef entDetalle As List(Of con_PartidasDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            entPartida.IdPartida = fd.ObtenerUltimoId("CON_PARTIDAS", "IdPartida") + 1
            entPartida.Numero = fd.GetNumPartida(entPartida.IdTipo, entPartida.Fecha, tran)
            objTablas.con_PartidasInsert(entPartida, tran)

            For Each detalle As con_PartidasDetalle In entDetalle
                detalle.IdPartida = entPartida.IdPartida
                objTablas.con_PartidasDetalleInsert(detalle, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function


    Function cpp_ObtenerUltAbono(ByVal IdProveedor As String) As Integer
        Dim sql As String = ""
        sql = "select isnull(max(IdComprobante),0) from cpp_Abonos where IdProveedor='" & IdProveedor & "'"
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Function cpp_AnulaAbono(ByVal IdComprobante As Integer, ByVal IdCheque As Integer, ByVal IdTransaccion As Integer, ByVal AnuladoPor As String, ByVal Eliminar As Integer) As String
        Dim sql As String = "update cpp_abonos set fechahoracancelacion=getdate(), CanceladoPor='" & AnuladoPor
        sql += "', concepto='<<< ABONO ANULADO >>> '+concepto, Cancelado=1, IdCheque=-9999, IdTransaccion=0,totalabono=0  where IdComprobante =  " & IdComprobante
        sql += "; update cpp_abonosdetalle set montoabonado=0.0, saldoactual=0.0 where idcomprobante = " & IdComprobante
        If IdCheque > 0 Then
            If Eliminar = 1 Then
                Dim entCheque As ban_Cheques = objTablas.ban_ChequesSelectByPK(IdCheque)
                sql += "; delete ban_cheques  where idcheque = " & IdCheque
                sql += "; delete con_partidas  where idpartida = " & entCheque.IdPartida
            End If
        End If
            If IdTransaccion > 0 Then
            Dim entTransacciones As ban_Transacciones = objTablas.ban_TransaccionesSelectByPK(IdTransaccion)
            sql += "; delete ban_transacciones where idtransaccion = " & IdTransaccion
            sql += "; delete con_partidas  where idpartida = " & entTransacciones.IdPartida
        End If
        Return db.ExecuteNonQuery(CommandType.Text, sql)
    End Function
    Public Function cpp_AplicarAbono(ByRef AbonoHeader As cpp_Abonos, ByRef AbonoDetalle As List(Of cpp_AbonosDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            AbonoHeader.IdComprobante = fd.ObtenerUltimoId("CPP_ABONOS", "IdComprobante") + 1
            objTablas.cpp_AbonosInsert(AbonoHeader, tran)

            For Each detalle As cpp_AbonosDetalle In AbonoDetalle
                detalle.IdComprobante = AbonoHeader.IdComprobante
                objTablas.cpp_AbonosDetalleInsert(detalle, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function cpp_AplicarTraslado(ByRef AbonoHeader As cpp_Abonos, ByRef AbonoDetalle As List(Of cpp_AbonosDetalle), ByVal TrasHeader As cpp_Traslado, ByRef TrasDetalle As List(Of cpp_TrasladoDetalle), ByRef DetalleCompra As List(Of com_Compras)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            'Registro el Traslado que se genero 
            TrasHeader.IdComprobante = fd.ObtenerUltimoId("cpp_Traslado", "IdComprobante") + 1
            TrasHeader.NumeroComprobante = TrasHeader.IdComprobante.ToString.PadLeft(6, "0")
            objTablas.cpp_TrasladoInsert(TrasHeader, tran)
            For Each detalle As cpp_TrasladoDetalle In TrasDetalle
                detalle.IdComprobante = TrasHeader.IdComprobante
                objTablas.cpp_TrasladoDetalleInsert(detalle, tran)
            Next

            'Abono para cancelar la Deuda Actual de la compra
            AbonoHeader.IdComprobante = fd.ObtenerUltimoId("CPP_ABONOS", "IdComprobante") + 1
            AbonoHeader.NumeroComprobante = AbonoHeader.IdComprobante.ToString.PadLeft(6, "0")
            objTablas.cpp_AbonosInsert(AbonoHeader, tran)
            For Each detalle As cpp_AbonosDetalle In AbonoDetalle
                detalle.IdComprobante = AbonoHeader.IdComprobante
                objTablas.cpp_AbonosDetalleInsert(detalle, tran)
            Next

            'Genero los nuevos Cargos a los Nuevos Proveedores
            Dim IdCompra As Integer = fd.ObtenerUltimoId("com_Compras", "IdComprobante")
            For Each detalle As com_Compras In DetalleCompra
                IdCompra += 1
                detalle.IdComprobante = IdCompra
                objTablas.com_ComprasInsert(detalle, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function cpp_AplicarNotaCredito(ByRef NotaDetalle As List(Of com_NotasCredito)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Try
            For Each detalle As com_NotasCredito In NotaDetalle
                objTablas.com_NotasCreditoInsert(detalle, tran)
            Next
        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try
        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function cpp_InsertarQuedan(ByVal entQuedan As cpp_Quedan, ByVal Detalle As List(Of cpp_QuedanDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            'obtiene el Id del quedan
            entQuedan.IdComprobante = fd.ObtenerUltimoId("CPP_QUEDAN", "IdComprobante") + 1

            'Guarda el quedan
            objTablas.cpp_QuedanInsert(entQuedan, tran)

            For Each entDetalle As cpp_QuedanDetalle In Detalle
                entDetalle.IdComprobante = entQuedan.IdComprobante
                objTablas.cpp_QuedanDetalleInsert(entDetalle, tran)
            Next

            Dim sql = "update adm_correlativos set UltimoValor = UltimoValor + 1 where correlativo='QUEDAN'"
            db.ExecuteNonQuery(CommandType.Text, sql)

        Catch ex As Exception
            tran.Rollback()
            cn.Close()
            Return ex.Message
        End Try
        tran.Commit()
        cn.Close()
        Return ""

    End Function
    Public Function cpp_ActualizarQuedan(ByVal entQuedan As cpp_Quedan, ByVal Detalle As List(Of cpp_QuedanDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            'se actualiza el quedan
            objTablas.cpp_QuedanUpdate(entQuedan, tran)
            Dim sql As String = "delete cpp_QuedanDetalle where IdComprobante = " & entQuedan.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sql)
            For Each entDetalle As cpp_QuedanDetalle In Detalle
                'entDetalle.IdComprobante = entQuedan.IdComprobante
                objTablas.cpp_QuedanDetalleInsert(entDetalle, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            cn.Close()

            Return ex.Message
        End Try
        tran.Commit()
        cn.Close()
        Return ""

    End Function
    Public Function cpp_EliminaNotaCredido(ByVal IdNota As Integer) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            'se elimina la Nota de credito
            Dim sql As String = "delete com_NotasCredito where IdComprobante = " & IdNota
            db.ExecuteNonQuery(tran, CommandType.Text, sql)
        Catch ex As Exception
            tran.Rollback()
            cn.Close()
            Return ex.Message
        End Try
        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Function getList_Abonos(ByVal dDesde As Date, ByVal dHasta As Date, ByVal IdProveedor As String, ByVal Tipo As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpp_spList_Abonos", dDesde, dHasta, IdProveedor, Tipo, IdSucursal).Tables(0)
    End Function
    Function getList_Quedan(ByVal iTipo_Fecha As Integer, ByVal iTipo_Reporte As Integer, ByVal dDesde As Date, ByVal dHasta As Date) As DataTable

        Dim sql As String = "Select Que.Numero, Que.Fecha, Que.FechaPago, Que.IdProveedor, Pro.Nombre, Que.Concepto, "
        sql &= "Que.TotalPago, SaldoActual = case when FechaCancelacion IS NULL then Que.SaldoActual else 0.0 end , Que.FechaCancelacion from cpp_Quedan Que INNER JOIN Com_Proveedores Pro ON Que.IdProveedor = Pro.IdProveedor "

        If iTipo_Fecha = 0 Then
            sql &= "Where Fecha >= '" & Format(dDesde, "yyyyMMdd") & "' AND Fecha < DateAdd(dd,1, '" & Format(dHasta, "yyyyMMdd") & "')"
        Else
            If iTipo_Fecha = 1 Then
                sql &= "Where FechaPago >= '" + Format(dDesde, "yyyyMMdd") + "' AND FechaPago < DateAdd(dd,1, '" + Format(dHasta, "yyyyMMdd") + "')"
            Else
                sql &= "Where FechaCancelacion >= '" + Format(dDesde, "yyyyMMdd") + "' AND FechaCancelacion < DateAdd(dd,1, '" + Format(dHasta, "yyyyMMdd") + "')"
            End If
        End If


        If iTipo_Reporte = 0 Then
            sql &= " and FechaCancelacion IS NULL"
        End If

        If iTipo_Reporte = 1 Then
            sql &= " and not FechaCancelacion IS NULL"
        End If

        sql &= " ORDER BY Fecha"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Function getAnalisis(ByVal sHastaProv As String, ByVal dHastaFecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpp_spAnalisis", sHastaProv, dHastaFecha, IdSucursal).Tables(0)
    End Function
    Function cpp_ResumenSaldosProveedor(ByVal IdSucursal As Integer, ByVal Hasta As Date) As DataTable
        Return db.ExecuteDataSet("cpp_spSaldosProveedor", IdSucursal, Hasta).Tables(0)
    End Function
    Function getAnalisisDetalle(ByVal sDesdeProv As String, ByVal sHastaProv As String, ByVal dHastaFecha As Date) As DataTable
        Return db.ExecuteDataSet("cpp_spAnalisis_Detalle", sDesdeProv, sHastaProv, dHastaFecha).Tables(0)
    End Function
    Function getAuxiliar(ByVal sHastaProv As String, ByVal dHastaFecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpp_spAuxiliar", sHastaProv, dHastaFecha, IdSucursal).Tables(0)
    End Function
    Function cpp_MovimientosProveedor(ByVal sIdProveedor As String, ByVal dDesdeFecha As Date, ByVal dHastaFecha As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("cpp_spMovimientosProveedor", sIdProveedor, dDesdeFecha, dHastaFecha, IdSucursal).Tables(0)
    End Function
    Function cpp_ObtenerDocumentosAbonados(ByVal sIdProveedor As String, ByVal IdComprobante As Integer) As String
        Return db.ExecuteScalar("cpp_spDocumentosAbonados", sIdProveedor,IdComprobante)
    End Function
    Function cpp_ObtenerAbonosPorProveedor(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProveedor As String) As DataTable
        Dim sql As String = "Select NumeroComprobante, Fecha, Concepto, TotalAbono, IdComprobante from cpp_Abonos where IdProveedor = '" & IdProveedor
        sql &= "' and Fecha >='" & Format(Desde, "yyyyMMdd") & "' and Fecha < dateadd(dd,1,'" & Format(Hasta, "yyyyMMdd") & "') order by Fecha"

        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
End Class
