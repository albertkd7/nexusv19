﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common
Imports NexusELL.TableEntities

Public Class FacturaDLL

    Dim db As Database
    Dim fd As FuncionesDLL
    Dim objTablas As TableData

    Public Sub New(ByVal strConexion As String)

        Dim dbc As DBconfig = New DBconfig(strConexion)
        db = dbc.db
        fd = New FuncionesDLL(strConexion)
        objTablas = New TableData(strConexion)

    End Sub
    Public Function GetIdSucursal() As Integer
        Dim sSQL As String = "select IdSucursal from adm_Parametros"
        Return db.ExecuteScalar(CommandType.Text, sSQL)
    End Function

    Public Function fac_EliminaKardexPedido(ByVal IdPedido As Integer) As String

        ''INSERTO AL KARDEX/ NUEVO PARA KEYRAN

        Dim dcCommand_DELETE As System.Data.Common.DbCommand
        dcCommand_DELETE = db.GetStoredProcCommand("inv_spDeleteKardexPedidos", IdPedido)
        dcCommand_DELETE.CommandTimeout = 60000

        Return db.ExecuteScalar(dcCommand_DELETE)

    End Function
    Public Function fac_InsertarKardexPedido(ByVal IdPedido As Integer) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()

        Dim sSQL As String = "select top 1 IdTipoComprobante=isnull(Idtipocomprobante,0) from adm_TiposComprobante where IdModuloAplica = 10 order by IdTipoComprobante "
        Dim IdComprobante As Integer = fd.SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), 0)

        Dim dcCommand As System.Data.Common.DbCommand
        dcCommand = db.GetStoredProcCommand("inv_spInsertKardexPedidos", IdPedido)
        dcCommand.CommandTimeout = 60000
        dcCommand.Connection = cn
        'dcCommand.ExecuteNonQuery()

        Dim dcCommand2 As System.Data.Common.DbCommand
        dcCommand2 = db.GetStoredProcCommand("inv_spRenumerarMovimientos_Existencia", IdPedido, IdComprobante) 'TIPO DE COMPROBANTE ESPECIAL PARA EL PEDIDO DURAN
        dcCommand2.CommandTimeout = 60000
        dcCommand2.Connection = cn
        'dcCommand2.ExecuteNonQuery()

        Dim dcCommand3 As System.Data.Common.DbCommand
        dcCommand3 = db.GetStoredProcCommand("inv_spRecalculoCosto", IdPedido, IdComprobante) 'TIPO DE COMPROBANTE ESPECIAL PARA EL PEDIDO DURAN
        dcCommand3.CommandTimeout = 60000
        dcCommand3.Connection = cn
        'dcCommand3.ExecuteNonQuery()

        Dim msj As String = ""
        msj = db.ExecuteScalar(dcCommand)

        If msj = "Ok" Then ' SI INSERTO CORRECTAMENTE, EJECUTO LA RENUMERCACION DE MOVIMIENTO Y EXISTENCIAS
            msj = db.ExecuteScalar(dcCommand2)

            If msj = "Ok" Then ' SE RENUMERA EL IDMOVPRODUCTO Y EL COSTO
                msj = db.ExecuteScalar(dcCommand3)
            End If
        End If

        ''>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        Return msj
    End Function
    Public Function fac_ObtenerFormasPagoDetalle(ByVal IdComprobante As Integer) As DataTable
        Dim sSQL As String = "SELECT f.IdFormaPago,IdComprobVenta=Convert(INT," & IdComprobante & "),Total =Convert(DECIMAL(18,2),0)INTO #Deta FROM fac_FormasPago f WHERE DiasCredito=0"
        sSQL += " UPDATE #Deta SET Total =dt.Total FROM #Deta t INNER JOIN fac_VentasDetallePago dt ON t.IdFormaPago=dt.IdFormaPago AND dt.IdComprobVenta= " & IdComprobante
        sSQL += "  select * from #Deta"
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function fac_InformeComprobantesRetencion(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipo As Integer, ByVal IdGerente As Integer, ByVal IdCliente As String, ByVal Enviar As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spInformeComprobantesRetencion", Desde, Hasta, IdTipo, IdGerente, IdCliente, Enviar).Tables(0)
    End Function
    Public Function fac_DocumentosAsignados() As DataTable
        Dim sSQL As String = "select * from adm_SeriesDocumentos order by IdSucursal, IdPunto"
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function fac_DocumentosLegales(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Dim dcCommand As System.Data.Common.DbCommand
        dcCommand = db.GetStoredProcCommand("fac_spDocumentosLegales", Desde, Hasta, IdSucursal, IdPunto)
        dcCommand.CommandTimeout = 90000 ' 6
        Return db.ExecuteDataSet(dcCommand).Tables(0)
    End Function
    Public Function GetRetencionesDetalle(ByVal IdComprobante As Integer) As DataTable
        Dim sql As String = "select * from fac_RetencionDetalle where IdComprobante=" & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function com_ObtenerRetencion(ByVal IdComprobante As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spObtenerRetencion", IdComprobante).Tables(0)
    End Function
    Public Function fac_ConsultaFacturasCredito(ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Return db.ExecuteDataSet("fac_spConsultaFacturasCredito", Desde, Hasta).Tables(0)
    End Function
    Public Function fac_AprobarFacturaCredito(ByVal IdDoc As Integer, ByVal AprobarReprobar As Integer, ByVal Fecha As DateTime, ByVal AprobadoPor As String) As String
        Dim sSQL As String = ""
        If AprobarReprobar = 1 Then
            sSQL = "  update fac_Ventas set VentaTransferida = 1,"
            sSQL += " ModificadoPor ='" & AprobadoPor + "' ,FechaHoraModificacion= convert(DATETIME,'" & Format(Fecha, "yyyyMMdd") & " " & Format(Fecha, "HH:mm:ss") & "') "
            sSQL += " where IdComprobante = " & IdDoc
        Else
        End If

        Return db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Function
    Public Function fac_ConsultaSaldoCliente(ByVal idcliente As String, ByVal Dias As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spAnalisisDiasVencidoCliente", idcliente, Dias).Tables(0)
    End Function
    Public Function inv_GeneralesProducto(ByVal IdProducto As String) As DataTable
        Return db.ExecuteDataSet("inv_spGeneralesProducto", IdProducto).Tables(0)
    End Function
    Public Function fac_ConsultaPedidosDetalleGerencial(ByVal IdComprobante As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spConsultaPedidosDetalleGerencial", IdComprobante).Tables(0)
    End Function
    Public Function inv_ObtieneProductoAlias(ByVal Codigo As String) As String
        Dim sSQL As String = "select top 1 IdProducto=isnull(IdProducto,'') from inv_Productos where codbarra <> '' and CodBarra = '"
        sSQL &= Codigo & "'"
        Return fd.SiEsNulo(db.ExecuteScalar(CommandType.Text, sSQL), "")
    End Function
    Public Function rptDatosDeclaracionJuradaCliente(ByVal IdCliente As String) As DataTable
        Return db.ExecuteDataSet("fac_spDeclaracionJuradaCliente", IdCliente).Tables(0)
    End Function
    Public Function fac_ConsultaHistorialPorCodigoProducto(ByVal Codigo As String) As DataSet
        Return db.ExecuteDataSet("fac_spConsultaHistorialPorCodigoProducto", Codigo)
    End Function
    Public Function fac_ConsultaGerencial(ByVal Desde As Date, ByVal Hasta As Date, ByVal TipoDoc As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spConsultaGerencial", Desde, Hasta, TipoDoc).Tables(0)
    End Function
    Public Function fac_ObtenerPedidoPreHoja(ByVal IdPedido As Integer, ByVal IncluirCaracteristicas As Boolean) As DataTable
        Return db.ExecuteDataSet("fac_spObtenerPedidoDetalle", IdPedido, IncluirCaracteristicas).Tables(0)
    End Function
    Public Function fac_ConsultaProductos(ByVal Condicion As String) As DataTable
        Return db.ExecuteDataSet(CommandType.Text, Condicion).Tables(0)
    End Function
    Public Function fac_ConsultaProductosPrecios(ByVal Fecha As DateTime, ByVal IdGrupo As Integer) As DataTable
        Return db.ExecuteDataSet("inv_spConsultaProductosPrecio", Fecha, IdGrupo).Tables(0)
    End Function
    Public Function fac_ObtenerSeriesDocumentos(ByVal IdUsuario As String) As DataTable
        Dim sql As String = " select s.IdSucursal, s.IdPunto, s.IdTipoComprobante, s.Serie, s.Resolucion, s.FechaAsignacion, s.CreadoPor, s.UltimoNumero"
        sql += ",s.DesdeNumero,s.HastaNumero,s.EsActivo from adm_SeriesDocumentos s inner join adm_UsuariosSucursales suc on s.idsucursal = suc.idsucursal"
        sql += " where suc.idusuario= '" & IdUsuario & "'"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function

    Public Function fac_FechaServer() As Date
        Dim sql As String = "select convert(char(10), getdate(), 104)"
        '"select getdate() "
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function fac_VentasSucursal(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasSucursales", Desde, Hasta, IdSucursal).Tables(0)
    End Function
    Public Function fac_VentasSucursalSubGrupos(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasSucursalesSubGrupos", Desde, Hasta, IdSucursal).Tables(0)
    End Function
    Public Function fac_VentasSucursalMarca(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasSucursalesMarca", Desde, Hasta, IdSucursal).Tables(0)
    End Function
    Public Function fac_VentasSucursalGrupos(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasSucursalesGrupos", Desde, Hasta, IdSucursal).Tables(0)
    End Function
    Public Function fac_ValidaToken(ByVal Token As String, ByVal Descuento As Decimal, ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal IdProducto As String) As String
        Return fd.SiEsNulo(db.ExecuteScalar("fac_spValidaToken", Token, Descuento, Fecha, IdSucursal, IdProducto), "")
    End Function
    Public Function fac_ListadoAutorizacionesPorTOKEN(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdUsuario As String)
        Return db.ExecuteDataSet("fac_spListadoAutorizacionesTOKEN", Desde, Hasta, IdSucursal, IdUsuario).Tables(0)
    End Function
    Public Function fac_DescuentoToken(ByVal Token As String) As Decimal
        Return fd.SiEsNulo(db.ExecuteScalar(CommandType.Text, "select Descuento from fac_AutorizacionesDescuentos where autorizacion='" & Token & "'"), 0.0)
    End Function
    Public Function fac_ConsultaCotizaciones(ByVal Usuario As String, Optional IdVendedor As Integer = 0) As DataSet
        Dim sql As String = "SELECT c.IdComprobante, c.Numero, c.Fecha, c.Nombre, c.AtencionA, c.IdSucursal, c.TotalComprobante, c.CreadoPor, c.FechaHoraCreacion"
        sql &= " FROM Fac_Cotizaciones c " &
               " inner join adm_UsuariosSucursales s on c.idsucursal= s.idsucursal " &
               " left join adm_Usuarios u on c.CreadoPor = u.IdUsuario "
        sql &= " where s.idusuario ='" & Usuario & "'"
        sql &= IIf(IdVendedor > 0, " and u.IdVendedor = " & IdVendedor & " ", " ")
        sql &= " order by c.IdComprobante desc; " &
               " select d.* from fac_CotizacionesDetalle d " &
               " inner join fac_cotizaciones c on d.idcomprobante = c.idcomprobante " &
               " inner join adm_UsuariosSucursales s on c.idsucursal= s.idsucursal " &
               " left join adm_Usuarios u on c.CreadoPor = u.IdUsuario "
        sql &= " where s.idusuario ='" & Usuario & "'"
        sql &= IIf(IdVendedor > 0, " and u.IdVendedor = " & IdVendedor & " ", " ")

        Dim ds As DataSet = db.ExecuteDataSet(CommandType.Text, sql)
        Dim dr As New DataRelation("Cotiza_Detalle", ds.Tables(0).Columns("IdComprobante"), ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function
    Public Function fac_RptConsusltaFormasPago(ByVal DesdeFecha As DateTime, ByVal HastaFecha As DateTime, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSetWait("fac_RptConsusltaFormasPago", DesdeFecha, HastaFecha, IdSucursal).Tables(0)
    End Function

    Public Function fac_spRptVentasRutas(ByVal DesdeFecha As DateTime, ByVal HastaFecha As DateTime, ByVal IdTipoComprobante As Integer, ByVal IdFormaPago As Integer, ByVal IdRuta As Integer) As DataTable
        Return db.ExecuteDataSetWait("fac_spRptVentasRutas", DesdeFecha, HastaFecha, IdTipoComprobante, IdFormaPago, IdRuta).Tables(0)
    End Function

    Public Function fac_ConsultaVales(ByVal Usuario As String) As DataTable
        Dim ds As New DataSet
        Dim sSQL As String


        sSQL = "SELECT c.IdComprobante, Numero=c.NumeroComprobante, c.Fecha, c.Nombre, c.IdSucursal, c.Valor, c.CreadoPor, c.FechaHoraCreacion"
        sSQL &= " FROM Fac_vales c inner join adm_UsuariosSucursales s on c.idsucursal= s.idsucursal "
        sSQL &= " where s.idusuario ='" & Usuario & "'"
        sSQL &= " order by c.IdComprobante desc"

        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function fac_ConsultaAperturasCaja(ByVal Usuario As String) As DataTable
        Dim ds As New DataSet
        Dim sSQL As String


        sSQL = "SELECT c.IdComprobante, c.Fecha, Encargado=c.responsable, c.IdSucursal, Fondo=c.fondoapertura, c.Comentario, c.CreadoPor, c.FechaHoraCreacion"
        sSQL &= " FROM Fac_aperturacaja c inner join adm_UsuariosSucursales s on c.idsucursal= s.idsucursal "
        sSQL &= " where s.idusuario ='" & Usuario & "'"
        sSQL &= " order by c.IdComprobante desc"

        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function

    Public Function fac_ConsultaRetenciones(ByVal Usuario As String) As DataSet
        Dim ds As New DataSet
        Dim sSQL As String



        sSQL = "SELECT r.IdComprobante, r.Numero, r.Fecha, r.Nombre, r.IdSucursal, r.TotalComprobante,  r.CreadoPor, r.FechaHoraCreacion from fac_Retencion r inner join adm_UsuariosSucursales s on r.idsucursal= s.idsucursal"
        sSQL &= " where s.idusuario ='" & Usuario & "'"
        sSQL &= " order by r.Fecha desc ; select d.* from fac_RetencionDetalle d inner join fac_Retencion r on d.idcomprobante = r.idcomprobante inner join adm_UsuariosSucursales s on r.idsucursal= s.idsucursal"
        sSQL &= " where s.idusuario ='" & Usuario & "'"

        ds = db.ExecuteDataSet(CommandType.Text, sSQL)

        Dim dr As New DataRelation("Notas_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
        ds.Relations.Add(dr)
        Return ds
    End Function

    Public Function fac_ObtenerEstructuraPago() As DataTable
        Dim sSQL As String = "select IdFormaPago, Total = convert(decimal(18,2),0), IdComprobVenta=convert(int,0) from fac_formaspago where DiasCredito=0 and  IdFormaPago=" & -999
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
	Public Function fac_GestionesbyCliente(ByVal IdCliente As String, ByVal IdVendedor As Integer) As DataTable
		Return db.ExecuteDataSet("fac_spGestionesbyCliente", IdCliente, IdVendedor).Tables(0)
	End Function
	Public Function fac_GestionesbyIdProspecto(ByVal IdComprobante As Integer, ByVal IdVendedor As Integer) As DataTable
		Return db.ExecuteDataSet("fac_spGestionesbyProspecto", IdComprobante, IdVendedor).Tables(0)
	End Function

    Public Function fac_ClientesInfoAnexos(ByVal IdCliente As String) As DataTable
        Dim SQL As String = "select * from fac_ClientesAnexo where IdCliente='" & IdCliente & "'"
        Return db.ExecuteDataSet(CommandType.Text, SQL).Tables(0)
    End Function

    Public Function fac_ClientesObtieneNombre(ByVal IdCliente As String) As String
        Dim query As String = "SELECT max(Nombre) FROM fac_Clientes WHERE IdCliente = '" & IdCliente & "'"
        Dim obj = db.ExecuteScalar(CommandType.Text, query)
        Return IIf(obj Is DBNull.Value, "", Convert.ToString(obj))
    End Function
    'Public Function fac_PedidosParaTraslados(ByVal Idbodega As Integer) As DataTable
    '    Dim sql As String = "Select p.Numero, p.Fecha, p.Nombre, Sucursal= su.nombre, p.CreadoPor, p.IdComprobante, p.IdBodega, p.FechaHoraCreacion from fac_pedidos p inner join adm_sucursales su on p.idsucursal = su.idsucursal where p.reservado=1 and p.facturado=0 and p.idbodega =  " & Idbodega
    '    sql += " and p.IdComprobante not in (Select Idpedido from inv_Traslados)"
    '    sql += " order by p.fecha desc"
    '    Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    'End Function
    Public Function fac_PedidosParaTraslados(ByVal IdSucursal As Integer) As DataTable
        Dim sql As String = "Select p.Numero, p.Fecha, p.Nombre, Sucursal= su.nombre, p.CreadoPor, p.IdComprobante, p.IdBodega, p.FechaHoraCreacion from fac_ventas p inner join adm_sucursales su on p.idsucursal = su.idsucursal where p.idtipocomprobante=19 and p.IdSucursalEnvio =  " & IdSucursal
        sql += " and p.IdComprobante not in (Select Idcomprobventa from inv_entradas)"
        sql += " order by p.fecha desc"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function fac_ExisteAperturaCaja(ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As Integer
        Dim sSQL As String = "Select IdComprobante=isnull(IdComprobante,0) from fac_aperturacaja where IdSucursal = " & IdSucursal
        sSQL += " and IdPunto = " & IdPunto
        sSQL += " and Fecha = '" & Format(Fecha, "yyyyMMdd") & "'"
        Return db.ExecuteScalar(CommandType.Text, sSQL)
    End Function
    Public Function fac_VerificaFacturadoPedido(ByVal IdPedido As Integer) As Integer
        Dim sSQL As String = String.Format("Select isnull(Facturado,0) from fac_pedidos where IdComprobante = {0}", IdPedido)
        Return db.ExecuteScalar(CommandType.Text, sSQL)
    End Function
    Public Function fac_VerificaExistenciasPorIdPedido(ByVal IdPedido As Integer, ByVal IdBodega As Integer, ByVal Tipo As Boolean) As DataTable
        Return db.ExecuteDataSet("fac_spVerificaExistenciasPorPedido", IdPedido, IdBodega, Tipo).Tables(0)
    End Function
    Public Function fac_GenerarFacturaPedido(ByVal IdComprobante As Integer, ByVal Factor As Decimal) As DataTable
        Return db.ExecuteDataSet("fac_spGenerarFacturaPedido", IdComprobante, Factor).Tables(0)
    End Function
    Public Function fac_ObtenerDetallePedidoParaArchivo(ByVal IdComprobante As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spObtenerDetallePedidoParaArchivo", IdComprobante).Tables(0)
    End Function
    Public Function fac_ObtenerVentasPedido(ByVal IdComprobante As Integer) As DataTable
        Dim sSQL As String = "Select IdComprobante, Numero, Fecha, IdCliente, Nombre, TotalComprobante from fac_ventas where IdComprobanteNota > 0 and TotalComprobante <> 0.0 and  IdComprobanteNota = " & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function fac_ActualizaFormaPago(ByVal IdComprobante As Integer, ByVal IdPedio As Integer, ByVal IdFormaPago As Integer) As String
        Dim sSQL As String = "update fac_Ventas set IdFormaPago = " & IdFormaPago
        sSQL += " where IdComprobante = " & IdComprobante
        sSQL += " ; update fac_Pedidos set IdFormaPago = " & IdFormaPago
        sSQL += " where IdComprobante = " & IdPedio

        Return db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Function

    Public Function fac_ExtraePedidoAnular(ByVal IdComprobante As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spObtienePedidoDevolucion", IdComprobante).Tables(0)
    End Function


    Public Function GetObtenerCorrelativoFacturacion(ByVal IdPunto As Integer, ByVal IdTipoComprobante As Integer) As Integer
        Dim sSQL As String = "select Serie, UltimoNumero, HastaNumero from adm_SeriesDocumentos Where EsActivo=1"
        sSQL &= String.Format(" and IdPunto={0} and IdTipoComprobante={1}", IdPunto, IdTipoComprobante)
        Dim UltimoNumero As Integer
        Dim dt As DataTable = db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
        If dt.Rows.Count < 1 Then  'NO SE HAN DEFINIDO LAS SERIES O CORRELATIVOS DE LOS DOCUMENTOS
            Return 0
        End If
        If dt.Rows(0).Item("UltimoNumero") + 1 > dt.Rows(0).Item("HastaNumero") Then 'ya no hay documentos disponibles para facturar
            Return 0
        End If
        UltimoNumero = dt.Rows(0).Item("UltimoNumero") + 1
        Return UltimoNumero
    End Function
    Public Function GetObtenerCorrelativoFacturacionUnico(ByVal IdPunto As Integer, ByVal IdSucursal As Integer) As Integer

        Dim sSQL As String = "select UltimoNumero, HastaNumero from adm_sucursales  "
        sSQL &= String.Format(" WHERE Idsucursal={0}", IdSucursal)
        Dim UltimoNumero As Integer
        Dim dt As DataTable = db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
        If dt.Rows.Count < 1 Then  'NO SE HAN DEFINIDO LAS SERIES O CORRELATIVOS DE LOS DOCUMENTOS
            Return 0
        End If
        If dt.Rows(0).Item("UltimoNumero") + 1 > dt.Rows(0).Item("HastaNumero") Then 'ya no hay documentos disponibles para facturar
            Return 0
        End If
        UltimoNumero = dt.Rows(0).Item("UltimoNumero") + 1
        Return UltimoNumero
    End Function

    Public Function GetObtenerSerieDocumento(ByVal IdPunto As Integer, ByVal IdTipoComprobante As Integer) As String
        Dim sSQL As String = "select Serie from adm_SeriesDocumentos Where EsActivo=1"
        sSQL &= String.Format(" and IdPunto={0} and IdTipoComprobante={1}", IdPunto, IdTipoComprobante)
        Dim dt As DataTable = db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
        If dt.Rows.Count < 1 Then  'NO SE HAN DEFINIDO LAS SERIES O CORRELATIVOS DE LOS DOCUMENTOS
            Return ""
        Else
            Return dt.Rows(0).Item("Serie")
        End If

    End Function
    Public Function InsertRemision(ByRef RemisionHeader As fac_NotasRemision, ByRef RemisionDetalle As List(Of fac_NotasRemisionDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj = ""
        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            RemisionHeader.IdComprobante = fd.ObtenerUltimoId("FAC_NOTASREMISION", "IdComprobante") + 1

            objTablas.fac_NotasRemisionInsert(RemisionHeader, tran)

            For Each detalle As fac_NotasRemisionDetalle In RemisionDetalle
                detalle.IdComprobante = RemisionHeader.IdComprobante
                objTablas.fac_NotasRemisionDetalleInsert(detalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj
    End Function
    Public Function InsertRetencion(ByRef RetencionHeader As fac_Retencion, ByRef RetencionDetalle As List(Of fac_RetencionDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj = ""
        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            RetencionHeader.IdComprobante = fd.ObtenerUltimoId("FAC_RETENCION", "IdComprobante") + 1

            objTablas.fac_RetencionInsert(RetencionHeader, tran)

            For Each detalle As fac_RetencionDetalle In RetencionDetalle
                detalle.IdComprobante = RetencionHeader.IdComprobante
                objTablas.fac_RetencionDetalleInsert(detalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj
    End Function


    Public Function InsertVendedores(ByRef Vendedores As fac_Vendedores, ByRef DetalleComision As List(Of fac_VendedoresDetalle), ByRef DetalleClientesVendedor As List(Of fac_VendedoresClientes)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj = ""

        Try
            objTablas.fac_VendedoresInsert(Vendedores, tran)
            For Each detalle As fac_VendedoresDetalle In DetalleComision
                detalle.IdVendedor = Vendedores.IdVendedor
                objTablas.fac_VendedoresDetalleInsert(detalle, tran)
            Next
            For Each cliente As fac_VendedoresClientes In DetalleClientesVendedor
                cliente.IdVendedor = Vendedores.IdVendedor
                objTablas.fac_VendedoresClientesInsert(cliente, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj
    End Function

    Public Function UpdateVendedores(ByRef Vendedores As fac_Vendedores, ByRef DetalleComision As List(Of fac_VendedoresDetalle), ByRef DetalleClientesVendedor As List(Of fac_VendedoresClientes)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj = ""

        Try
            objTablas.fac_VendedoresUpdate(Vendedores, tran)
            Dim sql1 As String = "delete fac_VendedoresDetalle where IdVendedor=" & Vendedores.IdVendedor
            Dim sql2 As String = "delete fac_VendedoresClientes where IdVendedor=" & Vendedores.IdVendedor
            db.ExecuteNonQuery(tran, CommandType.Text, sql1)
            db.ExecuteNonQuery(tran, CommandType.Text, sql2)

            For Each detalle As fac_VendedoresDetalle In DetalleComision
                detalle.IdVendedor = Vendedores.IdVendedor
                objTablas.fac_VendedoresDetalleInsert(detalle, tran)
            Next
            For Each cliente As fac_VendedoresClientes In DetalleClientesVendedor
                cliente.IdVendedor = Vendedores.IdVendedor
                objTablas.fac_VendedoresClientesInsert(cliente, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj
    End Function

    Public Function UpdateRemision(ByRef RemisionHeader As fac_NotasRemision, ByRef RemisionDetalle As List(Of fac_NotasRemisionDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj = ""
        Try
            objTablas.fac_NotasRemisionUpdate(RemisionHeader, tran)
            Dim sql As String = "delete fac_NotasRemisionDetalle where IdComprobante=" & RemisionHeader.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sql)
            For Each detalle As fac_NotasRemisionDetalle In RemisionDetalle
                objTablas.fac_NotasRemisionDetalleInsert(detalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj
    End Function
    Public Function UpdateRetencion(ByRef RetencionHeader As fac_Retencion, ByRef RetencionDetalle As List(Of fac_RetencionDetalle)) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj = ""
        Try
            objTablas.fac_RetencionUpdate(RetencionHeader, tran)
            Dim sql As String = "delete fac_RetencionDetalle where IdComprobante=" & RetencionHeader.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sql)
            For Each detalle As fac_RetencionDetalle In RetencionDetalle
                objTablas.fac_RetencionDetalleInsert(detalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj
    End Function


    Public Function fac_ObtenerIdNotaRemision(ByVal IdComprobante As Integer, ByVal TipoAvance As Integer) As Integer
        Dim sql As String = "select top 1 IdComprobante from fac_NotasRemision where IdComprobante < " & IdComprobante & " order by IdComprobante desc"
        If TipoAvance = 1 Then  'para obtener el registro anterior
            sql = "select top 1 IdComprobante from fac_NotasRemision where IdComprobante > " & IdComprobante
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function fac_ObtenerIdVale(ByVal IdComprobante As Integer, ByVal TipoAvance As Integer) As Integer
        Dim sql As String = "select top 1 IdComprobante from fac_vales where IdComprobante < " & IdComprobante & " order by IdComprobante desc"
        If TipoAvance = 1 Then  'para obtener el registro anterior
            sql = "select top 1 IdComprobante from fac_vales where IdComprobante > " & IdComprobante
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function fac_ObtenerIdRetencion(ByVal IdComprobante As Integer, ByVal TipoAvance As Integer) As Integer
        Dim sql As String = "select top 1 IdComprobante from fac_Retencion where IdComprobante < " & IdComprobante & " order by IdComprobante desc"
        If TipoAvance = 1 Then  'para obtener el registro anterior
            sql = "select top 1 IdComprobante from fac_Retencion where IdComprobante > " & IdComprobante
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function fac_ObtenerHorasClientes(ByVal Desde As DateTime, ByVal Hasta As DateTime, ByVal IdCliente As String) As DataTable
        Return db.ExecuteDataSet("pla_spHorasTrabajadasCliente", Desde, Hasta, IdCliente).Tables(0)
    End Function

    Public Function fac_ObtenerClientesPrecios(ByVal IdCliente As String) As DataTable
        Dim sql As String = "select * from fac_clientesprecios where idcliente='" & IdCliente & "'"
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function fac_InsertarPedido(ByRef PedidoHeader As fac_Pedidos, ByRef PedidoDetalle As List(Of fac_PedidosDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""
        Try
            PedidoHeader.IdComprobante = fd.ObtenerUltimoId("FAC_PEDIDOS", "IdComprobante") + 1
            If PedidoHeader.Numero = "" Then
                PedidoHeader.Numero = fd.ObtenerCorrelativoManual("PEDIDOS_INTERNOS", tran).ToString.PadLeft(6, "0")
            End If
            objTablas.fac_PedidosInsert(PedidoHeader, tran)

            For Each detalle As fac_PedidosDetalle In PedidoDetalle
                detalle.IdComprobante = PedidoHeader.IdComprobante
                objTablas.fac_PedidosDetalleInsert(detalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try
        Return msj


        Return ""
    End Function
    Public Function fac_ActualizaPedido(ByRef PedidoHeader As fac_Pedidos, ByRef PedidoDetalle As List(Of fac_PedidosDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""
        Try
            objTablas.fac_PedidosUpdate(PedidoHeader, tran)

            Dim sql As String = "delete fac_PedidosDetalle where IdComprobante=" & PedidoHeader.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sql)

            For Each detalle As fac_PedidosDetalle In PedidoDetalle
                detalle.IdComprobante = PedidoHeader.IdComprobante
                objTablas.fac_PedidosDetalleInsert(detalle, tran)
            Next
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj
    End Function
    Public Function fac_ConsultaPedidos(ByVal IdUsuario As String) As DataTable
        Return db.ExecuteDataSet("fac_spConsultaPedidos", IdUsuario).Tables(0)
    End Function
    Public Function fac_ConsultaPedidos(ByVal IdUsuario As String, ByVal Desde As Date, ByVal Hasta As Date, ByVal Pendiente As Boolean) As DataTable
        Return db.ExecuteDataSet("fac_spConsultaPedidosPrefac", IdUsuario, Desde, Hasta, Not Pendiente).Tables(0)
    End Function
    Public Function fac_ConsultaFacturacion(ByVal Desde As Date, ByVal Hasta As Date, ByVal TipoDoc As Integer, ByVal IdUsuario As String) As DataSet
        Dim ds As DataSet = db.ExecuteDataSet("fac_spConsultaFacturacion", Desde, Hasta, TipoDoc, IdUsuario)
        If ds.Tables.Count > 1 Then
            Dim dr As New DataRelation("Facturas_Detalle" _
                                   , ds.Tables(0).Columns("IdComprobante") _
                                   , ds.Tables(1).Columns("IdComprobante"))
            ds.Relations.Add(dr)
        End If
        Return ds
    End Function
    Public Function fac_PedidosPendientes(ByVal Desde As Date, ByVal Hasta As Date) As DataTable
        Return db.ExecuteDataSet("fac_spPedidosPendientes", Desde, Hasta).Tables(0)
    End Function
    Public Function fac_DetalleVale(ByVal ID As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVales", ID).Tables(0)
    End Function
    Public Function fac_ObtenerIdPedidoPorNumero(ByVal Numero As String) As Integer
        Dim sql As String = "select IdComprobante from fac_Pedidos where Numero = '" & Numero & "'"
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function

    Public Function fac_ObtenerContrasenaDescuento(ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal Descuento As Decimal, ByVal Usuario As String, ByVal IdProducto As String) As String
        Return db.ExecuteScalar("fac_spObgenerAutorizacionDesc", Fecha, IdSucursal, Descuento, Usuario, IdProducto)
    End Function

    Public Function fac_ObtenerPedidoDetalle(ByVal IdComprobante As Integer, ByVal Impuesto As Decimal, ByRef DocumentoDetallaIva As Boolean, ByVal TipoVenta As Integer) As DataTable
        Dim sql As String

        sql = "select IdProducto, IdPrecio, Cantidad, CantidadUG=convert(decimal(18,2),0.0), Descripcion, PrecioVenta, PorcDescuento, ValorDescuento, PrecioUnitario, PrecioTotal, "
        sql &= "VentaNoSujeta=0.0, VentaExenta=0.0, VentaAfecta = PrecioTotal, VentaNeta, TipoImpuesto=1, ValorIva, "
        sql &= "PrecioCosto=isnull(PrecioCosto,0), TipoProducto = isnull(TipoProducto,1), EsEspecial=isnull(EsEspecial,0), EsCompuesto=isnull(EsCompuesto,0), IdCentro=convert(varchar(10),'') "
        sql &= "from fac_PedidosDetalle where IdComprobante = " & IdComprobante

        Dim dt As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)

        If Not DocumentoDetallaIva Then
            Return dt
        End If

        For Each dr As DataRow In dt.Rows

            'cuando el iva esta incluído en la facturacion. El precio del producto está guardado con IVA incluido
            Dim PrecioVenta, Precio, Cantidad, PrecioIva, Iva As Decimal
            PrecioIva = dr("VentaAfecta")
            PrecioVenta = IIf(DocumentoDetallaIva, dr("PrecioVenta") / (Impuesto + 1), dr("PrecioVenta"))
            Precio = IIf(DocumentoDetallaIva, dr("PrecioUnitario") / (Impuesto + 1), dr("PrecioUnitario"))
            Cantidad = dr("Cantidad")
            Iva = dr("ValorIva")
            dr("ValorIVA") = Iva

            dr("ValorDescuento") = 0.0
            dr("ValorIVA") = 0.0
            If TipoVenta = 2 Then 'precio exento
                dr("VentaExenta") =  dr("PrecioTotal")
                dr("VentaAfecta") = 0.0
                dr("ValorNeto") = Precio
                dr("ValorIVA") = 0.0
            End If
        Next

        Return dt
    End Function
    Public Function fac_ObtenerIdPedido(ByVal IdComprobante As Integer, ByVal TipoAvance As Integer) As Integer
        Dim sql As String = String.Format("select top 1 IdComprobante from fac_Pedidos where IdComprobante < {0} order by IdComprobante desc", IdComprobante)
        If TipoAvance = 1 Then  'para obtener el registro anterior
            sql = "select top 1 IdComprobante from fac_Pedidos where IdComprobante > " & IdComprobante
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function

    Public Function fac_ObtenerIdDocumento(ByVal Tabla As String, ByVal IdComprobante As Integer, ByVal TipoAvance As Integer) As Integer
        Dim sql As String = String.Format("select top 1 IdComprobante from " & Tabla & " where IdComprobante < {0} order by IdComprobante desc", IdComprobante)
        If TipoAvance = 1 Then  'para obtener el registro anterior
            sql = "select top 1 IdComprobante from " & Tabla & " where IdComprobante > " & IdComprobante
        End If
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function fac_ObtenerPedido(ByVal IdPedido As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spObtenerPedido", IdPedido).Tables(0)
    End Function
    Public Function fac_ObtenerNotaRemision(ByVal IdComprobante As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spObtenerNotaRemision", IdComprobante).Tables(0)
    End Function
    Public Function fac_ObtenerFormasPago(ByVal IdFormaPago As Integer, ByVal TotalComprobante As Decimal) As DataTable
        Dim sSQL As String = "select IdFormaPago, Total = case when IdFormaPago =" & IdFormaPago
        sSQL += " then " & TotalComprobante
        sSQL += " else convert(decimal(18,2),0) end, IdComprobVenta=convert(int,0) from fac_formaspago  where DiasCredito=0 order by idformapago"
        Return db.ExecuteDataSet(CommandType.Text, sSQL).Tables(0)
    End Function
    Public Function fac_VerificaCodigoCliente(ByVal IdCliente As String) As Boolean
        Return db.ExecuteScalar(CommandType.Text, "select 1 from fac_clientes where IdCliente = '" & IdCliente & "'") = 1
    End Function
    Public Function fac_InsertarCotizacion(ByRef entCotizacion As fac_Cotizaciones, ByRef entCotizacionDetalle As List(Of fac_CotizacionesDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            entCotizacion.IdComprobante = fd.ObtenerUltimoId("FAC_COTIZACIONES", "IdComprobante") + 1
            objTablas.fac_CotizacionesInsert(entCotizacion, tran)

            For Each detalle As fac_CotizacionesDetalle In entCotizacionDetalle
                detalle.IdComprobante = entCotizacion.IdComprobante
                objTablas.fac_CotizacionesDetalleInsert(detalle, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function fac_ActualizarCotizacion(ByRef entCotiza As fac_Cotizaciones, ByRef CotizacionDetalle As List(Of fac_CotizacionesDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()

        Try
            objTablas.fac_CotizacionesUpdate(entCotiza, tran)

            Dim sSQL As String = "delete fac_CotizacionesDetalle where IdComprobante=" & entCotiza.IdComprobante
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)

            For Each detalle As fac_CotizacionesDetalle In CotizacionDetalle
                objTablas.fac_CotizacionesDetalleInsert(detalle, tran)
            Next

        Catch ex As Exception
            tran.Rollback()
            Return ex.Message
        End Try

        tran.Commit()
        cn.Close()
        Return ""
    End Function
    Public Function fac_InsertaFactura(ByRef FacturaHeader As fac_Ventas, ByRef FacturaDetalle As List(Of fac_VentasDetalle), ByRef FacturaDetallePago As List(Of fac_VentasDetallePago), ByVal Tipo As String, ByVal AsignarNumero As Boolean) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""

        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            Dim Serie As String = "", SerieUnico As String = "", UltimoNumero As Integer = 0, UltimoNumeroUnico As Integer = 0
            Dim entTipos As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(FacturaHeader.IdTipoComprobante)

            'para los digitadores no correra el correlativo, solo para los demas usuarios, porque siguen procesando datos
            If Not AsignarNumero Then
                UltimoNumero = CInt(FacturaHeader.Numero)
            End If

            Serie = GetSerie(FacturaHeader.IdPunto, FacturaHeader.IdTipoComprobante, Serie, UltimoNumero, AsignarNumero, tran)
            If UltimoNumero = 0 Then
                tran.Rollback()
                cn.Close()
                Return "No se pudo obtener el número del documento. No se han definido las series"
            End If
            If UltimoNumero = -1 Then
                tran.Rollback()
                cn.Close()
                Return "Ya no hay mas númeración disponible para facturar"
            End If

            If entTipos.FormularioUnico Then
                SerieUnico = GetNumeroUnico(FacturaHeader.IdPunto, FacturaHeader.IdSucursal, UltimoNumeroUnico, tran)

                If UltimoNumeroUnico = -1 Then
                    tran.Rollback()
                    cn.Close()
                    Return "Ya no hay mas númeración disponible para facturar con Formulario Único"
                End If
            End If

            FacturaHeader.Numero = UltimoNumero.ToString.PadLeft(6, "0")
            FacturaHeader.Serie = Serie
            FacturaHeader.IdComprobante = fd.ObtenerUltimoId("FAC_VENTAS", "IdComprobante") + 1
            If entTipos.FormularioUnico Then
                FacturaHeader.NumFormularioUnico = UltimoNumeroUnico.ToString.PadLeft(6, "0")
            End If

            If Tipo = "PEDIDO" Then

            End If
            'SI ES DEVOLUCION Y VIENE DE PEDIDO VUELVO NEGATIVO LOS VALORES
            If Tipo = "PEDIDO" And FacturaHeader.EsDevolucion Then
                FacturaHeader.TotalDescto = FacturaHeader.TotalDescto * -1
                FacturaHeader.TotalComprobante = FacturaHeader.TotalComprobante * -1
                FacturaHeader.TotalIva = FacturaHeader.TotalIva * -1
                FacturaHeader.TotalImpuesto1 = FacturaHeader.TotalImpuesto1 * -1
                FacturaHeader.TotalNoSujeto = FacturaHeader.TotalNoSujeto * -1
                FacturaHeader.TotalExento = FacturaHeader.TotalExento * -1
                FacturaHeader.TotalAfecto = FacturaHeader.TotalAfecto * -1
                FacturaHeader.TotalNeto = FacturaHeader.TotalNeto * -1
                FacturaHeader.SaldoActual = FacturaHeader.SaldoActual * -1
            End If

            objTablas.fac_VentasInsert(FacturaHeader, tran)

            For Each detalle As fac_VentasDetalle In FacturaDetalle
                detalle.IdComprobante = FacturaHeader.IdComprobante
                'SI ES DEVOLUCION Y VIENE DE PEDIDO VUELVO NEGATIVO LOS VALORES
                If Tipo = "PEDIDO" And FacturaHeader.EsDevolucion Then
                    detalle.Cantidad = detalle.Cantidad * -1
                    detalle.PrecioVenta = detalle.PrecioVenta * -1
                    detalle.PorcDescuento = detalle.PorcDescuento * -1
                    detalle.ValorDescuento = detalle.ValorDescuento * -1
                    detalle.PrecioUnitario = detalle.PrecioUnitario * -1
                    detalle.VentaNoSujeta = detalle.VentaNoSujeta * -1
                    detalle.VentaExenta = detalle.VentaExenta * -1
                    detalle.VentaAfecta = detalle.VentaAfecta * -1
                    detalle.VentaNeta = detalle.VentaNeta * -1
                    detalle.ValorIVA = detalle.ValorIVA * -1
                End If

                objTablas.fac_VentasDetalleInsert(detalle, tran)
                Dim sql As String
                If detalle.DescuentoAutorizadoPor <> "" Then
                    Sql = "update fac_AutorizacionesDescuentos set IdComprobante =" & detalle.IdComprobante
                    'sql += ", IdProducto='" & detalle.IdProducto & "'"
                    Sql += ", IdDetalle=" & detalle.IdDetalle
                    Sql += " where Autorizacion ='" & detalle.DescuentoAutorizadoPor & "'"
                    db.ExecuteNonQuery(CommandType.Text, Sql)
                End If
            Next

            If Tipo = "VENTA DIRECTA" Then
                For Each detalle As fac_VentasDetallePago In FacturaDetallePago
                    detalle.IdComprobVenta = FacturaHeader.IdComprobante
                    objTablas.fac_VentasDetallePagoInsert(detalle, tran)
                Next

                If FacturaHeader.OrdenCompra <> "" Then
                    Dim sql As String = String.Format("update fac_pedidos set Facturado= 1, IdComprobanteFactura={0} where numero = '{1}'", FacturaHeader.IdComprobante, FacturaHeader.OrdenCompra)
                    db.ExecuteNonQuery(tran, CommandType.Text, sql)
                End If
            End If

            If Tipo = "PEDIDO" Then  'se está facturando un pedido
                Dim sql As String = String.Format("update fac_pedidos set Facturado= 1, IdComprobanteFactura={0} where IdComprobante = {1}", FacturaHeader.IdComprobante, FacturaHeader.IdComprobanteNota)
                db.ExecuteNonQuery(tran, CommandType.Text, sql)
            End If

            If Tipo = "COTIZACION" Then  'se está facturando desde una cotización
                'Dim sql As String = "update fac_cotizaciones set Facturado= 1 where Numero = " & FacturaHeader.OrdenCompra
                'db.ExecuteNonQuery(tran, CommandType.Text, Sql)
            End If
            db.ExecuteNonQuery(tran, "inv_spInsertKardexVentas", FacturaHeader.IdComprobante)
            db.ExecuteNonQuery(tran, "inv_spRenumerarMovimientos_Existencia", FacturaHeader.IdComprobante, FacturaHeader.IdTipoComprobante)
            db.ExecuteNonQuery(tran, "inv_spRecalculoCosto", FacturaHeader.IdComprobante, FacturaHeader.IdTipoComprobante)


            tran.Commit()

        Catch ex As Exception
            tran.Rollback()
            msj = ex.ToString
        Finally

            cn.Close()
        End Try
        Return msj

    End Function
    'Public Function fac_ActualizaFactura(ByRef FacturaHeader As fac_Ventas, ByRef FacturaDetalle As List(Of fac_VentasDetalle)) As Boolean
    '    Dim cn As DbConnection = db.CreateConnection
    '    cn.Open()
    '    Dim tran As DbTransaction = cn.BeginTransaction()

    '    Try
    '        'Verifico si el comprobante es una devolución, 1= venta, 2=devolución
    '        'Dim iTipoAplicacion = GetTipoAplicacion(FacturaHeader.IdTipoComprobante)
    '        'los valores se vuelven negativos cuando es una devolución
    '        'If iTipoAplicacion = 2 Then
    '        '    FacturaHeader.TotalComprobante = -FacturaHeader.TotalComprobante
    '        '    FacturaHeader.TotalAfecto = -FacturaHeader.TotalAfecto
    '        '    FacturaHeader.TotalExento = -FacturaHeader.TotalExento
    '        '    FacturaHeader.TotalIva = -FacturaHeader.TotalIva
    '        '    FacturaHeader.TotalImpuesto1 = -FacturaHeader.TotalImpuesto1
    '        '    FacturaHeader.TotalImpuesto2 = -FacturaHeader.TotalImpuesto2
    '        '    FacturaHeader.TotalDescto = -FacturaHeader.TotalDescto
    '        '    FacturaHeader.SaldoActual = -FacturaHeader.SaldoActual
    '        'End If
    '        objTablas.fac_VentasUpdate(FacturaHeader, tran)

    '        Dim sSQL As String = "delete fac_ventasdetalle where IdComprobante=" & FacturaHeader.IdComprobante
    '        db.ExecuteNonQuery(tran, CommandType.Text, sSQL)

    '        For Each detalle As fac_VentasDetalle In FacturaDetalle
    '            objTablas.fac_VentasDetalleInsert(detalle, tran)
    '        Next
    '    Catch ex As Exception
    '        tran.Rollback()
    '        Return False
    '    End Try

    '    tran.Commit()
    '    cn.Close()
    '    Return True
    'End Function

    Public Function fac_InsertaFacturaImportacion(ByRef FacturaHeader As fac_Ventas, ByRef FacturaDetalle As List(Of fac_VentasDetalle)) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = "",IdOld1 As Integer = 0, IdOld2 As Integer = 0, IdNew As Integer = 0

        Try

            Dim Serie As String = ""
            Dim UltimoNumero As Integer = 0
            Serie = GetSerie(FacturaHeader.IdPunto, FacturaHeader.IdTipoComprobante, Serie, UltimoNumero, True, tran)
            If UltimoNumero = 0 Then
                tran.Rollback()
                cn.Close()
                Return "No se pudo obtener el número del documento. No se han definido las series"
            End If

            If UltimoNumero = -1 Then
                tran.Rollback()
                cn.Close()
                Return "Ya no hay mas númeración disponible para facturar"
            End If


            FacturaHeader.IdComprobante = fd.ObtenerUltimoId("FAC_VENTAS", "IdComprobante") + 1
            FacturaHeader.Serie = Serie
            objTablas.fac_VentasInsert(FacturaHeader, tran)

            For Each detalle As fac_VentasDetalle In FacturaDetalle
                detalle.IdComprobante = FacturaHeader.IdComprobante
                objTablas.fac_VentasDetalleInsert(detalle, tran)
            Next

            'db.ExecuteNonQuery(tran, "inv_spInsertKardexVentas", FacturaHeader.IdComprobante)
            tran.Commit()

        Catch ex As Exception
            tran.Rollback()
            msj = ex.ToString
        Finally

            cn.Close()
        End Try
        Return msj
    End Function
    Public Function fac_ObtenerDetalleDocumento(ByVal Tabla As String, ByVal IdComprobante As Integer) As DataTable
        Dim sql = "select * from " & Tabla & " where IdComprobante=" & IdComprobante

        If Tabla.IndexOf("Cotizaciones") > 0 Or Tabla.IndexOf("cotizaciones") > 0 Then
            sql = "select * from " & Tabla & " where IdComprobante=" & IdComprobante
        End If

        If Tabla = "fac_VentasDetalle" Or Tabla = "fac_ventasdetalle" Then
            Return db.ExecuteDataSet("fac_spDetalleFactura", IdComprobante).Tables(0)
        Else
            Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        End If


    End Function
    Public Function fac_ObtenerDetalleDocumentoVentaPedido(ByVal IdComprobante As Integer) As DataTable
        Dim sql = "select IdProducto, Cantidad, Descripcion, IdPrecio, PrecioVenta=PrecioUnitario, PrecioUnitario"
        sql += " ,PrecioTotal = VentaAfecta+VentaExenta+VentaNoSujeta, VentaNeta, ValorIva=ValorIVA, TipoImpuesto"
        sql += " from Fac_ventasdetalle"
        sql += " where IdComprobante=" & IdComprobante


        ' ValorIva=ValorIVA

        Return db.ExecuteDataSet(CommandType.Text, Sql).Tables(0)
    End Function


    Public Function fac_ObtenerDetalleCotizacion(ByVal IdComprobante As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spDetalleCotizacion", IdComprobante).Tables(0)
    End Function
    Public Function fac_ObtenerDetalleVendedores(ByVal IdVendedor As Integer) As DataTable
        Dim sql As String = "select * from fac_vendedoresdetalle where idvendedor=" & IdVendedor
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function fac_ObtenerClientesVendedor(ByVal IdVendedor As Integer) As DataTable
        Dim sql As String =
            " select a.*, b.Nombre " _
            & "  from fac_VendedoresClientes a " _
            & "  inner join fac_Clientes b on a.idcliente = b.idcliente " _
           & "   where a.idvendedor = " & IdVendedor
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function fac_ExisteRCV() As Boolean
        Dim query As String = "SELECT CASE WHEN OBJECT_ID (N'fac_VendedoresClientes', N'U') IS NOT NULL THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END"
        Return Convert.ToBoolean(db.ExecuteScalar(CommandType.Text, query))
    End Function
    Public Function fac_rpt_RCV(ByVal IdVendedor As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spClientesVendedor", IdVendedor).Tables(0)
    End Function

    Public Function fac_GetVendedores() As DataTable
        Return db.ExecuteDataSet(CommandType.Text, "Select * from fac_vendedores where activo=1").Tables(0)
    End Function
	Public Function fac_GestionesPeriodos(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdCliente As String, ByVal IdVendedor As Integer, ByVal IdCotizacion As Integer) As DataTable
		Return db.ExecuteDataSet("fac_spListadoGestiones", Desde, Hasta, IdCliente, IdVendedor, IdCotizacion).Tables(0)
	End Function
	Public Function fac_GestionesProspectosPeriodos(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProspecto As Integer, ByVal IdVendedor As Integer, ByVal RepGestiones As Boolean) As DataTable
		Return db.ExecuteDataSet("fac_spListadoGestionesProspectos", Desde, Hasta, IdProspecto, IdVendedor, RepGestiones).Tables(0)
	End Function
	Public Function fac_GetCitasAll(ByVal Fecha As Date) As DataTable
        Return db.ExecuteDataSet(CommandType.Text, "select * from Appointments").Tables(0)
    End Function


    Public Function fac_ObtenerCotizacionPedido(ByVal IdComprobante As Integer, Tipo As Integer) As DataTable
        Dim sql As String = "select IdProducto, IdPrecio, Cantidad, Descripcion, "
        'If Tipo = 5 Then  'CREDITO FISCAL, este cambio se hizo a la ligera para el ISYC, deberá cambiarse posteriormente
        '    sql &= "PrecioVenta=PrecioVenta * 1.13, PrecioUnitario=PrecioUnitario*1.13, PrecioTotal=Round(PrecioTotal*1.13,2)"
        'Else
        '    sql &= "PrecioVenta, PrecioUnitario, PrecioTotal"
        'End If

        sql &= "PrecioVenta, PrecioUnitario, PrecioTotal"
        sql &= ", PorcDescuento, VentaNeta, ValorIva From fac_CotizacionesDetalle where IdComprobante = " & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function


    Public Function fac_ObtenerAfectaNC(ByVal IdComprobante As Integer) As DataTable
        Dim sql = ""
        sql = "select NumComprobanteVenta, IdComprobVenta, MontoAbonado from fac_NotasCredito where IdComprobante =" & IdComprobante
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function
    Public Function ContabilizarVentas(ByVal dFechaIni As Date, ByVal dFechaFin As Date, ByVal sTipoPartida As String, ByVal sUserId As String, ByVal sConcepto As String, ByVal IdSucursal As Integer) As String
        Return db.ExecuteScalar("fac_spContabilizarVentas", dFechaIni, dFechaFin, sTipoPartida, sUserId, sConcepto, IdSucursal)
    End Function
    Public Function GetSerie(ByVal IdPunto As Integer, ByVal IdTipoComprobante As Integer _
                             , ByRef Serie As String, ByRef UltimoNumero As Integer, ByVal GenerarNumero As Boolean, ByVal tran As DbTransaction) As String
        Dim sql As String = "select Serie, UltimoNumero, HastaNumero from adm_SeriesDocumentos Where EsActivo=1"
        sql &= String.Format(" and IdPunto={0} and IdTipoComprobante={1}", IdPunto, IdTipoComprobante)

        Dim dt As DataTable = db.ExecuteDataSet(tran, CommandType.Text, sql).Tables(0)

        If dt.Rows.Count < 1 Then  'NO SE HAN DEFINIDO LAS SERIES O CORRELATIVOS DE LOS DOCUMENTOS
            UltimoNumero = 0
            Return ""
        End If

        If dt.Rows(0).Item("UltimoNumero") + 1 > dt.Rows(0).Item("HastaNumero") Then 'ya no hay documentos disponibles para facturar
            UltimoNumero = -1
            Return ""
        End If

        Serie = dt.Rows(0).Item("Serie")
        If GenerarNumero Then
            UltimoNumero = dt.Rows(0).Item("UltimoNumero") + 1
            sql = "update adm_SeriesDocumentos set UltimoNumero = UltimoNumero + 1 Where EsActivo=1"
            sql &= " and IdPunto=" & IdPunto & " and IdTipoComprobante=" & IdTipoComprobante

            db.ExecuteDataSet(tran, CommandType.Text, sql)
        End If
        Return Serie

    End Function
    Public Function GetNumeroUnico(ByVal IdPunto As Integer, ByVal IdSucursal As Integer, ByRef UltimoNumeroUnico As Integer, ByVal tran As DbTransaction) As String
        Dim sSQL As String = "select  UltimoNumero, HastaNumero from adm_sucursales "
        sSQL &= String.Format(" where Idsucursal={0} ", IdSucursal)

        Dim dt As DataTable = db.ExecuteDataSet(tran, CommandType.Text, sSQL).Tables(0)

        If dt.Rows.Count < 1 Then  'NO SE HAN DEFINIDO LAS SERIES O CORRELATIVOS DE LOS DOCUMENTOS
            UltimoNumeroUnico = 0
            Return ""
        End If

        If dt.Rows(0).Item("UltimoNumero") + 1 > dt.Rows(0).Item("HastaNumero") Then 'ya no hay documentos disponibles para facturar
            UltimoNumeroUnico = -1
            Return ""
        End If


        UltimoNumeroUnico = dt.Rows(0).Item("UltimoNumero") + 1
        sSQL = "update adm_sucursales set UltimoNumero = UltimoNumero + 1 "
        sSQL &= " where  IdSucursal=" & IdSucursal

        db.ExecuteDataSet(tran, CommandType.Text, sSQL)

        Return "Ok"

    End Function

    Public Sub fac_TipoAplicacionComprobante(ByVal IdTipoComprobante As Integer, ByRef DocumentoDetallaIva As Boolean, ByRef TipoAplicacion As Integer, ByRef LimiteLineas As Integer)
        Dim sql As String = "select TipoAplicacion,DetallaIva, NumeroLineas from adm_TiposComprobante where IdTipoComprobante=" & IdTipoComprobante
        Dim dt As DataTable = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        TipoAplicacion = dt.Rows(0).Item("TipoAplicacion")
        LimiteLineas = dt.Rows(0).Item("NumeroLineas")
        If TipoAplicacion = 2 Then TipoAplicacion = -1 'es una devolución
        DocumentoDetallaIva = dt.Rows(0).Item("DetallaIva")
    End Sub
    Public Sub fac_ObtenerDatosEncabezadoPieTicket(ByRef DatosEncabezado As String, ByRef DatosPie As String, IdPunto As Integer)

        Dim sql As String = ""
        Dim dt As DataTable
        Try
            sql = "select Linea,Tipo from fac_DatosTicket where IdPunto = " & IdPunto 'Quien agregar el IdPunto despues de varios años, las soluciones se piensan desde el principio
            dt = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        Catch ex As Exception
            sql = "select Linea,Tipo from fac_DatosTicket"
            dt = db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
        End Try
        DatosEncabezado = ""
        DatosPie = ""
        For Each Fila As DataRow In dt.Rows
            If Fila.Item("Tipo") = "E" Then
                DatosEncabezado += Fila.Item("Linea").ToString() & Chr(10)
            Else
                DatosPie += Fila.Item("Linea").ToString() & Chr(10)
            End If
        Next
    End Sub
    Public Function getIdComprobante(ByVal iIdSucursal As Integer, ByVal iIdPunto As Integer, ByVal iIdTipoComprobante As Integer, ByVal sSerie As String, ByVal sNumero As String) As Integer
        Dim sSQL As String = ""
        sSQL = "select IdComprobante from fac_Ventas where IdSucursal = " & iIdSucursal
        sSQL &= " and IdPunto = " & iIdPunto
        sSQL &= " and IdTipoComprobante = " & iIdTipoComprobante
        sSQL &= " and Serie = '" & sSerie & "' and Numero = '" & sNumero & "'"
        'recupero la primera columna de la primer fila con executeScalar
        Return db.ExecuteScalar(CommandType.Text, sSQL)
    End Function
    Public Function fac_SaldoComprobante(ByVal IdCliente As String, ByVal Fecha As Date, ByVal IdComprobante As Integer) As DataTable
        Return db.ExecuteDataSet("cpc_spSaldosAbonosDocumento", IdCliente, Fecha, IdComprobante).Tables(0)
    End Function
    Function Verificar_Nota(ByVal iIdComprobante As Integer) As String
        Dim sSQL = "select Serie+' - '+Numero from fac_Ventas where idtipocomprobante in (8,11) and IdComprobanteNota = " & iIdComprobante
        Return db.ExecuteScalar(CommandType.Text, sSQL)
    End Function
    Function Verificar_Abonos(ByVal iIdComprobante As Integer) As String
        Dim sSQL = "select Fecha from cpc_AbonosDetalle d inner join cpc_Abonos a on d.IdComprobante = a.IdComprobante where IdComprobVenta = " & iIdComprobante _
                & " and d.MontoAbonado > 0"
        Return db.ExecuteScalar(CommandType.Text, sSQL)
    End Function
    Function Verificar_AbonosEventos(ByVal iIdComprobante As Integer) As String
        Dim sSQL = "select top 1 Fecha from eve_AbonosDetalle d inner join eve_Abonos a on d.IdComprobante = a.IdComprobante where IdComprobVenta = " & iIdComprobante
        Return db.ExecuteScalar(CommandType.Text, sSQL)
    End Function
    Public Function fac_ObtenerSerieDocumento(ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdTipoDoc As Integer) As String
        Dim sql As String = "select top 1 Serie from adm_seriesDocumentos where EsActivo=1 and Idsucursal = " & IdSucursal
        sql &= " and IdPunto = " & IdPunto
        sql &= " and IdTipoComprobante = " & IdTipoDoc
        sql &= " order by FechaAsignacion desc"
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function
    Public Function fac_AnulaDocumentoVenta(ByVal IdComprobante As Integer, ByVal MotivoAnulacion As String, ByVal FechaAnulacion As Date, ByVal AnuladoPor As String, ByVal eliminar As Integer, ByVal CreaDocumento As Integer, ByRef entFactura As fac_Ventas) As String
        Dim sqlborra As String = "delete from fac_ventas where IdComprobante = " & IdComprobante
        sqlborra += " and anulado =  1 "

        Dim sqlborra2 As String = "update fac_ventas set Modificadopor='" & AnuladoPor & "'"
        sqlborra2 += " where IdComprobante = " & IdComprobante

        Dim msj As String = ""

        If CreaDocumento = 0 Then
            If eliminar = 1 Then
                db.ExecuteNonQuery(CommandType.Text, sqlborra2)
                db.ExecuteNonQuery(CommandType.Text, sqlborra)
            Else
                db.ExecuteNonQuery("fac_spAnulacionDocumento", IdComprobante, MotivoAnulacion, FechaAnulacion, AnuladoPor)
            End If
        Else
            Dim cn As DbConnection = db.CreateConnection
            cn.Open()
            Dim tran As DbTransaction = cn.BeginTransaction()
            Try
                Dim Numero As Integer = CInt(entFactura.Numero)
                entFactura.IdComprobante = fd.ObtenerUltimoId("FAC_VENTAS", "IdComprobante") + 1
                entFactura.Numero = Numero.ToString.PadLeft(6, "0")

                Dim sSQL As String = ""
                Dim entTipo As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(entFactura.IdTipoComprobante)
                ' solo genera correlativo de formulario unico si, aplica al tipo de comprobante
                If entTipo.FormularioUnico Then
                    entFactura.NumFormularioUnico = GetObtenerCorrelativoFacturacionUnico(entFactura.IdPunto, entFactura.IdSucursal)
                    sSQL &= "update adm_sucursales set UltimoNumero = UltimoNumero+1 where IdSucursal= " & entFactura.IdSucursal
                    db.ExecuteDataSet(tran, CommandType.Text, sSQL)
                End If

                objTablas.fac_VentasInsert(entFactura, tran)

                sSQL = "update adm_SeriesDocumentos set UltimoNumero = UltimoNumero + 1 Where EsActivo=1"
                sSQL &= " and IdPunto=" & entFactura.IdPunto & " and IdTipoComprobante=" & entFactura.IdTipoComprobante
                db.ExecuteDataSet(tran, CommandType.Text, sSQL)



                tran.Commit()
            Catch ex As Exception
                tran.Rollback()
                msj = ex.ToString
            Finally

                cn.Close()
            End Try
        End If
        Return msj
    End Function



    Public Function fac_ObtenerDataVentas(ByVal FechaI As Date, ByVal FechaF As Date) As DataTable
        Dim sql As String = ""
        sql &= "select v.IdSucursal, v.IdPunto, v.IdComprobante, v.IdTipoComprobante, v.Serie, v.Numero, v.Fecha, v.Nombre, v.Nrc, v.Nit, v.OrdenCompra, "
        sql &= "v.TotalComprobante, v.TotalIva, v.TotalImpuesto1, v.TotalImpuesto2, v.TotalAfecto, v.TotalExento, v.TotalNeto, "
        sql &= "d.IdProducto, d.Cantidad, d.Descripcion, d.PrecioUnitario, d.VentaNoSujeta, d.VentaExenta, d.VentaAfecta, d.VentaNeta "
        sql &= "from fac_ventas v "
        sql &= "inner join fac_VentasDetalle d on v.IdComprobante = d.IdComprobante "
        sql &= "where V.Fecha >= @FechaI and V.Fecha < dateadd(dd,1,@FechaF) "

        Dim cmd As DbCommand = db.GetSqlStringCommand(sql)

        db.AddInParameter(cmd, "@FechaI", DbType.Date, FechaI)
        db.AddInParameter(cmd, "@FechaF", DbType.Date, FechaF)

        Return db.ExecuteDataSet(cmd).Tables(0)
    End Function
    Public Function fac_InsertarCliente(ByRef Header As fac_Clientes, ByRef Detalle As List(Of fac_ClientesPrecios), ByRef DetalleInfoAdicional As fac_ClientesAnexo) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj = ""
        Try
            objTablas.fac_ClientesInsert(Header, tran)

            For Each det As fac_ClientesPrecios In Detalle
                objTablas.fac_ClientesPreciosInsert(det, tran)
            Next

            If DetalleInfoAdicional.CreadoPor <> "" Then
                DetalleInfoAdicional.IdCliente = Header.IdCliente
                objTablas.fac_ClientesAnexoInsert(DetalleInfoAdicional, tran)
            End If
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message
        Finally
            cn.Close()
        End Try

        Return msj
    End Function
    Public Function fac_ActualizarCliente(ByRef Header As fac_Clientes, ByRef Detalle As List(Of fac_ClientesPrecios), ByRef DetalleInfoAdicional As fac_ClientesAnexo) As String

        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj = ""
        Try
            objTablas.fac_ClientesUpdate(Header, tran)

            Dim sSQL As String = "delete fac_ClientesPrecios where IdCliente= '" & Header.IdCliente & "'"
            sSQL += " ; delete fac_ClientesAnexo where idcliente='" & Header.IdCliente & "'"
            db.ExecuteNonQuery(tran, CommandType.Text, sSQL)

            If DetalleInfoAdicional.CreadoPor <> "" Then
                DetalleInfoAdicional.IdCliente = Header.IdCliente
                objTablas.fac_ClientesAnexoInsert(DetalleInfoAdicional, tran)
            End If

            For Each det As fac_ClientesPrecios In Detalle
                objTablas.fac_ClientesPreciosInsert(det, tran)
            Next


            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            msj = ex.Message()
        Finally
            cn.Close()
        End Try

        Return msj
    End Function
    Public Function fac_ObtienePrecioCliente(ByVal IdProducto As String, ByVal IdCliente As String) As Decimal
        Dim sql As String = "select PrecioUnitario from fac_ClientesPrecios  where IdProducto = '" & IdProducto & "' and IdCliente='"
        sql &= IdCliente & "'"
        Return db.ExecuteScalar(CommandType.Text, sql)
    End Function

    Public Function fac_ObtenerDatosEncabezadoPieTicket(ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet(CommandType.Text, "select Linea,Tipo from fac_DatosTicket where IdPunto=" & IdPunto).Tables(0)
    End Function


    Public Function fac_ObtenerTicketsParaCinta(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdPunto As Integer) As DataTable
        Dim sql As String = "select IdComprobante, IdPunto, Numero, Fecha, v.Nombre, Nrc, Nit, Vendedor = ve.Nombre, EsDevolucion, TotalExento, TotalAfecto, TotalComprobante, TotalPagado, FechaHoraCreacion from fac_Ventas v inner join fac_Vendedores ve on v.IdVendedor = ve.IdVendedor where Fecha >='"
        sql &= Format(Desde, "yyyyMMdd") & "' and fecha < dateadd(dd,1, '" & Format(Hasta, "yyyyMMdd") & "') and IdTipoComprobante = 10 and v.IdPunto=" & IdPunto
        Return db.ExecuteDataSet(CommandType.Text, sql).Tables(0)
    End Function



#Region "Reportes"
    Public Function fac_VentasPeriodo(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdCentro As String) As DataTable
        Return db.ExecuteDataSet("fac_spVentasPeriodo", Desde, Hasta, IdSucursal, IdPunto, IdCentro).Tables(0)
    End Function
    Public Function fac_VentasProductoCliente(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable

        Return db.ExecuteDataSet("fac_spVentasProductoCliente", Desde, Hasta, IdProducto, IdSucursal, IdPunto).Tables(0)
    End Function
    Public Function fac_VentasProductoConsolidado(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasProductoConsolidado", Desde, Hasta, IdProducto, IdSucursal, IdPunto).Tables(0)
    End Function


    Public Function fac_VentasProductosAnual(ByVal Anio As Integer, ByVal IdProducto As String, ByVal IdPunto As Integer, ByVal IdGrupo As Integer, ByVal TipoReporte As Integer, ByVal IdBodega As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasProductosAnual", Anio, IdProducto, IdPunto, IdGrupo, TipoReporte, IdBodega).Tables(0)
    End Function

    Public Function fac_VentasProductosAnual2(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdGrupo As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasProductosAnual2", Desde, Hasta, IdProducto, IdSucursal, IdPunto, IdGrupo).Tables(0)
    End Function
    Public Function fac_VentasProductosComparacionAnual(ByVal Anio1 As Integer, ByVal Anio2 As Integer, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasProductosComparacionAnual", Anio1, Anio2, IdProducto, IdSucursal, IdPunto).Tables(0)
    End Function
    Public Function fac_VentasProductosComparacionMensual(ByVal Anio1 As Integer, ByVal Mes1 As Integer, ByVal Anio2 As Integer, ByVal Mes2 As Integer, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasProductosComparacionMensual", Anio1, Mes1, Anio2, Mes2, IdProducto, IdSucursal, IdPunto).Tables(0)
    End Function
    Public Function fac_VentasTrimestrales(ByVal Mes1 As Integer, ByVal Anio1 As Integer, ByVal Mes2 As Integer, ByVal Anio2 As Integer _
       , ByVal Mes3 As Integer, ByVal Anio3 As Integer, ByVal Mes4 As Integer, ByVal Anio4 As Integer _
       , ByVal Mes5 As Integer, ByVal Anio5 As Integer, ByVal Mes6 As Integer, ByVal Anio6 As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasTrimestrales", Mes1, Anio1, Mes2, Anio2, Mes3, Anio3, Mes4, Anio4, Mes5, Anio5, Mes6, Anio6).Tables(0)
    End Function
    Public Function fac_VentasClienteDocumento(ByVal Desde As Date, ByVal Hasta As Date, ByVal ClienteDesde As String, ByVal ClienteHasta As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasClienteDocumento", Desde, Hasta, ClienteDesde, ClienteHasta, IdSucursal, IdPunto).Tables(0)
    End Function
    Public Function fac_rptCotizaciones(ByVal Desde As Date, ByVal Hasta As Date)
        Return db.ExecuteDataSet("fac_rptCotizaciones", Desde, Hasta).Tables(0)
    End Function
    Public Function fac_spRptRetenciones(ByVal Desde As Date, ByVal Hasta As Date)
        Return db.ExecuteDataSet("fac_spRptRetenciones", Desde, Hasta).Tables(0)
    End Function
    Public Function fac_CorteCaja(ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdVendedor As Integer, ByVal TotalArqueo As Decimal, ByVal TotalCheques As Decimal, ByVal TotalRemesas As Decimal, ByVal TotalVaucher As Decimal) As DataSet
        Return db.ExecuteDataSet("fac_spCorteCajaFinal", Fecha, IdSucursal, IdPunto, IdVendedor, TotalArqueo, TotalCheques, TotalRemesas, TotalVaucher)
    End Function
    Public Function fac_VerificaVentaAprobada(ByVal IdComprobante As Integer) As Integer
        Dim sSQL As String = "Select Count(*) from fac_ventas where IdComprobante =" & IdComprobante & " and VentaTransferida=1 "
        Return db.ExecuteScalar(CommandType.Text, fd.SiEsNulo(sSQL, 1))
    End Function
    Public Function fac_VerificaCorteCajaActivo(ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As Integer
        Dim sSQL As String = "Select top 1 isnull(convert(int,Cerrado),1) from con_cierrecaja where fecha <'" & Format(Fecha, "yyyyMMdd") & "'"
        sSQL += " and IdSucursal = " & IdSucursal
        sSQL += " and IdPunto = " & IdPunto
        sSQL += " order by fecha desc "
        Return db.ExecuteScalar(CommandType.Text, fd.SiEsNulo(sSQL, 1))
    End Function
    Public Function GetArqueoCaja(ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal Fecha As Date) As DataSet
        If IdSucursal = 0 Then
            Return db.ExecuteDataSet("fac_spObtieneArqueoCorte", fd.SiEsNulo(Today, Today), IdSucursal, IdPunto)
        Else
            Return db.ExecuteDataSet("fac_spObtieneArqueoCorte", fd.SiEsNulo(Fecha, Today), IdSucursal, IdPunto)
        End If
    End Function

    Public Function fac_CierraCorteCaja(ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal Saldo As Decimal, ByVal CreadoPor As String) As Integer
        Dim sqlBorra As String = "delete from con_CierreCaja where fecha =  '" & Fecha & "'"
        sqlBorra += " and IdSucursal = " & IdSucursal
        sqlBorra += " and IdPunto = " & IdPunto
        db.ExecuteNonQuery(CommandType.Text, sqlBorra)


        Dim sSQL As String = "INSERT INTO con_CierreCaja VALUES ('" & Fecha & "'," & IdSucursal & "," & IdPunto & "," & Saldo
        sSQL += ", 1, '" & CreadoPor & "',getdate())"

        Return db.ExecuteNonQuery(CommandType.Text, sSQL)
    End Function


    Public Function fac_VentasClienteProducto(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdCliente As String, ByVal IdProducto As String, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasClienteProducto", Desde, Hasta, IdCliente, IdProducto, IdSucursal, IdPunto).Tables(0)
    End Function
    Public Function fac_ProductoDespachado(ByVal IdRuta As Integer, ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spProductoDespachado", IdRuta, Desde, Hasta, IdSucursal, IdPunto).Tables(0)
    End Function
    Public Function fac_VentasComprobante(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipoComprobante As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasComprobante", Desde, Hasta, IdTipoComprobante, IdSucursal, IdPunto).Tables(0)
    End Function
    Public Function fac_RepVales(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVales", Desde, Hasta, IdSucursal, IdPunto).Tables(0)
    End Function

    Public Function fac_VentasFormaPago(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdFormaPago As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdVendedor As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasFormaPago", Desde, Hasta, IdFormaPago, IdSucursal, IdPunto, IdVendedor).Tables(0)
    End Function
    Public Function fac_VentasFormaPagoZonas(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdFormaPago As Integer, ByVal IdSucursal As Integer _
                                             , ByVal IdPunto As Integer, ByVal IdVendedor As Integer, ByVal IdDepartamento As String, ByVal IdMunicipio As String) As DataTable
        Return db.ExecuteDataSet("fac_spVentasFormaPagoZona", Desde, Hasta, IdFormaPago, IdSucursal, IdPunto, IdVendedor, IdDepartamento, IdMunicipio).Tables(0)
    End Function
    Public Function fac_VentasDevoluciones(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasDevoluciones", Desde, Hasta, IdPunto).Tables(0)
    End Function

    Public Function fac_VentasVendedor(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdVendedor As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal Detallado As Boolean) As DataTable
        Return db.ExecuteDataSet("fac_spVentasVendedor", Desde, Hasta, IdVendedor, IdSucursal, IdPunto, Detallado).Tables(0)
    End Function
    Public Function fac_VentasProductoConsolidado(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal snTop As Boolean) As DataTable
        Return db.ExecuteDataSet("fac_spVentasProductoConsolidado2", Desde, Hasta, IdSucursal, IdPunto, snTop).Tables(0)
    End Function
    Public Function fac_VentasClienteConsolidado(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal snTop As Boolean) As DataTable
        Return db.ExecuteDataSet("fac_spVentasClienteConsolidado", Desde, Hasta, IdSucursal, IdPunto, snTop).Tables(0)
    End Function
    Public Function fac_PedidosPeriodo(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spListaPedidos", Desde, Hasta, IdSucursal, IdPunto).Tables(0)
    End Function
    Public Function getLibroContribuyentes(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal IdSucursal As Integer) As DataSet
        Return db.ExecuteDataSet("fac_spLibroContribuyentes", Mes, Ejercicio, IdSucursal)
    End Function
    Public Function getLiquidacionImpuestos(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal IdSucursal As Integer) As DataSet
        Return db.ExecuteDataSet("fac_spLiquidacionImpuestos", Mes, Ejercicio, IdSucursal)
    End Function


    Public Function getLibroConsumidor(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spLibroConsumidor", Mes, Ejercicio, IdSucursal).Tables(0)
    End Function
    Public Function getLibroConsumidorDetalle(ByVal Mes As Integer, ByVal Ejercicio As Integer, ByVal IdSucursal As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spLibroConsumidorDetalle", Mes, Ejercicio, IdSucursal).Tables(0)
    End Function
    Public Function fac_DocumentosAnulados(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdTipo As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spDocumentosAnulados", Desde, Hasta, IdTipo, IdSucursal, IdPunto).Tables(0)
    End Function
    Public Function fac_CorteX(ByVal Fecha As DateTime, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spGenerarCorteX", Fecha, IdSucursal, IdPunto).Tables(0)
    End Function
    Public Function fac_CorteZ(ByVal Fecha As Date, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spGenerarCorteZ", Fecha, IdSucursal, IdPunto).Tables(0)
    End Function
    Public Function fac_CorteGranTotalZ(ByVal Mes As Integer, ByVal Anio As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spGenerarCorteTotal", Mes, Anio, IdSucursal, IdPunto).Tables(0)
    End Function
    Public Function fac_ventasVendedorUtilidad(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdVendedor As Integer, ByVal Tipo As Integer) As DataTable
        Return db.ExecuteDataSet("fac_spVentasVendedoresUtilidad", Desde, Hasta, IdBodega, IdSucursal, IdPunto, IdVendedor, Tipo).Tables(0)
    End Function
    Public Function fac_VentasClientesUtilidad(ByVal Desde As Date, ByVal Hasta As Date, ByVal IdBodega As Integer, ByVal IdSucursal As Integer, ByVal IdPunto As Integer, ByVal IdCliente As String) As DataTable
        Return db.ExecuteDataSet("fac_spVentasClientesUtilidad", Desde, Hasta, IdBodega, IdSucursal, IdPunto, IdCliente).Tables(0)
    End Function

#End Region


    'FUNCIONES PARA COPA AIRLINES
    Public Function fac_InsertaFacturaBoletos(ByRef FacturaHeader As fac_Ventas, ByRef FacturaDetalle As List(Of fac_VentasDetalle), ByVal AsignarNumero As Boolean) ', ByRef FacturaDetallePago As List(Of fac_VentasDetallePago), ByVal Tipo As String, ByVal AsignarNumero As Boolean) As String
        Dim cn As DbConnection = db.CreateConnection
        cn.Open()
        Dim tran As DbTransaction = cn.BeginTransaction()
        Dim msj As String = ""

        Try
            'TODO Calcula correlativos y los actualiza en las entidades
            'Dim Serie As String = "", SerieUnico As String = "", UltimoNumero As Integer = 0, UltimoNumeroUnico As Integer = 0
            'Dim entTipos As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(FacturaHeader.IdTipoComprobante)

            ''para los digitadores no correra el correlativo, solo para los demas usuarios, porque siguen procesando datos
            'If Not AsignarNumero Then
            '    UltimoNumero = CInt(FacturaHeader.Numero)
            'End If
            'Serie = GetSerie(FacturaHeader.IdPunto, FacturaHeader.IdTipoComprobante, Serie, UltimoNumero, AsignarNumero, tran)
            'If UltimoNumero = 0 Then
            '    tran.Rollback()
            '    cn.Close()
            '    Return "No se pudo obtener el número del documento. No se han definido las series"
            'End If
            'If UltimoNumero = -1 Then
            '    tran.Rollback()
            '    cn.Close()
            '    Return "Ya no hay mas númeración disponible para facturar"
            'End If

            'If entTipos.FormularioUnico Then
            '    SerieUnico = GetNumeroUnico(FacturaHeader.IdPunto, FacturaHeader.IdSucursal, UltimoNumeroUnico, tran)

            '    If UltimoNumeroUnico = -1 Then
            '        tran.Rollback()
            '        cn.Close()
            '        Return "Ya no hay mas númeración disponible para facturar con Formulario Único"
            '    End If
            'End If

            'FacturaHeader.Numero = UltimoNumero.ToString.PadLeft(6, "0")
            'If entTipos.FormularioUnico Then
            '    FacturaHeader.NumFormularioUnico = UltimoNumeroUnico.ToString.PadLeft(6, "0")
            'End If

            'FacturaHeader.Serie = Serie

            FacturaHeader.IdComprobante = fd.ObtenerUltimoId("FAC_VENTAS", "IdComprobante") + 1

            objTablas.fac_VentasInsert(FacturaHeader, tran)

            For Each detalle As fac_VentasDetalle In FacturaDetalle
                detalle.IdComprobante = FacturaHeader.IdComprobante

                objTablas.fac_VentasDetalleInsert(detalle, tran)
                Dim sql As String
                If detalle.DescuentoAutorizadoPor <> "" Then
                    sql = "update fac_AutorizacionesDescuentos set IdComprobante =" & detalle.IdComprobante
                    'sql += ", IdProducto='" & detalle.IdProducto & "'"
                    sql += ", IdDetalle=" & detalle.IdDetalle
                    sql += " where Autorizacion ='" & detalle.DescuentoAutorizadoPor & "'"
                    db.ExecuteNonQuery(CommandType.Text, sql)
                End If
            Next

            db.ExecuteNonQuery(tran, "inv_spInsertKardexVentas", FacturaHeader.IdComprobante)
            db.ExecuteNonQuery(tran, "inv_spRenumerarMovimientos_Existencia", FacturaHeader.IdComprobante, FacturaHeader.IdTipoComprobante)
            db.ExecuteNonQuery(tran, "inv_spRecalculoCosto", FacturaHeader.IdComprobante, FacturaHeader.IdTipoComprobante)


            tran.Commit()

        Catch ex As Exception
            tran.Rollback()
            msj = ex.ToString
        Finally

            cn.Close()
        End Try
        Return msj

    End Function

    Public Function fac_ConsultaFacturacionBoletos(ByVal Numero As String, ByVal Fecha As Date) As DataTable
        Return db.ExecuteDataSet("fac_spConsultaFacturacionBoletos", Numero, Fecha).Tables(0)
    End Function
End Class
