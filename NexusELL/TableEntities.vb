﻿Public Class TableEntities

#Region "Appointments"
    Public Class Appointments
        Private _UniqueID As System.Int32
        Public Property UniqueID() As System.Int32
            Get
                Return _UniqueID
            End Get
            Set(ByVal value As System.Int32)
                _UniqueID = value
            End Set
        End Property

        Private _Type As System.Int32
        Public Property Type() As System.Int32
            Get
                Return _Type
            End Get
            Set(ByVal value As System.Int32)
                _Type = value
            End Set
        End Property

        Private _StartDate As Nullable(Of DateTime)
        Public Property StartDate() As Nullable(Of DateTime)
            Get
                Return _StartDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _StartDate = value
            End Set
        End Property

        Private _EndDate As Nullable(Of DateTime)
        Public Property EndDate() As Nullable(Of DateTime)
            Get
                Return _EndDate
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _EndDate = value
            End Set
        End Property

        Private _AllDay As System.Boolean
        Public Property AllDay() As System.Boolean
            Get
                Return _AllDay
            End Get
            Set(ByVal value As System.Boolean)
                _AllDay = value
            End Set
        End Property

        Private _Subject As System.String = ""
        Public Property Subject() As System.String
            Get
                Return _Subject
            End Get
            Set(ByVal value As System.String)
                _Subject = value
            End Set
        End Property

        Private _Location As System.String = ""
        Public Property Location() As System.String
            Get
                Return _Location
            End Get
            Set(ByVal value As System.String)
                _Location = value
            End Set
        End Property

        Private _Description As System.String = ""
        Public Property Description() As System.String
            Get
                Return _Description
            End Get
            Set(ByVal value As System.String)
                _Description = value
            End Set
        End Property

        Private _Status As System.Int32
        Public Property Status() As System.Int32
            Get
                Return _Status
            End Get
            Set(ByVal value As System.Int32)
                _Status = value
            End Set
        End Property

        Private _Label As System.Int32
        Public Property Label() As System.Int32
            Get
                Return _Label
            End Get
            Set(ByVal value As System.Int32)
                _Label = value
            End Set
        End Property

        Private _ResourceID As System.Int32
        Public Property ResourceID() As System.Int32
            Get
                Return _ResourceID
            End Get
            Set(ByVal value As System.Int32)
                _ResourceID = value
            End Set
        End Property

        Private _ResourceIDs As System.String = ""
        Public Property ResourceIDs() As System.String
            Get
                Return _ResourceIDs
            End Get
            Set(ByVal value As System.String)
                _ResourceIDs = value
            End Set
        End Property

        Private _ReminderInfo As System.String = ""
        Public Property ReminderInfo() As System.String
            Get
                Return _ReminderInfo
            End Get
            Set(ByVal value As System.String)
                _ReminderInfo = value
            End Set
        End Property

        Private _RecurrenceInfo As System.String = ""
        Public Property RecurrenceInfo() As System.String
            Get
                Return _RecurrenceInfo
            End Get
            Set(ByVal value As System.String)
                _RecurrenceInfo = value
            End Set
        End Property

        Private _TimeZoneId As System.String = ""
        Public Property TimeZoneId() As System.String
            Get
                Return _TimeZoneId
            End Get
            Set(ByVal value As System.String)
                _TimeZoneId = value
            End Set
        End Property

        Private _CustomField1 As System.String = ""
        Public Property CustomField1() As System.String
            Get
                Return _CustomField1
            End Get
            Set(ByVal value As System.String)
                _CustomField1 = value
            End Set
        End Property

        Private _CustomField2 As System.Int32
        Public Property CustomField2() As System.Int32
            Get
                Return _CustomField2
            End Get
            Set(ByVal value As System.Int32)
                _CustomField2 = value
            End Set
        End Property

    End Class
#End Region
#Region "adm_Parametros"
    Public Class adm_Parametros
        Private _NombreFirmante1 As System.String = ""
        Public Property NombreFirmante1() As System.String
            Get
                Return _NombreFirmante1
            End Get
            Set(ByVal value As System.String)
                _NombreFirmante1 = value
            End Set
        End Property

        Private _CargoFirmante1 As System.String = ""
        Public Property CargoFirmante1() As System.String
            Get
                Return _CargoFirmante1
            End Get
            Set(ByVal value As System.String)
                _CargoFirmante1 = value
            End Set
        End Property

        Private _Telefonos As System.String = ""
        Public Property Telefonos() As System.String
            Get
                Return _Telefonos
            End Get
            Set(ByVal value As System.String)
                _Telefonos = value
            End Set
        End Property

        Private _NombreFirmante2 As System.String = ""
        Public Property NombreFirmante2() As System.String
            Get
                Return _NombreFirmante2
            End Get
            Set(ByVal value As System.String)
                _NombreFirmante2 = value
            End Set
        End Property

        Private _CargoFirmante2 As System.String = ""
        Public Property CargoFirmante2() As System.String
            Get
                Return _CargoFirmante2
            End Get
            Set(ByVal value As System.String)
                _CargoFirmante2 = value
            End Set
        End Property

        Private _NombreFirmante3 As System.String = ""
        Public Property NombreFirmante3() As System.String
            Get
                Return _NombreFirmante3
            End Get
            Set(ByVal value As System.String)
                _NombreFirmante3 = value
            End Set
        End Property

        Private _CargoFirmante3 As System.String = ""
        Public Property CargoFirmante3() As System.String
            Get
                Return _CargoFirmante3
            End Get
            Set(ByVal value As System.String)
                _CargoFirmante3 = value
            End Set
        End Property

        Private _NombreFirmante4 As System.String = ""
        Public Property NombreFirmante4() As System.String
            Get
                Return _NombreFirmante4
            End Get
            Set(ByVal value As System.String)
                _NombreFirmante4 = value
            End Set
        End Property

        Private _CargoFirmante4 As System.String = ""
        Public Property CargoFirmante4() As System.String
            Get
                Return _CargoFirmante4
            End Get
            Set(ByVal value As System.String)
                _CargoFirmante4 = value
            End Set
        End Property

        Private _NombreFirmante5 As System.String = ""
        Public Property NombreFirmante5() As System.String
            Get
                Return _NombreFirmante5
            End Get
            Set(ByVal value As System.String)
                _NombreFirmante5 = value
            End Set
        End Property

        Private _CargoFirmante5 As System.String = ""
        Public Property CargoFirmante5() As System.String
            Get
                Return _CargoFirmante5
            End Get
            Set(ByVal value As System.String)
                _CargoFirmante5 = value
            End Set
        End Property

        Private _NombreFirmante6 As System.String = ""
        Public Property NombreFirmante6() As System.String
            Get
                Return _NombreFirmante6
            End Get
            Set(ByVal value As System.String)
                _NombreFirmante6 = value
            End Set
        End Property

        Private _CargoFirmante6 As System.String = ""
        Public Property CargoFirmante6() As System.String
            Get
                Return _CargoFirmante6
            End Get
            Set(ByVal value As System.String)
                _CargoFirmante6 = value
            End Set
        End Property

        Private _DescMoneda As System.String = ""
        Public Property DescMoneda() As System.String
            Get
                Return _DescMoneda
            End Get
            Set(ByVal value As System.String)
                _DescMoneda = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _Domicilio As System.String = ""
        Public Property Domicilio() As System.String
            Get
                Return _Domicilio
            End Get
            Set(ByVal value As System.String)
                _Domicilio = value
            End Set
        End Property

        Private _Municipio As System.String = ""
        Public Property Municipio() As System.String
            Get
                Return _Municipio
            End Get
            Set(ByVal value As System.String)
                _Municipio = value
            End Set
        End Property

        Private _Departamento As System.String = ""
        Public Property Departamento() As System.String
            Get
                Return _Departamento
            End Get
            Set(ByVal value As System.String)
                _Departamento = value
            End Set
        End Property

        Private _TipoContribuyente As System.Int16
        Public Property TipoContribuyente() As System.Int16
            Get
                Return _TipoContribuyente
            End Get
            Set(ByVal value As System.Int16)
                _TipoContribuyente = value
            End Set
        End Property

        Private _EsRetenedor As System.Boolean
        Public Property EsRetenedor() As System.Boolean
            Get
                Return _EsRetenedor
            End Get
            Set(ByVal value As System.Boolean)
                _EsRetenedor = value
            End Set
        End Property

        Private _NrcEmpresa As System.String = ""
        Public Property NrcEmpresa() As System.String
            Get
                Return _NrcEmpresa
            End Get
            Set(ByVal value As System.String)
                _NrcEmpresa = value
            End Set
        End Property

        Private _NitEmpresa As System.String = ""
        Public Property NitEmpresa() As System.String
            Get
                Return _NitEmpresa
            End Get
            Set(ByVal value As System.String)
                _NitEmpresa = value
            End Set
        End Property

        Private _FechaCierre As Nullable(Of DateTime)
        Public Property FechaCierre() As Nullable(Of DateTime)
            Get
                Return _FechaCierre
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaCierre = value
            End Set
        End Property

        Private _FechaLimite As Nullable(Of DateTime)
        Public Property FechaLimite() As Nullable(Of DateTime)
            Get
                Return _FechaLimite
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaLimite = value
            End Set
        End Property

        Private _Ejercicio As System.Int32
        Public Property Ejercicio() As System.Int32
            Get
                Return _Ejercicio
            End Get
            Set(ByVal value As System.Int32)
                _Ejercicio = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _IdBodega As System.String = ""
        Public Property IdBodega() As System.String
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.String)
                _IdBodega = value
            End Set
        End Property

        Private _NumeroPatronal As System.String = ""
        Public Property NumeroPatronal() As System.String
            Get
                Return _NumeroPatronal
            End Get
            Set(ByVal value As System.String)
                _NumeroPatronal = value
            End Set
        End Property

        Private _ActividadEconomica As System.String = ""
        Public Property ActividadEconomica() As System.String
            Get
                Return _ActividadEconomica
            End Get
            Set(ByVal value As System.String)
                _ActividadEconomica = value
            End Set
        End Property

        Private _PorcIVA As System.Double
        Public Property PorcIVA() As System.Double
            Get
                Return _PorcIVA
            End Get
            Set(ByVal value As System.Double)
                _PorcIVA = value
            End Set
        End Property

        Private _PorcReten As System.Double
        Public Property PorcReten() As System.Decimal
            Get
                Return _PorcReten
            End Get
            Set(ByVal value As System.Decimal)
                _PorcReten = value
            End Set
        End Property

        Private _PorcPercep As System.Double
        Public Property PorcPercep() As System.Decimal
            Get
                Return _PorcPercep
            End Get
            Set(ByVal value As System.Decimal)
                _PorcPercep = value
            End Set
        End Property

        Private _TipoNumeracion As System.Int32
        Public Property TipoNumeracionPartidas() As System.Int32
            Get
                Return _TipoNumeracion
            End Get
            Set(ByVal value As System.Int32)
                _TipoNumeracion = value
            End Set
        End Property

        Private _GuardarDesbalance As System.Boolean
        Public Property GuardarDesbalance() As System.Boolean
            Get
                Return _GuardarDesbalance
            End Get
            Set(ByVal value As System.Boolean)
                _GuardarDesbalance = value
            End Set
        End Property

        Private _UtilizarFormularioUnico As System.Boolean
        Public Property UtilizarFormularioUnico() As System.Boolean
            Get
                Return _UtilizarFormularioUnico
            End Get
            Set(ByVal value As System.Boolean)
                _UtilizarFormularioUnico = value
            End Set
        End Property

        Private _IdCuentaCaja As System.String = ""
        Public Property IdCuentaCaja() As System.String
            Get
                Return _IdCuentaCaja
            End Get
            Set(ByVal value As System.String)
                _IdCuentaCaja = value
            End Set
        End Property

        Private _IdCuentaCredito1 As System.String = ""
        Public Property IdCuentaCredito1() As System.String
            Get
                Return _IdCuentaCredito1
            End Get
            Set(ByVal value As System.String)
                _IdCuentaCredito1 = value
            End Set
        End Property

        Private _IdCuentaCredito2 As System.String = ""
        Public Property IdCuentaCredito2() As System.String
            Get
                Return _IdCuentaCredito2
            End Get
            Set(ByVal value As System.String)
                _IdCuentaCredito2 = value
            End Set
        End Property

        Private _IdCuentaDebito1 As System.String = ""
        Public Property IdCuentaDebito1() As System.String
            Get
                Return _IdCuentaDebito1
            End Get
            Set(ByVal value As System.String)
                _IdCuentaDebito1 = value
            End Set
        End Property

        Private _IdCuentaDebito2 As System.String = ""
        Public Property IdCuentaDebito2() As System.String
            Get
                Return _IdCuentaDebito2
            End Get
            Set(ByVal value As System.String)
                _IdCuentaDebito2 = value
            End Set
        End Property

        Private _IdCuentaRetencion1 As System.String = ""
        Public Property IdCuentaRetencion1() As System.String
            Get
                Return _IdCuentaRetencion1
            End Get
            Set(ByVal value As System.String)
                _IdCuentaRetencion1 = value
            End Set
        End Property

        Private _IdCuentaRetencion2 As System.String = ""
        Public Property IdCuentaRetencion2() As System.String
            Get
                Return _IdCuentaRetencion2
            End Get
            Set(ByVal value As System.String)
                _IdCuentaRetencion2 = value
            End Set
        End Property


        Private _IdCuentaPercepcion As System.String = ""
        Public Property IdCuentaPercepcion() As System.String
            Get
                Return _IdCuentaPercepcion
            End Get
            Set(ByVal value As System.String)
                _IdCuentaPercepcion = value
            End Set
        End Property

        Private _IdCuentaRetencionVentas As System.String = ""
        Public Property IdCuentaRetencionVentas() As System.String
            Get
                Return _IdCuentaRetencionVentas
            End Get
            Set(ByVal value As System.String)
                _IdCuentaRetencionVentas = value
            End Set
        End Property

        Private _IdCuentaPercepcionVentas As System.String = ""
        Public Property IdCuentaPercepcionVentas() As System.String
            Get
                Return _IdCuentaPercepcionVentas
            End Get
            Set(ByVal value As System.String)
                _IdCuentaPercepcionVentas = value
            End Set
        End Property
        Private _IdCuentaImportacion As System.String = ""
        Public Property IdCuentaImportacion() As System.String
            Get
                Return _IdCuentaImportacion
            End Get
            Set(ByVal value As System.String)
                _IdCuentaImportacion = value
            End Set
        End Property
        Private _IdCuentaExentoLiquidacion As System.String = ""
        Public Property IdCuentaExentoLiquidacion() As System.String
            Get
                Return _IdCuentaExentoLiquidacion
            End Get
            Set(ByVal value As System.String)
                _IdCuentaExentoLiquidacion = value
            End Set
        End Property


        Private _IdCuentaComisionLiquidacion As System.String = ""
        Public Property IdCuentaComisionLiquidacion() As System.String
            Get
                Return _IdCuentaComisionLiquidacion
            End Get
            Set(ByVal value As System.String)
                _IdCuentaComisionLiquidacion = value
            End Set
        End Property
        Private _IdCuentaPorCobrar As System.String = ""
        Public Property IdCuentaPorCobrar() As System.String
            Get
                Return _IdCuentaPorCobrar
            End Get
            Set(ByVal value As System.String)
                _IdCuentaPorCobrar = value
            End Set
        End Property

        Private _FacturarMenosCosto As System.Boolean
        Public Property FacturarMenosCosto() As System.Boolean
            Get
                Return _FacturarMenosCosto
            End Get
            Set(ByVal value As System.Boolean)
                _FacturarMenosCosto = value
            End Set
        End Property

        Private _IdTipoPrecio As System.Int32
        Public Property IdTipoPrecio() As System.Int32
            Get
                Return _IdTipoPrecio
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoPrecio = value
            End Set
        End Property

        Private _TipoPrecio As System.Int32
        Public Property TipoPrecio() As System.Int32
            Get
                Return _TipoPrecio
            End Get
            Set(ByVal value As System.Int32)
                _TipoPrecio = value
            End Set
        End Property
        Private _IdTipoPartidaCompras As System.String = ""
        Public Property IdTipoPartidaCompras() As System.String
            Get
                Return _IdTipoPartidaCompras
            End Get
            Set(ByVal value As System.String)
                _IdTipoPartidaCompras = value
            End Set
        End Property

        Private _ContabilizarLineaCompras As System.Boolean
        Public Property ContabilizarLineaCompras() As System.Boolean
            Get
                Return _ContabilizarLineaCompras
            End Get
            Set(ByVal value As System.Boolean)
                _ContabilizarLineaCompras = value
            End Set
        End Property
        Private _AlertaMinimos As System.Boolean
        Public Property AlertaMinimos() As System.Boolean
            Get
                Return _AlertaMinimos
            End Get
            Set(ByVal value As System.Boolean)
                _AlertaMinimos = value
            End Set
        End Property
        Private _AutorizarCheques As System.Boolean
        Public Property AutorizarCheques() As System.Boolean
            Get
                Return _AutorizarCheques
            End Get
            Set(ByVal value As System.Boolean)
                _AutorizarCheques = value
            End Set
        End Property
        Private _VistaPreviaFacturar As System.Boolean
        Public Property VistaPreviaFacturar() As System.Boolean
            Get
                Return _VistaPreviaFacturar
            End Get
            Set(ByVal value As System.Boolean)
                _VistaPreviaFacturar = value
            End Set
        End Property
        Private _ValidarExistencias As System.Boolean
        Public Property ValidarExistencias() As System.Boolean
            Get
                Return _ValidarExistencias
            End Get
            Set(ByVal value As System.Boolean)
                _ValidarExistencias = value
            End Set
        End Property

        Private _SeccionarFormaPago As System.Boolean
        Public Property SeccionarFormaPago() As System.Boolean
            Get
                Return _SeccionarFormaPago
            End Get
            Set(ByVal value As System.Boolean)
                _SeccionarFormaPago = value
            End Set
        End Property

        Private _BloquearFechaFacturar As System.Boolean
        Public Property BloquearFechaFacturar() As System.Boolean
            Get
                Return _BloquearFechaFacturar
            End Get
            Set(ByVal value As System.Boolean)
                _BloquearFechaFacturar = value
            End Set
        End Property

        Private _LogoEmpresa As System.Byte()
        Public Property LogoEmpresa() As System.Byte()
            Get
                Return _LogoEmpresa
            End Get
            Set(ByVal value As System.Byte())
                _LogoEmpresa = value
            End Set
        End Property

        Private _MesCierreAnual As System.Int32
        Public Property MesCierreAnual() As System.Int32
            Get
                Return _MesCierreAnual
            End Get
            Set(ByVal value As System.Int32)
                _MesCierreAnual = value
            End Set
        End Property

        Private _LimiteMontoDirectoEfectivo As System.Decimal
        Public Property LimiteMontoDirectoEfectivo() As System.Decimal
            Get
                Return _LimiteMontoDirectoEfectivo
            End Get
            Set(ByVal value As System.Decimal)
                _LimiteMontoDirectoEfectivo = value
            End Set
        End Property

        Private _LimiteMontoAcumuladoEfectivo As System.Decimal
        Public Property LimiteMontoAcumuladoEfectivo() As System.Decimal
            Get
                Return _LimiteMontoAcumuladoEfectivo
            End Get
            Set(ByVal value As System.Decimal)
                _LimiteMontoAcumuladoEfectivo = value
            End Set
        End Property

        Private _LimiteMontoDirectoOtros As System.Decimal
        Public Property LimiteMontoDirectoOtros() As System.Decimal
            Get
                Return _LimiteMontoDirectoOtros
            End Get
            Set(ByVal value As System.Decimal)
                _LimiteMontoDirectoOtros = value
            End Set
        End Property

        Private _LimiteMontoAcumuladoOtros As System.Decimal
        Public Property LimiteMontoAcumuladoOtros() As System.Decimal
            Get
                Return _LimiteMontoAcumuladoOtros
            End Get
            Set(ByVal value As System.Decimal)
                _LimiteMontoAcumuladoOtros = value
            End Set
        End Property

        Private _DiasMaximoCredito As System.Int32
        Public Property DiasMaximoCredito() As System.Int32
            Get
                Return _DiasMaximoCredito
            End Get
            Set(ByVal value As System.Int32)
                _DiasMaximoCredito = value
            End Set
        End Property
    End Class
#End Region

#Region "adm_Usuarios"
    Public Class adm_Usuarios
        Private _IdUsuario As System.String = ""
        Public Property IdUsuario() As System.String
            Get
                Return _IdUsuario
            End Get
            Set(ByVal value As System.String)
                _IdUsuario = value
            End Set
        End Property

        Private _Clave As System.String = ""
        Public Property Clave() As System.String
            Get
                Return _Clave
            End Get
            Set(ByVal value As System.String)
                _Clave = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Apellidos As System.String = ""
        Public Property Apellidos() As System.String
            Get
                Return _Apellidos
            End Get
            Set(ByVal value As System.String)
                _Apellidos = value
            End Set
        End Property

        Private _FechaCreacion As Nullable(Of DateTime)
        Public Property FechaCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaCreacion = value
            End Set
        End Property

        Private _FechaVencimiento As Nullable(Of DateTime)
        Public Property FechaVencimiento() As Nullable(Of DateTime)
            Get
                Return _FechaVencimiento
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaVencimiento = value
            End Set
        End Property
        Private _FechaUltCambioClave As Nullable(Of DateTime)
        Public Property FechaUltCambioClave() As Nullable(Of DateTime)
            Get
                Return _FechaUltCambioClave
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaUltCambioClave = value
            End Set
        End Property
        Private _VigenciaClave As System.Int32
        Public Property VigenciaClave() As System.Int32
            Get
                Return _VigenciaClave
            End Get
            Set(ByVal value As System.Int32)
                _VigenciaClave = value
            End Set
        End Property
        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property
        Private _Estado As System.Boolean
        Public Property Estado() As System.Boolean
            Get
                Return _Estado
            End Get
            Set(ByVal value As System.Boolean)
                _Estado = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdTipoPrecio As System.Int32
        Public Property IdTipoPrecio() As System.Int32
            Get
                Return _IdTipoPrecio
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoPrecio = value
            End Set
        End Property

        Private _CambiarPrecios As Boolean
        Public Property CambiarPrecios() As Boolean
            Get
                Return _CambiarPrecios
            End Get
            Set(ByVal value As Boolean)
                _CambiarPrecios = value
            End Set
        End Property

        Private _AccesoProductos As System.Boolean
        Public Property AccesoProductos() As System.Boolean
            Get
                Return _AccesoProductos
            End Get
            Set(ByVal value As System.Boolean)
                _AccesoProductos = value
            End Set
        End Property


        Private _AutorizaCredito As System.Boolean
        Public Property AutorizaCredito() As System.Boolean
            Get
                Return _AutorizaCredito
            End Get
            Set(ByVal value As System.Boolean)
                _AutorizaCredito = value
            End Set
        End Property

        Private _TodasLasSucursales As System.Boolean
        Public Property TodasLasSucursales() As System.Boolean
            Get
                Return _TodasLasSucursales
            End Get
            Set(ByVal value As System.Boolean)
                _TodasLasSucursales = value
            End Set
        End Property

        Private _AutorizarDescuentos As System.Boolean
        Public Property AutorizarDescuentos() As System.Boolean
            Get
                Return _AutorizarDescuentos
            End Get
            Set(ByVal value As System.Boolean)
                _AutorizarDescuentos = value
            End Set
        End Property
        Private _AutorizarRevertir As System.Boolean
        Public Property AutorizarRevertir() As System.Boolean
            Get
                Return _AutorizarRevertir
            End Get
            Set(ByVal value As System.Boolean)
                _AutorizarRevertir = value
            End Set
        End Property
        Private _BloquearBodega As System.Boolean
        Public Property BloquearBodega() As System.Boolean
            Get
                Return _BloquearBodega
            End Get
            Set(ByVal value As System.Boolean)
                _BloquearBodega = value
            End Set
        End Property



        Private _DesdeDescuentos As System.Decimal
        Public Property DesdeDescuentos() As System.Decimal
            Get
                Return _DesdeDescuentos
            End Get
            Set(ByVal value As System.Decimal)
                _DesdeDescuentos = value
            End Set
        End Property

        Private _HastaDescuentos As System.Decimal
        Public Property HastaDescuentos() As System.Decimal
            Get
                Return _HastaDescuentos
            End Get
            Set(ByVal value As System.Decimal)
                _HastaDescuentos = value
            End Set
        End Property
        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _IdVendedor As System.Int32
        Public Property IdVendedor() As System.Int32
            Get
                Return _IdVendedor
            End Get
            Set(ByVal value As System.Int32)
                _IdVendedor = value
            End Set
        End Property

    End Class
#End Region
#Region "adm_Libros"
    Public Class adm_Libros
        Private _IdLibro As System.Int32
        Public Property IdLibro() As System.Int32
            Get
                Return _IdLibro
            End Get
            Set(ByVal value As System.Int32)
                _IdLibro = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "adm_SeriesDocumentos"
    Public Class adm_SeriesDocumentos
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _IdTipoComprobante As System.Int32
        Public Property IdTipoComprobante() As System.Int32
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _Serie As System.String = ""
        Public Property Serie() As System.String
            Get
                Return _Serie
            End Get
            Set(ByVal value As System.String)
                _Serie = value
            End Set
        End Property
        Private _Resolucion As System.String = ""
        Public Property Resolucion() As System.String
            Get
                Return _Resolucion
            End Get
            Set(ByVal value As System.String)
                _Resolucion = value
            End Set
        End Property

        Private _FechaAsignacion As Nullable(Of DateTime)
        Public Property FechaAsignacion() As Nullable(Of DateTime)
            Get
                Return _FechaAsignacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaAsignacion = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _UltimoNumero As System.Int32
        Public Property UltimoNumero() As System.Int32
            Get
                Return _UltimoNumero
            End Get
            Set(ByVal value As System.Int32)
                _UltimoNumero = value
            End Set
        End Property

        Private _DesdeNumero As System.Int32
        Public Property DesdeNumero() As System.Int32
            Get
                Return _DesdeNumero
            End Get
            Set(ByVal value As System.Int32)
                _DesdeNumero = value
            End Set
        End Property

        Private _HastaNumero As System.Int32
        Public Property HastaNumero() As System.Int32
            Get
                Return _HastaNumero
            End Get
            Set(ByVal value As System.Int32)
                _HastaNumero = value
            End Set
        End Property

        Private _EsActivo As System.Boolean
        Public Property EsActivo() As System.Boolean
            Get
                Return _EsActivo
            End Get
            Set(ByVal value As System.Boolean)
                _EsActivo = value
            End Set
        End Property

    End Class
#End Region


#Region "adm_Sucursales"
    Public Class adm_Sucursales
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Responsable As System.String = ""
        Public Property Responsable() As System.String
            Get
                Return _Responsable
            End Get
            Set(ByVal value As System.String)
                _Responsable = value
            End Set
        End Property

        Private _Telefonos As System.String = ""
        Public Property Telefonos() As System.String
            Get
                Return _Telefonos
            End Get
            Set(ByVal value As System.String)
                _Telefonos = value
            End Set
        End Property

        Private _UltimoNumero As System.Int32
        Public Property UltimoNumero() As System.Int32
            Get
                Return _UltimoNumero
            End Get
            Set(ByVal value As System.Int32)
                _UltimoNumero = value
            End Set
        End Property

        Private _DesdeNumero As System.Int32
        Public Property DesdeNumero() As System.Int32
            Get
                Return _DesdeNumero
            End Get
            Set(ByVal value As System.Int32)
                _DesdeNumero = value
            End Set
        End Property

        Private _HastaNumero As System.Int32
        Public Property HastaNumero() As System.Int32
            Get
                Return _HastaNumero
            End Get
            Set(ByVal value As System.Int32)
                _HastaNumero = value
            End Set
        End Property
        Private _IdCuentaCostos As System.String = ""
        Public Property IdCuentaCostos() As System.String
            Get
                Return _IdCuentaCostos
            End Get
            Set(ByVal value As System.String)
                _IdCuentaCostos = value
            End Set
        End Property

        Private _IdCuentaCaja As System.String = ""
        Public Property IdCuentaCaja() As System.String
            Get
                Return _IdCuentaCaja
            End Get
            Set(ByVal value As System.String)
                _IdCuentaCaja = value
            End Set
        End Property

    End Class
#End Region

#Region "ban_ConciliacionesDetalle"
    Public Class ban_ConciliacionesDetalle
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdCuentaBancaria As System.Int32
        Public Property IdCuentaBancaria() As System.Int32
            Get
                Return _IdCuentaBancaria
            End Get
            Set(ByVal value As System.Int32)
                _IdCuentaBancaria = value
            End Set
        End Property

        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Cheque As System.Boolean
        Public Property Cheque() As System.Boolean
            Get
                Return _Cheque
            End Get
            Set(ByVal value As System.Boolean)
                _Cheque = value
            End Set
        End Property

        Private _IdCuentaContable As System.String = ""
        Public Property IdCuentaContable() As System.String
            Get
                Return _IdCuentaContable
            End Get
            Set(ByVal value As System.String)
                _IdCuentaContable = value
            End Set
        End Property

        Private _ProcesadaBanco As System.Boolean
        Public Property ProcesadaBanco() As System.Boolean
            Get
                Return _ProcesadaBanco
            End Get
            Set(ByVal value As System.Boolean)
                _ProcesadaBanco = value
            End Set
        End Property

        Private _Contabilizar As System.Boolean
        Public Property Contabilizar() As System.Boolean
            Get
                Return _Contabilizar
            End Get
            Set(ByVal value As System.Boolean)
                _Contabilizar = value
            End Set
        End Property

        Private _Referencia As System.String = ""
        Public Property Referencia() As System.String
            Get
                Return _Referencia
            End Get
            Set(ByVal value As System.String)
                _Referencia = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _MesConcilia As System.Int32
        Public Property MesConcilia() As System.Int32
            Get
                Return _MesConcilia
            End Get
            Set(ByVal value As System.Int32)
                _MesConcilia = value
            End Set
        End Property

        Private _EjercicioConcilia As System.Int32
        Public Property EjercicioConcilia() As System.Int32
            Get
                Return _EjercicioConcilia
            End Get
            Set(ByVal value As System.Int32)
                _EjercicioConcilia = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

        Private _Debe As System.Decimal
        Public Property Debe() As System.Decimal
            Get
                Return _Debe
            End Get
            Set(ByVal value As System.Decimal)
                _Debe = value
            End Set
        End Property

        Private _Haber As System.Decimal
        Public Property Haber() As System.Decimal
            Get
                Return _Haber
            End Get
            Set(ByVal value As System.Decimal)
                _Haber = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region

#Region "adm_TiposComprobante"
    Public Class adm_TiposComprobante
        Private _IdTipoComprobante As System.Int32
        Public Property IdTipoComprobante() As System.Int32
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Abreviatura As System.String = ""
        Public Property Abreviatura() As System.String
            Get
                Return _Abreviatura
            End Get
            Set(ByVal value As System.String)
                _Abreviatura = value
            End Set
        End Property

        Private _TipoAplicacion As System.Int32
        Public Property TipoAplicacion() As System.Int32
            Get
                Return _TipoAplicacion
            End Get
            Set(ByVal value As System.Int32)
                _TipoAplicacion = value
            End Set
        End Property

        Private _IdLibro As System.Int32
        Public Property IdLibro() As System.Int32
            Get
                Return _IdLibro
            End Get
            Set(ByVal value As System.Int32)
                _IdLibro = value
            End Set
        End Property

        Private _DetallaIVA As System.Boolean
        Public Property DetallaIVA() As System.Boolean
            Get
                Return _DetallaIVA
            End Get
            Set(ByVal value As System.Boolean)
                _DetallaIVA = value
            End Set
        End Property
        Private _FormularioUnico As System.Boolean
        Public Property FormularioUnico() As System.Boolean
            Get
                Return _FormularioUnico
            End Get
            Set(ByVal value As System.Boolean)
                _FormularioUnico = value
            End Set
        End Property
        Private _IdModuloAplica As System.Int32
        Public Property IdModuloAplica() As System.Int32
            Get
                Return _IdModuloAplica
            End Get
            Set(ByVal value As System.Int32)
                _IdModuloAplica = value
            End Set
        End Property

        Private _NumeroLineas As System.Int32
        Public Property NumeroLineas() As System.Int32
            Get
                Return _NumeroLineas
            End Get
            Set(ByVal value As System.Int32)
                _NumeroLineas = value
            End Set
        End Property

        Private _IdCuentaInv As System.String = ""
        Public Property IdCuentaInv() As System.String
            Get
                Return _IdCuentaInv
            End Get
            Set(ByVal value As System.String)
                _IdCuentaInv = value
            End Set
        End Property

        Private _NombreImpresor As System.String = ""
        Public Property NombreImpresor() As System.String
            Get
                Return _NombreImpresor
            End Get
            Set(ByVal value As System.String)
                _NombreImpresor = value
            End Set
        End Property


    End Class
#End Region


#Region "adm_Departamentos"
    Public Class adm_Departamentos
        Private _IdDepartamento As System.String = ""
        Public Property IdDepartamento() As System.String
            Get
                Return _IdDepartamento
            End Get
            Set(ByVal value As System.String)
                _IdDepartamento = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "adm_Municipios"
    Public Class adm_Municipios
        Private _IdMunicipio As System.String = ""
        Public Property IdMunicipio() As System.String
            Get
                Return _IdMunicipio
            End Get
            Set(ByVal value As System.String)
                _IdMunicipio = value
            End Set
        End Property

        Private _IdDepartamento As System.String = ""
        Public Property IdDepartamento() As System.String
            Get
                Return _IdDepartamento
            End Get
            Set(ByVal value As System.String)
                _IdDepartamento = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region


#Region "ban_Conciliaciones"
    Public Class ban_Conciliaciones
        Private _IdCuentaBancaria As System.Int32
        Public Property IdCuentaBancaria() As System.Int32
            Get
                Return _IdCuentaBancaria
            End Get
            Set(ByVal value As System.Int32)
                _IdCuentaBancaria = value
            End Set
        End Property

        Private _mes As System.Int32
        Public Property mes() As System.Int32
            Get
                Return _mes
            End Get
            Set(ByVal value As System.Int32)
                _mes = value
            End Set
        End Property

        Private _anio As System.Int32
        Public Property anio() As System.Int32
            Get
                Return _anio
            End Get
            Set(ByVal value As System.Int32)
                _anio = value
            End Set
        End Property

        Private _SaldoContable As System.Decimal
        Public Property SaldoContable() As System.Decimal
            Get
                Return _SaldoContable
            End Get
            Set(ByVal value As System.Decimal)
                _SaldoContable = value
            End Set
        End Property

        Private _CargosNoContab As System.Decimal
        Public Property CargosNoContab() As System.Decimal
            Get
                Return _CargosNoContab
            End Get
            Set(ByVal value As System.Decimal)
                _CargosNoContab = value
            End Set
        End Property

        Private _DepositosNoContab As System.Decimal
        Public Property DepositosNoContab() As System.Decimal
            Get
                Return _DepositosNoContab
            End Get
            Set(ByVal value As System.Decimal)
                _DepositosNoContab = value
            End Set
        End Property

        Private _ChequesPendientes As System.Decimal
        Public Property ChequesPendientes() As System.Decimal
            Get
                Return _ChequesPendientes
            End Get
            Set(ByVal value As System.Decimal)
                _ChequesPendientes = value
            End Set
        End Property

        Private _CargosPendientes As System.Decimal
        Public Property CargosPendientes() As System.Decimal
            Get
                Return _CargosPendientes
            End Get
            Set(ByVal value As System.Decimal)
                _CargosPendientes = value
            End Set
        End Property

        Private _DepositosPendientes As System.Decimal
        Public Property DepositosPendientes() As System.Decimal
            Get
                Return _DepositosPendientes
            End Get
            Set(ByVal value As System.Decimal)
                _DepositosPendientes = value
            End Set
        End Property

        Private _SaldoBanco As System.Decimal
        Public Property SaldoBanco() As System.Decimal
            Get
                Return _SaldoBanco
            End Get
            Set(ByVal value As System.Decimal)
                _SaldoBanco = value
            End Set
        End Property

        Private _Tipo As System.Int32
        Public Property Tipo() As System.Int32
            Get
                Return _Tipo
            End Get
            Set(ByVal value As System.Int32)
                _Tipo = value
            End Set
        End Property

        Private _Referencia As System.String = ""
        Public Property Referencia() As System.String
            Get
                Return _Referencia
            End Get
            Set(ByVal value As System.String)
                _Referencia = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

    End Class
#End Region
#Region "ban_ConciliacionesCerradas"
    Public Class ban_ConciliacionesCerradas
        Private _IdCuentaBancaria As System.Int32
        Public Property IdCuentaBancaria() As System.Int32
            Get
                Return _IdCuentaBancaria
            End Get
            Set(ByVal value As System.Int32)
                _IdCuentaBancaria = value
            End Set
        End Property

        Private _Mes As System.Byte
        Public Property Mes() As System.Byte
            Get
                Return _Mes
            End Get
            Set(ByVal value As System.Byte)
                _Mes = value
            End Set
        End Property

        Private _Ejercicio As System.Int32
        Public Property Ejercicio() As System.Int32
            Get
                Return _Ejercicio
            End Get
            Set(ByVal value As System.Int32)
                _Ejercicio = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region

#Region "ban_CuentasBancarias"
    Public Class ban_CuentasBancarias
        Private _IdCuentaBancaria As System.Int32
        Public Property IdCuentaBancaria() As System.Int32
            Get
                Return _IdCuentaBancaria
            End Get
            Set(ByVal value As System.Int32)
                _IdCuentaBancaria = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _NumeroCuenta As System.String = ""
        Public Property NumeroCuenta() As System.String
            Get
                Return _NumeroCuenta
            End Get
            Set(ByVal value As System.String)
                _NumeroCuenta = value
            End Set
        End Property

        Private _IdCuentaContable As System.String = ""
        Public Property IdCuentaContable() As System.String
            Get
                Return _IdCuentaContable
            End Get
            Set(ByVal value As System.String)
                _IdCuentaContable = value
            End Set
        End Property

        Private _NombreDigitador As System.String = ""
        Public Property NombreDigitador() As System.String
            Get
                Return _NombreDigitador
            End Get
            Set(ByVal value As System.String)
                _NombreDigitador = value
            End Set
        End Property

        Private _CargoDigitador As System.String = ""
        Public Property CargoDigitador() As System.String
            Get
                Return _CargoDigitador
            End Get
            Set(ByVal value As System.String)
                _CargoDigitador = value
            End Set
        End Property

        Private _NombreRevisa As System.String = ""
        Public Property NombreRevisa() As System.String
            Get
                Return _NombreRevisa
            End Get
            Set(ByVal value As System.String)
                _NombreRevisa = value
            End Set
        End Property

        Private _CargoRevisa As System.String = ""
        Public Property CargoRevisa() As System.String
            Get
                Return _CargoRevisa
            End Get
            Set(ByVal value As System.String)
                _CargoRevisa = value
            End Set
        End Property

        Private _NombreAutoriza As System.String = ""
        Public Property NombreAutoriza() As System.String
            Get
                Return _NombreAutoriza
            End Get
            Set(ByVal value As System.String)
                _NombreAutoriza = value
            End Set
        End Property

        Private _CargoAutoriza As System.String = ""
        Public Property CargoAutoriza() As System.String
            Get
                Return _CargoAutoriza
            End Get
            Set(ByVal value As System.String)
                _CargoAutoriza = value
            End Set
        End Property

        Private _UltNumeroCheque As System.Int32
        Public Property UltNumeroCheque() As System.Int32
            Get
                Return _UltNumeroCheque
            End Get
            Set(ByVal value As System.Int32)
                _UltNumeroCheque = value
            End Set
        End Property

        Private _IdTipoPartida As System.String = ""
        Public Property IdTipoPartida() As System.String
            Get
                Return _IdTipoPartida
            End Get
            Set(ByVal value As System.String)
                _IdTipoPartida = value
            End Set
        End Property

        Private _IdFormatoVaucher As System.Int32
        Public Property IdFormatoVaucher() As System.Int32
            Get
                Return _IdFormatoVaucher
            End Get
            Set(ByVal value As System.Int32)
                _IdFormatoVaucher = value
            End Set
        End Property

        Private _EstadoCuenta As System.Int32
        Public Property EstadoCuenta() As System.Int32
            Get
                Return _EstadoCuenta
            End Get
            Set(ByVal value As System.Int32)
                _EstadoCuenta = value
            End Set
        End Property

        Private _TipoImpresion As System.Int32
        Public Property TipoImpresion() As System.Int32
            Get
                Return _TipoImpresion
            End Get
            Set(ByVal value As System.Int32)
                _TipoImpresion = value
            End Set
        End Property

        Private _CuentaInactiva As System.Boolean
        Public Property CuentaInactiva() As System.Boolean
            Get
                Return _CuentaInactiva
            End Get
            Set(ByVal value As System.Boolean)
                _CuentaInactiva = value
            End Set
        End Property
        Private _IdCuentaContablePos As System.String = ""
        Public Property IdCuentaContablePos() As System.String
            Get
                Return _IdCuentaContablePos
            End Get
            Set(ByVal value As System.String)
                _IdCuentaContablePos = value
            End Set
        End Property
        Private _CodigoEmpresa As System.String = ""
        Public Property CodigoEmpresa() As System.String
            Get
                Return _CodigoEmpresa
            End Get
            Set(ByVal value As System.String)
                _CodigoEmpresa = value
            End Set
        End Property

        Private _IdBanco As System.Int32
        Public Property IdBanco() As System.Int32
            Get
                Return _IdBanco
            End Get
            Set(ByVal value As System.Int32)
                _IdBanco = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region

#Region "fac_Token"
    Public Class fac_Token
        Private _IdToken As System.Int32
        Public Property IdToken() As System.Int32
            Get
                Return _IdToken
            End Get
            Set(ByVal value As System.Int32)
                _IdToken = value
            End Set
        End Property

        Private _Token As System.String = ""
        Public Property Token() As System.String
            Get
                Return _Token
            End Get
            Set(ByVal value As System.String)
                _Token = value
            End Set
        End Property

        Private _DesdeFecha As Nullable(Of DateTime)
        Public Property DesdeFecha() As Nullable(Of DateTime)
            Get
                Return _DesdeFecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _DesdeFecha = value
            End Set
        End Property

        Private _HastaFecha As Nullable(Of DateTime)
        Public Property HastaFecha() As Nullable(Of DateTime)
            Get
                Return _HastaFecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _HastaFecha = value
            End Set
        End Property

    End Class
#End Region


#Region "ban_Cheques"
    Public Class ban_Cheques
        Private _IdCheque As System.Int32
        Public Property IdCheque() As System.Int32
            Get
                Return _IdCheque
            End Get
            Set(ByVal value As System.Int32)
                _IdCheque = value
            End Set
        End Property

        Private _IdCuentaBancaria As System.Int32
        Public Property IdCuentaBancaria() As System.Int32
            Get
                Return _IdCuentaBancaria
            End Get
            Set(ByVal value As System.Int32)
                _IdCuentaBancaria = value
            End Set
        End Property
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _Numero As String = ""
        Public Property Numero() As String
            Get
                Return _Numero
            End Get
            Set(ByVal value As String)
                _Numero = value
            End Set
        End Property

        Private _Sufijo As String = ""
        Public Property Sufijo() As String
            Get
                Return _Sufijo
            End Get
            Set(ByVal value As String)
                _Sufijo = value
            End Set
        End Property
        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

        Private _IdProveedor As String = ""
        Public Property IdProveedor() As String
            Get
                Return _IdProveedor
            End Get
            Set(ByVal value As String)
                _IdProveedor = value
            End Set
        End Property

        Private _AnombreDe As String = ""
        Public Property AnombreDe() As String
            Get
                Return _AnombreDe
            End Get
            Set(ByVal value As String)
                _AnombreDe = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _IdPartida As System.Int32
        Public Property IdPartida() As System.Int32
            Get
                Return _IdPartida
            End Get
            Set(ByVal value As System.Int32)
                _IdPartida = value
            End Set
        End Property

        Private _IdTipoPartida As System.String = ""
        Public Property IdTipoPartida() As System.String
            Get
                Return _IdTipoPartida
            End Get
            Set(ByVal value As System.String)
                _IdTipoPartida = value
            End Set
        End Property

        Private _NumeroPartida As System.String = ""
        Public Property NumeroPartida() As System.String
            Get
                Return _NumeroPartida
            End Get
            Set(ByVal value As System.String)
                _NumeroPartida = value
            End Set
        End Property

        Private _Impreso As System.Boolean
        Public Property Impreso() As System.Boolean
            Get
                Return _Impreso
            End Get
            Set(ByVal value As System.Boolean)
                _Impreso = value
            End Set
        End Property

        Private _Conciliado As System.Boolean
        Public Property Conciliado() As System.Boolean
            Get
                Return _Conciliado
            End Get
            Set(ByVal value As System.Boolean)
                _Conciliado = value
            End Set
        End Property

        Private _PagadoBanco As System.Boolean
        Public Property PagadoBanco() As System.Boolean
            Get
                Return _PagadoBanco
            End Get
            Set(ByVal value As System.Boolean)
                _PagadoBanco = value
            End Set
        End Property

        Private _Anulado As System.Boolean
        Public Property Anulado() As System.Boolean
            Get
                Return _Anulado
            End Get
            Set(ByVal value As System.Boolean)
                _Anulado = value
            End Set
        End Property

        Private _FechaAnulacion As Nullable(Of DateTime)
        Public Property FechaAnulacion() As Nullable(Of DateTime)
            Get
                Return _FechaAnulacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaAnulacion = value
            End Set
        End Property

        Private _MotivoAnulacion As System.String = ""
        Public Property MotivoAnulacion() As System.String
            Get
                Return _MotivoAnulacion
            End Get
            Set(ByVal value As System.String)
                _MotivoAnulacion = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "ban_ChequesDetalle"
    Public Class ban_ChequesDetalle
        Private _IdCheque As System.Int32
        Public Property IdCheque() As System.Int32
            Get
                Return _IdCheque
            End Get
            Set(ByVal value As System.Int32)
                _IdCheque = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdCuenta As System.String = ""
        Public Property IdCuenta() As System.String
            Get
                Return _IdCuenta
            End Get
            Set(ByVal value As System.String)
                _IdCuenta = value
            End Set
        End Property

        Private _Referencia As System.String = ""
        Public Property Referencia() As System.String
            Get
                Return _Referencia
            End Get
            Set(ByVal value As System.String)
                _Referencia = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _Debe As System.Decimal
        Public Property Debe() As System.Decimal
            Get
                Return _Debe
            End Get
            Set(ByVal value As System.Decimal)
                _Debe = value
            End Set
        End Property

        Private _Haber As System.Decimal
        Public Property Haber() As System.Decimal
            Get
                Return _Haber
            End Get
            Set(ByVal value As System.Decimal)
                _Haber = value
            End Set
        End Property

        Private _IdCentro As System.String = ""
        Public Property IdCentro() As System.String
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.String)
                _IdCentro = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "ban_TiposTransaccion"
    Public Class ban_TiposTransaccion
        Private _IdTipo As System.Int32
        Public Property IdTipo() As System.Int32
            Get
                Return _IdTipo
            End Get
            Set(ByVal value As System.Int32)
                _IdTipo = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _IdTipoPartida As System.String = ""
        Public Property IdTipoPartida() As System.String
            Get
                Return _IdTipoPartida
            End Get
            Set(ByVal value As System.String)
                _IdTipoPartida = value
            End Set
        End Property

        Private _TipoTransaccion As System.Int32
        Public Property TipoTransaccion() As System.Int32
            Get
                Return _TipoTransaccion
            End Get
            Set(ByVal value As System.Int32)
                _TipoTransaccion = value
            End Set
        End Property

    End Class
#End Region
#Region "ban_Transacciones"
    Public Class ban_Transacciones
        Private _IdTransaccion As System.Int32
        Public Property IdTransaccion() As System.Int32
            Get
                Return _IdTransaccion
            End Get
            Set(ByVal value As System.Int32)
                _IdTransaccion = value
            End Set
        End Property

        Private _IdCuentaBancaria As System.Int32
        Public Property IdCuentaBancaria() As System.Int32
            Get
                Return _IdCuentaBancaria
            End Get
            Set(ByVal value As System.Int32)
                _IdCuentaBancaria = value
            End Set
        End Property
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _FechaContable As Nullable(Of DateTime)
        Public Property FechaContable() As Nullable(Of DateTime)
            Get
                Return _FechaContable
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaContable = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

        Private _IdTipo As System.Int32
        Public Property IdTipo() As System.Int32
            Get
                Return _IdTipo
            End Get
            Set(ByVal value As System.Int32)
                _IdTipo = value
            End Set
        End Property

        Private _IncluirConciliacion As System.Boolean
        Public Property IncluirConciliacion() As System.Boolean
            Get
                Return _IncluirConciliacion
            End Get
            Set(ByVal value As System.Boolean)
                _IncluirConciliacion = value
            End Set
        End Property

        Private _Contabilizar As System.Boolean
        Public Property Contabilizar() As System.Boolean
            Get
                Return _Contabilizar
            End Get
            Set(ByVal value As System.Boolean)
                _Contabilizar = value
            End Set
        End Property

        Private _ProcesadaBanco As System.Boolean
        Public Property ProcesadaBanco() As System.Boolean
            Get
                Return _ProcesadaBanco
            End Get
            Set(ByVal value As System.Boolean)
                _ProcesadaBanco = value
            End Set
        End Property

        Private _IdTipoPartida As System.String = ""
        Public Property IdTipoPartida() As System.String
            Get
                Return _IdTipoPartida
            End Get
            Set(ByVal value As System.String)
                _IdTipoPartida = value
            End Set
        End Property

        Private _IdPartida As System.Int32
        Public Property IdPartida() As System.Int32
            Get
                Return _IdPartida
            End Get
            Set(ByVal value As System.Int32)
                _IdPartida = value
            End Set
        End Property

        Private _NumeroPartida As System.String = ""
        Public Property NumeroPartida() As System.String
            Get
                Return _NumeroPartida
            End Get
            Set(ByVal value As System.String)
                _NumeroPartida = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "ban_TransaccionesDetalle"
    Public Class ban_TransaccionesDetalle
        Private _IdTransaccion As System.Int32
        Public Property IdTransaccion() As System.Int32
            Get
                Return _IdTransaccion
            End Get
            Set(ByVal value As System.Int32)
                _IdTransaccion = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdCuenta As System.String = ""
        Public Property IdCuenta() As System.String
            Get
                Return _IdCuenta
            End Get
            Set(ByVal value As System.String)
                _IdCuenta = value
            End Set
        End Property

        Private _Referencia As System.String = ""
        Public Property Referencia() As System.String
            Get
                Return _Referencia
            End Get
            Set(ByVal value As System.String)
                _Referencia = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _Debe As System.Decimal
        Public Property Debe() As System.Decimal
            Get
                Return _Debe
            End Get
            Set(ByVal value As System.Decimal)
                _Debe = value
            End Set
        End Property

        Private _Haber As System.Decimal
        Public Property Haber() As System.Decimal
            Get
                Return _Haber
            End Get
            Set(ByVal value As System.Decimal)
                _Haber = value
            End Set
        End Property

        Private _IdCentro As System.String = ""
        Public Property IdCentro() As System.String
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.String)
                _IdCentro = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region

#Region "con_Cuentas"
    Public Class con_Cuentas
        Private _IdCuenta As System.String = ""
        Public Property IdCuenta() As System.String
            Get
                Return _IdCuenta
            End Get
            Set(ByVal value As System.String)
                _IdCuenta = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _IdCuentaMayor As System.String = ""
        Public Property IdCuentaMayor() As System.String
            Get
                Return _IdCuentaMayor
            End Get
            Set(ByVal value As System.String)
                _IdCuentaMayor = value
            End Set
        End Property

        Private _Tipo As Byte
        Public Property Tipo() As Byte
            Get
                Return _Tipo
            End Get
            Set(ByVal value As Byte)
                _Tipo = value
            End Set
        End Property

        Private _Nivel As Byte
        Public Property Nivel() As Byte
            Get
                Return _Nivel
            End Get
            Set(ByVal value As Byte)
                _Nivel = value
            End Set
        End Property

        Private _Naturaleza As Byte
        Public Property Naturaleza() As Byte
            Get
                Return _Naturaleza
            End Get
            Set(ByVal value As Byte)
                _Naturaleza = value
            End Set
        End Property

        Private _EsTransaccional As System.Boolean
        Public Property EsTransaccional() As System.Boolean
            Get
                Return _EsTransaccional
            End Get
            Set(ByVal value As System.Boolean)
                _EsTransaccional = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "con_CierreCaja"
    Public Class con_CierreCaja
        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _Saldo As System.Decimal
        Public Property Saldo() As System.Decimal
            Get
                Return _Saldo
            End Get
            Set(ByVal value As System.Decimal)
                _Saldo = value
            End Set
        End Property

        Private _Cerrado As System.Boolean
        Public Property Cerrado() As System.Boolean
            Get
                Return _Cerrado
            End Get
            Set(ByVal value As System.Boolean)
                _Cerrado = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region

#Region "con_ConceptosCaja"
    Public Class con_ConceptosCaja
        Private _IdConcepto As System.Int32
        Public Property IdConcepto() As System.Int32
            Get
                Return _IdConcepto
            End Get
            Set(ByVal value As System.Int32)
                _IdConcepto = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _IngresoEgreso As System.Int32
        Public Property IngresoEgreso() As System.Int32
            Get
                Return _IngresoEgreso
            End Get
            Set(ByVal value As System.Int32)
                _IngresoEgreso = value
            End Set
        End Property

        Private _CuentaContable As System.String = ""
        Public Property CuentaContable() As System.String
            Get
                Return _CuentaContable
            End Get
            Set(ByVal value As System.String)
                _CuentaContable = value
            End Set
        End Property

        Private _Orden As System.Int32
        Public Property Orden() As System.Int32
            Get
                Return _Orden
            End Get
            Set(ByVal value As System.Int32)
                _Orden = value
            End Set
        End Property

    End Class
#End Region

#Region "con_Transacciones"
    Public Class con_Transacciones
        Private _IdComprobante As System.Guid
        Public Property IdComprobante() As System.Guid
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Guid)
                _IdComprobante = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _IdConcepto As System.Int32
        Public Property IdConcepto() As System.Int32
            Get
                Return _IdConcepto
            End Get
            Set(ByVal value As System.Int32)
                _IdConcepto = value
            End Set
        End Property

        Private _IdCuentaBancaria As System.Int16
        Public Property IdCuentaBancaria() As System.Int16
            Get
                Return _IdCuentaBancaria
            End Get
            Set(ByVal value As System.Int16)
                _IdCuentaBancaria = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "con_CuentasPatrimonio"
    Public Class con_CuentasPatrimonio
        Private _IdCuenta As System.String = ""
        Public Property IdCuenta() As System.String
            Get
                Return _IdCuenta
            End Get
            Set(ByVal value As System.String)
                _IdCuenta = value
            End Set
        End Property

        Private _IdRubro As System.Int32
        Public Property IdRubro() As System.Int32
            Get
                Return _IdRubro
            End Get
            Set(ByVal value As System.Int32)
                _IdRubro = value
            End Set
        End Property

    End Class
#End Region

#Region "con_FormulasFinancieras"
	Public Class con_FormulasFinancieras
		Private _Id As System.Int32
		Public Property Id() As System.Int32
			Get
				Return _Id
			End Get
			Set(ByVal value As System.Int32)
				_Id = value
			End Set
		End Property

		Private _IdFormula As System.Int32
		Public Property IdFormula() As System.Int32
			Get
				Return _IdFormula
			End Get
			Set(ByVal value As System.Int32)
				_IdFormula = value
			End Set
		End Property

		Private _ValorFormula As System.String = ""
		Public Property ValorFormula() As System.String
			Get
				Return _ValorFormula
			End Get
			Set(ByVal value As System.String)
				_ValorFormula = value
			End Set
		End Property

		Private _Descripcion As System.String = ""
		Public Property Descripcion() As System.String
			Get
				Return _Descripcion
			End Get
			Set(ByVal value As System.String)
				_Descripcion = value
			End Set
		End Property

		Private _TipoValor As System.Int32
		Public Property TipoValor() As System.Int32
			Get
				Return _TipoValor
			End Get
			Set(ByVal value As System.Int32)
				_TipoValor = value
			End Set
		End Property

	End Class
#End Region




#Region "con_Partidas"
	Public Class con_Partidas
        Private _IdPartida As System.Int32
        Public Property IdPartida() As System.Int32
            Get
                Return _IdPartida
            End Get
            Set(ByVal value As System.Int32)
                _IdPartida = value
            End Set
        End Property
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdTipo As System.String = ""
        Public Property IdTipo() As System.String
            Get
                Return _IdTipo
            End Get
            Set(ByVal value As System.String)
                _IdTipo = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _Actualizada As System.Boolean
        Public Property Actualizada() As System.Boolean
            Get
                Return _Actualizada
            End Get
            Set(ByVal value As System.Boolean)
                _Actualizada = value
            End Set
        End Property

        Private _IdModuloOrigen As System.Byte
        Public Property IdModuloOrigen() As System.Byte
            Get
                Return _IdModuloOrigen
            End Get
            Set(ByVal value As System.Byte)
                _IdModuloOrigen = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "con_PartidasDetalle"
    Public Class con_PartidasDetalle
        Private _IdPartida As System.Int32
        Public Property IdPartida() As System.Int32
            Get
                Return _IdPartida
            End Get
            Set(ByVal value As System.Int32)
                _IdPartida = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdCuenta As System.String = ""
        Public Property IdCuenta() As System.String
            Get
                Return _IdCuenta
            End Get
            Set(ByVal value As System.String)
                _IdCuenta = value
            End Set
        End Property

        Private _Referencia As System.String = ""
        Public Property Referencia() As System.String
            Get
                Return _Referencia
            End Get
            Set(ByVal value As System.String)
                _Referencia = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _Debe As System.Decimal
        Public Property Debe() As System.Decimal
            Get
                Return _Debe
            End Get
            Set(ByVal value As System.Decimal)
                _Debe = value
            End Set
        End Property

        Private _Haber As System.Decimal
        Public Property Haber() As System.Decimal
            Get
                Return _Haber
            End Get
            Set(ByVal value As System.Decimal)
                _Haber = value
            End Set
        End Property

        Private _IdCentro As System.String = ""
        Public Property IdCentro() As System.String
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.String)
                _IdCentro = value
            End Set
        End Property
        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "con_TiposPartida"
    Public Class con_TiposPartida
        Private _IdTipo As System.String = ""
        Public Property IdTipo() As System.String
            Get
                Return _IdTipo
            End Get
            Set(ByVal value As System.String)
                _IdTipo = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "con_CentrosCosto"
    Public Class con_CentrosCosto
        Private _IdCentro As System.String = ""
        Public Property IdCentro() As System.String
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.String)
                _IdCentro = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "con_CentrosMayor"
    Public Class con_CentrosMayor
        Private _IdCuentaMayor As System.String = ""
        Public Property IdCuentaMayor() As System.String
            Get
                Return _IdCuentaMayor
            End Get
            Set(ByVal value As System.String)
                _IdCuentaMayor = value
            End Set
        End Property

        Private _IdCentro As System.String = ""
        Public Property IdCentro() As System.String
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.String)
                _IdCentro = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "con_NIIFConceptos"
    Public Class con_NIIFConceptos
        Private _IdReporte As System.Int32
        Public Property IdReporte() As System.Int32
            Get
                Return _IdReporte
            End Get
            Set(ByVal value As System.Int32)
                _IdReporte = value
            End Set
        End Property
        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property
        Private _NumeroLinea As System.Int32
        Public Property NumeroLinea() As System.Int32
            Get
                Return _NumeroLinea
            End Get
            Set(ByVal value As System.Int32)
                _NumeroLinea = value
            End Set
        End Property

        Private _ConceptoNIIF As System.String = ""
        Public Property ConceptoNIIF() As System.String
            Get
                Return _ConceptoNIIF
            End Get
            Set(ByVal value As System.String)
                _ConceptoNIIF = value
            End Set
        End Property

        Private _Resta As System.Boolean
        Public Property Resta() As System.Boolean
            Get
                Return _Resta
            End Get
            Set(ByVal value As System.Boolean)
                _Resta = value
            End Set
        End Property

        Private _IdCuentaContable As System.String = ""
        Public Property IdCuentaContable() As System.String
            Get
                Return _IdCuentaContable
            End Get
            Set(ByVal value As System.String)
                _IdCuentaContable = value
            End Set
        End Property

        Private _CuentaPadre As System.Boolean
        Public Property CuentaPadre() As System.Boolean
            Get
                Return _CuentaPadre
            End Get
            Set(ByVal value As System.Boolean)
                _CuentaPadre = value
            End Set
        End Property

        Private _SubTotal As System.Boolean
        Public Property SubTotal() As System.Boolean
            Get
                Return _SubTotal
            End Get
            Set(ByVal value As System.Boolean)
                _SubTotal = value
            End Set
        End Property

        Private _ImprimirLinea As System.Boolean
        Public Property ImprimirLinea() As System.Boolean
            Get
                Return _ImprimirLinea
            End Get
            Set(ByVal value As System.Boolean)
                _ImprimirLinea = value
            End Set
        End Property

        Private _Valor1 As System.Decimal
        Public Property Valor1() As System.Decimal
            Get
                Return _Valor1
            End Get
            Set(ByVal value As System.Decimal)
                _Valor1 = value
            End Set
        End Property

        Private _Valor2 As System.Decimal
        Public Property Valor2() As System.Decimal
            Get
                Return _Valor2
            End Get
            Set(ByVal value As System.Decimal)
                _Valor2 = value
            End Set
        End Property

        Private _EsSubTitulo As System.Boolean
        Public Property EsSubTitulo() As System.Boolean
            Get
                Return _EsSubTitulo
            End Get
            Set(ByVal value As System.Boolean)
                _EsSubTitulo = value
            End Set
        End Property

        Private _ImprimirLineaBajo As System.Boolean
        Public Property ImprimirLineaBajo() As System.Boolean
            Get
                Return _ImprimirLineaBajo
            End Get
            Set(ByVal value As System.Boolean)
                _ImprimirLineaBajo = value
            End Set
        End Property

    End Class
#End Region
#Region "con_RubroFlujo"
    Public Class con_RubroFlujo
        Private _IdRubro As System.Int32
        Public Property IdRubro() As System.Int32
            Get
                Return _IdRubro
            End Get
            Set(ByVal value As System.Int32)
                _IdRubro = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property
    End Class
#End Region
#Region "con_RubroFlujoDetalle"
    Public Class con_RubroFlujoDetalle
        Private _IdCuentaMayor As System.String = ""
        Public Property IdCuentaMayor() As System.String
            Get
                Return _IdCuentaMayor
            End Get
            Set(ByVal value As System.String)
                _IdCuentaMayor = value
            End Set
        End Property

        Private _IdRubro As System.Int32
        Public Property IdRubro() As System.Int32
            Get
                Return _IdRubro
            End Get
            Set(ByVal value As System.Int32)
                _IdRubro = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property
    End Class
#End Region

    'PRESUPUESTOS
#Region "pre_DepartamentosSucursal"
    Public Class pre_DepartamentosSucursal
        Private _IdDepartamentoSucursalPk As System.Int32
        Public Property IdDepartamentoSucursalPk() As System.Int32
            Get
                Return _IdDepartamentoSucursalPk
            End Get
            Set(ByVal value As System.Int32)
                _IdDepartamentoSucursalPk = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _Encargado As System.String = ""
        Public Property Encargado() As System.String
            Get
                Return _Encargado
            End Get
            Set(ByVal value As System.String)
                _Encargado = value
            End Set
        End Property

        Private _IdSucursalFk As System.Int32
        Public Property IdSucursalFk() As System.Int32
            Get
                Return _IdSucursalFk
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursalFk = value
            End Set
        End Property

    End Class
#End Region

#Region "pre_DetallesPresupuesto"
    Public Class pre_DetallesPresupuesto

        Public Enum Meses
            Enero = 1
            Febrero = 2
            Marzo = 3
            Abril = 4
            Mayo = 5
            Junio = 6
            Julio = 7
            Agosto = 8
            Septiembre = 9
            Octubre = 10
            Noviembre = 11
            Diciembre = 12
        End Enum

        Private _IdDetallePresupuestoPk As System.Int32
        Public Property IdDetallePresupuestoPk() As System.Int32
            Get
                Return _IdDetallePresupuestoPk
            End Get
            Set(ByVal value As System.Int32)
                _IdDetallePresupuestoPk = value
            End Set
        End Property

        Private _IdPresupuestoFk As System.Int32
        Public Property IdPresupuestoFk() As System.Int32
            Get
                Return _IdPresupuestoFk
            End Get
            Set(ByVal value As System.Int32)
                _IdPresupuestoFk = value
            End Set
        End Property

        Private _Mes As Meses
        Public Property Mes() As Meses
            Get
                Return _Mes
            End Get
            Set(ByVal value As Meses)
                _Mes = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

        Private _Comentario As System.String = ""
        Public Property Comentario() As System.String
            Get
                Return _Comentario
            End Get
            Set(ByVal value As System.String)
                _Comentario = value
            End Set
        End Property

        Private _FechaCreacion As Nullable(Of DateTime)
        Public Property FechaCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaCreacion = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

    End Class
#End Region

#Region "pre_PresupuestoCuenta"
    Public Class pre_PresupuestoCuenta
        Private _IdPresupuestoCuentaPk As System.Int32
        Public Property IdPresupuestoCuentaPk() As System.Int32
            Get
                Return _IdPresupuestoCuentaPk
            End Get
            Set(ByVal value As System.Int32)
                _IdPresupuestoCuentaPk = value
            End Set
        End Property

        Private _IdCuentaFk As System.String = ""
        Public Property IdCuentaFk() As System.String
            Get
                Return _IdCuentaFk
            End Get
            Set(ByVal value As System.String)
                _IdCuentaFk = value
            End Set
        End Property

        Private _IdPresupuestoFk As System.Int32
        Public Property IdPresupuestoFk() As System.Int32
            Get
                Return _IdPresupuestoFk
            End Get
            Set(ByVal value As System.Int32)
                _IdPresupuestoFk = value
            End Set
        End Property

    End Class
#End Region

#Region "pre_Presupuestos"
    Public Class pre_Presupuestos
        Private _IdPresupuestoPk As System.Int32
        Public Property IdPresupuestoPk() As System.Int32
            Get
                Return _IdPresupuestoPk
            End Get
            Set(ByVal value As System.Int32)
                _IdPresupuestoPk = value
            End Set
        End Property

        Private _IdSucursalFk As System.Int32
        Public Property IdSucursalFk() As System.Int32
            Get
                Return _IdSucursalFk
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursalFk = value
            End Set
        End Property

        Private _IdDepartamentoFk As System.Int32
        Public Property IdDepartamentoFk() As System.Int32
            Get
                Return _IdDepartamentoFk
            End Get
            Set(ByVal value As System.Int32)
                _IdDepartamentoFk = value
            End Set
        End Property

        Private _IdCentroCostosFk As System.String
        Public Property IdCentroCostosFk() As System.String
            Get
                Return _IdCentroCostosFk
            End Get
            Set(ByVal value As System.String)
                _IdCentroCostosFk = value
            End Set
        End Property

        Private _Anio As Nullable(Of Integer)
        Public Property Anio() As Nullable(Of Integer)
            Get
                Return _Anio
            End Get
            Set(ByVal value As Nullable(Of Integer))
                _Anio = value
            End Set
        End Property

        Private _Comentario As System.String = ""
        Public Property Comentario() As System.String
            Get
                Return _Comentario
            End Get
            Set(ByVal value As System.String)
                _Comentario = value
            End Set
        End Property

        Private _FechaCreacion As Nullable(Of DateTime)
        Public Property FechaCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaCreacion = value
            End Set
        End Property

    End Class
#End Region


#Region "com_Proveedores"
    Public Class com_Proveedores
        Private _IdProveedor As System.String = ""
        Public Property IdProveedor() As System.String
            Get
                Return _IdProveedor
            End Get
            Set(ByVal value As System.String)
                _IdProveedor = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Nrc As System.String = ""
        Public Property Nrc() As System.String
            Get
                Return _Nrc
            End Get
            Set(ByVal value As System.String)
                _Nrc = value
            End Set
        End Property

        Private _Nit As System.String = ""
        Public Property Nit() As System.String
            Get
                Return _Nit
            End Get
            Set(ByVal value As System.String)
                _Nit = value
            End Set
        End Property

        Private _Giro As System.String = ""
        Public Property Giro() As System.String
            Get
                Return _Giro
            End Get
            Set(ByVal value As System.String)
                _Giro = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _IdDepartamento As System.String = ""
        Public Property IdDepartamento() As System.String
            Get
                Return _IdDepartamento
            End Get
            Set(ByVal value As System.String)
                _IdDepartamento = value
            End Set
        End Property

        Private _IdMunicipio As System.String = ""
        Public Property IdMunicipio() As System.String
            Get
                Return _IdMunicipio
            End Get
            Set(ByVal value As System.String)
                _IdMunicipio = value
            End Set
        End Property
        Private _Nacionalidad As System.String = ""
        Public Property Nacionalidad() As System.String
            Get
                Return _Nacionalidad
            End Get
            Set(ByVal value As System.String)
                _Nacionalidad = value
            End Set
        End Property

        Private _Telefonos As System.String = ""
        Public Property Telefonos() As System.String
            Get
                Return _Telefonos
            End Get
            Set(ByVal value As System.String)
                _Telefonos = value
            End Set
        End Property

        Private _NumeroFax As System.String = ""
        Public Property NumeroFax() As System.String
            Get
                Return _NumeroFax
            End Get
            Set(ByVal value As System.String)
                _NumeroFax = value
            End Set
        End Property

        Private _CorreoElectronico As System.String = ""
        Public Property CorreoElectronico() As System.String
            Get
                Return _CorreoElectronico
            End Get
            Set(ByVal value As System.String)
                _CorreoElectronico = value
            End Set
        End Property

        Private _Contacto1 As System.String = ""
        Public Property Contacto1() As System.String
            Get
                Return _Contacto1
            End Get
            Set(ByVal value As System.String)
                _Contacto1 = value
            End Set
        End Property

        Private _Contacto2 As System.String = ""
        Public Property Contacto2() As System.String
            Get
                Return _Contacto2
            End Get
            Set(ByVal value As System.String)
                _Contacto2 = value
            End Set
        End Property

        Private _Contacto3 As System.String = ""
        Public Property Contacto3() As System.String
            Get
                Return _Contacto3
            End Get
            Set(ByVal value As System.String)
                _Contacto3 = value
            End Set
        End Property

        Private _IdCuentaContable As System.String = ""
        Public Property IdCuentaContable() As System.String
            Get
                Return _IdCuentaContable
            End Get
            Set(ByVal value As System.String)
                _IdCuentaContable = value
            End Set
        End Property

        Private _LimiteCredito As System.Decimal
        Public Property LimiteCredito() As System.Decimal
            Get
                Return _LimiteCredito
            End Get
            Set(ByVal value As System.Decimal)
                _LimiteCredito = value
            End Set
        End Property

        Private _DiasCredito As System.Int32
        Public Property DiasCredito() As System.Int32
            Get
                Return _DiasCredito
            End Get
            Set(ByVal value As System.Int32)
                _DiasCredito = value
            End Set
        End Property

        Private _Tipo As Integer
        Public Property Tipo() As Integer
            Get
                Return _Tipo
            End Get
            Set(ByVal value As Integer)
                _Tipo = value
            End Set
        End Property

        Private _AplicaRetencion As Boolean
        Public Property AplicaRetencion() As Boolean
            Get
                Return _AplicaRetencion
            End Get
            Set(ByVal value As Boolean)
                _AplicaRetencion = value
            End Set
        End Property

        Private _IdOrigen As Integer
        Public Property IdOrigen() As Integer
            Get
                Return _IdOrigen
            End Get
            Set(ByVal value As Integer)
                _IdOrigen = value
            End Set
        End Property

        Private _CreadoPor As String = ""
        Public Property CreadoPor() As String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property


        Private _NumCasa As System.String = ""
        Public Property NumCasa() As System.String
            Get
                Return _NumCasa
            End Get
            Set(ByVal value As System.String)
                _NumCasa = value
            End Set
        End Property

        Private _ApartamentoLocal As System.String = ""
        Public Property ApartamentoLocal() As System.String
            Get
                Return _ApartamentoLocal
            End Get
            Set(ByVal value As System.String)
                _ApartamentoLocal = value
            End Set
        End Property

        Private _OtrosDomicilio As System.String = ""
        Public Property OtrosDomicilio() As System.String
            Get
                Return _OtrosDomicilio
            End Get
            Set(ByVal value As System.String)
                _OtrosDomicilio = value
            End Set
        End Property

        Private _ColoniaBarrio As System.String = ""
        Public Property ColoniaBarrio() As System.String
            Get
                Return _ColoniaBarrio
            End Get
            Set(ByVal value As System.String)
                _ColoniaBarrio = value
            End Set
        End Property

        Private _NumeroExcluido As System.String = ""
        Public Property NumeroExcluido() As System.String
            Get
                Return _NumeroExcluido
            End Get
            Set(ByVal value As System.String)
                _NumeroExcluido = value
            End Set
        End Property

        Private _RepresentanteLegal As System.String = ""
        Public Property RepresentanteLegal() As System.String
            Get
                Return _RepresentanteLegal
            End Get
            Set(ByVal value As System.String)
                _RepresentanteLegal = value
            End Set
        End Property

        Private _CodPais As System.Int32
        Public Property CodPais() As System.Int32
            Get
                Return _CodPais
            End Get
            Set(ByVal value As System.Int32)
                _CodPais = value
            End Set
        End Property

    End Class
#End Region
#Region "com_GastosImportaciones"
    Public Class com_GastosImportaciones
        Private _IdGasto As System.Int32
        Public Property IdGasto() As System.Int32
            Get
                Return _IdGasto
            End Get
            Set(ByVal value As System.Int32)
                _IdGasto = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _IdCuenta As System.String = ""
        Public Property IdCuenta() As System.String
            Get
                Return _IdCuenta
            End Get
            Set(ByVal value As System.String)
                _IdCuenta = value
            End Set
        End Property

    End Class
#End Region
#Region "com_CajasChica"
    Public Class com_CajasChica
        Private _IdCaja As System.Int32
        Public Property IdCaja() As System.Int32
            Get
                Return _IdCaja
            End Get
            Set(ByVal value As System.Int32)
                _IdCaja = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _IdModuloAplica As System.Int32
        Public Property IdModuloAplica() As System.Int32
            Get
                Return _IdModuloAplica
            End Get
            Set(ByVal value As System.Int32)
                _IdModuloAplica = value
            End Set
        End Property

        Private _ElaboradoPor As System.String = ""
        Public Property ElaboradoPor() As System.String
            Get
                Return _ElaboradoPor
            End Get
            Set(ByVal value As System.String)
                _ElaboradoPor = value
            End Set
        End Property

        Private _AutorizadoPor1 As System.String = ""
        Public Property AutorizadoPor1() As System.String
            Get
                Return _AutorizadoPor1
            End Get
            Set(ByVal value As System.String)
                _AutorizadoPor1 = value
            End Set
        End Property

        Private _AutorizadoPor2 As System.String = ""
        Public Property AutorizadoPor2() As System.String
            Get
                Return _AutorizadoPor2
            End Get
            Set(ByVal value As System.String)
                _AutorizadoPor2 = value
            End Set
        End Property
        Private _IdCuentaContable As System.String = ""
        Public Property IdCuentaContable() As System.String
            Get
                Return _IdCuentaContable
            End Get
            Set(ByVal value As System.String)
                _IdCuentaContable = value
            End Set
        End Property

    End Class
#End Region
#Region "com_Impuestos"
    Public Class com_Impuestos
        Private _IdImpuesto As System.Int32
        Public Property IdImpuesto() As System.Int32
            Get
                Return _IdImpuesto
            End Get
            Set(ByVal value As System.Int32)
                _IdImpuesto = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _IdCuentaContable As System.String = ""
        Public Property IdCuentaContable() As System.String
            Get
                Return _IdCuentaContable
            End Get
            Set(ByVal value As System.String)
                _IdCuentaContable = value
            End Set
        End Property

        Private _TipoCalculo As System.Int32
        Public Property TipoCalculo() As System.Int32
            Get
                Return _TipoCalculo
            End Get
            Set(ByVal value As System.Int32)
                _TipoCalculo = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

    End Class
#End Region
#Region "com_ComprasDetalleImpuestos"
    Public Class com_ComprasDetalleImpuestos
        Private _IdCompra As System.Int32
        Public Property IdCompra() As System.Int32
            Get
                Return _IdCompra
            End Get
            Set(ByVal value As System.Int32)
                _IdCompra = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdImpuesto As System.Int32
        Public Property IdImpuesto() As System.Int32
            Get
                Return _IdImpuesto
            End Get
            Set(ByVal value As System.Int32)
                _IdImpuesto = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

    End Class
#End Region



#Region "com_Requisiciones"
    Public Class com_Requisiciones
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "com_RequisicionesDetalle"
    Public Class com_RequisicionesDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _CantidadAutorizada As System.Decimal
        Public Property CantidadAutorizada() As System.Decimal
            Get
                Return _CantidadAutorizada
            End Get
            Set(ByVal value As System.Decimal)
                _CantidadAutorizada = value
            End Set
        End Property

        Private _Consolidado As System.Boolean
        Public Property Consolidado() As System.Boolean
            Get
                Return _Consolidado
            End Get
            Set(ByVal value As System.Boolean)
                _Consolidado = value
            End Set
        End Property

        Private _UsuarioConsolido As System.String = ""
        Public Property UsuarioConsolido() As System.String
            Get
                Return _UsuarioConsolido
            End Get
            Set(ByVal value As System.String)
                _UsuarioConsolido = value
            End Set
        End Property

        Private _FechaConsolido As Nullable(Of DateTime)
        Public Property FechaConsolido() As Nullable(Of DateTime)
            Get
                Return _FechaConsolido
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaConsolido = value
            End Set
        End Property

    End Class
#End Region

#Region "com_OrdenesCompraDetallePago"
    Public Class com_OrdenesCompraDetallePago
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

    End Class
#End Region

#Region "com_OrdenesCompra"
    Public Class com_OrdenesCompra
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _FacturaAduana As System.String = ""
        Public Property FacturaAduana() As System.String
            Get
                Return _FacturaAduana
            End Get
            Set(ByVal value As System.String)
                _FacturaAduana = value
            End Set
        End Property

        Private _CartaAduana As System.String = ""
        Public Property CartaAduana() As System.String
            Get
                Return _CartaAduana
            End Get
            Set(ByVal value As System.String)
                _CartaAduana = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdProveedor As System.String = ""
        Public Property IdProveedor() As System.String
            Get
                Return _IdProveedor
            End Get
            Set(ByVal value As System.String)
                _IdProveedor = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _Giro As System.String = ""
        Public Property Giro() As System.String
            Get
                Return _Giro
            End Get
            Set(ByVal value As System.String)
                _Giro = value
            End Set
        End Property

        Private _Nit As System.String = ""
        Public Property Nit() As System.String
            Get
                Return _Nit
            End Get
            Set(ByVal value As System.String)
                _Nit = value
            End Set
        End Property

        Private _Nrc As System.String = ""
        Public Property Nrc() As System.String
            Get
                Return _Nrc
            End Get
            Set(ByVal value As System.String)
                _Nrc = value
            End Set
        End Property

        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _IdFormaPago As Int32
        Public Property IdFormaPago() As Int32
            Get
                Return _IdFormaPago
            End Get
            Set(ByVal value As Int32)
                _IdFormaPago = value
            End Set
        End Property

        Private _DiasCredito As Int16
        Public Property DiasCredito() As Int16
            Get
                Return _DiasCredito
            End Get
            Set(ByVal value As Int16)
                _DiasCredito = value
            End Set
        End Property

        Private _IdOrigen As Int32
        Public Property IdOrigen() As Int32
            Get
                Return _IdOrigen
            End Get
            Set(ByVal value As Int32)
                _IdOrigen = value
            End Set
        End Property

        Private _IdTipoCompra As Int32
        Public Property IdTipoCompra() As Int32
            Get
                Return _IdTipoCompra
            End Get
            Set(ByVal value As Int32)
                _IdTipoCompra = value
            End Set
        End Property

        Private _EnviarFacturaA As String = ""
        Public Property EnviarFacturaA() As String
            Get
                Return _EnviarFacturaA
            End Get
            Set(ByVal value As String)
                _EnviarFacturaA = value
            End Set
        End Property

        Private _DireccionEnvioFactura As String = ""
        Public Property DireccionEnvioFactura() As String
            Get
                Return _DireccionEnvioFactura
            End Get
            Set(ByVal value As String)
                _DireccionEnvioFactura = value
            End Set
        End Property
        Private _EmailEnvioFactura As System.String = ""
        Public Property EmailEnvioFactura() As System.String
            Get
                Return _EmailEnvioFactura
            End Get
            Set(ByVal value As System.String)
                _EmailEnvioFactura = value
            End Set
        End Property

        Private _SolicitadoPor As System.String = ""
        Public Property SolicitadoPor() As System.String
            Get
                Return _SolicitadoPor
            End Get
            Set(ByVal value As System.String)
                _SolicitadoPor = value
            End Set
        End Property

        Private _LugarEntrega As System.String = ""
        Public Property LugarEntrega() As System.String
            Get
                Return _LugarEntrega
            End Get
            Set(ByVal value As System.String)
                _LugarEntrega = value
            End Set
        End Property

        Private _DireccionEntrega As System.String = ""
        Public Property DireccionEntrega() As System.String
            Get
                Return _DireccionEntrega
            End Get
            Set(ByVal value As System.String)
                _DireccionEntrega = value
            End Set
        End Property

        Private _Iva As System.Decimal
        Public Property Iva() As System.Decimal
            Get
                Return _Iva
            End Get
            Set(ByVal value As System.Decimal)
                _Iva = value
            End Set
        End Property

        Private _TotalImpuesto1 As System.Decimal
        Public Property TotalImpuesto1() As System.Decimal
            Get
                Return _TotalImpuesto1
            End Get
            Set(ByVal value As System.Decimal)
                _TotalImpuesto1 = value
            End Set
        End Property
        Private _TotalImpuesto2 As System.Decimal
        Public Property TotalImpuesto2() As System.Decimal
            Get
                Return _TotalImpuesto2
            End Get
            Set(ByVal value As System.Decimal)
                _TotalImpuesto2 = value
            End Set
        End Property

        Private _Total As System.Decimal
        Public Property Total() As System.Decimal
            Get
                Return _Total
            End Get
            Set(ByVal value As System.Decimal)
                _Total = value
            End Set
        End Property


        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _Aprobada As System.Boolean
        Public Property Aprobada() As System.Boolean
            Get
                Return _Aprobada
            End Get
            Set(ByVal value As System.Boolean)
                _Aprobada = value
            End Set
        End Property

        Private _AprobadaPor As System.String = ""
        Public Property AprobadaPor() As System.String
            Get
                Return _AprobadaPor
            End Get
            Set(ByVal value As System.String)
                _AprobadaPor = value
            End Set
        End Property

        Private _ComentarioAprobacion As System.String = ""
        Public Property ComentarioAprobacion() As System.String
            Get
                Return _ComentarioAprobacion
            End Get
            Set(ByVal value As System.String)
                _ComentarioAprobacion = value
            End Set
        End Property
        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region


#Region "com_OrdenesCompraDetalle"
    Public Class com_OrdenesCompraDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioReferencia As Decimal
        Public Property PrecioReferencia() As Decimal
            Get
                Return _PrecioReferencia
            End Get
            Set(ByVal value As Decimal)
                _PrecioReferencia = value
            End Set
        End Property

        Private _PorcDescto As Decimal
        Public Property PorcDescto() As Decimal
            Get
                Return _PorcDescto
            End Get
            Set(ByVal value As Decimal)
                _PorcDescto = value
            End Set
        End Property
        
        Private _ValorDescto As Decimal
        Public Property ValorDescto() As Decimal
            Get
                Return _ValorDescto
            End Get
            Set(ByVal value As Decimal)
                _ValorDescto = value
            End Set
        End Property

        Private _PrecioUnitario As Decimal
        Public Property PrecioUnitario() As Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _ValorExento As System.Decimal
        Public Property ValorExento() As System.Decimal
            Get
                Return _ValorExento
            End Get
            Set(ByVal value As System.Decimal)
                _ValorExento = value
            End Set
        End Property

        Private _ValorAfecto As System.Decimal
        Public Property ValorAfecto() As System.Decimal
            Get
                Return _ValorAfecto
            End Get
            Set(ByVal value As System.Decimal)
                _ValorAfecto = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "com_Compras"
    Public Class com_Compras
        Private _IdComprobante As Integer
        Public Property IdComprobante() As Integer
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As Integer)
                _IdComprobante = value
            End Set
        End Property

        Private _IdTipoComprobante As Integer
        Public Property IdTipoComprobante() As Integer
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As Integer)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _Correlativo As Integer
        Public Property Correlativo() As Integer
            Get
                Return _Correlativo
            End Get
            Set(ByVal value As Integer)
                _Correlativo = value
            End Set
        End Property

        Private _Serie As String = ""
        Public Property Serie() As String
            Get
                Return _Serie
            End Get
            Set(ByVal value As String)
                _Serie = value
            End Set
        End Property

        Private _Numero As String = ""
        Public Property Numero() As String
            Get
                Return _Numero
            End Get
            Set(ByVal value As String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _FechaContable As Nullable(Of DateTime)
        Public Property FechaContable() As Nullable(Of DateTime)
            Get
                Return _FechaContable
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaContable = value
            End Set
        End Property

        Private _IdProveedor As System.String = ""
        Public Property IdProveedor() As System.String
            Get
                Return _IdProveedor
            End Get
            Set(ByVal value As System.String)
                _IdProveedor = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Nrc As String = ""
        Public Property Nrc() As String
            Get
                Return _Nrc
            End Get
            Set(ByVal value As String)
                _Nrc = value
            End Set
        End Property

        Private _Nit As String = ""
        Public Property Nit() As String
            Get
                Return _Nit
            End Get
            Set(ByVal value As String)
                _Nit = value
            End Set
        End Property


        Private _Giro As String = ""
        Public Property Giro() As String
            Get
                Return _Giro
            End Get
            Set(ByVal value As String)
                _Giro = value
            End Set
        End Property


        Private _Direccion As String = ""
        Public Property Direccion() As String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As String)
                _Direccion = value
            End Set
        End Property

        Private _IdFormaPago As System.Int32
        Public Property IdFormaPago() As System.Int32
            Get
                Return _IdFormaPago
            End Get
            Set(ByVal value As System.Int32)
                _IdFormaPago = value
            End Set
        End Property

        Private _NumOrdenCompra As System.String = ""
        Public Property NumOrdenCompra() As System.String
            Get
                Return _NumOrdenCompra
            End Get
            Set(ByVal value As System.String)
                _NumOrdenCompra = value
            End Set
        End Property

        Private _DiasCredito As System.Int32
        Public Property DiasCredito() As System.Int32
            Get
                Return _DiasCredito
            End Get
            Set(ByVal value As System.Int32)
                _DiasCredito = value
            End Set
        End Property

        Private _FechaVencimiento As DateTime
        Public Property FechaVencimiento() As DateTime
            Get
                Return _FechaVencimiento
            End Get
            Set(ByVal value As DateTime)
                _FechaVencimiento = value
            End Set
        End Property

        Private _IdTipoCompra As System.Int32
        Public Property IdTipoCompra() As System.Int32
            Get
                Return _IdTipoCompra
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoCompra = value
            End Set
        End Property

        Private _IdOrigenCompra As System.Int32
        Public Property IdOrigenCompra() As System.Int32
            Get
                Return _IdOrigenCompra
            End Get
            Set(ByVal value As System.Int32)
                _IdOrigenCompra = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property
        Private _IdCaja As System.Int32
        Public Property IdCaja() As System.Int32
            Get
                Return _IdCaja
            End Get
            Set(ByVal value As System.Int32)
                _IdCaja = value
            End Set
        End Property

        Private _CompraExcluido As System.Boolean
        Public Property CompraExcluido() As System.Boolean
            Get
                Return _CompraExcluido
            End Get
            Set(ByVal value As System.Boolean)
                _CompraExcluido = value
            End Set
        End Property

        Private _NumDocExcluido As System.String = ""
        Public Property NumDocExcluido() As System.String
            Get
                Return _NumDocExcluido
            End Get
            Set(ByVal value As System.String)
                _NumDocExcluido = value
            End Set
        End Property

        Private _ExcluirLibro As System.Boolean
        Public Property ExcluirLibro() As System.Boolean
            Get
                Return _ExcluirLibro
            End Get
            Set(ByVal value As System.Boolean)
                _ExcluirLibro = value
            End Set
        End Property

        Private _TotalExento As System.Decimal
        Public Property TotalExento() As System.Decimal
            Get
                Return _TotalExento
            End Get
            Set(ByVal value As System.Decimal)
                _TotalExento = value
            End Set
        End Property

        Private _TotalAfecto As System.Decimal
        Public Property TotalAfecto() As System.Decimal
            Get
                Return _TotalAfecto
            End Get
            Set(ByVal value As System.Decimal)
                _TotalAfecto = value
            End Set
        End Property

        Private _TotalIva As System.Decimal
        Public Property TotalIva() As System.Decimal
            Get
                Return _TotalIva
            End Get
            Set(ByVal value As System.Decimal)
                _TotalIva = value
            End Set
        End Property
        '1= retención, 2=percepción, 3= n/a
        Private _TipoImpuestoAdicional As Integer
        Public Property TipoImpuestoAdicional() As Integer
            Get
                Return _TipoImpuestoAdicional
            End Get
            Set(ByVal value As Integer)
                _TipoImpuestoAdicional = value
            End Set
        End Property

        Private _TotalImpuesto1 As System.Decimal
        Public Property TotalImpuesto1() As System.Decimal
            Get
                Return _TotalImpuesto1
            End Get
            Set(ByVal value As System.Decimal)
                _TotalImpuesto1 = value
            End Set
        End Property

        Private _TotalImpuesto2 As System.Decimal
        Public Property TotalImpuesto2() As System.Decimal
            Get
                Return _TotalImpuesto2
            End Get
            Set(ByVal value As System.Decimal)
                _TotalImpuesto2 = value
            End Set
        End Property

        Private _TotalComprobante As System.Decimal
        Public Property TotalComprobante() As System.Decimal
            Get
                Return _TotalComprobante
            End Get
            Set(ByVal value As System.Decimal)
                _TotalComprobante = value
            End Set
        End Property

        Private _AplicadaInventario As Boolean
        Public Property AplicadaInventario() As Boolean
            Get
                Return _AplicadaInventario
            End Get
            Set(ByVal value As Boolean)
                _AplicadaInventario = value
            End Set
        End Property

        Private _Contabilizada As Boolean
        Public Property Contabilizada() As Boolean
            Get
                Return _Contabilizada
            End Get
            Set(ByVal value As Boolean)
                _Contabilizada = value
            End Set
        End Property
        Private _AfectaCosto As System.Boolean
        Public Property AfectaCosto() As System.Boolean
            Get
                Return _AfectaCosto
            End Get
            Set(ByVal value As System.Boolean)
                _AfectaCosto = value
            End Set
        End Property
        Private _IdCheque As System.Int32
        Public Property IdCheque() As System.Int32
            Get
                Return _IdCheque
            End Get
            Set(ByVal value As System.Int32)
                _IdCheque = value
            End Set
        End Property
        Private _IdPartida As System.Int32
        Public Property IdPartida() As System.Int32
            Get
                Return _IdPartida
            End Get
            Set(ByVal value As System.Int32)
                _IdPartida = value
            End Set
        End Property
        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "com_ComprasDetalle"
    Public Class com_ComprasDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As Int32
        Public Property IdDetalle() As Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _TipoImpuesto As Integer
        Public Property TipoImpuesto() As Integer
            Get
                Return _TipoImpuesto
            End Get
            Set(ByVal value As Integer)
                _TipoImpuesto = value
            End Set
        End Property

        Private _PrecioReferencia As Decimal
        Public Property PrecioReferencia() As Decimal
            Get
                Return _PrecioReferencia
            End Get
            Set(ByVal value As Decimal)
                _PrecioReferencia = value
            End Set
        End Property

        Private _PorcDescto As System.Decimal
        Public Property PorcDescto() As System.Decimal
            Get
                Return _PorcDescto
            End Get
            Set(ByVal value As System.Decimal)
                _PorcDescto = value
            End Set
        End Property

        Private _ValorDescto As System.Decimal
        Public Property ValorDescto() As System.Decimal
            Get
                Return _ValorDescto
            End Get
            Set(ByVal value As System.Decimal)
                _ValorDescto = value
            End Set
        End Property
        
        Private _PrecioUnitario As Decimal
        Public Property PrecioUnitario() As Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _ValorExento As Decimal
        Public Property ValorExento() As Decimal
            Get
                Return _ValorExento
            End Get
            Set(ByVal value As Decimal)
                _ValorExento = value
            End Set
        End Property

        Private _ValorAfecto As System.Decimal
        Public Property ValorAfecto() As System.Decimal
            Get
                Return _ValorAfecto
            End Get
            Set(ByVal value As System.Decimal)
                _ValorAfecto = value
            End Set
        End Property

        Private _IdCuenta As System.String = ""
        Public Property IdCuenta() As System.String
            Get
                Return _IdCuenta
            End Get
            Set(ByVal value As System.String)
                _IdCuenta = value
            End Set
        End Property


        Private _IdCentro As System.String = ""
        Public Property IdCentro() As System.String
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.String)
                _IdCentro = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "com_Importaciones"
    Public Class com_Importaciones
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _NumeroFactura As System.String = ""
        Public Property NumeroFactura() As System.String
            Get
                Return _NumeroFactura
            End Get
            Set(ByVal value As System.String)
                _NumeroFactura = value
            End Set
        End Property

        Private _NumeroPoliza As System.String = ""
        Public Property NumeroPoliza() As System.String
            Get
                Return _NumeroPoliza
            End Get
            Set(ByVal value As System.String)
                _NumeroPoliza = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _FechaContable As Nullable(Of DateTime)
        Public Property FechaContable() As Nullable(Of DateTime)
            Get
                Return _FechaContable
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaContable = value
            End Set
        End Property

        Private _IdProveedor As System.String = ""
        Public Property IdProveedor() As System.String
            Get
                Return _IdProveedor
            End Get
            Set(ByVal value As System.String)
                _IdProveedor = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _DaiCalculado As System.Boolean
        Public Property DaiCalculado() As System.Boolean
            Get
                Return _DaiCalculado
            End Get
            Set(ByVal value As System.Boolean)
                _DaiCalculado = value
            End Set
        End Property

        Private _ValorDai As System.Decimal
        Public Property ValorDai() As System.Decimal
            Get
                Return _ValorDai
            End Get
            Set(ByVal value As System.Decimal)
                _ValorDai = value
            End Set
        End Property

        Private _ValorSeguros As System.Decimal
        Public Property ValorSeguros() As System.Decimal
            Get
                Return _ValorSeguros
            End Get
            Set(ByVal value As System.Decimal)
                _ValorSeguros = value
            End Set
        End Property

        Private _ValorTransporte As System.Decimal
        Public Property ValorTransporte() As System.Decimal
            Get
                Return _ValorTransporte
            End Get
            Set(ByVal value As System.Decimal)
                _ValorTransporte = value
            End Set
        End Property

        Private _ValorGastosImportacion As System.Decimal
        Public Property ValorGastosImportacion() As System.Decimal
            Get
                Return _ValorGastosImportacion
            End Get
            Set(ByVal value As System.Decimal)
                _ValorGastosImportacion = value
            End Set
        End Property

        Private _ValorGastosExternos As System.Decimal
        Public Property ValorGastosExternos() As System.Decimal
            Get
                Return _ValorGastosExternos
            End Get
            Set(ByVal value As System.Decimal)
                _ValorGastosExternos = value
            End Set
        End Property

        Private _ValorGastosInternos As System.Decimal
        Public Property ValorGastosInternos() As System.Decimal
            Get
                Return _ValorGastosInternos
            End Get
            Set(ByVal value As System.Decimal)
                _ValorGastosInternos = value
            End Set
        End Property

        Private _ValorOtrosGastos As System.Decimal
        Public Property ValorOtrosGastos() As System.Decimal
            Get
                Return _ValorOtrosGastos
            End Get
            Set(ByVal value As System.Decimal)
                _ValorOtrosGastos = value
            End Set
        End Property

        Private _ValorSujetoDeclaracion As System.Decimal
        Public Property ValorSujetoDeclaracion() As System.Decimal
            Get
                Return _ValorSujetoDeclaracion
            End Get
            Set(ByVal value As System.Decimal)
                _ValorSujetoDeclaracion = value
            End Set
        End Property
        Private _ValorExento As Decimal
        Public Property ValorExento() As Decimal
            Get
                Return _ValorExento
            End Get
            Set(ByVal value As Decimal)
                _ValorExento = value
            End Set
        End Property
        Private _ValorIva As System.Decimal
        Public Property ValorIva() As System.Decimal
            Get
                Return _ValorIva
            End Get
            Set(ByVal value As System.Decimal)
                _ValorIva = value
            End Set
        End Property

        Private _ValorImpuesto1 As System.Decimal
        Public Property ValorImpuesto1() As System.Decimal
            Get
                Return _ValorImpuesto1
            End Get
            Set(ByVal value As System.Decimal)
                _ValorImpuesto1 = value
            End Set
        End Property

        Private _ValorImpuesto2 As System.Decimal
        Public Property ValorImpuesto2() As System.Decimal
            Get
                Return _ValorImpuesto2
            End Get
            Set(ByVal value As System.Decimal)
                _ValorImpuesto2 = value
            End Set
        End Property

        Private _PorcDescuento As System.Decimal
        Public Property PorcDescuento() As System.Decimal
            Get
                Return _PorcDescuento
            End Get
            Set(ByVal value As System.Decimal)
                _PorcDescuento = value
            End Set
        End Property

        Private _TotalPoliza As System.Decimal
        Public Property TotalPoliza() As System.Decimal
            Get
                Return _TotalPoliza
            End Get
            Set(ByVal value As System.Decimal)
                _TotalPoliza = value
            End Set
        End Property

        Private _TotalPorPagar As System.Decimal
        Public Property TotalPorPagar() As System.Decimal
            Get
                Return _TotalPorPagar
            End Get
            Set(ByVal value As System.Decimal)
                _TotalPorPagar = value
            End Set
        End Property

        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _TipoImportacion As System.Int32
        Public Property TipoImportacion() As System.Int32
            Get
                Return _TipoImportacion
            End Get
            Set(ByVal value As System.Int32)
                _TipoImportacion = value
            End Set
        End Property

        Private _DiasCredito As System.Byte
        Public Property DiasCredito() As System.Byte
            Get
                Return _DiasCredito
            End Get
            Set(ByVal value As System.Byte)
                _DiasCredito = value
            End Set
        End Property

        Private _AplicadaInventario As System.Boolean
        Public Property AplicadaInventario() As System.Boolean
            Get
                Return _AplicadaInventario
            End Get
            Set(ByVal value As System.Boolean)
                _AplicadaInventario = value
            End Set
        End Property
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _IdPartida As System.Int32
        Public Property IdPartida() As System.Int32
            Get
                Return _IdPartida
            End Get
            Set(ByVal value As System.Int32)
                _IdPartida = value
            End Set
        End Property
        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

        Private _IdAduana As System.Int32
        Public Property IdAduana() As System.Int32
            Get
                Return _IdAduana
            End Get
            Set(ByVal value As System.Int32)
                _IdAduana = value
            End Set
        End Property

        Private _TipoDocumento As System.Int32
        Public Property TipoDocumento() As System.Int32
            Get
                Return _TipoDocumento
            End Get
            Set(ByVal value As System.Int32)
                _TipoDocumento = value
            End Set
        End Property

    End Class
#End Region


#Region "com_ImportacionesDetalle"
    Public Class com_ImportacionesDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _Descripcion As System.String
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _Referencia As System.String
        Public Property Referencia() As System.String
            Get
                Return _Referencia
            End Get
            Set(ByVal value As System.String)
                _Referencia = value
            End Set
        End Property

        Private _ValorDai As System.Decimal
        Public Property ValorDai() As System.Decimal
            Get
                Return _ValorDai
            End Get
            Set(ByVal value As System.Decimal)
                _ValorDai = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property
        Private _PrecioVentaAct As System.Decimal
        Public Property PrecioVentaAct() As System.Decimal
            Get
                Return _PrecioVentaAct
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioVentaAct = value
            End Set
        End Property

        Private _PrecioVentaCal As System.Decimal
        Public Property PrecioVentaCal() As System.Decimal
            Get
                Return _PrecioVentaCal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioVentaCal = value
            End Set
        End Property

        Private _PrecioVentaNuevo As System.Decimal
        Public Property PrecioVentaNuevo() As System.Decimal
            Get
                Return _PrecioVentaNuevo
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioVentaNuevo = value
            End Set
        End Property
        Private _PorcComision As System.Decimal
        Public Property PorcComision() As System.Decimal
            Get
                Return _PorcComision
            End Get
            Set(ByVal value As System.Decimal)
                _PorcComision = value
            End Set
        End Property
        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property


        Private _IdCentro As System.String
        Public Property IdCentro() As System.String
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.String)
                _IdCentro = value
            End Set
        End Property

        Private _CreadoPor As System.String
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "com_ImportacionesGastosDetalle"
    Public Class com_ImportacionesGastosDetalle
        Private _IdImportacion As System.Int32
        Public Property IdImportacion() As System.Int32
            Get
                Return _IdImportacion
            End Get
            Set(ByVal value As System.Int32)
                _IdImportacion = value
            End Set
        End Property

        Private _IdGasto As System.Int32
        Public Property IdGasto() As System.Int32
            Get
                Return _IdGasto
            End Get
            Set(ByVal value As System.Int32)
                _IdGasto = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property
        Private _Contabilizar As System.Boolean
        Public Property Contabilizar() As System.Boolean
            Get
                Return _Contabilizar
            End Get
            Set(ByVal value As System.Boolean)
                _Contabilizar = value
            End Set
        End Property

    End Class
#End Region
#Region "com_ImportacionesProveedores"
    Public Class com_ImportacionesProveedores
        Private _IdComprobante As Int32
        Public Property IdComprobante() As Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As Int32
        Public Property IdDetalle() As Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdImportacion As Int32
        Public Property IdImportacion() As Int32
            Get
                Return _IdImportacion
            End Get
            Set(ByVal value As Int32)
                _IdImportacion = value
            End Set
        End Property

        Private _IdProveedor As String = ""
        Public Property IdProveedor() As String
            Get
                Return _IdProveedor
            End Get
            Set(ByVal value As String)
                _IdProveedor = value
            End Set
        End Property

        Private _NumeroComprobante As String = ""
        Public Property NumeroComprobante() As String
            Get
                Return _NumeroComprobante
            End Get
            Set(ByVal value As String)
                _NumeroComprobante = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _FechaContable As Nullable(Of DateTime)
        Public Property FechaContable() As Nullable(Of DateTime)
            Get
                Return _FechaContable
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaContable = value
            End Set
        End Property

        Private _IdFormaPago As Int32
        Public Property IdFormaPago() As Int32
            Get
                Return _IdFormaPago
            End Get
            Set(ByVal value As Int32)
                _IdFormaPago = value
            End Set
        End Property

        Private _DiasCredito As Int32
        Public Property DiasCredito() As Int32
            Get
                Return _DiasCredito
            End Get
            Set(ByVal value As Int32)
                _DiasCredito = value
            End Set
        End Property

        Private _Valor As Decimal
        Public Property Valor() As Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As Decimal)
                _Valor = value
            End Set
        End Property

        Private _Contabilizar As Boolean
        Public Property Contabilizar() As Boolean
            Get
                Return _Contabilizar
            End Get
            Set(ByVal value As Boolean)
                _Contabilizar = value
            End Set
        End Property
    End Class
#End Region
#Region "com_ImportacionesGastosProductos"
    Public Class com_ImportacionesGastosProductos
        Private _IdImportacion As System.Int32
        Public Property IdImportacion() As System.Int32
            Get
                Return _IdImportacion
            End Get
            Set(ByVal value As System.Int32)
                _IdImportacion = value
            End Set
        End Property

        Private _Referencia As System.String = ""
        Public Property Referencia() As System.String
            Get
                Return _Referencia
            End Get
            Set(ByVal value As System.String)
                _Referencia = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _IdGasto As System.Int32
        Public Property IdGasto() As System.Int32
            Get
                Return _IdGasto
            End Get
            Set(ByVal value As System.Int32)
                _IdGasto = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _AplicaGasto As System.Boolean
        Public Property AplicaGasto() As System.Boolean
            Get
                Return _AplicaGasto
            End Get
            Set(ByVal value As System.Boolean)
                _AplicaGasto = value
            End Set
        End Property

    End Class
#End Region
#Region "com_Liquidacion"
    Public Class com_Liquidacion
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Serie As System.String = ""
        Public Property Serie() As System.String
            Get
                Return _Serie
            End Get
            Set(ByVal value As System.String)
                _Serie = value
            End Set
        End Property
        Private _SerieCF As System.String = ""
        Public Property SerieCF() As System.String
            Get
                Return _SerieCF
            End Get
            Set(ByVal value As System.String)
                _SerieCF = value
            End Set
        End Property


        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property
        Private _NumeroCF As System.String = ""
        Public Property NumeroCF() As System.String
            Get
                Return _NumeroCF
            End Get
            Set(ByVal value As System.String)
                _NumeroCF = value
            End Set
        End Property

        Private _Resolucion As System.String = ""
        Public Property Resolucion() As System.String
            Get
                Return _Resolucion
            End Get
            Set(ByVal value As System.String)
                _Resolucion = value
            End Set
        End Property

        Private _ResolucionCF As System.String = ""
        Public Property ResolucionCF() As System.String
            Get
                Return _ResolucionCF
            End Get
            Set(ByVal value As System.String)
                _ResolucionCF = value
            End Set
        End Property

        Private _NumControlInterno As System.String = ""
        Public Property NumControlInterno() As System.String
            Get
                Return _NumControlInterno
            End Get
            Set(ByVal value As System.String)
                _NumControlInterno = value
            End Set
        End Property

        Private _NumControlInternoCF As System.String = ""
        Public Property NumControlInternoCF() As System.String
            Get
                Return _NumControlInternoCF
            End Get
            Set(ByVal value As System.String)
                _NumControlInternoCF = value
            End Set
        End Property

        Private _ClaseDocumento As System.Int32
        Public Property ClaseDocumento() As System.Int32
            Get
                Return _ClaseDocumento
            End Get
            Set(ByVal value As System.Int32)
                _ClaseDocumento = value
            End Set
        End Property

        Private _ClaseDocumentoCF As System.Int32
        Public Property ClaseDocumentoCF() As System.Int32
            Get
                Return _ClaseDocumentoCF
            End Get
            Set(ByVal value As System.Int32)
                _ClaseDocumentoCF = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _FechaContable As Nullable(Of DateTime)
        Public Property FechaContable() As Nullable(Of DateTime)
            Get
                Return _FechaContable
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaContable = value
            End Set
        End Property
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _IdCuentaBancaria As System.Int32
        Public Property IdCuentaBancaria() As System.Int32
            Get
                Return _IdCuentaBancaria
            End Get
            Set(ByVal value As System.Int32)
                _IdCuentaBancaria = value
            End Set
        End Property
        Private _IdProveedor As System.String = ""
        Public Property IdProveedor() As System.String
            Get
                Return _IdProveedor
            End Get
            Set(ByVal value As System.String)
                _IdProveedor = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Nrc As System.String = ""
        Public Property Nrc() As System.String
            Get
                Return _Nrc
            End Get
            Set(ByVal value As System.String)
                _Nrc = value
            End Set
        End Property

        Private _Nit As System.String = ""
        Public Property Nit() As System.String
            Get
                Return _Nit
            End Get
            Set(ByVal value As System.String)
                _Nit = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _TotalIva As System.Decimal
        Public Property TotalIva() As System.Decimal
            Get
                Return _TotalIva
            End Get
            Set(ByVal value As System.Decimal)
                _TotalIva = value
            End Set
        End Property

        Private _TotalComprobante As System.Decimal
        Public Property TotalComprobante() As System.Decimal
            Get
                Return _TotalComprobante
            End Get
            Set(ByVal value As System.Decimal)
                _TotalComprobante = value
            End Set
        End Property

        Private _TotalExento As System.Decimal
        Public Property TotalExento() As System.Decimal
            Get
                Return _TotalExento
            End Get
            Set(ByVal value As System.Decimal)
                _TotalExento = value
            End Set
        End Property
        Private _Comision As System.Decimal
        Public Property Comision() As System.Decimal
            Get
                Return _Comision
            End Get
            Set(ByVal value As System.Decimal)
                _Comision = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

        Private _IvaComision As System.Decimal
        Public Property IvaComision() As System.Decimal
            Get
                Return _IvaComision
            End Get
            Set(ByVal value As System.Decimal)
                _IvaComision = value
            End Set
        End Property

    End Class
#End Region

#Region "com_Retenciones"
    Public Class com_Retenciones
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Serie As System.String = ""
        Public Property Serie() As System.String
            Get
                Return _Serie
            End Get
            Set(ByVal value As System.String)
                _Serie = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _FechaContable As Nullable(Of DateTime)
        Public Property FechaContable() As Nullable(Of DateTime)
            Get
                Return _FechaContable
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaContable = value
            End Set
        End Property

        Private _IdClienteProveedor As System.String = ""
        Public Property IdClienteProveedor() As System.String
            Get
                Return _IdClienteProveedor
            End Get
            Set(ByVal value As System.String)
                _IdClienteProveedor = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _Nrc As System.String = ""
        Public Property Nrc() As System.String
            Get
                Return _Nrc
            End Get
            Set(ByVal value As System.String)
                _Nrc = value
            End Set
        End Property

        Private _Nit As System.String = ""
        Public Property Nit() As System.String
            Get
                Return _Nit
            End Get
            Set(ByVal value As System.String)
                _Nit = value
            End Set
        End Property

        Private _PorcentajeIva As System.Decimal
        Public Property PorcentajeIva() As System.Decimal
            Get
                Return _PorcentajeIva
            End Get
            Set(ByVal value As System.Decimal)
                _PorcentajeIva = value
            End Set
        End Property

        Private _PorcentajeRet As System.Decimal
        Public Property PorcentajeRet() As System.Decimal
            Get
                Return _PorcentajeRet
            End Get
            Set(ByVal value As System.Decimal)
                _PorcentajeRet = value
            End Set
        End Property

        Private _TotalIva As System.Decimal
        Public Property TotalIva() As System.Decimal
            Get
                Return _TotalIva
            End Get
            Set(ByVal value As System.Decimal)
                _TotalIva = value
            End Set
        End Property

        Private _TotalImpuesto1 As System.Decimal
        Public Property TotalImpuesto1() As System.Decimal
            Get
                Return _TotalImpuesto1
            End Get
            Set(ByVal value As System.Decimal)
                _TotalImpuesto1 = value
            End Set
        End Property

        Private _TotalImpuesto2 As System.Decimal
        Public Property TotalImpuesto2() As System.Decimal
            Get
                Return _TotalImpuesto2
            End Get
            Set(ByVal value As System.Decimal)
                _TotalImpuesto2 = value
            End Set
        End Property

        Private _TotalComprobante As System.Decimal
        Public Property TotalComprobante() As System.Decimal
            Get
                Return _TotalComprobante
            End Get
            Set(ByVal value As System.Decimal)
                _TotalComprobante = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region

#Region "com_RetencionesDetalle"
    Public Class com_RetencionesDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property

    End Class
#End Region

#Region "con_Presupuestos"
    Public Class con_Presupuestos
        Private _IdPresupuesto As System.Int32
        Public Property IdPresupuesto() As System.Int32
            Get
                Return _IdPresupuesto
            End Get
            Set(ByVal value As System.Int32)
                _IdPresupuesto = value
            End Set
        End Property

        Private _Periodo As System.Int32
        Public Property Periodo() As System.Int32
            Get
                Return _Periodo
            End Get
            Set(ByVal value As System.Int32)
                _Periodo = value
            End Set
        End Property

        Private _Comentario As System.String = ""
        Public Property Comentario() As System.String
            Get
                Return _Comentario
            End Get
            Set(ByVal value As System.String)
                _Comentario = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region

#Region "con_PresupuestosDetalle"
    Public Class con_PresupuestosDetalle
        Private _IdPresupuesto As System.Int32
        Public Property IdPresupuesto() As System.Int32
            Get
                Return _IdPresupuesto
            End Get
            Set(ByVal value As System.Int32)
                _IdPresupuesto = value
            End Set
        End Property

        Private _IdCuenta As System.String = ""
        Public Property IdCuenta() As System.String
            Get
                Return _IdCuenta
            End Get
            Set(ByVal value As System.String)
                _IdCuenta = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Mes01 As System.Decimal
        Public Property Mes01() As System.Decimal
            Get
                Return _Mes01
            End Get
            Set(ByVal value As System.Decimal)
                _Mes01 = value
            End Set
        End Property

        Private _Mes02 As System.Decimal
        Public Property Mes02() As System.Decimal
            Get
                Return _Mes02
            End Get
            Set(ByVal value As System.Decimal)
                _Mes02 = value
            End Set
        End Property

        Private _Mes03 As System.Decimal
        Public Property Mes03() As System.Decimal
            Get
                Return _Mes03
            End Get
            Set(ByVal value As System.Decimal)
                _Mes03 = value
            End Set
        End Property

        Private _Mes04 As System.Decimal
        Public Property Mes04() As System.Decimal
            Get
                Return _Mes04
            End Get
            Set(ByVal value As System.Decimal)
                _Mes04 = value
            End Set
        End Property

        Private _Mes05 As System.Decimal
        Public Property Mes05() As System.Decimal
            Get
                Return _Mes05
            End Get
            Set(ByVal value As System.Decimal)
                _Mes05 = value
            End Set
        End Property

        Private _Mes06 As System.Decimal
        Public Property Mes06() As System.Decimal
            Get
                Return _Mes06
            End Get
            Set(ByVal value As System.Decimal)
                _Mes06 = value
            End Set
        End Property

        Private _Mes07 As System.Decimal
        Public Property Mes07() As System.Decimal
            Get
                Return _Mes07
            End Get
            Set(ByVal value As System.Decimal)
                _Mes07 = value
            End Set
        End Property

        Private _Mes08 As System.Decimal
        Public Property Mes08() As System.Decimal
            Get
                Return _Mes08
            End Get
            Set(ByVal value As System.Decimal)
                _Mes08 = value
            End Set
        End Property

        Private _Mes09 As System.Decimal
        Public Property Mes09() As System.Decimal
            Get
                Return _Mes09
            End Get
            Set(ByVal value As System.Decimal)
                _Mes09 = value
            End Set
        End Property

        Private _Mes10 As System.Decimal
        Public Property Mes10() As System.Decimal
            Get
                Return _Mes10
            End Get
            Set(ByVal value As System.Decimal)
                _Mes10 = value
            End Set
        End Property

        Private _Mes11 As System.Decimal
        Public Property Mes11() As System.Decimal
            Get
                Return _Mes11
            End Get
            Set(ByVal value As System.Decimal)
                _Mes11 = value
            End Set
        End Property

        Private _Mes12 As System.Decimal
        Public Property Mes12() As System.Decimal
            Get
                Return _Mes12
            End Get
            Set(ByVal value As System.Decimal)
                _Mes12 = value
            End Set
        End Property

    End Class
#End Region



#Region "com_TiposDocumento"
    Public Class com_TiposDocumento
        Private _IdTipo As System.Byte
        Public Property IdTipo() As System.Byte
            Get
                Return _IdTipo
            End Get
            Set(ByVal value As System.Byte)
                _IdTipo = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _TipoAplicacion As System.Byte
        Public Property TipoAplicacion() As System.Byte
            Get
                Return _TipoAplicacion
            End Get
            Set(ByVal value As System.Byte)
                _TipoAplicacion = value
            End Set
        End Property

        Private _AplicaLibro As System.Boolean
        Public Property AplicaLibro() As System.Boolean
            Get
                Return _AplicaLibro
            End Get
            Set(ByVal value As System.Boolean)
                _AplicaLibro = value
            End Set
        End Property

    End Class
#End Region
#Region "com_NotasCredito"
    Public Class com_NotasCredito
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _NumComprobanteCompra As System.String = ""
        Public Property NumComprobanteCompra() As System.String
            Get
                Return _NumComprobanteCompra
            End Get
            Set(ByVal value As System.String)
                _NumComprobanteCompra = value
            End Set
        End Property

        Private _MontoAbonado As System.Decimal
        Public Property MontoAbonado() As System.Decimal
            Get
                Return _MontoAbonado
            End Get
            Set(ByVal value As System.Decimal)
                _MontoAbonado = value
            End Set
        End Property

        Private _SaldoActual As System.Decimal
        Public Property SaldoActual() As System.Decimal
            Get
                Return _SaldoActual
            End Get
            Set(ByVal value As System.Decimal)
                _SaldoActual = value
            End Set
        End Property

        Private _IdComprobanteCompra As System.Int32
        Public Property IdComprobanteCompra() As System.Int32
            Get
                Return _IdComprobanteCompra
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobanteCompra = value
            End Set
        End Property

    End Class
#End Region

#Region "fac_Clientes"
    Public Class fac_Clientes
        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _RazonSocial As System.String = ""
        Public Property RazonSocial() As System.String
            Get
                Return _RazonSocial
            End Get
            Set(ByVal value As System.String)
                _RazonSocial = value
            End Set
        End Property

        Private _Nrc As System.String = ""
        Public Property Nrc() As System.String
            Get
                Return _Nrc
            End Get
            Set(ByVal value As System.String)
                _Nrc = value
            End Set
        End Property

        Private _Nit As System.String = ""
        Public Property Nit() As System.String
            Get
                Return _Nit
            End Get
            Set(ByVal value As System.String)
                _Nit = value
            End Set
        End Property

        Private _OtroDocumento As System.String = ""
        Public Property OtroDocumento() As System.String
            Get
                Return _OtroDocumento
            End Get
            Set(ByVal value As System.String)
                _OtroDocumento = value
            End Set
        End Property

        Private _Giro As System.String = ""
        Public Property Giro() As System.String
            Get
                Return _Giro
            End Get
            Set(ByVal value As System.String)
                _Giro = value
            End Set
        End Property

        Private _IdDepartamento As System.String = ""
        Public Property IdDepartamento() As System.String
            Get
                Return _IdDepartamento
            End Get
            Set(ByVal value As System.String)
                _IdDepartamento = value
            End Set
        End Property

        Private _IdMunicipio As System.String = ""
        Public Property IdMunicipio() As System.String
            Get
                Return _IdMunicipio
            End Get
            Set(ByVal value As System.String)
                _IdMunicipio = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _Telefonos As System.String = ""
        Public Property Telefonos() As System.String
            Get
                Return _Telefonos
            End Get
            Set(ByVal value As System.String)
                _Telefonos = value
            End Set
        End Property

        Private _Fax As System.String = ""
        Public Property Fax() As System.String
            Get
                Return _Fax
            End Get
            Set(ByVal value As System.String)
                _Fax = value
            End Set
        End Property

        Private _CorreoElectronico As System.String = ""
        Public Property CorreoElectronico() As System.String
            Get
                Return _CorreoElectronico
            End Get
            Set(ByVal value As System.String)
                _CorreoElectronico = value
            End Set
        End Property

        Private _IdCuentaContable As System.String = ""
        Public Property IdCuentaContable() As String
            Get
                Return _IdCuentaContable
            End Get
            Set(ByVal value As String)
                _IdCuentaContable = value
            End Set
        End Property

        Private _DiasCredito As Int32
        Public Property DiasCredito() As Int32
            Get
                Return _DiasCredito
            End Get
            Set(ByVal value As Int32)
                _DiasCredito = value
            End Set
        End Property

        Private _IdVendedor As System.Int32
        Public Property IdVendedor() As System.Int32
            Get
                Return _IdVendedor
            End Get
            Set(ByVal value As System.Int32)
                _IdVendedor = value
            End Set
        End Property

        Private _IdFormaPago As System.Int32
        Public Property IdFormaPago() As System.Int32
            Get
                Return _IdFormaPago
            End Get
            Set(ByVal value As System.Int32)
                _IdFormaPago = value
            End Set
        End Property

        Private _AplicaRetencion As System.Boolean
        Public Property AplicaRetencion() As System.Boolean
            Get
                Return _AplicaRetencion
            End Get
            Set(ByVal value As System.Boolean)
                _AplicaRetencion = value
            End Set
        End Property

        Private _LimiteCredito As System.Decimal
        Public Property LimiteCredito() As System.Decimal
            Get
                Return _LimiteCredito
            End Get
            Set(ByVal value As System.Decimal)
                _LimiteCredito = value
            End Set
        End Property

        Private _SaldoActual As System.Decimal
        Public Property SaldoActual() As System.Decimal
            Get
                Return _SaldoActual
            End Get
            Set(ByVal value As System.Decimal)
                _SaldoActual = value
            End Set
        End Property

        Private _FechaUltPago As Nullable(Of DateTime)
        Public Property FechaUltPago() As Nullable(Of DateTime)
            Get
                Return _FechaUltPago
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaUltPago = value
            End Set
        End Property

        Private _IdPrecio As System.Int32
        Public Property IdPrecio() As System.Int32
            Get
                Return _IdPrecio
            End Get
            Set(ByVal value As System.Int32)
                _IdPrecio = value
            End Set
        End Property

        Private _Contacto1 As System.String = ""
        Public Property Contacto1() As System.String
            Get
                Return _Contacto1
            End Get
            Set(ByVal value As System.String)
                _Contacto1 = value
            End Set
        End Property

        Private _InfoContacto1 As System.String = ""
        Public Property InfoContacto1() As System.String
            Get
                Return _InfoContacto1
            End Get
            Set(ByVal value As System.String)
                _InfoContacto1 = value
            End Set
        End Property

        Private _Contacto2 As System.String = ""
        Public Property Contacto2() As System.String
            Get
                Return _Contacto2
            End Get
            Set(ByVal value As System.String)
                _Contacto2 = value
            End Set
        End Property

        Private _InfoContacto2 As System.String = ""
        Public Property InfoContacto2() As System.String
            Get
                Return _InfoContacto2
            End Get
            Set(ByVal value As System.String)
                _InfoContacto2 = value
            End Set
        End Property

        Private _IdTipoComprobante As System.Int32
        Public Property IdTipoComprobante() As System.Int32
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _IdTipoImpuesto As System.Int32
        Public Property IdTipoImpuesto() As System.Int32
            Get
                Return _IdTipoImpuesto
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoImpuesto = value
            End Set
        End Property

        Private _PorcDescuento As System.Decimal
        Public Property PorcDescuento() As System.Decimal
            Get
                Return _PorcDescuento
            End Get
            Set(ByVal value As System.Decimal)
                _PorcDescuento = value
            End Set
        End Property

        Private _IdRuta As System.Int32
        Public Property IdRuta() As System.Int32
            Get
                Return _IdRuta
            End Get
            Set(ByVal value As System.Int32)
                _IdRuta = value
            End Set
        End Property

        Private _BloquearFacturacion As System.Boolean
        Public Property BloquearFacturacion() As System.Boolean
            Get
                Return _BloquearFacturacion
            End Get
            Set(ByVal value As System.Boolean)
                _BloquearFacturacion = value
            End Set
        End Property

        Private _TipoImpuestoAdicional As System.Int32
        Public Property TipoImpuestoAdicional() As System.Int32
            Get
                Return _TipoImpuestoAdicional
            End Get
            Set(ByVal value As System.Int32)
                _TipoImpuestoAdicional = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_RubrosActividades"
    Public Class fac_RubrosActividades
        Private _IdRubro As System.Int32
        Public Property IdRubro() As System.Int32
            Get
                Return _IdRubro
            End Get
            Set(ByVal value As System.Int32)
                _IdRubro = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region


#Region "fac_Actividades"
    Public Class fac_Actividades
        Private _IdRubro As System.Int32
        Public Property IdRubro() As System.Int32
            Get
                Return _IdRubro
            End Get
            Set(ByVal value As System.Int32)
                _IdRubro = value
            End Set
        End Property

        Private _IdActividad As System.Int32
        Public Property IdActividad() As System.Int32
            Get
                Return _IdActividad
            End Get
            Set(ByVal value As System.Int32)
                _IdActividad = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _EmiteAlerta As System.Boolean
        Public Property EmiteAlerta() As System.Boolean
            Get
                Return _EmiteAlerta
            End Get
            Set(ByVal value As System.Boolean)
                _EmiteAlerta = value
            End Set
        End Property

        Private _Peso As System.Decimal
        Public Property Peso() As System.Decimal
            Get
                Return _Peso
            End Get
            Set(ByVal value As System.Decimal)
                _Peso = value
            End Set
        End Property

        Private _Apnfd As System.Boolean
        Public Property Apnfd() As System.Boolean
            Get
                Return _Apnfd
            End Get
            Set(ByVal value As System.Boolean)
                _Apnfd = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_ClientesAnexo"
    Public Class fac_ClientesAnexo
        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _IdProfesion As System.Int32
        Public Property IdProfesion() As System.Int32
            Get
                Return _IdProfesion
            End Get
            Set(ByVal value As System.Int32)
                _IdProfesion = value
            End Set
        End Property

        Private _IdRubro As System.Int32
        Public Property IdRubro() As System.Int32
            Get
                Return _IdRubro
            End Get
            Set(ByVal value As System.Int32)
                _IdRubro = value
            End Set
        End Property

        Private _IdActividad As System.Int32
        Public Property IdActividad() As System.Int32
            Get
                Return _IdActividad
            End Get
            Set(ByVal value As System.Int32)
                _IdActividad = value
            End Set
        End Property

        Private _IngresosBruto As System.Int32
        Public Property IngresosBruto() As System.Int32
            Get
                Return _IngresosBruto
            End Get
            Set(ByVal value As System.Int32)
                _IngresosBruto = value
            End Set
        End Property

        Private _APNFD As System.Boolean
        Public Property APNFD() As System.Boolean
            Get
                Return _APNFD
            End Get
            Set(ByVal value As System.Boolean)
                _APNFD = value
            End Set
        End Property

        Private _Comentario As System.String = ""
        Public Property Comentario() As System.String
            Get
                Return _Comentario
            End Get
            Set(ByVal value As System.String)
                _Comentario = value
            End Set
        End Property

        Private _MontoEstimadoCompras As System.Decimal
        Public Property MontoEstimadoCompras() As System.Decimal
            Get
                Return _MontoEstimadoCompras
            End Get
            Set(ByVal value As System.Decimal)
                _MontoEstimadoCompras = value
            End Set
        End Property

        Private _PersonaJuridica As System.Boolean
        Public Property PersonaJuridica() As System.Boolean
            Get
                Return _PersonaJuridica
            End Get
            Set(ByVal value As System.Boolean)
                _PersonaJuridica = value
            End Set
        End Property

        Private _RepresentanteLegal As System.String = ""
        Public Property RepresentanteLegal() As System.String
            Get
                Return _RepresentanteLegal
            End Get
            Set(ByVal value As System.String)
                _RepresentanteLegal = value
            End Set
        End Property

        Private _TipoDocumento As System.Int32
        Public Property TipoDocumento() As System.Int32
            Get
                Return _TipoDocumento
            End Get
            Set(ByVal value As System.Int32)
                _TipoDocumento = value
            End Set
        End Property

        Private _NumDocumentoRepLegal As System.String = ""
        Public Property NumDocumentoRepLegal() As System.String
            Get
                Return _NumDocumentoRepLegal
            End Get
            Set(ByVal value As System.String)
                _NumDocumentoRepLegal = value
            End Set
        End Property

        Private _NITRepLegal As System.String = ""
        Public Property NITRepLegal() As System.String
            Get
                Return _NITRepLegal
            End Get
            Set(ByVal value As System.String)
                _NITRepLegal = value
            End Set
        End Property

        Private _IdDepartamentoRepLegal As System.String = ""
        Public Property IdDepartamentoRepLegal() As System.String
            Get
                Return _IdDepartamentoRepLegal
            End Get
            Set(ByVal value As System.String)
                _IdDepartamentoRepLegal = value
            End Set
        End Property

        Private _IdMunicipioRepLegal As System.String = ""
        Public Property IdMunicipioRepLegal() As System.String
            Get
                Return _IdMunicipioRepLegal
            End Get
            Set(ByVal value As System.String)
                _IdMunicipioRepLegal = value
            End Set
        End Property

        Private _IdProfesionRepLegal As System.Int32
        Public Property IdProfesionRepLegal() As System.Int32
            Get
                Return _IdProfesionRepLegal
            End Get
            Set(ByVal value As System.Int32)
                _IdProfesionRepLegal = value
            End Set
        End Property

        Private _DireccionRepLegal As System.String = ""
        Public Property DireccionRepLegal() As System.String
            Get
                Return _DireccionRepLegal
            End Get
            Set(ByVal value As System.String)
                _DireccionRepLegal = value
            End Set
        End Property

        Private _TelMovilRepLegal As System.String = ""
        Public Property TelMovilRepLegal() As System.String
            Get
                Return _TelMovilRepLegal
            End Get
            Set(ByVal value As System.String)
                _TelMovilRepLegal = value
            End Set
        End Property

        Private _TelefonosRepLegal As System.String = ""
        Public Property TelefonosRepLegal() As System.String
            Get
                Return _TelefonosRepLegal
            End Get
            Set(ByVal value As System.String)
                _TelefonosRepLegal = value
            End Set
        End Property

        Private _CorreoElectronicoRepLegal As System.String = ""
        Public Property CorreoElectronicoRepLegal() As System.String
            Get
                Return _CorreoElectronicoRepLegal
            End Get
            Set(ByVal value As System.String)
                _CorreoElectronicoRepLegal = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region



#Region "fac_Profesiones"
    Public Class fac_Profesiones
        Private _IdProfesion As System.Int16
        Public Property IdProfesion() As System.Int16
            Get
                Return _IdProfesion
            End Get
            Set(ByVal value As System.Int16)
                _IdProfesion = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Asegurado As System.Boolean
        Public Property Asegurado() As System.Boolean
            Get
                Return _Asegurado
            End Get
            Set(ByVal value As System.Boolean)
                _Asegurado = value
            End Set
        End Property

        Private _Peso As System.Decimal
        Public Property Peso() As System.Decimal
            Get
                Return _Peso
            End Get
            Set(ByVal value As System.Decimal)
                _Peso = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_CorreosAlerta"
    Public Class fac_CorreosAlerta
        Private _IdCorreo As System.Int32
        Public Property IdCorreo() As System.Int32
            Get
                Return _IdCorreo
            End Get
            Set(ByVal value As System.Int32)
                _IdCorreo = value
            End Set
        End Property

        Private _Correo As System.String = ""
        Public Property Correo() As System.String
            Get
                Return _Correo
            End Get
            Set(ByVal value As System.String)
                _Correo = value
            End Set
        End Property

        Private _Contacto As System.String = ""
        Public Property Contacto() As System.String
            Get
                Return _Contacto
            End Get
            Set(ByVal value As System.String)
                _Contacto = value
            End Set
        End Property

        Private _Cargo As System.String = ""
        Public Property Cargo() As System.String
            Get
                Return _Cargo
            End Get
            Set(ByVal value As System.String)
                _Cargo = value
            End Set
        End Property

        Private _Tipo As System.Int32
        Public Property Tipo() As System.Int32
            Get
                Return _Tipo
            End Get
            Set(ByVal value As System.Int32)
                _Tipo = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_BitacoraAlertas"
    Public Class fac_BitacoraAlertas
        Private _IdBitacora As System.Int32
        Public Property IdBitacora() As System.Int32
            Get
                Return _IdBitacora
            End Get
            Set(ByVal value As System.Int32)
                _IdBitacora = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _PesoRiesgo As System.Decimal
        Public Property PesoRiesgo() As System.Decimal
            Get
                Return _PesoRiesgo
            End Get
            Set(ByVal value As System.Decimal)
                _PesoRiesgo = value
            End Set
        End Property

        Private _IdAlerta As System.Int32
        Public Property IdAlerta() As System.Int32
            Get
                Return _IdAlerta
            End Get
            Set(ByVal value As System.Int32)
                _IdAlerta = value
            End Set
        End Property

        Private _NumeroReferencia As System.String = ""
        Public Property NumeroReferencia() As System.String
            Get
                Return _NumeroReferencia
            End Get
            Set(ByVal value As System.String)
                _NumeroReferencia = value
            End Set
        End Property

        Private _TipoTransaccion As System.Int32
        Public Property TipoTransaccion() As System.Int32
            Get
                Return _TipoTransaccion
            End Get
            Set(ByVal value As System.Int32)
                _TipoTransaccion = value
            End Set
        End Property

        Private _MontoEstimadoCompra As System.Decimal
        Public Property MontoEstimadoCompra() As System.Decimal
            Get
                Return _MontoEstimadoCompra
            End Get
            Set(ByVal value As System.Decimal)
                _MontoEstimadoCompra = value
            End Set
        End Property

        Private _MontoTransaccion As System.Decimal
        Public Property MontoTransaccion() As System.Decimal
            Get
                Return _MontoTransaccion
            End Get
            Set(ByVal value As System.Decimal)
                _MontoTransaccion = value
            End Set
        End Property

        Private _DiferenciaPerfilMensual As System.Decimal
        Public Property DiferenciaPerfilMensual() As System.Decimal
            Get
                Return _DiferenciaPerfilMensual
            End Get
            Set(ByVal value As System.Decimal)
                _DiferenciaPerfilMensual = value
            End Set
        End Property

        Private _DiferenciaPorcentaje As System.Decimal
        Public Property DiferenciaPorcentaje() As System.Decimal
            Get
                Return _DiferenciaPorcentaje
            End Get
            Set(ByVal value As System.Decimal)
                _DiferenciaPorcentaje = value
            End Set
        End Property

        Private _IdFormaPago As System.Int32
        Public Property IdFormaPago() As System.Int32
            Get
                Return _IdFormaPago
            End Get
            Set(ByVal value As System.Int32)
                _IdFormaPago = value
            End Set
        End Property

        Private _NombreTercero As System.String = ""
        Public Property NombreTercero() As System.String
            Get
                Return _NombreTercero
            End Get
            Set(ByVal value As System.String)
                _NombreTercero = value
            End Set
        End Property

        Private _Parentesco As System.String = ""
        Public Property Parentesco() As System.String
            Get
                Return _Parentesco
            End Get
            Set(ByVal value As System.String)
                _Parentesco = value
            End Set
        End Property

        Private _IdActividad As System.Int32
        Public Property IdActividad() As System.Int32
            Get
                Return _IdActividad
            End Get
            Set(ByVal value As System.Int32)
                _IdActividad = value
            End Set
        End Property

        Private _Profesion As System.String = ""
        Public Property Profesion() As System.String
            Get
                Return _Profesion
            End Get
            Set(ByVal value As System.String)
                _Profesion = value
            End Set
        End Property

        Private _IngresosDeclarados As System.Decimal
        Public Property IngresosDeclarados() As System.Decimal
            Get
                Return _IngresosDeclarados
            End Get
            Set(ByVal value As System.Decimal)
                _IngresosDeclarados = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _Revisada As System.Boolean
        Public Property Revisada() As System.Boolean
            Get
                Return _Revisada
            End Get
            Set(ByVal value As System.Boolean)
                _Revisada = value
            End Set
        End Property

        Private _FechaRevisada As Nullable(Of DateTime)
        Public Property FechaRevisada() As Nullable(Of DateTime)
            Get
                Return _FechaRevisada
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaRevisada = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _Comentario As System.String = ""
        Public Property Comentario() As System.String
            Get
                Return _Comentario
            End Get
            Set(ByVal value As System.String)
                _Comentario = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_ProspectacionClientes"
	Public Class fac_ProspectacionClientes
		Private _IdComprobante As System.Int32
		Public Property IdComprobante() As System.Int32
			Get
				Return _IdComprobante
			End Get
			Set(ByVal value As System.Int32)
				_IdComprobante = value
			End Set
		End Property

		Private _NombreCliente As System.String = ""
		Public Property NombreCliente() As System.String
			Get
				Return _NombreCliente
			End Get
			Set(ByVal value As System.String)
				_NombreCliente = value
			End Set
		End Property

		Private _RazonSocial As System.String = ""
		Public Property RazonSocial() As System.String
			Get
				Return _RazonSocial
			End Get
			Set(ByVal value As System.String)
				_RazonSocial = value
			End Set
		End Property

		Private _Direccion As System.String = ""
		Public Property Direccion() As System.String
			Get
				Return _Direccion
			End Get
			Set(ByVal value As System.String)
				_Direccion = value
			End Set
		End Property

		Private _IdDepartamento As System.String = ""
		Public Property IdDepartamento() As System.String
			Get
				Return _IdDepartamento
			End Get
			Set(ByVal value As System.String)
				_IdDepartamento = value
			End Set
		End Property

		Private _IdMunicipio As System.String = ""
		Public Property IdMunicipio() As System.String
			Get
				Return _IdMunicipio
			End Get
			Set(ByVal value As System.String)
				_IdMunicipio = value
			End Set
		End Property

		Private _Telefonos As System.String = ""
		Public Property Telefonos() As System.String
			Get
				Return _Telefonos
			End Get
			Set(ByVal value As System.String)
				_Telefonos = value
			End Set
		End Property

		Private _Nit As System.String = ""
		Public Property Nit() As System.String
			Get
				Return _Nit
			End Get
			Set(ByVal value As System.String)
				_Nit = value
			End Set
		End Property

		Private _Dui As System.String = ""
		Public Property Dui() As System.String
			Get
				Return _Dui
			End Get
			Set(ByVal value As System.String)
				_Dui = value
			End Set
		End Property

		Private _Nrc As System.String = ""
		Public Property Nrc() As System.String
			Get
				Return _Nrc
			End Get
			Set(ByVal value As System.String)
				_Nrc = value
			End Set
		End Property

		Private _Giro As System.String = ""
		Public Property Giro() As System.String
			Get
				Return _Giro
			End Get
			Set(ByVal value As System.String)
				_Giro = value
			End Set
		End Property

		Private _Email As System.String = ""
		Public Property Email() As System.String
			Get
				Return _Email
			End Get
			Set(ByVal value As System.String)
				_Email = value
			End Set
		End Property

		Private _ReferidoPor As System.String = ""
		Public Property ReferidoPor() As System.String
			Get
				Return _ReferidoPor
			End Get
			Set(ByVal value As System.String)
				_ReferidoPor = value
			End Set
		End Property

		Private _FechaContacto As Nullable(Of DateTime)
		Public Property FechaContacto() As Nullable(Of DateTime)
			Get
				Return _FechaContacto
			End Get
			Set(ByVal value As Nullable(Of DateTime))
				_FechaContacto = value
			End Set
		End Property

		Private _Asunto As System.String = ""
		Public Property Asunto() As System.String
			Get
				Return _Asunto
			End Get
			Set(ByVal value As System.String)
				_Asunto = value
			End Set
		End Property

		Private _LugarContacto As System.String = ""
		Public Property LugarContacto() As System.String
			Get
				Return _LugarContacto
			End Get
			Set(ByVal value As System.String)
				_LugarContacto = value
			End Set
		End Property

		Private _IdVendedor As System.Int32
		Public Property IdVendedor() As System.Int32
			Get
				Return _IdVendedor
			End Get
			Set(ByVal value As System.Int32)
				_IdVendedor = value
			End Set
		End Property

		Private _CreadoPor As System.String = ""
		Public Property CreadoPor() As System.String
			Get
				Return _CreadoPor
			End Get
			Set(ByVal value As System.String)
				_CreadoPor = value
			End Set
		End Property

		Private _FechaHoraCreacion As Nullable(Of DateTime)
		Public Property FechaHoraCreacion() As Nullable(Of DateTime)
			Get
				Return _FechaHoraCreacion
			End Get
			Set(ByVal value As Nullable(Of DateTime))
				_FechaHoraCreacion = value
			End Set
		End Property

		Private _ModificadoPor As System.String = ""
		Public Property ModificadoPor() As System.String
			Get
				Return _ModificadoPor
			End Get
			Set(ByVal value As System.String)
				_ModificadoPor = value
			End Set
		End Property

		Private _FechaHoraModificacion As Nullable(Of DateTime)
		Public Property FechaHoraModificacion() As Nullable(Of DateTime)
			Get
				Return _FechaHoraModificacion
			End Get
			Set(ByVal value As Nullable(Of DateTime))
				_FechaHoraModificacion = value
			End Set
		End Property

	End Class
#End Region


#Region "fac_ClientesPrecios"
	Public Class fac_ClientesPrecios
        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

    End Class
#End Region


#Region "fac_Cajas"
    Public Class fac_Cajas
        Private _NumeroCaja As System.String = ""
        Public Property NumeroCaja() As System.String
            Get
                Return _NumeroCaja
            End Get
            Set(ByVal value As System.String)
                _NumeroCaja = value
            End Set
        End Property

        Private _NombreCaja As System.String = ""
        Public Property NombreCaja() As System.String
            Get
                Return _NombreCaja
            End Get
            Set(ByVal value As System.String)
                _NombreCaja = value
            End Set
        End Property
        Private _UltimoCorte As System.Int32
        Public Property UltimoCorte() As System.Int32
            Get
                Return _UltimoCorte
            End Get
            Set(ByVal value As System.Int32)
                _UltimoCorte = value
            End Set
        End Property

        Private _FechaUltimoCorte As System.DateTime = Today
        Public Property FechaUltimoCorte() As System.DateTime
            Get
                Return _FechaUltimoCorte
            End Get
            Set(ByVal value As System.DateTime)
                _FechaUltimoCorte = value
            End Set
        End Property

        Private _Responsable As System.String = ""
        Public Property Responsable() As System.String
            Get
                Return _Responsable
            End Get
            Set(ByVal value As System.String)
                _Responsable = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_NotasRemision"
    Public Class fac_NotasRemision
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Nit As System.String = ""
        Public Property Nit() As System.String
            Get
                Return _Nit
            End Get
            Set(ByVal value As System.String)
                _Nit = value
            End Set
        End Property

        Private _Nrc As System.String = ""
        Public Property Nrc() As System.String
            Get
                Return _Nrc
            End Get
            Set(ByVal value As System.String)
                _Nrc = value
            End Set
        End Property

        Private _Giro As System.String = ""
        Public Property Giro() As System.String
            Get
                Return _Giro
            End Get
            Set(ByVal value As System.String)
                _Giro = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _IdDepartamento As System.String = ""
        Public Property IdDepartamento() As System.String
            Get
                Return _IdDepartamento
            End Get
            Set(ByVal value As System.String)
                _IdDepartamento = value
            End Set
        End Property

        Private _IdMunicipio As System.String = ""
        Public Property IdMunicipio() As System.String
            Get
                Return _IdMunicipio
            End Get
            Set(ByVal value As System.String)
                _IdMunicipio = value
            End Set
        End Property

        Private _Telefonos As System.String = ""
        Public Property Telefonos() As System.String
            Get
                Return _Telefonos
            End Get
            Set(ByVal value As System.String)
                _Telefonos = value
            End Set
        End Property

        Private _IdSucursal As System.Int16
        Public Property IdSucursal() As System.Int16
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int16)
                _IdSucursal = value
            End Set
        End Property

        Private _IdVendedor As System.Int16
        Public Property IdVendedor() As System.Int16
            Get
                Return _IdVendedor
            End Get
            Set(ByVal value As System.Int16)
                _IdVendedor = value
            End Set
        End Property

        Private _IdBodega As System.Int16
        Public Property IdBodega() As System.Int16
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int16)
                _IdBodega = value
            End Set
        End Property

        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_NotasRemisionDetalle"
    Public Class fac_NotasRemisionDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _IdPrecio As System.Int16
        Public Property IdPrecio() As System.Int16
            Get
                Return _IdPrecio
            End Get
            Set(ByVal value As System.Int16)
                _IdPrecio = value
            End Set
        End Property
        Private _Descripcion As System.String
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PjeDescuento As System.Decimal
        Public Property PjeDescuento() As System.Decimal
            Get
                Return _PjeDescuento
            End Get
            Set(ByVal value As System.Decimal)
                _PjeDescuento = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property

        Private _CreadoPor As System.String
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_Cotizaciones"
    Public Class fac_Cotizaciones
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Nrc As System.String = ""
        Public Property Nrc() As System.String
            Get
                Return _Nrc
            End Get
            Set(ByVal value As System.String)
                _Nrc = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _Nit As System.String = ""
        Public Property Nit() As System.String
            Get
                Return _Nit
            End Get
            Set(ByVal value As System.String)
                _Nit = value
            End Set
        End Property

        Private _AtencionA As System.String = ""
        Public Property AtencionA() As System.String
            Get
                Return _AtencionA
            End Get
            Set(ByVal value As System.String)
                _AtencionA = value
            End Set
        End Property

        Private _IdVendedor As System.Int32
        Public Property IdVendedor() As System.Int32
            Get
                Return _IdVendedor
            End Get
            Set(ByVal value As System.Int32)
                _IdVendedor = value
            End Set
        End Property

        Private _IdFormaPago As System.Int32
        Public Property IdFormaPago() As System.Int32
            Get
                Return _IdFormaPago
            End Get
            Set(ByVal value As System.Int32)
                _IdFormaPago = value
            End Set
        End Property

        Private _DiasCredito As System.Int32
        Public Property DiasCredito() As System.Int32
            Get
                Return _DiasCredito
            End Get
            Set(ByVal value As System.Int32)
                _DiasCredito = value
            End Set
        End Property

        Private _TiempoEntrega As String = ""
        Public Property TiempoEntrega() As String
            Get
                Return _TiempoEntrega
            End Get
            Set(ByVal value As String)
                _TiempoEntrega = value
            End Set
        End Property

        Private _Proyecto As String = ""
        Public Property Proyecto() As String
            Get
                Return _Proyecto
            End Get
            Set(ByVal value As String)
                _Proyecto = value
            End Set
        End Property
        Private _Garantia As System.String = ""
        Public Property Garantia() As System.String
            Get
                Return _Garantia
            End Get
            Set(ByVal value As System.String)
                _Garantia = value
            End Set
        End Property
        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _ImprimirPrecios As System.Boolean
        Public Property ImprimirPrecios() As System.Boolean
            Get
                Return _ImprimirPrecios
            End Get
            Set(ByVal value As System.Boolean)
                _ImprimirPrecios = value
            End Set
        End Property

        Private _TipoVenta As System.Int32
        Public Property TipoVenta() As System.Int32
            Get
                Return _TipoVenta
            End Get
            Set(ByVal value As System.Int32)
                _TipoVenta = value
            End Set
        End Property
        Private _IdTipoComprobante As System.Int32
        Public Property IdTipoComprobante() As System.Int32
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoComprobante = value
            End Set
        End Property
        Private _AplicaRetencionPercepcion As System.Boolean
        Public Property AplicaRetencionPercepcion() As System.Boolean
            Get
                Return _AplicaRetencionPercepcion
            End Get
            Set(ByVal value As System.Boolean)
                _AplicaRetencionPercepcion = value
            End Set
        End Property
        Private _TotalAfecto As System.Decimal
        Public Property TotalAfecto() As System.Decimal
            Get
                Return _TotalAfecto
            End Get
            Set(ByVal value As System.Decimal)
                _TotalAfecto = value
            End Set
        End Property
        Private _TotalIva As System.Decimal
        Public Property TotalIva() As System.Decimal
            Get
                Return _TotalIva
            End Get
            Set(ByVal value As System.Decimal)
                _TotalIva = value
            End Set
        End Property
        Private _TotalImpuesto1 As System.Decimal
        Public Property TotalImpuesto1() As System.Decimal
            Get
                Return _TotalImpuesto1
            End Get
            Set(ByVal value As System.Decimal)
                _TotalImpuesto1 = value
            End Set
        End Property
        Private _TotalComprobante As System.Decimal
        Public Property TotalComprobante() As System.Decimal
            Get
                Return _TotalComprobante
            End Get
            Set(ByVal value As System.Decimal)
                _TotalComprobante = value
            End Set
        End Property
        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_CotizacionesDetalle"
    Public Class fac_CotizacionesDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _CodigoPublico As System.String = ""
        Public Property CodigoPublico() As System.String
            Get
                Return _CodigoPublico
            End Get
            Set(ByVal value As System.String)
                _CodigoPublico = value
            End Set
        End Property

        Private _IdPrecio As System.Int32
        Public Property IdPrecio() As System.Int32
            Get
                Return _IdPrecio
            End Get
            Set(ByVal value As System.Int32)
                _IdPrecio = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property


        Private _Especificaciones As System.String = ""
        Public Property Especificaciones() As System.String
            Get
                Return _Especificaciones
            End Get
            Set(ByVal value As System.String)
                _Especificaciones = value
            End Set
        End Property

        Private _ArchivoImagen As System.String = ""
        Public Property ArchivoImagen() As System.String
            Get
                Return _ArchivoImagen
            End Get
            Set(ByVal value As System.String)
                _ArchivoImagen = value
            End Set
        End Property

        Private _PrecioVenta As System.Decimal
        Public Property PrecioVenta() As System.Decimal
            Get
                Return _PrecioVenta
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioVenta = value
            End Set
        End Property

        Private _PorcDescuento As System.Decimal
        Public Property PorcDescuento() As System.Decimal
            Get
                Return _PorcDescuento
            End Get
            Set(ByVal value As System.Decimal)
                _PorcDescuento = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property
        Private _VentaNeta As System.Decimal
        Public Property VentaNeta() As System.Decimal
            Get
                Return _VentaNeta
            End Get
            Set(ByVal value As System.Decimal)
                _VentaNeta = value
            End Set
        End Property
        Private _ValorIva As System.Decimal
        Public Property ValorIva() As System.Decimal
            Get
                Return _ValorIva
            End Get
            Set(ByVal value As System.Decimal)
                _ValorIva = value
            End Set
        End Property

        Private _DescuentoAutorizadoPor As System.String = ""
        Public Property DescuentoAutorizadoPor() As System.String
            Get
                Return _DescuentoAutorizadoPor
            End Get
            Set(ByVal value As System.String)
                _DescuentoAutorizadoPor = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_VentasRetencion"
    Public Class fac_VentasRetencion
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _FechaContable As Nullable(Of DateTime)
        Public Property FechaContable() As Nullable(Of DateTime)
            Get
                Return _FechaContable
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaContable = value
            End Set
        End Property

        Private _Observacion As System.String = ""
        Public Property Observacion() As System.String
            Get
                Return _Observacion
            End Get
            Set(ByVal value As System.String)
                _Observacion = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_Ventas"
    Public Class fac_Ventas
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdTipoComprobante As System.Int32
        Public Property IdTipoComprobante() As System.Int32
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _Serie As System.String = ""
        Public Property Serie() As System.String
            Get
                Return _Serie
            End Get
            Set(ByVal value As System.String)
                _Serie = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _NumFormularioUnico As System.String = ""
        Public Property NumFormularioUnico() As System.String
            Get
                Return _NumFormularioUnico
            End Get
            Set(ByVal value As System.String)
                _NumFormularioUnico = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Nrc As System.String = ""
        Public Property Nrc() As System.String
            Get
                Return _Nrc
            End Get
            Set(ByVal value As System.String)
                _Nrc = value
            End Set
        End Property

        Private _Nit As System.String = ""
        Public Property Nit() As System.String
            Get
                Return _Nit
            End Get
            Set(ByVal value As System.String)
                _Nit = value
            End Set
        End Property

        Private _Giro As System.String = ""
        Public Property Giro() As System.String
            Get
                Return _Giro
            End Get
            Set(ByVal value As System.String)
                _Giro = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _Telefono As System.String = ""
        Public Property Telefono() As System.String
            Get
                Return _Telefono
            End Get
            Set(ByVal value As System.String)
                _Telefono = value
            End Set
        End Property

        Private _VentaAcuentaDe As System.String = ""
        Public Property VentaAcuentaDe() As System.String
            Get
                Return _VentaAcuentaDe
            End Get
            Set(ByVal value As System.String)
                _VentaAcuentaDe = value
            End Set
        End Property

        Private _IdDepartamento As System.String = ""
        Public Property IdDepartamento() As System.String
            Get
                Return _IdDepartamento
            End Get
            Set(ByVal value As System.String)
                _IdDepartamento = value
            End Set
        End Property

        Private _IdMunicipio As System.String = ""
        Public Property IdMunicipio() As System.String
            Get
                Return _IdMunicipio
            End Get
            Set(ByVal value As System.String)
                _IdMunicipio = value
            End Set
        End Property

        Private _IdFormaPago As System.Int32
        Public Property IdFormaPago() As System.Int32
            Get
                Return _IdFormaPago
            End Get
            Set(ByVal value As System.Int32)
                _IdFormaPago = value
            End Set
        End Property

        Private _DiasCredito As System.Int32
        Public Property DiasCredito() As System.Int32
            Get
                Return _DiasCredito
            End Get
            Set(ByVal value As System.Int32)
                _DiasCredito = value
            End Set
        End Property

        Private _OrdenCompra As System.String = ""
        Public Property OrdenCompra() As System.String
            Get
                Return _OrdenCompra
            End Get
            Set(ByVal value As System.String)
                _OrdenCompra = value
            End Set
        End Property

        Private _IdVendedor As System.Int32
        Public Property IdVendedor() As System.Int32
            Get
                Return _IdVendedor
            End Get
            Set(ByVal value As System.Int32)
                _IdVendedor = value
            End Set
        End Property

        Private _TipoVenta As System.Int32
        Public Property TipoVenta() As System.Int32
            Get
                Return _TipoVenta
            End Get
            Set(ByVal value As System.Int32)
                _TipoVenta = value
            End Set
        End Property

        Private _TipoImpuesto As System.Int32
        Public Property TipoImpuesto() As System.Int32
            Get
                Return _TipoImpuesto
            End Get
            Set(ByVal value As System.Int32)
                _TipoImpuesto = value
            End Set
        End Property

        Private _PorcDescto As System.Decimal
        Public Property PorcDescto() As System.Decimal
            Get
                Return _PorcDescto
            End Get
            Set(ByVal value As System.Decimal)
                _PorcDescto = value
            End Set
        End Property

        Private _TotalDescto As System.Decimal
        Public Property TotalDescto() As System.Decimal
            Get
                Return _TotalDescto
            End Get
            Set(ByVal value As System.Decimal)
                _TotalDescto = value
            End Set
        End Property

        Private _TotalComprobante As System.Decimal
        Public Property TotalComprobante() As System.Decimal
            Get
                Return _TotalComprobante
            End Get
            Set(ByVal value As System.Decimal)
                _TotalComprobante = value
            End Set
        End Property

        Private _TotalIva As System.Decimal
        Public Property TotalIva() As System.Decimal
            Get
                Return _TotalIva
            End Get
            Set(ByVal value As System.Decimal)
                _TotalIva = value
            End Set
        End Property

        Private _TotalImpuesto1 As System.Decimal
        Public Property TotalImpuesto1() As System.Decimal
            Get
                Return _TotalImpuesto1
            End Get
            Set(ByVal value As System.Decimal)
                _TotalImpuesto1 = value
            End Set
        End Property

        Private _TotalImpuesto2 As System.Decimal
        Public Property TotalImpuesto2() As System.Decimal
            Get
                Return _TotalImpuesto2
            End Get
            Set(ByVal value As System.Decimal)
                _TotalImpuesto2 = value
            End Set
        End Property

        Private _TotalAfecto As System.Decimal
        Public Property TotalAfecto() As System.Decimal
            Get
                Return _TotalAfecto
            End Get
            Set(ByVal value As System.Decimal)
                _TotalAfecto = value
            End Set
        End Property

        Private _TotalExento As System.Decimal
        Public Property TotalExento() As System.Decimal
            Get
                Return _TotalExento
            End Get
            Set(ByVal value As System.Decimal)
                _TotalExento = value
            End Set
        End Property

        Private _TotalNoSujeto As System.Decimal
        Public Property TotalNoSujeto() As System.Decimal
            Get
                Return _TotalNoSujeto
            End Get
            Set(ByVal value As System.Decimal)
                _TotalNoSujeto = value
            End Set
        End Property

        Private _TotalNeto As System.Decimal
        Public Property TotalNeto() As System.Decimal
            Get
                Return _TotalNeto
            End Get
            Set(ByVal value As System.Decimal)
                _TotalNeto = value
            End Set
        End Property

        Private _TotalPagado As System.Decimal
        Public Property TotalPagado() As System.Decimal
            Get
                Return _TotalPagado
            End Get
            Set(ByVal value As System.Decimal)
                _TotalPagado = value
            End Set
        End Property

        Private _SaldoActual As System.Decimal
        Public Property SaldoActual() As System.Decimal
            Get
                Return _SaldoActual
            End Get
            Set(ByVal value As System.Decimal)
                _SaldoActual = value
            End Set
        End Property


        Private _FechaCancelacion As Nullable(Of DateTime)
        Public Property FechaCancelacion() As Nullable(Of DateTime)
            Get
                Return _FechaCancelacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaCancelacion = value
            End Set
        End Property

        Private _Anulado As System.Boolean
        Public Property Anulado() As System.Boolean
            Get
                Return _Anulado
            End Get
            Set(ByVal value As System.Boolean)
                _Anulado = value
            End Set
        End Property

        Private _MotivoAnulacion As System.String = ""
        Public Property MotivoAnulacion() As System.String
            Get
                Return _MotivoAnulacion
            End Get
            Set(ByVal value As System.String)
                _MotivoAnulacion = value
            End Set
        End Property

        Private _AnuladoPor As System.String = ""
        Public Property AnuladoPor() As System.String
            Get
                Return _AnuladoPor
            End Get
            Set(ByVal value As System.String)
                _AnuladoPor = value
            End Set
        End Property

        Private _FechaAnulacion As Nullable(Of DateTime)
        Public Property FechaAnulacion() As Nullable(Of DateTime)
            Get
                Return _FechaAnulacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaAnulacion = value
            End Set
        End Property

        Private _IdComprobanteNota As System.Int32
        Public Property IdComprobanteNota() As System.Int32
            Get
                Return _IdComprobanteNota
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobanteNota = value
            End Set
        End Property

        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _EstadoFactura As System.Int32
        Public Property EstadoFactura() As System.Int32
            Get
                Return _EstadoFactura
            End Get
            Set(ByVal value As System.Int32)
                _EstadoFactura = value
            End Set
        End Property

        Private _EsDevolucion As System.Boolean
        Public Property EsDevolucion() As System.Boolean
            Get
                Return _EsDevolucion
            End Get
            Set(ByVal value As System.Boolean)
                _EsDevolucion = value
            End Set
        End Property
        Private _AutorizoDescuento As System.String = ""
        Public Property AutorizoDescuento() As System.String
            Get
                Return _AutorizoDescuento
            End Get
            Set(ByVal value As System.String)
                _AutorizoDescuento = value
            End Set
        End Property
        Private _IdSucursalEnvio As System.Int32
        Public Property IdSucursalEnvio() As System.Int32
            Get
                Return _IdSucursalEnvio
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursalEnvio = value
            End Set
        End Property
        Private _Comentario As System.String = ""
        Public Property Comentario() As System.String
            Get
                Return _Comentario
            End Get
            Set(ByVal value As System.String)
                _Comentario = value
            End Set
        End Property
        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_Vales"
    Public Class fac_Vales
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property
        Private _NumeroComprobante As System.String = ""
        Public Property NumeroComprobante() As System.String
            Get
                Return _NumeroComprobante
            End Get
            Set(ByVal value As System.String)
                _NumeroComprobante = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property


        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_AperturaCaja"
    Public Class fac_AperturaCaja
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _FondoApertura As System.Decimal
        Public Property FondoApertura() As System.Decimal
            Get
                Return _FondoApertura
            End Get
            Set(ByVal value As System.Decimal)
                _FondoApertura = value
            End Set
        End Property

        Private _Responsable As System.String = ""
        Public Property Responsable() As System.String
            Get
                Return _Responsable
            End Get
            Set(ByVal value As System.String)
                _Responsable = value
            End Set
        End Property

        Private _Comentario As System.String = ""
        Public Property Comentario() As System.String
            Get
                Return _Comentario
            End Get
            Set(ByVal value As System.String)
                _Comentario = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_CorteCajaArqueo"
    Public Class fac_CorteCajaArqueo
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdDenominacion As System.Int32
        Public Property IdDenominacion() As System.Int32
            Get
                Return _IdDenominacion
            End Get
            Set(ByVal value As System.Int32)
                _IdDenominacion = value
            End Set
        End Property

        Private _Cantidad As System.Int32
        Public Property Cantidad() As System.Int32
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Int32)
                _Cantidad = value
            End Set
        End Property

        Private _Total As System.Decimal
        Public Property Total() As System.Decimal
            Get
                Return _Total
            End Get
            Set(ByVal value As System.Decimal)
                _Total = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_CorteCajaCheques"
    Public Class fac_CorteCajaCheques
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _NumCheque As System.String = ""
        Public Property NumCheque() As System.String
            Get
                Return _NumCheque
            End Get
            Set(ByVal value As System.String)
                _NumCheque = value
            End Set
        End Property

        Private _IdBanco As System.Int32
        Public Property IdBanco() As System.Int32
            Get
                Return _IdBanco
            End Get
            Set(ByVal value As System.Int32)
                _IdBanco = value
            End Set
        End Property
        Private _Cliente As System.String = ""
        Public Property Cliente() As System.String
            Get
                Return _Cliente
            End Get
            Set(ByVal value As System.String)
                _Cliente = value
            End Set
        End Property
        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

        Private _NumReserva As System.String = ""
        Public Property NumReserva() As System.String
            Get
                Return _NumReserva
            End Get
            Set(ByVal value As System.String)
                _NumReserva = value
            End Set
        End Property

        Private _IdBancoRemesa As System.Int32
        Public Property IdBancoRemesa() As System.Int32
            Get
                Return _IdBancoRemesa
            End Get
            Set(ByVal value As System.Int32)
                _IdBancoRemesa = value
            End Set
        End Property

    End Class
#End Region

#Region "fac_CorteCajaRemesas"
    Public Class fac_CorteCajaRemesas
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property
        Private _FechaRemesar As Nullable(Of DateTime)
        Public Property FechaRemesar() As Nullable(Of DateTime)
            Get
                Return _FechaRemesar
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaRemesar = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _IdBanco As System.Int32
        Public Property IdBanco() As System.Int32
            Get
                Return _IdBanco
            End Get
            Set(ByVal value As System.Int32)
                _IdBanco = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_CorteCajaPOS"
    Public Class fac_CorteCajaPOS
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _IdBanco As System.Int32
        Public Property IdBanco() As System.Int32
            Get
                Return _IdBanco
            End Get
            Set(ByVal value As System.Int32)
                _IdBanco = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

    End Class
#End Region







#Region "fac_VentasDetallePago"
    Public Class fac_VentasDetallePago
        Private _IdFormaPago As System.Int32
        Public Property IdFormaPago() As System.Int32
            Get
                Return _IdFormaPago
            End Get
            Set(ByVal value As System.Int32)
                _IdFormaPago = value
            End Set
        End Property

        Private _IdComprobVenta As System.Int32
        Public Property IdComprobVenta() As System.Int32
            Get
                Return _IdComprobVenta
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobVenta = value
            End Set
        End Property

        Private _Total As System.Decimal
        Public Property Total() As System.Decimal
            Get
                Return _Total
            End Get
            Set(ByVal value As System.Decimal)
                _Total = value
            End Set
        End Property

    End Class
#End Region


#Region "fac_VentasDetalle"
    Public Class fac_VentasDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property


        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _IdPrecio As System.Int32
        Public Property IdPrecio() As System.Int32
            Get
                Return _IdPrecio
            End Get
            Set(ByVal value As System.Int32)
                _IdPrecio = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioVenta As System.Decimal
        Public Property PrecioVenta() As System.Decimal
            Get
                Return _PrecioVenta
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioVenta = value
            End Set
        End Property

        Private _PorcDescuento As System.Decimal
        Public Property PorcDescuento() As System.Decimal
            Get
                Return _PorcDescuento
            End Get
            Set(ByVal value As System.Decimal)
                _PorcDescuento = value
            End Set
        End Property

        Private _ValorDescuento As System.Decimal
        Public Property ValorDescuento() As System.Decimal
            Get
                Return _ValorDescuento
            End Get
            Set(ByVal value As System.Decimal)
                _ValorDescuento = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _VentaNoSujeta As System.Decimal
        Public Property VentaNoSujeta() As System.Decimal
            Get
                Return _VentaNoSujeta
            End Get
            Set(ByVal value As System.Decimal)
                _VentaNoSujeta = value
            End Set
        End Property

        Private _VentaExenta As System.Decimal
        Public Property VentaExenta() As System.Decimal
            Get
                Return _VentaExenta
            End Get
            Set(ByVal value As System.Decimal)
                _VentaExenta = value
            End Set
        End Property

        Private _VentaAfecta As System.Decimal
        Public Property VentaAfecta() As System.Decimal
            Get
                Return _VentaAfecta
            End Get
            Set(ByVal value As System.Decimal)
                _VentaAfecta = value
            End Set
        End Property

        Private _VentaNeta As System.Decimal
        Public Property VentaNeta() As System.Decimal
            Get
                Return _VentaNeta
            End Get
            Set(ByVal value As System.Decimal)
                _VentaNeta = value
            End Set
        End Property

        Private _TipoImpuesto As System.Byte
        Public Property TipoImpuesto() As System.Byte
            Get
                Return _TipoImpuesto
            End Get
            Set(ByVal value As System.Byte)
                _TipoImpuesto = value
            End Set
        End Property

        Private _ValorIVA As System.Decimal
        Public Property ValorIVA() As System.Decimal
            Get
                Return _ValorIVA
            End Get
            Set(ByVal value As System.Decimal)
                _ValorIVA = value
            End Set
        End Property
        Private _PrecioCosto As System.Decimal
        Public Property PrecioCosto() As System.Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioCosto = value
            End Set
        End Property
        Private _PorcComision As System.Decimal
        Public Property PorcComision() As System.Decimal
            Get
                Return _PorcComision
            End Get
            Set(ByVal value As System.Decimal)
                _PorcComision = value
            End Set
        End Property
        Private _DescuentoAutorizadoPor As System.String = ""
        Public Property DescuentoAutorizadoPor() As System.String
            Get
                Return _DescuentoAutorizadoPor
            End Get
            Set(ByVal value As System.String)
                _DescuentoAutorizadoPor = value
            End Set
        End Property

        Private _IdCentro As System.String = ""
        Public Property IdCentro() As System.String
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.String)
                _IdCentro = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property
    End Class
#End Region
#Region "fac_Pedidos"
    Public Class fac_Pedidos
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Nrc As System.String = ""
        Public Property Nrc() As System.String
            Get
                Return _Nrc
            End Get
            Set(ByVal value As System.String)
                _Nrc = value
            End Set
        End Property

        Private _Nit As System.String = ""
        Public Property Nit() As System.String
            Get
                Return _Nit
            End Get
            Set(ByVal value As System.String)
                _Nit = value
            End Set
        End Property

        Private _Giro As System.String = ""
        Public Property Giro() As System.String
            Get
                Return _Giro
            End Get
            Set(ByVal value As System.String)
                _Giro = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _Telefono As System.String = ""
        Public Property Telefono() As System.String
            Get
                Return _Telefono
            End Get
            Set(ByVal value As System.String)
                _Telefono = value
            End Set
        End Property

        Private _IdMunicipio As System.String = ""
        Public Property IdMunicipio() As System.String
            Get
                Return _IdMunicipio
            End Get
            Set(ByVal value As System.String)
                _IdMunicipio = value
            End Set
        End Property

        Private _IdDepartamento As System.String = ""
        Public Property IdDepartamento() As System.String
            Get
                Return _IdDepartamento
            End Get
            Set(ByVal value As System.String)
                _IdDepartamento = value
            End Set
        End Property

        Private _VentaAcuentaDe As System.String = ""
        Public Property VentaAcuentaDe() As System.String
            Get
                Return _VentaAcuentaDe
            End Get
            Set(ByVal value As System.String)
                _VentaAcuentaDe = value
            End Set
        End Property

        Private _IdTipoComprobante As Integer
        Public Property IdTipoComprobante() As Integer
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As Integer)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _TipoVenta As System.Int32
        Public Property TipoVenta() As System.Int32
            Get
                Return _TipoVenta
            End Get
            Set(ByVal value As System.Int32)
                _TipoVenta = value
            End Set
        End Property

        Private _TipoImpuesto As Int32
        Public Property TipoImpuesto() As Int32
            Get
                Return _TipoImpuesto
            End Get
            Set(ByVal value As Int32)
                _TipoImpuesto = value
            End Set
        End Property

        Private _IdFormaPago As System.Int32
        Public Property IdFormaPago() As System.Int32
            Get
                Return _IdFormaPago
            End Get
            Set(ByVal value As System.Int32)
                _IdFormaPago = value
            End Set
        End Property

        Private _DiasCredito As System.Int32
        Public Property DiasCredito() As System.Int32
            Get
                Return _DiasCredito
            End Get
            Set(ByVal value As System.Int32)
                _DiasCredito = value
            End Set
        End Property

        Private _IdVendedor As System.Int32
        Public Property IdVendedor() As System.Int32
            Get
                Return _IdVendedor
            End Get
            Set(ByVal value As System.Int32)
                _IdVendedor = value
            End Set
        End Property

        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _Devolucion As System.Boolean
        Public Property Devolucion() As System.Boolean
            Get
                Return _Devolucion
            End Get
            Set(ByVal value As System.Boolean)
                _Devolucion = value
            End Set
        End Property

        Private _Facturado As System.Boolean
        Public Property Facturado() As System.Boolean
            Get
                Return _Facturado
            End Get
            Set(ByVal value As System.Boolean)
                _Facturado = value
            End Set
        End Property

        Private _Notas As System.String = ""
        Public Property Notas() As System.String
            Get
                Return _Notas
            End Get
            Set(ByVal value As System.String)
                _Notas = value
            End Set
        End Property

        Private _Anticipo As System.Decimal
        Public Property Anticipo() As System.Decimal
            Get
                Return _Anticipo
            End Get
            Set(ByVal value As System.Decimal)
                _Anticipo = value
            End Set
        End Property

        Private _TotalAfecto As System.Decimal
        Public Property TotalAfecto() As System.Decimal
            Get
                Return _TotalAfecto
            End Get
            Set(ByVal value As System.Decimal)
                _TotalAfecto = value
            End Set
        End Property
        Private _TotalIva As System.Decimal
        Public Property TotalIva() As System.Decimal
            Get
                Return _TotalIva
            End Get
            Set(ByVal value As System.Decimal)
                _TotalIva = value
            End Set
        End Property
        Private _TotalImpuesto1 As System.Decimal
        Public Property TotalImpuesto1() As System.Decimal
            Get
                Return _TotalImpuesto1
            End Get
            Set(ByVal value As System.Decimal)
                _TotalImpuesto1 = value
            End Set
        End Property
        Private _TotalComprobante As System.Decimal
        Public Property TotalComprobante() As System.Decimal
            Get
                Return _TotalComprobante
            End Get
            Set(ByVal value As System.Decimal)
                _TotalComprobante = value
            End Set
        End Property

        Private _PorcDescuento As System.Decimal
        Public Property PorcDescuento() As System.Decimal
            Get
                Return _PorcDescuento
            End Get
            Set(ByVal value As System.Decimal)
                _PorcDescuento = value
            End Set
        End Property
        Private _IdComprobanteFactura As System.Int32
        Public Property IdComprobanteFactura() As System.Int32
            Get
                Return _IdComprobanteFactura
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobanteFactura = value
            End Set
        End Property
        Private _AplicaRetencionPercepcion As System.Boolean
        Public Property AplicaRetencionPercepcion() As System.Boolean
            Get
                Return _AplicaRetencionPercepcion
            End Get
            Set(ByVal value As System.Boolean)
                _AplicaRetencionPercepcion = value
            End Set
        End Property
        Private _Reservado As System.Boolean
        Public Property Reservado() As System.Boolean
            Get
                Return _Reservado
            End Get
            Set(ByVal value As System.Boolean)
                _Reservado = value
            End Set
        End Property
        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_PedidosDetalle"
    Public Class fac_PedidosDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _IdPrecio As System.Int32
        Public Property IdPrecio() As System.Int32
            Get
                Return _IdPrecio
            End Get
            Set(ByVal value As System.Int32)
                _IdPrecio = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioVenta As System.Decimal
        Public Property PrecioVenta() As System.Decimal
            Get
                Return _PrecioVenta
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioVenta = value
            End Set
        End Property

        Private _PorcDescuento As System.Decimal
        Public Property PorcDescuento() As System.Decimal
            Get
                Return _PorcDescuento
            End Get
            Set(ByVal value As System.Decimal)
                _PorcDescuento = value
            End Set
        End Property

        Private _ValorDescuento As System.Decimal
        Public Property ValorDescuento() As System.Decimal
            Get
                Return _ValorDescuento
            End Get
            Set(ByVal value As System.Decimal)
                _ValorDescuento = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property
        Private _VentaNeta As System.Decimal
        Public Property VentaNeta() As System.Decimal
            Get
                Return _VentaNeta
            End Get
            Set(ByVal value As System.Decimal)
                _VentaNeta = value
            End Set
        End Property

        Private _ValorIva As Decimal
        Public Property ValorIva() As Decimal
            Get
                Return _ValorIva
            End Get
            Set(ByVal value As Decimal)
                _ValorIva = value
            End Set
        End Property

        Private _PrecioCosto As Decimal
        Public Property PrecioCosto() As Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As Decimal)
                _PrecioCosto = value
            End Set
        End Property

        Private _TipoProducto As Int32
        Public Property TipoProducto() As Int32
            Get
                Return _TipoProducto
            End Get
            Set(ByVal value As Int32)
                _TipoProducto = value
            End Set
        End Property

        Private _EsEspecial As Boolean
        Public Property EsEspecial() As Boolean
            Get
                Return _EsEspecial
            End Get
            Set(ByVal value As Boolean)
                _EsEspecial = value
            End Set
        End Property

        Private _EsCompuesto As Boolean
        Public Property EsCompuesto() As Boolean
            Get
                Return _EsCompuesto
            End Get
            Set(ByVal value As Boolean)
                _EsCompuesto = value
            End Set
        End Property
        Private _CreadoPor As String = ""
        Public Property CreadoPor() As String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_PedidosConDescuento"
    'entidad temporal solamente para llenar grid con descuentos
    Public Class fac_PedidosDescuento

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Nombre As String = ""
        Public Property Nombre() As String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As String)
                _Nombre = value
            End Set
        End Property

        Private _Cantidad As Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _PrecioVenta As Decimal
        Public Property PrecioVenta() As Decimal
            Get
                Return _PrecioVenta
            End Get
            Set(ByVal value As Decimal)
                _PrecioVenta = value
            End Set
        End Property

        Private _TotalVenta As Decimal
        Public Property TotalVenta() As Decimal
            Get
                Return _TotalVenta
            End Get
            Set(ByVal value As Decimal)
                _TotalVenta = value
            End Set
        End Property

        Private _PorcDescuento As Decimal
        Public Property PorcDescuento() As Decimal
            Get
                Return _PorcDescuento
            End Get
            Set(ByVal value As Decimal)
                _PorcDescuento = value
            End Set
        End Property

        Private _PrecioUnitario As Decimal
        Public Property PrecioUnitario() As Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As Decimal)
                _PrecioUnitario = value
            End Set
        End Property
        Private _TotalFacturado As Decimal
        Public Property TotalFacturado() As Decimal
            Get
                Return _TotalFacturado
            End Get
            Set(ByVal value As Decimal)
                _TotalFacturado = value
            End Set
        End Property

        Private _Diferencia As Decimal
        Public Property Diferencia() As Decimal
            Get
                Return _Diferencia
            End Get
            Set(ByVal value As Decimal)
                _Diferencia = value
            End Set
        End Property
    End Class
#End Region
#Region "fac_PuntosVenta"
    Public Class fac_PuntosVenta
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _Nombre As System.String
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property
        Private _IdCuentaCajaChica As System.String
        Public Property IdCuentaCajaChica() As System.String
            Get
                Return _IdCuentaCajaChica
            End Get
            Set(ByVal value As System.String)
                _IdCuentaCajaChica = value
            End Set
        End Property
        Private _IdCuentaBanco As System.String
        Public Property IdCuentaBanco() As System.String
            Get
                Return _IdCuentaBanco
            End Get
            Set(ByVal value As System.String)
                _IdCuentaBanco = value
            End Set
        End Property

        Private _VentaMensual As System.Decimal
        Public Property VentaMensual() As System.Decimal
            Get
                Return _VentaMensual
            End Get
            Set(ByVal value As System.Decimal)
                _VentaMensual = value
            End Set
        End Property
    End Class
#End Region
#Region "fac_SeriesDocumentos"
    Public Class fac_SeriesDocumentos
        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Serie As System.String
        Public Property Serie() As System.String
            Get
                Return _Serie
            End Get
            Set(ByVal value As System.String)
                _Serie = value
            End Set
        End Property

        Private _FechaAsignacion As Nullable(Of DateTime)
        Public Property FechaAsignacion() As Nullable(Of DateTime)
            Get
                Return _FechaAsignacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaAsignacion = value
            End Set
        End Property

        Private _CreadoPor As System.String
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _UltimoNumero As System.Int32
        Public Property UltimoNumero() As System.Int32
            Get
                Return _UltimoNumero
            End Get
            Set(ByVal value As System.Int32)
                _UltimoNumero = value
            End Set
        End Property

        Private _DesdeNumero As System.Int32
        Public Property DesdeNumero() As System.Int32
            Get
                Return _DesdeNumero
            End Get
            Set(ByVal value As System.Int32)
                _DesdeNumero = value
            End Set
        End Property

        Private _HastaNumero As System.Int32
        Public Property HastaNumero() As System.Int32
            Get
                Return _HastaNumero
            End Get
            Set(ByVal value As System.Int32)
                _HastaNumero = value
            End Set
        End Property

        Private _EsActivo As System.Boolean
        Public Property EsActivo() As System.Boolean
            Get
                Return _EsActivo
            End Get
            Set(ByVal value As System.Boolean)
                _EsActivo = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_Vendedores"
    Public Class fac_Vendedores
        Private _IdVendedor As Integer
        Public Property IdVendedor() As Integer
            Get
                Return _IdVendedor
            End Get
            Set(ByVal value As Integer)
                _IdVendedor = value
            End Set
        End Property

        Private _Nombre As System.String
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Activo As System.Boolean
        Public Property Activo() As System.Boolean
            Get
                Return _Activo
            End Get
            Set(ByVal value As System.Boolean)
                _Activo = value
            End Set
        End Property
        Private _IdSucursal As Integer
        Public Property IdSucursal() As Integer
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As Integer)
                _IdSucursal = value
            End Set
        End Property
        Private _Telefonos As System.String
        Public Property Telefonos() As System.String
            Get
                Return _Telefonos
            End Get
            Set(ByVal value As System.String)
                _Telefonos = value
            End Set
        End Property

        Private _Direccion As System.String
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _LimiteVenta As System.Decimal
        Public Property LimiteVenta() As System.Decimal
            Get
                Return _LimiteVenta
            End Get
            Set(ByVal value As System.Decimal)
                _LimiteVenta = value
            End Set
        End Property

        Private _PorcComision As System.Decimal
        Public Property PorcComision() As System.Decimal
            Get
                Return _PorcComision
            End Get
            Set(ByVal value As System.Decimal)
                _PorcComision = value
            End Set
        End Property
        Private _IdEmpleado As System.String = ""
        Public Property IdEmpleado() As System.String
            Get
                Return _IdEmpleado
            End Get
            Set(ByVal value As System.String)
                _IdEmpleado = value
            End Set
        End Property
        Private _AllRCV As System.Boolean
        Public Property AllRCV() As System.Boolean
            Get
                Return _AllRCV
            End Get
            Set(value As System.Boolean)
                _AllRCV = value
            End Set
        End Property
    End Class
#End Region
#Region "fac_VendedoresDetalle"
    Public Class fac_VendedoresDetalle
        Private _IdVendedor As System.Int32
        Public Property IdVendedor() As System.Int32
            Get
                Return _IdVendedor
            End Get
            Set(ByVal value As System.Int32)
                _IdVendedor = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _RangoDesde As System.Decimal
        Public Property RangoDesde() As System.Decimal
            Get
                Return _RangoDesde
            End Get
            Set(ByVal value As System.Decimal)
                _RangoDesde = value
            End Set
        End Property

        Private _RangoHasta As System.Decimal
        Public Property RangoHasta() As System.Decimal
            Get
                Return _RangoHasta
            End Get
            Set(ByVal value As System.Decimal)
                _RangoHasta = value
            End Set
        End Property

        Private _Porcentaje As System.Decimal
        Public Property Porcentaje() As System.Decimal
            Get
                Return _Porcentaje
            End Get
            Set(ByVal value As System.Decimal)
                _Porcentaje = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_VendedoresClientes"
    Public Class fac_VendedoresClientes
        Private _IdVendedor As System.Int32
        Public Property IdVendedor() As System.Int32
            Get
                Return _IdVendedor
            End Get
            Set(ByVal value As System.Int32)
                _IdVendedor = value
            End Set
        End Property

        Private _IdCliente As System.String
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(value As System.String)
                _IdCliente = value
            End Set
        End Property

    End Class
#End Region

#Region "fac_FormasPago"
    Public Class fac_FormasPago
        Private _IdFormaPago As Integer
        Public Property IdFormaPago() As Integer
            Get
                Return _IdFormaPago
            End Get
            Set(ByVal value As Integer)
                _IdFormaPago = value
            End Set
        End Property

        Private _Nombre As System.String
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _IdCuentaContable As System.String
        Public Property IdCuentaContable() As System.String
            Get
                Return _IdCuentaContable
            End Get
            Set(ByVal value As System.String)
                _IdCuentaContable = value
            End Set
        End Property
        Private _DiasCredito As System.Int32
        Public Property DiasCredito() As System.Int32
            Get
                Return _DiasCredito
            End Get
            Set(ByVal value As System.Int32)
                _DiasCredito = value
            End Set
        End Property
    End Class
#End Region
#Region "fac_Rutas"
    Public Class fac_Rutas
        Private _IdRuta As System.Int32
        Public Property IdRuta() As System.Int32
            Get
                Return _IdRuta
            End Get
            Set(ByVal value As System.Int32)
                _IdRuta = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_NotasCredito"
    Public Class fac_NotasCredito
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdComprobVenta As System.Int32
        Public Property IdComprobVenta() As System.Int32
            Get
                Return _IdComprobVenta
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobVenta = value
            End Set
        End Property

        Private _MontoAbonado As System.Decimal
        Public Property MontoAbonado() As System.Decimal
            Get
                Return _MontoAbonado
            End Get
            Set(ByVal value As System.Decimal)
                _MontoAbonado = value
            End Set
        End Property

        Private _SaldoComprobante As System.Decimal
        Public Property SaldoComprobante() As System.Decimal
            Get
                Return _SaldoComprobante
            End Get
            Set(ByVal value As System.Decimal)
                _SaldoComprobante = value
            End Set
        End Property

        Private _NumComprobanteVenta As System.String = ""
        Public Property NumComprobanteVenta() As System.String
            Get
                Return _NumComprobanteVenta
            End Get
            Set(ByVal value As System.String)
                _NumComprobanteVenta = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_Retencion"
    Public Class fac_Retencion
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Serie As System.String = ""
        Public Property Serie() As System.String
            Get
                Return _Serie
            End Get
            Set(ByVal value As System.String)
                _Serie = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdClienteProveedor As System.String
        Public Property IdClienteProveedor() As System.String
            Get
                Return _IdClienteProveedor
            End Get
            Set(ByVal value As System.String)
                _IdClienteProveedor = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _Giro As System.String = ""
        Public Property Giro() As System.String
            Get
                Return _Giro
            End Get
            Set(ByVal value As System.String)
                _Giro = value
            End Set
        End Property

        Private _Nrc As System.String = ""
        Public Property Nrc() As System.String
            Get
                Return _Nrc
            End Get
            Set(ByVal value As System.String)
                _Nrc = value
            End Set
        End Property

        Private _Nit As System.String = ""
        Public Property Nit() As System.String
            Get
                Return _Nit
            End Get
            Set(ByVal value As System.String)
                _Nit = value
            End Set
        End Property

        Private _Observaciones As System.String = ""
        Public Property Observaciones() As System.String
            Get
                Return _Observaciones
            End Get
            Set(ByVal value As System.String)
                _Observaciones = value
            End Set
        End Property

        Private _TotalIva As System.Decimal
        Public Property TotalIva() As System.Decimal
            Get
                Return _TotalIva
            End Get
            Set(ByVal value As System.Decimal)
                _TotalIva = value
            End Set
        End Property

        Private _TotalImpuesto1 As System.Decimal
        Public Property TotalImpuesto1() As System.Decimal
            Get
                Return _TotalImpuesto1
            End Get
            Set(ByVal value As System.Decimal)
                _TotalImpuesto1 = value
            End Set
        End Property

        Private _TotalImpuesto2 As System.Decimal
        Public Property TotalImpuesto2() As System.Decimal
            Get
                Return _TotalImpuesto2
            End Get
            Set(ByVal value As System.Decimal)
                _TotalImpuesto2 = value
            End Set
        End Property

        Private _TotalComprobante As System.Decimal
        Public Property TotalComprobante() As System.Decimal
            Get
                Return _TotalComprobante
            End Get
            Set(ByVal value As System.Decimal)
                _TotalComprobante = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property
        Private _FechaContable As Nullable(Of DateTime)
        Public Property FechaContable() As Nullable(Of DateTime)
            Get
                Return _FechaContable
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaContable = value
            End Set
        End Property
    End Class
#End Region
#Region "fac_RetencionDetalle"
    Public Class fac_RetencionDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property

    End Class
#End Region
#Region "fac_Gestiones"
    Public Class fac_Gestiones
        Private _IdGestion As System.Int32
        Public Property IdGestion() As System.Int32
            Get
                Return _IdGestion
            End Get
            Set(ByVal value As System.Int32)
                _IdGestion = value
            End Set
        End Property

        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _NombreCliente As System.String = ""
        Public Property NombreCliente() As System.String
            Get
                Return _NombreCliente
            End Get
            Set(ByVal value As System.String)
                _NombreCliente = value
            End Set
        End Property

        Private _Motivo As System.String = ""
        Public Property Motivo() As System.String
            Get
                Return _Motivo
            End Get
            Set(ByVal value As System.String)
                _Motivo = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _HoraLlamada As Nullable(Of DateTime)
        Public Property HoraLlamada() As Nullable(Of DateTime)
            Get
                Return _HoraLlamada
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _HoraLlamada = value
            End Set
        End Property

        Private _ComentarioGeneral As System.String = ""
        Public Property ComentarioGeneral() As System.String
            Get
                Return _ComentarioGeneral
            End Get
            Set(ByVal value As System.String)
                _ComentarioGeneral = value
            End Set
        End Property

        Private _ComentarioCliente As System.String = ""
        Public Property ComentarioCliente() As System.String
            Get
                Return _ComentarioCliente
            End Get
            Set(ByVal value As System.String)
                _ComentarioCliente = value
            End Set
        End Property

        Private _ProximaGestion As Nullable(Of DateTime)
        Public Property ProximaGestion() As Nullable(Of DateTime)
            Get
                Return _ProximaGestion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _ProximaGestion = value
            End Set
        End Property

        Private _FechaVisita As Nullable(Of DateTime)
        Public Property FechaVisita() As Nullable(Of DateTime)
            Get
                Return _FechaVisita
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaVisita = value
            End Set
        End Property

        Private _HoraVisita As Nullable(Of DateTime)
        Public Property HoraVisita() As Nullable(Of DateTime)
            Get
                Return _HoraVisita
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _HoraVisita = value
            End Set
        End Property

        Private _IdOrden As System.Int32
        Public Property IdOrden() As System.Int32
            Get
                Return _IdOrden
            End Get
            Set(ByVal value As System.Int32)
                _IdOrden = value
            End Set
        End Property

        Private _IdVendedor As System.Int32
        Public Property IdVendedor() As System.Int32
            Get
                Return _IdVendedor
            End Get
            Set(ByVal value As System.Int32)
                _IdVendedor = value
            End Set
        End Property

        Private _IdGestionProgramada As System.Int32
        Public Property IdGestionProgramada() As System.Int32
            Get
                Return _IdGestionProgramada
            End Get
            Set(ByVal value As System.Int32)
                _IdGestionProgramada = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _IdCotizacion As System.Int32
        Public Property IdCotizacion() As System.Int32
            Get
                Return _IdCotizacion
            End Get
            Set(ByVal value As System.Int32)
                _IdCotizacion = value
            End Set
        End Property

    End Class
#End Region



    'INVENTARIOS
#Region "inv_Categorias"
    Public Class inv_Categorias
        Private _IdCategoria As System.Int32
        Public Property IdCategoria() As System.Int32
            Get
                Return _IdCategoria
            End Get
            Set(ByVal value As System.Int32)
                _IdCategoria = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_CategoriasDetalle"
    Public Class inv_CategoriasDetalle
        Private _IdCategoria As System.Int32
        Public Property IdCategoria() As System.Int32
            Get
                Return _IdCategoria
            End Get
            Set(ByVal value As System.Int32)
                _IdCategoria = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdCuentaIngreso As System.String = ""
        Public Property IdCuentaIngreso() As System.String
            Get
                Return _IdCuentaIngreso
            End Get
            Set(ByVal value As System.String)
                _IdCuentaIngreso = value
            End Set
        End Property

    End Class
#End Region



#Region "inv_Bodegas"
    Public Class inv_Bodegas
        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Responsable As System.String = ""
        Public Property Responsable() As System.String
            Get
                Return _Responsable
            End Get
            Set(ByVal value As System.String)
                _Responsable = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _Telefono As System.String = ""
        Public Property Telefono() As System.String
            Get
                Return _Telefono
            End Get
            Set(ByVal value As System.String)
                _Telefono = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_Existencias"
    Public Class inv_Existencias
        Private _IdProducto As System.Int32
        Public Property IdProducto() As System.Int32
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.Int32)
                _IdProducto = value
            End Set
        End Property

        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _Entrada As System.Decimal
        Public Property Entrada() As System.Decimal
            Get
                Return _Entrada
            End Get
            Set(ByVal value As System.Decimal)
                _Entrada = value
            End Set
        End Property

        Private _Salida As System.Decimal
        Public Property Salida() As System.Decimal
            Get
                Return _Salida
            End Get
            Set(ByVal value As System.Decimal)
                _Salida = value
            End Set
        End Property

        Private _Saldo As System.Decimal
        Public Property Saldo() As System.Decimal
            Get
                Return _Saldo
            End Get
            Set(ByVal value As System.Decimal)
                _Saldo = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_Grupos"
    Public Class inv_Grupos
        Private _IdGrupo As System.Int32
        Public Property IdGrupo() As System.Int32
            Get
                Return _IdGrupo
            End Get
            Set(ByVal value As System.Int32)
                _IdGrupo = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _IdCategoria As System.Int32
        Public Property IdCategoria() As System.Int32
            Get
                Return _IdCategoria
            End Get
            Set(ByVal value As System.Int32)
                _IdCategoria = value
            End Set
        End Property

        Private _IdCuentaInventario As System.String = ""
        Public Property IdCuentaInventario() As System.String
            Get
                Return _IdCuentaInventario
            End Get
            Set(ByVal value As System.String)
                _IdCuentaInventario = value
            End Set
        End Property

        Private _IdCuentaIngreso As System.String = ""
        Public Property IdCuentaIngreso() As System.String
            Get
                Return _IdCuentaIngreso
            End Get
            Set(ByVal value As System.String)
                _IdCuentaIngreso = value
            End Set
        End Property

        Private _IdCuentaCostos As System.String = ""
        Public Property IdCuentaCostos() As System.String
            Get
                Return _IdCuentaCostos
            End Get
            Set(ByVal value As System.String)
                _IdCuentaCostos = value
            End Set
        End Property

        Private _IdCuentaRebajas As System.String = ""
        Public Property IdCuentaRebajas() As System.String
            Get
                Return _IdCuentaRebajas
            End Get
            Set(ByVal value As System.String)
                _IdCuentaRebajas = value
            End Set
        End Property

        Private _IdCuentaDevolucion As System.String = ""
        Public Property IdCuentaDevolucion() As System.String
            Get
                Return _IdCuentaDevolucion
            End Get
            Set(ByVal value As System.String)
                _IdCuentaDevolucion = value
            End Set
        End Property

        Private _UltCodProducto As System.Int32
        Public Property UltCodProducto() As System.Int32
            Get
                Return _UltCodProducto
            End Get
            Set(ByVal value As System.Int32)
                _UltCodProducto = value
            End Set
        End Property

        Private _pje1 As System.Decimal
        Public Property pje1() As System.Decimal
            Get
                Return _pje1
            End Get
            Set(ByVal value As System.Decimal)
                _pje1 = value
            End Set
        End Property

        Private _pje2 As System.Decimal
        Public Property pje2() As System.Decimal
            Get
                Return _pje2
            End Get
            Set(ByVal value As System.Decimal)
                _pje2 = value
            End Set
        End Property

        Private _pje3 As System.Decimal
        Public Property pje3() As System.Decimal
            Get
                Return _pje3
            End Get
            Set(ByVal value As System.Decimal)
                _pje3 = value
            End Set
        End Property

        Private _pje4 As System.Decimal
        Public Property pje4() As System.Decimal
            Get
                Return _pje4
            End Get
            Set(ByVal value As System.Decimal)
                _pje4 = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_Kardex"
    Public Class inv_Kardex
        Private _IdMovimiento As System.Int32
        Public Property IdMovimiento() As System.Int32
            Get
                Return _IdMovimiento
            End Get
            Set(ByVal value As System.Int32)
                _IdMovimiento = value
            End Set
        End Property

        Private _IdTipoComprobante As System.Int32
        Public Property IdTipoComprobante() As System.Int32
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _IdProducto As System.Int32
        Public Property IdProducto() As System.Int32
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.Int32)
                _IdProducto = value
            End Set
        End Property

        Private _TipoAplicacion As System.Int32
        Public Property TipoAplicacion() As System.Int32
            Get
                Return _TipoAplicacion
            End Get
            Set(ByVal value As System.Int32)
                _TipoAplicacion = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioCosto As System.Decimal
        Public Property PrecioCosto() As System.Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioCosto = value
            End Set
        End Property

        Private _SaldoExistencias As System.Decimal
        Public Property SaldoExistencias() As System.Decimal
            Get
                Return _SaldoExistencias
            End Get
            Set(ByVal value As System.Decimal)
                _SaldoExistencias = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_Marcas"
    Public Class inv_Marcas
        Private _IdMarca As System.Int32
        Public Property IdMarca() As System.Int32
            Get
                Return _IdMarca
            End Get
            Set(ByVal value As System.Int32)
                _IdMarca = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_Entradas"
    Public Class inv_Entradas
        Private _IdComprobante As Integer
        Public Property IdComprobante() As Integer
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As Integer)
                _IdComprobante = value
            End Set
        End Property

        Private _IdTipoComprobante As System.Byte
        Public Property IdTipoComprobante() As System.Byte
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Byte)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _IdComprobVenta As System.Int32
        Public Property IdComprobVenta() As System.Int32
            Get
                Return _IdComprobVenta
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobVenta = value
            End Set
        End Property
        Private _AplicadaInventario As Boolean
        Public Property AplicadaInventario() As Boolean
            Get
                Return _AplicadaInventario
            End Get
            Set(ByVal value As Boolean)
                _AplicadaInventario = value
            End Set
        End Property
        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_EntradasDetalle"
    Public Class inv_EntradasDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioCosto As System.Decimal
        Public Property PrecioCosto() As System.Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioCosto = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property

        Private _IdCentro As System.String = ""
        Public Property IdCentro() As System.String
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.String)
                _IdCentro = value
            End Set
        End Property


        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_Salidas"
    Public Class inv_Salidas
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdTipoComprobante As System.Byte
        Public Property IdTipoComprobante() As System.Byte
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Byte)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property



        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _AplicadaInventario As Boolean
        Public Property AplicadaInventario() As Boolean
            Get
                Return _AplicadaInventario
            End Get
            Set(ByVal value As Boolean)
                _AplicadaInventario = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property
        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property
        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property
    End Class
#End Region
#Region "inv_SalidasDetalle"
    Public Class inv_SalidasDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioCosto As System.Decimal
        Public Property PrecioCosto() As System.Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioCosto = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property

        Private _IdCentro As System.String = ""
        Public Property IdCentro() As System.String
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.String)
                _IdCentro = value
            End Set
        End Property


        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_Traslados"
    Public Class inv_Traslados
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdTipoComprobante As System.Int32
        Public Property IdTipoComprobante() As System.Int32
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _IdBodegaSalida As System.Int32
        Public Property IdBodegaSalida() As System.Int32
            Get
                Return _IdBodegaSalida
            End Get
            Set(ByVal value As System.Int32)
                _IdBodegaSalida = value
            End Set
        End Property

        Private _IdBodegaIngreso As System.Int32
        Public Property IdBodegaIngreso() As System.Int32
            Get
                Return _IdBodegaIngreso
            End Get
            Set(ByVal value As System.Int32)
                _IdBodegaIngreso = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _IdSucursalRecibe As System.Int32
        Public Property IdSucursalRecibe() As System.Int32
            Get
                Return _IdSucursalRecibe
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursalRecibe = value
            End Set
        End Property
        Private _AplicadaInventario As System.Boolean
        Public Property AplicadaInventario() As System.Boolean
            Get
                Return _AplicadaInventario
            End Get
            Set(ByVal value As System.Boolean)
                _AplicadaInventario = value
            End Set
        End Property

        Private _ImprimirValores As System.Boolean
        Public Property ImprimirValores() As System.Boolean
            Get
                Return _ImprimirValores
            End Get
            Set(ByVal value As System.Boolean)
                _ImprimirValores = value
            End Set
        End Property
        Private _IdPedido As System.Int32
        Public Property IdPedido() As System.Int32
            Get
                Return _IdPedido
            End Get
            Set(ByVal value As System.Int32)
                _IdPedido = value
            End Set
        End Property
        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region

#Region "inv_Producciones"
    Public Class inv_Producciones
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property
        Private _IdTipoComprobante As System.Int32
        Public Property IdTipoComprobante() As System.Int32
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _AplicadaInventario As System.Boolean
        Public Property AplicadaInventario() As System.Boolean
            Get
                Return _AplicadaInventario
            End Get
            Set(ByVal value As System.Boolean)
                _AplicadaInventario = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_ProduccionesDetalle"
    Public Class inv_ProduccionesDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioCosto As System.Decimal
        Public Property PrecioCosto() As System.Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioCosto = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property

        Private _CantidadPosible As System.Decimal
        Public Property CantidadPosible() As System.Decimal
            Get
                Return _CantidadPosible
            End Get
            Set(ByVal value As System.Decimal)
                _CantidadPosible = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_GastosProduccion"
    Public Class inv_GastosProduccion
        Private _IdGasto As System.Int32
        Public Property IdGasto() As System.Int32
            Get
                Return _IdGasto
            End Get
            Set(ByVal value As System.Int32)
                _IdGasto = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _IdCuenta As System.String = ""
        Public Property IdCuenta() As System.String
            Get
                Return _IdCuenta
            End Get
            Set(ByVal value As System.String)
                _IdCuenta = value
            End Set
        End Property

    End Class
#End Region

#Region "inv_ProduccionesGastosDetalle"
    Public Class inv_ProduccionesGastosDetalle
        Private _IdProduccion As System.Int32
        Public Property IdProduccion() As System.Int32
            Get
                Return _IdProduccion
            End Get
            Set(ByVal value As System.Int32)
                _IdProduccion = value
            End Set
        End Property

        Private _IdGasto As System.Int32
        Public Property IdGasto() As System.Int32
            Get
                Return _IdGasto
            End Get
            Set(ByVal value As System.Int32)
                _IdGasto = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

        Private _Contabilizar As System.Boolean
        Public Property Contabilizar() As System.Boolean
            Get
                Return _Contabilizar
            End Get
            Set(ByVal value As System.Boolean)
                _Contabilizar = value
            End Set
        End Property

    End Class
#End Region

#Region "inv_ProductosMargenes"
    Public Class inv_ProductosMargenes
        Private _IdMargen As System.Int32
        Public Property IdMargen() As System.Int32
            Get
                Return _IdMargen
            End Get
            Set(ByVal value As System.Int32)
                _IdMargen = value
            End Set
        End Property

        Private _Desde As System.Decimal
        Public Property Desde() As System.Decimal
            Get
                Return _Desde
            End Get
            Set(ByVal value As System.Decimal)
                _Desde = value
            End Set
        End Property

        Private _Hasta As System.Decimal
        Public Property Hasta() As System.Decimal
            Get
                Return _Hasta
            End Get
            Set(ByVal value As System.Decimal)
                _Hasta = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_ProductosKit"
    Public Class inv_ProductosKit
        Private _IdKit As System.String = ""
        Public Property IdKit() As System.String
            Get
                Return _IdKit
            End Get
            Set(ByVal value As System.String)
                _IdKit = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_TomaFisica"
    Public Class inv_TomaFisica
        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _ConteoFisico As System.Decimal
        Public Property ConteoFisico() As System.Decimal
            Get
                Return _ConteoFisico
            End Get
            Set(ByVal value As System.Decimal)
                _ConteoFisico = value
            End Set
        End Property

        Private _ConteoAcumulado As System.Decimal
        Public Property ConteoAcumulado() As System.Decimal
            Get
                Return _ConteoAcumulado
            End Get
            Set(ByVal value As System.Decimal)
                _ConteoAcumulado = value
            End Set
        End Property

        Private _ExistenciaActual As System.Decimal
        Public Property ExistenciaActual() As System.Decimal
            Get
                Return _ExistenciaActual
            End Get
            Set(ByVal value As System.Decimal)
                _ExistenciaActual = value
            End Set
        End Property

        Private _PrecioCosto As System.Decimal
        Public Property PrecioCosto() As System.Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioCosto = value
            End Set
        End Property

        Private _IdGrupo As System.Int32
        Public Property IdGrupo() As System.Int32
            Get
                Return _IdGrupo
            End Get
            Set(ByVal value As System.Int32)
                _IdGrupo = value
            End Set
        End Property

        Private _IdSubGrupo As System.Int32
        Public Property IdSubGrupo() As System.Int32
            Get
                Return _IdSubGrupo
            End Get
            Set(ByVal value As System.Int32)
                _IdSubGrupo = value
            End Set
        End Property

        Private _Revisado As System.Boolean
        Public Property Revisado() As System.Boolean
            Get
                Return _Revisado
            End Get
            Set(ByVal value As System.Boolean)
                _Revisado = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _HechaHoraCreacion As Nullable(Of DateTime)
        Public Property HechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _HechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _HechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region

#Region "inv_TrasladosDetalle"
    Public Class inv_TrasladosDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property
  
        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_Precios"
    Public Class inv_Precios
        Private _IdPrecio As System.Int32
        Public Property IdPrecio() As System.Int32
            Get
                Return _IdPrecio
            End Get
            Set(ByVal value As System.Int32)
                _IdPrecio = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _RequiereAutorizacion As System.Boolean
        Public Property RequiereAutorizacion() As System.Boolean
            Get
                Return _RequiereAutorizacion
            End Get
            Set(ByVal value As System.Boolean)
                _RequiereAutorizacion = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_Productos"
    Public Class inv_Productos
        Private _IdProducto As String = ""
        Public Property IdProducto() As String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As String)
                _IdProducto = value
            End Set
        End Property
        Private _CodBarra As String = ""
        Public Property CodBarra() As String
            Get
                Return _CodBarra
            End Get
            Set(ByVal value As String)
                _CodBarra = value
            End Set
        End Property
        Private _Nombre As String = ""
        Public Property Nombre() As String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As String)
                _Nombre = value
            End Set
        End Property
        Private _Traduccion As String = ""
        Public Property Traduccion() As String
            Get
                Return _Traduccion
            End Get
            Set(ByVal value As String)
                _Traduccion = value
            End Set
        End Property

        Private _Origen As String = ""
        Public Property Origen() As String
            Get
                Return _Origen
            End Get
            Set(ByVal value As String)
                _Origen = value
            End Set
        End Property

        Private _Cuantia As String = ""
        Public Property Cuantia() As String
            Get
                Return _Cuantia
            End Get
            Set(ByVal value As String)
                _Cuantia = value
            End Set
        End Property

        Private _IdGrupo As Int32
        Public Property IdGrupo() As Int32
            Get
                Return _IdGrupo
            End Get
            Set(ByVal value As Int32)
                _IdGrupo = value
            End Set
        End Property

        Private _IdSubGrupo As Int32
        Public Property IdSubGrupo() As Int32
            Get
                Return _IdSubGrupo
            End Get
            Set(ByVal value As Int32)
                _IdSubGrupo = value
            End Set
        End Property

        Private _IdUnidad As Int32
        Public Property IdUnidad() As Int32
            Get
                Return _IdUnidad
            End Get
            Set(ByVal value As Int32)
                _IdUnidad = value
            End Set
        End Property

        Private _IdMarca As Int32
        Public Property IdMarca() As Int32
            Get
                Return _IdMarca
            End Get
            Set(ByVal value As Int32)
                _IdMarca = value
            End Set
        End Property

        Private _IdProveedor As String = ""
        Public Property IdProveedor() As String
            Get
                Return _IdProveedor
            End Get
            Set(ByVal value As String)
                _IdProveedor = value
            End Set
        End Property

        Private _IdUbicacion As Int32
        Public Property IdUbicacion() As Int32
            Get
                Return _IdUbicacion
            End Get
            Set(ByVal value As Int32)
                _IdUbicacion = value
            End Set
        End Property

        Private _IdCategoria As Int32
        Public Property IdCategoria() As Int32
            Get
                Return _IdCategoria
            End Get
            Set(ByVal value As Int32)
                _IdCategoria = value
            End Set
        End Property

        Private _Talla As String = ""
        Public Property Talla() As String
            Get
                Return _Talla
            End Get
            Set(ByVal value As String)
                _Talla = value
            End Set
        End Property

        Private _Color As String = ""
        Public Property Color() As String
            Get
                Return _Color
            End Get
            Set(ByVal value As String)
                _Color = value
            End Set
        End Property

        Private _Estilo As String = ""
        Public Property Estilo() As String
            Get
                Return _Estilo
            End Get
            Set(ByVal value As String)
                _Estilo = value
            End Set
        End Property

        Private _UnidadesPresentacion As Decimal
        Public Property UnidadesPresentacion() As Decimal
            Get
                Return _UnidadesPresentacion
            End Get
            Set(ByVal value As Decimal)
                _UnidadesPresentacion = value
            End Set
        End Property

        Private _ExistenciaMaxima As Decimal
        Public Property ExistenciaMaxima() As Decimal
            Get
                Return _ExistenciaMaxima
            End Get
            Set(ByVal value As Decimal)
                _ExistenciaMaxima = value
            End Set
        End Property

        Private _ExistenciaMinima As Decimal
        Public Property ExistenciaMinima() As Decimal
            Get
                Return _ExistenciaMinima
            End Get
            Set(ByVal value As Decimal)
                _ExistenciaMinima = value
            End Set
        End Property

        Private _TipoProducto As Int32
        Public Property TipoProducto() As Int32
            Get
                Return _TipoProducto
            End Get
            Set(ByVal value As Int32)
                _TipoProducto = value
            End Set
        End Property

        Private _EsExento As Boolean
        Public Property EsExento() As Boolean
            Get
                Return _EsExento
            End Get
            Set(ByVal value As Boolean)
                _EsExento = value
            End Set
        End Property

        Private _EsEspecial As Boolean
        Public Property EsEspecial() As Boolean
            Get
                Return _EsEspecial
            End Get
            Set(ByVal value As Boolean)
                _EsEspecial = value
            End Set
        End Property

        Private _PermiteFacturar As Boolean
        Public Property PermiteFacturar() As Boolean
            Get
                Return _PermiteFacturar
            End Get
            Set(ByVal value As Boolean)
                _PermiteFacturar = value
            End Set
        End Property

        Private _FechaIngreso As Nullable(Of DateTime)
        Public Property FechaIngreso() As Nullable(Of DateTime)
            Get
                Return _FechaIngreso
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaIngreso = value
            End Set
        End Property

        Private _PrecioUltCosto As Decimal
        Public Property PrecioUltCosto() As Decimal
            Get
                Return _PrecioUltCosto
            End Get
            Set(ByVal value As Decimal)
                _PrecioUltCosto = value
            End Set
        End Property

        Private _PrecioCosto As Decimal
        Public Property PrecioCosto() As Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As Decimal)
                _PrecioCosto = value
            End Set
        End Property

        Private _InformacionAdicional As System.String = ""
        Public Property InformacionAdicional() As System.String
            Get
                Return _InformacionAdicional
            End Get
            Set(ByVal value As System.String)
                _InformacionAdicional = value
            End Set
        End Property

        Private _IdCuentaIng As System.String = ""
        Public Property IdCuentaIng() As System.String
            Get
                Return _IdCuentaIng
            End Get
            Set(ByVal value As System.String)
                _IdCuentaIng = value
            End Set
        End Property

        Private _IdCuentaInv As System.String = ""
        Public Property IdCuentaInv() As System.String
            Get
                Return _IdCuentaInv
            End Get
            Set(ByVal value As System.String)
                _IdCuentaInv = value
            End Set
        End Property

        Private _IdCuentaCos As System.String = ""
        Public Property IdCuentaCos() As System.String
            Get
                Return _IdCuentaCos
            End Get
            Set(ByVal value As System.String)
                _IdCuentaCos = value
            End Set
        End Property

        Private _ArchivoImagen As System.String = ""
        Public Property ArchivoImagen() As System.String
            Get
                Return _ArchivoImagen
            End Get
            Set(ByVal value As System.String)
                _ArchivoImagen = value
            End Set
        End Property

        Private _EstadoProducto As System.Byte
        Public Property EstadoProducto() As System.Byte
            Get
                Return _EstadoProducto
            End Get
            Set(ByVal value As System.Byte)
                _EstadoProducto = value
            End Set
        End Property

        Private _Compuesto As Boolean
        Public Property Compuesto() As Boolean
            Get
                Return _Compuesto
            End Get
            Set(ByVal value As Boolean)
                _Compuesto = value
            End Set
        End Property
        Private _PorcComision As System.Decimal
        Public Property PorcComision() As System.Decimal
            Get
                Return _PorcComision
            End Get
            Set(ByVal value As System.Decimal)
                _PorcComision = value
            End Set
        End Property
        Private _IdClasificacion As System.Int32
        Public Property IdClasificacion() As System.Int32
            Get
                Return _IdClasificacion
            End Get
            Set(ByVal value As System.Int32)
                _IdClasificacion = value
            End Set
        End Property

        Private _IdCentro As System.String
        Public Property IdCentro() As System.String
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.String)
                _IdCentro = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_ProductosDetalle"
    Public Class inv_ProductosDetalle
        Private _IdProducto As System.Int32
        Public Property IdProducto() As System.Int32
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.Int32)
                _IdProducto = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Valor As System.String = ""
        Public Property Valor() As System.String
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.String)
                _Valor = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_ProductosPrecios"
    Public Class inv_ProductosPrecios
        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _IdPrecio As System.Int32
        Public Property IdPrecio() As System.Int32
            Get
                Return _IdPrecio
            End Get
            Set(ByVal value As System.Int32)
                _IdPrecio = value
            End Set
        End Property

        Private _Precio As System.Decimal
        Public Property Precio() As System.Decimal
            Get
                Return _Precio
            End Get
            Set(ByVal value As System.Decimal)
                _Precio = value
            End Set
        End Property

        Private _Descuento As System.Decimal
        Public Property Descuento() As System.Decimal
            Get
                Return _Descuento
            End Get
            Set(ByVal value As System.Decimal)
                _Descuento = value
            End Set
        End Property

        Private _Margen As System.Decimal
        Public Property Margen() As System.Decimal
            Get
                Return _Margen
            End Get
            Set(ByVal value As System.Decimal)
                _Margen = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_SubGrupos"
    Public Class inv_SubGrupos
        Private _IdSubGrupo As System.Int32
        Public Property IdSubGrupo() As System.Int32
            Get
                Return _IdSubGrupo
            End Get
            Set(ByVal value As System.Int32)
                _IdSubGrupo = value
            End Set
        End Property

        Private _IdGrupo As System.Int32
        Public Property IdGrupo() As System.Int32
            Get
                Return _IdGrupo
            End Get
            Set(ByVal value As System.Int32)
                _IdGrupo = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_Ubicaciones"
    Public Class inv_Ubicaciones
        Private _IdUbicacion As System.Int32
        Public Property IdUbicacion() As System.Int32
            Get
                Return _IdUbicacion
            End Get
            Set(ByVal value As System.Int32)
                _IdUbicacion = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_UnidadesMedida"
    Public Class inv_UnidadesMedida
        Private _IdUnidad As System.Int32
        Public Property IdUnidad() As System.Int32
            Get
                Return _IdUnidad
            End Get
            Set(ByVal value As System.Int32)
                _IdUnidad = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_Formulas"
    Public Class inv_Formulas
        Private _IdFormula As System.Int32
        Public Property IdFormula() As System.Int32
            Get
                Return _IdFormula
            End Get
            Set(ByVal value As System.Int32)
                _IdFormula = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region

#Region "inv_FormulasDetalle"
    Public Class inv_FormulasDetalle
        Private _IdFormula As System.Int32
        Public Property IdFormula() As System.Int32
            Get
                Return _IdFormula
            End Get
            Set(ByVal value As System.Int32)
                _IdFormula = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _PrecioCosto As System.Decimal
        Public Property PrecioCosto() As System.Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioCosto = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_ProduccionesGastosProductos"
    Public Class inv_ProduccionesGastosProductos
        Private _IdProduccion As System.Int32
        Public Property IdProduccion() As System.Int32
            Get
                Return _IdProduccion
            End Get
            Set(ByVal value As System.Int32)
                _IdProduccion = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _IdGasto As System.Int32
        Public Property IdGasto() As System.Int32
            Get
                Return _IdGasto
            End Get
            Set(ByVal value As System.Int32)
                _IdGasto = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _AplicaGasto As System.Boolean
        Public Property AplicaGasto() As System.Boolean
            Get
                Return _AplicaGasto
            End Get
            Set(ByVal value As System.Boolean)
                _AplicaGasto = value
            End Set
        End Property

    End Class
#End Region
#Region "inv_Vinetas"
    Public Class inv_Vinetas
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _IdMarca As System.Int32
        Public Property IdMarca() As System.Int32
            Get
                Return _IdMarca
            End Get
            Set(ByVal value As System.Int32)
                _IdMarca = value
            End Set
        End Property

        Private _IdUnidad As System.Int32
        Public Property IdUnidad() As System.Int32
            Get
                Return _IdUnidad
            End Get
            Set(ByVal value As System.Int32)
                _IdUnidad = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region


    'PRODUCCION
#Region "pro_Actividades"
    Public Class pro_Actividades
        Private _IdActividad As System.Int32
        Public Property IdActividad() As System.Int32
            Get
                Return _IdActividad
            End Get
            Set(ByVal value As System.Int32)
                _IdActividad = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "pro_OrdenesProduccion"
    Public Class pro_OrdenesProduccion
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _FechaOrden As Nullable(Of DateTime)
        Public Property FechaOrden() As Nullable(Of DateTime)
            Get
                Return _FechaOrden
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaOrden = value
            End Set
        End Property

        Private _FechaEntrega As Nullable(Of DateTime)
        Public Property FechaEntrega() As Nullable(Of DateTime)
            Get
                Return _FechaEntrega
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaEntrega = value
            End Set
        End Property

        Private _NumeroOrden As System.String = ""
        Public Property NumeroOrden() As System.String
            Get
                Return _NumeroOrden
            End Get
            Set(ByVal value As System.String)
                _NumeroOrden = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property
        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property
        Private _CantidadUG As System.Decimal
        Public Property CantidadUG() As System.Decimal
            Get
                Return _CantidadUG
            End Get
            Set(ByVal value As System.Decimal)
                _CantidadUG = value
            End Set
        End Property
        Private _IdEmpleado As System.Int32
        Public Property IdEmpleado() As System.Int32
            Get
                Return _IdEmpleado
            End Get
            Set(ByVal value As System.Int32)
                _IdEmpleado = value
            End Set
        End Property

        Private _IdEmpleadoAutoriza As System.Int32
        Public Property IdEmpleadoAutoriza() As System.Int32
            Get
                Return _IdEmpleadoAutoriza
            End Get
            Set(ByVal value As System.Int32)
                _IdEmpleadoAutoriza = value
            End Set
        End Property

        Private _IdActividad As System.Int32
        Public Property IdActividad() As System.Int32
            Get
                Return _IdActividad
            End Get
            Set(ByVal value As System.Int32)
                _IdActividad = value
            End Set
        End Property
        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _Aplicada As System.Boolean
        Public Property Aplicada() As System.Boolean
            Get
                Return _Aplicada
            End Get
            Set(ByVal value As System.Boolean)
                _Aplicada = value
            End Set
        End Property
        Private _Observacion As System.String = ""
        Public Property Observacion() As System.String
            Get
                Return _Observacion
            End Get
            Set(ByVal value As System.String)
                _Observacion = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

    End Class
#End Region
#Region "pro_OrdenesProduccionDetalleManoObra"
    Public Class pro_OrdenesProduccionDetalleManoObra
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdEmpleado As System.Int32
        Public Property IdEmpleado() As System.Int32
            Get
                Return _IdEmpleado
            End Get
            Set(ByVal value As System.Int32)
                _IdEmpleado = value
            End Set
        End Property

        Private _NombreEmpleado As System.String = ""
        Public Property NombreEmpleado() As System.String
            Get
                Return _NombreEmpleado
            End Get
            Set(ByVal value As System.String)
                _NombreEmpleado = value
            End Set
        End Property

        Private _HoraInicio As Nullable(Of DateTime)
        Public Property HoraInicio() As Nullable(Of DateTime)
            Get
                Return _HoraInicio
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _HoraInicio = value
            End Set
        End Property

        Private _HoraFin As Nullable(Of DateTime)
        Public Property HoraFin() As Nullable(Of DateTime)
            Get
                Return _HoraFin
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _HoraFin = value
            End Set
        End Property


        Private _IdActividad As System.Int32
        Public Property IdActividad() As System.Int32
            Get
                Return _IdActividad
            End Get
            Set(ByVal value As System.Int32)
                _IdActividad = value
            End Set
        End Property

        Private _Total As System.Decimal
        Public Property Total() As System.Decimal
            Get
                Return _Total
            End Get
            Set(ByVal value As System.Decimal)
                _Total = value
            End Set
        End Property

    End Class
#End Region
#Region "pro_EntradasProduccion"
    Public Class pro_EntradasProduccion
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdTipoComprobante As System.Byte
        Public Property IdTipoComprobante() As System.Byte
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Byte)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _IdOrden As System.Int32
        Public Property IdOrden() As System.Int32
            Get
                Return _IdOrden
            End Get
            Set(ByVal value As System.Int32)
                _IdOrden = value
            End Set
        End Property

        Private _AplicadaInventario As System.Int32
        Public Property AplicadaInventario() As System.Int32
            Get
                Return _AplicadaInventario
            End Get
            Set(ByVal value As System.Int32)
                _AplicadaInventario = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region

#Region "pro_EntradasProduccionDetalle"
    Public Class pro_EntradasProduccionDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _CantidadUG As System.Decimal
        Public Property CantidadUG() As System.Decimal
            Get
                Return _CantidadUG
            End Get
            Set(ByVal value As System.Decimal)
                _CantidadUG = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioCosto As System.Decimal
        Public Property PrecioCosto() As System.Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioCosto = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region

#Region "pro_SalidasProduccion"
    Public Class pro_SalidasProduccion
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdTipoComprobante As System.Byte
        Public Property IdTipoComprobante() As System.Byte
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Byte)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _IdBodega As System.Int32
        Public Property IdBodega() As System.Int32
            Get
                Return _IdBodega
            End Get
            Set(ByVal value As System.Int32)
                _IdBodega = value
            End Set
        End Property

        Private _IdOrden As System.Int32
        Public Property IdOrden() As System.Int32
            Get
                Return _IdOrden
            End Get
            Set(ByVal value As System.Int32)
                _IdOrden = value
            End Set
        End Property

        Private _AplicadaInventario As System.Boolean
        Public Property AplicadaInventario() As System.Boolean
            Get
                Return _AplicadaInventario
            End Get
            Set(ByVal value As System.Boolean)
                _AplicadaInventario = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region

#Region "pro_SalidasProduccionDetalle"
    Public Class pro_SalidasProduccionDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _CantidadUG As System.Decimal
        Public Property CantidadUG() As System.Decimal
            Get
                Return _CantidadUG
            End Get
            Set(ByVal value As System.Decimal)
                _CantidadUG = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioCosto As System.Decimal
        Public Property PrecioCosto() As System.Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioCosto = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "pro_OrdenesGasDirectos"
    Public Class pro_OrdenesGasDirectos
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property
        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdGasto As System.Int32
        Public Property IdGasto() As System.Int32
            Get
                Return _IdGasto
            End Get
            Set(ByVal value As System.Int32)
                _IdGasto = value
            End Set
        End Property

        Private _Gasto As System.String = ""
        Public Property Gasto() As System.String
            Get
                Return _Gasto
            End Get
            Set(ByVal value As System.String)
                _Gasto = value
            End Set
        End Property

        Private _Importe As System.Decimal
        Public Property Importe() As System.Decimal
            Get
                Return _Importe
            End Get
            Set(ByVal value As System.Decimal)
                _Importe = value
            End Set
        End Property

    End Class
#End Region

#Region "pro_OrdenesGasIndirectos"
    Public Class pro_OrdenesGasIndirectos
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property
        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property
        Private _IdGasto As System.Int32
        Public Property IdGasto() As System.Int32
            Get
                Return _IdGasto
            End Get
            Set(ByVal value As System.Int32)
                _IdGasto = value
            End Set
        End Property

        Private _Gasto As System.String = ""
        Public Property Gasto() As System.String
            Get
                Return _Gasto
            End Get
            Set(ByVal value As System.String)
                _Gasto = value
            End Set
        End Property

        Private _Importe As System.Decimal
        Public Property Importe() As System.Decimal
            Get
                Return _Importe
            End Get
            Set(ByVal value As System.Decimal)
                _Importe = value
            End Set
        End Property

    End Class
#End Region
#Region "pro_OrdenesEmpaque"
    Public Class pro_OrdenesEmpaque
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdTipoComprobanteSal As System.Int32
        Public Property IdTipoComprobanteSal() As System.Int32
            Get
                Return _IdTipoComprobanteSal
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoComprobanteSal = value
            End Set
        End Property

        Private _IdTipoComprobanteEnt As System.Int32
        Public Property IdTipoComprobanteEnt() As System.Int32
            Get
                Return _IdTipoComprobanteEnt
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoComprobanteEnt = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _IdBodegaSal As System.Int32
        Public Property IdBodegaSal() As System.Int32
            Get
                Return _IdBodegaSal
            End Get
            Set(ByVal value As System.Int32)
                _IdBodegaSal = value
            End Set
        End Property

        Private _IdBodegaIng As System.Int32
        Public Property IdBodegaIng() As System.Int32
            Get
                Return _IdBodegaIng
            End Get
            Set(ByVal value As System.Int32)
                _IdBodegaIng = value
            End Set
        End Property
        Private _IdBodegaIngTerminado As System.Int32
        Public Property IdBodegaIngTerminado() As System.Int32
            Get
                Return _IdBodegaIngTerminado
            End Get
            Set(ByVal value As System.Int32)
                _IdBodegaIngTerminado = value
            End Set
        End Property
        Private _AplicadaInventario As System.Boolean
        Public Property AplicadaInventario() As System.Boolean
            Get
                Return _AplicadaInventario
            End Get
            Set(ByVal value As System.Boolean)
                _AplicadaInventario = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region

#Region "pro_OrdenesEmpaqueDetalleEntrada"
    Public Class pro_OrdenesEmpaqueDetalleEntrada
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _CantidadUG As System.Decimal
        Public Property CantidadUG() As System.Decimal
            Get
                Return _CantidadUG
            End Get
            Set(ByVal value As System.Decimal)
                _CantidadUG = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioCosto As System.Decimal
        Public Property PrecioCosto() As System.Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioCosto = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region

#Region "pro_OrdenesEmpaqueDetalleSalida"
    Public Class pro_OrdenesEmpaqueDetalleSalida
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdProducto As System.String = ""
        Public Property IdProducto() As System.String
            Get
                Return _IdProducto
            End Get
            Set(ByVal value As System.String)
                _IdProducto = value
            End Set
        End Property

        Private _Cantidad As System.Decimal
        Public Property Cantidad() As System.Decimal
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Decimal)
                _Cantidad = value
            End Set
        End Property

        Private _CantidadUG As System.Decimal
        Public Property CantidadUG() As System.Decimal
            Get
                Return _CantidadUG
            End Get
            Set(ByVal value As System.Decimal)
                _CantidadUG = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _PrecioUnitario As System.Decimal
        Public Property PrecioUnitario() As System.Decimal
            Get
                Return _PrecioUnitario
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioUnitario = value
            End Set
        End Property

        Private _PrecioCosto As System.Decimal
        Public Property PrecioCosto() As System.Decimal
            Get
                Return _PrecioCosto
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioCosto = value
            End Set
        End Property

        Private _PrecioTotal As System.Decimal
        Public Property PrecioTotal() As System.Decimal
            Get
                Return _PrecioTotal
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioTotal = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region





#Region "cpc_Abonos"
    Public Class cpc_Abonos
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _NumeroComprobante As System.String = ""
        Public Property NumeroComprobante() As System.String
            Get
                Return _NumeroComprobante
            End Get
            Set(ByVal value As System.String)
                _NumeroComprobante = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdCuentaBancaria As System.Int32
        Public Property IdCuentaBancaria() As System.Int32
            Get
                Return _IdCuentaBancaria
            End Get
            Set(ByVal value As System.Int32)
                _IdCuentaBancaria = value
            End Set
        End Property

        Private _IdCobrador As System.Int32
        Public Property IdCobrador() As System.Int32
            Get
                Return _IdCobrador
            End Get
            Set(ByVal value As System.Int32)
                _IdCobrador = value
            End Set
        End Property

        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _IdTipoMov As System.Byte
        Public Property IdTipoMov() As System.Byte
            Get
                Return _IdTipoMov
            End Get
            Set(ByVal value As System.Byte)
                _IdTipoMov = value
            End Set
        End Property
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _IdPuntoVenta As System.Int32
        Public Property IdPuntoVenta() As System.Int32
            Get
                Return _IdPuntoVenta
            End Get
            Set(ByVal value As System.Int32)
                _IdPuntoVenta = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _RecargoMora As System.Decimal
        Public Property RecargoMora() As System.Decimal
            Get
                Return _RecargoMora
            End Get
            Set(ByVal value As System.Decimal)
                _RecargoMora = value
            End Set
        End Property

        Private _PorcComision As System.Decimal
        Public Property PorcComision() As System.Decimal
            Get
                Return _PorcComision
            End Get
            Set(ByVal value As System.Decimal)
                _PorcComision = value
            End Set
        End Property
        Private _BancoProcedencia As System.String = ""
        Public Property BancoProcedencia() As System.String
            Get
                Return _BancoProcedencia
            End Get
            Set(ByVal value As System.String)
                _BancoProcedencia = value
            End Set
        End Property

        Private _NumCheque As System.String = ""
        Public Property NumCheque() As System.String
            Get
                Return _NumCheque
            End Get
            Set(ByVal value As System.String)
                _NumCheque = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

        Private _IdTransaccion As System.Int32
        Public Property IdTransaccion() As System.Int32
            Get
                Return _IdTransaccion
            End Get
            Set(ByVal value As System.Int32)
                _IdTransaccion = value
            End Set
        End Property

        Private _IdPartida As System.Int32
        Public Property IdPartida() As System.Int32
            Get
                Return _IdPartida
            End Get
            Set(ByVal value As System.Int32)
                _IdPartida = value
            End Set
        End Property


    End Class
#End Region
#Region "cpc_AbonosDetalle"
    Public Class cpc_AbonosDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdComprobVenta As System.Int32
        Public Property IdComprobVenta() As System.Int32
            Get
                Return _IdComprobVenta
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobVenta = value
            End Set
        End Property

        Private _MontoAbonado As System.Decimal
        Public Property MontoAbonado() As System.Decimal
            Get
                Return _MontoAbonado
            End Get
            Set(ByVal value As System.Decimal)
                _MontoAbonado = value
            End Set
        End Property

        Private _SaldoComprobante As System.Decimal
        Public Property SaldoComprobante() As System.Decimal
            Get
                Return _SaldoComprobante
            End Get
            Set(ByVal value As System.Decimal)
                _SaldoComprobante = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "cpc_Cargos"
    Public Class cpc_Cargos
        Private _Iden As System.Int32
        Public Property Iden() As System.Int32
            Get
                Return _Iden
            End Get
            Set(ByVal value As System.Int32)
                _Iden = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Monto As System.Decimal
        Public Property Monto() As System.Decimal
            Get
                Return _Monto
            End Get
            Set(ByVal value As System.Decimal)
                _Monto = value
            End Set
        End Property

        Private _IdTipoMov As System.Byte
        Public Property IdTipoMov() As System.Byte
            Get
                Return _IdTipoMov
            End Get
            Set(ByVal value As System.Byte)
                _IdTipoMov = value
            End Set
        End Property

    End Class
#End Region
#Region "cpc_CargosDetalle"
    Public Class cpc_CargosDetalle
        Private _Iden As System.Int32
        Public Property Iden() As System.Int32
            Get
                Return _Iden
            End Get
            Set(ByVal value As System.Int32)
                _Iden = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "cpc_Cobradores"
    Public Class cpc_Cobradores
        Private _IdCobrador As System.Int32
        Public Property IdCobrador() As System.Int32
            Get
                Return _IdCobrador
            End Get
            Set(ByVal value As System.Int32)
                _IdCobrador = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _PorcComision As System.Decimal
        Public Property PorcComision() As System.Decimal
            Get
                Return _PorcComision
            End Get
            Set(ByVal value As System.Decimal)
                _PorcComision = value
            End Set
        End Property

    End Class
#End Region
#Region "cpc_PoliticaAntiguedad"
    Public Class cpc_PoliticaAntiguedad
        Private _Id As System.Int16
        Public Property Id() As System.Int16
            Get
                Return _Id
            End Get
            Set(ByVal value As System.Int16)
                _Id = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Abreviatura As System.String = ""
        Public Property Abreviatura() As System.String
            Get
                Return _Abreviatura
            End Get
            Set(ByVal value As System.String)
                _Abreviatura = value
            End Set
        End Property

        Private _DesdeDias As System.Int32
        Public Property DesdeDias() As System.Int32
            Get
                Return _DesdeDias
            End Get
            Set(ByVal value As System.Int32)
                _DesdeDias = value
            End Set
        End Property

        Private _HastaDias As System.Int32
        Public Property HastaDias() As System.Int32
            Get
                Return _HastaDias
            End Get
            Set(ByVal value As System.Int32)
                _HastaDias = value
            End Set
        End Property

    End Class
#End Region
#Region "cpc_TiposMov"
    Public Class cpc_TiposMov
        Private _IdTipo As System.Int32
        Public Property IdTipo() As System.Int32
            Get
                Return _IdTipo
            End Get
            Set(ByVal value As System.Int32)
                _IdTipo = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _IdTipoTransaccion As System.Int32
        Public Property IdTipoTransaccion() As System.Int32
            Get
                Return _IdTipoTransaccion
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoTransaccion = value
            End Set
        End Property

        Private _IdTipoPartida As System.String = ""
        Public Property IdTipoPartida() As System.String
            Get
                Return _IdTipoPartida
            End Get
            Set(ByVal value As System.String)
                _IdTipoPartida = value
            End Set
        End Property

        Private _TipoAplicacion As System.Int32
        Public Property TipoAplicacion() As System.Int32
            Get
                Return _TipoAplicacion
            End Get
            Set(ByVal value As System.Int32)
                _TipoAplicacion = value
            End Set
        End Property

        Private _IdCuentaContable As System.String = ""
        Public Property IdCuentaContable() As System.String
            Get
                Return _IdCuentaContable
            End Get
            Set(ByVal value As System.String)
                _IdCuentaContable = value
            End Set
        End Property

        Private _AfectaBancos As System.Boolean
        Public Property AfectaBancos() As System.Boolean
            Get
                Return _AfectaBancos
            End Get
            Set(ByVal value As System.Boolean)
                _AfectaBancos = value
            End Set
        End Property

    End Class
#End Region

#Region "cpp_Traslado"
    Public Class cpp_Traslado
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _NumeroComprobante As System.String = ""
        Public Property NumeroComprobante() As System.String
            Get
                Return _NumeroComprobante
            End Get
            Set(ByVal value As System.String)
                _NumeroComprobante = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdProveedorOrigen As System.String = ""
        Public Property IdProveedorOrigen() As System.String
            Get
                Return _IdProveedorOrigen
            End Get
            Set(ByVal value As System.String)
                _IdProveedorOrigen = value
            End Set
        End Property

        Private _IdProveedorDestino As System.String = ""
        Public Property IdProveedorDestino() As System.String
            Get
                Return _IdProveedorDestino
            End Get
            Set(ByVal value As System.String)
                _IdProveedorDestino = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property


        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "cpp_Abonos"
    Public Class cpp_Abonos
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _NumeroComprobante As System.String = ""
        Public Property NumeroComprobante() As System.String
            Get
                Return _NumeroComprobante
            End Get
            Set(ByVal value As System.String)
                _NumeroComprobante = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdProveedor As System.String = ""
        Public Property IdProveedor() As System.String
            Get
                Return _IdProveedor
            End Get
            Set(ByVal value As System.String)
                _IdProveedor = value
            End Set
        End Property

        Private _IdTipo As System.Byte
        Public Property IdTipo() As System.Byte
            Get
                Return _IdTipo
            End Get
            Set(ByVal value As System.Byte)
                _IdTipo = value
            End Set
        End Property
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _TotalAbono As System.Decimal
        Public Property TotalAbono() As System.Decimal
            Get
                Return _TotalAbono
            End Get
            Set(ByVal value As System.Decimal)
                _TotalAbono = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _Cancelado As System.Boolean
        Public Property Cancelado() As System.Boolean
            Get
                Return _Cancelado
            End Get
            Set(ByVal value As System.Boolean)
                _Cancelado = value
            End Set
        End Property

        Private _IdCheque As System.Int32
        Public Property IdCheque() As System.Int32
            Get
                Return _IdCheque
            End Get
            Set(ByVal value As System.Int32)
                _IdCheque = value
            End Set
        End Property

        Private _IdTransaccion As System.Int32
        Public Property IdTransaccion() As System.Int32
            Get
                Return _IdTransaccion
            End Get
            Set(ByVal value As System.Int32)
                _IdTransaccion = value
            End Set
        End Property
        Private _IdCaja As System.Int32
        Public Property IdCaja() As System.Int32
            Get
                Return _IdCaja
            End Get
            Set(ByVal value As System.Int32)
                _IdCaja = value
            End Set
        End Property


        Private _CanceladoPor As System.String = ""
        Public Property CanceladoPor() As System.String
            Get
                Return _CanceladoPor
            End Get
            Set(ByVal value As System.String)
                _CanceladoPor = value
            End Set
        End Property

        Private _FechaHoraCancelacion As Nullable(Of DateTime)
        Public Property FechaHoraCancelacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCancelacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCancelacion = value
            End Set
        End Property

    End Class
#End Region
#Region "cpp_TrasladoDetalle"
    Public Class cpp_TrasladoDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _NumComprobanteCompra As System.String = ""
        Public Property NumComprobanteCompra() As System.String
            Get
                Return _NumComprobanteCompra
            End Get
            Set(ByVal value As System.String)
                _NumComprobanteCompra = value
            End Set
        End Property

        Private _SaldoActual As System.Decimal
        Public Property SaldoActual() As System.Decimal
            Get
                Return _SaldoActual
            End Get
            Set(ByVal value As System.Decimal)
                _SaldoActual = value
            End Set
        End Property

        Private _IdComprobanteCompra As System.Int32
        Public Property IdComprobanteCompra() As System.Int32
            Get
                Return _IdComprobanteCompra
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobanteCompra = value
            End Set
        End Property

        Private _TipoComprobanteCompra As System.Int32
        Public Property TipoComprobanteCompra() As System.Int32
            Get
                Return _TipoComprobanteCompra
            End Get
            Set(ByVal value As System.Int32)
                _TipoComprobanteCompra = value
            End Set
        End Property
    End Class
#End Region
#Region "cpc_DocumentosCobro"
    Public Class cpc_DocumentosCobro
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdPunto As System.Int32
        Public Property IdPunto() As System.Int32
            Get
                Return _IdPunto
            End Get
            Set(ByVal value As System.Int32)
                _IdPunto = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdVendedor As System.Int32
        Public Property IdVendedor() As System.Int32
            Get
                Return _IdVendedor
            End Get
            Set(ByVal value As System.Int32)
                _IdVendedor = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region

#Region "cpc_DocumentosCobroDetalle"
    Public Class cpc_DocumentosCobroDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdComprobVenta As System.Int32
        Public Property IdComprobVenta() As System.Int32
            Get
                Return _IdComprobVenta
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobVenta = value
            End Set
        End Property

    End Class
#End Region


#Region "cpp_AbonosDetalle"
    Public Class cpp_AbonosDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _NumComprobanteCompra As System.String = ""
        Public Property NumComprobanteCompra() As System.String
            Get
                Return _NumComprobanteCompra
            End Get
            Set(ByVal value As System.String)
                _NumComprobanteCompra = value
            End Set
        End Property

        Private _MontoAbonado As System.Decimal
        Public Property MontoAbonado() As System.Decimal
            Get
                Return _MontoAbonado
            End Get
            Set(ByVal value As System.Decimal)
                _MontoAbonado = value
            End Set
        End Property

        Private _SaldoActual As System.Decimal
        Public Property SaldoActual() As System.Decimal
            Get
                Return _SaldoActual
            End Get
            Set(ByVal value As System.Decimal)
                _SaldoActual = value
            End Set
        End Property

        Private _IdComprobanteCompra As System.Int32
        Public Property IdComprobanteCompra() As System.Int32
            Get
                Return _IdComprobanteCompra
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobanteCompra = value
            End Set
        End Property

        Private _TipoComprobanteCompra As System.Int32
        Public Property TipoComprobanteCompra() As System.Int32
            Get
                Return _TipoComprobanteCompra
            End Get
            Set(ByVal value As System.Int32)
                _TipoComprobanteCompra = value
            End Set
        End Property
    End Class
#End Region
#Region "cpp_Quedan"
    Public Class cpp_Quedan
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdProveedor As System.String = ""
        Public Property IdProveedor() As System.String
            Get
                Return _IdProveedor
            End Get
            Set(ByVal value As System.String)
                _IdProveedor = value
            End Set
        End Property

        Private _NombreProveedor As System.String = ""
        Public Property NombreProveedor() As System.String
            Get
                Return _NombreProveedor
            End Get
            Set(ByVal value As System.String)
                _NombreProveedor = value
            End Set
        End Property

        Private _NrcProveedor As System.String = ""
        Public Property NrcProveedor() As System.String
            Get
                Return _NrcProveedor
            End Get
            Set(ByVal value As System.String)
                _NrcProveedor = value
            End Set
        End Property

        Private _FechaPago As Nullable(Of DateTime)
        Public Property FechaPago() As Nullable(Of DateTime)
            Get
                Return _FechaPago
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaPago = value
            End Set
        End Property

        Private _TotalPago As System.Decimal
        Public Property TotalPago() As System.Decimal
            Get
                Return _TotalPago
            End Get
            Set(ByVal value As System.Decimal)
                _TotalPago = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _SaldoActual As System.Decimal
        Public Property SaldoActual() As System.Decimal
            Get
                Return _SaldoActual
            End Get
            Set(ByVal value As System.Decimal)
                _SaldoActual = value
            End Set
        End Property

        Private _Contabilizar As System.Boolean
        Public Property Contabilizar() As System.Boolean
            Get
                Return _Contabilizar
            End Get
            Set(ByVal value As System.Boolean)
                _Contabilizar = value
            End Set
        End Property

        Private _IdPartida As System.Int32
        Public Property IdPartida() As System.Int32
            Get
                Return _IdPartida
            End Get
            Set(ByVal value As System.Int32)
                _IdPartida = value
            End Set
        End Property

        Private _IdTipoPartida As System.String = ""
        Public Property IdTipoPartida() As System.String
            Get
                Return _IdTipoPartida
            End Get
            Set(ByVal value As System.String)
                _IdTipoPartida = value
            End Set
        End Property

        Private _NumeroPartida As System.String = ""
        Public Property NumeroPartida() As System.String
            Get
                Return _NumeroPartida
            End Get
            Set(ByVal value As System.String)
                _NumeroPartida = value
            End Set
        End Property
        Private _Anulado As System.Boolean
        Public Property Anulado() As System.Boolean
            Get
                Return _Anulado
            End Get
            Set(ByVal value As System.Boolean)
                _Anulado = value
            End Set
        End Property
        Private _FechaCancelacion As Nullable(Of DateTime)
        Public Property FechaCancelacion() As Nullable(Of DateTime)
            Get
                Return _FechaCancelacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaCancelacion = value
            End Set
        End Property
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property
        Private _AnuladoPor As System.String = ""
        Public Property AnuladoPor() As System.String
            Get
                Return _AnuladoPor
            End Get
            Set(ByVal value As System.String)
                _AnuladoPor = value
            End Set
        End Property

        Private _FechaHoraAnulacion As Nullable(Of DateTime)
        Public Property FechaHoraAnulacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraAnulacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraAnulacion = value
            End Set
        End Property
    End Class
#End Region
#Region "cpp_QuedanDetalle"
    Public Class cpp_QuedanDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdCompra As System.Int32
        Public Property IdCompra() As System.Int32
            Get
                Return _IdCompra
            End Get
            Set(ByVal value As System.Int32)
                _IdCompra = value
            End Set
        End Property

        Private _Numero As System.String = ""
        Public Property Numero() As System.String
            Get
                Return _Numero
            End Get
            Set(ByVal value As System.String)
                _Numero = value
            End Set
        End Property

        Private _IdTipo As System.Byte
        Public Property IdTipo() As System.Byte
            Get
                Return _IdTipo
            End Get
            Set(ByVal value As System.Byte)
                _IdTipo = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _FechaVence As Nullable(Of DateTime)
        Public Property FechaVence() As Nullable(Of DateTime)
            Get
                Return _FechaVence
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaVence = value
            End Set
        End Property

        Private _ValorGravado As System.Decimal
        Public Property ValorGravado() As System.Decimal
            Get
                Return _ValorGravado
            End Get
            Set(ByVal value As System.Decimal)
                _ValorGravado = value
            End Set
        End Property

        Private _ValorExento As System.Decimal
        Public Property ValorExento() As System.Decimal
            Get
                Return _ValorExento
            End Get
            Set(ByVal value As System.Decimal)
                _ValorExento = value
            End Set
        End Property

        Private _ValorIva As System.Decimal
        Public Property ValorIva() As System.Decimal
            Get
                Return _ValorIva
            End Get
            Set(ByVal value As System.Decimal)
                _ValorIva = value
            End Set
        End Property

        Private _SubTotal As System.Decimal
        Public Property SubTotal() As System.Decimal
            Get
                Return _SubTotal
            End Get
            Set(ByVal value As System.Decimal)
                _SubTotal = value
            End Set
        End Property

        Private _Retencion As System.Decimal
        Public Property Retencion() As System.Decimal
            Get
                Return _Retencion
            End Get
            Set(ByVal value As System.Decimal)
                _Retencion = value
            End Set
        End Property

        Private _ValorTotal As System.Decimal
        Public Property ValorTotal() As System.Decimal
            Get
                Return _ValorTotal
            End Get
            Set(ByVal value As System.Decimal)
                _ValorTotal = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region


#Region "ActivoFijo"
#Region "acf_Activos"
    Public Class acf_Activos
        Private _IdActivo As System.Int32
        Public Property IdActivo() As System.Int32
            Get
                Return _IdActivo
            End Get
            Set(ByVal value As System.Int32)
                _IdActivo = value
            End Set
        End Property

        Private _Codigo As System.String = ""
        Public Property Codigo() As System.String
            Get
                Return _Codigo
            End Get
            Set(ByVal value As System.String)
                _Codigo = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _FechaAdquisicion As Nullable(Of DateTime)
        Public Property FechaAdquisicion() As Nullable(Of DateTime)
            Get
                Return _FechaAdquisicion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaAdquisicion = value
            End Set
        End Property

        Private _IdMarca As System.Int32
        Public Property IdMarca() As System.Int32
            Get
                Return _IdMarca
            End Get
            Set(ByVal value As System.Int32)
                _IdMarca = value
            End Set
        End Property

        Private _IdModelo As System.Int32
        Public Property IdModelo() As System.Int32
            Get
                Return _IdModelo
            End Get
            Set(ByVal value As System.Int32)
                _IdModelo = value
            End Set
        End Property

        Private _IdEstilo As System.Int32
        Public Property IdEstilo() As System.Int32
            Get
                Return _IdEstilo
            End Get
            Set(ByVal value As System.Int32)
                _IdEstilo = value
            End Set
        End Property

        Private _Color As System.String = ""
        Public Property Color() As System.String
            Get
                Return _Color
            End Get
            Set(ByVal value As System.String)
                _Color = value
            End Set
        End Property

        Private _Serie As System.String = ""
        Public Property Serie() As System.String
            Get
                Return _Serie
            End Get
            Set(ByVal value As System.String)
                _Serie = value
            End Set
        End Property

        Private _Anio As System.Int32
        Public Property Anio() As System.Int32
            Get
                Return _Anio
            End Get
            Set(ByVal value As System.Int32)
                _Anio = value
            End Set
        End Property

        Private _NumCompra As System.String = ""
        Public Property NumCompra() As System.String
            Get
                Return _NumCompra
            End Get
            Set(ByVal value As System.String)
                _NumCompra = value
            End Set
        End Property

        Private _NumCheque As System.String = ""
        Public Property NumCheque() As System.String
            Get
                Return _NumCheque
            End Get
            Set(ByVal value As System.String)
                _NumCheque = value
            End Set
        End Property

        Private _Depreciable As System.Boolean
        Public Property Depreciable() As System.Boolean
            Get
                Return _Depreciable
            End Get
            Set(ByVal value As System.Boolean)
                _Depreciable = value
            End Set
        End Property

        Private _ValorAdquisicion As System.Decimal
        Public Property ValorAdquisicion() As System.Decimal
            Get
                Return _ValorAdquisicion
            End Get
            Set(ByVal value As System.Decimal)
                _ValorAdquisicion = value
            End Set
        End Property

        Private _ValorDepreciar As System.Decimal
        Public Property ValorDepreciar() As System.Decimal
            Get
                Return _ValorDepreciar
            End Get
            Set(ByVal value As System.Decimal)
                _ValorDepreciar = value
            End Set
        End Property

        Private _ValorContable As System.Decimal
        Public Property ValorContable() As System.Decimal
            Get
                Return _ValorContable
            End Get
            Set(ByVal value As System.Decimal)
                _ValorContable = value
            End Set
        End Property

        Private _MesesDepreciacion As System.Int32
        Public Property MesesDepreciacion() As System.Int32
            Get
                Return _MesesDepreciacion
            End Get
            Set(ByVal value As System.Int32)
                _MesesDepreciacion = value
            End Set
        End Property

        Private _CuotaMensual As System.Decimal
        Public Property CuotaMensual() As System.Decimal
            Get
                Return _CuotaMensual
            End Get
            Set(ByVal value As System.Decimal)
                _CuotaMensual = value
            End Set
        End Property

        Private _CuotaAnual As System.Decimal
        Public Property CuotaAnual() As System.Decimal
            Get
                Return _CuotaAnual
            End Get
            Set(ByVal value As System.Decimal)
                _CuotaAnual = value
            End Set
        End Property

        Private _FechaInicio As Nullable(Of DateTime)
        Public Property FechaInicio() As Nullable(Of DateTime)
            Get
                Return _FechaInicio
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaInicio = value
            End Set
        End Property

        Private _IdTipoDepreciacion As System.Byte
        Public Property IdTipoDepreciacion() As System.Byte
            Get
                Return _IdTipoDepreciacion
            End Get
            Set(ByVal value As System.Byte)
                _IdTipoDepreciacion = value
            End Set
        End Property

        Private _ValorRetiro As System.Decimal
        Public Property ValorRetiro() As System.Decimal
            Get
                Return _ValorRetiro
            End Get
            Set(ByVal value As System.Decimal)
                _ValorRetiro = value
            End Set
        End Property

        Private _IdClase As System.Int32
        Public Property IdClase() As System.Int32
            Get
                Return _IdClase
            End Get
            Set(ByVal value As System.Int32)
                _IdClase = value
            End Set
        End Property

        Private _IdProveedor As System.String = ""
        Public Property IdProveedor() As System.String
            Get
                Return _IdProveedor
            End Get
            Set(ByVal value As System.String)
                _IdProveedor = value
            End Set
        End Property

        Private _IdEmpleado As System.Int32
        Public Property IdEmpleado() As System.Int32
            Get
                Return _IdEmpleado
            End Get
            Set(ByVal value As System.Int32)
                _IdEmpleado = value
            End Set
        End Property

        Private _IdUbicacion As System.Int16
        Public Property IdUbicacion() As System.Int16
            Get
                Return _IdUbicacion
            End Get
            Set(ByVal value As System.Int16)
                _IdUbicacion = value
            End Set
        End Property

        Private _IdCuentaActivo As System.String = ""
        Public Property IdCuentaActivo() As System.String
            Get
                Return _IdCuentaActivo
            End Get
            Set(ByVal value As System.String)
                _IdCuentaActivo = value
            End Set
        End Property

        Private _IdCuentaDepreciacion As System.String = ""
        Public Property IdCuentaDepreciacion() As System.String
            Get
                Return _IdCuentaDepreciacion
            End Get
            Set(ByVal value As System.String)
                _IdCuentaDepreciacion = value
            End Set
        End Property

        Private _IdCuentaGasto As System.String = ""
        Public Property IdCuentaGasto() As System.String
            Get
                Return _IdCuentaGasto
            End Get
            Set(ByVal value As System.String)
                _IdCuentaGasto = value
            End Set
        End Property

        Private _IdEstado As System.Byte
        Public Property IdEstado() As System.Byte
            Get
                Return _IdEstado
            End Get
            Set(ByVal value As System.Byte)
                _IdEstado = value
            End Set
        End Property

        Private _IdMotivoBaja As System.Int32
        Public Property IdMotivoBaja() As System.Int32
            Get
                Return _IdMotivoBaja
            End Get
            Set(ByVal value As System.Int32)
                _IdMotivoBaja = value
            End Set
        End Property
        Private _FechaRetiro As Nullable(Of DateTime)
        Public Property FechaRetiro() As Nullable(Of DateTime)
            Get
                Return _FechaRetiro
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaRetiro = value
            End Set
        End Property

        Private _VectoGarantia As Nullable(Of DateTime)
        Public Property VectoGarantia() As Nullable(Of DateTime)
            Get
                Return _VectoGarantia
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _VectoGarantia = value
            End Set
        End Property

        Private _InfoAdicional As System.String = ""
        Public Property InfoAdicional() As System.String
            Get
                Return _InfoAdicional
            End Get
            Set(ByVal value As System.String)
                _InfoAdicional = value
            End Set
        End Property
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region


#Region "acf_Clases"
    Public Class acf_Clases
        Private _IdClase As System.Int16
        Public Property IdClase() As System.Int16
            Get
                Return _IdClase
            End Get
            Set(ByVal value As System.Int16)
                _IdClase = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Depreciable As System.Boolean
        Public Property Depreciable() As System.Boolean
            Get
                Return _Depreciable
            End Get
            Set(ByVal value As System.Boolean)
                _Depreciable = value
            End Set
        End Property

        Private _IdCtaActivo As System.String = ""
        Public Property IdCtaActivo() As System.String
            Get
                Return _IdCtaActivo
            End Get
            Set(ByVal value As System.String)
                _IdCtaActivo = value
            End Set
        End Property

        Private _IdCtaDepreciacion As System.String = ""
        Public Property IdCtaDepreciacion() As System.String
            Get
                Return _IdCtaDepreciacion
            End Get
            Set(ByVal value As System.String)
                _IdCtaDepreciacion = value
            End Set
        End Property

        Private _IdCtaGasto As System.String = ""
        Public Property IdCtaGasto() As System.String
            Get
                Return _IdCtaGasto
            End Get
            Set(ByVal value As System.String)
                _IdCtaGasto = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_DepreciacionesAjustes"
    Public Class acf_DepreciacionesAjustes
        Private _IdActivo As System.Int32
        Public Property IdActivo() As System.Int32
            Get
                Return _IdActivo
            End Get
            Set(ByVal value As System.Int32)
                _IdActivo = value
            End Set
        End Property

        Private _NumCuota As System.Int16
        Public Property NumCuota() As System.Int16
            Get
                Return _NumCuota
            End Get
            Set(ByVal value As System.Int16)
                _NumCuota = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdTipo As System.Int16
        Public Property IdTipo() As System.Int16
            Get
                Return _IdTipo
            End Get
            Set(ByVal value As System.Int16)
                _IdTipo = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_Estados"
    Public Class acf_Estados
        Private _IdEstado As System.Byte
        Public Property IdEstado() As System.Byte
            Get
                Return _IdEstado
            End Get
            Set(ByVal value As System.Byte)
                _IdEstado = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_Estilos"
    Public Class acf_Estilos
        Private _IdEstilo As System.Int32
        Public Property IdEstilo() As System.Int32
            Get
                Return _IdEstilo
            End Get
            Set(ByVal value As System.Int32)
                _IdEstilo = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_Fallas"
    Public Class acf_Fallas
        Private _IdFalla As System.Int32
        Public Property IdFalla() As System.Int32
            Get
                Return _IdFalla
            End Get
            Set(ByVal value As System.Int32)
                _IdFalla = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_Mantenimientos"
    Public Class acf_Mantenimientos
        Private _Id As System.Int32
        Public Property Id() As System.Int32
            Get
                Return _Id
            End Get
            Set(ByVal value As System.Int32)
                _Id = value
            End Set
        End Property

        Private _IdActivo As System.Int32
        Public Property IdActivo() As System.Int32
            Get
                Return _IdActivo
            End Get
            Set(ByVal value As System.Int32)
                _IdActivo = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

        Private _IdFalla As System.Int32
        Public Property IdFalla() As System.Int32
            Get
                Return _IdFalla
            End Get
            Set(ByVal value As System.Int32)
                _IdFalla = value
            End Set
        End Property

        Private _IdTecnico As System.Int32
        Public Property IdTecnico() As System.Int32
            Get
                Return _IdTecnico
            End Get
            Set(ByVal value As System.Int32)
                _IdTecnico = value
            End Set
        End Property

        Private _IncluyeGarantia As System.Boolean
        Public Property IncluyeGarantia() As System.Boolean
            Get
                Return _IncluyeGarantia
            End Get
            Set(ByVal value As System.Boolean)
                _IncluyeGarantia = value
            End Set
        End Property

        Private _FechaVenceGarantia As Nullable(Of DateTime)
        Public Property FechaVenceGarantia() As Nullable(Of DateTime)
            Get
                Return _FechaVenceGarantia
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaVenceGarantia = value
            End Set
        End Property

        Private _FechaProximoServicio As Nullable(Of DateTime)
        Public Property FechaProximoServicio() As Nullable(Of DateTime)
            Get
                Return _FechaProximoServicio
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaProximoServicio = value
            End Set
        End Property

        Private _DetalleProblema As System.String = ""
        Public Property DetalleProblema() As System.String
            Get
                Return _DetalleProblema
            End Get
            Set(ByVal value As System.String)
                _DetalleProblema = value
            End Set
        End Property

        Private _DetalleSolucion As System.String = ""
        Public Property DetalleSolucion() As System.String
            Get
                Return _DetalleSolucion
            End Get
            Set(ByVal value As System.String)
                _DetalleSolucion = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_Marcas"
    Public Class acf_Marcas
        Private _IdMarca As System.Int32
        Public Property IdMarca() As System.Int32
            Get
                Return _IdMarca
            End Get
            Set(ByVal value As System.Int32)
                _IdMarca = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_Modelos"
    Public Class acf_Modelos
        Private _IdModelo As System.Int32
        Public Property IdModelo() As System.Int32
            Get
                Return _IdModelo
            End Get
            Set(ByVal value As System.Int32)
                _IdModelo = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_MotivosBaja"
    Public Class acf_MotivosBaja
        Private _IdMotivo As System.Int32
        Public Property IdMotivo() As System.Int32
            Get
                Return _IdMotivo
            End Get
            Set(ByVal value As System.Int32)
                _IdMotivo = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_Tecnicos"
    Public Class acf_Tecnicos
        Private _IdTecnico As System.Int32
        Public Property IdTecnico() As System.Int32
            Get
                Return _IdTecnico
            End Get
            Set(ByVal value As System.Int32)
                _IdTecnico = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Empresa As System.String = ""
        Public Property Empresa() As System.String
            Get
                Return _Empresa
            End Get
            Set(ByVal value As System.String)
                _Empresa = value
            End Set
        End Property

        Private _Telefono As System.String = ""
        Public Property Telefono() As System.String
            Get
                Return _Telefono
            End Get
            Set(ByVal value As System.String)
                _Telefono = value
            End Set
        End Property

        Private _Email As System.String = ""
        Public Property Email() As System.String
            Get
                Return _Email
            End Get
            Set(ByVal value As System.String)
                _Email = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_TiposAjuste"
    Public Class acf_TiposAjuste
        Private _IdTipo As System.Byte
        Public Property IdTipo() As System.Byte
            Get
                Return _IdTipo
            End Get
            Set(ByVal value As System.Byte)
                _IdTipo = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _TipoAplicacion As System.Byte
        Public Property TipoAplicacion() As System.Byte
            Get
                Return _TipoAplicacion
            End Get
            Set(ByVal value As System.Byte)
                _TipoAplicacion = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_TiposDepreciacion"
    Public Class acf_TiposDepreciacion
        Private _IdTipo As System.Byte
        Public Property IdTipo() As System.Byte
            Get
                Return _IdTipo
            End Get
            Set(ByVal value As System.Byte)
                _IdTipo = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_Ubicaciones"
    Public Class acf_Ubicaciones
        Private _IdUbicacion As System.Int16
        Public Property IdUbicacion() As System.Int16
            Get
                Return _IdUbicacion
            End Get
            Set(ByVal value As System.Int16)
                _IdUbicacion = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "acf_Traslados"
    Public Class acf_Traslados
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _NumeroComprobante As System.String = ""
        Public Property NumeroComprobante() As System.String
            Get
                Return _NumeroComprobante
            End Get
            Set(ByVal value As System.String)
                _NumeroComprobante = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _IdUbicacionAnt As System.Int32
        Public Property IdUbicacionAnt() As System.Int32
            Get
                Return _IdUbicacionAnt
            End Get
            Set(ByVal value As System.Int32)
                _IdUbicacionAnt = value
            End Set
        End Property

        Private _IdUbicacionAct As System.Int32
        Public Property IdUbicacionAct() As System.Int32
            Get
                Return _IdUbicacionAct
            End Get
            Set(ByVal value As System.Int32)
                _IdUbicacionAct = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region

#Region "acf_TrasladosDetalle"
    Public Class acf_TrasladosDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdActivo As System.Int32
        Public Property IdActivo() As System.Int32
            Get
                Return _IdActivo
            End Get
            Set(ByVal value As System.Int32)
                _IdActivo = value
            End Set
        End Property

        Private _Descripcion As System.String = ""
        Public Property Descripcion() As System.String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As System.String)
                _Descripcion = value
            End Set
        End Property

        Private _IdResponsableAnt As System.Int32
        Public Property IdResponsableAnt() As System.Int32
            Get
                Return _IdResponsableAnt
            End Get
            Set(ByVal value As System.Int32)
                _IdResponsableAnt = value
            End Set
        End Property

        Private _IdResponsableAct As System.Int32
        Public Property IdResponsableAct() As System.Int32
            Get
                Return _IdResponsableAct
            End Get
            Set(ByVal value As System.Int32)
                _IdResponsableAct = value
            End Set
        End Property

    End Class
#End Region


#End Region
#Region "pla_Empleados"
    Public Class pla_Empleados
        Private _IdEmpleado As System.Int32
        Public Property IdEmpleado() As System.Int32
            Get
                Return _IdEmpleado
            End Get
            Set(ByVal value As System.Int32)
                _IdEmpleado = value
            End Set
        End Property

        Private _Codigo As System.String = ""
        Public Property Codigo() As System.String
            Get
                Return _Codigo
            End Get
            Set(ByVal value As System.String)
                _Codigo = value
            End Set
        End Property

        Private _Nombres As System.String = ""
        Public Property Nombres() As System.String
            Get
                Return _Nombres
            End Get
            Set(ByVal value As System.String)
                _Nombres = value
            End Set
        End Property

        Private _Apellidos As System.String = ""
        Public Property Apellidos() As System.String
            Get
                Return _Apellidos
            End Get
            Set(ByVal value As System.String)
                _Apellidos = value
            End Set
        End Property

        Private _IdSexo As System.Int32
        Public Property IdSexo() As System.Int32
            Get
                Return _IdSexo
            End Get
            Set(ByVal value As System.Int32)
                _IdSexo = value
            End Set
        End Property

        Private _NombresAfiliacion As System.String = ""
        Public Property NombresAfiliacion() As System.String
            Get
                Return _NombresAfiliacion
            End Get
            Set(ByVal value As System.String)
                _NombresAfiliacion = value
            End Set
        End Property

        Private _ApellidosAfiliacion As System.String = ""
        Public Property ApellidosAfiliacion() As System.String
            Get
                Return _ApellidosAfiliacion
            End Get
            Set(ByVal value As System.String)
                _ApellidosAfiliacion = value
            End Set
        End Property

        Private _NombresAfp As System.String = ""
        Public Property NombresAfp() As System.String
            Get
                Return _NombresAfp
            End Get
            Set(ByVal value As System.String)
                _NombresAfp = value
            End Set
        End Property

        Private _ApellidosAfp As System.String = ""
        Public Property ApellidosAfp() As System.String
            Get
                Return _ApellidosAfp
            End Get
            Set(ByVal value As System.String)
                _ApellidosAfp = value
            End Set
        End Property

        Private _NombresNIT As System.String = ""
        Public Property NombresNIT() As System.String
            Get
                Return _NombresNIT
            End Get
            Set(ByVal value As System.String)
                _NombresNIT = value
            End Set
        End Property

        Private _ApellidosNIT As System.String = ""
        Public Property ApellidosNIT() As System.String
            Get
                Return _ApellidosNIT
            End Get
            Set(ByVal value As System.String)
                _ApellidosNIT = value
            End Set
        End Property

        Private _Direccion As System.String = ""
        Public Property Direccion() As System.String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As System.String)
                _Direccion = value
            End Set
        End Property

        Private _IdDepto As System.String = ""
        Public Property IdDepto() As System.String
            Get
                Return _IdDepto
            End Get
            Set(ByVal value As System.String)
                _IdDepto = value
            End Set
        End Property

        Private _IdMunicipio As System.String = ""
        Public Property IdMunicipio() As System.String
            Get
                Return _IdMunicipio
            End Get
            Set(ByVal value As System.String)
                _IdMunicipio = value
            End Set
        End Property

        Private _Telefono1 As System.String = ""
        Public Property Telefono1() As System.String
            Get
                Return _Telefono1
            End Get
            Set(ByVal value As System.String)
                _Telefono1 = value
            End Set
        End Property

        Private _Telefono2 As System.String = ""
        Public Property Telefono2() As System.String
            Get
                Return _Telefono2
            End Get
            Set(ByVal value As System.String)
                _Telefono2 = value
            End Set
        End Property

        Private _TelefonoMovil As System.String = ""
        Public Property TelefonoMovil() As System.String
            Get
                Return _TelefonoMovil
            End Get
            Set(ByVal value As System.String)
                _TelefonoMovil = value
            End Set
        End Property

        Private _Email As System.String = ""
        Public Property Email() As System.String
            Get
                Return _Email
            End Get
            Set(ByVal value As System.String)
                _Email = value
            End Set
        End Property

        Private _NumeroDocIdentidad As System.String = ""
        Public Property NumeroDocIdentidad() As System.String
            Get
                Return _NumeroDocIdentidad
            End Get
            Set(ByVal value As System.String)
                _NumeroDocIdentidad = value
            End Set
        End Property

        Private _FechaExpedicionDoc As Nullable(Of DateTime)
        Public Property FechaExpedicionDoc() As Nullable(Of DateTime)
            Get
                Return _FechaExpedicionDoc
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaExpedicionDoc = value
            End Set
        End Property

        Private _LugarExpedicionDoc As System.String = ""
        Public Property LugarExpedicionDoc() As System.String
            Get
                Return _LugarExpedicionDoc
            End Get
            Set(ByVal value As System.String)
                _LugarExpedicionDoc = value
            End Set
        End Property

        Private _FechaVenceDoc As Nullable(Of DateTime)
        Public Property FechaVenceDoc() As Nullable(Of DateTime)
            Get
                Return _FechaVenceDoc
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaVenceDoc = value
            End Set
        End Property

        Private _FechaNacimiento As Nullable(Of DateTime)
        Public Property FechaNacimiento() As Nullable(Of DateTime)
            Get
                Return _FechaNacimiento
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaNacimiento = value
            End Set
        End Property

        Private _LugarNacimiento As System.String = ""
        Public Property LugarNacimiento() As System.String
            Get
                Return _LugarNacimiento
            End Get
            Set(ByVal value As System.String)
                _LugarNacimiento = value
            End Set
        End Property

        Private _CodigoZona As System.String = ""
        Public Property CodigoZona() As System.String
            Get
                Return _CodigoZona
            End Get
            Set(ByVal value As System.String)
                _CodigoZona = value
            End Set
        End Property

        Private _NIT As System.String = ""
        Public Property NIT() As System.String
            Get
                Return _NIT
            End Get
            Set(ByVal value As System.String)
                _NIT = value
            End Set
        End Property

        Private _NUP As System.String = ""
        Public Property NUP() As System.String
            Get
                Return _NUP
            End Get
            Set(ByVal value As System.String)
                _NUP = value
            End Set
        End Property

        Private _NumeroISSS As System.String = ""
        Public Property NumeroISSS() As System.String
            Get
                Return _NumeroISSS
            End Get
            Set(ByVal value As System.String)
                _NumeroISSS = value
            End Set
        End Property

        Private _LicenciaConducir As System.String = ""
        Public Property LicenciaConducir() As System.String
            Get
                Return _LicenciaConducir
            End Get
            Set(ByVal value As System.String)
                _LicenciaConducir = value
            End Set
        End Property

        Private _OrigenEmpleado As System.Int32
        Public Property OrigenEmpleado() As System.Int32
            Get
                Return _OrigenEmpleado
            End Get
            Set(ByVal value As System.Int32)
                _OrigenEmpleado = value
            End Set
        End Property

        Private _NumeroPasaporte As System.String = ""
        Public Property NumeroPasaporte() As System.String
            Get
                Return _NumeroPasaporte
            End Get
            Set(ByVal value As System.String)
                _NumeroPasaporte = value
            End Set
        End Property

        Private _TipoDocumento As System.Int32
        Public Property TipoDocumento() As System.Int32
            Get
                Return _TipoDocumento
            End Get
            Set(ByVal value As System.Int32)
                _TipoDocumento = value
            End Set
        End Property

        Private _FechaExpiraDocumento As Nullable(Of DateTime)
        Public Property FechaExpiraDocumento() As Nullable(Of DateTime)
            Get
                Return _FechaExpiraDocumento
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaExpiraDocumento = value
            End Set
        End Property

        Private _PermisoTrabajo As System.Int32
        Public Property PermisoTrabajo() As System.Int32
            Get
                Return _PermisoTrabajo
            End Get
            Set(ByVal value As System.Int32)
                _PermisoTrabajo = value
            End Set
        End Property

        Private _LicenciaArma As System.String = ""
        Public Property LicenciaArma() As System.String
            Get
                Return _LicenciaArma
            End Get
            Set(ByVal value As System.String)
                _LicenciaArma = value
            End Set
        End Property

        Private _FechaVenceArma As Nullable(Of DateTime)
        Public Property FechaVenceArma() As Nullable(Of DateTime)
            Get
                Return _FechaVenceArma
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaVenceArma = value
            End Set
        End Property

        Private _NivelEstudios As System.String = ""
        Public Property NivelEstudios() As System.String
            Get
                Return _NivelEstudios
            End Get
            Set(ByVal value As System.String)
                _NivelEstudios = value
            End Set
        End Property

        Private _IdEstadoCivil As System.Int32
        Public Property IdEstadoCivil() As System.Int32
            Get
                Return _IdEstadoCivil
            End Get
            Set(ByVal value As System.Int32)
                _IdEstadoCivil = value
            End Set
        End Property

        Private _Estatura As System.Decimal
        Public Property Estatura() As System.Decimal
            Get
                Return _Estatura
            End Get
            Set(ByVal value As System.Decimal)
                _Estatura = value
            End Set
        End Property

        Private _Peso As System.Decimal
        Public Property Peso() As System.Decimal
            Get
                Return _Peso
            End Get
            Set(ByVal value As System.Decimal)
                _Peso = value
            End Set
        End Property

        Private _TipoSangre As System.String = ""
        Public Property TipoSangre() As System.String
            Get
                Return _TipoSangre
            End Get
            Set(ByVal value As System.String)
                _TipoSangre = value
            End Set
        End Property

        Private _IdProfesion As System.Int32
        Public Property IdProfesion() As System.Int32
            Get
                Return _IdProfesion
            End Get
            Set(ByVal value As System.Int32)
                _IdProfesion = value
            End Set
        End Property

        Private _IdSector As System.Int32
        Public Property IdSector() As System.Int32
            Get
                Return _IdSector
            End Get
            Set(ByVal value As System.Int32)
                _IdSector = value
            End Set
        End Property

        Private _SenialesEspeciales As System.String = ""
        Public Property SenialesEspeciales() As System.String
            Get
                Return _SenialesEspeciales
            End Get
            Set(ByVal value As System.String)
                _SenialesEspeciales = value
            End Set
        End Property

        Private _NombrePadre As System.String = ""
        Public Property NombrePadre() As System.String
            Get
                Return _NombrePadre
            End Get
            Set(ByVal value As System.String)
                _NombrePadre = value
            End Set
        End Property

        Private _NombreMadre As System.String = ""
        Public Property NombreMadre() As System.String
            Get
                Return _NombreMadre
            End Get
            Set(ByVal value As System.String)
                _NombreMadre = value
            End Set
        End Property

        Private _NombreConyuge As System.String = ""
        Public Property NombreConyuge() As System.String
            Get
                Return _NombreConyuge
            End Get
            Set(ByVal value As System.String)
                _NombreConyuge = value
            End Set
        End Property

        Private _LugarTrabajoConyuge As System.String = ""
        Public Property LugarTrabajoConyuge() As System.String
            Get
                Return _LugarTrabajoConyuge
            End Get
            Set(ByVal value As System.String)
                _LugarTrabajoConyuge = value
            End Set
        End Property

        Private _TelefonoTrabajoConyuge As System.String = ""
        Public Property TelefonoTrabajoConyuge() As System.String
            Get
                Return _TelefonoTrabajoConyuge
            End Get
            Set(ByVal value As System.String)
                _TelefonoTrabajoConyuge = value
            End Set
        End Property

        Private _NumHijos As System.Decimal
        Public Property NumHijos() As System.Decimal
            Get
                Return _NumHijos
            End Get
            Set(ByVal value As System.Decimal)
                _NumHijos = value
            End Set
        End Property

        Private _Contactos As System.String = ""
        Public Property Contactos() As System.String
            Get
                Return _Contactos
            End Get
            Set(ByVal value As System.String)
                _Contactos = value
            End Set
        End Property

        Private _FechaIngreso As Nullable(Of DateTime)
        Public Property FechaIngreso() As Nullable(Of DateTime)
            Get
                Return _FechaIngreso
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaIngreso = value
            End Set
        End Property

        Private _FechaContratacion As Nullable(Of DateTime)
        Public Property FechaContratacion() As Nullable(Of DateTime)
            Get
                Return _FechaContratacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaContratacion = value
            End Set
        End Property

        Private _SalarioOrdinario As System.Decimal
        Public Property SalarioOrdinario() As System.Decimal
            Get
                Return _SalarioOrdinario
            End Get
            Set(ByVal value As System.Decimal)
                _SalarioOrdinario = value
            End Set
        End Property

        Private _FechaCambioSalario As Nullable(Of DateTime)
        Public Property FechaCambioSalario() As Nullable(Of DateTime)
            Get
                Return _FechaCambioSalario
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaCambioSalario = value
            End Set
        End Property

        Private _IdCentro As System.Int32
        Public Property IdCentro() As System.Int32
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.Int32)
                _IdCentro = value
            End Set
        End Property

        Private _IdAfp As System.Int32
        Public Property IdAfp() As System.Int32
            Get
                Return _IdAfp
            End Get
            Set(ByVal value As System.Int32)
                _IdAfp = value
            End Set
        End Property

        Private _PorcAfp As System.Decimal
        Public Property PorcAfp() As System.Decimal
            Get
                Return _PorcAfp
            End Get
            Set(ByVal value As System.Decimal)
                _PorcAfp = value
            End Set
        End Property

        Private _IdDepartamento As System.Int32
        Public Property IdDepartamento() As System.Int32
            Get
                Return _IdDepartamento
            End Get
            Set(ByVal value As System.Int32)
                _IdDepartamento = value
            End Set
        End Property

        Private _IdPlaza As System.Int32
        Public Property IdPlaza() As System.Int32
            Get
                Return _IdPlaza
            End Get
            Set(ByVal value As System.Int32)
                _IdPlaza = value
            End Set
        End Property

        Private _IdBanco As System.Int32
        Public Property IdBanco() As System.Int32
            Get
                Return _IdBanco
            End Get
            Set(ByVal value As System.Int32)
                _IdBanco = value
            End Set
        End Property

        Private _TipoCuentaBanco As System.Int32
        Public Property TipoCuentaBanco() As System.Int32
            Get
                Return _TipoCuentaBanco
            End Get
            Set(ByVal value As System.Int32)
                _TipoCuentaBanco = value
            End Set
        End Property

        Private _NumeroCuenta As System.String = ""
        Public Property NumeroCuenta() As System.String
            Get
                Return _NumeroCuenta
            End Get
            Set(ByVal value As System.String)
                _NumeroCuenta = value
            End Set
        End Property

        Private _PersonaAutorizada As System.String = ""
        Public Property PersonaAutorizada() As System.String
            Get
                Return _PersonaAutorizada
            End Get
            Set(ByVal value As System.String)
                _PersonaAutorizada = value
            End Set
        End Property

        Private _NumDoctoPersona As System.String = ""
        Public Property NumDoctoPersona() As System.String
            Get
                Return _NumDoctoPersona
            End Get
            Set(ByVal value As System.String)
                _NumDoctoPersona = value
            End Set
        End Property

        Private _IdTipoContratacion As System.Int32
        Public Property IdTipoContratacion() As System.Int32
            Get
                Return _IdTipoContratacion
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoContratacion = value
            End Set
        End Property

        Private _IdTipoPlanilla As System.Int32
        Public Property IdTipoPlanilla() As System.Int32
            Get
                Return _IdTipoPlanilla
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoPlanilla = value
            End Set
        End Property

        Private _IdCuentaSalario As System.String = ""
        Public Property IdCuentaSalario() As System.String
            Get
                Return _IdCuentaSalario
            End Get
            Set(ByVal value As System.String)
                _IdCuentaSalario = value
            End Set
        End Property

        Private _IdCuentaCPC As System.String = ""
        Public Property IdCuentaCPC() As System.String
            Get
                Return _IdCuentaCPC
            End Get
            Set(ByVal value As System.String)
                _IdCuentaCPC = value
            End Set
        End Property

        Private _IdCuentaOtros As System.String = ""
        Public Property IdCuentaOtros() As System.String
            Get
                Return _IdCuentaOtros
            End Get
            Set(ByVal value As System.String)
                _IdCuentaOtros = value
            End Set
        End Property

        Private _AplicaSeguroSocial As System.Boolean
        Public Property AplicaSeguroSocial() As System.Boolean
            Get
                Return _AplicaSeguroSocial
            End Get
            Set(ByVal value As System.Boolean)
                _AplicaSeguroSocial = value
            End Set
        End Property

        Private _AplicaISR As System.Boolean
        Public Property AplicaISR() As System.Boolean
            Get
                Return _AplicaISR
            End Get
            Set(ByVal value As System.Boolean)
                _AplicaISR = value
            End Set
        End Property

        Private _AplicaAFP As System.Boolean
        Public Property AplicaAFP() As System.Boolean
            Get
                Return _AplicaAFP
            End Get
            Set(ByVal value As System.Boolean)
                _AplicaAFP = value
            End Set
        End Property

        Private _RutaFoto As System.String = ""
        Public Property RutaFoto() As System.String
            Get
                Return _RutaFoto
            End Get
            Set(ByVal value As System.String)
                _RutaFoto = value
            End Set
        End Property

        Private _InformacionAdicional As System.String = ""
        Public Property InformacionAdicional() As System.String
            Get
                Return _InformacionAdicional
            End Get
            Set(ByVal value As System.String)
                _InformacionAdicional = value
            End Set
        End Property

        Private _IdEstadoEmpleado As System.Int32
        Public Property IdEstadoEmpleado() As System.Int32
            Get
                Return _IdEstadoEmpleado
            End Get
            Set(ByVal value As System.Int32)
                _IdEstadoEmpleado = value
            End Set
        End Property

        Private _FechaEstado1 As Nullable(Of DateTime)
        Public Property FechaEstado1() As Nullable(Of DateTime)
            Get
                Return _FechaEstado1
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaEstado1 = value
            End Set
        End Property

        Private _FechaEstado2 As Nullable(Of DateTime)
        Public Property FechaEstado2() As Nullable(Of DateTime)
            Get
                Return _FechaEstado2
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaEstado2 = value
            End Set
        End Property

        Private _DescripcionTrabajo As System.String = ""
        Public Property DescripcionTrabajo() As System.String
            Get
                Return _DescripcionTrabajo
            End Get
            Set(ByVal value As System.String)
                _DescripcionTrabajo = value
            End Set
        End Property

        Private _IdLugar As System.Int32
        Public Property IdLugar() As System.Int32
            Get
                Return _IdLugar
            End Get
            Set(ByVal value As System.Int32)
                _IdLugar = value
            End Set
        End Property

        Private _IdPlazaJefe As System.Int32
        Public Property IdPlazaJefe() As System.Int32
            Get
                Return _IdPlazaJefe
            End Get
            Set(ByVal value As System.Int32)
                _IdPlazaJefe = value
            End Set
        End Property

        Private _Horarios As System.String = ""
        Public Property Horarios() As System.String
            Get
                Return _Horarios
            End Get
            Set(ByVal value As System.String)
                _Horarios = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region

#Region "Eventos"
#Region "eve_Eventos"
    Public Class eve_Eventos
        Private _IdEvento As System.Int32
        Public Property IdEvento() As System.Int32
            Get
                Return _IdEvento
            End Get
            Set(ByVal value As System.Int32)
                _IdEvento = value
            End Set
        End Property
        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property
        Private _CodEvento As System.String = ""
        Public Property CodEvento() As System.String
            Get
                Return _CodEvento
            End Get
            Set(ByVal value As System.String)
                _CodEvento = value
            End Set
        End Property

        Private _FechaInicio As Nullable(Of DateTime)
        Public Property FechaInicio() As Nullable(Of DateTime)
            Get
                Return _FechaInicio
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaInicio = value
            End Set
        End Property

        Private _FechaFin As Nullable(Of DateTime)
        Public Property FechaFin() As Nullable(Of DateTime)
            Get
                Return _FechaFin
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaFin = value
            End Set
        End Property

        Private _DescripcionEvento As System.String = ""
        Public Property DescripcionEvento() As System.String
            Get
                Return _DescripcionEvento
            End Get
            Set(ByVal value As System.String)
                _DescripcionEvento = value
            End Set
        End Property
        Private _Grupo As System.String = ""
        Public Property Grupo() As System.String
            Get
                Return _Grupo
            End Get
            Set(ByVal value As System.String)
                _Grupo = value
            End Set
        End Property
        Private _Cerrado As System.Boolean
        Public Property Cerrado() As System.Boolean
            Get
                Return _Cerrado
            End Get
            Set(ByVal value As System.Boolean)
                _Cerrado = value
            End Set
        End Property

        Private _IdCuentaContable As System.String = ""
        Public Property IdCuentaContable() As System.String
            Get
                Return _IdCuentaContable
            End Get
            Set(ByVal value As System.String)
                _IdCuentaContable = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region

#Region "eve_EventosDetalle"
    Public Class eve_EventosDetalle
        Private _IdEvento As System.Int32
        Public Property IdEvento() As System.Int32
            Get
                Return _IdEvento
            End Get
            Set(ByVal value As System.Int32)
                _IdEvento = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdSeminario As System.Int32
        Public Property IdSeminario() As System.Int32
            Get
                Return _IdSeminario
            End Get
            Set(ByVal value As System.Int32)
                _IdSeminario = value
            End Set
        End Property

        Private _NumHoras As System.Decimal
        Public Property NumHoras() As System.Decimal
            Get
                Return _NumHoras
            End Get
            Set(ByVal value As System.Decimal)
                _NumHoras = value
            End Set
        End Property

        Private _PrecioSocio As System.Decimal
        Public Property PrecioSocio() As System.Decimal
            Get
                Return _PrecioSocio
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioSocio = value
            End Set
        End Property

        Private _PrecioNoSocio As System.Decimal
        Public Property PrecioNoSocio() As System.Decimal
            Get
                Return _PrecioNoSocio
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioNoSocio = value
            End Set
        End Property

    End Class
#End Region
#Region "eve_EventosDetallePago"
    Public Class eve_EventosDetallePago
        Private _IdEvento As System.Int32
        Public Property IdEvento() As System.Int32
            Get
                Return _IdEvento
            End Get
            Set(ByVal value As System.Int32)
                _IdEvento = value
            End Set
        End Property

        Private _NumCuota As System.Int32
        Public Property NumCuota() As System.Int32
            Get
                Return _NumCuota
            End Get
            Set(ByVal value As System.Int32)
                _NumCuota = value
            End Set
        End Property

        Private _PrecioSocio As System.Decimal
        Public Property PrecioSocio() As System.Decimal
            Get
                Return _PrecioSocio
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioSocio = value
            End Set
        End Property

        Private _PrecioNoSocio As System.Decimal
        Public Property PrecioNoSocio() As System.Decimal
            Get
                Return _PrecioNoSocio
            End Get
            Set(ByVal value As System.Decimal)
                _PrecioNoSocio = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

    End Class
#End Region
#Region "eve_CentrosCostos"
    Public Class eve_CentrosCostos
        Private _IdCentro As System.Int32
        Public Property IdCentro() As System.Int32
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.Int32)
                _IdCentro = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

        Private _Efecto As System.Int32
        Public Property Efecto() As System.Int32
            Get
                Return _Efecto
            End Get
            Set(ByVal value As System.Int32)
                _Efecto = value
            End Set
        End Property

    End Class
#End Region
#Region "eve_Abonos"
    Public Class eve_Abonos
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _NumeroComprobante As System.String = ""
        Public Property NumeroComprobante() As System.String
            Get
                Return _NumeroComprobante
            End Get
            Set(ByVal value As System.String)
                _NumeroComprobante = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdCuentaBancaria As System.Int32
        Public Property IdCuentaBancaria() As System.Int32
            Get
                Return _IdCuentaBancaria
            End Get
            Set(ByVal value As System.Int32)
                _IdCuentaBancaria = value
            End Set
        End Property

        Private _IdCobrador As System.Int32
        Public Property IdCobrador() As System.Int32
            Get
                Return _IdCobrador
            End Get
            Set(ByVal value As System.Int32)
                _IdCobrador = value
            End Set
        End Property

        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _IdTipoMov As System.Byte
        Public Property IdTipoMov() As System.Byte
            Get
                Return _IdTipoMov
            End Get
            Set(ByVal value As System.Byte)
                _IdTipoMov = value
            End Set
        End Property

        Private _IdSucursal As System.Int32
        Public Property IdSucursal() As System.Int32
            Get
                Return _IdSucursal
            End Get
            Set(ByVal value As System.Int32)
                _IdSucursal = value
            End Set
        End Property

        Private _IdEvento As System.Int32
        Public Property IdEvento() As System.Int32
            Get
                Return _IdEvento
            End Get
            Set(ByVal value As System.Int32)
                _IdEvento = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _BancoProcedencia As System.String = ""
        Public Property BancoProcedencia() As System.String
            Get
                Return _BancoProcedencia
            End Get
            Set(ByVal value As System.String)
                _BancoProcedencia = value
            End Set
        End Property

        Private _NumCheque As System.String = ""
        Public Property NumCheque() As System.String
            Get
                Return _NumCheque
            End Get
            Set(ByVal value As System.String)
                _NumCheque = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region
#Region "eve_InscripcionesParticipantes"
    Public Class eve_InscripcionesParticipantes
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Participante As System.String = ""
        Public Property Participante() As System.String
            Get
                Return _Participante
            End Get
            Set(ByVal value As System.String)
                _Participante = value
            End Set
        End Property

        Private _IdSeminario As System.Int32
        Public Property IdSeminario() As System.Int32
            Get
                Return _IdSeminario
            End Get
            Set(ByVal value As System.Int32)
                _IdSeminario = value
            End Set
        End Property

        Private _Aplica As System.Boolean
        Public Property Aplica() As System.Boolean
            Get
                Return _Aplica
            End Get
            Set(ByVal value As System.Boolean)
                _Aplica = value
            End Set
        End Property

    End Class
#End Region


#Region "eve_AbonosDetalle"
    Public Class eve_AbonosDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _IdComprobVenta As System.Int32
        Public Property IdComprobVenta() As System.Int32
            Get
                Return _IdComprobVenta
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobVenta = value
            End Set
        End Property

        Private _IdParticipante As System.Int32
        Public Property IdParticipante() As System.Int32
            Get
                Return _IdParticipante
            End Get
            Set(ByVal value As System.Int32)
                _IdParticipante = value
            End Set
        End Property

        Private _MontoAbonado As System.Decimal
        Public Property MontoAbonado() As System.Decimal
            Get
                Return _MontoAbonado
            End Get
            Set(ByVal value As System.Decimal)
                _MontoAbonado = value
            End Set
        End Property

        Private _SaldoComprobante As System.Decimal
        Public Property SaldoComprobante() As System.Decimal
            Get
                Return _SaldoComprobante
            End Get
            Set(ByVal value As System.Decimal)
                _SaldoComprobante = value
            End Set
        End Property

        Private _Concepto As System.String = ""
        Public Property Concepto() As System.String
            Get
                Return _Concepto
            End Get
            Set(ByVal value As System.String)
                _Concepto = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

    End Class
#End Region




#Region "eve_Seminarios"
    Public Class eve_Seminarios
        Private _IdSeminario As System.Int32
        Public Property IdSeminario() As System.Int32
            Get
                Return _IdSeminario
            End Get
            Set(ByVal value As System.Int32)
                _IdSeminario = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "eve_Inscripciones"
    Public Class eve_Inscripciones
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdCliente As System.String = ""
        Public Property IdCliente() As System.String
            Get
                Return _IdCliente
            End Get
            Set(ByVal value As System.String)
                _IdCliente = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdEvento As System.Int32
        Public Property IdEvento() As System.Int32
            Get
                Return _IdEvento
            End Get
            Set(ByVal value As System.Int32)
                _IdEvento = value
            End Set
        End Property

        Private _IdTipoComprobante As System.Int32
        Public Property IdTipoComprobante() As System.Int32
            Get
                Return _IdTipoComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoComprobante = value
            End Set
        End Property

        Private _Responsable As System.String = ""
        Public Property Responsable() As System.String
            Get
                Return _Responsable
            End Get
            Set(ByVal value As System.String)
                _Responsable = value
            End Set
        End Property

        Private _AplicaRetencion As System.Boolean
        Public Property AplicaRetencion() As System.Boolean
            Get
                Return _AplicaRetencion
            End Get
            Set(ByVal value As System.Boolean)
                _AplicaRetencion = value
            End Set
        End Property

        Private _ResponsablePago As System.String = ""
        Public Property ResponsablePago() As System.String
            Get
                Return _ResponsablePago
            End Get
            Set(ByVal value As System.String)
                _ResponsablePago = value
            End Set
        End Property

        Private _CargoResponsablePago As System.String = ""
        Public Property CargoResponsablePago() As System.String
            Get
                Return _CargoResponsablePago
            End Get
            Set(ByVal value As System.String)
                _CargoResponsablePago = value
            End Set
        End Property

        Private _TelefonosResponsablePago As System.String = ""
        Public Property TelefonosResponsablePago() As System.String
            Get
                Return _TelefonosResponsablePago
            End Get
            Set(ByVal value As System.String)
                _TelefonosResponsablePago = value
            End Set
        End Property

        Private _DeptoResponsablePago As System.String = ""
        Public Property DeptoResponsablePago() As System.String
            Get
                Return _DeptoResponsablePago
            End Get
            Set(ByVal value As System.String)
                _DeptoResponsablePago = value
            End Set
        End Property
        Private _DetallarParticipantes As System.Boolean
        Public Property DetallarParticipantes() As System.Boolean
            Get
                Return _DetallarParticipantes
            End Get
            Set(ByVal value As System.Boolean)
                _DetallarParticipantes = value
            End Set
        End Property

        Private _IdTipoPago As System.Int32
        Public Property IdTipoPago() As System.Int32
            Get
                Return _IdTipoPago
            End Get
            Set(ByVal value As System.Int32)
                _IdTipoPago = value
            End Set
        End Property


        Private _FechaUltAbono As Nullable(Of DateTime)
        Public Property FechaUltAbono() As Nullable(Of DateTime)
            Get
                Return _FechaUltAbono
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaUltAbono = value
            End Set
        End Property
        Private _FacturarPorGrupo As System.Boolean
        Public Property FacturarPorGrupo() As System.Boolean
            Get
                Return _FacturarPorGrupo
            End Get
            Set(ByVal value As System.Boolean)
                _FacturarPorGrupo = value
            End Set
        End Property
        Private _FacturarHoras As System.Boolean
        Public Property FacturarHoras() As System.Boolean
            Get
                Return _FacturarHoras
            End Get
            Set(ByVal value As System.Boolean)
                _FacturarHoras = value
            End Set
        End Property
        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region


#Region "eve_InscripcionesDetalle"
    Public Class eve_InscripcionesDetalle
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _IdDetalle As System.Int32
        Public Property IdDetalle() As System.Int32
            Get
                Return _IdDetalle
            End Get
            Set(ByVal value As System.Int32)
                _IdDetalle = value
            End Set
        End Property

        Private _PorcDescto As System.Decimal
        Public Property PorcDescto() As System.Decimal
            Get
                Return _PorcDescto
            End Get
            Set(ByVal value As System.Decimal)
                _PorcDescto = value
            End Set
        End Property

        Private _NombreParticipantes As System.String = ""
        Public Property NombreParticipantes() As System.String
            Get
                Return _NombreParticipantes
            End Get
            Set(ByVal value As System.String)
                _NombreParticipantes = value
            End Set
        End Property

        Private _Socio As System.Boolean
        Public Property Socio() As System.Boolean
            Get
                Return _Socio
            End Get
            Set(ByVal value As System.Boolean)
                _Socio = value
            End Set
        End Property

        Private _RequiereHoras As System.Boolean
        Public Property RequiereHoras() As System.Boolean
            Get
                Return _RequiereHoras
            End Get
            Set(ByVal value As System.Boolean)
                _RequiereHoras = value
            End Set
        End Property
        Private _Cantidad As System.Int32
        Public Property Cantidad() As System.Int32
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As System.Int32)
                _Cantidad = value
            End Set
        End Property

        Private _NumInscripcion As System.String = ""
        Public Property NumInscripcion() As System.String
            Get
                Return _NumInscripcion
            End Get
            Set(ByVal value As System.String)
                _NumInscripcion = value
            End Set
        End Property

        Private _Cargo As System.String = ""
        Public Property Cargo() As System.String
            Get
                Return _Cargo
            End Get
            Set(ByVal value As System.String)
                _Cargo = value
            End Set
        End Property

        Private _CorreoElectronico As System.String = ""
        Public Property CorreoElectronico() As System.String
            Get
                Return _CorreoElectronico
            End Get
            Set(ByVal value As System.String)
                _CorreoElectronico = value
            End Set
        End Property

    End Class
#End Region
#Region "eve_Participantes"
    Public Class eve_Participantes
        Private _NumInscripcion As System.String = ""
        Public Property NumInscripcion() As System.String
            Get
                Return _NumInscripcion
            End Get
            Set(ByVal value As System.String)
                _NumInscripcion = value
            End Set
        End Property

        Private _Nombre As System.String = ""
        Public Property Nombre() As System.String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As System.String)
                _Nombre = value
            End Set
        End Property

    End Class
#End Region
#Region "eve_TransaccionesEventos"
    Public Class eve_TransaccionesEventos
        Private _IdComprobante As System.Int32
        Public Property IdComprobante() As System.Int32
            Get
                Return _IdComprobante
            End Get
            Set(ByVal value As System.Int32)
                _IdComprobante = value
            End Set
        End Property

        Private _Fecha As Nullable(Of DateTime)
        Public Property Fecha() As Nullable(Of DateTime)
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _Fecha = value
            End Set
        End Property

        Private _IdEvento As System.Int32
        Public Property IdEvento() As System.Int32
            Get
                Return _IdEvento
            End Get
            Set(ByVal value As System.Int32)
                _IdEvento = value
            End Set
        End Property

        Private _IdCentro As System.Int32
        Public Property IdCentro() As System.Int32
            Get
                Return _IdCentro
            End Get
            Set(ByVal value As System.Int32)
                _IdCentro = value
            End Set
        End Property

        Private _Valor As System.Decimal
        Public Property Valor() As System.Decimal
            Get
                Return _Valor
            End Get
            Set(ByVal value As System.Decimal)
                _Valor = value
            End Set
        End Property

        Private _Observaciones As System.String = ""
        Public Property Observaciones() As System.String
            Get
                Return _Observaciones
            End Get
            Set(ByVal value As System.String)
                _Observaciones = value
            End Set
        End Property

        Private _Autorizado As System.Boolean
        Public Property Autorizado() As System.Boolean
            Get
                Return _Autorizado
            End Get
            Set(ByVal value As System.Boolean)
                _Autorizado = value
            End Set
        End Property

        Private _CreadoPor As System.String = ""
        Public Property CreadoPor() As System.String
            Get
                Return _CreadoPor
            End Get
            Set(ByVal value As System.String)
                _CreadoPor = value
            End Set
        End Property

        Private _FechaHoraCreacion As Nullable(Of DateTime)
        Public Property FechaHoraCreacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraCreacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraCreacion = value
            End Set
        End Property

        Private _ModificadoPor As System.String = ""
        Public Property ModificadoPor() As System.String
            Get
                Return _ModificadoPor
            End Get
            Set(ByVal value As System.String)
                _ModificadoPor = value
            End Set
        End Property

        Private _FechaHoraModificacion As Nullable(Of DateTime)
        Public Property FechaHoraModificacion() As Nullable(Of DateTime)
            Get
                Return _FechaHoraModificacion
            End Get
            Set(ByVal value As Nullable(Of DateTime))
                _FechaHoraModificacion = value
            End Set
        End Property

    End Class
#End Region





#End Region
End Class
