﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ban_frmLiquidarCajaChica
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ban_frmLiquidarCajaChica))
        Me.LayoutConverter1 = New DevExpress.XtraLayout.Converter.LayoutConverter(Me.components)
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.teTotal = New DevExpress.XtraEditors.TextEdit()
        Me.sbSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.ceFHC = New DevExpress.XtraEditors.CheckEdit()
        Me.deHasta = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.sbGenerar = New DevExpress.XtraEditors.SimpleButton()
        Me.deDesde = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.leCaja = New DevExpress.XtraEditors.LookUpEdit()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIdCompra = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colNumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSerie = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colProveedor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAplica = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkAplica = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.leGasto = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.ceFHC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAplica, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leGasto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.LabelControl20)
        Me.PanelControl1.Controls.Add(Me.teTotal)
        Me.PanelControl1.Controls.Add(Me.sbSalir)
        Me.PanelControl1.Controls.Add(Me.sbGuardar)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl1.Location = New System.Drawing.Point(0, 372)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(679, 39)
        Me.PanelControl1.TabIndex = 23
        '
        'LabelControl20
        '
        Me.LabelControl20.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl20.Location = New System.Drawing.Point(402, 5)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(98, 13)
        Me.LabelControl20.TabIndex = 26
        Me.LabelControl20.Text = "TOTAL A LIQUIDAR:"
        '
        'teTotal
        '
        Me.teTotal.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teTotal.EditValue = 0
        Me.teTotal.Location = New System.Drawing.Point(506, 2)
        Me.teTotal.Name = "teTotal"
        Me.teTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotal.Properties.Mask.EditMask = "n2"
        Me.teTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTotal.Properties.ReadOnly = True
        Me.teTotal.Size = New System.Drawing.Size(129, 20)
        Me.teTotal.TabIndex = 25
        '
        'sbSalir
        '
        Me.sbSalir.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbSalir.Appearance.Options.UseFont = True
        Me.sbSalir.Location = New System.Drawing.Point(200, 1)
        Me.sbSalir.Name = "sbSalir"
        Me.sbSalir.Size = New System.Drawing.Size(124, 33)
        Me.sbSalir.TabIndex = 24
        Me.sbSalir.Text = "Salir"
        '
        'sbGuardar
        '
        Me.sbGuardar.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbGuardar.Appearance.Options.UseFont = True
        Me.sbGuardar.Location = New System.Drawing.Point(69, 1)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(128, 33)
        Me.sbGuardar.TabIndex = 23
        Me.sbGuardar.Text = "Aceptar"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.ceFHC)
        Me.PanelControl2.Controls.Add(Me.deHasta)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.sbGenerar)
        Me.PanelControl2.Controls.Add(Me.deDesde)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.LabelControl22)
        Me.PanelControl2.Controls.Add(Me.leCaja)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl2.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(679, 56)
        Me.PanelControl2.TabIndex = 24
        '
        'ceFHC
        '
        Me.ceFHC.Location = New System.Drawing.Point(189, 31)
        Me.ceFHC.Name = "ceFHC"
        Me.ceFHC.Properties.Caption = "Fecha Creacion Compra"
        Me.ceFHC.Size = New System.Drawing.Size(144, 19)
        Me.ceFHC.TabIndex = 89
        '
        'deHasta
        '
        Me.deHasta.EditValue = Nothing
        Me.deHasta.EnterMoveNextControl = True
        Me.deHasta.Location = New System.Drawing.Point(75, 30)
        Me.deHasta.Name = "deHasta"
        Me.deHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deHasta.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deHasta.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deHasta.Size = New System.Drawing.Size(96, 20)
        Me.deHasta.TabIndex = 87
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(5, 33)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 88
        Me.LabelControl2.Text = "Hasta Fecha:"
        '
        'sbGenerar
        '
        Me.sbGenerar.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbGenerar.Appearance.Options.UseFont = True
        Me.sbGenerar.Location = New System.Drawing.Point(351, 30)
        Me.sbGenerar.Name = "sbGenerar"
        Me.sbGenerar.Size = New System.Drawing.Size(153, 21)
        Me.sbGenerar.TabIndex = 2
        Me.sbGenerar.Text = "Obtener Compras"
        '
        'deDesde
        '
        Me.deDesde.EditValue = Nothing
        Me.deDesde.EnterMoveNextControl = True
        Me.deDesde.Location = New System.Drawing.Point(75, 5)
        Me.deDesde.Name = "deDesde"
        Me.deDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deDesde.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deDesde.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deDesde.Size = New System.Drawing.Size(96, 20)
        Me.deDesde.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(5, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl1.TabIndex = 86
        Me.LabelControl1.Text = "Desde Fecha:"
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(189, 10)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl22.TabIndex = 84
        Me.LabelControl22.Text = "Caja Chica:"
        '
        'leCaja
        '
        Me.leCaja.EnterMoveNextControl = True
        Me.leCaja.Location = New System.Drawing.Point(247, 5)
        Me.leCaja.Name = "leCaja"
        Me.leCaja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCaja.Size = New System.Drawing.Size(257, 20)
        Me.leCaja.TabIndex = 1
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 56)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkAplica, Me.leGasto})
        Me.gc.Size = New System.Drawing.Size(679, 316)
        Me.gc.TabIndex = 25
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIdCompra, Me.colNumero, Me.GridColumn1, Me.colSerie, Me.colProveedor, Me.colTotal, Me.colAplica})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'colIdCompra
        '
        Me.colIdCompra.Caption = "Id"
        Me.colIdCompra.FieldName = "IdComprobante"
        Me.colIdCompra.Name = "colIdCompra"
        Me.colIdCompra.OptionsColumn.AllowEdit = False
        Me.colIdCompra.OptionsColumn.AllowFocus = False
        Me.colIdCompra.Visible = True
        Me.colIdCompra.VisibleIndex = 0
        Me.colIdCompra.Width = 39
        '
        'colNumero
        '
        Me.colNumero.Caption = "Número"
        Me.colNumero.FieldName = "Numero"
        Me.colNumero.Name = "colNumero"
        Me.colNumero.OptionsColumn.AllowEdit = False
        Me.colNumero.OptionsColumn.AllowFocus = False
        Me.colNumero.Visible = True
        Me.colNumero.VisibleIndex = 1
        Me.colNumero.Width = 69
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Fecha"
        Me.GridColumn1.FieldName = "Fecha"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.AllowFocus = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 2
        Me.GridColumn1.Width = 77
        '
        'colSerie
        '
        Me.colSerie.Caption = "Serie"
        Me.colSerie.FieldName = "Serie"
        Me.colSerie.Name = "colSerie"
        Me.colSerie.OptionsColumn.AllowEdit = False
        Me.colSerie.OptionsColumn.AllowFocus = False
        Me.colSerie.Visible = True
        Me.colSerie.VisibleIndex = 3
        '
        'colProveedor
        '
        Me.colProveedor.Caption = "Proveedor"
        Me.colProveedor.FieldName = "Nombre"
        Me.colProveedor.Name = "colProveedor"
        Me.colProveedor.OptionsColumn.AllowEdit = False
        Me.colProveedor.OptionsColumn.AllowFocus = False
        Me.colProveedor.Visible = True
        Me.colProveedor.VisibleIndex = 4
        Me.colProveedor.Width = 435
        '
        'colTotal
        '
        Me.colTotal.Caption = "Total Comprobante"
        Me.colTotal.DisplayFormat.FormatString = "n2"
        Me.colTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colTotal.FieldName = "TotalComprobante"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.OptionsColumn.AllowEdit = False
        Me.colTotal.Visible = True
        Me.colTotal.VisibleIndex = 5
        Me.colTotal.Width = 98
        '
        'colAplica
        '
        Me.colAplica.Caption = "Liquidar ?"
        Me.colAplica.ColumnEdit = Me.chkAplica
        Me.colAplica.FieldName = "Liquidar"
        Me.colAplica.Name = "colAplica"
        Me.colAplica.Visible = True
        Me.colAplica.VisibleIndex = 6
        Me.colAplica.Width = 54
        '
        'chkAplica
        '
        Me.chkAplica.AutoHeight = False
        Me.chkAplica.Name = "chkAplica"
        '
        'leGasto
        '
        Me.leGasto.AutoHeight = False
        Me.leGasto.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leGasto.Name = "leGasto"
        '
        'ban_frmLiquidarCajaChica
        '
        Me.ClientSize = New System.Drawing.Size(679, 411)
        Me.ControlBox = False
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ban_frmLiquidarCajaChica"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Liquidación de Caja Chica"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.ceFHC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAplica, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leGasto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutConverter1 As DevExpress.XtraLayout.Converter.LayoutConverter
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIdCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSerie As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leGasto As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents colAplica As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkAplica As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCaja As DevExpress.XtraEditors.LookUpEdit
    Public WithEvents deDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbGenerar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colProveedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents sbSalir As DevExpress.XtraEditors.SimpleButton
    Public WithEvents deHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ceFHC As DevExpress.XtraEditors.CheckEdit
End Class
