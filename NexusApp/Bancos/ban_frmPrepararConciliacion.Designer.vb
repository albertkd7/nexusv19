﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ban_frmPrepararConciliacion
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdProcesar = New System.Windows.Forms.Button()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.chkMarca = New DevExpress.XtraEditors.CheckEdit()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.dgCheques = New DevExpress.XtraGrid.GridControl()
        Me.gvCheques = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Conciliado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.snConciliado = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.IdCheque = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Numero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Anombrede = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.fecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.valor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.dgTransac = New DevExpress.XtraGrid.GridControl()
        Me.gvTransacciones = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.procesadabanco = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IdTransaccion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.num_referencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.concepto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.fecha_transaccion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.dgTransac2 = New DevExpress.XtraGrid.GridControl()
        Me.gvTransacciones2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.leCtaBancaria = New DevExpress.XtraEditors.LookUpEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.teChequesMarcados = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.teChequesNoMarcados = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teTransaccionesNoMarcadas = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teTransaccionesMarcadas = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.teChequesDelDia = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teTransaccionesDelDia = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMarca.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgCheques, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvCheques, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.snConciliado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgTransac, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvTransacciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.dgTransac2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvTransacciones2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teChequesMarcados.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teChequesNoMarcados.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTransaccionesNoMarcadas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTransaccionesMarcadas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teChequesDelDia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTransaccionesDelDia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(35, 61)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Banco / Cuenta:"
        '
        'cmdProcesar
        '
        Me.cmdProcesar.Location = New System.Drawing.Point(126, 83)
        Me.cmdProcesar.Name = "cmdProcesar"
        Me.cmdProcesar.Size = New System.Drawing.Size(104, 22)
        Me.cmdProcesar.TabIndex = 3
        Me.cmdProcesar.Text = "Obtener Datos..."
        Me.cmdProcesar.UseVisualStyleBackColor = True
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(34, 15)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl1.TabIndex = 29
        Me.LabelControl1.Text = "Seleccione Fecha:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.Location = New System.Drawing.Point(126, 12)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 0
        '
        'chkMarca
        '
        Me.chkMarca.Location = New System.Drawing.Point(522, 60)
        Me.chkMarca.Name = "chkMarca"
        Me.chkMarca.Properties.Caption = "Marcar Todos los registros"
        Me.chkMarca.Size = New System.Drawing.Size(179, 19)
        Me.chkMarca.TabIndex = 32
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(34, 136)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(988, 377)
        Me.TabControl1.TabIndex = 33
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.dgCheques)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(980, 351)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Cheques"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'dgCheques
        '
        Me.dgCheques.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgCheques.Location = New System.Drawing.Point(3, 3)
        Me.dgCheques.MainView = Me.gvCheques
        Me.dgCheques.Name = "dgCheques"
        Me.dgCheques.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.snConciliado})
        Me.dgCheques.Size = New System.Drawing.Size(974, 345)
        Me.dgCheques.TabIndex = 32
        Me.dgCheques.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvCheques})
        '
        'gvCheques
        '
        Me.gvCheques.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Conciliado, Me.IdCheque, Me.Numero, Me.Anombrede, Me.fecha, Me.valor, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10})
        Me.gvCheques.GridControl = Me.dgCheques
        Me.gvCheques.Name = "gvCheques"
        Me.gvCheques.OptionsView.ShowGroupPanel = False
        '
        'Conciliado
        '
        Me.Conciliado.Caption = "Conciliado"
        Me.Conciliado.ColumnEdit = Me.snConciliado
        Me.Conciliado.FieldName = "Conciliado"
        Me.Conciliado.Name = "Conciliado"
        Me.Conciliado.Visible = True
        Me.Conciliado.VisibleIndex = 0
        Me.Conciliado.Width = 104
        '
        'snConciliado
        '
        Me.snConciliado.AutoHeight = False
        Me.snConciliado.Name = "snConciliado"
        '
        'IdCheque
        '
        Me.IdCheque.Caption = "id cheque"
        Me.IdCheque.FieldName = "IdCheque"
        Me.IdCheque.Name = "IdCheque"
        '
        'Numero
        '
        Me.Numero.Caption = "Núm. de Cheque"
        Me.Numero.FieldName = "Numero"
        Me.Numero.Name = "Numero"
        Me.Numero.OptionsColumn.AllowEdit = False
        Me.Numero.Visible = True
        Me.Numero.VisibleIndex = 1
        Me.Numero.Width = 94
        '
        'Anombrede
        '
        Me.Anombrede.Caption = "A Nombre De"
        Me.Anombrede.FieldName = "AnombreDe"
        Me.Anombrede.Name = "Anombrede"
        Me.Anombrede.OptionsColumn.AllowEdit = False
        Me.Anombrede.Visible = True
        Me.Anombrede.VisibleIndex = 2
        Me.Anombrede.Width = 215
        '
        'fecha
        '
        Me.fecha.Caption = "Fecha de emisión"
        Me.fecha.FieldName = "Fecha"
        Me.fecha.Name = "fecha"
        Me.fecha.OptionsColumn.AllowEdit = False
        Me.fecha.Visible = True
        Me.fecha.VisibleIndex = 3
        Me.fecha.Width = 53
        '
        'valor
        '
        Me.valor.Caption = "Valor del Cheque"
        Me.valor.DisplayFormat.FormatString = "c"
        Me.valor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.valor.FieldName = "Valor"
        Me.valor.Name = "valor"
        Me.valor.OptionsColumn.AllowEdit = False
        Me.valor.Visible = True
        Me.valor.VisibleIndex = 4
        Me.valor.Width = 59
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "GridColumn8"
        Me.GridColumn8.FieldName = "IdCuentaContable"
        Me.GridColumn8.Name = "GridColumn8"
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "GridColumn9"
        Me.GridColumn9.FieldName = "Debe"
        Me.GridColumn9.Name = "GridColumn9"
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "GridColumn10"
        Me.GridColumn10.FieldName = "Haber"
        Me.GridColumn10.Name = "GridColumn10"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.dgTransac)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(980, 351)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Transacciones"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'dgTransac
        '
        Me.dgTransac.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgTransac.Location = New System.Drawing.Point(24, 13)
        Me.dgTransac.MainView = Me.gvTransacciones
        Me.dgTransac.Name = "dgTransac"
        Me.dgTransac.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.dgTransac.Size = New System.Drawing.Size(932, 326)
        Me.dgTransac.TabIndex = 41
        Me.dgTransac.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvTransacciones})
        '
        'gvTransacciones
        '
        Me.gvTransacciones.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.procesadabanco, Me.IdTransaccion, Me.num_referencia, Me.concepto, Me.fecha_transaccion, Me.GridColumn1, Me.GridColumn11, Me.GridColumn12})
        Me.gvTransacciones.GridControl = Me.dgTransac
        Me.gvTransacciones.Name = "gvTransacciones"
        Me.gvTransacciones.OptionsView.ShowGroupPanel = False
        '
        'procesadabanco
        '
        Me.procesadabanco.Caption = "Procesada?"
        Me.procesadabanco.FieldName = "ProcesadaBanco"
        Me.procesadabanco.Name = "procesadabanco"
        Me.procesadabanco.Visible = True
        Me.procesadabanco.VisibleIndex = 0
        Me.procesadabanco.Width = 69
        '
        'IdTransaccion
        '
        Me.IdTransaccion.Caption = "IdTransaccion"
        Me.IdTransaccion.FieldName = "IdTransaccion"
        Me.IdTransaccion.Name = "IdTransaccion"
        '
        'num_referencia
        '
        Me.num_referencia.Caption = "No. Referencia"
        Me.num_referencia.FieldName = "Referencia"
        Me.num_referencia.Name = "num_referencia"
        Me.num_referencia.OptionsColumn.AllowEdit = False
        Me.num_referencia.Visible = True
        Me.num_referencia.VisibleIndex = 1
        Me.num_referencia.Width = 102
        '
        'concepto
        '
        Me.concepto.Caption = "Concepto de la Transacción"
        Me.concepto.FieldName = "Concepto"
        Me.concepto.Name = "concepto"
        Me.concepto.OptionsColumn.AllowEdit = False
        Me.concepto.Visible = True
        Me.concepto.VisibleIndex = 2
        Me.concepto.Width = 153
        '
        'fecha_transaccion
        '
        Me.fecha_transaccion.Caption = "Fecha"
        Me.fecha_transaccion.FieldName = "Fecha"
        Me.fecha_transaccion.Name = "fecha_transaccion"
        Me.fecha_transaccion.OptionsColumn.AllowEdit = False
        Me.fecha_transaccion.Visible = True
        Me.fecha_transaccion.VisibleIndex = 3
        Me.fecha_transaccion.Width = 77
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Valor Transacción"
        Me.GridColumn1.DisplayFormat.FormatString = "c"
        Me.GridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn1.FieldName = "Valor"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 4
        Me.GridColumn1.Width = 82
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "GridColumn11"
        Me.GridColumn11.FieldName = "Debe"
        Me.GridColumn11.Name = "GridColumn11"
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "GridColumn12"
        Me.GridColumn12.FieldName = "Haber"
        Me.GridColumn12.Name = "GridColumn12"
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.dgTransac2)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(980, 351)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Transacciones Ptes. de Contabilizar"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'dgTransac2
        '
        Me.dgTransac2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgTransac2.Location = New System.Drawing.Point(24, 12)
        Me.dgTransac2.MainView = Me.gvTransacciones2
        Me.dgTransac2.Name = "dgTransac2"
        Me.dgTransac2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit2})
        Me.dgTransac2.Size = New System.Drawing.Size(932, 326)
        Me.dgTransac2.TabIndex = 42
        Me.dgTransac2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvTransacciones2})
        '
        'gvTransacciones2
        '
        Me.gvTransacciones2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn13, Me.GridColumn15, Me.GridColumn14})
        Me.gvTransacciones2.GridControl = Me.dgTransac2
        Me.gvTransacciones2.Name = "gvTransacciones2"
        Me.gvTransacciones2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Contabilizada?"
        Me.GridColumn2.FieldName = "ProcesadaBanco"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        Me.GridColumn2.Width = 69
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "IdTransaccion"
        Me.GridColumn3.FieldName = "IdTransaccion"
        Me.GridColumn3.Name = "GridColumn3"
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "No. Referencia"
        Me.GridColumn4.FieldName = "Referencia"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.OptionsColumn.AllowEdit = False
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 1
        Me.GridColumn4.Width = 102
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Concepto de la Transacción"
        Me.GridColumn5.FieldName = "Concepto"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.OptionsColumn.AllowEdit = False
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 2
        Me.GridColumn5.Width = 153
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Fecha"
        Me.GridColumn6.FieldName = "Fecha"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.OptionsColumn.AllowEdit = False
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 3
        Me.GridColumn6.Width = 77
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Valor Transacción"
        Me.GridColumn7.DisplayFormat.FormatString = "c"
        Me.GridColumn7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn7.FieldName = "Valor"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.OptionsColumn.AllowEdit = False
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 4
        Me.GridColumn7.Width = 82
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "GridColumn13"
        Me.GridColumn13.FieldName = "Debe"
        Me.GridColumn13.Name = "GridColumn13"
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "GridColumn14"
        Me.GridColumn14.FieldName = "Haber"
        Me.GridColumn14.Name = "GridColumn14"
        '
        'RepositoryItemCheckEdit2
        '
        Me.RepositoryItemCheckEdit2.AutoHeight = False
        Me.RepositoryItemCheckEdit2.Name = "RepositoryItemCheckEdit2"
        '
        'leCtaBancaria
        '
        Me.leCtaBancaria.Location = New System.Drawing.Point(126, 59)
        Me.leCtaBancaria.Name = "leCtaBancaria"
        Me.leCtaBancaria.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCtaBancaria.Size = New System.Drawing.Size(389, 20)
        Me.leCtaBancaria.TabIndex = 2
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(126, 36)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(389, 20)
        Me.leSucursal.TabIndex = 1
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(80, 39)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl7.TabIndex = 58
        Me.LabelControl7.Text = "Sucursal:"
        '
        'teChequesMarcados
        '
        Me.teChequesMarcados.Enabled = False
        Me.teChequesMarcados.EnterMoveNextControl = True
        Me.teChequesMarcados.Location = New System.Drawing.Point(880, 12)
        Me.teChequesMarcados.Name = "teChequesMarcados"
        Me.teChequesMarcados.Properties.Appearance.Options.UseTextOptions = True
        Me.teChequesMarcados.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teChequesMarcados.Properties.Mask.EditMask = "n2"
        Me.teChequesMarcados.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teChequesMarcados.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teChequesMarcados.Size = New System.Drawing.Size(135, 20)
        Me.teChequesMarcados.TabIndex = 59
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(752, 16)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(123, 13)
        Me.LabelControl6.TabIndex = 60
        Me.LabelControl6.Text = "Total  Cheques cobrados:"
        '
        'teChequesNoMarcados
        '
        Me.teChequesNoMarcados.Enabled = False
        Me.teChequesNoMarcados.EnterMoveNextControl = True
        Me.teChequesNoMarcados.Location = New System.Drawing.Point(880, 36)
        Me.teChequesNoMarcados.Name = "teChequesNoMarcados"
        Me.teChequesNoMarcados.Properties.Appearance.Options.UseTextOptions = True
        Me.teChequesNoMarcados.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teChequesNoMarcados.Properties.Mask.EditMask = "n2"
        Me.teChequesNoMarcados.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teChequesNoMarcados.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teChequesNoMarcados.Size = New System.Drawing.Size(135, 20)
        Me.teChequesNoMarcados.TabIndex = 61
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(739, 39)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(136, 13)
        Me.LabelControl2.TabIndex = 62
        Me.LabelControl2.Text = "Total Cheques No cobrados:"
        '
        'teTransaccionesNoMarcadas
        '
        Me.teTransaccionesNoMarcadas.Enabled = False
        Me.teTransaccionesNoMarcadas.EnterMoveNextControl = True
        Me.teTransaccionesNoMarcadas.Location = New System.Drawing.Point(880, 84)
        Me.teTransaccionesNoMarcadas.Name = "teTransaccionesNoMarcadas"
        Me.teTransaccionesNoMarcadas.Properties.Appearance.Options.UseTextOptions = True
        Me.teTransaccionesNoMarcadas.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTransaccionesNoMarcadas.Properties.Mask.EditMask = "n2"
        Me.teTransaccionesNoMarcadas.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTransaccionesNoMarcadas.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTransaccionesNoMarcadas.Size = New System.Drawing.Size(135, 20)
        Me.teTransaccionesNoMarcadas.TabIndex = 65
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(714, 88)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(164, 13)
        Me.LabelControl3.TabIndex = 66
        Me.LabelControl3.Text = "Total Transacciones No marcadas:"
        '
        'teTransaccionesMarcadas
        '
        Me.teTransaccionesMarcadas.Enabled = False
        Me.teTransaccionesMarcadas.EnterMoveNextControl = True
        Me.teTransaccionesMarcadas.Location = New System.Drawing.Point(880, 60)
        Me.teTransaccionesMarcadas.Name = "teTransaccionesMarcadas"
        Me.teTransaccionesMarcadas.Properties.Appearance.Options.UseTextOptions = True
        Me.teTransaccionesMarcadas.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTransaccionesMarcadas.Properties.Mask.EditMask = "n2"
        Me.teTransaccionesMarcadas.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTransaccionesMarcadas.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTransaccionesMarcadas.Size = New System.Drawing.Size(135, 20)
        Me.teTransaccionesMarcadas.TabIndex = 63
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(726, 65)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(151, 13)
        Me.LabelControl4.TabIndex = 64
        Me.LabelControl4.Text = "Total  Transacciones marcadas:"
        '
        'teChequesDelDia
        '
        Me.teChequesDelDia.Enabled = False
        Me.teChequesDelDia.EnterMoveNextControl = True
        Me.teChequesDelDia.Location = New System.Drawing.Point(565, 109)
        Me.teChequesDelDia.Name = "teChequesDelDia"
        Me.teChequesDelDia.Properties.Appearance.Options.UseTextOptions = True
        Me.teChequesDelDia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teChequesDelDia.Properties.Mask.EditMask = "n2"
        Me.teChequesDelDia.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teChequesDelDia.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teChequesDelDia.Size = New System.Drawing.Size(123, 20)
        Me.teChequesDelDia.TabIndex = 67
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(399, 113)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(162, 13)
        Me.LabelControl5.TabIndex = 68
        Me.LabelControl5.Text = "Total  Cheques marcados del dia :"
        '
        'teTransaccionesDelDia
        '
        Me.teTransaccionesDelDia.Enabled = False
        Me.teTransaccionesDelDia.EnterMoveNextControl = True
        Me.teTransaccionesDelDia.Location = New System.Drawing.Point(881, 110)
        Me.teTransaccionesDelDia.Name = "teTransaccionesDelDia"
        Me.teTransaccionesDelDia.Properties.Appearance.Options.UseTextOptions = True
        Me.teTransaccionesDelDia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTransaccionesDelDia.Properties.Mask.EditMask = "n2"
        Me.teTransaccionesDelDia.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTransaccionesDelDia.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTransaccionesDelDia.Size = New System.Drawing.Size(135, 20)
        Me.teTransaccionesDelDia.TabIndex = 69
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(696, 114)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(182, 13)
        Me.LabelControl8.TabIndex = 70
        Me.LabelControl8.Text = "Total Transacciones marcadas del dia:"
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "GridColumn15"
        Me.GridColumn15.FieldName = "IdDetalle"
        Me.GridColumn15.Name = "GridColumn15"
        '
        'ban_frmPrepararConciliacion
        '
        Me.ClientSize = New System.Drawing.Size(1040, 540)
        Me.Controls.Add(Me.teTransaccionesDelDia)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.teChequesDelDia)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.teTransaccionesNoMarcadas)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.teTransaccionesMarcadas)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.teChequesNoMarcados)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.teChequesMarcados)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.leSucursal)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.leCtaBancaria)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.chkMarca)
        Me.Controls.Add(Me.deFecha)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.cmdProcesar)
        Me.Controls.Add(Me.Label1)
        Me.Modulo = "Bancos"
        Me.Name = "ban_frmPrepararConciliacion"
        Me.OptionId = "002004"
        Me.Text = "Conciliar cheques y transacciones bancarias"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.cmdProcesar, 0)
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.deFecha, 0)
        Me.Controls.SetChildIndex(Me.chkMarca, 0)
        Me.Controls.SetChildIndex(Me.TabControl1, 0)
        Me.Controls.SetChildIndex(Me.leCtaBancaria, 0)
        Me.Controls.SetChildIndex(Me.LabelControl7, 0)
        Me.Controls.SetChildIndex(Me.leSucursal, 0)
        Me.Controls.SetChildIndex(Me.LabelControl6, 0)
        Me.Controls.SetChildIndex(Me.teChequesMarcados, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        Me.Controls.SetChildIndex(Me.teChequesNoMarcados, 0)
        Me.Controls.SetChildIndex(Me.LabelControl4, 0)
        Me.Controls.SetChildIndex(Me.teTransaccionesMarcadas, 0)
        Me.Controls.SetChildIndex(Me.LabelControl3, 0)
        Me.Controls.SetChildIndex(Me.teTransaccionesNoMarcadas, 0)
        Me.Controls.SetChildIndex(Me.LabelControl5, 0)
        Me.Controls.SetChildIndex(Me.teChequesDelDia, 0)
        Me.Controls.SetChildIndex(Me.LabelControl8, 0)
        Me.Controls.SetChildIndex(Me.teTransaccionesDelDia, 0)
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMarca.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.dgCheques, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvCheques, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.snConciliado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.dgTransac, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvTransacciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.dgTransac2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvTransacciones2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teChequesMarcados.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teChequesNoMarcados.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTransaccionesNoMarcadas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTransaccionesMarcadas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teChequesDelDia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTransaccionesDelDia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdProcesar As System.Windows.Forms.Button
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents chkMarca As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents dgCheques As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvCheques As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Conciliado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents snConciliado As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents IdCheque As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Numero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents anombre_de As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents fecha_cheque As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents valor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents dgTransac As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvTransacciones As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents procesada_banco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IdTransaccion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents num_referencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents concepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents fecha_transaccion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents Anombrede As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents fecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents procesadabanco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leCtaBancaria As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents dgTransac2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvTransacciones2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teChequesMarcados As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teChequesNoMarcados As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTransaccionesNoMarcadas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTransaccionesMarcadas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teChequesDelDia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTransaccionesDelDia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn

End Class
