﻿Imports NexusBLL

Public Class ban_frmLibroBancos
    Dim bl As New BancosBLL(g_ConnectionString)

    
    Private Sub frmLibroBancos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DateEdit1.DateTime = Today
        DateEdit2.DateTime = Today
        objCombos.banCuentasBancarias(leCtaBancaria)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub SimpleButton1_Click() Handles Me.Reporte
        Dim dt As New DataTable
        Dim IdCuenta As String = bl.GetCuentaCbleBanco(leCtaBancaria.EditValue)
        dt = bl.GetLibroBancos(DateEdit1.EditValue, DateEdit2.EditValue, IdCuenta, leSucursal.EditValue)

        Dim rpt As New ban_rptLibroBancos() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = txtTitulo.EditValue & ", " & leCtaBancaria.Text & ", SUCURSAL: " & leSucursal.Text
        rpt.xrlPeriodo.Text = (FechaToString(DateEdit1.DateTime, DateEdit2.DateTime)).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.ShowPreviewDialog()

    End Sub
    
End Class
