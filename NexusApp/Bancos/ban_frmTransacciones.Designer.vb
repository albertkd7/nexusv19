﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ban_frmTransacciones
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.LayoutConverter1 = New DevExpress.XtraLayout.Converter.LayoutConverter(Me.components)
        Me.xtcTransacciones = New DevExpress.XtraTab.XtraTabControl
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage
        Me.gc2 = New DevExpress.XtraGrid.GridControl
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcIdCuenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.riteIdCuenta = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.gcReferencia = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcConcepto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcDebe = New DevExpress.XtraGrid.Columns.GridColumn
        Me.teDebe = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.gcHaber = New DevExpress.XtraGrid.Columns.GridColumn
        Me.teHaber = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.gcIdCentro = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rileIdCentro = New DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit
        Me.RepositoryItemGridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.pcFooter = New DevExpress.XtraEditors.PanelControl
        Me.teDiferencia = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.teNombreCuenta = New DevExpress.XtraEditors.TextEdit
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl
        Me.cmdUP = New DevExpress.XtraEditors.SimpleButton
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton
        Me.cmdDel = New DevExpress.XtraEditors.SimpleButton
        Me.gcHeader = New DevExpress.XtraEditors.GroupControl
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.leCtaBancaria = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.teValor = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.leTipoPartida = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.deFecha = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.deFechaContable = New DevExpress.XtraEditors.DateEdit
        Me.teNumeroPartida = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.meConcepto = New DevExpress.XtraEditors.MemoEdit
        Me.ceContabilizar = New DevExpress.XtraEditors.CheckEdit
        Me.ceIncluirConciliacion = New DevExpress.XtraEditors.CheckEdit
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.leTipoTransaccion = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl
        Me.teIdTransac = New DevExpress.XtraEditors.TextEdit
        Me.teIdPartida = New DevExpress.XtraEditors.TextEdit
        Me.sbImportar = New DevExpress.XtraEditors.SimpleButton
        CType(Me.xtcTransacciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcTransacciones.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteIdCuenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDebe, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teHaber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rileIdCentro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemGridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcFooter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcFooter.SuspendLayout()
        CType(Me.teDiferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombreCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcHeader.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teValor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaContable.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaContable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumeroPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceContabilizar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceIncluirConciliacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoTransaccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdTransac.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcTransacciones
        '
        Me.xtcTransacciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcTransacciones.Location = New System.Drawing.Point(0, 0)
        Me.xtcTransacciones.Name = "xtcTransacciones"
        Me.xtcTransacciones.SelectedTabPage = Me.xtpLista
        Me.xtcTransacciones.Size = New System.Drawing.Size(1034, 407)
        Me.xtcTransacciones.TabIndex = 4
        Me.xtcTransacciones.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc2)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(1028, 381)
        Me.xtpLista.Text = "Consulta de Transacciones"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(1028, 381)
        Me.gc2.TabIndex = 3
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn11, Me.GridColumn5, Me.GridColumn7, Me.GridColumn9, Me.GridColumn10, Me.GridColumn6})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdTransaccion"
        Me.GridColumn1.FieldName = "IdTransaccion"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 47
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Tipo Partida"
        Me.GridColumn2.FieldName = "IdTipoPartida"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 54
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Numero Partida"
        Me.GridColumn3.FieldName = "NumeroPartida"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 68
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Numero Cuenta"
        Me.GridColumn4.FieldName = "NumeroCuenta"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 64
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Sucursal"
        Me.GridColumn11.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn11.FieldName = "IdSucursal"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 4
        Me.GridColumn11.Width = 60
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Fecha"
        Me.GridColumn5.FieldName = "Fecha"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 5
        Me.GridColumn5.Width = 61
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Concepto"
        Me.GridColumn7.FieldName = "Concepto"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 6
        Me.GridColumn7.Width = 103
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Creado Por"
        Me.GridColumn9.FieldName = "CreadoPor"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 8
        Me.GridColumn9.Width = 68
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "FechaHoraCreación"
        Me.GridColumn10.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.GridColumn10.FieldName = "FechaHoraCreacion"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 9
        Me.GridColumn10.Width = 76
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        Me.RepositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Valor"
        Me.GridColumn6.DisplayFormat.FormatString = "n2"
        Me.GridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn6.FieldName = "Valor"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 7
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.gc)
        Me.xtpDatos.Controls.Add(Me.pcFooter)
        Me.xtpDatos.Controls.Add(Me.pcBotones)
        Me.xtpDatos.Controls.Add(Me.gcHeader)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(1028, 381)
        Me.xtpDatos.Text = "Transacciones Bancarias"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 112)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteIdCuenta, Me.teDebe, Me.teHaber, Me.rileIdCentro})
        Me.gc.Size = New System.Drawing.Size(989, 240)
        Me.gc.TabIndex = 5
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Appearance.TopNewRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.gv.Appearance.TopNewRow.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.gv.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Black
        Me.gv.Appearance.TopNewRow.Options.UseBackColor = True
        Me.gv.Appearance.TopNewRow.Options.UseForeColor = True
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdCuenta, Me.gcReferencia, Me.gcConcepto, Me.gcDebe, Me.gcHaber, Me.gcIdCentro})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.NewItemRowText = "Click aquí para agregar nuevo registro. <Esc> para cancelar"
        Me.gv.OptionsNavigation.AutoFocusNewRow = True
        Me.gv.OptionsNavigation.EnterMoveNextColumn = True
        Me.gv.OptionsSelection.MultiSelect = True
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        Me.gv.ViewCaption = "Elmer Rodriguez"
        '
        'gcIdCuenta
        '
        Me.gcIdCuenta.Caption = "Cód. Cuenta"
        Me.gcIdCuenta.ColumnEdit = Me.riteIdCuenta
        Me.gcIdCuenta.FieldName = "IdCuenta"
        Me.gcIdCuenta.Name = "gcIdCuenta"
        Me.gcIdCuenta.Visible = True
        Me.gcIdCuenta.VisibleIndex = 0
        Me.gcIdCuenta.Width = 108
        '
        'riteIdCuenta
        '
        Me.riteIdCuenta.AutoHeight = False
        Me.riteIdCuenta.Name = "riteIdCuenta"
        '
        'gcReferencia
        '
        Me.gcReferencia.Caption = "Referencia"
        Me.gcReferencia.FieldName = "Referencia"
        Me.gcReferencia.Name = "gcReferencia"
        Me.gcReferencia.Visible = True
        Me.gcReferencia.VisibleIndex = 1
        Me.gcReferencia.Width = 86
        '
        'gcConcepto
        '
        Me.gcConcepto.Caption = "Concepto de la Transacción"
        Me.gcConcepto.FieldName = "Concepto"
        Me.gcConcepto.Name = "gcConcepto"
        Me.gcConcepto.Visible = True
        Me.gcConcepto.VisibleIndex = 2
        Me.gcConcepto.Width = 379
        '
        'gcDebe
        '
        Me.gcDebe.Caption = "Debe"
        Me.gcDebe.ColumnEdit = Me.teDebe
        Me.gcDebe.FieldName = "Debe"
        Me.gcDebe.Name = "gcDebe"
        Me.gcDebe.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Debe", "{0:n2}")})
        Me.gcDebe.Visible = True
        Me.gcDebe.VisibleIndex = 3
        Me.gcDebe.Width = 91
        '
        'teDebe
        '
        Me.teDebe.Appearance.Options.UseTextOptions = True
        Me.teDebe.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teDebe.AutoHeight = False
        Me.teDebe.Mask.EditMask = "n"
        Me.teDebe.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teDebe.Mask.UseMaskAsDisplayFormat = True
        Me.teDebe.Name = "teDebe"
        '
        'gcHaber
        '
        Me.gcHaber.Caption = "Haber"
        Me.gcHaber.ColumnEdit = Me.teHaber
        Me.gcHaber.FieldName = "Haber"
        Me.gcHaber.Name = "gcHaber"
        Me.gcHaber.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Haber", "{0:n2}")})
        Me.gcHaber.Visible = True
        Me.gcHaber.VisibleIndex = 4
        Me.gcHaber.Width = 85
        '
        'teHaber
        '
        Me.teHaber.Appearance.Options.UseTextOptions = True
        Me.teHaber.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teHaber.AutoHeight = False
        Me.teHaber.Mask.EditMask = "n"
        Me.teHaber.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teHaber.Mask.UseMaskAsDisplayFormat = True
        Me.teHaber.Name = "teHaber"
        '
        'gcIdCentro
        '
        Me.gcIdCentro.Caption = "Centro de Costo"
        Me.gcIdCentro.ColumnEdit = Me.rileIdCentro
        Me.gcIdCentro.FieldName = "IdCentro"
        Me.gcIdCentro.Name = "gcIdCentro"
        Me.gcIdCentro.Visible = True
        Me.gcIdCentro.VisibleIndex = 5
        Me.gcIdCentro.Width = 152
        '
        'rileIdCentro
        '
        Me.rileIdCentro.AutoHeight = False
        Me.rileIdCentro.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rileIdCentro.Name = "rileIdCentro"
        Me.rileIdCentro.View = Me.RepositoryItemGridLookUpEdit1View
        '
        'RepositoryItemGridLookUpEdit1View
        '
        Me.RepositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.RepositoryItemGridLookUpEdit1View.Name = "RepositoryItemGridLookUpEdit1View"
        Me.RepositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.RepositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'pcFooter
        '
        Me.pcFooter.Controls.Add(Me.teDiferencia)
        Me.pcFooter.Controls.Add(Me.LabelControl7)
        Me.pcFooter.Controls.Add(Me.teNombreCuenta)
        Me.pcFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pcFooter.Location = New System.Drawing.Point(0, 352)
        Me.pcFooter.Name = "pcFooter"
        Me.pcFooter.Size = New System.Drawing.Size(989, 29)
        Me.pcFooter.TabIndex = 7
        '
        'teDiferencia
        '
        Me.teDiferencia.EditValue = "0"
        Me.teDiferencia.Enabled = False
        Me.teDiferencia.Location = New System.Drawing.Point(851, 5)
        Me.teDiferencia.Name = "teDiferencia"
        Me.teDiferencia.Properties.Appearance.Options.UseTextOptions = True
        Me.teDiferencia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teDiferencia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teDiferencia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teDiferencia.Properties.Mask.EditMask = "n2"
        Me.teDiferencia.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teDiferencia.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teDiferencia.Properties.ReadOnly = True
        Me.teDiferencia.Size = New System.Drawing.Size(84, 20)
        Me.teDiferencia.TabIndex = 1
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(789, 8)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl7.TabIndex = 19
        Me.LabelControl7.Text = "Diferencia:"
        '
        'teNombreCuenta
        '
        Me.teNombreCuenta.Location = New System.Drawing.Point(11, 5)
        Me.teNombreCuenta.Name = "teNombreCuenta"
        Me.teNombreCuenta.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.teNombreCuenta.Properties.Appearance.ForeColor = System.Drawing.Color.DarkRed
        Me.teNombreCuenta.Properties.Appearance.Options.UseFont = True
        Me.teNombreCuenta.Properties.Appearance.Options.UseForeColor = True
        Me.teNombreCuenta.Size = New System.Drawing.Size(738, 22)
        Me.teNombreCuenta.TabIndex = 0
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdUP)
        Me.pcBotones.Controls.Add(Me.cmdDown)
        Me.pcBotones.Controls.Add(Me.cmdDel)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(989, 112)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(39, 269)
        Me.pcBotones.TabIndex = 6
        '
        'cmdUP
        '
        Me.cmdUP.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUP.Location = New System.Drawing.Point(5, 6)
        Me.cmdUP.Name = "cmdUP"
        Me.cmdUP.Size = New System.Drawing.Size(28, 24)
        Me.cmdUP.TabIndex = 0
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(5, 36)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(28, 24)
        Me.cmdDown.TabIndex = 1
        '
        'cmdDel
        '
        Me.cmdDel.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdDel.Location = New System.Drawing.Point(5, 66)
        Me.cmdDel.Name = "cmdDel"
        Me.cmdDel.Size = New System.Drawing.Size(28, 29)
        Me.cmdDel.TabIndex = 2
        '
        'gcHeader
        '
        Me.gcHeader.CaptionLocation = DevExpress.Utils.Locations.Top
        Me.gcHeader.Controls.Add(Me.leSucursal)
        Me.gcHeader.Controls.Add(Me.LabelControl12)
        Me.gcHeader.Controls.Add(Me.LabelControl1)
        Me.gcHeader.Controls.Add(Me.leCtaBancaria)
        Me.gcHeader.Controls.Add(Me.LabelControl2)
        Me.gcHeader.Controls.Add(Me.teValor)
        Me.gcHeader.Controls.Add(Me.LabelControl3)
        Me.gcHeader.Controls.Add(Me.leTipoPartida)
        Me.gcHeader.Controls.Add(Me.LabelControl4)
        Me.gcHeader.Controls.Add(Me.deFecha)
        Me.gcHeader.Controls.Add(Me.LabelControl5)
        Me.gcHeader.Controls.Add(Me.deFechaContable)
        Me.gcHeader.Controls.Add(Me.teNumeroPartida)
        Me.gcHeader.Controls.Add(Me.LabelControl6)
        Me.gcHeader.Controls.Add(Me.meConcepto)
        Me.gcHeader.Controls.Add(Me.ceContabilizar)
        Me.gcHeader.Controls.Add(Me.ceIncluirConciliacion)
        Me.gcHeader.Controls.Add(Me.LabelControl8)
        Me.gcHeader.Controls.Add(Me.leTipoTransaccion)
        Me.gcHeader.Controls.Add(Me.LabelControl11)
        Me.gcHeader.Controls.Add(Me.LabelControl10)
        Me.gcHeader.Controls.Add(Me.LabelControl9)
        Me.gcHeader.Controls.Add(Me.teIdTransac)
        Me.gcHeader.Controls.Add(Me.teIdPartida)
        Me.gcHeader.Controls.Add(Me.sbImportar)
        Me.gcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.gcHeader.Location = New System.Drawing.Point(0, 0)
        Me.gcHeader.Name = "gcHeader"
        Me.gcHeader.ShowCaption = False
        Me.gcHeader.Size = New System.Drawing.Size(1028, 112)
        Me.gcHeader.TabIndex = 4
        Me.gcHeader.Text = "Root"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(113, 68)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(450, 20)
        Me.leSucursal.TabIndex = 5
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(67, 71)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl12.TabIndex = 60
        Me.LabelControl12.Text = "Sucursal:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(77, 6)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Banco:"
        '
        'leCtaBancaria
        '
        Me.leCtaBancaria.EnterMoveNextControl = True
        Me.leCtaBancaria.Location = New System.Drawing.Point(113, 3)
        Me.leCtaBancaria.Name = "leCtaBancaria"
        Me.leCtaBancaria.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCtaBancaria.Size = New System.Drawing.Size(450, 20)
        Me.leCtaBancaria.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(618, 50)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "Monto:"
        '
        'teValor
        '
        Me.teValor.EnterMoveNextControl = True
        Me.teValor.Location = New System.Drawing.Point(655, 46)
        Me.teValor.Name = "teValor"
        Me.teValor.Properties.Appearance.Options.UseTextOptions = True
        Me.teValor.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teValor.Properties.Mask.EditMask = "n"
        Me.teValor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teValor.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teValor.Size = New System.Drawing.Size(92, 20)
        Me.teValor.TabIndex = 4
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(576, 70)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl3.TabIndex = 12
        Me.LabelControl3.Text = "Tipo de Partida:"
        '
        'leTipoPartida
        '
        Me.leTipoPartida.EnterMoveNextControl = True
        Me.leTipoPartida.Location = New System.Drawing.Point(655, 67)
        Me.leTipoPartida.Name = "leTipoPartida"
        Me.leTipoPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPartida.Size = New System.Drawing.Size(228, 20)
        Me.leTipoPartida.TabIndex = 6
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(578, 7)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(74, 13)
        Me.LabelControl4.TabIndex = 13
        Me.LabelControl4.Text = "Fecha s/Banco:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(655, 4)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFecha.Size = New System.Drawing.Size(92, 20)
        Me.deFecha.TabIndex = 1
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(573, 27)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl5.TabIndex = 14
        Me.LabelControl5.Text = "Fecha Contable:"
        '
        'deFechaContable
        '
        Me.deFechaContable.EditValue = Nothing
        Me.deFechaContable.EnterMoveNextControl = True
        Me.deFechaContable.Location = New System.Drawing.Point(655, 25)
        Me.deFechaContable.Name = "deFechaContable"
        Me.deFechaContable.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaContable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaContable.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFechaContable.Size = New System.Drawing.Size(92, 20)
        Me.deFechaContable.TabIndex = 3
        '
        'teNumeroPartida
        '
        Me.teNumeroPartida.EnterMoveNextControl = True
        Me.teNumeroPartida.Location = New System.Drawing.Point(655, 88)
        Me.teNumeroPartida.Name = "teNumeroPartida"
        Me.teNumeroPartida.Size = New System.Drawing.Size(92, 20)
        Me.teNumeroPartida.TabIndex = 8
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(60, 28)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl6.TabIndex = 16
        Me.LabelControl6.Text = "Concepto:"
        '
        'meConcepto
        '
        Me.meConcepto.EnterMoveNextControl = True
        Me.meConcepto.Location = New System.Drawing.Point(113, 25)
        Me.meConcepto.Name = "meConcepto"
        Me.meConcepto.Size = New System.Drawing.Size(450, 41)
        Me.meConcepto.TabIndex = 2
        '
        'ceContabilizar
        '
        Me.ceContabilizar.Location = New System.Drawing.Point(753, 8)
        Me.ceContabilizar.Name = "ceContabilizar"
        Me.ceContabilizar.Properties.Caption = "Contabilizar"
        Me.ceContabilizar.Size = New System.Drawing.Size(88, 19)
        Me.ceContabilizar.TabIndex = 8
        '
        'ceIncluirConciliacion
        '
        Me.ceIncluirConciliacion.Location = New System.Drawing.Point(753, 33)
        Me.ceIncluirConciliacion.Name = "ceIncluirConciliacion"
        Me.ceIncluirConciliacion.Properties.Caption = "Incluir en conciliación"
        Me.ceIncluirConciliacion.Size = New System.Drawing.Size(134, 19)
        Me.ceIncluirConciliacion.TabIndex = 9
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(11, 92)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(99, 13)
        Me.LabelControl8.TabIndex = 21
        Me.LabelControl8.Text = "Tipo de Transacción:"
        '
        'leTipoTransaccion
        '
        Me.leTipoTransaccion.EnterMoveNextControl = True
        Me.leTipoTransaccion.Location = New System.Drawing.Point(113, 89)
        Me.leTipoTransaccion.Name = "leTipoTransaccion"
        Me.leTipoTransaccion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoTransaccion.Size = New System.Drawing.Size(450, 20)
        Me.leTipoTransaccion.TabIndex = 7
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(872, 91)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl11.TabIndex = 22
        Me.LabelControl11.Text = "Id.Transacción:"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(895, 71)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl10.TabIndex = 22
        Me.LabelControl10.Text = "Id.Partida:"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(579, 91)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl9.TabIndex = 22
        Me.LabelControl9.Text = "No. de Partida:"
        '
        'teIdTransac
        '
        Me.teIdTransac.Enabled = False
        Me.teIdTransac.Location = New System.Drawing.Point(950, 88)
        Me.teIdTransac.Name = "teIdTransac"
        Me.teIdTransac.Properties.AllowFocused = False
        Me.teIdTransac.Properties.ReadOnly = True
        Me.teIdTransac.Size = New System.Drawing.Size(74, 20)
        Me.teIdTransac.TabIndex = 12
        '
        'teIdPartida
        '
        Me.teIdPartida.Enabled = False
        Me.teIdPartida.Location = New System.Drawing.Point(950, 67)
        Me.teIdPartida.Name = "teIdPartida"
        Me.teIdPartida.Properties.AllowFocused = False
        Me.teIdPartida.Properties.ReadOnly = True
        Me.teIdPartida.Size = New System.Drawing.Size(74, 20)
        Me.teIdPartida.TabIndex = 11
        '
        'sbImportar
        '
        Me.sbImportar.AllowFocus = False
        Me.sbImportar.Location = New System.Drawing.Point(755, 87)
        Me.sbImportar.Name = "sbImportar"
        Me.sbImportar.Size = New System.Drawing.Size(92, 22)
        Me.sbImportar.TabIndex = 9
        Me.sbImportar.Text = "Importar de CSV"
        '
        'ban_frmTransacciones
        '
        Me.ClientSize = New System.Drawing.Size(1034, 432)
        Me.Controls.Add(Me.xtcTransacciones)
        Me.Modulo = "Bancos"
        Me.Name = "ban_frmTransacciones"
        Me.OptionId = "002002"
        Me.Text = "Transacciones bancarias"
        Me.Controls.SetChildIndex(Me.xtcTransacciones, 0)
        CType(Me.xtcTransacciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcTransacciones.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteIdCuenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDebe, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teHaber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rileIdCentro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemGridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcFooter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcFooter.ResumeLayout(False)
        Me.pcFooter.PerformLayout()
        CType(Me.teDiferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombreCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcHeader.ResumeLayout(False)
        Me.gcHeader.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teValor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaContable.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaContable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumeroPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceContabilizar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceIncluirConciliacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoTransaccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdTransac.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutConverter1 As DevExpress.XtraLayout.Converter.LayoutConverter
    Friend WithEvents xtcTransacciones As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Private WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteIdCuenta As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcReferencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDebe As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teDebe As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcHaber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teHaber As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcIdCentro As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rileIdCentro As DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit
    Friend WithEvents RepositoryItemGridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents pcFooter As DevExpress.XtraEditors.PanelControl
    Friend WithEvents teDiferencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombreCuenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdUP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCtaBancaria As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teValor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaContable As DevExpress.XtraEditors.DateEdit
    Friend WithEvents teNumeroPartida As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents ceContabilizar As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceIncluirConciliacion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoTransaccion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdTransac As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teIdPartida As DevExpress.XtraEditors.TextEdit
    Friend WithEvents sbImportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn

End Class
