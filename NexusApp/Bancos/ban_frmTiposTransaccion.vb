﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class ban_frmTiposTransaccion
    Dim entidad As ban_TiposTransaccion

    Private Sub ban_frmTiposTransaccion_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub ban_frmTiposTransaccion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.conTiposPartida(leTipoPartida)
        objCombos.conTiposPartida(riteTipoPartida)
        objCombos.Ban_CargoAbono(leTipoTransaccion)
        objCombos.Ban_CargoAbono(riteTipoTransaccion)

        gc.DataSource = objTablas.ban_TiposTransaccionSelectAll
        entidad = objTablas.ban_TiposTransaccionSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipo"))

        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub cban_frmTiposTransaccion_Nuevo_Click() Handles Me.Nuevo
        entidad = New ban_TiposTransaccion
        entidad.IdTipo = objFunciones.ObtenerUltimoId("BAN_TIPOSTRANSACCION", "IdTipo") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub ban_frmTiposTransaccion_Save_Click() Handles Me.Guardar
        If teNombre.EditValue = "" Or SiEsNulo(leTipoTransaccion.EditValue, 0) = 0 Or SiEsNulo(leTipoPartida.EditValue, "") = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Tipo de Partida, Nombre, Tipo Aplicación]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.ban_TiposTransaccionInsert(entidad)
        Else
            objTablas.ban_TiposTransaccionUpdate(entidad)
        End If
        gc.DataSource = objTablas.ban_TiposTransaccionSelectAll
        entidad = objTablas.ban_TiposTransaccionSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipo"))
        CargaPantalla()
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub ban_frmTiposTransaccion_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el tipo de transacción seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.ban_TiposTransaccionDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.ban_TiposTransaccionSelectAll
                entidad = objTablas.ban_TiposTransaccionSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipo"))
                CargaPantalla()
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL REGISTRO:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub ban_frmTiposTransaccion_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entidad
            teId.EditValue = .IdTipo
            teNombre.EditValue = .Nombre
            leTipoPartida.EditValue = .IdTipoPartida
            leTipoTransaccion.EditValue = .TipoTransaccion
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entidad
            .IdTipo = teId.EditValue
            .Nombre = teNombre.EditValue
            .IdTipoPartida = leTipoPartida.EditValue
            .TipoTransaccion = leTipoTransaccion.EditValue
        End With
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entidad = objTablas.ban_TiposTransaccionSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipo"))
        CargaPantalla()
    End Sub

    Private Sub cban_frmTiposTransaccion_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teId.Properties.ReadOnly = True
        teId.Focus()
    End Sub

End Class
