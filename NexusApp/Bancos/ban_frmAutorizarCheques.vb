﻿Imports NexusBLL
Imports DevExpress.XtraGrid.Views.Grid
Public Class ban_frmAutorizarCheques
    Dim bl As New BancosBLL(g_ConnectionString)
    Dim dt, dttrans As DataTable


    Private Sub frmLoad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        deFecha.DateTime = Today()
        objCombos.banCuentasBancarias(leCtaBancaria)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        leSucursal.EditValue = piIdSucursalUsuario
    End Sub



    Private Sub frmConciliacionCheques_Update_Click() Handles Me.Guardar
        For i = 0 To gvCheques.RowCount - 1
            If gvCheques.GetRowCellValue(i, "Impreso") Then
                bl.AutorizaCheques(gvCheques.GetRowCellValue(i, "IdCheque"), objMenu.User)
            End If
        Next

        dt = bl.GetChequesAutoriza(leCtaBancaria.EditValue, deFecha.DateTime, leSucursal.EditValue)
        dgCheques.DataSource = dt
    End Sub


    Private Sub chkMarca_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMarca.CheckedChanged
        Dim flag As Boolean
        If chkMarca.Checked Then
            flag = True
        Else
            flag = False
        End If

        Dim vista As GridView = dgCheques.FocusedView
        For i = 0 To vista.RowCount - 1
            vista.FocusedRowHandle = i
            vista.FocusedColumn = vista.Columns("Impreso")
            vista.SetFocusedValue(flag)
        Next

    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        dt = bl.GetChequesAutoriza(leCtaBancaria.EditValue, deFecha.DateTime, leSucursal.EditValue)
        dgCheques.DataSource = dt
    End Sub
    Private Sub snConciliado_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles snConciliado.CheckedChanged
        If gvCheques.FocusedColumn.FieldName = "Impreso" Then
            gvCheques.SetFocusedRowCellValue("Impreso", gvCheques.EditingValue)
        End If
    End Sub
End Class
