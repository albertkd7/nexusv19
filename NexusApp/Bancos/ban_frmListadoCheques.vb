﻿Imports NexusBLL

Public Class ban_frmListadoCheques
    Dim bl As New BancosBLL(g_ConnectionString)


    Private Sub frmListadoCheques_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.DateTime = Today
        deHasta.DateTime = Today
        objCombos.banCuentasBancarias(leCtaBanco, "-- TODAS LAS CUENTAS BANCARIAS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub frmListadoCheques_Report_Click() Handles Me.Reporte
        Dim rpt As New ban_rptListadoCheques
        Dim IdCta As Integer = leCtaBanco.EditValue
        rpt.DataSource = bl.GetListadoCheques(deDesde.EditValue, deHasta.EditValue, IdCta, leSucursal.EditValue)
        rpt.DataMember = ""
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Listado de cheques - " & leCtaBanco.Text & " - SUCURSAL: " & leSucursal.Text
        rpt.xrlPeriodo.Text = FechaToString(deDesde.DateTime, deHasta.DateTime)
        rpt.ShowPreview()
    End Sub


End Class
