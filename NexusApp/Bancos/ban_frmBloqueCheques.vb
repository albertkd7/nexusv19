﻿Imports NexusBLL

Public Class ban_frmBloqueCheques
    Dim bl As New BancosBLL(g_ConnectionString)


    Private Sub ban_frmBloqueCheques_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        seDesde.EditValue = 0
        seHasta.EditValue = 0
        objCombos.banCuentasBancarias(leCtaBancaria)

    End Sub


    Private Sub btGeneraImpresion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGeneraImpresion.Click
        If MsgBox("Está seguro(a) de generar los cheques por bloques?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim dt As DataTable = bl.GetBloqueCheques(seDesde.EditValue, seHasta.EditValue, leCtaBancaria.EditValue)

        Dim iNum_Formato = bl.GetIdFormatoCheque(leCtaBancaria.EditValue)
        Dim rpt As New ban_rptCheque01
        'Dim dir As String = Application.StartupPath
        'Dim Template = Application.StartupPath & "\Plantillas\cheque" & iNum_Formato.ToString.PadLeft(2, "0") & ".repx"

        'If Not FileIO.FileSystem.FileExists(Template) Then
        '    MsgBox("No existe la plantilla necesaria para imprimir el cheque", MsgBoxStyle.Critical, "Nota")
        '    Exit Sub
        'End If

        rpt.DataSource = dt
        rpt.DataMember = ""

        rpt.ShowPreviewDialog()

    End Sub
End Class
