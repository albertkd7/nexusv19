﻿Public Class ban_rptCheque01


    Private Sub xrlLugarFecha_BeforePrint_1(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlLugarFecha.BeforePrint
        If GetCurrentColumnValue("IdFormatoVaucher") = 1 Or GetCurrentColumnValue("IdFormatoVaucher") = 5 Then 'AGRICOLA Y AMERICA CENTRAL
            Dim LugarFecha As String = GetCurrentColumnValue("Domicilio") & ", " & CDate(GetCurrentColumnValue("Fecha")).Day.ToString() & " de "
            LugarFecha += ObtieneMesString(CDate(GetCurrentColumnValue("Fecha")).Month)
            xrlLugarFecha.Text = LugarFecha

        Else
            If GetCurrentColumnValue("IdFormatoVaucher") = 4 Then 'ScotiaBank
                xrlLugarFecha.Text = GetCurrentColumnValue("Domicilio")
                xrlDia.Text = CDate(GetCurrentColumnValue("Fecha")).Day.ToString()
                xrlMes.Text = ObtieneMesString(CDate(GetCurrentColumnValue("Fecha")).Month)
                xrlAnio.Text = CDate(GetCurrentColumnValue("Fecha")).Year.ToString()
            Else 'Cheque 2, 3, 6, 7, 8 y 9
                xrlLugarFecha.Text = String.Format("{0}, {1}", GetCurrentColumnValue("Domicilio"), FechaToString(GetCurrentColumnValue("Fecha"), GetCurrentColumnValue("Fecha")))
            End If
        End If
    End Sub

    Private Sub xrlValorCheque_BeforePrint_1(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlValorCheque.BeforePrint
        Dim Valor As String = "********************" + Format(GetCurrentColumnValue("Valor"), "###,###,##0.00")
        xrlValorCheque.Text = Valor.Substring(Valor.Length - 15, 15)
    End Sub

    Private Sub xrlCantidad_BeforePrint_1(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlCantidad.BeforePrint
        Dim sDecimal As String = ""
        sDecimal = String.Format("{0:c}", GetCurrentColumnValue("Valor"))
        sDecimal = sDecimal.Substring(sDecimal.Length - 2) & "/100"
        xrlCantidad.Text = String.Format("{0} {1}", Num2Text(Int(GetCurrentColumnValue("Valor"))), sDecimal)
    End Sub
End Class