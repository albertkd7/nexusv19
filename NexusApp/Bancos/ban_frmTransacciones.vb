﻿Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns
Imports System.IO
Imports NexusBLL


Public Class ban_frmTransacciones
    Dim bl As New BancosBLL(g_ConnectionString)
    Dim blConta As New ContabilidadBLL(g_ConnectionString), blAdmon As New AdmonBLL(g_ConnectionString)
    Dim Header As ban_Transacciones
    Dim Detalle As List(Of ban_TransaccionesDetalle)
    Dim bFlagDetalleAgregado As Boolean
    Dim sConcepto = "", entCuenta As con_Cuentas, dtParam As DataTable = blAdmon.ObtieneParametros(), entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim FechaConsulta As String = String.Format("{0:yyyyMMdd}", dtParam.Rows(0).Item("FechaCierre"))

    Private Sub ban_frmTransacciones_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        gc2.DataSource = bl.ConsultaTransacciones(objMenu.User, FechaConsulta).Tables(0)
        gv2.BestFitColumns()
    End Sub

    Private Sub ban_frmTransacciones_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        CargarCombos()
        teIdTransac.EditValue = objFunciones.ObtenerUltimoId("BAN_TRANSACCIONES", "IdTransaccion")
        CargaControles(0)
        ActivarControles(False)
        gv.BestFitColumns()

    End Sub
    Private Sub ban_frmTransacciones_Nuevo() Handles Me.Nuevo
        ActivarControles(True)
        LimpiarControles(True)

        ceContabilizar.EditValue = True
        ceIncluirConciliacion.EditValue = True
        leCtaBancaria.EditValue = 1
        leTipoTransaccion.EditValue = 1
        Dim entTipo As ban_TiposTransaccion = objTablas.ban_TiposTransaccionSelectByPK(leTipoTransaccion.EditValue)
        leTipoPartida.EditValue = entTipo.IdTipoPartida
        leSucursal.EditValue = piIdSucursalUsuario
        xtcTransacciones.SelectedTabPage = xtpDatos
    End Sub
    Private Sub ban_frmTransacciones_Guardar() Handles Me.Guardar
        If Not DatosValidos() Then
            Exit Sub
        End If

        CargaEntidades()
        Dim Contabilizar As Boolean = ceContabilizar.EditValue '  gsNombre_Empresa.StartsWith("PITUTA")
        Dim msj As String = ""
        If DbMode = DbModeType.insert Then
            msj = bl.InsertarTransaccion(Header, Detalle, Contabilizar)
            If msj = "Ok" Then
                MsgBox("La transacción ha sido registrada con éxito", 64, "Nota")
                teNumeroPartida.EditValue = Header.NumeroPartida
            Else
                MsgBox(String.Format("No fue posible guardar la transacción{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
                Exit Sub
            End If
        Else
            msj = bl.ActualizarTransaccion(Header, Detalle, Contabilizar)
            If msj = "Ok" Then
                MsgBox("La transacción ha sido actualizada con éxito", MsgBoxStyle.Information, "Nota")
                teNumeroPartida.EditValue = Header.NumeroPartida
            Else
                MsgBox("NO FUE POSIBLE ACTUALIZAR LA TRANSACCIÓN" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If
        bFlagDetalleAgregado = False
        teIdPartida.EditValue = Header.IdPartida
        teIdTransac.EditValue = Header.IdTransaccion

        xtcTransacciones.SelectedTabPage = xtpLista
        gc2.DataSource = bl.ConsultaTransacciones(objMenu.User, FechaConsulta).Tables(0)
        gc2.Focus()

        MostrarModoInicial()
        ActivarControles(False)
    End Sub
    Private Sub ban_frmTransacciones_Edit_Click() Handles Me.Editar
        ActivarControles(True)
        leCtaBancaria.Focus()
    End Sub
    Private Sub ban_frmTransacciones_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro?", MsgBoxStyle.YesNo + MsgBoxStyle.Information) = MsgBoxResult.Yes Then
            Dim EsOk As Boolean = ValidarFechaCierre(gv2.GetRowCellValue(gv2.FocusedRowHandle, "Fecha"))
            If Not EsOk Then
                MsgBox("Fecha del documento corresponde a un período ya cerrado", MsgBoxStyle.Critical)
                Return
            End If

            Header = objTablas.ban_TransaccionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdTransaccion"))
            Header.ModificadoPor = objMenu.User
            Header.FechaHoraModificacion = Now
            objTablas.ban_TransaccionesUpdate(Header)

            objTablas.ban_TransaccionesDeleteByPK(Header.IdTransaccion)
            objTablas.con_PartidasDeleteByPK(Header.IdPartida)
            objFunciones.ActualizaAbono("TRANSACCIONES", Header.IdTransaccion) ' ACTIVO EL ABONO PARA QUE LO BORREN Y LO VUELVAN A HACER
            teIdTransac.EditValue = objFunciones.ObtenerUltimoId("BAN_TRANSACCIONES", "IdTransaccion") + 1
            gc2.DataSource = bl.ConsultaTransacciones(objMenu.User, FechaConsulta).Tables(0)
            CargaControles(0)
        End If
    End Sub
    Private Sub ban_frmTransacciones_Report_Click() Handles Me.Reporte
        Dim dt As DataTable
        Header = objTablas.ban_TransaccionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdTransaccion"))
        If MsgBox("¿Imprimir en formato de partida?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            dt = blConta.rptPartidaReporteByPk(Header.IdPartida)
            Dim rpt As New con_rptPartida() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = "Partida contable"
            rpt.xrlDescMoneda.Text = FechaToString(Header.Fecha, Header.Fecha)
            rpt.ShowPreviewDialog()
        Else
            dt = bl.ObtieneTransaccionByPk(Header.IdTransaccion)
            Dim entTipoTra As ban_TiposTransaccion = objTablas.ban_TiposTransaccionSelectByPK(Header.IdTipo)
            Dim rpt As New ban_rptTransaccion() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.XrLTipoTransacion.Text = leTipoTransaccion.Text
            rpt.XrLValor.Text = teValor.Text
            rpt.xrlTitulo.Text = String.Format("TRANSACCION BANCARIA - {0} / {1}", leCtaBancaria.Text, entTipoTra.Nombre)
            rpt.xrlPeriodo.Text = ""
            rpt.ShowPreviewDialog()
        End If
    End Sub
    Private Sub ban_frmTransacciones_Query_Click() Handles Me.Consulta
        'teIdTransac.EditValue = objConsultas.ConsultaTransacciones(frmConsultaDetalle, Fecha)
        'CargaControles(0)
    End Sub
    Private Sub ban_frmTransacciones_Undo_Click() Handles Me.Revertir
        xtcTransacciones.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub
    
    Private Sub LimpiarControles(ByVal Value As Boolean)
        'leCtaBancaria.ItemIndex = 0
        'leCtaBancaria.DoValidate()
        gv.CancelUpdateCurrentRow()
        If Value Then
            deFecha.EditValue = Today
            deFechaContable.EditValue = Today
            meConcepto.EditValue = ""
            teNumeroPartida.EditValue = ""
            teValor.EditValue = 0.0
            If DbMode = DbModeType.insert Then
                gc.DataSource = bl.GetTransaccionDetalle(-1)
            End If
        Else
            gc.DataSource = Nothing
        End If
        Header = New ban_Transacciones
        Detalle = New List(Of ban_TransaccionesDetalle)


        leCtaBancaria.Focus()
    End Sub
    Private Sub CargarCombos()
        objCombos.banCuentasBancarias(leCtaBancaria)
        objCombos.conTiposPartida(leTipoPartida)
        objCombos.banTiposTransaccion(leTipoTransaccion)
        objCombos.con_CentrosCosto(rileIdCentro, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
    End Sub
    Private Sub CargaEntidades()
        With Header
            .IdCuentaBancaria = leCtaBancaria.EditValue
            .Fecha = deFecha.EditValue
            .FechaContable = deFechaContable.EditValue
            .Valor = teValor.EditValue
            .IdTipo = leTipoTransaccion.EditValue
            .IdSucursal = leSucursal.EditValue
            .Concepto = meConcepto.Text
            '.IdPartida = 0  'El Id partida se llena en la capa de datos
            '.NumeroPartida = ""   'el numero de partida se llena en la capa de datos
            .IdTipoPartida = leTipoPartida.EditValue
            .Contabilizar = ceContabilizar.EditValue
            .IncluirConciliacion = ceIncluirConciliacion.EditValue
            .ProcesadaBanco = True
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .FechaHoraModificacion = Nothing
                .ModificadoPor = ""
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If
        End With

        Detalle = New List(Of ban_TransaccionesDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New ban_TransaccionesDetalle
            With entDetalle
                .IdTransaccion = Header.IdTransaccion
                .IdDetalle = i + 1
                .IdCuenta = gv.GetRowCellValue(i, "IdCuenta")
                .Referencia = gv.GetRowCellValue(i, "Referencia")
                .Concepto = gv.GetRowCellValue(i, "Concepto")
                .Debe = gv.GetRowCellValue(i, "Debe")
                .Haber = gv.GetRowCellValue(i, "Haber")
                .IdCentro = gv.GetRowCellValue(i, "IdCentro")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            Detalle.Add(entDetalle)
        Next
    End Sub
    Public Sub CargaControles(ByVal TipoAvance As Integer)
        'If TipoAvance = 0 Then
        'Else
        '    teIdTransac.EditValue = bl.ObtieneIdTransac(teIdTransac.EditValue, TipoAvance)
        'End If
        'If teIdTransac.EditValue = 0 Then
        '    teIdTransac.EditValue = 1
        'End If
        'Header = objTablas.ban_TransaccionesSelectByPK(teIdTransac.EditValue)
        'If Header.IdTransaccion = 0 Then
        '    Exit Sub
        'End If
        'With Header
        '    leCtaBancaria.EditValue = .IdCuentaBancaria
        '    ceContabilizar.EditValue = .Contabilizar
        '    ceIncluirConciliacion.EditValue = .IncluirConciliacion
        '    meConcepto.EditValue = .Concepto
        '    deFecha.EditValue = .Fecha
        '    deFechaContable.EditValue = .FechaContable
        '    teValor.EditValue = .Valor
        '    teIdPartida.EditValue = .IdPartida
        '    leTipoPartida.EditValue = .IdTipoPartida
        '    teNumeroPartida.EditValue = .NumeroPartida
        '    leTipoTransaccion.EditValue = .IdTipo
        '    leSucursal.EditValue = .IdSucursal
        '    gc.DataSource = bl.GetTransaccionDetalle(.IdTransaccion)

        '    Dim entCuentaBancaria As New ban_CuentasBancarias
        '    entCuentaBancaria = objTablas.ban_CuentasBancariasSelectByPK(.IdCuentaBancaria)
        'End With
    End Sub
#Region "Grid"

    Private Sub gv_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gv.FocusedRowChanged
        Dim entCuentas As con_Cuentas
        entCuentas = objTablas.con_CuentasSelectByPK(gv.GetFocusedRowCellValue("IdCuenta"))
        teNombreCuenta.EditValue = entCuentas.Nombre
    End Sub

    'Private Sub gv_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.GotFocus
    '    SendKeys.SendWait("{Enter}")
    'End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        'gv.SetRowCellValue(gc.NewItemRowHandle, "IdCuenta", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", sConcepto)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Debe", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Haber", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", "")
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        Dim IdCuenta As String = SiEsNulo(gv.GetFocusedRowCellValue("IdCuenta"), "")
        'Dim IdCentro As String = SiEsNulo(gv.GetFocusedRowCellValue("IdCentro"), "")
        If IdCuenta = "" Then
            MsgBox("Debe de especificar la cuenta contable", MsgBoxStyle.Critical, "Nota")
            e.Valid = False
            Exit Sub
        End If
        If gv.GetRowCellValue(gv.FocusedRowHandle, "Debe") <> 0 And gv.GetRowCellValue(gv.FocusedRowHandle, "Haber") <> 0 Then
            MsgBox("Solo debe colocar valores en debe o haber no en ambos", 16, "Error")
            e.Valid = False
        End If
        'If IdCentro = "" Then
        '    MsgBox("Es necesario especificar el centro de costo", MsgBoxStyle.Critical, "Nota")
        '    e.Valid = False
        '    Exit Sub
        'End If
        'Dim i As Integer = blConta.con_ValidaCentroCuenta(IdCuenta, IdCentro)

        'If i = 1 Then
        '    MsgBox("Es necesario definir el centro de costo a ésta partida", MsgBoxStyle.Information, "Nota")
        '    e.Valid = False
        '    Exit Sub
        'End If
        'If i = 2 Then
        '    MsgBox("El centro de costo especificado no existe o debe de especificarlo para ésta cuenta", MsgBoxStyle.Exclamation, "Nota")
        '    e.Valid = False
        '    Exit Sub
        'End If
        'If i = 3 Then
        '    MsgBox("El centro de costo especificado está mal asociado a la cuenta contable", MsgBoxStyle.Exclamation, "Nota")
        '    Exit Sub
        'End If
        sConcepto = gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto")
        gv.UpdateTotalSummary()
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        teDiferencia.EditValue = gcDebe.SummaryItem.SummaryValue - gcHaber.SummaryItem.SummaryValue
    End Sub

    Private Sub riteIdCuenta_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles riteIdCuenta.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            gv.EditingValue = SiEsNulo(gv.EditingValue, "")

            entCuenta = objConsultas.cnsCuentas(con_frmConsultaCuentas, gv.EditingValue)
            gv.EditingValue = entCuenta.IdCuenta

            If entCuenta.IdCuenta = "" Then
                Exit Sub
            End If

            If Not entCuenta.EsTransaccional And entCuenta.IdCuenta <> "" Then
                MsgBox("La cuenta no permite aceptar transacciones" & Chr(13) & _
                       "Es de mayor o tiene subcuentas", 64, "Nota")
            End If
            teNombreCuenta.EditValue = entCuenta.Nombre

            If gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto") = "" Then
                sConcepto = entCuenta.Nombre
            End If
            gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", sConcepto)
            'End If
        End If
    End Sub
    'Private Sub riteIdCuenta_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles riteIdCuenta.Leave
    '    If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCuenta"), "") = "" Then
    '        Exit Sub
    '    End If
    '    objCombos.con_CargarCentrosCostoCuentaMayor(rileIdCentro, gv.GetRowCellValue(gv.FocusedRowHandle, "IdCuenta"))
    'End Sub
    Private Sub teNumPartida_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles teNumeroPartida.LostFocus
        If Not gv.IsNewItemRow(gv.FocusedRowHandle) Then 'revisar este proceso, se debería de hacer en el gv
            'gv.AddNewRow()
            'gv.FocusedColumn = gv.Columns(0)
            gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
        End If

    End Sub
    Private Sub cmdUP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUP.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then

            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDel.Click
        gv.DeleteSelectedRows()
    End Sub
#End Region
    Private Function DatosValidos()
        If meConcepto.EditValue = "" Then
            MsgBox("Existen datos que no pueden quedar vacíos" & Chr(13) & _
                   "Imposible guardar la transacción", 16, "Nota")
            Return False
        End If
        If teDiferencia.EditValue <> 0.0 Then
            If dtParam.Rows(0).Item("GuardarDesbalance") Then
                If MsgBox("La partida no está cuadrada" & Chr(13) & _
                      "Está seguro(a) de continuar?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
                    Return False
                End If
            Else
                MsgBox("La partida no está cuadrada", MsgBoxStyle.Critical, "Imposible salir")
                Return False
            End If
        End If

        For i = 0 To gv.DataRowCount - 1
            Dim entCta As con_Cuentas = objTablas.con_CuentasSelectByPK(gv.GetRowCellValue(i, "IdCuenta"))
            If entCta.EsTransaccional = 0 Then
                MsgBox("La cuenta: " & entCta.IdCuenta & ", tiene sub-cuentas" & Chr(13) & "Solo puede usar cuentas de detalle", 16, "Error de datos")
                Return False
            End If
        Next

        If leSucursal.EditValue = 0 Or leSucursal.EditValue = Nothing Or leSucursal.Text = "" Then
            MsgBox("Debe seleccionar una sucursal", 64, "Nota")
            Return False
        End If

        If SiEsNulo(leTipoPartida.EditValue, "") = "" Or SiEsNulo(leTipoPartida.Text, "") = "" Then
            MsgBox("Debe seleccionar una tipo de partida", 64, "Nota")
            Return False
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(deFechaContable.EditValue)
        If Not EsOk Then
            MsgBox("Fecha de la transacción corresponde a un período ya cerrado", 64, "Nota")
            Return False
        End If

        Return True
    End Function
    Private Sub teValor_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles teValor.KeyDown
        If e.KeyCode = Keys.Enter And DbMode = DbModeType.insert And Not bFlagDetalleAgregado Then
            Dim sConcepto = meConcepto.EditValue
            Dim sIdCuenta = bl.GetCuentaCbleBanco(leCtaBancaria.EditValue)
            Dim entTipo As ban_TiposTransaccion = objTablas.ban_TiposTransaccionSelectByPK(leTipoTransaccion.EditValue)

            gv.AddNewRow()
            gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", sIdCuenta)
            gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", "")
            gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", sConcepto)
            If entTipo.TipoTransaccion = 1 Then  'ESTE ES UN DEBE
                gv.SetRowCellValue(gv.FocusedRowHandle, "Debe", teValor.EditValue)
                gv.SetRowCellValue(gv.FocusedRowHandle, "Haber", 0.0)
            Else
                gv.SetRowCellValue(gv.FocusedRowHandle, "Debe", 0.0)
                gv.SetRowCellValue(gv.FocusedRowHandle, "Haber", teValor.EditValue)
            End If
            gv.UpdateCurrentRow()
            bFlagDetalleAgregado = True
        End If
    End Sub
    Private Sub leTipoTransaccion_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leTipoTransaccion.EditValueChanged
        Dim entTipo As ban_TiposTransaccion = objTablas.ban_TiposTransaccionSelectByPK(leTipoTransaccion.EditValue)
        leTipoPartida.EditValue = entTipo.IdTipoPartida
    End Sub
    
    Private Sub teIdPartida_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles teIdPartida.Enter
        teIdPartida.Enabled = True
    End Sub
    Private Sub sbImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbImportar.Click

        If Not gsNombre_Empresa.ToUpper.StartsWith("TRANS EXPRESS") Then
            Dim ofd As New OpenFileDialog()
            ofd.ShowDialog()
            If ofd.FileName = "" Then
                Return
            End If
            Dim csv_file As StreamReader = File.OpenText(ofd.FileName)
            Try
                While csv_file.Peek() > 0
                    Dim sLine As String = csv_file.ReadLine()
                    Dim aData As Array = sLine.Split(",")
                    Dim i As Integer = blConta.pro_ValidaCentroCuenta(aData(0), aData(5))
                    'If i > 0 Then
                    '    ValidaCuentaCentroCosto(i, aData(0), aData(5))
                    'Else
                    gv.AddNewRow()
                    gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", aData(0))
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", aData(1))
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", aData(2))
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Debe", SiEsNulo(aData(3), 0))
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Haber", SiEsNulo(aData(4), 0))
                    gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", SiEsNulo(aData(5), ""))
                    gv.UpdateCurrentRow()
                    'End If
                End While
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                Return
            End Try

            csv_file.Close()
        Else
            Dim ofd As New OpenFileDialog()
            ofd.ShowDialog()
            If ofd.FileName = "" Then
                Return
            End If
            Dim Archivo As System.IO.StreamReader = File.OpenText(ofd.FileName)
            Dim separador As String = InputBox("Separador:", "Defina el delimitador del archivo", "|")
            If separador = "" Then
                separador = "|"
            End If
            Try
                While Archivo.Peek() > 0
                    Dim sLine As String = Archivo.ReadLine()
                    Dim aData As Array = sLine.Split(separador)
                    Dim Fech As String = aData(2)

                    If aData(0) <> "" Then
                        deFecha.EditValue = CDate(Fech)
                        deFechaContable.EditValue = CDate(Fech)

                        gv.AddNewRow()
                        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", aData(0))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", aData(1))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", aData(3))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Debe", SiEsNulo(aData(4), 0))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Haber", SiEsNulo(aData(5), 0))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", "")
                        gv.UpdateCurrentRow()
                    End If
                End While
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                Return
            End Try
            Archivo.Close()
        End If

        'Dim ofd As New OpenFileDialog()
        'ofd.ShowDialog()
        'If ofd.FileName = "" Then
        '    Return
        'End If
        'Dim csv_file As System.IO.StreamReader = File.OpenText(ofd.FileName)
        'Try
        '    While csv_file.Peek() > 0
        '        Dim sLine As String = csv_file.ReadLine()
        '        Dim aData As Array = sLine.Split(",")
        '        gv.AddNewRow()
        '        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", aData(0))
        '        gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", aData(1))
        '        gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", aData(2))
        '        gv.SetRowCellValue(gv.FocusedRowHandle, "Debe", SiEsNulo(aData(3), 0))
        '        gv.SetRowCellValue(gv.FocusedRowHandle, "Haber", SiEsNulo(aData(4), 0))
        '        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", SiEsNulo(aData(4), 0))
        '        gv.UpdateCurrentRow()
        '    End While
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        '    Return
        'End Try

        'csv_file.Close()
    End Sub

    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In gcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next

        'If Tipo = True Then
        '    objCombos.banCuentasBancarias(leCtaBancaria, "NuevoEditar")
        'Else
        '    objCombos.banCuentasBancarias(leCtaBancaria)
        'End If
        gv.OptionsBehavior.Editable = Tipo

    End Sub
    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        Header = objTablas.ban_TransaccionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdTransaccion"))
        CargaPantalla()
        DbMode = DbModeType.update

    End Sub
    Private Sub CargaPantalla()
        xtcTransacciones.SelectedTabPage = xtpDatos


        With Header
            leCtaBancaria.EditValue = .IdCuentaBancaria
            ceContabilizar.EditValue = .Contabilizar
            ceIncluirConciliacion.EditValue = .IncluirConciliacion
            meConcepto.EditValue = .Concepto
            deFecha.EditValue = .Fecha
            deFechaContable.EditValue = .FechaContable
            teValor.EditValue = .Valor
            teIdPartida.EditValue = .IdPartida
            leTipoPartida.EditValue = .IdTipoPartida
            teNumeroPartida.EditValue = .NumeroPartida
            leTipoTransaccion.EditValue = .IdTipo
            leSucursal.EditValue = .IdSucursal
            gc.DataSource = bl.GetTransaccionDetalle(.IdTransaccion)

            Dim entCuentaBancaria As New ban_CuentasBancarias
            entCuentaBancaria = objTablas.ban_CuentasBancariasSelectByPK(.IdCuentaBancaria)
        End With

    End Sub
End Class
