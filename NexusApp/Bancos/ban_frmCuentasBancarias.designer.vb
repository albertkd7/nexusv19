﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ban_frmCuentasBancarias
    Inherits gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.pc1 = New DevExpress.XtraEditors.PanelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteSucursal = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.pc2 = New DevExpress.XtraEditors.PanelControl()
        Me.teCodEmpresa = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoBanco = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.ceCuentaInactiva = New DevExpress.XtraEditors.CheckEdit()
        Me.BeCtaContable1 = New Nexus.beCtaContable()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoImpresion = New DevExpress.XtraEditors.LookUpEdit()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.teCargo1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.tePerEmite = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoPartida = New DevExpress.XtraEditors.LookUpEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.teCargo2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.tePerRevisa = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.teCargo3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.tePerAutoriza = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.seFormatoCheque = New DevExpress.XtraEditors.SpinEdit()
        Me.teBanco = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.seUltCheque = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumeroCuenta = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.BeCtaContable2 = New Nexus.beCtaContable()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pc1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pc1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteSucursal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pc2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pc2.SuspendLayout()
        CType(Me.teCodEmpresa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceCuentaInactiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoImpresion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.teCargo1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePerEmite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.teCargo2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePerRevisa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.teCargo3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePerAutoriza.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seFormatoCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seUltCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumeroCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"Uno", "Dos", "Tres"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'pc1
        '
        Me.pc1.Controls.Add(Me.gc)
        Me.pc1.Dock = System.Windows.Forms.DockStyle.Left
        Me.pc1.Location = New System.Drawing.Point(0, 0)
        Me.pc1.Name = "pc1"
        Me.pc1.Size = New System.Drawing.Size(489, 713)
        Me.pc1.TabIndex = 67
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(2, 2)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteSucursal})
        Me.gc.Size = New System.Drawing.Size(485, 709)
        Me.gc.TabIndex = 1
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn5})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Id Cuenta"
        Me.GridColumn1.FieldName = "IdCuentaBancaria"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 54
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombre"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 212
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Número Cuenta"
        Me.GridColumn3.FieldName = "NumeroCuenta"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 66
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Cuenta Contable"
        Me.GridColumn5.FieldName = "IdCuentaContable"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 3
        Me.GridColumn5.Width = 69
        '
        'riteSucursal
        '
        Me.riteSucursal.AutoHeight = False
        Me.riteSucursal.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteSucursal.Name = "riteSucursal"
        '
        'pc2
        '
        Me.pc2.Controls.Add(Me.LabelControl16)
        Me.pc2.Controls.Add(Me.BeCtaContable2)
        Me.pc2.Controls.Add(Me.teCodEmpresa)
        Me.pc2.Controls.Add(Me.LabelControl15)
        Me.pc2.Controls.Add(Me.leTipoBanco)
        Me.pc2.Controls.Add(Me.LabelControl14)
        Me.pc2.Controls.Add(Me.ceCuentaInactiva)
        Me.pc2.Controls.Add(Me.BeCtaContable1)
        Me.pc2.Controls.Add(Me.LabelControl13)
        Me.pc2.Controls.Add(Me.leTipoImpresion)
        Me.pc2.Controls.Add(Me.GroupControl1)
        Me.pc2.Controls.Add(Me.leTipoPartida)
        Me.pc2.Controls.Add(Me.GroupControl2)
        Me.pc2.Controls.Add(Me.LabelControl6)
        Me.pc2.Controls.Add(Me.GroupControl3)
        Me.pc2.Controls.Add(Me.seFormatoCheque)
        Me.pc2.Controls.Add(Me.teBanco)
        Me.pc2.Controls.Add(Me.LabelControl5)
        Me.pc2.Controls.Add(Me.LabelControl1)
        Me.pc2.Controls.Add(Me.seUltCheque)
        Me.pc2.Controls.Add(Me.LabelControl2)
        Me.pc2.Controls.Add(Me.teNumeroCuenta)
        Me.pc2.Controls.Add(Me.LabelControl4)
        Me.pc2.Controls.Add(Me.LabelControl3)
        Me.pc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pc2.Location = New System.Drawing.Point(489, 0)
        Me.pc2.Name = "pc2"
        Me.pc2.Size = New System.Drawing.Size(865, 713)
        Me.pc2.TabIndex = 68
        '
        'teCodEmpresa
        '
        Me.teCodEmpresa.Location = New System.Drawing.Point(512, 167)
        Me.teCodEmpresa.Name = "teCodEmpresa"
        Me.teCodEmpresa.Size = New System.Drawing.Size(101, 20)
        Me.teCodEmpresa.TabIndex = 88
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(403, 170)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl15.TabIndex = 87
        Me.LabelControl15.Text = "Cód. Empresa Planilla:"
        '
        'leTipoBanco
        '
        Me.leTipoBanco.Location = New System.Drawing.Point(186, 167)
        Me.leTipoBanco.Name = "leTipoBanco"
        Me.leTipoBanco.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoBanco.Size = New System.Drawing.Size(201, 20)
        Me.leTipoBanco.TabIndex = 85
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(115, 170)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl14.TabIndex = 86
        Me.LabelControl14.Text = "Banco Planilla:"
        '
        'ceCuentaInactiva
        '
        Me.ceCuentaInactiva.Location = New System.Drawing.Point(184, 191)
        Me.ceCuentaInactiva.Name = "ceCuentaInactiva"
        Me.ceCuentaInactiva.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ceCuentaInactiva.Properties.Appearance.Options.UseFont = True
        Me.ceCuentaInactiva.Properties.Caption = "CUENTA INACTIVA"
        Me.ceCuentaInactiva.Size = New System.Drawing.Size(146, 19)
        Me.ceCuentaInactiva.TabIndex = 84
        '
        'BeCtaContable1
        '
        Me.BeCtaContable1.Location = New System.Drawing.Point(183, 66)
        Me.BeCtaContable1.Name = "BeCtaContable1"
        Me.BeCtaContable1.Size = New System.Drawing.Size(599, 26)
        Me.BeCtaContable1.TabIndex = 71
        Me.BeCtaContable1.ValidarMayor = False
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(420, 148)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl13.TabIndex = 83
        Me.LabelControl13.Text = "Tipo de Impresión:"
        '
        'leTipoImpresion
        '
        Me.leTipoImpresion.Location = New System.Drawing.Point(512, 145)
        Me.leTipoImpresion.Name = "leTipoImpresion"
        Me.leTipoImpresion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoImpresion.Size = New System.Drawing.Size(215, 20)
        Me.leTipoImpresion.TabIndex = 76
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.teCargo1)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.tePerEmite)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Location = New System.Drawing.Point(14, 218)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(711, 53)
        Me.GroupControl1.TabIndex = 80
        Me.GroupControl1.Text = "Persona que emite el Cheque"
        '
        'teCargo1
        '
        Me.teCargo1.Location = New System.Drawing.Point(434, 24)
        Me.teCargo1.Name = "teCargo1"
        Me.teCargo1.Size = New System.Drawing.Size(266, 20)
        Me.teCargo1.TabIndex = 1
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(398, 27)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl8.TabIndex = 2
        Me.LabelControl8.Text = "Cargo:"
        '
        'tePerEmite
        '
        Me.tePerEmite.Location = New System.Drawing.Point(81, 24)
        Me.tePerEmite.Name = "tePerEmite"
        Me.tePerEmite.Size = New System.Drawing.Size(269, 20)
        Me.tePerEmite.TabIndex = 0
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(37, 27)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl7.TabIndex = 0
        Me.LabelControl7.Text = "Nombre:"
        '
        'leTipoPartida
        '
        Me.leTipoPartida.Location = New System.Drawing.Point(186, 145)
        Me.leTipoPartida.Name = "leTipoPartida"
        Me.leTipoPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPartida.Size = New System.Drawing.Size(201, 20)
        Me.leTipoPartida.TabIndex = 75
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.teCargo2)
        Me.GroupControl2.Controls.Add(Me.LabelControl10)
        Me.GroupControl2.Controls.Add(Me.tePerRevisa)
        Me.GroupControl2.Controls.Add(Me.LabelControl9)
        Me.GroupControl2.Location = New System.Drawing.Point(14, 282)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(711, 53)
        Me.GroupControl2.TabIndex = 81
        Me.GroupControl2.Text = "Persona que revisa el cheque"
        '
        'teCargo2
        '
        Me.teCargo2.Location = New System.Drawing.Point(434, 24)
        Me.teCargo2.Name = "teCargo2"
        Me.teCargo2.Size = New System.Drawing.Size(266, 20)
        Me.teCargo2.TabIndex = 1
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(398, 27)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl10.TabIndex = 2
        Me.LabelControl10.Text = "Cargo:"
        '
        'tePerRevisa
        '
        Me.tePerRevisa.Location = New System.Drawing.Point(81, 24)
        Me.tePerRevisa.Name = "tePerRevisa"
        Me.tePerRevisa.Size = New System.Drawing.Size(269, 20)
        Me.tePerRevisa.TabIndex = 0
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(37, 28)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl9.TabIndex = 0
        Me.LabelControl9.Text = "Nombre:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(3, 148)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(180, 13)
        Me.LabelControl6.TabIndex = 79
        Me.LabelControl6.Text = "Tipo de Partida a Utilizar en Cheques:"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.teCargo3)
        Me.GroupControl3.Controls.Add(Me.LabelControl12)
        Me.GroupControl3.Controls.Add(Me.tePerAutoriza)
        Me.GroupControl3.Controls.Add(Me.LabelControl11)
        Me.GroupControl3.Location = New System.Drawing.Point(14, 348)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(711, 53)
        Me.GroupControl3.TabIndex = 82
        Me.GroupControl3.Text = "Persona que Aurotiza el Cheque"
        '
        'teCargo3
        '
        Me.teCargo3.Location = New System.Drawing.Point(434, 21)
        Me.teCargo3.Name = "teCargo3"
        Me.teCargo3.Size = New System.Drawing.Size(266, 20)
        Me.teCargo3.TabIndex = 1
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(402, 24)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl12.TabIndex = 2
        Me.LabelControl12.Text = "Cargo"
        '
        'tePerAutoriza
        '
        Me.tePerAutoriza.Location = New System.Drawing.Point(81, 21)
        Me.tePerAutoriza.Name = "tePerAutoriza"
        Me.tePerAutoriza.Size = New System.Drawing.Size(269, 20)
        Me.tePerAutoriza.TabIndex = 0
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(37, 25)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl11.TabIndex = 0
        Me.LabelControl11.Text = "Nombre:"
        '
        'seFormatoCheque
        '
        Me.seFormatoCheque.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seFormatoCheque.Location = New System.Drawing.Point(512, 124)
        Me.seFormatoCheque.Name = "seFormatoCheque"
        Me.seFormatoCheque.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seFormatoCheque.Size = New System.Drawing.Size(64, 20)
        Me.seFormatoCheque.TabIndex = 74
        '
        'teBanco
        '
        Me.teBanco.Location = New System.Drawing.Point(186, 22)
        Me.teBanco.Name = "teBanco"
        Me.teBanco.Size = New System.Drawing.Size(531, 20)
        Me.teBanco.TabIndex = 68
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(410, 127)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(99, 13)
        Me.LabelControl5.TabIndex = 78
        Me.LabelControl5.Text = "Formato de Cheque:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(11, 25)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(172, 13)
        Me.LabelControl1.TabIndex = 67
        Me.LabelControl1.Text = "Banco donde se apertura la cuenta:"
        '
        'seUltCheque
        '
        Me.seUltCheque.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seUltCheque.Location = New System.Drawing.Point(186, 124)
        Me.seUltCheque.Name = "seUltCheque"
        Me.seUltCheque.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seUltCheque.Size = New System.Drawing.Size(64, 20)
        Me.seUltCheque.TabIndex = 72
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(89, 47)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl2.TabIndex = 70
        Me.LabelControl2.Text = "Número de Cuenta:"
        '
        'teNumeroCuenta
        '
        Me.teNumeroCuenta.Location = New System.Drawing.Point(186, 44)
        Me.teNumeroCuenta.Name = "teNumeroCuenta"
        Me.teNumeroCuenta.Size = New System.Drawing.Size(201, 20)
        Me.teNumeroCuenta.TabIndex = 69
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(85, 127)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(98, 13)
        Me.LabelControl4.TabIndex = 77
        Me.LabelControl4.Text = "Ult. Cheque Emitido:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(98, 70)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl3.TabIndex = 73
        Me.LabelControl3.Text = "Cuenta Contable:"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(76, 91)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(108, 13)
        Me.LabelControl16.TabIndex = 92
        Me.LabelControl16.Text = "Cuenta Contable POS:"
        '
        'BeCtaContable2
        '
        Me.BeCtaContable2.Location = New System.Drawing.Point(184, 88)
        Me.BeCtaContable2.Name = "BeCtaContable2"
        Me.BeCtaContable2.Size = New System.Drawing.Size(599, 26)
        Me.BeCtaContable2.TabIndex = 91
        Me.BeCtaContable2.ValidarMayor = False
        '
        'ban_frmCuentasBancarias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1354, 738)
        Me.Controls.Add(Me.pc2)
        Me.Controls.Add(Me.pc1)
        Me.Modulo = "Bancos"
        Me.Name = "ban_frmCuentasBancarias"
        Me.OptionId = "001002"
        Me.Text = "Cuentas Bancarias"
        Me.Controls.SetChildIndex(Me.pc1, 0)
        Me.Controls.SetChildIndex(Me.pc2, 0)
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pc1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pc1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteSucursal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pc2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pc2.ResumeLayout(False)
        Me.pc2.PerformLayout()
        CType(Me.teCodEmpresa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceCuentaInactiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoImpresion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.teCargo1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePerEmite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.teCargo2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePerRevisa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.teCargo3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePerAutoriza.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seFormatoCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seUltCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumeroCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents pc1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pc2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents teCodEmpresa As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoBanco As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceCuentaInactiva As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents BeCtaContable1 As Nexus.beCtaContable
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoImpresion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents teCargo1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tePerEmite As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents teCargo2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tePerRevisa As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents teCargo3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tePerAutoriza As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seFormatoCheque As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents teBanco As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seUltCheque As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumeroCuenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteSucursal As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BeCtaContable2 As beCtaContable

End Class
