﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ban_frmAutorizarCheques
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.leCtaBancaria = New DevExpress.XtraEditors.LookUpEdit()
        Me.chkMarca = New DevExpress.XtraEditors.CheckEdit()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.dgCheques = New DevExpress.XtraGrid.GridControl()
        Me.gvCheques = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Conciliado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.snConciliado = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.IdCheque = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Numero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Anombrede = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.fecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.valor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMarca.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgCheques, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvCheques, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.snConciliado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.SimpleButton1)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.leCtaBancaria)
        Me.GroupControl1.Controls.Add(Me.chkMarca)
        Me.GroupControl1.Controls.Add(Me.deFecha)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1021, 104)
        Me.GroupControl1.TabIndex = 60
        Me.GroupControl1.Text = "Parámetros"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(521, 72)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(90, 23)
        Me.SimpleButton1.TabIndex = 66
        Me.SimpleButton1.Text = "Obtener Datos"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(121, 53)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(389, 20)
        Me.leSucursal.TabIndex = 60
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(74, 56)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl7.TabIndex = 65
        Me.LabelControl7.Text = "Sucursal:"
        '
        'leCtaBancaria
        '
        Me.leCtaBancaria.Location = New System.Drawing.Point(121, 76)
        Me.leCtaBancaria.Name = "leCtaBancaria"
        Me.leCtaBancaria.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCtaBancaria.Size = New System.Drawing.Size(389, 20)
        Me.leCtaBancaria.TabIndex = 61
        '
        'chkMarca
        '
        Me.chkMarca.Location = New System.Drawing.Point(520, 49)
        Me.chkMarca.Name = "chkMarca"
        Me.chkMarca.Properties.Caption = "Autorizar Todos los registros"
        Me.chkMarca.Size = New System.Drawing.Size(171, 19)
        Me.chkMarca.TabIndex = 64
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.Location = New System.Drawing.Point(121, 29)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 59
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(54, 32)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl1.TabIndex = 63
        Me.LabelControl1.Text = "Hasta Fecha:"
        '
        'dgCheques
        '
        Me.dgCheques.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgCheques.Location = New System.Drawing.Point(0, 104)
        Me.dgCheques.MainView = Me.gvCheques
        Me.dgCheques.Name = "dgCheques"
        Me.dgCheques.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.snConciliado})
        Me.dgCheques.Size = New System.Drawing.Size(1021, 340)
        Me.dgCheques.TabIndex = 61
        Me.dgCheques.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvCheques})
        '
        'gvCheques
        '
        Me.gvCheques.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Conciliado, Me.IdCheque, Me.Numero, Me.Anombrede, Me.GridColumn1, Me.fecha, Me.valor, Me.GridColumn2, Me.GridColumn3})
        Me.gvCheques.GridControl = Me.dgCheques
        Me.gvCheques.Name = "gvCheques"
        Me.gvCheques.OptionsView.ShowGroupPanel = False
        '
        'Conciliado
        '
        Me.Conciliado.Caption = "Autorizar"
        Me.Conciliado.ColumnEdit = Me.snConciliado
        Me.Conciliado.FieldName = "Impreso"
        Me.Conciliado.Name = "Conciliado"
        Me.Conciliado.Visible = True
        Me.Conciliado.VisibleIndex = 0
        Me.Conciliado.Width = 88
        '
        'snConciliado
        '
        Me.snConciliado.AutoHeight = False
        Me.snConciliado.Name = "snConciliado"
        '
        'IdCheque
        '
        Me.IdCheque.Caption = "id cheque"
        Me.IdCheque.FieldName = "IdCheque"
        Me.IdCheque.Name = "IdCheque"
        '
        'Numero
        '
        Me.Numero.Caption = "Núm. de Cheque"
        Me.Numero.FieldName = "Numero"
        Me.Numero.Name = "Numero"
        Me.Numero.OptionsColumn.AllowEdit = False
        Me.Numero.Visible = True
        Me.Numero.VisibleIndex = 1
        Me.Numero.Width = 81
        '
        'Anombrede
        '
        Me.Anombrede.Caption = "A Nombre De"
        Me.Anombrede.FieldName = "AnombreDe"
        Me.Anombrede.Name = "Anombrede"
        Me.Anombrede.OptionsColumn.AllowEdit = False
        Me.Anombrede.Visible = True
        Me.Anombrede.VisibleIndex = 2
        Me.Anombrede.Width = 279
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Descripción"
        Me.GridColumn1.FieldName = "Concepto"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 3
        Me.GridColumn1.Width = 161
        '
        'fecha
        '
        Me.fecha.Caption = "Fecha de emisión"
        Me.fecha.FieldName = "Fecha"
        Me.fecha.Name = "fecha"
        Me.fecha.OptionsColumn.AllowEdit = False
        Me.fecha.Visible = True
        Me.fecha.VisibleIndex = 4
        Me.fecha.Width = 113
        '
        'valor
        '
        Me.valor.Caption = "Valor del Cheque"
        Me.valor.DisplayFormat.FormatString = "c"
        Me.valor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.valor.FieldName = "Valor"
        Me.valor.Name = "valor"
        Me.valor.OptionsColumn.AllowEdit = False
        Me.valor.Visible = True
        Me.valor.VisibleIndex = 5
        Me.valor.Width = 96
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Creado Por"
        Me.GridColumn2.FieldName = "CreadoPor"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 6
        Me.GridColumn2.Width = 89
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Fecha Hora Creación"
        Me.GridColumn3.FieldName = "FechaHoraCreacion"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.AllowEdit = False
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 7
        Me.GridColumn3.Width = 96
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(35, 80)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl2.TabIndex = 65
        Me.LabelControl2.Text = "Cuenta Bancaria:"
        '
        'ban_frmAutorizarCheques
        '
        Me.ClientSize = New System.Drawing.Size(1021, 472)
        Me.Controls.Add(Me.dgCheques)
        Me.Controls.Add(Me.GroupControl1)
        Me.Modulo = "Bancos"
        Me.Name = "ban_frmAutorizarCheques"
        Me.OptionId = "002006"
        Me.Text = "Autorizar Cheques"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.dgCheques, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMarca.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgCheques, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvCheques, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.snConciliado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents anombre_de As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents fecha_cheque As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents procesada_banco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCtaBancaria As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents chkMarca As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dgCheques As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvCheques As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Conciliado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents snConciliado As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents IdCheque As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Numero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Anombrede As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents fecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents valor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl

End Class
