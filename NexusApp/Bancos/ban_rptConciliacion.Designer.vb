<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class ban_rptConciliacion
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ban_rptConciliacion))
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotalBanco = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotalConta = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCargosPend = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDepositosPend = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSaldoBanco = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCargosNoContab = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDepNoConta = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSaldoConta = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.xrlChequesPend = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCuentaContable = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPeriodo = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlBanco = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTitulo = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlEmpresa = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrLine4 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.DsBancos1 = New Nexus.dsBancos()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrLine5 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLHechoPor = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrlRevisadoPor = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLAutorizadoPor = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me.DsBancos1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
        resources.ApplyResources(Me.Detail, "Detail")
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'XrLabel4
        '
        Me.XrLabel4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Conciliacion.Valor", "{0:n2}")})
        resources.ApplyResources(Me.XrLabel4, "XrLabel4")
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        '
        'XrLabel3
        '
        Me.XrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Conciliacion.Concepto")})
        resources.ApplyResources(Me.XrLabel3, "XrLabel3")
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.StylePriority.UseFont = False
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Conciliacion.Fecha", "{0}")})
        resources.ApplyResources(Me.XrLabel2, "XrLabel2")
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Conciliacion.Referencia")})
        resources.ApplyResources(Me.XrLabel1, "XrLabel1")
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrLine1, Me.XrLabel26, Me.XrLabel25, Me.xrlTotalBanco, Me.xrlTotalConta, Me.xrlCargosPend, Me.xrlDepositosPend, Me.xrlSaldoBanco, Me.xrlCargosNoContab, Me.xrlDepNoConta, Me.xrlSaldoConta, Me.XrLabel15, Me.XrLabel13, Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.XrLabel9, Me.XrLabel21, Me.XrPageInfo2, Me.XrPageInfo1, Me.xrlChequesPend, Me.XrLabel6, Me.xrlCuentaContable, Me.xrlPeriodo, Me.xrlBanco, Me.xrlTitulo, Me.xrlEmpresa})
        resources.ApplyResources(Me.PageHeader, "PageHeader")
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'XrPanel1
        '
        Me.XrPanel1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel14, Me.XrLabel5, Me.XrLabel27, Me.XrLabel8})
        resources.ApplyResources(Me.XrPanel1, "XrPanel1")
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.StylePriority.UseBorders = False
        '
        'XrLabel14
        '
        Me.XrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None
        resources.ApplyResources(Me.XrLabel14, "XrLabel14")
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.StylePriority.UseBorders = False
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        '
        'XrLabel5
        '
        Me.XrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None
        resources.ApplyResources(Me.XrLabel5, "XrLabel5")
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.StylePriority.UseBorders = False
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        '
        'XrLabel27
        '
        Me.XrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None
        resources.ApplyResources(Me.XrLabel27, "XrLabel27")
        Me.XrLabel27.Name = "XrLabel27"
        Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel27.StylePriority.UseBorders = False
        Me.XrLabel27.StylePriority.UseFont = False
        Me.XrLabel27.StylePriority.UseTextAlignment = False
        '
        'XrLabel8
        '
        Me.XrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None
        resources.ApplyResources(Me.XrLabel8, "XrLabel8")
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.StylePriority.UseBorders = False
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        '
        'XrLine1
        '
        resources.ApplyResources(Me.XrLine1, "XrLine1")
        Me.XrLine1.Name = "XrLine1"
        '
        'XrLabel26
        '
        resources.ApplyResources(Me.XrLabel26, "XrLabel26")
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.StylePriority.UseFont = False
        '
        'XrLabel25
        '
        resources.ApplyResources(Me.XrLabel25, "XrLabel25")
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel25.StylePriority.UseFont = False
        '
        'xrlTotalBanco
        '
        resources.ApplyResources(Me.xrlTotalBanco, "xrlTotalBanco")
        Me.xrlTotalBanco.Name = "xrlTotalBanco"
        Me.xrlTotalBanco.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotalBanco.StylePriority.UseFont = False
        Me.xrlTotalBanco.StylePriority.UseTextAlignment = False
        '
        'xrlTotalConta
        '
        resources.ApplyResources(Me.xrlTotalConta, "xrlTotalConta")
        Me.xrlTotalConta.Name = "xrlTotalConta"
        Me.xrlTotalConta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotalConta.StylePriority.UseFont = False
        Me.xrlTotalConta.StylePriority.UseTextAlignment = False
        '
        'xrlCargosPend
        '
        resources.ApplyResources(Me.xrlCargosPend, "xrlCargosPend")
        Me.xrlCargosPend.Name = "xrlCargosPend"
        Me.xrlCargosPend.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargosPend.StylePriority.UseFont = False
        Me.xrlCargosPend.StylePriority.UseTextAlignment = False
        '
        'xrlDepositosPend
        '
        resources.ApplyResources(Me.xrlDepositosPend, "xrlDepositosPend")
        Me.xrlDepositosPend.Name = "xrlDepositosPend"
        Me.xrlDepositosPend.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDepositosPend.StylePriority.UseFont = False
        Me.xrlDepositosPend.StylePriority.UseTextAlignment = False
        '
        'xrlSaldoBanco
        '
        resources.ApplyResources(Me.xrlSaldoBanco, "xrlSaldoBanco")
        Me.xrlSaldoBanco.Name = "xrlSaldoBanco"
        Me.xrlSaldoBanco.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSaldoBanco.StylePriority.UseFont = False
        Me.xrlSaldoBanco.StylePriority.UseTextAlignment = False
        '
        'xrlCargosNoContab
        '
        resources.ApplyResources(Me.xrlCargosNoContab, "xrlCargosNoContab")
        Me.xrlCargosNoContab.Name = "xrlCargosNoContab"
        Me.xrlCargosNoContab.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargosNoContab.StylePriority.UseFont = False
        Me.xrlCargosNoContab.StylePriority.UseTextAlignment = False
        '
        'xrlDepNoConta
        '
        resources.ApplyResources(Me.xrlDepNoConta, "xrlDepNoConta")
        Me.xrlDepNoConta.Name = "xrlDepNoConta"
        Me.xrlDepNoConta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDepNoConta.StylePriority.UseFont = False
        Me.xrlDepNoConta.StylePriority.UseTextAlignment = False
        '
        'xrlSaldoConta
        '
        resources.ApplyResources(Me.xrlSaldoConta, "xrlSaldoConta")
        Me.xrlSaldoConta.Name = "xrlSaldoConta"
        Me.xrlSaldoConta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSaldoConta.StylePriority.UseFont = False
        Me.xrlSaldoConta.StylePriority.UseTextAlignment = False
        '
        'XrLabel15
        '
        resources.ApplyResources(Me.XrLabel15, "XrLabel15")
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        '
        'XrLabel13
        '
        resources.ApplyResources(Me.XrLabel13, "XrLabel13")
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        '
        'XrLabel12
        '
        resources.ApplyResources(Me.XrLabel12, "XrLabel12")
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        '
        'XrLabel11
        '
        resources.ApplyResources(Me.XrLabel11, "XrLabel11")
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.StylePriority.UseFont = False
        '
        'XrLabel10
        '
        resources.ApplyResources(Me.XrLabel10, "XrLabel10")
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.StylePriority.UseFont = False
        '
        'XrLabel9
        '
        resources.ApplyResources(Me.XrLabel9, "XrLabel9")
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.StylePriority.UseFont = False
        '
        'XrLabel21
        '
        resources.ApplyResources(Me.XrLabel21, "XrLabel21")
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel21.StylePriority.UseFont = False
        '
        'XrPageInfo2
        '
        resources.ApplyResources(Me.XrPageInfo2, "XrPageInfo2")
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo2.StylePriority.UseFont = False
        '
        'XrPageInfo1
        '
        resources.ApplyResources(Me.XrPageInfo1, "XrPageInfo1")
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.StylePriority.UseFont = False
        '
        'xrlChequesPend
        '
        resources.ApplyResources(Me.xrlChequesPend, "xrlChequesPend")
        Me.xrlChequesPend.Name = "xrlChequesPend"
        Me.xrlChequesPend.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlChequesPend.StylePriority.UseFont = False
        Me.xrlChequesPend.StylePriority.UseTextAlignment = False
        '
        'XrLabel6
        '
        resources.ApplyResources(Me.XrLabel6, "XrLabel6")
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.StylePriority.UseFont = False
        '
        'xrlCuentaContable
        '
        resources.ApplyResources(Me.xrlCuentaContable, "xrlCuentaContable")
        Me.xrlCuentaContable.Name = "xrlCuentaContable"
        Me.xrlCuentaContable.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCuentaContable.StylePriority.UseFont = False
        Me.xrlCuentaContable.StylePriority.UseTextAlignment = False
        '
        'xrlPeriodo
        '
        resources.ApplyResources(Me.xrlPeriodo, "xrlPeriodo")
        Me.xrlPeriodo.Name = "xrlPeriodo"
        Me.xrlPeriodo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPeriodo.StylePriority.UseFont = False
        Me.xrlPeriodo.StylePriority.UseTextAlignment = False
        '
        'xrlBanco
        '
        resources.ApplyResources(Me.xrlBanco, "xrlBanco")
        Me.xrlBanco.Name = "xrlBanco"
        Me.xrlBanco.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlBanco.StylePriority.UseFont = False
        Me.xrlBanco.StylePriority.UseTextAlignment = False
        '
        'xrlTitulo
        '
        resources.ApplyResources(Me.xrlTitulo, "xrlTitulo")
        Me.xrlTitulo.Name = "xrlTitulo"
        Me.xrlTitulo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTitulo.StylePriority.UseFont = False
        Me.xrlTitulo.StylePriority.UseTextAlignment = False
        '
        'xrlEmpresa
        '
        resources.ApplyResources(Me.xrlEmpresa, "xrlEmpresa")
        Me.xrlEmpresa.Name = "xrlEmpresa"
        Me.xrlEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlEmpresa.StylePriority.UseFont = False
        Me.xrlEmpresa.StylePriority.UseTextAlignment = False
        '
        'PageFooter
        '
        resources.ApplyResources(Me.PageFooter, "PageFooter")
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'XrLine4
        '
        resources.ApplyResources(Me.XrLine4, "XrLine4")
        Me.XrLine4.Name = "XrLine4"
        '
        'XrLine3
        '
        resources.ApplyResources(Me.XrLine3, "XrLine3")
        Me.XrLine3.Name = "XrLine3"
        '
        'XrLine2
        '
        resources.ApplyResources(Me.XrLine2, "XrLine2")
        Me.XrLine2.Name = "XrLine2"
        '
        'XrLabel17
        '
        resources.ApplyResources(Me.XrLabel17, "XrLabel17")
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.StylePriority.UseFont = False
        '
        'XrLabel16
        '
        resources.ApplyResources(Me.XrLabel16, "XrLabel16")
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.StylePriority.UseFont = False
        '
        'XrLabel7
        '
        resources.ApplyResources(Me.XrLabel7, "XrLabel7")
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.StylePriority.UseFont = False
        '
        'DsBancos1
        '
        Me.DsBancos1.DataSetName = "dsBancos"
        Me.DsBancos1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TopMarginBand1
        '
        resources.ApplyResources(Me.TopMarginBand1, "TopMarginBand1")
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'BottomMarginBand1
        '
        resources.ApplyResources(Me.BottomMarginBand1, "BottomMarginBand1")
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLAutorizadoPor, Me.XrlRevisadoPor, Me.XrLHechoPor, Me.XrLabel7, Me.XrLabel16, Me.XrLabel17, Me.XrLine2, Me.XrLine3, Me.XrLine4})
        resources.ApplyResources(Me.ReportFooter, "ReportFooter")
        Me.ReportFooter.Name = "ReportFooter"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel18})
        resources.ApplyResources(Me.GroupHeader1, "GroupHeader1")
        Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("Tipo", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'XrLabel18
        '
        Me.XrLabel18.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Conciliacion.NombreTipo")})
        resources.ApplyResources(Me.XrLabel18, "XrLabel18")
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.StylePriority.UseFont = False
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine5, Me.XrLabel20, Me.XrLabel19, Me.XrLabel22})
        resources.ApplyResources(Me.GroupFooter1, "GroupFooter1")
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'XrLine5
        '
        resources.ApplyResources(Me.XrLine5, "XrLine5")
        Me.XrLine5.Name = "XrLine5"
        '
        'XrLabel20
        '
        Me.XrLabel20.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Conciliacion.Valor")})
        resources.ApplyResources(Me.XrLabel20, "XrLabel20")
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.StylePriority.UseTextAlignment = False
        resources.ApplyResources(XrSummary1, "XrSummary1")
        XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel20.Summary = XrSummary1
        '
        'XrLabel19
        '
        resources.ApplyResources(Me.XrLabel19, "XrLabel19")
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        '
        'XrLabel22
        '
        Me.XrLabel22.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Conciliacion.NombreTipo")})
        resources.ApplyResources(Me.XrLabel22, "XrLabel22")
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel22.StylePriority.UseFont = False
        '
        'XrLHechoPor
        '
        resources.ApplyResources(Me.XrLHechoPor, "XrLHechoPor")
        Me.XrLHechoPor.Name = "XrLHechoPor"
        Me.XrLHechoPor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLHechoPor.StylePriority.UseFont = False
        '
        'XrlRevisadoPor
        '
        resources.ApplyResources(Me.XrlRevisadoPor, "XrlRevisadoPor")
        Me.XrlRevisadoPor.Name = "XrlRevisadoPor"
        Me.XrlRevisadoPor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrlRevisadoPor.StylePriority.UseFont = False
        '
        'XrLAutorizadoPor
        '
        resources.ApplyResources(Me.XrLAutorizadoPor, "XrLAutorizadoPor")
        Me.XrLAutorizadoPor.Name = "XrLAutorizadoPor"
        Me.XrLAutorizadoPor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLAutorizadoPor.StylePriority.UseFont = False
        '
        'ban_rptConciliacion
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter, Me.GroupHeader1, Me.GroupFooter1})
        Me.DataMember = "Conciliacion"
        Me.DataSource = Me.DsBancos1
        Me.DrawGrid = False
        resources.ApplyResources(Me, "$this")
        Me.SnapToGrid = False
        Me.Version = "16.1"
        CType(Me.DsBancos1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DsBancos1 As Nexus.dsBancos
    Friend WithEvents xrlTitulo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlEmpresa As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlBanco As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPeriodo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCuentaContable As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlChequesPend As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargosNoContab As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDepNoConta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSaldoConta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargosPend As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDepositosPend As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSaldoBanco As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotalBanco As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotalConta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents XrLine4 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLine5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLAutorizadoPor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrlRevisadoPor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLHechoPor As DevExpress.XtraReports.UI.XRLabel
End Class
