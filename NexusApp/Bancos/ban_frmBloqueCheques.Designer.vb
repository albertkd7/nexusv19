﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ban_frmBloqueCheques
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.leCtaBancaria = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.seDesde = New DevExpress.XtraEditors.SpinEdit()
        Me.seHasta = New DevExpress.XtraEditors.SpinEdit()
        Me.btGeneraImpresion = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btGeneraImpresion)
        Me.GroupControl1.Controls.Add(Me.seHasta)
        Me.GroupControl1.Controls.Add(Me.seDesde)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.leCtaBancaria)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Size = New System.Drawing.Size(621, 289)
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(41, 49)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(116, 13)
        Me.LabelControl1.TabIndex = 19
        Me.LabelControl1.Text = "Banco/Cuenta Bancaria:"
        '
        'leCtaBancaria
        '
        Me.leCtaBancaria.EnterMoveNextControl = True
        Me.leCtaBancaria.Location = New System.Drawing.Point(160, 46)
        Me.leCtaBancaria.Name = "leCtaBancaria"
        Me.leCtaBancaria.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCtaBancaria.Size = New System.Drawing.Size(364, 20)
        Me.leCtaBancaria.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(83, 79)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(74, 13)
        Me.LabelControl2.TabIndex = 21
        Me.LabelControl2.Text = "Desde Cheque:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(85, 109)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl3.TabIndex = 20
        Me.LabelControl3.Text = "Hasta Cheque:"
        '
        'seDesde
        '
        Me.seDesde.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDesde.EnterMoveNextControl = True
        Me.seDesde.Location = New System.Drawing.Point(160, 76)
        Me.seDesde.Name = "seDesde"
        Me.seDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDesde.Size = New System.Drawing.Size(101, 20)
        Me.seDesde.TabIndex = 22
        '
        'seHasta
        '
        Me.seHasta.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seHasta.EnterMoveNextControl = True
        Me.seHasta.Location = New System.Drawing.Point(160, 102)
        Me.seHasta.Name = "seHasta"
        Me.seHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seHasta.Size = New System.Drawing.Size(101, 20)
        Me.seHasta.TabIndex = 23
        '
        'btGeneraImpresion
        '
        Me.btGeneraImpresion.Location = New System.Drawing.Point(160, 144)
        Me.btGeneraImpresion.Name = "btGeneraImpresion"
        Me.btGeneraImpresion.Size = New System.Drawing.Size(108, 35)
        Me.btGeneraImpresion.TabIndex = 24
        Me.btGeneraImpresion.Text = "Generar Impresión"
        '
        'ban_frmBloqueCheques
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(621, 314)
        Me.Modulo = "Bancos"
        Me.Name = "ban_frmBloqueCheques"
        Me.Text = "Impresión Bloque de Cheques"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCtaBancaria As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seHasta As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seDesde As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents btGeneraImpresion As DevExpress.XtraEditors.SimpleButton

End Class
