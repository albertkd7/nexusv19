﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ban_frmCheques
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xtcCheques = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.gc2 = New DevExpress.XtraGrid.GridControl()
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.xtpCheques = New DevExpress.XtraTab.XtraTabPage()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdCuenta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteIdCuenta = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcReferencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcConcepto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDebe = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcHaber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdCentro = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.rileIdCentro = New DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit()
        Me.RepositoryItemGridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.seDiferencia = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombreCuenta = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdUP = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.sbLiquidaCaja = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.beProveedor = New DevExpress.XtraEditors.ButtonEdit()
        Me.leCtaBancaria = New DevExpress.XtraEditors.LookUpEdit()
        Me.sbAgregarCompra = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdPartida = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdCheque = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumPartida = New DevExpress.XtraEditors.TextEdit()
        Me.meConcepto = New DevExpress.XtraEditors.MemoEdit()
        Me.leTipoPartida = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.teSufijo = New DevExpress.XtraEditors.TextEdit()
        Me.teNumCheque = New DevExpress.XtraEditors.TextEdit()
        Me.teValor = New DevExpress.XtraEditors.TextEdit()
        Me.teANombreDe = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkAnulado = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        CType(Me.xtcCheques, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcCheques.SuspendLayout()
        Me.xtpDatos.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpCheques.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteIdCuenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rileIdCentro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemGridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.seDiferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombreCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSufijo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teValor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teANombreDe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAnulado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcCheques
        '
        Me.xtcCheques.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcCheques.Location = New System.Drawing.Point(0, 0)
        Me.xtcCheques.Name = "xtcCheques"
        Me.xtcCheques.SelectedTabPage = Me.xtpDatos
        Me.xtcCheques.Size = New System.Drawing.Size(1056, 447)
        Me.xtcCheques.TabIndex = 6
        Me.xtcCheques.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpDatos, Me.xtpCheques})
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.gc2)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(1050, 419)
        Me.xtpDatos.Text = "Consulta de Cheques"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.leSucursalDetalle, Me.RepositoryItemTextEdit1, Me.chkAnulado})
        Me.gc2.Size = New System.Drawing.Size(1050, 419)
        Me.gc2.TabIndex = 2
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn8, Me.GridColumn12, Me.GridColumn11, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn13, Me.GridColumn9, Me.GridColumn10})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdCheque"
        Me.GridColumn1.FieldName = "IdCheque"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 47
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Tipo Partida"
        Me.GridColumn2.FieldName = "IdTipoPartida"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 54
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Numero Partida"
        Me.GridColumn3.FieldName = "NumeroPartida"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 68
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Numero Cuenta"
        Me.GridColumn4.FieldName = "NumeroCuenta"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 64
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Numero Cheque"
        Me.GridColumn8.FieldName = "Numero"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 4
        Me.GridColumn8.Width = 60
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Valor Cheque"
        Me.GridColumn12.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.GridColumn12.FieldName = "Valor"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 5
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Mask.EditMask = "n2"
        Me.RepositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RepositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Sucursal"
        Me.GridColumn11.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn11.FieldName = "IdSucursal"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 6
        Me.GridColumn11.Width = 60
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Fecha"
        Me.GridColumn5.FieldName = "Fecha"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 7
        Me.GridColumn5.Width = 61
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "A Nombre de"
        Me.GridColumn6.FieldName = "AnombreDe"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 8
        Me.GridColumn6.Width = 337
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Concepto"
        Me.GridColumn7.FieldName = "Concepto"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 9
        Me.GridColumn7.Width = 103
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Creado Por"
        Me.GridColumn9.FieldName = "CreadoPor"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 11
        Me.GridColumn9.Width = 68
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "FechaHoraCreación"
        Me.GridColumn10.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.GridColumn10.FieldName = "FechaHoraCreacion"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 12
        Me.GridColumn10.Width = 76
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemDateEdit1.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'xtpCheques
        '
        Me.xtpCheques.Controls.Add(Me.gc)
        Me.xtpCheques.Controls.Add(Me.PanelControl3)
        Me.xtpCheques.Controls.Add(Me.PanelControl2)
        Me.xtpCheques.Controls.Add(Me.pcHeader)
        Me.xtpCheques.Name = "xtpCheques"
        Me.xtpCheques.Size = New System.Drawing.Size(1050, 419)
        Me.xtpCheques.Text = "Cheques"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 110)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rileIdCentro, Me.riteIdCuenta})
        Me.gc.Size = New System.Drawing.Size(1017, 271)
        Me.gc.TabIndex = 7
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Appearance.TopNewRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.gv.Appearance.TopNewRow.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.gv.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Black
        Me.gv.Appearance.TopNewRow.Options.UseBackColor = True
        Me.gv.Appearance.TopNewRow.Options.UseForeColor = True
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdCuenta, Me.gcReferencia, Me.gcConcepto, Me.gcDebe, Me.gcHaber, Me.gcIdCentro})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.NewItemRowText = "Click aquí para agregar nuevo registro. <Esc> para cancelar"
        Me.gv.OptionsNavigation.AutoFocusNewRow = True
        Me.gv.OptionsNavigation.EnterMoveNextColumn = True
        Me.gv.OptionsSelection.MultiSelect = True
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdCuenta
        '
        Me.gcIdCuenta.Caption = "Cód.Cuenta"
        Me.gcIdCuenta.ColumnEdit = Me.riteIdCuenta
        Me.gcIdCuenta.FieldName = "IdCuenta"
        Me.gcIdCuenta.Name = "gcIdCuenta"
        Me.gcIdCuenta.Visible = True
        Me.gcIdCuenta.VisibleIndex = 0
        Me.gcIdCuenta.Width = 114
        '
        'riteIdCuenta
        '
        Me.riteIdCuenta.AutoHeight = False
        Me.riteIdCuenta.Name = "riteIdCuenta"
        '
        'gcReferencia
        '
        Me.gcReferencia.Caption = "Referencia"
        Me.gcReferencia.FieldName = "Referencia"
        Me.gcReferencia.Name = "gcReferencia"
        Me.gcReferencia.Visible = True
        Me.gcReferencia.VisibleIndex = 1
        Me.gcReferencia.Width = 86
        '
        'gcConcepto
        '
        Me.gcConcepto.Caption = "Concepto"
        Me.gcConcepto.FieldName = "Concepto"
        Me.gcConcepto.Name = "gcConcepto"
        Me.gcConcepto.Visible = True
        Me.gcConcepto.VisibleIndex = 2
        Me.gcConcepto.Width = 350
        '
        'gcDebe
        '
        Me.gcDebe.Caption = "Debe"
        Me.gcDebe.DisplayFormat.FormatString = "{0:n2}"
        Me.gcDebe.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcDebe.FieldName = "Debe"
        Me.gcDebe.Name = "gcDebe"
        Me.gcDebe.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Debe", "{0:c2}")})
        Me.gcDebe.Visible = True
        Me.gcDebe.VisibleIndex = 3
        Me.gcDebe.Width = 97
        '
        'gcHaber
        '
        Me.gcHaber.Caption = "Haber"
        Me.gcHaber.DisplayFormat.FormatString = "{0:n2}"
        Me.gcHaber.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcHaber.FieldName = "Haber"
        Me.gcHaber.Name = "gcHaber"
        Me.gcHaber.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Haber", "{0:c2}")})
        Me.gcHaber.Visible = True
        Me.gcHaber.VisibleIndex = 4
        Me.gcHaber.Width = 94
        '
        'gcIdCentro
        '
        Me.gcIdCentro.Caption = "Centro de Costo"
        Me.gcIdCentro.ColumnEdit = Me.rileIdCentro
        Me.gcIdCentro.FieldName = "IdCentro"
        Me.gcIdCentro.Name = "gcIdCentro"
        Me.gcIdCentro.Visible = True
        Me.gcIdCentro.VisibleIndex = 5
        Me.gcIdCentro.Width = 224
        '
        'rileIdCentro
        '
        Me.rileIdCentro.AutoHeight = False
        Me.rileIdCentro.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rileIdCentro.Name = "rileIdCentro"
        Me.rileIdCentro.View = Me.RepositoryItemGridLookUpEdit1View
        '
        'RepositoryItemGridLookUpEdit1View
        '
        Me.RepositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.RepositoryItemGridLookUpEdit1View.Name = "RepositoryItemGridLookUpEdit1View"
        Me.RepositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.RepositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.seDiferencia)
        Me.PanelControl3.Controls.Add(Me.LabelControl9)
        Me.PanelControl3.Controls.Add(Me.teNombreCuenta)
        Me.PanelControl3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl3.Location = New System.Drawing.Point(0, 381)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(1017, 38)
        Me.PanelControl3.TabIndex = 6
        '
        'seDiferencia
        '
        Me.seDiferencia.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDiferencia.Location = New System.Drawing.Point(802, 13)
        Me.seDiferencia.Name = "seDiferencia"
        Me.seDiferencia.Properties.Mask.EditMask = "n2"
        Me.seDiferencia.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seDiferencia.Properties.ReadOnly = True
        Me.seDiferencia.Size = New System.Drawing.Size(160, 20)
        Me.seDiferencia.TabIndex = 19
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(744, 16)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl9.TabIndex = 15
        Me.LabelControl9.Text = "Diferencia:"
        '
        'teNombreCuenta
        '
        Me.teNombreCuenta.Enabled = False
        Me.teNombreCuenta.Location = New System.Drawing.Point(12, 8)
        Me.teNombreCuenta.Name = "teNombreCuenta"
        Me.teNombreCuenta.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.teNombreCuenta.Properties.Appearance.ForeColor = System.Drawing.Color.Red
        Me.teNombreCuenta.Properties.Appearance.Options.UseFont = True
        Me.teNombreCuenta.Properties.Appearance.Options.UseForeColor = True
        Me.teNombreCuenta.Properties.ReadOnly = True
        Me.teNombreCuenta.Size = New System.Drawing.Size(715, 24)
        Me.teNombreCuenta.TabIndex = 20
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cmdBorrar)
        Me.PanelControl2.Controls.Add(Me.cmdUP)
        Me.PanelControl2.Controls.Add(Me.cmdDown)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelControl2.Location = New System.Drawing.Point(1017, 110)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(33, 309)
        Me.PanelControl2.TabIndex = 8
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(3, 81)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(28, 24)
        Me.cmdBorrar.TabIndex = 17
        Me.cmdBorrar.ToolTip = "Eliminar el registro"
        Me.cmdBorrar.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.cmdBorrar.ToolTipTitle = "Eliminar"
        '
        'cmdUP
        '
        Me.cmdUP.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdUP.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUP.Location = New System.Drawing.Point(3, 24)
        Me.cmdUP.Name = "cmdUP"
        Me.cmdUP.Size = New System.Drawing.Size(28, 24)
        Me.cmdUP.TabIndex = 15
        Me.cmdUP.ToolTip = "Mueve una linea hacia arriba"
        '
        'cmdDown
        '
        Me.cmdDown.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(3, 51)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(28, 24)
        Me.cmdDown.TabIndex = 16
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.LabelControl12)
        Me.pcHeader.Controls.Add(Me.sbLiquidaCaja)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Controls.Add(Me.beProveedor)
        Me.pcHeader.Controls.Add(Me.leCtaBancaria)
        Me.pcHeader.Controls.Add(Me.sbAgregarCompra)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.teIdPartida)
        Me.pcHeader.Controls.Add(Me.LabelControl10)
        Me.pcHeader.Controls.Add(Me.teIdCheque)
        Me.pcHeader.Controls.Add(Me.LabelControl8)
        Me.pcHeader.Controls.Add(Me.LabelControl3)
        Me.pcHeader.Controls.Add(Me.teNumPartida)
        Me.pcHeader.Controls.Add(Me.meConcepto)
        Me.pcHeader.Controls.Add(Me.leTipoPartida)
        Me.pcHeader.Controls.Add(Me.LabelControl4)
        Me.pcHeader.Controls.Add(Me.LabelControl11)
        Me.pcHeader.Controls.Add(Me.LabelControl7)
        Me.pcHeader.Controls.Add(Me.teSufijo)
        Me.pcHeader.Controls.Add(Me.teNumCheque)
        Me.pcHeader.Controls.Add(Me.teValor)
        Me.pcHeader.Controls.Add(Me.teANombreDe)
        Me.pcHeader.Controls.Add(Me.LabelControl6)
        Me.pcHeader.Controls.Add(Me.LabelControl5)
        Me.pcHeader.Controls.Add(Me.deFecha)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(1050, 110)
        Me.pcHeader.TabIndex = 4
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(115, 88)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(474, 20)
        Me.leSucursal.TabIndex = 9
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(69, 91)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl12.TabIndex = 58
        Me.LabelControl12.Text = "Sucursal:"
        '
        'sbLiquidaCaja
        '
        Me.sbLiquidaCaja.AllowFocus = False
        Me.sbLiquidaCaja.Location = New System.Drawing.Point(890, 68)
        Me.sbLiquidaCaja.Name = "sbLiquidaCaja"
        Me.sbLiquidaCaja.Size = New System.Drawing.Size(120, 20)
        Me.sbLiquidaCaja.TabIndex = 21
        Me.sbLiquidaCaja.Text = "Liquidar Caja Chica"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(10, 7)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Banco/Cta. Bancaria:"
        '
        'beProveedor
        '
        Me.beProveedor.EnterMoveNextControl = True
        Me.beProveedor.Location = New System.Drawing.Point(115, 25)
        Me.beProveedor.Name = "beProveedor"
        Me.beProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beProveedor.Size = New System.Drawing.Size(116, 20)
        Me.beProveedor.TabIndex = 3
        '
        'leCtaBancaria
        '
        Me.leCtaBancaria.EnterMoveNextControl = True
        Me.leCtaBancaria.Location = New System.Drawing.Point(115, 4)
        Me.leCtaBancaria.Name = "leCtaBancaria"
        Me.leCtaBancaria.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCtaBancaria.Size = New System.Drawing.Size(474, 20)
        Me.leCtaBancaria.TabIndex = 0
        '
        'sbAgregarCompra
        '
        Me.sbAgregarCompra.AllowFocus = False
        Me.sbAgregarCompra.Location = New System.Drawing.Point(890, 89)
        Me.sbAgregarCompra.Name = "sbAgregarCompra"
        Me.sbAgregarCompra.Size = New System.Drawing.Size(120, 20)
        Me.sbAgregarCompra.TabIndex = 20
        Me.sbAgregarCompra.Text = "Importar desde Excel"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(45, 28)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(67, 13)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "A Nombre De:"
        '
        'teIdPartida
        '
        Me.teIdPartida.Enabled = False
        Me.teIdPartida.EnterMoveNextControl = True
        Me.teIdPartida.Location = New System.Drawing.Point(922, 25)
        Me.teIdPartida.Name = "teIdPartida"
        Me.teIdPartida.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.teIdPartida.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.teIdPartida.Properties.ReadOnly = True
        Me.teIdPartida.Size = New System.Drawing.Size(88, 20)
        Me.teIdPartida.TabIndex = 6
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(867, 28)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl10.TabIndex = 19
        Me.LabelControl10.Text = "Id.Partida:"
        '
        'teIdCheque
        '
        Me.teIdCheque.Enabled = False
        Me.teIdCheque.EnterMoveNextControl = True
        Me.teIdCheque.Location = New System.Drawing.Point(922, 4)
        Me.teIdCheque.Name = "teIdCheque"
        Me.teIdCheque.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.teIdCheque.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.teIdCheque.Properties.ReadOnly = True
        Me.teIdCheque.Size = New System.Drawing.Size(88, 20)
        Me.teIdCheque.TabIndex = 3
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(846, 7)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl8.TabIndex = 19
        Me.LabelControl8.Text = "Id. de Cheque:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(62, 50)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl3.TabIndex = 8
        Me.LabelControl3.Text = "Concepto:"
        '
        'teNumPartida
        '
        Me.teNumPartida.EnterMoveNextControl = True
        Me.teNumPartida.Location = New System.Drawing.Point(922, 46)
        Me.teNumPartida.Name = "teNumPartida"
        Me.teNumPartida.Properties.ReadOnly = True
        Me.teNumPartida.Size = New System.Drawing.Size(88, 20)
        Me.teNumPartida.TabIndex = 9
        '
        'meConcepto
        '
        Me.meConcepto.EnterMoveNextControl = True
        Me.meConcepto.Location = New System.Drawing.Point(115, 46)
        Me.meConcepto.Name = "meConcepto"
        Me.meConcepto.Size = New System.Drawing.Size(474, 41)
        Me.meConcepto.TabIndex = 5
        '
        'leTipoPartida
        '
        Me.leTipoPartida.EnterMoveNextControl = True
        Me.leTipoPartida.Location = New System.Drawing.Point(707, 88)
        Me.leTipoPartida.Name = "leTipoPartida"
        Me.leTipoPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPartida.Size = New System.Drawing.Size(135, 20)
        Me.leTipoPartida.TabIndex = 8
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(628, 7)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl4.TabIndex = 10
        Me.LabelControl4.Text = "No. de Cheque:"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(861, 49)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl11.TabIndex = 13
        Me.LabelControl11.Text = "No. Partida:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(628, 91)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl7.TabIndex = 13
        Me.LabelControl7.Text = "Tipo de Partida:"
        '
        'teSufijo
        '
        Me.teSufijo.EnterMoveNextControl = True
        Me.teSufijo.Location = New System.Drawing.Point(810, 4)
        Me.teSufijo.Name = "teSufijo"
        Me.teSufijo.Size = New System.Drawing.Size(32, 20)
        Me.teSufijo.TabIndex = 2
        '
        'teNumCheque
        '
        Me.teNumCheque.EnterMoveNextControl = True
        Me.teNumCheque.Location = New System.Drawing.Point(707, 4)
        Me.teNumCheque.Name = "teNumCheque"
        Me.teNumCheque.Size = New System.Drawing.Size(101, 20)
        Me.teNumCheque.TabIndex = 1
        '
        'teValor
        '
        Me.teValor.EnterMoveNextControl = True
        Me.teValor.Location = New System.Drawing.Point(707, 67)
        Me.teValor.Name = "teValor"
        Me.teValor.Properties.Appearance.Options.UseTextOptions = True
        Me.teValor.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teValor.Properties.Mask.EditMask = "n2"
        Me.teValor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teValor.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teValor.Size = New System.Drawing.Size(135, 20)
        Me.teValor.TabIndex = 7
        '
        'teANombreDe
        '
        Me.teANombreDe.EnterMoveNextControl = True
        Me.teANombreDe.Location = New System.Drawing.Point(237, 25)
        Me.teANombreDe.Name = "teANombreDe"
        Me.teANombreDe.Size = New System.Drawing.Size(605, 20)
        Me.teANombreDe.TabIndex = 4
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(613, 71)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl6.TabIndex = 12
        Me.LabelControl6.Text = "Monto del Cheque:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(618, 50)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl5.TabIndex = 11
        Me.LabelControl5.Text = "Fecha de Emisión:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(707, 46)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(135, 20)
        Me.deFecha.TabIndex = 6
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Anulado"
        Me.GridColumn13.ColumnEdit = Me.chkAnulado
        Me.GridColumn13.FieldName = "Anulado"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 10
        '
        'chkAnulado
        '
        Me.chkAnulado.AutoHeight = False
        Me.chkAnulado.Name = "chkAnulado"
        '
        'ban_frmCheques
        '
        Me.ClientSize = New System.Drawing.Size(1056, 472)
        Me.Controls.Add(Me.xtcCheques)
        Me.Modulo = "Bancos"
        Me.Name = "ban_frmCheques"
        Me.OptionId = "002001"
        Me.Text = "Emisión de cheques"
        Me.Controls.SetChildIndex(Me.xtcCheques, 0)
        CType(Me.xtcCheques, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcCheques.ResumeLayout(False)
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpCheques.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteIdCuenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rileIdCentro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemGridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.seDiferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombreCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSufijo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teValor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teANombreDe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAnulado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents xtcCheques As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpCheques As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteIdCuenta As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcReferencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDebe As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcHaber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdCentro As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rileIdCentro As DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit
    Friend WithEvents RepositoryItemGridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents seDiferencia As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombreCuenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdUP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbLiquidaCaja As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beProveedor As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents leCtaBancaria As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents sbAgregarCompra As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdPartida As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdCheque As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumPartida As DevExpress.XtraEditors.TextEdit
    Friend WithEvents meConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents leTipoPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumCheque As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teValor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teANombreDe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teSufijo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkAnulado As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit

End Class
