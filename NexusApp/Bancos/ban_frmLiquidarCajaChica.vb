﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class ban_frmLiquidarCajaChica
    Dim dtDetalle As New DataTable
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim dtCompras As DataTable

    Private _TotalLiquidacion As Decimal
    Public Property TotalLiquidacion() As Decimal
        Get
            Return _TotalLiquidacion
        End Get
        Set(ByVal value As Decimal)
            _TotalLiquidacion = value
        End Set
    End Property

    Private _IdCaja As Integer
    Public Property IdCaja() As Integer
        Get
            Return _IdCaja
        End Get
        Set(ByVal value As Integer)
            _IdCaja = value
        End Set
    End Property
    Private _IdProducto As String
    Public Property IdProducto() As String
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As String)
            _IdProducto = value
        End Set
    End Property

    Private _NumerosComprobantes As String
    Public Property NumerosComprobantes() As String
        Get
            Return _NumerosComprobantes
        End Get
        Set(ByVal value As String)
            _NumerosComprobantes = value
        End Set
    End Property
    Private _NumeroCheque As String
    Public Property NumeroCheque() As String
        Get
            Return _NumeroCheque
        End Get
        Set(ByVal value As String)
            _NumeroCheque = value
        End Set
    End Property

    Private _IdsCompras As List(Of Integer)
    Public Property IdsCompras() As List(Of Integer)
        Get
            Return _IdsCompras
        End Get
        Set(ByVal value As List(Of Integer))
            _IdsCompras = value
        End Set
    End Property

    Private Sub com_frmDetallarGastosImportaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        teTotal.EditValue = 0.0
        objCombos.Com_CajasChicaCompras(leCaja, "")
        sbGenerar_Click("", New EventArgs)
        IdsCompras = New List(Of Integer)
    End Sub

    Private Sub LlenarEntidades()
        Dim frmCheques As ban_frmCheques = Me.Owner
        Dim dt As DataTable = frmCheques.dtLiquidacion

        NumerosComprobantes = "Liquidación de Caja Chica: " + leCaja.Text
        NumerosComprobantes += " CCF. # "
        IdCaja = leCaja.EditValue

        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "Liquidar") Then
                NumerosComprobantes += " ,"
                NumerosComprobantes += " " + gv.GetRowCellValue(i, "Numero")
                TotalLiquidacion += gv.GetRowCellValue(i, "TotalComprobante")
                IdsCompras.Add(gv.GetRowCellValue(i, "IdComprobante"))

                Dim dr As DataRow = frmCheques.dtLiquidacion.NewRow()
                dr("IdComprobante") = gv.GetRowCellValue(i, "IdComprobante")
                frmCheques.dtLiquidacion.Rows.Add(dr)
            End If
        Next

        Me.Close()
    End Sub

    Private Sub sbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGuardar.Click
        LlenarEntidades()
    End Sub


    Private Sub sbGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGenerar.Click
        gc.DataSource = bl.com_LiquidaCajaChica(deDesde.EditValue, deHasta.EditValue, leCaja.EditValue, ceFHC.EditValue)
        chkAplica_CheckedChanged("", New EventArgs)
        'chkAplica_EditValueChanged("", New EventArgs)
    End Sub

    Private Sub sbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbSalir.Click
        Me.Close()
    End Sub

    Private Sub chkAplica_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAplica.CheckedChanged
        If gv.FocusedColumn.FieldName = "Liquidar" Then
            gv.SetFocusedRowCellValue("Liquidar", SiEsNulo(gv.EditingValue, False))
        End If

        teTotal.EditValue = 0.0
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "Liquidar") Then
                teTotal.EditValue += gv.GetRowCellValue(i, "TotalComprobante")
            End If
        Next
    End Sub
End Class
