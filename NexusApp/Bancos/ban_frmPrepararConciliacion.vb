﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraReports.UI
Public Class ban_frmPrepararConciliacion
    Dim bl As New BancosBLL(g_ConnectionString)
    Dim dt As DataTable
    Dim dttrans As DataTable
    Dim dttrans2 As DataTable
    Dim DetalleConciliacion As List(Of ban_ConciliacionesDetalle)

    Private Sub frmLoad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFecha.DateTime = Today()
        objCombos.banCuentasBancarias(leCtaBancaria)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        leSucursal.EditValue = piIdSucursal
    End Sub

    Private Sub cmdProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProcesar.Click
        Dim dDesdeFecha As Date
        dDesdeFecha = CDate("01/05/2017")

        If deFecha.EditValue < dDesdeFecha Then
            Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
            If Not EsOk Then
                MsgBox("Fecha de la conciliación corresponde a un período ya cerrado", 64, "Nota")
                Exit Sub
            End If
        End If
        Dim sn As Boolean = True 'MsgBox("Incluir registros conciliados?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes
        Dim ds As DataSet = bl.GetConciliaRegistrosBancos(leCtaBancaria.EditValue, deFecha.DateTime, sn, leSucursal.EditValue)
        dt = ds.Tables(0)
        dgCheques.DataSource = dt

        dttrans = ds.Tables(1)
        dgTransac.DataSource = dttrans

        dttrans2 = ds.Tables(2)
        dgTransac2.DataSource = dttrans2
        CalculoTotales()

    End Sub

    Private Sub frmConciliacionCheques_Update_Click() Handles Me.Guardar

        If gvCheques.DataRowCount + gvTransacciones.DataRowCount + gvTransacciones2.DataRowCount = 0 Then
            MsgBox(String.Format("Debe de cargar los cheques y transacciones "), MsgBoxStyle.Critical)
            Exit Sub
        End If

        CargaEntidad()
        Dim sn As Boolean = MsgBox("Esta seguro de guardar los cambios?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes

        If sn = False Then
            MsgBox(String.Format("LOS DATOS NO FUERON GUARDADOS"), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If

        Dim msj As String = ""
        msj = bl.ActualizaDetalleConciliacion(DetalleConciliacion, leCtaBancaria.EditValue, leSucursal.EditValue _
                                                , CDate(deFecha.EditValue).Month, CDate(deFecha.EditValue).Year, deFecha.EditValue _
                                                , True, objMenu.User)


        If msj = "" Then
            MsgBox("Los registros fueron guardados con éxito", 64, "Nota")
        Else
            MsgBox(String.Format("NO FUE POSIBLE GUARDAR LOS REGISTROS{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If

        'For i = 0 To gvCheques.RowCount - 1
        '    If gvCheques.GetRowCellValue(i, "Conciliado") Then
        '        ' bl.ActualizaMarcaConciliado(gvCheques.GetRowCellValue(i, "IdCheque"), 1)

        '        'IdCuentaBancaria, IdSucursal, Mes, Anio, Fecha, Cheques, CreadoPor
        '        bl.ActualizaDetalleConciliacion(DetalleConciliacion, leCtaBancaria.EditValue, leSucursal.EditValue _
        '                                        , CDate(deFecha.EditValue).Month, CDate(deFecha.EditValue).Year, deFecha.EditValue _
        '                                        , True, objMenu.User)
        '    Else
        '        'bl.ActualizaMarcaConciliado(gvCheques.GetRowCellValue(i, "IdCheque"), 0)
        '        bl.ActualizaDetalleConciliacion(DetalleConciliacion, leCtaBancaria.EditValue, leSucursal.EditValue _
        '                        , CDate(deFecha.EditValue).Month, CDate(deFecha.EditValue).Year, deFecha.EditValue _
        '                        , True, objMenu.User)
        '    End If
        'Next

        dt = Nothing

        'For i = 0 To gvTransacciones.RowCount - 1
        '    If gvTransacciones.GetRowCellValue(i, "ProcesadaBanco") Then
        '        bl.ActualizaMarcaProcesado(gvTransacciones.GetRowCellValue(i, "IdTransaccion"), 1, True)
        '    Else
        '        bl.ActualizaMarcaProcesado(gvTransacciones.GetRowCellValue(i, "IdTransaccion"), 0, True)
        '    End If
        'Next

        dttrans = Nothing


        'For i = 0 To gvTransacciones2.RowCount - 1
        '    If gvTransacciones2.GetRowCellValue(i, "ProcesadaBanco") Then
        '        bl.ActualizaMarcaProcesado(gvTransacciones2.GetRowCellValue(i, "IdTransaccion"), 1, True)
        '    Else
        '        bl.ActualizaMarcaProcesado(gvTransacciones2.GetRowCellValue(i, "IdTransaccion"), 0, False)
        '    End If
        'Next

        dttrans2 = Nothing

        dgCheques.DataSource = dt
        dgTransac.DataSource = dttrans
        dgTransac2.DataSource = dttrans2

        Dim dtFecha As DateTime
        dtFecha = deFecha.EditValue

        Dim sCtaContable As String
        sCtaContable = bl.GetCuentaCbleBanco(leCtaBancaria.EditValue)

        Dim dSaldoContab As Decimal = bl.GetSaldoCuentaBanco(sCtaContable, dtFecha, leSucursal.EditValue)
        Dim dCargosNoCon As Decimal = bl.GetCargosNoContab(sCtaContable, dtFecha, leSucursal.EditValue)
        Dim dDepositosNC As Decimal = bl.GetDepositosNoContab(sCtaContable, dtFecha, leSucursal.EditValue)
        Dim dDepositosTr As Decimal = bl.GetDepositosTransito(sCtaContable, dtFecha, leSucursal.EditValue)
        Dim deSaldoBanco As Decimal = 0.0
        Dim dChequesPend As Decimal = bl.GetChequesPendientes(sCtaContable, dtFecha, leSucursal.EditValue)
        Dim dCargosPend As Decimal = bl.GetCargosPendientes(sCtaContable, dtFecha, leSucursal.EditValue)

        'Dim sCtaBanco = cboCuentaBanco.SelectedValue
        bl.GeneraConciliacion(leCtaBancaria.EditValue, sCtaContable, dtFecha _
                              , dSaldoContab, dCargosNoCon _
                              , dDepositosNC, dChequesPend _
                              , dCargosPend, dDepositosTr, deSaldoBanco, leSucursal.EditValue)

        Dim dtPendientes As DataTable = bl.ObtieneConciliacion(leCtaBancaria.EditValue, dtFecha, leSucursal.EditValue)
        'Dim rpt As New ban_rptChequesPendientes() With {.DataSource = dtPendientes, .DataMember = ""}
        'rpt.xrlEmpresa.Text = gsNombre_Empresa
        'rpt.xrlPeriodo.Text = "CORRESPONDIENTE AL MES DE " & (ObtieneMesString(Month(dtFecha))).ToUpper & " DE " & Year(deFecha.EditValue).ToString
        'rpt.xrlCuentaContable.Text = "CUENTA CONTABLE: " & bl.GetCuentaCbleBanco(leCtaBancaria.EditValue)
        'rpt.xrlBanco.Text = "BANCO: " & leCtaBancaria.Text
        'rpt.ShowPreviewDialog()
    End Sub

    Private Sub CargaEntidad()
        Dim entCuentaBancaria As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(leCtaBancaria.EditValue)

        'lleno la entidad de los cheques
        DetalleConciliacion = New List(Of ban_ConciliacionesDetalle)
        For i = 0 To gvCheques.DataRowCount - 1
            Dim entDetalle As New ban_ConciliacionesDetalle
            With entDetalle
                .IdSucursal = leSucursal.EditValue
                .IdCuentaBancaria = leCtaBancaria.EditValue
                .IdComprobante = gvCheques.GetRowCellValue(i, "IdCheque")
                .Cheque = True
                .IdCuentaContable = entCuentaBancaria.IdCuentaContable
                .ProcesadaBanco = gvCheques.GetRowCellValue(i, "Conciliado")
                .Contabilizar = True
                .Referencia = gvCheques.GetRowCellValue(i, "Numero")
                .Concepto = gvCheques.GetRowCellValue(i, "AnombreDe")
                .MesConcilia = CDate(deFecha.EditValue).Month
                .EjercicioConcilia = CDate(deFecha.EditValue).Year
                .Fecha = gvCheques.GetRowCellValue(i, "Fecha")
                .Valor = gvCheques.GetRowCellValue(i, "Valor")
                .Debe = gvCheques.GetRowCellValue(i, "Debe")
                .Haber = gvCheques.GetRowCellValue(i, "Haber")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            DetalleConciliacion.Add(entDetalle)
        Next
        For i = 0 To gvTransacciones.DataRowCount - 1
            Dim entDetalle2 As New ban_ConciliacionesDetalle
            With entDetalle2
                .IdSucursal = leSucursal.EditValue
                .IdCuentaBancaria = leCtaBancaria.EditValue
                .IdComprobante = gvTransacciones.GetRowCellValue(i, "IdTransaccion")
                .Cheque = False
                .IdCuentaContable = entCuentaBancaria.IdCuentaContable
                .ProcesadaBanco = gvTransacciones.GetRowCellValue(i, "ProcesadaBanco")
                .Contabilizar = True
                .Referencia = gvTransacciones.GetRowCellValue(i, "Referencia")
                .Concepto = gvTransacciones.GetRowCellValue(i, "Concepto")
                .MesConcilia = CDate(deFecha.EditValue).Month
                .EjercicioConcilia = CDate(deFecha.EditValue).Year
                .Fecha = gvTransacciones.GetRowCellValue(i, "Fecha")
                .Valor = gvTransacciones.GetRowCellValue(i, "Valor")
                .Debe = gvTransacciones.GetRowCellValue(i, "Debe")
                .Haber = gvTransacciones.GetRowCellValue(i, "Haber")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            DetalleConciliacion.Add(entDetalle2)
        Next
        For i = 0 To gvTransacciones2.DataRowCount - 1
            Dim entDetalle3 As New ban_ConciliacionesDetalle
            With entDetalle3
                .IdSucursal = leSucursal.EditValue
                .IdCuentaBancaria = leCtaBancaria.EditValue
                .IdComprobante = gvTransacciones2.GetRowCellValue(i, "IdTransaccion")
                .Cheque = False
                .IdCuentaContable = entCuentaBancaria.IdCuentaContable
                .ProcesadaBanco = gvTransacciones2.GetRowCellValue(i, "ProcesadaBanco")
                .Contabilizar = False 'gvTransacciones2.GetRowCellValue(i, "ProcesadaBanco")
                .Referencia = gvTransacciones2.GetRowCellValue(i, "Referencia")
                .Concepto = gvTransacciones2.GetRowCellValue(i, "Concepto")
                .MesConcilia = CDate(deFecha.EditValue).Month
                .EjercicioConcilia = CDate(deFecha.EditValue).Year
                .Fecha = gvTransacciones2.GetRowCellValue(i, "Fecha")
                .Valor = gvTransacciones2.GetRowCellValue(i, "Valor")
                .Debe = gvTransacciones2.GetRowCellValue(i, "Debe")
                .Haber = gvTransacciones2.GetRowCellValue(i, "Haber")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            DetalleConciliacion.Add(entDetalle3)
        Next
    End Sub

    Private Sub chkMarca_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMarca.CheckedChanged
        Dim flag As Boolean
        If chkMarca.Checked Then
            flag = True
        Else
            flag = False
        End If

        Dim vista As GridView = dgCheques.FocusedView
        For i = 0 To vista.RowCount - 1
            vista.FocusedRowHandle = i
            vista.FocusedColumn = vista.Columns("Conciliado")
            vista.SetFocusedValue(flag)
        Next

        Dim vistaTrans As GridView = dgTransac.FocusedView
        For i = 0 To vistaTrans.RowCount - 1
            vistaTrans.FocusedRowHandle = i
            vistaTrans.FocusedColumn = vistaTrans.Columns("ProcesadaBanco")
            vistaTrans.SetFocusedValue(flag)
        Next

        Dim vistaTrans2 As GridView = dgTransac2.FocusedView
        For i = 0 To vistaTrans2.RowCount - 1
            vistaTrans2.FocusedRowHandle = i
            vistaTrans2.FocusedColumn = vistaTrans2.Columns("ProcesadaBanco")
            vistaTrans2.SetFocusedValue(flag)
        Next
        CalculoTotales()
    End Sub

    Public Sub CalculoTotales()

        Dim dFechaCheques As DateTime
        dFechaCheques = SiEsNulo(gvCheques.GetRowCellValue(gvCheques.FocusedRowHandle, "Fecha"), Today)
        Dim dFechaTransacciones As DateTime
        dFechaTransacciones = SiEsNulo(gvTransacciones.GetRowCellValue(gvTransacciones.FocusedRowHandle, "Fecha"), Today)

        Dim TotalChequesDia As Decimal = 0.0
        Dim TotalTransaccionesDia As Decimal = 0.0

        Dim ChequesNoCobrados As Decimal = 0.0
        Dim ChequesCobrados As Decimal = 0.0

        Dim TransaccionesNoCobradas As Decimal = 0.0
        Dim TransaccionesCobradas As Decimal = 0.0

        For i = 0 To gvCheques.RowCount - 1
            If gvCheques.GetRowCellValue(i, "Conciliado") = False Then
                ChequesNoCobrados = ChequesNoCobrados + gvCheques.GetRowCellValue(i, "Valor")
            Else
                ChequesCobrados = ChequesCobrados + gvCheques.GetRowCellValue(i, "Valor")
            End If
            If gvCheques.GetRowCellValue(i, "Fecha") = dFechaCheques And gvCheques.GetRowCellValue(i, "Conciliado") = True Then
                TotalChequesDia = TotalChequesDia + gvCheques.GetRowCellValue(i, "Valor")
            End If
        Next
        teChequesMarcados.EditValue = ChequesCobrados
        teChequesNoMarcados.EditValue = ChequesNoCobrados
        teChequesDelDia.EditValue = TotalChequesDia

        For i = 0 To gvTransacciones.RowCount - 1
            If gvTransacciones.GetRowCellValue(i, "ProcesadaBanco") = False Then
                TransaccionesNoCobradas = TransaccionesNoCobradas + gvTransacciones.GetRowCellValue(i, "Valor")
            Else
                TransaccionesCobradas = TransaccionesCobradas + gvTransacciones.GetRowCellValue(i, "Valor")
            End If
            If gvTransacciones.GetRowCellValue(i, "Fecha") = dFechaTransacciones And gvTransacciones.GetRowCellValue(i, "ProcesadaBanco") = True Then
                TotalTransaccionesDia = TotalTransaccionesDia + gvTransacciones.GetRowCellValue(i, "Valor")
            End If
        Next

        teTransaccionesMarcadas.EditValue = TransaccionesCobradas
        teTransaccionesNoMarcadas.EditValue = TransaccionesNoCobradas
        teTransaccionesDelDia.EditValue = TotalTransaccionesDia
    End Sub

    Private Sub snConciliado_Leave(sender As Object, e As EventArgs) Handles snConciliado.Leave
        CalculoTotales()
    End Sub

    Private Sub gvCheques_RowClick(sender As Object, e As RowClickEventArgs) Handles gvCheques.RowClick
        CalculoTotales()
    End Sub

    Private Sub gvCheques_ValidateRow(sender As Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gvCheques.ValidateRow
        CalculoTotales()
    End Sub

    Private Sub gvTransacciones_RowClick(sender As Object, e As RowClickEventArgs) Handles gvTransacciones.RowClick
        CalculoTotales()
    End Sub

    Private Sub gvTransacciones_ValidateRow(sender As Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gvTransacciones.ValidateRow
        CalculoTotales()
    End Sub

    Private Sub gvTransacciones2_RowUpdated(sender As Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gvTransacciones2.RowUpdated

    End Sub
End Class
