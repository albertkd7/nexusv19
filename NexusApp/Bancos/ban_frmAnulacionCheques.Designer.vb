﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ban_frmAnulacionCheques
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl
        Me.sbAnular = New DevExpress.XtraEditors.SimpleButton
        Me.meConcepto = New DevExpress.XtraEditors.MemoEdit
        Me.teMotivo = New DevExpress.XtraEditors.TextEdit
        Me.teAnombreDe = New DevExpress.XtraEditors.TextEdit
        Me.teValor = New DevExpress.XtraEditors.TextEdit
        Me.teFechaCheque = New DevExpress.XtraEditors.TextEdit
        Me.teNumCheque = New DevExpress.XtraEditors.TextEdit
        Me.leTiposPartida = New DevExpress.XtraEditors.LookUpEdit
        Me.leCuentaBanco = New DevExpress.XtraEditors.LookUpEdit
        Me.deFecha = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teMotivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teAnombreDe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teValor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teFechaCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTiposPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCuentaBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.sbAnular)
        Me.GroupControl1.Controls.Add(Me.meConcepto)
        Me.GroupControl1.Controls.Add(Me.teMotivo)
        Me.GroupControl1.Controls.Add(Me.teAnombreDe)
        Me.GroupControl1.Controls.Add(Me.teValor)
        Me.GroupControl1.Controls.Add(Me.teFechaCheque)
        Me.GroupControl1.Controls.Add(Me.teNumCheque)
        Me.GroupControl1.Controls.Add(Me.leTiposPartida)
        Me.GroupControl1.Controls.Add(Me.leCuentaBanco)
        Me.GroupControl1.Controls.Add(Me.deFecha)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(666, 373)
        Me.GroupControl1.TabIndex = 31
        Me.GroupControl1.Text = "Datos para anular el cheque"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(44, 31)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(116, 13)
        Me.LabelControl10.TabIndex = 49
        Me.LabelControl10.Text = "Banco/Cuenta Bancaria:"
        '
        'sbAnular
        '
        Me.sbAnular.Location = New System.Drawing.Point(166, 297)
        Me.sbAnular.Name = "sbAnular"
        Me.sbAnular.Size = New System.Drawing.Size(111, 35)
        Me.sbAnular.TabIndex = 40
        Me.sbAnular.Text = "&Proceder a Anular"
        '
        'meConcepto
        '
        Me.meConcepto.Location = New System.Drawing.Point(166, 155)
        Me.meConcepto.Name = "meConcepto"
        Me.meConcepto.Properties.ReadOnly = True
        Me.meConcepto.Size = New System.Drawing.Size(421, 55)
        Me.meConcepto.TabIndex = 36
        '
        'teMotivo
        '
        Me.teMotivo.Location = New System.Drawing.Point(165, 265)
        Me.teMotivo.Name = "teMotivo"
        Me.teMotivo.Size = New System.Drawing.Size(421, 20)
        Me.teMotivo.TabIndex = 39
        '
        'teAnombreDe
        '
        Me.teAnombreDe.Location = New System.Drawing.Point(166, 78)
        Me.teAnombreDe.Name = "teAnombreDe"
        Me.teAnombreDe.Properties.AllowFocused = False
        Me.teAnombreDe.Properties.ReadOnly = True
        Me.teAnombreDe.Size = New System.Drawing.Size(421, 20)
        Me.teAnombreDe.TabIndex = 33
        '
        'teValor
        '
        Me.teValor.Location = New System.Drawing.Point(166, 130)
        Me.teValor.Name = "teValor"
        Me.teValor.Properties.AllowFocused = False
        Me.teValor.Properties.ReadOnly = True
        Me.teValor.Size = New System.Drawing.Size(100, 20)
        Me.teValor.TabIndex = 35
        '
        'teFechaCheque
        '
        Me.teFechaCheque.Location = New System.Drawing.Point(166, 103)
        Me.teFechaCheque.Name = "teFechaCheque"
        Me.teFechaCheque.Properties.AllowFocused = False
        Me.teFechaCheque.Properties.ReadOnly = True
        Me.teFechaCheque.Size = New System.Drawing.Size(100, 20)
        Me.teFechaCheque.TabIndex = 34
        '
        'teNumCheque
        '
        Me.teNumCheque.EnterMoveNextControl = True
        Me.teNumCheque.Location = New System.Drawing.Point(166, 54)
        Me.teNumCheque.Name = "teNumCheque"
        Me.teNumCheque.Size = New System.Drawing.Size(100, 20)
        Me.teNumCheque.TabIndex = 32
        '
        'leTiposPartida
        '
        Me.leTiposPartida.Location = New System.Drawing.Point(165, 239)
        Me.leTiposPartida.Name = "leTiposPartida"
        Me.leTiposPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTiposPartida.Size = New System.Drawing.Size(239, 20)
        Me.leTiposPartida.TabIndex = 38
        '
        'leCuentaBanco
        '
        Me.leCuentaBanco.EnterMoveNextControl = True
        Me.leCuentaBanco.Location = New System.Drawing.Point(166, 28)
        Me.leCuentaBanco.Name = "leCuentaBanco"
        Me.leCuentaBanco.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCuentaBanco.Size = New System.Drawing.Size(420, 20)
        Me.leCuentaBanco.TabIndex = 31
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.Location = New System.Drawing.Point(165, 215)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 37
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(60, 268)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(99, 13)
        Me.LabelControl7.TabIndex = 46
        Me.LabelControl7.Text = "Motivo de anulación:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(39, 242)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl8.TabIndex = 47
        Me.LabelControl8.Text = "Tipo de Partida a Utilizar:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(63, 218)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(96, 13)
        Me.LabelControl4.TabIndex = 44
        Me.LabelControl4.Text = "Fecha de anulación:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(54, 157)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(105, 13)
        Me.LabelControl6.TabIndex = 41
        Me.LabelControl6.Text = "Concepto del cheque:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(76, 133)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl5.TabIndex = 42
        Me.LabelControl5.Text = "Valor del cheque:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(71, 107)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(88, 13)
        Me.LabelControl1.TabIndex = 43
        Me.LabelControl1.Text = "Fecha del cheque:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(94, 80)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl3.TabIndex = 45
        Me.LabelControl3.Text = "A nombre de:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(65, 56)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl2.TabIndex = 48
        Me.LabelControl2.Text = "Número de cheque:"
        '
        'ban_frmAnulacionCheques
        '
        Me.ClientSize = New System.Drawing.Size(666, 398)
        Me.Controls.Add(Me.GroupControl1)
        Me.Modulo = "Bancos"
        Me.Name = "ban_frmAnulacionCheques"
        Me.OptionId = "002003"
        Me.Text = "Anulación de cheques"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teMotivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teAnombreDe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teValor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teFechaCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTiposPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCuentaBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbAnular As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents meConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents teMotivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teAnombreDe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teValor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teFechaCheque As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNumCheque As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leTiposPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leCuentaBanco As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl

End Class
