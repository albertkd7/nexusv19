﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns


Public Class ban_frmCheques
    Dim bl As New BancosBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim blCompras As New ComprasBLL(g_ConnectionString)
    Dim ChequeDetalle As List(Of ban_ChequesDetalle)
    Dim entCuentas As con_Cuentas, ChequeHeader As ban_Cheques
    Dim bFlagDetalleAgregado = False, ElUsuarioDioClic As Boolean
    Dim sConcepto As String = "", dtParam As DataTable = blAdmon.ObtieneParametros()
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim FechaConsulta As String = String.Format("{0:yyyyMMdd}", dtParam.Rows(0).Item("FechaCierre"))
    Dim IdsComprasCaja As List(Of Integer) = New List(Of Integer)
    Dim _dtLiquidacion As DataTable
    Property dtLiquidacion() As DataTable
        Get
            Return _dtLiquidacion
        End Get
        Set(ByVal value As DataTable)
            _dtLiquidacion = value
        End Set
    End Property

    Private Sub ban_frmCheques_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        gc2.DataSource = bl.ConsultaCheques(objMenu.User, FechaConsulta).Tables(0)
        gv2.BestFitColumns()
    End Sub
    Private Sub ban_frmCheques_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        objCombos.conTiposPartida(leTipoPartida)
        objCombos.banCuentasBancarias(leCtaBancaria)
        objCombos.con_CentrosCosto(rileIdCentro, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")

        teIdCheque.EditValue = objFunciones.ObtenerUltimoId("BAN_CHEQUES", "IdCheque")

        'CargaControles(0)
        ActivarControles(False)


    End Sub
    Private Sub ban_frmCheques_Nuevo() Handles Me.Nuevo
        ActivarControles(True)
        LimpiarControles(True)

        leCtaBancaria.EditValue = 1
        Dim entCta As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(leCtaBancaria.EditValue)
        teNumCheque.EditValue = bl.GetNumeroCheque(leCtaBancaria.EditValue)
        leTipoPartida.EditValue = entCta.IdTipoPartida
        xtcCheques.SelectedTabPage = xtpCheques
        dtLiquidacion = New DataTable
        dtLiquidacion = blCompras.com_ObtenerEstructuraIdCompra()
    End Sub
    Private Sub ban_frmCheques_Editar() Handles Me.Editar
        ActivarControles(True)
        teNumCheque.Focus()

        dtLiquidacion = New DataTable
        dtLiquidacion = blCompras.com_ObtenerEstructuraIdCompra()
    End Sub
    Private Sub ban_frmCheques_Guardar() Handles Me.Guardar
        If Not DatosValidos() Then
            Exit Sub
        End If
        CargaEntidades()

        If DbMode = DbModeType.insert Then
            ChequeHeader.IdCheque = 0
        End If
        'Dim Contabilizar As Boolean = gsNombre_Empresa.StartsWith("PITUTA")

        Dim msj As String = bl.ban_InsertarCheque(ChequeHeader, ChequeDetalle, True)
        If msj = "Ok" Then
            MsgBox("El cheque fue guardado con éxito", 64, "Nota")
            teNumPartida.EditValue = ChequeHeader.NumeroPartida
            'actualizo las compras, para que ya no aparezcan para liquidar la caja hica
            If DbMode = DbModeType.insert Then  'CUANDO SE ESTÁ CREANDO EL CHEQUE
                For i = 0 To dtLiquidacion.Rows.Count - 1
                    bl.ActualizaCompraLiquidada(dtLiquidacion.Rows(i).Item("IdComprobante"), ChequeHeader.IdCheque)
                Next
                For Each idc As Integer In IdsComprasCaja
                    bl.ActualizaCompraLiquidada(idc, ChequeHeader.IdCheque)
                Next
            End If
        Else
            MsgBox(String.Format("NO FUE POSIBLE GUARDAR EL CHEQUE{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        bFlagDetalleAgregado = False

        teIdCheque.EditValue = ChequeHeader.IdCheque
        xtcCheques.SelectedTabPage = xtpDatos
        gc2.DataSource = bl.ConsultaCheques(objMenu.User, FechaConsulta).Tables(0)
        gc2.Focus()
        MostrarModoInicial()
        ActivarControles(False)
    End Sub

    Private Sub ban_frmCheques_Consulta() Handles Me.Consulta
        ActivarControles(False)
        Dim Fecha As String = String.Format("{0:yyyyMMdd}", dtParam.Rows(0).Item("FechaCierre"))
        Try
            teIdCheque.EditValue = objConsultas.ConsultaCheques(frmConsultaDetalle, Fecha)
        Catch ex As Exception
            teIdCheque.EditValue = 0
        End Try


        ChequeHeader = objTablas.ban_ChequesSelectByPK(teIdCheque.EditValue)
        CargaPantalla()
        DbMode = DbModeType.update
        leCtaBancaria.Focus()
    End Sub

    Private Sub ban_frmCheques_Reporte() Handles Me.Reporte
        ChequeHeader = objTablas.ban_ChequesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdCheque"))

        If dtParam.Rows(0).Item("AutorizarCheques") And Not ChequeHeader.Impreso Then
            MsgBox("Debe autorizar el cheque para poder realizar la impresión", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim entCta = objTablas.ban_CuentasBancariasSelectByPK(ChequeHeader.IdCuentaBancaria)

        If entCta.TipoImpresion = 1 Then  'solo vaucher impreso
            ImprimeVaucher(entCta)
        End If
        If entCta.TipoImpresion = 2 Then  'cheque vaucher
            ImprimeCheque(entCta)
        End If
        If entCta.TipoImpresion = 3 Then  'vaucher y cheque
            ImprimeVaucher(entCta)
            ImprimeCheque(entCta)
        End If
    End Sub
    Private Sub ban_frmCheques_Eliminar() Handles Me.Eliminar
        ChequeHeader = objTablas.ban_ChequesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdCheque"))
        If ChequeHeader.Impreso Then
            MsgBox("No es posible eliminar un cheque que ya fue impreso", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim EsOk As Boolean = ValidarFechaCierre(ChequeHeader.Fecha)
        If Not EsOk Then
            MsgBox("No es posible eliminar el cheque" & Chr(13) & _
                   "La fecha corresponde a un período ya cerrado", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If MsgBox("Está seguro(a) de eliminar el cheque?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        ChequeHeader.ModificadoPor = objMenu.User
        ChequeHeader.FechaHoraModificacion = Now
        objTablas.ban_ChequesUpdate(ChequeHeader)

        objTablas.ban_ChequesDeleteByPK(ChequeHeader.IdCheque)
        objTablas.con_PartidasDeleteByPK(ChequeHeader.IdPartida)
        objFunciones.ActualizaAbono("CHEQUES", ChequeHeader.IdCheque)

        teIdCheque.EditValue = objFunciones.ObtenerUltimoId("BAN_CHEQUES", "IdCheque")
        gc2.DataSource = bl.ConsultaCheques(objMenu.User, FechaConsulta).Tables(0)
        ' CargaControles(0)
    End Sub
    Private Sub ban_frmCheques_Revertir() Handles Me.Revertir
        xtcCheques.SelectedTabPage = xtpDatos
        gc2.Focus()
        ActivarControles(False)
    End Sub
    Private Sub teValor_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles teValor.KeyDown
        If e.KeyCode = Keys.Enter And DbMode = DbModeType.insert And Not bFlagDetalleAgregado Then
            Dim sConcepto = meConcepto.EditValue

            Dim sIdCuenta = bl.GetCuentaCbleBanco(leCtaBancaria.EditValue)
            If gsNombre_Empresa.ToUpper.StartsWith("TRANS EXPRESS") Then
                sConcepto = ""
            End If
            sConcepto = String.Format("Cheque {0}. {1}. {2}", teNumCheque.EditValue, teANombreDe.EditValue, sConcepto)

            gv.AddNewRow()
            gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", sIdCuenta)
            gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", teNumCheque.EditValue)
            gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", sConcepto)
            gv.SetRowCellValue(gv.FocusedRowHandle, "Debe", 0.0)
            gv.SetRowCellValue(gv.FocusedRowHandle, "Haber", teValor.EditValue)
            gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", "")
            gv.UpdateCurrentRow()
            If Not gv.IsNewItemRow(gv.FocusedRowHandle) Then 'revisar este proceso, se debería de hacer en el gv
                'gv.AddNewRow()
                'gv.FocusedColumn = gv.Columns(0)
                gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
                gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", "")
                'gv.ActiveEditor.IsModified = True
            End If
            'If gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            'gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
            'End If
            'gv.AddNewRow()
            bFlagDetalleAgregado = True
        Else
            'Dim iRow As Integer = gv.RowCount
            'preguntar a victor
            'For Each row As GridColumn In gv.rows
            'gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            'Next
        End If
    End Sub
#Region "grid"
    'Private Sub gv_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.GotFocus
    '    SendKeys.Send("{Enter}")
    'End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub
    Private Sub cmdUP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUP.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle

        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        'gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", "Chq. " & teNumCheque.EditValue)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", sConcepto)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Debe", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Haber", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", "")
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        Dim IdCuenta As String = SiEsNulo(gv.GetFocusedRowCellValue("IdCuenta"), "")
        'Dim IdCentro As String = SiEsNulo(gv.GetFocusedRowCellValue("IdCentro"), "")

        If IdCuenta = "" Then
            MsgBox("Debe de especificar la cuenta contable", MsgBoxStyle.Critical, "Nota")
            e.Valid = False
            Exit Sub
        End If
        If gv.GetFocusedRowCellValue("Debe") <> 0 And gv.GetFocusedRowCellValue("Haber") <> 0 Then
            MsgBox("Solo debe colocar valores en debe o haber no en ambos", 16, "Error")
            e.Valid = False
            Exit Sub
        End If
        'If IdCentro = "" Then
        '    MsgBox("Es necesario especificar el centro de costo", MsgBoxStyle.Information, "Nota")
        '    e.Valid = False
        '    Exit Sub
        'End If
        'Dim i As Integer = bl.ValidaCentroCuenta(IdCuenta, IdCentro)
        'If i = 1 Then
        '    MsgBox("Es necesario definir el centro de costo a ésta partida", MsgBoxStyle.Information, "Nota")
        '    e.Valid = False
        '    Exit Sub
        '    'gv.SetColumnError(gv.Columns("IdCuenta"), "Esta cuenta no tiene asignado el centro de costo o está mal definido")
        'End If
        'If i = 2 Then
        '    e.Valid = False
        '    MsgBox("El centro de costo especificado no existe o debe de especificarlo para ésta cuenta", MsgBoxStyle.Information, "Nota")
        '    Exit Sub
        'End If
        'If i = 3 Then
        '    e.Valid = False
        '    MsgBox("El centro de costo especificado está mal asociado a la cuenta contable", MsgBoxStyle.Information, "Nota")
        '    Exit Sub
        'End If
        sConcepto = gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto")
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        seDiferencia.EditValue = gcDebe.SummaryItem.SummaryValue - gcHaber.SummaryItem.SummaryValue
    End Sub
    Private Sub gv_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gv.FocusedRowChanged
        entCuentas = objTablas.con_CuentasSelectByPK(SiEsNulo(gv.GetFocusedRowCellValue("IdCuenta"), ""))
        teNombreCuenta.EditValue = entCuentas.Nombre
    End Sub
    Private Sub riteIdCuenta_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles riteIdCuenta.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then

            gv.EditingValue = SiEsNulo(gv.EditingValue, "")

            entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, gv.EditingValue)
            gv.EditingValue = entCuentas.IdCuenta

            If entCuentas.IdCuenta = "" Then
                Exit Sub
            End If

            If Not entCuentas.EsTransaccional And entCuentas.IdCuenta <> "" Then
                MsgBox("La cuenta no permite aceptar transacciones" & Chr(13) & _
                       "Es de mayor o tiene subcuentas", 64, "Nota")
            End If
            teNombreCuenta.EditValue = entCuentas.Nombre

            If gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto") = "" Then
                sConcepto = entCuentas.Nombre
            End If
            gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", sConcepto)
        End If
    End Sub
    'Private Sub riteIdCuenta_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles riteIdCuenta.Leave
    '    If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCuenta"), "") = "" Then
    '        Exit Sub
    '    End If
    '    objCombos.con_CargarCentrosCostoCuentaMayor(rileIdCentro, gv.GetRowCellValue(gv.FocusedRowHandle, "IdCuenta"))
    'End Sub
#End Region
    'Private Sub teNumPartida_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles teNumPartida.LostFocus
    '    If gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
    '        gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
    '    End If
    'End Sub
    Private Sub CargaEntidades()
        If DbMode = DbModeType.insert Then
            ChequeHeader = New ban_Cheques
        End If
        With ChequeHeader
            .IdCuentaBancaria = leCtaBancaria.EditValue
            .Numero = teNumCheque.EditValue
            .Sufijo = teSufijo.EditValue
            .Fecha = deFecha.DateTime
            .Valor = teValor.EditValue
            .AnombreDe = teANombreDe.EditValue
            .Concepto = meConcepto.EditValue
            .IdProveedor = SiEsNulo(beProveedor.EditValue, "")
            .IdTipoPartida = leTipoPartida.EditValue
            .Impreso = False
            .Conciliado = False
            .Anulado = False
            .MotivoAnulacion = ""
            .PagadoBanco = True
            .IdSucursal = leSucursal.EditValue
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .FechaHoraModificacion = Nothing
                .ModificadoPor = ""
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If
        End With

        ChequeDetalle = New List(Of ban_ChequesDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New ban_ChequesDetalle
            With entDetalle
                .IdCheque = ChequeHeader.IdCheque
                .IdDetalle = i + 1
                .IdCuenta = gv.GetRowCellValue(i, "IdCuenta")
                .Referencia = gv.GetRowCellValue(i, "Referencia")
                .Concepto = gv.GetRowCellValue(i, "Concepto")
                .Debe = gv.GetRowCellValue(i, "Debe")
                .Haber = gv.GetRowCellValue(i, "Haber")
                .IdCentro = gv.GetRowCellValue(i, "IdCentro")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            ChequeDetalle.Add(entDetalle)
        Next
    End Sub
    Private Sub LimpiarControles(ByVal Value As Boolean)
        gv.CancelUpdateCurrentRow()
        If Value Then
            deFecha.EditValue = Today
            meConcepto.EditValue = ""
            teNumPartida.EditValue = ""
            teANombreDe.EditValue = ""
            teValor.EditValue = 0.0
            teSufijo.EditValue = ""
            If DbMode = DbModeType.insert Then
                gc.DataSource = bl.GetChequesDetalle(-1)
            End If
        End If
        leCtaBancaria.Focus()
        leSucursal.EditValue = piIdSucursalUsuario

    End Sub
    Private Function DatosValidos() As Boolean
        If SiEsNulo(teNumCheque.EditValue, "") = "" Or meConcepto.EditValue = "" Or teANombreDe.EditValue = "" Then
            MsgBox("Existen datos que no pueden quedar vacíos" & Chr(13) &
                   "Imposible guardar el cheque", 16, "Nota")
            Return False
        End If

        For i = 0 To gv.DataRowCount - 1
            Dim entCta As con_Cuentas = objTablas.con_CuentasSelectByPK(gv.GetRowCellValue(i, "IdCuenta"))
            If entCta.EsTransaccional = 0 Then
                MsgBox("La cuenta: " & entCta.IdCuenta & ", tiene sub-cuentas" & Chr(13) & "Solo puede usar cuentas de detalle", 16, "Error de datos")
                Return False
            End If
        Next

        If seDiferencia.EditValue <> 0.0 Then
            If dtParam.Rows(0).Item("GuardarDesbalance") Then
                If MsgBox("La partida no está cuadrada" & Chr(13) & _
                      "Está seguro(a) de continuar?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
                    Return False
                End If
            Else
                MsgBox("La partida no está cuadrada", MsgBoxStyle.Critical, "Imposible salir")
                Return False
            End If
        End If

        If leSucursal.EditValue = 0 Or leSucursal.EditValue = Nothing Or leSucursal.Text = "" Then
            MsgBox("Debe seleccionar una sucursal", 64, "Nota")
            Return False
        End If

        If SiEsNulo(leTipoPartida.EditValue, "") = "" Or SiEsNulo(leTipoPartida.Text, "") = "" Then
            MsgBox("Debe seleccionar una tipo de partida", 64, "Nota")
            Return False
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            MsgBox("Fecha del cheque corresponde a un período ya cerrado", 64, "Nota")
            Return False
        End If
        If bl.ExisteCheque(leCtaBancaria.EditValue, teNumCheque.EditValue) > 0 And DbMode = DbModeType.insert Then
            MsgBox("Ya existe un cheque con este número", MsgBoxStyle.Critical)
            Return False
        End If
        Return True
    End Function

    Private Sub leCtaBancaria_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leCtaBancaria.EditValueChanged
        If DbMode = DbModeType.insert Then
            teNumCheque.EditValue = bl.GetNumeroCheque(leCtaBancaria.EditValue)
        End If
    End Sub
    
    'Public Sub CargaControles(ByVal TipoAvance As Integer)
    '    'If TipoAvance = 0 Then 'no es necesario obtener el Id
    '    'Else
    '    '    teIdCheque.EditValue = bl.ObtieneIdCheque(teIdCheque.EditValue, TipoAvance)
    '    'End If
    '    'If teIdCheque.EditValue = 0 Then
    '    '    teIdCheque.EditValue = 1
    '    'End If
    '    'ChequeHeader = objTablas.ban_ChequesSelectByPK(teIdCheque.EditValue)
    '    'If ChequeHeader.Numero = "" Then
    '    '    Exit Sub
    '    'End If
    '    'With ChequeHeader
    '    '    teIdCheque.EditValue = .IdCheque
    '    '    teIdPartida.EditValue = .IdPartida
    '    '    leCtaBancaria.EditValue = .IdCuentaBancaria
    '    '    teNumCheque.EditValue = .Numero
    '    '    beProveedor.EditValue = .IdProveedor
    '    '    teANombreDe.EditValue = .AnombreDe
    '    '    meConcepto.EditValue = .Concepto
    '    '    deFecha.DateTime = .Fecha
    '    '    teValor.EditValue = .Valor
    '    '    leTipoPartida.EditValue = .IdTipoPartida
    '    '    teNumPartida.EditValue = .NumeroPartida
    '    '    leSucursal.EditValue = .IdSucursal
    '    '    gc.DataSource = bl.GetChequesDetalle(.IdCheque)
    '    '    Dim entCuentaBancaria As New ban_CuentasBancarias
    '    '    entCuentaBancaria = objTablas.ban_CuentasBancariasSelectByPK(ChequeHeader.IdCuentaBancaria)
    '    '    leCtaBancaria.EditValue = entCuentaBancaria.IdCuentaBancaria
    '    'End With
    '    ''CalculaTotales()
    'End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
        teIdCheque.Properties.ReadOnly = True
        teNumCheque.Properties.ReadOnly = True
        teIdPartida.Properties.ReadOnly = True

        'If Tipo = True Then
        '    objCombos.banCuentasBancarias(leCtaBancaria, "NuevoEditar")
        'Else
        '    objCombos.banCuentasBancarias(leCtaBancaria)
        'End If

    End Sub

    Private Sub CargaPantalla()
        xtcCheques.SelectedTabPage = xtpCheques
        leCtaBancaria.Focus()

        With ChequeHeader
            teIdCheque.EditValue = .IdCheque
            teIdPartida.EditValue = .IdPartida
            leCtaBancaria.EditValue = .IdCuentaBancaria
            teNumCheque.EditValue = .Numero
            teSufijo.EditValue = .Sufijo
            beProveedor.EditValue = .IdProveedor
            teANombreDe.EditValue = .AnombreDe
            meConcepto.EditValue = .Concepto
            deFecha.DateTime = .Fecha
            teValor.EditValue = .Valor
            leTipoPartida.EditValue = .IdTipoPartida
            teNumPartida.EditValue = .NumeroPartida
            leSucursal.EditValue = .IdSucursal
            gc.DataSource = bl.GetChequesDetalle(.IdCheque)
            Dim entCuentaBancaria As New ban_CuentasBancarias
            entCuentaBancaria = objTablas.ban_CuentasBancariasSelectByPK(SiEsNulo(ChequeHeader.IdCuentaBancaria, 1))
            leCtaBancaria.EditValue = entCuentaBancaria.IdCuentaBancaria
        End With

    End Sub

    Private Sub beProveedor_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beProveedor.ButtonClick
        beProveedor.EditValue = ""

        beProveedor_Validated(sender, New System.EventArgs)
        'beCodigo.EditValue = ""
        ElUsuarioDioClic = True
        'beCodigo_Validated(sender, New System.EventArgs)
    End Sub
    Private Sub beProveedor_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beProveedor.Validated
        If Not ElUsuarioDioClic And beProveedor.EditValue = "" Then
            Exit Sub
        End If
        Dim ent As com_Proveedores = objConsultas.cnsProveedores(frmConsultas, beProveedor.EditValue)
        ElUsuarioDioClic = False

        beProveedor.EditValue = ent.IdProveedor
        teANombreDe.EditValue = ent.Nombre
        teANombreDe.Focus()
    End Sub
    Private Sub sbAgregarCompra_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbAgregarCompra.Click

        Form1.ShowDialog()

    End Sub
    Private Sub ImprimeVaucher(ByRef entCta As ban_CuentasBancarias)

        Dim dt As DataTable = bl.ReporteCheques(ChequeHeader.IdCheque, 0)
        Dim sDecimal As String = ""
        sDecimal = String.Format("{0:c}", ChequeHeader.Valor)

        sDecimal = sDecimal.Substring(sDecimal.Length - 2) & "/100"

        Dim rpt As New Vaucher() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlNumeroCheque.Text = ChequeHeader.Numero
        rpt.xrlNumeroCuenta.Text = entCta.NumeroCuenta
        rpt.xrlValorCheque.Text = "***" & Format(ChequeHeader.Valor, "###,###,###.00")
        rpt.xrlFecha.Text = dtParam.Rows(0).Item("Domicilio") & ", " & (FechaToString(ChequeHeader.Fecha, ChequeHeader.Fecha)).ToUpper
        rpt.xrlANombreDe.Text = ChequeHeader.AnombreDe
        rpt.xrlCantidad.Text = Num2Text(Int(ChequeHeader.Valor)) & " " & sDecimal & " DÓLARES"
        rpt.xrlIdPartida.Text = ChequeHeader.IdCheque
        rpt.xrlNoPartida.Text = ChequeHeader.IdTipoPartida & "-" & ChequeHeader.NumeroPartida
        rpt.xrlConcepto.Text = ChequeHeader.Concepto
        rpt.xrlBanco.Text = entCta.Nombre
        rpt.xrlHecho.Text = entCta.NombreDigitador
        rpt.xrlRevisado.Text = entCta.NombreRevisa
        rpt.xrlAutorizado.Text = entCta.NombreAutoriza
        rpt.ShowPreviewDialog()
    End Sub
    Private Sub ImprimeCheque(ByRef entCta As ban_CuentasBancarias)
        Dim Tipo As Integer = 0 'indica si la columna del debe y el haber se sumna
        If entCta.IdFormatoVaucher = 2 Then 'citi
            Tipo = 1
        End If
        Dim dt As DataTable = bl.ReporteCheques(ChequeHeader.IdCheque, Tipo)
        Dim sDecimal As String = ""
        '1=Agricola, 2=Citi, 3=HSBC, 4=Scotiabank, 5=America Central, 6=Promerica, 7=Hipotecario, 8=Procredit, 9=Industrial/GYT
        Dim rpt As New ban_rptCheque01
        Dim Template = Application.StartupPath & "\Plantillas\cheque" & entCta.IdFormatoVaucher.ToString.PadLeft(2, "0") & ".repx"
        If Not FileIO.FileSystem.FileExists(Template) Then
            MsgBox("No existe la plantilla necesaria para imprimir el cheque", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        sDecimal = String.Format("{0:c}", ChequeHeader.Valor)
        sDecimal = sDecimal.Substring(sDecimal.Length - 2) & "/100" & "****"
        Dim Valor As String = "********************" + Format(ChequeHeader.Valor, "###,###,##0.00")
        Valor = Valor.Substring(Valor.Length - 15, 15)

        rpt.LoadLayout(Template)
        rpt.DataSource = dt
        rpt.DataMember = ""
        rpt.PrinterName = dtParam.Rows(0).Item("ImpresorCheques")
        If entCta.IdFormatoVaucher = 1 Or entCta.IdFormatoVaucher = 5 Then 'AGRICOLA Y AMERICA CENTRAL
            Dim LugarFecha As String = dtParam.Rows(0).Item("Domicilio") & ", " & CDate(ChequeHeader.Fecha).Day.ToString() & " de "
            LugarFecha += ObtieneMesString(CDate(ChequeHeader.Fecha).Month)
            rpt.xrlLugarFecha.Text = LugarFecha
            rpt.xrlAnio.Text = CDate(ChequeHeader.Fecha).Year.ToString()
        Else
            If entCta.IdFormatoVaucher = 4 Then 'ScotiaBank
                rpt.xrlLugarFecha.Text = String.Format("{0} ", dtParam.Rows(0).Item("Domicilio"))
                rpt.xrlDia.Text = CDate(ChequeHeader.Fecha).Day.ToString()
                rpt.xrlMes.Text = ObtieneMesString(CDate(ChequeHeader.Fecha).Month)
                rpt.xrlAnio.Text = CDate(ChequeHeader.Fecha).Year.ToString()
            Else 'Cheque 2, 3, 6, 7, 8 y 9
                rpt.xrlLugarFecha.Text = String.Format("{0}, {1}", dtParam.Rows(0).Item("Domicilio"), FechaToString(ChequeHeader.Fecha, ChequeHeader.Fecha))
            End If
        End If
        rpt.xrlValorCheque.Text = Valor 'Format(ChequeHeader.Valor, "###,###,##0.00") 'ChequeHeader.Valor
        rpt.xrlANombreDe.Text = ChequeHeader.AnombreDe
        rpt.xrlCantidad.Text = String.Format("{0} {1}", Num2Text(Int(ChequeHeader.Valor)), sDecimal)
        rpt.xrlConcepto.Text = ChequeHeader.Concepto
        rpt.ShowPrintMarginsWarning = False

        If dtParam.Rows(0).Item("PreviewCheques") Then
            rpt.ShowPreviewDialog()
        Else
            rpt.PrintDialog()
        End If
    End Sub
    'Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
    '    If gv.FocusedColumn.FieldName = "Debe" Or gv.FocusedColumn.FieldName = "Haber" Then
    '        'gv.UpdateSummary()
    '        'gv.UpdateCurrentRow()
    '        seDiferencia.EditValue = gcDebe.SummaryItem.SummaryValue - gcHaber.SummaryItem.SummaryValue
    '    End If
    'End Sub
    Private Sub gv_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles gv.CellValueChanged
        If gv.FocusedRowHandle >= 0 And (gv.FocusedColumn.Name = "gcDebe" Or gv.FocusedColumn.Name = "gcHaber") Then
            gv.UpdateCurrentRow()
        End If
    End Sub


    Private Sub sbDesembolso_Click(ByVal sender As Object, ByVal e As EventArgs) Handles sbLiquidaCaja.Click
        If rbMenu.qbSave.Visibility = DevExpress.XtraBars.BarItemVisibility.Never Then
            Exit Sub
        End If

        dtLiquidacion = New DataTable
        dtLiquidacion = blCompras.com_ObtenerEstructuraIdCompra()

        Dim frmLiquidaCaja As New ban_frmLiquidarCajaChica
        Me.AddOwnedForm(frmLiquidaCaja)
        frmLiquidaCaja.IdCaja = 0
        frmLiquidaCaja.NumerosComprobantes = ""
        frmLiquidaCaja.TotalLiquidacion = 0.0
        frmLiquidaCaja.ShowDialog()

        Dim sConcepto = frmLiquidaCaja.NumerosComprobantes
        Dim entCaja As com_CajasChica = objTablas.com_CajasChicaSelectByPK(SiEsNulo(frmLiquidaCaja.IdCaja, 0))
        Dim sIdCuenta = entCaja.IdCuentaContable
        Dim Valor As Decimal = frmLiquidaCaja.TotalLiquidacion
        IdsComprasCaja = frmLiquidaCaja.IdsCompras
        sConcepto = String.Format("Cheque {0}. {1}. {2}", teNumCheque.EditValue, teANombreDe.EditValue, sConcepto)

        gv.AddNewRow()
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", entCaja.IdCuentaContable)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", teNumCheque.EditValue)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", sConcepto)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Debe", Valor)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Haber", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", "")
        gv.UpdateCurrentRow()

        If Not gv.IsNewItemRow(gv.FocusedRowHandle) Then 'revisar este proceso, se debería de hacer en el gv
            gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
            gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", "")
        End If
    End Sub
    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        ChequeHeader = objTablas.ban_ChequesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdCheque"))
        CargaPantalla()
        DbMode = DbModeType.update
        leCtaBancaria.Focus()
    End Sub


End Class
