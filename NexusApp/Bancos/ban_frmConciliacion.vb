﻿Imports DevExpress.XtraReports.UI
Imports NexusBLL
Public Class ban_frmConciliacion
    Dim bl As New BancosBLL(g_ConnectionString)
    Dim dtFecha As Date
    Dim sCtaContable As String
    Dim Generada As Boolean

    
    Private Sub frmConciliacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        teSaldoBanco.EditValue = 0.0
        seEjercicio.EditValue = Now.Year
        meMes.Month = Now.Month

        objCombos.banCuentasBancarias(leCtaBancaria)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        'leSucursal.EditValue = piIdSucursal
    End Sub

    Private Sub sbGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGenerar.Click
        Dim mes As String = meMes.EditValue
        Dim year As String = seEjercicio.EditValue

        dtFecha = year & "/" & mes.PadLeft(2, "0") & "/01"
        dtFecha = DateAdd(DateInterval.Month, 1, dtFecha)
        dtFecha = DateAdd(DateInterval.Day, -1, dtFecha)

        sCtaContable = bl.GetCuentaCbleBanco(leCtaBancaria.EditValue)

        teSaldoContab.EditValue = bl.GetSaldoCuentaBanco(sCtaContable, dtFecha, leSucursal.EditValue )
        teCargosNoContab.EditValue = bl.GetCargosNoContab(sCtaContable, dtFecha, leSucursal.EditValue)
        teDepositosNoContab.EditValue = bl.GetDepositosNoContab(sCtaContable, dtFecha, leSucursal.EditValue)
        teDepositosTransito.EditValue = bl.GetDepositosTransito(sCtaContable, dtFecha, leSucursal.EditValue)
        teChequesPendientes.EditValue = bl.GetChequesPendientes(sCtaContable, dtFecha, leSucursal.EditValue)
        teCargosPendientes.EditValue = bl.GetCargosPendientes(sCtaContable, dtFecha, leSucursal.EditValue)
        'llamar el store de los ban_movimientos_pendientes mientras no este cerrado
        Dim dSaldoContab As Decimal = teSaldoContab.EditValue
        Dim dDepositosNC As Decimal = teDepositosNoContab.EditValue
        Dim dDepositosTr As Decimal = teDepositosTransito.EditValue
        Dim deSaldoBanco As Decimal = teSaldoBanco.EditValue
        Dim dChequesPend As Decimal = teChequesPendientes.EditValue
        Dim dCargosNoCon As Decimal = teCargosNoContab.EditValue
        Dim dCargosPend As Decimal = teCargosPendientes.EditValue
        'Dim sCtaBanco = cboCuentaBanco.SelectedValue
        bl.GeneraConciliacion(leCtaBancaria.EditValue, sCtaContable, dtFecha _
                              , dSaldoContab, dCargosNoCon _
                              , dDepositosNC, dChequesPend _
                              , dCargosPend, dDepositosTr, deSaldoBanco, leSucursal.EditValue)
        Generada = True
    End Sub


    Private Sub frmConciliacion_Report_Click() Handles Me.Reporte
        If Not Generada Then
            MsgBox("Debe generar la conciliación antes de imprimir", MsgBoxStyle.OkOnly, "Nota")
            Exit Sub
        End If
        dtFecha = seEjercicio.EditValue & "/" & meMes.EditValue.ToString.PadLeft(2, "0") & "/01"
        dtFecha = DateAdd(DateInterval.Month, 1, dtFecha)
        dtFecha = DateAdd(DateInterval.Day, -1, dtFecha)

        'Dim dt As DataTable = db.ExecuteDataSet("ban_ObtieneConciliacion", cboCuentaBanco.SelectedValue, dtFecha).Tables(0)
        Dim dt As DataTable = bl.ObtieneConciliacion(leCtaBancaria.EditValue, dtFecha, leSucursal.EditValue)
        Dim rpt As New ban_rptConciliacion() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlPeriodo.Text = "CORRESPONDIENTE AL MES DE " & (ObtieneMesString(meMes.Month)).ToUpper & " DE " & seEjercicio.Text
        rpt.xrlCuentaContable.Text = "CUENTA CONTABLE: " & bl.GetCuentaCbleBanco(leCtaBancaria.EditValue)
        rpt.xrlBanco.Text = "BANCO: " & leCtaBancaria.Text
        rpt.xrlSaldoConta.Text = Format(teSaldoContab.EditValue, "###,###,###,###.00")
        rpt.xrlCargosNoContab.Text = Format(teCargosNoContab.EditValue, "###,###,###,###.00")
        rpt.xrlDepNoConta.Text = Format(teDepositosNoContab.EditValue, "###,###,###,###.00")
        rpt.xrlTotalConta.Text = Format(teSaldoContab.EditValue - teCargosNoContab.EditValue + teDepositosNoContab.EditValue, "###,###,###,###.00")

        rpt.xrlSaldoBanco.Text = Format(teSaldoBanco.EditValue, "###,###,###,###.00")
        rpt.xrlDepositosPend.Text = Format(teDepositosTransito.EditValue, "###,###,###,###.00")
        rpt.xrlCargosPend.Text = Format(teCargosPendientes.EditValue, "###,###,###,###.00")
        rpt.xrlChequesPend.Text = Format(teChequesPendientes.EditValue, "###,###,###,###.00")
        rpt.xrlTotalBanco.Text = Format(teSaldoBanco.EditValue + teDepositosTransito.EditValue - teCargosPendientes.EditValue - teChequesPendientes.EditValue, "###,###,###,###.00")
        rpt.XrLHechoPor.Text = teHechoPor.Text
        rpt.XrLAutorizadoPor.Text = TeAutorizadoPor.Text
        rpt.XrlRevisadoPor.Text = TeRevisadoPor.Text
        rpt.ShowPreviewDialog()
    End Sub


End Class
