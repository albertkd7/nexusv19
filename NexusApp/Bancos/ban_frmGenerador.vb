﻿Imports NexusBLL
Imports System.IO
Public Class ban_frmGenerador
    Dim bl As New BancosBLL(g_ConnectionString)
    Dim ShowColumns As Boolean = True
    
    Private Sub ban_frmGenerador_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFecIni.EditValue = Today
        deFecFin.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        gv.ColumnsCustomization()
    End Sub
    Private Sub ShowColumnSelector()
        If ShowColumns Then
            gv.ColumnsCustomization()
            sbMostrarOcultar.Text = "Ocultar &Selector de Columnas"
        Else
            gv.DestroyCustomization()
            sbMostrarOcultar.Text = "Mostrar &Selector de Columnas"
        End If
    End Sub

    Private Sub sbGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGenerar.Click
        gc.DataSource = bl.ban_ObtieneDataBancos(deFecIni.EditValue, deFecFin.EditValue, leSucursal.EditValue)
        gv.BestFitColumns()
    End Sub
    Private Sub sbMostrarOcultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbMostrarOcultar.Click
        ShowColumns = Not ShowColumns
        ShowColumnSelector()
    End Sub
    
    Private Function ObtieneNombreArchivo() As String
        Dim NombreArchivo As String = "bancos" & Today.Year.ToString.PadLeft(4, "0") & Today.Month.ToString.PadLeft(2, "0") & Today.Day.ToString.PadLeft(2, "0")
        NombreArchivo = InputBox("Nombre del archivo", "Digite el nombre del archivo", NombreArchivo)

        Dim myFolderBrowserDialog As New FolderBrowserDialog

        With myFolderBrowserDialog

            .RootFolder = Environment.SpecialFolder.Desktop
            .SelectedPath = "c:\"
            .Description = "Seleccione la carpeta destino"
            If .ShowDialog = DialogResult.OK Then
                NombreArchivo = .SelectedPath & "\" & NombreArchivo
            End If
        End With
        Return NombreArchivo
    End Function
    Private Sub sbToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbToExcel.Click
        Dim NombreArchivo As String = ""
        NombreArchivo = ObtieneNombreArchivo() & ".xls"
        gc.ExportToXls(NombreArchivo)

        MsgBox("El documento ha sido exportado con éxito en formato Excel", 64, "Nota")
    End Sub
    Private Sub sbToPdf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbToPdf.Click
        Dim NombreArchivo As String = ""
        NombreArchivo = ObtieneNombreArchivo() & ".pdf"
        gc.ExportToPdf(NombreArchivo)

        MsgBox("El documento ha sido exportado con éxito en formato PDF", 64, "Nota")
    End Sub
    Private Sub sbToText_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbToText.Click
        Dim NombreArchivo As String = ""
        NombreArchivo = ObtieneNombreArchivo() & ".txt"
        gc.ExportToText(NombreArchivo)

        MsgBox("El documento ha sido exportado con éxito en formato de texto", 64, "Nota")
    End Sub
End Class
