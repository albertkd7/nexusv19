﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports System.Net
Imports System.IO

Public Class Form1
    Dim bl As New BancosBLL(g_ConnectionString)
    Dim ChequeDetalle As List(Of ban_ChequesDetalle)
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        objCombos.banCuentasBancarias(leCtaBancaria)
        deFecha.EditValue = Today
    End Sub

    Private Sub sbAceptar_Click(sender As Object, e As EventArgs) Handles sbAceptar.Click
        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            MsgBox("Fecha del cheque corresponde a un período ya cerrado", 64, "Nota")
            Exit Sub
        End If

        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            MsgBox("No seleccionó ningun archivo", MsgBoxStyle.Exclamation, "Nota")
            Return
        End If

        Dim ExcObj As New Object
        Try
            ExcObj = CreateObject("EXCEL.APPLICATION")
            ExcObj.WORKBOOKS.Open(ofd.FileName)
        Catch ex As Exception
            MsgBox("No se pudo abrir el archivo o no tiene el formato correcto")
            Exit Sub
        End Try

        'For x = 1 To ExcObj.Sheets.Count
        'ExcObj.Sheets(x).Select()
        'Next

        ExcObj.Sheets(1).Select()
        'Dim lnCol As Integer = ExcObj.ActiveSheet.UsedRange.COLUMNS.Count
        Dim TotFilas As Integer = ExcObj.ActiveSheet.UsedRange.ROWS.COUNT
        If TotFilas < 2 Then
            MsgBox("No se encontró ningun dato para hacer la importación" & Chr(13) & "Recuerde que la fila 1 deben contener los títulos", MsgBoxStyle.Information, "Nota")
            ExcObj.WORKBOOKS.Close()
            Exit Sub
        End If
        Dim entCD1 As New ban_ChequesDetalle, entCD2 As New ban_ChequesDetalle
        Dim Nombre As String, Monto As Decimal, Concepto As String, ANombreDe As String, IdCuentaGasto As String
        Dim entCtaBancaria As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(leCtaBancaria.EditValue)
        Dim Procede As Boolean, msj As String
        If MsgBox("Está seguro(a) de hacer esta importación de cheques?" & Chr(13) & "Este proceso afectará tambien la contabilidad" & Chr(13) & "Se emitirán " & TotFilas - 1 & " cheques", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            ExcObj.WORKBOOKS.Close()
            ExcObj = Nothing
            Exit Sub
        End If
        For nf = 2 To TotFilas
            '1=Nombre, 2=Monto, 3=Concepto, 4=AnombreDe, 5=Cuenta
            Nombre = SiEsNulo(ExcObj.activesheet.cells(nf, 1).VALUE, "")
            Monto = SiEsNulo(ExcObj.activesheet.cells(nf, 2).VALUE, 0)
            Concepto = SiEsNulo(ExcObj.activesheet.cells(nf, 3).VALUE, "")
            ANombreDe = SiEsNulo(ExcObj.activesheet.cells(nf, 4).VALUE, "")
            IdCuentaGasto = SiEsNulo(ExcObj.activesheet.cells(nf, 5).VALUE, "")
            Dim entCta As con_Cuentas = objTablas.con_CuentasSelectByPK(IdCuentaGasto)

            If entCta.Nombre = "" Then
                MsgBox("La cuenta " & IdCuentaGasto & " no existe. El cheque no será generado")
                Continue For
            End If
            Procede = True
            If Nombre = "" Then
                MsgBox("El registro de la fila " & nf & " no tiene el nombre" & Chr(13) & "El cheque no será emitido")
                Procede = False
            Else
                If ANombreDe = "" Then
                    MsgBox("El registro de la fila " & nf & " no tiene a nombre de quien se emitirá el cheque." & Chr(13) & "El cheque no será emitido")
                    Procede = False
                Else
                    If Monto = 0.0 Then
                        MsgBox("El registro de la fila " & nf & " no tiene el monto del cheque" & Chr(13) & "El cheque no será emitido")
                        Procede = False
                    Else
                        If IdCuentaGasto = "" Then
                            MsgBox("El registro de la fila " & nf & " no tiene la cuenta contable." & Chr(13) & "El cheque no será emitido")
                            Procede = False
                        End If
                    End If
                End If
            End If
            If Not Procede Then
                Continue For
            End If

            'LLENANDO LAS ENTIDADES
            Dim entCheque As New ban_Cheques, ChequeDetalle = New List(Of ban_ChequesDetalle)
            With entCheque
                .IdCuentaBancaria = leCtaBancaria.EditValue
                .Numero = entCtaBancaria.UltNumeroCheque + 1
                .Sufijo = "0"
                .Fecha = deFecha.EditValue
                .Valor = Monto
                .AnombreDe = ANombreDe
                .Concepto = Concepto & ", " & Nombre
                .IdProveedor = ""
                .IdTipoPartida = entCtaBancaria.IdTipoPartida
                .Impreso = False
                .Conciliado = False
                .Anulado = False
                .MotivoAnulacion = ""
                .PagadoBanco = True
                .IdSucursal = piIdSucursal
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .FechaHoraModificacion = Nothing
                .ModificadoPor = ""
            End With

            With entCD1
                .IdCheque = 0
                .IdDetalle = 1
                .IdCuenta = entCtaBancaria.IdCuentaContable
                .Referencia = entCheque.Numero
                .Concepto = Concepto
                .Debe = 0.0
                .Haber = Monto
                .IdCentro = ""
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            ChequeDetalle.Add(entCD1)

            'LA FILA DEL CARGO
            With entCD2
                .IdCheque = 0
                .IdDetalle = 2
                .IdCuenta = IdCuentaGasto
                .Referencia = entCheque.Numero
                .Concepto = Concepto
                .Debe = Monto
                .Haber = 0.0
                .IdCentro = ""
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            ChequeDetalle.Add(entCD2)
            'ROBERTO ORELLANA
            '
            'INSERTO EL REGISTRO
            msj = bl.ban_InsertarCheque(entCheque, ChequeDetalle, True)
            entCheque = Nothing
            ChequeDetalle = Nothing
            If msj = "Ok" Then
            Else
                MsgBox(String.Format("NO FUE POSIBLE GUARDAR EL CHEQUE", Chr(13), msj), MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If
            entCtaBancaria.UltNumeroCheque = entCtaBancaria.UltNumeroCheque + 1
        Next

        ExcObj.WORKBOOKS.Close()
        System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcObj)
        ExcObj = Nothing

        MsgBox("El proceso de importación ha concluido")
        Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        If MsgBox("Subir, presione Yes o Bajar presione No", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            FtpUploadFile("c:\temp\prueba.xls", "ftp.nexuserp.com", "erodriguez", "remle3972$")
        Else

            FTPDownloadFile("d:\temp\soporteRemotonexusElmer.zip", "ftp://itosv.com/SoporteRemotoNexusERP.zip", "erodriguez@itosv.com", "Remle3972$")
            'FTPDownloadFile("/bcr/FIVA/Act_BCR_02052017/exes/nexus.pi", "ftp.nexuserp.com", "erodriguez", "Remle3972$")
        End If
    End Sub
    Private Sub FtpUploadFile(ByVal filetoupload As String, ByVal ftpuri As String, ByVal ftpusername As String, ByVal ftppassword As String)
        ' Create a web request that will be used to talk with the server and set the request method to upload a file by ftp.       
        Dim ftpRequest As FtpWebRequest = CType(WebRequest.Create(ftpuri), FtpWebRequest)

        Try
            ftpRequest.Method = WebRequestMethods.Ftp.UploadFile

            ' Confirm the Network credentials based on the user name and password passed in.
            ftpRequest.Credentials = New NetworkCredential(ftpusername, ftppassword)

            ' Read into a Byte array the contents of the file to be uploaded 
            Dim bytes() As Byte = System.IO.File.ReadAllBytes(filetoupload)

            ' Transfer the byte array contents into the request stream, write and then close when done.
            ftpRequest.ContentLength = bytes.Length
            Using UploadStream As Stream = ftpRequest.GetRequestStream()
                UploadStream.Write(bytes, 0, bytes.Length)
                UploadStream.Close()
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Exit Sub
        End Try

        MessageBox.Show("Process Complete")
    End Sub


    Private Sub FTPDownloadFile(ByVal downloadpath As String, ByVal ftpuri As String, ByVal ftpusername As String, ByVal ftppassword As String)
        'Create a WebClient.
        Dim request As New WebClient()       

        ' Confirm the Network credentials based on the user name and password passed in.
        request.Credentials = New NetworkCredential(ftpusername, ftppassword)

        'Read the file data into a Byte array
        Dim bytes() As Byte = request.DownloadData(ftpuri)

        Try
            '  Create a FileStream to read the file into
            Dim DownloadStream As FileStream = IO.File.Create(downloadpath)
            '  Stream this data into the file
            DownloadStream.Write(bytes, 0, bytes.Length)
            '  Close the FileStream
            DownloadStream.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Exit Sub
        End Try

        MessageBox.Show("Process Complete")

    End Sub
End Class