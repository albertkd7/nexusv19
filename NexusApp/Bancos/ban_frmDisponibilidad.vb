﻿
Imports NexusBLL
Public Class ban_frmDisponibilidad
    Dim bl As New BancosBLL(g_ConnectionString)

    
    Private Sub btnOk_Click() Handles Me.Reporte
        Dim dt, dtd As New DataTable
        dt = bl.ObtieneDisponibilidad(deIni.DateTime, deFin.DateTime, leSucursal.EditValue)
        dtd = bl.ObtieneDisponibilidadDet(deIni.DateTime, deFin.DateTime, leSucursal.EditValue)

        Dim rpt As New ban_rptDisponibilidad() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlPeriodo.Text = (FechaToString(deIni.EditValue, deFin.EditValue)).ToUpper
        rpt.xrlTitulo.Text = teTitulo.EditValue & " - SUCURSAL: " & leSucursal.Text
        rpt.XrSubreportDetalle.ReportSource.DataSource = dtd
        rpt.XrSubreportDetalle.ReportSource.DataMember = ""
        rpt.ShowPreviewDialog()

    End Sub

    Private Sub frmDisponibilidad_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deIni.DateTime = Today
        deFin.DateTime = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

End Class