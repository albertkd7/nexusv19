﻿Imports NexusBLL

Public Class ban_frmListadoTrans
    Dim bl As New BancosBLL(g_ConnectionString)

    Private Sub Ban_frmListadoTrans_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DateEdit1.DateTime = Today
        DateEdit2.DateTime = Today
        objCombos.banCuentasBancarias(leCtaBanco)
        objCombos.banTiposTransaccion(leTipoTransaccion, "-- TODAS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub Ban_frmListadoTrans_Report_Click() Handles Me.Reporte
        Dim rpt As New ban_rptListadoTrans() With {.DataSource = bl.GetListadoTrans(DateEdit1.EditValue, DateEdit2.EditValue, leCtaBanco.EditValue, leTipoTransaccion.EditValue, leSucursal.EditValue), .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Listado de movimientos de banco - " & leCtaBanco.Text & " - " & leSucursal.Text
        rpt.xrlPeriodo.Text = leTipoTransaccion.Text & FechaToString(DateEdit1.DateTime, DateEdit2.DateTime)
        rpt.ShowPreview()
    End Sub
End Class
