﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ban_frmConciliacion
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xelCargosPend = New DevExpress.XtraEditors.LabelControl()
        Me.xelChequesPend = New DevExpress.XtraEditors.LabelControl()
        Me.xelDepTransito = New DevExpress.XtraEditors.LabelControl()
        Me.xelCargosNoConta = New DevExpress.XtraEditors.LabelControl()
        Me.xelDepNoConta = New DevExpress.XtraEditors.LabelControl()
        Me.xelSaldoConta = New DevExpress.XtraEditors.LabelControl()
        Me.xelBanco = New DevExpress.XtraEditors.LabelControl()
        Me.sbGenerar = New DevExpress.XtraEditors.SimpleButton()
        Me.leCtaBancaria = New DevExpress.XtraEditors.LookUpEdit()
        Me.teChequesPendientes = New DevExpress.XtraEditors.TextEdit()
        Me.teDepositosTransito = New DevExpress.XtraEditors.TextEdit()
        Me.teCargosNoContab = New DevExpress.XtraEditors.TextEdit()
        Me.teDepositosNoContab = New DevExpress.XtraEditors.TextEdit()
        Me.teSaldoContab = New DevExpress.XtraEditors.TextEdit()
        Me.teSaldoBanco = New DevExpress.XtraEditors.TextEdit()
        Me.teCargosPendientes = New DevExpress.XtraEditors.TextEdit()
        Me.seEjercicio = New DevExpress.XtraEditors.SpinEdit()
        Me.xelMes = New DevExpress.XtraEditors.LabelControl()
        Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teHechoPor = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TeRevisadoPor = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TeAutorizadoPor = New DevExpress.XtraEditors.TextEdit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teChequesPendientes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDepositosTransito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCargosNoContab.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDepositosNoContab.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSaldoContab.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSaldoBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCargosPendientes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teHechoPor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeRevisadoPor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeAutorizadoPor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.TeAutorizadoPor)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.TeRevisadoPor)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.teHechoPor)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.xelCargosPend)
        Me.GroupControl1.Controls.Add(Me.xelChequesPend)
        Me.GroupControl1.Controls.Add(Me.xelDepTransito)
        Me.GroupControl1.Controls.Add(Me.xelCargosNoConta)
        Me.GroupControl1.Controls.Add(Me.xelDepNoConta)
        Me.GroupControl1.Controls.Add(Me.xelSaldoConta)
        Me.GroupControl1.Controls.Add(Me.xelBanco)
        Me.GroupControl1.Controls.Add(Me.sbGenerar)
        Me.GroupControl1.Controls.Add(Me.leCtaBancaria)
        Me.GroupControl1.Controls.Add(Me.teChequesPendientes)
        Me.GroupControl1.Controls.Add(Me.teDepositosTransito)
        Me.GroupControl1.Controls.Add(Me.teCargosNoContab)
        Me.GroupControl1.Controls.Add(Me.teDepositosNoContab)
        Me.GroupControl1.Controls.Add(Me.teSaldoContab)
        Me.GroupControl1.Controls.Add(Me.teSaldoBanco)
        Me.GroupControl1.Controls.Add(Me.teCargosPendientes)
        Me.GroupControl1.Controls.Add(Me.seEjercicio)
        Me.GroupControl1.Controls.Add(Me.xelMes)
        Me.GroupControl1.Controls.Add(Me.meMes)
        Me.GroupControl1.Size = New System.Drawing.Size(621, 435)
        '
        'xelCargosPend
        '
        Me.xelCargosPend.Location = New System.Drawing.Point(357, 189)
        Me.xelCargosPend.Name = "xelCargosPend"
        Me.xelCargosPend.Size = New System.Drawing.Size(94, 13)
        Me.xelCargosPend.TabIndex = 80
        Me.xelCargosPend.Text = "Cargos Pendientes:"
        '
        'xelChequesPend
        '
        Me.xelChequesPend.Location = New System.Drawing.Point(304, 164)
        Me.xelChequesPend.Name = "xelChequesPend"
        Me.xelChequesPend.Size = New System.Drawing.Size(147, 13)
        Me.xelChequesPend.TabIndex = 79
        Me.xelChequesPend.Text = "Cheques pendientes de cobro:"
        '
        'xelDepTransito
        '
        Me.xelDepTransito.Location = New System.Drawing.Point(343, 141)
        Me.xelDepTransito.Name = "xelDepTransito"
        Me.xelDepTransito.Size = New System.Drawing.Size(108, 13)
        Me.xelDepTransito.TabIndex = 78
        Me.xelDepTransito.Text = "Depósitos en Tránsito:"
        '
        'xelCargosNoConta
        '
        Me.xelCargosNoConta.Location = New System.Drawing.Point(24, 188)
        Me.xelCargosNoConta.Name = "xelCargosNoConta"
        Me.xelCargosNoConta.Size = New System.Drawing.Size(140, 13)
        Me.xelCargosNoConta.TabIndex = 77
        Me.xelCargosNoConta.Text = "(-) Cargos no Contabilizados:"
        '
        'xelDepNoConta
        '
        Me.xelDepNoConta.Location = New System.Drawing.Point(9, 164)
        Me.xelDepNoConta.Name = "xelDepNoConta"
        Me.xelDepNoConta.Size = New System.Drawing.Size(155, 13)
        Me.xelDepNoConta.TabIndex = 76
        Me.xelDepNoConta.Text = "(+) Depósitos no contabilizados:"
        '
        'xelSaldoConta
        '
        Me.xelSaldoConta.Location = New System.Drawing.Point(39, 141)
        Me.xelSaldoConta.Name = "xelSaldoConta"
        Me.xelSaldoConta.Size = New System.Drawing.Size(125, 13)
        Me.xelSaldoConta.TabIndex = 75
        Me.xelSaldoConta.Text = "Saldo Según Contabilidad:"
        '
        'xelBanco
        '
        Me.xelBanco.Location = New System.Drawing.Point(42, 77)
        Me.xelBanco.Name = "xelBanco"
        Me.xelBanco.Size = New System.Drawing.Size(122, 13)
        Me.xelBanco.TabIndex = 74
        Me.xelBanco.Text = "Banco / Cuenta Bancaria:"
        '
        'sbGenerar
        '
        Me.sbGenerar.Location = New System.Drawing.Point(201, 247)
        Me.sbGenerar.Name = "sbGenerar"
        Me.sbGenerar.Size = New System.Drawing.Size(157, 33)
        Me.sbGenerar.TabIndex = 5
        Me.sbGenerar.Text = "&Generar Conciliación..."
        '
        'leCtaBancaria
        '
        Me.leCtaBancaria.EnterMoveNextControl = True
        Me.leCtaBancaria.Location = New System.Drawing.Point(167, 74)
        Me.leCtaBancaria.Name = "leCtaBancaria"
        Me.leCtaBancaria.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCtaBancaria.Size = New System.Drawing.Size(387, 20)
        Me.leCtaBancaria.TabIndex = 3
        '
        'teChequesPendientes
        '
        Me.teChequesPendientes.EditValue = 0
        Me.teChequesPendientes.EnterMoveNextControl = True
        Me.teChequesPendientes.Location = New System.Drawing.Point(454, 161)
        Me.teChequesPendientes.Name = "teChequesPendientes"
        Me.teChequesPendientes.Properties.Appearance.Options.UseTextOptions = True
        Me.teChequesPendientes.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teChequesPendientes.Properties.Mask.EditMask = "C"
        Me.teChequesPendientes.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teChequesPendientes.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teChequesPendientes.Properties.ReadOnly = True
        Me.teChequesPendientes.Size = New System.Drawing.Size(100, 20)
        Me.teChequesPendientes.TabIndex = 8
        '
        'teDepositosTransito
        '
        Me.teDepositosTransito.EditValue = 0
        Me.teDepositosTransito.EnterMoveNextControl = True
        Me.teDepositosTransito.Location = New System.Drawing.Point(454, 138)
        Me.teDepositosTransito.Name = "teDepositosTransito"
        Me.teDepositosTransito.Properties.Appearance.Options.UseTextOptions = True
        Me.teDepositosTransito.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teDepositosTransito.Properties.Mask.EditMask = "c2"
        Me.teDepositosTransito.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teDepositosTransito.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teDepositosTransito.Properties.ReadOnly = True
        Me.teDepositosTransito.Size = New System.Drawing.Size(100, 20)
        Me.teDepositosTransito.TabIndex = 7
        '
        'teCargosNoContab
        '
        Me.teCargosNoContab.EditValue = 0
        Me.teCargosNoContab.EnterMoveNextControl = True
        Me.teCargosNoContab.Location = New System.Drawing.Point(167, 185)
        Me.teCargosNoContab.Name = "teCargosNoContab"
        Me.teCargosNoContab.Properties.Appearance.Options.UseTextOptions = True
        Me.teCargosNoContab.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teCargosNoContab.Properties.Mask.EditMask = "C"
        Me.teCargosNoContab.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teCargosNoContab.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teCargosNoContab.Properties.ReadOnly = True
        Me.teCargosNoContab.Size = New System.Drawing.Size(100, 20)
        Me.teCargosNoContab.TabIndex = 6
        '
        'teDepositosNoContab
        '
        Me.teDepositosNoContab.EditValue = 0
        Me.teDepositosNoContab.EnterMoveNextControl = True
        Me.teDepositosNoContab.Location = New System.Drawing.Point(167, 161)
        Me.teDepositosNoContab.Name = "teDepositosNoContab"
        Me.teDepositosNoContab.Properties.Appearance.Options.UseTextOptions = True
        Me.teDepositosNoContab.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teDepositosNoContab.Properties.Mask.EditMask = "C"
        Me.teDepositosNoContab.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teDepositosNoContab.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teDepositosNoContab.Properties.ReadOnly = True
        Me.teDepositosNoContab.Size = New System.Drawing.Size(100, 20)
        Me.teDepositosNoContab.TabIndex = 5
        '
        'teSaldoContab
        '
        Me.teSaldoContab.EditValue = 0
        Me.teSaldoContab.EnterMoveNextControl = True
        Me.teSaldoContab.Location = New System.Drawing.Point(167, 138)
        Me.teSaldoContab.Name = "teSaldoContab"
        Me.teSaldoContab.Properties.Appearance.Options.UseTextOptions = True
        Me.teSaldoContab.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teSaldoContab.Properties.Mask.EditMask = "C"
        Me.teSaldoContab.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teSaldoContab.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teSaldoContab.Properties.ReadOnly = True
        Me.teSaldoContab.Size = New System.Drawing.Size(100, 20)
        Me.teSaldoContab.TabIndex = 4
        '
        'teSaldoBanco
        '
        Me.teSaldoBanco.EditValue = 0
        Me.teSaldoBanco.EnterMoveNextControl = True
        Me.teSaldoBanco.Location = New System.Drawing.Point(454, 104)
        Me.teSaldoBanco.Name = "teSaldoBanco"
        Me.teSaldoBanco.Properties.Appearance.Options.UseTextOptions = True
        Me.teSaldoBanco.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teSaldoBanco.Properties.DisplayFormat.FormatString = "C"
        Me.teSaldoBanco.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teSaldoBanco.Properties.EditFormat.FormatString = "C"
        Me.teSaldoBanco.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teSaldoBanco.Properties.Mask.EditMask = "C"
        Me.teSaldoBanco.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teSaldoBanco.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teSaldoBanco.Size = New System.Drawing.Size(100, 20)
        Me.teSaldoBanco.TabIndex = 4
        '
        'teCargosPendientes
        '
        Me.teCargosPendientes.EditValue = 0
        Me.teCargosPendientes.EnterMoveNextControl = True
        Me.teCargosPendientes.Location = New System.Drawing.Point(454, 185)
        Me.teCargosPendientes.Name = "teCargosPendientes"
        Me.teCargosPendientes.Properties.Appearance.Options.UseTextOptions = True
        Me.teCargosPendientes.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teCargosPendientes.Properties.Mask.EditMask = "C"
        Me.teCargosPendientes.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teCargosPendientes.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teCargosPendientes.Properties.ReadOnly = True
        Me.teCargosPendientes.Size = New System.Drawing.Size(100, 20)
        Me.teCargosPendientes.TabIndex = 9
        '
        'seEjercicio
        '
        Me.seEjercicio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seEjercicio.EnterMoveNextControl = True
        Me.seEjercicio.Location = New System.Drawing.Point(307, 23)
        Me.seEjercicio.Name = "seEjercicio"
        Me.seEjercicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seEjercicio.Size = New System.Drawing.Size(79, 20)
        Me.seEjercicio.TabIndex = 1
        '
        'xelMes
        '
        Me.xelMes.Location = New System.Drawing.Point(77, 26)
        Me.xelMes.Name = "xelMes"
        Me.xelMes.Size = New System.Drawing.Size(87, 13)
        Me.xelMes.TabIndex = 73
        Me.xelMes.Text = "Seleccione el Mes:"
        '
        'meMes
        '
        Me.meMes.EnterMoveNextControl = True
        Me.meMes.Location = New System.Drawing.Point(167, 23)
        Me.meMes.Name = "meMes"
        Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes.Size = New System.Drawing.Size(100, 20)
        Me.meMes.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(217, 108)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(234, 13)
        Me.LabelControl1.TabIndex = 81
        Me.LabelControl1.Text = "SALDO DEL BANCO SEGUN ESTADO DE CUENTA:"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(167, 51)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(387, 20)
        Me.leSucursal.TabIndex = 2
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(120, 54)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl7.TabIndex = 83
        Me.LabelControl7.Text = "Sucursal:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(46, 295)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl5.TabIndex = 85
        Me.LabelControl5.Text = "Hecho Por:"
        '
        'teHechoPor
        '
        Me.teHechoPor.EnterMoveNextControl = True
        Me.teHechoPor.Location = New System.Drawing.Point(104, 292)
        Me.teHechoPor.Name = "teHechoPor"
        Me.teHechoPor.Size = New System.Drawing.Size(316, 20)
        Me.teHechoPor.TabIndex = 84
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(32, 321)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(67, 13)
        Me.LabelControl2.TabIndex = 87
        Me.LabelControl2.Text = "Revisado Por:"
        '
        'TeRevisadoPor
        '
        Me.TeRevisadoPor.EnterMoveNextControl = True
        Me.TeRevisadoPor.Location = New System.Drawing.Point(104, 318)
        Me.TeRevisadoPor.Name = "TeRevisadoPor"
        Me.TeRevisadoPor.Size = New System.Drawing.Size(316, 20)
        Me.TeRevisadoPor.TabIndex = 86
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(24, 347)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl3.TabIndex = 89
        Me.LabelControl3.Text = "Autorizado Por:"
        '
        'TeAutorizadoPor
        '
        Me.TeAutorizadoPor.EnterMoveNextControl = True
        Me.TeAutorizadoPor.Location = New System.Drawing.Point(104, 344)
        Me.TeAutorizadoPor.Name = "TeAutorizadoPor"
        Me.TeAutorizadoPor.Size = New System.Drawing.Size(316, 20)
        Me.TeAutorizadoPor.TabIndex = 88
        '
        'ban_frmConciliacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(621, 460)
        Me.Modulo = "Bancos"
        Me.Name = "ban_frmConciliacion"
        Me.Text = "Conciliación Bancaria"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teChequesPendientes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDepositosTransito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCargosNoContab.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDepositosNoContab.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSaldoContab.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSaldoBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCargosPendientes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teHechoPor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeRevisadoPor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeAutorizadoPor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents xelCargosPend As DevExpress.XtraEditors.LabelControl
    Friend WithEvents xelChequesPend As DevExpress.XtraEditors.LabelControl
    Friend WithEvents xelDepTransito As DevExpress.XtraEditors.LabelControl
    Friend WithEvents xelCargosNoConta As DevExpress.XtraEditors.LabelControl
    Friend WithEvents xelDepNoConta As DevExpress.XtraEditors.LabelControl
    Friend WithEvents xelSaldoConta As DevExpress.XtraEditors.LabelControl
    Friend WithEvents xelBanco As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbGenerar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents leCtaBancaria As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teChequesPendientes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teDepositosTransito As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCargosNoContab As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teDepositosNoContab As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teSaldoContab As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teSaldoBanco As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCargosPendientes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents seEjercicio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents xelMes As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TeAutorizadoPor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TeRevisadoPor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teHechoPor As DevExpress.XtraEditors.TextEdit

End Class
