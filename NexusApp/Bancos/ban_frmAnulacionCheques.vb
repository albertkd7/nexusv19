﻿Imports NexusBLL, NexusELL.TableEntities
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls

Public Class ban_frmAnulacionCheques
    Dim bl As New BancosBLL(g_ConnectionString)

    Dim entCheque As ban_Cheques

    Private Sub frmAnulacionCheques_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFecha.DateTime = Today
        objCombos.banCuentasBancarias(leCuentaBanco)
        objCombos.conTiposPartida(leTiposPartida)
    End Sub


    Private Sub teNumCheque_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles teNumCheque.Validated
        Dim nIdCheque = bl.ExisteCheque(leCuentaBanco.EditValue, teNumCheque.EditValue)
        If nIdCheque = 0 Then
            MsgBox("No se ha emitido ningún número de cheque con estos datos", 64, "Nota")
            Exit Sub
        End If
        entCheque = objTablas.ban_ChequesSelectByPK(nIdCheque)
        teAnombreDe.EditValue = entCheque.AnombreDe
        teFechaCheque.EditValue = Format(entCheque.Fecha, "dd/MM/yyyy")
        teValor.EditValue = entCheque.Valor
        meConcepto.EditValue = entCheque.Concepto
        deFecha.EditValue = Today
    End Sub

    Private Sub sbAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbAnular.Click
        If entCheque Is Nothing Then
            MsgBox("No se ha emitido ningún número de cheque con estos datos", 64, "Nota")
            Exit Sub
        End If
        If teMotivo.EditValue = "" Then
            MsgBox("Debe de especificar el motivo de anulación", 64, "Nota")
            Exit Sub
        End If
        If MsgBox("Está seguro de anular éste cheque?", MsgBoxStyle.YesNo, "Nota") = MsgBoxResult.No Then
            Exit Sub
        End If

        If entCheque.Anulado Then
            MsgBox("Este cheque ya ha sido anulado. Para que proceder!", 64, "Nota")
            Exit Sub
        End If

        Dim bAnulado = False
        Dim EsOk As Boolean = ValidarFechaCierre(entCheque.Fecha)

        If EsOk Then
            bAnulado = bl.AnulacionChequeAbierto(entCheque.IdCheque, objMenu.User, deFecha.EditValue, teMotivo.EditValue) = 0
        Else
            If MsgBox("La fecha del cheque no corresponde al período vigente!" & Chr(13) & _
                   "Está seguro(a) de continuar con la anulación?", MsgBoxStyle.YesNo, "Nota") = MsgBoxResult.No Then
                Exit Sub
            End If
            Dim iIdTransaccion = objFunciones.ObtenerUltimoId("BAN_TRANSACCIONES", "IdTransaccion") + 1
            Dim iIdPartida = objFunciones.ObtenerUltimoId("CON_PARTIDAS", "IdPartida") + 1
            bAnulado = bl.AnulacionChequeCerrado(entCheque.IdCheque, iIdTransaccion, 2, leTiposPartida.EditValue, iIdPartida, deFecha.EditValue, teMotivo.EditValue, objMenu.User) = 0

        End If
        If bAnulado Then
            objFunciones.ActualizaAbono("CHEQUES", entCheque.IdCheque) ' ACTIVO EL ABONO PARA QUE LO BORREN Y LO VUELVAN A HACER
            MsgBox("El cheque ha sido anulado con éxito", 64, "Nota")
        Else
            MsgBox("El cheque NO fue anulado, hubo algún error en la base de datos", 16, "Error")
        End If
    End Sub

End Class
