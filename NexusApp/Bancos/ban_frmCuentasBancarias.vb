﻿Imports NexusELL.TableEntities
Imports NexusBLL

Public Class ban_frmCuentasBancarias
    Dim entCta As ban_CuentasBancarias
    Dim bl As New BancosBLL(g_ConnectionString)
    Dim IdCta As Integer

    Private Sub ban_frmCuentasBancarias_Editar() Handles Me.Editar
        ActivarControles(True)
        teBanco.Focus()
    End Sub

    Private Sub ban_frmCuentasBancarias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.conTiposPartida(leTipoPartida)
        objCombos.ban_TipoBanco(leTipoBanco)
        objCombos.ban_TipoImpresion(leTipoImpresion)

        IdCta = objFunciones.ObtenerUltimoId("BAN_CUENTASBANCARIAS", "IdCuentaBancaria")

        gc.DataSource = objTablas.ban_CuentasBancariasSelectAll
        entCta = objTablas.ban_CuentasBancariasSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCuentaBancaria"))

        CargaPantalla()

        ActivarControles(False)
        ' CargaControles(0)
    End Sub

    Private Sub ban_frmCuentasBancarias_Nuevo_Click() Handles Me.Nuevo
        '1= Agricola, 2=Citi, 3= Scotiabank, 4= HSBC, 5=America Central, 6= Promerica, 7=Hipotecario, 8=Procredit, 9=Industrial
        ActivarControles(True)
        IdCta = -1
        entCta = New ban_CuentasBancarias
        entCta.IdCuentaBancaria = 0
        CargaPantalla()
        teBanco.Focus()
    End Sub

    Private Sub ban_frmCuentasBancarias_Guardar() Handles Me.Guardar

        If SiEsNulo(leTipoPartida.EditValue, "") = "" Then
            MsgBox("Debe especificar el tipo de partida contable de la cuenta bancaria", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        If SiEsNulo(BeCtaContable1.beIdCuenta.EditValue, "") = "" Then
            MsgBox("Debe especificar la cuenta contable de la cuenta bancaria", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        If SiEsNulo(teNumeroCuenta.EditValue, "") = "" Then
            MsgBox("Debe especificar el número de cuenta bancaria", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            entCta.IdCuentaBancaria = objFunciones.ObtenerUltimoId("BAN_CUENTASBANCARIAS", "IdCuentaBancaria") + 1
            objTablas.ban_CuentasBancariasInsert(entCta)
        Else
            objTablas.ban_CuentasBancariasUpdate(entCta)
        End If

        gc.DataSource = objTablas.ban_CuentasBancariasSelectAll
        entCta = objTablas.ban_CuentasBancariasSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCuentaBancaria"))
        CargaPantalla()
        MostrarModoInicial()
        ActivarControles(False)
    End Sub
    Private Sub ban_frmCuentasBancarias_Eliminar() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Return
        End If

        If MsgBox("Está seguro(a) de eliminar la cuenta bancaria seleccionada?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.ban_CuentasBancariasDeleteByPK(IdCta)
                gc.DataSource = objTablas.ban_CuentasBancariasSelectAll
                entCta = objTablas.ban_CuentasBancariasSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCuentaBancaria"))
                CargaPantalla()
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR LA CUENTA BANCARIA:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entCta = objTablas.ban_CuentasBancariasSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCuentaBancaria"))
        CargaPantalla()
    End Sub

    Private Sub ban_frmCuentasBancarias_Reporte() Handles Me.Reporte
    End Sub
    Private Sub CargaPantalla()
        With entCta
            teBanco.EditValue = .Nombre
            teNumeroCuenta.EditValue = .NumeroCuenta
            BeCtaContable1.beIdCuenta.EditValue = .IdCuentaContable
            seUltCheque.EditValue = .UltNumeroCheque
            seFormatoCheque.EditValue = .IdFormatoVaucher
            leTipoPartida.EditValue = .IdTipoPartida
            teCargo1.EditValue = .CargoDigitador
            teCargo2.EditValue = .CargoRevisa
            teCargo3.EditValue = .CargoAutoriza
            tePerEmite.EditValue = .NombreDigitador
            tePerRevisa.EditValue = .NombreRevisa
            tePerAutoriza.EditValue = .NombreAutoriza
            leTipoImpresion.EditValue = .TipoImpresion
            ceCuentaInactiva.EditValue = .CuentaInactiva
            leTipoBanco.EditValue = .IdBanco
            teCodEmpresa.EditValue = .CodigoEmpresa
            BeCtaContable2.beIdCuenta.EditValue = .IdCuentaContablePos
        End With
    End Sub
    Private Sub CargaEntidad()
        With entCta
            .Nombre = teBanco.EditValue
            .NumeroCuenta = teNumeroCuenta.EditValue
            .IdCuentaContable = BeCtaContable1.beIdCuenta.EditValue
            .UltNumeroCheque = seUltCheque.EditValue
            .IdFormatoVaucher = seFormatoCheque.EditValue
            .IdTipoPartida = leTipoPartida.EditValue
            .NombreDigitador = tePerEmite.EditValue
            .NombreRevisa = tePerRevisa.EditValue
            .NombreAutoriza = tePerAutoriza.EditValue
            .CargoDigitador = teCargo1.EditValue
            .CargoRevisa = teCargo2.EditValue
            .CargoAutoriza = teCargo3.EditValue
            .EstadoCuenta = 1  'pendiente de setear este dato, podría ser, pignorada.TipoImpresion = leTipoImpresion.EditValue
            .TipoImpresion = leTipoImpresion.EditValue
            .CuentaInactiva = ceCuentaInactiva.EditValue
            .IdBanco = leTipoBanco.EditValue
            .CodigoEmpresa = teCodEmpresa.EditValue
            .IdCuentaContablePos = BeCtaContable2.beIdCuenta.EditValue

            If DbMode = DbModeType.insert Then
                .FechaHoraCreacion = Now
                .CreadoPor = objMenu.User
            End If
        End With
    End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pc2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        For Each ctrl In GroupControl1.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        For Each ctrl In GroupControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        For Each ctrl In GroupControl3.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
        Next

    End Sub


End Class