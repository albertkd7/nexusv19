﻿Imports NexusBLL
Public Class con_frmEliminaPartidas
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    

    Private Sub con_frmEliminaPartidas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.conTiposPartida(leTipoPartida)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub sbEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbEliminar.Click
        Dim bl2 As New AdmonBLL(g_ConnectionString)
        Dim dt As DataTable = bl2.ObtieneFechaCierre
        If deDesde.EditValue <= CDate(dt.Rows(0).Item("FechaCierre")) Then
            MsgBox("No puede eliminar éstas paridas, la fecha inicial es menor al cierre", 64, "Nota")
            Exit Sub
        End If
        If MsgBox("Está seguro(a) de eliminar éstas partidas?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Dim msj As String = ""
            msj = bl.pro_EliminaPartidasPorFecha(deDesde.EditValue, deHasta.EditValue, leTipoPartida.EditValue, leSucursal.EditValue)
            If msj = "" Then
                MsgBox("Las partidas han sido eliminadas", 64, "Nota")
            Else
                MsgBox("Hubo algún error al tratar de eliminar" + Chr(13) + msj, 64, "Nota")
            End If
        End If
    End Sub
End Class
