﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmCierreMes
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(con_frmCierreMes))
        Me.beCerrar = New DevExpress.XtraEditors.SimpleButton()
        Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit()
        Me.seEjercicio = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.rgTipo = New DevExpress.XtraEditors.RadioGroup()
        Me.lbMsg = New DevExpress.XtraEditors.LabelControl()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.rgTipo)
        Me.GroupControl1.Controls.Add(Me.lbMsg)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.beCerrar)
        Me.GroupControl1.Size = New System.Drawing.Size(554, 178)
        Me.GroupControl1.Text = "Cerrar periodo por mes"
        '
        'beCerrar
        '
        Me.beCerrar.Location = New System.Drawing.Point(157, 107)
        Me.beCerrar.Name = "beCerrar"
        Me.beCerrar.Size = New System.Drawing.Size(171, 35)
        Me.beCerrar.TabIndex = 4
        Me.beCerrar.Text = "Ejecutar cierre..."
        '
        'meMes
        '
        Me.meMes.Enabled = False
        Me.meMes.Location = New System.Drawing.Point(157, 81)
        Me.meMes.Name = "meMes"
        Me.meMes.Properties.AllowFocused = False
        Me.meMes.Properties.AppearanceDisabled.BorderColor = System.Drawing.Color.Black
        Me.meMes.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.meMes.Properties.AppearanceDisabled.Options.UseBorderColor = True
        Me.meMes.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes.Size = New System.Drawing.Size(92, 20)
        Me.meMes.TabIndex = 23
        '
        'seEjercicio
        '
        Me.seEjercicio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seEjercicio.Enabled = False
        Me.seEjercicio.Location = New System.Drawing.Point(255, 81)
        Me.seEjercicio.Name = "seEjercicio"
        Me.seEjercicio.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.seEjercicio.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.seEjercicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seEjercicio.Properties.DisplayFormat.FormatString = "n0"
        Me.seEjercicio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seEjercicio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seEjercicio.Properties.Mask.EditMask = "n0"
        Me.seEjercicio.Size = New System.Drawing.Size(73, 20)
        Me.seEjercicio.TabIndex = 24
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(83, 52)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl1.TabIndex = 26
        Me.LabelControl1.Text = "Tipo de Cierre:"
        Me.LabelControl1.Visible = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(49, 84)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(105, 13)
        Me.LabelControl2.TabIndex = 26
        Me.LabelControl2.Text = "Período que se cierra:"
        '
        'rgTipo
        '
        Me.rgTipo.EditValue = 1
        Me.rgTipo.Location = New System.Drawing.Point(157, 46)
        Me.rgTipo.Name = "rgTipo"
        Me.rgTipo.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Cierre Definitivo"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Pre-Cierre")})
        Me.rgTipo.Size = New System.Drawing.Size(245, 25)
        Me.rgTipo.TabIndex = 27
        Me.rgTipo.Visible = False
        '
        'lbMsg
        '
        Me.lbMsg.Location = New System.Drawing.Point(339, 84)
        Me.lbMsg.Name = "lbMsg"
        Me.lbMsg.Size = New System.Drawing.Size(204, 39)
        Me.lbMsg.TabIndex = 26
        Me.lbMsg.Text = "No existen registros de periodos cerrados, " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "favor indique el primer periodo a pr" &
    "ocesar" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "-- Nexus ERP --"
        Me.lbMsg.Visible = False
        '
        'con_frmCierreMes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(554, 203)
        Me.Controls.Add(Me.seEjercicio)
        Me.Controls.Add(Me.meMes)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmCierreMes"
        Me.Text = "Cierre de período contable"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.meMes, 0)
        Me.Controls.SetChildIndex(Me.seEjercicio, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents beCerrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents seEjercicio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgTipo As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbMsg As DevExpress.XtraEditors.LabelControl
End Class
