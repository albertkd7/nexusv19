﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmTransaccionesCorte
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.txtValor = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.txtConcepto = New DevExpress.XtraEditors.TextEdit()
        Me.leConceptosCaja = New DevExpress.XtraEditors.LookUpEdit()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdConcepto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.reIdConcepto = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcIdCuentaBancaria = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.reIdCuentaBancaria = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcValor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcConcepto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.xelBanco = New DevExpress.XtraEditors.LabelControl()
        Me.leCtaBancaria = New DevExpress.XtraEditors.LookUpEdit()
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.sbCerrar = New DevExpress.XtraEditors.SimpleButton()
        Me.lePuntoVenta = New DevExpress.XtraEditors.LookUpEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.txtValor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leConceptosCaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.reIdConcepto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.reIdCuentaBancaria, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(47, 52)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "Concepto:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(69, 72)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl2.TabIndex = 6
        Me.LabelControl2.Text = "Valor:"
        '
        'txtValor
        '
        Me.txtValor.EditValue = "0.00"
        Me.txtValor.Location = New System.Drawing.Point(100, 70)
        Me.txtValor.Name = "txtValor"
        Me.txtValor.Properties.Mask.EditMask = "n2"
        Me.txtValor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtValor.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtValor.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtValor.Size = New System.Drawing.Size(100, 20)
        Me.txtValor.TabIndex = 7
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(47, 94)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl3.TabIndex = 8
        Me.LabelControl3.Text = "Concepto:"
        '
        'txtConcepto
        '
        Me.txtConcepto.Location = New System.Drawing.Point(100, 91)
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.Size = New System.Drawing.Size(594, 20)
        Me.txtConcepto.TabIndex = 9
        '
        'leConceptosCaja
        '
        Me.leConceptosCaja.Location = New System.Drawing.Point(100, 49)
        Me.leConceptosCaja.Name = "leConceptosCaja"
        Me.leConceptosCaja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leConceptosCaja.Size = New System.Drawing.Size(193, 20)
        Me.leConceptosCaja.TabIndex = 13
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.Location = New System.Drawing.Point(100, 29)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 14
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(64, 32)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl4.TabIndex = 15
        Me.LabelControl4.Text = "Fecha:"
        '
        'gc
        '
        Me.gc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gc.Location = New System.Drawing.Point(1, 125)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.reIdConcepto, Me.reIdCuentaBancaria})
        Me.gc.Size = New System.Drawing.Size(693, 196)
        Me.gc.TabIndex = 16
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdConcepto, Me.gcIdCuentaBancaria, Me.gcValor, Me.gcConcepto, Me.gcIdComprobante})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdConcepto
        '
        Me.gcIdConcepto.Caption = "Concepto"
        Me.gcIdConcepto.ColumnEdit = Me.reIdConcepto
        Me.gcIdConcepto.FieldName = "IdConcepto"
        Me.gcIdConcepto.Name = "gcIdConcepto"
        Me.gcIdConcepto.OptionsColumn.ReadOnly = True
        Me.gcIdConcepto.Visible = True
        Me.gcIdConcepto.VisibleIndex = 0
        Me.gcIdConcepto.Width = 138
        '
        'reIdConcepto
        '
        Me.reIdConcepto.AutoHeight = False
        Me.reIdConcepto.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.reIdConcepto.Name = "reIdConcepto"
        '
        'gcIdCuentaBancaria
        '
        Me.gcIdCuentaBancaria.Caption = "Banco"
        Me.gcIdCuentaBancaria.ColumnEdit = Me.reIdCuentaBancaria
        Me.gcIdCuentaBancaria.FieldName = "IdCuentaBancaria"
        Me.gcIdCuentaBancaria.Name = "gcIdCuentaBancaria"
        Me.gcIdCuentaBancaria.OptionsColumn.ReadOnly = True
        Me.gcIdCuentaBancaria.Visible = True
        Me.gcIdCuentaBancaria.VisibleIndex = 1
        Me.gcIdCuentaBancaria.Width = 150
        '
        'reIdCuentaBancaria
        '
        Me.reIdCuentaBancaria.AutoHeight = False
        Me.reIdCuentaBancaria.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.reIdCuentaBancaria.Name = "reIdCuentaBancaria"
        '
        'gcValor
        '
        Me.gcValor.Caption = "Valor"
        Me.gcValor.FieldName = "Valor"
        Me.gcValor.Name = "gcValor"
        Me.gcValor.OptionsColumn.ReadOnly = True
        Me.gcValor.Visible = True
        Me.gcValor.VisibleIndex = 2
        '
        'gcConcepto
        '
        Me.gcConcepto.Caption = "Concepto"
        Me.gcConcepto.FieldName = "Concepto"
        Me.gcConcepto.Name = "gcConcepto"
        Me.gcConcepto.OptionsColumn.ReadOnly = True
        Me.gcConcepto.Visible = True
        Me.gcConcepto.VisibleIndex = 3
        Me.gcConcepto.Width = 250
        '
        'gcIdComprobante
        '
        Me.gcIdComprobante.Caption = "IdComprobante"
        Me.gcIdComprobante.FieldName = "IdComprobante"
        Me.gcIdComprobante.Name = "gcIdComprobante"
        '
        'xelBanco
        '
        Me.xelBanco.Location = New System.Drawing.Point(495, 11)
        Me.xelBanco.Name = "xelBanco"
        Me.xelBanco.Size = New System.Drawing.Size(122, 13)
        Me.xelBanco.TabIndex = 76
        Me.xelBanco.Text = "Banco / Cuenta Bancaria:"
        '
        'leCtaBancaria
        '
        Me.leCtaBancaria.EnterMoveNextControl = True
        Me.leCtaBancaria.Location = New System.Drawing.Point(387, 32)
        Me.leCtaBancaria.Name = "leCtaBancaria"
        Me.leCtaBancaria.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCtaBancaria.Size = New System.Drawing.Size(307, 20)
        Me.leCtaBancaria.TabIndex = 75
        '
        'sbGuardar
        '
        Me.sbGuardar.Location = New System.Drawing.Point(389, 62)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(75, 23)
        Me.sbGuardar.TabIndex = 77
        Me.sbGuardar.Text = "Agregar"
        '
        'sbCerrar
        '
        Me.sbCerrar.Location = New System.Drawing.Point(619, 62)
        Me.sbCerrar.Name = "sbCerrar"
        Me.sbCerrar.Size = New System.Drawing.Size(75, 23)
        Me.sbCerrar.TabIndex = 78
        Me.sbCerrar.Text = "Cierre"
        Me.sbCerrar.Visible = False
        '
        'lePuntoVenta
        '
        Me.lePuntoVenta.Location = New System.Drawing.Point(388, 8)
        Me.lePuntoVenta.Name = "lePuntoVenta"
        Me.lePuntoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePuntoVenta.Size = New System.Drawing.Size(306, 20)
        Me.lePuntoVenta.TabIndex = 80
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(100, 8)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(193, 20)
        Me.leSucursal.TabIndex = 79
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(306, 11)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl6.TabIndex = 82
        Me.LabelControl6.Text = "Punto de Venta:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(53, 11)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl5.TabIndex = 81
        Me.LabelControl5.Text = "Sucursal:"
        '
        'con_frmTransaccionesCorte
        '
        Me.ClientSize = New System.Drawing.Size(724, 352)
        Me.Controls.Add(Me.lePuntoVenta)
        Me.Controls.Add(Me.leSucursal)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.sbCerrar)
        Me.Controls.Add(Me.sbGuardar)
        Me.Controls.Add(Me.xelBanco)
        Me.Controls.Add(Me.leCtaBancaria)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.deFecha)
        Me.Controls.Add(Me.leConceptosCaja)
        Me.Controls.Add(Me.txtConcepto)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.txtValor)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmTransaccionesCorte"
        Me.OptionId = "002008"
        Me.Text = "Ingresos - Egresos"
        Me.TipoFormulario = 2
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        Me.Controls.SetChildIndex(Me.txtValor, 0)
        Me.Controls.SetChildIndex(Me.LabelControl3, 0)
        Me.Controls.SetChildIndex(Me.txtConcepto, 0)
        Me.Controls.SetChildIndex(Me.leConceptosCaja, 0)
        Me.Controls.SetChildIndex(Me.deFecha, 0)
        Me.Controls.SetChildIndex(Me.LabelControl4, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        Me.Controls.SetChildIndex(Me.leCtaBancaria, 0)
        Me.Controls.SetChildIndex(Me.xelBanco, 0)
        Me.Controls.SetChildIndex(Me.sbGuardar, 0)
        Me.Controls.SetChildIndex(Me.sbCerrar, 0)
        Me.Controls.SetChildIndex(Me.LabelControl5, 0)
        Me.Controls.SetChildIndex(Me.LabelControl6, 0)
        Me.Controls.SetChildIndex(Me.leSucursal, 0)
        Me.Controls.SetChildIndex(Me.lePuntoVenta, 0)
        CType(Me.txtValor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leConceptosCaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.reIdConcepto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.reIdCuentaBancaria, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtValor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leConceptosCaja As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdCuentaBancaria As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents reIdConcepto As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents reIdCuentaBancaria As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents xelBanco As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCtaBancaria As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcIdComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents sbCerrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lePuntoVenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl

End Class
