﻿Imports NexusELL.TableEntities

Public Class pre_MantNuevoPresupuesto

    Private IdPresupuesto As Integer

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        DbMode = DbModeType.insert

    End Sub

    Public Sub New(ByVal pIdPresupuesto As Integer)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        IdPresupuesto = pIdPresupuesto
        DbMode = DbModeType.update

    End Sub

    Private Sub pre_MantNuevoPresupuesto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objCombos.adm_Sucursales(lueSucursales, objMenu.User, "")
        seAnio.Value = DateTime.Now.Year

        Select Case DbMode
            Case DbModeType.insert
                '
            Case DbModeType.update
                '
                Dim pre As pre_Presupuestos = objTablas.pre_PresupuestosSelectByPK(IdPresupuesto)

                lueSucursales.EditValue = pre.IdSucursalFk
                objCombos.pre_Departamentos(lueDepartamentos, lueSucursales.EditValue)
                lueDepartamentos.EditValue = pre.IdDepartamentoFk
                objCombos.con_CentrosCosto(lueCentroCostos, "", "")
                lueCentroCostos.EditValue = pre.IdCentroCostosFk
                seAnio.Value = pre.Anio
                meComentario.Text = pre.Comentario

        End Select

    End Sub

    Private Sub lueDepartamentos_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lueDepartamentos.EditValueChanged
        objCombos.con_CentrosCosto(lueCentroCostos, "", "")
    End Sub

    Private Sub lueSucursales_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lueSucursales.EditValueChanged
        objCombos.pre_Departamentos(lueDepartamentos, lueSucursales.EditValue)
    End Sub

    Private Sub sbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGuardar.Click

        If lueSucursales.EditValue = 0 Then
            MsgBox("Debe de seleccionar una sucursal.", MsgBoxStyle.Exclamation)
            lueSucursales.Focus()
            Return
        End If

        If lueDepartamentos.EditValue = 0 Then
            MsgBox("Debe de seleccionar una departamento.", MsgBoxStyle.Exclamation)
            lueDepartamentos.Focus()
            Return
        End If

        If lueCentroCostos.EditValue < 1 Then
            MsgBox("Debe de seleccionar un centro de costo.", MsgBoxStyle.Exclamation)
            lueCentroCostos.Focus()
            Return
        End If

        If objTablas.pre_PresupuestosSelectRepeated(lueSucursales.EditValue, lueDepartamentos.EditValue, lueCentroCostos.EditValue, seAnio.Value) > 0 Then
            If DbMode = DbModeType.insert Then
                MsgBox("Ya existe un presupuesto con estos datos.", MsgBoxStyle.Exclamation)
                Return
            End If
        End If

        Dim entidad As New pre_Presupuestos

        With entidad
            .IdSucursalFk = lueSucursales.EditValue
            .IdDepartamentoFk = lueDepartamentos.EditValue
            .IdCentroCostosFk = lueCentroCostos.EditValue
            .Anio = Convert.ToInt32(seAnio.Value)
            .Comentario = meComentario.Text
            .FechaCreacion = DateTime.Now
        End With

        Select Case DbMode
            Case DbModeType.insert
                '
                Try
                    objTablas.pre_PresupuestosInsert(entidad)
                    MsgBox("El registro a sido insertado con exito.", MsgBoxStyle.Information)
                    DialogResult = DialogResult.OK
                Catch ex As Exception
                    MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
                    DialogResult = DialogResult.Cancel
                End Try
            Case DbModeType.update
                '
                Try
                    entidad.IdPresupuestoPk = IdPresupuesto
                    objTablas.pre_PresupuestosUpdate(entidad)
                    MsgBox("El registro a sido actualizado con exito.", MsgBoxStyle.Information)
                    DialogResult = DialogResult.OK
                Catch ex As Exception
                    MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
                    DialogResult = DialogResult.Cancel
                End Try
        End Select

    End Sub

    Private Sub sbCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCancelar.Click
        Close()
    End Sub

End Class
