﻿Imports NexusBLL
Imports System.IO
Public Class con_frmGenerarArchivosICV
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Private Sub btGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGenerar.Click
        Dim fbd As New FolderBrowserDialog

        fbd.ShowDialog()
        If fbd.SelectedPath = "" Then
            MsgBox("No se seleccionó ninguna carpeta", MsgBoxStyle.Critical, "Nota")
            Return
        End If

        Dim ds As DataSet = bl.pro_ObtenerDatosICV(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        
        Try
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-01"), ds.Tables(0))
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-02"), ds.Tables(1))
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-03"), ds.Tables(2))
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-04"), ds.Tables(3))
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-05"), ds.Tables(4))
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-06"), ds.Tables(5))
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-07"), ds.Tables(6))
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-08"), ds.Tables(7))
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-09"), ds.Tables(8))

            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-10"), ds.Tables(9))
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-11"), ds.Tables(10))
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-12"), ds.Tables(11))
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "ICV-13"), ds.Tables(12))

            MsgBox("Los archivos han sido generado con éxito." + Chr(13) + "Y fueron colocados en " + fbd.SelectedPath, MsgBoxStyle.Information, "Nota")

        Catch ex As Exception
            MsgBox("NO FUE POSIBLE GENERAR LOS ARCHIVOS" + Chr(13) + ex.Message.ToString, MsgBoxStyle.Critical, "Nota")
        End Try
    End Sub
    Private Sub GenerarArchivo(ByVal Archivo As String, ByVal dt As DataTable)
        'FileOpen(1, Archivo, OpenMode.Output)
        'Dim sb As String = ""
        'Dim dc As DataColumn

        'Dim i As Integer = 0
        'Dim dr As DataRow
        'For Each dr In dt.Rows
        '    i = 0 : sb = ""
        '    For Each dc In dt.Columns
        '        If Not IsDBNull(dr(i)) Then
        '            sb &= CStr(dr(i))
        '        End If
        '        i += 1
        '    Next
        '    PrintLine(1, sb)
        'Next
        'FileClose(1)


        Dim sw As New StreamWriter(Archivo)
        Dim separator As String = ";"
        Dim delimiter As String = ""

        Try
            For Each fila As DataRow In dt.Rows
                Dim sepText As String = ""
                Dim linea As String = ""
                For i As Integer = 0 To dt.Columns.Count - 1
                    linea = linea & sepText & delimiter & fila.Item(i).ToString.Trim & delimiter
                    sepText = separator
                Next
                sw.WriteLine(linea)
            Next
            sw.Close()
            'MsgBox("El archivo se genero con éxito" + Chr(13), MsgBoxStyle.Information, "Nota")
        Catch ex As Exception
            sw.Close()
            MsgBox("Hubo un error al generar el archivo" + Chr(13) + ex.Message.ToString, MsgBoxStyle.Information, "Nota")
        End Try

    End Sub

    Private Sub con_frmGenerarArchivosICV_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        leSucursal.EditValue = piIdSucursalUsuario
    End Sub
End Class
