<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmConsultaCuentas
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(con_frmConsultaCuentas))
        Me.gcProductos = New DevExpress.XtraGrid.GridControl()
        Me.gvProd = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.rgTipoBusqueda = New DevExpress.XtraEditors.RadioGroup()
        Me.teCodigo = New DevExpress.XtraEditors.TextEdit()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        CType(Me.gcProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipoBusqueda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcProductos
        '
        Me.gcProductos.Location = New System.Drawing.Point(2, 47)
        Me.gcProductos.MainView = Me.gvProd
        Me.gcProductos.Name = "gcProductos"
        Me.gcProductos.Size = New System.Drawing.Size(591, 473)
        Me.gcProductos.TabIndex = 2
        Me.gcProductos.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvProd})
        '
        'gvProd
        '
        Me.gvProd.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.gvProd.GridControl = Me.gcProductos
        Me.gvProd.Name = "gvProd"
        Me.gvProd.OptionsBehavior.Editable = False
        Me.gvProd.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Cuenta Contable"
        Me.GridColumn1.FieldName = "IdCuenta"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 184
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombre"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 377
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(115, 4)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl24.TabIndex = 49
        Me.LabelControl24.Text = "Tipo de Busqueda:"
        '
        'rgTipoBusqueda
        '
        Me.rgTipoBusqueda.Location = New System.Drawing.Point(206, 1)
        Me.rgTipoBusqueda.Name = "rgTipoBusqueda"
        Me.rgTipoBusqueda.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Contiene"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Inicia Con")})
        Me.rgTipoBusqueda.Size = New System.Drawing.Size(153, 21)
        Me.rgTipoBusqueda.TabIndex = 48
        '
        'teCodigo
        '
        Me.teCodigo.Location = New System.Drawing.Point(18, 26)
        Me.teCodigo.Name = "teCodigo"
        Me.teCodigo.Size = New System.Drawing.Size(186, 20)
        Me.teCodigo.TabIndex = 1
        '
        'teNombre
        '
        Me.teNombre.Location = New System.Drawing.Point(206, 26)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(387, 20)
        Me.teNombre.TabIndex = 0
        '
        'con_frmConsultaCuentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(602, 525)
        Me.Controls.Add(Me.LabelControl24)
        Me.Controls.Add(Me.rgTipoBusqueda)
        Me.Controls.Add(Me.teCodigo)
        Me.Controls.Add(Me.teNombre)
        Me.Controls.Add(Me.gcProductos)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "con_frmConsultaCuentas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Consulta de Cuentas"
        CType(Me.gcProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipoBusqueda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gcProductos As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvProd As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgTipoBusqueda As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents teCodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
End Class
