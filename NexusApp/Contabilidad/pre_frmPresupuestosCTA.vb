﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class pre_frmPresupuestosCTA

    Private blPre As New ContabilidadBLL(g_ConnectionString)
    Dim IdSucursal As Integer = 1

    Public Sub CargarGridPresupuestos()
        gcPresupuestos.DataSource = blPre.pre_ObtenerPresupuestosCTA()
        gvPresupuestos.Columns("FechaHoraCreacion").DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        gvPresupuestos.Columns("FechaHoraCreacion").DisplayFormat.FormatString = "g"
        gvPresupuestos.BestFitColumns()
    End Sub

    Private Sub pre_frmPresupuestos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargarGridPresupuestos()
    End Sub

    Private Sub sbNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbNuevo.Click

        Dim frm As New pre_MantNuevoPresupuestoCTA
        If frm.ShowDialog(Me) = DialogResult.OK Then
            CargarGridPresupuestos()
        End If

    End Sub

    Private Sub sbEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbEditar.Click

        Try
            Dim idPresupuesto As Integer = gvPresupuestos.GetFocusedRowCellDisplayText("IdPresupuesto")

            Dim frm As New pre_MantNuevoPresupuestoCTA(idPresupuesto)
            If frm.ShowDialog(Me) = DialogResult.OK Then
                CargarGridPresupuestos()
            End If

        Catch ex As Exception
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub sbAsociarCuentas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbAsociarCuentas.Click

        Try
            Dim idPresupuesto As Integer = gvPresupuestos.GetFocusedRowCellDisplayText("IdPresupuesto")

            Dim frm As New pre_frmConfigPresupuestosCTA(idPresupuesto)
            If frm.ShowDialog(Me) = DialogResult.OK Then
                CargarGridPresupuestos()
            End If

        Catch ex As Exception
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
        End Try

        CargarGridPresupuestos()

    End Sub

    Private Sub gcPresupuestos_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gcPresupuestos.DoubleClick

        If gvPresupuestos.SelectedRowsCount = 1 Then
            Try
                'Dim numCuentas As Integer = gvPresupuestos.GetFocusedRowCellDisplayText("Cuentas")
                Dim idPresupuesto As Integer = gvPresupuestos.GetFocusedRowCellDisplayText("IdPresupuesto")

                Dim frm As New pre_frmConfigPresupuestosCTA(idPresupuesto)
                frm.ShowDialog(Me)

                CargarGridPresupuestos()

            Catch ex As Exception
                MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
            End Try

        End If

    End Sub

End Class
