﻿Imports NexusBLL
Public Class con_frmExcluirCuentas
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Private Sub con_frmExcluirCuentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Usuarios(leUsuarios, "")
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles SimpleButton1.Click
        gc.DataSource = bl.ObtenerCuentasExcluidas(leUsuarios.EditValue)
    End Sub

    Private Sub SimpleButton2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles SimpleButton2.Click
        gc.DataSource = bl.ObtenerCuentasAExcluir(BeCtaContable1.beIdCuenta.EditValue, BeCtaContable2.beIdCuenta.EditValue)
    End Sub

    Private Sub SimpleButton3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles SimpleButton3.Click
        For i = 0 To gv.RowCount - 1
            bl.InsertarCuentaExcluida(leUsuarios.EditValue, gv.GetRowCellValue(i, "IdCuenta"), gv.GetRowCellValue(i, "Motivo"), objMenu.User, gv.GetRowCellValue(i, "Excluir"))
        Next
        MsgBox("Las cambios han sido aplicados con éxito", MsgBoxStyle.Information, "Nota")
    End Sub
End Class
