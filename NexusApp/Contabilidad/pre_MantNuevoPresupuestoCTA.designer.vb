﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pre_MantNuevoPresupuestoCTA
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.meComentario = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.seAnio = New DevExpress.XtraEditors.SpinEdit()
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.sbCancelar = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.meComentario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl42
        '
        Me.LabelControl42.Location = New System.Drawing.Point(24, 55)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl42.TabIndex = 91
        Me.LabelControl42.Text = "Comentario:"
        '
        'meComentario
        '
        Me.meComentario.Location = New System.Drawing.Point(89, 52)
        Me.meComentario.Name = "meComentario"
        Me.meComentario.Size = New System.Drawing.Size(249, 76)
        Me.meComentario.TabIndex = 90
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(11, 20)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl3.TabIndex = 89
        Me.LabelControl3.Text = "Año / Ejercicio:"
        '
        'seAnio
        '
        Me.seAnio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seAnio.Location = New System.Drawing.Point(89, 17)
        Me.seAnio.Name = "seAnio"
        Me.seAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seAnio.Properties.IsFloatValue = False
        Me.seAnio.Properties.Mask.EditMask = "N00"
        Me.seAnio.Size = New System.Drawing.Size(128, 20)
        Me.seAnio.TabIndex = 88
        '
        'sbGuardar
        '
        Me.sbGuardar.Location = New System.Drawing.Point(75, 143)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(75, 23)
        Me.sbGuardar.TabIndex = 92
        Me.sbGuardar.Text = "Guardar"
        '
        'sbCancelar
        '
        Me.sbCancelar.Location = New System.Drawing.Point(239, 143)
        Me.sbCancelar.Name = "sbCancelar"
        Me.sbCancelar.Size = New System.Drawing.Size(75, 23)
        Me.sbCancelar.TabIndex = 93
        Me.sbCancelar.Text = "Cancelar"
        '
        'pre_MantNuevoPresupuesto
        '
        Me.ClientSize = New System.Drawing.Size(387, 201)
        Me.Controls.Add(Me.sbCancelar)
        Me.Controls.Add(Me.sbGuardar)
        Me.Controls.Add(Me.LabelControl42)
        Me.Controls.Add(Me.meComentario)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.seAnio)
        Me.Modulo = "Presupuestos"
        Me.Name = "pre_MantNuevoPresupuesto"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Presupuesto"
        Me.Controls.SetChildIndex(Me.seAnio, 0)
        Me.Controls.SetChildIndex(Me.LabelControl3, 0)
        Me.Controls.SetChildIndex(Me.meComentario, 0)
        Me.Controls.SetChildIndex(Me.LabelControl42, 0)
        Me.Controls.SetChildIndex(Me.sbGuardar, 0)
        Me.Controls.SetChildIndex(Me.sbCancelar, 0)
        CType(Me.meComentario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meComentario As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbCancelar As DevExpress.XtraEditors.SimpleButton

End Class
