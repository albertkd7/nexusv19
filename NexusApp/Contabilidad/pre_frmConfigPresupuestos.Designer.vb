﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pre_frmConfigPresupuestos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lueSucursales = New DevExpress.XtraEditors.LookUpEdit
        Me.lcSucursal = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.lueDepartamentos = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.lueCentroCostos = New DevExpress.XtraEditors.LookUpEdit
        Me.gcCuentasContables = New DevExpress.XtraGrid.GridControl
        Me.gvCuentasContables = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcId = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcCodigo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcCuenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.seAnio = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl
        Me.beEnero = New DevExpress.XtraEditors.ButtonEdit
        Me.beFebrero = New DevExpress.XtraEditors.ButtonEdit
        Me.beMarzo = New DevExpress.XtraEditors.ButtonEdit
        Me.beAbril = New DevExpress.XtraEditors.ButtonEdit
        Me.beMayo = New DevExpress.XtraEditors.ButtonEdit
        Me.beJunio = New DevExpress.XtraEditors.ButtonEdit
        Me.beJulio = New DevExpress.XtraEditors.ButtonEdit
        Me.beAgosto = New DevExpress.XtraEditors.ButtonEdit
        Me.beSeptiembre = New DevExpress.XtraEditors.ButtonEdit
        Me.beOctubre = New DevExpress.XtraEditors.ButtonEdit
        Me.beNoviembre = New DevExpress.XtraEditors.ButtonEdit
        Me.beDiciembre = New DevExpress.XtraEditors.ButtonEdit
        Me.teTotal = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl
        Me.lblEditEnero = New DevExpress.XtraEditors.LabelControl
        Me.lblEditFebrero = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl
        Me.lblEditMarzo = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl
        Me.lblEditAbril = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl
        Me.lblEditMayo = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl
        Me.lblEditJunio = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl
        Me.lblEditJulio = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl
        Me.lblEditAgosto = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl
        Me.lblEditSeptiembre = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl
        Me.lblEditOctubre = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl
        Me.lblEditNoviembre = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl
        Me.lblEditDiciembre = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl
        Me.sbAgregarCuenta = New DevExpress.XtraEditors.SimpleButton
        Me.sbEliminarCuenta = New DevExpress.XtraEditors.SimpleButton
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl
        Me.meComentario = New DevExpress.XtraEditors.MemoEdit
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl
        CType(Me.lueSucursales.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lueDepartamentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lueCentroCostos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcCuentasContables, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvCuentasContables, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beEnero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beFebrero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beMarzo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beAbril.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beMayo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beJunio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beJulio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beAgosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beSeptiembre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beOctubre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beNoviembre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beDiciembre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meComentario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lueSucursales
        '
        Me.lueSucursales.Location = New System.Drawing.Point(116, 24)
        Me.lueSucursales.Name = "lueSucursales"
        Me.lueSucursales.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueSucursales.Properties.ReadOnly = True
        Me.lueSucursales.Size = New System.Drawing.Size(285, 20)
        Me.lueSucursales.TabIndex = 0
        '
        'lcSucursal
        '
        Me.lcSucursal.Location = New System.Drawing.Point(66, 27)
        Me.lcSucursal.Name = "lcSucursal"
        Me.lcSucursal.Size = New System.Drawing.Size(44, 13)
        Me.lcSucursal.TabIndex = 5
        Me.lcSucursal.Text = "Sucursal:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(37, 62)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl1.TabIndex = 7
        Me.LabelControl1.Text = "Departamento:"
        '
        'lueDepartamentos
        '
        Me.lueDepartamentos.Location = New System.Drawing.Point(116, 59)
        Me.lueDepartamentos.Name = "lueDepartamentos"
        Me.lueDepartamentos.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueDepartamentos.Properties.ReadOnly = True
        Me.lueDepartamentos.Size = New System.Drawing.Size(285, 20)
        Me.lueDepartamentos.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(24, 98)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl2.TabIndex = 9
        Me.LabelControl2.Text = "Centro de costos:"
        '
        'lueCentroCostos
        '
        Me.lueCentroCostos.Location = New System.Drawing.Point(116, 95)
        Me.lueCentroCostos.Name = "lueCentroCostos"
        Me.lueCentroCostos.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueCentroCostos.Properties.ReadOnly = True
        Me.lueCentroCostos.Size = New System.Drawing.Size(285, 20)
        Me.lueCentroCostos.TabIndex = 2
        '
        'gcCuentasContables
        '
        Me.gcCuentasContables.Location = New System.Drawing.Point(24, 148)
        Me.gcCuentasContables.MainView = Me.gvCuentasContables
        Me.gcCuentasContables.Name = "gcCuentasContables"
        Me.gcCuentasContables.Size = New System.Drawing.Size(377, 329)
        Me.gcCuentasContables.TabIndex = 5
        Me.gcCuentasContables.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvCuentasContables})
        '
        'gvCuentasContables
        '
        Me.gvCuentasContables.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcId, Me.gcCodigo, Me.gcCuenta})
        Me.gvCuentasContables.GridControl = Me.gcCuentasContables
        Me.gvCuentasContables.Name = "gvCuentasContables"
        Me.gvCuentasContables.OptionsBehavior.Editable = False
        Me.gvCuentasContables.OptionsBehavior.ReadOnly = True
        Me.gvCuentasContables.OptionsView.ShowGroupPanel = False
        '
        'gcId
        '
        Me.gcId.Caption = "IdPresupuestoCuenta"
        Me.gcId.FieldName = "IdPresupuestoCuentaPk"
        Me.gcId.Name = "gcId"
        '
        'gcCodigo
        '
        Me.gcCodigo.Caption = "Codigo"
        Me.gcCodigo.FieldName = "IdCuenta"
        Me.gcCodigo.Name = "gcCodigo"
        Me.gcCodigo.Visible = True
        Me.gcCodigo.VisibleIndex = 0
        '
        'gcCuenta
        '
        Me.gcCuenta.Caption = "Cuenta contable"
        Me.gcCuenta.FieldName = "Nombre"
        Me.gcCuenta.Name = "gcCuenta"
        Me.gcCuenta.Visible = True
        Me.gcCuenta.VisibleIndex = 1
        '
        'seAnio
        '
        Me.seAnio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seAnio.Location = New System.Drawing.Point(493, 24)
        Me.seAnio.Name = "seAnio"
        Me.seAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seAnio.Properties.IsFloatValue = False
        Me.seAnio.Properties.Mask.EditMask = "N00"
        Me.seAnio.Properties.ReadOnly = True
        Me.seAnio.Size = New System.Drawing.Size(109, 20)
        Me.seAnio.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(412, 27)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl3.TabIndex = 13
        Me.LabelControl3.Text = "Año / Ejercicio:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(452, 148)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl4.TabIndex = 27
        Me.LabelControl4.Text = "Enero:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(442, 174)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(42, 13)
        Me.LabelControl5.TabIndex = 28
        Me.LabelControl5.Text = "Febrero:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(451, 200)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl6.TabIndex = 29
        Me.LabelControl6.Text = "Marzo:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(459, 226)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl7.TabIndex = 30
        Me.LabelControl7.Text = "Abril:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(454, 252)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(30, 13)
        Me.LabelControl8.TabIndex = 31
        Me.LabelControl8.Text = "Mayo:"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(455, 278)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl9.TabIndex = 32
        Me.LabelControl9.Text = "Junio:"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(459, 304)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl10.TabIndex = 33
        Me.LabelControl10.Text = "Julio:"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(446, 330)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl11.TabIndex = 34
        Me.LabelControl11.Text = "Agosto:"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(426, 356)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl12.TabIndex = 35
        Me.LabelControl12.Text = "Septiembre:"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(445, 382)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl13.TabIndex = 36
        Me.LabelControl13.Text = "Octubre"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(429, 408)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl14.TabIndex = 37
        Me.LabelControl14.Text = "Noviembre:"
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(434, 434)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl15.TabIndex = 38
        Me.LabelControl15.Text = "Diciembre:"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl16.Location = New System.Drawing.Point(448, 460)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl16.TabIndex = 39
        Me.LabelControl16.Text = "TOTAL:"
        '
        'beEnero
        '
        Me.beEnero.EditValue = "0"
        Me.beEnero.Location = New System.Drawing.Point(493, 145)
        Me.beEnero.Name = "beEnero"
        Me.beEnero.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beEnero.Properties.ReadOnly = True
        Me.beEnero.Size = New System.Drawing.Size(100, 20)
        Me.beEnero.TabIndex = 6
        Me.beEnero.Tag = "1"
        '
        'beFebrero
        '
        Me.beFebrero.EditValue = "0"
        Me.beFebrero.Location = New System.Drawing.Point(493, 171)
        Me.beFebrero.Name = "beFebrero"
        Me.beFebrero.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beFebrero.Properties.ReadOnly = True
        Me.beFebrero.Size = New System.Drawing.Size(100, 20)
        Me.beFebrero.TabIndex = 7
        Me.beFebrero.Tag = "2"
        '
        'beMarzo
        '
        Me.beMarzo.EditValue = "0"
        Me.beMarzo.Location = New System.Drawing.Point(493, 197)
        Me.beMarzo.Name = "beMarzo"
        Me.beMarzo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beMarzo.Properties.ReadOnly = True
        Me.beMarzo.Size = New System.Drawing.Size(100, 20)
        Me.beMarzo.TabIndex = 8
        Me.beMarzo.Tag = "3"
        '
        'beAbril
        '
        Me.beAbril.EditValue = "0"
        Me.beAbril.Location = New System.Drawing.Point(493, 223)
        Me.beAbril.Name = "beAbril"
        Me.beAbril.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beAbril.Properties.ReadOnly = True
        Me.beAbril.Size = New System.Drawing.Size(100, 20)
        Me.beAbril.TabIndex = 9
        Me.beAbril.Tag = "4"
        '
        'beMayo
        '
        Me.beMayo.EditValue = "0"
        Me.beMayo.Location = New System.Drawing.Point(493, 249)
        Me.beMayo.Name = "beMayo"
        Me.beMayo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beMayo.Properties.ReadOnly = True
        Me.beMayo.Size = New System.Drawing.Size(100, 20)
        Me.beMayo.TabIndex = 10
        Me.beMayo.Tag = "5"
        '
        'beJunio
        '
        Me.beJunio.EditValue = "0"
        Me.beJunio.Location = New System.Drawing.Point(493, 275)
        Me.beJunio.Name = "beJunio"
        Me.beJunio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beJunio.Properties.ReadOnly = True
        Me.beJunio.Size = New System.Drawing.Size(100, 20)
        Me.beJunio.TabIndex = 11
        Me.beJunio.Tag = "6"
        '
        'beJulio
        '
        Me.beJulio.EditValue = "0"
        Me.beJulio.Location = New System.Drawing.Point(493, 301)
        Me.beJulio.Name = "beJulio"
        Me.beJulio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beJulio.Properties.ReadOnly = True
        Me.beJulio.Size = New System.Drawing.Size(100, 20)
        Me.beJulio.TabIndex = 12
        Me.beJulio.Tag = "7"
        '
        'beAgosto
        '
        Me.beAgosto.EditValue = "0"
        Me.beAgosto.Location = New System.Drawing.Point(493, 327)
        Me.beAgosto.Name = "beAgosto"
        Me.beAgosto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beAgosto.Properties.ReadOnly = True
        Me.beAgosto.Size = New System.Drawing.Size(100, 20)
        Me.beAgosto.TabIndex = 13
        Me.beAgosto.Tag = "8"
        '
        'beSeptiembre
        '
        Me.beSeptiembre.EditValue = "0"
        Me.beSeptiembre.Location = New System.Drawing.Point(493, 353)
        Me.beSeptiembre.Name = "beSeptiembre"
        Me.beSeptiembre.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beSeptiembre.Properties.ReadOnly = True
        Me.beSeptiembre.Size = New System.Drawing.Size(100, 20)
        Me.beSeptiembre.TabIndex = 14
        Me.beSeptiembre.Tag = "9"
        '
        'beOctubre
        '
        Me.beOctubre.EditValue = "0"
        Me.beOctubre.Location = New System.Drawing.Point(493, 379)
        Me.beOctubre.Name = "beOctubre"
        Me.beOctubre.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beOctubre.Properties.ReadOnly = True
        Me.beOctubre.Size = New System.Drawing.Size(100, 20)
        Me.beOctubre.TabIndex = 15
        Me.beOctubre.Tag = "10"
        '
        'beNoviembre
        '
        Me.beNoviembre.EditValue = "0"
        Me.beNoviembre.Location = New System.Drawing.Point(493, 405)
        Me.beNoviembre.Name = "beNoviembre"
        Me.beNoviembre.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beNoviembre.Properties.ReadOnly = True
        Me.beNoviembre.Size = New System.Drawing.Size(100, 20)
        Me.beNoviembre.TabIndex = 16
        Me.beNoviembre.Tag = "11"
        '
        'beDiciembre
        '
        Me.beDiciembre.EditValue = "0"
        Me.beDiciembre.Location = New System.Drawing.Point(493, 431)
        Me.beDiciembre.Name = "beDiciembre"
        Me.beDiciembre.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beDiciembre.Properties.ReadOnly = True
        Me.beDiciembre.Size = New System.Drawing.Size(100, 20)
        Me.beDiciembre.TabIndex = 17
        Me.beDiciembre.Tag = "12"
        '
        'teTotal
        '
        Me.teTotal.EditValue = "0"
        Me.teTotal.Location = New System.Drawing.Point(493, 457)
        Me.teTotal.Name = "teTotal"
        Me.teTotal.Properties.ReadOnly = True
        Me.teTotal.Size = New System.Drawing.Size(147, 20)
        Me.teTotal.TabIndex = 52
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(603, 148)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl17.TabIndex = 53
        Me.LabelControl17.Text = "# Ediciones:"
        '
        'lblEditEnero
        '
        Me.lblEditEnero.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEditEnero.Location = New System.Drawing.Point(677, 148)
        Me.lblEditEnero.Name = "lblEditEnero"
        Me.lblEditEnero.Size = New System.Drawing.Size(6, 13)
        Me.lblEditEnero.TabIndex = 54
        Me.lblEditEnero.Tag = "1"
        Me.lblEditEnero.Text = "0"
        '
        'lblEditFebrero
        '
        Me.lblEditFebrero.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEditFebrero.Location = New System.Drawing.Point(677, 174)
        Me.lblEditFebrero.Name = "lblEditFebrero"
        Me.lblEditFebrero.Size = New System.Drawing.Size(6, 13)
        Me.lblEditFebrero.TabIndex = 56
        Me.lblEditFebrero.Tag = "2"
        Me.lblEditFebrero.Text = "0"
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(603, 174)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl20.TabIndex = 55
        Me.LabelControl20.Text = "# Ediciones:"
        '
        'lblEditMarzo
        '
        Me.lblEditMarzo.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEditMarzo.Location = New System.Drawing.Point(677, 200)
        Me.lblEditMarzo.Name = "lblEditMarzo"
        Me.lblEditMarzo.Size = New System.Drawing.Size(6, 13)
        Me.lblEditMarzo.TabIndex = 58
        Me.lblEditMarzo.Tag = "3"
        Me.lblEditMarzo.Text = "0"
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(603, 200)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl22.TabIndex = 57
        Me.LabelControl22.Text = "# Ediciones:"
        '
        'lblEditAbril
        '
        Me.lblEditAbril.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEditAbril.Location = New System.Drawing.Point(677, 226)
        Me.lblEditAbril.Name = "lblEditAbril"
        Me.lblEditAbril.Size = New System.Drawing.Size(6, 13)
        Me.lblEditAbril.TabIndex = 60
        Me.lblEditAbril.Tag = "4"
        Me.lblEditAbril.Text = "0"
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(603, 226)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl24.TabIndex = 59
        Me.LabelControl24.Text = "# Ediciones:"
        '
        'lblEditMayo
        '
        Me.lblEditMayo.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEditMayo.Location = New System.Drawing.Point(677, 252)
        Me.lblEditMayo.Name = "lblEditMayo"
        Me.lblEditMayo.Size = New System.Drawing.Size(6, 13)
        Me.lblEditMayo.TabIndex = 62
        Me.lblEditMayo.Tag = "5"
        Me.lblEditMayo.Text = "0"
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(603, 252)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl26.TabIndex = 61
        Me.LabelControl26.Text = "# Ediciones:"
        '
        'lblEditJunio
        '
        Me.lblEditJunio.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEditJunio.Location = New System.Drawing.Point(677, 278)
        Me.lblEditJunio.Name = "lblEditJunio"
        Me.lblEditJunio.Size = New System.Drawing.Size(6, 13)
        Me.lblEditJunio.TabIndex = 64
        Me.lblEditJunio.Tag = "6"
        Me.lblEditJunio.Text = "0"
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(603, 278)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl28.TabIndex = 63
        Me.LabelControl28.Text = "# Ediciones:"
        '
        'lblEditJulio
        '
        Me.lblEditJulio.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEditJulio.Location = New System.Drawing.Point(677, 304)
        Me.lblEditJulio.Name = "lblEditJulio"
        Me.lblEditJulio.Size = New System.Drawing.Size(6, 13)
        Me.lblEditJulio.TabIndex = 66
        Me.lblEditJulio.Tag = "7"
        Me.lblEditJulio.Text = "0"
        '
        'LabelControl30
        '
        Me.LabelControl30.Location = New System.Drawing.Point(603, 304)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl30.TabIndex = 65
        Me.LabelControl30.Text = "# Ediciones:"
        '
        'lblEditAgosto
        '
        Me.lblEditAgosto.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEditAgosto.Location = New System.Drawing.Point(677, 330)
        Me.lblEditAgosto.Name = "lblEditAgosto"
        Me.lblEditAgosto.Size = New System.Drawing.Size(6, 13)
        Me.lblEditAgosto.TabIndex = 68
        Me.lblEditAgosto.Tag = "8"
        Me.lblEditAgosto.Text = "0"
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(603, 330)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl32.TabIndex = 67
        Me.LabelControl32.Text = "# Ediciones:"
        '
        'lblEditSeptiembre
        '
        Me.lblEditSeptiembre.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEditSeptiembre.Location = New System.Drawing.Point(677, 356)
        Me.lblEditSeptiembre.Name = "lblEditSeptiembre"
        Me.lblEditSeptiembre.Size = New System.Drawing.Size(6, 13)
        Me.lblEditSeptiembre.TabIndex = 70
        Me.lblEditSeptiembre.Tag = "9"
        Me.lblEditSeptiembre.Text = "0"
        '
        'LabelControl34
        '
        Me.LabelControl34.Location = New System.Drawing.Point(603, 356)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl34.TabIndex = 69
        Me.LabelControl34.Text = "# Ediciones:"
        '
        'lblEditOctubre
        '
        Me.lblEditOctubre.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEditOctubre.Location = New System.Drawing.Point(677, 382)
        Me.lblEditOctubre.Name = "lblEditOctubre"
        Me.lblEditOctubre.Size = New System.Drawing.Size(6, 13)
        Me.lblEditOctubre.TabIndex = 72
        Me.lblEditOctubre.Tag = "10"
        Me.lblEditOctubre.Text = "0"
        '
        'LabelControl36
        '
        Me.LabelControl36.Location = New System.Drawing.Point(603, 382)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl36.TabIndex = 71
        Me.LabelControl36.Text = "# Ediciones:"
        '
        'lblEditNoviembre
        '
        Me.lblEditNoviembre.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEditNoviembre.Location = New System.Drawing.Point(677, 408)
        Me.lblEditNoviembre.Name = "lblEditNoviembre"
        Me.lblEditNoviembre.Size = New System.Drawing.Size(6, 13)
        Me.lblEditNoviembre.TabIndex = 74
        Me.lblEditNoviembre.Tag = "11"
        Me.lblEditNoviembre.Text = "0"
        '
        'LabelControl38
        '
        Me.LabelControl38.Location = New System.Drawing.Point(603, 408)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl38.TabIndex = 73
        Me.LabelControl38.Text = "# Ediciones:"
        '
        'lblEditDiciembre
        '
        Me.lblEditDiciembre.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEditDiciembre.Location = New System.Drawing.Point(677, 434)
        Me.lblEditDiciembre.Name = "lblEditDiciembre"
        Me.lblEditDiciembre.Size = New System.Drawing.Size(6, 13)
        Me.lblEditDiciembre.TabIndex = 76
        Me.lblEditDiciembre.Tag = "12"
        Me.lblEditDiciembre.Text = "0"
        '
        'LabelControl40
        '
        Me.LabelControl40.Location = New System.Drawing.Point(603, 434)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl40.TabIndex = 75
        Me.LabelControl40.Text = "# Ediciones:"
        '
        'sbAgregarCuenta
        '
        Me.sbAgregarCuenta.Location = New System.Drawing.Point(198, 485)
        Me.sbAgregarCuenta.Name = "sbAgregarCuenta"
        Me.sbAgregarCuenta.Size = New System.Drawing.Size(93, 23)
        Me.sbAgregarCuenta.TabIndex = 18
        Me.sbAgregarCuenta.Text = "Agregar cuenta"
        '
        'sbEliminarCuenta
        '
        Me.sbEliminarCuenta.Location = New System.Drawing.Point(308, 485)
        Me.sbEliminarCuenta.Name = "sbEliminarCuenta"
        Me.sbEliminarCuenta.Size = New System.Drawing.Size(93, 23)
        Me.sbEliminarCuenta.TabIndex = 19
        Me.sbEliminarCuenta.Text = "Eliminar cuenta"
        '
        'LabelControl41
        '
        Me.LabelControl41.Location = New System.Drawing.Point(24, 129)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(143, 13)
        Me.LabelControl41.TabIndex = 79
        Me.LabelControl41.Text = "Cuentas contables asociadas:"
        '
        'meComentario
        '
        Me.meComentario.Location = New System.Drawing.Point(493, 59)
        Me.meComentario.Name = "meComentario"
        Me.meComentario.Properties.ReadOnly = True
        Me.meComentario.Size = New System.Drawing.Size(194, 56)
        Me.meComentario.TabIndex = 4
        '
        'LabelControl42
        '
        Me.LabelControl42.Location = New System.Drawing.Point(425, 62)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl42.TabIndex = 81
        Me.LabelControl42.Text = "Comentario:"
        '
        'pre_frmConfigPresupuestos
        '
        Me.ClientSize = New System.Drawing.Size(701, 545)
        Me.Controls.Add(Me.LabelControl42)
        Me.Controls.Add(Me.meComentario)
        Me.Controls.Add(Me.LabelControl41)
        Me.Controls.Add(Me.sbEliminarCuenta)
        Me.Controls.Add(Me.sbAgregarCuenta)
        Me.Controls.Add(Me.lblEditDiciembre)
        Me.Controls.Add(Me.LabelControl40)
        Me.Controls.Add(Me.lblEditNoviembre)
        Me.Controls.Add(Me.LabelControl38)
        Me.Controls.Add(Me.lblEditOctubre)
        Me.Controls.Add(Me.LabelControl36)
        Me.Controls.Add(Me.lblEditSeptiembre)
        Me.Controls.Add(Me.LabelControl34)
        Me.Controls.Add(Me.lblEditAgosto)
        Me.Controls.Add(Me.LabelControl32)
        Me.Controls.Add(Me.lblEditJulio)
        Me.Controls.Add(Me.LabelControl30)
        Me.Controls.Add(Me.lblEditJunio)
        Me.Controls.Add(Me.LabelControl28)
        Me.Controls.Add(Me.lblEditMayo)
        Me.Controls.Add(Me.LabelControl26)
        Me.Controls.Add(Me.lblEditAbril)
        Me.Controls.Add(Me.LabelControl24)
        Me.Controls.Add(Me.lblEditMarzo)
        Me.Controls.Add(Me.LabelControl22)
        Me.Controls.Add(Me.lblEditFebrero)
        Me.Controls.Add(Me.LabelControl20)
        Me.Controls.Add(Me.lblEditEnero)
        Me.Controls.Add(Me.LabelControl17)
        Me.Controls.Add(Me.teTotal)
        Me.Controls.Add(Me.beDiciembre)
        Me.Controls.Add(Me.beNoviembre)
        Me.Controls.Add(Me.beOctubre)
        Me.Controls.Add(Me.beSeptiembre)
        Me.Controls.Add(Me.beAgosto)
        Me.Controls.Add(Me.beJulio)
        Me.Controls.Add(Me.beJunio)
        Me.Controls.Add(Me.beMayo)
        Me.Controls.Add(Me.beAbril)
        Me.Controls.Add(Me.beMarzo)
        Me.Controls.Add(Me.beFebrero)
        Me.Controls.Add(Me.beEnero)
        Me.Controls.Add(Me.LabelControl16)
        Me.Controls.Add(Me.LabelControl15)
        Me.Controls.Add(Me.LabelControl14)
        Me.Controls.Add(Me.LabelControl13)
        Me.Controls.Add(Me.LabelControl12)
        Me.Controls.Add(Me.LabelControl11)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.seAnio)
        Me.Controls.Add(Me.gcCuentasContables)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.lueCentroCostos)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.lueDepartamentos)
        Me.Controls.Add(Me.lcSucursal)
        Me.Controls.Add(Me.lueSucursales)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Modulo = "Presupuestos"
        Me.Name = "pre_frmConfigPresupuestos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Configuración presupuesto"
        Me.Controls.SetChildIndex(Me.lueSucursales, 0)
        Me.Controls.SetChildIndex(Me.lcSucursal, 0)
        Me.Controls.SetChildIndex(Me.lueDepartamentos, 0)
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.lueCentroCostos, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        Me.Controls.SetChildIndex(Me.gcCuentasContables, 0)
        Me.Controls.SetChildIndex(Me.seAnio, 0)
        Me.Controls.SetChildIndex(Me.LabelControl3, 0)
        Me.Controls.SetChildIndex(Me.LabelControl4, 0)
        Me.Controls.SetChildIndex(Me.LabelControl5, 0)
        Me.Controls.SetChildIndex(Me.LabelControl6, 0)
        Me.Controls.SetChildIndex(Me.LabelControl7, 0)
        Me.Controls.SetChildIndex(Me.LabelControl8, 0)
        Me.Controls.SetChildIndex(Me.LabelControl9, 0)
        Me.Controls.SetChildIndex(Me.LabelControl10, 0)
        Me.Controls.SetChildIndex(Me.LabelControl11, 0)
        Me.Controls.SetChildIndex(Me.LabelControl12, 0)
        Me.Controls.SetChildIndex(Me.LabelControl13, 0)
        Me.Controls.SetChildIndex(Me.LabelControl14, 0)
        Me.Controls.SetChildIndex(Me.LabelControl15, 0)
        Me.Controls.SetChildIndex(Me.LabelControl16, 0)
        Me.Controls.SetChildIndex(Me.beEnero, 0)
        Me.Controls.SetChildIndex(Me.beFebrero, 0)
        Me.Controls.SetChildIndex(Me.beMarzo, 0)
        Me.Controls.SetChildIndex(Me.beAbril, 0)
        Me.Controls.SetChildIndex(Me.beMayo, 0)
        Me.Controls.SetChildIndex(Me.beJunio, 0)
        Me.Controls.SetChildIndex(Me.beJulio, 0)
        Me.Controls.SetChildIndex(Me.beAgosto, 0)
        Me.Controls.SetChildIndex(Me.beSeptiembre, 0)
        Me.Controls.SetChildIndex(Me.beOctubre, 0)
        Me.Controls.SetChildIndex(Me.beNoviembre, 0)
        Me.Controls.SetChildIndex(Me.beDiciembre, 0)
        Me.Controls.SetChildIndex(Me.teTotal, 0)
        Me.Controls.SetChildIndex(Me.LabelControl17, 0)
        Me.Controls.SetChildIndex(Me.lblEditEnero, 0)
        Me.Controls.SetChildIndex(Me.LabelControl20, 0)
        Me.Controls.SetChildIndex(Me.lblEditFebrero, 0)
        Me.Controls.SetChildIndex(Me.LabelControl22, 0)
        Me.Controls.SetChildIndex(Me.lblEditMarzo, 0)
        Me.Controls.SetChildIndex(Me.LabelControl24, 0)
        Me.Controls.SetChildIndex(Me.lblEditAbril, 0)
        Me.Controls.SetChildIndex(Me.LabelControl26, 0)
        Me.Controls.SetChildIndex(Me.lblEditMayo, 0)
        Me.Controls.SetChildIndex(Me.LabelControl28, 0)
        Me.Controls.SetChildIndex(Me.lblEditJunio, 0)
        Me.Controls.SetChildIndex(Me.LabelControl30, 0)
        Me.Controls.SetChildIndex(Me.lblEditJulio, 0)
        Me.Controls.SetChildIndex(Me.LabelControl32, 0)
        Me.Controls.SetChildIndex(Me.lblEditAgosto, 0)
        Me.Controls.SetChildIndex(Me.LabelControl34, 0)
        Me.Controls.SetChildIndex(Me.lblEditSeptiembre, 0)
        Me.Controls.SetChildIndex(Me.LabelControl36, 0)
        Me.Controls.SetChildIndex(Me.lblEditOctubre, 0)
        Me.Controls.SetChildIndex(Me.LabelControl38, 0)
        Me.Controls.SetChildIndex(Me.lblEditNoviembre, 0)
        Me.Controls.SetChildIndex(Me.LabelControl40, 0)
        Me.Controls.SetChildIndex(Me.lblEditDiciembre, 0)
        Me.Controls.SetChildIndex(Me.sbAgregarCuenta, 0)
        Me.Controls.SetChildIndex(Me.sbEliminarCuenta, 0)
        Me.Controls.SetChildIndex(Me.LabelControl41, 0)
        Me.Controls.SetChildIndex(Me.meComentario, 0)
        Me.Controls.SetChildIndex(Me.LabelControl42, 0)
        CType(Me.lueSucursales.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lueDepartamentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lueCentroCostos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcCuentasContables, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvCuentasContables, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beEnero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beFebrero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beMarzo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beAbril.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beMayo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beJunio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beJulio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beAgosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beSeptiembre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beOctubre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beNoviembre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beDiciembre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meComentario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lueSucursales As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lcSucursal As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lueDepartamentos As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lueCentroCostos As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents gcCuentasContables As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvCuentasContables As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents seAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beEnero As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beFebrero As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beMarzo As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beAbril As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beMayo As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beJunio As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beJulio As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beAgosto As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beSeptiembre As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beOctubre As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beNoviembre As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beDiciembre As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents teTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEditEnero As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEditFebrero As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEditMarzo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEditAbril As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEditMayo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEditJunio As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEditJulio As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEditAgosto As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEditSeptiembre As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEditOctubre As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEditNoviembre As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEditDiciembre As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbAgregarCuenta As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbEliminarCuenta As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcCodigo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents meComentario As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcId As DevExpress.XtraGrid.Columns.GridColumn

End Class
