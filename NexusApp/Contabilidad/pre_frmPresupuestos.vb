﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class pre_frmPresupuestos

    Private blPre As New ContabilidadBLL(g_ConnectionString)
    Dim IdSucursal As Integer = 1

    Public Sub CargarGridPresupuestos()
        gcPresupuestos.DataSource = blPre.pre_ObtenerPresupuestos()
        gvPresupuestos.BestFitColumns()
    End Sub

    Private Sub pre_frmPresupuestos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CargarGridPresupuestos()
        IdSucursal = gvPresupuestos.GetFocusedRowCellValue(gvPresupuestos.Columns(0))
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.pre_Departamentos(leDepartamento, IdSucursal)
        objCombos.con_CentrosCosto(leCentroCosto, "", "")
    End Sub

    Private Sub sbNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbNuevo.Click

        Dim frm As New pre_MantNuevoPresupuesto
        If frm.ShowDialog(Me) = DialogResult.OK Then
            CargarGridPresupuestos()
        End If

    End Sub

    Private Sub sbEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbEditar.Click

        Try
            Dim idPresupuesto As Integer = gvPresupuestos.GetFocusedRowCellDisplayText("IdPresupuestoPk")

            Dim frm As New pre_MantNuevoPresupuesto(idPresupuesto)
            If frm.ShowDialog(Me) = DialogResult.OK Then
                CargarGridPresupuestos()
            End If

        Catch ex As Exception
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub sbAsociarCuentas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbAsociarCuentas.Click

        Try
            Dim idPresupuesto As Integer = gvPresupuestos.GetFocusedRowCellDisplayText("IdPresupuestoPk")

            Dim frm As New pre_frmConfigPresupuestos(idPresupuesto)
            If frm.ShowDialog(Me) = DialogResult.OK Then
                CargarGridPresupuestos()
            End If

        Catch ex As Exception
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
        End Try

        CargarGridPresupuestos()

    End Sub

    Private Sub gcPresupuestos_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gcPresupuestos.DoubleClick

        If gvPresupuestos.SelectedRowsCount = 1 Then
            Try
                'Dim numCuentas As Integer = gvPresupuestos.GetFocusedRowCellDisplayText("Cuentas")
                Dim idPresupuesto As Integer = gvPresupuestos.GetFocusedRowCellDisplayText("IdPresupuestoPk")

                Dim frm As New pre_frmConfigPresupuestos(idPresupuesto)
                frm.ShowDialog(Me)

                CargarGridPresupuestos()

            Catch ex As Exception
                MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
            End Try

        End If

    End Sub

End Class
