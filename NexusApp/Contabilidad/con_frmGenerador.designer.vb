﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmGenerador
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.deFecIni = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.deFecFin = New DevExpress.XtraEditors.DateEdit
        Me.sbGenerar = New DevExpress.XtraEditors.SimpleButton
        Me.sbToExcel = New DevExpress.XtraEditors.SimpleButton
        Me.sbToPdf = New DevExpress.XtraEditors.SimpleButton
        Me.sbMostrarOcultar = New DevExpress.XtraEditors.SimpleButton
        Me.sbToText = New DevExpress.XtraEditors.SimpleButton
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecIni.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecIni.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecFin.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecFin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.sbToText)
        Me.GroupControl1.Controls.Add(Me.sbToPdf)
        Me.GroupControl1.Controls.Add(Me.sbToExcel)
        Me.GroupControl1.Controls.Add(Me.sbMostrarOcultar)
        Me.GroupControl1.Controls.Add(Me.sbGenerar)
        Me.GroupControl1.Controls.Add(Me.deFecFin)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.deFecIni)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Size = New System.Drawing.Size(842, 120)
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 120)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(842, 169)
        Me.gc.TabIndex = 3
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsFilter.ShowAllTableValuesInFilterPopup = True
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(45, 33)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(63, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Fecha Inicial:"
        '
        'deFecIni
        '
        Me.deFecIni.EditValue = Nothing
        Me.deFecIni.Location = New System.Drawing.Point(114, 30)
        Me.deFecIni.Name = "deFecIni"
        Me.deFecIni.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecIni.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecIni.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFecIni.Size = New System.Drawing.Size(100, 20)
        Me.deFecIni.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(230, 33)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Hasta Fecha:"
        '
        'deFecFin
        '
        Me.deFecFin.EditValue = Nothing
        Me.deFecFin.Location = New System.Drawing.Point(300, 30)
        Me.deFecFin.Name = "deFecFin"
        Me.deFecFin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecFin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecFin.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFecFin.Size = New System.Drawing.Size(100, 20)
        Me.deFecFin.TabIndex = 1
        '
        'sbGenerar
        '
        Me.sbGenerar.Location = New System.Drawing.Point(114, 75)
        Me.sbGenerar.Name = "sbGenerar"
        Me.sbGenerar.Size = New System.Drawing.Size(100, 23)
        Me.sbGenerar.TabIndex = 2
        Me.sbGenerar.Text = "Generar..."
        '
        'sbToExcel
        '
        Me.sbToExcel.Location = New System.Drawing.Point(425, 27)
        Me.sbToExcel.Name = "sbToExcel"
        Me.sbToExcel.Size = New System.Drawing.Size(107, 23)
        Me.sbToExcel.TabIndex = 3
        Me.sbToExcel.Text = "Exportar a Excel"
        '
        'sbToPdf
        '
        Me.sbToPdf.Location = New System.Drawing.Point(539, 27)
        Me.sbToPdf.Name = "sbToPdf"
        Me.sbToPdf.Size = New System.Drawing.Size(107, 23)
        Me.sbToPdf.TabIndex = 4
        Me.sbToPdf.Text = "Exportar a PDF"
        '
        'sbMostrarOcultar
        '
        Me.sbMostrarOcultar.Location = New System.Drawing.Point(230, 76)
        Me.sbMostrarOcultar.Name = "sbMostrarOcultar"
        Me.sbMostrarOcultar.Size = New System.Drawing.Size(170, 23)
        Me.sbMostrarOcultar.TabIndex = 4
        Me.sbMostrarOcultar.Text = "Ocultar Selector de Columnas"
        '
        'sbToText
        '
        Me.sbToText.Location = New System.Drawing.Point(651, 27)
        Me.sbToText.Name = "sbToText"
        Me.sbToText.Size = New System.Drawing.Size(107, 23)
        Me.sbToText.TabIndex = 5
        Me.sbToText.Text = "Exportar a Texto"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(114, 52)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(286, 20)
        Me.leSucursal.TabIndex = 61
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(68, 55)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl7.TabIndex = 62
        Me.LabelControl7.Text = "Sucursal:"
        '
        'con_frmGenerador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(842, 314)
        Me.Controls.Add(Me.gc)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmGenerador"
        Me.Text = "Generador de reportes"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecIni.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecIni.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecFin.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecFin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents sbGenerar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents deFecFin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecIni As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbToPdf As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbToExcel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbMostrarOcultar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbToText As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl

End Class
