﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmGenerarArchivosICV
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.deDesde = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.deHasta = New DevExpress.XtraEditors.DateEdit()
        Me.btGenerar = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit9 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit10 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit11 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit12 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit13 = New DevExpress.XtraEditors.TextEdit()
        CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(37, 13)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl1.TabIndex = 4
        Me.LabelControl1.Text = "Desde Fecha:"
        '
        'deDesde
        '
        Me.deDesde.EditValue = Nothing
        Me.deDesde.EnterMoveNextControl = True
        Me.deDesde.Location = New System.Drawing.Point(106, 11)
        Me.deDesde.Name = "deDesde"
        Me.deDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deDesde.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deDesde.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deDesde.Size = New System.Drawing.Size(101, 20)
        Me.deDesde.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(39, 40)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Hasta Fecha:"
        '
        'deHasta
        '
        Me.deHasta.EditValue = Nothing
        Me.deHasta.EnterMoveNextControl = True
        Me.deHasta.Location = New System.Drawing.Point(106, 37)
        Me.deHasta.Name = "deHasta"
        Me.deHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deHasta.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deHasta.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deHasta.Size = New System.Drawing.Size(101, 20)
        Me.deHasta.TabIndex = 3
        '
        'btGenerar
        '
        Me.btGenerar.Location = New System.Drawing.Point(429, 113)
        Me.btGenerar.Name = "btGenerar"
        Me.btGenerar.Size = New System.Drawing.Size(121, 23)
        Me.btGenerar.TabIndex = 4
        Me.btGenerar.Text = "Proceder a Generar"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(98, 94)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(222, 16)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "ARCHIVOS QUE SE VAN A GENERAR"
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = "PROVEEDORES INSCRITOS EN IVA (SUMARIO) --> ICV-01"
        Me.TextEdit1.Location = New System.Drawing.Point(106, 110)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.ReadOnly = True
        Me.TextEdit1.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit1.TabIndex = 5
        '
        'TextEdit2
        '
        Me.TextEdit2.EditValue = "PROVEEDORES EXTRANJEROS (SUMARIO) --> ICV-03"
        Me.TextEdit2.Location = New System.Drawing.Point(106, 157)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.ReadOnly = True
        Me.TextEdit2.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit2.TabIndex = 5
        '
        'TextEdit3
        '
        Me.TextEdit3.EditValue = "PROVEEDORES EXCLUIDOS EN IVA (SUMARIO) --> ICV-05"
        Me.TextEdit3.Location = New System.Drawing.Point(106, 202)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.ReadOnly = True
        Me.TextEdit3.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit3.TabIndex = 5
        '
        'TextEdit4
        '
        Me.TextEdit4.EditValue = "CLIENTES (SUMARIO) --> ICV-07"
        Me.TextEdit4.Location = New System.Drawing.Point(106, 251)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Properties.ReadOnly = True
        Me.TextEdit4.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit4.TabIndex = 5
        '
        'TextEdit5
        '
        Me.TextEdit5.EditValue = "CLIENTES (FACTURAS < $200 DETALLE)  --> ICV-09"
        Me.TextEdit5.Location = New System.Drawing.Point(106, 299)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Properties.ReadOnly = True
        Me.TextEdit5.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit5.TabIndex = 5
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(106, 62)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(266, 20)
        Me.leSucursal.TabIndex = 57
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(60, 65)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl7.TabIndex = 58
        Me.LabelControl7.Text = "Sucursal:"
        '
        'TextEdit6
        '
        Me.TextEdit6.EditValue = "DEUDORES (SUMARIO) --> ICV-12"
        Me.TextEdit6.Location = New System.Drawing.Point(106, 372)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Properties.ReadOnly = True
        Me.TextEdit6.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit6.TabIndex = 59
        '
        'TextEdit7
        '
        Me.TextEdit7.EditValue = "ACREEDORES (SUMARIO) --> ICV-10"
        Me.TextEdit7.Location = New System.Drawing.Point(106, 324)
        Me.TextEdit7.Name = "TextEdit7"
        Me.TextEdit7.Properties.ReadOnly = True
        Me.TextEdit7.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit7.TabIndex = 60
        '
        'TextEdit8
        '
        Me.TextEdit8.EditValue = "PROVEEDORES INSCRITOS EN IVA (DETALLE) --> ICV-02"
        Me.TextEdit8.Location = New System.Drawing.Point(106, 131)
        Me.TextEdit8.Name = "TextEdit8"
        Me.TextEdit8.Properties.ReadOnly = True
        Me.TextEdit8.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit8.TabIndex = 61
        '
        'TextEdit9
        '
        Me.TextEdit9.EditValue = "PROVEEDORES EXTRANJEROS (DETALLE) --> ICV-04"
        Me.TextEdit9.Location = New System.Drawing.Point(106, 177)
        Me.TextEdit9.Name = "TextEdit9"
        Me.TextEdit9.Properties.ReadOnly = True
        Me.TextEdit9.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit9.TabIndex = 62
        '
        'TextEdit10
        '
        Me.TextEdit10.EditValue = "PROVEEDORES EXCLUIDOS EN IVA (DETALLE) --> ICV-06"
        Me.TextEdit10.Location = New System.Drawing.Point(106, 223)
        Me.TextEdit10.Name = "TextEdit10"
        Me.TextEdit10.Properties.ReadOnly = True
        Me.TextEdit10.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit10.TabIndex = 63
        '
        'TextEdit11
        '
        Me.TextEdit11.EditValue = "CLIENTES (DETALLE) --> ICV-08"
        Me.TextEdit11.Location = New System.Drawing.Point(106, 272)
        Me.TextEdit11.Name = "TextEdit11"
        Me.TextEdit11.Properties.ReadOnly = True
        Me.TextEdit11.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit11.TabIndex = 64
        '
        'TextEdit12
        '
        Me.TextEdit12.EditValue = "ACREEDORES (DETALLE) --> ICV-11"
        Me.TextEdit12.Location = New System.Drawing.Point(106, 344)
        Me.TextEdit12.Name = "TextEdit12"
        Me.TextEdit12.Properties.ReadOnly = True
        Me.TextEdit12.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit12.TabIndex = 65
        '
        'TextEdit13
        '
        Me.TextEdit13.EditValue = "DEUDORES (DETALLE) --> ICV-13"
        Me.TextEdit13.Location = New System.Drawing.Point(106, 393)
        Me.TextEdit13.Name = "TextEdit13"
        Me.TextEdit13.Properties.ReadOnly = True
        Me.TextEdit13.Size = New System.Drawing.Size(297, 20)
        Me.TextEdit13.TabIndex = 66
        '
        'con_frmGenerarArchivosICV
        '
        Me.ClientSize = New System.Drawing.Size(738, 467)
        Me.Controls.Add(Me.TextEdit13)
        Me.Controls.Add(Me.TextEdit12)
        Me.Controls.Add(Me.TextEdit11)
        Me.Controls.Add(Me.TextEdit10)
        Me.Controls.Add(Me.TextEdit9)
        Me.Controls.Add(Me.TextEdit8)
        Me.Controls.Add(Me.TextEdit6)
        Me.Controls.Add(Me.TextEdit7)
        Me.Controls.Add(Me.leSucursal)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.TextEdit5)
        Me.Controls.Add(Me.TextEdit4)
        Me.Controls.Add(Me.TextEdit3)
        Me.Controls.Add(Me.TextEdit2)
        Me.Controls.Add(Me.TextEdit1)
        Me.Controls.Add(Me.btGenerar)
        Me.Controls.Add(Me.deHasta)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.deDesde)
        Me.Controls.Add(Me.LabelControl1)
        Me.Name = "con_frmGenerarArchivosICV"
        Me.Text = "Generar Archivos ICV (Formulario 987 v2)"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.deDesde, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        Me.Controls.SetChildIndex(Me.LabelControl3, 0)
        Me.Controls.SetChildIndex(Me.deHasta, 0)
        Me.Controls.SetChildIndex(Me.btGenerar, 0)
        Me.Controls.SetChildIndex(Me.TextEdit1, 0)
        Me.Controls.SetChildIndex(Me.TextEdit2, 0)
        Me.Controls.SetChildIndex(Me.TextEdit3, 0)
        Me.Controls.SetChildIndex(Me.TextEdit4, 0)
        Me.Controls.SetChildIndex(Me.TextEdit5, 0)
        Me.Controls.SetChildIndex(Me.LabelControl7, 0)
        Me.Controls.SetChildIndex(Me.leSucursal, 0)
        Me.Controls.SetChildIndex(Me.TextEdit7, 0)
        Me.Controls.SetChildIndex(Me.TextEdit6, 0)
        Me.Controls.SetChildIndex(Me.TextEdit8, 0)
        Me.Controls.SetChildIndex(Me.TextEdit9, 0)
        Me.Controls.SetChildIndex(Me.TextEdit10, 0)
        Me.Controls.SetChildIndex(Me.TextEdit11, 0)
        Me.Controls.SetChildIndex(Me.TextEdit12, 0)
        Me.Controls.SetChildIndex(Me.TextEdit13, 0)
        CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents btGenerar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit12 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit13 As DevExpress.XtraEditors.TextEdit
End Class
