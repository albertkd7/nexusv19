﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmExcluirCuentas
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
        Me.BeCtaContable2 = New Nexus.beCtaContable
        Me.BeCtaContable1 = New Nexus.beCtaContable
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.leUsuarios = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcIdCuenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcMotivo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcExcluir = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.leUsuarios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.BeCtaContable2)
        Me.GroupControl1.Controls.Add(Me.BeCtaContable1)
        Me.GroupControl1.Controls.Add(Me.SimpleButton3)
        Me.GroupControl1.Controls.Add(Me.SimpleButton2)
        Me.GroupControl1.Controls.Add(Me.SimpleButton1)
        Me.GroupControl1.Controls.Add(Me.leUsuarios)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(828, 131)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Excluir cuentas contables en consultas de auxiliares y movimientos de cuenta"
        '
        'BeCtaContable2
        '
        Me.BeCtaContable2.Location = New System.Drawing.Point(113, 82)
        Me.BeCtaContable2.Name = "BeCtaContable2"
        Me.BeCtaContable2.Size = New System.Drawing.Size(594, 20)
        Me.BeCtaContable2.TabIndex = 3
        Me.BeCtaContable2.ValidarMayor = False
        '
        'BeCtaContable1
        '
        Me.BeCtaContable1.Location = New System.Drawing.Point(113, 57)
        Me.BeCtaContable1.Name = "BeCtaContable1"
        Me.BeCtaContable1.Size = New System.Drawing.Size(594, 20)
        Me.BeCtaContable1.TabIndex = 2
        Me.BeCtaContable1.ValidarMayor = False
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Location = New System.Drawing.Point(546, 105)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(104, 23)
        Me.SimpleButton3.TabIndex = 5
        Me.SimpleButton3.Text = "Guardar Cambios"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(116, 105)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(104, 23)
        Me.SimpleButton2.TabIndex = 4
        Me.SimpleButton2.Text = "Obtener Cuentas"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(383, 28)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(138, 23)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Ver Cuentas Excluidas"
        '
        'leUsuarios
        '
        Me.leUsuarios.Location = New System.Drawing.Point(116, 30)
        Me.leUsuarios.Name = "leUsuarios"
        Me.leUsuarios.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leUsuarios.Size = New System.Drawing.Size(248, 20)
        Me.leUsuarios.TabIndex = 0
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(41, 86)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl3.TabIndex = 6
        Me.LabelControl3.Text = "Desde Cuenta:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(41, 60)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl2.TabIndex = 6
        Me.LabelControl2.Text = "Desde Cuenta:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(73, 33)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl1.TabIndex = 6
        Me.LabelControl1.Text = "Usuario:"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 131)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.gc.Size = New System.Drawing.Size(831, 150)
        Me.gc.TabIndex = 5
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv, Me.GridView2})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdCuenta, Me.gcNombre, Me.gcMotivo, Me.gcExcluir})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdCuenta
        '
        Me.gcIdCuenta.Caption = "Id. de Cuenta"
        Me.gcIdCuenta.FieldName = "IdCuenta"
        Me.gcIdCuenta.Name = "gcIdCuenta"
        Me.gcIdCuenta.OptionsColumn.AllowEdit = False
        Me.gcIdCuenta.Visible = True
        Me.gcIdCuenta.VisibleIndex = 0
        Me.gcIdCuenta.Width = 119
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.OptionsColumn.AllowEdit = False
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 1
        Me.gcNombre.Width = 425
        '
        'gcMotivo
        '
        Me.gcMotivo.Caption = "Motivo de la exclusión"
        Me.gcMotivo.FieldName = "Motivo"
        Me.gcMotivo.Name = "gcMotivo"
        Me.gcMotivo.Visible = True
        Me.gcMotivo.VisibleIndex = 2
        Me.gcMotivo.Width = 277
        '
        'gcExcluir
        '
        Me.gcExcluir.Caption = "Excluir?"
        Me.gcExcluir.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.gcExcluir.FieldName = "Excluir"
        Me.gcExcluir.Name = "gcExcluir"
        Me.gcExcluir.Visible = True
        Me.gcExcluir.VisibleIndex = 3
        Me.gcExcluir.Width = 51
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.gc
        Me.GridView2.Name = "GridView2"
        '
        'con_frmExcluirCuentas
        '
        Me.ClientSize = New System.Drawing.Size(828, 306)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.GroupControl1)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmExcluirCuentas"
        Me.OptionId = "001007"
        Me.Text = "Excluir Cuentas"
        Me.TipoFormulario = 2
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.leUsuarios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcExcluir As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents leUsuarios As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BeCtaContable2 As Nexus.beCtaContable
    Friend WithEvents BeCtaContable1 As Nexus.beCtaContable
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcMotivo As DevExpress.XtraGrid.Columns.GridColumn

End Class
