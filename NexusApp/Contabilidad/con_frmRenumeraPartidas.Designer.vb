<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmRenumeraPartidas
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(con_frmRenumeraPartidas))
        Me.gpr = New DevExpress.XtraEditors.GroupControl
        Me.leTipoPartida = New DevExpress.XtraEditors.LookUpEdit
        Me.seEjercicio = New DevExpress.XtraEditors.SpinEdit
        Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit
        Me.btnOk = New DevExpress.XtraEditors.SimpleButton
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        CType(Me.gpr, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpr.SuspendLayout()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gpr
        '
        Me.gpr.Controls.Add(Me.leTipoPartida)
        Me.gpr.Controls.Add(Me.seEjercicio)
        Me.gpr.Controls.Add(Me.meMes)
        Me.gpr.Controls.Add(Me.btnOk)
        Me.gpr.Controls.Add(Me.LabelControl2)
        Me.gpr.Controls.Add(Me.LabelControl1)
        Me.gpr.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gpr.Location = New System.Drawing.Point(0, 0)
        Me.gpr.Name = "gpr"
        Me.gpr.Size = New System.Drawing.Size(614, 351)
        Me.gpr.TabIndex = 6
        Me.gpr.Text = "Renumeración"
        '
        'leTipoPartida
        '
        Me.leTipoPartida.Location = New System.Drawing.Point(177, 92)
        Me.leTipoPartida.Name = "leTipoPartida"
        Me.leTipoPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPartida.Size = New System.Drawing.Size(253, 20)
        Me.leTipoPartida.TabIndex = 2
        '
        'seEjercicio
        '
        Me.seEjercicio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seEjercicio.Location = New System.Drawing.Point(282, 52)
        Me.seEjercicio.Name = "seEjercicio"
        Me.seEjercicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seEjercicio.Properties.Mask.EditMask = "n0"
        Me.seEjercicio.Size = New System.Drawing.Size(66, 20)
        Me.seEjercicio.TabIndex = 1
        '
        'meMes
        '
        Me.meMes.Location = New System.Drawing.Point(177, 52)
        Me.meMes.Name = "meMes"
        Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes.Size = New System.Drawing.Size(100, 20)
        Me.meMes.TabIndex = 0
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(177, 151)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(150, 31)
        Me.btnOk.TabIndex = 3
        Me.btnOk.Text = "&Proceder a renumerar..."
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(28, 94)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(142, 13)
        Me.LabelControl2.TabIndex = 8
        Me.LabelControl2.Text = "Tipo de partida a re-numerar:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(37, 55)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(133, 13)
        Me.LabelControl1.TabIndex = 7
        Me.LabelControl1.Text = "Mes que desea re-numerar:"
        '
        'con_frmRenumeraPartidas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(614, 376)
        Me.Controls.Add(Me.gpr)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmRenumeraPartidas"
        Me.OptionId = "002003"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Re-Numeración de partidas"
        Me.Controls.SetChildIndex(Me.gpr, 0)
        CType(Me.gpr, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpr.ResumeLayout(False)
        Me.gpr.PerformLayout()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gpr As DevExpress.XtraEditors.GroupControl
    Friend WithEvents leTipoPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents seEjercicio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents btnOk As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
End Class
