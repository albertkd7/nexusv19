﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pre_frmBusquedaCuenta
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gcCuentasContables = New DevExpress.XtraGrid.GridControl
        Me.gvCuentasContables = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcCodigo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcCuenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.teBusquedaCuenta = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.sbBuscarCuenta = New DevExpress.XtraEditors.SimpleButton
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.sbAgregar = New DevExpress.XtraEditors.SimpleButton
        CType(Me.gcCuentasContables, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvCuentasContables, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teBusquedaCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcCuentasContables
        '
        Me.gcCuentasContables.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gcCuentasContables.Location = New System.Drawing.Point(0, 60)
        Me.gcCuentasContables.MainView = Me.gvCuentasContables
        Me.gcCuentasContables.Name = "gcCuentasContables"
        Me.gcCuentasContables.Size = New System.Drawing.Size(470, 221)
        Me.gcCuentasContables.TabIndex = 11
        Me.gcCuentasContables.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvCuentasContables})
        '
        'gvCuentasContables
        '
        Me.gvCuentasContables.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcCodigo, Me.gcCuenta})
        Me.gvCuentasContables.GridControl = Me.gcCuentasContables
        Me.gvCuentasContables.Name = "gvCuentasContables"
        Me.gvCuentasContables.OptionsBehavior.Editable = False
        Me.gvCuentasContables.OptionsSelection.MultiSelect = True
        Me.gvCuentasContables.OptionsView.ShowGroupPanel = False
        '
        'gcCodigo
        '
        Me.gcCodigo.Caption = "Codigo"
        Me.gcCodigo.FieldName = "IdCuenta"
        Me.gcCodigo.Name = "gcCodigo"
        Me.gcCodigo.Visible = True
        Me.gcCodigo.VisibleIndex = 0
        '
        'gcCuenta
        '
        Me.gcCuenta.Caption = "Cuenta contable"
        Me.gcCuenta.FieldName = "Nombre"
        Me.gcCuenta.Name = "gcCuenta"
        Me.gcCuenta.Visible = True
        Me.gcCuenta.VisibleIndex = 1
        '
        'teBusquedaCuenta
        '
        Me.teBusquedaCuenta.Location = New System.Drawing.Point(92, 12)
        Me.teBusquedaCuenta.Name = "teBusquedaCuenta"
        Me.teBusquedaCuenta.Size = New System.Drawing.Size(152, 20)
        Me.teBusquedaCuenta.TabIndex = 12
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(12, 15)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl1.TabIndex = 13
        Me.LabelControl1.Text = "Codigo cuenta:"
        '
        'sbBuscarCuenta
        '
        Me.sbBuscarCuenta.Location = New System.Drawing.Point(265, 9)
        Me.sbBuscarCuenta.Name = "sbBuscarCuenta"
        Me.sbBuscarCuenta.Size = New System.Drawing.Size(75, 23)
        Me.sbBuscarCuenta.TabIndex = 14
        Me.sbBuscarCuenta.Text = "Buscar"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(12, 41)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(423, 13)
        Me.LabelControl2.TabIndex = 15
        Me.LabelControl2.Text = "Nota: Has doble click sobre la cuenta que deseas agregar o has clic en el botón a" & _
            "gregar."
        '
        'sbAgregar
        '
        Me.sbAgregar.Location = New System.Drawing.Point(372, 9)
        Me.sbAgregar.Name = "sbAgregar"
        Me.sbAgregar.Size = New System.Drawing.Size(75, 23)
        Me.sbAgregar.TabIndex = 16
        Me.sbAgregar.Text = "Agregar"
        '
        'pre_frmBusquedaCuenta
        '
        Me.ClientSize = New System.Drawing.Size(470, 306)
        Me.Controls.Add(Me.sbAgregar)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.sbBuscarCuenta)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.teBusquedaCuenta)
        Me.Controls.Add(Me.gcCuentasContables)
        Me.Name = "pre_frmBusquedaCuenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Cuentas contables"
        Me.Controls.SetChildIndex(Me.gcCuentasContables, 0)
        Me.Controls.SetChildIndex(Me.teBusquedaCuenta, 0)
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.sbBuscarCuenta, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        Me.Controls.SetChildIndex(Me.sbAgregar, 0)
        CType(Me.gcCuentasContables, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvCuentasContables, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teBusquedaCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gcCuentasContables As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvCuentasContables As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcCodigo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teBusquedaCuenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbBuscarCuenta As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbAgregar As DevExpress.XtraEditors.SimpleButton

End Class
