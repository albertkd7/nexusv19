﻿Imports NexusELL.TableEntities

Public Class pre_frmBusquedaCuenta

    Private IdPresupuesto As Integer

    Public Sub New(ByVal pIdPresupuesto As Integer)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        IdPresupuesto = pIdPresupuesto
    End Sub

    Private _ListaCuentas As New List(Of con_Cuentas)

    Property ListaCuentas() As List(Of con_Cuentas)
        Get
            Return _ListaCuentas
        End Get
        Set(ByVal value As List(Of con_Cuentas))
            _ListaCuentas = value
        End Set
    End Property

    Public Sub CargarGridCuentas(ByVal pIdCuenta As String)
        gcCuentasContables.DataSource = objTablas.con_CuentasSearchByPk(pIdCuenta, IdPresupuesto)
        gvCuentasContables.BestFitColumns()
    End Sub

    Private Sub sbBuscarCuenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbBuscarCuenta.Click
        CargarGridCuentas(teBusquedaCuenta.Text.Trim())
    End Sub

    Private Sub gcCuentasContables_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gcCuentasContables.DoubleClick

        Dim temp_Cuenta As New con_Cuentas

        With temp_Cuenta
            .IdCuenta = gvCuentasContables.GetFocusedRowCellDisplayText("IdCuenta")
            .Nombre = gvCuentasContables.GetFocusedRowCellDisplayText("Nombre")
        End With

        _ListaCuentas.Add(temp_Cuenta)

        DialogResult = DialogResult.OK

    End Sub

    Private Sub sbAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbAgregar.Click

        If gvCuentasContables.SelectedRowsCount > 0 Then
            For i As Integer = 0 To gvCuentasContables.RowCount - 1

                If gvCuentasContables.IsRowSelected(i) Then

                    Dim cuenta As New con_Cuentas()

                    With cuenta
                        .IdCuenta = gvCuentasContables.GetRowCellValue(i, "IdCuenta")
                        .Nombre = gvCuentasContables.GetRowCellValue(i, "Nombre")
                    End With

                    _ListaCuentas.Add(cuenta)
                End If

            Next

            DialogResult = DialogResult.OK

        End If

    End Sub

    Private Sub pre_frmBusquedaCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
