﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmRevierteMes
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(con_frmRevierteMes))
        Me.seEjercicio = New DevExpress.XtraEditors.SpinEdit
        Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.btnRevertir = New DevExpress.XtraEditors.SimpleButton
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.seEjercicio)
        Me.GroupControl1.Controls.Add(Me.meMes)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.btnRevertir)
        Me.GroupControl1.Size = New System.Drawing.Size(527, 304)
        Me.GroupControl1.Text = "Revertie el cierre de un mes en particular. El proceso es retroactivo"
        '
        'seEjercicio
        '
        Me.seEjercicio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seEjercicio.Enabled = False
        Me.seEjercicio.Location = New System.Drawing.Point(229, 51)
        Me.seEjercicio.Name = "seEjercicio"
        Me.seEjercicio.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.seEjercicio.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.seEjercicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seEjercicio.Properties.DisplayFormat.FormatString = "n0"
        Me.seEjercicio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seEjercicio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seEjercicio.Properties.Mask.EditMask = "n0"
        Me.seEjercicio.Size = New System.Drawing.Size(73, 20)
        Me.seEjercicio.TabIndex = 29
        '
        'meMes
        '
        Me.meMes.Enabled = False
        Me.meMes.Location = New System.Drawing.Point(131, 51)
        Me.meMes.Name = "meMes"
        Me.meMes.Properties.AllowFocused = False
        Me.meMes.Properties.AppearanceDisabled.BorderColor = System.Drawing.Color.Black
        Me.meMes.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.meMes.Properties.AppearanceDisabled.Options.UseBorderColor = True
        Me.meMes.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes.Size = New System.Drawing.Size(92, 20)
        Me.meMes.TabIndex = 28
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(37, 54)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(88, 13)
        Me.LabelControl1.TabIndex = 30
        Me.LabelControl1.Text = "Período a revertir:"
        '
        'btnRevertir
        '
        Me.btnRevertir.Location = New System.Drawing.Point(131, 107)
        Me.btnRevertir.Name = "btnRevertir"
        Me.btnRevertir.Size = New System.Drawing.Size(114, 33)
        Me.btnRevertir.TabIndex = 27
        Me.btnRevertir.Text = "&Revertir ..."
        '
        'con_frmRevierteMes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(527, 329)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmRevierteMes"
        Me.Text = "Revertir cierre de período contable"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents seEjercicio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnRevertir As DevExpress.XtraEditors.SimpleButton
End Class
