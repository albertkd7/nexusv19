﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmCuentas
    Inherits gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(con_frmCuentas))
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.ImageCollection1 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.SplitContainer = New DevExpress.XtraEditors.SplitContainerControl()
        Me.tlCuentas = New DevExpress.XtraTreeList.TreeList()
        Me.TreeListColumn1 = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.TreeListColumn2 = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.TreeListColumn3 = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.TreeListColumn4 = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.TreeListColumn5 = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.TreeListColumn6 = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.gcCuentas = New DevExpress.XtraEditors.GroupControl()
        Me.lblTipo = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNivelCuenta = New DevExpress.XtraEditors.TextEdit()
        Me.txtIdCuentaPadre = New DevExpress.XtraEditors.TextEdit()
        Me.txtNombreCuenta = New DevExpress.XtraEditors.TextEdit()
        Me.txtIdCuenta = New DevExpress.XtraEditors.TextEdit()
        Me.cboTipo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.sbRevisar = New DevExpress.XtraEditors.SimpleButton()
        Me.sbCrearNormal = New DevExpress.XtraEditors.SimpleButton()
        Me.sbCrearTitular = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageCollection1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer.SuspendLayout()
        CType(Me.tlCuentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcCuentas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcCuentas.SuspendLayout()
        CType(Me.txtNivelCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtIdCuentaPadre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombreCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtIdCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"Uno", "Dosd", "Tres"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'ImageCollection1
        '
        Me.ImageCollection1.ImageStream = CType(resources.GetObject("ImageCollection1.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        '
        'SplitContainer
        '
        Me.SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer.Horizontal = False
        Me.SplitContainer.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer.Name = "SplitContainer"
        Me.SplitContainer.Panel1.Controls.Add(Me.tlCuentas)
        Me.SplitContainer.Panel1.Text = "Panel1"
        Me.SplitContainer.Panel2.Controls.Add(Me.gcCuentas)
        Me.SplitContainer.Panel2.Text = "Panel2"
        Me.SplitContainer.Size = New System.Drawing.Size(950, 490)
        Me.SplitContainer.SplitterPosition = 321
        Me.SplitContainer.TabIndex = 1
        Me.SplitContainer.Text = "SplitContainerControl1"
        '
        'tlCuentas
        '
        Me.tlCuentas.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        Me.tlCuentas.Appearance.Empty.Options.UseBackColor = True
        Me.tlCuentas.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.tlCuentas.Appearance.EvenRow.BackColor2 = System.Drawing.Color.GhostWhite
        Me.tlCuentas.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
        Me.tlCuentas.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal
        Me.tlCuentas.Appearance.EvenRow.Options.UseBackColor = True
        Me.tlCuentas.Appearance.EvenRow.Options.UseForeColor = True
        Me.tlCuentas.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(225, Byte), Integer))
        Me.tlCuentas.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
        Me.tlCuentas.Appearance.FocusedCell.Options.UseBackColor = True
        Me.tlCuentas.Appearance.FocusedCell.Options.UseForeColor = True
        Me.tlCuentas.Appearance.FocusedRow.BackColor = System.Drawing.Color.Navy
        Me.tlCuentas.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(178, Byte), Integer))
        Me.tlCuentas.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
        Me.tlCuentas.Appearance.FocusedRow.Options.UseBackColor = True
        Me.tlCuentas.Appearance.FocusedRow.Options.UseForeColor = True
        Me.tlCuentas.Appearance.FooterPanel.BackColor = System.Drawing.Color.Silver
        Me.tlCuentas.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Silver
        Me.tlCuentas.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
        Me.tlCuentas.Appearance.FooterPanel.Options.UseBackColor = True
        Me.tlCuentas.Appearance.FooterPanel.Options.UseBorderColor = True
        Me.tlCuentas.Appearance.FooterPanel.Options.UseForeColor = True
        Me.tlCuentas.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver
        Me.tlCuentas.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver
        Me.tlCuentas.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black
        Me.tlCuentas.Appearance.GroupButton.Options.UseBackColor = True
        Me.tlCuentas.Appearance.GroupButton.Options.UseBorderColor = True
        Me.tlCuentas.Appearance.GroupButton.Options.UseForeColor = True
        Me.tlCuentas.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(202, Byte), Integer), CType(CType(202, Byte), Integer))
        Me.tlCuentas.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(202, Byte), Integer), CType(CType(202, Byte), Integer), CType(CType(202, Byte), Integer))
        Me.tlCuentas.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
        Me.tlCuentas.Appearance.GroupFooter.Options.UseBackColor = True
        Me.tlCuentas.Appearance.GroupFooter.Options.UseBorderColor = True
        Me.tlCuentas.Appearance.GroupFooter.Options.UseForeColor = True
        Me.tlCuentas.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Silver
        Me.tlCuentas.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Silver
        Me.tlCuentas.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.tlCuentas.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.tlCuentas.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.tlCuentas.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.tlCuentas.Appearance.HeaderPanel.Options.UseFont = True
        Me.tlCuentas.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.tlCuentas.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray
        Me.tlCuentas.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(212, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(200, Byte), Integer))
        Me.tlCuentas.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.tlCuentas.Appearance.HideSelectionRow.Options.UseForeColor = True
        Me.tlCuentas.Appearance.HorzLine.BackColor = System.Drawing.Color.Silver
        Me.tlCuentas.Appearance.HorzLine.Options.UseBackColor = True
        Me.tlCuentas.Appearance.OddRow.BackColor = System.Drawing.Color.White
        Me.tlCuentas.Appearance.OddRow.BackColor2 = System.Drawing.Color.White
        Me.tlCuentas.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
        Me.tlCuentas.Appearance.OddRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal
        Me.tlCuentas.Appearance.OddRow.Options.UseBackColor = True
        Me.tlCuentas.Appearance.OddRow.Options.UseForeColor = True
        Me.tlCuentas.Appearance.Preview.BackColor = System.Drawing.Color.White
        Me.tlCuentas.Appearance.Preview.ForeColor = System.Drawing.Color.Navy
        Me.tlCuentas.Appearance.Preview.Options.UseBackColor = True
        Me.tlCuentas.Appearance.Preview.Options.UseForeColor = True
        Me.tlCuentas.Appearance.Row.BackColor = System.Drawing.Color.White
        Me.tlCuentas.Appearance.Row.ForeColor = System.Drawing.Color.Black
        Me.tlCuentas.Appearance.Row.Options.UseBackColor = True
        Me.tlCuentas.Appearance.Row.Options.UseForeColor = True
        Me.tlCuentas.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(10, Byte), Integer), CType(CType(10, Byte), Integer), CType(CType(138, Byte), Integer))
        Me.tlCuentas.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
        Me.tlCuentas.Appearance.SelectedRow.Options.UseBackColor = True
        Me.tlCuentas.Appearance.SelectedRow.Options.UseForeColor = True
        Me.tlCuentas.Appearance.TreeLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(58, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(58, Byte), Integer))
        Me.tlCuentas.Appearance.TreeLine.Options.UseBackColor = True
        Me.tlCuentas.Appearance.VertLine.BackColor = System.Drawing.Color.Silver
        Me.tlCuentas.Appearance.VertLine.Options.UseBackColor = True
        Me.tlCuentas.Columns.AddRange(New DevExpress.XtraTreeList.Columns.TreeListColumn() {Me.TreeListColumn1, Me.TreeListColumn2, Me.TreeListColumn3, Me.TreeListColumn4, Me.TreeListColumn5, Me.TreeListColumn6})
        Me.tlCuentas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlCuentas.ImageIndexFieldName = "Nivel"
        Me.tlCuentas.Location = New System.Drawing.Point(0, 0)
        Me.tlCuentas.LookAndFeel.SkinName = "Money Twins"
        Me.tlCuentas.LookAndFeel.UseDefaultLookAndFeel = False
        Me.tlCuentas.LookAndFeel.UseWindowsXPTheme = True
        Me.tlCuentas.Name = "tlCuentas"
        Me.tlCuentas.OptionsBehavior.AutoPopulateColumns = False
        Me.tlCuentas.OptionsBehavior.CloseEditorOnLostFocus = False
        Me.tlCuentas.OptionsBehavior.Editable = False
        Me.tlCuentas.OptionsBehavior.PopulateServiceColumns = True
        Me.tlCuentas.OptionsView.EnableAppearanceEvenRow = True
        Me.tlCuentas.OptionsView.EnableAppearanceOddRow = True
        Me.tlCuentas.PreviewFieldName = "IdCuenta"
        Me.tlCuentas.SelectImageList = Me.ImageCollection1
        Me.tlCuentas.Size = New System.Drawing.Size(950, 321)
        Me.tlCuentas.TabIndex = 0
        '
        'TreeListColumn1
        '
        Me.TreeListColumn1.Caption = "Cuenta"
        Me.TreeListColumn1.FieldName = "IdCuenta"
        Me.TreeListColumn1.MinWidth = 34
        Me.TreeListColumn1.Name = "TreeListColumn1"
        Me.TreeListColumn1.Visible = True
        Me.TreeListColumn1.VisibleIndex = 0
        Me.TreeListColumn1.Width = 135
        '
        'TreeListColumn2
        '
        Me.TreeListColumn2.Caption = "Nombre"
        Me.TreeListColumn2.FieldName = "Nombre"
        Me.TreeListColumn2.Name = "TreeListColumn2"
        Me.TreeListColumn2.Visible = True
        Me.TreeListColumn2.VisibleIndex = 1
        Me.TreeListColumn2.Width = 349
        '
        'TreeListColumn3
        '
        Me.TreeListColumn3.Caption = "Tipo de Cuenta"
        Me.TreeListColumn3.FieldName = "Tipo"
        Me.TreeListColumn3.Name = "TreeListColumn3"
        Me.TreeListColumn3.Visible = True
        Me.TreeListColumn3.VisibleIndex = 2
        Me.TreeListColumn3.Width = 144
        '
        'TreeListColumn4
        '
        Me.TreeListColumn4.Caption = "Nivel"
        Me.TreeListColumn4.FieldName = "Nivel"
        Me.TreeListColumn4.Name = "TreeListColumn4"
        Me.TreeListColumn4.Visible = True
        Me.TreeListColumn4.VisibleIndex = 3
        Me.TreeListColumn4.Width = 51
        '
        'TreeListColumn5
        '
        Me.TreeListColumn5.Caption = "Naturaleza"
        Me.TreeListColumn5.FieldName = "Naturaleza"
        Me.TreeListColumn5.Name = "TreeListColumn5"
        Me.TreeListColumn5.Visible = True
        Me.TreeListColumn5.VisibleIndex = 4
        Me.TreeListColumn5.Width = 112
        '
        'TreeListColumn6
        '
        Me.TreeListColumn6.Caption = "Transaccional?"
        Me.TreeListColumn6.FieldName = "EsTransaccional"
        Me.TreeListColumn6.Name = "TreeListColumn6"
        Me.TreeListColumn6.Visible = True
        Me.TreeListColumn6.VisibleIndex = 5
        Me.TreeListColumn6.Width = 138
        '
        'gcCuentas
        '
        Me.gcCuentas.Controls.Add(Me.lblTipo)
        Me.gcCuentas.Controls.Add(Me.LabelControl3)
        Me.gcCuentas.Controls.Add(Me.LabelControl2)
        Me.gcCuentas.Controls.Add(Me.LabelControl1)
        Me.gcCuentas.Controls.Add(Me.txtNivelCuenta)
        Me.gcCuentas.Controls.Add(Me.txtIdCuentaPadre)
        Me.gcCuentas.Controls.Add(Me.txtNombreCuenta)
        Me.gcCuentas.Controls.Add(Me.txtIdCuenta)
        Me.gcCuentas.Controls.Add(Me.cboTipo)
        Me.gcCuentas.Controls.Add(Me.sbGuardar)
        Me.gcCuentas.Controls.Add(Me.sbRevisar)
        Me.gcCuentas.Controls.Add(Me.sbCrearNormal)
        Me.gcCuentas.Controls.Add(Me.sbCrearTitular)
        Me.gcCuentas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcCuentas.Location = New System.Drawing.Point(0, 0)
        Me.gcCuentas.Name = "gcCuentas"
        Me.gcCuentas.Size = New System.Drawing.Size(950, 164)
        Me.gcCuentas.TabIndex = 0
        Me.gcCuentas.Text = "Mantenimiento de cuentas"
        Me.gcCuentas.Visible = False
        '
        'lblTipo
        '
        Me.lblTipo.Location = New System.Drawing.Point(18, 97)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(77, 13)
        Me.lblTipo.TabIndex = 11
        Me.lblTipo.Text = "Tipo de Cuenta:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(68, 76)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl3.TabIndex = 11
        Me.LabelControl3.Text = "Nivel:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(56, 55)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl2.TabIndex = 11
        Me.LabelControl2.Text = "Cuenta:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(25, 34)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(70, 13)
        Me.LabelControl1.TabIndex = 11
        Me.LabelControl1.Text = "Cuenta Padre:"
        '
        'txtNivelCuenta
        '
        Me.txtNivelCuenta.EnterMoveNextControl = True
        Me.txtNivelCuenta.Location = New System.Drawing.Point(98, 73)
        Me.txtNivelCuenta.Name = "txtNivelCuenta"
        Me.txtNivelCuenta.Properties.ReadOnly = True
        Me.txtNivelCuenta.Size = New System.Drawing.Size(30, 20)
        Me.txtNivelCuenta.TabIndex = 3
        '
        'txtIdCuentaPadre
        '
        Me.txtIdCuentaPadre.EnterMoveNextControl = True
        Me.txtIdCuentaPadre.Location = New System.Drawing.Point(98, 31)
        Me.txtIdCuentaPadre.Name = "txtIdCuentaPadre"
        Me.txtIdCuentaPadre.Properties.ReadOnly = True
        Me.txtIdCuentaPadre.Size = New System.Drawing.Size(625, 20)
        Me.txtIdCuentaPadre.TabIndex = 0
        '
        'txtNombreCuenta
        '
        Me.txtNombreCuenta.EnterMoveNextControl = True
        Me.txtNombreCuenta.Location = New System.Drawing.Point(289, 52)
        Me.txtNombreCuenta.Name = "txtNombreCuenta"
        Me.txtNombreCuenta.Size = New System.Drawing.Size(434, 20)
        Me.txtNombreCuenta.TabIndex = 2
        '
        'txtIdCuenta
        '
        Me.txtIdCuenta.EnterMoveNextControl = True
        Me.txtIdCuenta.Location = New System.Drawing.Point(98, 52)
        Me.txtIdCuenta.Name = "txtIdCuenta"
        Me.txtIdCuenta.Size = New System.Drawing.Size(187, 20)
        Me.txtIdCuenta.TabIndex = 1
        '
        'cboTipo
        '
        Me.cboTipo.EditValue = "Título/Elemento"
        Me.cboTipo.EnterMoveNextControl = True
        Me.cboTipo.Location = New System.Drawing.Point(98, 94)
        Me.cboTipo.Name = "cboTipo"
        Me.cboTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipo.Properties.Items.AddRange(New Object() {"Título/Elemento", "Rubro", "Mayor", "Sub-Cuenta"})
        Me.cboTipo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cboTipo.Size = New System.Drawing.Size(187, 20)
        Me.cboTipo.TabIndex = 4
        '
        'sbGuardar
        '
        Me.sbGuardar.Location = New System.Drawing.Point(475, 126)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(124, 23)
        Me.sbGuardar.TabIndex = 7
        Me.sbGuardar.Text = "&Guardar Cuenta"
        '
        'sbRevisar
        '
        Me.sbRevisar.Location = New System.Drawing.Point(605, 126)
        Me.sbRevisar.Name = "sbRevisar"
        Me.sbRevisar.Size = New System.Drawing.Size(124, 23)
        Me.sbRevisar.TabIndex = 8
        Me.sbRevisar.Text = "&Revisar integridad..."
        '
        'sbCrearNormal
        '
        Me.sbCrearNormal.Location = New System.Drawing.Point(273, 126)
        Me.sbCrearNormal.Name = "sbCrearNormal"
        Me.sbCrearNormal.Size = New System.Drawing.Size(173, 23)
        Me.sbCrearNormal.TabIndex = 6
        Me.sbCrearNormal.Text = "&Crear cuenta en nivel actual"
        '
        'sbCrearTitular
        '
        Me.sbCrearTitular.Location = New System.Drawing.Point(98, 126)
        Me.sbCrearTitular.Name = "sbCrearTitular"
        Me.sbCrearTitular.Size = New System.Drawing.Size(173, 23)
        Me.sbCrearTitular.TabIndex = 5
        Me.sbCrearTitular.Text = "&Crear cuenta de Tipo Titular"
        '
        'con_frmCuentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(950, 515)
        Me.Controls.Add(Me.SplitContainer)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmCuentas"
        Me.OptionId = "001001"
        Me.Text = "Cuentas"
        Me.TipoFormulario = 2
        Me.Controls.SetChildIndex(Me.SplitContainer, 0)
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageCollection1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer.ResumeLayout(False)
        CType(Me.tlCuentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcCuentas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcCuentas.ResumeLayout(False)
        Me.gcCuentas.PerformLayout()
        CType(Me.txtNivelCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtIdCuentaPadre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombreCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtIdCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ImageCollection1 As DevExpress.Utils.ImageCollection
    Friend WithEvents SplitContainer As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents gcCuentas As DevExpress.XtraEditors.GroupControl
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents tlCuentas As DevExpress.XtraTreeList.TreeList
    Friend WithEvents TreeListColumn1 As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents TreeListColumn2 As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents TreeListColumn3 As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents TreeListColumn4 As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents TreeListColumn5 As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents sbCrearTitular As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbCrearNormal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbRevisar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TreeListColumn6 As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents cboTipo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txtNombreCuenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtIdCuenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtIdCuentaPadre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTipo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNivelCuenta As DevExpress.XtraEditors.TextEdit
End Class
