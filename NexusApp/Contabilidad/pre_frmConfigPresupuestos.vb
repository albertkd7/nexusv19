﻿Imports DevExpress.Charts.Native
Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraEditors
Imports System.Data.SqlClient

Public Class pre_frmConfigPresupuestos

    Private IdPresupuesto As Integer
    Private ListaCuentas As New List(Of con_Cuentas)
    Private DetallesPresupuestoCuenta As New List(Of pre_DetallesPresupuesto)

#Region "Contructores"
    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        DbMode = DbModeType.insert

    End Sub

    Public Sub New(ByVal pIdPresupuesto As Integer)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        DbMode = DbModeType.update
        IdPresupuesto = pIdPresupuesto

    End Sub
#End Region

    Private Sub CargarGridCuentas()
        gcCuentasContables.DataSource = Nothing
        gcCuentasContables.DataSource = objTablas.pre_PresupuestoCuentaSelectByPresupuesto(IdPresupuesto)
        gvCuentasContables.RefreshData()
    End Sub

    Private Sub CargarDatosDetallesPresupuesto(ByVal idPreCuenta As Integer)

        beEnero.Text = objTablas.pre_ObtenerTotalPresupuestoMes(beEnero.Tag, idPreCuenta)
        beFebrero.Text = objTablas.pre_ObtenerTotalPresupuestoMes(beFebrero.Tag, idPreCuenta)
        beMarzo.Text = objTablas.pre_ObtenerTotalPresupuestoMes(beMarzo.Tag, idPreCuenta)
        beAbril.Text = objTablas.pre_ObtenerTotalPresupuestoMes(beAbril.Tag, idPreCuenta)
        beMayo.Text = objTablas.pre_ObtenerTotalPresupuestoMes(beMayo.Tag, idPreCuenta)
        beJunio.Text = objTablas.pre_ObtenerTotalPresupuestoMes(beJunio.Tag, idPreCuenta)
        beJulio.Text = objTablas.pre_ObtenerTotalPresupuestoMes(beJulio.Tag, idPreCuenta)
        beAgosto.Text = objTablas.pre_ObtenerTotalPresupuestoMes(beAgosto.Tag, idPreCuenta)
        beSeptiembre.Text = objTablas.pre_ObtenerTotalPresupuestoMes(beSeptiembre.Tag, idPreCuenta)
        beOctubre.Text = objTablas.pre_ObtenerTotalPresupuestoMes(beOctubre.Tag, idPreCuenta)
        beNoviembre.Text = objTablas.pre_ObtenerTotalPresupuestoMes(beNoviembre.Tag, idPreCuenta)
        beDiciembre.Text = objTablas.pre_ObtenerTotalPresupuestoMes(beDiciembre.Tag, idPreCuenta)

        lblEditEnero.Text = (objTablas.pre_ObtenerNumeroEdicionesMes(lblEditEnero.Tag, idPreCuenta)).ToString()
        lblEditFebrero.Text = (objTablas.pre_ObtenerNumeroEdicionesMes(lblEditFebrero.Tag, idPreCuenta)).ToString()
        lblEditMarzo.Text = (objTablas.pre_ObtenerNumeroEdicionesMes(lblEditMarzo.Tag, idPreCuenta)).ToString()
        lblEditAbril.Text = (objTablas.pre_ObtenerNumeroEdicionesMes(lblEditAbril.Tag, idPreCuenta)).ToString()
        lblEditMayo.Text = (objTablas.pre_ObtenerNumeroEdicionesMes(lblEditMayo.Tag, idPreCuenta)).ToString()
        lblEditJunio.Text = (objTablas.pre_ObtenerNumeroEdicionesMes(lblEditJunio.Tag, idPreCuenta)).ToString()
        lblEditJulio.Text = (objTablas.pre_ObtenerNumeroEdicionesMes(lblEditJulio.Tag, idPreCuenta)).ToString()
        lblEditAgosto.Text = (objTablas.pre_ObtenerNumeroEdicionesMes(lblEditAgosto.Tag, idPreCuenta)).ToString()
        lblEditSeptiembre.Text = (objTablas.pre_ObtenerNumeroEdicionesMes(lblEditSeptiembre.Tag, idPreCuenta)).ToString()
        lblEditOctubre.Text = (objTablas.pre_ObtenerNumeroEdicionesMes(lblEditOctubre.Tag, idPreCuenta)).ToString()
        lblEditNoviembre.Text = (objTablas.pre_ObtenerNumeroEdicionesMes(lblEditNoviembre.Tag, idPreCuenta)).ToString()
        lblEditDiciembre.Text = (objTablas.pre_ObtenerNumeroEdicionesMes(lblEditDiciembre.Tag, idPreCuenta)).ToString()

        teTotal.Text = (objTablas.pre_ObtenerTotalPresupuesto(idPreCuenta))
    End Sub

    Private Sub pre_frmConfigPresupuestos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objCombos.adm_Sucursales(lueSucursales, objMenu.User, "")
        objCombos.con_CentrosCosto(lueCentroCostos, "", "")
        CargarDatosDetallesPresupuesto(IdPresupuesto)

        Select Case DbMode
            Case DbModeType.insert
                '
            Case DbModeType.update
                '
                Dim pre As pre_Presupuestos = objTablas.pre_PresupuestosSelectByPK(IdPresupuesto)

                lueSucursales.EditValue = pre.IdSucursalFk
                objCombos.pre_Departamentos(lueDepartamentos, lueSucursales.EditValue)
                lueDepartamentos.EditValue = pre.IdDepartamentoFk
                lueCentroCostos.EditValue = pre.IdCentroCostosFk
                seAnio.Value = pre.Anio.Value
                meComentario.Text = pre.Comentario

                CargarGridCuentas()
        End Select
    End Sub

    Private Sub lueSucursales_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lueSucursales.EditValueChanged
        objCombos.pre_Departamentos(lueDepartamentos, lueSucursales.EditValue)
    End Sub



    Private Sub sbAgregarCuenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbAgregarCuenta.Click

        Dim frm As New pre_frmBusquedaCuenta(IdPresupuesto)

        frm.ShowDialog(Me)

        If frm.DialogResult = DialogResult.OK Then
            For Each cuenta As con_Cuentas In frm.ListaCuentas
                'ListaCuentas.Add(cuenta)
                Dim presupuestoCuenta As New pre_PresupuestoCuenta
                presupuestoCuenta.IdCuentaFk = cuenta.IdCuenta
                presupuestoCuenta.IdPresupuestoFk = IdPresupuesto
                Try
                    objTablas.pre_PresupuestoCuentaInsert(presupuestoCuenta)
                Catch ex As Exception
                    MsgBox("Ha ocurrido un error al agregar la cuenta: " + cuenta.Nombre + " Error: " + ex.Message, MsgBoxStyle.Critical)
                End Try
            Next

            'gcCuentasContables.DataSource = ListaCuentas
            CargarGridCuentas()
            gvCuentasContables.RefreshData()

        ElseIf frm.DialogResult = DialogResult.Cancel Then
            MsgBox("El proceso de selección se canceló.", MsgBoxStyle.Information)
        End If

    End Sub

    Private Sub sbEliminarCuenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbEliminarCuenta.Click

        If gvCuentasContables.SelectedRowsCount = 0 Then
            MsgBox("No exiten cuentas que eliminar.", MsgBoxStyle.Information)
            Return
        End If

        If MsgBox("¿Esta seguro que desea eliminar la cuenta contable?", MsgBoxStyle.YesNo, MsgBoxStyle.Question) = MsgBoxResult.No Then
            Return
        End If

        Dim idPreCuenta As Integer = gvCuentasContables.GetFocusedRowCellDisplayText("IdPresupuestoCuentaPk")

        Try
            objTablas.pre_PresupuestoCuentaDeleteByPK(idPreCuenta)
        Catch ex As SqlException
            MsgBox("Ha ocurrido un error al eliminar la cuenta.  La cuenta no puede ser eliminada debido a que ya posee valores asociados.", MsgBoxStyle.Critical)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al eliminar la cuenta.  Error: " + ex.Message, MsgBoxStyle.Critical)
        End Try

        CargarGridCuentas()
        'gvCuentasContables.RefreshData()

    End Sub

#Region "Botones de edicion de costos"


    Private Sub beEnero_ButtonClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beEnero.ButtonClick, beSeptiembre.ButtonClick, beAgosto.ButtonClick, beJulio.ButtonClick, beJunio.ButtonClick, beMayo.ButtonClick, beAbril.ButtonClick, beMarzo.ButtonClick, beDiciembre.ButtonClick, beNoviembre.ButtonClick, beOctubre.ButtonClick, beFebrero.ButtonClick

        If gvCuentasContables.SelectedRowsCount() > 0 Then

            Dim textEdit As TextEdit = CType(sender, TextEdit)
            'Dim idPresupuestoCuenta As Integer = gvCuentasContables.GetFocusedRowCellDisplayText("IdPresupuestoCuentaPk")

            Dim _
                frmEditarvalor As _
                    New pre_frmValorPresupuesto(IdPresupuesto, CType(textEdit.Tag, pre_DetallesPresupuesto.Meses))

            If frmEditarvalor.ShowDialog(Me) = DialogResult.OK Then
                'textEdit.Text = objTablas.pre_ObtenerTotalPresupuestoMes(textEdit.Tag, idPresupuesto)
                'teTotal.Text = objTablas.pre_ObtenerTotalPresupuesto(idPresupuesto)
                CargarDatosDetallesPresupuesto(IdPresupuesto)
            End If
        End If

    End Sub

#End Region

    Private Sub beEnero_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles beEnero.KeyPress, beFebrero.KeyPress, beSeptiembre.KeyPress, beOctubre.KeyPress, beNoviembre.KeyPress, beMayo.KeyPress, beMarzo.KeyPress, beJunio.KeyPress, beJulio.KeyPress, beDiciembre.KeyPress, beAgosto.KeyPress, beAbril.KeyPress

        If gvCuentasContables.SelectedRowsCount() > 0 And Asc(e.KeyChar) = 13 Then

            Dim textEdit As TextEdit = CType(sender, TextEdit)
            'Dim idPresupuestoCuenta As Integer = gvCuentasContables.GetFocusedRowCellDisplayText("IdPresupuestoCuentaPk")

            Dim _
                frmEditarvalor As _
                    New pre_frmValorPresupuesto(IdPresupuesto, CType(textEdit.Tag, pre_DetallesPresupuesto.Meses))

            If frmEditarvalor.ShowDialog(Me) = DialogResult.OK Then
                'textEdit.Text = objTablas.pre_ObtenerTotalPresupuestoMes(textEdit.Tag, idPresupuesto)
                'teTotal.Text = objTablas.pre_ObtenerTotalPresupuesto(idPresupuesto)
                CargarDatosDetallesPresupuesto(IdPresupuesto)
            End If

        End If

    End Sub
End Class