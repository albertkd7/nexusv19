﻿Imports DevExpress.XtraTreeList
Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraTreeList.Nodes
Public Class con_frmCuentas
    Dim blConta As New ContabilidadBLL(g_ConnectionString)
    Dim blTablas As New TableBusiness(g_ConnectionString)
    Dim entCuenta As New con_cuentas
    Dim bPermitir_Guardar As Boolean = False
    Private Sub frmCuentas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'ShowModeList()
        'rbMenu.qbSave.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        'rbMenu.qbBack.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        'rbMenu.qbNext.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        CargarArbol()
    End Sub
    

    Private Sub con_frmCuentas_Guardar() Handles sbGuardar.Click
        If Not bPermitir_Guardar Then
            Exit Sub
        End If
        With entCuenta
            .IdCuenta = txtIdCuenta.Text
            .Nombre = txtNombreCuenta.Text
            .Tipo = cboTipo.SelectedIndex
            .Nivel = txtNivelCuenta.Text
            .CargarComo = cboCM.SelectedIndex + 1
            .EsTransaccional = chkTransaccional.Checked
        End With
        Dim msj = "La cuenta fue creada exitosamente"
        If DbMode = DbModeType.insert Then
            blTablas.con_CuentasInsert(entCuenta)
            DbMode = DbModeType.update

            'Dim parentNode As TreeListNode = tlCuentas.Nodes(0)
            Dim parentNode As TreeListNode = tlCuentas.FocusedNode()
            ' insert a new node as the first child within its parent
            Dim newNode As TreeListNode = tlCuentas.AppendNode(New Object() {entCuenta.IdCuenta, entCuenta.Nombre, entCuenta.Tipo, entCuenta.CargarComo, entCuenta.Nivel, entCuenta.EsTransaccional, entCuenta.EsTransaccional}, parentNode)
        Else
            blTablas.con_CuentasUpdate(entCuenta)
            tlCuentas.FocusedNode.SetValue(1, entCuenta.Nombre)
            msj = "La cuenta fue actualizada exitosamente"
        End If
        MsgBox(msj, MsgBoxStyle.Information, "Nota")
        'If MsgBox(sMsg + Chr(13) + "Desea actualizar el árbol nuevamente?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
        '    CargarArbol()
        'Else
        'End If

        bPermitir_Guardar = False
    End Sub
    Private Sub frmCuentas_Delete_Click() Handles Me.Eliminar
        If txtIdCuenta.Text = "" Then
            Exit Sub
        End If
        If Not tlCuentas.FocusedNode.GetValue(5) Then
            MsgBox("No puede eliminar ésta cuenta, tiene sub-cuentas", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        If MsgBox("Está seguro(a) de eliminar la cuenta?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            objTablas.con_CuentasDeleteByPK(txtIdCuenta.Text)
        End If
    End Sub
    Private Sub frmCuentas_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = objTablas.con_CuentasSelectAll()

        Dim rpt As New con_rptCuentas

        rpt.DataSource = dt
        rpt.DataMember = ""
        rpt.xlrEmpresa.Text = gsNombre_Empresa
        rpt.xlrTitulo.Text = "Listado para revisión del catálogo de cuentas"
        rpt.ShowPreviewDialog()

    End Sub
    Private Sub CargarArbol()
        Dim dt As DataTable = blTablas.con_CuentasSelectAll
        Dim dv As New DataView(dt)
        tlCuentas.KeyFieldName = "IdCuenta"
        tlCuentas.ParentFieldName = "IdCuentaMayor"
        tlCuentas.DataSource = dv
        tlCuentas.BestFitColumns()
    End Sub
    Private Sub tlCuentas_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tlCuentas.MouseClick
        If e.Button = Windows.Forms.MouseButtons.Right Then
            If tlCuentas.FocusedNode.GetValue(5) And blConta.IsCuentaConSaldo(tlCuentas.FocusedNode.GetValue(0)) Then
                sbCrearTitular.Enabled = False
            Else
                sbCrearNormal.Enabled = True
            End If
        End If
        If e.Button = Windows.Forms.MouseButtons.Left Then
            ActualizarCuenta()
        End If
    End Sub
    Private Sub ActualizarCuenta()
        Dim snTransac As Boolean = tlCuentas.FocusedNode.GetValue(5)
        gcCuentas.Visible = True
        DbMode = DbModeType.update

        chkTransaccional.Properties.ReadOnly = tlCuentas.FocusedNode.HasChildren
        
        Dim NodoPadre As Nodes.TreeListNode = tlCuentas.FocusedNode.ParentNode
        If Not NodoPadre Is Nothing Then
            txtIdCuentaPadre.Text = NodoPadre.GetDisplayText(0).Trim & " - " & NodoPadre.GetDisplayText(1)
        Else
            txtIdCuentaPadre.Text = ""
        End If

        chkTransaccional.Checked = snTransac
        txtIdCuenta.Text = tlCuentas.FocusedNode.GetDisplayText(0)
        txtNombreCuenta.Text = tlCuentas.FocusedNode.GetDisplayText(1)
        txtNivelCuenta.Text = tlCuentas.FocusedNode.Level
        cboTipo.SelectedIndex = tlCuentas.FocusedNode.GetDisplayText(2)
        cboCM.SelectedIndex = tlCuentas.FocusedNode.GetDisplayText(3) - 1
        With entCuenta
            If NodoPadre Is Nothing Then
                .IdCuentaMayor = ""
            Else
                .IdCuentaMayor = NodoPadre.GetDisplayText(0).Trim
            End If
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .FechaHoraModificacion = Now
            .ModificadoPor = objMenu.User
        End With
        bPermitir_Guardar = True
        txtIdCuenta.Properties.ReadOnly = True

    End Sub
    Private Sub tlCuentas_FocusedNodeChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraTreeList.FocusedNodeChangedEventArgs) Handles tlCuentas.FocusedNodeChanged
        cboCM.SelectedIndex = 0
        txtIdCuenta.Text = ""
        txtIdCuentaPadre.Text = ""
        txtNombreCuenta.Text = ""
        cboTipo.SelectedIndex = 0
    End Sub
    Private Sub sbCrearTitular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCrearTitular.Click
        Dim snTransac As Boolean = False
        DbMode = DbModeType.insert
        gcCuentas.Visible = True
        txtIdCuentaPadre.Text = ""

        txtIdCuenta.Text = ""
        txtNombreCuenta.Text = ""
        chkTransaccional.Checked = snTransac
        txtNivelCuenta.Text = "0"
        cboCM.SelectedIndex = 0
        cboTipo.SelectedIndex = 0
        With entCuenta
            .IdCuentaMayor = ""
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .FechaHoraModificacion = Nothing
            .ModificadoPor = ""
        End With
        txtIdCuenta.Properties.ReadOnly = False
        chkTransaccional.Enabled = False
        txtIdCuenta.Focus()
        bPermitir_Guardar = True
    End Sub
    Private Sub sbCrearNormal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCrearNormal.Click
        'Dim snTransac As Boolean = tlCuentas.FocusedNode.GetValue(5)
        DbMode = DbModeType.insert
        gcCuentas.Visible = True
        txtIdCuentaPadre.Text = tlCuentas.FocusedNode.GetDisplayText(0).Trim & _
            " - " & tlCuentas.FocusedNode.GetDisplayText(1)
        Dim UltimoNodo As Nodes.TreeListNode = tlCuentas.FocusedNode.LastNode
        If Not UltimoNodo Is Nothing Then
            txtIdCuenta.Text = UltimoNodo.GetDisplayText(0)
        End If
        chkTransaccional.Checked = False
        txtNivelCuenta.Text = tlCuentas.FocusedNode.Level + 1
        With entCuenta
            .IdCuentaMayor = tlCuentas.FocusedNode.GetDisplayText(0).Trim
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .FechaHoraModificacion = Nothing
            .ModificadoPor = ""
        End With
        txtIdCuenta.Properties.ReadOnly = False
        txtNombreCuenta.Text = ""
        txtIdCuenta.Focus()
        chkTransaccional.Enabled = True
        bPermitir_Guardar = True
    End Sub
    Private Sub sbRevisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbRevisar.Click
        blConta.ChequeaCuentas()
        MsgBox("Cuentas revisadas", MsgBoxStyle.Information, "Nota")
    End Sub
End Class