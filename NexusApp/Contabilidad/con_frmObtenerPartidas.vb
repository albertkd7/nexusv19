Imports NexusELL.TableEntities
Imports NexusBLL
Imports System.IO
Public Class con_frmObtenerPartidas
    Dim dtDetalle As New DataTable
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Dim blfac As New FacturaBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim fd As New FuncionesBLL(g_ConnectionString)
    Dim dtParametros As DataTable = blAdmon.ObtieneParametros()
    Dim PartidaHeader As con_Partidas, PartidaDetalle As List(Of con_PartidasDetalle)
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim IdSucUser As Integer = SiEsNulo(EntUsuario.IdSucursal, 1)
    ' Dim bl As New FacturaBLL(g_ConnectionString), fd As New FuncionesBLL(g_ConnectionString)

    Private _IdCliente As System.String
    Public Property IdCliente() As System.String
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As System.String)
            _IdCliente = value
        End Set
    End Property

    Private _TotalAnticipos As System.Decimal
    Public Property TotalAnticipos() As System.Decimal
        Get
            Return _TotalAnticipos
        End Get
        Set(ByVal value As System.Decimal)
            _TotalAnticipos = value
        End Set
    End Property

    Private _TipoVenta As System.Int32
    Public Property TipoVenta() As System.Int32
        Get
            Return _TipoVenta
        End Get
        Set(ByVal value As System.Int32)
            _TipoVenta = value
        End Set
    End Property


    Private _Aceptar As Boolean = False

    Public Property Aceptar() As Boolean
        Get
            Return _Aceptar
        End Get
        Set(ByVal value As Boolean)
            _Aceptar = value
        End Set
    End Property

    Private _DocumentoDetallaIva As Boolean = False

    Public Property DocumentoDetallaIva() As Boolean
        Get
            Return _DocumentoDetallaIva
        End Get
        Set(ByVal value As Boolean)
            _DocumentoDetallaIva = value
        End Set
    End Property


    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        LlenarEntidades()
        Aceptar = True
        Close()
    End Sub


    Private Sub sbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCancel.Click
        TotalAnticipos = 0.0
        Aceptar = False
        Close()
    End Sub


    Private Sub fac_frmMostrarCambio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        InicializaGrid()
    End Sub


    Private Sub LlenarEntidades()
        Dim IdPartida As Integer = fd.ObtenerUltimoId("con_Partidas", "IdPartida") + 1
        PartidaHeader = New con_Partidas
        For i = 0 To gvFP.DataRowCount - 1
            If gvFP.GetRowCellValue(i - 1, "Numero") <> gvFP.GetRowCellValue(i, "Numero") Then
                With PartidaHeader
                    .IdPartida = IdPartida 'gvFP.GetRowCellValue(i, "IdPartida")
                    .Fecha = gvFP.GetRowCellValue(i, "Fecha") 'deFecha.DateTime
                    .IdTipo = gvFP.GetRowCellValue(i, "IdTipo") 'leTiposPartida.EditValue
                    .IdSucursal = piIdSucursal 'leSucursal.EditValue
                    .Numero = gvFP.GetRowCellValue(i, "Numero")
                    .Concepto = "" 'teConceptoPartida.EditValue
                    .Actualizada = 0
                    'If ceExcluir.EditValue And ceExcluir.Visible Then
                    .IdModuloOrigen = 99
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                    .ModificadoPor = ""
                    .FechaHoraModificacion = Nothing
                End With

                ' cargando el detalle
                PartidaDetalle = New List(Of con_PartidasDetalle)
                Dim n As Integer
                'Dim entDetalle As New con_PartidasDetalle
                'If PartidaHeader.IdPartida = gvFP.GetRowCellValue(i, "IdPartida") Then
                For j = 0 To gvFP.DataRowCount - 1
                    Dim entDetalle As New con_PartidasDetalle
                    ' n = 0
                    If PartidaHeader.Numero = gvFP.GetRowCellValue(j, "Numero") Then
                        'Dim entDetalle As New con_PartidasDetalle
                        With entDetalle
                            .IdPartida = PartidaHeader.IdPartida 'gvFP.GetRowCellValue(j, "IdPartida") '
                            .IdCuenta = gvFP.GetRowCellValue(j, "IdCuenta")
                            .Referencia = gvFP.GetRowCellValue(j, "Numero")
                            .Concepto = "" 'gvFP.GetRowCellValue(j, "Concepto")
                            .Debe = SiEsNulo(gvFP.GetRowCellValue(j, "Debe"), 0)
                            .Haber = SiEsNulo(gvFP.GetRowCellValue(j, "Haber"), 0)
                            .IdCentro = "" 'gvFP.GetRowCellValue(j, "IdCentro")
                            .IdDetalle = j + 1
                            .CreadoPor = objMenu.User
                            .FechaHoraCreacion = Now
                        End With
                        PartidaDetalle.Add(entDetalle)
                    End If
                Next
                Dim msj As String = bl.InsertaPartidasImport(PartidaHeader, PartidaDetalle)
            Else

            End If

        Next

        MsgBox("Partidas importados exitůsamente", MsgBoxStyle.Information, "Nota")

    End Sub

    Private Sub sbImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbImportar.Click

        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            Return
        End If
        Dim csv_file As StreamReader = File.OpenText(ofd.FileName)
        Try
            While csv_file.Peek() > 0
                Dim sLine As String = csv_file.ReadLine()
                Dim aData As Array = sLine.Split(",")

                gvFP.AddNewRow()
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "IdTipo", aData(0))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Numero", aData(1))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Fecha", aData(2))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "IdCuenta", aData(3))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Debe", SiEsNulo(aData(4), 0.0))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Haber", SiEsNulo(aData(5), 0.0))
                'gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "IdCuentaAuxiliar", SiEsNulo(aData(6), ""))
                'gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Banco", SiEsNulo(aData(7), ""))
                'gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "NumCheque", SiEsNulo(aData(8), ""))
                'gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "IdCuentaCompleta", SiEsNulo(aData(9), ""))
                'gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Fecha", aData(10))
                'gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "IdPartida", aData(11))
                gvFP.UpdateCurrentRow()

            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Return
        End Try

        csv_file.Close()

    End Sub

    Private Sub InicializaGrid()
        dtDetalle = New DataTable
        dtDetalle.Columns.Add(New DataColumn("IdTipo", GetType(String)))
        dtDetalle.Columns.Add(New DataColumn("Numero", GetType(String)))
        dtDetalle.Columns.Add(New DataColumn("Fecha1", GetType(Date)))
        dtDetalle.Columns.Add(New DataColumn("IdCuenta", GetType(String)))
        dtDetalle.Columns.Add(New DataColumn("Debe", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("Haber", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("IdCuentaAuxiliar", GetType(String)))
        dtDetalle.Columns.Add(New DataColumn("Banco", GetType(String)))
        dtDetalle.Columns.Add(New DataColumn("NumCheque", GetType(String)))
        dtDetalle.Columns.Add(New DataColumn("IdCuentaCompleta", GetType(String)))
        dtDetalle.Columns.Add(New DataColumn("Fecha", GetType(Date)))
        dtDetalle.Columns.Add(New DataColumn("IdPartida", GetType(Integer)))
        gcFP.DataSource = dtDetalle
    End Sub

End Class