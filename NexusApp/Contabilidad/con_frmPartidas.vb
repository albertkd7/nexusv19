﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns
Imports System.IO

Public Class con_frmPartidas
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Dim PartidaHeader As con_Partidas
    Dim PartidaDetalle As List(Of con_PartidasDetalle)
    Dim blAmon As New AdmonBLL(g_ConnectionString)
    Dim sConcepto As String = ""
    Dim entCuentas As con_Cuentas, dtParam As DataTable = blAmon.ObtieneParametros(), entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim FechaConsulta As String = String.Format("{0:yyyyMMdd}", dtParam.Rows(0).Item("FechaCierre"))

    Private Sub con_frmPartidas_Activated(sender As Object, e As EventArgs) Handles Me.Activated ' para cuando se active el formulario actualice todos los registros
        gc2.DataSource = bl.ConsultaPartidas(objMenu.User, FechaConsulta).Tables(0)
        gv.BestFitColumns()
    End Sub

    Private Sub con_frmPartidas_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        CargaCombos()
        teIdPartida.EditValue = objFunciones.ObtenerUltimoId("CON_PARTIDAS", "IdPartida")
        Dim Template = Application.StartupPath & "\Copa.Ini"
        If FileIO.FileSystem.FileExists(Template) Then
            sbImportarCsv.Visible = True
        Else
            sbImportarCsv.Visible = False
        End If

        ActivarControles(False)
    End Sub
  
    Private Sub con_frmPartidas_Nuevo() Handles Me.Nuevo
        ActivarControles(True)
        gv.CancelUpdateCurrentRow()

        deFecha.EditValue = Today
        teNumero.EditValue = ""
        teConceptoPartida.EditValue = ""
        PartidaHeader = New con_Partidas
        PartidaHeader.IdModuloOrigen = 1
        leSucursal.EditValue = entUsuario.IdSucursal
        leTiposPartida.ItemIndex = 1
        gc.DataSource = bl.pro_ObtenerPartidasDetalle(-1)

        teNumero.Focus()
        'gv.AddNewRow()
        gv.FocusedColumn = gv.Columns(0)
        xtcPartidas.SelectedTabPage = xtpDatos
        teNumero.Focus()
    End Sub
    Private Sub con_frmPartidas_Guardar() Handles Me.Guardar
        If Not DatosValidos() Then
            Return
        End If
        CargaEntidades()
        Dim msj As String = ""
        If DbMode = DbModeType.insert Then
            msj = bl.InsertaPartidas(PartidaHeader, PartidaDetalle)
            If msj = "" Then
                MsgBox("La partida fue almacenada exitosamente", MsgBoxStyle.Information)
                teNumero.EditValue = PartidaHeader.Numero
                teIdPartida.EditValue = PartidaHeader.IdPartida
            Else
                MsgBox(String.Format("ERROR AL CREAR LA PARTIDA{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
                Exit Sub
            End If
        Else
            msj = bl.UpdatePartidas(PartidaHeader, PartidaDetalle)
            If msj = "" Then
                MsgBox("La partida fue actualizada exitosamente", MsgBoxStyle.Information)
            Else
                MsgBox(String.Format("ERROR AL ACTUALIZAR LA PARTIDA{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If
        xtcPartidas.SelectedTabPage = xtpLista
        gc2.DataSource = bl.ConsultaPartidas(objMenu.User, FechaConsulta).Tables(0)
        gc2.Focus()
        MostrarModoInicial()
        ActivarControles(False)
    End Sub
    Private Sub con_frmPartidas_Editar() Handles Me.Editar
        ActivarControles(True)
        teNumero.Focus()
    End Sub
    Private Sub con_frmPartidas_Consulta() Handles Me.Consulta
        'Dim Fecha As String = String.Format("{0:yyyyMMdd}", dtParam.Rows(0).Item("FechaCierre"))
        'teIdPartida.EditValue = objConsultas.ConsultaPartidas(frmConsultaDetalle, Fecha)
        'CargaControles(0)
    End Sub
    Private Sub con_frmPartidas_Revertir() Handles Me.Revertir
        xtcPartidas.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub
    Private Sub con_frmPartidas_Eliminar() Handles Me.Eliminar

        PartidaHeader = objTablas.con_PartidasSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdPartida"))

        Dim EsOk As Boolean = ValidarFechaCierre(PartidaHeader.Fecha)
        If Not EsOk Then
            MsgBox("Fecha de la partida corresponde a un período ya cerrado", MsgBoxStyle.Critical, "Imposible eliminar")
            Return
        End If
        If MsgBox("Está seguro(a) de eliminar la partida?", MsgBoxStyle.YesNo + MsgBoxStyle.Information, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        If PartidaHeader.IdModuloOrigen = 2 Then
            MsgBox("No puede eliminar ésta partida. Proviene del módulo de bancos", MsgBoxStyle.Critical, "Nota")
            Return
        End If
        If PartidaHeader.IdModuloOrigen > 2 And PartidaHeader.IdModuloOrigen <> 99 Then
            MsgBox("No puede eliminar ésta partida. Proviene del otro módulo ", MsgBoxStyle.Critical, "Nota")
            Return
        End If

        PartidaHeader.ModificadoPor = objMenu.User
        PartidaHeader.FechaHoraModificacion = Now
        objTablas.con_PartidasUpdate(PartidaHeader)

        objTablas.con_PartidasDeleteByPK(PartidaHeader.IdPartida)
        teIdPartida.EditValue = objFunciones.ObtenerUltimoId("con_partidas", "IdPartida")
        gc2.DataSource = bl.ConsultaPartidas(objMenu.User, FechaConsulta).Tables(0)
    End Sub
    Private Sub con_frmPartidas_Reporte() Handles Me.Reporte
        Dim dt As New DataTable
        PartidaHeader = objTablas.con_PartidasSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdPartida"))
        dt = bl.rptPartidaReporteByPk(SiEsNulo(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdPartida"), 0))

        Dim rpt As New con_rptPartida() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Partida contable"
        rpt.xrlDescMoneda.Text = FechaToString(PartidaHeader.Fecha, PartidaHeader.Fecha)
        rpt.ShowPreviewDialog()
    End Sub
    
    Private Sub CargaCombos()
        objCombos.conTiposPartida(leTiposPartida)
        objCombos.con_CentrosCosto(rileIdCentro, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
    End Sub

    Private Sub CargaPantalla()
        xtcPartidas.SelectedTabPage = xtpDatos
        teNumero.Focus()

        With PartidaHeader
            teIdPartida.EditValue = .IdPartida
            teNumero.EditValue = .Numero
            leTiposPartida.EditValue = .IdTipo
            leSucursal.EditValue = .IdSucursal
            deFecha.DateTime = SiEsNulo(.Fecha, Today())
            teConceptoPartida.EditValue = .Concepto
            gc.DataSource = bl.pro_ObtenerPartidasDetalle(.IdPartida)
            ceExcluir.Visible = .IdModuloOrigen = 99 Or (deFecha.DateTime.Day = 31 And deFecha.DateTime.Month = 12)
            ceExcluir.EditValue = .IdModuloOrigen = 99
        End With

    End Sub
    Private Sub CargaEntidades()
        With PartidaHeader
            .Fecha = deFecha.DateTime
            .IdTipo = leTiposPartida.EditValue
            .IdSucursal = leSucursal.EditValue
            .Concepto = teConceptoPartida.EditValue
            .Actualizada = 0
            If ceExcluir.EditValue And ceExcluir.Visible Then
                .IdModuloOrigen = 99
            Else
                .IdModuloOrigen = 0
            End If

            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
            Else
                .ModificadoPor = objMenu.User
                .FechaHoraModificacion = Now
            End If
        End With
        ' cargando el detalle
        PartidaDetalle = New List(Of con_PartidasDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New con_PartidasDetalle
            With entDetalle
                .IdPartida = PartidaHeader.IdPartida
                .IdCuenta = gv.GetRowCellValue(i, "IdCuenta")
                .Referencia = gv.GetRowCellValue(i, "Referencia")
                .Concepto = gv.GetRowCellValue(i, "Concepto")
                .Debe = gv.GetRowCellValue(i, "Debe")
                .Haber = gv.GetRowCellValue(i, "Haber")
                .IdCentro = gv.GetRowCellValue(i, "IdCentro")
                .IdDetalle = i + 1
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            PartidaDetalle.Add(entDetalle)
        Next
    End Sub
    Private Sub teIdCuenta_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles teIdCuenta.KeyDown
        If e.KeyCode = Keys.Enter Then
            gv.ActiveEditor.EditValue = SiEsNulo(gv.ActiveEditor.EditValue, "")
        End If
    End Sub
    Private Sub teIdCuenta_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles teIdCuenta.Validating
        gv.EditingValue = SiEsNulo(gv.EditingValue, "")

        Dim entCuenta As con_Cuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, gv.ActiveEditor.EditValue) 'CORREGIR
        If Not entCuenta.EsTransaccional And entCuenta.IdCuenta <> "" Then
            MsgBox("La cuenta no permite aceptar transacciones" & Chr(13) & _
                   "Es de mayor o tiene subcuentas", 64, "Nota")
            e.Cancel = True
        End If

        gv.ActiveEditor.EditValue = entCuenta.IdCuenta
        gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", sConcepto)
        teNombreCuenta.EditValue = entCuenta.Nombre
    End Sub
    'Private Sub teIdCuenta_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles teIdCuenta.Leave
    '    objCombos.con_CargarCentrosCostoCuentaMayor(rileIdCentro, SiEsNulo(gv.GetFocusedRowCellValue("IdCuenta"), ""))
    'End Sub
    
#Region "Grid"

    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub
    Private Sub gv_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles gv.CellValueChanged
        If gv.FocusedRowHandle >= 0 And (gv.FocusedColumn.Name = "gcDebe" Or gv.FocusedColumn.Name = "gcHaber") Then
            gv.UpdateCurrentRow()
        End If
    End Sub
    Private Sub gv_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gv.FocusedRowChanged
        entCuentas = objTablas.con_CuentasSelectByPK(gv.GetFocusedRowCellValue("IdCuenta"))
        teNombreCuenta.EditValue = entCuentas.Nombre
    End Sub
    'Private Sub gv_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.GotFocus
    '    SendKeys.Send("{Enter}")
    'End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        'gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", sConcepto)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Haber", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Debe", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", "")
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        Dim IdCuenta As String = SiEsNulo(gv.GetFocusedRowCellValue("IdCuenta"), "")
        'Dim IdCentro As String = SiEsNulo(gv.GetFocusedRowCellValue("IdCentro"), "")
        If IdCuenta = "" Then
            MsgBox("Debe de especificar la cuenta contable", MsgBoxStyle.Critical, "Nota")
            e.Valid = False
            Exit Sub
        End If
        'If IdCentro = "" Then
        '    MsgBox("Es necesario especificar el centro de costo", MsgBoxStyle.Critical, "Nota")
        '    e.Valid = False
        '    Exit Sub
        'End If
        'Dim i As Integer = bl.con_ValidaCentroCuenta(IdCuenta, IdCentro)

        'If i = 1 Then
        '    MsgBox("Es necesario definir el centro de costo a ésta partida", MsgBoxStyle.Information, "Nota")
        '    e.Valid = False
        '    Exit Sub
        'End If
        'If i = 2 Then
        '    MsgBox("El centro de costo especificado no existe o debe de especificarlo para ésta cuenta", MsgBoxStyle.Exclamation, "Nota")
        '    e.Valid = False
        '    Exit Sub
        'End If
        'If i = 3 Then
        '    MsgBox("El centro de costo especificado está mal asociado a la cuenta contable", MsgBoxStyle.Exclamation, "Nota")
        '    Exit Sub
        'End If
        Dim dCargo As Decimal = gv.GetFocusedRowCellValue("Debe")
        Dim dHaber As Decimal = gv.GetFocusedRowCellValue("Haber")
        If dCargo <> 0 And dHaber <> 0 Then
            MsgBox("Solo puede colocar valor en debe o haber no en ambos", MsgBoxStyle.Exclamation, "Nota")
            e.Valid = False
            Exit Sub
        End If
        sConcepto = gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto")
        gv.UpdateTotalSummary()
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        txtDiferencia.EditValue = gcDebe.SummaryItem.SummaryValue - gcHaber.SummaryItem.SummaryValue
    End Sub

#End Region
    Private Function DatosValidos()
        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            MsgBox("La fecha de la partida debe ser de un período válido", 16, "Error de datos")
            Return False
        End If
        If PartidaHeader.IdModuloOrigen > 1 And PartidaHeader.IdModuloOrigen <> 99 Then
            MsgBox("No es posible actualizar la partida" & Chr(13) & _
                   "Proviene de otro módulo", 16, "Error de usuario")
            Return False
        End If

        For i = 0 To gv.DataRowCount - 1
            Dim entCta As con_Cuentas = objTablas.con_CuentasSelectByPK(gv.GetRowCellValue(i, "IdCuenta"))
            If entCta.EsTransaccional = 0 Then
                MsgBox("La cuenta: " & entCta.IdCuenta & ", tiene sub-cuentas" & Chr(13) & "Solo puede usar cuentas de detalle", 16, "Error de datos")
                Return False
            End If
        Next

        If leSucursal.EditValue = 0 Or leSucursal.EditValue = Nothing Or leSucursal.Text = "" Then
            MsgBox("Debe seleccionar una sucursal", 64, "Nota")
            Return False
        End If

        If SiEsNulo(leTiposPartida.EditValue, "") = "" Or SiEsNulo(leTiposPartida.Text, "") = "" Then
            MsgBox("Debe seleccionar una tipo de partida", 64, "Nota")
            Return False
        End If

        If txtDiferencia.EditValue <> 0.0 Then
            If dtParam.Rows(0).Item("GuardarDesbalance") Then
                If MsgBox("La partida no está cuadrada" & Chr(13) & _
                      "Está seguro(a) de continuar?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
                    Return False
                End If
            Else
                MsgBox("La partida no está cuadrada", MsgBoxStyle.Critical, "Imposible salir")
                Return False
            End If

        End If
        Return True
    End Function
    
    Private Sub sbCopiar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles sbCopiar.Click

        Dim EsOk As Boolean = ValidarFechaCierre(Today)
        If Not EsOk Then
            MsgBox("La fecha del computador no corresponde al período activo", MsgBoxStyle.Information)
            Return
        End If
        Dim FechaNueva As Date = Today
        con_frmPideFecha.ShowDialog()
        FechaNueva = con_frmPideFecha.deFechaNew.EditValue
        If MsgBox("Está seguro de copiar la partida actual con fecha: " + FechaNueva.ToString(), MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        CargaEntidades()
        PartidaHeader.Fecha = FechaNueva
        PartidaHeader.CreadoPor = objMenu.User
        PartidaHeader.FechaHoraCreacion = Now
        Dim msj As String = ""

        msj = bl.InsertaPartidas(PartidaHeader, PartidaDetalle)
        If msj = "" Then
            MsgBox("La partida fue copiada exitosamente", MsgBoxStyle.Information)
            teNumero.EditValue = PartidaHeader.Numero
            teIdPartida.EditValue = PartidaHeader.IdPartida
        Else
            MsgBox(String.Format("SE DETECTÓ UN ERROR AL CREAR LA PARTIDA{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
            Exit Sub
        End If
    End Sub
    Private Sub sbImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbImportar.Click
        If deFecha.Properties.ReadOnly Then
            Exit Sub
        End If

        If Not gsNombre_Empresa.ToUpper.StartsWith("TRANS EXPRESS") Then
            Dim ofd As New OpenFileDialog()
            ofd.ShowDialog()
            If ofd.FileName = "" Then
                Return
            End If
            Dim csv_file As StreamReader = File.OpenText(ofd.FileName)
            Try
                While csv_file.Peek() > 0
                    Dim sLine As String = csv_file.ReadLine()
                    Dim aData As Array = sLine.Split(",")
                    Dim i As Integer = bl.pro_ValidaCentroCuenta(aData(0), aData(5))
                    'If i > 0 Then
                    '    ValidaCuentaCentroCosto(i, aData(0), aData(5))
                    'Else
                    gv.AddNewRow()
                    gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", aData(0))
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", aData(1))
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", aData(2))
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Debe", SiEsNulo(aData(3), 0))
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Haber", SiEsNulo(aData(4), 0))
                    gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", SiEsNulo(aData(5), ""))
                    gv.UpdateCurrentRow()
                    'End If
                End While
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                Return
            End Try

            csv_file.Close()
        Else
            Dim ofd As New OpenFileDialog()
            ofd.ShowDialog()
            If ofd.FileName = "" Then
                Return
            End If
            Dim Archivo As System.IO.StreamReader = File.OpenText(ofd.FileName)
            Dim separador As String = InputBox("Separador:", "Defina el delimitador del archivo", "|")
            If separador = "" Then
                separador = "|"
            End If
            Try
                While Archivo.Peek() > 0
                    Dim sLine As String = Archivo.ReadLine()
                    Dim aData As Array = sLine.Split(separador)
                    Dim Fech As String = aData(2)

                    If aData(0) <> "" Then
                        deFecha.EditValue = CDate(Fech)

                        gv.AddNewRow()
                        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", aData(0))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", aData(1))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Concepto", aData(3))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Debe", SiEsNulo(aData(4), 0))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Haber", SiEsNulo(aData(5), 0))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", "")
                        gv.UpdateCurrentRow()
                    End If
                End While
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                Return
            End Try
            Archivo.Close()
        End If

    End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
        teIdPartida.Properties.ReadOnly = True
    End Sub


    Private Sub sbAgregarCompra_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbAgregarCompra.Click
        com_frmCompras.sbGuardar.Visible = True
        com_frmCompras.ShowDialog()
    End Sub

    Private Sub teDebe_MouseSpin(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.SpinEventArgs) Handles teDebe.Spin, teHaber.Spin
        e.Handled = True
    End Sub


    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        PartidaHeader = objTablas.con_PartidasSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdPartida"))
        CargaPantalla()
        DbMode = DbModeType.update
        teNumero.Focus()
    End Sub

    Private Sub sbImportarCsv_Click(sender As Object, e As EventArgs) Handles sbImportarCsv.Click
        Dim frmObtenerfacturacionNR As New con_frmObtenerPartidas
        Me.AddOwnedForm(frmObtenerfacturacionNR)

        'frmObtenerfacturacionNR.IdCliente = beCodCliente.EditValue
        frmObtenerfacturacionNR.ShowDialog()


        If Not frmObtenerfacturacionNR.Aceptar Then
            frmObtenerfacturacionNR.Dispose()
            Exit Sub
        End If

        'lFacturaPedio = True
        'RefrescaGrid()
        frmObtenerfacturacionNR.Dispose()
    End Sub
End Class
