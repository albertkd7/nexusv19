﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class pre_frmDepartamentos

    Public Sub CargarGridDepartamentos()

        gcDepartamentos.DataSource = objTablas.pre_DepartamentosSucursalSelectAll()
        gvDepartamentos.BestFitColumns()

    End Sub

    Private Sub pre_frmDepartamentos_Eliminar() Handles Me.Eliminar
        Dim idDepartamento As Integer = gvDepartamentos.GetFocusedRowCellDisplayText("IdDepartamentoSucursalPk")
        'Dim id As Integer = gvDepartamentos.GetFocusedRowCellValue("IdDepartamentoSucursalPk")

        If MessageBox.Show("¿Esta seguro que dese eliminar el registro?", "Elliminar", MessageBoxButtons.YesNo) = DialogResult.Yes Then

            Try
                objTablas.pre_DepartamentosSucursalDeleteByPK(idDepartamento)
            Catch ex As Exception
                MsgBox("Ha ocurrido un error: " + ex.Message, MsgBoxStyle.Critical)
            End Try

        End If

    End Sub

    Private Sub pre_frmDepartamentos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' objTablas.pre_DepartamentosSelectAll()
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        CargarGridDepartamentos()

    End Sub

    Private Sub gcDepartamentos_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gcDepartamentos.DoubleClick

        Try
            Dim idDepartamento As Integer = gvDepartamentos.GetFocusedRowCellDisplayText("IdDepartamentoSucursalPk")

            Dim mantDepartamentos As New pre_frmMantDepartamentos(idDepartamento)
            mantDepartamentos.ShowDialog(Me)

        Catch ex As Exception
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
        End Try

        CargarGridDepartamentos()

    End Sub

    Private Sub pre_frmDepartamentos_Reporte() Handles Me.Reporte
        gvDepartamentos.ShowPrintPreview()
    End Sub

    Private Sub dbNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dbNuevo.Click

        Dim mantDepartamentos As New pre_frmMantDepartamentos()
        mantDepartamentos.ShowDialog(Me)

        CargarGridDepartamentos()

    End Sub

    Private Sub sbEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbEditar.Click

        Try
            Dim idDepartamento As Integer = gvDepartamentos.GetFocusedRowCellDisplayText("IdDepartamentoSucursalPk")

            Dim mantDepartamentos As New pre_frmMantDepartamentos(idDepartamento)
            mantDepartamentos.ShowDialog(Me)

        Catch ex As Exception
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
        End Try

        CargarGridDepartamentos()

    End Sub
End Class
