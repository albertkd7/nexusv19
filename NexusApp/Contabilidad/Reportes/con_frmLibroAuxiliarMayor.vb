﻿Imports NexusBLL

Public Class con_frmLibroAuxiliarMayor
    ReadOnly bl As New ContabilidadBLL(g_ConnectionString)
    Dim TotalDebeCuenta As Decimal = 0, TotalHaberCuenta As Decimal = 0
    Dim TotalDebeMayor As Decimal, TotalHaberMayor As Decimal
    Dim TotalDebeGeneral As Decimal, TotalHaberGeneral As Decimal, SaldoCuentaMayor As Decimal

    Private Sub con_frmLibroAuxiliarMayor_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "--TODAS LAS SUCURSALES")
    End Sub

    Private Sub con_frmLibroAuxiliarMayor_Report_Click() Handles Me.Reporte
        'BeCtaContable1.beIdCuenta.EditValue = SiEsNulo(BeCtaContable1.beIdCuenta.EditValue, "1")
        'BeCtaContable2.beIdCuenta.EditValue = SiEsNulo(BeCtaContable2.beIdCuenta.EditValue, "9")
        If bl.VerificaAccesoCuentas(objMenu.User, BeCtaContable1.beIdCuenta.EditValue, BeCtaContable2.beIdCuenta.EditValue) Then
            MsgBox("No le está autorizado ver transacciones de alguna de las cuentas especificadas", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        Dim dt As DataTable = bl.rptLibroAuxiliarMayor(deDesde.EditValue, deHasta.EditValue, BeCtaContable1.beIdCuenta.EditValue, BeCtaContable2.beIdCuenta.EditValue, leSucursal.EditValue)
        Dim rpt As New con_rptLibroAuxiliar() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = (FechaToString(deDesde.EditValue, deHasta.EditValue)).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
        rpt.ShowPreview()
    End Sub
    Private Sub ExportarLibroExcel()
        If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_Auxiliar.XLSX") Then
            If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_Auxiliar.XLS") Then
                MsgBox("No existe la plantilla necesaria para poder exportar", MsgBoxStyle.Critical, "Nota")
                Exit Sub
            End If
        End If
        Dim Template As String
        teProgreso.EditValue = "PREPARANDO LOS DATOS. ESPERE..."
        teProgreso.Visible = True
        If FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_Auxiliar.XLSX") Then
            Template = Application.StartupPath & "\Plantillas\LibroAuxiliarMayor.XLSX"
            FileCopy(Application.StartupPath & "\Plantillas\Libro_Auxiliar.XLSX", Template)
        Else
            Template = Application.StartupPath & "\Plantillas\LibroAuxiliarMayor.XLS"
            FileCopy(Application.StartupPath & "\Plantillas\Libro_Auxiliar.XLS", Template)
        End If

        Dim oApp As Object = CreateObject("Excel.Application")

        'oApp.UserControl = True
        'verifico si no está en una cultura diferente, por el momento de Inglés a Español, pendiente de verificar cuando sea a la inversa
        Dim oldCI As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        Try
            oApp.DisplayAlerts = False
        Catch ex As Exception
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")
            oApp.DisplayAlerts = False
        End Try

        Dim Range As String

        oApp.workbooks.Open(Template)
        oApp.Cells(1, 1).Value = gsNombre_Empresa
        oApp.Cells(2, 1).Value = teTitulo.EditValue
        oApp.Cells(3, 1).Value = FechaToString(deDesde.EditValue, deHasta.EditValue).ToUpper()

        Dim Linea As Integer = 5, i As Integer = 0

        Dim IdCta1 As String = BeCtaContable1.beIdCuenta.EditValue, IdCta2 = BeCtaContable2.beIdCuenta.EditValue

        'obtengo el bloque de cuentas que se van a generar con sus saldos anteriores
        Dim dt As DataTable = bl.rptCuentasLibroAuxiliarMayorExcel(IdCta1, IdCta2, deDesde.EditValue, leSucursal.EditValue)

        While i <= dt.Rows.Count - 1 'recorro todas las cuentas

            'obtengo las partidas para la cuenta actual
            Dim IdCuenta As String = dt.Rows(i).Item("IdCuenta")
            teProgreso.EditValue = String.Format("GENERANDO LA CUENTA {0}. ESPERE...", IdCuenta)
            Dim dtp As DataTable = bl.rptPartidasLibroAuxiliarMayorExcel(IdCuenta, deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)

            'se genera el detalle del auxiliar si tiene saldo anterior o si hay movimientos en el período
            If dt.Rows(i).Item("SaldoAnterior") <> 0 Or dtp.Rows.Count > 0 Then
                GenerarDetalle(oApp, dt.Rows(i), dtp, Linea)
            End If

            '-- verifico si se viene una nueva cuenta de mayor o si cambia
            Dim IdMayorSiguiente As String = ""
            Try
                IdMayorSiguiente = dt.Rows(i + 1).Item("IdCuentaMayor")
            Catch ex As Exception
                'nothing todo... 
            End Try
            If dt.Rows(i).Item("IdCuentaMayor") <> IdMayorSiguiente Then

                oApp.Cells(Linea, 3).Value = String.Format("TOTAL: {0}, {1}", dt.Rows(i).Item("IdCuentaMayor"), dt.Rows(i).Item("NombreMayor"))
                oApp.Cells(Linea, 4).Value = TotalDebeMayor
                oApp.Cells(Linea, 5).Value = TotalHaberMayor
                oApp.Cells(Linea, 6).Value = SaldoCuentaMayor

                Linea += 2

                TotalDebeMayor = 0.0
                TotalHaberMayor = 0.0
                SaldoCuentaMayor = 0.0
            End If
            i += 1
        End While

        Range = "$A$1:$F$" + CStr(Linea)
        oApp.ActiveSheet.PageSetup.PrintArea = Range
        oApp.Range("A1").Select()
        'oApp.ActiveSheet.Protect("DrawingObjects:=False, Contents:=True, Scenarios:= False")
        'oApp.Save()
        teProgreso.Visible = False
        oApp.Visible = True
        System.Threading.Thread.CurrentThread.CurrentCulture = oldCI
    End Sub
    Private Sub GenerarDetalle(ByVal oApp As Object, ByVal dr As DataRow, ByVal dtp As DataTable, ByRef Linea As Integer)

        Dim Naturaleza As String = dr.Item("Naturaleza")
        Dim SaldoAnterior As Decimal = dr.Item("SaldoAnterior")

        PonerLineaExcel(oApp, Linea)
        'se genera la estructura de cuentas de mayor
        Dim dtm As DataTable = bl.rptDetalleCuentasMayorLibroAuxiliarExcel(dr.Item("IdCuenta"), dr.Item("IdCuentaMayor"))
        If dtm.Rows.Count > 0 Then
            For Each Fila As DataRow In dtm.Rows
                oApp.Cells(Linea, 3).Value = String.Format("{0}   {1}", Fila.Item("IdCuenta"), Fila.Item("Nombre"))
                oApp.Cells(Linea, 3).WrapText = False
                Linea += 1
            Next
        End If
        oApp.Cells(Linea, 3).Value = String.Format("{0}   {1}", dr.Item("IdCuenta"), dr.Item("Nombre"))
        oApp.Cells(Linea, 3).WrapText = False
        Linea += 1
        PonerLineaExcel(oApp, Linea)

        oApp.Cells(Linea, 1).Value = "No.Partida"
        oApp.Cells(Linea, 2).Value = "Fecha"
        oApp.Cells(Linea, 3).Value = "Concepto de la transacción"
        oApp.Cells(Linea, 4).Value = "Debe"
        oApp.Cells(Linea, 5).Value = "Haber"
        oApp.Cells(Linea, 6).Value = "Saldo"
        Linea += 1
        PonerLineaExcel(oApp, Linea)
        oApp.Cells(Linea, 4).Value = "Saldo Anterior ==>"
        oApp.Cells(Linea, 6).Value = SaldoAnterior
        'oApp.Cells(nLin,6).Style = "Comma"
        Linea += 1

        'recoriendo el detalle de partidas
        TotalDebeCuenta = 0.0
        TotalHaberCuenta = 0.0
        For Each Fila As DataRow In dtp.Rows
            With Fila
                oApp.Cells(Linea, 1).Value = String.Format("'{0}-{1}", .Item("IdTipo"), .Item("Numero"))
                oApp.Cells(Linea, 2).Value = "'" & .Item("Fecha")
                oApp.Cells(Linea, 3).Value = .Item("Concepto")

                If .Item("Debe") <> 0.0 Then
                    oApp.Cells(Linea, 4).Value = .Item("Debe")
                Else
                    oApp.Cells(Linea, 5).Value = .Item("Haber")
                End If
                'actualizo el saldo de la cuenta
                If Naturaleza = "A" Then 'es una cuenta de activo o gasto 
                    SaldoAnterior += .Item("Debe") - .Item("Haber")
                Else
                    SaldoAnterior += .Item("Haber") - .Item("Debe")
                End If

                oApp.Cells(Linea, 6).Value = SaldoAnterior

                TotalDebeCuenta += .Item("Debe")
                TotalDebeMayor += .Item("Debe")
                TotalDebeGeneral += .Item("Debe")

                TotalHaberCuenta += .Item("Haber")
                TotalHaberMayor += .Item("Haber")
                TotalHaberGeneral += .Item("Haber")

            End With
            Linea += 1
        Next
        SaldoCuentaMayor += SaldoAnterior

        Dim Range As String = String.Format("D{0}:F{0}", CStr(Linea))
        oApp.Cells(6, 26).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
        oApp.Cells(Linea, 3).Value = Space(40) + "Totales cuenta:"
        oApp.Cells(Linea, 4).Value = TotalDebeCuenta
        oApp.Cells(Linea, 5).Value = TotalHaberCuenta
        oApp.Cells(Linea, 6).Value = SaldoAnterior
        Linea += 2
    End Sub
    Private Sub PonerLineaExcel(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("A{0}:F{0}", CStr(Linea))
        oApp.Cells(6, 26).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub

    Private Sub btExportXLS_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btExportXLS.Click
        ExportarLibroExcel()
    End Sub
End Class
