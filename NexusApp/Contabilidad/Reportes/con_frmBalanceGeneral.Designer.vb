﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmBalanceGeneral
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.opgNivel = New DevExpress.XtraEditors.RadioGroup
        Me.opgTipo = New DevExpress.XtraEditors.RadioGroup
        Me.seEjercicio = New DevExpress.XtraEditors.SpinEdit
        Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit
        Me.ceDateInfo = New DevExpress.XtraEditors.CheckEdit
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opgNivel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opgTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceDateInfo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.ceDateInfo)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.opgNivel)
        Me.GroupControl1.Controls.Add(Me.opgTipo)
        Me.GroupControl1.Controls.Add(Me.seEjercicio)
        Me.GroupControl1.Controls.Add(Me.meMes)
        Me.GroupControl1.Size = New System.Drawing.Size(621, 289)
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "BALANCE GENERAL"
        Me.teTitulo.Location = New System.Drawing.Point(198, 167)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(341, 20)
        Me.teTitulo.TabIndex = 5
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(105, 141)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl4.TabIndex = 56
        Me.LabelControl4.Text = "Nivel de impresión:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(109, 170)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl3.TabIndex = 55
        Me.LabelControl3.Text = "Título del informe:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(74, 87)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(121, 13)
        Me.LabelControl2.TabIndex = 54
        Me.LabelControl2.Text = "Tipo de saldos a mostrar:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(16, 62)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(179, 13)
        Me.LabelControl1.TabIndex = 53
        Me.LabelControl1.Text = "Mes y Ejercicio para generar informe:"
        '
        'opgNivel
        '
        Me.opgNivel.EditValue = 1
        Me.opgNivel.Location = New System.Drawing.Point(198, 136)
        Me.opgNivel.Name = "opgNivel"
        Me.opgNivel.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Mayor"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Detalle")})
        Me.opgNivel.Size = New System.Drawing.Size(188, 24)
        Me.opgNivel.TabIndex = 4
        '
        'opgTipo
        '
        Me.opgTipo.EditValue = 1
        Me.opgTipo.Location = New System.Drawing.Point(198, 84)
        Me.opgTipo.Name = "opgTipo"
        Me.opgTipo.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Basado en saldos acumulados"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Basado en saldos del mes")})
        Me.opgTipo.Size = New System.Drawing.Size(188, 43)
        Me.opgTipo.TabIndex = 3
        '
        'seEjercicio
        '
        Me.seEjercicio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seEjercicio.Location = New System.Drawing.Point(302, 59)
        Me.seEjercicio.Name = "seEjercicio"
        Me.seEjercicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seEjercicio.Properties.Mask.EditMask = "f0"
        Me.seEjercicio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seEjercicio.Size = New System.Drawing.Size(73, 20)
        Me.seEjercicio.TabIndex = 2
        '
        'meMes
        '
        Me.meMes.Location = New System.Drawing.Point(198, 59)
        Me.meMes.Name = "meMes"
        Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes.Size = New System.Drawing.Size(100, 20)
        Me.meMes.TabIndex = 1
        '
        'ceDateInfo
        '
        Me.ceDateInfo.Location = New System.Drawing.Point(196, 210)
        Me.ceDateInfo.Name = "ceDateInfo"
        Me.ceDateInfo.Properties.Caption = "Incluir la fecha y hora de impresión"
        Me.ceDateInfo.Size = New System.Drawing.Size(226, 19)
        Me.ceDateInfo.TabIndex = 57
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(198, 36)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(341, 20)
        Me.leSucursal.TabIndex = 0
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(152, 39)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl7.TabIndex = 60
        Me.LabelControl7.Text = "Sucursal:"
        '
        'con_frmBalanceGeneral
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(621, 314)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmBalanceGeneral"
        Me.Text = "Balance General"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opgNivel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opgTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceDateInfo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents opgNivel As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents opgTipo As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents seEjercicio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents ceDateInfo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl

End Class
