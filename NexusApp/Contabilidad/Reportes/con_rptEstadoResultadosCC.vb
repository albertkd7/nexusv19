Public Class con_rptEstadoResultadosCC
    Dim SaldoRes As Decimal = 0.0


    Private Sub Detail_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Detail.BeforePrint
        If GetCurrentColumnValue("Naturaleza") = 5 Then
            SaldoRes += GetCurrentColumnValue("Saldo")
        Else
            SaldoRes -= GetCurrentColumnValue("Saldo")
        End If

    End Sub

    Private Sub xrlTotalDebe_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlTotalDebe.BeforePrint
        xrlTotalDebe.Text = Format(SaldoRes, "##,###,##0.00")
    End Sub

    Private Sub GroupHeader3_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles GroupHeader3.BeforePrint
        SaldoRes = 0
    End Sub
End Class