﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class con_rptEstadosResultadosNIIF
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.XrLine4 = New DevExpress.XtraReports.UI.XRLine
        Me.XrActual = New DevExpress.XtraReports.UI.XRLabel
        Me.XrAnterior = New DevExpress.XtraReports.UI.XRLabel
        Me.xrpFecha = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xrlPage = New DevExpress.XtraReports.UI.XRLabel
        Me.xrpNumero = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xrlDescMoneda = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlPeriodo = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTitulo = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlEmpresa = New DevExpress.XtraReports.UI.XRLabel
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
        Me.DsContabilidad1 = New Nexus.dsContabilidad
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
        Me.xrlResultado2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine5 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlResultado1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlResultado = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCargo4 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCargo5 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFirmante4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine10 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlFirmante5 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine12 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlCargo6 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine11 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlFirmante6 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCargo1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFirmante2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine8 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLine7 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlFirmante3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine9 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlCargo2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCargo3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFirmante1 = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
        Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
        CType(Me.DsContabilidad1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine4, Me.XrActual, Me.XrAnterior, Me.xrpFecha, Me.xrlPage, Me.xrpNumero, Me.xrlDescMoneda, Me.xrlPeriodo, Me.xrlTitulo, Me.xrlEmpresa})
        Me.PageHeader.HeightF = 148.7083!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLine4
        '
        Me.XrLine4.LocationFloat = New DevExpress.Utils.PointFloat(521.7499!, 145.7083!)
        Me.XrLine4.Name = "XrLine4"
        Me.XrLine4.SizeF = New System.Drawing.SizeF(199.0!, 3.0!)
        '
        'XrActual
        '
        Me.XrActual.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrActual.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrActual.LocationFloat = New DevExpress.Utils.PointFloat(540.8749!, 129.7083!)
        Me.XrActual.Name = "XrActual"
        Me.XrActual.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrActual.SizeF = New System.Drawing.SizeF(59.0!, 16.0!)
        Me.XrActual.StylePriority.UseBorders = False
        Me.XrActual.StylePriority.UseFont = False
        Me.XrActual.StylePriority.UseTextAlignment = False
        Me.XrActual.Text = "Actual"
        Me.XrActual.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrAnterior
        '
        Me.XrAnterior.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrAnterior.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrAnterior.LocationFloat = New DevExpress.Utils.PointFloat(641.0!, 129.7083!)
        Me.XrAnterior.Name = "XrAnterior"
        Me.XrAnterior.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrAnterior.SizeF = New System.Drawing.SizeF(61.0!, 16.0!)
        Me.XrAnterior.StylePriority.UseBorders = False
        Me.XrAnterior.StylePriority.UseFont = False
        Me.XrAnterior.StylePriority.UseTextAlignment = False
        Me.XrAnterior.Text = "Anterior"
        Me.XrAnterior.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'xrpFecha
        '
        Me.xrpFecha.Format = "{0:dd/MM/yyyy hh:mm tt}"
        Me.xrpFecha.LocationFloat = New DevExpress.Utils.PointFloat(641.0!, 0.0!)
        Me.xrpFecha.Name = "xrpFecha"
        Me.xrpFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpFecha.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.xrpFecha.SizeF = New System.Drawing.SizeF(125.0!, 16.0!)
        '
        'xrlPage
        '
        Me.xrlPage.LocationFloat = New DevExpress.Utils.PointFloat(509.0!, 0.0!)
        Me.xrlPage.Name = "xrlPage"
        Me.xrlPage.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPage.SizeF = New System.Drawing.SizeF(56.0!, 16.0!)
        Me.xrlPage.Text = "Pág. No."
        '
        'xrpNumero
        '
        Me.xrpNumero.LocationFloat = New DevExpress.Utils.PointFloat(569.0!, 0.0!)
        Me.xrpNumero.Name = "xrpNumero"
        Me.xrpNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpNumero.SizeF = New System.Drawing.SizeF(66.0!, 16.0!)
        '
        'xrlDescMoneda
        '
        Me.xrlDescMoneda.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlDescMoneda.LocationFloat = New DevExpress.Utils.PointFloat(22.0!, 84.0!)
        Me.xrlDescMoneda.Name = "xrlDescMoneda"
        Me.xrlDescMoneda.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDescMoneda.SizeF = New System.Drawing.SizeF(747.0!, 19.0!)
        Me.xrlDescMoneda.StylePriority.UseFont = False
        Me.xrlDescMoneda.StylePriority.UseTextAlignment = False
        Me.xrlDescMoneda.Text = "xrlDescMoneda"
        Me.xrlDescMoneda.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlPeriodo
        '
        Me.xrlPeriodo.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.xrlPeriodo.LocationFloat = New DevExpress.Utils.PointFloat(22.0!, 63.0!)
        Me.xrlPeriodo.Name = "xrlPeriodo"
        Me.xrlPeriodo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPeriodo.SizeF = New System.Drawing.SizeF(747.0!, 19.0!)
        Me.xrlPeriodo.StylePriority.UseFont = False
        Me.xrlPeriodo.StylePriority.UseTextAlignment = False
        Me.xrlPeriodo.Text = "xrlPeriodo"
        Me.xrlPeriodo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlTitulo
        '
        Me.xrlTitulo.Font = New System.Drawing.Font("Arial", 11.0!)
        Me.xrlTitulo.LocationFloat = New DevExpress.Utils.PointFloat(22.0!, 41.0!)
        Me.xrlTitulo.Name = "xrlTitulo"
        Me.xrlTitulo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTitulo.SizeF = New System.Drawing.SizeF(747.0!, 19.0!)
        Me.xrlTitulo.StylePriority.UseFont = False
        Me.xrlTitulo.StylePriority.UseTextAlignment = False
        Me.xrlTitulo.Text = "xrlTitulo"
        Me.xrlTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlEmpresa
        '
        Me.xrlEmpresa.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.xrlEmpresa.LocationFloat = New DevExpress.Utils.PointFloat(22.0!, 19.0!)
        Me.xrlEmpresa.Name = "xrlEmpresa"
        Me.xrlEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlEmpresa.SizeF = New System.Drawing.SizeF(747.0!, 22.0!)
        Me.xrlEmpresa.StylePriority.UseFont = False
        Me.xrlEmpresa.StylePriority.UseTextAlignment = False
        Me.xrlEmpresa.Text = "xrlEmpresa"
        Me.xrlEmpresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.HeightF = 40.0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultadoNIIF.NombreNIIF")})
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 3.000005!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(459.0833!, 15.99999!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.Text = "XrLabel2"
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel5, Me.XrLabel2, Me.XrLine2})
        Me.Detail.HeightF = 19.87502!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel6
        '
        Me.XrLabel6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultadoNIIF.Actual", "{0:n2}")})
        Me.XrLabel6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(494.2083!, 3.000005!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(110.6666!, 16.0!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        XrSummary1.FormatString = "{0:n2}"
        Me.XrLabel6.Summary = XrSummary1
        Me.XrLabel6.Text = "XrLabel6"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel5
        '
        Me.XrLabel5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultadoNIIF.Anterior", "{0:n2}")})
        Me.XrLabel5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(612.3334!, 3.0!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(110.6666!, 16.0!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "XrLabel5"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLine2
        '
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(462.7499!, 0.0!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(261.7085!, 2.874994!)
        Me.XrLine2.Visible = False
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.HeightF = 40.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'DsContabilidad1
        '
        Me.DsContabilidad1.DataSetName = "dsContabilidad"
        Me.DsContabilidad1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlResultado2, Me.XrLine5, Me.xrlResultado1, Me.XrLabel11, Me.XrLine3, Me.xrlResultado, Me.xrlCargo4, Me.xrlCargo5, Me.xrlFirmante4, Me.XrLine10, Me.xrlFirmante5, Me.XrLine12, Me.xrlCargo6, Me.XrLine11, Me.xrlFirmante6, Me.xrlCargo1, Me.xrlFirmante2, Me.XrLine8, Me.XrLine7, Me.xrlFirmante3, Me.XrLine9, Me.xrlCargo2, Me.xrlCargo3, Me.xrlFirmante1})
        Me.ReportFooter.HeightF = 328.375!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'xrlResultado2
        '
        Me.xrlResultado2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultadosNIFF.Actual")})
        Me.xrlResultado2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlResultado2.LocationFloat = New DevExpress.Utils.PointFloat(494.2083!, 13.00001!)
        Me.xrlResultado2.Name = "xrlResultado2"
        Me.xrlResultado2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlResultado2.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.xrlResultado2.SizeF = New System.Drawing.SizeF(110.6666!, 16.0!)
        Me.xrlResultado2.StylePriority.UseFont = False
        Me.xrlResultado2.StylePriority.UseTextAlignment = False
        XrSummary2.FormatString = "{0:n2}"
        XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
        Me.xrlResultado2.Summary = XrSummary2
        Me.xrlResultado2.Text = "xrlResultado2"
        Me.xrlResultado2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLine5
        '
        Me.XrLine5.LocationFloat = New DevExpress.Utils.PointFloat(269.8333!, 29.00001!)
        Me.XrLine5.Name = "XrLine5"
        Me.XrLine5.SizeF = New System.Drawing.SizeF(454.625!, 2.0!)
        '
        'xrlResultado1
        '
        Me.xrlResultado1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultadosNIFF.Anterior")})
        Me.xrlResultado1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlResultado1.LocationFloat = New DevExpress.Utils.PointFloat(612.3334!, 12.99998!)
        Me.xrlResultado1.Name = "xrlResultado1"
        Me.xrlResultado1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlResultado1.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.xrlResultado1.SizeF = New System.Drawing.SizeF(110.6666!, 16.0!)
        Me.xrlResultado1.StylePriority.UseFont = False
        Me.xrlResultado1.StylePriority.UseTextAlignment = False
        XrSummary3.FormatString = "{0:n2}"
        XrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom
        XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
        Me.xrlResultado1.Summary = XrSummary3
        Me.xrlResultado1.Text = "xrlResultado1"
        Me.xrlResultado1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel11
        '
        Me.XrLabel11.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(269.8334!, 12.00001!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(43.99997!, 17.0!)
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.Text = "Total:"
        '
        'XrLine3
        '
        Me.XrLine3.LocationFloat = New DevExpress.Utils.PointFloat(269.8334!, 7.999979!)
        Me.XrLine3.Name = "XrLine3"
        Me.XrLine3.SizeF = New System.Drawing.SizeF(454.625!, 2.0!)
        '
        'xrlResultado
        '
        Me.xrlResultado.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlResultado.LocationFloat = New DevExpress.Utils.PointFloat(313.8334!, 12.00001!)
        Me.xrlResultado.Name = "xrlResultado"
        Me.xrlResultado.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlResultado.SizeF = New System.Drawing.SizeF(150.25!, 17.0!)
        Me.xrlResultado.StylePriority.UseFont = False
        Me.xrlResultado.Text = "EXEDENTE/DEFICIT"
        '
        'xrlCargo4
        '
        Me.xrlCargo4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo4.LocationFloat = New DevExpress.Utils.PointFloat(516.0!, 216.0!)
        Me.xrlCargo4.Name = "xrlCargo4"
        Me.xrlCargo4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo4.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo4.StylePriority.UseFont = False
        Me.xrlCargo4.StylePriority.UseTextAlignment = False
        Me.xrlCargo4.Text = "xrlCargo4"
        Me.xrlCargo4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo5
        '
        Me.xrlCargo5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo5.LocationFloat = New DevExpress.Utils.PointFloat(42.0!, 314.0!)
        Me.xrlCargo5.Name = "xrlCargo5"
        Me.xrlCargo5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo5.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo5.StylePriority.UseFont = False
        Me.xrlCargo5.StylePriority.UseTextAlignment = False
        Me.xrlCargo5.Text = "xrlCargo5"
        Me.xrlCargo5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlFirmante4
        '
        Me.xrlFirmante4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante4.LocationFloat = New DevExpress.Utils.PointFloat(516.0!, 200.0!)
        Me.xrlFirmante4.Name = "xrlFirmante4"
        Me.xrlFirmante4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante4.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante4.StylePriority.UseFont = False
        Me.xrlFirmante4.StylePriority.UseTextAlignment = False
        Me.xrlFirmante4.Text = "xrlFirmante4"
        Me.xrlFirmante4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine10
        '
        Me.XrLine10.LocationFloat = New DevExpress.Utils.PointFloat(516.0!, 194.0!)
        Me.XrLine10.Name = "XrLine10"
        Me.XrLine10.SizeF = New System.Drawing.SizeF(222.0!, 3.0!)
        '
        'xrlFirmante5
        '
        Me.xrlFirmante5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante5.LocationFloat = New DevExpress.Utils.PointFloat(42.0!, 298.0!)
        Me.xrlFirmante5.Name = "xrlFirmante5"
        Me.xrlFirmante5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante5.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante5.StylePriority.UseFont = False
        Me.xrlFirmante5.StylePriority.UseTextAlignment = False
        Me.xrlFirmante5.Text = "xrlFirmante5"
        Me.xrlFirmante5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine12
        '
        Me.XrLine12.LocationFloat = New DevExpress.Utils.PointFloat(520.0!, 290.0!)
        Me.XrLine12.Name = "XrLine12"
        Me.XrLine12.SizeF = New System.Drawing.SizeF(222.0!, 3.0!)
        '
        'xrlCargo6
        '
        Me.xrlCargo6.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo6.LocationFloat = New DevExpress.Utils.PointFloat(520.0!, 314.0!)
        Me.xrlCargo6.Name = "xrlCargo6"
        Me.xrlCargo6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo6.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo6.StylePriority.UseFont = False
        Me.xrlCargo6.StylePriority.UseTextAlignment = False
        Me.xrlCargo6.Text = "xrlCargo6"
        Me.xrlCargo6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine11
        '
        Me.XrLine11.LocationFloat = New DevExpress.Utils.PointFloat(42.0!, 290.0!)
        Me.XrLine11.Name = "XrLine11"
        Me.XrLine11.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlFirmante6
        '
        Me.xrlFirmante6.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante6.LocationFloat = New DevExpress.Utils.PointFloat(520.0!, 298.0!)
        Me.xrlFirmante6.Name = "xrlFirmante6"
        Me.xrlFirmante6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante6.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante6.StylePriority.UseFont = False
        Me.xrlFirmante6.StylePriority.UseTextAlignment = False
        Me.xrlFirmante6.Text = "xrlFirmante6"
        Me.xrlFirmante6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo1
        '
        Me.xrlCargo1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo1.LocationFloat = New DevExpress.Utils.PointFloat(42.0!, 116.0!)
        Me.xrlCargo1.Name = "xrlCargo1"
        Me.xrlCargo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo1.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo1.StylePriority.UseFont = False
        Me.xrlCargo1.StylePriority.UseTextAlignment = False
        Me.xrlCargo1.Text = "xrlCargo1"
        Me.xrlCargo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlFirmante2
        '
        Me.xrlFirmante2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante2.LocationFloat = New DevExpress.Utils.PointFloat(520.0!, 100.0!)
        Me.xrlFirmante2.Name = "xrlFirmante2"
        Me.xrlFirmante2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante2.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante2.StylePriority.UseFont = False
        Me.xrlFirmante2.StylePriority.UseTextAlignment = False
        Me.xrlFirmante2.Text = "xrlFirmante2"
        Me.xrlFirmante2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine8
        '
        Me.XrLine8.LocationFloat = New DevExpress.Utils.PointFloat(520.0!, 94.0!)
        Me.XrLine8.Name = "XrLine8"
        Me.XrLine8.SizeF = New System.Drawing.SizeF(222.0!, 3.0!)
        '
        'XrLine7
        '
        Me.XrLine7.LocationFloat = New DevExpress.Utils.PointFloat(42.0!, 94.0!)
        Me.XrLine7.Name = "XrLine7"
        Me.XrLine7.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlFirmante3
        '
        Me.xrlFirmante3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante3.LocationFloat = New DevExpress.Utils.PointFloat(38.0!, 200.0!)
        Me.xrlFirmante3.Name = "xrlFirmante3"
        Me.xrlFirmante3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante3.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante3.StylePriority.UseFont = False
        Me.xrlFirmante3.StylePriority.UseTextAlignment = False
        Me.xrlFirmante3.Text = "xrlFirmante3"
        Me.xrlFirmante3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine9
        '
        Me.XrLine9.LocationFloat = New DevExpress.Utils.PointFloat(38.0!, 194.0!)
        Me.XrLine9.Name = "XrLine9"
        Me.XrLine9.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlCargo2
        '
        Me.xrlCargo2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo2.LocationFloat = New DevExpress.Utils.PointFloat(520.0!, 116.0!)
        Me.xrlCargo2.Name = "xrlCargo2"
        Me.xrlCargo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo2.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo2.StylePriority.UseFont = False
        Me.xrlCargo2.StylePriority.UseTextAlignment = False
        Me.xrlCargo2.Text = "xrlCargo2"
        Me.xrlCargo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo3
        '
        Me.xrlCargo3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo3.LocationFloat = New DevExpress.Utils.PointFloat(38.0!, 216.0!)
        Me.xrlCargo3.Name = "xrlCargo3"
        Me.xrlCargo3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo3.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo3.StylePriority.UseFont = False
        Me.xrlCargo3.StylePriority.UseTextAlignment = False
        Me.xrlCargo3.Text = "xrlCargo3"
        Me.xrlCargo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlFirmante1
        '
        Me.xrlFirmante1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante1.LocationFloat = New DevExpress.Utils.PointFloat(42.0!, 100.0!)
        Me.xrlFirmante1.Name = "xrlFirmante1"
        Me.xrlFirmante1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante1.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante1.StylePriority.UseFont = False
        Me.xrlFirmante1.StylePriority.UseTextAlignment = False
        Me.xrlFirmante1.Text = "xrlFirmante1"
        Me.xrlFirmante1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
        Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("NumMayor", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader1.HeightF = 21.79168!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultadosNIFF.NombreMayor")})
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(18.0!, 1.000007!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(327.3333!, 17.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.Text = "XrLabel1"
        '
        'XrLabel3
        '
        Me.XrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultadosNIFF.Titulo")})
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.9999981!, 0.0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(327.3333!, 17.0!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.Text = "XrLabel3"
        '
        'GroupFooter1
        '
        Me.GroupFooter1.HeightF = 0.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'XrLabel8
        '
        Me.XrLabel8.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultadoNIIF.Anterior")})
        Me.XrLabel8.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(612.3334!, 6.000007!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(110.6666!, 19.0!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        XrSummary4.FormatString = "{0:n2}"
        XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel8.Summary = XrSummary4
        Me.XrLabel8.Text = "XrLabel8"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel7
        '
        Me.XrLabel7.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(269.8334!, 6.0!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(43.99997!, 17.0!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.Text = "Total:"
        '
        'XrLabel4
        '
        Me.XrLabel4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultadosNIFF.Titulo")})
        Me.XrLabel4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(313.8334!, 6.0!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(150.25!, 17.0!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.Text = "XrLabel3"
        '
        'XrLine1
        '
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(269.8334!, 4.0!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(454.625!, 2.0!)
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3})
        Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("Tipo", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader2.HeightF = 22.91667!
        Me.GroupHeader2.Level = 1
        Me.GroupHeader2.Name = "GroupHeader2"
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel4, Me.XrLine1, Me.XrLabel7, Me.XrLabel8})
        Me.GroupFooter2.HeightF = 29.00001!
        Me.GroupFooter2.Level = 1
        Me.GroupFooter2.Name = "GroupFooter2"
        '
        'XrLabel9
        '
        Me.XrLabel9.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultadoNIIF.Actual")})
        Me.XrLabel9.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(494.2083!, 6.000007!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(110.6666!, 19.0!)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        XrSummary5.FormatString = "{0:n2}"
        XrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel9.Summary = XrSummary5
        Me.XrLabel9.Text = "XrLabel9"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'con_rptEstadosResultadosNIIF
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter, Me.GroupHeader1, Me.GroupFooter1, Me.GroupHeader2, Me.GroupFooter2})
        Me.DataMember = "EstadoResultadosNIFF"
        Me.DataSource = Me.DsContabilidad1
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(35, 35, 40, 40)
        Me.SnapGridSize = 2.0!
        Me.SnapToGrid = False
        Me.Version = "11.1"
        CType(Me.DsContabilidad1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents xrpFecha As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents xrlPage As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrpNumero As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents xrlDescMoneda As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPeriodo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTitulo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlEmpresa As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents DsContabilidad1 As Nexus.dsContabilidad
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents xrlCargo4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFirmante4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine10 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine12 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlCargo6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine11 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFirmante2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine8 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine7 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine9 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlCargo2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlResultado1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlResultado As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrActual As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrAnterior As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine4 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlResultado2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
End Class
