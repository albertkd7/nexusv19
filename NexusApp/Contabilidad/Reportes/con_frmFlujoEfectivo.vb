﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Views.Grid
Public Class con_frmFlujoEfectivo
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Dim entDetalle As con_RubroFlujoDetalle
    Dim dt As DataTable

    Private Sub con_frmFlujoEfectivo_Guardar() Handles Me.Guardar
        Dim vista As GridView = gc.FocusedView
        For i = 0 To vista.RowCount - 1
            entDetalle = New con_RubroFlujoDetalle
            entDetalle.IdCuentaMayor = vista.GetRowCellValue(i, "IdCuenta")
            entDetalle.Concepto = vista.GetRowCellValue(i, "Concepto")
            entDetalle.IdRubro = vista.GetRowCellValue(i, "IdRubro")
            vista.FocusedRowHandle = i
            vista.FocusedColumn = vista.Columns("EsFlujo")
            If vista.FocusedValue Then
                objTablas.con_RubroFlujoDetalleUpdate(entDetalle)
            End If
        Next
    End Sub


    Private Sub frmLoad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dt = bl.con_CargarCuentasDeMayor()
        gc.DataSource = dt
        objCombos.con_CargarRubroFlujoEfectivo(ActividadLookUpEdit2)
    End Sub

    Private Sub EsFlujoCheckEdit2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles EsFlujoCheckEdit2.CheckedChanged
        entDetalle = New con_RubroFlujoDetalle
        entDetalle.IdCuentaMayor = gv.GetRowCellValue(gv.FocusedRowHandle, "IdCuenta")
        entDetalle.Concepto = gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto")
        entDetalle.IdRubro = gv.GetRowCellValue(gv.FocusedRowHandle, "IdRubro")
        gv.PostEditor()
        If gv.GetDataRow(gv.FocusedRowHandle)(gv.FocusedColumn.FieldName) Then
            objTablas.con_RubroFlujoDetalleInsert(entDetalle)
        Else
            objTablas.con_RubroFlujoDetalleDeleteByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCuenta"))
        End If
    End Sub
End Class
