Public Class con_rptAnexos
    Private dTotal As Decimal = 0

    Private Sub xrlTotal_SummaryGetResult(ByVal sender As System.Object, ByVal e As DevExpress.XtraReports.UI.SummaryGetResultEventArgs) Handles xrlTotal.SummaryGetResult
        e.Result = dTotal
        e.Handled = True
    End Sub

    Private Sub xrlTotal_SummaryReset(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles xrlTotal.SummaryReset
        dTotal = 0
    End Sub

    Private Sub xrlTotal_SummaryRowChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles xrlTotal.SummaryRowChanged
        If GetCurrentColumnValue("Nivel") = 1 Then
            dTotal += SiEsNulo(GetCurrentColumnValue("SaldoNivel1"), 0)
        End If
    End Sub
    Private Sub xrlTituloTotal_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlTituloTotal.BeforePrint
        If GetCurrentColumnValue("Tipo") = "A" Then
            xrlTituloTotal.Text = "TOTAL DE ACTIVOS"
        Else
            xrlTituloTotal.Text = "TOTAL DE PASIVO Y CAPITAL"
        End If
    End Sub
    Private Sub XrLine1_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLine1.BeforePrint
        If GetNextColumnValue("Nivel") = GetCurrentColumnValue("Nivel") Then
            e.Cancel = True
        End If
        If SiEsNulo(GetCurrentColumnValue("SaldoNivel4"), 0) = 0 Then
            e.Cancel = True
        End If
    End Sub
End Class