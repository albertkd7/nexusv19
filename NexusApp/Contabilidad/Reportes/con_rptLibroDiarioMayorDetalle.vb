Public Class con_rptLibroDiarioMayorDetalle
    Dim Saldo As Double = 0.0
    Private Sub XrLabel14_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel14.BeforePrint
        If SiEsNulo(GetCurrentColumnValue("Haber"), 0) = 0 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub XrLabel13_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel13.BeforePrint
        If SiEsNulo(GetCurrentColumnValue("Debe"), 0) = 0 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub xrlSaldo_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldo.BeforePrint
        If GetCurrentColumnValue("Naturaleza") = 1 Or GetCurrentColumnValue("Naturaleza") = 4 Then
            Saldo += Convert.ToDouble(GetCurrentColumnValue("Debe")) - Convert.ToDouble(GetCurrentColumnValue("Haber"))
            xrlSaldo.Text = String.Format("{0:n2}", Saldo)
        Else
            Saldo += Convert.ToDouble(GetCurrentColumnValue("Haber")) - Convert.ToDouble(GetCurrentColumnValue("Debe"))
            xrlSaldo.Text = String.Format("{0:n2}", Saldo)
        End If
    End Sub

    Private Sub xrlSaldoAnt_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldoAnt.BeforePrint
        Saldo = Convert.ToDouble(GetCurrentColumnValue("SaldoInicial"))
    End Sub


End Class