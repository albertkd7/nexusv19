﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmLibroDiarioMayor
    Inherits gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(con_frmLibroDiarioMayor))
        Me.seNumero = New DevExpress.XtraEditors.SpinEdit()
        Me.ceNumerar = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.deFin = New DevExpress.XtraEditors.DateEdit()
        Me.deIni = New DevExpress.XtraEditors.DateEdit()
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit()
        Me.rgFormato = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.ceDateInfo = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.seNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceNumerar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFin.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deIni.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deIni.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgFormato.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceDateInfo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.rgFormato)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.seNumero)
        Me.GroupControl1.Controls.Add(Me.ceDateInfo)
        Me.GroupControl1.Controls.Add(Me.ceNumerar)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.deFin)
        Me.GroupControl1.Controls.Add(Me.deIni)
        Me.GroupControl1.Size = New System.Drawing.Size(690, 357)
        '
        'seNumero
        '
        Me.seNumero.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.seNumero.Location = New System.Drawing.Point(161, 216)
        Me.seNumero.Name = "seNumero"
        Me.seNumero.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seNumero.Properties.Mask.EditMask = "f0"
        Me.seNumero.Size = New System.Drawing.Size(63, 20)
        Me.seNumero.TabIndex = 5
        '
        'ceNumerar
        '
        Me.ceNumerar.Location = New System.Drawing.Point(161, 249)
        Me.ceNumerar.Name = "ceNumerar"
        Me.ceNumerar.Properties.Caption = "Númerar las hojas del libro"
        Me.ceNumerar.Size = New System.Drawing.Size(158, 19)
        Me.ceNumerar.TabIndex = 4
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(47, 219)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(111, 13)
        Me.LabelControl4.TabIndex = 35
        Me.LabelControl4.Text = "Número inicial del Folio:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(72, 130)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl3.TabIndex = 36
        Me.LabelControl3.Text = "Título del reporte:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(94, 105)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 34
        Me.LabelControl2.Text = "Hasta Fecha:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(92, 81)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl1.TabIndex = 33
        Me.LabelControl1.Text = "Desde Fecha:"
        '
        'deFin
        '
        Me.deFin.EditValue = New Date(2008, 1, 5, 0, 0, 0, 0)
        Me.deFin.EnterMoveNextControl = True
        Me.deFin.Location = New System.Drawing.Point(161, 102)
        Me.deFin.Name = "deFin"
        Me.deFin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFin.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFin.Size = New System.Drawing.Size(88, 20)
        Me.deFin.TabIndex = 1
        '
        'deIni
        '
        Me.deIni.EditValue = New Date(2008, 1, 1, 0, 0, 0, 0)
        Me.deIni.EnterMoveNextControl = True
        Me.deIni.Location = New System.Drawing.Point(161, 78)
        Me.deIni.Name = "deIni"
        Me.deIni.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deIni.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deIni.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deIni.Size = New System.Drawing.Size(88, 20)
        Me.deIni.TabIndex = 0
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "Libro Diario Mayor"
        Me.teTitulo.EnterMoveNextControl = True
        Me.teTitulo.Location = New System.Drawing.Point(161, 127)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(281, 20)
        Me.teTitulo.TabIndex = 3
        '
        'rgFormato
        '
        Me.rgFormato.EditValue = 1
        Me.rgFormato.Location = New System.Drawing.Point(161, 158)
        Me.rgFormato.Name = "rgFormato"
        Me.rgFormato.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Con Detalle de Transacciones"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Consolidado por Tipo de partida")})
        Me.rgFormato.Size = New System.Drawing.Size(390, 30)
        Me.rgFormato.TabIndex = 4
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(71, 166)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl5.TabIndex = 39
        Me.LabelControl5.Text = "Formato del Libro:"
        '
        'ceDateInfo
        '
        Me.ceDateInfo.Location = New System.Drawing.Point(161, 275)
        Me.ceDateInfo.Name = "ceDateInfo"
        Me.ceDateInfo.Properties.Caption = "Incluir la fecha y hora de impresión"
        Me.ceDateInfo.Size = New System.Drawing.Size(226, 19)
        Me.ceDateInfo.TabIndex = 4
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(113, 51)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl6.TabIndex = 41
        Me.LabelControl6.Text = "Sucursal:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(161, 48)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(390, 20)
        Me.leSucursal.TabIndex = 2
        '
        'con_frmLibroDiarioMayor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(690, 382)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmLibroDiarioMayor"
        Me.Text = "Libro Diario Mayor"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.seNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceNumerar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFin.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deIni.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deIni.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgFormato.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceDateInfo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents seNumero As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents ceNumerar As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents deIni As DevExpress.XtraEditors.DateEdit
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents rgFormato As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceDateInfo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
End Class
