Public Class con_rptBalanceTipoCuentaResumen
    Private TotalActivo As Decimal = 0, TotalPasivo As Decimal = 0, Niv As Integer

    Private Sub xrlLineaA_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlLineaA.BeforePrint
        e.Cancel = SiEsNulo(GetCurrentColumnValue("a_sn_linea"), 0) = 0
    End Sub
    Private Sub xrlLineaP_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlLineaP.BeforePrint
        e.Cancel = SiEsNulo(GetCurrentColumnValue("p_sn_linea"), 0) = 0
    End Sub

    Private Sub xrlSaldoNivel2a_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldoNivel2a.BeforePrint
        Niv = SiEsNulo(GetCurrentColumnValue("a_Nivel"), 1)
        e.Cancel = Niv = 1 Or GetCurrentColumnValue("a_Saldo") = 0
    End Sub
    Private Sub xrlSaldoNivel1a_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldoNivel1a.BeforePrint
        Niv = SiEsNulo(GetCurrentColumnValue("a_Nivel"), 2)
        e.Cancel = Niv = 2 Or GetCurrentColumnValue("a_Saldo") = 0
    End Sub
    
    Private Sub xrlSaldoNivel2p_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldoNivel2p.BeforePrint
        Niv = SiEsNulo(GetCurrentColumnValue("p_Nivel"), 1)
        e.Cancel = Niv = 1 Or GetCurrentColumnValue("p_Saldo") = 0 'si es de nivel 1 no se imprime, solo nivel 2
    End Sub

    Private Sub xrlSaldoNivel1p_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldoNivel1p.BeforePrint
        Niv = SiEsNulo(GetCurrentColumnValue("p_Nivel"), 2)
        e.Cancel = Niv = 2 Or GetCurrentColumnValue("p_Saldo") = 0 'si es de nivel 2 no se imprime, solo nivel 1
    End Sub

    Private Sub Detail_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Detail.BeforePrint
        If SiEsNulo(GetCurrentColumnValue("a_Nivel"), 0) = 1 Then
            TotalActivo += GetCurrentColumnValue("a_Saldo")
        End If
        If SiEsNulo(GetCurrentColumnValue("p_Nivel"), 0) = 1 Then
            TotalPasivo += GetCurrentColumnValue("p_Saldo")
        End If
    End Sub

    Private Sub xrlTotal_A_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlTotal_A.BeforePrint
        xrlTotal_A.Text = Format(TotalActivo, "###,##0.00")
    End Sub

    Private Sub xrlTotal_B_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlTotal_P.BeforePrint
        xrlTotal_P.Text = Format(TotalPasivo, "###,##0.00")
    End Sub
End Class