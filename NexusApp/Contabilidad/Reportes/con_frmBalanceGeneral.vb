﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class con_frmBalanceGeneral
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub con_frmBalanceGeneral_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "--TODAS LAS SUCURSALES")
        bl.ObtienePeriodoContable(Mes, Ejercicio)
        seEjercicio.EditValue = Ejercicio
        meMes.Month = Mes
    End Sub


    Private Sub con_frmBalanceGeneral_Report_Click() Handles Me.Reporte
        Dim dbFirmante As DataTable = blAdmon.ObtieneParametros()
        If opgNivel.Text = 1 Then
            Dim rpt As New con_rptBalanceTipoCuentaResumen() With {.DataSource = bl.con_rptBalanceGeneralResumen(meMes.Month, seEjercicio.EditValue, opgTipo.EditValue, leSucursal.EditValue), .DataMember = ""}
            With dbFirmante.Rows(0)
                rpt.xrlFirmante1.Text = .Item("NombreFirmante1")
                rpt.xrlFirmante2.Text = .Item("NombreFirmante2")
                rpt.xrlFirmante3.Text = .Item("NombreFirmante3")
                rpt.xrlFirmante4.Text = .Item("NombreFirmante4")
                rpt.xrlFirmante5.Text = .Item("NombreFirmante5")
                rpt.xrlFirmante6.Text = .Item("NombreFirmante6")
                
                rpt.xrlCargo1.Text = .Item("CargoFirmante1")
                rpt.xrlCargo2.Text = .Item("CargoFirmante2")
                rpt.xrlCargo3.Text = .Item("CargoFirmante3")
                rpt.xrlCargo4.Text = .Item("CargoFirmante4")
                rpt.xrlCargo5.Text = .Item("CargoFirmante5")
                rpt.xrlCargo6.Text = .Item("CargoFirmante6")
                rpt.xrLineaFirma1.Visible = .Item("CargoFirmante1") <> ""
                rpt.xrLineaFirma2.Visible = .Item("CargoFirmante2") <> ""
                rpt.xrLineaFirma3.Visible = .Item("CargoFirmante3") <> ""
                rpt.xrLineaFirma4.Visible = .Item("CargoFirmante4") <> ""
                rpt.xrLineaFirma5.Visible = .Item("CargoFirmante5") <> ""
                rpt.xrLineaFirma6.Visible = .Item("CargoFirmante6") <> ""
            End With
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.EditValue
            rpt.xrpFecha.Visible = ceDateInfo.Checked
            rpt.xrlPage.Visible = ceDateInfo.Checked
            rpt.xrpFecha.Visible = ceDateInfo.Checked
            If opgTipo.EditValue = 1 Then
                Dim tmpFecha As String = String.Format("01/{0}/{1}", meMes.EditValue, seEjercicio.EditValue)
                Dim Fecha As Date = CDate(tmpFecha)
                Fecha = DateAdd(DateInterval.Month, 1, Fecha)
                Fecha = DateAdd(DateInterval.Day, -1, Fecha)
                rpt.xrlPeriodo.Text = "AL " & FechaToString(Fecha, Fecha).ToUpper()
            Else
                rpt.xrlPeriodo.Text = String.Format("CORRESPONDIENTE AL MES {0} DE {1}", (meMes.Text).ToUpper(), seEjercicio.EditValue)
            End If
            rpt.xrlDescMoneda.Text = gsDesc_Moneda
            rpt.ShowPreview()
        Else
            Dim rpt As New con_rptBalanceTipoCuentaDetalle() With {.DataSource = bl.con_rptBalanceGeneralDetalle(meMes.Month, seEjercicio.EditValue, opgTipo.EditValue, leSucursal.EditValue), .DataMember = ""}
            With dbFirmante.Rows(0)
                rpt.xrlFirmante1.Text = .Item("NombreFirmante1")
                rpt.xrlFirmante2.Text = .Item("NombreFirmante2")
                rpt.xrlFirmante3.Text = .Item("NombreFirmante3")
                rpt.xrlFirmante4.Text = .Item("NombreFirmante4")
                rpt.xrlFirmante5.Text = .Item("NombreFirmante5")
                rpt.xrlFirmante6.Text = .Item("NombreFirmante6")
                rpt.xrlCargo1.Text = .Item("CargoFirmante1")
                rpt.xrlCargo2.Text = .Item("CargoFirmante2")
                rpt.xrlCargo3.Text = .Item("CargoFirmante3")
                rpt.xrlCargo4.Text = .Item("CargoFirmante4")
                rpt.xrlCargo5.Text = .Item("CargoFirmante5")
                rpt.xrlCargo6.Text = .Item("CargoFirmante6")
                rpt.xrLineaFirma1.Visible = .Item("CargoFirmante1") <> ""
                rpt.xrLineaFirma2.Visible = .Item("CargoFirmante2") <> ""
                rpt.xrLineaFirma3.Visible = .Item("CargoFirmante3") <> ""
                rpt.xrLineaFirma4.Visible = .Item("CargoFirmante4") <> ""
                rpt.xrLineaFirma5.Visible = .Item("CargoFirmante5") <> ""
                rpt.xrLineaFirma6.Visible = .Item("CargoFirmante6") <> ""
            End With
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.EditValue
            rpt.xrpFecha.Visible = ceDateInfo.Checked
            rpt.xrlPage.Visible = ceDateInfo.Checked
            rpt.xrpFecha.Visible = ceDateInfo.Checked
            If opgTipo.EditValue = 1 Then
                Dim tmpFecha As String = String.Format("01/{0}/{1}", meMes.EditValue, seEjercicio.EditValue)
                Dim Fecha As Date = CDate(tmpFecha)
                Fecha = DateAdd(DateInterval.Month, 1, Fecha)
                Fecha = DateAdd(DateInterval.Day, -1, Fecha)
                rpt.xrlPeriodo.Text = "AL " & FechaToString(Fecha, Fecha)
            Else
                rpt.xrlPeriodo.Text = String.Format("CORRESPONDIENTE AL MES DE {0} de {1}", meMes.Text, seEjercicio.EditValue)
            End If
            rpt.xrlDescMoneda.Text = gsDesc_Moneda
            rpt.ShowPreviewDialog()
        End If
    End Sub

End Class
