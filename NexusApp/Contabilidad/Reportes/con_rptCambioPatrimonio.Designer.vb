﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class con_rptCambioPatrimonio
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xrlPage = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrActual = New DevExpress.XtraReports.UI.XRLabel
        Me.xrpFecha = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xrpNumero = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xrlDescMoneda = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlPeriodo = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTitulo = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlEmpresa = New DevExpress.XtraReports.UI.XRLabel
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
        Me.xrlCargo6 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFirmante5 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLineaFirma5 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlFirmante6 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLineaFirma6 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlCargo5 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLineaFirma4 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlFirmante3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLineaFirma3 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlFirmante4 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCargo3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCargo4 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLineaFirma2 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlFirmante1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCargo1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFirmante2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLineaFirma1 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlCargo2 = New DevExpress.XtraReports.UI.XRLabel
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xrlTotal = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlUtilidad = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlOtrasReservas = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlReserva = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCapital = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDescripcion = New DevExpress.XtraReports.UI.XRLabel
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
        Me.DsContabilidad1 = New Nexus.dsContabilidad
        CType(Me.DsContabilidad1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xrlPage
        '
        Me.xrlPage.LocationFloat = New DevExpress.Utils.PointFloat(509.0!, 0.0!)
        Me.xrlPage.Name = "xrlPage"
        Me.xrlPage.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPage.SizeF = New System.Drawing.SizeF(56.0!, 16.0!)
        Me.xrlPage.Text = "Pág. No."
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.xrpFecha, Me.xrlPage, Me.xrpNumero, Me.xrlDescMoneda, Me.xrlPeriodo, Me.xrlTitulo, Me.xrlEmpresa})
        Me.PageHeader.HeightF = 158.0833!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPanel1
        '
        Me.XrPanel1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel14, Me.XrLabel13, Me.XrLabel12, Me.XrLabel11, Me.XrActual})
        Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 112.0!)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.SizeF = New System.Drawing.SizeF(778.0!, 46.08334!)
        Me.XrPanel1.StylePriority.UseBorders = False
        '
        'XrLabel14
        '
        Me.XrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel14.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(697.6667!, 8.000038!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(77.0!, 32.0!)
        Me.XrLabel14.StylePriority.UseBorders = False
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        Me.XrLabel14.Text = "Total Patrimonio"
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel13
        '
        Me.XrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel13.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(598.6667!, 8.000007!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(86.0!, 32.0!)
        Me.XrLabel13.StylePriority.UseBorders = False
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "Utilidades Acumuladas"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel12
        '
        Me.XrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel12.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(519.5834!, 8.000007!)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(66.0!, 32.0!)
        Me.XrLabel12.StylePriority.UseBorders = False
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "Otras Reservas"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel11
        '
        Me.XrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel11.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(380.2917!, 8.000007!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(50.0!, 32.0!)
        Me.XrLabel11.StylePriority.UseBorders = False
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "Capital Social"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrActual
        '
        Me.XrActual.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrActual.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrActual.LocationFloat = New DevExpress.Utils.PointFloat(441.2917!, 8.000038!)
        Me.XrActual.Name = "XrActual"
        Me.XrActual.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrActual.SizeF = New System.Drawing.SizeF(66.0!, 32.0!)
        Me.XrActual.StylePriority.UseBorders = False
        Me.XrActual.StylePriority.UseFont = False
        Me.XrActual.StylePriority.UseTextAlignment = False
        Me.XrActual.Text = "Reserva Legal 7%"
        Me.XrActual.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrpFecha
        '
        Me.xrpFecha.Format = "{0:dd/MM/yyyy hh:mm tt}"
        Me.xrpFecha.LocationFloat = New DevExpress.Utils.PointFloat(641.0!, 0.0!)
        Me.xrpFecha.Name = "xrpFecha"
        Me.xrpFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpFecha.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.xrpFecha.SizeF = New System.Drawing.SizeF(125.0!, 16.0!)
        '
        'xrpNumero
        '
        Me.xrpNumero.LocationFloat = New DevExpress.Utils.PointFloat(569.0!, 0.0!)
        Me.xrpNumero.Name = "xrpNumero"
        Me.xrpNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpNumero.SizeF = New System.Drawing.SizeF(66.0!, 16.0!)
        '
        'xrlDescMoneda
        '
        Me.xrlDescMoneda.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlDescMoneda.LocationFloat = New DevExpress.Utils.PointFloat(22.0!, 84.0!)
        Me.xrlDescMoneda.Name = "xrlDescMoneda"
        Me.xrlDescMoneda.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDescMoneda.SizeF = New System.Drawing.SizeF(747.0!, 19.0!)
        Me.xrlDescMoneda.StylePriority.UseFont = False
        Me.xrlDescMoneda.StylePriority.UseTextAlignment = False
        Me.xrlDescMoneda.Text = "xrlDescMoneda"
        Me.xrlDescMoneda.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlPeriodo
        '
        Me.xrlPeriodo.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.xrlPeriodo.LocationFloat = New DevExpress.Utils.PointFloat(22.0!, 63.0!)
        Me.xrlPeriodo.Name = "xrlPeriodo"
        Me.xrlPeriodo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPeriodo.SizeF = New System.Drawing.SizeF(747.0!, 19.0!)
        Me.xrlPeriodo.StylePriority.UseFont = False
        Me.xrlPeriodo.StylePriority.UseTextAlignment = False
        Me.xrlPeriodo.Text = "xrlPeriodo"
        Me.xrlPeriodo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlTitulo
        '
        Me.xrlTitulo.Font = New System.Drawing.Font("Arial", 11.0!)
        Me.xrlTitulo.LocationFloat = New DevExpress.Utils.PointFloat(22.0!, 41.0!)
        Me.xrlTitulo.Name = "xrlTitulo"
        Me.xrlTitulo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTitulo.SizeF = New System.Drawing.SizeF(747.0!, 19.0!)
        Me.xrlTitulo.StylePriority.UseFont = False
        Me.xrlTitulo.StylePriority.UseTextAlignment = False
        Me.xrlTitulo.Text = "xrlTitulo"
        Me.xrlTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlEmpresa
        '
        Me.xrlEmpresa.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.xrlEmpresa.LocationFloat = New DevExpress.Utils.PointFloat(22.0!, 19.0!)
        Me.xrlEmpresa.Name = "xrlEmpresa"
        Me.xrlEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlEmpresa.SizeF = New System.Drawing.SizeF(747.0!, 22.0!)
        Me.xrlEmpresa.StylePriority.UseFont = False
        Me.xrlEmpresa.StylePriority.UseTextAlignment = False
        Me.xrlEmpresa.Text = "xrlEmpresa"
        Me.xrlEmpresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlCargo6, Me.xrlFirmante5, Me.xrLineaFirma5, Me.xrlFirmante6, Me.xrLineaFirma6, Me.xrlCargo5, Me.xrLineaFirma4, Me.xrlFirmante3, Me.xrLineaFirma3, Me.xrlFirmante4, Me.xrlCargo3, Me.xrlCargo4, Me.xrLineaFirma2, Me.xrlFirmante1, Me.xrlCargo1, Me.xrlFirmante2, Me.xrLineaFirma1, Me.xrlCargo2})
        Me.ReportFooter.HeightF = 315.875!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'xrlCargo6
        '
        Me.xrlCargo6.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo6.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 250.6041!)
        Me.xrlCargo6.Name = "xrlCargo6"
        Me.xrlCargo6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo6.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo6.StylePriority.UseFont = False
        Me.xrlCargo6.StylePriority.UseTextAlignment = False
        Me.xrlCargo6.Text = "xrlCargo6"
        Me.xrlCargo6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlFirmante5
        '
        Me.xrlFirmante5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante5.LocationFloat = New DevExpress.Utils.PointFloat(40.00007!, 237.6042!)
        Me.xrlFirmante5.Name = "xrlFirmante5"
        Me.xrlFirmante5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante5.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante5.StylePriority.UseFont = False
        Me.xrlFirmante5.StylePriority.UseTextAlignment = False
        Me.xrlFirmante5.Text = "xrlFirmante5"
        Me.xrlFirmante5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma5
        '
        Me.xrLineaFirma5.LocationFloat = New DevExpress.Utils.PointFloat(40.00007!, 233.6042!)
        Me.xrLineaFirma5.Name = "xrLineaFirma5"
        Me.xrLineaFirma5.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlFirmante6
        '
        Me.xrlFirmante6.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante6.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 237.6042!)
        Me.xrlFirmante6.Name = "xrlFirmante6"
        Me.xrlFirmante6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante6.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante6.StylePriority.UseFont = False
        Me.xrlFirmante6.StylePriority.UseTextAlignment = False
        Me.xrlFirmante6.Text = "xrlFirmante6"
        Me.xrlFirmante6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma6
        '
        Me.xrLineaFirma6.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 233.6042!)
        Me.xrLineaFirma6.Name = "xrLineaFirma6"
        Me.xrLineaFirma6.SizeF = New System.Drawing.SizeF(222.0!, 3.0!)
        '
        'xrlCargo5
        '
        Me.xrlCargo5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo5.LocationFloat = New DevExpress.Utils.PointFloat(40.00007!, 250.6041!)
        Me.xrlCargo5.Name = "xrlCargo5"
        Me.xrlCargo5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo5.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo5.StylePriority.UseFont = False
        Me.xrlCargo5.StylePriority.UseTextAlignment = False
        Me.xrlCargo5.Text = "xrlCargo5"
        Me.xrlCargo5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma4
        '
        Me.xrLineaFirma4.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 141.0625!)
        Me.xrLineaFirma4.Name = "xrLineaFirma4"
        Me.xrLineaFirma4.SizeF = New System.Drawing.SizeF(222.0!, 2.0!)
        '
        'xrlFirmante3
        '
        Me.xrlFirmante3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante3.LocationFloat = New DevExpress.Utils.PointFloat(40.0!, 144.0625!)
        Me.xrlFirmante3.Name = "xrlFirmante3"
        Me.xrlFirmante3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante3.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante3.StylePriority.UseFont = False
        Me.xrlFirmante3.StylePriority.UseTextAlignment = False
        Me.xrlFirmante3.Text = "xrlFirmante3"
        Me.xrlFirmante3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma3
        '
        Me.xrLineaFirma3.LocationFloat = New DevExpress.Utils.PointFloat(40.0!, 141.0625!)
        Me.xrLineaFirma3.Name = "xrLineaFirma3"
        Me.xrLineaFirma3.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlFirmante4
        '
        Me.xrlFirmante4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante4.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 144.0625!)
        Me.xrlFirmante4.Name = "xrlFirmante4"
        Me.xrlFirmante4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante4.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante4.StylePriority.UseFont = False
        Me.xrlFirmante4.StylePriority.UseTextAlignment = False
        Me.xrlFirmante4.Text = "xrlFirmante4"
        Me.xrlFirmante4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo3
        '
        Me.xrlCargo3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo3.LocationFloat = New DevExpress.Utils.PointFloat(40.0!, 157.0625!)
        Me.xrlCargo3.Name = "xrlCargo3"
        Me.xrlCargo3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo3.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo3.StylePriority.UseFont = False
        Me.xrlCargo3.StylePriority.UseTextAlignment = False
        Me.xrlCargo3.Text = "xrlCargo3"
        Me.xrlCargo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo4
        '
        Me.xrlCargo4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo4.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 157.0625!)
        Me.xrlCargo4.Name = "xrlCargo4"
        Me.xrlCargo4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo4.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo4.StylePriority.UseFont = False
        Me.xrlCargo4.StylePriority.UseTextAlignment = False
        Me.xrlCargo4.Text = "xrlCargo4"
        Me.xrlCargo4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma2
        '
        Me.xrLineaFirma2.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 53.27087!)
        Me.xrLineaFirma2.Name = "xrLineaFirma2"
        Me.xrLineaFirma2.SizeF = New System.Drawing.SizeF(222.0!, 2.0!)
        '
        'xrlFirmante1
        '
        Me.xrlFirmante1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante1.LocationFloat = New DevExpress.Utils.PointFloat(40.00007!, 56.27087!)
        Me.xrlFirmante1.Name = "xrlFirmante1"
        Me.xrlFirmante1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante1.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante1.StylePriority.UseFont = False
        Me.xrlFirmante1.StylePriority.UseTextAlignment = False
        Me.xrlFirmante1.Text = "xrlFirmante1"
        Me.xrlFirmante1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo1
        '
        Me.xrlCargo1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo1.LocationFloat = New DevExpress.Utils.PointFloat(40.00007!, 69.27084!)
        Me.xrlCargo1.Name = "xrlCargo1"
        Me.xrlCargo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo1.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo1.StylePriority.UseFont = False
        Me.xrlCargo1.StylePriority.UseTextAlignment = False
        Me.xrlCargo1.Text = "xrlCargo1"
        Me.xrlCargo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlFirmante2
        '
        Me.xrlFirmante2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante2.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 56.27087!)
        Me.xrlFirmante2.Name = "xrlFirmante2"
        Me.xrlFirmante2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante2.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante2.StylePriority.UseFont = False
        Me.xrlFirmante2.StylePriority.UseTextAlignment = False
        Me.xrlFirmante2.Text = "xrlFirmante2"
        Me.xrlFirmante2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma1
        '
        Me.xrLineaFirma1.LocationFloat = New DevExpress.Utils.PointFloat(40.00007!, 53.27087!)
        Me.xrLineaFirma1.Name = "xrLineaFirma1"
        Me.xrLineaFirma1.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlCargo2
        '
        Me.xrlCargo2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo2.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 69.27084!)
        Me.xrlCargo2.Name = "xrlCargo2"
        Me.xrlCargo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo2.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo2.StylePriority.UseFont = False
        Me.xrlCargo2.StylePriority.UseTextAlignment = False
        Me.xrlCargo2.Text = "xrlCargo2"
        Me.xrlCargo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlTotal, Me.xrlUtilidad, Me.xrlOtrasReservas, Me.xrlReserva, Me.xrlCapital, Me.xrlDescripcion})
        Me.Detail.HeightF = 26.12502!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTotal
        '
        Me.xrlTotal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Tmp_Patrimonio.TotalPatrimonio", "{0:n2}")})
        Me.xrlTotal.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlTotal.LocationFloat = New DevExpress.Utils.PointFloat(694.0001!, 2.0!)
        Me.xrlTotal.Name = "xrlTotal"
        Me.xrlTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.xrlTotal.SizeF = New System.Drawing.SizeF(79.66663!, 19.0!)
        Me.xrlTotal.StylePriority.UseFont = False
        Me.xrlTotal.StylePriority.UseTextAlignment = False
        Me.xrlTotal.Text = "xrlTotal"
        Me.xrlTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlUtilidad
        '
        Me.xrlUtilidad.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Tmp_Patrimonio.Utilidades", "{0:n2}")})
        Me.xrlUtilidad.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlUtilidad.LocationFloat = New DevExpress.Utils.PointFloat(597.0!, 2.0!)
        Me.xrlUtilidad.Name = "xrlUtilidad"
        Me.xrlUtilidad.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlUtilidad.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.xrlUtilidad.SizeF = New System.Drawing.SizeF(86.66663!, 19.0!)
        Me.xrlUtilidad.StylePriority.UseFont = False
        Me.xrlUtilidad.StylePriority.UseTextAlignment = False
        Me.xrlUtilidad.Text = "xrlUtilidad"
        Me.xrlUtilidad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlOtrasReservas
        '
        Me.xrlOtrasReservas.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Tmp_Patrimonio.OtrasReservas", "{0:n2}")})
        Me.xrlOtrasReservas.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlOtrasReservas.LocationFloat = New DevExpress.Utils.PointFloat(511.6666!, 2.0!)
        Me.xrlOtrasReservas.Name = "xrlOtrasReservas"
        Me.xrlOtrasReservas.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlOtrasReservas.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.xrlOtrasReservas.SizeF = New System.Drawing.SizeF(72.6666!, 19.0!)
        Me.xrlOtrasReservas.StylePriority.UseFont = False
        Me.xrlOtrasReservas.StylePriority.UseTextAlignment = False
        Me.xrlOtrasReservas.Text = "xrlOtrasReservas"
        Me.xrlOtrasReservas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlReserva
        '
        Me.xrlReserva.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Tmp_Patrimonio.Reserva", "{0:n2}")})
        Me.xrlReserva.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlReserva.LocationFloat = New DevExpress.Utils.PointFloat(432.0!, 2.0!)
        Me.xrlReserva.Name = "xrlReserva"
        Me.xrlReserva.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlReserva.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.xrlReserva.SizeF = New System.Drawing.SizeF(72.6666!, 19.0!)
        Me.xrlReserva.StylePriority.UseFont = False
        Me.xrlReserva.StylePriority.UseTextAlignment = False
        Me.xrlReserva.Text = "xrlReserva"
        Me.xrlReserva.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCapital
        '
        Me.xrlCapital.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Tmp_Patrimonio.Capital", "{0:n2}")})
        Me.xrlCapital.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlCapital.LocationFloat = New DevExpress.Utils.PointFloat(362.3334!, 3.0!)
        Me.xrlCapital.Name = "xrlCapital"
        Me.xrlCapital.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCapital.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.xrlCapital.SizeF = New System.Drawing.SizeF(67.6666!, 19.0!)
        Me.xrlCapital.StylePriority.UseFont = False
        Me.xrlCapital.StylePriority.UseTextAlignment = False
        Me.xrlCapital.Text = "xrlCapital"
        Me.xrlCapital.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDescripcion
        '
        Me.xrlDescripcion.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Tmp_Patrimonio.Descripcion")})
        Me.xrlDescripcion.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlDescripcion.LocationFloat = New DevExpress.Utils.PointFloat(3.666691!, 3.0!)
        Me.xrlDescripcion.Name = "xrlDescripcion"
        Me.xrlDescripcion.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDescripcion.SizeF = New System.Drawing.SizeF(352.3333!, 19.0!)
        Me.xrlDescripcion.StylePriority.UseFont = False
        Me.xrlDescripcion.Text = "xrlDescripcion"
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.HeightF = 25.41666!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.HeightF = 40.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'GroupFooter1
        '
        Me.GroupFooter1.HeightF = 30.20833!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'DsContabilidad1
        '
        Me.DsContabilidad1.DataSetName = "dsContabilidad"
        Me.DsContabilidad1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'con_rptCambioPatrimonio
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter, Me.GroupFooter1})
        Me.DataMember = "Tmp_Patrimonio"
        Me.DataSource = Me.DsContabilidad1
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(35, 35, 25, 40)
        Me.SnapGridSize = 2.0!
        Me.SnapToGrid = False
        Me.Version = "11.1"
        CType(Me.DsContabilidad1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents xrlPage As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrActual As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrpFecha As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents xrpNumero As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents xrlDescMoneda As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPeriodo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTitulo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlEmpresa As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents xrlCapital As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDescripcion As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlUtilidad As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlOtrasReservas As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlReserva As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DsContabilidad1 As Nexus.dsContabilidad
    Friend WithEvents xrlCargo6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFirmante5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma6 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlCargo5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma4 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFirmante2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlCargo2 As DevExpress.XtraReports.UI.XRLabel
End Class
