﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class con_frmAnexos
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)


    Private Sub con_frmAnexos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        bl.ObtienePeriodoContable(Mes, Ejercicio)
        meMes.Month = Mes
        seEjercicio.EditValue = Ejercicio
    End Sub

    Private Sub con_frmAnexos_Reporte() Handles Me.Reporte
        Dim dt As DataTable = bl.con_rptAnexos(meMes.EditValue, seEjercicio.EditValue, rgTipo.EditValue, leSucursal.EditValue)
        Dim rpt As New con_rptAnexos() With {.DataSource = dt, .DataMember = ""}

        rpt.xrlEmpresa.Text = gsNombre_Empresa

        Dim Hasta As Date = CDate(String.Format("01/{0}/{1}", meMes.EditValue, seEjercicio.EditValue))
        Hasta = DateAdd(DateInterval.Month, 1, Hasta)
        Hasta = DateAdd(DateInterval.Day, -1, Hasta)
        rpt.xrlPeriodo.Text = ("AL " + FechaToString(Hasta, Hasta)).ToUpper
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlMoneda.Text = gsDesc_Moneda

        rpt.ShowPreviewDialog()
    End Sub
End Class
