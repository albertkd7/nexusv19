﻿Imports NexusBLL
Public Class con_frmSaldosCentro
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Private Sub con_frmAnexos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(lesucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub


    Private Sub con_frmSaldosCentro_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.ObtenerSaldosCentros(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)

        Dim rpt As New con_rptSaldosCentroCosto() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = FechaToString(deDesde.DateTime, deHasta.DateTime)
        rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
        rpt.ShowPreviewDialog()

    End Sub
End Class
