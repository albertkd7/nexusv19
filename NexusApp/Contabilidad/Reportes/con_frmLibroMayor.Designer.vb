﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmLibroMayor
    Inherits gen_frmBaseRpt
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(con_frmLibroMayor))
        Me.deFin = New DevExpress.XtraEditors.DateEdit()
        Me.chkNumerar = New DevExpress.XtraEditors.CheckEdit()
        Me.deIni = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.ceInfoDateTime = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.seFolio = New DevExpress.XtraEditors.SpinEdit()
        Me.teConcepto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.rgTipo = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.deFin.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNumerar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deIni.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deIni.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceInfoDateTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.rgTipo)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.teConcepto)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.seFolio)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.deFin)
        Me.GroupControl1.Controls.Add(Me.ceInfoDateTime)
        Me.GroupControl1.Controls.Add(Me.chkNumerar)
        Me.GroupControl1.Controls.Add(Me.deIni)
        Me.GroupControl1.Size = New System.Drawing.Size(577, 337)
        '
        'deFin
        '
        Me.deFin.EditValue = New Date(2008, 1, 5, 0, 0, 0, 0)
        Me.deFin.EnterMoveNextControl = True
        Me.deFin.Location = New System.Drawing.Point(154, 74)
        Me.deFin.Name = "deFin"
        Me.deFin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFin.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFin.Size = New System.Drawing.Size(100, 20)
        Me.deFin.TabIndex = 2
        '
        'chkNumerar
        '
        Me.chkNumerar.Location = New System.Drawing.Point(154, 199)
        Me.chkNumerar.Name = "chkNumerar"
        Me.chkNumerar.Properties.Caption = "Númerar páginas"
        Me.chkNumerar.Size = New System.Drawing.Size(124, 19)
        Me.chkNumerar.TabIndex = 6
        '
        'deIni
        '
        Me.deIni.EditValue = New Date(2008, 1, 1, 0, 0, 0, 0)
        Me.deIni.EnterMoveNextControl = True
        Me.deIni.Location = New System.Drawing.Point(154, 51)
        Me.deIni.Name = "deIni"
        Me.deIni.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deIni.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deIni.Properties.CalendarTimeProperties.Mask.EditMask = "d"
        Me.deIni.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deIni.Size = New System.Drawing.Size(100, 20)
        Me.deIni.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(85, 54)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl1.TabIndex = 4
        Me.LabelControl1.Text = "Desde Fecha:"
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "LIBRO MAYOR"
        Me.teTitulo.Location = New System.Drawing.Point(154, 133)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(379, 20)
        Me.teTitulo.TabIndex = 4
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(87, 77)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Hasta Fecha:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(65, 136)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "Título del reporte:"
        '
        'ceInfoDateTime
        '
        Me.ceInfoDateTime.Location = New System.Drawing.Point(154, 233)
        Me.ceInfoDateTime.Name = "ceInfoDateTime"
        Me.ceInfoDateTime.Properties.Caption = "Imprimir información de fecha y hora"
        Me.ceInfoDateTime.Size = New System.Drawing.Size(228, 19)
        Me.ceInfoDateTime.TabIndex = 8
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(326, 203)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl4.TabIndex = 6
        Me.LabelControl4.Text = "Folio Inicial:"
        '
        'seFolio
        '
        Me.seFolio.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.seFolio.Location = New System.Drawing.Point(389, 201)
        Me.seFolio.Name = "seFolio"
        Me.seFolio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seFolio.Size = New System.Drawing.Size(77, 20)
        Me.seFolio.TabIndex = 7
        '
        'teConcepto
        '
        Me.teConcepto.EditValue = "MAYORIZACION DE ESTE DÍA"
        Me.teConcepto.Location = New System.Drawing.Point(154, 165)
        Me.teConcepto.Name = "teConcepto"
        Me.teConcepto.Size = New System.Drawing.Size(379, 20)
        Me.teConcepto.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(17, 168)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(134, 13)
        Me.LabelControl5.TabIndex = 7
        Me.LabelControl5.Text = "Concepto de c/Transacción:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(106, 31)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl6.TabIndex = 35
        Me.LabelControl6.Text = "Sucursal:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(154, 26)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(379, 20)
        Me.leSucursal.TabIndex = 0
        '
        'rgTipo
        '
        Me.rgTipo.EditValue = CType(0, Short)
        Me.rgTipo.Location = New System.Drawing.Point(154, 104)
        Me.rgTipo.Name = "rgTipo"
        Me.rgTipo.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(0, Short), "por Fecha"), New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(1, Short), "por Cuenta de Mayor")})
        Me.rgTipo.Size = New System.Drawing.Size(333, 23)
        Me.rgTipo.TabIndex = 3
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(89, 108)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(62, 13)
        Me.LabelControl7.TabIndex = 36
        Me.LabelControl7.Text = "Agrupar por:"
        '
        'con_frmLibroMayor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(577, 362)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmLibroMayor"
        Me.Text = "Libro Mayor"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.deFin.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNumerar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deIni.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deIni.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceInfoDateTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents deFin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents chkNumerar As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents deIni As DevExpress.XtraEditors.DateEdit
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceInfoDateTime As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seFolio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents teConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents rgTipo As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
End Class
