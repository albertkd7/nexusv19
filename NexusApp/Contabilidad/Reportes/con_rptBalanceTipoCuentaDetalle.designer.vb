<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class con_rptBalanceTipoCuentaDetalle
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.xrlCargo5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLineaFirma1 = New DevExpress.XtraReports.UI.XRLine()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.xrlCargo6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLineaFirma6 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlFirmante6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLineaFirma5 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlFirmante5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCargo4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLineaFirma4 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlFirmante4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLineaFirma3 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlFirmante3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCargo3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCargo2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLineaFirma2 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlFirmante2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCargo1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFirmante1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine6 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine5 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlTotal_P = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal_A = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrpNumero = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrpFecha = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.xrlPage = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine4 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSaldoNivel2a = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLine14 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlSaldoNivel3a = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine13 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlSaldoNivel3p = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLineParcial1 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlSaldoNivel1a = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSaldoNivel1p = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSaldoNivel2p = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.DsContabilidad1 = New Nexus.dsContabilidad()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.xrlDescMoneda = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlEmpresa = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTitulo = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPeriodo = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        CType(Me.DsContabilidad1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xrlCargo5
        '
        Me.xrlCargo5.Dpi = 100.0!
        Me.xrlCargo5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo5.LocationFloat = New DevExpress.Utils.PointFloat(175.0!, 357.0!)
        Me.xrlCargo5.Name = "xrlCargo5"
        Me.xrlCargo5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo5.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo5.StylePriority.UseFont = False
        Me.xrlCargo5.StylePriority.UseTextAlignment = False
        Me.xrlCargo5.Text = "xrlCargo5"
        Me.xrlCargo5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma1
        '
        Me.xrLineaFirma1.Dpi = 100.0!
        Me.xrLineaFirma1.LocationFloat = New DevExpress.Utils.PointFloat(175.0!, 117.0!)
        Me.xrLineaFirma1.Name = "xrLineaFirma1"
        Me.xrLineaFirma1.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlCargo6, Me.xrLineaFirma6, Me.xrlFirmante6, Me.xrLineaFirma5, Me.xrlFirmante5, Me.xrlCargo5, Me.xrlCargo4, Me.xrLineaFirma4, Me.xrlFirmante4, Me.xrLineaFirma3, Me.xrlFirmante3, Me.xrlCargo3, Me.xrlCargo2, Me.xrLineaFirma2, Me.xrlFirmante2, Me.xrlCargo1, Me.xrlFirmante1, Me.xrLineaFirma1, Me.XrLine6, Me.XrLine5, Me.XrLine3, Me.xrlTotal_P, Me.XrLabel7, Me.XrLine2, Me.XrLabel15, Me.xrlTotal_A})
        Me.GroupFooter1.Dpi = 100.0!
        Me.GroupFooter1.HeightF = 377.8333!
        Me.GroupFooter1.Name = "GroupFooter1"
        Me.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'xrlCargo6
        '
        Me.xrlCargo6.Dpi = 100.0!
        Me.xrlCargo6.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo6.LocationFloat = New DevExpress.Utils.PointFloat(659.0!, 357.0!)
        Me.xrlCargo6.Name = "xrlCargo6"
        Me.xrlCargo6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo6.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo6.StylePriority.UseFont = False
        Me.xrlCargo6.StylePriority.UseTextAlignment = False
        Me.xrlCargo6.Text = "xrlCargo6"
        Me.xrlCargo6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma6
        '
        Me.xrLineaFirma6.Dpi = 100.0!
        Me.xrLineaFirma6.LocationFloat = New DevExpress.Utils.PointFloat(659.0!, 340.0!)
        Me.xrLineaFirma6.Name = "xrLineaFirma6"
        Me.xrLineaFirma6.SizeF = New System.Drawing.SizeF(222.0!, 3.0!)
        '
        'xrlFirmante6
        '
        Me.xrlFirmante6.Dpi = 100.0!
        Me.xrlFirmante6.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante6.LocationFloat = New DevExpress.Utils.PointFloat(659.0!, 344.0!)
        Me.xrlFirmante6.Name = "xrlFirmante6"
        Me.xrlFirmante6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante6.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante6.StylePriority.UseFont = False
        Me.xrlFirmante6.StylePriority.UseTextAlignment = False
        Me.xrlFirmante6.Text = "xrlFirmante6"
        Me.xrlFirmante6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma5
        '
        Me.xrLineaFirma5.Dpi = 100.0!
        Me.xrLineaFirma5.LocationFloat = New DevExpress.Utils.PointFloat(175.0!, 340.0!)
        Me.xrLineaFirma5.Name = "xrLineaFirma5"
        Me.xrLineaFirma5.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlFirmante5
        '
        Me.xrlFirmante5.Dpi = 100.0!
        Me.xrlFirmante5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante5.LocationFloat = New DevExpress.Utils.PointFloat(175.0!, 344.0!)
        Me.xrlFirmante5.Name = "xrlFirmante5"
        Me.xrlFirmante5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante5.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante5.StylePriority.UseFont = False
        Me.xrlFirmante5.StylePriority.UseTextAlignment = False
        Me.xrlFirmante5.Text = "xrlFirmante5"
        Me.xrlFirmante5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo4
        '
        Me.xrlCargo4.Dpi = 100.0!
        Me.xrlCargo4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo4.LocationFloat = New DevExpress.Utils.PointFloat(656.0!, 246.0!)
        Me.xrlCargo4.Name = "xrlCargo4"
        Me.xrlCargo4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo4.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo4.StylePriority.UseFont = False
        Me.xrlCargo4.StylePriority.UseTextAlignment = False
        Me.xrlCargo4.Text = "xrlCargo4"
        Me.xrlCargo4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma4
        '
        Me.xrLineaFirma4.Dpi = 100.0!
        Me.xrLineaFirma4.LocationFloat = New DevExpress.Utils.PointFloat(656.0!, 229.0!)
        Me.xrLineaFirma4.Name = "xrLineaFirma4"
        Me.xrLineaFirma4.SizeF = New System.Drawing.SizeF(222.0!, 3.0!)
        '
        'xrlFirmante4
        '
        Me.xrlFirmante4.Dpi = 100.0!
        Me.xrlFirmante4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante4.LocationFloat = New DevExpress.Utils.PointFloat(656.0!, 233.0!)
        Me.xrlFirmante4.Name = "xrlFirmante4"
        Me.xrlFirmante4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante4.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante4.StylePriority.UseFont = False
        Me.xrlFirmante4.StylePriority.UseTextAlignment = False
        Me.xrlFirmante4.Text = "xrlFirmante4"
        Me.xrlFirmante4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma3
        '
        Me.xrLineaFirma3.Dpi = 100.0!
        Me.xrLineaFirma3.LocationFloat = New DevExpress.Utils.PointFloat(172.0!, 229.0!)
        Me.xrLineaFirma3.Name = "xrLineaFirma3"
        Me.xrLineaFirma3.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlFirmante3
        '
        Me.xrlFirmante3.Dpi = 100.0!
        Me.xrlFirmante3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante3.LocationFloat = New DevExpress.Utils.PointFloat(172.0!, 233.0!)
        Me.xrlFirmante3.Name = "xrlFirmante3"
        Me.xrlFirmante3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante3.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante3.StylePriority.UseFont = False
        Me.xrlFirmante3.StylePriority.UseTextAlignment = False
        Me.xrlFirmante3.Text = "xrlFirmante3"
        Me.xrlFirmante3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo3
        '
        Me.xrlCargo3.Dpi = 100.0!
        Me.xrlCargo3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo3.LocationFloat = New DevExpress.Utils.PointFloat(172.0!, 246.0!)
        Me.xrlCargo3.Name = "xrlCargo3"
        Me.xrlCargo3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo3.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo3.StylePriority.UseFont = False
        Me.xrlCargo3.StylePriority.UseTextAlignment = False
        Me.xrlCargo3.Text = "xrlCargo3"
        Me.xrlCargo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo2
        '
        Me.xrlCargo2.Dpi = 100.0!
        Me.xrlCargo2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo2.LocationFloat = New DevExpress.Utils.PointFloat(659.0!, 134.0!)
        Me.xrlCargo2.Name = "xrlCargo2"
        Me.xrlCargo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo2.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo2.StylePriority.UseFont = False
        Me.xrlCargo2.StylePriority.UseTextAlignment = False
        Me.xrlCargo2.Text = "xrlCargo2"
        Me.xrlCargo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma2
        '
        Me.xrLineaFirma2.Dpi = 100.0!
        Me.xrLineaFirma2.LocationFloat = New DevExpress.Utils.PointFloat(659.0!, 117.0!)
        Me.xrLineaFirma2.Name = "xrLineaFirma2"
        Me.xrLineaFirma2.SizeF = New System.Drawing.SizeF(222.0!, 3.0!)
        '
        'xrlFirmante2
        '
        Me.xrlFirmante2.Dpi = 100.0!
        Me.xrlFirmante2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante2.LocationFloat = New DevExpress.Utils.PointFloat(659.0!, 121.0!)
        Me.xrlFirmante2.Name = "xrlFirmante2"
        Me.xrlFirmante2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante2.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante2.StylePriority.UseFont = False
        Me.xrlFirmante2.StylePriority.UseTextAlignment = False
        Me.xrlFirmante2.Text = "xrlFirmante2"
        Me.xrlFirmante2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo1
        '
        Me.xrlCargo1.Dpi = 100.0!
        Me.xrlCargo1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo1.LocationFloat = New DevExpress.Utils.PointFloat(175.0!, 134.0!)
        Me.xrlCargo1.Name = "xrlCargo1"
        Me.xrlCargo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo1.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo1.StylePriority.UseFont = False
        Me.xrlCargo1.StylePriority.UseTextAlignment = False
        Me.xrlCargo1.Text = "xrlCargo1"
        Me.xrlCargo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlFirmante1
        '
        Me.xrlFirmante1.Dpi = 100.0!
        Me.xrlFirmante1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante1.LocationFloat = New DevExpress.Utils.PointFloat(175.0!, 121.0!)
        Me.xrlFirmante1.Name = "xrlFirmante1"
        Me.xrlFirmante1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante1.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante1.StylePriority.UseFont = False
        Me.xrlFirmante1.StylePriority.UseTextAlignment = False
        Me.xrlFirmante1.Text = "xrlFirmante1"
        Me.xrlFirmante1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine6
        '
        Me.XrLine6.Dpi = 100.0!
        Me.XrLine6.LocationFloat = New DevExpress.Utils.PointFloat(938.0!, 25.0!)
        Me.XrLine6.Name = "XrLine6"
        Me.XrLine6.SizeF = New System.Drawing.SizeF(88.0!, 6.0!)
        '
        'XrLine5
        '
        Me.XrLine5.Dpi = 100.0!
        Me.XrLine5.LocationFloat = New DevExpress.Utils.PointFloat(416.0!, 25.0!)
        Me.XrLine5.Name = "XrLine5"
        Me.XrLine5.SizeF = New System.Drawing.SizeF(88.0!, 6.0!)
        '
        'XrLine3
        '
        Me.XrLine3.Dpi = 100.0!
        Me.XrLine3.LocationFloat = New DevExpress.Utils.PointFloat(938.0!, 0!)
        Me.XrLine3.Name = "XrLine3"
        Me.XrLine3.SizeF = New System.Drawing.SizeF(88.0!, 2.0!)
        '
        'xrlTotal_P
        '
        Me.xrlTotal_P.Dpi = 100.0!
        Me.xrlTotal_P.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrlTotal_P.LocationFloat = New DevExpress.Utils.PointFloat(938.0!, 9.0!)
        Me.xrlTotal_P.Name = "xrlTotal_P"
        Me.xrlTotal_P.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal_P.SizeF = New System.Drawing.SizeF(88.0!, 16.0!)
        Me.xrlTotal_P.StylePriority.UseFont = False
        Me.xrlTotal_P.StylePriority.UseTextAlignment = False
        XrSummary1.FormatString = "{0:n2}"
        XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom
        Me.xrlTotal_P.Summary = XrSummary1
        Me.xrlTotal_P.Text = "xrlTotal_P"
        Me.xrlTotal_P.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel7
        '
        Me.XrLabel7.Dpi = 100.0!
        Me.XrLabel7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(800.0!, 9.0!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(128.0!, 16.0!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "TOTAL PASIVOS"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLine2
        '
        Me.XrLine2.Dpi = 100.0!
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(416.0!, 0!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(88.0!, 6.0!)
        '
        'XrLabel15
        '
        Me.XrLabel15.Dpi = 100.0!
        Me.XrLabel15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(284.0!, 9.0!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(128.0!, 16.0!)
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        Me.XrLabel15.Text = "TOTAL ACTIVOS"
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal_A
        '
        Me.xrlTotal_A.Dpi = 100.0!
        Me.xrlTotal_A.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrlTotal_A.LocationFloat = New DevExpress.Utils.PointFloat(422.0!, 9.0!)
        Me.xrlTotal_A.Name = "xrlTotal_A"
        Me.xrlTotal_A.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal_A.SizeF = New System.Drawing.SizeF(81.0!, 16.0!)
        Me.xrlTotal_A.StylePriority.UseFont = False
        Me.xrlTotal_A.StylePriority.UseTextAlignment = False
        XrSummary2.FormatString = "{0:n2}"
        XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom
        Me.xrlTotal_A.Summary = XrSummary2
        Me.xrlTotal_A.Text = "xrlTotal_A"
        Me.xrlTotal_A.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrpNumero
        '
        Me.xrpNumero.Dpi = 100.0!
        Me.xrpNumero.LocationFloat = New DevExpress.Utils.PointFloat(876.0!, 0!)
        Me.xrpNumero.Name = "xrpNumero"
        Me.xrpNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpNumero.SizeF = New System.Drawing.SizeF(38.0!, 16.0!)
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel13, Me.XrLabel9})
        Me.PageHeader.Dpi = 100.0!
        Me.PageHeader.HeightF = 34.25!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLine1
        '
        Me.XrLine1.Dpi = 100.0!
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 30.0!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(1031.0!, 2.0!)
        '
        'XrLabel13
        '
        Me.XrLabel13.Dpi = 100.0!
        Me.XrLabel13.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(756.0!, 8.0!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(178.0!, 19.0!)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "PASIVOS"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel9
        '
        Me.XrLabel9.Dpi = 100.0!
        Me.XrLabel9.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(225.0!, 8.0!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(178.0!, 19.0!)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.Text = "ACTIVOS"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrpFecha
        '
        Me.xrpFecha.Dpi = 100.0!
        Me.xrpFecha.Format = "{0:dd/MM/yyyy hh:mm tt}"
        Me.xrpFecha.LocationFloat = New DevExpress.Utils.PointFloat(914.9999!, 0!)
        Me.xrpFecha.Name = "xrpFecha"
        Me.xrpFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpFecha.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.xrpFecha.SizeF = New System.Drawing.SizeF(125.0!, 16.0!)
        '
        'xrlPage
        '
        Me.xrlPage.Dpi = 100.0!
        Me.xrlPage.LocationFloat = New DevExpress.Utils.PointFloat(816.0001!, 0!)
        Me.xrlPage.Name = "xrlPage"
        Me.xrlPage.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPage.SizeF = New System.Drawing.SizeF(56.0!, 16.0!)
        Me.xrlPage.Text = "P�g. No."
        '
        'XrLine4
        '
        Me.XrLine4.Dpi = 100.0!
        Me.XrLine4.LocationFloat = New DevExpress.Utils.PointFloat(884.0!, 15.0!)
        Me.XrLine4.Name = "XrLine4"
        Me.XrLine4.SizeF = New System.Drawing.SizeF(72.0!, 2.0!)
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobCuentaRes.a_Nombre")})
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(12.0!, 0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(266.0!, 14.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.Text = "XrLabel2"
        '
        'xrlSaldoNivel2a
        '
        Me.xrlSaldoNivel2a.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobCuentaRes.a_Saldo", "{0:n2}")})
        Me.xrlSaldoNivel2a.Dpi = 100.0!
        Me.xrlSaldoNivel2a.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlSaldoNivel2a.LocationFloat = New DevExpress.Utils.PointFloat(362.0!, 0!)
        Me.xrlSaldoNivel2a.Name = "xrlSaldoNivel2a"
        Me.xrlSaldoNivel2a.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSaldoNivel2a.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.xrlSaldoNivel2a.SizeF = New System.Drawing.SizeF(69.0!, 14.0!)
        Me.xrlSaldoNivel2a.StylePriority.UseFont = False
        Me.xrlSaldoNivel2a.StylePriority.UseTextAlignment = False
        Me.xrlSaldoNivel2a.Text = "xrlSaldoNivel2a"
        Me.xrlSaldoNivel2a.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine14, Me.xrlSaldoNivel3a, Me.XrLine13, Me.xrlSaldoNivel3p, Me.XrLine4, Me.XrLineParcial1, Me.xrlSaldoNivel1a, Me.xrlSaldoNivel1p, Me.xrlSaldoNivel2p, Me.xrlSaldoNivel2a, Me.XrLabel2, Me.XrLabel1})
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 17.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLine14
        '
        Me.XrLine14.Dpi = 100.0!
        Me.XrLine14.LocationFloat = New DevExpress.Utils.PointFloat(284.0!, 14.0!)
        Me.XrLine14.Name = "XrLine14"
        Me.XrLine14.SizeF = New System.Drawing.SizeF(69.0!, 2.0!)
        '
        'xrlSaldoNivel3a
        '
        Me.xrlSaldoNivel3a.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobCuentaRes.a_Saldo", "{0:n2}")})
        Me.xrlSaldoNivel3a.Dpi = 100.0!
        Me.xrlSaldoNivel3a.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlSaldoNivel3a.LocationFloat = New DevExpress.Utils.PointFloat(284.0!, 0!)
        Me.xrlSaldoNivel3a.Name = "xrlSaldoNivel3a"
        Me.xrlSaldoNivel3a.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSaldoNivel3a.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.xrlSaldoNivel3a.SizeF = New System.Drawing.SizeF(69.0!, 14.0!)
        Me.xrlSaldoNivel3a.StylePriority.UseFont = False
        Me.xrlSaldoNivel3a.StylePriority.UseTextAlignment = False
        Me.xrlSaldoNivel3a.Text = "XrLabel3"
        Me.xrlSaldoNivel3a.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLine13
        '
        Me.XrLine13.Dpi = 100.0!
        Me.XrLine13.LocationFloat = New DevExpress.Utils.PointFloat(806.0!, 15.0!)
        Me.XrLine13.Name = "XrLine13"
        Me.XrLine13.SizeF = New System.Drawing.SizeF(72.0!, 2.0!)
        '
        'xrlSaldoNivel3p
        '
        Me.xrlSaldoNivel3p.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobCuentaRes.p_Saldo", "{0:n2}")})
        Me.xrlSaldoNivel3p.Dpi = 100.0!
        Me.xrlSaldoNivel3p.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlSaldoNivel3p.LocationFloat = New DevExpress.Utils.PointFloat(806.0!, 0!)
        Me.xrlSaldoNivel3p.Name = "xrlSaldoNivel3p"
        Me.xrlSaldoNivel3p.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSaldoNivel3p.SizeF = New System.Drawing.SizeF(69.0!, 14.0!)
        Me.xrlSaldoNivel3p.StylePriority.UseFont = False
        Me.xrlSaldoNivel3p.StylePriority.UseTextAlignment = False
        Me.xrlSaldoNivel3p.Text = "XrLabel4"
        Me.xrlSaldoNivel3p.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLineParcial1
        '
        Me.XrLineParcial1.Dpi = 100.0!
        Me.XrLineParcial1.LocationFloat = New DevExpress.Utils.PointFloat(362.0!, 14.0!)
        Me.XrLineParcial1.Name = "XrLineParcial1"
        Me.XrLineParcial1.SizeF = New System.Drawing.SizeF(69.0!, 2.0!)
        '
        'xrlSaldoNivel1a
        '
        Me.xrlSaldoNivel1a.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobCuentaRes.a_Saldo", "{0:n2}")})
        Me.xrlSaldoNivel1a.Dpi = 100.0!
        Me.xrlSaldoNivel1a.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlSaldoNivel1a.LocationFloat = New DevExpress.Utils.PointFloat(434.0!, 0!)
        Me.xrlSaldoNivel1a.Name = "xrlSaldoNivel1a"
        Me.xrlSaldoNivel1a.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSaldoNivel1a.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.xrlSaldoNivel1a.SizeF = New System.Drawing.SizeF(69.0!, 14.0!)
        Me.xrlSaldoNivel1a.StylePriority.UseFont = False
        Me.xrlSaldoNivel1a.StylePriority.UseTextAlignment = False
        XrSummary3.FormatString = "{0:n2}"
        Me.xrlSaldoNivel1a.Summary = XrSummary3
        Me.xrlSaldoNivel1a.Text = "xrlSaldoNivel1a"
        Me.xrlSaldoNivel1a.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlSaldoNivel1p
        '
        Me.xrlSaldoNivel1p.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobCuentaRes.p_Saldo", "{0:n2}")})
        Me.xrlSaldoNivel1p.Dpi = 100.0!
        Me.xrlSaldoNivel1p.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlSaldoNivel1p.LocationFloat = New DevExpress.Utils.PointFloat(956.0!, 0!)
        Me.xrlSaldoNivel1p.Name = "xrlSaldoNivel1p"
        Me.xrlSaldoNivel1p.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSaldoNivel1p.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.xrlSaldoNivel1p.SizeF = New System.Drawing.SizeF(69.0!, 14.0!)
        Me.xrlSaldoNivel1p.StylePriority.UseFont = False
        Me.xrlSaldoNivel1p.StylePriority.UseTextAlignment = False
        Me.xrlSaldoNivel1p.Text = "xrlSaldoNivel1p"
        Me.xrlSaldoNivel1p.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlSaldoNivel2p
        '
        Me.xrlSaldoNivel2p.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobCuentaRes.p_Saldo", "{0:n2}")})
        Me.xrlSaldoNivel2p.Dpi = 100.0!
        Me.xrlSaldoNivel2p.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlSaldoNivel2p.LocationFloat = New DevExpress.Utils.PointFloat(884.0!, 0!)
        Me.xrlSaldoNivel2p.Name = "xrlSaldoNivel2p"
        Me.xrlSaldoNivel2p.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSaldoNivel2p.SizeF = New System.Drawing.SizeF(69.0!, 14.0!)
        Me.xrlSaldoNivel2p.StylePriority.UseFont = False
        Me.xrlSaldoNivel2p.StylePriority.UseTextAlignment = False
        Me.xrlSaldoNivel2p.Text = "xrlSaldoNivel2p"
        Me.xrlSaldoNivel2p.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobCuentaRes.p_Nombre")})
        Me.XrLabel1.Dpi = 100.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(522.0!, 0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(278.0!, 14.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.Text = "XrLabel1"
        '
        'DsContabilidad1
        '
        Me.DsContabilidad1.DataSetName = "dsContabilidad"
        Me.DsContabilidad1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlDescMoneda, Me.xrlEmpresa, Me.xrlTitulo, Me.xrlPeriodo, Me.xrpNumero, Me.xrlPage, Me.xrpFecha})
        Me.ReportHeader.Dpi = 100.0!
        Me.ReportHeader.HeightF = 96.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xrlDescMoneda
        '
        Me.xrlDescMoneda.Dpi = 100.0!
        Me.xrlDescMoneda.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlDescMoneda.LocationFloat = New DevExpress.Utils.PointFloat(12.0!, 78.0!)
        Me.xrlDescMoneda.Name = "xrlDescMoneda"
        Me.xrlDescMoneda.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDescMoneda.SizeF = New System.Drawing.SizeF(1025.0!, 18.0!)
        Me.xrlDescMoneda.StylePriority.UseFont = False
        Me.xrlDescMoneda.StylePriority.UseTextAlignment = False
        Me.xrlDescMoneda.Text = "xrlDescMoneda"
        Me.xrlDescMoneda.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlEmpresa
        '
        Me.xrlEmpresa.Dpi = 100.0!
        Me.xrlEmpresa.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.xrlEmpresa.LocationFloat = New DevExpress.Utils.PointFloat(12.0!, 19.0!)
        Me.xrlEmpresa.Name = "xrlEmpresa"
        Me.xrlEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlEmpresa.SizeF = New System.Drawing.SizeF(1025.0!, 22.0!)
        Me.xrlEmpresa.StylePriority.UseFont = False
        Me.xrlEmpresa.StylePriority.UseTextAlignment = False
        Me.xrlEmpresa.Text = "xrlEmpresa"
        Me.xrlEmpresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlTitulo
        '
        Me.xrlTitulo.Dpi = 100.0!
        Me.xrlTitulo.Font = New System.Drawing.Font("Arial", 11.0!)
        Me.xrlTitulo.LocationFloat = New DevExpress.Utils.PointFloat(12.0!, 41.0!)
        Me.xrlTitulo.Name = "xrlTitulo"
        Me.xrlTitulo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTitulo.SizeF = New System.Drawing.SizeF(1025.0!, 18.0!)
        Me.xrlTitulo.StylePriority.UseFont = False
        Me.xrlTitulo.StylePriority.UseTextAlignment = False
        Me.xrlTitulo.Text = "xrlTitulo"
        Me.xrlTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlPeriodo
        '
        Me.xrlPeriodo.Dpi = 100.0!
        Me.xrlPeriodo.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.xrlPeriodo.LocationFloat = New DevExpress.Utils.PointFloat(12.0!, 59.0!)
        Me.xrlPeriodo.Name = "xrlPeriodo"
        Me.xrlPeriodo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPeriodo.SizeF = New System.Drawing.SizeF(1025.0!, 19.0!)
        Me.xrlPeriodo.StylePriority.UseFont = False
        Me.xrlPeriodo.StylePriority.UseTextAlignment = False
        Me.xrlPeriodo.Text = "xrlPeriodo"
        Me.xrlPeriodo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.Dpi = 100.0!
        Me.TopMarginBand1.HeightF = 35.0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.Dpi = 100.0!
        Me.BottomMarginBand1.HeightF = 35.29167!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'con_rptBalanceTipoCuentaDetalle
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.GroupFooter1, Me.ReportHeader, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.DataMember = "BalanceComprobCuentaRes"
        Me.DataSource = Me.DsContabilidad1
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 35, 35)
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.SnapGridSize = 3.0!
        Me.SnappingMode = DevExpress.XtraReports.UI.SnappingMode.SnapToGrid
        Me.Version = "16.2"
        CType(Me.DsContabilidad1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents xrlCargo5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents xrlCargo6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma6 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma4 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFirmante1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine6 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlTotal_P As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal_A As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrpNumero As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents xrpFecha As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents xrlPage As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine4 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSaldoNivel2a As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents XrLineParcial1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlSaldoNivel1a As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSaldoNivel1p As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSaldoNivel2p As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine14 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlSaldoNivel3a As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine13 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlSaldoNivel3p As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DsContabilidad1 As Nexus.dsContabilidad
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents xrlDescMoneda As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlEmpresa As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTitulo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPeriodo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
End Class
