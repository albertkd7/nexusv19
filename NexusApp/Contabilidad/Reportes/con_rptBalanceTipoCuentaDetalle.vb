Public Class con_rptBalanceTipoCuentaDetalle
    Private TotalActivo As Decimal = 0, TotalPasivo As Decimal = 0, Nivel As Integer = 0

    Private Sub xrlSaldoNivel3a_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldoNivel3a.BeforePrint
        Nivel = SiEsNulo(GetCurrentColumnValue("a_Nivel"), 0)
        e.Cancel = Nivel <> 3 'si el nivel es diferente de 3, no se imprime
    End Sub

    Private Sub xrlSaldoNivel2a_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldoNivel2a.BeforePrint
        Nivel = SiEsNulo(GetCurrentColumnValue("a_Nivel"), 0)
        e.Cancel = Nivel <> 2 'si el nivel no es 2, no se imprime
    End Sub

    Private Sub xrlSaldoNivel1a_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldoNivel1a.BeforePrint
        Nivel = SiEsNulo(GetCurrentColumnValue("a_Nivel"), 0)
        e.Cancel = Nivel <> 1
    End Sub

    Private Sub xrlSaldoNivel3p_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldoNivel3p.BeforePrint
        Nivel = SiEsNulo(GetCurrentColumnValue("p_Nivel"), 0)
        e.Cancel = Nivel <> 3
    End Sub

    Private Sub xrlSaldoNivel2p_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldoNivel2p.BeforePrint
        Nivel = SiEsNulo(GetCurrentColumnValue("p_Nivel"), 0)
        e.Cancel = Nivel <> 2
    End Sub

    Private Sub xrlSaldoNivel1p_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldoNivel1p.BeforePrint
        Nivel = SiEsNulo(GetCurrentColumnValue("p_Nivel"), 0)
        e.Cancel = Nivel <> 1
    End Sub

    Private Sub XrLine14_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLine14.BeforePrint
        Dim clinea As String = (SiEsNulo(GetCurrentColumnValue("a_sn_linea1"), 0)).ToString
        If ("0").IndexOf(clinea) <> -1 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub XrLineParcial1_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLineParcial1.BeforePrint
        Dim clinea As String = (SiEsNulo(GetCurrentColumnValue("a_sn_linea2"), 0)).ToString
        If ("0").IndexOf(clinea) <> -1 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub XrLine13_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLine13.BeforePrint
        Dim clinea As String = (SiEsNulo(GetCurrentColumnValue("p_sn_linea1"), 0)).ToString
        If ("0").IndexOf(clinea) <> -1 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub XrLine4_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLine4.BeforePrint
        Dim clinea As String = (SiEsNulo(GetCurrentColumnValue("p_sn_linea2"), 0)).ToString
        If ("0").IndexOf(clinea) <> -1 Then
            e.Cancel = True
        End If
    End Sub
    Private Sub Detail_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Detail.BeforePrint
        If SiEsNulo(GetCurrentColumnValue("a_Nivel"), 0) = 1 Then
            TotalActivo += GetCurrentColumnValue("a_Saldo")
        End If
        If SiEsNulo(GetCurrentColumnValue("p_Nivel"), 0) = 1 Then
            TotalPasivo += GetCurrentColumnValue("p_Saldo")
        End If
    End Sub

    Private Sub xrlTotal_A_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlTotal_A.BeforePrint
        xrlTotal_A.Text = Format(TotalActivo, "###,##0.00")
    End Sub

    Private Sub xrlTotal_P_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlTotal_P.BeforePrint
        xrlTotal_P.Text = Format(TotalPasivo, "###,##0.00")
    End Sub
End Class