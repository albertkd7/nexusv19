Public Class con_rptMovimientosCuenta
    Dim Saldo As Decimal = 0.0, Nat As Integer, Correl As Integer = 0, ImpresoSaldo As Boolean = False

    Private Sub Detail_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Detail.BeforePrint
        Nat = CInt(GetCurrentColumnValue("Naturaleza"))
        If Nat = 1 Or Nat = 4 Or Nat = 7 Then
            Saldo += CDec(GetCurrentColumnValue("Debe")) - CDec(GetCurrentColumnValue("Haber"))
        Else
            Saldo += CDec(GetCurrentColumnValue("Haber")) - CDec(GetCurrentColumnValue("Debe"))
        End If
        XrTableCell6.Text = Format(Saldo, "###,###,##0.00")
    End Sub

    Private Sub xrlSaldo_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldo.BeforePrint
        If Correl = 0 Then
            Saldo = GetCurrentColumnValue("SaldoAnterior")
        End If
        Correl += 1

    End Sub

    Private Sub PageHeader_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PageHeader.BeforePrint
        If ImpresoSaldo = False Then
            ImpresoSaldo = True
            xrlSaldo.Visible = True
            XrLabel7.Visible = True
        Else
            xrlSaldo.Visible = False
            XrLabel7.Visible = False
        End If
    End Sub
End Class