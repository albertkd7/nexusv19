<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class con_rptBalanceComprobReporte
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.dsContabilidad1 = New Nexus.dsContabilidad
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell
        Me.xrpFecha = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xrlPage = New DevExpress.XtraReports.UI.XRLabel
        Me.xrpNumero = New DevExpress.XtraReports.UI.XRPageInfo
        Me.xrlDescMoneda = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlPeriodo = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTitulo = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlEmpresa = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTotal = New DevExpress.XtraReports.UI.XRLabel
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
        Me.xrLineaFirma4 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlCargo5 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLineaFirma3 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlFirmante3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLineaFirma6 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlFirmante5 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCargo6 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFirmante6 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLineaFirma5 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlFirmante2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCargo1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCargo2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLineaFirma1 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlFirmante1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCargo3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFirmante4 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrLineaFirma2 = New DevExpress.XtraReports.UI.XRLine
        Me.xrlCargo4 = New DevExpress.XtraReports.UI.XRLabel
        CType(Me.dsContabilidad1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel14, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
        Me.Detail.HeightF = 14.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel14
        '
        Me.XrLabel14.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobacionReporte.SaldoFinal", "{0:n2}")})
        Me.XrLabel14.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(680.8134!, 0.0!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(96.35327!, 14.0!)
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        XrSummary1.FormatString = "{0:n2}"
        Me.XrLabel14.Summary = XrSummary1
        Me.XrLabel14.Text = "XrLabel6"
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel5
        '
        Me.XrLabel5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobacionReporte.TotalHaber", "{0:n2}")})
        Me.XrLabel5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(595.8134!, 0.0!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(85.0!, 14.0!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "XrLabel5"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel4
        '
        Me.XrLabel4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobacionReporte.TotalDebe", "{0:n2}")})
        Me.XrLabel4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(509.0!, 0.0!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(86.81339!, 14.0!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "XrLabel4"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel3
        '
        Me.XrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobacionReporte.SaldoInicial", "{0:n2}")})
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(425.8134!, 0.0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(83.18668!, 14.0!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "XrLabel3"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobacionReporte.Nombre")})
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(131.0!, 0.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(294.8134!, 14.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.Text = "XrLabel2"
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobacionReporte.IdCuenta")})
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(125.0!, 14.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.Text = "XrLabel1"
        '
        'dsContabilidad1
        '
        Me.dsContabilidad1.DataSetName = "dsContabilidad"
        Me.dsContabilidad1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1, Me.xrpFecha, Me.xrlPage, Me.xrpNumero, Me.xrlDescMoneda, Me.xrlPeriodo, Me.xrlTitulo, Me.xrlEmpresa})
        Me.PageHeader.HeightF = 116.6667!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrTable1
        '
        Me.XrTable1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                    Or DevExpress.XtraPrinting.BorderSide.Right) _
                    Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(1.333333!, 98.6667!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(775.8334!, 18.0!)
        Me.XrTable1.StylePriority.UseBorders = False
        Me.XrTable1.StylePriority.UseFont = False
        Me.XrTable1.StylePriority.UseTextAlignment = False
        Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell3, Me.XrTableCell4, Me.XrTableCell5, Me.XrTableCell7})
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 1
        '
        'XrTableCell1
        '
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.Text = "C�d.Cuenta"
        Me.XrTableCell1.Weight = 0.48134075586260716
        '
        'XrTableCell2
        '
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.Text = "Nombre de la Cuenta"
        Me.XrTableCell2.Weight = 1.1600428535724605
        '
        'XrTableCell3
        '
        Me.XrTableCell3.Name = "XrTableCell3"
        Me.XrTableCell3.Text = "Saldo Anterior"
        Me.XrTableCell3.Weight = 0.32867882578089658
        '
        'XrTableCell4
        '
        Me.XrTableCell4.Name = "XrTableCell4"
        Me.XrTableCell4.Text = "Debe"
        Me.XrTableCell4.Weight = 0.328678815655159
        '
        'XrTableCell5
        '
        Me.XrTableCell5.Name = "XrTableCell5"
        Me.XrTableCell5.Text = "Haber"
        Me.XrTableCell5.Weight = 0.32867882863710074
        '
        'XrTableCell7
        '
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.Text = "Saldo Final"
        Me.XrTableCell7.Weight = 0.372579920491776
        '
        'xrpFecha
        '
        Me.xrpFecha.Format = "{0:dd/MM/yyyy hh:mm tt}"
        Me.xrpFecha.LocationFloat = New DevExpress.Utils.PointFloat(641.0!, 0.0!)
        Me.xrpFecha.Name = "xrpFecha"
        Me.xrpFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpFecha.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.xrpFecha.SizeF = New System.Drawing.SizeF(125.0!, 16.0!)
        '
        'xrlPage
        '
        Me.xrlPage.LocationFloat = New DevExpress.Utils.PointFloat(509.0!, 0.0!)
        Me.xrlPage.Name = "xrlPage"
        Me.xrlPage.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPage.SizeF = New System.Drawing.SizeF(56.0!, 16.0!)
        Me.xrlPage.Text = "P�g. No."
        '
        'xrpNumero
        '
        Me.xrpNumero.LocationFloat = New DevExpress.Utils.PointFloat(569.0!, 0.0!)
        Me.xrpNumero.Name = "xrpNumero"
        Me.xrpNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpNumero.SizeF = New System.Drawing.SizeF(66.0!, 16.0!)
        '
        'xrlDescMoneda
        '
        Me.xrlDescMoneda.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlDescMoneda.LocationFloat = New DevExpress.Utils.PointFloat(9.0!, 79.0!)
        Me.xrlDescMoneda.Name = "xrlDescMoneda"
        Me.xrlDescMoneda.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDescMoneda.SizeF = New System.Drawing.SizeF(756.0!, 16.0!)
        Me.xrlDescMoneda.StylePriority.UseFont = False
        Me.xrlDescMoneda.StylePriority.UseTextAlignment = False
        Me.xrlDescMoneda.Text = "xrlDescMoneda"
        Me.xrlDescMoneda.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlPeriodo
        '
        Me.xrlPeriodo.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.xrlPeriodo.LocationFloat = New DevExpress.Utils.PointFloat(9.0!, 60.0!)
        Me.xrlPeriodo.Name = "xrlPeriodo"
        Me.xrlPeriodo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPeriodo.SizeF = New System.Drawing.SizeF(756.0!, 19.0!)
        Me.xrlPeriodo.StylePriority.UseFont = False
        Me.xrlPeriodo.StylePriority.UseTextAlignment = False
        Me.xrlPeriodo.Text = "xrlPeriodo"
        Me.xrlPeriodo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlTitulo
        '
        Me.xrlTitulo.Font = New System.Drawing.Font("Arial", 11.0!)
        Me.xrlTitulo.LocationFloat = New DevExpress.Utils.PointFloat(9.0!, 41.0!)
        Me.xrlTitulo.Name = "xrlTitulo"
        Me.xrlTitulo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTitulo.SizeF = New System.Drawing.SizeF(756.0!, 19.0!)
        Me.xrlTitulo.StylePriority.UseFont = False
        Me.xrlTitulo.StylePriority.UseTextAlignment = False
        Me.xrlTitulo.Text = "xrlTitulo"
        Me.xrlTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlEmpresa
        '
        Me.xrlEmpresa.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.xrlEmpresa.LocationFloat = New DevExpress.Utils.PointFloat(9.0!, 19.0!)
        Me.xrlEmpresa.Name = "xrlEmpresa"
        Me.xrlEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlEmpresa.SizeF = New System.Drawing.SizeF(756.0!, 22.0!)
        Me.xrlEmpresa.StylePriority.UseFont = False
        Me.xrlEmpresa.StylePriority.UseTextAlignment = False
        Me.xrlEmpresa.Text = "xrlEmpresa"
        Me.xrlEmpresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine2, Me.XrLabel15, Me.xrlTotal})
        Me.GroupFooter1.HeightF = 58.625!
        Me.GroupFooter1.Name = "GroupFooter1"
        Me.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'XrLine2
        '
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(550.0!, 0.0!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(228.0!, 6.0!)
        '
        'XrLabel15
        '
        Me.XrLabel15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(588.0!, 9.0!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.Text = "TOTAL"
        '
        'xrlTotal
        '
        Me.xrlTotal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobacionReporte.SaldoFinal")})
        Me.xrlTotal.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrlTotal.LocationFloat = New DevExpress.Utils.PointFloat(680.8134!, 9.000015!)
        Me.xrlTotal.Name = "xrlTotal"
        Me.xrlTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal.SizeF = New System.Drawing.SizeF(97.18658!, 17.00001!)
        Me.xrlTotal.StylePriority.UseFont = False
        Me.xrlTotal.StylePriority.UseTextAlignment = False
        XrSummary2.FormatString = "{0:n2}"
        XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.xrlTotal.Summary = XrSummary2
        Me.xrlTotal.Text = "xrlTotal"
        Me.xrlTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7})
        Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("titulo", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader1.HeightF = 21.0!
        Me.GroupHeader1.KeepTogether = True
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'XrLabel7
        '
        Me.XrLabel7.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "BalanceComprobacionReporte.titulo")})
        Me.XrLabel7.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(247.0!, 1.0!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(294.0!, 20.0!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "XrLabel7"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.HeightF = 40.0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.HeightF = 40.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrLineaFirma4, Me.xrlCargo5, Me.xrLineaFirma3, Me.xrlFirmante3, Me.xrLineaFirma6, Me.xrlFirmante5, Me.xrlCargo6, Me.xrlFirmante6, Me.xrLineaFirma5, Me.xrlFirmante2, Me.xrlCargo1, Me.xrlCargo2, Me.xrLineaFirma1, Me.xrlFirmante1, Me.xrlCargo3, Me.xrlFirmante4, Me.xrLineaFirma2, Me.xrlCargo4})
        Me.ReportFooter.HeightF = 230.4167!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.StylePriority.UseTextAlignment = False
        Me.ReportFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopJustify
        '
        'xrLineaFirma4
        '
        Me.xrLineaFirma4.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 103.5417!)
        Me.xrLineaFirma4.Name = "xrLineaFirma4"
        Me.xrLineaFirma4.SizeF = New System.Drawing.SizeF(222.0!, 2.0!)
        '
        'xrlCargo5
        '
        Me.xrlCargo5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo5.LocationFloat = New DevExpress.Utils.PointFloat(40.00004!, 213.0833!)
        Me.xrlCargo5.Name = "xrlCargo5"
        Me.xrlCargo5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo5.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo5.StylePriority.UseFont = False
        Me.xrlCargo5.StylePriority.UseTextAlignment = False
        Me.xrlCargo5.Text = "xrlCargo5"
        Me.xrlCargo5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma3
        '
        Me.xrLineaFirma3.LocationFloat = New DevExpress.Utils.PointFloat(39.99997!, 103.5417!)
        Me.xrLineaFirma3.Name = "xrLineaFirma3"
        Me.xrLineaFirma3.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlFirmante3
        '
        Me.xrlFirmante3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante3.LocationFloat = New DevExpress.Utils.PointFloat(39.99997!, 106.5417!)
        Me.xrlFirmante3.Name = "xrlFirmante3"
        Me.xrlFirmante3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante3.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante3.StylePriority.UseFont = False
        Me.xrlFirmante3.StylePriority.UseTextAlignment = False
        Me.xrlFirmante3.Text = "xrlFirmante3"
        Me.xrlFirmante3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma6
        '
        Me.xrLineaFirma6.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 196.0834!)
        Me.xrLineaFirma6.Name = "xrLineaFirma6"
        Me.xrLineaFirma6.SizeF = New System.Drawing.SizeF(222.0!, 3.0!)
        '
        'xrlFirmante5
        '
        Me.xrlFirmante5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante5.LocationFloat = New DevExpress.Utils.PointFloat(40.00004!, 200.0834!)
        Me.xrlFirmante5.Name = "xrlFirmante5"
        Me.xrlFirmante5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante5.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante5.StylePriority.UseFont = False
        Me.xrlFirmante5.StylePriority.UseTextAlignment = False
        Me.xrlFirmante5.Text = "xrlFirmante5"
        Me.xrlFirmante5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo6
        '
        Me.xrlCargo6.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo6.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 213.0833!)
        Me.xrlCargo6.Name = "xrlCargo6"
        Me.xrlCargo6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo6.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo6.StylePriority.UseFont = False
        Me.xrlCargo6.StylePriority.UseTextAlignment = False
        Me.xrlCargo6.Text = "xrlCargo6"
        Me.xrlCargo6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlFirmante6
        '
        Me.xrlFirmante6.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante6.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 200.0834!)
        Me.xrlFirmante6.Name = "xrlFirmante6"
        Me.xrlFirmante6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante6.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante6.StylePriority.UseFont = False
        Me.xrlFirmante6.StylePriority.UseTextAlignment = False
        Me.xrlFirmante6.Text = "xrlFirmante6"
        Me.xrlFirmante6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma5
        '
        Me.xrLineaFirma5.LocationFloat = New DevExpress.Utils.PointFloat(40.00004!, 196.0834!)
        Me.xrLineaFirma5.Name = "xrLineaFirma5"
        Me.xrLineaFirma5.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlFirmante2
        '
        Me.xrlFirmante2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante2.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 18.75004!)
        Me.xrlFirmante2.Name = "xrlFirmante2"
        Me.xrlFirmante2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante2.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante2.StylePriority.UseFont = False
        Me.xrlFirmante2.StylePriority.UseTextAlignment = False
        Me.xrlFirmante2.Text = "xrlFirmante2"
        Me.xrlFirmante2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo1
        '
        Me.xrlCargo1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo1.LocationFloat = New DevExpress.Utils.PointFloat(40.00004!, 31.75001!)
        Me.xrlCargo1.Name = "xrlCargo1"
        Me.xrlCargo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo1.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo1.StylePriority.UseFont = False
        Me.xrlCargo1.StylePriority.UseTextAlignment = False
        Me.xrlCargo1.Text = "xrlCargo1"
        Me.xrlCargo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo2
        '
        Me.xrlCargo2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo2.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 31.75001!)
        Me.xrlCargo2.Name = "xrlCargo2"
        Me.xrlCargo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo2.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo2.StylePriority.UseFont = False
        Me.xrlCargo2.StylePriority.UseTextAlignment = False
        Me.xrlCargo2.Text = "xrlCargo2"
        Me.xrlCargo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma1
        '
        Me.xrLineaFirma1.LocationFloat = New DevExpress.Utils.PointFloat(40.00004!, 15.75004!)
        Me.xrLineaFirma1.Name = "xrLineaFirma1"
        Me.xrLineaFirma1.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlFirmante1
        '
        Me.xrlFirmante1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante1.LocationFloat = New DevExpress.Utils.PointFloat(40.00004!, 18.75004!)
        Me.xrlFirmante1.Name = "xrlFirmante1"
        Me.xrlFirmante1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante1.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante1.StylePriority.UseFont = False
        Me.xrlFirmante1.StylePriority.UseTextAlignment = False
        Me.xrlFirmante1.Text = "xrlFirmante1"
        Me.xrlFirmante1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo3
        '
        Me.xrlCargo3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo3.LocationFloat = New DevExpress.Utils.PointFloat(39.99997!, 119.5417!)
        Me.xrlCargo3.Name = "xrlCargo3"
        Me.xrlCargo3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo3.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo3.StylePriority.UseFont = False
        Me.xrlCargo3.StylePriority.UseTextAlignment = False
        Me.xrlCargo3.Text = "xrlCargo3"
        Me.xrlCargo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlFirmante4
        '
        Me.xrlFirmante4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante4.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 106.5417!)
        Me.xrlFirmante4.Name = "xrlFirmante4"
        Me.xrlFirmante4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante4.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante4.StylePriority.UseFont = False
        Me.xrlFirmante4.StylePriority.UseTextAlignment = False
        Me.xrlFirmante4.Text = "xrlFirmante4"
        Me.xrlFirmante4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLineaFirma2
        '
        Me.xrLineaFirma2.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 15.75004!)
        Me.xrLineaFirma2.Name = "xrLineaFirma2"
        Me.xrLineaFirma2.SizeF = New System.Drawing.SizeF(222.0!, 2.0!)
        '
        'xrlCargo4
        '
        Me.xrlCargo4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo4.LocationFloat = New DevExpress.Utils.PointFloat(518.0!, 119.5417!)
        Me.xrlCargo4.Name = "xrlCargo4"
        Me.xrlCargo4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo4.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo4.StylePriority.UseFont = False
        Me.xrlCargo4.StylePriority.UseTextAlignment = False
        Me.xrlCargo4.Text = "xrlCargo4"
        Me.xrlCargo4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'con_rptBalanceComprobReporte
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.GroupFooter1, Me.GroupHeader1, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
        Me.DataMember = "BalanceComprobacionReporte"
        Me.DataSource = Me.dsContabilidad1
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(35, 35, 40, 40)
        Me.SnapGridSize = 2.0!
        Me.SnapToGrid = False
        Me.Version = "11.1"
        CType(Me.dsContabilidad1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents dsContabilidad1 As Nexus.dsContabilidad
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents xrlEmpresa As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTitulo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPeriodo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDescMoneda As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrpFecha As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents xrlPage As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrpNumero As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents xrLineaFirma4 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlCargo5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma6 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFirmante6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFirmante4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLineaFirma2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlCargo4 As DevExpress.XtraReports.UI.XRLabel
End Class
