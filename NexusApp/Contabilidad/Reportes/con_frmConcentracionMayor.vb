﻿Imports NexusBLL

Public Class con_frmConcentracionMayor
    Dim bl As New ContabilidadBLL(g_ConnectionString)


    Private Sub cmd1_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.rptConcentracionMayor(deFec1.DateTime, deFec2.DateTime, leTipoPartidas.EditValue, leSucursal.EditValue)

        Dim rpt As New con_rptConcentracionMayor() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = txtTitulo.Text
        rpt.xrlPeriodo.Text = FechaToString(deFec1.DateTime, deFec2.DateTime)
        rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
        rpt.ShowPreviewDialog()

    End Sub


    Private Sub con_frmConcentracionMayor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFec1.DateTime = Today
        deFec2.DateTime = Today
        objCombos.conTiposPartida(leTipoPartidas)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub


End Class