﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmEditorEstadosFinancierosNIIF
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.cheImprimeLineaBajo = New DevExpress.XtraEditors.CheckEdit
        Me.cheSubTitulo = New DevExpress.XtraEditors.CheckEdit
        Me.cheImprimirLineaArriba = New DevExpress.XtraEditors.CheckEdit
        Me.cheSubtotal = New DevExpress.XtraEditors.CheckEdit
        Me.cheCuentaPadre = New DevExpress.XtraEditors.CheckEdit
        Me.cboSumaResta = New DevExpress.XtraEditors.ComboBoxEdit
        Me.teCtaContable = New DevExpress.XtraEditors.TextEdit
        Me.beCtaContable = New DevExpress.XtraEditors.ButtonEdit
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.teConcepto = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.teNumeroLinea = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.btnVer = New DevExpress.XtraEditors.SimpleButton
        Me.leEFinanciero = New DevExpress.XtraEditors.LookUpEdit
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.cheImprimeLineaBajo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cheSubTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cheImprimirLineaArriba.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cheSubtotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cheCuentaPadre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboSumaResta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCtaContable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCtaContable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumeroLinea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leEFinanciero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 44)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage2
        Me.XtraTabControl1.Size = New System.Drawing.Size(773, 355)
        Me.XtraTabControl1.TabIndex = 3
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LabelControl3)
        Me.XtraTabPage2.Controls.Add(Me.cheImprimeLineaBajo)
        Me.XtraTabPage2.Controls.Add(Me.cheSubTitulo)
        Me.XtraTabPage2.Controls.Add(Me.cheImprimirLineaArriba)
        Me.XtraTabPage2.Controls.Add(Me.cheSubtotal)
        Me.XtraTabPage2.Controls.Add(Me.cheCuentaPadre)
        Me.XtraTabPage2.Controls.Add(Me.cboSumaResta)
        Me.XtraTabPage2.Controls.Add(Me.teCtaContable)
        Me.XtraTabPage2.Controls.Add(Me.beCtaContable)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl8)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl7)
        Me.XtraTabPage2.Controls.Add(Me.teConcepto)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl2)
        Me.XtraTabPage2.Controls.Add(Me.teNumeroLinea)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl1)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(767, 329)
        Me.XtraTabPage2.Text = "Datos del Concepto"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(26, 281)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(694, 13)
        Me.LabelControl3.TabIndex = 28
        Me.LabelControl3.Text = "NOTA:   El efecto a realizar para los conceptos que son Sub-Total queda anulado, " & _
            "pues el valor que mostrará, es el resultado de la suma y resta."
        '
        'cheImprimeLineaBajo
        '
        Me.cheImprimeLineaBajo.Location = New System.Drawing.Point(223, 215)
        Me.cheImprimeLineaBajo.Name = "cheImprimeLineaBajo"
        Me.cheImprimeLineaBajo.Properties.Caption = "Imprime Linea en la parte Inferior del Valor"
        Me.cheImprimeLineaBajo.Size = New System.Drawing.Size(239, 19)
        Me.cheImprimeLineaBajo.TabIndex = 27
        '
        'cheSubTitulo
        '
        Me.cheSubTitulo.Location = New System.Drawing.Point(223, 238)
        Me.cheSubTitulo.Name = "cheSubTitulo"
        Me.cheSubTitulo.Properties.Caption = "Es Sub-Titulo"
        Me.cheSubTitulo.Size = New System.Drawing.Size(100, 19)
        Me.cheSubTitulo.TabIndex = 26
        '
        'cheImprimirLineaArriba
        '
        Me.cheImprimirLineaArriba.Location = New System.Drawing.Point(223, 192)
        Me.cheImprimirLineaArriba.Name = "cheImprimirLineaArriba"
        Me.cheImprimirLineaArriba.Properties.Caption = "Imprimir una Linea en la parte Superior del Valor"
        Me.cheImprimirLineaArriba.Size = New System.Drawing.Size(275, 19)
        Me.cheImprimirLineaArriba.TabIndex = 25
        '
        'cheSubtotal
        '
        Me.cheSubtotal.Location = New System.Drawing.Point(223, 169)
        Me.cheSubtotal.Name = "cheSubtotal"
        Me.cheSubtotal.Properties.Caption = "Es Sub-Total"
        Me.cheSubtotal.Size = New System.Drawing.Size(100, 19)
        Me.cheSubtotal.TabIndex = 24
        '
        'cheCuentaPadre
        '
        Me.cheCuentaPadre.AutoSizeInLayoutControl = True
        Me.cheCuentaPadre.Location = New System.Drawing.Point(223, 146)
        Me.cheCuentaPadre.Name = "cheCuentaPadre"
        Me.cheCuentaPadre.Properties.Caption = "Es cuenta padre"
        Me.cheCuentaPadre.Size = New System.Drawing.Size(113, 19)
        Me.cheCuentaPadre.TabIndex = 23
        '
        'cboSumaResta
        '
        Me.cboSumaResta.EditValue = "Suma"
        Me.cboSumaResta.Location = New System.Drawing.Point(223, 78)
        Me.cboSumaResta.Name = "cboSumaResta"
        Me.cboSumaResta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboSumaResta.Properties.Items.AddRange(New Object() {"Suma", "Resta"})
        Me.cboSumaResta.Size = New System.Drawing.Size(100, 20)
        Me.cboSumaResta.TabIndex = 22
        '
        'teCtaContable
        '
        Me.teCtaContable.Enabled = False
        Me.teCtaContable.Location = New System.Drawing.Point(337, 105)
        Me.teCtaContable.Name = "teCtaContable"
        Me.teCtaContable.Properties.ReadOnly = True
        Me.teCtaContable.Size = New System.Drawing.Size(314, 20)
        Me.teCtaContable.TabIndex = 20
        '
        'beCtaContable
        '
        Me.beCtaContable.EnterMoveNextControl = True
        Me.beCtaContable.Location = New System.Drawing.Point(223, 103)
        Me.beCtaContable.Name = "beCtaContable"
        Me.beCtaContable.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beCtaContable.Size = New System.Drawing.Size(100, 20)
        Me.beCtaContable.TabIndex = 18
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(128, 103)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl8.TabIndex = 14
        Me.LabelControl8.Text = "Cuenta Contable:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(128, 78)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl7.TabIndex = 12
        Me.LabelControl7.Text = "Efecto a realizar :"
        '
        'teConcepto
        '
        Me.teConcepto.EnterMoveNextControl = True
        Me.teConcepto.Location = New System.Drawing.Point(223, 53)
        Me.teConcepto.Name = "teConcepto"
        Me.teConcepto.Size = New System.Drawing.Size(382, 20)
        Me.teConcepto.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(139, 53)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(74, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Concepto NIIF:"
        '
        'teNumeroLinea
        '
        Me.teNumeroLinea.EnterMoveNextControl = True
        Me.teNumeroLinea.Location = New System.Drawing.Point(223, 28)
        Me.teNumeroLinea.Name = "teNumeroLinea"
        Me.teNumeroLinea.Size = New System.Drawing.Size(100, 20)
        Me.teNumeroLinea.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(161, 28)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Ubicación :"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.gc)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(767, 329)
        Me.XtraTabPage1.Text = "Conceptos"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(662, 329)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv, Me.GridView2})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Ubicación"
        Me.GridColumn1.FieldName = "NumeroLinea"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 104
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Concepto NIIF"
        Me.GridColumn2.FieldName = "ConceptoNIIF"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 537
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "IdDetalle"
        Me.GridColumn3.FieldName = "IdDetalle"
        Me.GridColumn3.Name = "GridColumn3"
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.gc
        Me.GridView2.Name = "GridView2"
        '
        'btnVer
        '
        Me.btnVer.Location = New System.Drawing.Point(327, 12)
        Me.btnVer.Name = "btnVer"
        Me.btnVer.Size = New System.Drawing.Size(82, 23)
        Me.btnVer.TabIndex = 5
        Me.btnVer.Text = "Ver Detalle"
        '
        'leEFinanciero
        '
        Me.leEFinanciero.Location = New System.Drawing.Point(17, 12)
        Me.leEFinanciero.Name = "leEFinanciero"
        Me.leEFinanciero.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leEFinanciero.Size = New System.Drawing.Size(290, 20)
        Me.leEFinanciero.TabIndex = 6
        '
        'con_frmEditorEstadosFinancierosNIIF
        '
        Me.ClientSize = New System.Drawing.Size(790, 434)
        Me.Controls.Add(Me.leEFinanciero)
        Me.Controls.Add(Me.btnVer)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmEditorEstadosFinancierosNIIF"
        Me.OptionId = "001005"
        Me.Text = "Editor Estados Financieros"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.XtraTabControl1, 0)
        Me.Controls.SetChildIndex(Me.btnVer, 0)
        Me.Controls.SetChildIndex(Me.leEFinanciero, 0)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage2.ResumeLayout(False)
        Me.XtraTabPage2.PerformLayout()
        CType(Me.cheImprimeLineaBajo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cheSubTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cheImprimirLineaArriba.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cheSubtotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cheCuentaPadre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboSumaResta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCtaContable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCtaContable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumeroLinea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leEFinanciero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumeroLinea As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCtaContable As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCtaContable As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnVer As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents leEFinanciero As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cboSumaResta As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cheSubtotal As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cheCuentaPadre As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cheImprimeLineaBajo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cheSubTitulo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cheImprimirLineaArriba As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl

End Class
