﻿Imports NexusBLL
Public Class con_frmMovimientosCuenta
    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private Sub conMovimientosCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.con_CentrosCosto(leCentro, "", "-- TODOS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "--TODAS LAS SUCURSALES")
        leCentro.EditValue = -1
    End Sub

    Private Sub conMovimientosCuenta_Report_Click() Handles Me.Reporte
        If BeCtaContable1.beIdCuenta.EditValue = "" Then
            MsgBox("Debe de especificar la cuenta", MsgBoxStyle.Critical, "Nota")
            Return
        End If
        Dim dt As DataTable = bl.rptMovimientosCuenta(deDesde.EditValue, deHasta.EditValue, BeCtaContable1.beIdCuenta.EditValue, leCentro.EditValue, leSucursal.EditValue)

        Dim rpt As New con_rptMovimientosCuenta() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Movimientos de cuenta"
        rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
        rpt.xrlPeriodo.Text = FechaToString(deDesde.DateTime, deHasta.DateTime)

        rpt.ShowPreviewDialog()
    End Sub
End Class
