﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class con_frmEstadoResultadosCentro
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub con_frmEstadoResultadosCentro_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objCombos.con_CentrosCosto(leDesde, "", "-- TODOS LOS CENTROS DE COSTO --")
        objCombos.con_CentrosCosto(leHasta, "", "-- TODOS LOS CENTROS DE COSTO --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "--TODAS LAS SUCURSALES")
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        bl.ObtienePeriodoContable(Mes, Ejercicio)

        deFechaIni.EditValue = CDate("01/01/" & Ejercicio)
        deFechaFin.EditValue = Today
    End Sub

    Private Sub con_frmEstadoResultadosCentro_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.rep_EstadoResultadosCC(deFechaIni.EditValue, deFechaFin.EditValue, leDesde.EditValue, leHasta.EditValue, leSucursal.EditValue)
        Dim rpt As New con_rptEstadoResultadosCC() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlDescMoneda.Text = gsDesc_Moneda
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = FechaToString(deFechaIni.EditValue, deFechaFin.EditValue)
        rpt.ShowPreviewDialog()
    End Sub
End Class
