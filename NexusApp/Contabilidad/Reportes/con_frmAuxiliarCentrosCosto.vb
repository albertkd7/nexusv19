﻿Imports NexusBLL
Public Class con_frmAuxiliarCentrosCosto
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    
    Private Sub con_frmReporteCentrosDetalle_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFechaIni.EditValue = Today
        deFechaFin.EditValue = Today
        objCombos.con_CentrosCosto(leCentroCosto, "", "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub con_frmReporteCentrosDetalle_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = bl.rep_AuxiliarCentrosCosto(deFechaIni.DateTime, deFechaFin.DateTime, leCentroCosto.EditValue, BeCtaContable1.beIdCuenta.EditValue, BeCtaContable2.beIdCuenta.EditValue, leSucursal.EditValue)
        Dim rpt As New con_rptCentrosCostoDetalle() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = (FechaToString(deFechaIni.DateTime, deFechaFin.DateTime)).ToUpper
        rpt.ShowPreviewDialog()
    End Sub


End Class
