﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmComparacionAnual
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.seEjercicio = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.ceIncluir = New DevExpress.XtraEditors.CheckEdit
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.rgTipo = New DevExpress.XtraEditors.RadioGroup
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.spnNivel = New DevExpress.XtraEditors.SpinEdit
        Me.BeCtaContable1 = New Nexus.beCtaContable
        Me.BeCtaContable2 = New Nexus.beCtaContable
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceIncluir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spnNivel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.BeCtaContable2)
        Me.GroupControl1.Controls.Add(Me.BeCtaContable1)
        Me.GroupControl1.Controls.Add(Me.seEjercicio)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.ceIncluir)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.rgTipo)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.spnNivel)
        Me.GroupControl1.Size = New System.Drawing.Size(746, 331)
        '
        'seEjercicio
        '
        Me.seEjercicio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seEjercicio.Location = New System.Drawing.Point(143, 34)
        Me.seEjercicio.Name = "seEjercicio"
        Me.seEjercicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seEjercicio.Properties.Mask.EditMask = "f0"
        Me.seEjercicio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seEjercicio.Size = New System.Drawing.Size(69, 20)
        Me.seEjercicio.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(38, 37)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl1.TabIndex = 63
        Me.LabelControl1.Text = "Ejercicio a Comparar:"
        '
        'ceIncluir
        '
        Me.ceIncluir.Location = New System.Drawing.Point(141, 237)
        Me.ceIncluir.Name = "ceIncluir"
        Me.ceIncluir.Properties.Caption = "Incluir códigos de cuenta en el Reporte"
        Me.ceIncluir.Size = New System.Drawing.Size(226, 19)
        Me.ceIncluir.TabIndex = 5
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(70, 187)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(70, 13)
        Me.LabelControl7.TabIndex = 61
        Me.LabelControl7.Text = "Hasta Cuenta:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(68, 153)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl6.TabIndex = 62
        Me.LabelControl6.Text = "Desde Cuenta:"
        '
        'rgTipo
        '
        Me.rgTipo.EditValue = 1
        Me.rgTipo.Location = New System.Drawing.Point(143, 62)
        Me.rgTipo.Name = "rgTipo"
        Me.rgTipo.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Basado en Saldos Acumulados"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Basado en Saldos del mes")})
        Me.rgTipo.Size = New System.Drawing.Size(171, 52)
        Me.rgTipo.TabIndex = 1
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "Balance comparativo de saldos Anual"
        Me.teTitulo.Location = New System.Drawing.Point(143, 271)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(375, 20)
        Me.teTitulo.TabIndex = 6
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(54, 274)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl3.TabIndex = 60
        Me.LabelControl3.Text = "Título del informe:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(62, 127)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl2.TabIndex = 59
        Me.LabelControl2.Text = "Nivel de Detalle:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(21, 66)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(119, 13)
        Me.LabelControl5.TabIndex = 58
        Me.LabelControl5.Text = "Tipo de Reporte a Emitir:"
        '
        'spnNivel
        '
        Me.spnNivel.EditValue = New Decimal(New Integer() {4, 0, 0, 0})
        Me.spnNivel.Location = New System.Drawing.Point(143, 124)
        Me.spnNivel.Name = "spnNivel"
        Me.spnNivel.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.spnNivel.Size = New System.Drawing.Size(43, 20)
        Me.spnNivel.TabIndex = 2
        '
        'BeCtaContable1
        '
        Me.BeCtaContable1.Location = New System.Drawing.Point(140, 150)
        Me.BeCtaContable1.Name = "BeCtaContable1"
        Me.BeCtaContable1.Size = New System.Drawing.Size(594, 20)
        Me.BeCtaContable1.TabIndex = 3
        Me.BeCtaContable1.ValidarMayor = False
        '
        'BeCtaContable2
        '
        Me.BeCtaContable2.Location = New System.Drawing.Point(140, 183)
        Me.BeCtaContable2.Name = "BeCtaContable2"
        Me.BeCtaContable2.Size = New System.Drawing.Size(594, 20)
        Me.BeCtaContable2.TabIndex = 4
        Me.BeCtaContable2.ValidarMayor = False
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(90, 215)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl8.TabIndex = 65
        Me.LabelControl8.Text = "Sucursal:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(140, 211)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(375, 20)
        Me.leSucursal.TabIndex = 5
        '
        'con_frmComparacionAnual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(746, 356)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmComparacionAnual"
        Me.Text = "Comparación anual de cuentas"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceIncluir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spnNivel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents seEjercicio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceIncluir As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgTipo As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents spnNivel As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents BeCtaContable2 As Nexus.beCtaContable
    Friend WithEvents BeCtaContable1 As Nexus.beCtaContable
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit

End Class
