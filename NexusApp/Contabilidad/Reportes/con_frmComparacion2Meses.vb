﻿Imports NexusBLL
Public Class con_frmComparacion2Meses
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Private Sub con_frmComparacion2Meses_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        bl.ObtienePeriodoContable(Mes, Ejercicio)
        meMes1.Month = Mes
        seAnio1.EditValue = Ejercicio
        meMes2.Month = Mes
        seAnio2.EditValue = Ejercicio
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub con_frmComparacion2Meses_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = bl.rptComparacion2Meses(meMes1.Month, seAnio1.EditValue, meMes2.Month, seAnio2.EditValue, spnNivel.EditValue, rgTipoReporte.EditValue, BeCtaContable1.beIdCuenta.EditValue, BeCtaContable2.beIdCuenta.EditValue, leSucursal.EditValue)
        Dim rpt As New con_rptComparacion2Meses() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlDescMoneda.Text = gsDesc_Moneda
        rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
        rpt.xrlMes1.Text = meMes1.Text & "/" & seAnio1.Text
        rpt.xrlMes2.Text = meMes2.Text & "/" & seAnio2.Text
        rpt.ShowPreview()
    End Sub
End Class
