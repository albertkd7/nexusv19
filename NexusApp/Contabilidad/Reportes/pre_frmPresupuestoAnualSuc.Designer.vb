﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pre_frmPresupuestoAnualSuc
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.seAnio = New DevExpress.XtraEditors.SpinEdit
        Me.lcSucursal = New DevExpress.XtraEditors.LabelControl
        Me.lueSucursales = New DevExpress.XtraEditors.LookUpEdit
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lueSucursales.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.seAnio)
        Me.GroupControl1.Controls.Add(Me.lcSucursal)
        Me.GroupControl1.Controls.Add(Me.lueSucursales)
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(63, 77)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl3.TabIndex = 103
        Me.LabelControl3.Text = "Año / Ejercicio:"
        '
        'seAnio
        '
        Me.seAnio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seAnio.Location = New System.Drawing.Point(141, 74)
        Me.seAnio.Name = "seAnio"
        Me.seAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seAnio.Properties.IsFloatValue = False
        Me.seAnio.Properties.Mask.EditMask = "N00"
        Me.seAnio.Size = New System.Drawing.Size(99, 20)
        Me.seAnio.TabIndex = 102
        '
        'lcSucursal
        '
        Me.lcSucursal.Location = New System.Drawing.Point(91, 39)
        Me.lcSucursal.Name = "lcSucursal"
        Me.lcSucursal.Size = New System.Drawing.Size(44, 13)
        Me.lcSucursal.TabIndex = 99
        Me.lcSucursal.Text = "Sucursal:"
        '
        'lueSucursales
        '
        Me.lueSucursales.Location = New System.Drawing.Point(141, 36)
        Me.lueSucursales.Name = "lueSucursales"
        Me.lueSucursales.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueSucursales.Size = New System.Drawing.Size(251, 20)
        Me.lueSucursales.TabIndex = 98
        '
        'pre_frmPresupuestoAnualSuc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(839, 314)
        Me.Modulo = "Contabilidad"
        Me.Name = "pre_frmPresupuestoAnualSuc"
        Me.Text = "Presupuesto Anual por Sucursal"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lueSucursales.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents lcSucursal As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lueSucursales As DevExpress.XtraEditors.LookUpEdit

End Class
