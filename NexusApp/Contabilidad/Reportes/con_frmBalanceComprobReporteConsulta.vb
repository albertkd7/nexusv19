﻿Imports NexusELL.TableEntities
Imports NexusBLL

Public Class con_frmBalanceComprobReporteConsulta
    Dim bl As New ContabilidadBLL(g_ConnectionString), blAdmon As New AdmonBLL(g_ConnectionString)
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)


    Private Sub con_frmBalanceComprobReporte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        bl.ObtienePeriodoContable(Mes, Ejercicio)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "--TODAS LAS SUCURSALES")
        leSucursal.EditValue = piIdSucursalUsuario
        meMes.Month = Mes
        seEjercicio.Text = Ejercicio
    End Sub

 
    Private Sub GeneraDatos()
        If SiEsNulo(leSucursal.EditValue, 0) = 0 Or SiEsNulo(meMes.EditValue, 0) = 0 Or SiEsNulo(seEjercicio.EditValue, 0) = 0 Then
            Exit Sub
        End If
        gc.DataSource = bl.con_rptBalanceComprobacionReporte(meMes.Month, seEjercicio.EditValue, spnNivel.EditValue, 1, leSucursal.EditValue)
        gv.BestFitColumns()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        GeneraDatos()
    End Sub

    Private Sub meMes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles meMes.SelectedIndexChanged
        GeneraDatos()
    End Sub

    Private Sub seEjercicio_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles seEjercicio.EditValueChanged
        GeneraDatos()
    End Sub

    Private Sub spnNivel_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles spnNivel.EditValueChanged
        GeneraDatos()
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc.DoubleClick

        If Not gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns("EsTransaccional")) Then
            MsgBox("La cuenta no es transaccional", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        Dim tmpFecha As String = String.Format("01/{0}/{1}", meMes.EditValue, seEjercicio.EditValue)
        Dim FechaIni As Date = CDate(tmpFecha)
        Dim FechaFin As Date = DateAdd(DateInterval.Month, 1, FechaIni)
        FechaFin = DateAdd(DateInterval.Day, -1, FechaFin)

        Dim IdCuenta As String = gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns("IdCuenta"))
        Dim dt As DataTable = bl.rptMovimientosCuenta(FechaIni, FechaFin, IdCuenta, "-1", leSucursal.EditValue)
        Dim rpt As New con_rptMovimientosCuenta() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Movimientos de cuenta"
        rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
        rpt.xrlPeriodo.Text = FechaToString(FechaIni, FechaFin)

        rpt.ShowPreviewDialog()
    End Sub
End Class
