﻿Imports NexusBLL
Public Class con_frmConcentracionPartidas
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    

    Private Sub con_frmConcentracionPartidas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        DateEdit1.EditValue = Today
        DateEdit2.EditValue = Today
        teDesde.EditValue = ""
        teHasta.EditValue = ""
        objCombos.conTiposPartida(leTipoPartida, "-- TODOS LOS TIPOS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub con_frmConcentracionPartidas_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.rptPartidasConcentradas(leTipoPartida.EditValue, DateEdit1.EditValue, DateEdit2.EditValue, teDesde.EditValue, teHasta.EditValue, leSucursal.EditValue)
        Dim rpt As New con_rptPartidaConcentrada() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "CONCENTRACION DE PARTIDAS - " & leTipoPartida.Text
        rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
        rpt.xrlPeriodo.Text = (FechaToString(DateEdit1.EditValue, DateEdit2.EditValue)).ToUpper

        rpt.ShowPreviewDialog()
    End Sub

    Private Sub GroupControl1_Paint(sender As Object, e As PaintEventArgs) Handles GroupControl1.Paint

    End Sub
End Class
