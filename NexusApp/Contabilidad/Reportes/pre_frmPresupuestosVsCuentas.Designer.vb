﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pre_frmPresupuestosVsCuentas
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.seAnio = New DevExpress.XtraEditors.SpinEdit
        Me.lueMes = New DevExpress.XtraScheduler.UI.MonthEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.lueCentroCostos = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.lueDepartamentos = New DevExpress.XtraEditors.LookUpEdit
        Me.lcSucursal = New DevExpress.XtraEditors.LabelControl
        Me.lueSucursales = New DevExpress.XtraEditors.LookUpEdit
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lueMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lueCentroCostos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lueDepartamentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lueSucursales.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.lueCentroCostos)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.lueDepartamentos)
        Me.GroupControl1.Controls.Add(Me.lcSucursal)
        Me.GroupControl1.Controls.Add(Me.lueSucursales)
        Me.GroupControl1.Controls.Add(Me.lueMes)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.seAnio)
        Me.GroupControl1.Size = New System.Drawing.Size(839, 286)
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(71, 134)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(23, 13)
        Me.LabelControl4.TabIndex = 105
        Me.LabelControl4.Text = "Mes:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(22, 160)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl3.TabIndex = 104
        Me.LabelControl3.Text = "Año / Ejercicio:"
        '
        'seAnio
        '
        Me.seAnio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seAnio.Location = New System.Drawing.Point(100, 157)
        Me.seAnio.Name = "seAnio"
        Me.seAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seAnio.Properties.IsFloatValue = False
        Me.seAnio.Properties.Mask.EditMask = "N00"
        Me.seAnio.Size = New System.Drawing.Size(126, 20)
        Me.seAnio.TabIndex = 1
        '
        'lueMes
        '
        Me.lueMes.Location = New System.Drawing.Point(100, 131)
        Me.lueMes.Name = "lueMes"
        Me.lueMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueMes.Size = New System.Drawing.Size(126, 20)
        Me.lueMes.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(8, 103)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl2.TabIndex = 111
        Me.LabelControl2.Text = "Centro de costos:"
        '
        'lueCentroCostos
        '
        Me.lueCentroCostos.Location = New System.Drawing.Point(100, 100)
        Me.lueCentroCostos.Name = "lueCentroCostos"
        Me.lueCentroCostos.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueCentroCostos.Size = New System.Drawing.Size(278, 20)
        Me.lueCentroCostos.TabIndex = 110
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(21, 75)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl1.TabIndex = 109
        Me.LabelControl1.Text = "Departamento:"
        '
        'lueDepartamentos
        '
        Me.lueDepartamentos.Location = New System.Drawing.Point(100, 72)
        Me.lueDepartamentos.Name = "lueDepartamentos"
        Me.lueDepartamentos.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueDepartamentos.Size = New System.Drawing.Size(278, 20)
        Me.lueDepartamentos.TabIndex = 108
        '
        'lcSucursal
        '
        Me.lcSucursal.Location = New System.Drawing.Point(50, 49)
        Me.lcSucursal.Name = "lcSucursal"
        Me.lcSucursal.Size = New System.Drawing.Size(44, 13)
        Me.lcSucursal.TabIndex = 107
        Me.lcSucursal.Text = "Sucursal:"
        '
        'lueSucursales
        '
        Me.lueSucursales.Location = New System.Drawing.Point(100, 44)
        Me.lueSucursales.Name = "lueSucursales"
        Me.lueSucursales.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueSucursales.Size = New System.Drawing.Size(278, 20)
        Me.lueSucursales.TabIndex = 106
        '
        'pre_frmPresupuestosVsCuentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(839, 314)
        Me.Modulo = "Contabilidad"
        Me.Name = "pre_frmPresupuestosVsCuentas"
        Me.Text = "Presupuestos Vs Costos reales"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lueMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lueCentroCostos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lueDepartamentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lueSucursales.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents lueMes As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lueCentroCostos As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lueDepartamentos As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lcSucursal As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lueSucursales As DevExpress.XtraEditors.LookUpEdit

End Class
