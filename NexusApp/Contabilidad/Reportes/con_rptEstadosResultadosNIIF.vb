﻿Public Class con_rptEstadosResultadosNIIF
    Private dTotalIngreso As Decimal = 0
    Private dTotalGasto As Decimal = 0
    Private dTotalIngreso2 As Decimal = 0
    Private dTotalGasto2 As Decimal = 0

    Private Sub XrLine2_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLine2.BeforePrint
        If GetCurrentColumnValue("Imprimir") Then
            XrLine2.Visible = True
        Else
            XrLine2.Visible = False
        End If
    End Sub

    Private Sub xrlResultado1_SummaryRowChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles xrlResultado1.SummaryRowChanged
        If SiEsNulo(GetCurrentColumnValue("Tipo"), 0) = 1 Then 'SiEsNulo(GetCurrentColumnValue("Nivel"), 0) = 1 And
            dTotalIngreso += GetCurrentColumnValue("Anterior")
        End If
        If SiEsNulo(GetCurrentColumnValue("Tipo"), 0) = 2 Then 'SiEsNulo(GetCurrentColumnValue("Nivel"), 0) = 1 And
            dTotalGasto += GetCurrentColumnValue("Anterior")
        End If
    End Sub

    Private Sub xrlResultado2_SummaryRowChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles xrlResultado2.SummaryRowChanged
        If SiEsNulo(GetCurrentColumnValue("Tipo"), 0) = 1 Then 'SiEsNulo(GetCurrentColumnValue("Nivel"), 0) = 1 And
            dTotalIngreso2 += GetCurrentColumnValue("Actual")
        End If
        If SiEsNulo(GetCurrentColumnValue("Tipo"), 0) = 2 Then 'SiEsNulo(GetCurrentColumnValue("Nivel"), 0) = 1 And
            dTotalGasto2 += GetCurrentColumnValue("Actual")
        End If
    End Sub

    Private Sub xrlResultado1_SummaryGetResult(ByVal sender As System.Object, ByVal e As DevExpress.XtraReports.UI.SummaryGetResultEventArgs) Handles xrlResultado1.SummaryGetResult
        e.Result = dTotalIngreso - dTotalGasto
        e.Handled = True
    End Sub

    Private Sub xrlResultado2_SummaryGetResult(ByVal sender As System.Object, ByVal e As DevExpress.XtraReports.UI.SummaryGetResultEventArgs) Handles xrlResultado2.SummaryGetResult
        e.Result = dTotalIngreso2 - dTotalGasto2
        e.Handled = True
    End Sub
End Class