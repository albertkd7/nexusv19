﻿Imports NexusELL.TableEntities
Public Class con_frmEditorEstadosFinancierosNIIF
    Dim entConceptoNIIF As con_NIIFConceptos
    Dim bl As New NexusBLL.ContabilidadBLL(g_ConnectionString)
    Dim CuentaAnterior As String
    Private Sub con_frmEditorEstadosFinancierosNIIF_Eliminar() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el concepto seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            objTablas.con_NIIFConceptosDeleteByPK(Me.leEFinanciero.EditValue, gv.GetRowCellValue(gv.FocusedRowHandle, "IdDetalle"))
            gc.DataSource = bl.con_ObtenerDetalleReporteNIIF(leEFinanciero.EditValue)
        End If
    End Sub

    Private Sub con_frmEditorEstadosFinancierosNIIF_Guardar() Handles Me.Guardar
        ''Validando la cuenta contable
        Dim ValorDeBusqueda As Integer = 0
        If DbMode = DbModeType.update Then
            If CuentaAnterior <> Me.beCtaContable.EditValue Then
                ValorDeBusqueda = 0  ''Cambió la cuenta contable
            Else
                ValorDeBusqueda = 1  ''Se está consultando a si misma, por esa razón ya existe; es mejor incrementar.
            End If
        Else
            ValorDeBusqueda = 0
        End If

        Dim dt As DataTable = CType(gc.DataSource, DataTable)
        Dim FilaEncontrada() As Data.DataRow
        FilaEncontrada = dt.Select("IdCuentaContable = '" + Me.beCtaContable.EditValue + "'")
        If FilaEncontrada.Length > ValorDeBusqueda And Me.beCtaContable.EditValue <> "" Then
            MsgBox("No se puede repetir una cuenta Contable")
            beCtaContable.EditValue = ""
            teCtaContable.EditValue = ""
            Exit Sub
        End If

        'Validadno Sub-total
        If Not VerificarSubTotal() Then
            MsgBox("Proceso Cancelado", MsgBoxStyle.OkOnly, "Cancelado")
            Exit Sub
        End If
        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.con_NIIFConceptosInsert(entConceptoNIIF)
        Else
            objTablas.con_NIIFConceptosUpdate(entConceptoNIIF)
        End If

        DbMode = DbModeType.insert
        XtraTabControl1.SelectedTabPage = XtraTabPage1
        gc.DataSource = bl.con_ObtenerDetalleReporteNIIF(leEFinanciero.EditValue)
        gc.Focus()
        MostrarModoInicial()
    End Sub

    Private Sub con_frmEditorEstadosResultadosNIIF_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objCombos.con_CargarEstadosFinancierosNIIF(leEFinanciero)
        XtraTabControl1.SelectedTabPage = XtraTabPage1
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc.DoubleClick
        CargaPantalla()
        MostrarModoEditar()
        teNumeroLinea.Focus()
    End Sub

    Private Sub CargaPantalla()
        XtraTabControl1.SelectedTabPage = XtraTabPage2
        With entConceptoNIIF
            teNumeroLinea.EditValue = .NumeroLinea
            teConcepto.Text = .ConceptoNIIF
            cboSumaResta.SelectedIndex = IIf(.Resta, 1, 0)
            beCtaContable.EditValue = .IdCuentaContable
            cheCuentaPadre.EditValue = .CuentaPadre
            cheSubtotal.Checked = .SubTotal
            cheImprimirLineaArriba.Checked = .ImprimirLinea
            cheImprimeLineaBajo.Checked = .ImprimirLineaBajo
            cheSubTitulo.Checked = .EsSubTitulo
            Dim entCuentas As con_Cuentas = objTablas.con_CuentasSelectByPK(.IdCuentaContable)
            beCtaContable.EditValue = entCuentas.IdCuenta
            teCtaContable.EditValue = entCuentas.Nombre
        End With
    End Sub
    Private Sub CargaEntidad()
        With entConceptoNIIF
            .NumeroLinea = teNumeroLinea.EditValue
            .ConceptoNIIF = teConcepto.Text
            .Resta = cboSumaResta.SelectedIndex
            .IdCuentaContable = beCtaContable.EditValue
            .CuentaPadre = cheCuentaPadre.EditValue
            .SubTotal = cheSubtotal.EditValue
            .ImprimirLinea = cheImprimirLineaArriba.EditValue
            .ImprimirLineaBajo = cheImprimeLineaBajo.EditValue
            .EsSubTitulo = cheSubTitulo.EditValue
        End With
    End Sub

    Private Sub XtraTabControl1_SelectedPageChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles XtraTabControl1.SelectedPageChanged
        If e.Page.Name = "XtraTabPage2" And DbMode = DbModeType.Update Then
            entConceptoNIIF = objTablas.con_NIIFConceptosSelectByPK(Me.leEFinanciero.EditValue, gv.GetRowCellValue(gv.FocusedRowHandle, "IdDetalle"))
            CuentaAnterior = entConceptoNIIF.IdCuentaContable
            CargaPantalla()
        End If
    End Sub

    Private Sub inv_frmGrupos_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub

    Private Sub btnVer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVer.Click
        gc.DataSource = bl.con_ObtenerDetalleReporteNIIF(leEFinanciero.EditValue)
    End Sub

    Private Sub con_frmEditorEstadosResultadosNIIF_Nuevo() Handles MyBase.Nuevo
        entConceptoNIIF = New con_NIIFConceptos
        entConceptoNIIF.IdReporte = leEFinanciero.EditValue
        entConceptoNIIF.IdDetalle = bl.con_ObtenerUltimoIdDetalleNIIF(leEFinanciero.EditValue) + 1
        CargaPantalla()
    End Sub



    Private Sub beCtaContable_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCtaContable.Validated
        Dim entCuentas As con_Cuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCtaContable.EditValue)
        beCtaContable.EditValue = entCuentas.IdCuenta
        teCtaContable.EditValue = entCuentas.Nombre
    End Sub

    Private Sub cheSubtotal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cheSubtotal.CheckedChanged
        VerificarSubTotal()
    End Sub
    Function VerificarSubTotal() As Boolean
        ''Si se corrigió el problema la función retornará true, de lo contrario retornará false.
        If Me.beCtaContable.EditValue <> "" And Me.cheSubtotal.Checked Then
            If MsgBox("Los conceptos que son Sub-Totales, NO deben de poseer nada en la casilla Cuenta Contable, ¿Desea borrar la cuenta contable para este concepto?", MsgBoxStyle.YesNo, "Verificar") = MsgBoxResult.Yes Then
                Me.beCtaContable.EditValue = ""
                Me.teCtaContable.EditValue = ""
                Me.cboSumaResta.Focus()
                Return True
            Else
                cheSubtotal.Checked = False
                Return False
            End If
        End If
        Return True
    End Function

    Private Sub con_frmEditorEstadosFinancierosNIIF_Revertir() Handles MyBase.Revertir
        DbMode = DbModeType.insert
        XtraTabControl1.SelectedTabPage = XtraTabPage1
        gc.DataSource = bl.con_ObtenerDetalleReporteNIIF(leEFinanciero.EditValue)
        gc.Focus()
        MostrarModoInicial()
    End Sub
End Class
