﻿Imports NexusBLL

Public Class pre_frmPresupuestosGeneral

    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private Sub pre_frmPresupuestosPorSucursal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lueMes.EditValue = Today.Month
        seAnio.Value = DateTime.Now.Year
    End Sub

    Private Sub pre_frmPresupuestosPorSucursal_Reporte() Handles Me.Reporte

        If lueMes.EditValue = 0 Then
            If MsgBox("No ha seleccionado un mes, Se tomarán todos los presupuestos del año " & seAnio.Value & " ¿Desea continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return
            End If
        End If

        Dim orden As String = rgOrdenadoPor.EditValue

        Select Case orden
            Case "succcdepto"
                Dim rpt As New pre_rptPresupuestosSucCCDepto

                If lueMes.EditValue = 0 Then
                    rpt.DataSource = bl.pre_PresupuestoPorSucursal(seAnio.EditValue)
                Else
                    rpt.DataSource = bl.pre_PresupuestoPorSucursal(seAnio.EditValue, lueMes.EditValue)
                End If

                rpt.DataMember = ""

                rpt.xrlTituloEmpresa.Text = gsNombre_Empresa
                rpt.xrlNombreReporte.Text = "REPORTE DE PRESUPUESTOS GENERALIZADO POR SUCURSALES."
                rpt.xrlOtrainfo.Text = "CORRESPONDIENTE AL MES DE " & ObtieneMesString(lueMes.EditValue) & " DEL AÑO " & seAnio.EditValue

                rpt.ShowPreviewDialog()
            Case "sucdeptocc"
                Dim rpt As New pre_rptPresupuestosSucDeptoCC

                If lueMes.EditValue = 0 Then
                    rpt.DataSource = bl.pre_PresupuestoPorSucursal(seAnio.EditValue)
                Else
                    rpt.DataSource = bl.pre_PresupuestoPorSucursal(seAnio.EditValue, lueMes.EditValue)
                End If

                rpt.DataMember = ""

                rpt.xrlTituloEmpresa.Text = gsNombre_Empresa
                rpt.xrlNombreReporte.Text = "REPORTE DE PRESUPUESTOS GENERALIZADO POR SUCURSAL."
                rpt.xrlOtrainfo.Text = "CORRESPONDIENTE AL MES DE " & ObtieneMesString(lueMes.EditValue) & " DEL AÑO " & seAnio.EditValue

                rpt.ShowPreviewDialog()
            Case "deptoccsuc"
                Dim rpt As New pre_rptPresupuestosDeptoCCSuc

                If lueMes.EditValue = 0 Then
                    rpt.DataSource = bl.pre_PresupuestoPorSucursal(seAnio.EditValue)
                Else
                    rpt.DataSource = bl.pre_PresupuestoPorSucursal(seAnio.EditValue, lueMes.EditValue)
                End If

                rpt.DataMember = ""

                rpt.xrlTituloEmpresa.Text = gsNombre_Empresa
                rpt.xrlNombreReporte.Text = "REPORTE DE PRESUPUESTOS GENERALIZADO POR DEPARTAMENTO."
                rpt.xrlOtrainfo.Text = "CORRESPONDIENTE AL MES DE " & ObtieneMesString(lueMes.EditValue) & " DEL AÑO " & seAnio.EditValue

                rpt.ShowPreviewDialog()
            Case "deptosuccc"
                Dim rpt As New pre_rptPresupuestosDeptoSucCC

                If lueMes.EditValue = 0 Then
                    rpt.DataSource = bl.pre_PresupuestoPorSucursal(seAnio.EditValue)
                Else
                    rpt.DataSource = bl.pre_PresupuestoPorSucursal(seAnio.EditValue, lueMes.EditValue)
                End If

                rpt.DataMember = ""

                rpt.xrlTituloEmpresa.Text = gsNombre_Empresa
                rpt.xrlNombreReporte.Text = "REPORTE DE PRESUPUESTOS GENERALIZADO POR DEPARTAMENTO."
                rpt.xrlOtrainfo.Text = "CORRESPONDIENTE AL MES DE " & ObtieneMesString(lueMes.EditValue) & " DEL AÑO " & seAnio.EditValue

                rpt.ShowPreviewDialog()
            Case "ccsucdepto"
                Dim rpt As New pre_rptPresupuestosCCSucDepto

                If lueMes.EditValue = 0 Then
                    rpt.DataSource = bl.pre_PresupuestoPorSucursal(seAnio.EditValue)
                Else
                    rpt.DataSource = bl.pre_PresupuestoPorSucursal(seAnio.EditValue, lueMes.EditValue)
                End If

                rpt.DataMember = ""

                rpt.xrlTituloEmpresa.Text = gsNombre_Empresa
                rpt.xrlNombreReporte.Text = "REPORTE DE PRESUPUESTOS GENERALIZADO POR CENTRO DE COSTOS."
                rpt.xrlOtrainfo.Text = "CORRESPONDIENTE AL MES DE " & ObtieneMesString(lueMes.EditValue) & " DEL AÑO " & seAnio.EditValue

                rpt.ShowPreviewDialog()
            Case Else
                MsgBox("Debe de seleccionar un ordén de reporte antes de poder visualizarlo.", MsgBoxStyle.Critical)
        End Select


    End Sub
End Class
