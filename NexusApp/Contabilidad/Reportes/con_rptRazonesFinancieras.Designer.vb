﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class con_rptRazonesFinancieras
	Inherits DevExpress.XtraReports.UI.XtraReport

	'XtraReport overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing AndAlso components IsNot Nothing Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Designer
	'It can be modified using the Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.XrLabel39 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel33 = New DevExpress.XtraReports.UI.XRLabel()
		Me.xrlPage = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel40 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel32 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel34 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLine13 = New DevExpress.XtraReports.UI.XRLine()
		Me.xrpNumero = New DevExpress.XtraReports.UI.XRPageInfo()
		Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
		Me.xrlTitulo = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
		Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
		Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLine5 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLine10 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel42 = New DevExpress.XtraReports.UI.XRLabel()
		Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
		Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLine9 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
		Me.xrlSaldoNivel2p = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel43 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel35 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel37 = New DevExpress.XtraReports.UI.XRLabel()
		Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
		Me.XrLabel53 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLine21 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLabel52 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel51 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel50 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel49 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel48 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel47 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel46 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel45 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel44 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLine18 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLine19 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLine20 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLine15 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLine16 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLine17 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLine14 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLine12 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLabel41 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel38 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel36 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLine11 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLine8 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLine7 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLine6 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLine4 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine()
		Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
		Me.xrpFecha = New DevExpress.XtraReports.UI.XRPageInfo()
		Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
		Me.xrlPeriodo = New DevExpress.XtraReports.UI.XRLabel()
		Me.xrlEmpresa = New DevExpress.XtraReports.UI.XRLabel()
		Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
		Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
		Me.DsContabilidad1 = New Nexus.dsContabilidad()
		CType(Me.DsContabilidad1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
		'
		'XrLabel39
		'
		Me.XrLabel39.Dpi = 100.0!
		Me.XrLabel39.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel39.LocationFloat = New DevExpress.Utils.PointFloat(556.8541!, 562.1875!)
		Me.XrLabel39.Name = "XrLabel39"
		Me.XrLabel39.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel39.SizeF = New System.Drawing.SizeF(50.95831!, 19.0!)
		Me.XrLabel39.StylePriority.UseFont = False
		Me.XrLabel39.StylePriority.UseTextAlignment = False
		Me.XrLabel39.Text = "= Días"
		Me.XrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel22
		'
		Me.XrLabel22.Dpi = 100.0!
		Me.XrLabel22.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(349.2917!, 500.3751!)
		Me.XrLabel22.Name = "XrLabel22"
		Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel22.SizeF = New System.Drawing.SizeF(161.9583!, 18.0!)
		Me.XrLabel22.StylePriority.UseFont = False
		Me.XrLabel22.StylePriority.UseTextAlignment = False
		Me.XrLabel22.Text = "Cuentas por Cobrar"
		Me.XrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLabel33
		'
		Me.XrLabel33.Dpi = 100.0!
		Me.XrLabel33.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel33.LocationFloat = New DevExpress.Utils.PointFloat(315.7917!, 862.9374!)
		Me.XrLabel33.Name = "XrLabel33"
		Me.XrLabel33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel33.SizeF = New System.Drawing.SizeF(228.9583!, 19.0!)
		Me.XrLabel33.StylePriority.UseFont = False
		Me.XrLabel33.StylePriority.UseTextAlignment = False
		Me.XrLabel33.Text = "Pasivo Total"
		Me.XrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'xrlPage
		'
		Me.xrlPage.Dpi = 100.0!
		Me.xrlPage.LocationFloat = New DevExpress.Utils.PointFloat(532.0!, 1.0!)
		Me.xrlPage.Name = "xrlPage"
		Me.xrlPage.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrlPage.SizeF = New System.Drawing.SizeF(56.0!, 15.0!)
		Me.xrlPage.Text = "Pág. No."
		'
		'XrLabel40
		'
		Me.XrLabel40.Dpi = 100.0!
		Me.XrLabel40.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel40.LocationFloat = New DevExpress.Utils.PointFloat(556.8541!, 643.4375!)
		Me.XrLabel40.Name = "XrLabel40"
		Me.XrLabel40.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel40.SizeF = New System.Drawing.SizeF(50.95831!, 19.0!)
		Me.XrLabel40.StylePriority.UseFont = False
		Me.XrLabel40.StylePriority.UseTextAlignment = False
		Me.XrLabel40.Text = "= Días"
		Me.XrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel32
		'
		Me.XrLabel32.Dpi = 100.0!
		Me.XrLabel32.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel32.LocationFloat = New DevExpress.Utils.PointFloat(349.2917!, 885.0208!)
		Me.XrLabel32.Name = "XrLabel32"
		Me.XrLabel32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel32.SizeF = New System.Drawing.SizeF(161.9583!, 18.0!)
		Me.XrLabel32.StylePriority.UseFont = False
		Me.XrLabel32.StylePriority.UseTextAlignment = False
		Me.XrLabel32.Text = "Patrimonio"
		Me.XrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLabel34
		'
		Me.XrLabel34.Dpi = 100.0!
		Me.XrLabel34.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel34.LocationFloat = New DevExpress.Utils.PointFloat(556.8541!, 182.7292!)
		Me.XrLabel34.Name = "XrLabel34"
		Me.XrLabel34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel34.SizeF = New System.Drawing.SizeF(50.95831!, 19.0!)
		Me.XrLabel34.StylePriority.UseFont = False
		Me.XrLabel34.StylePriority.UseTextAlignment = False
		Me.XrLabel34.Text = "= Veces"
		Me.XrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel20
		'
		Me.XrLabel20.Dpi = 100.0!
		Me.XrLabel20.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(315.7917!, 406.9583!)
		Me.XrLabel20.Name = "XrLabel20"
		Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel20.SizeF = New System.Drawing.SizeF(228.9583!, 19.0!)
		Me.XrLabel20.StylePriority.UseFont = False
		Me.XrLabel20.StylePriority.UseTextAlignment = False
		Me.XrLabel20.Text = "Cuentas por Cobrar X Días en el Año"
		Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLabel1
		'
		Me.XrLabel1.Dpi = 100.0!
		Me.XrLabel1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(23.0!, 181.6875!)
		Me.XrLabel1.Name = "XrLabel1"
		Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel1.SizeF = New System.Drawing.SizeF(178.0!, 19.0!)
		Me.XrLabel1.StylePriority.UseFont = False
		Me.XrLabel1.StylePriority.UseTextAlignment = False
		Me.XrLabel1.Text = "LIQUIDEZ GENERAL"
		Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLine13
		'
		Me.XrLine13.Dpi = 100.0!
		Me.XrLine13.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 309.0625!)
		Me.XrLine13.Name = "XrLine13"
		Me.XrLine13.SizeF = New System.Drawing.SizeF(775.0!, 2.0!)
		'
		'xrpNumero
		'
		Me.xrpNumero.Dpi = 100.0!
		Me.xrpNumero.LocationFloat = New DevExpress.Utils.PointFloat(592.0!, 1.0!)
		Me.xrpNumero.Name = "xrpNumero"
		Me.xrpNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrpNumero.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
		'
		'XrLine2
		'
		Me.XrLine2.Dpi = 100.0!
		Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(326.2709!, 191.625!)
		Me.XrLine2.Name = "XrLine2"
		Me.XrLine2.SizeF = New System.Drawing.SizeF(208.0!, 2.0!)
		'
		'xrlTitulo
		'
		Me.xrlTitulo.Dpi = 100.0!
		Me.xrlTitulo.Font = New System.Drawing.Font("Arial", 11.0!)
		Me.xrlTitulo.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 40.0!)
		Me.xrlTitulo.Name = "xrlTitulo"
		Me.xrlTitulo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrlTitulo.SizeF = New System.Drawing.SizeF(782.0!, 19.0!)
		Me.xrlTitulo.StylePriority.UseFont = False
		Me.xrlTitulo.StylePriority.UseTextAlignment = False
		Me.xrlTitulo.Text = "xrlTitulo"
		Me.xrlTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'XrLabel26
		'
		Me.XrLabel26.Dpi = 100.0!
		Me.XrLabel26.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(349.2917!, 657.6251!)
		Me.XrLabel26.Name = "XrLabel26"
		Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel26.SizeF = New System.Drawing.SizeF(161.9583!, 18.0!)
		Me.XrLabel26.StylePriority.UseFont = False
		Me.XrLabel26.StylePriority.UseTextAlignment = False
		Me.XrLabel26.Text = "Ventas"
		Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'BottomMarginBand1
		'
		Me.BottomMarginBand1.Dpi = 100.0!
		Me.BottomMarginBand1.HeightF = 35.0!
		Me.BottomMarginBand1.Name = "BottomMarginBand1"
		'
		'XrLabel15
		'
		Me.XrLabel15.Dpi = 100.0!
		Me.XrLabel15.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(374.2917!, 193.875!)
		Me.XrLabel15.Name = "XrLabel15"
		Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel15.SizeF = New System.Drawing.SizeF(111.9583!, 18.0!)
		Me.XrLabel15.StylePriority.UseFont = False
		Me.XrLabel15.StylePriority.UseTextAlignment = False
		Me.XrLabel15.Text = "Pasivo Circualante"
		Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLabel30
		'
		Me.XrLabel30.Dpi = 100.0!
		Me.XrLabel30.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(349.2917!, 802.2084!)
		Me.XrLabel30.Name = "XrLabel30"
		Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel30.SizeF = New System.Drawing.SizeF(161.9583!, 18.0!)
		Me.XrLabel30.StylePriority.UseFont = False
		Me.XrLabel30.StylePriority.UseTextAlignment = False
		Me.XrLabel30.Text = "Activos Totales"
		Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLine5
		'
		Me.XrLine5.Dpi = 100.0!
		Me.XrLine5.LocationFloat = New DevExpress.Utils.PointFloat(326.2709!, 426.7916!)
		Me.XrLine5.Name = "XrLine5"
		Me.XrLine5.SizeF = New System.Drawing.SizeF(208.0!, 2.0!)
		'
		'XrLabel11
		'
		Me.XrLabel11.Dpi = 100.0!
		Me.XrLabel11.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(23.0!, 867.5208!)
		Me.XrLabel11.Name = "XrLabel11"
		Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel11.SizeF = New System.Drawing.SizeF(259.0!, 19.0!)
		Me.XrLabel11.StylePriority.UseFont = False
		Me.XrLabel11.StylePriority.UseTextAlignment = False
		Me.XrLabel11.Text = "ESTRUCTURA DE CAPITAL"
		Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel31
		'
		Me.XrLabel31.Dpi = 100.0!
		Me.XrLabel31.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(315.7917!, 780.125!)
		Me.XrLabel31.Name = "XrLabel31"
		Me.XrLabel31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel31.SizeF = New System.Drawing.SizeF(228.9583!, 19.0!)
		Me.XrLabel31.StylePriority.UseFont = False
		Me.XrLabel31.StylePriority.UseTextAlignment = False
		Me.XrLabel31.Text = "Ventas"
		Me.XrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLabel3
		'
		Me.XrLabel3.Dpi = 100.0!
		Me.XrLabel3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(23.0!, 335.5!)
		Me.XrLabel3.Name = "XrLabel3"
		Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel3.SizeF = New System.Drawing.SizeF(178.0!, 19.0!)
		Me.XrLabel3.StylePriority.UseFont = False
		Me.XrLabel3.StylePriority.UseTextAlignment = False
		Me.XrLabel3.Text = "PRUEBA DEFENSIVA"
		Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLine10
		'
		Me.XrLine10.Dpi = 100.0!
		Me.XrLine10.LocationFloat = New DevExpress.Utils.PointFloat(326.2709!, 799.9584!)
		Me.XrLine10.Name = "XrLine10"
		Me.XrLine10.SizeF = New System.Drawing.SizeF(208.0!, 2.0!)
		'
		'XrLabel4
		'
		Me.XrLabel4.Dpi = 100.0!
		Me.XrLabel4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(23.0!, 412.5417!)
		Me.XrLabel4.Name = "XrLabel4"
		Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel4.SizeF = New System.Drawing.SizeF(259.0!, 19.0!)
		Me.XrLabel4.StylePriority.UseFont = False
		Me.XrLabel4.StylePriority.UseTextAlignment = False
		Me.XrLabel4.Text = "PERIODO PROMEDIO DE COBRANZA"
		Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel42
		'
		Me.XrLabel42.Dpi = 100.0!
		Me.XrLabel42.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel42.LocationFloat = New DevExpress.Utils.PointFloat(556.8541!, 788.3958!)
		Me.XrLabel42.Name = "XrLabel42"
		Me.XrLabel42.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel42.SizeF = New System.Drawing.SizeF(50.95831!, 19.0!)
		Me.XrLabel42.StylePriority.UseFont = False
		Me.XrLabel42.StylePriority.UseTextAlignment = False
		Me.XrLabel42.Text = "= Veces"
		Me.XrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'TopMarginBand1
		'
		Me.TopMarginBand1.Dpi = 100.0!
		Me.TopMarginBand1.HeightF = 35.0!
		Me.TopMarginBand1.Name = "TopMarginBand1"
		'
		'XrLine1
		'
		Me.XrLine1.Dpi = 100.0!
		Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 156.0!)
		Me.XrLine1.Name = "XrLine1"
		Me.XrLine1.SizeF = New System.Drawing.SizeF(775.0!, 6.0!)
		'
		'XrLabel8
		'
		Me.XrLabel8.Dpi = 100.0!
		Me.XrLabel8.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(23.0!, 640.2813!)
		Me.XrLabel8.Name = "XrLabel8"
		Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel8.SizeF = New System.Drawing.SizeF(237.0!, 19.0!)
		Me.XrLabel8.StylePriority.UseFont = False
		Me.XrLabel8.StylePriority.UseTextAlignment = False
		Me.XrLabel8.Text = "PERIODO DE PAGO A PROVEEDORES"
		Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel5
		'
		Me.XrLabel5.Dpi = 100.0!
		Me.XrLabel5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(23.0!, 483.6354!)
		Me.XrLabel5.Name = "XrLabel5"
		Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel5.SizeF = New System.Drawing.SizeF(270.0!, 19.0!)
		Me.XrLabel5.StylePriority.UseFont = False
		Me.XrLabel5.StylePriority.UseTextAlignment = False
		Me.XrLabel5.Text = "ROTACION DE LAS CUENTAS POR COBRAR"
		Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLine9
		'
		Me.XrLine9.Dpi = 100.0!
		Me.XrLine9.LocationFloat = New DevExpress.Utils.PointFloat(326.2709!, 729.25!)
		Me.XrLine9.Name = "XrLine9"
		Me.XrLine9.SizeF = New System.Drawing.SizeF(208.0!, 2.0!)
		'
		'XrLabel17
		'
		Me.XrLabel17.Dpi = 100.0!
		Me.XrLabel17.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(374.2917!, 272.5834!)
		Me.XrLabel17.Name = "XrLabel17"
		Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel17.SizeF = New System.Drawing.SizeF(111.9583!, 18.0!)
		Me.XrLabel17.StylePriority.UseFont = False
		Me.XrLabel17.StylePriority.UseTextAlignment = False
		Me.XrLabel17.Text = "Pasivo Circualante"
		Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'xrlSaldoNivel2p
		'
		Me.xrlSaldoNivel2p.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "RazonesFinancieras.ValorFormula1", "{0:n2}")})
		Me.xrlSaldoNivel2p.Dpi = 100.0!
		Me.xrlSaldoNivel2p.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.xrlSaldoNivel2p.LocationFloat = New DevExpress.Utils.PointFloat(665.9583!, 183.2917!)
		Me.xrlSaldoNivel2p.Name = "xrlSaldoNivel2p"
		Me.xrlSaldoNivel2p.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrlSaldoNivel2p.SizeF = New System.Drawing.SizeF(84.0!, 16.0!)
		Me.xrlSaldoNivel2p.StylePriority.UseFont = False
		Me.xrlSaldoNivel2p.StylePriority.UseTextAlignment = False
		Me.xrlSaldoNivel2p.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
		'
		'XrLabel43
		'
		Me.XrLabel43.Dpi = 100.0!
		Me.XrLabel43.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel43.LocationFloat = New DevExpress.Utils.PointFloat(556.8541!, 871.6875!)
		Me.XrLabel43.Name = "XrLabel43"
		Me.XrLabel43.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel43.SizeF = New System.Drawing.SizeF(50.95831!, 19.0!)
		Me.XrLabel43.StylePriority.UseFont = False
		Me.XrLabel43.StylePriority.UseTextAlignment = False
		Me.XrLabel43.Text = "= Días"
		Me.XrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel35
		'
		Me.XrLabel35.Dpi = 100.0!
		Me.XrLabel35.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel35.LocationFloat = New DevExpress.Utils.PointFloat(556.8541!, 261.2292!)
		Me.XrLabel35.Name = "XrLabel35"
		Me.XrLabel35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel35.SizeF = New System.Drawing.SizeF(50.95831!, 19.0!)
		Me.XrLabel35.StylePriority.UseFont = False
		Me.XrLabel35.StylePriority.UseTextAlignment = False
		Me.XrLabel35.Text = "= Veces"
		Me.XrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel37
		'
		Me.XrLabel37.Dpi = 100.0!
		Me.XrLabel37.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel37.LocationFloat = New DevExpress.Utils.PointFloat(556.8541!, 412.2292!)
		Me.XrLabel37.Name = "XrLabel37"
		Me.XrLabel37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel37.SizeF = New System.Drawing.SizeF(50.95831!, 19.0!)
		Me.XrLabel37.StylePriority.UseFont = False
		Me.XrLabel37.StylePriority.UseTextAlignment = False
		Me.XrLabel37.Text = "= Días"
		Me.XrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'PageHeader
		'
		Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel53, Me.XrLine21, Me.XrLabel52, Me.XrLabel51, Me.XrLabel50, Me.XrLabel49, Me.XrLabel48, Me.XrLabel47, Me.XrLabel46, Me.XrLabel45, Me.XrLabel44, Me.XrLine18, Me.XrLine19, Me.XrLine20, Me.XrLine15, Me.XrLine16, Me.XrLine17, Me.XrLine14, Me.XrLine13, Me.XrLine12, Me.XrLabel43, Me.XrLabel42, Me.XrLabel41, Me.XrLabel40, Me.XrLabel39, Me.XrLabel38, Me.XrLabel37, Me.XrLabel36, Me.XrLabel35, Me.XrLabel34, Me.XrLine11, Me.XrLabel32, Me.XrLabel33, Me.XrLine10, Me.XrLabel30, Me.XrLabel31, Me.XrLabel28, Me.XrLabel29, Me.XrLine9, Me.XrLine8, Me.XrLabel26, Me.XrLabel27, Me.XrLabel24, Me.XrLabel25, Me.XrLine7, Me.XrLine6, Me.XrLabel22, Me.XrLabel23, Me.XrLabel20, Me.XrLabel21, Me.XrLine5, Me.XrLine4, Me.XrLabel18, Me.XrLabel19, Me.XrLabel16, Me.XrLabel17, Me.XrLine3, Me.XrLine2, Me.XrLabel15, Me.XrLabel14, Me.XrLabel12, Me.XrLabel10, Me.XrLabel11, Me.XrLabel5, Me.XrLabel6, Me.XrLabel7, Me.XrLabel8, Me.XrLabel3, Me.XrLabel4, Me.XrLabel2, Me.XrLabel1, Me.xrlSaldoNivel2p, Me.xrpFecha, Me.xrlPage, Me.xrpNumero, Me.XrLine1, Me.XrLabel13, Me.XrLabel9, Me.xrlPeriodo, Me.xrlTitulo, Me.xrlEmpresa})
		Me.PageHeader.Dpi = 100.0!
		Me.PageHeader.HeightF = 953.375!
		Me.PageHeader.Name = "PageHeader"
		Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
		Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		'
		'XrLabel53
		'
		Me.XrLabel53.Dpi = 100.0!
		Me.XrLabel53.Font = New System.Drawing.Font("Arial", 10.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.XrLabel53.LocationFloat = New DevExpress.Utils.PointFloat(10.08333!, 95.39584!)
		Me.XrLabel53.Name = "XrLabel53"
		Me.XrLabel53.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel53.SizeF = New System.Drawing.SizeF(782.0!, 19.0!)
		Me.XrLabel53.StylePriority.UseFont = False
		Me.XrLabel53.StylePriority.UseTextAlignment = False
		Me.XrLabel53.Text = "INDICADORES DE LIQUIDEZ"
		Me.XrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'XrLine21
		'
		Me.XrLine21.Dpi = 100.0!
		Me.XrLine21.LocationFloat = New DevExpress.Utils.PointFloat(8.5!, 913.7709!)
		Me.XrLine21.Name = "XrLine21"
		Me.XrLine21.SizeF = New System.Drawing.SizeF(775.0!, 2.0!)
		'
		'XrLabel52
		'
		Me.XrLabel52.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "RazonesFinancieras.ValorFormula10", "{0:n2}")})
		Me.XrLabel52.Dpi = 100.0!
		Me.XrLabel52.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.XrLabel52.LocationFloat = New DevExpress.Utils.PointFloat(665.9583!, 873.8125!)
		Me.XrLabel52.Name = "XrLabel52"
		Me.XrLabel52.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel52.SizeF = New System.Drawing.SizeF(84.0!, 16.0!)
		Me.XrLabel52.StylePriority.UseFont = False
		Me.XrLabel52.StylePriority.UseTextAlignment = False
		Me.XrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
		'
		'XrLabel51
		'
		Me.XrLabel51.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "RazonesFinancieras.ValorFormula9", "{0:n2}")})
		Me.XrLabel51.Dpi = 100.0!
		Me.XrLabel51.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.XrLabel51.LocationFloat = New DevExpress.Utils.PointFloat(665.9583!, 794.6458!)
		Me.XrLabel51.Name = "XrLabel51"
		Me.XrLabel51.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel51.SizeF = New System.Drawing.SizeF(84.0!, 16.0!)
		Me.XrLabel51.StylePriority.UseFont = False
		Me.XrLabel51.StylePriority.UseTextAlignment = False
		Me.XrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
		'
		'XrLabel50
		'
		Me.XrLabel50.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "RazonesFinancieras.ValorFormula8", "{0:n2}")})
		Me.XrLabel50.Dpi = 100.0!
		Me.XrLabel50.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.XrLabel50.LocationFloat = New DevExpress.Utils.PointFloat(665.9583!, 720.7292!)
		Me.XrLabel50.Name = "XrLabel50"
		Me.XrLabel50.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel50.SizeF = New System.Drawing.SizeF(84.0!, 16.0!)
		Me.XrLabel50.StylePriority.UseFont = False
		Me.XrLabel50.StylePriority.UseTextAlignment = False
		Me.XrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
		'
		'XrLabel49
		'
		Me.XrLabel49.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "RazonesFinancieras.ValorFormula7", "{0:n2}")})
		Me.XrLabel49.Dpi = 100.0!
		Me.XrLabel49.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.XrLabel49.LocationFloat = New DevExpress.Utils.PointFloat(665.9583!, 646.7708!)
		Me.XrLabel49.Name = "XrLabel49"
		Me.XrLabel49.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel49.SizeF = New System.Drawing.SizeF(84.0!, 16.0!)
		Me.XrLabel49.StylePriority.UseFont = False
		Me.XrLabel49.StylePriority.UseTextAlignment = False
		Me.XrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
		'
		'XrLabel48
		'
		Me.XrLabel48.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "RazonesFinancieras.ValorFormula6", "{0:n2}")})
		Me.XrLabel48.Dpi = 100.0!
		Me.XrLabel48.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.XrLabel48.LocationFloat = New DevExpress.Utils.PointFloat(665.9583!, 565.4792!)
		Me.XrLabel48.Name = "XrLabel48"
		Me.XrLabel48.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel48.SizeF = New System.Drawing.SizeF(84.0!, 16.0!)
		Me.XrLabel48.StylePriority.UseFont = False
		Me.XrLabel48.StylePriority.UseTextAlignment = False
		Me.XrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
		'
		'XrLabel47
		'
		Me.XrLabel47.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "RazonesFinancieras.ValorFormula5", "{0:n2}")})
		Me.XrLabel47.Dpi = 100.0!
		Me.XrLabel47.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.XrLabel47.LocationFloat = New DevExpress.Utils.PointFloat(665.9583!, 490.4792!)
		Me.XrLabel47.Name = "XrLabel47"
		Me.XrLabel47.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel47.SizeF = New System.Drawing.SizeF(84.0!, 16.0!)
		Me.XrLabel47.StylePriority.UseFont = False
		Me.XrLabel47.StylePriority.UseTextAlignment = False
		Me.XrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
		'
		'XrLabel46
		'
		Me.XrLabel46.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "RazonesFinancieras.ValorFormula4", "{0:n2}")})
		Me.XrLabel46.Dpi = 100.0!
		Me.XrLabel46.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.XrLabel46.LocationFloat = New DevExpress.Utils.PointFloat(665.9583!, 416.5208!)
		Me.XrLabel46.Name = "XrLabel46"
		Me.XrLabel46.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel46.SizeF = New System.Drawing.SizeF(84.0!, 16.0!)
		Me.XrLabel46.StylePriority.UseFont = False
		Me.XrLabel46.StylePriority.UseTextAlignment = False
		Me.XrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
		'
		'XrLabel45
		'
		Me.XrLabel45.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "RazonesFinancieras.ValorFormula3", "{0:n2}")})
		Me.XrLabel45.Dpi = 100.0!
		Me.XrLabel45.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.XrLabel45.LocationFloat = New DevExpress.Utils.PointFloat(665.9583!, 341.5208!)
		Me.XrLabel45.Name = "XrLabel45"
		Me.XrLabel45.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel45.SizeF = New System.Drawing.SizeF(84.0!, 16.0!)
		Me.XrLabel45.StylePriority.UseFont = False
		Me.XrLabel45.StylePriority.UseTextAlignment = False
		Me.XrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
		'
		'XrLabel44
		'
		Me.XrLabel44.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "RazonesFinancieras.ValorFormula2", "{0:n2}")})
		Me.XrLabel44.Dpi = 100.0!
		Me.XrLabel44.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.XrLabel44.LocationFloat = New DevExpress.Utils.PointFloat(665.9583!, 263.3542!)
		Me.XrLabel44.Name = "XrLabel44"
		Me.XrLabel44.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel44.SizeF = New System.Drawing.SizeF(84.0!, 16.0!)
		Me.XrLabel44.StylePriority.UseFont = False
		Me.XrLabel44.StylePriority.UseTextAlignment = False
		Me.XrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
		'
		'XrLine18
		'
		Me.XrLine18.Dpi = 100.0!
		Me.XrLine18.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 693.8334!)
		Me.XrLine18.Name = "XrLine18"
		Me.XrLine18.SizeF = New System.Drawing.SizeF(775.0!, 2.0!)
		'
		'XrLine19
		'
		Me.XrLine19.Dpi = 100.0!
		Me.XrLine19.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 768.0834!)
		Me.XrLine19.Name = "XrLine19"
		Me.XrLine19.SizeF = New System.Drawing.SizeF(775.0!, 2.0!)
		'
		'XrLine20
		'
		Me.XrLine20.Dpi = 100.0!
		Me.XrLine20.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 846.9584!)
		Me.XrLine20.Name = "XrLine20"
		Me.XrLine20.SizeF = New System.Drawing.SizeF(775.0!, 2.0!)
		'
		'XrLine15
		'
		Me.XrLine15.Dpi = 100.0!
		Me.XrLine15.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 458.625!)
		Me.XrLine15.Name = "XrLine15"
		Me.XrLine15.SizeF = New System.Drawing.SizeF(775.0!, 2.0!)
		'
		'XrLine16
		'
		Me.XrLine16.Dpi = 100.0!
		Me.XrLine16.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 539.875!)
		Me.XrLine16.Name = "XrLine16"
		Me.XrLine16.SizeF = New System.Drawing.SizeF(775.0!, 2.0!)
		'
		'XrLine17
		'
		Me.XrLine17.Dpi = 100.0!
		Me.XrLine17.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 611.75!)
		Me.XrLine17.Name = "XrLine17"
		Me.XrLine17.SizeF = New System.Drawing.SizeF(775.0!, 2.0!)
		'
		'XrLine14
		'
		Me.XrLine14.Dpi = 100.0!
		Me.XrLine14.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 380.9375!)
		Me.XrLine14.Name = "XrLine14"
		Me.XrLine14.SizeF = New System.Drawing.SizeF(775.0!, 2.0!)
		'
		'XrLine12
		'
		Me.XrLine12.Dpi = 100.0!
		Me.XrLine12.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 227.8125!)
		Me.XrLine12.Name = "XrLine12"
		Me.XrLine12.SizeF = New System.Drawing.SizeF(775.0!, 2.0!)
		'
		'XrLabel41
		'
		Me.XrLabel41.Dpi = 100.0!
		Me.XrLabel41.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel41.LocationFloat = New DevExpress.Utils.PointFloat(556.8541!, 717.4792!)
		Me.XrLabel41.Name = "XrLabel41"
		Me.XrLabel41.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel41.SizeF = New System.Drawing.SizeF(50.95831!, 19.0!)
		Me.XrLabel41.StylePriority.UseFont = False
		Me.XrLabel41.StylePriority.UseTextAlignment = False
		Me.XrLabel41.Text = "= Días"
		Me.XrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel38
		'
		Me.XrLabel38.Dpi = 100.0!
		Me.XrLabel38.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel38.LocationFloat = New DevExpress.Utils.PointFloat(556.8541!, 486.1875!)
		Me.XrLabel38.Name = "XrLabel38"
		Me.XrLabel38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel38.SizeF = New System.Drawing.SizeF(50.95831!, 19.0!)
		Me.XrLabel38.StylePriority.UseFont = False
		Me.XrLabel38.StylePriority.UseTextAlignment = False
		Me.XrLabel38.Text = "= Veces"
		Me.XrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel36
		'
		Me.XrLabel36.Dpi = 100.0!
		Me.XrLabel36.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel36.LocationFloat = New DevExpress.Utils.PointFloat(556.8541!, 339.1875!)
		Me.XrLabel36.Name = "XrLabel36"
		Me.XrLabel36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel36.SizeF = New System.Drawing.SizeF(50.95831!, 19.0!)
		Me.XrLabel36.StylePriority.UseFont = False
		Me.XrLabel36.StylePriority.UseTextAlignment = False
		Me.XrLabel36.Text = "= %"
		Me.XrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLine11
		'
		Me.XrLine11.Dpi = 100.0!
		Me.XrLine11.LocationFloat = New DevExpress.Utils.PointFloat(326.2709!, 882.7708!)
		Me.XrLine11.Name = "XrLine11"
		Me.XrLine11.SizeF = New System.Drawing.SizeF(208.0!, 2.0!)
		'
		'XrLabel28
		'
		Me.XrLabel28.Dpi = 100.0!
		Me.XrLabel28.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(315.7917!, 709.4167!)
		Me.XrLabel28.Name = "XrLabel28"
		Me.XrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel28.SizeF = New System.Drawing.SizeF(228.9583!, 19.0!)
		Me.XrLabel28.StylePriority.UseFont = False
		Me.XrLabel28.StylePriority.UseTextAlignment = False
		Me.XrLabel28.Text = "Caja y Bancos X 360"
		Me.XrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLabel29
		'
		Me.XrLabel29.Dpi = 100.0!
		Me.XrLabel29.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(349.2917!, 731.5001!)
		Me.XrLabel29.Name = "XrLabel29"
		Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel29.SizeF = New System.Drawing.SizeF(161.9583!, 18.0!)
		Me.XrLabel29.StylePriority.UseFont = False
		Me.XrLabel29.StylePriority.UseTextAlignment = False
		Me.XrLabel29.Text = "Ventas"
		Me.XrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLine8
		'
		Me.XrLine8.Dpi = 100.0!
		Me.XrLine8.LocationFloat = New DevExpress.Utils.PointFloat(326.2709!, 655.375!)
		Me.XrLine8.Name = "XrLine8"
		Me.XrLine8.SizeF = New System.Drawing.SizeF(208.0!, 2.0!)
		'
		'XrLabel27
		'
		Me.XrLabel27.Dpi = 100.0!
		Me.XrLabel27.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(315.7917!, 635.5417!)
		Me.XrLabel27.Name = "XrLabel27"
		Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel27.SizeF = New System.Drawing.SizeF(228.9583!, 19.0!)
		Me.XrLabel27.StylePriority.UseFont = False
		Me.XrLabel27.StylePriority.UseTextAlignment = False
		Me.XrLabel27.Text = "Cuentas por Pagar (Promedio) X 360"
		Me.XrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLabel24
		'
		Me.XrLabel24.Dpi = 100.0!
		Me.XrLabel24.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(315.7917!, 554.6666!)
		Me.XrLabel24.Name = "XrLabel24"
		Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel24.SizeF = New System.Drawing.SizeF(228.9583!, 19.0!)
		Me.XrLabel24.StylePriority.UseFont = False
		Me.XrLabel24.StylePriority.UseTextAlignment = False
		Me.XrLabel24.Text = "Cuentas por Cobrar Promedio X 360"
		Me.XrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLabel25
		'
		Me.XrLabel25.Dpi = 100.0!
		Me.XrLabel25.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(349.2917!, 576.75!)
		Me.XrLabel25.Name = "XrLabel25"
		Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel25.SizeF = New System.Drawing.SizeF(161.9583!, 18.0!)
		Me.XrLabel25.StylePriority.UseFont = False
		Me.XrLabel25.StylePriority.UseTextAlignment = False
		Me.XrLabel25.Text = "Ventas"
		Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLine7
		'
		Me.XrLine7.Dpi = 100.0!
		Me.XrLine7.LocationFloat = New DevExpress.Utils.PointFloat(326.2709!, 574.5!)
		Me.XrLine7.Name = "XrLine7"
		Me.XrLine7.SizeF = New System.Drawing.SizeF(208.0!, 2.0!)
		'
		'XrLine6
		'
		Me.XrLine6.Dpi = 100.0!
		Me.XrLine6.LocationFloat = New DevExpress.Utils.PointFloat(326.2709!, 498.125!)
		Me.XrLine6.Name = "XrLine6"
		Me.XrLine6.SizeF = New System.Drawing.SizeF(208.0!, 2.0!)
		'
		'XrLabel23
		'
		Me.XrLabel23.Dpi = 100.0!
		Me.XrLabel23.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(315.7917!, 478.2917!)
		Me.XrLabel23.Name = "XrLabel23"
		Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel23.SizeF = New System.Drawing.SizeF(228.9583!, 19.0!)
		Me.XrLabel23.StylePriority.UseFont = False
		Me.XrLabel23.StylePriority.UseTextAlignment = False
		Me.XrLabel23.Text = "Ventas Anuales al Crédito"
		Me.XrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLabel21
		'
		Me.XrLabel21.Dpi = 100.0!
		Me.XrLabel21.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(349.2917!, 429.0417!)
		Me.XrLabel21.Name = "XrLabel21"
		Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel21.SizeF = New System.Drawing.SizeF(161.9583!, 18.0!)
		Me.XrLabel21.StylePriority.UseFont = False
		Me.XrLabel21.StylePriority.UseTextAlignment = False
		Me.XrLabel21.Text = "Ventas Anuales al Crédito"
		Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLine4
		'
		Me.XrLine4.Dpi = 100.0!
		Me.XrLine4.LocationFloat = New DevExpress.Utils.PointFloat(326.2709!, 349.0833!)
		Me.XrLine4.Name = "XrLine4"
		Me.XrLine4.SizeF = New System.Drawing.SizeF(208.0!, 2.0!)
		'
		'XrLabel18
		'
		Me.XrLabel18.Dpi = 100.0!
		Me.XrLabel18.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(374.2917!, 351.3333!)
		Me.XrLabel18.Name = "XrLabel18"
		Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel18.SizeF = New System.Drawing.SizeF(111.9583!, 18.0!)
		Me.XrLabel18.StylePriority.UseFont = False
		Me.XrLabel18.StylePriority.UseTextAlignment = False
		Me.XrLabel18.Text = "Pasivo Circualante"
		Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLabel19
		'
		Me.XrLabel19.Dpi = 100.0!
		Me.XrLabel19.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(339.7917!, 329.25!)
		Me.XrLabel19.Name = "XrLabel19"
		Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel19.SizeF = New System.Drawing.SizeF(180.9583!, 19.0!)
		Me.XrLabel19.StylePriority.UseFont = False
		Me.XrLabel19.StylePriority.UseTextAlignment = False
		Me.XrLabel19.Text = "Caja y Bancos"
		Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLabel16
		'
		Me.XrLabel16.Dpi = 100.0!
		Me.XrLabel16.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(339.7917!, 250.5!)
		Me.XrLabel16.Name = "XrLabel16"
		Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel16.SizeF = New System.Drawing.SizeF(180.9583!, 19.0!)
		Me.XrLabel16.StylePriority.UseFont = False
		Me.XrLabel16.StylePriority.UseTextAlignment = False
		Me.XrLabel16.Text = "Activo Circulante - Inventarios"
		Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLine3
		'
		Me.XrLine3.Dpi = 100.0!
		Me.XrLine3.LocationFloat = New DevExpress.Utils.PointFloat(326.2709!, 270.3334!)
		Me.XrLine3.Name = "XrLine3"
		Me.XrLine3.SizeF = New System.Drawing.SizeF(208.0!, 2.0!)
		'
		'XrLabel14
		'
		Me.XrLabel14.Dpi = 100.0!
		Me.XrLabel14.Font = New System.Drawing.Font("Arial", 9.0!)
		Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(379.7917!, 171.7917!)
		Me.XrLabel14.Name = "XrLabel14"
		Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel14.SizeF = New System.Drawing.SizeF(100.9583!, 19.0!)
		Me.XrLabel14.StylePriority.UseFont = False
		Me.XrLabel14.StylePriority.UseTextAlignment = False
		Me.XrLabel14.Text = "Activo Circulante"
		Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
		'
		'XrLabel12
		'
		Me.XrLabel12.Dpi = 100.0!
		Me.XrLabel12.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(340.75!, 136.75!)
		Me.XrLabel12.Name = "XrLabel12"
		Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel12.SizeF = New System.Drawing.SizeF(178.0!, 19.0!)
		Me.XrLabel12.StylePriority.UseFont = False
		Me.XrLabel12.StylePriority.UseTextAlignment = False
		Me.XrLabel12.Text = "FORMULA"
		Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'XrLabel10
		'
		Me.XrLabel10.Dpi = 100.0!
		Me.XrLabel10.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(23.0!, 787.3542!)
		Me.XrLabel10.Name = "XrLabel10"
		Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel10.SizeF = New System.Drawing.SizeF(237.0!, 19.0!)
		Me.XrLabel10.StylePriority.UseFont = False
		Me.XrLabel10.StylePriority.UseTextAlignment = False
		Me.XrLabel10.Text = "ROTACION DE LOS ACTIVOS TOTALES"
		Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel6
		'
		Me.XrLabel6.Dpi = 100.0!
		Me.XrLabel6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(23.0!, 559.4687!)
		Me.XrLabel6.Name = "XrLabel6"
		Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel6.SizeF = New System.Drawing.SizeF(178.0!, 19.0!)
		Me.XrLabel6.StylePriority.UseFont = False
		Me.XrLabel6.StylePriority.UseTextAlignment = False
		Me.XrLabel6.Text = "ROTACION DE LA CARTERA"
		Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel7
		'
		Me.XrLabel7.Dpi = 100.0!
		Me.XrLabel7.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(23.0!, 713.2396!)
		Me.XrLabel7.Name = "XrLabel7"
		Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel7.SizeF = New System.Drawing.SizeF(259.0!, 19.0!)
		Me.XrLabel7.StylePriority.UseFont = False
		Me.XrLabel7.StylePriority.UseTextAlignment = False
		Me.XrLabel7.Text = "ROTACION DE CAJA Y BANCOS"
		Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'XrLabel2
		'
		Me.XrLabel2.Dpi = 100.0!
		Me.XrLabel2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(23.0!, 255.5208!)
		Me.XrLabel2.Name = "XrLabel2"
		Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel2.SizeF = New System.Drawing.SizeF(178.0!, 19.0!)
		Me.XrLabel2.StylePriority.UseFont = False
		Me.XrLabel2.StylePriority.UseTextAlignment = False
		Me.XrLabel2.Text = "PRUEBA ACIDA"
		Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
		'
		'xrpFecha
		'
		Me.xrpFecha.Dpi = 100.0!
		Me.xrpFecha.Format = "{0:dd/MM/yyyy hh:mm tt}"
		Me.xrpFecha.LocationFloat = New DevExpress.Utils.PointFloat(666.0!, 1.0!)
		Me.xrpFecha.Name = "xrpFecha"
		Me.xrpFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrpFecha.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
		Me.xrpFecha.SizeF = New System.Drawing.SizeF(125.0!, 15.0!)
		'
		'XrLabel13
		'
		Me.XrLabel13.Dpi = 100.0!
		Me.XrLabel13.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(640.0!, 137.0!)
		Me.XrLabel13.Name = "XrLabel13"
		Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel13.SizeF = New System.Drawing.SizeF(134.0!, 19.0!)
		Me.XrLabel13.StylePriority.UseFont = False
		Me.XrLabel13.StylePriority.UseTextAlignment = False
		Me.XrLabel13.Text = "RESULTADO"
		Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'XrLabel9
		'
		Me.XrLabel9.Dpi = 100.0!
		Me.XrLabel9.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
		Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(81.0!, 137.0!)
		Me.XrLabel9.Name = "XrLabel9"
		Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel9.SizeF = New System.Drawing.SizeF(178.0!, 19.0!)
		Me.XrLabel9.StylePriority.UseFont = False
		Me.XrLabel9.StylePriority.UseTextAlignment = False
		Me.XrLabel9.Text = "INDICADOR"
		Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'xrlPeriodo
		'
		Me.xrlPeriodo.Dpi = 100.0!
		Me.xrlPeriodo.Font = New System.Drawing.Font("Arial", 10.0!)
		Me.xrlPeriodo.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 59.0!)
		Me.xrlPeriodo.Name = "xrlPeriodo"
		Me.xrlPeriodo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrlPeriodo.SizeF = New System.Drawing.SizeF(782.0!, 19.0!)
		Me.xrlPeriodo.StylePriority.UseFont = False
		Me.xrlPeriodo.StylePriority.UseTextAlignment = False
		Me.xrlPeriodo.Text = "xrlPeriodo"
		Me.xrlPeriodo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'xrlEmpresa
		'
		Me.xrlEmpresa.Dpi = 100.0!
		Me.xrlEmpresa.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
		Me.xrlEmpresa.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 18.0!)
		Me.xrlEmpresa.Name = "xrlEmpresa"
		Me.xrlEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrlEmpresa.SizeF = New System.Drawing.SizeF(782.0!, 22.0!)
		Me.xrlEmpresa.StylePriority.UseFont = False
		Me.xrlEmpresa.StylePriority.UseTextAlignment = False
		Me.xrlEmpresa.Text = "xrlEmpresa"
		Me.xrlEmpresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'GroupFooter1
		'
		Me.GroupFooter1.Dpi = 100.0!
		Me.GroupFooter1.HeightF = 19.49997!
		Me.GroupFooter1.Name = "GroupFooter1"
		Me.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
		'
		'Detail
		'
		Me.Detail.Dpi = 100.0!
		Me.Detail.HeightF = 0!
		Me.Detail.Name = "Detail"
		Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
		Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		'
		'DsContabilidad1
		'
		Me.DsContabilidad1.DataSetName = "dsContabilidad"
		Me.DsContabilidad1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'con_rptRazonesFinancieras
		'
		Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.GroupFooter1, Me.TopMarginBand1, Me.BottomMarginBand1})
		Me.DataMember = "RazonesFinancieras"
		Me.DataSource = Me.DsContabilidad1
		Me.DrawGrid = False
		Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Margins = New System.Drawing.Printing.Margins(25, 25, 35, 35)
		Me.SnapGridSize = 3.125!
		Me.SnappingMode = DevExpress.XtraReports.UI.SnappingMode.None
		Me.Version = "16.2"
		CType(Me.DsContabilidad1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

	End Sub

	Friend WithEvents XrLabel39 As XRLabel
	Friend WithEvents XrLabel22 As XRLabel
	Friend WithEvents XrLabel33 As XRLabel
	Friend WithEvents xrlPage As XRLabel
	Friend WithEvents XrLabel40 As XRLabel
	Friend WithEvents XrLabel32 As XRLabel
	Friend WithEvents XrLabel34 As XRLabel
	Friend WithEvents XrLabel20 As XRLabel
	Friend WithEvents XrLabel1 As XRLabel
	Friend WithEvents XrLine13 As XRLine
	Friend WithEvents xrpNumero As XRPageInfo
	Friend WithEvents XrLine2 As XRLine
	Friend WithEvents xrlTitulo As XRLabel
	Friend WithEvents XrLabel26 As XRLabel
	Friend WithEvents BottomMarginBand1 As BottomMarginBand
	Friend WithEvents XrLabel15 As XRLabel
	Friend WithEvents XrLabel30 As XRLabel
	Friend WithEvents XrLine5 As XRLine
	Friend WithEvents XrLabel11 As XRLabel
	Friend WithEvents XrLabel31 As XRLabel
	Friend WithEvents XrLabel3 As XRLabel
	Friend WithEvents XrLine10 As XRLine
	Friend WithEvents XrLabel4 As XRLabel
	Friend WithEvents XrLabel42 As XRLabel
	Friend WithEvents TopMarginBand1 As TopMarginBand
	Friend WithEvents XrLine1 As XRLine
	Friend WithEvents XrLabel8 As XRLabel
	Friend WithEvents XrLabel5 As XRLabel
	Friend WithEvents XrLine9 As XRLine
	Friend WithEvents XrLabel17 As XRLabel
	Friend WithEvents xrlSaldoNivel2p As XRLabel
	Friend WithEvents XrLabel43 As XRLabel
	Friend WithEvents XrLabel35 As XRLabel
	Friend WithEvents XrLabel37 As XRLabel
	Friend WithEvents PageHeader As PageHeaderBand
	Friend WithEvents XrLine18 As XRLine
	Friend WithEvents XrLine19 As XRLine
	Friend WithEvents XrLine20 As XRLine
	Friend WithEvents XrLine15 As XRLine
	Friend WithEvents XrLine16 As XRLine
	Friend WithEvents XrLine17 As XRLine
	Friend WithEvents XrLine14 As XRLine
	Friend WithEvents XrLine12 As XRLine
	Friend WithEvents XrLabel41 As XRLabel
	Friend WithEvents XrLabel38 As XRLabel
	Friend WithEvents XrLabel36 As XRLabel
	Friend WithEvents XrLine11 As XRLine
	Friend WithEvents XrLabel28 As XRLabel
	Friend WithEvents XrLabel29 As XRLabel
	Friend WithEvents XrLine8 As XRLine
	Friend WithEvents XrLabel27 As XRLabel
	Friend WithEvents XrLabel24 As XRLabel
	Friend WithEvents XrLabel25 As XRLabel
	Friend WithEvents XrLine7 As XRLine
	Friend WithEvents XrLine6 As XRLine
	Friend WithEvents XrLabel23 As XRLabel
	Friend WithEvents XrLabel21 As XRLabel
	Friend WithEvents XrLine4 As XRLine
	Friend WithEvents XrLabel18 As XRLabel
	Friend WithEvents XrLabel19 As XRLabel
	Friend WithEvents XrLabel16 As XRLabel
	Friend WithEvents XrLine3 As XRLine
	Friend WithEvents XrLabel14 As XRLabel
	Friend WithEvents XrLabel12 As XRLabel
	Friend WithEvents XrLabel10 As XRLabel
	Friend WithEvents XrLabel6 As XRLabel
	Friend WithEvents XrLabel7 As XRLabel
	Friend WithEvents XrLabel2 As XRLabel
	Friend WithEvents xrpFecha As XRPageInfo
	Friend WithEvents XrLabel13 As XRLabel
	Friend WithEvents XrLabel9 As XRLabel
	Friend WithEvents xrlPeriodo As XRLabel
	Friend WithEvents xrlEmpresa As XRLabel
	Friend WithEvents GroupFooter1 As GroupFooterBand
	Friend WithEvents Detail As DetailBand
	Friend WithEvents DsContabilidad1 As dsContabilidad
	Friend WithEvents XrLabel52 As XRLabel
	Friend WithEvents XrLabel51 As XRLabel
	Friend WithEvents XrLabel50 As XRLabel
	Friend WithEvents XrLabel49 As XRLabel
	Friend WithEvents XrLabel48 As XRLabel
	Friend WithEvents XrLabel47 As XRLabel
	Friend WithEvents XrLabel46 As XRLabel
	Friend WithEvents XrLabel45 As XRLabel
	Friend WithEvents XrLabel44 As XRLabel
	Friend WithEvents XrLine21 As XRLine
	Friend WithEvents XrLabel53 As XRLabel
End Class
