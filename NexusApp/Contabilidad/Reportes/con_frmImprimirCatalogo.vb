﻿Imports NexusBLL
Public Class con_frmImprimirCatalogo

    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private Sub cmd_Click() Handles Me.Reporte

        If RadioGroup1.Text = 1 Then
            Dim rpt As New con_rptCuentas() With {.DataSource = objTablas.con_CuentasSelectAll(), .DataMember = ""}
            rpt.xlrEmpresa.Text = gsNombre_Empresa
            rpt.xlrTitulo.Text = "Listado para revisión del catálogo de cuentas"
            rpt.ShowPreviewDialog()
        Else
            Dim rpt As New con_rptCatalogoSaldos() With {.DataSource = bl.rptCatalogoConSaldos(meMes.Month, seEjercicio.EditValue), .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.Text
            rpt.xrlPeriodo.Text = String.Format("CORRESPONDIENTE AL MES DE {0} DE {1}", ObtieneMesString(meMes.Month), seEjercicio.Text)
            rpt.xrlMoneda.Text = gsDesc_Moneda
            rpt.ShowPreviewDialog()
        End If
    End Sub

    Private Sub frmImprimirCatalogo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        bl.ObtienePeriodoContable(Mes, Ejercicio)
        meMes.EditValue = Mes
        seEjercicio.EditValue = Ejercicio
    End Sub

End Class
