﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmBalanceComprobReporte
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.spnNivel = New DevExpress.XtraEditors.SpinEdit()
        Me.seEjercicio = New DevExpress.XtraEditors.SpinEdit()
        Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.opgTipo = New DevExpress.XtraEditors.RadioGroup()
        Me.ceDateInfo = New DevExpress.XtraEditors.CheckEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.ceFormatoCat = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spnNivel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.opgTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceDateInfo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceFormatoCat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.ceFormatoCat)
        Me.GroupControl1.Controls.Add(Me.ceDateInfo)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.opgTipo)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.spnNivel)
        Me.GroupControl1.Controls.Add(Me.seEjercicio)
        Me.GroupControl1.Controls.Add(Me.meMes)
        Me.GroupControl1.Size = New System.Drawing.Size(690, 358)
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "BALANCE DE COMPROBACIÓN DE SALDOS"
        Me.teTitulo.Location = New System.Drawing.Point(166, 158)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(375, 20)
        Me.teTitulo.TabIndex = 5
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(77, 161)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl3.TabIndex = 12
        Me.LabelControl3.Text = "Título del informe:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(71, 92)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(92, 13)
        Me.LabelControl2.TabIndex = 11
        Me.LabelControl2.Text = "Nivel de Impresión:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(32, 60)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(131, 13)
        Me.LabelControl1.TabIndex = 10
        Me.LabelControl1.Text = "Mes para Generar Informe:"
        '
        'spnNivel
        '
        Me.spnNivel.EditValue = New Decimal(New Integer() {4, 0, 0, 0})
        Me.spnNivel.Location = New System.Drawing.Point(166, 89)
        Me.spnNivel.Name = "spnNivel"
        Me.spnNivel.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.spnNivel.Size = New System.Drawing.Size(51, 20)
        Me.spnNivel.TabIndex = 3
        '
        'seEjercicio
        '
        Me.seEjercicio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seEjercicio.Location = New System.Drawing.Point(272, 59)
        Me.seEjercicio.Name = "seEjercicio"
        Me.seEjercicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seEjercicio.Size = New System.Drawing.Size(73, 20)
        Me.seEjercicio.TabIndex = 2
        '
        'meMes
        '
        Me.meMes.Location = New System.Drawing.Point(166, 59)
        Me.meMes.Name = "meMes"
        Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes.Size = New System.Drawing.Size(100, 20)
        Me.meMes.TabIndex = 1
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(42, 128)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(121, 13)
        Me.LabelControl4.TabIndex = 47
        Me.LabelControl4.Text = "Tipo de saldos a mostrar:"
        '
        'opgTipo
        '
        Me.opgTipo.EditValue = 1
        Me.opgTipo.Location = New System.Drawing.Point(166, 121)
        Me.opgTipo.Name = "opgTipo"
        Me.opgTipo.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Basado en saldos acumulados"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Basado en saldos del mes")})
        Me.opgTipo.Size = New System.Drawing.Size(375, 28)
        Me.opgTipo.TabIndex = 4
        '
        'ceDateInfo
        '
        Me.ceDateInfo.Location = New System.Drawing.Point(166, 184)
        Me.ceDateInfo.Name = "ceDateInfo"
        Me.ceDateInfo.Properties.Caption = "Incluir página, fecha y hora de impresión"
        Me.ceDateInfo.Size = New System.Drawing.Size(226, 19)
        Me.ceDateInfo.TabIndex = 5
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(166, 34)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(375, 20)
        Me.leSucursal.TabIndex = 0
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(120, 37)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl7.TabIndex = 60
        Me.LabelControl7.Text = "Sucursal:"
        '
        'ceFormatoCat
        '
        Me.ceFormatoCat.Location = New System.Drawing.Point(166, 234)
        Me.ceFormatoCat.Name = "ceFormatoCat"
        Me.ceFormatoCat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ceFormatoCat.Properties.Appearance.Options.UseFont = True
        Me.ceFormatoCat.Properties.Caption = "IMPRIMIR EN FORMA DE CATALOGO"
        Me.ceFormatoCat.Size = New System.Drawing.Size(226, 19)
        Me.ceFormatoCat.TabIndex = 5
        '
        'con_frmBalanceComprobReporte
        '
        Me.Appearance.BackColor = System.Drawing.Color.White
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(690, 383)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmBalanceComprobReporte"
        Me.Text = "Balance de Comprobación Tipo Reporte"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spnNivel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.opgTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceDateInfo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceFormatoCat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents spnNivel As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seEjercicio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents opgTipo As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents ceDateInfo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceFormatoCat As DevExpress.XtraEditors.CheckEdit

End Class
