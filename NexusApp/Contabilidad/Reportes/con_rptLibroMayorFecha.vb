﻿Public Class con_rptLibroMayorFecha

    Private Sub xrlDebe_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles xrlDebe.BeforePrint
        If GetCurrentColumnValue("Debe") = 0 Then
            e.Cancel = True
        End If
    End Sub
    Private Sub xrlHaber_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles xrlHaber.BeforePrint
        If GetCurrentColumnValue("Haber") = 0 Then
            e.Cancel = True
        End If
    End Sub
End Class