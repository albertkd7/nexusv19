Public Class con_rptLibroAuxiliar
    Dim SaldoCuenta As Decimal = 0.0

    Private Sub groupCuenta_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles groupCuenta.BeforePrint
        SaldoCuenta = GetCurrentColumnValue("SaldoInicial")
    End Sub

    Private Sub Detail_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Detail.BeforePrint
        If GetCurrentColumnValue("Naturaleza") = "A" Then
            SaldoCuenta += GetCurrentColumnValue("Debe") - GetCurrentColumnValue("Haber")
        Else
            SaldoCuenta += GetCurrentColumnValue("Haber") - GetCurrentColumnValue("Debe")
        End If
    End Sub

    Private Sub xrlSaldo_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlSaldo.BeforePrint
        xrlSaldo.Text = Format(SaldoCuenta, "##,###,##0.00")
    End Sub

End Class