﻿Imports NexusBLL
Imports System.Data.SqlClient
Imports DevExpress.XtraReports.UI
Public Class pre_frmPresupuestosVsCuentasCTA

    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private Sub pre_frmPresupuestosVsCuentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lueMes.EditValue = Today.Month
        seAnio.Value = DateTime.Now.Year
    End Sub

    Private Sub pre_frmPresupuestosVsCuentas_Reporte() Handles MyBase.Reporte
        ExportarLibroExcel()
    End Sub

    Private Sub ExportarLibroExcel()
        If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Presupuestos.XLSX") Then
            If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Presupuestos.XLS") Then
                MsgBox("No existe la plantilla necesaria para poder exportar", MsgBoxStyle.Critical, "Nota")
                Exit Sub
            End If
        End If

        Dim Template As String

        Try
            If FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Presupuestos.XLSX") Then
                Template = Application.StartupPath & "\Plantillas\Presupuestos_TMP.XLSX"
                FileCopy(Application.StartupPath & "\Plantillas\Presupuestos.XLSX", Template)
            Else
                Template = Application.StartupPath & "\Plantillas\Presupuestos_TMP.XLS"
                FileCopy(Application.StartupPath & "\Plantillas\Presupuestos.XLS", Template)
            End If
        Catch ex As Exception
            MsgBox("Verifique que no esté abierto el documento en este momento" + Chr(13) + ex.Message(), MsgBoxStyle.Information, "Nota")
            Exit Sub
        End Try


        Dim oApp As Object = CreateObject("Excel.Application")

        'verifico si no está en una cultura diferente, por el momento de Inglés a Español, pendiente de verificar cuando sea a la inversa
        Dim oldCI As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        Try
            oApp.DisplayAlerts = False
        Catch ex As Exception
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")
            oApp.DisplayAlerts = False
        End Try

        Dim Range As String

        oApp.workbooks.Open(Template)
        oApp.Cells(2, 2).Value = gsNombre_Empresa
        oApp.Cells(3, 2).Value = "PRESUPUESTOS ANUAL"
        oApp.Cells(4, 2).Value = "HASTA EL MES DE: " + lueMes.Text

        Dim Linea As Integer = 7, i As Integer = 0, UltimoRegistro As Integer = 0
        Dim TotalPresupuesto As Decimal = 0.0, TotalReal As Decimal = 0.0, TotalDiferencia As Decimal = 0.0

        Dim nTotalFinal As Decimal = 0.0, TotalFila As Decimal = 0.0, TotalRubroGeneral As Decimal = 0.0
        Dim cId As Integer = 0, indice = 0
        Dim cUni As String = ""
        Dim cCentro As String = ""

        Dim dt As DataTable = bl.con_ReportePresupuesto(seAnio.EditValue, lueMes.EditValue)

        If 1 <= lueMes.EditValue Then
            oApp.Cells((Linea - 1), 4).Value = "ENERO"
            oApp.Cells(Linea, 4).Value = "PRESU."
            oApp.Cells(Linea, 5).Value = "REAL"
            oApp.Cells(Linea, 6).Value = "DIFERENCIA"
        End If

        If 2 <= lueMes.EditValue Then
            oApp.Cells((Linea - 1), 7).Value = "FEBRERO"
            oApp.Cells(Linea, 7).Value = "PRESU."
            oApp.Cells(Linea, 8).Value = "REAL"
            oApp.Cells(Linea, 9).Value = "DIFERENCIA"
        End If

        If 3 <= lueMes.EditValue Then
            oApp.Cells((Linea - 1), 10).Value = "MARZO"
            oApp.Cells(Linea, 10).Value = "PRESU."
            oApp.Cells(Linea, 11).Value = "REAL"
            oApp.Cells(Linea, 12).Value = "DIFERENCIA"
        End If

        If 4 <= lueMes.EditValue Then
            oApp.Cells((Linea - 1), 13).Value = "ABRIL"
            oApp.Cells(Linea, 13).Value = "PRESU."
            oApp.Cells(Linea, 14).Value = "REAL"
            oApp.Cells(Linea, 15).Value = "DIFERENCIA"
        End If

        If 5 <= lueMes.EditValue Then
            oApp.Cells((Linea - 1), 16).Value = "MAYO"
            oApp.Cells(Linea, 16).Value = "PRESU."
            oApp.Cells(Linea, 17).Value = "REAL"
            oApp.Cells(Linea, 18).Value = "DIFERENCIA"
        End If

        If 6 <= lueMes.EditValue Then
            oApp.Cells((Linea - 1), 19).Value = "JUNIO"
            oApp.Cells(Linea, 19).Value = "PRESU."
            oApp.Cells(Linea, 20).Value = "REAL"
            oApp.Cells(Linea, 21).Value = "DIFERENCIA"
        End If

        If 7 <= lueMes.EditValue Then
            oApp.Cells((Linea - 1), 22).Value = "JULIO"
            oApp.Cells(Linea, 22).Value = "PRESU."
            oApp.Cells(Linea, 23).Value = "REAL"
            oApp.Cells(Linea, 24).Value = "DIFERENCIA"
        End If

        If 8 <= lueMes.EditValue Then
            oApp.Cells((Linea - 1), 25).Value = "AGOSTO"
            oApp.Cells(Linea, 25).Value = "PRESU."
            oApp.Cells(Linea, 26).Value = "REAL"
            oApp.Cells(Linea, 27).Value = "DIFERENCIA"
        End If

        If 9 <= lueMes.EditValue Then
            oApp.Cells((Linea - 1), 28).Value = "SEPTIEMBRE"
            oApp.Cells(Linea, 28).Value = "PRESU."
            oApp.Cells(Linea, 29).Value = "REAL"
            oApp.Cells(Linea, 30).Value = "DIFERENCIA"
        End If

        If 10 <= lueMes.EditValue Then
            oApp.Cells((Linea - 1), 31).Value = "OCTUBRE"
            oApp.Cells(Linea, 31).Value = "PRESU."
            oApp.Cells(Linea, 32).Value = "REAL"
            oApp.Cells(Linea, 33).Value = "DIFERENCIA"
        End If
        If 11 <= lueMes.EditValue Then
            oApp.Cells((Linea - 1), 34).Value = "NOVIEMBRE"
            oApp.Cells(Linea, 34).Value = "PRESU."
            oApp.Cells(Linea, 35).Value = "REAL"
            oApp.Cells(Linea, 36).Value = "DIFERENCIA"
        End If
        If 12 <= lueMes.EditValue Then
            oApp.Cells((Linea - 1), 37).Value = "DICIEMBRE"
            oApp.Cells(Linea, 37).Value = "PRESU."
            oApp.Cells(Linea, 38).Value = "REAL"
            oApp.Cells(Linea, 39).Value = "DIFERENCIA"
        End If

        Linea += 1
        While i <= dt.Rows.Count - 1


            oApp.Cells(Linea, 2).Value = dt.Rows(i).Item("IdCuenta")
            oApp.Cells(Linea, 3).Value = dt.Rows(i).Item("Nombre")

            If 1 <= lueMes.EditValue Then
                oApp.Cells(Linea, 4).Value = dt.Rows(i).Item("Mes01")
                oApp.Cells(Linea, 5).Value = dt.Rows(i).Item("SaldoMes01")
                oApp.Cells(Linea, 6).Value = dt.Rows(i).Item("Diferencia01")

                UltimoRegistro = 6

                TotalPresupuesto += dt.Rows(i).Item("Mes01")
                TotalReal += dt.Rows(i).Item("SaldoMes01")
                TotalDiferencia += dt.Rows(i).Item("Diferencia01")
            End If
            If 2 <= lueMes.EditValue Then
                oApp.Cells(Linea, 7).Value = dt.Rows(i).Item("Mes02")
                oApp.Cells(Linea, 8).Value = dt.Rows(i).Item("SaldoMes02")
                oApp.Cells(Linea, 9).Value = dt.Rows(i).Item("Diferencia02")

                UltimoRegistro = 9
                TotalPresupuesto += dt.Rows(i).Item("Mes02")
                TotalReal += dt.Rows(i).Item("SaldoMes02")
                TotalDiferencia += dt.Rows(i).Item("Diferencia02")
            End If
            If 3 <= lueMes.EditValue Then
                oApp.Cells(Linea, 10).Value = dt.Rows(i).Item("Mes03")
                oApp.Cells(Linea, 11).Value = dt.Rows(i).Item("SaldoMes03")
                oApp.Cells(Linea, 12).Value = dt.Rows(i).Item("Diferencia03")

                UltimoRegistro = 12
                TotalPresupuesto += dt.Rows(i).Item("Mes03")
                TotalReal += dt.Rows(i).Item("SaldoMes03")
                TotalDiferencia += dt.Rows(i).Item("Diferencia03")

            End If
            If 4 <= lueMes.EditValue Then
                oApp.Cells(Linea, 13).Value = dt.Rows(i).Item("Mes04")
                oApp.Cells(Linea, 14).Value = dt.Rows(i).Item("SaldoMes04")
                oApp.Cells(Linea, 15).Value = dt.Rows(i).Item("Diferencia04")

                UltimoRegistro = 15
                TotalPresupuesto += dt.Rows(i).Item("Mes04")
                TotalReal += dt.Rows(i).Item("SaldoMes04")
                TotalDiferencia += dt.Rows(i).Item("Diferencia04")
            End If
            If 5 <= lueMes.EditValue Then
                oApp.Cells(Linea, 16).Value = dt.Rows(i).Item("Mes05")
                oApp.Cells(Linea, 17).Value = dt.Rows(i).Item("SaldoMes05")
                oApp.Cells(Linea, 18).Value = dt.Rows(i).Item("Diferencia05")

                UltimoRegistro = 18
                TotalPresupuesto += dt.Rows(i).Item("Mes05")
                TotalReal += dt.Rows(i).Item("SaldoMes05")
                TotalDiferencia += dt.Rows(i).Item("Diferencia05")
            End If
            If 6 <= lueMes.EditValue Then
                oApp.Cells(Linea, 19).Value = dt.Rows(i).Item("Mes06")
                oApp.Cells(Linea, 20).Value = dt.Rows(i).Item("SaldoMes06")
                oApp.Cells(Linea, 21).Value = dt.Rows(i).Item("Diferencia06")

                UltimoRegistro = 21
                TotalPresupuesto += dt.Rows(i).Item("Mes06")
                TotalReal += dt.Rows(i).Item("SaldoMes06")
                TotalDiferencia += dt.Rows(i).Item("Diferencia06")
            End If
            If 7 <= lueMes.EditValue Then
                oApp.Cells(Linea, 22).Value = dt.Rows(i).Item("Mes07")
                oApp.Cells(Linea, 23).Value = dt.Rows(i).Item("SaldoMes07")
                oApp.Cells(Linea, 24).Value = dt.Rows(i).Item("Diferencia07")

                UltimoRegistro = 24
                TotalPresupuesto += dt.Rows(i).Item("Mes07")
                TotalReal += dt.Rows(i).Item("SaldoMes07")
                TotalDiferencia += dt.Rows(i).Item("Diferencia07")
            End If
            If 8 <= lueMes.EditValue Then
                oApp.Cells(Linea, 25).Value = dt.Rows(i).Item("Mes08")
                oApp.Cells(Linea, 26).Value = dt.Rows(i).Item("SaldoMes08")
                oApp.Cells(Linea, 27).Value = dt.Rows(i).Item("Diferencia08")

                UltimoRegistro = 27
                TotalPresupuesto += dt.Rows(i).Item("Mes08")
                TotalReal += dt.Rows(i).Item("SaldoMes08")
                TotalDiferencia += dt.Rows(i).Item("Diferencia08")
            End If
            If 9 <= lueMes.EditValue Then
                oApp.Cells(Linea, 28).Value = dt.Rows(i).Item("Mes09")
                oApp.Cells(Linea, 29).Value = dt.Rows(i).Item("SaldoMes09")
                oApp.Cells(Linea, 30).Value = dt.Rows(i).Item("Diferencia09")

                UltimoRegistro = 30
                TotalPresupuesto += dt.Rows(i).Item("Mes09")
                TotalReal += dt.Rows(i).Item("SaldoMes09")
                TotalDiferencia += dt.Rows(i).Item("Diferencia09")
            End If
            If 10 <= lueMes.EditValue Then
                oApp.Cells(Linea, 31).Value = dt.Rows(i).Item("Mes10")
                oApp.Cells(Linea, 32).Value = dt.Rows(i).Item("SaldoMes10")
                oApp.Cells(Linea, 33).Value = dt.Rows(i).Item("Diferencia10")

                UltimoRegistro = 33
                TotalPresupuesto += dt.Rows(i).Item("Mes10")
                TotalReal += dt.Rows(i).Item("SaldoMes10")
                TotalDiferencia += dt.Rows(i).Item("Diferencia10")
            End If
            If 11 <= lueMes.EditValue Then
                oApp.Cells(Linea, 34).Value = dt.Rows(i).Item("Mes11")
                oApp.Cells(Linea, 35).Value = dt.Rows(i).Item("SaldoMes11")
                oApp.Cells(Linea, 36).Value = dt.Rows(i).Item("Diferencia11")

                UltimoRegistro = 36
                TotalPresupuesto += dt.Rows(i).Item("Mes11")
                TotalReal += dt.Rows(i).Item("SaldoMes11")
                TotalDiferencia += dt.Rows(i).Item("Diferencia11")
            End If
            If 12 <= lueMes.EditValue Then
                oApp.Cells(Linea, 37).Value = dt.Rows(i).Item("Mes12")
                oApp.Cells(Linea, 38).Value = dt.Rows(i).Item("SaldoMes12")
                oApp.Cells(Linea, 39).Value = dt.Rows(i).Item("Diferencia12")

                UltimoRegistro = 39
                TotalPresupuesto += dt.Rows(i).Item("Mes12")
                TotalReal += dt.Rows(i).Item("SaldoMes12")
                TotalDiferencia += dt.Rows(i).Item("Diferencia12")
            End If

            oApp.Cells(Linea, (UltimoRegistro + 1)).Value = TotalPresupuesto
            oApp.Cells(Linea, (UltimoRegistro + 2)).Value = TotalReal
            oApp.Cells(Linea, (UltimoRegistro + 3)).Value = TotalDiferencia


            i += 1
            Linea += 1

            TotalPresupuesto = 0.0
            TotalReal = 0.0
            TotalDiferencia = 0.0
        End While

        '.font.Size = 9
        oApp.Cells(6, (UltimoRegistro + 1)).Value = "TOTALES"
        oApp.Cells(6, (UltimoRegistro + 1)).font.Size = 10

        oApp.Cells(7, (UltimoRegistro + 1)).Value = "TOT. PRESU."
        oApp.Cells(7, (UltimoRegistro + 1)).font.Size = 10

        oApp.Cells(7, (UltimoRegistro + 2)).Value = "TOT. REAL"
        oApp.Cells(7, (UltimoRegistro + 1)).font.Size = 10

        oApp.Cells(7, (UltimoRegistro + 3)).Value = "TOT. DIFERENCIA"
        oApp.Cells(7, (UltimoRegistro + 1)).font.Size = 10

        oApp.Visible = True
        System.Threading.Thread.CurrentThread.CurrentCulture = oldCI

    End Sub
    'Private Sub PonerLineaExcel(ByVal oApp As Object, ByVal Linea As Integer)
    '    Dim Range As String = String.Format("A{0}:T{0}", CStr(Linea))
    '    oApp.Cells(5, 33).Copy()
    '    oApp.Range(Range).Select()
    '    oApp.ActiveSheet.Paste()
    'End Sub
    'Private Sub PonerLineaExcel2(ByVal oApp As Object, ByVal Linea As Integer)
    '    Dim Range As String = String.Format("E{0}:L{0}", CStr(Linea))
    '    oApp.Cells(5, 33).Copy()
    '    oApp.Range(Range).Select()
    '    oApp.ActiveSheet.Paste()
    'End Sub
    'Private Sub PonerLineaExcel3(ByVal oApp As Object, ByVal Linea As Integer)
    '    Dim Range As String = String.Format("E{0}:H{0}", CStr(Linea))
    '    oApp.Cells(5, 33).Copy()
    '    oApp.Range(Range).Select()
    '    oApp.ActiveSheet.Paste()
    'End Sub
End Class
