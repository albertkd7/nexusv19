﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmConcentracionMayor
    Inherits gen_frmBaseRpt
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(con_frmConcentracionMayor))
        Me.leTipoPartidas = New DevExpress.XtraEditors.LookUpEdit
        Me.deFec2 = New DevExpress.XtraEditors.DateEdit
        Me.deFec1 = New DevExpress.XtraEditors.DateEdit
        Me.txtTitulo = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.leTipoPartidas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFec2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFec2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFec1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFec1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.txtTitulo)
        Me.GroupControl1.Controls.Add(Me.leTipoPartidas)
        Me.GroupControl1.Controls.Add(Me.deFec2)
        Me.GroupControl1.Controls.Add(Me.deFec1)
        Me.GroupControl1.Size = New System.Drawing.Size(654, 350)
        '
        'leTipoPartidas
        '
        Me.leTipoPartidas.EnterMoveNextControl = True
        Me.leTipoPartidas.Location = New System.Drawing.Point(150, 101)
        Me.leTipoPartidas.Name = "leTipoPartidas"
        Me.leTipoPartidas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPartidas.Size = New System.Drawing.Size(338, 20)
        Me.leTipoPartidas.TabIndex = 2
        '
        'deFec2
        '
        Me.deFec2.EditValue = New Date(2008, 9, 14, 23, 35, 55, 97)
        Me.deFec2.EnterMoveNextControl = True
        Me.deFec2.Location = New System.Drawing.Point(150, 72)
        Me.deFec2.Name = "deFec2"
        Me.deFec2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFec2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFec2.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFec2.Size = New System.Drawing.Size(100, 20)
        Me.deFec2.TabIndex = 1
        '
        'deFec1
        '
        Me.deFec1.EditValue = New Date(2008, 9, 14, 23, 35, 31, 556)
        Me.deFec1.EnterMoveNextControl = True
        Me.deFec1.Location = New System.Drawing.Point(150, 39)
        Me.deFec1.Name = "deFec1"
        Me.deFec1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFec1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFec1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFec1.Size = New System.Drawing.Size(100, 20)
        Me.deFec1.TabIndex = 0
        '
        'txtTitulo
        '
        Me.txtTitulo.EditValue = "Concentración de Cuentas de Mayor"
        Me.txtTitulo.Location = New System.Drawing.Point(150, 159)
        Me.txtTitulo.Name = "txtTitulo"
        Me.txtTitulo.Size = New System.Drawing.Size(338, 20)
        Me.txtTitulo.TabIndex = 3
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(78, 42)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl1.TabIndex = 33
        Me.LabelControl1.Text = "Desde Fecha:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(80, 75)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 33
        Me.LabelControl2.Text = "Hasta Fecha:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(18, 108)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(126, 13)
        Me.LabelControl3.TabIndex = 33
        Me.LabelControl3.Text = "Tipo de Partida a Imprimir:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(58, 162)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl4.TabIndex = 33
        Me.LabelControl4.Text = "Título del reporte:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(100, 137)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl5.TabIndex = 35
        Me.LabelControl5.Text = "Sucursal:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(150, 133)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(338, 20)
        Me.leSucursal.TabIndex = 34
        '
        'con_frmConcentracionMayor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(654, 375)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmConcentracionMayor"
        Me.Text = "Concentración de debe y haber de cuentas de mayor"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.leTipoPartidas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFec2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFec2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFec1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFec1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents leTipoPartidas As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents deFec2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents deFec1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
End Class
