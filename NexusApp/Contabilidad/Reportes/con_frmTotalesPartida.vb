﻿Imports NexusBLL

Public Class con_frmTotalesPartida
    Dim bl As New ContabilidadBLL(g_ConnectionString)


    Private Sub cmd1_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.rptTotalesPartida(deFec1.DateTime, deFec2.DateTime, leTipoPartida.EditValue, ceDescuadre.EditValue, leSucursal.EditValue)
        Dim rpt As New con_rptTotalesPartida() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlPeriodo.Text = (FechaToString(deFec1.DateTime, deFec2.DateTime)).ToUpper
        rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
        rpt.ShowPreviewDialog()
    End Sub


    Private Sub frmPartidasReporte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFec1.DateTime = Today
        deFec2.DateTime = Today

        objCombos.conTiposPartida(leTipoPartida, "-- TODOS LOS TIPOS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub


End Class