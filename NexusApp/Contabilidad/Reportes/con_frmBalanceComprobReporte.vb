﻿Imports NexusELL.TableEntities
Imports NexusBLL

Public Class con_frmBalanceComprobReporte
    Dim bl As New ContabilidadBLL(g_ConnectionString), blAdmon As New AdmonBLL(g_ConnectionString)
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub con_frmBalanceComprobReporte_ExportarExcel() Handles Me.ExportarExcel
        DataTableToExcel(bl.con_rptBalanceComprobacionReporte(meMes.Month, seEjercicio.EditValue, spnNivel.EditValue, opgTipo.EditValue, leSucursal.EditValue), "")
        'ExportToExcel(bl.con_rptBalanceComprobacionReporte(meMes.Month, seEjercicio.EditValue, spnNivel.EditValue, opgTipo.EditValue))
    End Sub

    
    Private Sub con_frmBalanceComprobReporte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        bl.ObtienePeriodoContable(Mes, Ejercicio)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        meMes.Month = Mes
        seEjercicio.Text = Ejercicio
    End Sub

    Private Sub con_frmBalanceComprobReporte_Reporte() Handles Me.Reporte
        Dim dtFirma As DataTable = blAdmon.ObtieneParametros()
        Dim dt As DataTable = bl.con_rptBalanceComprobacionReporte(meMes.Month, seEjercicio.EditValue, spnNivel.EditValue, opgTipo.EditValue, leSucursal.EditValue)
        If ceFormatoCat.Checked Then

            Dim rpt As New con_rptBalanceReporteTipoCatalogo() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.EditValue
            If opgTipo.EditValue = 1 Then
                rpt.xrlPeriodo.Text = String.Format("SALDOS ACUMULADOS AL MES DE {0} DE {1}", (meMes.Text).ToUpper, seEjercicio.EditValue)
            Else
                rpt.xrlPeriodo.Text = String.Format("SALDOS DEL MES DE {0} DE {1}", (meMes.Text).ToUpper, seEjercicio.EditValue)
            End If
            rpt.xrlPage.Visible = ceDateInfo.Checked
            rpt.xrpNumero.Visible = ceDateInfo.Checked
            rpt.xrpFecha.Visible = ceDateInfo.Checked
            rpt.xrlDescMoneda.Text = gsDesc_Moneda
            With dtFirma.Rows(0)
                rpt.xrlFirmante1.Text = .Item("NombreFirmante1")
                rpt.xrlCargo1.Text = .Item("CargoFirmante1")
                rpt.xrLineaFirma1.Visible = .Item("CargoFirmante1") <> ""
                rpt.xrlFirmante2.Text = .Item("NombreFirmante2")
                rpt.xrlCargo2.Text = .Item("CargoFirmante2")
                rpt.xrLineaFirma2.Visible = .Item("CargoFirmante2") <> ""
                rpt.xrlFirmante3.Text = .Item("NombreFirmante3")
                rpt.xrlCargo3.Text = .Item("CargoFirmante3")
                rpt.xrLineaFirma3.Visible = .Item("CargoFirmante3") <> ""
                rpt.xrlFirmante4.Text = .Item("NombreFirmante4")
                rpt.xrlCargo4.Text = .Item("CargoFirmante4")
                rpt.xrLineaFirma4.Visible = .Item("CargoFirmante4") <> ""
                rpt.xrlFirmante5.Text = .Item("NombreFirmante5")
                rpt.xrlCargo5.Text = .Item("CargoFirmante5")
                rpt.xrLineaFirma5.Visible = .Item("CargoFirmante5") <> ""
                rpt.xrlFirmante6.Text = .Item("NombreFirmante6")
                rpt.xrlCargo6.Text = .Item("CargoFirmante6")
                rpt.xrLineaFirma6.Visible = .Item("CargoFirmante6") <> ""
            End With
            rpt.ShowPreview()
        Else
            Dim rpt As New con_rptBalanceComprobReporte() With {.DataSource = bl.con_rptBalanceComprobacionReporte(meMes.Month, seEjercicio.EditValue, spnNivel.EditValue, opgTipo.EditValue, leSucursal.EditValue), .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.EditValue
            If opgTipo.EditValue = 1 Then
                rpt.xrlPeriodo.Text = String.Format("SALDOS ACUMULADOS AL MES DE {0} DE {1}", (meMes.Text).ToUpper, seEjercicio.EditValue)
            Else
                rpt.xrlPeriodo.Text = String.Format("SALDOS DEL MES DE {0} DE {1}", (meMes.Text).ToUpper, seEjercicio.EditValue)
            End If
            rpt.xrlPage.Visible = ceDateInfo.Checked
            rpt.xrpNumero.Visible = ceDateInfo.Checked
            rpt.xrpFecha.Visible = ceDateInfo.Checked
            rpt.xrlDescMoneda.Text = gsDesc_Moneda
            With dtFirma.Rows(0)
                rpt.xrlFirmante1.Text = .Item("NombreFirmante1")
                rpt.xrlCargo1.Text = .Item("CargoFirmante1")
                rpt.xrLineaFirma1.Visible = .Item("CargoFirmante1") <> ""
                rpt.xrlFirmante2.Text = .Item("NombreFirmante2")
                rpt.xrlCargo2.Text = .Item("CargoFirmante2")
                rpt.xrLineaFirma2.Visible = .Item("CargoFirmante2") <> ""
                rpt.xrlFirmante3.Text = .Item("NombreFirmante3")
                rpt.xrlCargo3.Text = .Item("CargoFirmante3")
                rpt.xrLineaFirma3.Visible = .Item("CargoFirmante3") <> ""
                rpt.xrlFirmante4.Text = .Item("NombreFirmante4")
                rpt.xrlCargo4.Text = .Item("CargoFirmante4")
                rpt.xrLineaFirma4.Visible = .Item("CargoFirmante4") <> ""
                rpt.xrlFirmante5.Text = .Item("NombreFirmante5")
                rpt.xrlCargo5.Text = .Item("CargoFirmante5")
                rpt.xrLineaFirma5.Visible = .Item("CargoFirmante5") <> ""
                rpt.xrlFirmante6.Text = .Item("NombreFirmante6")
                rpt.xrlCargo6.Text = .Item("CargoFirmante6")
                rpt.xrLineaFirma6.Visible = .Item("CargoFirmante6") <> ""
            End With
            rpt.ShowPreview()
        End If
       
    End Sub
End Class
