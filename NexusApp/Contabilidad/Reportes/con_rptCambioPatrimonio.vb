﻿
Public Class con_rptCambioPatrimonio

    Private Sub xrlDescripcion_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlDescripcion.BeforePrint
        If GetCurrentColumnValue("IdCuenta") = "T" Then
            xrlDescripcion.Font = New Font("Arial", 8, FontStyle.Bold)
            xrlCapital.Font = New Font("Arial", 8, FontStyle.Bold)
            xrlReserva.Font = New Font("Arial", 8, FontStyle.Bold)
            xrlOtrasReservas.Font = New Font("Arial", 8, FontStyle.Bold)
            xrlUtilidad.Font = New Font("Arial", 8, FontStyle.Bold)
            xrlTotal.Font = New Font("Arial", 8, FontStyle.Bold)
        Else
            xrlDescripcion.Font = New Font("Arial", 8)
            xrlCapital.Font = New Font("Arial", 8)
            xrlReserva.Font = New Font("Arial", 8)
            xrlOtrasReservas.Font = New Font("Arial", 8)
            xrlUtilidad.Font = New Font("Arial", 8)
            xrlTotal.Font = New Font("Arial", 8)

        End If
    End Sub

End Class