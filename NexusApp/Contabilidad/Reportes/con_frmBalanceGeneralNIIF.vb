﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class con_frmBalanceGeneralNIIF
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub con_frmBalanceGeneral_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "--TODAS LAS SUCURSALES")
        bl.ObtienePeriodoContable(Mes, Ejercicio)
        seEjercicio.EditValue = Ejercicio
        meMes.Month = Mes
    End Sub

    Private Sub con_frmBalanceGeneral_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = blAdmon.ObtieneParametros()
        If opgNivel.Text = 1 Then
            Dim rpt As New con_rptBalanceGeneralNIIF() With {.DataSource = bl.con_rptBalanceGeneralNIIF(meMes.Month, seEjercicio.EditValue, opgTipo.EditValue, leSucursal.EditValue), .DataMember = ""}
            With dt.Rows(0)
                rpt.XrAnterior.Text = seEjercicio.EditValue - 1
                rpt.XrActual.Text = seEjercicio.EditValue
                rpt.xrlFirmante1.Text = .Item("NombreFirmante1")
                rpt.xrlCargo1.Text = .Item("CargoFirmante1")
                rpt.XrLine7.Visible = .Item("CargoFirmante1") <> ""
                rpt.xrlFirmante2.Text = .Item("NombreFirmante2")
                rpt.xrlCargo2.Text = .Item("CargoFirmante2")
                rpt.XrLine8.Visible = .Item("CargoFirmante2") <> ""
                rpt.xrlFirmante3.Text = .Item("NombreFirmante3")
                rpt.xrlCargo3.Text = .Item("CargoFirmante3")
                rpt.XrLine9.Visible = .Item("CargoFirmante3") <> ""
                rpt.xrlFirmante4.Text = .Item("NombreFirmante4")
                rpt.xrlCargo4.Text = .Item("CargoFirmante4")
                rpt.XrLine10.Visible = .Item("CargoFirmante4") <> ""
                rpt.xrlFirmante5.Text = .Item("NombreFirmante5")
                rpt.xrlCargo5.Text = .Item("CargoFirmante5")
                rpt.XrLine11.Visible = .Item("CargoFirmante5") <> ""
                rpt.xrlFirmante6.Text = .Item("NombreFirmante6")
                rpt.xrlCargo6.Text = .Item("CargoFirmante6")
                rpt.XrLine12.Visible = .Item("CargoFirmante6") <> ""
            End With
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.EditValue
            If opgTipo.EditValue = 1 Then
                Dim tmpFecha As String = String.Format("01/{0}/{1}", meMes.EditValue, seEjercicio.EditValue)
                Dim Fecha As Date = CDate(tmpFecha)
                Fecha = DateAdd(DateInterval.Month, 1, Fecha)
                Fecha = DateAdd(DateInterval.Day, -1, Fecha)
                rpt.xrlPeriodo.Text = "AL " & FechaToString(Fecha, Fecha).ToUpper()
            Else
                rpt.xrlPeriodo.Text = String.Format("Correspondiente al mes de {0} de {1}", (meMes.Text).ToUpper(), seEjercicio.EditValue)
            End If
            rpt.xrlDescMoneda.Text = gsDesc_Moneda
            rpt.ShowPreview()
        Else
            Dim rpt As New con_rptBalanceGeneralNIIF() With {.DataSource = bl.con_rptBalanceGeneralNIIF(meMes.Month, seEjercicio.EditValue, opgTipo.EditValue, leSucursal.EditValue), .DataMember = ""}
            rpt.XrAnterior.Text = seEjercicio.EditValue - 1
            rpt.XrActual.Text = seEjercicio.EditValue
            With dt.Rows(0)
                rpt.xrlFirmante1.Text = .Item(0)
                rpt.xrlCargo1.Text = .Item(1)
                rpt.xrlFirmante2.Text = .Item(2)
                rpt.xrlCargo2.Text = .Item(3)
                rpt.xrlFirmante3.Text = .Item(4)
                rpt.xrlCargo3.Text = .Item(5)
                rpt.xrlFirmante4.Text = .Item(6)
                rpt.xrlCargo4.Text = .Item(7)
                rpt.xrlFirmante5.Text = .Item(8)
                rpt.xrlCargo5.Text = .Item(9)
                rpt.xrlFirmante6.Text = .Item(10)
                rpt.xrlCargo6.Text = .Item(11)
            End With
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.EditValue
            If opgTipo.EditValue = 1 Then
                Dim tmpFecha As String = String.Format("01/{0}/{1}", meMes.EditValue, seEjercicio.EditValue)
                Dim Fecha As Date = CDate(tmpFecha)
                Fecha = DateAdd(DateInterval.Month, 1, Fecha)
                Fecha = DateAdd(DateInterval.Day, -1, Fecha)
                rpt.xrlPeriodo.Text = "AL " & (FechaToString(Fecha, Fecha)).ToUpper()
            Else
                rpt.xrlPeriodo.Text = String.Format("Correspondiente al mes de {0} de {1}", meMes.Text, seEjercicio.EditValue)
            End If
            rpt.xrlDescMoneda.Text = gsDesc_Moneda
            rpt.ShowPreviewDialog()
        End If
    End Sub

End Class
