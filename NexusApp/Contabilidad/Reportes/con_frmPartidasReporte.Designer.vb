﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmPartidasReporte
    Inherits gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(con_frmPartidasReporte))
        Me.cePagina = New DevExpress.XtraEditors.CheckEdit
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit
        Me.leTiposPartida = New DevExpress.XtraEditors.LookUpEdit
        Me.deFec2 = New DevExpress.XtraEditors.DateEdit
        Me.deFec1 = New DevExpress.XtraEditors.DateEdit
        Me.ceFechaHora = New DevExpress.XtraEditors.CheckEdit
        Me.ceGenerarSalto = New DevExpress.XtraEditors.CheckEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.teHasta = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.teDesde = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cePagina.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTiposPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFec2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFec2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFec1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFec1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceFechaHora.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceGenerarSalto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.teHasta)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.teDesde)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.ceGenerarSalto)
        Me.GroupControl1.Controls.Add(Me.ceFechaHora)
        Me.GroupControl1.Controls.Add(Me.cePagina)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.leTiposPartida)
        Me.GroupControl1.Controls.Add(Me.deFec2)
        Me.GroupControl1.Controls.Add(Me.deFec1)
        Me.GroupControl1.Size = New System.Drawing.Size(691, 346)
        '
        'cePagina
        '
        Me.cePagina.EditValue = True
        Me.cePagina.Location = New System.Drawing.Point(143, 233)
        Me.cePagina.Name = "cePagina"
        Me.cePagina.Properties.Caption = "Incluir númeración de páginas"
        Me.cePagina.Size = New System.Drawing.Size(238, 19)
        Me.cePagina.TabIndex = 37
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "Partida Contable"
        Me.teTitulo.EnterMoveNextControl = True
        Me.teTitulo.Location = New System.Drawing.Point(145, 160)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(304, 20)
        Me.teTitulo.TabIndex = 4
        '
        'leTiposPartida
        '
        Me.leTiposPartida.EnterMoveNextControl = True
        Me.leTiposPartida.Location = New System.Drawing.Point(145, 115)
        Me.leTiposPartida.Name = "leTiposPartida"
        Me.leTiposPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTiposPartida.Size = New System.Drawing.Size(304, 20)
        Me.leTiposPartida.TabIndex = 2
        '
        'deFec2
        '
        Me.deFec2.EditValue = New Date(2008, 9, 14, 23, 35, 55, 97)
        Me.deFec2.EnterMoveNextControl = True
        Me.deFec2.Location = New System.Drawing.Point(145, 75)
        Me.deFec2.Name = "deFec2"
        Me.deFec2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFec2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFec2.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFec2.Size = New System.Drawing.Size(100, 20)
        Me.deFec2.TabIndex = 1
        '
        'deFec1
        '
        Me.deFec1.EditValue = New Date(2008, 9, 14, 23, 35, 31, 556)
        Me.deFec1.EnterMoveNextControl = True
        Me.deFec1.Location = New System.Drawing.Point(145, 42)
        Me.deFec1.Name = "deFec1"
        Me.deFec1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFec1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFec1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFec1.Size = New System.Drawing.Size(100, 20)
        Me.deFec1.TabIndex = 0
        '
        'ceFechaHora
        '
        Me.ceFechaHora.EditValue = True
        Me.ceFechaHora.Location = New System.Drawing.Point(143, 258)
        Me.ceFechaHora.Name = "ceFechaHora"
        Me.ceFechaHora.Properties.Caption = "Incluir información de fecha y hora de impresión"
        Me.ceFechaHora.Size = New System.Drawing.Size(306, 19)
        Me.ceFechaHora.TabIndex = 37
        '
        'ceGenerarSalto
        '
        Me.ceGenerarSalto.Location = New System.Drawing.Point(143, 305)
        Me.ceGenerarSalto.Name = "ceGenerarSalto"
        Me.ceGenerarSalto.Properties.Caption = "Generar salto de página después de cada partida"
        Me.ceGenerarSalto.Size = New System.Drawing.Size(306, 19)
        Me.ceGenerarSalto.TabIndex = 38
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(76, 45)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl1.TabIndex = 39
        Me.LabelControl1.Text = "Desde Fecha:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(78, 78)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 39
        Me.LabelControl2.Text = "Hasta Fecha:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(16, 119)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(126, 13)
        Me.LabelControl3.TabIndex = 39
        Me.LabelControl3.Text = "Tipo de Partida a Imprimir:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(60, 163)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl4.TabIndex = 39
        Me.LabelControl4.Text = "Titulo del reporte"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(145, 209)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(209, 13)
        Me.LabelControl8.TabIndex = 68
        Me.LabelControl8.Text = "* En blanco para generar todas las partidas"
        '
        'teHasta
        '
        Me.teHasta.EditValue = ""
        Me.teHasta.EnterMoveNextControl = True
        Me.teHasta.Location = New System.Drawing.Point(349, 186)
        Me.teHasta.Name = "teHasta"
        Me.teHasta.Size = New System.Drawing.Size(100, 20)
        Me.teHasta.TabIndex = 6
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(272, 189)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl6.TabIndex = 67
        Me.LabelControl6.Text = "Hasta Número:"
        '
        'teDesde
        '
        Me.teDesde.EditValue = ""
        Me.teDesde.EnterMoveNextControl = True
        Me.teDesde.Location = New System.Drawing.Point(145, 186)
        Me.teDesde.Name = "teDesde"
        Me.teDesde.Size = New System.Drawing.Size(100, 20)
        Me.teDesde.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(68, 188)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(74, 13)
        Me.LabelControl5.TabIndex = 66
        Me.LabelControl5.Text = "Desde Número:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(98, 141)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl7.TabIndex = 70
        Me.LabelControl7.Text = "Sucursal:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(145, 137)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(304, 20)
        Me.leSucursal.TabIndex = 3
        '
        'con_frmPartidasReporte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(691, 371)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmPartidasReporte"
        Me.Text = "Partidas tipo reporte"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cePagina.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTiposPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFec2.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFec2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFec1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFec1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceFechaHora.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceGenerarSalto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cePagina As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leTiposPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents deFec2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents deFec1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents ceFechaHora As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceGenerarSalto As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teHasta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDesde As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
End Class
