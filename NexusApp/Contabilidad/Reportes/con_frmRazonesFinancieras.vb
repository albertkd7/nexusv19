﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class con_frmRazonesFinancieras
	Dim bl As New ContabilidadBLL(g_ConnectionString)
	Dim blAdmon As New AdmonBLL(g_ConnectionString)
	Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

	Private Sub con_frmBalanceGeneral_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
		objCombos.adm_Sucursales(leSucursal, objMenu.User, "--TODAS LAS SUCURSALES")
		bl.ObtienePeriodoContable(Mes, Ejercicio)
		seEjercicio.EditValue = Ejercicio
		meMes.Month = Mes
	End Sub


	Private Sub con_frmBalanceGeneral_Report_Click() Handles Me.Reporte
		Dim dbFirmante As DataTable = blAdmon.ObtieneParametros()
		Dim dt As DataTable = bl.con_rptRazonesFinancieras(meMes.Month, seEjercicio.EditValue, leSucursal.EditValue)

		Dim rpt As New con_rptRazonesFinancieras() With {.DataSource = dt, .DataMember = ""}

		rpt.xrlEmpresa.Text = gsNombre_Empresa
			rpt.xrlTitulo.Text = teTitulo.EditValue
			rpt.xrpFecha.Visible = ceDateInfo.Checked
			rpt.xrlPage.Visible = ceDateInfo.Checked
		rpt.xrpFecha.Visible = ceDateInfo.Checked
		rpt.xrlPeriodo.Text = String.Format("CORRESPONDIENTE AL MES {0} DE {1}", (meMes.Text).ToUpper(), seEjercicio.EditValue)
		rpt.ShowPreviewDialog

		Dim rpt2 As New con_rptRazonesFinancieras2() With {.DataSource = dt, .DataMember = ""}

		rpt2.xrlEmpresa.Text = gsNombre_Empresa
		rpt2.xrlTitulo.Text = teTitulo.EditValue
		rpt2.xrpFecha.Visible = ceDateInfo.Checked
		rpt2.xrlPage.Visible = ceDateInfo.Checked
		rpt2.xrpFecha.Visible = ceDateInfo.Checked
		rpt2.xrlPeriodo.Text = String.Format("CORRESPONDIENTE AL MES {0} DE {1}", (meMes.Text).ToUpper(), seEjercicio.EditValue)
		rpt2.ShowPreviewDialog

	End Sub

End Class
