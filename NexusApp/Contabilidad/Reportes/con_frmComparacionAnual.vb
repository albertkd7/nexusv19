﻿Imports NexusBLL
Public Class con_frmComparacionAnual
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Private Sub con_frmComparacionAnual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        bl.ObtienePeriodoContable(Mes, Ejercicio)
        seEjercicio.EditValue = Ejercicio
        objCombos.adm_Sucursales(lesucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub
    

    Private Sub con_frmComparacionAnual_Reporte() Handles Me.Reporte
        Dim rpt As New con_rptComparacionAnual() With {.DataSource = bl.rptComparacionAnual(seEjercicio.EditValue, spnNivel.EditValue, rgTipo.EditValue, BeCtaContable1.beIdCuenta.EditValue, BeCtaContable2.beIdCuenta.EditValue, leSucursal.EditValue), .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.Parameters(0).Value = ceIncluir.Checked
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
        rpt.ShowPreview()
    End Sub
End Class
