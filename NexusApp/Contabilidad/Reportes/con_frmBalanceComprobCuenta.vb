﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class con_frmBalanceComprobCuenta
    Dim bl As New ContabilidadBLL(g_ConnectionString), blAdmon As New AdmonBLL(g_ConnectionString)
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Private Sub con_frmBalanceComprobCuenta_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "--TODAS LAS SUCURSALES")
        bl.ObtienePeriodoContable(Mes, Ejercicio)
        meMes.Month = Mes
        seEjercicio.Value = Ejercicio
    End Sub

    Private Sub con_frmBalanceComprobCuenta_Reporte() Handles Me.Reporte
        Dim dt As New DataTable
        Dim dbFirmante As DataTable = blAdmon.ObtieneParametros()
        If opgNivel.EditValue = 1 Then 'resumen
            dt = bl.con_rptBalanceComprobacionResumen(seEjercicio.Value, meMes.Month, opgTipo.EditValue, leSucursal.EditValue)
            Dim rpt As New con_rptBalanceTipoCuentaResumen
            rpt.DataSource = dt
            rpt.DataMember = ""
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.Text
            If opgTipo.EditValue = 1 Then
                rpt.xrlPeriodo.Text = String.Format("SALDOS ACUMULADOS AL MES DE {0} DE {1}", ObtieneMesString(meMes.Month).ToUpper, seEjercicio.EditValue)
            Else
                rpt.xrlPeriodo.Text = String.Format("CORRESPONDIENTE AL MES DE {0} DE {1}", ObtieneMesString(meMes.Month).ToUpper, seEjercicio.EditValue)
            End If
            rpt.xrlPage.Visible = ceDateInfo.Checked
            rpt.xrpNumero.Visible = ceDateInfo.Checked
            rpt.xrpFecha.Visible = ceDateInfo.Checked
            rpt.xrlDescMoneda.Text = gsDesc_Moneda
            With dbFirmante.Rows(0)
                rpt.xrlFirmante1.Text = .Item("NombreFirmante1")
                rpt.xrlCargo1.Text = .Item("CargoFirmante1")
                rpt.xrLineaFirma1.Visible = .Item("CargoFirmante1") <> ""
                rpt.xrlFirmante2.Text = .Item("NombreFirmante2")
                rpt.xrlCargo2.Text = .Item("CargoFirmante2")
                rpt.xrLineaFirma2.Visible = .Item("CargoFirmante2") <> ""
                rpt.xrlFirmante3.Text = .Item("NombreFirmante3")
                rpt.xrlCargo3.Text = .Item("CargoFirmante3")
                rpt.xrLineaFirma3.Visible = .Item("CargoFirmante3") <> ""
                rpt.xrlFirmante4.Text = .Item("NombreFirmante4")
                rpt.xrlCargo4.Text = .Item("CargoFirmante4")
                rpt.xrLineaFirma4.Visible = .Item("CargoFirmante4") <> ""
                rpt.xrlFirmante5.Text = .Item("NombreFirmante5")
                rpt.xrlCargo5.Text = .Item("CargoFirmante5")
                rpt.xrLineaFirma5.Visible = .Item("CargoFirmante5") <> ""
                rpt.xrlFirmante6.Text = .Item("NombreFirmante6")
                rpt.xrlCargo6.Text = .Item("CargoFirmante6")
                rpt.xrLineaFirma6.Visible = .Item("CargoFirmante6") <> ""
            End With
            
            rpt.ShowPreviewDialog()
        Else
            dt = bl.con_rptBalanceComprobacionDetalle(seEjercicio.Value, meMes.Month, opgTipo.EditValue, leSucursal.EditValue)
            Dim rpt As New con_rptBalanceTipoCuentaDetalle
            rpt.DataSource = dt
            rpt.DataMember = ""
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.EditValue
            If opgTipo.EditValue = 1 Then
                rpt.xrlPeriodo.Text = String.Format("SALDOS ACUMULADOS AL MES DE {0} DE {1}", ObtieneMesString(meMes.Month).ToUpper, seEjercicio.EditValue)
            Else
                rpt.xrlPeriodo.Text = String.Format("CORRESPONDIENTE AL MES {0} DE {1}", ObtieneMesString(meMes.Month).ToUpper, seEjercicio.EditValue)
            End If
            rpt.xrlPage.Visible = ceDateInfo.Checked
            rpt.xrpNumero.Visible = ceDateInfo.Checked
            rpt.xrpFecha.Visible = ceDateInfo.Checked
            rpt.xrlDescMoneda.Text = gsDesc_Moneda
            With dbFirmante.Rows(0)
                rpt.xrlFirmante1.Text = .Item("NombreFirmante1")
                rpt.xrlCargo1.Text = .Item("CargoFirmante1")
                rpt.xrLineaFirma1.Visible = .Item("CargoFirmante1") <> ""
                rpt.xrlFirmante2.Text = .Item("NombreFirmante2")
                rpt.xrlCargo2.Text = .Item("CargoFirmante2")
                rpt.xrLineaFirma2.Visible = .Item("CargoFirmante2") <> ""
                rpt.xrlFirmante3.Text = .Item("NombreFirmante3")
                rpt.xrlCargo3.Text = .Item("CargoFirmante3")
                rpt.xrLineaFirma3.Visible = .Item("CargoFirmante3") <> ""
                rpt.xrlFirmante4.Text = .Item("NombreFirmante4")
                rpt.xrlCargo4.Text = .Item("CargoFirmante4")
                rpt.xrLineaFirma4.Visible = .Item("CargoFirmante4") <> ""
                rpt.xrlFirmante5.Text = .Item("NombreFirmante5")
                rpt.xrlCargo5.Text = .Item("CargoFirmante5")
                rpt.xrLineaFirma5.Visible = .Item("CargoFirmante5") <> ""
                rpt.xrlFirmante6.Text = .Item("NombreFirmante6")
                rpt.xrlCargo6.Text = .Item("CargoFirmante6")
                rpt.xrLineaFirma6.Visible = .Item("CargoFirmante6") <> ""
            End With

            rpt.ShowPreviewDialog()
        End If
    End Sub

End Class