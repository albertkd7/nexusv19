﻿Imports NexusBLL

Public Class pre_frmPresupuestoAnualSuc

    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private Sub pre_frmPresupuestoAnualSuc_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(lueSucursales, objMenu.User, "")
        seAnio.Value = DateTime.Now.Year
    End Sub

    Private Sub pre_frmPresupuestoAnualSuc_Reporte() Handles MyBase.Reporte

        If lueSucursales.EditValue = 0 Then
            MsgBox("Debe seleccionar la sucursal antes de presentar el reporte.", MsgBoxStyle.Critical)
            lueSucursales.Focus()
            Return
        End If

        Dim rpt As New pre_rptPresupuestosAnualSuc

        rpt.DataSource = bl.pre_PresupuestoAnualSuc(seAnio.Value, lueSucursales.EditValue)

        rpt.DataMember = ""

        rpt.xrlTituloEmpresa.Text = gsNombre_Empresa
        rpt.xrlNombreReporte.Text = "REPORTE DE PRESUPUESTOS ANUAL PARA LA SUCURSAL DE " & lueSucursales.Text
        rpt.xrlOtrainfo.Text = "CORRESPONDIENTE AL AÑO " & seAnio.EditValue

        rpt.ShowPreviewDialog()

    End Sub
End Class
