﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class con_frmFlujoEfectivoNIIF
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim Mes As Integer
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub con_frmFlujoEfectivoNIIF_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Mes = 12
        seEjercicio.EditValue = Today.Year - 1
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "--TODAS LAS SUCURSALES")
    End Sub

    Private Sub con_frmFlujoEfectivoNIIF_Reporte() Handles Me.Reporte
        Dim dt As New DataTable
        dt = objTablas.con_RubroFlujoDetalleSelectAll
        If dt.Rows.Count <= 0 Then
            MsgBox("Aun no se han clasificado las cuentas para el flujo de efectivo", MsgBoxStyle.OkOnly, "Cuentas")
            Exit Sub
        End If

        dt = bl.con_AniosCerradosFlujoEfectivo(seEjercicio.EditValue)
        If dt.Rows.Count <= 1 Then
            MsgBox("No hay suficientes ejercicios contables en la base de datos", MsgBoxStyle.OkOnly, "Ejercicio")
            Exit Sub
        End If

        If dt.Select("Ejercicio = " + seEjercicio.EditValue.ToString).Length <= 0 Then
            MsgBox("El año seleccionado, aun no se ha cerrado o no existe en la base de datos", MsgBoxStyle.OkOnly, "Cierre")
            Exit Sub
        End If
        
        
        Dim dbFirmante As DataTable
        dbFirmante = blAdmon.ObtieneParametros()
        Dim rpt As New con_rptFlujoEfectivoNIIF() With {.DataSource = bl.rptFlujoEfectivoNIIF(Mes, seEjercicio.EditValue, leSucursal.EditValue), .DataMember = ""}
        With dbFirmante.Rows(0)
            rpt.XrAnterior.Text = seEjercicio.EditValue - 1
            rpt.XrActual.Text = seEjercicio.EditValue
            rpt.xrlFirmante1.Text = .Item("NombreFirmante1")
            rpt.xrlCargo1.Text = .Item("CargoFirmante1")
            rpt.XrLine7.Visible = .Item("CargoFirmante1") <> ""
            rpt.xrlFirmante2.Text = .Item("NombreFirmante2")
            rpt.xrlCargo2.Text = .Item("CargoFirmante2")
            rpt.XrLine8.Visible = .Item("CargoFirmante2") <> ""
            rpt.xrlFirmante3.Text = .Item("NombreFirmante3")
            rpt.xrlCargo3.Text = .Item("CargoFirmante3")
            rpt.XrLine9.Visible = .Item("CargoFirmante3") <> ""
            rpt.xrlFirmante4.Text = .Item("NombreFirmante4")
            rpt.xrlCargo4.Text = .Item("CargoFirmante4")
            rpt.XrLine10.Visible = .Item("CargoFirmante4") <> ""
            rpt.xrlFirmante5.Text = .Item("NombreFirmante5")
            rpt.xrlCargo5.Text = .Item("CargoFirmante5")
            rpt.XrLine11.Visible = .Item("CargoFirmante5") <> ""
            rpt.xrlFirmante6.Text = .Item("NombreFirmante6")
            rpt.xrlCargo6.Text = .Item("CargoFirmante6")
            rpt.XrLine12.Visible = .Item("CargoFirmante6") <> ""
        End With
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        If opgTipo.EditValue = 1 Then
            Dim tmpFecha As String = String.Format("01/{0}/{1}", 12, seEjercicio.EditValue)
            Dim Fecha As Date = CDate(tmpFecha)
            Fecha = DateAdd(DateInterval.Month, 1, Fecha)
            Fecha = DateAdd(DateInterval.Day, -1, Fecha)
            rpt.xrlPeriodo.Text = "AL " & FechaToString(Fecha, Fecha).ToUpper()
        Else
            rpt.xrlPeriodo.Text = String.Format("Correspondiente al mes de {0} de {1}", Mes.ToString.ToUpper(), seEjercicio.EditValue)
        End If
        rpt.xrlDescMoneda.Text = gsDesc_Moneda
        rpt.ShowPreview()
    End Sub
End Class
