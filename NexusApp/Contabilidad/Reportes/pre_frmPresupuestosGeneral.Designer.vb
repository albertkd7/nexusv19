﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pre_frmPresupuestosGeneral
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.seAnio = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.rgOrdenadoPor = New DevExpress.XtraEditors.RadioGroup
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.lueMes = New DevExpress.XtraScheduler.UI.MonthEdit
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgOrdenadoPor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lueMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.lueMes)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.rgOrdenadoPor)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.seAnio)
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(13, 37)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(23, 13)
        Me.LabelControl4.TabIndex = 101
        Me.LabelControl4.Text = "Mes:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(198, 37)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl3.TabIndex = 100
        Me.LabelControl3.Text = "Año / Ejercicio:"
        '
        'seAnio
        '
        Me.seAnio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seAnio.Location = New System.Drawing.Point(291, 34)
        Me.seAnio.Name = "seAnio"
        Me.seAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seAnio.Properties.IsFloatValue = False
        Me.seAnio.Properties.Mask.EditMask = "N00"
        Me.seAnio.Size = New System.Drawing.Size(99, 20)
        Me.seAnio.TabIndex = 99
        '
        'LabelControl1
        '
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(14, 155)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(377, 47)
        Me.LabelControl1.TabIndex = 103
        Me.LabelControl1.Text = "Nota: El campo ""año / ejercicio"" es obligatorio, el campo mes es opcional," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "si no" & _
            " hace ninguna selección del mes se tomarán todos los presupuestos del " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "año sele" & _
            "ccionado."
        '
        'rgOrdenadoPor
        '
        Me.rgOrdenadoPor.EditValue = "succcdepto"
        Me.rgOrdenadoPor.Location = New System.Drawing.Point(13, 77)
        Me.rgOrdenadoPor.Name = "rgOrdenadoPor"
        Me.rgOrdenadoPor.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.rgOrdenadoPor.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("succcdepto", "Por Suc / CC / Depto"), New DevExpress.XtraEditors.Controls.RadioGroupItem("sucdeptocc", "Por Suc / Depto / CC"), New DevExpress.XtraEditors.Controls.RadioGroupItem("deptoccsuc", "Por Depto / CC / Suc"), New DevExpress.XtraEditors.Controls.RadioGroupItem("deptosuccc", "Por Depto / Suc / CC"), New DevExpress.XtraEditors.Controls.RadioGroupItem("ccsucdepto", "Por CC / Suc / Depto")})
        Me.rgOrdenadoPor.Size = New System.Drawing.Size(377, 72)
        Me.rgOrdenadoPor.TabIndex = 104
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(14, 62)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl2.TabIndex = 105
        Me.LabelControl2.Text = "Ordén del reporte"
        '
        'lueMes
        '
        Me.lueMes.Location = New System.Drawing.Point(79, 34)
        Me.lueMes.Name = "lueMes"
        Me.lueMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueMes.Size = New System.Drawing.Size(100, 20)
        Me.lueMes.TabIndex = 106
        '
        'pre_frmPresupuestosGeneral
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(839, 314)
        Me.Modulo = "Contabilidad"
        Me.Name = "pre_frmPresupuestosGeneral"
        Me.Text = "Reporte de Presupuestos géneral"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgOrdenadoPor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lueMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgOrdenadoPor As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lueMes As DevExpress.XtraScheduler.UI.MonthEdit

End Class
