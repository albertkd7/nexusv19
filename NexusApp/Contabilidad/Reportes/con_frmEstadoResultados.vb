﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class con_frmEstadoResultados
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub con_frmEstadoResultados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "--TODAS LAS SUCURSALES--")
        bl.ObtienePeriodoContable(Mes, Ejercicio)
        meMes.Month = Mes
        seEjercicio.EditValue = Ejercicio
    End Sub

    Private Sub con_frmEstadoResultados_Reporte() Handles Me.Reporte
        Dim dbFirmante As DataTable = blAdmon.ObtieneParametros()

        Dim rpt As New con_rptEstadoResultados() With {.DataSource = bl.con_rptEstadoResultados(meMes.Month, seEjercicio.EditValue, opgNivel.EditValue + 1, opgTipo.EditValue, leSucursal.EditValue), .DataMember = ""}
        rpt.xrlFirmante1.Text = dbFirmante.Rows(0).Item("NombreFirmante1")
        rpt.xrlCargo1.Text = dbFirmante.Rows(0).Item("CargoFirmante1")
        rpt.xrlFirmante2.Text = dbFirmante.Rows(0).Item("NombreFirmante2")
        rpt.xrlCargo2.Text = dbFirmante.Rows(0).Item("CargoFirmante2")
        rpt.xrlFirmante3.Text = dbFirmante.Rows(0).Item("NombreFirmante3")
        rpt.xrlCargo3.Text = dbFirmante.Rows(0).Item("CargoFirmante3")
        rpt.xrlFirmante4.Text = dbFirmante.Rows(0).Item("NombreFirmante4")
        rpt.xrlCargo4.Text = dbFirmante.Rows(0).Item("CargoFirmante4")
        rpt.xrlFirmante5.Text = dbFirmante.Rows(0).Item("NombreFirmante5")
        rpt.xrlCargo5.Text = dbFirmante.Rows(0).Item("CargoFirmante5")
        rpt.xrlFirmante6.Text = dbFirmante.Rows(0).Item("NombreFirmante6")
        rpt.xrlCargo6.Text = dbFirmante.Rows(0).Item("CargoFirmante6")
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue

        If opgTipo.EditValue = 1 Then
            Dim tmpFecha As String = String.Format("01/{0}/{1}", meMes.EditValue, seEjercicio.EditValue)
            Dim Fecha As Date = CDate(tmpFecha)
            Fecha = DateAdd(DateInterval.Month, 1, Fecha)
            Fecha = DateAdd(DateInterval.Day, -1, Fecha)
            rpt.xrlPeriodo.Text = "Del 1 de enero al " & FechaToString(Fecha, Fecha)
        Else
            rpt.xrlPeriodo.Text = String.Format("Correspondiente al mes de {0} de {1}", meMes.Text, seEjercicio.EditValue)
        End If

        rpt.xrlDescMoneda.Text = gsDesc_Moneda
        rpt.ShowPreview()
    End Sub

End Class
