﻿Imports NexusBLL
Imports System.Data.SqlClient

Public Class pre_frmPresupuestosVsCuentas

    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private Sub pre_frmPresupuestosVsCuentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lueMes.EditValue = Today.Month
        seAnio.Value = DateTime.Now.Year
        objCombos.adm_Sucursales(lueSucursales, objMenu.User, "--TODAS LAS SUCURSALES--")
        objCombos.con_CentrosCosto(lueCentroCostos, "", "")
    End Sub
    Private Sub lueSucursales_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lueSucursales.EditValueChanged
        objCombos.pre_Departamentos(lueDepartamentos, lueSucursales.EditValue, "--TODOS LOS DEPARTAMENTOS--")
    End Sub
    Private Sub pre_frmPresupuestosVsCuentas_Reporte() Handles MyBase.Reporte


        Dim rpt As New pre_rptPresupuestosVsCuentas
        Dim adapter As New SqlDataAdapter

        'Dim dt As dsPresupuestos.pre_PresupuestosVsCuentasDataTable = bl.pre_PresupuestosVsCuentas(seAnio.Value, lueMes.SelectedIndex)

        rpt.DataSource = bl.pre_PresupuestosVsCuentas(seAnio.Value, lueMes.EditValue, lueSucursales.EditValue, lueDepartamentos.EditValue, lueCentroCostos.EditValue)
        rpt.DataMember = ""

        rpt.xrlTituloEmpresa.Text = gsNombre_Empresa
        rpt.xrlNombreReporte.Text = "REPORTE DE PRESUPUESTOS VS COSTOS REALES."
        rpt.xrlOtrainfo.Text = "CORRESPONDIENTE AL MES DE  " & ObtieneMesString(lueMes.EditValue) & " DEL AÑO " & seAnio.EditValue

        rpt.ShowPreviewDialog()

    End Sub

End Class
