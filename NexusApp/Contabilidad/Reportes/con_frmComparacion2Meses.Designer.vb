﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmComparacion2Meses
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ceIncluir = New DevExpress.XtraEditors.CheckEdit
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.rgTipoReporte = New DevExpress.XtraEditors.RadioGroup
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.spnNivel = New DevExpress.XtraEditors.SpinEdit
        Me.seAnio2 = New DevExpress.XtraEditors.SpinEdit
        Me.seAnio1 = New DevExpress.XtraEditors.SpinEdit
        Me.meMes2 = New DevExpress.XtraScheduler.UI.MonthEdit
        Me.meMes1 = New DevExpress.XtraScheduler.UI.MonthEdit
        Me.BeCtaContable1 = New Nexus.beCtaContable
        Me.BeCtaContable2 = New Nexus.beCtaContable
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.ceIncluir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipoReporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.spnNivel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seAnio2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seAnio1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.BeCtaContable2)
        Me.GroupControl1.Controls.Add(Me.BeCtaContable1)
        Me.GroupControl1.Controls.Add(Me.ceIncluir)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.rgTipoReporte)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.spnNivel)
        Me.GroupControl1.Controls.Add(Me.seAnio2)
        Me.GroupControl1.Controls.Add(Me.seAnio1)
        Me.GroupControl1.Controls.Add(Me.meMes2)
        Me.GroupControl1.Controls.Add(Me.meMes1)
        Me.GroupControl1.Size = New System.Drawing.Size(736, 405)
        '
        'ceIncluir
        '
        Me.ceIncluir.Location = New System.Drawing.Point(137, 251)
        Me.ceIncluir.Name = "ceIncluir"
        Me.ceIncluir.Properties.Caption = "Incluir códigos de cuenta en el Reporte"
        Me.ceIncluir.Size = New System.Drawing.Size(226, 19)
        Me.ceIncluir.TabIndex = 8
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(66, 208)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(70, 13)
        Me.LabelControl7.TabIndex = 33
        Me.LabelControl7.Text = "Hasta Cuenta:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(64, 158)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl6.TabIndex = 34
        Me.LabelControl6.Text = "Desde Cuenta:"
        '
        'rgTipoReporte
        '
        Me.rgTipoReporte.EditValue = 1
        Me.rgTipoReporte.Location = New System.Drawing.Point(139, 67)
        Me.rgTipoReporte.Name = "rgTipoReporte"
        Me.rgTipoReporte.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Basado en Saldos Acumulados"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Basado en Saldos del mes")})
        Me.rgTipoReporte.Size = New System.Drawing.Size(171, 52)
        Me.rgTipoReporte.TabIndex = 4
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "INFORME COMPARATIVO DE SALDOS"
        Me.teTitulo.Location = New System.Drawing.Point(139, 276)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(375, 20)
        Me.teTitulo.TabIndex = 9
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(50, 280)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl3.TabIndex = 32
        Me.LabelControl3.Text = "Título del informe:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(58, 132)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl2.TabIndex = 31
        Me.LabelControl2.Text = "Nivel de Detalle:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(332, 41)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl4.TabIndex = 30
        Me.LabelControl4.Text = "Comparar con:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(17, 71)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(119, 13)
        Me.LabelControl5.TabIndex = 28
        Me.LabelControl5.Text = "Tipo de Reporte a Emitir:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(54, 41)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl1.TabIndex = 29
        Me.LabelControl1.Text = "Mes a Comparar:"
        '
        'spnNivel
        '
        Me.spnNivel.EditValue = New Decimal(New Integer() {4, 0, 0, 0})
        Me.spnNivel.Location = New System.Drawing.Point(139, 129)
        Me.spnNivel.Name = "spnNivel"
        Me.spnNivel.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.spnNivel.Size = New System.Drawing.Size(43, 20)
        Me.spnNivel.TabIndex = 5
        '
        'seAnio2
        '
        Me.seAnio2.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seAnio2.Location = New System.Drawing.Point(508, 38)
        Me.seAnio2.Name = "seAnio2"
        Me.seAnio2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seAnio2.Properties.Mask.EditMask = "n0"
        Me.seAnio2.Size = New System.Drawing.Size(69, 20)
        Me.seAnio2.TabIndex = 3
        '
        'seAnio1
        '
        Me.seAnio1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seAnio1.Location = New System.Drawing.Point(241, 38)
        Me.seAnio1.Name = "seAnio1"
        Me.seAnio1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seAnio1.Properties.Mask.EditMask = "n0"
        Me.seAnio1.Properties.Mask.IgnoreMaskBlank = False
        Me.seAnio1.Size = New System.Drawing.Size(69, 20)
        Me.seAnio1.TabIndex = 1
        '
        'meMes2
        '
        Me.meMes2.Location = New System.Drawing.Point(406, 38)
        Me.meMes2.Name = "meMes2"
        Me.meMes2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes2.Size = New System.Drawing.Size(100, 20)
        Me.meMes2.TabIndex = 2
        '
        'meMes1
        '
        Me.meMes1.Location = New System.Drawing.Point(139, 38)
        Me.meMes1.Name = "meMes1"
        Me.meMes1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes1.Size = New System.Drawing.Size(100, 20)
        Me.meMes1.TabIndex = 0
        '
        'BeCtaContable1
        '
        Me.BeCtaContable1.Location = New System.Drawing.Point(136, 155)
        Me.BeCtaContable1.Name = "BeCtaContable1"
        Me.BeCtaContable1.Size = New System.Drawing.Size(594, 20)
        Me.BeCtaContable1.TabIndex = 6
        Me.BeCtaContable1.ValidarMayor = False
        '
        'BeCtaContable2
        '
        Me.BeCtaContable2.Location = New System.Drawing.Point(136, 205)
        Me.BeCtaContable2.Name = "BeCtaContable2"
        Me.BeCtaContable2.Size = New System.Drawing.Size(594, 20)
        Me.BeCtaContable2.TabIndex = 7
        Me.BeCtaContable2.ValidarMayor = False
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(89, 232)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl8.TabIndex = 37
        Me.LabelControl8.Text = "Sucursal:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(139, 228)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(375, 20)
        Me.leSucursal.TabIndex = 36
        '
        'con_frmComparacion2Meses
        '
        Me.Appearance.BackColor = System.Drawing.Color.White
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(736, 430)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmComparacion2Meses"
        Me.Text = "Comparación de saldos de dos meses cualquiera"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.ceIncluir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipoReporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.spnNivel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seAnio2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seAnio1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ceIncluir As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgTipoReporte As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents spnNivel As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seAnio2 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seAnio1 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents meMes2 As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents meMes1 As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents BeCtaContable2 As Nexus.beCtaContable
    Friend WithEvents BeCtaContable1 As Nexus.beCtaContable
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit

End Class
