﻿Imports NexusBLL

Public Class pre_frmPresupuestoCostosReales

    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private Sub pre_frmPresupuestoCostosReales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lueMes.SelectedIndex = 0
        seAnio.Value = DateTime.Now.Year
        objCombos.adm_Sucursales(lueSucursales, objMenu.User, "")
    End Sub

    Private Sub lueSucursales_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lueSucursales.EditValueChanged
        objCombos.pre_Departamentos(lueDepartamentos, lueSucursales.EditValue)
    End Sub

    Private Sub lueDepartamentos_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lueDepartamentos.EditValueChanged
        objCombos.con_CentrosCosto(lueCentroCostos, "", "")
    End Sub

    Private Sub pre_frmPresupuestoCostosReales_Reporte() Handles MyBase.Reporte

        Dim idPresupuesto As Integer

        If lueSucursales.EditValue = 0 Then
            MsgBox("Debe seleccionar la sucursal antes de presentar el reporte.", MsgBoxStyle.Critical)
            lueSucursales.Focus()
            Return
        End If

        If lueDepartamentos.EditValue = 0 Then
            MsgBox("Debe seleccionar el departamento antes de presentar el reporte.", MsgBoxStyle.Critical)
            lueDepartamentos.Focus()
            Return
        End If

        If lueCentroCostos.EditValue = 0 Then
            MsgBox("Debe seleccionar el centrro de costos antes de presentar el reporte.", MsgBoxStyle.Critical)
            lueCentroCostos.Focus()
            Return
        End If

        If lueMes.EditValue = 0 Then
            If MsgBox("No ha seleccionado un mes, Se tomarán todos los presupuestos del año " & seAnio.Value & " ¿Desea continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Return
            End If
        End If

        Dim table As DataTable
        Dim rpt As New pre_rptCostosRealesPresupuesto

        If lueMes.EditValue = 0 Then
            table = bl.pre_PresupuestoCabecera(seAnio.EditValue, lueSucursales.EditValue, lueDepartamentos.EditValue, lueCentroCostos.EditValue)
        Else
            table = bl.pre_PresupuestoCabeceraMes(seAnio.EditValue, lueSucursales.EditValue, lueDepartamentos.EditValue, lueCentroCostos.EditValue, lueMes.EditValue)
        End If

        If table.Rows.Count > 0 Then
            idPresupuesto = table.Rows(0).Item("IdPresupuestoPk")
        Else
            MsgBox("No se encontró ningún presupuesto con el mes " & lueMes.Text & " y año " & seAnio.Value & " y la combinación sucursal, departamento y centro de costos seleccionada.", MsgBoxStyle.Critical)
            lueSucursales.Focus()
            Return
        End If

        rpt.DataSource = table
        rpt.DataMember = ""

        If lueMes.SelectedIndex = 0 Then
            rpt.XrSubreportDetalles.ReportSource.DataSource = bl.pre_PresupuestoCuentaConSaldos(seAnio.Value, idPresupuesto)
        Else
            rpt.XrSubreportDetalles.ReportSource.DataSource = bl.pre_PresupuestoCuentaConSaldosMes(seAnio.Value, lueMes.EditValue, idPresupuesto)
        End If

        rpt.XrSubreportDetalles.ReportSource.DataMember = ""

        rpt.xrlTituloEmpresa.Text = gsNombre_Empresa
        rpt.xrlNombreReporte.Text = "REPORTE DE PRESUPUESTOS CON DETALLE DE COSTOS REALES."

        If lueMes.EditValue = 0 Then
            rpt.xrlOtrainfo.Text = "CORRESPONDIENTE AL AÑO DE " & Me.seAnio.EditValue
        Else
            rpt.xrlOtrainfo.Text = "CORRESPONDIENTE AL MES DE " & ObtieneMesString(lueMes.EditValue) & " DEL AÑO " & Me.seAnio.EditValue
        End If

        rpt.ShowPreviewDialog()

    End Sub
End Class