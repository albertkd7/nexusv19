<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class con_rptEstadoResultados
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.XrLine12 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlCargo1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrpNumero = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.xrlFirmante2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFirmante5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPage = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCargo6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCargo4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCargo2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine7 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlCargo5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFirmante3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlFirmante6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine11 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine10 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlFirmante4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine9 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlCargo3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine8 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlFirmante1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine5 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlPeriodo = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrpFecha = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLine14 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLineParcial1 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlTitulo = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlEmpresa = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDescMoneda = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.DsContabilidad1 = New Nexus.dsContabilidad()
        Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        CType(Me.DsContabilidad1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'XrLine12
        '
        Me.XrLine12.Dpi = 100.0!
        Me.XrLine12.LocationFloat = New DevExpress.Utils.PointFloat(491.0!, 404.0!)
        Me.XrLine12.Name = "XrLine12"
        Me.XrLine12.SizeF = New System.Drawing.SizeF(222.0!, 3.0!)
        '
        'xrlCargo1
        '
        Me.xrlCargo1.Dpi = 100.0!
        Me.xrlCargo1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo1.LocationFloat = New DevExpress.Utils.PointFloat(103.0!, 169.0!)
        Me.xrlCargo1.Name = "xrlCargo1"
        Me.xrlCargo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo1.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo1.StylePriority.UseFont = False
        Me.xrlCargo1.StylePriority.UseTextAlignment = False
        Me.xrlCargo1.Text = "xrlCargo1"
        Me.xrlCargo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrpNumero
        '
        Me.xrpNumero.Dpi = 100.0!
        Me.xrpNumero.LocationFloat = New DevExpress.Utils.PointFloat(706.0!, 0!)
        Me.xrpNumero.Name = "xrpNumero"
        Me.xrpNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpNumero.SizeF = New System.Drawing.SizeF(66.0!, 22.0!)
        '
        'xrlFirmante2
        '
        Me.xrlFirmante2.Dpi = 100.0!
        Me.xrlFirmante2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante2.LocationFloat = New DevExpress.Utils.PointFloat(491.0!, 153.0!)
        Me.xrlFirmante2.Name = "xrlFirmante2"
        Me.xrlFirmante2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante2.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante2.StylePriority.UseFont = False
        Me.xrlFirmante2.StylePriority.UseTextAlignment = False
        Me.xrlFirmante2.Text = "xrlFirmante2"
        Me.xrlFirmante2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlFirmante5
        '
        Me.xrlFirmante5.Dpi = 100.0!
        Me.xrlFirmante5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante5.LocationFloat = New DevExpress.Utils.PointFloat(103.0!, 411.0!)
        Me.xrlFirmante5.Name = "xrlFirmante5"
        Me.xrlFirmante5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante5.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante5.StylePriority.UseFont = False
        Me.xrlFirmante5.StylePriority.UseTextAlignment = False
        Me.xrlFirmante5.Text = "xrlFirmante5"
        Me.xrlFirmante5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlPage
        '
        Me.xrlPage.Dpi = 100.0!
        Me.xrlPage.LocationFloat = New DevExpress.Utils.PointFloat(647.0!, 0!)
        Me.xrlPage.Name = "xrlPage"
        Me.xrlPage.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPage.SizeF = New System.Drawing.SizeF(56.0!, 22.0!)
        Me.xrlPage.Text = "P�g. No."
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultados.nombre")})
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(6.0!, 0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(497.0!, 16.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.Text = "XrLabel2"
        '
        'xrlCargo6
        '
        Me.xrlCargo6.Dpi = 100.0!
        Me.xrlCargo6.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo6.LocationFloat = New DevExpress.Utils.PointFloat(491.0!, 426.0!)
        Me.xrlCargo6.Name = "xrlCargo6"
        Me.xrlCargo6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo6.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo6.StylePriority.UseFont = False
        Me.xrlCargo6.StylePriority.UseTextAlignment = False
        Me.xrlCargo6.Text = "xrlCargo6"
        Me.xrlCargo6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel11
        '
        Me.XrLabel11.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultados.saldo", "{0:n2}")})
        Me.XrLabel11.Dpi = 100.0!
        Me.XrLabel11.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(506.0!, 0!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(84.0!, 16.0!)
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "XrLabel11"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCargo4
        '
        Me.xrlCargo4.Dpi = 100.0!
        Me.xrlCargo4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo4.LocationFloat = New DevExpress.Utils.PointFloat(488.0!, 300.0!)
        Me.xrlCargo4.Name = "xrlCargo4"
        Me.xrlCargo4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo4.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo4.StylePriority.UseFont = False
        Me.xrlCargo4.StylePriority.UseTextAlignment = False
        Me.xrlCargo4.Text = "xrlCargo4"
        Me.xrlCargo4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlCargo2
        '
        Me.xrlCargo2.Dpi = 100.0!
        Me.xrlCargo2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo2.LocationFloat = New DevExpress.Utils.PointFloat(491.0!, 169.0!)
        Me.xrlCargo2.Name = "xrlCargo2"
        Me.xrlCargo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo2.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlCargo2.StylePriority.UseFont = False
        Me.xrlCargo2.StylePriority.UseTextAlignment = False
        Me.xrlCargo2.Text = "xrlCargo2"
        Me.xrlCargo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine7
        '
        Me.XrLine7.Dpi = 100.0!
        Me.XrLine7.LocationFloat = New DevExpress.Utils.PointFloat(103.0!, 147.0!)
        Me.XrLine7.Name = "XrLine7"
        Me.XrLine7.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlCargo5
        '
        Me.xrlCargo5.Dpi = 100.0!
        Me.xrlCargo5.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo5.LocationFloat = New DevExpress.Utils.PointFloat(103.0!, 426.0!)
        Me.xrlCargo5.Name = "xrlCargo5"
        Me.xrlCargo5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo5.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo5.StylePriority.UseFont = False
        Me.xrlCargo5.StylePriority.UseTextAlignment = False
        Me.xrlCargo5.Text = "xrlCargo5"
        Me.xrlCargo5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel3
        '
        Me.XrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultados.saldo", "{0:n2}")})
        Me.XrLabel3.Dpi = 100.0!
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(594.0!, 0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(91.0!, 16.0!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "XrLabel3"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlFirmante3
        '
        Me.xrlFirmante3.Dpi = 100.0!
        Me.xrlFirmante3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante3.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 285.0!)
        Me.xrlFirmante3.Name = "xrlFirmante3"
        Me.xrlFirmante3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante3.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante3.StylePriority.UseFont = False
        Me.xrlFirmante3.StylePriority.UseTextAlignment = False
        Me.xrlFirmante3.Text = "xrlFirmante3"
        Me.xrlFirmante3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine3, Me.xrlCargo6, Me.XrLine12, Me.xrlFirmante6, Me.XrLine11, Me.xrlFirmante5, Me.xrlCargo5, Me.xrlCargo4, Me.XrLine10, Me.xrlFirmante4, Me.XrLine9, Me.xrlFirmante3, Me.xrlCargo3, Me.xrlCargo2, Me.XrLine8, Me.xrlFirmante2, Me.xrlCargo1, Me.xrlFirmante1, Me.XrLine7, Me.XrLine5, Me.XrLine2, Me.XrLabel15, Me.xrlTotal})
        Me.GroupFooter1.Dpi = 100.0!
        Me.GroupFooter1.HeightF = 457.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        Me.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'XrLine3
        '
        Me.XrLine3.Dpi = 100.0!
        Me.XrLine3.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 0!)
        Me.XrLine3.Name = "XrLine3"
        Me.XrLine3.SizeF = New System.Drawing.SizeF(778.0!, 6.0!)
        '
        'xrlFirmante6
        '
        Me.xrlFirmante6.Dpi = 100.0!
        Me.xrlFirmante6.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante6.LocationFloat = New DevExpress.Utils.PointFloat(491.0!, 411.0!)
        Me.xrlFirmante6.Name = "xrlFirmante6"
        Me.xrlFirmante6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante6.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante6.StylePriority.UseFont = False
        Me.xrlFirmante6.StylePriority.UseTextAlignment = False
        Me.xrlFirmante6.Text = "xrlFirmante6"
        Me.xrlFirmante6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine11
        '
        Me.XrLine11.Dpi = 100.0!
        Me.XrLine11.LocationFloat = New DevExpress.Utils.PointFloat(103.0!, 404.0!)
        Me.XrLine11.Name = "XrLine11"
        Me.XrLine11.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'XrLine10
        '
        Me.XrLine10.Dpi = 100.0!
        Me.XrLine10.LocationFloat = New DevExpress.Utils.PointFloat(488.0!, 278.0!)
        Me.XrLine10.Name = "XrLine10"
        Me.XrLine10.SizeF = New System.Drawing.SizeF(222.0!, 3.0!)
        '
        'xrlFirmante4
        '
        Me.xrlFirmante4.Dpi = 100.0!
        Me.xrlFirmante4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante4.LocationFloat = New DevExpress.Utils.PointFloat(488.0!, 285.0!)
        Me.xrlFirmante4.Name = "xrlFirmante4"
        Me.xrlFirmante4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante4.SizeF = New System.Drawing.SizeF(222.0!, 12.0!)
        Me.xrlFirmante4.StylePriority.UseFont = False
        Me.xrlFirmante4.StylePriority.UseTextAlignment = False
        Me.xrlFirmante4.Text = "xrlFirmante4"
        Me.xrlFirmante4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine9
        '
        Me.XrLine9.Dpi = 100.0!
        Me.XrLine9.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 278.0!)
        Me.XrLine9.Name = "XrLine9"
        Me.XrLine9.SizeF = New System.Drawing.SizeF(216.0!, 3.0!)
        '
        'xrlCargo3
        '
        Me.xrlCargo3.Dpi = 100.0!
        Me.xrlCargo3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCargo3.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 300.0!)
        Me.xrlCargo3.Name = "xrlCargo3"
        Me.xrlCargo3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCargo3.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlCargo3.StylePriority.UseFont = False
        Me.xrlCargo3.StylePriority.UseTextAlignment = False
        Me.xrlCargo3.Text = "xrlCargo3"
        Me.xrlCargo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine8
        '
        Me.XrLine8.Dpi = 100.0!
        Me.XrLine8.LocationFloat = New DevExpress.Utils.PointFloat(491.0!, 147.0!)
        Me.XrLine8.Name = "XrLine8"
        Me.XrLine8.SizeF = New System.Drawing.SizeF(222.0!, 3.0!)
        '
        'xrlFirmante1
        '
        Me.xrlFirmante1.Dpi = 100.0!
        Me.xrlFirmante1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlFirmante1.LocationFloat = New DevExpress.Utils.PointFloat(103.0!, 153.0!)
        Me.xrlFirmante1.Name = "xrlFirmante1"
        Me.xrlFirmante1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFirmante1.SizeF = New System.Drawing.SizeF(216.0!, 12.0!)
        Me.xrlFirmante1.StylePriority.UseFont = False
        Me.xrlFirmante1.StylePriority.UseTextAlignment = False
        Me.xrlFirmante1.Text = "xrlFirmante1"
        Me.xrlFirmante1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine5
        '
        Me.XrLine5.Dpi = 100.0!
        Me.XrLine5.LocationFloat = New DevExpress.Utils.PointFloat(678.0!, 38.0!)
        Me.XrLine5.Name = "XrLine5"
        Me.XrLine5.SizeF = New System.Drawing.SizeF(100.0!, 6.0!)
        '
        'XrLine2
        '
        Me.XrLine2.Dpi = 100.0!
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(678.0!, 12.0!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(100.0!, 6.0!)
        '
        'XrLabel15
        '
        Me.XrLabel15.Dpi = 100.0!
        Me.XrLabel15.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 22.0!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(191.0!, 16.0!)
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.Text = "UTILIDAD EJERCICIO"
        '
        'xrlTotal
        '
        Me.xrlTotal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultados.saldo")})
        Me.xrlTotal.Dpi = 100.0!
        Me.xrlTotal.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xrlTotal.LocationFloat = New DevExpress.Utils.PointFloat(684.0!, 22.0!)
        Me.xrlTotal.Name = "xrlTotal"
        Me.xrlTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal.SizeF = New System.Drawing.SizeF(94.0!, 16.0!)
        Me.xrlTotal.StylePriority.UseFont = False
        Me.xrlTotal.StylePriority.UseTextAlignment = False
        XrSummary1.FormatString = "{0:n2}"
        XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom
        XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
        Me.xrlTotal.Summary = XrSummary1
        Me.xrlTotal.Text = "xrlTotal"
        Me.xrlTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel6
        '
        Me.XrLabel6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "EstadoResultados.saldo", "{0:n2}")})
        Me.XrLabel6.Dpi = 100.0!
        Me.XrLabel6.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(688.0!, 0!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(91.0!, 16.0!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        XrSummary2.FormatString = "{0:n2}"
        Me.XrLabel6.Summary = XrSummary2
        Me.XrLabel6.Text = "XrLabel6"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLine1
        '
        Me.XrLine1.Dpi = 100.0!
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(6.0!, 47.0!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(784.0!, 6.0!)
        '
        'xrlPeriodo
        '
        Me.xrlPeriodo.Dpi = 100.0!
        Me.xrlPeriodo.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.xrlPeriodo.LocationFloat = New DevExpress.Utils.PointFloat(6.0!, 75.0!)
        Me.xrlPeriodo.Name = "xrlPeriodo"
        Me.xrlPeriodo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPeriodo.SizeF = New System.Drawing.SizeF(773.0!, 22.0!)
        Me.xrlPeriodo.StylePriority.UseFont = False
        Me.xrlPeriodo.StylePriority.UseTextAlignment = False
        Me.xrlPeriodo.Text = "xrlPeriodo"
        Me.xrlPeriodo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrpFecha
        '
        Me.xrpFecha.Dpi = 100.0!
        Me.xrpFecha.Format = "{0:dd/MM/yyyy hh:mm tt}"
        Me.xrpFecha.LocationFloat = New DevExpress.Utils.PointFloat(647.0!, 22.0!)
        Me.xrpFecha.Name = "xrpFecha"
        Me.xrpFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpFecha.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.xrpFecha.SizeF = New System.Drawing.SizeF(125.0!, 19.0!)
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine14, Me.XrLabel11, Me.XrLineParcial1, Me.XrLabel6, Me.XrLabel3, Me.XrLabel2})
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 19.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLine14
        '
        Me.XrLine14.Dpi = 100.0!
        Me.XrLine14.LocationFloat = New DevExpress.Utils.PointFloat(506.0!, 16.0!)
        Me.XrLine14.Name = "XrLine14"
        Me.XrLine14.SizeF = New System.Drawing.SizeF(84.0!, 3.0!)
        '
        'XrLineParcial1
        '
        Me.XrLineParcial1.Dpi = 100.0!
        Me.XrLineParcial1.LocationFloat = New DevExpress.Utils.PointFloat(594.0!, 16.0!)
        Me.XrLineParcial1.Name = "XrLineParcial1"
        Me.XrLineParcial1.SizeF = New System.Drawing.SizeF(91.0!, 3.0!)
        '
        'xrlTitulo
        '
        Me.xrlTitulo.Dpi = 100.0!
        Me.xrlTitulo.Font = New System.Drawing.Font("Arial", 11.0!)
        Me.xrlTitulo.LocationFloat = New DevExpress.Utils.PointFloat(6.0!, 50.0!)
        Me.xrlTitulo.Name = "xrlTitulo"
        Me.xrlTitulo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTitulo.SizeF = New System.Drawing.SizeF(773.0!, 22.0!)
        Me.xrlTitulo.StylePriority.UseFont = False
        Me.xrlTitulo.StylePriority.UseTextAlignment = False
        Me.xrlTitulo.Text = "xrlTitulo"
        Me.xrlTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlEmpresa
        '
        Me.xrlEmpresa.Dpi = 100.0!
        Me.xrlEmpresa.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.xrlEmpresa.LocationFloat = New DevExpress.Utils.PointFloat(6.0!, 28.0!)
        Me.xrlEmpresa.Name = "xrlEmpresa"
        Me.xrlEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlEmpresa.SizeF = New System.Drawing.SizeF(773.0!, 22.0!)
        Me.xrlEmpresa.StylePriority.UseFont = False
        Me.xrlEmpresa.StylePriority.UseTextAlignment = False
        Me.xrlEmpresa.Text = "xrlEmpresa"
        Me.xrlEmpresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlDescMoneda
        '
        Me.xrlDescMoneda.Dpi = 100.0!
        Me.xrlDescMoneda.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlDescMoneda.LocationFloat = New DevExpress.Utils.PointFloat(6.0!, 97.0!)
        Me.xrlDescMoneda.Name = "xrlDescMoneda"
        Me.xrlDescMoneda.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDescMoneda.SizeF = New System.Drawing.SizeF(773.0!, 22.0!)
        Me.xrlDescMoneda.StylePriority.UseFont = False
        Me.xrlDescMoneda.StylePriority.UseTextAlignment = False
        Me.xrlDescMoneda.Text = "xrlDescMoneda"
        Me.xrlDescMoneda.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrpFecha, Me.xrlPage, Me.xrpNumero, Me.XrLine1})
        Me.PageHeader.Dpi = 100.0!
        Me.PageHeader.HeightF = 54.0!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlDescMoneda, Me.xrlEmpresa, Me.xrlTitulo, Me.xrlPeriodo})
        Me.ReportHeader.Dpi = 100.0!
        Me.ReportHeader.HeightF = 129.0!
        Me.ReportHeader.Name = "ReportHeader"
        Me.ReportHeader.StyleName = "XrControlStyle1"
        '
        'DsContabilidad1
        '
        Me.DsContabilidad1.DataSetName = "dsContabilidad"
        Me.DsContabilidad1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'XrControlStyle1
        '
        Me.XrControlStyle1.Name = "XrControlStyle1"
        Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.Dpi = 100.0!
        Me.TopMarginBand1.HeightF = 35.0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.Dpi = 100.0!
        Me.BottomMarginBand1.HeightF = 35.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'con_rptEstadoResultados
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.GroupFooter1, Me.ReportHeader, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.DataMember = "EstadoResultados"
        Me.DataSource = Me.DsContabilidad1
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 35, 35)
        Me.SnapGridSize = 3.0!
        Me.SnappingMode = CType((DevExpress.XtraReports.UI.SnappingMode.SnapLines Or DevExpress.XtraReports.UI.SnappingMode.SnapToGrid), DevExpress.XtraReports.UI.SnappingMode)
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
        Me.Version = "16.2"
        CType(Me.DsContabilidad1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrLine12 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlCargo1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrpNumero As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents xrlFirmante2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFirmante5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPage As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCargo2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine7 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlCargo5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFirmante3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents xrlFirmante6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine11 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine10 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine9 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlCargo3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine8 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFirmante1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlPeriodo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrpFecha As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents XrLine14 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLineParcial1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlTitulo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlEmpresa As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDescMoneda As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrLine3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents DsContabilidad1 As Nexus.dsContabilidad
    Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
End Class
