﻿Imports NexusBLL

Public Class con_frmLibroMayor
    Dim bl As New ContabilidadBLL(g_ConnectionString)


    Private Sub btnOk_Click() Handles Me.Reporte
        Dim dt As DataTable = bl.rptLibroMayor(deIni.EditValue, deFin.EditValue, leSucursal.EditValue, rgTipo.EditValue)
        If rgTipo.EditValue = 0 Then 'AGRUPADO POR FECHA

            Dim rpt As New con_rptLibroMayor() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.EditValue
            rpt.xrlPeriodo.Text = (FechaToString(deIni.EditValue, deFin.EditValue)).ToUpper
            rpt.xrlConcepto.Text = teConcepto.EditValue
            rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
            rpt.xrlPage.Visible = chkNumerar.Checked
            rpt.XrPageInfo1.Visible = chkNumerar.Checked
            rpt.XrPageInfo1.StartPageNumber = seFolio.EditValue
            rpt.XrPageInfo2.Visible = ceInfoDateTime.Checked

            rpt.ShowPreviewDialog()
            Return
        End If


        Dim rpt1 As New con_rptLibroMayorConsolidado() With {.DataSource = dt, .DataMember = ""}
        rpt1.xrlEmpresa.Text = gsNombre_Empresa
        rpt1.xrlTitulo.Text = teTitulo.EditValue
        rpt1.xrlPeriodo.Text = (FechaToString(deIni.EditValue, deFin.EditValue)).ToUpper
        rpt1.xrlConcepto.Text = teConcepto.EditValue
        rpt1.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
        rpt1.xrlPage.Visible = chkNumerar.Checked
        rpt1.XrPageInfo1.Visible = chkNumerar.Checked
        rpt1.XrPageInfo1.StartPageNumber = seFolio.EditValue
        rpt1.XrPageInfo2.Visible = ceInfoDateTime.Checked

        rpt1.ShowPreviewDialog()

    End Sub

    Private Sub frmLibroMayor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deIni.DateTime = Today
        deFin.DateTime = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

End Class