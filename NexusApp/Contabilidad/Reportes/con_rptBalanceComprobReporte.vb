Public Class con_rptBalanceComprobReporte
    Private dTotal As Decimal = 0

    Private Sub xrlTotal_SummaryGetResult(ByVal sender As System.Object, ByVal e As DevExpress.XtraReports.UI.SummaryGetResultEventArgs) Handles xrlTotal.SummaryGetResult
        e.Result = dTotal
        e.Handled = True
    End Sub

    Private Sub xrlTotal_SummaryReset(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles xrlTotal.SummaryReset
        dTotal = 0
    End Sub

    Private Sub xrlTotal_SummaryRowChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles xrlTotal.SummaryRowChanged
        If GetCurrentColumnValue("Nivel") = 0 Then
            dTotal += GetCurrentColumnValue("SaldoFinal")
        End If
    End Sub

    'Private Sub XrLabel4_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel4.BeforePrint
    '    If Me.GetCurrentRow() IsNot Nothing Then
    '        If GetCurrentRow().Row("TotalDebe") = 0 Then
    '            e.Cancel = True
    '        End If
    '    End If
    'End Sub
    'Private Sub XrLabel5_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel5.BeforePrint
    '    If Me.GetCurrentRow() IsNot Nothing Then
    '        If GetCurrentRow().Row("TotalHaber") = 0 Then
    '            e.Cancel = True
    '        End If
    '    End If
    'End Sub
End Class