Public Class con_rptLibroDiario

    Private Sub xrlDebe_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlDebe.BeforePrint
        If GetCurrentColumnValue("Debe") = 0 Then
            e.Cancel = True
        End If
    End Sub
    Private Sub xrlHaber_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlHaber.BeforePrint
        If GetCurrentColumnValue("Haber") = 0 Then
            e.Cancel = True
        End If
    End Sub
End Class