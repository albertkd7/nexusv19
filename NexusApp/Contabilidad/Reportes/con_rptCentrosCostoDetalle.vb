Public Class con_rptCentrosCostoDetalle
    Dim CurrentSummary As Double = 0.0
    Private Sub XrLabel25_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel25.BeforePrint
        CurrentSummary = CurrentSummary + Convert.ToDouble(GetCurrentColumnValue("Debe")) - Convert.ToDouble(GetCurrentColumnValue("Haber"))
        If GetCurrentColumnValue("Naturaleza") = 1 Or GetCurrentColumnValue("Naturaleza") = 4 Then
            XrLabel25.Text = String.Format("{0:n2}", CurrentSummary)
        Else
            XrLabel25.Text = String.Format("{0:n2}", CurrentSummary * -1)
        End If

    End Sub

    Private Sub XrLabel26_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel26.BeforePrint
        CurrentSummary = Convert.ToDouble(GetCurrentColumnValue("SaldoInicial"))
    End Sub
End Class