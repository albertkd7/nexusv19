﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmFlujoEfectivo
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.EsFlujoCheckEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Actividad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ActividadLookUpEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.RepositoryItemLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EsFlujoCheckEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActividadLookUpEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gc
        '
        Me.gc.Location = New System.Drawing.Point(12, 52)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.EsFlujoCheckEdit2, Me.RepositoryItemComboBox1, Me.RepositoryItemLookUpEdit1, Me.ActividadLookUpEdit2})
        Me.gc.Size = New System.Drawing.Size(757, 389)
        Me.gc.TabIndex = 35
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn6, Me.Actividad})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Flujo de Efectivo"
        Me.GridColumn2.ColumnEdit = Me.EsFlujoCheckEdit2
        Me.GridColumn2.FieldName = "EsFlujo"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        Me.GridColumn2.Width = 90
        '
        'EsFlujoCheckEdit2
        '
        Me.EsFlujoCheckEdit2.AutoHeight = False
        Me.EsFlujoCheckEdit2.Name = "EsFlujoCheckEdit2"
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Cuenta"
        Me.GridColumn3.FieldName = "IdCuenta"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.AllowEdit = False
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        Me.GridColumn3.Width = 110
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Cuenta Mayor"
        Me.GridColumn4.FieldName = "Nombre"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.OptionsColumn.AllowEdit = False
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 2
        Me.GridColumn4.Width = 192
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Concepto Personalizado"
        Me.GridColumn6.FieldName = "Concepto"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 3
        Me.GridColumn6.Width = 192
        '
        'Actividad
        '
        Me.Actividad.Caption = "Actividad"
        Me.Actividad.ColumnEdit = Me.ActividadLookUpEdit2
        Me.Actividad.FieldName = "IdRubro"
        Me.Actividad.Name = "Actividad"
        Me.Actividad.Visible = True
        Me.Actividad.VisibleIndex = 4
        Me.Actividad.Width = 151
        '
        'ActividadLookUpEdit2
        '
        Me.ActividadLookUpEdit2.AutoHeight = False
        Me.ActividadLookUpEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ActividadLookUpEdit2.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Nombre", "Nombre")})
        Me.ActividadLookUpEdit2.Name = "ActividadLookUpEdit2"
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"Operativas", "Inversión", "Financieras", "No Efectivo"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'RepositoryItemLookUpEdit1
        '
        Me.RepositoryItemLookUpEdit1.AutoHeight = False
        Me.RepositoryItemLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit1.Name = "RepositoryItemLookUpEdit1"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(13, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(506, 38)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "NOTA: Tiene que seleccionar todas las cuentas de balance excepto, las que represe" & _
    "ntan Efectivo y Bancos.  Las cuentas de resultado tienen que quedar excluidas."
        '
        'con_frmFlujoEfectivo
        '
        Me.ClientSize = New System.Drawing.Size(862, 472)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.gc)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmFlujoEfectivo"
        Me.OptionId = "001004"
        Me.Text = "Editor Flujo Efectivo"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.gc, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EsFlujoCheckEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActividadLookUpEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents anombre_de As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents fecha_cheque As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents procesada_banco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents EsFlujoCheckEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents Actividad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ActividadLookUpEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label

End Class
