﻿Imports NexusBLL

Public Class con_frmPartidasReporte
    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private Sub cmd1_Click() Handles Me.Reporte
        Dim dt As New DataTable

        dt = bl.rptPartidaReporteByFecha(deFec1.DateTime, deFec2.DateTime, leTiposPartida.EditValue, teDesde.EditValue, teHasta.EditValue, leSucursal.EditValue)

        Dim rpt As New con_rptPartida() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlDescMoneda.Text = gsDesc_Moneda
        If ceGenerarSalto.Checked Then
            rpt.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        End If
        rpt.xrlPagina.Visible = cePagina.EditValue
        rpt.XrPageInfo1.Visible = cePagina.EditValue
        rpt.XrPageInfo2.Visible = ceFechaHora.EditValue
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub frmPartidasReporte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFec1.DateTime = Today
        deFec2.DateTime = Today
        teDesde.EditValue = ""
        teHasta.EditValue = ""
        objCombos.conTiposPartida(leTiposPartida, "-- TODOS LOS TIPOS DE PARTIDA --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

End Class