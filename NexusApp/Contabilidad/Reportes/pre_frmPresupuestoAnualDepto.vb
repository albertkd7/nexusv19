﻿Imports NexusBLL

Public Class pre_frmPresupuestoAnualDepto

    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private Sub pre_frmPresupuestoAnulDepto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(lueSucursales, objMenu.User, "")
        seAnio.Value = DateTime.Now.Year
    End Sub

    Private Sub lueSucursales_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lueSucursales.EditValueChanged
        objCombos.pre_Departamentos(lueDepartamentos, lueSucursales.EditValue)
    End Sub

    Private Sub pre_frmPresupuestoAnulDepto_Reporte() Handles MyBase.Reporte

        If lueSucursales.EditValue = 0 Then
            MsgBox("Debe seleccionar la sucursal antes de presentar el reporte.", MsgBoxStyle.Critical)
            lueSucursales.Focus()
            Return
        End If

        If lueDepartamentos.EditValue = 0 Then
            MsgBox("Debe seleccionar el departamento antes de presentar el reporte.", MsgBoxStyle.Critical)
            lueDepartamentos.Focus()
            Return
        End If

        Dim rpt As New pre_rptPresupuestosAnualDepto

        rpt.DataSource = bl.pre_PresupuestoAnualDepto(seAnio.Value, lueDepartamentos.EditValue)

        rpt.DataMember = ""

        rpt.xrlTituloEmpresa.Text = gsNombre_Empresa
        rpt.xrlNombreReporte.Text = "REPORTE DE PRESUPUESTOS ANUAL PARA EL DEPARTAMENTO DE " & lueDepartamentos.Text
        rpt.xrlOtrainfo.Text = "CORRESPONDIENTE AL AÑO " & seAnio.EditValue
                
        rpt.ShowPreviewDialog()

    End Sub
End Class
