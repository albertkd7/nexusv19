﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class con_frmRatiosFinancieros
	Dim bl As New ContabilidadBLL(g_ConnectionString)
	Private Sub fac_frmConsultaFacturacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		seAnio.EditValue = Year(Now)
		meMes.EditValue = Month(Now)
		teSituacionAct.EditValue = 0.0
	End Sub

#Region "VerFormula"

	Private Sub sbVerFormula1_Click(sender As Object, e As EventArgs) Handles sbVerFormula1.Click
		CargaFormula(1, "LIQUIDEZ GENERAL")
	End Sub
	Private Sub sbVerFormula2_Click(sender As Object, e As EventArgs) Handles sbVerFormula2.Click
		CargaFormula(2, "PRUEBA ACIDA")
	End Sub

	Private Sub sbVerFormula3_Click(sender As Object, e As EventArgs) Handles sbVerFormula3.Click
		CargaFormula(3, "PRUEBA DEFENSIVA")
	End Sub

	Private Sub sbVerFormula4_Click(sender As Object, e As EventArgs) Handles sbVerFormula4.Click
		CargaFormula(4, "PERIODO PROMEDIO DE COBRANZA")
	End Sub

	Private Sub sbVerFormula5_Click(sender As Object, e As EventArgs) Handles sbVerFormula5.Click
		CargaFormula(5, "ROTACION DE LAS CUENTAS POR COBRAR")
	End Sub

	Private Sub sbVerFormula6_Click(sender As Object, e As EventArgs) Handles sbVerFormula6.Click
		CargaFormula(6, "ROTACION DE CARTERA")
	End Sub

	Private Sub sbVerFormula7_Click(sender As Object, e As EventArgs) Handles sbVerFormula7.Click
		CargaFormula(7, "PERIODO DE PAGO A PROVEEDORES")
	End Sub

	Private Sub sbVerFormula8_Click(sender As Object, e As EventArgs) Handles sbVerFormula8.Click
		CargaFormula(8, "ROTACION DE CAJAS Y BANCOS")
	End Sub

	Private Sub sbVerFormula9_Click(sender As Object, e As EventArgs) Handles sbVerFormula9.Click
		CargaFormula(9, "ROTACION DE ACTIVOS TOTALES")
	End Sub

	Private Sub sbVerFormula10_Click(sender As Object, e As EventArgs) Handles sbVerFormula10.Click
		CargaFormula(10, "ESTRUCTURA DE CAPITAL")
	End Sub
	Private Sub sbVerFormula11_Click(sender As Object, e As EventArgs) Handles sbVerFormula11.Click
		CargaFormula(11, "RAZON DE ENDEUDAMIENTO")
	End Sub

	Private Sub sbVerFormula12_Click(sender As Object, e As EventArgs) Handles sbVerFormula12.Click
		CargaFormula(12, "COBERTURA DE GASTOS FINANCIEROS")
	End Sub

	Private Sub sbVerFormula13_Click(sender As Object, e As EventArgs) Handles sbVerFormula13.Click
		CargaFormula(13, "COBERTURA DE GASTOS FIJOS")
	End Sub
	Private Sub sbVerFormula14_Click(sender As Object, e As EventArgs) Handles sbVerFormula14.Click
		CargaFormula(14, "RENDIMIENTO SOBRE EL PATRIMONIO")
	End Sub

	Private Sub sbVerFormula15_Click(sender As Object, e As EventArgs) Handles sbVerFormula15.Click
		CargaFormula(15, "RENDIMIENTO SOBRE LA INVERSION")
	End Sub

	Private Sub sbVerFormula16_Click(sender As Object, e As EventArgs) Handles sbVerFormula16.Click
		CargaFormula(16, "UTILIDAD SOBRE LOS ACTIVOS")
	End Sub

	Private Sub sbVerFormula17_Click(sender As Object, e As EventArgs) Handles sbVerFormula17.Click
		CargaFormula(17, "UTILIDAD SOBRE LAS VENTAS")
	End Sub

	Private Sub sbVerFormula18_Click(sender As Object, e As EventArgs) Handles sbVerFormula18.Click
		CargaFormula(18, "UTILIDAD POR ACCION")
	End Sub

	Private Sub sbVerFormula19_Click(sender As Object, e As EventArgs) Handles sbVerFormula19.Click
		CargaFormula(19, "MARGEN DE UTILIDAD BRUTA")
	End Sub

	Private Sub sbVerFormula20_Click(sender As Object, e As EventArgs) Handles sbVerFormula20.Click
		CargaFormula(20, "MARGEN NETO DE UTILIDAD")
	End Sub

	Private Sub sbVerFormula21_Click(sender As Object, e As EventArgs) Handles sbVerFormula21.Click
		CargaFormula(21, "DUPONT")
	End Sub
#End Region

#Region "SituacionActual"

	Private Sub sbResultado1_Click(sender As Object, e As EventArgs) Handles sbResultado1.Click
		SituacionActual(1)
	End Sub

	Private Sub sbResultado2_Click(sender As Object, e As EventArgs) Handles sbResultado2.Click
SituacionActual(2)
	End Sub

	Private Sub sbResultado3_Click(sender As Object, e As EventArgs) Handles sbResultado3.Click
		SituacionActual(3)
	End Sub

	Private Sub sbResultado4_Click(sender As Object, e As EventArgs) Handles sbResultado4.Click
		SituacionActual(4)
	End Sub

	Private Sub sbResultado5_Click(sender As Object, e As EventArgs) Handles sbResultado5.Click
		SituacionActual(5)
	End Sub

	Private Sub sbResultado6_Click(sender As Object, e As EventArgs) Handles sbResultado6.Click
		SituacionActual(6)
	End Sub

	Private Sub sbResultado7_Click(sender As Object, e As EventArgs) Handles sbResultado7.Click
		SituacionActual(7)
	End Sub

	Private Sub sbResultado8_Click(sender As Object, e As EventArgs) Handles sbResultado8.Click
		SituacionActual(8)
	End Sub

	Private Sub sbResultado9_Click(sender As Object, e As EventArgs) Handles sbResultado9.Click
		SituacionActual(9)
	End Sub

	Private Sub sbResultado10_Click(sender As Object, e As EventArgs) Handles sbResultado10.Click
		SituacionActual(10)
	End Sub
	Private Sub sbResultado11_Click(sender As Object, e As EventArgs) Handles sbResultado11.Click
		SituacionActual(11)
	End Sub

	Private Sub sbResultado12_Click(sender As Object, e As EventArgs) Handles sbResultado12.Click
		SituacionActual(12)
	End Sub

	Private Sub sbResultado13_Click(sender As Object, e As EventArgs) Handles sbResultado13.Click
		SituacionActual(13)
	End Sub
	Private Sub sbResultado14_Click(sender As Object, e As EventArgs) Handles sbResultado14.Click
		SituacionActual(14)
	End Sub

	Private Sub sbResultado15_Click(sender As Object, e As EventArgs) Handles sbResultado15.Click
		SituacionActual(15)
	End Sub

	Private Sub sbResultado16_Click(sender As Object, e As EventArgs) Handles sbResultado16.Click
		SituacionActual(16)
	End Sub

	Private Sub sbResultado17_Click(sender As Object, e As EventArgs) Handles sbResultado17.Click
		SituacionActual(17)
	End Sub

	Private Sub sbResultado18_Click(sender As Object, e As EventArgs) Handles sbResultado18.Click
		SituacionActual(18)
	End Sub

	Private Sub sbResultado19_Click(sender As Object, e As EventArgs) Handles sbResultado19.Click
		SituacionActual(19)
	End Sub

	Private Sub sbResultado20_Click(sender As Object, e As EventArgs) Handles sbResultado20.Click
		SituacionActual(20)
	End Sub

	Private Sub sbResultado21_Click(sender As Object, e As EventArgs) Handles sbResultado21.Click
		SituacionActual(21)
	End Sub
#End Region

	Private Sub CargaFormula(ByVal IdFormula As Integer, ByVal Nombre As String)
		con_frmFormulasFinancieras.IdFormula = IdFormula
		con_frmFormulasFinancieras.cIndice = Nombre
		con_frmFormulasFinancieras.ShowDialog()
	End Sub

	Private Sub SituacionActual(ByVal IdFormula As Integer)
		Dim Valor As Decimal
		Try
			Valor = bl.rptIndicador(IdFormula, meMes.EditValue, seAnio.EditValue, 1)
			teSituacionAct.EditValue = Format(Decimal.Round(Valor * 1, 2, MidpointRounding.AwayFromZero), "###,###,##0.00")
		Catch ex As Exception
			teSituacionAct.EditValue = 0.0
			MsgBox("Existe un problema al consultar la situacion actual" + Chr(13) + "Verifique los siguientes datos: " + Chr(13) + "FORMULA DEFINIDA" + Chr(13) + "FORMULA SIN ERRORES", MsgBoxStyle.Critical, "Error de Usuario")
		End Try

	End Sub


End Class
