Public Class con_rptPartida
    Private Sub xrlHaber_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlHaber.BeforePrint
        If GetCurrentColumnValue("Haber") = 0 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub xrlDebe_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlDebe.BeforePrint
        If GetCurrentColumnValue("Debe") = 0 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub xrlParcial_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlParcial.BeforePrint
        If GetCurrentColumnValue("Parcial") = 0 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub xrLineaParcial_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrLineaParcial.BeforePrint
        If GetNextColumnValue("Nivel") <> 2 Then
            e.Cancel = True
        End If
    End Sub
End Class