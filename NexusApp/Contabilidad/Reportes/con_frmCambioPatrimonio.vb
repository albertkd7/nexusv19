﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class con_frmCambioPatrimonio
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim Mes As Integer
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub con_frmCambioPatrimonio_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Mes = 12
        seEjercicio.EditValue = Today.Year
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "--TODAS LAS SUCURSALES")
    End Sub

    Private Sub con_frmCambioPatrimonio_Reporte() Handles Me.Reporte
        Dim dt As New DataTable

        dt = objTablas.con_CuentasPatrimonioSelectAll

        If dt.Rows.Count <= 0 Then
            MsgBox("Aun no se han clasificado las cuentas para el cambio en el patrimonio", MsgBoxStyle.OkOnly, "Cuentas")
            Exit Sub
        End If

        Dim dbFirmante As DataTable = blAdmon.ObtieneParametros()
        Dim rpt As New con_rptCambioPatrimonio() With {.DataSource = bl.rptCambioPatrimonio(seEjercicio.EditValue, leSucursal.EditValue), .DataMember = ""}

        With dbFirmante.Rows(0)

            rpt.xrlPeriodo.Text = seEjercicio.EditValue

            rpt.xrlFirmante1.Text = .Item("NombreFirmante1")
            rpt.xrlCargo1.Text = .Item("CargoFirmante1")
            rpt.xrLineaFirma1.Visible = .Item("CargoFirmante1") <> ""
            rpt.xrlFirmante2.Text = .Item("NombreFirmante2")
            rpt.xrlCargo2.Text = .Item("CargoFirmante2")
            rpt.xrLineaFirma2.Visible = .Item("CargoFirmante2") <> ""
            rpt.xrlFirmante3.Text = .Item("NombreFirmante3")
            rpt.xrlCargo3.Text = .Item("CargoFirmante3")
            rpt.xrLineaFirma3.Visible = .Item("CargoFirmante3") <> ""
            rpt.xrlFirmante4.Text = .Item("NombreFirmante4")
            rpt.xrlCargo4.Text = .Item("CargoFirmante4")
            rpt.xrLineaFirma4.Visible = .Item("CargoFirmante4") <> ""
            rpt.xrlFirmante5.Text = .Item("NombreFirmante5")
            rpt.xrlCargo5.Text = .Item("CargoFirmante5")
            rpt.xrLineaFirma5.Visible = .Item("CargoFirmante5") <> ""
            rpt.xrlFirmante6.Text = .Item("NombreFirmante6")
            rpt.xrlCargo6.Text = .Item("CargoFirmante6")
            rpt.xrLineaFirma6.Visible = .Item("CargoFirmante6") <> ""

        End With


        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue

        rpt.xrlDescMoneda.Text = gsDesc_Moneda
        rpt.ShowPreviewDialog()
    End Sub
End Class
