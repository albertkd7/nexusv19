﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class con_frmRazonesFinancieras
	Inherits Nexus.gen_frmBaseRpt

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing AndAlso components IsNot Nothing Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Me.teTitulo = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
		Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
		Me.seEjercicio = New DevExpress.XtraEditors.SpinEdit()
		Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit()
		Me.ceDateInfo = New DevExpress.XtraEditors.CheckEdit()
		Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
		Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
		CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupControl1.SuspendLayout()
		CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ceDateInfo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'GroupControl1
		'
		Me.GroupControl1.Controls.Add(Me.leSucursal)
		Me.GroupControl1.Controls.Add(Me.LabelControl7)
		Me.GroupControl1.Controls.Add(Me.ceDateInfo)
		Me.GroupControl1.Controls.Add(Me.teTitulo)
		Me.GroupControl1.Controls.Add(Me.LabelControl3)
		Me.GroupControl1.Controls.Add(Me.LabelControl1)
		Me.GroupControl1.Controls.Add(Me.seEjercicio)
		Me.GroupControl1.Controls.Add(Me.meMes)
		Me.GroupControl1.Size = New System.Drawing.Size(621, 289)
		'
		'teTitulo
		'
		Me.teTitulo.EditValue = "RAZONES FINANCIERAS"
		Me.teTitulo.Location = New System.Drawing.Point(198, 99)
		Me.teTitulo.Name = "teTitulo"
		Me.teTitulo.Size = New System.Drawing.Size(341, 20)
		Me.teTitulo.TabIndex = 5
		'
		'LabelControl3
		'
		Me.LabelControl3.Location = New System.Drawing.Point(109, 102)
		Me.LabelControl3.Name = "LabelControl3"
		Me.LabelControl3.Size = New System.Drawing.Size(86, 13)
		Me.LabelControl3.TabIndex = 55
		Me.LabelControl3.Text = "Título del informe:"
		'
		'LabelControl1
		'
		Me.LabelControl1.Location = New System.Drawing.Point(16, 72)
		Me.LabelControl1.Name = "LabelControl1"
		Me.LabelControl1.Size = New System.Drawing.Size(179, 13)
		Me.LabelControl1.TabIndex = 53
		Me.LabelControl1.Text = "Mes y Ejercicio para generar informe:"
		'
		'seEjercicio
		'
		Me.seEjercicio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
		Me.seEjercicio.Location = New System.Drawing.Point(312, 69)
		Me.seEjercicio.Name = "seEjercicio"
		Me.seEjercicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.seEjercicio.Properties.Mask.EditMask = "f0"
		Me.seEjercicio.Properties.Mask.UseMaskAsDisplayFormat = True
		Me.seEjercicio.Size = New System.Drawing.Size(73, 20)
		Me.seEjercicio.TabIndex = 2
		'
		'meMes
		'
		Me.meMes.Location = New System.Drawing.Point(198, 69)
		Me.meMes.Name = "meMes"
		Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.meMes.Size = New System.Drawing.Size(100, 20)
		Me.meMes.TabIndex = 1
		'
		'ceDateInfo
		'
		Me.ceDateInfo.Location = New System.Drawing.Point(198, 125)
		Me.ceDateInfo.Name = "ceDateInfo"
		Me.ceDateInfo.Properties.Caption = "Incluir la fecha y hora de impresión"
		Me.ceDateInfo.Size = New System.Drawing.Size(226, 19)
		Me.ceDateInfo.TabIndex = 57
		Me.ceDateInfo.Visible = False
		'
		'leSucursal
		'
		Me.leSucursal.Location = New System.Drawing.Point(198, 46)
		Me.leSucursal.Name = "leSucursal"
		Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.leSucursal.Size = New System.Drawing.Size(341, 20)
		Me.leSucursal.TabIndex = 0
		'
		'LabelControl7
		'
		Me.LabelControl7.Location = New System.Drawing.Point(152, 49)
		Me.LabelControl7.Name = "LabelControl7"
		Me.LabelControl7.Size = New System.Drawing.Size(44, 13)
		Me.LabelControl7.TabIndex = 60
		Me.LabelControl7.Text = "Sucursal:"
		'
		'con_frmRazonesFinancieras
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.ClientSize = New System.Drawing.Size(621, 314)
		Me.Modulo = "Contabilidad"
		Me.Name = "con_frmRazonesFinancieras"
		Me.Text = "Razones Financieras"
		CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupControl1.ResumeLayout(False)
		Me.GroupControl1.PerformLayout()
		CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.seEjercicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ceDateInfo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
	Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents seEjercicio As DevExpress.XtraEditors.SpinEdit
	Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
	Friend WithEvents ceDateInfo As DevExpress.XtraEditors.CheckEdit
	Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
End Class
