﻿Imports NexusBLL

Public Class con_frmLibroDiario
    Dim bl As New ContabilidadBLL(g_ConnectionString)


    Private Sub cmd_Click() Handles Me.Reporte
        Dim dt As DataTable = bl.rptLibroDiario(leSucursal.EditValue, deDesde.DateTime, deHasta.DateTime, rgTipo.EditValue)
        If rgTipo.EditValue = 0 Then

            Dim rpt As New con_rptLibroDiario() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = txtTitulo.Text
            rpt.xrlPeriodo.Text = (FechaToString(deDesde.DateTime, deHasta.DateTime)).ToUpper
            rpt.xrlMoneda.Text = gsDesc_Moneda
            rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
            If ceSaltoPagina.EditValue Then
                rpt.GroupFooter1.PageBreak = PageBreak.AfterBand
            End If
            rpt.XrPageInfo1.Visible = ceNumerar.EditValue
            rpt.xrlPageNo.Visible = ceNumerar.EditValue
            rpt.XrPageInfo2.Visible = ceInfoDateTime.EditValue
            'los firmantes
            rpt.xrLine1.Visible = ceFirmantes.EditValue
            rpt.xrLine2.Visible = ceFirmantes.EditValue
            rpt.xrLine3.Visible = ceFirmantes.EditValue
            rpt.xrlHecho.Visible = ceFirmantes.EditValue
            rpt.xrlRevisado.Visible = ceFirmantes.EditValue
            rpt.xrlAutorizado.Visible = ceFirmantes.EditValue
            rpt.ShowPreviewDialog()
        Else
             
            Dim rpt As New con_rptLibroMayorFecha() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = txtTitulo.Text
            rpt.xrlPeriodo.Text = (FechaToString(deDesde.DateTime, deHasta.DateTime)).ToUpper
            rpt.xrlMoneda.Text = gsDesc_Moneda
            rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
             
            rpt.XrPageInfo1.Visible = ceNumerar.EditValue
            rpt.XrlPageInfo.Visible = ceNumerar.EditValue
            rpt.XrPageInfo2.Visible = ceInfoDateTime.EditValue
           
            rpt.ShowPreviewDialog()
        End If

    End Sub

    Private Sub frmLibroDiario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.DateTime = Today
        deHasta.DateTime = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

End Class