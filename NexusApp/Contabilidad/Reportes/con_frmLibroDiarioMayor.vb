﻿Imports NexusBLL

Public Class con_frmLibroDiarioMayor
    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private Sub btnReporte_Click() Handles Me.Reporte

        If rgFormato.EditValue = 1 Then
            Dim dt As DataTable = bl.rptLibroDiarioMayorDetalle(deIni.DateTime, deFin.DateTime, leSucursal.EditValue)
            Dim rpt As New con_rptLibroDiarioMayorDetalle() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.EditValue
            rpt.xrlPeriodo.Text = (FechaToString(deIni.DateTime, deFin.DateTime)).ToUpper
            rpt.xrlPage.Visible = ceNumerar.EditValue
            rpt.XrPageInfo1.Visible = ceNumerar.EditValue
            rpt.XrPageInfo1.StartPageNumber = seNumero.EditValue
            rpt.XrPageInfo2.Visible = ceDateInfo.EditValue
            rpt.xrlDescMoneda.Text = gsDesc_Moneda
            rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
            rpt.ShowPreview()
        Else
            Dim dt As DataTable = bl.rptLibroDiarioMayor(deIni.DateTime, deFin.DateTime, leSucursal.EditValue)

            Dim rpt As New con_rptLibroDiarioMayorConsolidado
            rpt.DataMember = ""
            rpt.DataSource = dt
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.EditValue
            rpt.xrlPeriodo.Text = (FechaToString(deIni.DateTime, deFin.DateTime)).ToUpper
            rpt.xrlPage.Visible = ceNumerar.EditValue
            rpt.XrPageInfo1.Visible = ceNumerar.EditValue
            rpt.XrPageInfo1.StartPageNumber = seNumero.EditValue
            rpt.XrPageInfo2.Visible = ceDateInfo.EditValue
            rpt.xrlDescMoneda.Text = gsDesc_Moneda
            rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
            rpt.ShowPreview()
        End If
    End Sub

    Private Sub frmLibroDiarioMayor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deIni.DateTime = Today
        deFin.DateTime = Today
        ceNumerar.EditValue = True
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub


    Private Sub chkNumerar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ceNumerar.CheckedChanged, ceDateInfo.CheckedChanged
        seNumero.Enabled = ceNumerar.EditValue
    End Sub

End Class