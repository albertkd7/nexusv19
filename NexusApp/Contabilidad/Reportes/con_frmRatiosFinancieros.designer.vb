﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class con_frmRatiosFinancieros
	Inherits Nexus.gen_frmBaseRpt

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing AndAlso components IsNot Nothing Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLiquidez = New DevExpress.XtraTab.XtraTabPage()
        Me.sbVerFormula10 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula9 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula8 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula7 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula6 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl8 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl79 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado10 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl9 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl78 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado9 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl10 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl77 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado8 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl11 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl76 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado7 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl12 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl75 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado6 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
        Me.sbVerFormula5 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula4 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula3 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula2 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl74 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado5 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl73 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado4 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl72 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado3 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl71 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado2 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.sbVerFormula1 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl70 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado1 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.xtpEndeudamiento = New DevExpress.XtraTab.XtraTabPage()
        Me.sbVerFormula13 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula12 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl59 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado13 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl13 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado12 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.sbVerFormula11 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl14 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado11 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.xtpRentabilidad = New DevExpress.XtraTab.XtraTabPage()
        Me.sbVerFormula21 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula20 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula19 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl16 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl87 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado21 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl17 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl86 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado20 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl18 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl85 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado19 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        Me.sbVerFormula18 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula17 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula16 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbVerFormula15 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl19 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl84 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado18 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl50 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl20 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl83 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl54 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado17 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl21 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl82 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado16 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl55 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl56 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl22 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl81 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado15 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl57 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl58 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl60 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl61 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl62 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl63 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl64 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl65 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl66 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl67 = New DevExpress.XtraEditors.LabelControl()
        Me.sbVerFormula14 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl23 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl80 = New DevExpress.XtraEditors.LabelControl()
        Me.sbResultado14 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl68 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl69 = New DevExpress.XtraEditors.LabelControl()
        Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit()
        Me.seAnio = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl52 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
        Me.teSituacionAct = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraScrollableControl1 = New DevExpress.XtraEditors.XtraScrollableControl()
        Me.XtraScrollableControl2 = New DevExpress.XtraEditors.XtraScrollableControl()
        Me.XtraScrollableControl3 = New DevExpress.XtraEditors.XtraScrollableControl()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.xtpLiquidez.SuspendLayout()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl10.SuspendLayout()
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl11.SuspendLayout()
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl12.SuspendLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.xtpEndeudamiento.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl13.SuspendLayout()
        CType(Me.GroupControl14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl14.SuspendLayout()
        Me.xtpRentabilidad.SuspendLayout()
        CType(Me.GroupControl16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl16.SuspendLayout()
        CType(Me.GroupControl17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl17.SuspendLayout()
        CType(Me.GroupControl18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl18.SuspendLayout()
        CType(Me.GroupControl19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl19.SuspendLayout()
        CType(Me.GroupControl20, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl20.SuspendLayout()
        CType(Me.GroupControl21, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl21.SuspendLayout()
        CType(Me.GroupControl22, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl22.SuspendLayout()
        CType(Me.GroupControl23, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl23.SuspendLayout()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSituacionAct.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraScrollableControl1.SuspendLayout()
        Me.XtraScrollableControl2.SuspendLayout()
        Me.XtraScrollableControl3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.meMes)
        Me.GroupControl1.Controls.Add(Me.seAnio)
        Me.GroupControl1.Controls.Add(Me.LabelControl52)
        Me.GroupControl1.Controls.Add(Me.LabelControl53)
        Me.GroupControl1.Controls.Add(Me.teSituacionAct)
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Size = New System.Drawing.Size(1368, 72)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Parámetros de los Ratios (Situación Actual)"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 72)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.xtpLiquidez
        Me.XtraTabControl1.Size = New System.Drawing.Size(1368, 586)
        Me.XtraTabControl1.TabIndex = 6
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLiquidez, Me.xtpEndeudamiento, Me.xtpRentabilidad})
        '
        'xtpLiquidez
        '
        Me.xtpLiquidez.Controls.Add(Me.XtraScrollableControl1)
        Me.xtpLiquidez.Name = "xtpLiquidez"
        Me.xtpLiquidez.Size = New System.Drawing.Size(1362, 558)
        Me.xtpLiquidez.Text = "Indices de Liquidez"
        '
        'sbVerFormula10
        '
        Me.sbVerFormula10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula10.Appearance.Options.UseFont = True
        Me.sbVerFormula10.Location = New System.Drawing.Point(985, 462)
        Me.sbVerFormula10.Name = "sbVerFormula10"
        Me.sbVerFormula10.Size = New System.Drawing.Size(91, 41)
        Me.sbVerFormula10.TabIndex = 59
        Me.sbVerFormula10.Text = "Ver Formula..."
        '
        'sbVerFormula9
        '
        Me.sbVerFormula9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula9.Appearance.Options.UseFont = True
        Me.sbVerFormula9.Location = New System.Drawing.Point(985, 346)
        Me.sbVerFormula9.Name = "sbVerFormula9"
        Me.sbVerFormula9.Size = New System.Drawing.Size(91, 41)
        Me.sbVerFormula9.TabIndex = 58
        Me.sbVerFormula9.Text = "Ver Formula..."
        '
        'sbVerFormula8
        '
        Me.sbVerFormula8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula8.Appearance.Options.UseFont = True
        Me.sbVerFormula8.Location = New System.Drawing.Point(985, 248)
        Me.sbVerFormula8.Name = "sbVerFormula8"
        Me.sbVerFormula8.Size = New System.Drawing.Size(91, 41)
        Me.sbVerFormula8.TabIndex = 57
        Me.sbVerFormula8.Text = "Ver Formula..."
        '
        'sbVerFormula7
        '
        Me.sbVerFormula7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula7.Appearance.Options.UseFont = True
        Me.sbVerFormula7.Location = New System.Drawing.Point(985, 151)
        Me.sbVerFormula7.Name = "sbVerFormula7"
        Me.sbVerFormula7.Size = New System.Drawing.Size(91, 41)
        Me.sbVerFormula7.TabIndex = 56
        Me.sbVerFormula7.Text = "Ver Formula..."
        '
        'sbVerFormula6
        '
        Me.sbVerFormula6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula6.Appearance.Options.UseFont = True
        Me.sbVerFormula6.Location = New System.Drawing.Point(985, 47)
        Me.sbVerFormula6.Name = "sbVerFormula6"
        Me.sbVerFormula6.Size = New System.Drawing.Size(91, 41)
        Me.sbVerFormula6.TabIndex = 55
        Me.sbVerFormula6.Text = "Ver Formula..."
        '
        'GroupControl8
        '
        Me.GroupControl8.Controls.Add(Me.LabelControl79)
        Me.GroupControl8.Controls.Add(Me.sbResultado10)
        Me.GroupControl8.Controls.Add(Me.LabelControl32)
        Me.GroupControl8.Controls.Add(Me.LabelControl33)
        Me.GroupControl8.Location = New System.Drawing.Point(763, 434)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(218, 96)
        Me.GroupControl8.TabIndex = 54
        Me.GroupControl8.Text = "Fórmula"
        '
        'LabelControl79
        '
        Me.LabelControl79.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl79.Appearance.Options.UseForeColor = True
        Me.LabelControl79.Location = New System.Drawing.Point(177, 38)
        Me.LabelControl79.Name = "LabelControl79"
        Me.LabelControl79.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl79.TabIndex = 22
        Me.LabelControl79.Text = "= %"
        '
        'sbResultado10
        '
        Me.sbResultado10.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado10.Name = "sbResultado10"
        Me.sbResultado10.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado10.TabIndex = 19
        Me.sbResultado10.Text = "Ver Resultado..."
        '
        'LabelControl32
        '
        Me.LabelControl32.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl32.Appearance.Options.UseForeColor = True
        Me.LabelControl32.Appearance.Options.UseTextOptions = True
        Me.LabelControl32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl32.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl32.Location = New System.Drawing.Point(45, 47)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl32.TabIndex = 18
        Me.LabelControl32.Text = "Patrimonio"
        '
        'LabelControl33
        '
        Me.LabelControl33.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl33.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl33.Appearance.Options.UseFont = True
        Me.LabelControl33.Appearance.Options.UseForeColor = True
        Me.LabelControl33.Location = New System.Drawing.Point(44, 33)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl33.TabIndex = 17
        Me.LabelControl33.Text = "     Pasivo Total      "
        '
        'GroupControl9
        '
        Me.GroupControl9.Controls.Add(Me.LabelControl78)
        Me.GroupControl9.Controls.Add(Me.sbResultado9)
        Me.GroupControl9.Controls.Add(Me.LabelControl36)
        Me.GroupControl9.Controls.Add(Me.LabelControl37)
        Me.GroupControl9.Location = New System.Drawing.Point(763, 329)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(218, 96)
        Me.GroupControl9.TabIndex = 53
        Me.GroupControl9.Text = "Fórmula"
        '
        'LabelControl78
        '
        Me.LabelControl78.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl78.Appearance.Options.UseForeColor = True
        Me.LabelControl78.Location = New System.Drawing.Point(177, 41)
        Me.LabelControl78.Name = "LabelControl78"
        Me.LabelControl78.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl78.TabIndex = 25
        Me.LabelControl78.Text = "= Veces"
        '
        'sbResultado9
        '
        Me.sbResultado9.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado9.Name = "sbResultado9"
        Me.sbResultado9.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado9.TabIndex = 19
        Me.sbResultado9.Text = "Ver Resultado..."
        '
        'LabelControl36
        '
        Me.LabelControl36.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl36.Appearance.Options.UseForeColor = True
        Me.LabelControl36.Appearance.Options.UseTextOptions = True
        Me.LabelControl36.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl36.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl36.Location = New System.Drawing.Point(45, 47)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl36.TabIndex = 18
        Me.LabelControl36.Text = "Activos Totales"
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl37.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl37.Appearance.Options.UseFont = True
        Me.LabelControl37.Appearance.Options.UseForeColor = True
        Me.LabelControl37.Location = New System.Drawing.Point(49, 33)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl37.TabIndex = 17
        Me.LabelControl37.Text = "        Ventas       "
        '
        'GroupControl10
        '
        Me.GroupControl10.Controls.Add(Me.LabelControl77)
        Me.GroupControl10.Controls.Add(Me.sbResultado8)
        Me.GroupControl10.Controls.Add(Me.LabelControl40)
        Me.GroupControl10.Controls.Add(Me.LabelControl41)
        Me.GroupControl10.Location = New System.Drawing.Point(763, 223)
        Me.GroupControl10.Name = "GroupControl10"
        Me.GroupControl10.Size = New System.Drawing.Size(218, 96)
        Me.GroupControl10.TabIndex = 52
        Me.GroupControl10.Text = "Fórmula"
        '
        'LabelControl77
        '
        Me.LabelControl77.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl77.Appearance.Options.UseForeColor = True
        Me.LabelControl77.Location = New System.Drawing.Point(177, 37)
        Me.LabelControl77.Name = "LabelControl77"
        Me.LabelControl77.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl77.TabIndex = 61
        Me.LabelControl77.Text = "= Días"
        '
        'sbResultado8
        '
        Me.sbResultado8.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado8.Name = "sbResultado8"
        Me.sbResultado8.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado8.TabIndex = 19
        Me.sbResultado8.Text = "Ver Resultado..."
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl40.Appearance.Options.UseForeColor = True
        Me.LabelControl40.Appearance.Options.UseTextOptions = True
        Me.LabelControl40.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl40.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl40.Location = New System.Drawing.Point(45, 47)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl40.TabIndex = 18
        Me.LabelControl40.Text = "Ventas"
        '
        'LabelControl41
        '
        Me.LabelControl41.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl41.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl41.Appearance.Options.UseFont = True
        Me.LabelControl41.Appearance.Options.UseForeColor = True
        Me.LabelControl41.Location = New System.Drawing.Point(41, 33)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(98, 13)
        Me.LabelControl41.TabIndex = 17
        Me.LabelControl41.Text = "Caja y Bancos * 360"
        '
        'GroupControl11
        '
        Me.GroupControl11.Controls.Add(Me.LabelControl76)
        Me.GroupControl11.Controls.Add(Me.sbResultado7)
        Me.GroupControl11.Controls.Add(Me.LabelControl44)
        Me.GroupControl11.Controls.Add(Me.LabelControl45)
        Me.GroupControl11.Location = New System.Drawing.Point(763, 119)
        Me.GroupControl11.Name = "GroupControl11"
        Me.GroupControl11.Size = New System.Drawing.Size(218, 96)
        Me.GroupControl11.TabIndex = 51
        Me.GroupControl11.Text = "Fórmula"
        '
        'LabelControl76
        '
        Me.LabelControl76.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl76.Appearance.Options.UseForeColor = True
        Me.LabelControl76.Location = New System.Drawing.Point(177, 36)
        Me.LabelControl76.Name = "LabelControl76"
        Me.LabelControl76.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl76.TabIndex = 61
        Me.LabelControl76.Text = "= Días"
        '
        'sbResultado7
        '
        Me.sbResultado7.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado7.Name = "sbResultado7"
        Me.sbResultado7.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado7.TabIndex = 19
        Me.sbResultado7.Text = "Ver Resultado..."
        '
        'LabelControl44
        '
        Me.LabelControl44.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl44.Appearance.Options.UseForeColor = True
        Me.LabelControl44.Appearance.Options.UseTextOptions = True
        Me.LabelControl44.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl44.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl44.Location = New System.Drawing.Point(30, 47)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(121, 13)
        Me.LabelControl44.TabIndex = 18
        Me.LabelControl44.Text = "Compras  a Proveedores"
        '
        'LabelControl45
        '
        Me.LabelControl45.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl45.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl45.Appearance.Options.UseFont = True
        Me.LabelControl45.Appearance.Options.UseForeColor = True
        Me.LabelControl45.Location = New System.Drawing.Point(3, 33)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(175, 13)
        Me.LabelControl45.TabIndex = 17
        Me.LabelControl45.Text = "Cuentas por Pagar (Promedio) * 360"
        '
        'GroupControl12
        '
        Me.GroupControl12.Controls.Add(Me.LabelControl75)
        Me.GroupControl12.Controls.Add(Me.sbResultado6)
        Me.GroupControl12.Controls.Add(Me.LabelControl48)
        Me.GroupControl12.Controls.Add(Me.LabelControl49)
        Me.GroupControl12.Location = New System.Drawing.Point(763, 16)
        Me.GroupControl12.Name = "GroupControl12"
        Me.GroupControl12.Size = New System.Drawing.Size(218, 96)
        Me.GroupControl12.TabIndex = 50
        Me.GroupControl12.Text = "Fórmula"
        '
        'LabelControl75
        '
        Me.LabelControl75.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl75.Appearance.Options.UseForeColor = True
        Me.LabelControl75.Location = New System.Drawing.Point(177, 36)
        Me.LabelControl75.Name = "LabelControl75"
        Me.LabelControl75.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl75.TabIndex = 60
        Me.LabelControl75.Text = "= Días"
        '
        'sbResultado6
        '
        Me.sbResultado6.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado6.Name = "sbResultado6"
        Me.sbResultado6.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado6.TabIndex = 19
        Me.sbResultado6.Text = "Ver Resultado..."
        '
        'LabelControl48
        '
        Me.LabelControl48.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl48.Appearance.Options.UseForeColor = True
        Me.LabelControl48.Appearance.Options.UseTextOptions = True
        Me.LabelControl48.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl48.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl48.Location = New System.Drawing.Point(45, 47)
        Me.LabelControl48.Name = "LabelControl48"
        Me.LabelControl48.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl48.TabIndex = 18
        Me.LabelControl48.Text = "Ventas"
        '
        'LabelControl49
        '
        Me.LabelControl49.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl49.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl49.Appearance.Options.UseFont = True
        Me.LabelControl49.Appearance.Options.UseForeColor = True
        Me.LabelControl49.Location = New System.Drawing.Point(4, 33)
        Me.LabelControl49.Name = "LabelControl49"
        Me.LabelControl49.Size = New System.Drawing.Size(172, 13)
        Me.LabelControl49.TabIndex = 17
        Me.LabelControl49.Text = "Cuentas por Cobrar Promedio * 360"
        '
        'sbVerFormula5
        '
        Me.sbVerFormula5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula5.Appearance.Options.UseFont = True
        Me.sbVerFormula5.Location = New System.Drawing.Point(458, 462)
        Me.sbVerFormula5.Name = "sbVerFormula5"
        Me.sbVerFormula5.Size = New System.Drawing.Size(93, 41)
        Me.sbVerFormula5.TabIndex = 49
        Me.sbVerFormula5.Text = "Ver Formula..."
        '
        'sbVerFormula4
        '
        Me.sbVerFormula4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula4.Appearance.Options.UseFont = True
        Me.sbVerFormula4.Location = New System.Drawing.Point(458, 346)
        Me.sbVerFormula4.Name = "sbVerFormula4"
        Me.sbVerFormula4.Size = New System.Drawing.Size(93, 41)
        Me.sbVerFormula4.TabIndex = 48
        Me.sbVerFormula4.Text = "Ver Formula..."
        '
        'sbVerFormula3
        '
        Me.sbVerFormula3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula3.Appearance.Options.UseFont = True
        Me.sbVerFormula3.Location = New System.Drawing.Point(458, 248)
        Me.sbVerFormula3.Name = "sbVerFormula3"
        Me.sbVerFormula3.Size = New System.Drawing.Size(93, 41)
        Me.sbVerFormula3.TabIndex = 47
        Me.sbVerFormula3.Text = "Ver Formula..."
        '
        'sbVerFormula2
        '
        Me.sbVerFormula2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula2.Appearance.Options.UseFont = True
        Me.sbVerFormula2.Location = New System.Drawing.Point(458, 151)
        Me.sbVerFormula2.Name = "sbVerFormula2"
        Me.sbVerFormula2.Size = New System.Drawing.Size(93, 41)
        Me.sbVerFormula2.TabIndex = 46
        Me.sbVerFormula2.Text = "Ver Formula..."
        '
        'GroupControl7
        '
        Me.GroupControl7.Controls.Add(Me.LabelControl74)
        Me.GroupControl7.Controls.Add(Me.sbResultado5)
        Me.GroupControl7.Controls.Add(Me.LabelControl28)
        Me.GroupControl7.Controls.Add(Me.LabelControl29)
        Me.GroupControl7.Location = New System.Drawing.Point(236, 434)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(217, 96)
        Me.GroupControl7.TabIndex = 45
        Me.GroupControl7.Text = "Fórmula"
        '
        'LabelControl74
        '
        Me.LabelControl74.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl74.Appearance.Options.UseForeColor = True
        Me.LabelControl74.Location = New System.Drawing.Point(176, 42)
        Me.LabelControl74.Name = "LabelControl74"
        Me.LabelControl74.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl74.TabIndex = 24
        Me.LabelControl74.Text = "= Veces"
        '
        'sbResultado5
        '
        Me.sbResultado5.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado5.Name = "sbResultado5"
        Me.sbResultado5.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado5.TabIndex = 19
        Me.sbResultado5.Text = "Ver Resultado..."
        '
        'LabelControl28
        '
        Me.LabelControl28.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl28.Appearance.Options.UseForeColor = True
        Me.LabelControl28.Appearance.Options.UseTextOptions = True
        Me.LabelControl28.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl28.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl28.Location = New System.Drawing.Point(25, 47)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(128, 13)
        Me.LabelControl28.TabIndex = 18
        Me.LabelControl28.Text = "Cuentas por Cobrar"
        '
        'LabelControl29
        '
        Me.LabelControl29.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl29.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl29.Appearance.Options.UseFont = True
        Me.LabelControl29.Appearance.Options.UseForeColor = True
        Me.LabelControl29.Location = New System.Drawing.Point(28, 33)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(123, 13)
        Me.LabelControl29.TabIndex = 17
        Me.LabelControl29.Text = "Ventas Anuales al Crédito"
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.LabelControl73)
        Me.GroupControl5.Controls.Add(Me.sbResultado4)
        Me.GroupControl5.Controls.Add(Me.LabelControl20)
        Me.GroupControl5.Controls.Add(Me.LabelControl21)
        Me.GroupControl5.Location = New System.Drawing.Point(236, 329)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(217, 96)
        Me.GroupControl5.TabIndex = 44
        Me.GroupControl5.Text = "Fórmula"
        '
        'LabelControl73
        '
        Me.LabelControl73.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl73.Appearance.Options.UseForeColor = True
        Me.LabelControl73.Location = New System.Drawing.Point(176, 37)
        Me.LabelControl73.Name = "LabelControl73"
        Me.LabelControl73.Size = New System.Drawing.Size(31, 13)
        Me.LabelControl73.TabIndex = 22
        Me.LabelControl73.Text = "= Días"
        '
        'sbResultado4
        '
        Me.sbResultado4.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado4.Name = "sbResultado4"
        Me.sbResultado4.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado4.TabIndex = 19
        Me.sbResultado4.Text = "Ver Resultado..."
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl20.Appearance.Options.UseForeColor = True
        Me.LabelControl20.Appearance.Options.UseTextOptions = True
        Me.LabelControl20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl20.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl20.Location = New System.Drawing.Point(25, 47)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(128, 13)
        Me.LabelControl20.TabIndex = 18
        Me.LabelControl20.Text = "Ventas Anuales al Crédito"
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl21.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Appearance.Options.UseForeColor = True
        Me.LabelControl21.Location = New System.Drawing.Point(2, 33)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(175, 13)
        Me.LabelControl21.TabIndex = 17
        Me.LabelControl21.Text = "Cuentas por Cobrar x Días en el Año"
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.LabelControl72)
        Me.GroupControl6.Controls.Add(Me.sbResultado3)
        Me.GroupControl6.Controls.Add(Me.LabelControl24)
        Me.GroupControl6.Controls.Add(Me.LabelControl25)
        Me.GroupControl6.Location = New System.Drawing.Point(236, 223)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(217, 96)
        Me.GroupControl6.TabIndex = 43
        Me.GroupControl6.Text = "Fórmula"
        '
        'LabelControl72
        '
        Me.LabelControl72.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl72.Appearance.Options.UseForeColor = True
        Me.LabelControl72.Location = New System.Drawing.Point(176, 35)
        Me.LabelControl72.Name = "LabelControl72"
        Me.LabelControl72.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl72.TabIndex = 21
        Me.LabelControl72.Text = "= %"
        '
        'sbResultado3
        '
        Me.sbResultado3.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado3.Name = "sbResultado3"
        Me.sbResultado3.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado3.TabIndex = 19
        Me.sbResultado3.Text = "Ver Resultado..."
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl24.Appearance.Options.UseForeColor = True
        Me.LabelControl24.Appearance.Options.UseTextOptions = True
        Me.LabelControl24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl24.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl24.Location = New System.Drawing.Point(44, 47)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl24.TabIndex = 18
        Me.LabelControl24.Text = "Pasivo Circulante"
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl25.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl25.Appearance.Options.UseFont = True
        Me.LabelControl25.Appearance.Options.UseForeColor = True
        Me.LabelControl25.Location = New System.Drawing.Point(55, 33)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl25.TabIndex = 17
        Me.LabelControl25.Text = "Caja y Bancos"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.LabelControl71)
        Me.GroupControl3.Controls.Add(Me.sbResultado2)
        Me.GroupControl3.Controls.Add(Me.LabelControl16)
        Me.GroupControl3.Controls.Add(Me.LabelControl17)
        Me.GroupControl3.Location = New System.Drawing.Point(236, 119)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(217, 96)
        Me.GroupControl3.TabIndex = 42
        Me.GroupControl3.Text = "Fórmula"
        '
        'LabelControl71
        '
        Me.LabelControl71.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl71.Appearance.Options.UseForeColor = True
        Me.LabelControl71.Location = New System.Drawing.Point(176, 35)
        Me.LabelControl71.Name = "LabelControl71"
        Me.LabelControl71.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl71.TabIndex = 23
        Me.LabelControl71.Text = "= Veces"
        '
        'sbResultado2
        '
        Me.sbResultado2.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado2.Name = "sbResultado2"
        Me.sbResultado2.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado2.TabIndex = 19
        Me.sbResultado2.Text = "Ver Resultado..."
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl16.Appearance.Options.UseForeColor = True
        Me.LabelControl16.Appearance.Options.UseTextOptions = True
        Me.LabelControl16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl16.Location = New System.Drawing.Point(44, 46)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl16.TabIndex = 18
        Me.LabelControl16.Text = "Pasivo Circulante"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl17.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Appearance.Options.UseForeColor = True
        Me.LabelControl17.Location = New System.Drawing.Point(15, 32)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(149, 13)
        Me.LabelControl17.TabIndex = 17
        Me.LabelControl17.Text = "Activo Circulante  - Inventarios"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl15.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl15.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Appearance.Options.UseForeColor = True
        Me.LabelControl15.Appearance.Options.UseImageAlign = True
        Me.LabelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl15.Location = New System.Drawing.Point(565, 472)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(150, 38)
        Me.LabelControl15.TabIndex = 41
        Me.LabelControl15.Text = "ESTRUCTURA DE CAPITAL:"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl14.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl14.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Appearance.Options.UseForeColor = True
        Me.LabelControl14.Appearance.Options.UseImageAlign = True
        Me.LabelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl14.Location = New System.Drawing.Point(565, 373)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(192, 38)
        Me.LabelControl14.TabIndex = 40
        Me.LabelControl14.Text = "ROTACION DE ACTIVOS TOTALES:"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl13.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl13.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Appearance.Options.UseForeColor = True
        Me.LabelControl13.Appearance.Options.UseImageAlign = True
        Me.LabelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl13.Location = New System.Drawing.Point(565, 258)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(191, 38)
        Me.LabelControl13.TabIndex = 39
        Me.LabelControl13.Text = "ROTACION DE CAJAS Y BANCOS:"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl12.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl12.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Appearance.Options.UseForeColor = True
        Me.LabelControl12.Appearance.Options.UseImageAlign = True
        Me.LabelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl12.Location = New System.Drawing.Point(564, 149)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(173, 38)
        Me.LabelControl12.TabIndex = 38
        Me.LabelControl12.Text = "PERIODO DE PAGO A PROVEEDORES:"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl10.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl10.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Appearance.Options.UseForeColor = True
        Me.LabelControl10.Appearance.Options.UseImageAlign = True
        Me.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl10.Location = New System.Drawing.Point(565, 57)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(154, 38)
        Me.LabelControl10.TabIndex = 37
        Me.LabelControl10.Text = "ROTACION DE CARTERA:"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl9.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl9.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Appearance.Options.UseForeColor = True
        Me.LabelControl9.Appearance.Options.UseImageAlign = True
        Me.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl9.Location = New System.Drawing.Point(29, 472)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(206, 38)
        Me.LabelControl9.TabIndex = 36
        Me.LabelControl9.Text = "ROTACION DE LAS CUENTAS POR COBRAR:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl4.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Appearance.Options.UseForeColor = True
        Me.LabelControl4.Appearance.Options.UseImageAlign = True
        Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl4.Location = New System.Drawing.Point(29, 366)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(199, 38)
        Me.LabelControl4.TabIndex = 35
        Me.LabelControl4.Text = "PERIODO PROMEDIO DE COBRANZA:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl3.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Appearance.Options.UseForeColor = True
        Me.LabelControl3.Appearance.Options.UseImageAlign = True
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl3.Location = New System.Drawing.Point(29, 258)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(199, 19)
        Me.LabelControl3.TabIndex = 34
        Me.LabelControl3.Text = "PRUEBA DEFENSIVA:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl2.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Appearance.Options.UseForeColor = True
        Me.LabelControl2.Appearance.Options.UseImageAlign = True
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl2.Location = New System.Drawing.Point(29, 149)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(130, 19)
        Me.LabelControl2.TabIndex = 33
        Me.LabelControl2.Text = "PRUEBA ACIDA:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl1.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseForeColor = True
        Me.LabelControl1.Appearance.Options.UseImageAlign = True
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl1.Location = New System.Drawing.Point(29, 57)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(203, 19)
        Me.LabelControl1.TabIndex = 32
        Me.LabelControl1.Text = "LIQUIDEZ GENERAL:"
        '
        'sbVerFormula1
        '
        Me.sbVerFormula1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula1.Appearance.Options.UseFont = True
        Me.sbVerFormula1.Location = New System.Drawing.Point(458, 47)
        Me.sbVerFormula1.Name = "sbVerFormula1"
        Me.sbVerFormula1.Size = New System.Drawing.Size(93, 41)
        Me.sbVerFormula1.TabIndex = 30
        Me.sbVerFormula1.Text = "Ver Formula..."
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.LabelControl70)
        Me.GroupControl2.Controls.Add(Me.sbResultado1)
        Me.GroupControl2.Controls.Add(Me.LabelControl8)
        Me.GroupControl2.Controls.Add(Me.LabelControl7)
        Me.GroupControl2.Location = New System.Drawing.Point(236, 16)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(217, 96)
        Me.GroupControl2.TabIndex = 19
        Me.GroupControl2.Text = "Fórmula"
        '
        'LabelControl70
        '
        Me.LabelControl70.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl70.Appearance.Options.UseForeColor = True
        Me.LabelControl70.Location = New System.Drawing.Point(176, 37)
        Me.LabelControl70.Name = "LabelControl70"
        Me.LabelControl70.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl70.TabIndex = 22
        Me.LabelControl70.Text = "= Veces"
        '
        'sbResultado1
        '
        Me.sbResultado1.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado1.Name = "sbResultado1"
        Me.sbResultado1.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado1.TabIndex = 19
        Me.sbResultado1.Text = "Ver Resultado..."
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl8.Appearance.Options.UseForeColor = True
        Me.LabelControl8.Appearance.Options.UseTextOptions = True
        Me.LabelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl8.Location = New System.Drawing.Point(44, 47)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl8.TabIndex = 18
        Me.LabelControl8.Text = "Pasivo Circulante"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl7.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Appearance.Options.UseForeColor = True
        Me.LabelControl7.Location = New System.Drawing.Point(35, 33)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(108, 13)
        Me.LabelControl7.TabIndex = 17
        Me.LabelControl7.Text = "     Activo Circulante    "
        '
        'xtpEndeudamiento
        '
        Me.xtpEndeudamiento.Controls.Add(Me.XtraScrollableControl2)
        Me.xtpEndeudamiento.Name = "xtpEndeudamiento"
        Me.xtpEndeudamiento.Size = New System.Drawing.Size(1362, 558)
        Me.xtpEndeudamiento.Text = "Indices de Endeudamiento"
        '
        'sbVerFormula13
        '
        Me.sbVerFormula13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula13.Appearance.Options.UseFont = True
        Me.sbVerFormula13.Location = New System.Drawing.Point(436, 259)
        Me.sbVerFormula13.Name = "sbVerFormula13"
        Me.sbVerFormula13.Size = New System.Drawing.Size(95, 41)
        Me.sbVerFormula13.TabIndex = 56
        Me.sbVerFormula13.Text = "Ver Formula..."
        '
        'sbVerFormula12
        '
        Me.sbVerFormula12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula12.Appearance.Options.UseFont = True
        Me.sbVerFormula12.Location = New System.Drawing.Point(436, 162)
        Me.sbVerFormula12.Name = "sbVerFormula12"
        Me.sbVerFormula12.Size = New System.Drawing.Size(95, 41)
        Me.sbVerFormula12.TabIndex = 55
        Me.sbVerFormula12.Text = "Ver Formula..."
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.LabelControl59)
        Me.GroupControl4.Controls.Add(Me.sbResultado13)
        Me.GroupControl4.Controls.Add(Me.LabelControl5)
        Me.GroupControl4.Controls.Add(Me.LabelControl6)
        Me.GroupControl4.Location = New System.Drawing.Point(226, 234)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(206, 96)
        Me.GroupControl4.TabIndex = 54
        Me.GroupControl4.Text = "Fórmula"
        '
        'LabelControl59
        '
        Me.LabelControl59.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl59.Appearance.Options.UseForeColor = True
        Me.LabelControl59.Location = New System.Drawing.Point(163, 37)
        Me.LabelControl59.Name = "LabelControl59"
        Me.LabelControl59.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl59.TabIndex = 22
        Me.LabelControl59.Text = "= Veces"
        '
        'sbResultado13
        '
        Me.sbResultado13.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado13.Name = "sbResultado13"
        Me.sbResultado13.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado13.TabIndex = 19
        Me.sbResultado13.Text = "Ver Resultado..."
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl5.Appearance.Options.UseForeColor = True
        Me.LabelControl5.Appearance.Options.UseTextOptions = True
        Me.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl5.Location = New System.Drawing.Point(62, 47)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl5.TabIndex = 18
        Me.LabelControl5.Text = "Gastos Fijos"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl6.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Appearance.Options.UseForeColor = True
        Me.LabelControl6.Location = New System.Drawing.Point(73, 33)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl6.TabIndex = 17
        Me.LabelControl6.Text = "Utilidad Bruta"
        '
        'GroupControl13
        '
        Me.GroupControl13.Controls.Add(Me.LabelControl34)
        Me.GroupControl13.Controls.Add(Me.sbResultado12)
        Me.GroupControl13.Controls.Add(Me.LabelControl18)
        Me.GroupControl13.Controls.Add(Me.LabelControl19)
        Me.GroupControl13.Location = New System.Drawing.Point(226, 130)
        Me.GroupControl13.Name = "GroupControl13"
        Me.GroupControl13.Size = New System.Drawing.Size(206, 96)
        Me.GroupControl13.TabIndex = 53
        Me.GroupControl13.Text = "Fórmula"
        '
        'LabelControl34
        '
        Me.LabelControl34.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl34.Appearance.Options.UseForeColor = True
        Me.LabelControl34.Location = New System.Drawing.Point(164, 37)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl34.TabIndex = 21
        Me.LabelControl34.Text = "= Veces"
        '
        'sbResultado12
        '
        Me.sbResultado12.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado12.Name = "sbResultado12"
        Me.sbResultado12.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado12.TabIndex = 19
        Me.sbResultado12.Text = "Ver Resultado..."
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl18.Appearance.Options.UseForeColor = True
        Me.LabelControl18.Appearance.Options.UseTextOptions = True
        Me.LabelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl18.Location = New System.Drawing.Point(62, 46)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl18.TabIndex = 18
        Me.LabelControl18.Text = "Gastos Financieros"
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl19.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Appearance.Options.UseForeColor = True
        Me.LabelControl19.Location = New System.Drawing.Point(33, 32)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(130, 13)
        Me.LabelControl19.TabIndex = 17
        Me.LabelControl19.Text = "Utilidad Antes de Intereses"
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl22.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl22.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Appearance.Options.UseForeColor = True
        Me.LabelControl22.Appearance.Options.UseImageAlign = True
        Me.LabelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl22.Location = New System.Drawing.Point(20, 269)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(199, 38)
        Me.LabelControl22.TabIndex = 52
        Me.LabelControl22.Text = "COBERTURA DE GASTOS FIJOS"
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl23.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl23.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Appearance.Options.UseForeColor = True
        Me.LabelControl23.Appearance.Options.UseImageAlign = True
        Me.LabelControl23.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl23.Location = New System.Drawing.Point(20, 160)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(130, 57)
        Me.LabelControl23.TabIndex = 51
        Me.LabelControl23.Text = "COBERTURA DE GASTOS FINANCIEROS"
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl26.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl26.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl26.Appearance.Options.UseFont = True
        Me.LabelControl26.Appearance.Options.UseForeColor = True
        Me.LabelControl26.Appearance.Options.UseImageAlign = True
        Me.LabelControl26.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl26.Location = New System.Drawing.Point(20, 68)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(203, 38)
        Me.LabelControl26.TabIndex = 50
        Me.LabelControl26.Text = "RAZON DE ENDEUDAMIENTO:"
        '
        'sbVerFormula11
        '
        Me.sbVerFormula11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula11.Appearance.Options.UseFont = True
        Me.sbVerFormula11.Location = New System.Drawing.Point(436, 58)
        Me.sbVerFormula11.Name = "sbVerFormula11"
        Me.sbVerFormula11.Size = New System.Drawing.Size(95, 41)
        Me.sbVerFormula11.TabIndex = 49
        Me.sbVerFormula11.Text = "Ver Formula..."
        '
        'GroupControl14
        '
        Me.GroupControl14.Controls.Add(Me.LabelControl31)
        Me.GroupControl14.Controls.Add(Me.sbResultado11)
        Me.GroupControl14.Controls.Add(Me.LabelControl27)
        Me.GroupControl14.Controls.Add(Me.LabelControl30)
        Me.GroupControl14.Location = New System.Drawing.Point(226, 27)
        Me.GroupControl14.Name = "GroupControl14"
        Me.GroupControl14.Size = New System.Drawing.Size(206, 96)
        Me.GroupControl14.TabIndex = 48
        Me.GroupControl14.Text = "Fórmula"
        '
        'LabelControl31
        '
        Me.LabelControl31.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl31.Appearance.Options.UseForeColor = True
        Me.LabelControl31.Location = New System.Drawing.Point(172, 36)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl31.TabIndex = 20
        Me.LabelControl31.Text = "= %"
        '
        'sbResultado11
        '
        Me.sbResultado11.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado11.Name = "sbResultado11"
        Me.sbResultado11.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado11.TabIndex = 19
        Me.sbResultado11.Text = "Ver Resultado..."
        '
        'LabelControl27
        '
        Me.LabelControl27.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl27.Appearance.Options.UseForeColor = True
        Me.LabelControl27.Appearance.Options.UseTextOptions = True
        Me.LabelControl27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl27.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl27.Location = New System.Drawing.Point(62, 47)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl27.TabIndex = 18
        Me.LabelControl27.Text = "Activo Total"
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl30.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Appearance.Options.UseForeColor = True
        Me.LabelControl30.Location = New System.Drawing.Point(53, 33)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(112, 13)
        Me.LabelControl30.TabIndex = 17
        Me.LabelControl30.Text = "          Pasivo Total        "
        '
        'xtpRentabilidad
        '
        Me.xtpRentabilidad.Controls.Add(Me.XtraScrollableControl3)
        Me.xtpRentabilidad.Name = "xtpRentabilidad"
        Me.xtpRentabilidad.Size = New System.Drawing.Size(1362, 558)
        Me.xtpRentabilidad.Text = "Indice de Rentabilidad"
        '
        'sbVerFormula21
        '
        Me.sbVerFormula21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula21.Appearance.Options.UseFont = True
        Me.sbVerFormula21.Location = New System.Drawing.Point(953, 250)
        Me.sbVerFormula21.Name = "sbVerFormula21"
        Me.sbVerFormula21.Size = New System.Drawing.Size(91, 41)
        Me.sbVerFormula21.TabIndex = 84
        Me.sbVerFormula21.Text = "Ver Formula..."
        '
        'sbVerFormula20
        '
        Me.sbVerFormula20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula20.Appearance.Options.UseFont = True
        Me.sbVerFormula20.Location = New System.Drawing.Point(953, 153)
        Me.sbVerFormula20.Name = "sbVerFormula20"
        Me.sbVerFormula20.Size = New System.Drawing.Size(91, 41)
        Me.sbVerFormula20.TabIndex = 83
        Me.sbVerFormula20.Text = "Ver Formula..."
        '
        'sbVerFormula19
        '
        Me.sbVerFormula19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula19.Appearance.Options.UseFont = True
        Me.sbVerFormula19.Location = New System.Drawing.Point(953, 49)
        Me.sbVerFormula19.Name = "sbVerFormula19"
        Me.sbVerFormula19.Size = New System.Drawing.Size(91, 41)
        Me.sbVerFormula19.TabIndex = 82
        Me.sbVerFormula19.Text = "Ver Formula..."
        '
        'GroupControl16
        '
        Me.GroupControl16.Controls.Add(Me.LabelControl87)
        Me.GroupControl16.Controls.Add(Me.sbResultado21)
        Me.GroupControl16.Controls.Add(Me.LabelControl35)
        Me.GroupControl16.Controls.Add(Me.LabelControl38)
        Me.GroupControl16.Location = New System.Drawing.Point(740, 225)
        Me.GroupControl16.Name = "GroupControl16"
        Me.GroupControl16.Size = New System.Drawing.Size(206, 96)
        Me.GroupControl16.TabIndex = 80
        Me.GroupControl16.Text = "Fórmula"
        '
        'LabelControl87
        '
        Me.LabelControl87.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl87.Appearance.Options.UseForeColor = True
        Me.LabelControl87.Location = New System.Drawing.Point(175, 39)
        Me.LabelControl87.Name = "LabelControl87"
        Me.LabelControl87.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl87.TabIndex = 23
        Me.LabelControl87.Text = "= %"
        '
        'sbResultado21
        '
        Me.sbResultado21.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado21.Name = "sbResultado21"
        Me.sbResultado21.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado21.TabIndex = 19
        Me.sbResultado21.Text = "Ver Resultado..."
        '
        'LabelControl35
        '
        Me.LabelControl35.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl35.Appearance.Options.UseForeColor = True
        Me.LabelControl35.Appearance.Options.UseTextOptions = True
        Me.LabelControl35.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl35.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl35.Location = New System.Drawing.Point(52, 47)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl35.TabIndex = 18
        Me.LabelControl35.Text = "Activo Total"
        '
        'LabelControl38
        '
        Me.LabelControl38.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl38.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl38.Appearance.Options.UseFont = True
        Me.LabelControl38.Appearance.Options.UseForeColor = True
        Me.LabelControl38.Location = New System.Drawing.Point(30, 33)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(134, 13)
        Me.LabelControl38.TabIndex = 17
        Me.LabelControl38.Text = "Utilidad Antes de Impuestos"
        '
        'GroupControl17
        '
        Me.GroupControl17.Controls.Add(Me.LabelControl86)
        Me.GroupControl17.Controls.Add(Me.sbResultado20)
        Me.GroupControl17.Controls.Add(Me.LabelControl39)
        Me.GroupControl17.Controls.Add(Me.LabelControl42)
        Me.GroupControl17.Location = New System.Drawing.Point(740, 121)
        Me.GroupControl17.Name = "GroupControl17"
        Me.GroupControl17.Size = New System.Drawing.Size(206, 96)
        Me.GroupControl17.TabIndex = 79
        Me.GroupControl17.Text = "Fórmula"
        '
        'LabelControl86
        '
        Me.LabelControl86.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl86.Appearance.Options.UseForeColor = True
        Me.LabelControl86.Location = New System.Drawing.Point(175, 34)
        Me.LabelControl86.Name = "LabelControl86"
        Me.LabelControl86.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl86.TabIndex = 22
        Me.LabelControl86.Text = "= %"
        '
        'sbResultado20
        '
        Me.sbResultado20.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado20.Name = "sbResultado20"
        Me.sbResultado20.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado20.TabIndex = 19
        Me.sbResultado20.Text = "Ver Resultado..."
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl39.Appearance.Options.UseForeColor = True
        Me.LabelControl39.Appearance.Options.UseTextOptions = True
        Me.LabelControl39.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl39.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl39.Location = New System.Drawing.Point(37, 47)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(121, 13)
        Me.LabelControl39.TabIndex = 18
        Me.LabelControl39.Text = "Ventas Netas              "
        '
        'LabelControl42
        '
        Me.LabelControl42.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl42.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl42.Appearance.Options.UseFont = True
        Me.LabelControl42.Appearance.Options.UseForeColor = True
        Me.LabelControl42.Location = New System.Drawing.Point(20, 33)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(154, 13)
        Me.LabelControl42.TabIndex = 17
        Me.LabelControl42.Text = "                 Utilidad Neta              "
        '
        'GroupControl18
        '
        Me.GroupControl18.Controls.Add(Me.LabelControl85)
        Me.GroupControl18.Controls.Add(Me.sbResultado19)
        Me.GroupControl18.Controls.Add(Me.LabelControl43)
        Me.GroupControl18.Controls.Add(Me.LabelControl46)
        Me.GroupControl18.Location = New System.Drawing.Point(740, 18)
        Me.GroupControl18.Name = "GroupControl18"
        Me.GroupControl18.Size = New System.Drawing.Size(206, 96)
        Me.GroupControl18.TabIndex = 78
        Me.GroupControl18.Text = "Fórmula"
        '
        'LabelControl85
        '
        Me.LabelControl85.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl85.Appearance.Options.UseForeColor = True
        Me.LabelControl85.Location = New System.Drawing.Point(175, 33)
        Me.LabelControl85.Name = "LabelControl85"
        Me.LabelControl85.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl85.TabIndex = 21
        Me.LabelControl85.Text = "= %"
        '
        'sbResultado19
        '
        Me.sbResultado19.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado19.Name = "sbResultado19"
        Me.sbResultado19.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado19.TabIndex = 19
        Me.sbResultado19.Text = "Ver Resultado..."
        '
        'LabelControl43
        '
        Me.LabelControl43.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl43.Appearance.Options.UseForeColor = True
        Me.LabelControl43.Appearance.Options.UseTextOptions = True
        Me.LabelControl43.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl43.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl43.Location = New System.Drawing.Point(52, 47)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl43.TabIndex = 18
        Me.LabelControl43.Text = "Ventas"
        '
        'LabelControl46
        '
        Me.LabelControl46.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl46.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl46.Appearance.Options.UseFont = True
        Me.LabelControl46.Appearance.Options.UseForeColor = True
        Me.LabelControl46.Location = New System.Drawing.Point(34, 33)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(127, 13)
        Me.LabelControl46.TabIndex = 17
        Me.LabelControl46.Text = "Ventas - Costos de Ventas"
        '
        'sbVerFormula18
        '
        Me.sbVerFormula18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula18.Appearance.Options.UseFont = True
        Me.sbVerFormula18.Location = New System.Drawing.Point(451, 464)
        Me.sbVerFormula18.Name = "sbVerFormula18"
        Me.sbVerFormula18.Size = New System.Drawing.Size(90, 41)
        Me.sbVerFormula18.TabIndex = 77
        Me.sbVerFormula18.Text = "Ver Formula..."
        '
        'sbVerFormula17
        '
        Me.sbVerFormula17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula17.Appearance.Options.UseFont = True
        Me.sbVerFormula17.Location = New System.Drawing.Point(451, 348)
        Me.sbVerFormula17.Name = "sbVerFormula17"
        Me.sbVerFormula17.Size = New System.Drawing.Size(90, 41)
        Me.sbVerFormula17.TabIndex = 76
        Me.sbVerFormula17.Text = "Ver Formula..."
        '
        'sbVerFormula16
        '
        Me.sbVerFormula16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula16.Appearance.Options.UseFont = True
        Me.sbVerFormula16.Location = New System.Drawing.Point(451, 250)
        Me.sbVerFormula16.Name = "sbVerFormula16"
        Me.sbVerFormula16.Size = New System.Drawing.Size(90, 41)
        Me.sbVerFormula16.TabIndex = 75
        Me.sbVerFormula16.Text = "Ver Formula..."
        '
        'sbVerFormula15
        '
        Me.sbVerFormula15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula15.Appearance.Options.UseFont = True
        Me.sbVerFormula15.Location = New System.Drawing.Point(451, 153)
        Me.sbVerFormula15.Name = "sbVerFormula15"
        Me.sbVerFormula15.Size = New System.Drawing.Size(90, 41)
        Me.sbVerFormula15.TabIndex = 74
        Me.sbVerFormula15.Text = "Ver Formula..."
        '
        'GroupControl19
        '
        Me.GroupControl19.Controls.Add(Me.LabelControl84)
        Me.GroupControl19.Controls.Add(Me.sbResultado18)
        Me.GroupControl19.Controls.Add(Me.LabelControl47)
        Me.GroupControl19.Controls.Add(Me.LabelControl50)
        Me.GroupControl19.Location = New System.Drawing.Point(229, 436)
        Me.GroupControl19.Name = "GroupControl19"
        Me.GroupControl19.Size = New System.Drawing.Size(218, 96)
        Me.GroupControl19.TabIndex = 73
        Me.GroupControl19.Text = "Fórmula"
        '
        'LabelControl84
        '
        Me.LabelControl84.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl84.Appearance.Options.UseForeColor = True
        Me.LabelControl84.Location = New System.Drawing.Point(193, 42)
        Me.LabelControl84.Name = "LabelControl84"
        Me.LabelControl84.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl84.TabIndex = 22
        Me.LabelControl84.Text = "= %"
        '
        'sbResultado18
        '
        Me.sbResultado18.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado18.Name = "sbResultado18"
        Me.sbResultado18.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado18.TabIndex = 19
        Me.sbResultado18.Text = "Ver Resultado..."
        '
        'LabelControl47
        '
        Me.LabelControl47.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl47.Appearance.Options.UseForeColor = True
        Me.LabelControl47.Appearance.Options.UseTextOptions = True
        Me.LabelControl47.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl47.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl47.Location = New System.Drawing.Point(12, 47)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(168, 13)
        Me.LabelControl47.TabIndex = 18
        Me.LabelControl47.Text = "Número de Acciones Comunes"
        '
        'LabelControl50
        '
        Me.LabelControl50.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl50.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl50.Appearance.Options.UseFont = True
        Me.LabelControl50.Appearance.Options.UseForeColor = True
        Me.LabelControl50.Location = New System.Drawing.Point(51, 33)
        Me.LabelControl50.Name = "LabelControl50"
        Me.LabelControl50.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl50.TabIndex = 17
        Me.LabelControl50.Text = "    Utilidad Neta      "
        '
        'GroupControl20
        '
        Me.GroupControl20.Controls.Add(Me.LabelControl83)
        Me.GroupControl20.Controls.Add(Me.LabelControl54)
        Me.GroupControl20.Controls.Add(Me.sbResultado17)
        Me.GroupControl20.Controls.Add(Me.LabelControl51)
        Me.GroupControl20.Location = New System.Drawing.Point(229, 331)
        Me.GroupControl20.Name = "GroupControl20"
        Me.GroupControl20.Size = New System.Drawing.Size(218, 96)
        Me.GroupControl20.TabIndex = 72
        Me.GroupControl20.Text = "Fórmula"
        '
        'LabelControl83
        '
        Me.LabelControl83.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl83.Appearance.Options.UseForeColor = True
        Me.LabelControl83.Location = New System.Drawing.Point(193, 31)
        Me.LabelControl83.Name = "LabelControl83"
        Me.LabelControl83.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl83.TabIndex = 21
        Me.LabelControl83.Text = "= %"
        '
        'LabelControl54
        '
        Me.LabelControl54.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl54.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl54.Appearance.Options.UseFont = True
        Me.LabelControl54.Appearance.Options.UseForeColor = True
        Me.LabelControl54.Location = New System.Drawing.Point(1, 31)
        Me.LabelControl54.Name = "LabelControl54"
        Me.LabelControl54.Size = New System.Drawing.Size(191, 13)
        Me.LabelControl54.TabIndex = 20
        Me.LabelControl54.Text = "Utilidad antes de Intereses e Impuestos"
        '
        'sbResultado17
        '
        Me.sbResultado17.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado17.Name = "sbResultado17"
        Me.sbResultado17.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado17.TabIndex = 19
        Me.sbResultado17.Text = "Ver Resultado..."
        '
        'LabelControl51
        '
        Me.LabelControl51.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl51.Appearance.Options.UseForeColor = True
        Me.LabelControl51.Appearance.Options.UseTextOptions = True
        Me.LabelControl51.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl51.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl51.Location = New System.Drawing.Point(32, 47)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(128, 13)
        Me.LabelControl51.TabIndex = 18
        Me.LabelControl51.Text = "Ventas"
        '
        'GroupControl21
        '
        Me.GroupControl21.Controls.Add(Me.LabelControl82)
        Me.GroupControl21.Controls.Add(Me.sbResultado16)
        Me.GroupControl21.Controls.Add(Me.LabelControl55)
        Me.GroupControl21.Controls.Add(Me.LabelControl56)
        Me.GroupControl21.Location = New System.Drawing.Point(229, 225)
        Me.GroupControl21.Name = "GroupControl21"
        Me.GroupControl21.Size = New System.Drawing.Size(218, 96)
        Me.GroupControl21.TabIndex = 71
        Me.GroupControl21.Text = "Fórmula"
        '
        'LabelControl82
        '
        Me.LabelControl82.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl82.Appearance.Options.UseForeColor = True
        Me.LabelControl82.Location = New System.Drawing.Point(193, 35)
        Me.LabelControl82.Name = "LabelControl82"
        Me.LabelControl82.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl82.TabIndex = 23
        Me.LabelControl82.Text = "= %"
        '
        'sbResultado16
        '
        Me.sbResultado16.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado16.Name = "sbResultado16"
        Me.sbResultado16.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado16.TabIndex = 19
        Me.sbResultado16.Text = "Ver Resultado..."
        '
        'LabelControl55
        '
        Me.LabelControl55.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl55.Appearance.Options.UseForeColor = True
        Me.LabelControl55.Appearance.Options.UseTextOptions = True
        Me.LabelControl55.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl55.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl55.Location = New System.Drawing.Point(51, 47)
        Me.LabelControl55.Name = "LabelControl55"
        Me.LabelControl55.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl55.TabIndex = 18
        Me.LabelControl55.Text = "Activos"
        '
        'LabelControl56
        '
        Me.LabelControl56.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl56.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl56.Appearance.Options.UseFont = True
        Me.LabelControl56.Appearance.Options.UseForeColor = True
        Me.LabelControl56.Location = New System.Drawing.Point(1, 33)
        Me.LabelControl56.Name = "LabelControl56"
        Me.LabelControl56.Size = New System.Drawing.Size(191, 13)
        Me.LabelControl56.TabIndex = 17
        Me.LabelControl56.Text = "Utilidad antes de Intereses e Impuestos"
        '
        'GroupControl22
        '
        Me.GroupControl22.Controls.Add(Me.LabelControl81)
        Me.GroupControl22.Controls.Add(Me.sbResultado15)
        Me.GroupControl22.Controls.Add(Me.LabelControl57)
        Me.GroupControl22.Controls.Add(Me.LabelControl58)
        Me.GroupControl22.Location = New System.Drawing.Point(229, 121)
        Me.GroupControl22.Name = "GroupControl22"
        Me.GroupControl22.Size = New System.Drawing.Size(218, 96)
        Me.GroupControl22.TabIndex = 70
        Me.GroupControl22.Text = "Fórmula"
        '
        'LabelControl81
        '
        Me.LabelControl81.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl81.Appearance.Options.UseForeColor = True
        Me.LabelControl81.Location = New System.Drawing.Point(193, 34)
        Me.LabelControl81.Name = "LabelControl81"
        Me.LabelControl81.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl81.TabIndex = 22
        Me.LabelControl81.Text = "= %"
        '
        'sbResultado15
        '
        Me.sbResultado15.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado15.Name = "sbResultado15"
        Me.sbResultado15.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado15.TabIndex = 19
        Me.sbResultado15.Text = "Ver Resultado..."
        '
        'LabelControl57
        '
        Me.LabelControl57.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl57.Appearance.Options.UseForeColor = True
        Me.LabelControl57.Appearance.Options.UseTextOptions = True
        Me.LabelControl57.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl57.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl57.Location = New System.Drawing.Point(51, 46)
        Me.LabelControl57.Name = "LabelControl57"
        Me.LabelControl57.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl57.TabIndex = 18
        Me.LabelControl57.Text = "Activo Total"
        '
        'LabelControl58
        '
        Me.LabelControl58.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl58.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl58.Appearance.Options.UseFont = True
        Me.LabelControl58.Appearance.Options.UseForeColor = True
        Me.LabelControl58.Location = New System.Drawing.Point(54, 32)
        Me.LabelControl58.Name = "LabelControl58"
        Me.LabelControl58.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl58.TabIndex = 17
        Me.LabelControl58.Text = "    Utilidad Neta    "
        '
        'LabelControl60
        '
        Me.LabelControl60.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl60.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl60.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl60.Appearance.Options.UseFont = True
        Me.LabelControl60.Appearance.Options.UseForeColor = True
        Me.LabelControl60.Appearance.Options.UseImageAlign = True
        Me.LabelControl60.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl60.Location = New System.Drawing.Point(558, 260)
        Me.LabelControl60.Name = "LabelControl60"
        Me.LabelControl60.Size = New System.Drawing.Size(168, 19)
        Me.LabelControl60.TabIndex = 68
        Me.LabelControl60.Text = "DUPONT:"
        '
        'LabelControl61
        '
        Me.LabelControl61.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl61.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl61.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl61.Appearance.Options.UseFont = True
        Me.LabelControl61.Appearance.Options.UseForeColor = True
        Me.LabelControl61.Appearance.Options.UseImageAlign = True
        Me.LabelControl61.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl61.Location = New System.Drawing.Point(558, 151)
        Me.LabelControl61.Name = "LabelControl61"
        Me.LabelControl61.Size = New System.Drawing.Size(173, 38)
        Me.LabelControl61.TabIndex = 67
        Me.LabelControl61.Text = "MARGEN NETO DE UTILIDAD:"
        '
        'LabelControl62
        '
        Me.LabelControl62.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl62.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl62.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl62.Appearance.Options.UseFont = True
        Me.LabelControl62.Appearance.Options.UseForeColor = True
        Me.LabelControl62.Appearance.Options.UseImageAlign = True
        Me.LabelControl62.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl62.Location = New System.Drawing.Point(559, 59)
        Me.LabelControl62.Name = "LabelControl62"
        Me.LabelControl62.Size = New System.Drawing.Size(154, 38)
        Me.LabelControl62.TabIndex = 66
        Me.LabelControl62.Text = "MARGEN DE UTILIDAD BRUTA:"
        '
        'LabelControl63
        '
        Me.LabelControl63.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl63.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl63.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl63.Appearance.Options.UseFont = True
        Me.LabelControl63.Appearance.Options.UseForeColor = True
        Me.LabelControl63.Appearance.Options.UseImageAlign = True
        Me.LabelControl63.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl63.Location = New System.Drawing.Point(23, 474)
        Me.LabelControl63.Name = "LabelControl63"
        Me.LabelControl63.Size = New System.Drawing.Size(206, 19)
        Me.LabelControl63.TabIndex = 65
        Me.LabelControl63.Text = "UTILIDAD POR ACCION:"
        '
        'LabelControl64
        '
        Me.LabelControl64.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl64.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl64.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl64.Appearance.Options.UseFont = True
        Me.LabelControl64.Appearance.Options.UseForeColor = True
        Me.LabelControl64.Appearance.Options.UseImageAlign = True
        Me.LabelControl64.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl64.Location = New System.Drawing.Point(23, 368)
        Me.LabelControl64.Name = "LabelControl64"
        Me.LabelControl64.Size = New System.Drawing.Size(199, 38)
        Me.LabelControl64.TabIndex = 64
        Me.LabelControl64.Text = "UTILIDAD SOBRE VENTAS:"
        '
        'LabelControl65
        '
        Me.LabelControl65.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl65.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl65.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl65.Appearance.Options.UseFont = True
        Me.LabelControl65.Appearance.Options.UseForeColor = True
        Me.LabelControl65.Appearance.Options.UseImageAlign = True
        Me.LabelControl65.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl65.Location = New System.Drawing.Point(23, 260)
        Me.LabelControl65.Name = "LabelControl65"
        Me.LabelControl65.Size = New System.Drawing.Size(199, 38)
        Me.LabelControl65.TabIndex = 63
        Me.LabelControl65.Text = "UTILIDAD SOBRE LOS ACTIVOS:"
        '
        'LabelControl66
        '
        Me.LabelControl66.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl66.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl66.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl66.Appearance.Options.UseFont = True
        Me.LabelControl66.Appearance.Options.UseForeColor = True
        Me.LabelControl66.Appearance.Options.UseImageAlign = True
        Me.LabelControl66.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl66.Location = New System.Drawing.Point(23, 151)
        Me.LabelControl66.Name = "LabelControl66"
        Me.LabelControl66.Size = New System.Drawing.Size(130, 57)
        Me.LabelControl66.TabIndex = 62
        Me.LabelControl66.Text = "RENDIMIENTO SOBRE LA INVERSION:"
        '
        'LabelControl67
        '
        Me.LabelControl67.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl67.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl67.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.LabelControl67.Appearance.Options.UseFont = True
        Me.LabelControl67.Appearance.Options.UseForeColor = True
        Me.LabelControl67.Appearance.Options.UseImageAlign = True
        Me.LabelControl67.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl67.Location = New System.Drawing.Point(23, 59)
        Me.LabelControl67.Name = "LabelControl67"
        Me.LabelControl67.Size = New System.Drawing.Size(203, 38)
        Me.LabelControl67.TabIndex = 61
        Me.LabelControl67.Text = "RENDIMIENTO SOBRE EL PATRIMONIO:"
        '
        'sbVerFormula14
        '
        Me.sbVerFormula14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbVerFormula14.Appearance.Options.UseFont = True
        Me.sbVerFormula14.Location = New System.Drawing.Point(451, 49)
        Me.sbVerFormula14.Name = "sbVerFormula14"
        Me.sbVerFormula14.Size = New System.Drawing.Size(90, 41)
        Me.sbVerFormula14.TabIndex = 60
        Me.sbVerFormula14.Text = "Ver Formula..."
        '
        'GroupControl23
        '
        Me.GroupControl23.Controls.Add(Me.LabelControl80)
        Me.GroupControl23.Controls.Add(Me.sbResultado14)
        Me.GroupControl23.Controls.Add(Me.LabelControl68)
        Me.GroupControl23.Controls.Add(Me.LabelControl69)
        Me.GroupControl23.Location = New System.Drawing.Point(229, 18)
        Me.GroupControl23.Name = "GroupControl23"
        Me.GroupControl23.Size = New System.Drawing.Size(218, 96)
        Me.GroupControl23.TabIndex = 59
        Me.GroupControl23.Text = "Fórmula"
        '
        'LabelControl80
        '
        Me.LabelControl80.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl80.Appearance.Options.UseForeColor = True
        Me.LabelControl80.Location = New System.Drawing.Point(193, 33)
        Me.LabelControl80.Name = "LabelControl80"
        Me.LabelControl80.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl80.TabIndex = 21
        Me.LabelControl80.Text = "= %"
        '
        'sbResultado14
        '
        Me.sbResultado14.Location = New System.Drawing.Point(17, 66)
        Me.sbResultado14.Name = "sbResultado14"
        Me.sbResultado14.Size = New System.Drawing.Size(170, 23)
        Me.sbResultado14.TabIndex = 19
        Me.sbResultado14.Text = "Ver Resultado..."
        '
        'LabelControl68
        '
        Me.LabelControl68.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl68.Appearance.Options.UseForeColor = True
        Me.LabelControl68.Appearance.Options.UseTextOptions = True
        Me.LabelControl68.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl68.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl68.Location = New System.Drawing.Point(27, 47)
        Me.LabelControl68.Name = "LabelControl68"
        Me.LabelControl68.Size = New System.Drawing.Size(139, 13)
        Me.LabelControl68.TabIndex = 18
        Me.LabelControl68.Text = "Capital o Patrimonio"
        '
        'LabelControl69
        '
        Me.LabelControl69.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.LabelControl69.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl69.Appearance.Options.UseFont = True
        Me.LabelControl69.Appearance.Options.UseForeColor = True
        Me.LabelControl69.Location = New System.Drawing.Point(48, 33)
        Me.LabelControl69.Name = "LabelControl69"
        Me.LabelControl69.Size = New System.Drawing.Size(97, 13)
        Me.LabelControl69.TabIndex = 17
        Me.LabelControl69.Text = "     Utilidad Neta       "
        '
        'meMes
        '
        Me.meMes.Location = New System.Drawing.Point(39, 47)
        Me.meMes.Name = "meMes"
        Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes.Size = New System.Drawing.Size(112, 20)
        Me.meMes.TabIndex = 39
        '
        'seAnio
        '
        Me.seAnio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seAnio.Location = New System.Drawing.Point(39, 23)
        Me.seAnio.Name = "seAnio"
        Me.seAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seAnio.Size = New System.Drawing.Size(112, 20)
        Me.seAnio.TabIndex = 36
        '
        'LabelControl52
        '
        Me.LabelControl52.Location = New System.Drawing.Point(14, 26)
        Me.LabelControl52.Name = "LabelControl52"
        Me.LabelControl52.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl52.TabIndex = 37
        Me.LabelControl52.Text = "Año"
        '
        'LabelControl53
        '
        Me.LabelControl53.Location = New System.Drawing.Point(14, 50)
        Me.LabelControl53.Name = "LabelControl53"
        Me.LabelControl53.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl53.TabIndex = 38
        Me.LabelControl53.Text = "Mes"
        '
        'teSituacionAct
        '
        Me.teSituacionAct.Location = New System.Drawing.Point(334, 23)
        Me.teSituacionAct.Name = "teSituacionAct"
        Me.teSituacionAct.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.teSituacionAct.Properties.Appearance.Options.UseFont = True
        Me.teSituacionAct.Properties.ReadOnly = True
        Me.teSituacionAct.Size = New System.Drawing.Size(108, 22)
        Me.teSituacionAct.TabIndex = 35
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(215, 26)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(110, 16)
        Me.LabelControl11.TabIndex = 34
        Me.LabelControl11.Text = "Situación Actual:"
        '
        'XtraScrollableControl1
        '
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl1)
        Me.XtraScrollableControl1.Controls.Add(Me.sbVerFormula10)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl12)
        Me.XtraScrollableControl1.Controls.Add(Me.GroupControl10)
        Me.XtraScrollableControl1.Controls.Add(Me.GroupControl2)
        Me.XtraScrollableControl1.Controls.Add(Me.GroupControl11)
        Me.XtraScrollableControl1.Controls.Add(Me.GroupControl7)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl10)
        Me.XtraScrollableControl1.Controls.Add(Me.sbVerFormula9)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl13)
        Me.XtraScrollableControl1.Controls.Add(Me.GroupControl5)
        Me.XtraScrollableControl1.Controls.Add(Me.GroupControl9)
        Me.XtraScrollableControl1.Controls.Add(Me.sbVerFormula1)
        Me.XtraScrollableControl1.Controls.Add(Me.GroupControl12)
        Me.XtraScrollableControl1.Controls.Add(Me.sbVerFormula2)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl9)
        Me.XtraScrollableControl1.Controls.Add(Me.sbVerFormula8)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl14)
        Me.XtraScrollableControl1.Controls.Add(Me.GroupControl6)
        Me.XtraScrollableControl1.Controls.Add(Me.GroupControl8)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl2)
        Me.XtraScrollableControl1.Controls.Add(Me.sbVerFormula5)
        Me.XtraScrollableControl1.Controls.Add(Me.sbVerFormula3)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl4)
        Me.XtraScrollableControl1.Controls.Add(Me.sbVerFormula7)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl15)
        Me.XtraScrollableControl1.Controls.Add(Me.GroupControl3)
        Me.XtraScrollableControl1.Controls.Add(Me.sbVerFormula6)
        Me.XtraScrollableControl1.Controls.Add(Me.LabelControl3)
        Me.XtraScrollableControl1.Controls.Add(Me.sbVerFormula4)
        Me.XtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraScrollableControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraScrollableControl1.Name = "XtraScrollableControl1"
        Me.XtraScrollableControl1.Size = New System.Drawing.Size(1362, 558)
        Me.XtraScrollableControl1.TabIndex = 60
        '
        'XtraScrollableControl2
        '
        Me.XtraScrollableControl2.Controls.Add(Me.LabelControl26)
        Me.XtraScrollableControl2.Controls.Add(Me.sbVerFormula13)
        Me.XtraScrollableControl2.Controls.Add(Me.GroupControl14)
        Me.XtraScrollableControl2.Controls.Add(Me.sbVerFormula12)
        Me.XtraScrollableControl2.Controls.Add(Me.sbVerFormula11)
        Me.XtraScrollableControl2.Controls.Add(Me.GroupControl4)
        Me.XtraScrollableControl2.Controls.Add(Me.LabelControl23)
        Me.XtraScrollableControl2.Controls.Add(Me.GroupControl13)
        Me.XtraScrollableControl2.Controls.Add(Me.LabelControl22)
        Me.XtraScrollableControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraScrollableControl2.Location = New System.Drawing.Point(0, 0)
        Me.XtraScrollableControl2.Name = "XtraScrollableControl2"
        Me.XtraScrollableControl2.Size = New System.Drawing.Size(1362, 558)
        Me.XtraScrollableControl2.TabIndex = 57
        '
        'XtraScrollableControl3
        '
        Me.XtraScrollableControl3.Controls.Add(Me.GroupControl23)
        Me.XtraScrollableControl3.Controls.Add(Me.sbVerFormula21)
        Me.XtraScrollableControl3.Controls.Add(Me.sbVerFormula14)
        Me.XtraScrollableControl3.Controls.Add(Me.sbVerFormula20)
        Me.XtraScrollableControl3.Controls.Add(Me.LabelControl67)
        Me.XtraScrollableControl3.Controls.Add(Me.sbVerFormula19)
        Me.XtraScrollableControl3.Controls.Add(Me.LabelControl66)
        Me.XtraScrollableControl3.Controls.Add(Me.GroupControl16)
        Me.XtraScrollableControl3.Controls.Add(Me.LabelControl65)
        Me.XtraScrollableControl3.Controls.Add(Me.GroupControl17)
        Me.XtraScrollableControl3.Controls.Add(Me.LabelControl64)
        Me.XtraScrollableControl3.Controls.Add(Me.GroupControl18)
        Me.XtraScrollableControl3.Controls.Add(Me.LabelControl63)
        Me.XtraScrollableControl3.Controls.Add(Me.sbVerFormula18)
        Me.XtraScrollableControl3.Controls.Add(Me.LabelControl62)
        Me.XtraScrollableControl3.Controls.Add(Me.sbVerFormula17)
        Me.XtraScrollableControl3.Controls.Add(Me.LabelControl61)
        Me.XtraScrollableControl3.Controls.Add(Me.sbVerFormula16)
        Me.XtraScrollableControl3.Controls.Add(Me.LabelControl60)
        Me.XtraScrollableControl3.Controls.Add(Me.sbVerFormula15)
        Me.XtraScrollableControl3.Controls.Add(Me.GroupControl22)
        Me.XtraScrollableControl3.Controls.Add(Me.GroupControl19)
        Me.XtraScrollableControl3.Controls.Add(Me.GroupControl21)
        Me.XtraScrollableControl3.Controls.Add(Me.GroupControl20)
        Me.XtraScrollableControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraScrollableControl3.Location = New System.Drawing.Point(0, 0)
        Me.XtraScrollableControl3.Name = "XtraScrollableControl3"
        Me.XtraScrollableControl3.Size = New System.Drawing.Size(1362, 558)
        Me.XtraScrollableControl3.TabIndex = 85
        '
        'con_frmRatiosFinancieros
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(1368, 683)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmRatiosFinancieros"
        Me.Text = "Ratios Financieros"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.XtraTabControl1, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.xtpLiquidez.ResumeLayout(False)
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        Me.GroupControl8.PerformLayout()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.GroupControl9.PerformLayout()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl10.ResumeLayout(False)
        Me.GroupControl10.PerformLayout()
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl11.ResumeLayout(False)
        Me.GroupControl11.PerformLayout()
        CType(Me.GroupControl12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl12.ResumeLayout(False)
        Me.GroupControl12.PerformLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        Me.xtpEndeudamiento.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl13.ResumeLayout(False)
        Me.GroupControl13.PerformLayout()
        CType(Me.GroupControl14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl14.ResumeLayout(False)
        Me.GroupControl14.PerformLayout()
        Me.xtpRentabilidad.ResumeLayout(False)
        CType(Me.GroupControl16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl16.ResumeLayout(False)
        Me.GroupControl16.PerformLayout()
        CType(Me.GroupControl17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl17.ResumeLayout(False)
        Me.GroupControl17.PerformLayout()
        CType(Me.GroupControl18, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl18.ResumeLayout(False)
        Me.GroupControl18.PerformLayout()
        CType(Me.GroupControl19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl19.ResumeLayout(False)
        Me.GroupControl19.PerformLayout()
        CType(Me.GroupControl20, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl20.ResumeLayout(False)
        Me.GroupControl20.PerformLayout()
        CType(Me.GroupControl21, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl21.ResumeLayout(False)
        Me.GroupControl21.PerformLayout()
        CType(Me.GroupControl22, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl22.ResumeLayout(False)
        Me.GroupControl22.PerformLayout()
        CType(Me.GroupControl23, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl23.ResumeLayout(False)
        Me.GroupControl23.PerformLayout()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSituacionAct.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraScrollableControl1.ResumeLayout(False)
        Me.XtraScrollableControl2.ResumeLayout(False)
        Me.XtraScrollableControl3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
	Friend WithEvents seAnio As DevExpress.XtraEditors.SpinEdit
	Friend WithEvents LabelControl52 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents teSituacionAct As DevExpress.XtraEditors.TextEdit
	Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
	Friend WithEvents xtpLiquidez As DevExpress.XtraTab.XtraTabPage
	Friend WithEvents sbVerFormula10 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula9 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula8 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula7 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula6 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado10 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado9 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl10 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado8 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl11 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado7 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl12 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado6 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents sbVerFormula5 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula4 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula3 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula2 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado5 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado4 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado3 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado2 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents sbVerFormula1 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado1 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents xtpEndeudamiento As DevExpress.XtraTab.XtraTabPage
	Friend WithEvents xtpRentabilidad As DevExpress.XtraTab.XtraTabPage
	Friend WithEvents sbVerFormula13 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula12 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado13 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl13 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado12 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents sbVerFormula11 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents GroupControl14 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado11 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents sbVerFormula21 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula20 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula19 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents GroupControl16 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado21 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl17 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado20 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl18 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado19 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents sbVerFormula18 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula17 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula16 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents sbVerFormula15 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents GroupControl19 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado18 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl50 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl20 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado17 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl21 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado16 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl55 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl56 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents GroupControl22 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado15 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl57 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl58 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl60 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl61 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl62 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl63 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl64 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl65 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl66 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl67 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents sbVerFormula14 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents GroupControl23 As DevExpress.XtraEditors.GroupControl
	Friend WithEvents sbResultado14 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents LabelControl68 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl69 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl79 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl78 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl77 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl76 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl75 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl74 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl73 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl72 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl71 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl70 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl59 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl87 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl86 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl85 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl84 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl83 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl54 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl82 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl81 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl80 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraScrollableControl1 As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents XtraScrollableControl2 As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents XtraScrollableControl3 As DevExpress.XtraEditors.XtraScrollableControl
End Class
