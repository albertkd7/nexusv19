Public Class con_rptEstadoResultados
    Private dTotalIngreso As Decimal = 0
    Private dTotalGasto As Decimal = 0

    Private Sub XrLabel11_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel11.BeforePrint
        Dim cNivel As String = (SiEsNulo(GetCurrentColumnValue("nivel"), 0)).ToString
        If ("012").IndexOf(cNivel) > -1 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub XrLabel3_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel3.BeforePrint
        Dim cNivel As String = (SiEsNulo(GetCurrentColumnValue("nivel"), 0)).ToString
        If ("013").IndexOf(cNivel) > -1 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub XrLabel6_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel6.BeforePrint
        Dim cNivel As String = (SiEsNulo(GetCurrentColumnValue("nivel"), 0)).ToString
        If ("023").IndexOf(cNivel) > -1 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub XrLine14_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLine14.BeforePrint
        If SiEsNulo(GetCurrentColumnValue("sn_Linea1"), 0) = 1 Then
            e.Cancel = False
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub XrLineParcial1_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLineParcial1.BeforePrint
        If SiEsNulo(GetCurrentColumnValue("sn_Linea2"), 0) = 1 Then
            e.Cancel = False
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub xrlTotal_SummaryRowChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles xrlTotal.SummaryRowChanged
        If SiEsNulo(GetCurrentColumnValue("nivel"), 0) = 1 And SiEsNulo(GetCurrentColumnValue("Tipo"), 0) = 1 Then
            dTotalIngreso += GetCurrentColumnValue("Saldo")
        End If
        If SiEsNulo(GetCurrentColumnValue("nivel"), 0) = 1 And SiEsNulo(GetCurrentColumnValue("Tipo"), 0) = 2 Then
            dTotalGasto += GetCurrentColumnValue("Saldo")
        End If
    End Sub

    Private Sub xrlTotal_SummaryGetResult(ByVal sender As System.Object, ByVal e As DevExpress.XtraReports.UI.SummaryGetResultEventArgs) Handles xrlTotal.SummaryGetResult
        e.Result = dTotalIngreso - dTotalGasto
        e.Handled = True
    End Sub


    Private Sub XrLabel2_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel2.BeforePrint
        Dim nNivel As Integer = SiEsNulo(GetCurrentColumnValue("nivel"), 0)
        If nNivel = 1 Then
            XrLabel2.Text = "" & GetCurrentRow().Row("nombre")
        End If
        If nNivel = 2 Then
            XrLabel2.Text = "    " & GetCurrentRow().Row("nombre")
        End If
        If nNivel = 3 Then
            XrLabel2.Text = "       " & GetCurrentRow().Row("nombre")
        End If
    End Sub

    Private Sub XrLabel15_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel15.BeforePrint
        If dTotalIngreso - dTotalGasto > 0 Then
            XrLabel15.Text = "UTILIDAD DEL EJERCICIO" '--> Antes era "EXCENDE..."
        Else
            XrLabel15.Text = "PERDIDA DEL EJERCICIO" '--> Antes era "DEFICIT..."
        End If
    End Sub
End Class