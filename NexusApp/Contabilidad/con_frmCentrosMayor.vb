﻿Imports NexusBLL
Public Class con_frmCentrosMayor
    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private Sub con_frmCentrosMayor_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        objCombos.con_CentrosCosto(leCentros, "")
        gc.DataSource = bl.ObtenerCuentasMayor(leCentros.EditValue)
    End Sub

    Private Sub leCentros_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles leCentros.EditValueChanged
        gc.DataSource = bl.ObtenerCuentasMayor(leCentros.EditValue)
    End Sub

    Private Sub con_frmCentrosMayor_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub

    Private Sub con_frmCentrosMayor_Save_Click() Handles Me.Guardar
        bl.EliminaCuentasMayor(leCentros.EditValue)
        For i = 0 To gv.RowCount - 1
            If gv.GetRowCellValue(i, "Incluir") Then
                bl.InsertarCuentaMayor(leCentros.EditValue, gv.GetRowCellValue(i, "IdCuenta"), objMenu.User)
            End If
        Next
        MsgBox("Las cuentas han sido asociadas al centro de costo")
    End Sub
End Class
