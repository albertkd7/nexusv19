﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pre_frmPresupuestosCTA
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.sbNuevo = New DevExpress.XtraEditors.SimpleButton()
        Me.sbAsociarCuentas = New DevExpress.XtraEditors.SimpleButton()
        Me.sbEditar = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.gcPresupuestos = New DevExpress.XtraGrid.GridControl()
        Me.gvPresupuestos = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leSucursal = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.leDepartamento = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.leCentroCosto = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gcPresupuestos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPresupuestos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leDepartamento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCentroCosto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sbNuevo
        '
        Me.sbNuevo.Location = New System.Drawing.Point(12, 12)
        Me.sbNuevo.Name = "sbNuevo"
        Me.sbNuevo.Size = New System.Drawing.Size(109, 23)
        Me.sbNuevo.TabIndex = 10
        Me.sbNuevo.Text = "Nuevo"
        '
        'sbAsociarCuentas
        '
        Me.sbAsociarCuentas.Location = New System.Drawing.Point(261, 12)
        Me.sbAsociarCuentas.Name = "sbAsociarCuentas"
        Me.sbAsociarCuentas.Size = New System.Drawing.Size(125, 23)
        Me.sbAsociarCuentas.TabIndex = 12
        Me.sbAsociarCuentas.Text = "Detallar Presupuesto"
        '
        'sbEditar
        '
        Me.sbEditar.Location = New System.Drawing.Point(142, 12)
        Me.sbEditar.Name = "sbEditar"
        Me.sbEditar.Size = New System.Drawing.Size(100, 23)
        Me.sbEditar.TabIndex = 15
        Me.sbEditar.Text = "Editar"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(12, 45)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl2.TabIndex = 14
        Me.LabelControl2.Text = "Presupuestos:"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.sbNuevo)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.sbEditar)
        Me.PanelControl1.Controls.Add(Me.sbAsociarCuentas)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(872, 62)
        Me.PanelControl1.TabIndex = 16
        '
        'gcPresupuestos
        '
        Me.gcPresupuestos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcPresupuestos.Location = New System.Drawing.Point(0, 62)
        Me.gcPresupuestos.MainView = Me.gvPresupuestos
        Me.gcPresupuestos.Name = "gcPresupuestos"
        Me.gcPresupuestos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.leSucursal, Me.leDepartamento, Me.leCentroCosto})
        Me.gcPresupuestos.Size = New System.Drawing.Size(872, 445)
        Me.gcPresupuestos.TabIndex = 17
        Me.gcPresupuestos.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvPresupuestos})
        '
        'gvPresupuestos
        '
        Me.gvPresupuestos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn8, Me.GridColumn9, Me.GridColumn2, Me.GridColumn10})
        Me.gvPresupuestos.GridControl = Me.gcPresupuestos
        Me.gvPresupuestos.Name = "gvPresupuestos"
        Me.gvPresupuestos.OptionsBehavior.Editable = False
        Me.gvPresupuestos.OptionsBehavior.ReadOnly = True
        Me.gvPresupuestos.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdPresupuesto"
        Me.GridColumn1.FieldName = "IdPresupuesto"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Año"
        Me.GridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn8.FieldName = "Periodo"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 0
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Comentario"
        Me.GridColumn9.FieldName = "Comentario"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 1
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Creado Por"
        Me.GridColumn2.FieldName = "CreadoPor"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 3
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Fecha de creación"
        Me.GridColumn10.FieldName = "FechaHoraCreacion"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 2
        '
        'leSucursal
        '
        Me.leSucursal.AutoHeight = False
        Me.leSucursal.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Name = "leSucursal"
        '
        'leDepartamento
        '
        Me.leDepartamento.AutoHeight = False
        Me.leDepartamento.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leDepartamento.Name = "leDepartamento"
        '
        'leCentroCosto
        '
        Me.leCentroCosto.AutoHeight = False
        Me.leCentroCosto.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCentroCosto.Name = "leCentroCosto"
        '
        'pre_frmPresupuestosCTA
        '
        Me.ClientSize = New System.Drawing.Size(872, 532)
        Me.Controls.Add(Me.gcPresupuestos)
        Me.Controls.Add(Me.PanelControl1)
        Me.Modulo = "Contabilidad"
        Me.Name = "pre_frmPresupuestosCTA"
        Me.OptionId = "004002002"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Presupuestos por Cuenta Contable"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.gcPresupuestos, 0)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.gcPresupuestos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPresupuestos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leDepartamento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCentroCosto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents sbNuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbAsociarCuentas As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbEditar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gcPresupuestos As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvPresupuestos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursal As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents leDepartamento As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents leCentroCosto As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn

End Class
