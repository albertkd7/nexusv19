﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Views.Grid
Public Class con_frmCuentasPatrimonio

    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Dim bl1 As New TableBusiness(g_ConnectionString)
    Dim entDetalle As con_CuentasPatrimonio

    Private Sub con_frmCentrosMayor_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        objCombos.con_RubrosPatrimonio(leRubroPatrimonio, "")
        gc.DataSource = bl.ObtenerCuentasPatrimonio()
    End Sub

    Private Sub leCentros_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
        gc.DataSource = bl.ObtenerCuentasPatrimonio()
    End Sub

    Private Sub con_frmCentrosMayor_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub

    Private Sub con_frmRubrosResultado_Guardar() Handles Me.Guardar
        Dim vista As GridView = gc.FocusedView
        Dim entRubroDetalle As con_CuentasPatrimonio

        For i = 0 To gv.DataRowCount - 1
            entDetalle = New con_CuentasPatrimonio
            entDetalle.IdCuenta = vista.GetRowCellValue(i, "IdCuenta")
            entDetalle.IdRubro = vista.GetRowCellValue(i, "IdRubro")
            entRubroDetalle = bl1.con_CuentasPatrimonioSelectByPK(entDetalle.IdCuenta)
            entRubroDetalle.IdCuenta = SiEsNulo(entRubroDetalle.IdCuenta, Nothing)

            If entRubroDetalle.IdCuenta <> "" Then
                bl1.con_CuentasPatrimonioUpdate(entDetalle)
            Else
                bl1.con_CuentasPatrimonioInsert(entDetalle)
            End If

        Next
        MsgBox("Las modificaciones fueron almacenadas con éxito", MsgBoxStyle.Information)
        Me.Close()
    End Sub
End Class
