﻿Imports NexusELL.TableEntities
Imports DevExpress.XtraEditors

Public Class pre_frmValorPresupuesto

    Private _Valor As Decimal

    Public Property Valor() As Decimal
        Get
            Return _Valor
        End Get
        Set(ByVal value As Decimal)
            _Valor = value
        End Set
    End Property

    Private IdPresupuesto As Integer

    Private Mes As pre_DetallesPresupuesto.Meses

    Public Sub New(ByVal pIdPresupuesto As Integer, ByVal pMes As pre_DetallesPresupuesto.Meses)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        IdPresupuesto = pIdPresupuesto
        Mes = pMes

        If CargarGridValores(IdPresupuesto, Mes) Then
            DbMode = DbModeType.insert
        Else
            DbMode = DbModeType.update
        End If

    End Sub

    Public Function CargarGridValores(ByVal pIdPreCuenta As Integer, ByVal pMes As pre_DetallesPresupuesto.Meses) As Boolean
        Dim resultado = False

        Dim valores As DataTable = objTablas.pre_DetallesPresupuestoSelectByCuenta(pIdPreCuenta, pMes)

        If valores.Rows.Count = 0 Then
            resultado = True
        End If

        gcDetallePreCuenta.DataSource = valores
        gvDetallePreCuenta.BestFitColumns()

        Return resultado
    End Function

    Private Sub pre_frmValorPresupuesto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        teValor.Text = objTablas.pre_ObtenerTotalPresupuestoMes(Mes, IdPresupuesto)
        teValor.Focus()

    End Sub

    Private Sub sbCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCancelar.Click
        Close()
    End Sub

    Private Sub sbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGuardar.Click

        Dim nuevoValor As Decimal
        Dim detalle As pre_DetallesPresupuesto = New pre_DetallesPresupuesto()

        If Not Decimal.TryParse(teValor.Text, nuevoValor) Then
            MsgBox("El formato de la nueva cantidad para el presupuesto no es correcto.", MsgBoxStyle.Exclamation)
            teValor.Focus()
            Return
        End If

        If String.IsNullOrEmpty(meComentario.Text.Trim()) Then
            MsgBox("El campo del comentario no puede estar vacio ni contener solamente espacio en blanco.", MsgBoxStyle.Exclamation)
            meComentario.Focus()
            Return
        End If

        With detalle
            .IdPresupuestoFk = IdPresupuesto
            .Comentario = meComentario.Text.Trim()
            .CreadoPor = objMenu.User
            .FechaCreacion = DateTime.Now
            .Mes = Mes
        End With

        If DbMode = DbModeType.insert Then
            'Insercion completa
            detalle.Valor = nuevoValor
            Try
                objTablas.pre_DetallesPresupuestoInsert(detalle)
                CargarGridValores(IdPresupuesto, Mes)
                MsgBox("Nuevo valor ingresado con exito.", MsgBoxStyle.Information)
                DialogResult = DialogResult.OK
            Catch ex As Exception
                MsgBox("Error al ingresar el nuevo registro, error: " & ex.Message, MsgBoxStyle.Critical)
                DialogResult = DialogResult.Cancel
            End Try

        ElseIf DbMode = DbModeType.update Then
            'Insercion parcial - (totalAntiguo - valorNuevo)
            detalle.Valor = -(objTablas.pre_ObtenerTotalPresupuestoMes(Mes, IdPresupuesto) - nuevoValor)
            Try
                objTablas.pre_DetallesPresupuestoInsert(detalle) 'INSERTAMOS
                CargarGridValores(IdPresupuesto, Mes) 'ACTUALIZAMOS EL GRID
                MsgBox("Nuevo valor ingresado con exito.", MsgBoxStyle.Information)
                DialogResult = DialogResult.OK
            Catch ex As Exception
                MsgBox("Error al ingresar el nuevo registro, error: " & ex.Message, MsgBoxStyle.Critical)
                DialogResult = DialogResult.Cancel
            End Try

        End If

    End Sub

    Private Sub teValor_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles teValor.Enter
        CType(sender, TextEdit).SelectAll()
    End Sub

    Private Sub teValor_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles teValor.KeyPress

        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsPunctuation(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If

    End Sub
End Class
