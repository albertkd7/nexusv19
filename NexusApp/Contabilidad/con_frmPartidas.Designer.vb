﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmPartidas
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutConverter1 = New DevExpress.XtraLayout.Converter.LayoutConverter(Me.components)
        Me.xtcPartidas = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage()
        Me.gc2 = New DevExpress.XtraGrid.GridControl()
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIdCuenta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.teIdCuenta = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.txtReferencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtConcepto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDebe = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.teDebe = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcHaber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.teHaber = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcIdCentro = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.rileIdCentro = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.pcFooter = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombreCuenta = New DevExpress.XtraEditors.TextEdit()
        Me.txtDiferencia = New DevExpress.XtraEditors.TextEdit()
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.sbImportarCsv = New DevExpress.XtraEditors.SimpleButton()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.ceExcluir = New DevExpress.XtraEditors.CheckEdit()
        Me.sbAgregarCompra = New DevExpress.XtraEditors.SimpleButton()
        Me.teConceptoPartida = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.leTiposPartida = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdPartida = New DevExpress.XtraEditors.TextEdit()
        Me.sbImportar = New DevExpress.XtraEditors.SimpleButton()
        Me.sbCopiar = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.xtcPartidas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcPartidas.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdCuenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDebe, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teHaber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rileIdCentro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcFooter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcFooter.SuspendLayout()
        CType(Me.teNombreCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDiferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceExcluir.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teConceptoPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTiposPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcPartidas
        '
        Me.xtcPartidas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcPartidas.Location = New System.Drawing.Point(0, 0)
        Me.xtcPartidas.Name = "xtcPartidas"
        Me.xtcPartidas.SelectedTabPage = Me.xtpLista
        Me.xtcPartidas.Size = New System.Drawing.Size(1102, 386)
        Me.xtcPartidas.TabIndex = 4
        Me.xtcPartidas.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc2)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(1096, 358)
        Me.xtpLista.Text = "Consultas de Partidas"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(1096, 358)
        Me.gc2.TabIndex = 1
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn8, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdPartida"
        Me.GridColumn1.FieldName = "IdPartida"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 59
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Tipo Partida"
        Me.GridColumn2.FieldName = "IdTipo"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 68
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Numero"
        Me.GridColumn3.FieldName = "Numero"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 85
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Fecha"
        Me.GridColumn4.FieldName = "Fecha"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 81
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Sucursal"
        Me.GridColumn8.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn8.FieldName = "IdSucursal"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 4
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Concepto"
        Me.GridColumn5.FieldName = "Concepto"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 5
        Me.GridColumn5.Width = 440
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Creado Por"
        Me.GridColumn6.FieldName = "CreadoPor"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 6
        Me.GridColumn6.Width = 98
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Fecha Hora Creacion"
        Me.GridColumn7.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.GridColumn7.FieldName = "FechaHoraCreacion"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 7
        Me.GridColumn7.Width = 114
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemDateEdit1.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.gc)
        Me.xtpDatos.Controls.Add(Me.pcFooter)
        Me.xtpDatos.Controls.Add(Me.pcBotones)
        Me.xtpDatos.Controls.Add(Me.pcHeader)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(1096, 358)
        Me.xtpDatos.Text = "Partidas"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 93)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.teIdCuenta, Me.teDebe, Me.teHaber, Me.rileIdCentro})
        Me.gc.Size = New System.Drawing.Size(1055, 230)
        Me.gc.TabIndex = 4
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Appearance.TopNewRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.gv.Appearance.TopNewRow.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.gv.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Black
        Me.gv.Appearance.TopNewRow.Options.UseBackColor = True
        Me.gv.Appearance.TopNewRow.Options.UseForeColor = True
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIdCuenta, Me.txtReferencia, Me.txtConcepto, Me.gcDebe, Me.gcHaber, Me.gcIdCentro})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.NewItemRowText = "Click aquí para agregar nuevo registro. <Esc> para cancelar"
        Me.gv.OptionsNavigation.AutoFocusNewRow = True
        Me.gv.OptionsNavigation.EnterMoveNextColumn = True
        Me.gv.OptionsSelection.MultiSelect = True
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'colIdCuenta
        '
        Me.colIdCuenta.Caption = "Cód. de Cuenta"
        Me.colIdCuenta.ColumnEdit = Me.teIdCuenta
        Me.colIdCuenta.FieldName = "IdCuenta"
        Me.colIdCuenta.Name = "colIdCuenta"
        Me.colIdCuenta.ToolTip = "Código de Cuenta. <Enter> para consultar"
        Me.colIdCuenta.Visible = True
        Me.colIdCuenta.VisibleIndex = 0
        Me.colIdCuenta.Width = 89
        '
        'teIdCuenta
        '
        Me.teIdCuenta.AutoHeight = False
        Me.teIdCuenta.Name = "teIdCuenta"
        '
        'txtReferencia
        '
        Me.txtReferencia.Caption = "Referencia"
        Me.txtReferencia.FieldName = "Referencia"
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Visible = True
        Me.txtReferencia.VisibleIndex = 1
        Me.txtReferencia.Width = 63
        '
        'txtConcepto
        '
        Me.txtConcepto.Caption = "Concepto de la transacción"
        Me.txtConcepto.FieldName = "Concepto"
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.Visible = True
        Me.txtConcepto.VisibleIndex = 2
        Me.txtConcepto.Width = 360
        '
        'gcDebe
        '
        Me.gcDebe.Caption = "Debe"
        Me.gcDebe.ColumnEdit = Me.teDebe
        Me.gcDebe.FieldName = "Debe"
        Me.gcDebe.Name = "gcDebe"
        Me.gcDebe.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Debe", "{0:n2}")})
        Me.gcDebe.ToolTip = "Haber"
        Me.gcDebe.Visible = True
        Me.gcDebe.VisibleIndex = 3
        Me.gcDebe.Width = 122
        '
        'teDebe
        '
        Me.teDebe.AutoHeight = False
        Me.teDebe.Mask.EditMask = "n2"
        Me.teDebe.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teDebe.Mask.UseMaskAsDisplayFormat = True
        Me.teDebe.Name = "teDebe"
        '
        'gcHaber
        '
        Me.gcHaber.Caption = "Haber"
        Me.gcHaber.ColumnEdit = Me.teHaber
        Me.gcHaber.FieldName = "Haber"
        Me.gcHaber.Name = "gcHaber"
        Me.gcHaber.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Haber", "{0:n2}")})
        Me.gcHaber.Visible = True
        Me.gcHaber.VisibleIndex = 4
        Me.gcHaber.Width = 115
        '
        'teHaber
        '
        Me.teHaber.AutoHeight = False
        Me.teHaber.Mask.EditMask = "n2"
        Me.teHaber.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teHaber.Mask.UseMaskAsDisplayFormat = True
        Me.teHaber.Name = "teHaber"
        '
        'gcIdCentro
        '
        Me.gcIdCentro.Caption = "Centro de Costo"
        Me.gcIdCentro.ColumnEdit = Me.rileIdCentro
        Me.gcIdCentro.FieldName = "IdCentro"
        Me.gcIdCentro.Name = "gcIdCentro"
        Me.gcIdCentro.Visible = True
        Me.gcIdCentro.VisibleIndex = 5
        Me.gcIdCentro.Width = 87
        '
        'rileIdCentro
        '
        Me.rileIdCentro.AutoHeight = False
        Me.rileIdCentro.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rileIdCentro.Name = "rileIdCentro"
        '
        'pcFooter
        '
        Me.pcFooter.Controls.Add(Me.LabelControl2)
        Me.pcFooter.Controls.Add(Me.teNombreCuenta)
        Me.pcFooter.Controls.Add(Me.txtDiferencia)
        Me.pcFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pcFooter.Location = New System.Drawing.Point(0, 323)
        Me.pcFooter.Name = "pcFooter"
        Me.pcFooter.Size = New System.Drawing.Size(1055, 35)
        Me.pcFooter.TabIndex = 6
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(575, 10)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(121, 13)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Diferencia Debe y Haber:"
        '
        'teNombreCuenta
        '
        Me.teNombreCuenta.Location = New System.Drawing.Point(6, 4)
        Me.teNombreCuenta.Name = "teNombreCuenta"
        Me.teNombreCuenta.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold)
        Me.teNombreCuenta.Properties.Appearance.ForeColor = System.Drawing.Color.Red
        Me.teNombreCuenta.Properties.Appearance.Options.UseFont = True
        Me.teNombreCuenta.Properties.Appearance.Options.UseForeColor = True
        Me.teNombreCuenta.Properties.ReadOnly = True
        Me.teNombreCuenta.Size = New System.Drawing.Size(528, 24)
        Me.teNombreCuenta.TabIndex = 0
        '
        'txtDiferencia
        '
        Me.txtDiferencia.EditValue = 0
        Me.txtDiferencia.Location = New System.Drawing.Point(697, 6)
        Me.txtDiferencia.Name = "txtDiferencia"
        Me.txtDiferencia.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDiferencia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtDiferencia.Properties.Mask.EditMask = "n2"
        Me.txtDiferencia.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtDiferencia.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDiferencia.Properties.ReadOnly = True
        Me.txtDiferencia.Size = New System.Drawing.Size(120, 20)
        Me.txtDiferencia.TabIndex = 2
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdBorrar)
        Me.pcBotones.Controls.Add(Me.cmdUp)
        Me.pcBotones.Controls.Add(Me.cmdDown)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(1055, 93)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(41, 265)
        Me.pcBotones.TabIndex = 5
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(5, 81)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(28, 28)
        Me.cmdBorrar.TabIndex = 31
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(5, 11)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(28, 24)
        Me.cmdUp.TabIndex = 29
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(5, 37)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(28, 24)
        Me.cmdDown.TabIndex = 30
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.sbImportarCsv)
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.LabelControl7)
        Me.pcHeader.Controls.Add(Me.ceExcluir)
        Me.pcHeader.Controls.Add(Me.sbAgregarCompra)
        Me.pcHeader.Controls.Add(Me.teConceptoPartida)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Controls.Add(Me.LabelControl3)
        Me.pcHeader.Controls.Add(Me.teNumero)
        Me.pcHeader.Controls.Add(Me.LabelControl4)
        Me.pcHeader.Controls.Add(Me.deFecha)
        Me.pcHeader.Controls.Add(Me.LabelControl5)
        Me.pcHeader.Controls.Add(Me.leTiposPartida)
        Me.pcHeader.Controls.Add(Me.LabelControl6)
        Me.pcHeader.Controls.Add(Me.teIdPartida)
        Me.pcHeader.Controls.Add(Me.sbImportar)
        Me.pcHeader.Controls.Add(Me.sbCopiar)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(1096, 93)
        Me.pcHeader.TabIndex = 1
        '
        'sbImportarCsv
        '
        Me.sbImportarCsv.Location = New System.Drawing.Point(931, 66)
        Me.sbImportarCsv.Name = "sbImportarCsv"
        Me.sbImportarCsv.Size = New System.Drawing.Size(107, 26)
        Me.sbImportarCsv.TabIndex = 123
        Me.sbImportarCsv.Text = "Importar CSV"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(678, 5)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(253, 20)
        Me.leSucursal.TabIndex = 3
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(633, 9)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl7.TabIndex = 56
        Me.LabelControl7.Text = "Sucursal:"
        '
        'ceExcluir
        '
        Me.ceExcluir.Location = New System.Drawing.Point(676, 47)
        Me.ceExcluir.Name = "ceExcluir"
        Me.ceExcluir.Properties.Caption = "EXCLUIR ESTA PARTIDA DE CIERRE CONTABLE"
        Me.ceExcluir.Size = New System.Drawing.Size(255, 19)
        Me.ceExcluir.TabIndex = 55
        Me.ceExcluir.Visible = False
        '
        'sbAgregarCompra
        '
        Me.sbAgregarCompra.AllowFocus = False
        Me.sbAgregarCompra.Location = New System.Drawing.Point(794, 66)
        Me.sbAgregarCompra.Name = "sbAgregarCompra"
        Me.sbAgregarCompra.Size = New System.Drawing.Size(131, 26)
        Me.sbAgregarCompra.TabIndex = 7
        Me.sbAgregarCompra.Text = "Agregar Docto. de Compra"
        Me.sbAgregarCompra.Visible = False
        '
        'teConceptoPartida
        '
        Me.teConceptoPartida.EnterMoveNextControl = True
        Me.teConceptoPartida.Location = New System.Drawing.Point(90, 25)
        Me.teConceptoPartida.Name = "teConceptoPartida"
        Me.teConceptoPartida.Size = New System.Drawing.Size(500, 60)
        Me.teConceptoPartida.TabIndex = 4
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(39, 31)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl1.TabIndex = 46
        Me.LabelControl1.Text = "Concepto:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(193, 7)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl3.TabIndex = 50
        Me.LabelControl3.Text = "No. de Partida:"
        '
        'teNumero
        '
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(268, 3)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(73, 20)
        Me.teNumero.TabIndex = 1
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(400, 7)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(96, 13)
        Me.LabelControl4.TabIndex = 51
        Me.LabelControl4.Text = "Fecha de la Partida:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(498, 3)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(92, 20)
        Me.deFecha.TabIndex = 2
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(601, 31)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl5.TabIndex = 53
        Me.LabelControl5.Text = "Tipo de Partida:"
        '
        'leTiposPartida
        '
        Me.leTiposPartida.EnterMoveNextControl = True
        Me.leTiposPartida.Location = New System.Drawing.Point(678, 27)
        Me.leTiposPartida.Name = "leTiposPartida"
        Me.leTiposPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTiposPartida.Properties.NullText = ""
        Me.leTiposPartida.Size = New System.Drawing.Size(253, 20)
        Me.leTiposPartida.TabIndex = 5
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(8, 7)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(81, 13)
        Me.LabelControl6.TabIndex = 54
        Me.LabelControl6.Text = "Id. de la Partida:"
        '
        'teIdPartida
        '
        Me.teIdPartida.Location = New System.Drawing.Point(90, 3)
        Me.teIdPartida.Name = "teIdPartida"
        Me.teIdPartida.Properties.ReadOnly = True
        Me.teIdPartida.Size = New System.Drawing.Size(74, 20)
        Me.teIdPartida.TabIndex = 0
        '
        'sbImportar
        '
        Me.sbImportar.AllowFocus = False
        Me.sbImportar.Location = New System.Drawing.Point(596, 66)
        Me.sbImportar.Name = "sbImportar"
        Me.sbImportar.Size = New System.Drawing.Size(93, 26)
        Me.sbImportar.TabIndex = 5
        Me.sbImportar.Text = "Importar Partida"
        '
        'sbCopiar
        '
        Me.sbCopiar.AllowFocus = False
        Me.sbCopiar.Location = New System.Drawing.Point(695, 66)
        Me.sbCopiar.Name = "sbCopiar"
        Me.sbCopiar.Size = New System.Drawing.Size(93, 26)
        Me.sbCopiar.TabIndex = 6
        Me.sbCopiar.Text = "Copiar Partida..."
        '
        'con_frmPartidas
        '
        Me.ClientSize = New System.Drawing.Size(1102, 411)
        Me.Controls.Add(Me.xtcPartidas)
        Me.Modulo = "Contabilidad"
        Me.Name = "con_frmPartidas"
        Me.OptionId = "002001"
        Me.Text = "Partidas"
        Me.Controls.SetChildIndex(Me.xtcPartidas, 0)
        CType(Me.xtcPartidas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcPartidas.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdCuenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDebe, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teHaber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rileIdCentro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcFooter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcFooter.ResumeLayout(False)
        Me.pcFooter.PerformLayout()
        CType(Me.teNombreCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDiferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceExcluir.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teConceptoPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTiposPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LayoutConverter1 As DevExpress.XtraLayout.Converter.LayoutConverter
    Friend WithEvents xtcPartidas As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIdCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teIdCuenta As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtReferencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDebe As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teDebe As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcHaber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teHaber As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcIdCentro As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rileIdCentro As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents pcFooter As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombreCuenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDiferencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceExcluir As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents sbAgregarCompra As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teConceptoPartida As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTiposPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdPartida As DevExpress.XtraEditors.TextEdit
    Friend WithEvents sbImportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbCopiar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents sbImportarCsv As DevExpress.XtraEditors.SimpleButton


End Class
