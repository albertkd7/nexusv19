﻿Imports NexusBLL
Imports NexusELL
Public Class con_frmCentrosCosto
    
    Private Sub con_CentrosCosto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.con_CentrosCostoSelectAll
    End Sub

    Private Sub con_frmCentrosCosto_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el centro de costo?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Return
        End If

        Dim IdCentro As String = gv.GetFocusedRowCellValue(gv.Columns(0))
        objTablas.con_CentrosCostoDeleteByPK(IdCentro)
        gc.DataSource = objTablas.con_CentrosCostoSelectAll
    End Sub
    Private Sub con_frmCentrosCostoReport_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub

    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        Dim entCentro As New TableEntities.con_CentrosCosto
        Dim NumFila As Integer
        If e.RowHandle < 0 Then
            DbMode = DbModeType.Insert
            NumFila = gv.RowCount - 1
        Else
            DbMode = DbModeType.Update
            NumFila = e.RowHandle
        End If

        If Not AllowInsert Or Not AllowEdit Then
            MsgBox("No le está permitido ingresar o editar información", MsgBoxStyle.Exclamation, Me.Text)
            gc.DataSource = objTablas.con_CentrosCostoSelectAll
            Exit Sub
        End If

        With entCentro
            .IdCentro = gv.GetRowCellValue(NumFila, gv.Columns(0).FieldName)
            .Nombre = gv.GetRowCellValue(NumFila, gv.Columns(1).FieldName)
        End With
        If DbMode = DbModeType.Insert Then
            objTablas.con_CentrosCostoInsert(entCentro)
        Else
            objTablas.con_CentrosCostoUpdate(entCentro)
        End If
    End Sub

End Class
