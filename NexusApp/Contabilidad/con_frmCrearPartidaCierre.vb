﻿Imports NexusBLL
Public Class con_frmCrearPartidaCierre
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    
    Private Sub con_frmCrearCierre_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        deFecha.EditValue = CDate("31/12/" + CStr(Today.Year))
        objCombos.conTiposPartida(leTipoPartida)
        objCombos.conTiposCuentaCierre(leTipoCuenta, "-- TODAS LAS CUENTAS DE RESULTADO --")
        BeCtaContable1.beIdCuenta.EditValue = bl.ObtenerCuentaLiquidadora()
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        leSucursal.EditValue = piIdSucursalUsuario
        
    End Sub

    Private Sub sbAceptar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles sbAceptar.Click
        Dim res As Integer = bl.pro_CrearCierre(deFecha.DateTime.Year, leTipoPartida.EditValue, deFecha.EditValue, teConcepto.EditValue, objMenu.User, BeCtaContable1.beIdCuenta.EditValue, leTipoCuenta.EditValue, leSucursal.EditValue)
        If res = 1 Then
            MsgBox("El cierre se ha ejecutado con éxito", MsgBoxStyle.Information, "Nota")
        Else
            MsgBox("No se realizó ninguna partida de cierre", MsgBoxStyle.Critical, "Error")
        End If
    End Sub
End Class
