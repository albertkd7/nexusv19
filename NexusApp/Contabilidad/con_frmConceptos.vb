﻿Imports NexusELL.TableEntities
Imports NexusBLL.ContabilidadBLL
Public Class con_frmConceptos
    Dim blConta As New NexusBLL.ContabilidadBLL(g_ConnectionString)
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        Dim entConcepto As New con_ConceptosCaja
        Dim fila As Integer
        If e.RowHandle < 0 Then
            DbMode = DbModeType.insert
            fila = gv.RowCount - 1
        Else
            DbMode = DbModeType.update
            fila = e.RowHandle
        End If
        If Not AllowInsert Or Not AllowEdit Then
            MsgBox("No le está permitido ingresar o editar información", MsgBoxStyle.Exclamation, Me.Text)
            gc.DataSource = objTablas.con_ConceptosCajaSelectAll
            Exit Sub
        End If

        With entConcepto
            .IdConcepto = SiEsNulo(gv.GetRowCellValue(fila, gv.Columns("IdConcepto").FieldName), objFunciones.ObtenerUltimoId("con_ConceptosCaja", "IdConcepto") + 1)
            .Nombre = gv.GetRowCellValue(fila, gv.Columns("Nombre").FieldName)
            .IngresoEgreso = SiEsNulo(gv.GetRowCellValue(fila, gv.Columns("IngresoEgreso").FieldName), 0)

            .CuentaContable = SiEsNulo(gv.GetRowCellValue(fila, gv.Columns("CuentaContable").FieldName), "")

            .Orden = SiEsNulo(gv.GetRowCellValue(fila, gv.Columns("Orden").FieldName), 0)
        End With
        If DbMode = DbModeType.insert Then
            objTablas.con_ConceptosCajaInsert(entConcepto)
        Else
            objTablas.con_ConceptosCajaUpdate(entConcepto)
        End If
        gc.DataSource = objTablas.con_ConceptosCajaSelectAll
    End Sub

    Private Sub caj_frmConceptos_Eliminar() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar este registro?", MsgBoxStyle.YesNo, "Nexus") = MsgBoxResult.No Then
            Return
        End If

        Dim IdConcepto As String = gv.GetFocusedRowCellValue(gv.Columns(0))
        objTablas.con_ConceptosCajaDeleteByPK(IdConcepto)
        gc.DataSource = objTablas.con_ConceptosCajaSelectAll
    End Sub

    Private Sub caj_frmConceptos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LlenarCombobox()
        gc.DataSource = objTablas.con_ConceptosCajaSelectAll
    End Sub
    Sub LlenarCombobox()
        Dim dt As New DataTable
        Dim dr As DataRow
        dt.Columns.Add(New DataColumn("IngresoEgreso", System.Type.GetType("System.Int32")))
        dt.Columns.Add(New DataColumn("Nombre", System.Type.GetType("System.String")))
        dr = dt.NewRow
        dr("IngresoEgreso") = 0
        dr("Nombre") = "Egreso"
        dt.Rows.Add(dr)
        dr = dt.NewRow
        dr("IngresoEgreso") = 1
        dr("Nombre") = "Ingreso"
        dt.Rows.Add(dr)
        reIngEgr.DataSource = dt
        reIngEgr.DisplayMember = "Nombre"
        reIngEgr.ValueMember = "IngresoEgreso"

        Dim dtc As New DataTable

        dtc = blConta.GetCuentasContables
        Dim drc As DataRow
        drc = dtc.NewRow()
        drc("IdCuenta") = "..."
        drc("Nombre") = "[NADA]"
        dtc.Rows.Add(drc)

        reIdCuenta.DataSource = dtc
        reIdCuenta.DisplayMember = "Nombre"
        reIdCuenta.ValueMember = "IdCuenta"
    End Sub
    Private Sub caj_frmConceptos_Reporte() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
End Class
