﻿Imports NexusBLL
Public Class con_frmRevierteMes
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Private Sub btnRevertir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRevertir.Click
        If MsgBox("Está seguro(a) de revertir este cierre?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Return
        End If

        If bl.pro_RevierteCierreMensual(seEjercicio.EditValue, meMes.EditValue) Then
            MsgBox("El cierre ha sido revertido con éxito")
        Else
            MsgBox("No fue posible revertir el cierre")
        End If

        Me.Close()
    End Sub

    Private Sub frmCierreMes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        bl.ObtienePeriodoContable(Mes, Ejercicio)
        seEjercicio.Value = Ejercicio
        meMes.Month = Mes
    End Sub
End Class