<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class con_frmObtenerPartidas
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(con_frmObtenerPartidas))
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.gcFP = New DevExpress.XtraGrid.GridControl()
        Me.gvFP = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdTipo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFecha1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdCuenta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDebe = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcHaber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdCuentaAuxiliar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcBanco = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNumeroCheque = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdCuentaCompleta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdPartida = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.riteFacturar = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.sbImportar = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.gcFP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvFP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteFacturar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(776, 380)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(95, 23)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Aceptar"
        '
        'sbCancel
        '
        Me.sbCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.sbCancel.Appearance.Options.UseFont = True
        Me.sbCancel.Location = New System.Drawing.Point(877, 380)
        Me.sbCancel.Name = "sbCancel"
        Me.sbCancel.Size = New System.Drawing.Size(95, 23)
        Me.sbCancel.TabIndex = 2
        Me.sbCancel.Text = "Cancelar"
        '
        'gcFP
        '
        Me.gcFP.Location = New System.Drawing.Point(12, 2)
        Me.gcFP.MainView = Me.gvFP
        Me.gcFP.Name = "gcFP"
        Me.gcFP.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar, Me.riteFacturar})
        Me.gcFP.Size = New System.Drawing.Size(1009, 372)
        Me.gcFP.TabIndex = 6
        Me.gcFP.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvFP})
        '
        'gvFP
        '
        Me.gvFP.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdTipo, Me.gcNumero, Me.gcFecha1, Me.gcIdCuenta, Me.gcDebe, Me.gcHaber, Me.gcIdCuentaAuxiliar, Me.gcBanco, Me.gcNumeroCheque, Me.gcIdCuentaCompleta, Me.gcFecha, Me.gcIdPartida})
        Me.gvFP.GridControl = Me.gcFP
        Me.gvFP.Name = "gvFP"
        Me.gvFP.OptionsBehavior.Editable = False
        Me.gvFP.OptionsView.ShowFooter = True
        Me.gvFP.OptionsView.ShowGroupPanel = False
        '
        'gcIdTipo
        '
        Me.gcIdTipo.Caption = "Id Tipo"
        Me.gcIdTipo.FieldName = "IdTipo"
        Me.gcIdTipo.Name = "gcIdTipo"
        Me.gcIdTipo.OptionsColumn.AllowEdit = False
        Me.gcIdTipo.OptionsColumn.AllowFocus = False
        Me.gcIdTipo.OptionsColumn.AllowMove = False
        Me.gcIdTipo.Visible = True
        Me.gcIdTipo.VisibleIndex = 0
        Me.gcIdTipo.Width = 64
        '
        'gcNumero
        '
        Me.gcNumero.Caption = "N�mero"
        Me.gcNumero.FieldName = "Numero"
        Me.gcNumero.Name = "gcNumero"
        Me.gcNumero.OptionsColumn.AllowEdit = False
        Me.gcNumero.OptionsColumn.AllowFocus = False
        Me.gcNumero.Visible = True
        Me.gcNumero.VisibleIndex = 1
        Me.gcNumero.Width = 126
        '
        'gcFecha1
        '
        Me.gcFecha1.Caption = "Fecha1"
        Me.gcFecha1.FieldName = "Fecha1"
        Me.gcFecha1.Name = "gcFecha1"
        '
        'gcIdCuenta
        '
        Me.gcIdCuenta.Caption = "Id Cuenta"
        Me.gcIdCuenta.FieldName = "IdCuenta"
        Me.gcIdCuenta.Name = "gcIdCuenta"
        Me.gcIdCuenta.OptionsColumn.AllowEdit = False
        Me.gcIdCuenta.Visible = True
        Me.gcIdCuenta.VisibleIndex = 2
        Me.gcIdCuenta.Width = 267
        '
        'gcDebe
        '
        Me.gcDebe.Caption = "Debe"
        Me.gcDebe.FieldName = "Debe"
        Me.gcDebe.Name = "gcDebe"
        Me.gcDebe.Visible = True
        Me.gcDebe.VisibleIndex = 3
        Me.gcDebe.Width = 136
        '
        'gcHaber
        '
        Me.gcHaber.Caption = "Haber"
        Me.gcHaber.FieldName = "Haber"
        Me.gcHaber.Name = "gcHaber"
        Me.gcHaber.Visible = True
        Me.gcHaber.VisibleIndex = 4
        Me.gcHaber.Width = 136
        '
        'gcIdCuentaAuxiliar
        '
        Me.gcIdCuentaAuxiliar.Caption = "Id Cuenta Auxiliar"
        Me.gcIdCuentaAuxiliar.FieldName = "IdCuentaAux"
        Me.gcIdCuentaAuxiliar.Name = "gcIdCuentaAuxiliar"
        Me.gcIdCuentaAuxiliar.OptionsColumn.AllowEdit = False
        Me.gcIdCuentaAuxiliar.Width = 124
        '
        'gcBanco
        '
        Me.gcBanco.Caption = "Banco"
        Me.gcBanco.FieldName = "Banco"
        Me.gcBanco.Name = "gcBanco"
        Me.gcBanco.Width = 101
        '
        'gcNumeroCheque
        '
        Me.gcNumeroCheque.Caption = "N�mero Cheque"
        Me.gcNumeroCheque.FieldName = "NumCheque"
        Me.gcNumeroCheque.Name = "gcNumeroCheque"
        Me.gcNumeroCheque.OptionsColumn.AllowEdit = False
        Me.gcNumeroCheque.Width = 202
        '
        'gcIdCuentaCompleta
        '
        Me.gcIdCuentaCompleta.Caption = "Cuenta Completa"
        Me.gcIdCuentaCompleta.FieldName = "IdCuentaCompleta"
        Me.gcIdCuentaCompleta.Name = "gcIdCuentaCompleta"
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha"
        Me.gcFecha.FieldName = "Fecha"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.OptionsColumn.AllowEdit = False
        Me.gcFecha.OptionsColumn.AllowFocus = False
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 5
        Me.gcFecha.Width = 126
        '
        'gcIdPartida
        '
        Me.gcIdPartida.Caption = "IdPartida"
        Me.gcIdPartida.FieldName = "IdPartida"
        Me.gcIdPartida.Name = "gcIdPartida"
        Me.gcIdPartida.Width = 136
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'riteFacturar
        '
        Me.riteFacturar.AutoHeight = False
        Me.riteFacturar.EditFormat.FormatString = "n2"
        Me.riteFacturar.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteFacturar.Mask.EditMask = "n2"
        Me.riteFacturar.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteFacturar.Mask.UseMaskAsDisplayFormat = True
        Me.riteFacturar.Name = "riteFacturar"
        '
        'sbImportar
        '
        Me.sbImportar.AllowFocus = False
        Me.sbImportar.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.sbImportar.Appearance.Options.UseFont = True
        Me.sbImportar.Location = New System.Drawing.Point(667, 380)
        Me.sbImportar.Name = "sbImportar"
        Me.sbImportar.Size = New System.Drawing.Size(103, 23)
        Me.sbImportar.TabIndex = 7
        Me.sbImportar.Text = "Importar CSV"
        '
        'con_frmObtenerPartidas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1011, 415)
        Me.ControlBox = False
        Me.Controls.Add(Me.sbImportar)
        Me.Controls.Add(Me.gcFP)
        Me.Controls.Add(Me.sbCancel)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "con_frmObtenerPartidas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Importaci�n de Partidas"
        CType(Me.gcFP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvFP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteFacturar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcFP As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvFP As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents riteFacturar As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcBanco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNumeroCheque As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdCuentaAuxiliar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdPartida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDebe As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcHaber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdCuentaCompleta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents sbImportar As DevExpress.XtraEditors.SimpleButton
End Class
