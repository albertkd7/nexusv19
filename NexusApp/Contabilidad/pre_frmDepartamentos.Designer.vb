﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pre_frmDepartamentos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gcDepartamentos = New DevExpress.XtraGrid.GridControl
        Me.gvDepartamentos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcIdDepartamentoSucursalPk = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcDepartamento = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcEncargado = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcIdSucursalFk = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcSucursal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.leSucursal = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.dbNuevo = New DevExpress.XtraEditors.SimpleButton
        Me.sbEditar = New DevExpress.XtraEditors.SimpleButton
        CType(Me.gcDepartamentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvDepartamentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcDepartamentos
        '
        Me.gcDepartamentos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gcDepartamentos.Location = New System.Drawing.Point(13, 49)
        Me.gcDepartamentos.MainView = Me.gvDepartamentos
        Me.gcDepartamentos.Name = "gcDepartamentos"
        Me.gcDepartamentos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.leSucursal})
        Me.gcDepartamentos.Size = New System.Drawing.Size(713, 209)
        Me.gcDepartamentos.TabIndex = 5
        Me.gcDepartamentos.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvDepartamentos})
        '
        'gvDepartamentos
        '
        Me.gvDepartamentos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdDepartamentoSucursalPk, Me.gcDepartamento, Me.gcDescripcion, Me.gcEncargado, Me.gcIdSucursalFk, Me.gcSucursal})
        Me.gvDepartamentos.GridControl = Me.gcDepartamentos
        Me.gvDepartamentos.Name = "gvDepartamentos"
        Me.gvDepartamentos.OptionsBehavior.Editable = False
        Me.gvDepartamentos.OptionsBehavior.ReadOnly = True
        '
        'gcIdDepartamentoSucursalPk
        '
        Me.gcIdDepartamentoSucursalPk.Caption = "IdDepartamentoSucursalPk"
        Me.gcIdDepartamentoSucursalPk.FieldName = "IdDepartamentoSucursalPk"
        Me.gcIdDepartamentoSucursalPk.Name = "gcIdDepartamentoSucursalPk"
        '
        'gcDepartamento
        '
        Me.gcDepartamento.Caption = "Departamento"
        Me.gcDepartamento.FieldName = "Nombre"
        Me.gcDepartamento.Name = "gcDepartamento"
        Me.gcDepartamento.Visible = True
        Me.gcDepartamento.VisibleIndex = 0
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Descripcion"
        Me.gcDescripcion.FieldName = "Descripcion"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 1
        '
        'gcEncargado
        '
        Me.gcEncargado.Caption = "Encargado"
        Me.gcEncargado.FieldName = "Encargado"
        Me.gcEncargado.Name = "gcEncargado"
        Me.gcEncargado.Visible = True
        Me.gcEncargado.VisibleIndex = 2
        '
        'gcIdSucursalFk
        '
        Me.gcIdSucursalFk.Caption = "IdSucursalFk"
        Me.gcIdSucursalFk.FieldName = "IdSucursalFk"
        Me.gcIdSucursalFk.Name = "gcIdSucursalFk"
        '
        'gcSucursal
        '
        Me.gcSucursal.Caption = "Sucursal"
        Me.gcSucursal.ColumnEdit = Me.leSucursal
        Me.gcSucursal.FieldName = "IdSucursalFk"
        Me.gcSucursal.Name = "gcSucursal"
        Me.gcSucursal.Visible = True
        Me.gcSucursal.VisibleIndex = 3
        '
        'leSucursal
        '
        Me.leSucursal.AutoHeight = False
        Me.leSucursal.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Name = "leSucursal"
        '
        'dbNuevo
        '
        Me.dbNuevo.Location = New System.Drawing.Point(13, 14)
        Me.dbNuevo.Name = "dbNuevo"
        Me.dbNuevo.Size = New System.Drawing.Size(109, 23)
        Me.dbNuevo.TabIndex = 6
        Me.dbNuevo.Text = "Nuevo"
        '
        'sbEditar
        '
        Me.sbEditar.Location = New System.Drawing.Point(151, 14)
        Me.sbEditar.Name = "sbEditar"
        Me.sbEditar.Size = New System.Drawing.Size(100, 23)
        Me.sbEditar.TabIndex = 7
        Me.sbEditar.Text = "Editar"
        '
        'pre_frmDepartamentos
        '
        Me.ClientSize = New System.Drawing.Size(738, 306)
        Me.Controls.Add(Me.sbEditar)
        Me.Controls.Add(Me.dbNuevo)
        Me.Controls.Add(Me.gcDepartamentos)
        Me.Modulo = "Contabilidad"
        Me.Name = "pre_frmDepartamentos"
        Me.OptionId = "004001001"
        Me.Text = "Departamentos"
        Me.TipoFormulario = 2
        Me.Controls.SetChildIndex(Me.gcDepartamentos, 0)
        Me.Controls.SetChildIndex(Me.dbNuevo, 0)
        Me.Controls.SetChildIndex(Me.sbEditar, 0)
        CType(Me.gcDepartamentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvDepartamentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gcDepartamentos As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvDepartamentos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdDepartamentoSucursalPk As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDepartamento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcEncargado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdSucursalFk As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dbNuevo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbEditar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents leSucursal As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit

End Class
