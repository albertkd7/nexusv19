﻿Imports NexusBLL


Public Class con_frmCierreMes
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Dim FechaMin As Date = Today.Date
    Private Sub beCerrar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles beCerrar.Click

        'If rgTipo.EditValue = 0 Then
        'Dim ds As DataSet = bl.pro_VerificaIntegridadCierreMensual(seEjercicio.Value, meMes.Month)
        'If ds.Tables(0).Rows.Count > 0 Or ds.Tables(1).Rows.Count > 0 Then

        '    Dim rpt As New con_rptCuadraturaCierre()
        '    rpt.DataSource = ds.Tables(0)
        '    rpt.DataMember = ""

        '    rpt.XrSubreport1.ReportSource.DataSource = ds.Tables(1)
        '    rpt.XrSubreport1.ReportSource.DataMember = ""

        '    rpt.xrlEmpresa.Text = gsNombre_Empresa
        '    rpt.xrlTitulo.Text = "INTEGRIDAD DE SALDOS CONTABLES Y MODULOS DEL SISTEMA"
        '    rpt.ShowPreviewDialog()
        'End If

        Dim CountPartidasActuales As Integer = bl.CountPartidasCierre()

        If meMes.Enabled = True And seEjercicio.Enabled = True Then
            If (meMes.Month <> FechaMin.Month Or seEjercicio.Value <> FechaMin.Year) And CountPartidasActuales > 0 Then
                MsgBox("El periodo seleccionado para inicio de operaciones debe corresponder a la fecha minima en registros de partidas contables." & Chr(13) & "Fecha Minina Actual: " & FechaMin.ToString("dd MMMM yyyy").ToUpper(), MsgBoxStyle.Information, "ERROR")
                Exit Sub
            End If
        End If

        If MsgBox("¿Está seguro(a) de realizar el cierre definitivo?" & Chr(13) & "YA NO PODRÁ EDITAR NINGUN DATO", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Return
        End If

        If bl.pro_CierreMensual(seEjercicio.Value, meMes.Month, objMenu.User) Then
            MsgBox("Cierre realizado con éxito", MsgBoxStyle.Information, "Nota")
        Else
            MsgBox("No fue posible realizar el cierre", MsgBoxStyle.Information, "Nota")
        End If

        Close()
    End Sub

    Private Sub frmCierreMes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        bl.ObtienePeriodoContable(Mes, Ejercicio, FechaMin)

        If Mes <= 0 And Ejercicio <= 0 Then
            meMes.Month = IIf(FechaMin = Nothing, Today.Month, FechaMin.Month)
            meMes.Enabled = True
            seEjercicio.Value = IIf(FechaMin = Nothing, Today.Year, FechaMin.Year)
            seEjercicio.Enabled = True
            lbMsg.Visible = True
        ElseIf Mes = 12 Then
            meMes.Month = 1
            seEjercicio.EditValue = Ejercicio + 1
        Else
            meMes.Month = Mes + 1
            seEjercicio.Value = Ejercicio
        End If

    End Sub
End Class