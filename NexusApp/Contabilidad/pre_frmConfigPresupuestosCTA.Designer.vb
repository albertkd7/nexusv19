﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pre_frmConfigPresupuestosCTA
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.seAnio = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.meComentario = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.sbImportar = New DevExpress.XtraEditors.SimpleButton()
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.gcCuentasContables = New DevExpress.XtraGrid.GridControl()
        Me.gvCuentasContables = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCodigo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCuenta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteMes = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meComentario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gcCuentasContables, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvCuentasContables, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteMes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'seAnio
        '
        Me.seAnio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seAnio.Location = New System.Drawing.Point(90, 3)
        Me.seAnio.Name = "seAnio"
        Me.seAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seAnio.Properties.IsFloatValue = False
        Me.seAnio.Properties.Mask.EditMask = "N00"
        Me.seAnio.Properties.ReadOnly = True
        Me.seAnio.Size = New System.Drawing.Size(109, 20)
        Me.seAnio.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(9, 6)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl3.TabIndex = 13
        Me.LabelControl3.Text = "Año / Ejercicio:"
        '
        'meComentario
        '
        Me.meComentario.Location = New System.Drawing.Point(90, 26)
        Me.meComentario.Name = "meComentario"
        Me.meComentario.Properties.ReadOnly = True
        Me.meComentario.Size = New System.Drawing.Size(552, 43)
        Me.meComentario.TabIndex = 4
        '
        'LabelControl42
        '
        Me.LabelControl42.Location = New System.Drawing.Point(22, 29)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl42.TabIndex = 81
        Me.LabelControl42.Text = "Comentario:"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.sbImportar)
        Me.PanelControl1.Controls.Add(Me.sbGuardar)
        Me.PanelControl1.Controls.Add(Me.meComentario)
        Me.PanelControl1.Controls.Add(Me.LabelControl42)
        Me.PanelControl1.Controls.Add(Me.seAnio)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1065, 73)
        Me.PanelControl1.TabIndex = 82
        '
        'sbImportar
        '
        '  Me.sbImportar.Image = Global.Nexus.My.Resources.Resources.excel
        Me.sbImportar.Location = New System.Drawing.Point(665, 20)
        Me.sbImportar.Name = "sbImportar"
        Me.sbImportar.Size = New System.Drawing.Size(172, 46)
        Me.sbImportar.TabIndex = 83
        Me.sbImportar.Text = "Importar EXCEL"
        '
        'sbGuardar
        '
        Me.sbGuardar.Image = Global.Nexus.My.Resources.Resources.Save
        Me.sbGuardar.Location = New System.Drawing.Point(843, 20)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(139, 46)
        Me.sbGuardar.TabIndex = 82
        Me.sbGuardar.Text = "Guardar Cambios"
        '
        'gcCuentasContables
        '
        Me.gcCuentasContables.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcCuentasContables.Location = New System.Drawing.Point(0, 73)
        Me.gcCuentasContables.MainView = Me.gvCuentasContables
        Me.gcCuentasContables.Name = "gcCuentasContables"
        Me.gcCuentasContables.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteMes})
        Me.gcCuentasContables.Size = New System.Drawing.Size(1065, 510)
        Me.gcCuentasContables.TabIndex = 83
        Me.gcCuentasContables.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvCuentasContables})
        '
        'gvCuentasContables
        '
        Me.gvCuentasContables.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcId, Me.gcCodigo, Me.gcCuenta, Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12})
        Me.gvCuentasContables.GridControl = Me.gcCuentasContables
        Me.gvCuentasContables.Name = "gvCuentasContables"
        Me.gvCuentasContables.OptionsView.ShowAutoFilterRow = True
        Me.gvCuentasContables.OptionsView.ShowGroupPanel = False
        '
        'gcId
        '
        Me.gcId.Caption = "IdPresupuestoCuenta"
        Me.gcId.FieldName = "IdPresupuesto"
        Me.gcId.Name = "gcId"
        '
        'gcCodigo
        '
        Me.gcCodigo.Caption = "Codigo"
        Me.gcCodigo.FieldName = "IdCuenta"
        Me.gcCodigo.Name = "gcCodigo"
        Me.gcCodigo.OptionsColumn.AllowEdit = False
        Me.gcCodigo.Visible = True
        Me.gcCodigo.VisibleIndex = 0
        Me.gcCodigo.Width = 110
        '
        'gcCuenta
        '
        Me.gcCuenta.Caption = "Cuenta contable"
        Me.gcCuenta.FieldName = "Nombre"
        Me.gcCuenta.Name = "gcCuenta"
        Me.gcCuenta.OptionsColumn.AllowEdit = False
        Me.gcCuenta.Visible = True
        Me.gcCuenta.VisibleIndex = 1
        Me.gcCuenta.Width = 713
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Enero"
        Me.GridColumn1.ColumnEdit = Me.riteMes
        Me.GridColumn1.FieldName = "Mes01"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 2
        '
        'riteMes
        '
        Me.riteMes.AutoHeight = False
        Me.riteMes.EditFormat.FormatString = "n2"
        Me.riteMes.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteMes.Mask.EditMask = "n2"
        Me.riteMes.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteMes.Mask.UseMaskAsDisplayFormat = True
        Me.riteMes.Name = "riteMes"
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Febrero"
        Me.GridColumn2.ColumnEdit = Me.riteMes
        Me.GridColumn2.FieldName = "Mes02"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 3
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Marzo"
        Me.GridColumn3.ColumnEdit = Me.riteMes
        Me.GridColumn3.FieldName = "Mes03"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 4
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Abril"
        Me.GridColumn4.ColumnEdit = Me.riteMes
        Me.GridColumn4.FieldName = "Mes04"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 5
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Mayo"
        Me.GridColumn5.ColumnEdit = Me.riteMes
        Me.GridColumn5.FieldName = "Mes05"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 6
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Junio"
        Me.GridColumn6.ColumnEdit = Me.riteMes
        Me.GridColumn6.FieldName = "Mes06"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 7
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Julio"
        Me.GridColumn7.ColumnEdit = Me.riteMes
        Me.GridColumn7.FieldName = "Mes07"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 8
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Agosto"
        Me.GridColumn8.ColumnEdit = Me.riteMes
        Me.GridColumn8.FieldName = "Mes08"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 9
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Septiembre"
        Me.GridColumn9.ColumnEdit = Me.riteMes
        Me.GridColumn9.FieldName = "Mes09"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 10
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Octubre"
        Me.GridColumn10.ColumnEdit = Me.riteMes
        Me.GridColumn10.FieldName = "Mes10"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 11
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Noviembre"
        Me.GridColumn11.ColumnEdit = Me.riteMes
        Me.GridColumn11.FieldName = "Mes11"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 12
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Diciembre"
        Me.GridColumn12.ColumnEdit = Me.riteMes
        Me.GridColumn12.FieldName = "Mes12"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 13
        '
        'pre_frmConfigPresupuestosCTA
        '
        Me.ClientSize = New System.Drawing.Size(1065, 608)
        Me.Controls.Add(Me.gcCuentasContables)
        Me.Controls.Add(Me.PanelControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Modulo = "Presupuestos"
        Me.Name = "pre_frmConfigPresupuestosCTA"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Configuración presupuesto"
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.gcCuentasContables, 0)
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meComentario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.gcCuentasContables, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvCuentasContables, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteMes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents seAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meComentario As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gcCuentasContables As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvCuentasContables As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCodigo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteMes As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbImportar As DevExpress.XtraEditors.SimpleButton

End Class
