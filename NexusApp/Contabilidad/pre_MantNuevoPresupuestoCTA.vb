﻿Imports NexusELL.TableEntities
Imports NexusBLL

Public Class pre_MantNuevoPresupuestoCTA
    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private IdPresupuesto As Integer

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        DbMode = DbModeType.insert

    End Sub

    Public Sub New(ByVal pIdPresupuesto As Integer)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        IdPresupuesto = pIdPresupuesto
        DbMode = DbModeType.update

    End Sub

    Private Sub pre_MantNuevoPresupuesto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        seAnio.Value = DateTime.Now.Year

        Select Case DbMode
            Case DbModeType.insert
                '
            Case DbModeType.update
                '
                Dim pre As con_Presupuestos = objTablas.con_PresupuestosSelectByPK(IdPresupuesto)
                seAnio.Value = pre.Periodo
                meComentario.Text = pre.Comentario

        End Select

    End Sub


    Private Sub sbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGuardar.Click



        If bl.pre_PresupuestosSelectRepeated(seAnio.Value) > 0 Then
            If DbMode = DbModeType.insert Then
                MsgBox("Ya existe un presupuesto con estos datos.", MsgBoxStyle.Exclamation)
                Return
            End If
        End If

        Dim entidad As New con_Presupuestos

        With entidad
            .Periodo = Convert.ToInt32(seAnio.Value)
            .Comentario = meComentario.Text
            .FechaHoraCreacion = DateTime.Now
            .CreadoPor = objMenu.User
        End With

        Select Case DbMode
            Case DbModeType.insert
                '
                Try
                    entidad.IdPresupuesto = SiEsNulo(objFunciones.ObtenerUltimoId("con_Presupuestos", "IdPresupuesto"), 0) + 1
                    objTablas.con_PresupuestosInsert(entidad)
                    MsgBox("El registro a sido insertado con exito.", MsgBoxStyle.Information)
                    DialogResult = DialogResult.OK
                Catch ex As Exception
                    MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
                    DialogResult = DialogResult.Cancel
                End Try
            Case DbModeType.update
                '
                Try
                    entidad.IdPresupuesto = IdPresupuesto
                    objTablas.con_PresupuestosUpdate(entidad)
                    MsgBox("El registro a sido actualizado con exito.", MsgBoxStyle.Information)
                    DialogResult = DialogResult.OK
                Catch ex As Exception
                    MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
                    DialogResult = DialogResult.Cancel
                End Try
        End Select

    End Sub

    Private Sub sbCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCancelar.Click
        Close()
    End Sub

End Class
