IF OBJECT_ID ('dbo.fac_spGenerarFacturaMenor200ICV') IS NOT NULL
	DROP PROCEDURE dbo.fac_spGenerarFacturaMenor200ICV
GO

CREATE procedure fac_spGenerarFacturaMenor200ICV
(@Desde datetime, @Hasta datetime)
as

SELECT Ano= convert(char(4),datepart(yy,Fecha))
,Mes= convert(char(2),right('0'+convert(varchar(2),datepart(mm,Fecha)),2))
,Dia= right('0'+convert(varchar(2),datepart(dd,Fecha)),2)
,Numero = rtrim(convert(varchar(8),Numero))
,Valor= convert(int, TotalComprobante * 100)
,IVA= convert(int, TotalIva * 100)
into #tmp
FROM fac_Ventas
WHERE Fecha > = @Desde and Fecha < dateadd(dd,1,@Hasta) AND IdTipoComprobante = 6 AND TotalComprobante < 200


select Ano+Mes+Dia+right('00000000'+convert(varchar(8),Numero),8)
+right('00000000000'+convert(varchar(11),Valor),11)
+right('00000000000'+convert(varchar(11),IVA),11)
+Ano
from #tmp order by Ano,Mes,Dia
drop table #tmp

--fac_spGenerarFacturaMenor200ICV '20120101','20120827'


GO

