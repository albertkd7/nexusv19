﻿Imports DevExpress.Charts.Native
Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraEditors
Imports System.Data.SqlClient
Imports System.IO

Public Class pre_frmConfigPresupuestosCTA
    Dim bl As New ContabilidadBLL(g_ConnectionString)
    Private IdPresupuesto As Integer
    Private ListaCuentas As New List(Of con_Cuentas)
    Private DetallesPresupuesto As New List(Of con_PresupuestosDetalle)

#Region "Contructores"
    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        DbMode = DbModeType.insert

    End Sub

    Public Sub New(ByVal pIdPresupuesto As Integer)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        DbMode = DbModeType.update
        IdPresupuesto = pIdPresupuesto

    End Sub
#End Region

    Private Sub CargarGridCuentas()
        gcCuentasContables.DataSource = Nothing
        gcCuentasContables.DataSource = bl.pre_ObtenerDetallePresupuesto(IdPresupuesto)
        gvCuentasContables.RefreshData()
        gvCuentasContables.BestFitColumns()
    End Sub

    Private Sub LimpiaGrid()
        gcCuentasContables.DataSource = Nothing
        gcCuentasContables.DataSource = bl.pre_ObtenerDetallePresupuesto(-1)
        gvCuentasContables.RefreshData()
        gvCuentasContables.BestFitColumns()
    End Sub

    Private Sub pre_frmConfigPresupuestos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Select Case DbMode
            Case DbModeType.insert
                '
            Case DbModeType.update
                '
                Dim pre As con_Presupuestos = objTablas.con_PresupuestosSelectByPK(IdPresupuesto)

                seAnio.Value = pre.Periodo
                meComentario.Text = pre.Comentario

                CargarGridCuentas()
        End Select
    End Sub

    Private Sub sbGuardar_Click(sender As Object, e As EventArgs) Handles sbGuardar.Click

        If gvCuentasContables.DataRowCount <= 0 Then
            MsgBox("No hay registros para guardar el presupuesto", MsgBoxStyle.Critical)
            Exit Sub
        End If
        Dim DUMMY As String = ""

        DetallesPresupuesto = New List(Of con_PresupuestosDetalle)
        For i = 0 To gvCuentasContables.DataRowCount - 1
            Dim entDetalle As New con_PresupuestosDetalle
            With entDetalle
                .IdPresupuesto = IdPresupuesto
                .IdCuenta = gvCuentasContables.GetRowCellValue(i, "IdCuenta")
                Dim entcta As con_Cuentas = objTablas.con_CuentasSelectByPK(.IdCuenta)
                If SiEsNulo(entcta.IdCuenta, "") = "" Then
                    MsgBox("LA CUENTA NO EXISTE " & .IdCuenta, MsgBoxStyle.Critical)
                    DUMMY = "X"
                    Exit For
                End If
                .Nombre = gvCuentasContables.GetRowCellValue(i, "Nombre")
                .Mes01 = gvCuentasContables.GetRowCellValue(i, "Mes01")
                .Mes02 = gvCuentasContables.GetRowCellValue(i, "Mes02")
                .Mes03 = gvCuentasContables.GetRowCellValue(i, "Mes03")
                .Mes04 = gvCuentasContables.GetRowCellValue(i, "Mes04")
                .Mes05 = gvCuentasContables.GetRowCellValue(i, "Mes05")
                .Mes06 = gvCuentasContables.GetRowCellValue(i, "Mes06")
                .Mes07 = gvCuentasContables.GetRowCellValue(i, "Mes07")
                .Mes08 = gvCuentasContables.GetRowCellValue(i, "Mes08")
                .Mes09 = gvCuentasContables.GetRowCellValue(i, "Mes09")
                .Mes10 = gvCuentasContables.GetRowCellValue(i, "Mes10")
                .Mes11 = gvCuentasContables.GetRowCellValue(i, "Mes11")
                .Mes12 = gvCuentasContables.GetRowCellValue(i, "Mes12")
            End With
            DetallesPresupuesto.Add(entDetalle)
        Next

        If DUMMY <> "" Then
            Exit Sub
        End If

        Dim msj As String = ""
        Select Case DbMode
            Case DbModeType.insert
                msj = bl.InsertaDetallePresupuesto(DetallesPresupuesto)
                If msj = "" Then
                    MsgBox("El presupuesto ha sido registrado con éxito", MsgBoxStyle.Information)
                Else
                    MsgBox("SE DETECTÓ UN ERROR AL CREAR EL PRESUPUESTO" + Chr(13) + msj, MsgBoxStyle.Critical)
                    Exit Sub
                End If
                Me.Close()
            Case DbModeType.update
                '
                msj = bl.ModificaDetallePresupuesto(IdPresupuesto, DetallesPresupuesto)
                If msj = "" Then
                    MsgBox("El presupuesto ha sido actualizado con éxito", MsgBoxStyle.Information)
                Else
                    MsgBox("SE DETECTÓ UN ERROR AL ACTUALIZAR EL PRESUPUESTO" + Chr(13) + msj, MsgBoxStyle.Critical)
                    Exit Sub
                End If
                Me.Close()
        End Select
    End Sub

    Private Sub sbImportar_Click(sender As Object, e As EventArgs) Handles sbImportar.Click

        LimpiaGrid()

        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            MsgBox("No seleccionó ningun archivo", MsgBoxStyle.Exclamation, "Nota")
            Return
        End If

        Dim ExcObj As New Object
        Try
            ExcObj = CreateObject("EXCEL.APPLICATION")
            ExcObj.WORKBOOKS.Open(ofd.FileName)
        Catch ex As Exception
            MsgBox("No se pudo abrir el archivo o no tiene el formato correcto")
            Exit Sub
        End Try


        ExcObj.Sheets(1).Select()
        'Dim lnCol As Integer = ExcObj.ActiveSheet.UsedRange.COLUMNS.Count
        Dim TotFilas As Integer = ExcObj.ActiveSheet.UsedRange.ROWS.COUNT
        If TotFilas < 2 Then
            MsgBox("No se encontró ningun dato para hacer la importación" & Chr(13) & "Recuerde que la fila 1 deben contener los títulos", MsgBoxStyle.Information, "Nota")
            ExcObj.WORKBOOKS.Close()
            Exit Sub
        End If

        Try
            For nf = 2 To TotFilas
                gvCuentasContables.AddNewRow()
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "IdCuenta", SiEsNulo(ExcObj.activesheet.cells(nf, 1).VALUE, ""))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Nombre", SiEsNulo(ExcObj.activesheet.cells(nf, 2).VALUE, 0.0))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes01", SiEsNulo(ExcObj.activesheet.cells(nf, 3).VALUE, 0.0))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes02", SiEsNulo(ExcObj.activesheet.cells(nf, 4).VALUE, 0.0))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes03", SiEsNulo(ExcObj.activesheet.cells(nf, 5).VALUE, 0.0))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes04", SiEsNulo(ExcObj.activesheet.cells(nf, 6).VALUE, 0.0))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes05", SiEsNulo(ExcObj.activesheet.cells(nf, 7).VALUE, 0.0))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes06", SiEsNulo(ExcObj.activesheet.cells(nf, 8).VALUE, 0.0))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes07", SiEsNulo(ExcObj.activesheet.cells(nf, 9).VALUE, 0.0))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes08", SiEsNulo(ExcObj.activesheet.cells(nf, 10).VALUE, 0.0))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes09", SiEsNulo(ExcObj.activesheet.cells(nf, 11).VALUE, 0.0))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes10", SiEsNulo(ExcObj.activesheet.cells(nf, 12).VALUE, 0.0))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes11", SiEsNulo(ExcObj.activesheet.cells(nf, 13).VALUE, 0.0))
                gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes12", SiEsNulo(ExcObj.activesheet.cells(nf, 14).VALUE, 0.0))
                gvCuentasContables.UpdateCurrentRow()
            Next

            ExcObj.WORKBOOKS.Close()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcObj)
            ExcObj = Nothing

            MsgBox("El proceso de importación ha concluido")
        Catch ex As Exception
            MsgBox("Sucedio un problema al importar las series" + Chr(13) + ex.Message)
        End Try

        'Dim ofd As New OpenFileDialog()
        'ofd.ShowDialog()

        'If ofd.FileName = "" Then
        '    Return
        'End If

        'Dim csv_file As System.IO.StreamReader = File.OpenText(ofd.FileName)
        'Try
        '    While csv_file.Peek() > 0
        '        Dim sLine As String = csv_file.ReadLine()
        '        Dim aData As Array = sLine.Split(",")
        '        gvCuentasContables.AddNewRow()
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "IdCuenta", SiEsNulo(aData(0), ""))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Nombre", SiEsNulo(aData(1), ""))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes01", SiEsNulo(aData(2), 0))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes02", SiEsNulo(aData(3), 0))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes03", SiEsNulo(aData(4), 0))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes04", SiEsNulo(aData(5), 0))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes05", SiEsNulo(aData(6), 0))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes06", SiEsNulo(aData(7), 0))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes07", SiEsNulo(aData(8), 0))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes08", SiEsNulo(aData(9), 0))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes09", SiEsNulo(aData(10), 0))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes10", SiEsNulo(aData(11), 0))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes11", SiEsNulo(aData(13), 0))
        '        gvCuentasContables.SetRowCellValue(gvCuentasContables.FocusedRowHandle, "Mes12", SiEsNulo(aData(14), 0))
        '        gvCuentasContables.UpdateCurrentRow()

        '    End While
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        '    Return
        'End Try

        'csv_file.Close()
    End Sub
End Class