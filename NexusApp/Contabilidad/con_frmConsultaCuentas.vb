Imports NexusBLL
Public Class con_frmConsultaCuentas
    Dim blInventa As New InventarioBLL(g_ConnectionString)
    Dim Prod As DataTable, lDummy As Boolean = False

    Private Sub inv_frmConsultaProductos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        teNombre.Focus()
    End Sub
    Private Sub inv_frmConsultaProductos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gcProductos.DataSource = New DataView(cnsDataTable)
        IdCuenta = ""
        teCodigo.EditValue = ""
        teNombre.EditValue = ""
        lDummy = False
        teNombre.Focus()
    End Sub

    Private _cnsDatatable As DataTable
    Public Property cnsDataTable() As DataTable
        Get
            Return _cnsDatatable
        End Get
        Set(ByVal value As DataTable)
            _cnsDatatable = value
        End Set
    End Property
    Private _IdCuenta As String = ""
    Public Property IdCuenta() As String
        Get
            Return _IdCuenta
        End Get
        Set(ByVal value As String)
            _IdCuenta = value
        End Set
    End Property

    Private Sub gvProd_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvProd.KeyDown
        If e.KeyCode = Keys.Enter Then
            IdCuenta = gvProd.GetRowCellValue(gvProd.FocusedRowHandle, "IdCuenta")
            lDummy = True
            Me.Close()
        End If
    End Sub

    Private Sub gcProductos_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gcProductos.DoubleClick
        IdCuenta = gvProd.GetRowCellValue(gvProd.FocusedRowHandle, "IdCuenta")
        lDummy = True
        Me.Close()
    End Sub
    Private Sub inv_frmConsultaProductos_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            IdCuenta = ""
            Me.Close()
        End If
    End Sub

#Region "Tipo de Busqueda"
    Private Sub txtNombre_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles teNombre.EditValueChanged
        If rgTipoBusqueda.SelectedIndex = 0 Then
            Dim Transaccion As String
            Dim CadenaPrincipal As String = teNombre.EditValue

            Transaccion = teNombre.EditValue
            If teNombre.EditValue <> "" And Transaccion.IndexOf("%") > 0 Then
                Dim V() As String
                Dim Cadena As String
                Dim SentenciaBusqueda As String = "Nombre like '%"
                Dim x As Integer

                'Cadena a tratar 
                Cadena = teNombre.EditValue

                'Remplazando todos los espaciadores incorrectos por ";" 
                For x = 0 To 1
                    Cadena = Replace(Cadena, "%", ";")
                Next x

                'Separo la cadena gracias al delimitador ";" 
                V = Split(Cadena, ";")

                'Muestro la cadena 
                For x = 0 To UBound(V)
                    If x = 0 Then
                        SentenciaBusqueda += Trim(V(x)) + "%'"
                    Else
                        SentenciaBusqueda += IIf(Trim(V(x)) <> "", " and Nombre like '%" + Trim(V(x)) + "%'", Trim(V(x)))
                    End If
                Next x
                cnsDataTable.DefaultView.RowFilter = (SentenciaBusqueda)
            Else
                cnsDataTable.DefaultView.RowFilter = ("Nombre like '%" + teNombre.EditValue + "%'")
            End If
        Else
            cnsDataTable.DefaultView.RowFilter = ("Nombre like '" + teNombre.EditValue + "%'")
        End If
        gcProductos.DataSource = cnsDataTable.DefaultView
    End Sub

    Private Sub txtCodigo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles teCodigo.EditValueChanged
        If rgTipoBusqueda.SelectedIndex = 0 Then
            cnsDataTable.DefaultView.RowFilter = ("IdCuenta like '%" + teCodigo.EditValue + "%'")
        Else
            cnsDataTable.DefaultView.RowFilter = ("IdCuenta like '" + teCodigo.EditValue + "%'")
        End If
        IdCuenta = gvProd.GetRowCellValue(gvProd.FocusedRowHandle, "IdCuenta")
        gcProductos.DataSource = cnsDataTable.DefaultView
    End Sub

    Private Sub rgTipoBusqueda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgTipoBusqueda.SelectedIndexChanged
        If teCodigo.EditValue <> "" Then
            txtCodigo_EditValueChanged("", New EventArgs)
        Else
            If teNombre.EditValue <> "" Then
                txtNombre_EditValueChanged("", New EventArgs)
            End If
        End If
    End Sub
#End Region

    Private Sub inv_frmConsultaProductos_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' si solo cerro el formulario, no selecciono codigo de producto
        If Not lDummy Then
            IdCuenta = ""
        End If
    End Sub

End Class