﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pre_frmMantDepartamentos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton
        Me.sbCancelar = New DevExpress.XtraEditors.SimpleButton
        Me.teNombre = New DevExpress.XtraEditors.TextEdit
        Me.teEncargado = New DevExpress.XtraEditors.TextEdit
        Me.lcSucursal = New DevExpress.XtraEditors.LabelControl
        Me.lueSucursales = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.meDescripcion = New DevExpress.XtraEditors.MemoEdit
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teEncargado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lueSucursales.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sbGuardar
        '
        Me.sbGuardar.Location = New System.Drawing.Point(80, 201)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(75, 23)
        Me.sbGuardar.TabIndex = 4
        Me.sbGuardar.Text = "Guardar"
        '
        'sbCancelar
        '
        Me.sbCancelar.Location = New System.Drawing.Point(256, 201)
        Me.sbCancelar.Name = "sbCancelar"
        Me.sbCancelar.Size = New System.Drawing.Size(75, 23)
        Me.sbCancelar.TabIndex = 5
        Me.sbCancelar.Text = "Cancelar"
        '
        'teNombre
        '
        Me.teNombre.Location = New System.Drawing.Point(156, 45)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(251, 20)
        Me.teNombre.TabIndex = 1
        '
        'teEncargado
        '
        Me.teEncargado.Location = New System.Drawing.Point(156, 74)
        Me.teEncargado.Name = "teEncargado"
        Me.teEncargado.Size = New System.Drawing.Size(251, 20)
        Me.teEncargado.TabIndex = 2
        '
        'lcSucursal
        '
        Me.lcSucursal.Location = New System.Drawing.Point(30, 16)
        Me.lcSucursal.Name = "lcSucursal"
        Me.lcSucursal.Size = New System.Drawing.Size(44, 13)
        Me.lcSucursal.TabIndex = 10
        Me.lcSucursal.Text = "Sucursal:"
        '
        'lueSucursales
        '
        Me.lueSucursales.Location = New System.Drawing.Point(156, 13)
        Me.lueSucursales.Name = "lueSucursales"
        Me.lueSucursales.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lueSucursales.Size = New System.Drawing.Size(251, 20)
        Me.lueSucursales.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(30, 48)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl1.TabIndex = 11
        Me.LabelControl1.Text = "Departamento:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(30, 77)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl2.TabIndex = 12
        Me.LabelControl2.Text = "Encargado:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(30, 103)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl3.TabIndex = 13
        Me.LabelControl3.Text = "Descripción:"
        '
        'meDescripcion
        '
        Me.meDescripcion.Location = New System.Drawing.Point(156, 102)
        Me.meDescripcion.Name = "meDescripcion"
        Me.meDescripcion.Size = New System.Drawing.Size(251, 81)
        Me.meDescripcion.TabIndex = 3
        '
        'pre_frmMantDepartamentos
        '
        Me.ClientSize = New System.Drawing.Size(442, 265)
        Me.Controls.Add(Me.meDescripcion)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.lcSucursal)
        Me.Controls.Add(Me.lueSucursales)
        Me.Controls.Add(Me.teEncargado)
        Me.Controls.Add(Me.teNombre)
        Me.Controls.Add(Me.sbCancelar)
        Me.Controls.Add(Me.sbGuardar)
        Me.Modulo = "Presupuestos"
        Me.Name = "pre_frmMantDepartamentos"
        Me.OptionId = ""
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Departamentos"
        Me.Controls.SetChildIndex(Me.sbGuardar, 0)
        Me.Controls.SetChildIndex(Me.sbCancelar, 0)
        Me.Controls.SetChildIndex(Me.teNombre, 0)
        Me.Controls.SetChildIndex(Me.teEncargado, 0)
        Me.Controls.SetChildIndex(Me.lueSucursales, 0)
        Me.Controls.SetChildIndex(Me.lcSucursal, 0)
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        Me.Controls.SetChildIndex(Me.LabelControl3, 0)
        Me.Controls.SetChildIndex(Me.meDescripcion, 0)
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teEncargado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lueSucursales.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teEncargado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lcSucursal As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lueSucursales As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meDescripcion As DevExpress.XtraEditors.MemoEdit

End Class
