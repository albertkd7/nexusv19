﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class con_frmTiposPartida
    Dim entidad As con_TiposPartida

    Private Sub inv_frmUbicaciones_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub con_frmTiposPartida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.con_TiposPartidaSelectAll
        entidad = objTablas.con_TiposPartidaSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipo"))

        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub con_frmTiposPartida_Nuevo_Click() Handles Me.Nuevo
        entidad = New con_TiposPartida
        entidad.IdTipo = ""
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub con_frmTiposPartida_Save_Click() Handles Me.Guardar
        If teNombre.EditValue = "" Or teId.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Tipo de Partida, Nombre]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.con_TiposPartidaInsert(entidad)
        Else
            objTablas.con_TiposPartidaUpdate(entidad)
        End If
        gc.DataSource = objTablas.con_TiposPartidaSelectAll
        entidad = objTablas.con_TiposPartidaSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipo"))
        CargaPantalla()
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub con_frmTiposPartida_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el tipo de partida seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.con_TiposPartidaDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.con_TiposPartidaSelectAll
                entidad = objTablas.con_TiposPartidaSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipo"))
                CargaPantalla()
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL TIPO DE PARTIDA:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub con_frmTiposPartida_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entidad
            teId.EditValue = .IdTipo
            teNombre.EditValue = .Nombre
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entidad
            .IdTipo = teId.EditValue
            .Nombre = teNombre.EditValue
        End With


    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entidad = objTablas.con_TiposPartidaSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipo"))
        CargaPantalla()
    End Sub

    Private Sub con_frmTiposPartida_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        ' teId.Properties.ReadOnly = True
        teId.Focus()
    End Sub

End Class
