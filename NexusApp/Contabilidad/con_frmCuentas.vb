﻿Imports DevExpress.XtraTreeList
Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraTreeList.Nodes
Imports DevExpress.Utils.Menu
Imports DevExpress.XtraTreeList.Menu
Imports DevExpress.XtraSplashScreen

Public Class con_frmCuentas
    Dim blConta As New ContabilidadBLL(g_ConnectionString)
    Dim EsNueva As Boolean
    Private Sub frmCuentas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim dt As DataTable = objTablas.con_CuentasSelectParaArbol
        Dim dv As New DataView(dt)
        tlCuentas.KeyFieldName = "IdCuenta"
        tlCuentas.ParentFieldName = "IdCuentaMayor"
        tlCuentas.DataSource = dv
        tlCuentas.BestFitColumns()
    End Sub

    Private Sub con_frmCuentas_Guardar() Handles sbGuardar.Click

        Dim IdMayor = tlCuentas.FocusedNode.GetDisplayText(0).Trim 'tlCuentas.FocusedNode.ParentNode.GetDisplayText(0).Trim
        Dim entCta As con_Cuentas = objTablas.con_CuentasSelectByPK(IdMayor)
        If Not EsNueva Then
            IdMayor = entCta.IdCuentaMayor
        End If

        Dim entCuenta As New con_Cuentas
        With entCuenta
            .IdCuenta = txtIdCuenta.Text
            .Nombre = txtNombreCuenta.Text
            .IdCuentaMayor = IdMayor
            .Tipo = cboTipo.SelectedIndex
            .Nivel = txtNivelCuenta.Text
            .Naturaleza = entCta.Naturaleza
            .EsTransaccional = True
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .FechaHoraModificacion = Nothing
            .ModificadoPor = ""
        End With
        Dim msj = "La cuenta fue creada exitosamente"
        If DbMode = DbModeType.insert Then
            objTablas.con_CuentasInsert(entCuenta)
            DbMode = DbModeType.update

            Dim parentNode As TreeListNode = tlCuentas.FocusedNode()
            
            Dim Nat As String = tlCuentas.FocusedNode.GetDisplayText(4)
            Dim Niv As String = entCuenta.Nivel.ToString()
            Dim Tip As String = cboTipo.Text
            tlCuentas.AppendNode(New Object() {entCuenta.IdCuenta, entCuenta.Nombre, Tip, Niv, Nat, True, entCta.IdCuenta}, parentNode)
        Else
            objTablas.con_CuentasUpdate(entCuenta)
            tlCuentas.FocusedNode.SetValue(1, entCuenta.Nombre)
            msj = "La cuenta fue actualizada exitosamente"
        End If
        blConta.ChequeaCuentas()
        MsgBox(msj, MsgBoxStyle.Information, "Nota")
        EsNueva = False
    End Sub
    Private Sub frmCuentas_Elimnar() Handles Me.Eliminar
        If txtIdCuenta.Text = "" Then
            Exit Sub
        End If
        If Not tlCuentas.FocusedNode.GetValue(5) Then
            MsgBox("No puede eliminar ésta cuenta, tiene sub-cuentas", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        If MsgBox("Está seguro(a) de eliminar la cuenta?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.con_CuentasDeleteByPK(txtIdCuenta.Text)
                tlCuentas.DeleteNode(tlCuentas.FocusedNode)
                MsgBox("Cuenta eliminada", MsgBoxStyle.Information, "Nota")
            Catch ex As Exception
                MsgBox("Hubo algún error al eliminar la cuenta" & Chr(13) & ex.Message, MsgBoxStyle.Critical, "Error")
            End Try

        End If
    End Sub
    Private Sub frmCuentas_Reporte() Handles Me.Reporte
        Dim dt As DataTable = objTablas.con_CuentasSelectParaArbol()

        Dim rpt As New con_rptCuentas

        rpt.DataSource = dt
        rpt.DataMember = ""
        rpt.xlrEmpresa.Text = gsNombre_Empresa
        rpt.xlrTitulo.Text = "Listado para revisión del catálogo de cuentas"
        rpt.ShowPreviewDialog()

    End Sub
    Private Sub tlCuentas_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tlCuentas.MouseClick
        If tlCuentas.Nodes.Count > 0 Then
            If e.Button = Windows.Forms.MouseButtons.Right Then
                If tlCuentas.FocusedNode.GetValue(5) And blConta.IsCuentaConSaldo(tlCuentas.FocusedNode.GetValue(0)) Then
                    sbCrearTitular.Enabled = False
                Else
                    sbCrearNormal.Enabled = True
                End If
            End If
            If e.Button = Windows.Forms.MouseButtons.Left Then
                ActualizarCuenta()
            End If
        End If
    End Sub
    Private Sub ActualizarCuenta()

        gcCuentas.Visible = True
        DbMode = DbModeType.update

        Dim NodoPadre As Nodes.TreeListNode = tlCuentas.FocusedNode.ParentNode
        If Not NodoPadre Is Nothing Then
            txtIdCuentaPadre.Text = NodoPadre.GetDisplayText(0).Trim & " - " & NodoPadre.GetDisplayText(1)
        Else
            txtIdCuentaPadre.Text = ""
        End If

        txtIdCuenta.Text = tlCuentas.FocusedNode.GetDisplayText(0)
        txtNombreCuenta.Text = tlCuentas.FocusedNode.GetDisplayText(1)
        txtNivelCuenta.Text = tlCuentas.FocusedNode.Level
        Dim Nat As String = tlCuentas.FocusedNode.GetDisplayText(2)
        'Título, Rubro, Cuenta de Mayor, Sub-Cuenta
        cboTipo.SelectedIndex = 0
        If Nat.StartsWith("R") Then
            cboTipo.SelectedIndex = 1
        End If
        If Nat.StartsWith("C") Then
            cboTipo.SelectedIndex = 2
        End If
        If Nat.StartsWith("S") Then
            cboTipo.SelectedIndex = 3
        End If
        txtIdCuenta.Properties.ReadOnly = True
    End Sub
    Private Sub tlCuentas_FocusedNodeChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraTreeList.FocusedNodeChangedEventArgs) Handles tlCuentas.FocusedNodeChanged
        txtIdCuenta.Text = ""
        txtIdCuentaPadre.Text = ""
        txtNombreCuenta.Text = ""
        cboTipo.SelectedIndex = 0
    End Sub
    Private Sub sbCrearTitular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCrearTitular.Click
        DbMode = DbModeType.insert
        gcCuentas.Visible = True
        txtIdCuentaPadre.Text = ""
        txtIdCuenta.Text = ""
        txtNombreCuenta.Text = ""
        txtNivelCuenta.Text = "0"
        'lblTipo.Visible = True
        'cboTipo.Visible = True
        cboTipo.SelectedIndex = 0
        txtIdCuenta.Properties.ReadOnly = False
        txtIdCuenta.Focus()
        EsNueva = True
    End Sub
    Private Sub sbCrearNormal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles sbCrearNormal.Click

        DbMode = DbModeType.insert
        gcCuentas.Visible = True
        txtIdCuentaPadre.Text = tlCuentas.FocusedNode.GetDisplayText(0).Trim & " - " & tlCuentas.FocusedNode.GetDisplayText(1)
        Dim UltimoNodo As Nodes.TreeListNode = tlCuentas.FocusedNode.LastNode
        If UltimoNodo Is Nothing Then
            txtIdCuenta.Text = tlCuentas.FocusedNode.GetDisplayText(0).Trim & "01"
        Else
            txtIdCuenta.Text = UltimoNodo.GetDisplayText(0)
        End If
        txtNivelCuenta.Text = tlCuentas.FocusedNode.Level + 1
        cboTipo.SelectedIndex = 3

        txtIdCuenta.Properties.ReadOnly = False
        txtNombreCuenta.Text = ""
        txtIdCuenta.Focus()
        EsNueva = True
    End Sub
    Private Sub sbRevisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbRevisar.Click
        blConta.ChequeaCuentas()
        MsgBox("Cuentas revisadas", MsgBoxStyle.Information, "Nota")
    End Sub

    Private Sub tlCuentas_PopupMenuShowing(sender As Object, e As PopupMenuShowingEventArgs) Handles tlCuentas.PopupMenuShowing
        Dim hitInfo As TreeListHitInfo = CType(sender, TreeList).CalcHitInfo(e.Point)
        Dim node As TreeListNode = Nothing

        '''Create a menu for Columns
        If hitInfo.HitInfoType = HitInfoType.Column Then
            Dim menu As TreeListMenu = TryCast(e.Menu, TreeListMenu)
            If menu IsNot Nothing Then
                Dim MenuItem As DXMenuItem = New DXMenuItem("Importar Catalogo CSV", AddressOf ColumnNodeMenuItemClick, Nexus.My.Resources.icon_upload_file)
                menu.Items.Add(MenuItem)
            End If
        End If

        '''Create a menu for Rows
        If hitInfo.HitInfoType = HitInfoType.Cell Then
            node = hitInfo.Node
            If node IsNot Nothing Then
                Dim MenuItem As DXMenuItem = New DXMenuItem("Cambiar Naturaleza de cuenta", AddressOf CellNodeMenuItemClick)
                MenuItem.Tag = node
                e.Menu.Items.Add(MenuItem)
            End If
        End If
    End Sub

    Private Sub ColumnNodeMenuItemClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim ofd As New OpenFileDialog()
        ofd.Filter = "CSV Files (*.csv)|*.csv|TXT Files (*.txt)|*.txt"
        ofd.Title = "Por Favor indique nombre del archivo a cargar..."

        If ofd.ShowDialog() = DialogResult.OK Then
            Dim DelimitadorCSV As String = InputBox("Ingrese el delimitador del CSV", "Verificacion CSV", ",")

            SplashScreenManager.ShowForm(Me, WaitForm.GetType(), True, True, False)
            SplashScreenManager.Default.SetWaitFormCaption("Procesando")

            Dim NuevoCatalogo As New List(Of con_Cuentas)

            Dim lineas = IO.File.ReadAllLines(ofd.FileName)
            Try
                If MessageBox.Show("Esta seguro de continuar con la cargar de Catalogo?, el proceso incluye eliminar el actual y cargar el nuevo catalogo", "CONFIRMACION", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.No Then
                    SplashScreenManager.CloseForm()
                    Exit Sub
                End If

                SplashScreenManager.Default.SetWaitFormDescription("Extrayendo informacion...")
                For Each line In lineas
                    Dim objFields = From field In line.Split(DelimitadorCSV)
                                    Select CType(field, Object)
                    NuevoCatalogo.Add(
                        New con_Cuentas() With {
                            .IdCuenta = objFields(0),
                            .Nombre = objFields(1),
                            .IdCuentaMayor = "",
                            .EsTransaccional = 0,
                            .Nivel = 0,
                            .Tipo = 0,
                            .Naturaleza = 0,
                            .FechaHoraCreacion = Date.Now,
                            .CreadoPor = objMenu.User,
                            .ModificadoPor = "",
                            .FechaHoraModificacion = Nothing
                        })
                Next

                If NuevoCatalogo.Count = 0 Then
                    MessageBox.Show("No se encontro datos para procesar, verique si ingreso el delimitador correcto...", "FALLO", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    SplashScreenManager.CloseForm()
                    Exit Sub
                End If

                Dim i As Integer = 1
                For Each _cuenta In NuevoCatalogo
                    SplashScreenManager.Default.SetWaitFormDescription("Analizando: " & i & " / " & NuevoCatalogo.Count)
                    Dim x As Integer = _cuenta.IdCuenta.Length - 1
                    While x > 0
                        Dim _cuentaMayor = NuevoCatalogo.FirstOrDefault(Function(a) a.IdCuenta = _cuenta.IdCuenta.Substring(0, x))
                        If _cuentaMayor IsNot Nothing Then
                            _cuenta.IdCuentaMayor = _cuentaMayor.IdCuenta
                            x = -1
                        End If
                        x -= 1
                    End While
                    _cuenta.Naturaleza = CType(_cuenta.IdCuenta.Substring(0, 1), Integer)
                    i += 1
                Next

                Dim Nivel As Integer = 0
                Dim Nodos = NuevoCatalogo.GroupBy(Function(x) x.IdCuentaMayor).OrderBy(Function(x) x.Key).ToList()
                For Each Nodo In Nodos

                    Nivel = NuevoCatalogo.Where(Function(x) Nodo.Key.StartsWith(x.IdCuentaMayor)).GroupBy(Function(x) x.IdCuentaMayor).Count - 1
                    For Each SubNodo In NuevoCatalogo.Where(Function(x) x.IdCuentaMayor = Nodo.Key).ToList()
                        SubNodo.Nivel = Nivel
                        SubNodo.Tipo = IIf(Nivel > 4, 3, Nivel)
                    Next
                Next

                blConta.EliminarCatalgoParaCarga()
                i = 1
                SplashScreenManager.Default.SetWaitFormDescription("Guardando informacion...")
                For Each NuevaCuenta In NuevoCatalogo
                    SplashScreenManager.Default.SetWaitFormDescription("Guardando: " & i & " / " & NuevoCatalogo.Count)
                    objTablas.con_CuentasInsert(NuevaCuenta)
                    i += 1
                Next
                blConta.ChequeaCuentas()
                frmCuentas_Load(Nothing, New EventArgs())

                SplashScreenManager.Default.SetWaitFormCaption("Completado!")
                SplashScreenManager.Default.SetWaitFormDescription("Proceso terminado")
                SplashScreenManager.CloseForm()

                MessageBox.Show("Catalago cargado con Exito!", "OPERACION", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                SplashScreenManager.CloseForm()
                MessageBox.Show("Formato incorrecto CSV", "OPERACION", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try

        End If

    End Sub

    Private Sub CellNodeMenuItemClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim _IdCuenta As String = tlCuentas.FocusedNode.GetValue(tlCuentas.Columns(0))
        Dim _Cuenta = objTablas.con_CuentasSelectByPK(_IdCuenta)
        If MessageBox.Show("Esta seguro de modificar la Naturaleza(" & _Cuenta.Naturaleza & ") de la cuenta: " & _IdCuenta & " y sub-subcuentas?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.Yes Then

            Dim _Naturaleza As String = InputBox("Ingrese la nueva Naturaleza de la cuenta y sus sub-Cuentas de --> " & _IdCuenta, "Cambio Naturaleza", _Cuenta.Naturaleza)

            If MessageBox.Show("Esta seguro de modificar la Naturaleza(" & _Cuenta.Naturaleza & ") de la cuenta: " & _IdCuenta & " y sub-subcuentas a " & _Naturaleza & "?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Information) Then
                objTablas.con_CuentasUpdateNaturaleza(_Cuenta.IdCuenta, _Naturaleza, True)
                MessageBox.Show("Actulizacion de Naturaleza con Exito!")
                frmCuentas_Load(Nothing, New EventArgs())
            End If
        End If
    End Sub
End Class