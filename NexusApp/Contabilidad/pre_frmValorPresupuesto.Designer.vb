﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class pre_frmValorPresupuesto
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem
        Me.gcDetallePreCuenta = New DevExpress.XtraGrid.GridControl
        Me.gvDetallePreCuenta = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcIdPresupuestoCuentaFk = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcValor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcComentario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcFechaCreacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcCreadoPor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcMes = New DevExpress.XtraGrid.Columns.GridColumn
        Me.meComentario = New DevExpress.XtraEditors.MemoEdit
        Me.teValor = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton
        Me.sbCancelar = New DevExpress.XtraEditors.SimpleButton
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        CType(Me.gcDetallePreCuenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvDetallePreCuenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meComentario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teValor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcDetallePreCuenta
        '
        Me.gcDetallePreCuenta.Location = New System.Drawing.Point(263, 32)
        Me.gcDetallePreCuenta.MainView = Me.gvDetallePreCuenta
        Me.gcDetallePreCuenta.Name = "gcDetallePreCuenta"
        Me.gcDetallePreCuenta.Size = New System.Drawing.Size(380, 204)
        Me.gcDetallePreCuenta.TabIndex = 4
        Me.gcDetallePreCuenta.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvDetallePreCuenta})
        '
        'gvDetallePreCuenta
        '
        Me.gvDetallePreCuenta.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdPresupuestoCuentaFk, Me.gcValor, Me.gcComentario, Me.gcFechaCreacion, Me.gcCreadoPor, Me.gcMes})
        Me.gvDetallePreCuenta.GridControl = Me.gcDetallePreCuenta
        Me.gvDetallePreCuenta.Name = "gvDetallePreCuenta"
        Me.gvDetallePreCuenta.OptionsBehavior.Editable = False
        Me.gvDetallePreCuenta.OptionsBehavior.ReadOnly = True
        Me.gvDetallePreCuenta.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateFocusedItem
        Me.gvDetallePreCuenta.OptionsView.ColumnAutoWidth = False
        Me.gvDetallePreCuenta.OptionsView.ShowGroupPanel = False
        '
        'gcIdPresupuestoCuentaFk
        '
        Me.gcIdPresupuestoCuentaFk.Caption = "IdPresupuestoCuentaFk"
        Me.gcIdPresupuestoCuentaFk.FieldName = "IdPresupuestoFk"
        Me.gcIdPresupuestoCuentaFk.Name = "gcIdPresupuestoCuentaFk"
        '
        'gcValor
        '
        Me.gcValor.Caption = "Valor"
        Me.gcValor.FieldName = "Valor"
        Me.gcValor.Name = "gcValor"
        Me.gcValor.Visible = True
        Me.gcValor.VisibleIndex = 0
        '
        'gcComentario
        '
        Me.gcComentario.Caption = "Comentario"
        Me.gcComentario.FieldName = "Comentario"
        Me.gcComentario.Name = "gcComentario"
        Me.gcComentario.Visible = True
        Me.gcComentario.VisibleIndex = 1
        Me.gcComentario.Width = 150
        '
        'gcFechaCreacion
        '
        Me.gcFechaCreacion.Caption = "Fecha de creación"
        Me.gcFechaCreacion.FieldName = "FechaCreacion"
        Me.gcFechaCreacion.Name = "gcFechaCreacion"
        Me.gcFechaCreacion.Visible = True
        Me.gcFechaCreacion.VisibleIndex = 2
        '
        'gcCreadoPor
        '
        Me.gcCreadoPor.Caption = "Creado por"
        Me.gcCreadoPor.FieldName = "CreadoPor"
        Me.gcCreadoPor.Name = "gcCreadoPor"
        Me.gcCreadoPor.Visible = True
        Me.gcCreadoPor.VisibleIndex = 3
        '
        'gcMes
        '
        Me.gcMes.Caption = "Mes"
        Me.gcMes.FieldName = "Mes"
        Me.gcMes.Name = "gcMes"
        '
        'meComentario
        '
        Me.meComentario.Location = New System.Drawing.Point(12, 96)
        Me.meComentario.Name = "meComentario"
        Me.meComentario.Size = New System.Drawing.Size(227, 100)
        ToolTipTitleItem1.Text = "Comentario."
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Escriba un comentario relacionado con el ingreso del valor para la cuenta selecci" & _
            "onada, el comentario debe de explicar el motivo del cambio de valor o ingreso de" & _
            "l mismo."
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.meComentario.SuperTip = SuperToolTip1
        Me.meComentario.TabIndex = 1
        '
        'teValor
        '
        Me.teValor.EnterMoveNextControl = True
        Me.teValor.Location = New System.Drawing.Point(76, 29)
        Me.teValor.Name = "teValor"
        Me.teValor.Size = New System.Drawing.Size(163, 20)
        ToolTipTitleItem2.Text = "Valor."
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Ingrese el nuevo valor para el presupuesto en dolares ($)."
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.teValor.SuperTip = SuperToolTip2
        Me.teValor.TabIndex = 0
        Me.teValor.ToolTip = "Ingrese el nuevo valor para el presupuesto."
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(12, 32)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl1.TabIndex = 14
        Me.LabelControl1.Text = "Valor $:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(12, 77)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl2.TabIndex = 15
        Me.LabelControl2.Text = "Comentario:"
        '
        'sbGuardar
        '
        Me.sbGuardar.Location = New System.Drawing.Point(34, 209)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(75, 23)
        Me.sbGuardar.TabIndex = 2
        Me.sbGuardar.Text = "Guardar"
        '
        'sbCancelar
        '
        Me.sbCancelar.Location = New System.Drawing.Point(140, 209)
        Me.sbCancelar.Name = "sbCancelar"
        Me.sbCancelar.Size = New System.Drawing.Size(75, 23)
        Me.sbCancelar.TabIndex = 3
        Me.sbCancelar.Text = "Cancelar"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(263, 13)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(129, 13)
        Me.LabelControl3.TabIndex = 18
        Me.LabelControl3.Text = "Historial de modificaciones:"
        '
        'pre_frmValorPresupuesto
        '
        Me.ClientSize = New System.Drawing.Size(654, 267)
        Me.ControlBox = False
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.sbCancelar)
        Me.Controls.Add(Me.sbGuardar)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.teValor)
        Me.Controls.Add(Me.meComentario)
        Me.Controls.Add(Me.gcDetallePreCuenta)
        Me.Modulo = "Presupuestos"
        Me.Name = "pre_frmValorPresupuesto"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Ingreso de valor"
        Me.Controls.SetChildIndex(Me.gcDetallePreCuenta, 0)
        Me.Controls.SetChildIndex(Me.meComentario, 0)
        Me.Controls.SetChildIndex(Me.teValor, 0)
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        Me.Controls.SetChildIndex(Me.sbGuardar, 0)
        Me.Controls.SetChildIndex(Me.sbCancelar, 0)
        Me.Controls.SetChildIndex(Me.LabelControl3, 0)
        CType(Me.gcDetallePreCuenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvDetallePreCuenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meComentario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teValor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gcDetallePreCuenta As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvDetallePreCuenta As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents meComentario As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents teValor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcIdPresupuestoCuentaFk As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcComentario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFechaCreacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCreadoPor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcMes As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl

End Class
