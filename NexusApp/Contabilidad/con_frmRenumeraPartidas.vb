Imports NexusBLL
Public Class con_frmRenumeraPartidas
    Dim bl As New ContabilidadBLL(g_ConnectionString)

    Private Sub con_frmRenumeraPartidas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        bl.ObtienePeriodoContable(Mes, Ejercicio)
        seEjercicio.EditValue = Ejercicio
        meMes.EditValue = Mes
        objCombos.conTiposPartida(leTipoPartida, "")
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim dFechaCierre As Date = Now
        dFechaCierre = CDate(CStr(seEjercicio.EditValue) & "/" & meMes.Month.ToString.PadLeft(2, "0") & "/01")
        dFechaCierre = DateAdd(DateInterval.Day, -1, dFechaCierre)
        dFechaCierre = DateAdd(DateInterval.Month, 1, dFechaCierre)
        Dim EsOk As Boolean = ValidarFechaCierre(dFechaCierre)
        If Not EsOk Then
            MsgBox("No puede realizar esta re-numeraci�n" & Chr(13) & "El mes ya est� cerrado")
            Return
        End If
        If MsgBox("Est� seguro(a) de realizar la re-numeraci�n?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Return
        End If

        Dim iNP As Integer = bl.pro_RenumeraPartidas(meMes.Month, seEjercicio.EditValue, leTipoPartida.EditValue)
        If iNP = 0 Then
            MsgBox("No se encontraron partidas para actualizar", MsgBoxStyle.Critical)
        Else
            MsgBox("La re-numeraci�n se ha realizado con �xito" & Chr(13) & _
                   "Se actualizaron " & CStr(iNP) & " partidas", MsgBoxStyle.Information, "Nota")
        End If
    End Sub

End Class