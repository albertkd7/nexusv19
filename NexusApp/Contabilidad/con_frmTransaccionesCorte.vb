﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Imports System.Xml
Public Class con_frmTransaccionesCorte
    Dim entTrans As ban_Transacciones
    Dim bl As New NexusBLL.TableBusiness(g_ConnectionString)
    Dim blCaja As New NexusBLL.ContabilidadBLL(g_ConnectionString)
    Dim blAdmon As New NexusBLL.AdmonBLL(g_ConnectionString)
    Dim dtParametros As New DataTable
    Dim IdSucursal As Integer = 0
    Dim Cierre As Boolean = False

    Private Sub caj_frmTransacciones_Eliminar() Handles Me.Eliminar
        If Cierre Then
            MsgBox("No se puede eliminar, este dia esta cerrado", MsgBoxStyle.OkOnly, "Nexus")
            Exit Sub
        End If
        If MsgBox("Está seguro(a) de eliminar este registro?", MsgBoxStyle.YesNo, "Nexus") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim Id As Guid = gv.GetFocusedRowCellValue(gv.Columns("IdComprobante"))
        bl.con_TransaccionesDeleteByPK(Id)
        LlenarGrid()
    End Sub

    Private Sub agua_frmCajaMovimientos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        deFecha.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        leSucursal.EditValue = piIdSucursal
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "")
        dtParametros = blAdmon.ObtieneParametros()
        IdSucursal = leSucursal.EditValue
        LlenarCombobox()
        LimpiarVariables()
        LlenarGrid()
    End Sub
    Private Sub lePuntoVenta_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lePuntoVenta.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "")
    End Sub
    Sub LlenarCombobox()
        objCombos.con_Conceptos(leConceptosCaja)
        objCombos.con_Conceptos(reIdConcepto)
        objCombos.banCuentasBancarias(reIdCuentaBancaria)
        objCombos.banCuentasBancarias(leCtaBancaria)
    End Sub
    Sub LlenarGrid()
        Dim dt As DataTable = blCaja.con_ObtenerDatoCierre(deFecha.EditValue)
        If dt.Rows.Count = 0 Then
            Cierre = False
        Else
            Cierre = dt.Rows(0).Item("Cerrado")
        End If

        If Cierre Then
            sbGuardar.Enabled = False
            sbCerrar.Enabled = False
        Else
            sbGuardar.Enabled = True
            sbCerrar.Enabled = True
        End If
        gc.DataSource = blCaja.con_ObtenerTransaccionesCaja(IdSucursal, deFecha.EditValue)
    End Sub

    Sub LimpiarVariables()

        txtValor.EditValue = 0.0
        txtConcepto.EditValue = ""
        leConceptosCaja.ItemIndex = 0
    End Sub

    Private Sub leConceptosCaja_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leConceptosCaja.EditValueChanged
        If leConceptosCaja.EditValue = 1 Then
            leCtaBancaria.Visible = True
            xelBanco.Visible = True
        Else
            leCtaBancaria.Visible = False
            xelBanco.Visible = False
        End If
    End Sub

    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In Me.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If

        Next
        gv.OptionsBehavior.Editable = Tipo
    End Sub

    Private Sub sbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGuardar.Click
        If txtValor.EditValue = 0 Then
            MsgBox("Necesita introducir un valor", MsgBoxStyle.OkOnly, "Nota")
            Exit Sub
        End If
        Dim entTrans As New con_Transacciones
        entTrans.IdComprobante = Guid.NewGuid
        entTrans.IdSucursal = leSucursal.EditValue
        entTrans.IdPunto = lePuntoVenta.EditValue
        entTrans.IdConcepto = leConceptosCaja.EditValue
        If leConceptosCaja.EditValue = 1 Then
            entTrans.IdCuentaBancaria = leCtaBancaria.EditValue
        Else
            entTrans.IdCuentaBancaria = 0
        End If
        entTrans.Fecha = deFecha.EditValue
        entTrans.Valor = txtValor.EditValue
        entTrans.Concepto = txtConcepto.EditValue
        entTrans.CreadoPor = objMenu.User
        entTrans.FechaHoraCreacion = Now
        bl.con_TransaccionesInsert(entTrans)
        LimpiarVariables()
        LlenarGrid()
    End Sub

    Private Sub deFecha_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deFecha.EditValueChanged
        If deFecha.EditValue <> Nothing Then
            LlenarGrid()
        End If
    End Sub

    Private Sub caj_frmTransacciones_Reporte() Handles Me.Reporte

        Dim dt As DataTable = blCaja.con_ObtenerLiqCaja(deFecha.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, objMenu.User, False)
        Dim rpt As New con_rptCorteCaja
        rpt.DataSource = dt
        rpt.DataMember = ""
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlPeriodo.Text = "Fecha: " & deFecha.EditValue.ToString.Substring(0, 10)
        rpt.xrlTitulo.Text = "LIQUIDACION DE CAJA"
        rpt.ShowPreviewDialog()

    End Sub

    Private Sub sbCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCerrar.Click
        If MsgBox("Ya no podra ingresar o modificar registros. Está seguro(a) realizar el cierre?", MsgBoxStyle.YesNo, "Nexus") = MsgBoxResult.No Then
            Exit Sub
        End If
        Dim dt As DataTable = blCaja.con_ObtenerLiqCaja(deFecha.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, objMenu.User, True)
        LlenarGrid()
    End Sub
End Class
