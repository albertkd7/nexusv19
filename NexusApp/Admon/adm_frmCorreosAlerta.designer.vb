﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adm_frmCorreosAlerta
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(adm_frmCorreosAlerta))
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCorreo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcContacto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCargo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcTipo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leRubro = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.tePeso = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.chkEmite = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.chkApnfd = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leRubro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePeso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEmite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkApnfd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.leRubro, Me.tePeso, Me.chkEmite, Me.chkApnfd})
        Me.gc.Size = New System.Drawing.Size(786, 335)
        Me.gc.TabIndex = 5
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcId, Me.gcCorreo, Me.gcContacto, Me.gcCargo, Me.gcTipo})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.NewItemRowText = "Click aquí para ingresar nueva forma de pago. Esc para cancelar"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcId
        '
        Me.gcId.Caption = "Id. Correo"
        Me.gcId.FieldName = "IdCorreo"
        Me.gcId.Name = "gcId"
        Me.gcId.Visible = True
        Me.gcId.VisibleIndex = 0
        Me.gcId.Width = 67
        '
        'gcCorreo
        '
        Me.gcCorreo.Caption = "Correo"
        Me.gcCorreo.FieldName = "Correo"
        Me.gcCorreo.Name = "gcCorreo"
        Me.gcCorreo.Visible = True
        Me.gcCorreo.VisibleIndex = 1
        Me.gcCorreo.Width = 266
        '
        'gcContacto
        '
        Me.gcContacto.Caption = "Contacto"
        Me.gcContacto.FieldName = "Contacto"
        Me.gcContacto.Name = "gcContacto"
        Me.gcContacto.Visible = True
        Me.gcContacto.VisibleIndex = 2
        Me.gcContacto.Width = 182
        '
        'gcCargo
        '
        Me.gcCargo.Caption = "Cargo"
        Me.gcCargo.FieldName = "Cargo"
        Me.gcCargo.Name = "gcCargo"
        Me.gcCargo.Visible = True
        Me.gcCargo.VisibleIndex = 3
        Me.gcCargo.Width = 115
        '
        'gcTipo
        '
        Me.gcTipo.Caption = "Tipo"
        Me.gcTipo.ColumnEdit = Me.leRubro
        Me.gcTipo.FieldName = "Tipo"
        Me.gcTipo.Name = "gcTipo"
        Me.gcTipo.Visible = True
        Me.gcTipo.VisibleIndex = 4
        Me.gcTipo.Width = 138
        '
        'leRubro
        '
        Me.leRubro.AutoHeight = False
        Me.leRubro.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leRubro.Name = "leRubro"
        '
        'tePeso
        '
        Me.tePeso.AutoHeight = False
        Me.tePeso.Mask.EditMask = "n2"
        Me.tePeso.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tePeso.Name = "tePeso"
        '
        'chkEmite
        '
        Me.chkEmite.AutoHeight = False
        Me.chkEmite.Name = "chkEmite"
        '
        'chkApnfd
        '
        Me.chkApnfd.AutoHeight = False
        Me.chkApnfd.Name = "chkApnfd"
        '
        'adm_frmCorreosAlerta
        '
        Me.ClientSize = New System.Drawing.Size(882, 360)
        Me.Controls.Add(Me.gc)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Administración"
        Me.Name = "adm_frmCorreosAlerta"
        Me.OptionId = "002003"
        Me.Text = "Correos Alerta"
        Me.TipoFormulario = 2
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leRubro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePeso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEmite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkApnfd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCorreo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leRubro As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents tePeso As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents chkEmite As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents chkApnfd As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents gcContacto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCargo As DevExpress.XtraGrid.Columns.GridColumn

End Class
