﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class adm_frmPuntosVenta
    Dim bl As New AdmonBLL(g_ConnectionString)
    Dim entCuentas As con_Cuentas
    Dim entidad As fac_PuntosVenta


    Private Sub adm_frmTiposComprobante_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub adm_frmTiposComprobante_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(riteSucursal, objMenu.User, "")
        gc.DataSource = objTablas.fac_PuntosVentaSelectAll
        entidad = objTablas.fac_PuntosVentaSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdSucursal"), gv.GetRowCellValue(gv.FocusedRowHandle, "IdPunto"))
        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub adm_frmTiposComprobante_Nuevo_Click() Handles Me.Nuevo
        entidad = New fac_PuntosVenta
        entidad.IdPunto = objFunciones.ObtenerUltimoId("FAC_PUNTOSVENTA", "IdPunto") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub adm_frmTiposComprobante_Save_Click() Handles Me.Guardar
        If teNombre.EditValue = "" Or SiEsNulo(leSucursal.EditValue, 0) = 0 Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Nombre, Sucursal]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.fac_PuntosVentaInsert(entidad)
        Else
            objTablas.fac_PuntosVentaUpdate(entidad)
        End If
        gc.DataSource = objTablas.fac_PuntosVentaSelectAll
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub adm_frmTiposComprobante_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el punto de venta seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.fac_PuntosVentaDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(2)), gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.fac_PuntosVentaSelectAll
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL PUNTO DE VENTA:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub adm_frmTiposComprobante_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entidad
            teIdPunto.EditValue = .IdPunto
            teNombre.EditValue = .Nombre
            leSucursal.EditValue = .IdSucursal
            teValorMensual.EditValue = .VentaMensual
            beCtaBancaria.EditValue = .IdCuentaBanco
            beCtaCajaChica.EditValue = .IdCuentaCajaChica
            teValorMensual.EditValue = .VentaMensual
        End With

        beCtaCajaChica_Validated(beCtaCajaChica, New System.EventArgs)
        beCtaBancaria_Validated(beCtaBancaria, New System.EventArgs)

        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entidad
            .IdPunto = teIdPunto.EditValue
            .Nombre = teNombre.EditValue
            .IdSucursal = leSucursal.EditValue
            .VentaMensual = teValorMensual.EditValue
            .IdCuentaBanco = beCtaBancaria.EditValue
            .IdCuentaCajaChica = beCtaCajaChica.EditValue
            .VentaMensual = teValorMensual.EditValue
        End With
    End Sub
    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entidad = objTablas.fac_PuntosVentaSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdSucursal"), gv.GetRowCellValue(gv.FocusedRowHandle, "IdPunto"))
        CargaPantalla()
    End Sub
    Private Sub adm_frmTiposComprobante_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teIdPunto.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub
    Private Sub beCtaCajaChica_Validated(sender As Object, e As EventArgs) Handles beCtaCajaChica.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCtaCajaChica.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCtaCajaChica.EditValue = ""
            teCtaCajaChica.EditValue = ""
            Exit Sub
        End If
        beCtaCajaChica.EditValue = entCuentas.IdCuenta
        teCtaCajaChica.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCtaBancaria_Validated(sender As Object, e As EventArgs) Handles beCtaBancaria.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCtaBancaria.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCtaBancaria.EditValue = ""
            teCtaBancaria.EditValue = ""
            Exit Sub
        End If
        beCtaBancaria.EditValue = entCuentas.IdCuenta
        teCtaBancaria.EditValue = entCuentas.Nombre
    End Sub

    Private Sub beCtaCajaChica_ButtonClick(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCtaCajaChica.ButtonClick
        beCtaCajaChica.EditValue = ""
        beCtaCajaChica_Validated(beCtaCajaChica, New System.EventArgs)
    End Sub

    Private Sub beCtaBancaria_ButtonClick(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCtaBancaria.ButtonClick
        beCtaBancaria.EditValue = ""
        beCtaBancaria_Validated(beCtaBancaria, New System.EventArgs)
    End Sub
End Class
