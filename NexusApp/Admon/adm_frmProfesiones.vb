﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class adm_frmProfesiones

    Private Sub pre_frmProfesiones_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar ésta Profesión?", MsgBoxStyle.YesNo, "Nexus") = MsgBoxResult.No Then
            Return
        End If
        objTablas.fac_ProfesionesDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
        gc.DataSource = objTablas.fac_ProfesionesSelectAll
    End Sub


    Private Sub pre_frmProfesiones_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        gc.DataSource = objTablas.fac_ProfesionesSelectAll
    End Sub

    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        Dim entProfesiones As New fac_Profesiones
        Dim iFila As Integer
        If e.RowHandle < 0 Then
            DbMode = DbModeType.insert
            iFila = gv.RowCount - 1
        Else
            DbMode = DbModeType.update
            iFila = e.RowHandle
        End If
        If Not AllowInsert Or Not AllowEdit Then
            MsgBox("No le está permitido ingresar o editar información", MsgBoxStyle.Exclamation, Me.Text)
            gc.DataSource = objTablas.fac_ProfesionesSelectAll
            Exit Sub
        End If
        With (entProfesiones)
            .IdProfesion = gv.GetRowCellValue(iFila, gv.Columns(0).FieldName)
            .Nombre = gv.GetRowCellValue(iFila, gv.Columns(1).FieldName)
            .Asegurado = False ' gv.GetRowCellValue(iFila, gv.Columns(2).FieldName)
            .Peso = 0 ' gv.GetRowCellValue(iFila, gv.Columns(3).FieldName)
        End With
        If DbMode = DbModeType.insert Then
            objTablas.fac_ProfesionesInsert(entProfesiones)
        Else
            objTablas.fac_ProfesionesUpdate(entProfesiones)
        End If
    End Sub

    Private Sub pre_frmProfesiones_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub

End Class
