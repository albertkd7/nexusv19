﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adm_frmPuntosVenta
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteSucursal = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.teCtaBancaria = New DevExpress.XtraEditors.TextEdit()
        Me.beCtaBancaria = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl54 = New DevExpress.XtraEditors.LabelControl()
        Me.teCtaCajaChica = New DevExpress.XtraEditors.TextEdit()
        Me.beCtaCajaChica = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teValorMensual = New DevExpress.XtraEditors.TextEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdPunto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteSucursal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.teCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCtaCajaChica.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCtaCajaChica.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teValorMensual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdPunto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.gc)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(498, 444)
        Me.PanelControl1.TabIndex = 4
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(2, 2)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteSucursal})
        Me.gc.Size = New System.Drawing.Size(494, 440)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Punto de Venta"
        Me.GridColumn1.FieldName = "IdPunto"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 94
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombre"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 234
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Sucursal"
        Me.GridColumn3.ColumnEdit = Me.riteSucursal
        Me.GridColumn3.FieldName = "IdSucursal"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 145
        '
        'riteSucursal
        '
        Me.riteSucursal.AutoHeight = False
        Me.riteSucursal.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteSucursal.Name = "riteSucursal"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.teCtaBancaria)
        Me.PanelControl2.Controls.Add(Me.beCtaBancaria)
        Me.PanelControl2.Controls.Add(Me.LabelControl54)
        Me.PanelControl2.Controls.Add(Me.teCtaCajaChica)
        Me.PanelControl2.Controls.Add(Me.beCtaCajaChica)
        Me.PanelControl2.Controls.Add(Me.LabelControl51)
        Me.PanelControl2.Controls.Add(Me.LabelControl5)
        Me.PanelControl2.Controls.Add(Me.teValorMensual)
        Me.PanelControl2.Controls.Add(Me.leSucursal)
        Me.PanelControl2.Controls.Add(Me.LabelControl8)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.teIdPunto)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.teNombre)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(498, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(543, 444)
        Me.PanelControl2.TabIndex = 5
        '
        'teCtaBancaria
        '
        Me.teCtaBancaria.Enabled = False
        Me.teCtaBancaria.Location = New System.Drawing.Point(190, 271)
        Me.teCtaBancaria.Name = "teCtaBancaria"
        Me.teCtaBancaria.Size = New System.Drawing.Size(331, 20)
        Me.teCtaBancaria.TabIndex = 53
        '
        'beCtaBancaria
        '
        Me.beCtaBancaria.Location = New System.Drawing.Point(24, 271)
        Me.beCtaBancaria.Name = "beCtaBancaria"
        Me.beCtaBancaria.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCtaBancaria.Size = New System.Drawing.Size(160, 20)
        Me.beCtaBancaria.TabIndex = 49
        '
        'LabelControl54
        '
        Me.LabelControl54.Location = New System.Drawing.Point(24, 252)
        Me.LabelControl54.Name = "LabelControl54"
        Me.LabelControl54.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl54.TabIndex = 52
        Me.LabelControl54.Text = "Caja General:"
        '
        'teCtaCajaChica
        '
        Me.teCtaCajaChica.Enabled = False
        Me.teCtaCajaChica.Location = New System.Drawing.Point(190, 223)
        Me.teCtaCajaChica.Name = "teCtaCajaChica"
        Me.teCtaCajaChica.Size = New System.Drawing.Size(331, 20)
        Me.teCtaCajaChica.TabIndex = 51
        '
        'beCtaCajaChica
        '
        Me.beCtaCajaChica.Location = New System.Drawing.Point(24, 223)
        Me.beCtaCajaChica.Name = "beCtaCajaChica"
        Me.beCtaCajaChica.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCtaCajaChica.Size = New System.Drawing.Size(160, 20)
        Me.beCtaCajaChica.TabIndex = 48
        '
        'LabelControl51
        '
        Me.LabelControl51.Location = New System.Drawing.Point(24, 204)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(93, 13)
        Me.LabelControl51.TabIndex = 50
        Me.LabelControl51.Text = "Cuenta Caja Chica:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(24, 158)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(116, 13)
        Me.LabelControl5.TabIndex = 47
        Me.LabelControl5.Text = "Meta de Venta Mensual:"
        '
        'teValorMensual
        '
        Me.teValorMensual.EditValue = 0
        Me.teValorMensual.Location = New System.Drawing.Point(24, 175)
        Me.teValorMensual.Name = "teValorMensual"
        Me.teValorMensual.Properties.Appearance.Options.UseTextOptions = True
        Me.teValorMensual.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teValorMensual.Properties.Mask.EditMask = "n2"
        Me.teValorMensual.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teValorMensual.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teValorMensual.Size = New System.Drawing.Size(129, 20)
        Me.teValorMensual.TabIndex = 46
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(24, 118)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(323, 20)
        Me.leSucursal.TabIndex = 2
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(24, 102)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl8.TabIndex = 45
        Me.LabelControl8.Text = "Sucursal:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(24, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(45, 13)
        Me.LabelControl1.TabIndex = 34
        Me.LabelControl1.Text = "Id Punto:"
        '
        'teIdPunto
        '
        Me.teIdPunto.EnterMoveNextControl = True
        Me.teIdPunto.Location = New System.Drawing.Point(24, 28)
        Me.teIdPunto.Name = "teIdPunto"
        Me.teIdPunto.Size = New System.Drawing.Size(95, 20)
        Me.teIdPunto.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(24, 57)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 35
        Me.LabelControl2.Text = "Nombre:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(24, 73)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(323, 20)
        Me.teNombre.TabIndex = 1
        '
        'adm_frmPuntosVenta
        '
        Me.ClientSize = New System.Drawing.Size(1041, 469)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.DbMode = 1
        Me.Modulo = "Administración"
        Me.Name = "adm_frmPuntosVenta"
        Me.OptionId = "001006"
        Me.Text = "Puntos de Venta"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.PanelControl2, 0)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteSucursal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.teCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCtaCajaChica.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCtaCajaChica.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teValorMensual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdPunto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdPunto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents riteSucursal As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teValorMensual As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCtaBancaria As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCtaBancaria As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl54 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCtaCajaChica As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCtaCajaChica As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl

End Class
