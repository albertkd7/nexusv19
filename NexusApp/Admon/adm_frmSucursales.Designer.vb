﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adm_frmSucursales
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.seHasta = New DevExpress.XtraEditors.SpinEdit()
        Me.seDesde = New DevExpress.XtraEditors.SpinEdit()
        Me.seUltimo = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teTelefonos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teResponsable = New DevExpress.XtraEditors.TextEdit()
        Me.teCta02 = New DevExpress.XtraEditors.TextEdit()
        Me.teCta01 = New DevExpress.XtraEditors.TextEdit()
        Me.beCta02 = New DevExpress.XtraEditors.ButtonEdit()
        Me.beCta01 = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdSucursal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.seHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seUltimo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teResponsable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.gc)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(372, 435)
        Me.PanelControl1.TabIndex = 4
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(2, 2)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(368, 431)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Id. Sucursal"
        Me.GridColumn1.FieldName = "IdSucursal"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 83
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Sucursal"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 264
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.seHasta)
        Me.PanelControl2.Controls.Add(Me.seDesde)
        Me.PanelControl2.Controls.Add(Me.seUltimo)
        Me.PanelControl2.Controls.Add(Me.LabelControl7)
        Me.PanelControl2.Controls.Add(Me.LabelControl6)
        Me.PanelControl2.Controls.Add(Me.LabelControl5)
        Me.PanelControl2.Controls.Add(Me.teTelefonos)
        Me.PanelControl2.Controls.Add(Me.LabelControl4)
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.teResponsable)
        Me.PanelControl2.Controls.Add(Me.teCta02)
        Me.PanelControl2.Controls.Add(Me.teCta01)
        Me.PanelControl2.Controls.Add(Me.beCta02)
        Me.PanelControl2.Controls.Add(Me.beCta01)
        Me.PanelControl2.Controls.Add(Me.LabelControl8)
        Me.PanelControl2.Controls.Add(Me.LabelControl27)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.teIdSucursal)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.teNombre)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(372, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(518, 435)
        Me.PanelControl2.TabIndex = 5
        '
        'seHasta
        '
        Me.seHasta.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seHasta.Location = New System.Drawing.Point(335, 217)
        Me.seHasta.Name = "seHasta"
        Me.seHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seHasta.Size = New System.Drawing.Size(114, 20)
        Me.seHasta.TabIndex = 160
        '
        'seDesde
        '
        Me.seDesde.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDesde.Location = New System.Drawing.Point(180, 217)
        Me.seDesde.Name = "seDesde"
        Me.seDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDesde.Size = New System.Drawing.Size(116, 20)
        Me.seDesde.TabIndex = 159
        '
        'seUltimo
        '
        Me.seUltimo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seUltimo.Location = New System.Drawing.Point(14, 217)
        Me.seUltimo.Name = "seUltimo"
        Me.seUltimo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seUltimo.Size = New System.Drawing.Size(122, 20)
        Me.seUltimo.TabIndex = 158
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(335, 198)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(114, 13)
        Me.LabelControl7.TabIndex = 155
        Me.LabelControl7.Text = "Hasta Formulario Único:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(180, 198)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(116, 13)
        Me.LabelControl6.TabIndex = 156
        Me.LabelControl6.Text = "Desde Formulario Único:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(14, 198)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(122, 13)
        Me.LabelControl5.TabIndex = 157
        Me.LabelControl5.Text = "Ult. Número Emitido F.U.:"
        '
        'teTelefonos
        '
        Me.teTelefonos.EnterMoveNextControl = True
        Me.teTelefonos.Location = New System.Drawing.Point(14, 166)
        Me.teTelefonos.Name = "teTelefonos"
        Me.teTelefonos.Size = New System.Drawing.Size(282, 20)
        Me.teTelefonos.TabIndex = 154
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(14, 147)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl4.TabIndex = 153
        Me.LabelControl4.Text = "Teléfonos:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(14, 102)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl3.TabIndex = 152
        Me.LabelControl3.Text = "Responsable:"
        '
        'teResponsable
        '
        Me.teResponsable.EnterMoveNextControl = True
        Me.teResponsable.Location = New System.Drawing.Point(14, 121)
        Me.teResponsable.Name = "teResponsable"
        Me.teResponsable.Size = New System.Drawing.Size(435, 20)
        Me.teResponsable.TabIndex = 151
        '
        'teCta02
        '
        Me.teCta02.Enabled = False
        Me.teCta02.Location = New System.Drawing.Point(180, 324)
        Me.teCta02.Name = "teCta02"
        Me.teCta02.Size = New System.Drawing.Size(269, 20)
        Me.teCta02.TabIndex = 149
        '
        'teCta01
        '
        Me.teCta01.Enabled = False
        Me.teCta01.Location = New System.Drawing.Point(180, 267)
        Me.teCta01.Name = "teCta01"
        Me.teCta01.Size = New System.Drawing.Size(269, 20)
        Me.teCta01.TabIndex = 149
        '
        'beCta02
        '
        Me.beCta02.Location = New System.Drawing.Point(14, 324)
        Me.beCta02.Name = "beCta02"
        Me.beCta02.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta02.Size = New System.Drawing.Size(160, 20)
        Me.beCta02.TabIndex = 148
        '
        'beCta01
        '
        Me.beCta01.Location = New System.Drawing.Point(14, 267)
        Me.beCta01.Name = "beCta01"
        Me.beCta01.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta01.Size = New System.Drawing.Size(160, 20)
        Me.beCta01.TabIndex = 148
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(14, 305)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl8.TabIndex = 150
        Me.LabelControl8.Text = "Cuenta Contable Caja"
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(14, 248)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(121, 13)
        Me.LabelControl27.TabIndex = 150
        Me.LabelControl27.Text = "Cuenta Contable Costos:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(14, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl1.TabIndex = 34
        Me.LabelControl1.Text = "Id. Sucursal:"
        '
        'teIdSucursal
        '
        Me.teIdSucursal.EnterMoveNextControl = True
        Me.teIdSucursal.Location = New System.Drawing.Point(14, 31)
        Me.teIdSucursal.Name = "teIdSucursal"
        Me.teIdSucursal.Size = New System.Drawing.Size(95, 20)
        Me.teIdSucursal.TabIndex = 27
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(14, 57)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl2.TabIndex = 35
        Me.LabelControl2.Text = "Descripción:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(14, 76)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(435, 20)
        Me.teNombre.TabIndex = 28
        '
        'adm_frmSucursales
        '
        Me.ClientSize = New System.Drawing.Size(890, 460)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.DbMode = 1
        Me.Modulo = "Administración"
        Me.Name = "adm_frmSucursales"
        Me.OptionId = "001005"
        Me.Text = "Sucursales"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.PanelControl2, 0)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.seHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seUltimo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teResponsable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdSucursal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teCta01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCta01 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTelefonos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teResponsable As DevExpress.XtraEditors.TextEdit
    Friend WithEvents seHasta As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seDesde As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seUltimo As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCta02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCta02 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
End Class
