﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adm_frmRecuperarArchivo
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btProceder = New DevExpress.XtraEditors.SimpleButton
        Me.OpenFile = New System.Windows.Forms.OpenFileDialog
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcId = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcFechaHora = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.gcIdTipo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.leIdTipoComprobante = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.gcNumero = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcNrc = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcNit = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.teTotal = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.deHasta = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.deDesde = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.btAplicar = New DevExpress.XtraEditors.SimpleButton
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leIdTipoComprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btProceder
        '
        Me.btProceder.Location = New System.Drawing.Point(376, 57)
        Me.btProceder.Name = "btProceder"
        Me.btProceder.Size = New System.Drawing.Size(75, 23)
        Me.btProceder.TabIndex = 7
        Me.btProceder.Text = "Cargar Datos"
        '
        'OpenFile
        '
        Me.OpenFile.FileName = "OpenFileDialog1"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gc.Location = New System.Drawing.Point(0, 89)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.leIdTipoComprobante, Me.RepositoryItemTextEdit1, Me.teTotal})
        Me.gc.Size = New System.Drawing.Size(925, 401)
        Me.gc.TabIndex = 147
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv, Me.GridView2})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcId, Me.gcFecha, Me.gcFechaHora, Me.gcIdTipo, Me.gcNumero, Me.gcCliente, Me.gcNrc, Me.gcNit, Me.gcTotal})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowFooter = True
        '
        'gcId
        '
        Me.gcId.Caption = "Id Transacción"
        Me.gcId.FieldName = "NumTransaccion"
        Me.gcId.Name = "gcId"
        Me.gcId.OptionsColumn.ReadOnly = True
        Me.gcId.Visible = True
        Me.gcId.VisibleIndex = 0
        Me.gcId.Width = 83
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha"
        Me.gcFecha.FieldName = "Fecha"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.OptionsColumn.ReadOnly = True
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 1
        Me.gcFecha.Width = 69
        '
        'gcFechaHora
        '
        Me.gcFechaHora.Caption = "FechaHora"
        Me.gcFechaHora.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.gcFechaHora.FieldName = "FechaHora"
        Me.gcFechaHora.Name = "gcFechaHora"
        Me.gcFechaHora.OptionsColumn.ReadOnly = True
        Me.gcFechaHora.Visible = True
        Me.gcFechaHora.VisibleIndex = 2
        Me.gcFechaHora.Width = 106
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.EditFormat.FormatString = "d"
        Me.RepositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'gcIdTipo
        '
        Me.gcIdTipo.Caption = "Tipo Comprobante"
        Me.gcIdTipo.ColumnEdit = Me.leIdTipoComprobante
        Me.gcIdTipo.FieldName = "IdTipoComprobante"
        Me.gcIdTipo.Name = "gcIdTipo"
        Me.gcIdTipo.OptionsColumn.ReadOnly = True
        Me.gcIdTipo.Visible = True
        Me.gcIdTipo.VisibleIndex = 3
        Me.gcIdTipo.Width = 106
        '
        'leIdTipoComprobante
        '
        Me.leIdTipoComprobante.AutoHeight = False
        Me.leIdTipoComprobante.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leIdTipoComprobante.DisplayFormat.FormatString = "d"
        Me.leIdTipoComprobante.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.leIdTipoComprobante.EditFormat.FormatString = "d"
        Me.leIdTipoComprobante.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.leIdTipoComprobante.Name = "leIdTipoComprobante"
        '
        'gcNumero
        '
        Me.gcNumero.Caption = "Numero Comprobante"
        Me.gcNumero.FieldName = "Numero"
        Me.gcNumero.Name = "gcNumero"
        Me.gcNumero.Visible = True
        Me.gcNumero.VisibleIndex = 4
        Me.gcNumero.Width = 123
        '
        'gcCliente
        '
        Me.gcCliente.Caption = "Nombre Cliente"
        Me.gcCliente.FieldName = "NombreCliente"
        Me.gcCliente.Name = "gcCliente"
        Me.gcCliente.Visible = True
        Me.gcCliente.VisibleIndex = 5
        Me.gcCliente.Width = 101
        '
        'gcNrc
        '
        Me.gcNrc.Caption = "Num. Registro"
        Me.gcNrc.FieldName = "Nrc"
        Me.gcNrc.Name = "gcNrc"
        Me.gcNrc.Visible = True
        Me.gcNrc.VisibleIndex = 6
        Me.gcNrc.Width = 101
        '
        'gcNit
        '
        Me.gcNit.Caption = "Nit"
        Me.gcNit.FieldName = "Nit"
        Me.gcNit.Name = "gcNit"
        Me.gcNit.Visible = True
        Me.gcNit.VisibleIndex = 7
        Me.gcNit.Width = 119
        '
        'gcTotal
        '
        Me.gcTotal.Caption = "Total"
        Me.gcTotal.ColumnEdit = Me.teTotal
        Me.gcTotal.DisplayFormat.FormatString = "{0:n2}"
        Me.gcTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcTotal.FieldName = "Total"
        Me.gcTotal.Name = "gcTotal"
        Me.gcTotal.OptionsColumn.ReadOnly = True
        Me.gcTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", "{0:n2}")})
        Me.gcTotal.Visible = True
        Me.gcTotal.VisibleIndex = 8
        Me.gcTotal.Width = 99
        '
        'teTotal
        '
        Me.teTotal.AutoHeight = False
        Me.teTotal.Mask.EditMask = "n2"
        Me.teTotal.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotal.Name = "teTotal"
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.gc
        Me.GridView2.Name = "GridView2"
        '
        'deHasta
        '
        Me.deHasta.EditValue = Nothing
        Me.deHasta.Location = New System.Drawing.Point(55, 34)
        Me.deHasta.Name = "deHasta"
        Me.deHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deHasta.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deHasta.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deHasta.Size = New System.Drawing.Size(100, 20)
        Me.deHasta.TabIndex = 151
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(18, 37)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl2.TabIndex = 150
        Me.LabelControl2.Text = "Desde:"
        '
        'deDesde
        '
        Me.deDesde.EditValue = Nothing
        Me.deDesde.Location = New System.Drawing.Point(55, 12)
        Me.deDesde.Name = "deDesde"
        Me.deDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deDesde.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deDesde.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deDesde.Size = New System.Drawing.Size(100, 20)
        Me.deDesde.TabIndex = 148
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(18, 15)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl1.TabIndex = 149
        Me.LabelControl1.Text = "Desde:"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(55, 60)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(315, 20)
        Me.leSucursal.TabIndex = 153
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(8, 63)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl4.TabIndex = 152
        Me.LabelControl4.Text = "Sucursal:"
        '
        'btAplicar
        '
        Me.btAplicar.Location = New System.Drawing.Point(790, 57)
        Me.btAplicar.Name = "btAplicar"
        Me.btAplicar.Size = New System.Drawing.Size(115, 23)
        Me.btAplicar.TabIndex = 154
        Me.btAplicar.Text = "Aplicar Importación"
        '
        'adm_frmRecuperarArchivo
        '
        Me.ClientSize = New System.Drawing.Size(925, 515)
        Me.Controls.Add(Me.btAplicar)
        Me.Controls.Add(Me.leSucursal)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.deHasta)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.deDesde)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.btProceder)
        Me.Modulo = "Administración"
        Me.Name = "adm_frmRecuperarArchivo"
        Me.OptionId = "001007"
        Me.Text = "Importar Facturación"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.btProceder, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.deDesde, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        Me.Controls.SetChildIndex(Me.deHasta, 0)
        Me.Controls.SetChildIndex(Me.LabelControl4, 0)
        Me.Controls.SetChildIndex(Me.leSucursal, 0)
        Me.Controls.SetChildIndex(Me.btAplicar, 0)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leIdTipoComprobante, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btProceder As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents OpenFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents deHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFechaHora As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leIdTipoComprobante As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents teTotal As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNrc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btAplicar As DevExpress.XtraEditors.SimpleButton

End Class
