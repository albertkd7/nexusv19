﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class adm_frmFormasPago
    Dim bl As New AdmonBLL(g_ConnectionString)
    Dim entCuentas As con_Cuentas
    Dim entFormas As fac_FormasPago


    Private Sub adm_frmTiposComprobante_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub adm_frmTiposComprobante_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        gc.DataSource = objTablas.fac_FormasPagoSelectAll
        entFormas = objTablas.fac_FormasPagoSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdFormaPago"))

        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub adm_frmTiposComprobante_Nuevo_Click() Handles Me.Nuevo
        entFormas = New fac_FormasPago
        entFormas.IdFormaPago = objFunciones.ObtenerUltimoId("FAC_FORMASPAGO", "IdFormaPago") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub adm_frmTiposComprobante_Save_Click() Handles Me.Guardar
        If teNombre.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Nombre]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.fac_FormasPagoInsert(entFormas)
        Else
            objTablas.fac_FormasPagoUpdate(entFormas)
        End If
        gc.DataSource = objTablas.fac_FormasPagoSelectAll

        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub adm_frmTiposComprobante_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar la forma de pago seleccionada?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.fac_FormasPagoDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.fac_FormasPagoSelectAll
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR LA FORMA DE PAGO:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub adm_frmTiposComprobante_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entFormas
            teIdForma.EditValue = .IdFormaPago
            teNombre.EditValue = .Nombre
            seDias.EditValue = .DiasCredito
            beCta01.EditValue = .IdCuentaContable
            entCuentas = objTablas.con_CuentasSelectByPK(SiEsNulo(.IdCuentaContable, ""))
            teCta01.EditValue = entCuentas.Nombre
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entFormas
            .IdFormaPago = teIdForma.EditValue
            .Nombre = teNombre.EditValue
            .IdCuentaContable = beCta01.EditValue
            .DiasCredito = seDias.EditValue
        End With


    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entFormas = objTablas.fac_FormasPagoSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdFormaPago"))
        CargaPantalla()
    End Sub

    Private Sub adm_frmTiposComprobante_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teIdForma.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub
    Private Sub beCta01_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta01.ButtonClick
        beCta01.EditValue = ""
        beCta01_Validated(beCta01, New System.EventArgs)
    End Sub
    Private Sub beCta01_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta01.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta01.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta01.EditValue = ""
            teCta01.EditValue = ""
            Exit Sub
        End If
        beCta01.EditValue = entCuentas.IdCuenta
        teCta01.EditValue = entCuentas.Nombre
    End Sub
End Class
