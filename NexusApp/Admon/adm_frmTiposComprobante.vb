﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class adm_frmTiposComprobante
    Dim bl As New AdmonBLL(g_ConnectionString)
    Dim entCuentas As con_Cuentas
    Dim entTipos As adm_TiposComprobante


    Private Sub adm_frmTiposComprobante_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub adm_frmTiposComprobante_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_TipoLibroAplica(leLibro)
        objCombos.adm_TipoModuloAplicaComprobante(leModulo)
        objCombos.adm_TipoAplicacionComprobante(leTipoAplicacion)

        objCombos.adm_TipoLibroAplica(riteLibro1)
        objCombos.adm_TipoModuloAplicaComprobante(riteModulo1)
        objCombos.adm_TipoAplicacionComprobante(riteTipoAplicacion1)

        gc.DataSource = objTablas.adm_TiposComprobanteSelectAll
        entTipos = objTablas.adm_TiposComprobanteSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipoComprobante"))

        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub adm_frmTiposComprobante_Nuevo_Click() Handles Me.Nuevo
        entTipos = New adm_TiposComprobante
        entTipos.IdTipoComprobante = objFunciones.ObtenerUltimoId("ADM_TIPOSCOMPROBANTE", "IdTipoComprobante") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub adm_frmTiposComprobante_Save_Click() Handles Me.Guardar
        If teNombre.EditValue = "" Or teAbreviatura.EditValue = "" Or leTipoAplicacion.EditValue = 0 Or seNumLineas.EditValue = 0 Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Nombre, Abreviatura, Número de Líneas y Tipo de Aplicación]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.adm_TiposComprobanteInsert(entTipos)
        Else
            objTablas.adm_TiposComprobanteUpdate(entTipos)
        End If
        gc.DataSource = objTablas.adm_TiposComprobanteSelectAll

        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub adm_frmTiposComprobante_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el comprobante seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.adm_TiposComprobanteDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.adm_TiposComprobanteSelectAll
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL TIPO DE COMPROBANTE:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub adm_frmTiposComprobante_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entTipos
            teIdComprobante.EditValue = .IdTipoComprobante
            teNombre.EditValue = .Nombre
            teAbreviatura.EditValue = .Abreviatura
            leTipoAplicacion.EditValue = .TipoAplicacion
            leLibro.EditValue = .IdLibro
            leModulo.EditValue = .IdModuloAplica
            seNumLineas.EditValue = .NumeroLineas
            beCta01.EditValue = .IdCuentaInv
            teImpresor.EditValue = .NombreImpresor
            ceDetallaIva.EditValue = .DetallaIVA

            entCuentas = objTablas.con_CuentasSelectByPK(SiEsNulo(.IdCuentaInv, ""))
            teCta01.EditValue = entCuentas.Nombre
            ceFormulario.EditValue = .FormularioUnico
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entTipos
            .IdTipoComprobante = teIdComprobante.EditValue
            .Nombre = teNombre.EditValue
            .Abreviatura = teAbreviatura.EditValue
            .TipoAplicacion = leTipoAplicacion.EditValue
            .IdLibro = leLibro.EditValue
            .IdModuloAplica = leModulo.EditValue
            .NumeroLineas = seNumLineas.EditValue
            .IdCuentaInv = beCta01.EditValue
            .NombreImpresor = teImpresor.EditValue
            .DetallaIVA = ceDetallaIva.EditValue
            .FormularioUnico = ceFormulario.EditValue
        End With


    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entTipos = objTablas.adm_TiposComprobanteSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipoComprobante"))
        CargaPantalla()
    End Sub

    Private Sub adm_frmTiposComprobante_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teIdComprobante.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub
    Private Sub beCta01_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta01.ButtonClick
        beCta01.EditValue = ""
        beCta01_Validated(beCta01, New System.EventArgs)
    End Sub
    Private Sub beCta01_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta01.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta01.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta01.EditValue = ""
            teCta01.EditValue = ""
            Exit Sub
        End If
        beCta01.EditValue = entCuentas.IdCuenta
        teCta01.EditValue = entCuentas.Nombre
    End Sub


End Class
