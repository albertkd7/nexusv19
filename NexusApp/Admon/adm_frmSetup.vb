﻿Imports DevExpress.Skins
Imports NexusBLL
Imports NexusELL.TableEntities
Imports System.Drawing
Imports System.Configuration
Imports System.Security.Cryptography
Imports System.Web.Script.Serialization
Imports DevExpress.XtraSplashScreen

Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports System.Text

Imports System.IO
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class adm_frmSetup
    Dim entCuentas As con_Cuentas
    Dim FileName As String
    Dim myBL As New AdmonBLL(g_ConnectionString), blConta As New ContabilidadBLL(g_ConnectionString)
    Dim NxSchemas As Boolean = IIf(ConfigurationManager.AppSettings("NxSchemas:Disabled") = "True", True, False)

    Private Sub frmSetup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim dt As DataTable = myBL.ObtieneParametros
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue)
        objCombos.inv_Bodegas(leBodega)
        objCombos.inv_Precios(leTipoPrecio)
        objCombos.fac_FormaPrecios(lePrecios)
        objCombos.conTiposPartida(leTipoPartidaCompras)

        GetSkins()
        With dt.Rows(0)
            teEmpresa.EditValue = .Item("NombreEmpresa")
            teNRC.EditValue = .Item("NrcEmpresa")
            teNIT.EditValue = .Item("NitEmpresa")
            rgTipoContrib.SelectedIndex = .Item("TipoContribuyente") - 1
            ceAgente.EditValue = .Item("EsRetenedor")
            teDireccion.EditValue = .Item("Direccion")
            teDomicilio.EditValue = .Item("Domicilio")
            teMunicipio.EditValue = .Item("Municipio")
            teDepartamento.EditValue = .Item("Departamento")
            teNumeroPatronal.EditValue = .Item("NumeroPatronal")
            teActividad.EditValue = .Item("ActividadEconomica")
            teNombre1.EditValue = .Item("NombreFirmante1")
            teNombre2.EditValue = .Item("NombreFirmante2")
            teNombre3.EditValue = .Item("NombreFirmante3")
            teNombre4.EditValue = .Item("NombreFirmante4")
            teNombre5.EditValue = .Item("NombreFirmante5")
            teNombre6.EditValue = .Item("NombreFirmante6")
            teCargo1.EditValue = .Item("CargoFirmante1")
            teCargo2.EditValue = .Item("CargoFirmante2")
            teCargo3.EditValue = .Item("CargoFirmante3")
            teCargo4.EditValue = .Item("CargoFirmante4")
            teCargo5.EditValue = .Item("CargoFirmante5")
            teCargo6.EditValue = .Item("CargoFirmante6")
            teMoneda.EditValue = .Item("DescMoneda")

            deFechaMinima.EditValue = .Item("FechaCierre")
            deFechaMaxima.EditValue = .Item("FechaLimite")
            sePorcIVA.EditValue = .Item("PorcIVA")
            sePorcReten.EditValue = .Item("PorcRetencion")
            sePorcPercep.EditValue = .Item("PorcPercepcion")
            leSucursal.EditValue = .Item("IdSucursal")
            lePuntoVenta.EditValue = .Item("IdPunto")
            leBodega.EditValue = .Item("IdBodega")
            cboTipoPartida.SelectedIndex = .Item("TipoNumeracionPartidas") - 1
            ceGuardarDescuadre.EditValue = .Item("GuardarDesbalance")
            leTipoPrecio.EditValue = .Item("IdTipoPrecio")
            lePrecios.EditValue = .Item("TipoPrecio")
            beCta01.EditValue = .Item("IdCuentaCaja")
            beCta02.EditValue = .Item("IdCuentaDebito1")
            beCta03.EditValue = .Item("IdCuentaDebito2")
            beCta04.EditValue = .Item("IdCuentaCredito1")
            beCta05.EditValue = .Item("IdCuentaCredito2")
            beCta06.EditValue = .Item("IdCuentaRetencion1")
            beCta07.EditValue = .Item("IdCuentaRetencion2")
            beCta08.EditValue = .Item("IdCuentaPercepcion")
            beCta09.EditValue = .Item("IdCuentaRetencionVentas")
            beCta10.EditValue = .Item("IdCuentaPercepcionVentas")
            beCta11.EditValue = .Item("IdCuentaImportacion")
            beCta12.EditValue = .Item("IdCuentaComisionLiquidacion")
            beCta13.EditValue = .Item("IdCuentaPorCobrar")

            seCorrelativo.EditValue = myBL.ObtieneCorrelativos("QUEDAN")
            seSujetosExcluidos.EditValue = myBL.ObtieneCorrelativos("SUJETO_EXCLUIDO")
            seCorrelEntradas.EditValue = myBL.ObtieneCorrelativos("ENTRADAS_INV")
            seCorrelSalidas.EditValue = myBL.ObtieneCorrelativos("SALIDAS_INV")
            seCorrelTraslados.EditValue = myBL.ObtieneCorrelativos("TRASLADOS_INV")
            seCorrelNA.EditValue = myBL.ObtieneCorrelativos("ABONOS_CPC")
            seCorrelVale.EditValue = myBL.ObtieneCorrelativos("FAC_VALES")
            seCorrelOC.EditValue = myBL.ObtieneCorrelativos("ORDEN_COMPRA")
            ceValidaExistencia.EditValue = .Item("ValidarExistencias")
            ceAlertaMinimos.EditValue = .Item("AlertaMinimos")
            leTipoPartidaCompras.EditValue = .Item("IdTipoPartidaCompras")
            ceLineaCompras.EditValue = .Item("ContabilizarLineaCompras")
            ceAutorizaCheques.EditValue = .Item("AutorizarCheques")
            ceVistaPrevia.EditValue = .Item("VistaPreviaFacturar")
            ceUtilizarFormularioUnico.EditValue = .Item("UtilizarFormularioUnico")
            ceSeccionarFormasPago.EditValue = .Item("SeccionarFormaPago")
            ceBloquearFecha.EditValue = .Item("BloquearFechaFacturar")
            'peFoto.Image = ByteToImagen(SiEsNulo(.Item("LogoEmpresa"), "")) '-->> NO IMAGENES EN LA BASE, JAMAS....
            teTelefonos.EditValue = .Item("Telefonos")
            chkFacturarMenosCosto.EditValue = .Item("FacturarMenosCosto")
            meMes.Month = .Item("MesCierreAnual")
            teMontoDirectoEfectivo.EditValue = SiEsNulo(.Item("LimiteMontoDirectoEfectivo"), 0)
            teMontoAcumuladoEfectivo.EditValue = SiEsNulo(.Item("LimiteMontoAcumuladoEfectivo"), 0)
            teMontoDirectoNoEfectivo.EditValue = SiEsNulo(.Item("LimiteMontoDirectoOtros"), 0)
            teMontoAcumuladoNoEfectivo.EditValue = SiEsNulo(.Item("LimiteMontoAcumuladoOtros"), 0)
            SpiDiasCreditoVencido.EditValue = .Item("DiasMaximoCredito")
        End With

        If NxSchemas Then
            sbtnEliminarEmpresaActual.Visible = NxSchemas
        End If

    End Sub
    Private Sub Button1_Click() Handles Me.Guardar
        If deFechaMaxima.EditValue < deFechaMinima.EditValue Then
            MsgBox("La fecha máxima no puede ser menor a la fecha mínima de cierre", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        'Dim dt As DataTable = blConta.ObtienePeriodoContable()
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        blConta.ObtienePeriodoContable(Mes, Ejercicio)

        Dim Fecha As Date = Date.Now
        If Mes > 0 And Ejercicio > 0 Then
            Fecha = New Date(Ejercicio, Mes, 1) 'CDate("01/" + Mes.ToString.PadLeft(2) + "/" + Ejercicio.ToString.PadLeft(4))
        End If
        Fecha = DateAdd(DateInterval.Month, 1, Fecha)
        Fecha = DateAdd(DateInterval.Day, -1, Fecha)
        If deFechaMinima.EditValue <= Fecha Then
            MsgBox("La fecha mínima es inferior al ultimo mes cerrado" + Chr(13) + "Debe ser por lo menos un día después; " & CType(Fecha, DateTime).ToString("dd/MM/yyyy"), MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        Dim adm_Para As New adm_Parametros

        adm_Para.NrcEmpresa = teNRC.EditValue
        adm_Para.NitEmpresa = teNIT.EditValue
        adm_Para.Direccion = teDireccion.EditValue
        adm_Para.Domicilio = teDomicilio.EditValue
        adm_Para.Municipio = teMunicipio.EditValue
        adm_Para.Departamento = teDepartamento.EditValue
        adm_Para.NumeroPatronal = teNumeroPatronal.EditValue
        adm_Para.ActividadEconomica = teActividad.EditValue
        adm_Para.TipoContribuyente = rgTipoContrib.EditValue
        adm_Para.EsRetenedor = ceAgente.EditValue
        adm_Para.NombreFirmante1 = teNombre1.EditValue
        adm_Para.NombreFirmante2 = teNombre2.EditValue
        adm_Para.NombreFirmante3 = teNombre3.EditValue
        adm_Para.NombreFirmante4 = teNombre4.EditValue
        adm_Para.NombreFirmante5 = teNombre5.EditValue
        adm_Para.NombreFirmante6 = teNombre6.EditValue
        adm_Para.CargoFirmante1 = teCargo1.EditValue
        adm_Para.CargoFirmante2 = teCargo2.EditValue
        adm_Para.CargoFirmante3 = teCargo3.EditValue
        adm_Para.CargoFirmante4 = teCargo4.EditValue
        adm_Para.CargoFirmante5 = teCargo5.EditValue
        adm_Para.CargoFirmante6 = teCargo6.EditValue

        adm_Para.DescMoneda = teMoneda.EditValue
        adm_Para.Ejercicio = 0
        adm_Para.FechaCierre = deFechaMinima.EditValue
        adm_Para.FechaLimite = deFechaMaxima.EditValue
        adm_Para.PorcIVA = sePorcIVA.EditValue
        adm_Para.PorcReten = sePorcReten.EditValue
        adm_Para.PorcPercep = sePorcPercep.EditValue
        adm_Para.IdSucursal = leSucursal.EditValue
        adm_Para.IdPunto = lePuntoVenta.EditValue
        adm_Para.IdBodega = leBodega.EditValue

        adm_Para.TipoNumeracionPartidas = cboTipoPartida.SelectedIndex + 1
        adm_Para.GuardarDesbalance = ceGuardarDescuadre.Checked
        adm_Para.IdTipoPrecio = leTipoPrecio.EditValue
        adm_Para.TipoPrecio = lePrecios.EditValue
        adm_Para.IdCuentaCaja = beCta01.EditValue
        adm_Para.IdCuentaDebito1 = beCta02.EditValue
        adm_Para.IdCuentaDebito2 = beCta03.EditValue
        adm_Para.IdCuentaCredito1 = beCta04.EditValue
        adm_Para.IdCuentaCredito2 = beCta05.EditValue
        adm_Para.IdCuentaRetencion1 = beCta06.EditValue
        adm_Para.IdCuentaRetencion2 = beCta07.EditValue
        adm_Para.IdCuentaPercepcion = beCta08.EditValue
        adm_Para.IdCuentaRetencionVentas = beCta09.EditValue
        adm_Para.IdCuentaPercepcionVentas = beCta10.EditValue
        adm_Para.IdCuentaImportacion = beCta11.EditValue
        adm_Para.IdCuentaComisionLiquidacion = beCta12.EditValue
        adm_Para.ValidarExistencias = ceValidaExistencia.EditValue
        adm_Para.IdTipoPartidaCompras = leTipoPartidaCompras.EditValue
        adm_Para.ContabilizarLineaCompras = ceLineaCompras.EditValue
        adm_Para.AlertaMinimos = ceAlertaMinimos.EditValue
        adm_Para.AutorizarCheques = ceAutorizaCheques.EditValue
        adm_Para.VistaPreviaFacturar = ceVistaPrevia.EditValue
        adm_Para.UtilizarFormularioUnico = ceUtilizarFormularioUnico.EditValue
        adm_Para.SeccionarFormaPago = ceSeccionarFormasPago.EditValue
        adm_Para.BloquearFechaFacturar = ceBloquearFecha.EditValue
        adm_Para.LogoEmpresa = Nothing 'ImagenToByte(peFoto.Image)
        adm_Para.Telefonos = SiEsNulo(teTelefonos.EditValue, "")
        adm_Para.IdCuentaPorCobrar = beCta13.EditValue
        adm_Para.FacturarMenosCosto = chkFacturarMenosCosto.EditValue
        adm_Para.MesCierreAnual = meMes.Month
        adm_Para.LimiteMontoDirectoEfectivo = teMontoDirectoEfectivo.EditValue
        adm_Para.LimiteMontoAcumuladoEfectivo = teMontoAcumuladoEfectivo.EditValue
        adm_Para.LimiteMontoDirectoOtros = teMontoDirectoNoEfectivo.EditValue
        adm_Para.LimiteMontoAcumuladoOtros = teMontoAcumuladoNoEfectivo.EditValue
        adm_Para.DiasMaximoCredito = SpiDiasCreditoVencido.EditValue
        pnIVA = sePorcIVA.EditValue / 100
        gsDesc_Moneda = teMoneda.EditValue

        Try
            myBL.adm_ParametrosUpdate(adm_Para, seCorrelativo.EditValue, seSujetosExcluidos.EditValue _
            , seCorrelEntradas.EditValue, seCorrelSalidas.EditValue, seCorrelTraslados.EditValue _
            , seCorrelNA.EditValue, seCorrelVale.EditValue, seCorrelOC.EditValue)
            MsgBox("La configuración ha sido guardada con éxito", MsgBoxStyle.Information, "Nota")
        Catch ex As Exception
            MsgBox("No fue posible Actualizar los Parámetros" + Chr(13) + ex.Message(), 16, "Verifique el error")
            ' ex.Message(), 16, "Verifique el error")
            Exit Sub
        End Try


        My.Settings.SkinName = cbeSkins.Text
        DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(cbeSkins.Text)
        My.Settings.Save()


    End Sub

    Private Sub GetSkins()
        cbeSkins.Text = My.Settings.SkinName
        For Each cnt As SkinContainer In SkinManager.Default.Skins
            cbeSkins.Properties.Items.Add(cnt.SkinName)
        Next cnt
    End Sub


    Private Sub beCta01_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta01.ButtonClick
        beCta01.EditValue = ""
        beCta01_Validated(beCta01, New System.EventArgs)
    End Sub
    Private Sub beCta02_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta02.ButtonClick
        beCta02.EditValue = ""
        beCta02_Validated(beCta01, New System.EventArgs)
    End Sub
    Private Sub beCta03_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta03.ButtonClick
        beCta03.EditValue = ""
        beCta03_Validated(beCta03, New System.EventArgs)
    End Sub
    Private Sub beCta04_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta04.ButtonClick
        beCta04.EditValue = ""
        beCta04_Validated(beCta04, New System.EventArgs)
    End Sub
    Private Sub beCta05_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta05.ButtonClick
        beCta05.EditValue = ""
        beCta05_Validated(beCta05, New System.EventArgs)
    End Sub
    Private Sub beCta06_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta06.ButtonClick
        beCta06.EditValue = ""
        beCta06_Validated(beCta06, New System.EventArgs)
    End Sub
    Private Sub beCta07_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta07.ButtonClick
        beCta07.EditValue = ""
        beCta07_Validated(beCta07, New System.EventArgs)
    End Sub
    Private Sub beCta08_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta08.ButtonClick
        beCta08.EditValue = ""
        beCta08_Validated(beCta08, New System.EventArgs)
    End Sub
    Private Sub beCta09_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta09.ButtonClick
        beCta09.EditValue = ""
        beCta09_Validated(beCta09, New System.EventArgs)
    End Sub
    Private Sub beCta10_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta10.ButtonClick
        beCta10.EditValue = ""
        beCta10_Validated(beCta10, New System.EventArgs)
    End Sub
    Private Sub beCta11_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta11.ButtonClick
        beCta11.EditValue = ""
        beCta11_Validated(beCta11, New System.EventArgs)
    End Sub
    Private Sub beCta12_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta12.ButtonClick
        beCta12.EditValue = ""
        beCta12_Validated(beCta12, New System.EventArgs)
    End Sub
    Private Sub beCta13_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta13.ButtonClick
        beCta13.EditValue = ""
        beCta13_Validated(beCta13, New System.EventArgs)
    End Sub
    Private Sub beCta01_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta01.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta01.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta01.EditValue = ""
            teCta01.EditValue = ""
            Exit Sub
        End If
        beCta01.EditValue = entCuentas.IdCuenta
        teCta01.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta02_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta02.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta02.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta02.EditValue = ""
            teCta02.EditValue = ""
            Exit Sub
        End If
        beCta02.EditValue = entCuentas.IdCuenta
        teCta02.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta03_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta03.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta03.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta03.EditValue = ""
            teCta03.EditValue = ""
            Exit Sub
        End If
        beCta03.EditValue = entCuentas.IdCuenta
        teCta03.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta04_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta04.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta04.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta04.EditValue = ""
            teCta04.EditValue = ""
            Exit Sub
        End If
        beCta04.EditValue = entCuentas.IdCuenta
        teCta04.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta05_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta05.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta05.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta05.EditValue = ""
            teCta05.EditValue = ""
            Exit Sub
        End If
        beCta05.EditValue = entCuentas.IdCuenta
        teCta05.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta06_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta06.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta06.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta06.EditValue = ""
            teCta06.EditValue = ""
            Exit Sub
        End If
        beCta06.EditValue = entCuentas.IdCuenta
        teCta06.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta07_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta07.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta07.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta07.EditValue = ""
            teCta07.EditValue = ""
            Exit Sub
        End If
        beCta07.EditValue = entCuentas.IdCuenta
        teCta07.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta08_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta08.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta08.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta08.EditValue = ""
            teCta08.EditValue = ""
            Exit Sub
        End If
        beCta08.EditValue = entCuentas.IdCuenta
        teCta08.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta09_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta09.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta09.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta09.EditValue = ""
            teCta09.EditValue = ""
            Exit Sub
        End If
        beCta09.EditValue = entCuentas.IdCuenta
        teCta09.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta10_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta10.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta10.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta10.EditValue = ""
            teCta10.EditValue = ""
            Exit Sub
        End If
        beCta10.EditValue = entCuentas.IdCuenta
        teCta10.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta11_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta11.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta11.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta11.EditValue = ""
            teCta11.EditValue = ""
            Exit Sub
        End If
        beCta11.EditValue = entCuentas.IdCuenta
        teCta11.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta12_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta12.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta12.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta12.EditValue = ""
            teCta12.EditValue = ""
            Exit Sub
        End If
        beCta12.EditValue = entCuentas.IdCuenta
        teCta12.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta13_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta13.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta13.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta13.EditValue = ""
            teCta13.EditValue = ""
            Exit Sub
        End If
        beCta13.EditValue = entCuentas.IdCuenta
        teCta13.EditValue = entCuentas.Nombre
    End Sub
    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue)
    End Sub
    Private Sub peFoto_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles peFoto.DoubleClick
        OpenFile.ShowDialog()
        FileName = OpenFile.FileName
        'beImagen.Text = OpenFile.FileName
        Dim bm As New Bitmap(FileName)
        peFoto.Image = bm
    End Sub

    Private Sub CargaImagen(ByVal FileName As String)
        If Not FileIO.FileSystem.FileExists(FileName) Or FileName = "" Then
            peFoto.Image = Nothing
            lcExisteArchivo.Text = "-DOBLE CLIC PARA CARGAR IMAGEN-"
        Else
            Dim bm As New Bitmap(FileName)
            peFoto.Image = bm
            lcExisteArchivo.Text = "-DOBLE CLIC PARA CAMBIAR IMAGEN-"
        End If
    End Sub

    Private Sub sbtnEliminarEmpresaActual_Click(sender As Object, e As EventArgs) Handles sbtnEliminarEmpresaActual.Click
        Dim ListEsquemas As New List(Of Esquemas)
        If MessageBox.Show("¿Está seguro de Eliminar la empresa Actual?", "SEGURIDAD ELIMINACION", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes Then
            Dim jsonschemas = System.IO.Directory.GetCurrentDirectory() & "\Nexus.xgg"
            If System.IO.File.Exists(jsonschemas) Then
                Dim jss As New JavaScriptSerializer()
                Dim jsonencry = System.IO.File.ReadAllText(jsonschemas)
                ListEsquemas = jss.Deserialize(Of List(Of Esquemas))(jsonencry.DesencriptarMNU())
            End If

            If MessageBox.Show("Confirme nuevamente para proceder con la eliminación...", "CONFIRMACION ELIMINACION", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.Yes Then
                SplashScreenManager.ShowForm(Me, WaitForm.GetType(), True, True, False)
                SplashScreenManager.Default.SetWaitFormCaption("Procesando Eliminación")
                Dim Esq = ListEsquemas.FirstOrDefault(Function(x) ("cns" & String.Format("{0:00}", x.IdDB)) = g_ConnectionString)
                Dim NameSchema As String = myBL.GetNameSchemaByUser(Esq.UserDB)
                SplashScreenManager.Default.SetWaitFormDescription(Esq.Nombre)
                Try
                    Dim dbf As SqlDatabase = Nothing

                    '''Establecer conexion a la base
                    Dim dbx As Database = Nothing
                    If NxSchemas = True Then
                        Dim settings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings
                        If Not settings Is Nothing Then
                            For Each cs As ConnectionStringSettings In settings
                                If cs.Name = "MultiEmpresa" Then
                                    dbx = DatabaseFactory.CreateDatabase(cs.Name)
                                End If
                            Next
                        End If
                    End If

                    If dbx Is Nothing Then
                        SplashScreenManager.CloseForm()
                        MessageBox.Show("Imposible continuar, no se encuentra configurada la conexion bilateral para continuar con la operacion", "FALLO EN CONFIGURACION", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Me.Enabled = True
                        Exit Sub
                    End If

                    Dim CNSx As String = DesencriptarCNS(dbx.ConnectionString)
                    dbf = New SqlDatabase(CNSx) ''Acceso como SA

                    Dim result As String = CType(dbf.ExecuteScalarWait("EXEC dbo.DropLogicDB64 '" & NameSchema & "'"), String)
                    If result.ToUpper() = "OK" Then
                        'myBL.EliminaEmpresa_Lista("cns" & String.Format("{0:00}", Esq.IdDB))
                        SplashScreenManager.Default.SetWaitFormCaption("Completado")
                        SplashScreenManager.Default.SetWaitFormDescription("Eliminacion Finalizada con exito!")
                        SplashScreenManager.CloseForm()
                        MessageBox.Show("Empresa Eliminada con Exito", "TERMINADO", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Close()
                    End If
                Catch ex As Exception
                    MessageBox.Show("Lo sentimos ha ocurrido un error en el proceso: " & vbCrLf & ex.Message, "FALLO EN ELIMINACION", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    SplashScreenManager.CloseForm()
                End Try
            End If
        End If
    End Sub

    Private Function AccessMaster(ByVal StringConexion As String, Optional namedb As String = "master")
        'Ejemplo de cadena:
        '( Data Source=tcp:jurosa.database.windows.net,1433;Initial Catalog=NEXUS_JURO;User ID=administrador@jurosa;Password=nexus123+ )
        Dim newcnx As String = String.Empty
        Dim sctemp As String = StringConexion.Replace(" ", "") _
                                             .ReplaceIgnoreCase("DataSource=", "") _
                                             .ReplaceIgnoreCase("InitialCatalog=", "") _
                                             .ReplaceIgnoreCase("UserID=", "") _
                                             .ReplaceIgnoreCase("Password=", "") _
                                             .ReplaceIgnoreCase("IntegratedSecurity=True", "")

        Dim RS = sctemp.Split(";")
        newcnx = String.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", RS(0), namedb, RS(2), RS(3))
        Return newcnx
    End Function
    'Return CType(db.ExecuteScalarWait("EXEC dbo.DropLogicDB64 '" & NameSchema & "'"), String)

    Public Function DesencriptarCNS(ByVal Input As String) As String
        Dim cs As String = Input
        Try
            Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("NexusKey") 'La clave debe ser de 8 caracteres
            Dim EncryptionKey() As Byte = Convert.FromBase64String("rpaSPvIvVLlrcmtzPU9/c67Gkj7yL1S9") 'No se puede alterar la cantidad de caracteres pero si la clave
            Dim buffer() As Byte = Convert.FromBase64String(Input)
            Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
            des.Key = EncryptionKey
            des.IV = IV
            cs = Encoding.UTF8.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length()))
        Catch ex As Exception
        End Try
        Return cs
    End Function
End Class

Public Class Esquemas
    Public Esquema As String
    Public Nombre As String
    Public Nrc As String
    Public UserDB As String
    Public PassDB As String
    Public IdDB As Integer
End Class