﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class adm_frmCorreosAlerta

    Private Sub rie_frmCorreosAlerta_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar éste correo electronico?", MsgBoxStyle.YesNo, "DBCoop v2013") = MsgBoxResult.No Then
            Return
        End If
        objTablas.fac_CorreosAlertaDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
        gc.DataSource = objTablas.fac_CorreosAlertaSelectAll
    End Sub

    Private Sub rie_frmCorreosAlerta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CargarCombos()
        gc.DataSource = objTablas.fac_CorreosAlertaSelectAll
    End Sub

    Private Sub CargarCombos()
        objCombos.adm_SujetoAlerta(leRubro)
    End Sub

    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        Dim entCorreo As New fac_CorreosAlerta
        Dim iFila As Integer
        If e.RowHandle < 0 Then
            DbMode = DbModeType.insert
            iFila = gv.RowCount - 1
        Else
            DbMode = DbModeType.update
            iFila = e.RowHandle
        End If
        If Not AllowInsert Or Not AllowEdit Then
            MsgBox("No le está permitido ingresar o editar información", MsgBoxStyle.Exclamation, Me.Text)
            gc.DataSource = objTablas.fac_CorreosAlertaSelectAll
            Exit Sub
        End If
        With (entCorreo)
            .IdCorreo = gv.GetRowCellValue(iFila, gv.Columns(0).FieldName)
            .Correo = gv.GetRowCellValue(iFila, gv.Columns(1).FieldName)
            .Contacto = gv.GetRowCellValue(iFila, gv.Columns(2).FieldName)
            .Cargo = gv.GetRowCellValue(iFila, gv.Columns(3).FieldName)
            .Tipo = gv.GetRowCellValue(iFila, gv.Columns(4).FieldName)
        End With
        If DbMode = DbModeType.insert Then
            objTablas.fac_CorreosAlertaInsert(entCorreo)
        Else
            objTablas.fac_CorreosAlertaUpdate(entCorreo)
        End If
    End Sub

    Private Sub rie_frmCorreosAlerta_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub


End Class
