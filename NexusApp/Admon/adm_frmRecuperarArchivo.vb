﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports System.Math
'Imports CajaBL

Public Class adm_frmRecuperarArchivo
    Dim bl As New AdmonBLL(g_ConnectionString)
    Dim fd As New FuncionesBLL(g_ConnectionString)
    Dim blFac As New FacturaBLL(g_ConnectionString)
    Dim dt As New DataTable()
    Dim FacturaHeader As List(Of fac_Ventas)
    Dim FacturaDetalle As List(Of fac_VentasDetalle)
    Dim FacturaHeaderFin As fac_Ventas
    Dim FacturaDetalleFin As List(Of fac_VentasDetalle)

    Private Sub adm_frmCierreDia_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_TiposComprobante(leIdTipoComprobante, "")
        leSucursal.EditValue = piIdSucursal

        gc.DataSource = Nothing
        gc.DataMember = ""
        gv.BestFitColumns()
    End Sub


    Private Sub btProceder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btProceder.Click
        gc.DataSource = bl.ObtieneFacturasInterface(leSucursal.EditValue, deDesde.EditValue, deHasta.EditValue)
        gv.BestFitColumns()
    End Sub

    Private Sub btAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAplicar.Click
        If MsgBox("Está seguro(a) de aplicar la facturación al sistema?", MsgBoxStyle.YesNo + MsgBoxStyle.Information) <> MsgBoxResult.Yes Then
            Exit Sub
        End If

        If Not DatosValidos() Then
            Exit Sub
        End If

        CargaEntidades() ' cargo todas las entiendas con lo que esta en el grid
        Ventas(FacturaHeader, FacturaDetalle)

    End Sub

    Private Function DatosValidos()

        Dim Incompleto As Boolean = False
        Dim SinNumero As Boolean = False

        For i = 0 To gv.DataRowCount - 1 ' 
            If gv.GetRowCellValue(i, "IdTipoComprobante") = 5 Then
                If gv.GetRowCellValue(i, "NombreCliente") = "" Or gv.GetRowCellValue(i, "Nit") = "" Or gv.GetRowCellValue(i, "Nrc") = "" Then
                    Incompleto = True
                End If
            End If

            If gv.GetRowCellValue(i, "Numero") = "" Then
                SinNumero = True
            End If
        Next

        If Incompleto Then
            MsgBox("Hay Créditos Fiscales con información incompleta", MsgBoxStyle.Critical, "Nota")
            Return False
        End If

        If SinNumero Then
            MsgBox("Hay Documentos sin números de comprobante", MsgBoxStyle.Critical, "Nota")
            Return False
        End If

        Return True
    End Function

    Private Sub CargaEntidades()
        FacturaHeader = New List(Of fac_Ventas)
        FacturaDetalle = New List(Of fac_VentasDetalle)

        For i = 0 To gv.DataRowCount - 1

            'AFECTO LA TABLA DE VENTAS
            Dim FacturaDetHeader = New fac_Ventas

            With FacturaDetHeader
                .IdComprobante = gv.GetRowCellValue(i, "NumTransaccion")
                Dim cNum As String = gv.GetRowCellValue(i, "Numero")

                If gv.GetRowCellValue(i, "IdTipoComprobante") = 5 Then
                    .Numero = cNum.Substring(2, 5)
                Else
                    .Numero = gv.GetRowCellValue(i, "Numero")
                End If

                .IdSucursal = leSucursal.EditValue
                .IdPunto = leSucursal.EditValue
                .IdTipoComprobante = gv.GetRowCellValue(i, "IdTipoComprobante")
                .Fecha = gv.GetRowCellValue(i, "Fecha")
                .IdCliente = "00001"
                .Nombre = gv.GetRowCellValue(i, "NombreCliente")
                .Nrc = gv.GetRowCellValue(i, "Nrc")
                .Nit = gv.GetRowCellValue(i, "Nit")
                .Giro = ""
                .Direccion = ""
                .Telefono = ""
                .VentaAcuentaDe = ""
                .IdMunicipio = "0614"
                .IdDepartamento = "06"
                .IdFormaPago = 1
                .DiasCredito = 0
                .IdVendedor = 1
                .TipoVenta = IIf(gv.GetRowCellValue(i, "IdTipoComprobante") = 7, 3, 1)
                .PorcDescto = 0.0
                .EsDevolucion = False
                .TotalDescto = 0.0
                .TotalComprobante = gv.GetRowCellValue(i, "Total")
                .TotalIva = IIf(gv.GetRowCellValue(i, "IdTipoComprobante") = 7, 0.0, gv.GetRowCellValue(i, "Total") - Round(gv.GetRowCellValue(i, "Total") / (pnIVA + 1), 2))
                .TotalImpuesto1 = 0.0
                .TotalImpuesto2 = 0.0
                .TotalNoSujeto = 0.0
                .TotalExento = 0.0

                If gv.GetRowCellValue(i, "IdTipoComprobante") = 5 Then
                    .TotalAfecto = Round(gv.GetRowCellValue(i, "Total") / (pnIVA + 1), 2)
                    .TotalNeto = Round(gv.GetRowCellValue(i, "Total") / (pnIVA + 1), 2)
                Else
                    If gv.GetRowCellValue(i, "IdTipoComprobante") = 7 Then ' EXPORTACION
                        .TotalAfecto = gv.GetRowCellValue(i, "Total")
                        .TotalNeto = gv.GetRowCellValue(i, "Total")
                    Else
                        .TotalAfecto = gv.GetRowCellValue(i, "Total")
                        .TotalNeto = Round(gv.GetRowCellValue(i, "Total") / (pnIVA + 1), 2)
                    End If

                End If

                .TotalPagado = 0.0
                .SaldoActual = gv.GetRowCellValue(i, "Total")

                'TODO Falta llenar estos valores
                .IdComprobanteNota = 0
                .OrdenCompra = ""
                .IdBodega = 1
                .MotivoAnulacion = ""
                .AnuladoPor = ""

                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
            End With
            FacturaHeader.Add(FacturaDetHeader)

            'AFECTO EL DETALLE DE LA FACTURACION

            Dim entDetalle As New fac_VentasDetalle
            With entDetalle
                .IdComprobante = gv.GetRowCellValue(i, "NumTransaccion")
                .IdProducto = "S/N"
                .IdPrecio = 1
                .Cantidad = 1
                .Descripcion = "VENTA DEL DIA SEGUN DOCUMENTOS ANEXOS"
                .PrecioVenta = gv.GetRowCellValue(i, "Total")
                .PorcDescuento = 0.0
                .ValorDescuento = 0.0
                .PrecioUnitario = gv.GetRowCellValue(i, "Total")
                .VentaNoSujeta = 0.0
                .VentaExenta = 0.0
                .VentaAfecta = gv.GetRowCellValue(i, "Total")

                If .VentaAfecta = gv.GetRowCellValue(i, "IdTipoComprobante") = 5 Then
                    .VentaNeta = gv.GetRowCellValue(i, "Total")
                Else
                    If gv.GetRowCellValue(i, "IdTipoComprobante") = 7 Then
                        .VentaNeta = gv.GetRowCellValue(i, "Total")
                    Else
                        .VentaNeta = Round(gv.GetRowCellValue(i, "Total") / (pnIVA + 1), 2)
                    End If
                End If

                .TipoImpuesto = 1
                .ValorIVA = IIf(gv.GetRowCellValue(i, "IdTipoComprobante") = 7, 0.0, gv.GetRowCellValue(i, "Total") - Round(gv.GetRowCellValue(i, "Total") / (pnIVA + 1), 2))
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .IdDetalle = 1
            End With
            FacturaDetalle.Add(entDetalle)
        Next
    End Sub

    Private Sub LlenarEntidadesFin(ByVal entVta As fac_Ventas)

        FacturaHeaderFin = New fac_Ventas
        FacturaDetalleFin = New List(Of fac_VentasDetalle)


            'AFECTO LA TABLA DE VENTAS

        With FacturaHeaderFin
            .IdComprobante = entVta.IdComprobante
            .Numero = entVta.Numero
            .IdSucursal = entVta.IdSucursal
            .IdPunto = entVta.IdPunto
            .IdTipoComprobante = entVta.IdTipoComprobante
            .Fecha = entVta.Fecha
            .IdCliente = entVta.IdCliente
            .Nombre = entVta.Nombre
            .Nrc = entVta.Nrc
            .Nit = entVta.Nit
            .Giro = entVta.Giro
            .Direccion = entVta.Direccion
            .Telefono = entVta.Telefono
            .VentaAcuentaDe = entVta.VentaAcuentaDe
            .IdMunicipio = entVta.IdMunicipio
            .IdDepartamento = entVta.IdDepartamento
            .IdFormaPago = entVta.IdFormaPago
            .DiasCredito = entVta.DiasCredito
            .IdVendedor = entVta.IdVendedor
            .TipoVenta = entVta.TipoVenta
            .PorcDescto = entVta.PorcDescto
            .EsDevolucion = entVta.EsDevolucion
            .TotalDescto = entVta.TotalDescto
            .TotalComprobante = entVta.TotalComprobante
            .TotalIva = entVta.TotalIva
            .TotalImpuesto1 = entVta.TotalImpuesto1
            .TotalImpuesto2 = entVta.TotalImpuesto2
            .TotalNoSujeto = entVta.TotalNoSujeto
            .TotalExento = entVta.TotalExento
            .TotalNeto = entVta.TotalNeto
            .TotalAfecto = entVta.TotalAfecto
            .TotalPagado = entVta.TotalPagado
            .SaldoActual = entVta.SaldoActual
            .IdComprobanteNota = entVta.IdComprobanteNota
            .OrdenCompra = entVta.OrdenCompra
            .IdBodega = entVta.IdBodega
            .MotivoAnulacion = entVta.MotivoAnulacion
            .AnuladoPor = entVta.AnuladoPor

            .CreadoPor = entVta.CreadoPor
            .FechaHoraCreacion = entVta.FechaHoraCreacion
            .ModificadoPor = entVta.ModificadoPor
            .FechaHoraModificacion = entVta.FechaHoraModificacion
        End With

        For Each detalle As fac_VentasDetalle In FacturaDetalle

            If detalle.IdComprobante = FacturaHeaderFin.IdComprobante Then
                Dim entDetalleFin As New fac_VentasDetalle

                With entDetalleFin
                    .IdComprobante = detalle.IdComprobante
                    .IdProducto = detalle.IdProducto
                    .IdPrecio = detalle.IdPrecio
                    .Cantidad = detalle.Cantidad
                    .Descripcion = detalle.Descripcion
                    .PrecioVenta = detalle.PrecioVenta
                    .PorcDescuento = detalle.PorcDescuento
                    .ValorDescuento = detalle.ValorDescuento
                    .PrecioUnitario = detalle.PrecioUnitario
                    .VentaNoSujeta = detalle.VentaNoSujeta
                    .VentaExenta = detalle.VentaExenta
                    .VentaAfecta = detalle.VentaAfecta
                    .VentaNeta = detalle.VentaNeta
                    .TipoImpuesto = detalle.TipoImpuesto
                    .ValorIVA = detalle.ValorIVA
                    .CreadoPor = detalle.CreadoPor
                    .FechaHoraCreacion = detalle.FechaHoraCreacion
                    .IdDetalle = detalle.IdDetalle
                End With
                FacturaDetalleFin.Add(entDetalleFin)
            End If

        Next
    End Sub
    Private Sub Ventas(ByRef FacturaHeader As List(Of fac_Ventas), ByRef FacturaDetalle As List(Of fac_VentasDetalle))

        For Each Header As fac_Ventas In FacturaHeader
            'lleno la entidad final que mandare uno por uno para insertarla
            LlenarEntidadesFin(Header)
            Dim msj As String = blFac.fac_InsertaFacturaImportacion(FacturaHeaderFin, FacturaDetalleFin)

            If msj = "" Then
            Else
                MsgBox("NO SE PUDO IMPORTAR LAS VENTAS" + Chr(13) + msj, MsgBoxStyle.Critical, "Error de Importación")
                Exit Sub
            End If

        Next
        MsgBox("La Importación se ha realizado con éxito", MsgBoxStyle.Information, "Nota")
    End Sub
End Class


