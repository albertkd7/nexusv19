﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adm_frmCambiarClave
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblMensaje = New DevExpress.XtraEditors.LabelControl()
        Me.teConPass = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.teNewPass = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.teId = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teAntPass = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.teConPass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNewPass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teAntPass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Controls.Add(Me.LabelControl15)
        Me.GroupControl1.Controls.Add(Me.LabelControl13)
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.LabelControl16)
        Me.GroupControl1.Controls.Add(Me.teAntPass)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.lblMensaje)
        Me.GroupControl1.Controls.Add(Me.teConPass)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.teNewPass)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.SimpleButton2)
        Me.GroupControl1.Controls.Add(Me.btGuardar)
        Me.GroupControl1.Controls.Add(Me.teId)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Size = New System.Drawing.Size(863, 339)
        Me.GroupControl1.Text = "Parámetros"
        '
        'lblMensaje
        '
        Me.lblMensaje.Appearance.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensaje.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblMensaje.Appearance.Options.UseFont = True
        Me.lblMensaje.Appearance.Options.UseForeColor = True
        Me.lblMensaje.Location = New System.Drawing.Point(131, 160)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(162, 23)
        Me.lblMensaje.TabIndex = 85
        Me.lblMensaje.Text = "Clave Vulnerable"
        Me.lblMensaje.Visible = False
        '
        'teConPass
        '
        Me.teConPass.EditValue = ""
        Me.teConPass.EnterMoveNextControl = True
        Me.teConPass.Location = New System.Drawing.Point(120, 98)
        Me.teConPass.Name = "teConPass"
        Me.teConPass.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.teConPass.Size = New System.Drawing.Size(196, 20)
        Me.teConPass.TabIndex = 4
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(7, 101)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(110, 13)
        Me.LabelControl7.TabIndex = 84
        Me.LabelControl7.Text = "Confirmar Contraseña:"
        '
        'teNewPass
        '
        Me.teNewPass.EditValue = ""
        Me.teNewPass.EnterMoveNextControl = True
        Me.teNewPass.Location = New System.Drawing.Point(120, 77)
        Me.teNewPass.Name = "teNewPass"
        Me.teNewPass.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.teNewPass.Size = New System.Drawing.Size(196, 20)
        Me.teNewPass.TabIndex = 3
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(23, 78)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl6.TabIndex = 83
        Me.LabelControl6.Text = "Nueva Contraseña:"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(223, 127)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(90, 28)
        Me.SimpleButton2.TabIndex = 6
        Me.SimpleButton2.Text = "&Regresar"
        '
        'btGuardar
        '
        Me.btGuardar.Location = New System.Drawing.Point(120, 127)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(90, 28)
        Me.btGuardar.TabIndex = 5
        Me.btGuardar.Text = "&Guardar"
        '
        'teId
        '
        Me.teId.EnterMoveNextControl = True
        Me.teId.Location = New System.Drawing.Point(120, 35)
        Me.teId.Name = "teId"
        Me.teId.Properties.ReadOnly = True
        Me.teId.Size = New System.Drawing.Size(196, 20)
        Me.teId.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(49, 38)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Identificación:"
        '
        'teAntPass
        '
        Me.teAntPass.EditValue = ""
        Me.teAntPass.EnterMoveNextControl = True
        Me.teAntPass.Location = New System.Drawing.Point(120, 56)
        Me.teAntPass.Name = "teAntPass"
        Me.teAntPass.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.teAntPass.Size = New System.Drawing.Size(196, 20)
        Me.teAntPass.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(15, 57)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl2.TabIndex = 87
        Me.LabelControl2.Text = "Contraseña Anterior:"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl9.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Appearance.Options.UseForeColor = True
        Me.LabelControl9.Location = New System.Drawing.Point(336, 115)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(237, 11)
        Me.LabelControl9.TabIndex = 109
        Me.LabelControl9.Text = "5- La contraseña debe ser diferente a las últimas 6 usadas"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl15.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Appearance.Options.UseForeColor = True
        Me.LabelControl15.Location = New System.Drawing.Point(336, 96)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(260, 11)
        Me.LabelControl15.TabIndex = 108
        Me.LabelControl15.Text = "4- La contraseña debe de tener un caracter especial (#$%&+{? )"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl13.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Appearance.Options.UseForeColor = True
        Me.LabelControl13.Location = New System.Drawing.Point(336, 77)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(174, 11)
        Me.LabelControl13.TabIndex = 107
        Me.LabelControl13.Text = "3- La contraseña debe de tener un número"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl12.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Appearance.Options.UseForeColor = True
        Me.LabelControl12.Location = New System.Drawing.Point(336, 58)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(213, 11)
        Me.LabelControl12.TabIndex = 106
        Me.LabelControl12.Text = "2- La contraseña debe de tener una letra Mayúscula"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl10.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Appearance.Options.UseForeColor = True
        Me.LabelControl10.Location = New System.Drawing.Point(336, 39)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(257, 11)
        Me.LabelControl10.TabIndex = 104
        Me.LabelControl10.Text = "1- La longitud menor de la contraseña debe ser de 6 caracteres"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl16.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Appearance.Options.UseForeColor = True
        Me.LabelControl16.Location = New System.Drawing.Point(336, 25)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(153, 11)
        Me.LabelControl16.TabIndex = 103
        Me.LabelControl16.Text = "* POLITICAS DE CONTRASEÑAS"
        '
        'adm_frmCambiarClave
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(863, 367)
        Me.Modulo = "Administración"
        Me.Name = "adm_frmCambiarClave"
        Me.Text = "Cambiar Clave de Usuarios"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.teConPass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNewPass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teAntPass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents teAntPass As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblMensaje As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teConPass As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNewPass As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teId As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl

End Class
