﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class adm_frmCambiarClave
    Dim bl As New AdmonBLL(g_ConnectionString)
    Dim entUser As New adm_Usuarios
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim blFac = New FacturaBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()


    Private Sub adm_frmCambiarClave_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        entUser = objTablas.adm_UsuariosSelectByPK(objMenu.User)
        teId.EditValue = entUser.IdUsuario
    End Sub


    Private Sub btGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        If teNewPass.EditValue = "" Then
            MsgBox("Debe de especificar la Nueva contraseña del usuario", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        If teAntPass.EditValue = "" Then
            MsgBox("Debe de especificar la contraseña anterior del usuario", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        If teConPass.EditValue = "" Then
            MsgBox("Debe de Confirmar la Nueva contraseña del usuario", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If teConPass.EditValue <> teNewPass.EditValue Then
            MsgBox("La Nueva Contraseña no coincide con la confirmación", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim Password As String = teNewPass.EditValue
        If Password.Length < 6 Then
            MsgBox("La contraseña debe tener 6 caracteres como mínimo", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If Password.Length > 150 Then
            MsgBox("La contraseña debe tener máximo 150 caracteres", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        'VERIFICO QUE LA CONTRASEÑA TENGA UN NUMERO, UNA MAYUSCULA, Y UN CARACTER ESPECIAL
        Dim ContieneNumero As Boolean = False
        Dim ContieneMayus As Boolean = False
        Dim ContineEspecial As Boolean = False
        Dim ClaveUtilizada As Boolean = False

        Dim len As Integer = Password.Length - 1
        For i As Integer = 0 To len
            Dim ch As Char = Password(i)
            If Char.IsNumber(ch) Then   'Si el caracter es un numero
                ContieneNumero = True
            End If
            If Char.IsUpper(ch) Then    'Si es mayuscula
                ContieneMayus = True
            End If
            If InStr(dtParam.Rows(0).Item("Caracteres"), ch) = 0 Then    'Si es mayuscula
                ContineEspecial = True
            End If
        Next

        If Not ContieneMayus Or Not ContieneNumero Or Not ContineEspecial Then
            lblMensaje.Visible = True
            lblMensaje.Text = "Clave Vulnerable"
            Exit Sub
        Else
            lblMensaje.Visible = True
            lblMensaje.Text = "Clave Segura"
        End If

        'VERIFICO QUE LA CONTRASEÑA ANTERIOR SEA CORRECTA
        Dim Provider As New Security.Cryptography.SHA1CryptoServiceProvider
        Dim Bytes As Byte() = System.Text.Encoding.UTF8.GetBytes(teAntPass.EditValue)
        Dim inArray As Byte() = Provider.ComputeHash(Bytes)
        Provider.Clear()

        Dim sAnteriorPassWord As String = Convert.ToBase64String(inArray)
        If sAnteriorPassWord <> entUser.Clave Then
            MsgBox("La contraseña Anterior no coincide", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim ProviderNew As New Security.Cryptography.SHA1CryptoServiceProvider
        Dim BytesNew As Byte() = System.Text.Encoding.UTF8.GetBytes(teNewPass.EditValue)
        Dim inArrayNew As Byte() = ProviderNew.ComputeHash(BytesNew)
        ProviderNew.Clear()

        'encripto el password
        Dim sPassWord As String = Convert.ToBase64String(inArrayNew)

        'VERIFICO QUE LA NUEVA CLAVE NO SEA DE LAS ULTIMAS 6 CREADAS
        Dim dtUltimasClaves As DataTable = bl.ObtenerUltimasClaves(teId.EditValue)
        For i = 0 To dtUltimasClaves.Rows.Count - 1
            If (dtUltimasClaves.Rows(i).Item("Clave")) = sPassWord Then
                ClaveUtilizada = True
            End If
        Next

        If ClaveUtilizada Then
            MsgBox("La contraseña debe ser diferente a las ultimas 6 utilizadas", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If


        Dim FechaServer As Date = blFac.fac_FechaServer()
        entUser.IdUsuario = teId.EditValue
        entUser.Clave = sPassWord
        entUser.FechaUltCambioClave = FechaServer

        Try
            objTablas.adm_UsuariosUpdate(entUser)
            blAdmon.HistorialClaves(teId.EditValue, sPassWord)
            MsgBox("La clave ha sido guardada", MsgBoxStyle.Information, "Nota")
        Catch ex As Exception
            MsgBox("No fue posible guardar el cambio de clave" & Chr(13) & ex.Message(), 16, "Verifique el error")
            Exit Sub
        End Try

        Me.Close()
    End Sub

    Private Sub SimpleButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
End Class
