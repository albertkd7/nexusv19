﻿Imports NexusBLL
Imports System.IO
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web.Script.Serialization

Public Class adm_frmLogin
    Private NumIntentos As Integer = 0, AccesoPermitido As Boolean
    Dim bl As AdmonBLL, blFac As FacturaBLL
    Dim dtUsuario As New DataTable

    Private _Timer As Timer
    Dim NexusControlXml As New System.Xml.XmlDocument
    Dim xmlLocation As String = Application.StartupPath & "\nexus-control.xml"
    Dim NxUpActive As Boolean = False
    Dim NxSchemas As Boolean = IIf(System.Configuration.ConfigurationManager.AppSettings("NxSchemas:Disabled") = "True", True, False)
    Dim NxLimitSchemas As String = System.Configuration.ConfigurationManager.AppSettings("NxSchemas:Limit")

    Public Sub New()
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        _Timer = New Timer()
        _Timer.Interval = 1000
        AddHandler _Timer.Tick, AddressOf Timer_tick
    End Sub

    Private Sub Timer_tick(sender As Object, e As EventArgs)
        Try
            If System.IO.File.Exists(xmlLocation) Then
                NexusControlXml.Load(xmlLocation)
                Boolean.TryParse(NexusControlXml.SelectSingleNode("nexus").Attributes("active").Value, NxUpActive)
                If NxUpActive = True Then
                    Dispose()
                    Close()
                    End
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub adm_frmLogin_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Not AccesoPermitido Then
            End
        End If
    End Sub

    Private Sub frmLogin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargaCombo()
        cboEmpresa.SelectedIndex = 0
        DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle(My.Settings.SkinName)
        _Timer.Start()

        If NxSchemas = True Then
            Dim jss As New JavaScriptSerializer()
            cboEmpresa.Items.Clear()

            bl = New AdmonBLL(g_ConnectionString)
            Dim query = bl.GetSchemasDB() _
                          .AsEnumerable() _
                          .Select(Function(x) New With {
                            Key .Esquema = x.Field(Of String)("Esquema"),
                            Key .Nombre = x.Field(Of String)("Nombre"),
                            Key .Nrc = x.Field(Of String)("Nrc"),
                            Key .UserDB = x.Field(Of String)("UserDB"),
                            Key .PassDB = x.Field(Of String)("PassDB"),
                            Key .IdDB = x.Field(Of Integer)("IdDB")
                          }).ToList()

            Dim jsonschemas = Application.StartupPath & "\Nexus.xgg"
            If System.IO.File.Exists(jsonschemas) Then
                System.IO.File.Delete(jsonschemas)
            End If

            Dim ow As New System.IO.StreamWriter(jsonschemas)
            ow.WriteLine(jss.Serialize(query).EncriptarMNU())
            ow.Close()

            If Not String.IsNullOrEmpty(NxLimitSchemas) Then
                If Not NxLimitSchemas.ToUpper().Contains("ALL") Then
                    Dim listLimit = NxLimitSchemas.Split(",").Select(Function(x) New With {Key .IdL = CType(x, Integer)}).ToList()
                    query = query.Where(Function(x) listLimit.Any(Function(y) y.IdL = x.IdDB) = True).ToList()
                End If
            End If

            For Each q In query
                Dim Empresa As New TipoEmpresa
                Empresa.cnString = "cns" & String.Format("{0:00}", q.IdDB)
                Empresa.Nombre = q.IdDB.ToString() & ". " & q.Nombre
                cboEmpresa.Items.Add(Empresa)
            Next
            cboEmpresa.SelectedIndex = 0
        End If
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        g_ConnectionString = CType(cboEmpresa.SelectedItem, TipoEmpresa).cnString
    End Sub
    Private Sub CargaCombo()
        Dim sr As New StreamReader("App.Ini")

        While (Not sr.EndOfStream)
            Dim linea As String = sr.ReadLine
            Dim Empresa As New TipoEmpresa
            Empresa.cnString = linea.Substring(0, linea.IndexOf(","))
            Empresa.Nombre = linea.Substring(linea.IndexOf(",") + 1)
            cboEmpresa.Items.Add(Empresa)
        End While

    End Sub

    Private Sub sbOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbOk.Click
        bl = New AdmonBLL(g_ConnectionString)
        Dim dt As DataTable = bl.ObtieneParametros()


        Using provider As New Security.Cryptography.SHA1CryptoServiceProvider()

            Dim bytesTmp As Byte() = System.Text.Encoding.UTF8.GetBytes(tePassword.Text)
            Dim bytesNit As Byte() = System.Text.Encoding.UTF8.GetBytes(dt.Rows(0).Item("NitEmpresa"))
            Dim bytesNrc As Byte() = System.Text.Encoding.UTF8.GetBytes(dt.Rows(0).Item("NrcEmpresa"))

            Dim inArrayTmp As Byte() = provider.ComputeHash(bytesTmp)
            Dim inArrayNit As Byte() = provider.ComputeHash(bytesNit)
            Dim inArrayNrc As Byte() = provider.ComputeHash(bytesNrc)

            'encripto el password, nit y nrc
            Dim sPas As String = Convert.ToBase64String(inArrayTmp)
            Dim sNit As String = Convert.ToBase64String(inArrayNit)
            Dim sNrc As String = Convert.ToBase64String(inArrayNrc)

            'bl = New AdmonBLL(g_ConnectionString)
            Dim msj As String = bl.ValidarUsuario(teUserName.Text, sPas)
            If msj <> "Ok" Then
                If msj = "No" Then
                    MsgBox("Usuario o contraseña mal ingresados", MsgBoxStyle.Critical)
                    NumIntentos += 1
                    bl.IngresoFallido(teUserName.Text, System.Net.Dns.GetHostName(), NumIntentos) 'guardo los itentos fallidos
                    If NumIntentos = 3 Then
                        Dispose()
                        Close()

                        End

                    End If
                    Return
                Else
                    MsgBox(String.Format("SE DETECTO UN ERROR DE CONEXIÓN{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Nota")
                    Return
                End If
            End If

            With dt.Rows(0)
                bytesTmp = System.Text.Encoding.UTF8.GetBytes("remle3972" + .Item("NombreEmpresa") + "REMLE3972")
                bytesNit = System.Text.Encoding.UTF8.GetBytes("remle3972" + .Item("NitEmpresa") + "REMLE3972")
                bytesNrc = System.Text.Encoding.UTF8.GetBytes("remle3972" + .Item("NrcEmpresa") + "REMLE3972")

                inArrayTmp = provider.ComputeHash(bytesTmp)
                inArrayNit = provider.ComputeHash(bytesNit)
                inArrayNrc = provider.ComputeHash(bytesNrc)

                If NxSchemas Or (Convert.ToBase64String(inArrayTmp) = .Item("DatoEncriptado2") And Convert.ToBase64String(inArrayNrc) = .Item("DatoEncriptado1") And Convert.ToBase64String(inArrayNit) = .Item("DatoEncriptado3")) Then
                    'ok... copia legal
                    'DatoEncriptado1= el nrc
                    'DatoEncriptado2= la empresa
                    'DatoEncriptado3= el Nit

                    'Dim emp As String = .Item("DatoEncriptado2")
                    'emp = System.Text.UTF8Encoding.GetEncoding
                    _Timer.Stop()
                Else
                    MsgBox(String.Format("No está autorizado a usar ésta aplicación{0}{1}", Chr(13), "Favor contacte con I. T. O."), MsgBoxStyle.Critical, "Nota")
                    Dispose()
                    Close()
                    End

                End If

            End With
            provider.Clear()
        End Using

        dtUsuario = bl.ExtraeUsuario(teUserName.Text)

        If Not ValidaVigenciaUsuario() Then
            Dispose()
            Close()
            End
        End If

        'Dim dtGestiones As DataTable = bl.GestionesProgramadas(Today, Today, SiEsNulo(dtUsuario.Rows(0).Item("IdVendedor"), 0))

        AccesoPermitido = True
        piIdVendedor = SiEsNulo(dtUsuario.Rows(0).Item("IdVendedor"), -1)
        piIdSucursalUsuario = SiEsNulo(dtUsuario.Rows(0).Item("IdSucursal"), 1)
        CrearObjetos()
        CrearVariablesGlobales()
        objMenu.User = teUserName.Text
        Application.EnableVisualStyles()

        'If dtGestiones.Rows.Count > 0 Then
        '    If MsgBox("¿Generar reporte de gestiones programadas para este día?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
        '        Close()
        '    End If
        '    Dim rpt As New fac_rptGestionProgramada
        '    rpt.DataSource = dtGestiones
        '    rpt.DataMember = ""
        '    rpt.xrlEmpresa.Text = gsNombre_Empresa
        '    rpt.xrlTitulo.Text = "LISTADO DE GESTIONES PROGRAMADAS"
        '    rpt.xrlPeriodo.Text = FechaToString(Today, Today)
        '    rpt.ShowPreviewDialog()
        '    Close()
        'Else
        '    Close()
        'End If
        Close()
    End Sub

    Private Sub sbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCancel.Click
        Dispose()
        Close()
        End
    End Sub

    Private Function ValidaVigenciaUsuario()
        Dim msj As String = ""
        blFac = New FacturaBLL(g_ConnectionString)
        'Dim FechaServer As Date = blFac.fac_FechaServer() REACTIVAR LUEGO

        If dtUsuario.Rows(0).Item("Estado") = 0 Then
            msj = "El usuario esta Inactivo"
            MsgBox(msj + Chr(13) + "Favor contacte al Administrador del Sistema", MsgBoxStyle.Critical, "Nota")
            Return False
        End If
        'If dtUsuario.Rows(0).Item("FechaVencimiento") <= FechaServer Then
        '    msj = "Cuenta de usuario Caducada"
        '    MsgBox(msj + Chr(13) + "Favor contacte al Administrador del Sistema", MsgBoxStyle.Critical, "Nota")
        '    Return False
        'End If
        'Dim FechaVence As Date = dtUsuario.Rows(0).Item("FechaUltCambioClave")
        'FechaVence = DateAdd(DateInterval.Day, dtUsuario.Rows(0).Item("VigenciaClave"), FechaVence)

        'If FechaVence <= FechaServer Then
        '    MsgBox("Clave caducada, necesita cambiar su clave ahora", MsgBoxStyle.Information, "Nota")
        '    adm_frmModificaClave.IdUsuario = teUserName.EditValue
        '    adm_frmModificaClave.lblMensaje.Text = "Clave Vulnerable"
        '    adm_frmModificaClave.lblMensaje.Visible = False
        '    adm_frmModificaClave.teNewPass.EditValue = ""
        '    adm_frmModificaClave.teConPass.EditValue = ""
        '    adm_frmModificaClave.ShowDialog()

        '    If Not adm_frmModificaClave.Acceso Then
        '        msj = "Clave no Aceptada"
        '    End If
        'End If

        If msj <> "" Then
            MsgBox(msj + Chr(13) + "Favor contacte al Administrador del Sistema", MsgBoxStyle.Critical, "Nota")
            Return False
        End If

        Return True
    End Function

    Private Sub tePassword_GotFocus(sender As Object, e As EventArgs) Handles tePassword.GotFocus, teUserName.GotFocus
        lblMayuscula.Visible = My.Computer.Keyboard.CapsLock
    End Sub
    Private Sub Text_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tePassword.KeyDown, teUserName.KeyDown
        If e.KeyCode = Keys.CapsLock Then
            lblMayuscula.Visible = My.Computer.Keyboard.CapsLock
        End If
    End Sub

End Class
Public Class TipoEmpresa
    Public Nombre As String
    Public cnString As String

    Public Overrides Function ToString() As String
        Return Nombre
    End Function

End Class
