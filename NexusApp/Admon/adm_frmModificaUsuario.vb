﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class adm_frmModificaUsuario
    Dim bl As New AdmonBLL(g_ConnectionString)
    Dim blFac As New FacturaBLL(g_ConnectionString)
    Dim entUser As New adm_Usuarios
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim _IdUsuario As String = 0
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Public Property IdUsuario() As String
        Get
            Return _IdUsuario
        End Get
        Set(ByVal value As String)
            _IdUsuario = value
        End Set
    End Property


    Private Sub adm_frmModificaUsuario_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "--AUTO-ELEGIBLE--")
        objCombos.inv_Precios(leTipoPrecio, "")
        objCombos.adm_Sucursales(leSucursalUsuario, objMenu.User, "")
        objCombos.inv_Bodegas(leBodega, "")
        objCombos.fac_Vendedores(leVendedor, "--NO APLICA--")
        objCombos.adm_Usuarios(LeUsuarios)
        entUser = objTablas.adm_UsuariosSelectByPK(IdUsuario)
        gcPrecios.DataSource = bl.ObtenerPreciosUsuario(IdUsuario)
        gcSucursales.DataSource = bl.ObtenerSucursalesUsuario(IdUsuario)
        LeUsuarios.EditValue = "ADMINISTRADOR"
        'clbListaPrecios.DataSource = 
        'clbListaPrecios.DisplayMember = "Nombre"
        'clbListaPrecios.ValueMember = "IdPrecio"
        teId.EditValue = entUser.IdUsuario
        'tePass.EditValue = entUser.Clave
        teApellidos.EditValue = entUser.Apellidos
        teNombres.EditValue = entUser.Nombre
        leSucursal.EditValue = entUser.IdSucursal
        lePuntoVenta.EditValue = entUser.IdPunto
        leBodega.EditValue = entUser.IdBodega
        leTipoPrecio.EditValue = entUser.IdTipoPrecio
        deFecha.EditValue = entUser.FechaVencimiento
        seDias.EditValue = entUser.VigenciaClave
        ceActivo.EditValue = entUser.Estado
        chkCambiarPrecios.EditValue = entUser.CambiarPrecios
        chkCambiaDescripciones.EditValue = entUser.AccesoProductos
        ceCredito.EditValue = entUser.AutorizaCredito
        ceTodasSucursales.EditValue = entUser.TodasLasSucursales
        ceAutorizaDescuentos.EditValue = entUser.AutorizarDescuentos
        ceBloqueoBodega.EditValue = entUser.BloquearBodega
        chkRevertirInventarios.EditValue = entUser.AutorizarRevertir
        seDesdeDescto.EditValue = entUser.DesdeDescuentos
        seHastaDescto.EditValue = entUser.HastaDescuentos
        leVendedor.EditValue = entUser.IdVendedor
    End Sub

    Private Sub btGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        'If tePass.EditValue = "" Then
        '    MsgBox("Debe de especificar la contraseña del usuario", MsgBoxStyle.Information, "Nota")
        '    Exit Sub
        'End If
        Dim entUser As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(IdUsuario)
        Dim FechaServer As Date = blFac.fac_FechaServer()

        entUser.IdUsuario = teId.EditValue
        entUser.Nombre = teNombres.EditValue
        entUser.Apellidos = teApellidos.EditValue
        'entUser.Clave = tePass.EditValue
        entUser.FechaCreacion = Today
        entUser.FechaVencimiento = deFecha.EditValue
        entUser.FechaUltCambioClave = FechaServer
        entUser.VigenciaClave = seDias.EditValue
        entUser.IdSucursal = leSucursal.EditValue
        entUser.IdBodega = leBodega.EditValue
        entUser.IdTipoPrecio = leTipoPrecio.EditValue
        entUser.Estado = ceActivo.Checked
        entUser.CambiarPrecios = chkCambiarPrecios.EditValue
        entUser.AccesoProductos = chkCambiaDescripciones.EditValue
        entUser.AutorizaCredito = ceCredito.EditValue
        entUser.TodasLasSucursales = ceTodasSucursales.EditValue
        entUser.AutorizarDescuentos = ceAutorizaDescuentos.EditValue
        entUser.DesdeDescuentos = seDesdeDescto.EditValue
        entUser.HastaDescuentos = seHastaDescto.EditValue
        entUser.AutorizarRevertir = chkRevertirInventarios.EditValue
        entUser.BloquearBodega = ceBloqueoBodega.EditValue
        entUser.IdVendedor = leVendedor.EditValue
        entUser.IdPunto = lePuntoVenta.EditValue

        Try
            objTablas.adm_UsuariosUpdate(entUser)
            bl.EliminarPreciosUsuario(IdUsuario)
            bl.EliminarSucursalesUsuario(IdUsuario)

            For i = 0 To gvPrecios.DataRowCount - 1
                If gvPrecios.GetRowCellValue(i, "Acceso") Then
                    bl.InsertarPrecioUsuario(teId.EditValue, gvPrecios.GetRowCellValue(i, "IdPrecio"), objMenu.User)
                End If
            Next
            For i = 0 To gvSucursales.DataRowCount - 1
                If gvSucursales.GetRowCellValue(i, "Acceso") Then
                    bl.InsertarSucursalUsuario(teId.EditValue, gvSucursales.GetRowCellValue(i, "IdSucursal"), objMenu.User)
                End If
            Next
            MsgBox("El usuario ha sido guardado", MsgBoxStyle.Information, "Nota")
        Catch ex As Exception
            MsgBox("No fue posible guardar el usuario" & Chr(13) & ex.Message(), 16, "Verifique el error")
            Exit Sub
        End Try
        Me.Close()
    End Sub

    'Private Sub SimpleButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SimpleButton2.Click
    '    Me.Close()
    'End Sub

    Private Sub sbResetear_Click(sender As Object, e As EventArgs) Handles sbResetear.Click
        If MsgBox("Está seguro de re-setear la contraseña?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim Provider As New Security.Cryptography.SHA1CryptoServiceProvider
        Dim Bytes As Byte() = System.Text.Encoding.UTF8.GetBytes("Nexus1$")
        Dim inArray As Byte() = Provider.ComputeHash(Bytes)
        Provider.Clear()

        Dim sPassWord As String = Convert.ToBase64String(inArray)
        Dim entUser As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(IdUsuario)

        entUser.Clave = sPassWord
        entUser.Estado = True
        objTablas.adm_UsuariosUpdate(entUser)
        MsgBox("La contraseña reseteada es ahora --> Nexus1$" & Chr(13) & "Asegurese que el usuario cambie la contraseña en la próxima sesión", MsgBoxStyle.Information, "Nota")
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        If IdUsuario = LeUsuarios.EditValue Then
            MsgBox("No puede seleccionar el Usuario Actual ", MsgBoxStyle.Information, "Nota")
        End If
        If MsgBox("Está seguro de Copiar los Permisos del usuario :" & LeUsuarios.Text & " Al Usuario Actual ?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        bl.AsignaPermisosDeUsuarioAsuario(IdUsuario, LeUsuarios.EditValue)
        MsgBox("Los Permisos se Asignaron con Exito ", MsgBoxStyle.Information, "Nota")

    End Sub

    Private Sub leSucursal_EditValueChanged(sender As Object, e As EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "--AUTO-ELEGIBLE--")
    End Sub
End Class