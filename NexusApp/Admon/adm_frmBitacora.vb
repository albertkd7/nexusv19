﻿
Imports NexusBLL
Imports System.Math
Public Class adm_frmBitacora
    Dim bl As New AdmonBLL(g_ConnectionString)


    Private Sub frmLibroConsumidor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.adm_Modulos(leModulo, "-- TODOS LOS MODULOS --")
        objCombos.adm_Usuarios(leUsuario, "-- TODOS LOS USUARIOS --")
    End Sub


    Private Sub Rep_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.adm_rptBitacora(deDesde.EditValue, deHasta.EditValue, leModulo.EditValue, leUsuario.EditValue)

        Dim rpt As New adm_rptBitacora() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt.ShowPreviewDialog()

    End Sub


End Class
