﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adm_frmActividades
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(adm_frmActividades))
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdRubro = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leRubro = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPeso = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.tePeso = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcEnviaAlerta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkEmite = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.gcApnfd = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkApnfd = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leRubro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePeso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkEmite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkApnfd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.leRubro, Me.tePeso, Me.chkEmite, Me.chkApnfd})
        Me.gc.Size = New System.Drawing.Size(786, 335)
        Me.gc.TabIndex = 5
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdRubro, Me.gcId, Me.gcNombre, Me.gcPeso, Me.gcEnviaAlerta, Me.gcApnfd})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.NewItemRowText = "Click aquí para ingresar nueva forma de pago. Esc para cancelar"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdRubro
        '
        Me.gcIdRubro.Caption = "Rubro"
        Me.gcIdRubro.ColumnEdit = Me.leRubro
        Me.gcIdRubro.FieldName = "IdRubro"
        Me.gcIdRubro.Name = "gcIdRubro"
        Me.gcIdRubro.Visible = True
        Me.gcIdRubro.VisibleIndex = 0
        Me.gcIdRubro.Width = 133
        '
        'leRubro
        '
        Me.leRubro.AutoHeight = False
        Me.leRubro.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leRubro.Name = "leRubro"
        '
        'gcId
        '
        Me.gcId.Caption = "Id. Actividad"
        Me.gcId.FieldName = "IdActividad"
        Me.gcId.Name = "gcId"
        Me.gcId.Visible = True
        Me.gcId.VisibleIndex = 1
        Me.gcId.Width = 95
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 2
        Me.gcNombre.Width = 308
        '
        'gcPeso
        '
        Me.gcPeso.Caption = "Peso"
        Me.gcPeso.ColumnEdit = Me.tePeso
        Me.gcPeso.FieldName = "Peso"
        Me.gcPeso.Name = "gcPeso"
        '
        'tePeso
        '
        Me.tePeso.AutoHeight = False
        Me.tePeso.Mask.EditMask = "n2"
        Me.tePeso.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tePeso.Name = "tePeso"
        '
        'gcEnviaAlerta
        '
        Me.gcEnviaAlerta.Caption = "Envia Alerta"
        Me.gcEnviaAlerta.ColumnEdit = Me.chkEmite
        Me.gcEnviaAlerta.FieldName = "EmiteAlerta"
        Me.gcEnviaAlerta.Name = "gcEnviaAlerta"
        '
        'chkEmite
        '
        Me.chkEmite.AutoHeight = False
        Me.chkEmite.Name = "chkEmite"
        '
        'gcApnfd
        '
        Me.gcApnfd.Caption = "APNFD"
        Me.gcApnfd.ColumnEdit = Me.chkApnfd
        Me.gcApnfd.FieldName = "Apnfd"
        Me.gcApnfd.Name = "gcApnfd"
        '
        'chkApnfd
        '
        Me.chkApnfd.AutoHeight = False
        Me.chkApnfd.Name = "chkApnfd"
        '
        'adm_frmActividades
        '
        Me.ClientSize = New System.Drawing.Size(882, 360)
        Me.Controls.Add(Me.gc)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Administración"
        Me.Name = "adm_frmActividades"
        Me.OptionId = "002001"
        Me.Text = "Actividades Económicas de los Asociados"
        Me.TipoFormulario = 2
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leRubro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePeso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkEmite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkApnfd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdRubro As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leRubro As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents gcPeso As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tePeso As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcEnviaAlerta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkEmite As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents gcApnfd As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkApnfd As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit

End Class
