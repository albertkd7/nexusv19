﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adm_frmModificaClave
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(adm_frmModificaClave))
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.teId = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teNewPass = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.teConPass = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.lblMensaje = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.teId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNewPass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teConPass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(221, 79)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(90, 28)
        Me.SimpleButton2.TabIndex = 4
        Me.SimpleButton2.Text = "&Regresar"
        '
        'btGuardar
        '
        Me.btGuardar.Location = New System.Drawing.Point(118, 79)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(90, 28)
        Me.btGuardar.TabIndex = 3
        Me.btGuardar.Text = "&Guardar"
        '
        'teId
        '
        Me.teId.Location = New System.Drawing.Point(117, 3)
        Me.teId.Name = "teId"
        Me.teId.Properties.ReadOnly = True
        Me.teId.Size = New System.Drawing.Size(196, 20)
        Me.teId.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(46, 6)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl1.TabIndex = 40
        Me.LabelControl1.Text = "Identificación:"
        '
        'teNewPass
        '
        Me.teNewPass.EditValue = ""
        Me.teNewPass.Location = New System.Drawing.Point(117, 27)
        Me.teNewPass.Name = "teNewPass"
        Me.teNewPass.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.teNewPass.Size = New System.Drawing.Size(196, 20)
        Me.teNewPass.TabIndex = 1
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(20, 30)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl6.TabIndex = 52
        Me.LabelControl6.Text = "Nueva Contraseña:"
        '
        'teConPass
        '
        Me.teConPass.EditValue = ""
        Me.teConPass.Location = New System.Drawing.Point(117, 50)
        Me.teConPass.Name = "teConPass"
        Me.teConPass.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.teConPass.Size = New System.Drawing.Size(196, 20)
        Me.teConPass.TabIndex = 2
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(4, 53)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(110, 13)
        Me.LabelControl7.TabIndex = 54
        Me.LabelControl7.Text = "Confirmar Contraseña:"
        '
        'lblMensaje
        '
        Me.lblMensaje.Appearance.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensaje.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblMensaje.Appearance.Options.UseFont = True
        Me.lblMensaje.Appearance.Options.UseForeColor = True
        Me.lblMensaje.Location = New System.Drawing.Point(129, 112)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(162, 23)
        Me.lblMensaje.TabIndex = 76
        Me.lblMensaje.Text = "Clave Vulnerable"
        Me.lblMensaje.Visible = False
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl9.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Appearance.Options.UseForeColor = True
        Me.LabelControl9.Location = New System.Drawing.Point(331, 95)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(242, 11)
        Me.LabelControl9.TabIndex = 109
        Me.LabelControl9.Text = "5- La contraseña debe ser diferente a las últimas 12 usadas"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl15.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Appearance.Options.UseForeColor = True
        Me.LabelControl15.Location = New System.Drawing.Point(331, 76)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(260, 11)
        Me.LabelControl15.TabIndex = 108
        Me.LabelControl15.Text = "4- La contraseña debe de tener un caracter especial (#$%&+{? )"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl13.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Appearance.Options.UseForeColor = True
        Me.LabelControl13.Location = New System.Drawing.Point(331, 57)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(174, 11)
        Me.LabelControl13.TabIndex = 107
        Me.LabelControl13.Text = "3- La contraseña debe de tener un número"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl12.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Appearance.Options.UseForeColor = True
        Me.LabelControl12.Location = New System.Drawing.Point(331, 38)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(213, 11)
        Me.LabelControl12.TabIndex = 106
        Me.LabelControl12.Text = "2- La contraseña debe de tener una letra Mayúscula"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl10.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Appearance.Options.UseForeColor = True
        Me.LabelControl10.Location = New System.Drawing.Point(331, 20)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(257, 11)
        Me.LabelControl10.TabIndex = 104
        Me.LabelControl10.Text = "1- La longitud menor de la contraseña debe ser de 8 caracteres"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl16.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Appearance.Options.UseForeColor = True
        Me.LabelControl16.Location = New System.Drawing.Point(331, 6)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(153, 11)
        Me.LabelControl16.TabIndex = 103
        Me.LabelControl16.Text = "* POLITICAS DE CONTRASEÑAS"
        '
        'adm_frmModificaClave
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(623, 188)
        Me.ControlBox = False
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.LabelControl15)
        Me.Controls.Add(Me.LabelControl13)
        Me.Controls.Add(Me.LabelControl12)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.LabelControl16)
        Me.Controls.Add(Me.lblMensaje)
        Me.Controls.Add(Me.teConPass)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.teNewPass)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.btGuardar)
        Me.Controls.Add(Me.teId)
        Me.Controls.Add(Me.LabelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "adm_frmModificaClave"
        Me.Text = "Modificación de Clave de Seguridad"
        CType(Me.teId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNewPass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teConPass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teId As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNewPass As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teConPass As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblMensaje As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
End Class
