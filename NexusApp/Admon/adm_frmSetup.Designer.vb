﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class adm_frmSetup
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(adm_frmSetup))
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.teTelefonos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
        Me.teDepartamento = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.teMunicipio = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.ceAgente = New DevExpress.XtraEditors.CheckEdit()
        Me.rgTipoContrib = New DevExpress.XtraEditors.RadioGroup()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.teCargo6 = New DevExpress.XtraEditors.TextEdit()
        Me.teNombre6 = New DevExpress.XtraEditors.TextEdit()
        Me.teCargo5 = New DevExpress.XtraEditors.TextEdit()
        Me.teNombre5 = New DevExpress.XtraEditors.TextEdit()
        Me.teCargo4 = New DevExpress.XtraEditors.TextEdit()
        Me.teNombre4 = New DevExpress.XtraEditors.TextEdit()
        Me.teCargo3 = New DevExpress.XtraEditors.TextEdit()
        Me.teNombre3 = New DevExpress.XtraEditors.TextEdit()
        Me.teCargo2 = New DevExpress.XtraEditors.TextEdit()
        Me.teNombre2 = New DevExpress.XtraEditors.TextEdit()
        Me.teCargo1 = New DevExpress.XtraEditors.TextEdit()
        Me.teNombre1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumeroPatronal = New DevExpress.XtraEditors.TextEdit()
        Me.teNIT = New DevExpress.XtraEditors.TextEdit()
        Me.teNRC = New DevExpress.XtraEditors.TextEdit()
        Me.teDireccion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.teEmpresa = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.teActividad = New DevExpress.XtraEditors.TextEdit()
        Me.teDomicilio = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.SpiDiasCreditoVencido = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl60 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl55 = New DevExpress.XtraEditors.LabelControl()
        Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit()
        Me.chkFacturarMenosCosto = New DevExpress.XtraEditors.CheckEdit()
        Me.seCorrelOC = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl52 = New DevExpress.XtraEditors.LabelControl()
        Me.ceBloquearFecha = New DevExpress.XtraEditors.CheckEdit()
        Me.ceUtilizarFormularioUnico = New DevExpress.XtraEditors.CheckEdit()
        Me.ceSeccionarFormasPago = New DevExpress.XtraEditors.CheckEdit()
        Me.ceVistaPrevia = New DevExpress.XtraEditors.CheckEdit()
        Me.ceAutorizaCheques = New DevExpress.XtraEditors.CheckEdit()
        Me.ceAlertaMinimos = New DevExpress.XtraEditors.CheckEdit()
        Me.leTipoPartidaCompras = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
        Me.ceLineaCompras = New DevExpress.XtraEditors.CheckEdit()
        Me.seCorrelVale = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.seCorrelNA = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.seCorrelTraslados = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.seCorrelSalidas = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.seCorrelEntradas = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.ceValidaExistencia = New DevExpress.XtraEditors.CheckEdit()
        Me.seSujetosExcluidos = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.seCorrelativo = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.lePrecios = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.cbeSkins = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.leTipoPrecio = New DevExpress.XtraEditors.LookUpEdit()
        Me.lePuntoVenta = New DevExpress.XtraEditors.LookUpEdit()
        Me.leBodega = New DevExpress.XtraEditors.LookUpEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.cboTipoPartida = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.teMoneda = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaMaxima = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.sePorcPercep = New DevExpress.XtraEditors.SpinEdit()
        Me.sePorcReten = New DevExpress.XtraEditors.SpinEdit()
        Me.sePorcIVA = New DevExpress.XtraEditors.SpinEdit()
        Me.ceGuardarDescuadre = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaMinima = New DevExpress.XtraEditors.DateEdit()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.teCta13 = New DevExpress.XtraEditors.TextEdit()
        Me.beCta13 = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl54 = New DevExpress.XtraEditors.LabelControl()
        Me.teCta12 = New DevExpress.XtraEditors.TextEdit()
        Me.beCta12 = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.teCta11 = New DevExpress.XtraEditors.TextEdit()
        Me.beCta11 = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl50 = New DevExpress.XtraEditors.LabelControl()
        Me.teCta10 = New DevExpress.XtraEditors.TextEdit()
        Me.beCta10 = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
        Me.teCta09 = New DevExpress.XtraEditors.TextEdit()
        Me.beCta09 = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        Me.teCta08 = New DevExpress.XtraEditors.TextEdit()
        Me.teCta07 = New DevExpress.XtraEditors.TextEdit()
        Me.teCta06 = New DevExpress.XtraEditors.TextEdit()
        Me.teCta05 = New DevExpress.XtraEditors.TextEdit()
        Me.teCta04 = New DevExpress.XtraEditors.TextEdit()
        Me.teCta03 = New DevExpress.XtraEditors.TextEdit()
        Me.teCta02 = New DevExpress.XtraEditors.TextEdit()
        Me.teCta01 = New DevExpress.XtraEditors.TextEdit()
        Me.beCta08 = New DevExpress.XtraEditors.ButtonEdit()
        Me.beCta07 = New DevExpress.XtraEditors.ButtonEdit()
        Me.beCta06 = New DevExpress.XtraEditors.ButtonEdit()
        Me.beCta05 = New DevExpress.XtraEditors.ButtonEdit()
        Me.beCta04 = New DevExpress.XtraEditors.ButtonEdit()
        Me.beCta03 = New DevExpress.XtraEditors.ButtonEdit()
        Me.beCta02 = New DevExpress.XtraEditors.ButtonEdit()
        Me.beCta01 = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage()
        Me.sbtnEliminarEmpresaActual = New DevExpress.XtraEditors.SimpleButton()
        Me.teMontoAcumuladoNoEfectivo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl59 = New DevExpress.XtraEditors.LabelControl()
        Me.teMontoDirectoNoEfectivo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl58 = New DevExpress.XtraEditors.LabelControl()
        Me.teMontoAcumuladoEfectivo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl57 = New DevExpress.XtraEditors.LabelControl()
        Me.teMontoDirectoEfectivo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl56 = New DevExpress.XtraEditors.LabelControl()
        Me.peFoto = New DevExpress.XtraEditors.PictureEdit()
        Me.lcExisteArchivo = New DevExpress.XtraEditors.LabelControl()
        Me.OpenFile = New System.Windows.Forms.OpenFileDialog()
        Me.LabelControl61 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDepartamento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teMunicipio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceAgente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipoContrib.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.teCargo6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCargo5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCargo4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCargo3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCargo2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCargo1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumeroPatronal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teEmpresa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teActividad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.SpiDiasCreditoVencido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFacturarMenosCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seCorrelOC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceBloquearFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceUtilizarFormularioUnico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceSeccionarFormasPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceVistaPrevia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceAutorizaCheques.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceAlertaMinimos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPartidaCompras.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceLineaCompras.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seCorrelVale.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seCorrelNA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seCorrelTraslados.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seCorrelSalidas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seCorrelEntradas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceValidaExistencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seSujetosExcluidos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePrecios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbeSkins.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPrecio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teMoneda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaMaxima.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaMaxima.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sePorcPercep.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sePorcReten.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sePorcIVA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceGuardarDescuadre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaMinima.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaMinima.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.teCta13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta09.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta09.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta08.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta07.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta06.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta05.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta04.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta03.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta08.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta07.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta06.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta05.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta04.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta03.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.teMontoAcumuladoNoEfectivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teMontoDirectoNoEfectivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teMontoAcumuladoEfectivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teMontoDirectoEfectivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peFoto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(981, 474)
        Me.XtraTabControl1.TabIndex = 3
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.teTelefonos)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl53)
        Me.XtraTabPage1.Controls.Add(Me.teDepartamento)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl41)
        Me.XtraTabPage1.Controls.Add(Me.teMunicipio)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl38)
        Me.XtraTabPage1.Controls.Add(Me.ceAgente)
        Me.XtraTabPage1.Controls.Add(Me.rgTipoContrib)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl1)
        Me.XtraTabPage1.Controls.Add(Me.teNumeroPatronal)
        Me.XtraTabPage1.Controls.Add(Me.teNIT)
        Me.XtraTabPage1.Controls.Add(Me.teNRC)
        Me.XtraTabPage1.Controls.Add(Me.teDireccion)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl35)
        Me.XtraTabPage1.Controls.Add(Me.teEmpresa)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl61)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl12)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl11)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl13)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl6)
        Me.XtraTabPage1.Controls.Add(Me.teActividad)
        Me.XtraTabPage1.Controls.Add(Me.teDomicilio)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl36)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl26)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(975, 446)
        Me.XtraTabPage1.Text = "Datos de la empresa"
        '
        'teTelefonos
        '
        Me.teTelefonos.Location = New System.Drawing.Point(463, 167)
        Me.teTelefonos.Name = "teTelefonos"
        Me.teTelefonos.Size = New System.Drawing.Size(210, 20)
        Me.teTelefonos.TabIndex = 67
        '
        'LabelControl53
        '
        Me.LabelControl53.Location = New System.Drawing.Point(410, 170)
        Me.LabelControl53.Name = "LabelControl53"
        Me.LabelControl53.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl53.TabIndex = 68
        Me.LabelControl53.Text = "Teléfonos:"
        '
        'teDepartamento
        '
        Me.teDepartamento.Location = New System.Drawing.Point(463, 124)
        Me.teDepartamento.Name = "teDepartamento"
        Me.teDepartamento.Size = New System.Drawing.Size(210, 20)
        Me.teDepartamento.TabIndex = 65
        '
        'LabelControl41
        '
        Me.LabelControl41.Location = New System.Drawing.Point(388, 127)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl41.TabIndex = 66
        Me.LabelControl41.Text = "Departamento:"
        '
        'teMunicipio
        '
        Me.teMunicipio.Location = New System.Drawing.Point(176, 124)
        Me.teMunicipio.Name = "teMunicipio"
        Me.teMunicipio.Size = New System.Drawing.Size(203, 20)
        Me.teMunicipio.TabIndex = 63
        '
        'LabelControl38
        '
        Me.LabelControl38.Location = New System.Drawing.Point(126, 127)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl38.TabIndex = 64
        Me.LabelControl38.Text = "Municipio:"
        '
        'ceAgente
        '
        Me.ceAgente.Location = New System.Drawing.Point(446, 79)
        Me.ceAgente.Name = "ceAgente"
        Me.ceAgente.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ceAgente.Properties.Appearance.Options.UseFont = True
        Me.ceAgente.Properties.Caption = "Agente de Retención y percepción"
        Me.ceAgente.Size = New System.Drawing.Size(227, 19)
        Me.ceAgente.TabIndex = 4
        '
        'rgTipoContrib
        '
        Me.rgTipoContrib.EditValue = 1
        Me.rgTipoContrib.Location = New System.Drawing.Point(176, 76)
        Me.rgTipoContrib.Name = "rgTipoContrib"
        Me.rgTipoContrib.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Pequeño"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Mediano"), New DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Grande")})
        Me.rgTipoContrib.Size = New System.Drawing.Size(260, 25)
        Me.rgTipoContrib.TabIndex = 3
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl21)
        Me.GroupControl1.Controls.Add(Me.LabelControl20)
        Me.GroupControl1.Controls.Add(Me.LabelControl19)
        Me.GroupControl1.Controls.Add(Me.LabelControl18)
        Me.GroupControl1.Controls.Add(Me.LabelControl17)
        Me.GroupControl1.Controls.Add(Me.LabelControl16)
        Me.GroupControl1.Controls.Add(Me.teCargo6)
        Me.GroupControl1.Controls.Add(Me.teNombre6)
        Me.GroupControl1.Controls.Add(Me.teCargo5)
        Me.GroupControl1.Controls.Add(Me.teNombre5)
        Me.GroupControl1.Controls.Add(Me.teCargo4)
        Me.GroupControl1.Controls.Add(Me.teNombre4)
        Me.GroupControl1.Controls.Add(Me.teCargo3)
        Me.GroupControl1.Controls.Add(Me.teNombre3)
        Me.GroupControl1.Controls.Add(Me.teCargo2)
        Me.GroupControl1.Controls.Add(Me.teNombre2)
        Me.GroupControl1.Controls.Add(Me.teCargo1)
        Me.GroupControl1.Controls.Add(Me.teNombre1)
        Me.GroupControl1.Controls.Add(Me.LabelControl15)
        Me.GroupControl1.Controls.Add(Me.LabelControl14)
        Me.GroupControl1.Location = New System.Drawing.Point(9, 229)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(664, 184)
        Me.GroupControl1.TabIndex = 62
        Me.GroupControl1.Text = "Firmantes en los balances"
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(18, 161)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(10, 13)
        Me.LabelControl21.TabIndex = 2
        Me.LabelControl21.Text = "6."
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(18, 138)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(10, 13)
        Me.LabelControl20.TabIndex = 2
        Me.LabelControl20.Text = "5."
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(18, 116)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(10, 13)
        Me.LabelControl19.TabIndex = 2
        Me.LabelControl19.Text = "4."
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(18, 94)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(10, 13)
        Me.LabelControl18.TabIndex = 2
        Me.LabelControl18.Text = "3."
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(18, 72)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(10, 13)
        Me.LabelControl17.TabIndex = 2
        Me.LabelControl17.Text = "2."
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(18, 51)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(10, 13)
        Me.LabelControl16.TabIndex = 2
        Me.LabelControl16.Text = "1."
        '
        'teCargo6
        '
        Me.teCargo6.Location = New System.Drawing.Point(343, 157)
        Me.teCargo6.Name = "teCargo6"
        Me.teCargo6.Size = New System.Drawing.Size(298, 20)
        Me.teCargo6.TabIndex = 11
        '
        'teNombre6
        '
        Me.teNombre6.Location = New System.Drawing.Point(31, 157)
        Me.teNombre6.Name = "teNombre6"
        Me.teNombre6.Size = New System.Drawing.Size(298, 20)
        Me.teNombre6.TabIndex = 10
        '
        'teCargo5
        '
        Me.teCargo5.Location = New System.Drawing.Point(343, 135)
        Me.teCargo5.Name = "teCargo5"
        Me.teCargo5.Size = New System.Drawing.Size(298, 20)
        Me.teCargo5.TabIndex = 9
        '
        'teNombre5
        '
        Me.teNombre5.Location = New System.Drawing.Point(31, 135)
        Me.teNombre5.Name = "teNombre5"
        Me.teNombre5.Size = New System.Drawing.Size(298, 20)
        Me.teNombre5.TabIndex = 8
        '
        'teCargo4
        '
        Me.teCargo4.Location = New System.Drawing.Point(343, 113)
        Me.teCargo4.Name = "teCargo4"
        Me.teCargo4.Size = New System.Drawing.Size(298, 20)
        Me.teCargo4.TabIndex = 7
        '
        'teNombre4
        '
        Me.teNombre4.Location = New System.Drawing.Point(31, 113)
        Me.teNombre4.Name = "teNombre4"
        Me.teNombre4.Size = New System.Drawing.Size(298, 20)
        Me.teNombre4.TabIndex = 6
        '
        'teCargo3
        '
        Me.teCargo3.Location = New System.Drawing.Point(343, 91)
        Me.teCargo3.Name = "teCargo3"
        Me.teCargo3.Size = New System.Drawing.Size(298, 20)
        Me.teCargo3.TabIndex = 5
        '
        'teNombre3
        '
        Me.teNombre3.Location = New System.Drawing.Point(31, 91)
        Me.teNombre3.Name = "teNombre3"
        Me.teNombre3.Size = New System.Drawing.Size(298, 20)
        Me.teNombre3.TabIndex = 4
        '
        'teCargo2
        '
        Me.teCargo2.Location = New System.Drawing.Point(343, 69)
        Me.teCargo2.Name = "teCargo2"
        Me.teCargo2.Size = New System.Drawing.Size(298, 20)
        Me.teCargo2.TabIndex = 3
        '
        'teNombre2
        '
        Me.teNombre2.Location = New System.Drawing.Point(31, 69)
        Me.teNombre2.Name = "teNombre2"
        Me.teNombre2.Size = New System.Drawing.Size(298, 20)
        Me.teNombre2.TabIndex = 2
        '
        'teCargo1
        '
        Me.teCargo1.Location = New System.Drawing.Point(343, 47)
        Me.teCargo1.Name = "teCargo1"
        Me.teCargo1.Size = New System.Drawing.Size(298, 20)
        Me.teCargo1.TabIndex = 1
        '
        'teNombre1
        '
        Me.teNombre1.Location = New System.Drawing.Point(31, 47)
        Me.teNombre1.Name = "teNombre1"
        Me.teNombre1.Size = New System.Drawing.Size(298, 20)
        Me.teNombre1.TabIndex = 0
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(356, 32)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(42, 13)
        Me.LabelControl15.TabIndex = 0
        Me.LabelControl15.Text = "CARGOS"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(54, 31)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl14.TabIndex = 0
        Me.LabelControl14.Text = "NOMBRES"
        '
        'teNumeroPatronal
        '
        Me.teNumeroPatronal.Location = New System.Drawing.Point(176, 167)
        Me.teNumeroPatronal.Name = "teNumeroPatronal"
        Me.teNumeroPatronal.Size = New System.Drawing.Size(158, 20)
        Me.teNumeroPatronal.TabIndex = 7
        '
        'teNIT
        '
        Me.teNIT.Location = New System.Drawing.Point(176, 54)
        Me.teNIT.Name = "teNIT"
        Me.teNIT.Size = New System.Drawing.Size(158, 20)
        Me.teNIT.TabIndex = 2
        '
        'teNRC
        '
        Me.teNRC.Location = New System.Drawing.Point(176, 33)
        Me.teNRC.Name = "teNRC"
        Me.teNRC.Size = New System.Drawing.Size(158, 20)
        Me.teNRC.TabIndex = 1
        '
        'teDireccion
        '
        Me.teDireccion.Location = New System.Drawing.Point(176, 102)
        Me.teDireccion.Name = "teDireccion"
        Me.teDireccion.Size = New System.Drawing.Size(497, 20)
        Me.teDireccion.TabIndex = 5
        '
        'LabelControl35
        '
        Me.LabelControl35.Location = New System.Drawing.Point(89, 170)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl35.TabIndex = 57
        Me.LabelControl35.Text = "Número Patronal:"
        '
        'teEmpresa
        '
        Me.teEmpresa.Location = New System.Drawing.Point(176, 11)
        Me.teEmpresa.Name = "teEmpresa"
        Me.teEmpresa.Properties.ReadOnly = True
        Me.teEmpresa.Size = New System.Drawing.Size(497, 20)
        Me.teEmpresa.TabIndex = 0
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(82, 56)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl12.TabIndex = 57
        Me.LabelControl12.Text = "NIT de la Empresa:"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(78, 35)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(95, 13)
        Me.LabelControl11.TabIndex = 55
        Me.LabelControl11.Text = "NRC de la Empresa:"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(126, 104)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl13.TabIndex = 56
        Me.LabelControl13.Text = "Dirección:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(62, 13)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(111, 13)
        Me.LabelControl6.TabIndex = 54
        Me.LabelControl6.Text = "Nombre de la empresa:"
        '
        'teActividad
        '
        Me.teActividad.Location = New System.Drawing.Point(176, 189)
        Me.teActividad.Name = "teActividad"
        Me.teActividad.Size = New System.Drawing.Size(497, 20)
        Me.teActividad.TabIndex = 8
        '
        'teDomicilio
        '
        Me.teDomicilio.Location = New System.Drawing.Point(176, 146)
        Me.teDomicilio.Name = "teDomicilio"
        Me.teDomicilio.Size = New System.Drawing.Size(497, 20)
        Me.teDomicilio.TabIndex = 6
        '
        'LabelControl36
        '
        Me.LabelControl36.Location = New System.Drawing.Point(36, 192)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(137, 13)
        Me.LabelControl36.TabIndex = 53
        Me.LabelControl36.Text = "Actividad o Giro del Negocio:"
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(62, 81)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(111, 13)
        Me.LabelControl26.TabIndex = 53
        Me.LabelControl26.Text = "Tipo de Contribuyente:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(19, 149)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(154, 13)
        Me.LabelControl1.TabIndex = 53
        Me.LabelControl1.Text = "Nombre de la Ciudad (Domicilio):"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.SpiDiasCreditoVencido)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl60)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl55)
        Me.XtraTabPage2.Controls.Add(Me.meMes)
        Me.XtraTabPage2.Controls.Add(Me.chkFacturarMenosCosto)
        Me.XtraTabPage2.Controls.Add(Me.seCorrelOC)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl52)
        Me.XtraTabPage2.Controls.Add(Me.ceBloquearFecha)
        Me.XtraTabPage2.Controls.Add(Me.ceUtilizarFormularioUnico)
        Me.XtraTabPage2.Controls.Add(Me.ceSeccionarFormasPago)
        Me.XtraTabPage2.Controls.Add(Me.ceVistaPrevia)
        Me.XtraTabPage2.Controls.Add(Me.ceAutorizaCheques)
        Me.XtraTabPage2.Controls.Add(Me.ceAlertaMinimos)
        Me.XtraTabPage2.Controls.Add(Me.leTipoPartidaCompras)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl49)
        Me.XtraTabPage2.Controls.Add(Me.ceLineaCompras)
        Me.XtraTabPage2.Controls.Add(Me.seCorrelVale)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl47)
        Me.XtraTabPage2.Controls.Add(Me.seCorrelNA)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl45)
        Me.XtraTabPage2.Controls.Add(Me.seCorrelTraslados)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl44)
        Me.XtraTabPage2.Controls.Add(Me.seCorrelSalidas)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl43)
        Me.XtraTabPage2.Controls.Add(Me.seCorrelEntradas)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl42)
        Me.XtraTabPage2.Controls.Add(Me.ceValidaExistencia)
        Me.XtraTabPage2.Controls.Add(Me.seSujetosExcluidos)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl40)
        Me.XtraTabPage2.Controls.Add(Me.seCorrelativo)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl39)
        Me.XtraTabPage2.Controls.Add(Me.lePrecios)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl8)
        Me.XtraTabPage2.Controls.Add(Me.cbeSkins)
        Me.XtraTabPage2.Controls.Add(Me.leTipoPrecio)
        Me.XtraTabPage2.Controls.Add(Me.lePuntoVenta)
        Me.XtraTabPage2.Controls.Add(Me.leBodega)
        Me.XtraTabPage2.Controls.Add(Me.leSucursal)
        Me.XtraTabPage2.Controls.Add(Me.cboTipoPartida)
        Me.XtraTabPage2.Controls.Add(Me.teMoneda)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl10)
        Me.XtraTabPage2.Controls.Add(Me.deFechaMaxima)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl9)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl7)
        Me.XtraTabPage2.Controls.Add(Me.sePorcPercep)
        Me.XtraTabPage2.Controls.Add(Me.sePorcReten)
        Me.XtraTabPage2.Controls.Add(Me.sePorcIVA)
        Me.XtraTabPage2.Controls.Add(Me.ceGuardarDescuadre)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl5)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl4)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl25)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl24)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl23)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl22)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl3)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl37)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl2)
        Me.XtraTabPage2.Controls.Add(Me.deFechaMinima)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(975, 446)
        Me.XtraTabPage2.Text = "Parámetros generales"
        '
        'SpiDiasCreditoVencido
        '
        Me.SpiDiasCreditoVencido.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.SpiDiasCreditoVencido.Location = New System.Drawing.Point(558, 376)
        Me.SpiDiasCreditoVencido.Name = "SpiDiasCreditoVencido"
        Me.SpiDiasCreditoVencido.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.SpiDiasCreditoVencido.Properties.Mask.EditMask = "n0"
        Me.SpiDiasCreditoVencido.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.SpiDiasCreditoVencido.Size = New System.Drawing.Size(100, 20)
        Me.SpiDiasCreditoVencido.TabIndex = 144
        '
        'LabelControl60
        '
        Me.LabelControl60.Location = New System.Drawing.Point(413, 378)
        Me.LabelControl60.Name = "LabelControl60"
        Me.LabelControl60.Size = New System.Drawing.Size(141, 13)
        Me.LabelControl60.TabIndex = 145
        Me.LabelControl60.Text = "Dias Maximo Credito Factura:"
        '
        'LabelControl55
        '
        Me.LabelControl55.Location = New System.Drawing.Point(627, 42)
        Me.LabelControl55.Name = "LabelControl55"
        Me.LabelControl55.Size = New System.Drawing.Size(110, 13)
        Me.LabelControl55.TabIndex = 102
        Me.LabelControl55.Text = "Mes para Cierre Anual:"
        '
        'meMes
        '
        Me.meMes.Location = New System.Drawing.Point(742, 39)
        Me.meMes.Name = "meMes"
        Me.meMes.Properties.AllowFocused = False
        Me.meMes.Properties.AppearanceDisabled.BorderColor = System.Drawing.Color.Black
        Me.meMes.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.meMes.Properties.AppearanceDisabled.Options.UseBorderColor = True
        Me.meMes.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes.Size = New System.Drawing.Size(92, 20)
        Me.meMes.TabIndex = 101
        '
        'chkFacturarMenosCosto
        '
        Me.chkFacturarMenosCosto.Location = New System.Drawing.Point(626, 61)
        Me.chkFacturarMenosCosto.Name = "chkFacturarMenosCosto"
        Me.chkFacturarMenosCosto.Properties.Caption = "Permitir facturar a menos del costo"
        Me.chkFacturarMenosCosto.Size = New System.Drawing.Size(189, 19)
        Me.chkFacturarMenosCosto.TabIndex = 100
        '
        'seCorrelOC
        '
        Me.seCorrelOC.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seCorrelOC.Location = New System.Drawing.Point(558, 354)
        Me.seCorrelOC.Name = "seCorrelOC"
        Me.seCorrelOC.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seCorrelOC.Properties.Mask.EditMask = "n0"
        Me.seCorrelOC.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seCorrelOC.Size = New System.Drawing.Size(100, 20)
        Me.seCorrelOC.TabIndex = 98
        '
        'LabelControl52
        '
        Me.LabelControl52.Location = New System.Drawing.Point(447, 356)
        Me.LabelControl52.Name = "LabelControl52"
        Me.LabelControl52.Size = New System.Drawing.Size(107, 13)
        Me.LabelControl52.TabIndex = 99
        Me.LabelControl52.Text = "Ultimo Correlativo OC:"
        '
        'ceBloquearFecha
        '
        Me.ceBloquearFecha.Location = New System.Drawing.Point(626, 115)
        Me.ceBloquearFecha.Name = "ceBloquearFecha"
        Me.ceBloquearFecha.Properties.Caption = "Bloquear Fecha al Facturar"
        Me.ceBloquearFecha.Size = New System.Drawing.Size(155, 19)
        Me.ceBloquearFecha.TabIndex = 97
        '
        'ceUtilizarFormularioUnico
        '
        Me.ceUtilizarFormularioUnico.Location = New System.Drawing.Point(397, 39)
        Me.ceUtilizarFormularioUnico.Name = "ceUtilizarFormularioUnico"
        Me.ceUtilizarFormularioUnico.Properties.Caption = "Utilizar Formulario Único"
        Me.ceUtilizarFormularioUnico.Size = New System.Drawing.Size(137, 19)
        Me.ceUtilizarFormularioUnico.TabIndex = 96
        '
        'ceSeccionarFormasPago
        '
        Me.ceSeccionarFormasPago.Location = New System.Drawing.Point(626, 79)
        Me.ceSeccionarFormasPago.Name = "ceSeccionarFormasPago"
        Me.ceSeccionarFormasPago.Properties.Caption = "Multi-Forma de Pago al Facturar"
        Me.ceSeccionarFormasPago.Size = New System.Drawing.Size(189, 19)
        Me.ceSeccionarFormasPago.TabIndex = 95
        '
        'ceVistaPrevia
        '
        Me.ceVistaPrevia.Location = New System.Drawing.Point(397, 133)
        Me.ceVistaPrevia.Name = "ceVistaPrevia"
        Me.ceVistaPrevia.Properties.Caption = "Generar Vista Prevía al Facturar"
        Me.ceVistaPrevia.Size = New System.Drawing.Size(189, 19)
        Me.ceVistaPrevia.TabIndex = 94
        '
        'ceAutorizaCheques
        '
        Me.ceAutorizaCheques.Location = New System.Drawing.Point(397, 114)
        Me.ceAutorizaCheques.Name = "ceAutorizaCheques"
        Me.ceAutorizaCheques.Properties.Caption = "Autorizar Cheques para Impresión"
        Me.ceAutorizaCheques.Size = New System.Drawing.Size(189, 19)
        Me.ceAutorizaCheques.TabIndex = 93
        '
        'ceAlertaMinimos
        '
        Me.ceAlertaMinimos.Location = New System.Drawing.Point(397, 95)
        Me.ceAlertaMinimos.Name = "ceAlertaMinimos"
        Me.ceAlertaMinimos.Properties.Caption = "Alerta de Productos en Mínimos al Facturar"
        Me.ceAlertaMinimos.Size = New System.Drawing.Size(224, 19)
        Me.ceAlertaMinimos.TabIndex = 92
        '
        'leTipoPartidaCompras
        '
        Me.leTipoPartidaCompras.EnterMoveNextControl = True
        Me.leTipoPartidaCompras.Location = New System.Drawing.Point(687, 270)
        Me.leTipoPartidaCompras.Name = "leTipoPartidaCompras"
        Me.leTipoPartidaCompras.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPartidaCompras.Size = New System.Drawing.Size(137, 20)
        Me.leTipoPartidaCompras.TabIndex = 90
        '
        'LabelControl49
        '
        Me.LabelControl49.Location = New System.Drawing.Point(531, 272)
        Me.LabelControl49.Name = "LabelControl49"
        Me.LabelControl49.Size = New System.Drawing.Size(151, 13)
        Me.LabelControl49.TabIndex = 91
        Me.LabelControl49.Text = "Tipo de Partida Cont. Compras:"
        '
        'ceLineaCompras
        '
        Me.ceLineaCompras.Location = New System.Drawing.Point(397, 58)
        Me.ceLineaCompras.Name = "ceLineaCompras"
        Me.ceLineaCompras.Properties.Caption = "Contabilizar en Línea Compras"
        Me.ceLineaCompras.Size = New System.Drawing.Size(170, 19)
        Me.ceLineaCompras.TabIndex = 89
        '
        'seCorrelVale
        '
        Me.seCorrelVale.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seCorrelVale.Location = New System.Drawing.Point(558, 333)
        Me.seCorrelVale.Name = "seCorrelVale"
        Me.seCorrelVale.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seCorrelVale.Properties.Mask.EditMask = "n0"
        Me.seCorrelVale.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seCorrelVale.Size = New System.Drawing.Size(100, 20)
        Me.seCorrelVale.TabIndex = 87
        '
        'LabelControl47
        '
        Me.LabelControl47.Location = New System.Drawing.Point(442, 335)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(112, 13)
        Me.LabelControl47.TabIndex = 88
        Me.LabelControl47.Text = "Ultimo Correlativo Vale:"
        '
        'seCorrelNA
        '
        Me.seCorrelNA.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seCorrelNA.Location = New System.Drawing.Point(558, 313)
        Me.seCorrelNA.Name = "seCorrelNA"
        Me.seCorrelNA.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seCorrelNA.Properties.Mask.EditMask = "n0"
        Me.seCorrelNA.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seCorrelNA.Size = New System.Drawing.Size(100, 20)
        Me.seCorrelNA.TabIndex = 85
        '
        'LabelControl45
        '
        Me.LabelControl45.Location = New System.Drawing.Point(444, 315)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(110, 13)
        Me.LabelControl45.TabIndex = 86
        Me.LabelControl45.Text = "Ultimo Correlativo N/A:"
        '
        'seCorrelTraslados
        '
        Me.seCorrelTraslados.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seCorrelTraslados.Location = New System.Drawing.Point(285, 400)
        Me.seCorrelTraslados.Name = "seCorrelTraslados"
        Me.seCorrelTraslados.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seCorrelTraslados.Properties.Mask.EditMask = "n0"
        Me.seCorrelTraslados.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seCorrelTraslados.Size = New System.Drawing.Size(100, 20)
        Me.seCorrelTraslados.TabIndex = 83
        '
        'LabelControl44
        '
        Me.LabelControl44.Location = New System.Drawing.Point(118, 406)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(161, 13)
        Me.LabelControl44.TabIndex = 84
        Me.LabelControl44.Text = "Ultimo Correlativo Traslados Inv.:"
        '
        'seCorrelSalidas
        '
        Me.seCorrelSalidas.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seCorrelSalidas.Location = New System.Drawing.Point(285, 379)
        Me.seCorrelSalidas.Name = "seCorrelSalidas"
        Me.seCorrelSalidas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seCorrelSalidas.Properties.Mask.EditMask = "n0"
        Me.seCorrelSalidas.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seCorrelSalidas.Size = New System.Drawing.Size(100, 20)
        Me.seCorrelSalidas.TabIndex = 81
        '
        'LabelControl43
        '
        Me.LabelControl43.Location = New System.Drawing.Point(131, 383)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(148, 13)
        Me.LabelControl43.TabIndex = 82
        Me.LabelControl43.Text = "Ultimo Correlativo Salidas Inv.:"
        '
        'seCorrelEntradas
        '
        Me.seCorrelEntradas.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seCorrelEntradas.Location = New System.Drawing.Point(285, 358)
        Me.seCorrelEntradas.Name = "seCorrelEntradas"
        Me.seCorrelEntradas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seCorrelEntradas.Properties.Mask.EditMask = "n0"
        Me.seCorrelEntradas.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seCorrelEntradas.Size = New System.Drawing.Size(100, 20)
        Me.seCorrelEntradas.TabIndex = 79
        '
        'LabelControl42
        '
        Me.LabelControl42.Location = New System.Drawing.Point(121, 361)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(158, 13)
        Me.LabelControl42.TabIndex = 80
        Me.LabelControl42.Text = "Ultimo Correlativo Entradas Inv.:"
        '
        'ceValidaExistencia
        '
        Me.ceValidaExistencia.Location = New System.Drawing.Point(397, 76)
        Me.ceValidaExistencia.Name = "ceValidaExistencia"
        Me.ceValidaExistencia.Properties.Caption = "Validar Existencias al Facturar"
        Me.ceValidaExistencia.Size = New System.Drawing.Size(170, 19)
        Me.ceValidaExistencia.TabIndex = 78
        '
        'seSujetosExcluidos
        '
        Me.seSujetosExcluidos.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seSujetosExcluidos.Location = New System.Drawing.Point(285, 335)
        Me.seSujetosExcluidos.Name = "seSujetosExcluidos"
        Me.seSujetosExcluidos.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seSujetosExcluidos.Properties.Mask.EditMask = "n0"
        Me.seSujetosExcluidos.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seSujetosExcluidos.Size = New System.Drawing.Size(100, 20)
        Me.seSujetosExcluidos.TabIndex = 76
        '
        'LabelControl40
        '
        Me.LabelControl40.Location = New System.Drawing.Point(104, 339)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(175, 13)
        Me.LabelControl40.TabIndex = 77
        Me.LabelControl40.Text = "Ultimo Correlativo Sujetos Excluídos:"
        '
        'seCorrelativo
        '
        Me.seCorrelativo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seCorrelativo.Location = New System.Drawing.Point(285, 313)
        Me.seCorrelativo.Name = "seCorrelativo"
        Me.seCorrelativo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seCorrelativo.Properties.Mask.EditMask = "n0"
        Me.seCorrelativo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seCorrelativo.Size = New System.Drawing.Size(100, 20)
        Me.seCorrelativo.TabIndex = 74
        '
        'LabelControl39
        '
        Me.LabelControl39.Location = New System.Drawing.Point(149, 315)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(130, 13)
        Me.LabelControl39.TabIndex = 75
        Me.LabelControl39.Text = "Ultimo Correlativo Quedán:"
        '
        'lePrecios
        '
        Me.lePrecios.Location = New System.Drawing.Point(285, 246)
        Me.lePrecios.Name = "lePrecios"
        Me.lePrecios.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePrecios.Size = New System.Drawing.Size(373, 20)
        Me.lePrecios.TabIndex = 70
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(150, 250)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(129, 13)
        Me.LabelControl8.TabIndex = 69
        Me.LabelControl8.Text = "Modalidad de  Facturación:"
        '
        'cbeSkins
        '
        Me.cbeSkins.Location = New System.Drawing.Point(285, 292)
        Me.cbeSkins.Name = "cbeSkins"
        Me.cbeSkins.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbeSkins.Size = New System.Drawing.Size(239, 20)
        Me.cbeSkins.TabIndex = 13
        '
        'leTipoPrecio
        '
        Me.leTipoPrecio.Location = New System.Drawing.Point(285, 223)
        Me.leTipoPrecio.Name = "leTipoPrecio"
        Me.leTipoPrecio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPrecio.Size = New System.Drawing.Size(373, 20)
        Me.leTipoPrecio.TabIndex = 10
        '
        'lePuntoVenta
        '
        Me.lePuntoVenta.Location = New System.Drawing.Point(285, 201)
        Me.lePuntoVenta.Name = "lePuntoVenta"
        Me.lePuntoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePuntoVenta.Size = New System.Drawing.Size(373, 20)
        Me.lePuntoVenta.TabIndex = 9
        '
        'leBodega
        '
        Me.leBodega.Location = New System.Drawing.Point(285, 155)
        Me.leBodega.Name = "leBodega"
        Me.leBodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leBodega.Size = New System.Drawing.Size(373, 20)
        Me.leBodega.TabIndex = 7
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(285, 178)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(373, 20)
        Me.leSucursal.TabIndex = 8
        '
        'cboTipoPartida
        '
        Me.cboTipoPartida.EditValue = "Anual Por Tipo de Partida"
        Me.cboTipoPartida.Location = New System.Drawing.Point(285, 270)
        Me.cboTipoPartida.Name = "cboTipoPartida"
        Me.cboTipoPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboTipoPartida.Properties.Items.AddRange(New Object() {"Mensual por Tipo de Partida", "Anual Por Tipo de Partida", "Mensual sin Tipo de Partida", "Anual sin Tipo de Partida"})
        Me.cboTipoPartida.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cboTipoPartida.Size = New System.Drawing.Size(239, 20)
        Me.cboTipoPartida.TabIndex = 11
        '
        'teMoneda
        '
        Me.teMoneda.Location = New System.Drawing.Point(285, 17)
        Me.teMoneda.Name = "teMoneda"
        Me.teMoneda.Size = New System.Drawing.Size(549, 20)
        Me.teMoneda.TabIndex = 0
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(30, 64)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(249, 13)
        Me.LabelControl10.TabIndex = 67
        Me.LabelControl10.Text = "Fecha Límite o Máxima para registrar transacciones:"
        '
        'deFechaMaxima
        '
        Me.deFechaMaxima.EditValue = Nothing
        Me.deFechaMaxima.Location = New System.Drawing.Point(285, 61)
        Me.deFechaMaxima.Name = "deFechaMaxima"
        Me.deFechaMaxima.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaMaxima.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaMaxima.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaMaxima.Size = New System.Drawing.Size(100, 20)
        Me.deFechaMaxima.TabIndex = 3
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(17, 42)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(262, 13)
        Me.LabelControl9.TabIndex = 66
        Me.LabelControl9.Text = "Fecha de Cierre o Mínima para registrar transacciones:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(154, 19)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(125, 13)
        Me.LabelControl7.TabIndex = 64
        Me.LabelControl7.Text = "Descripción de la moneda:"
        '
        'sePorcPercep
        '
        Me.sePorcPercep.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.sePorcPercep.Location = New System.Drawing.Point(285, 127)
        Me.sePorcPercep.Name = "sePorcPercep"
        Me.sePorcPercep.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.sePorcPercep.Properties.Mask.EditMask = "P2"
        Me.sePorcPercep.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.sePorcPercep.Size = New System.Drawing.Size(100, 20)
        Me.sePorcPercep.TabIndex = 6
        '
        'sePorcReten
        '
        Me.sePorcReten.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.sePorcReten.Location = New System.Drawing.Point(285, 105)
        Me.sePorcReten.Name = "sePorcReten"
        Me.sePorcReten.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.sePorcReten.Properties.Mask.EditMask = "P2"
        Me.sePorcReten.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.sePorcReten.Size = New System.Drawing.Size(100, 20)
        Me.sePorcReten.TabIndex = 5
        '
        'sePorcIVA
        '
        Me.sePorcIVA.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.sePorcIVA.Location = New System.Drawing.Point(285, 83)
        Me.sePorcIVA.Name = "sePorcIVA"
        Me.sePorcIVA.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.sePorcIVA.Properties.Mask.EditMask = "P2"
        Me.sePorcIVA.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.sePorcIVA.Size = New System.Drawing.Size(100, 20)
        Me.sePorcIVA.TabIndex = 4
        '
        'ceGuardarDescuadre
        '
        Me.ceGuardarDescuadre.Location = New System.Drawing.Point(626, 96)
        Me.ceGuardarDescuadre.Name = "ceGuardarDescuadre"
        Me.ceGuardarDescuadre.Properties.Caption = "Permitir guardar partidas en desbalance"
        Me.ceGuardarDescuadre.Size = New System.Drawing.Size(217, 19)
        Me.ceGuardarDescuadre.TabIndex = 12
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(153, 131)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(126, 13)
        Me.LabelControl5.TabIndex = 62
        Me.LabelControl5.Text = "Porcentaje de percepción:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(160, 107)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(119, 13)
        Me.LabelControl4.TabIndex = 61
        Me.LabelControl4.Text = "Porcentaje de retención:"
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(129, 227)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(150, 13)
        Me.LabelControl25.TabIndex = 59
        Me.LabelControl25.Text = "Tipo de Precio Predeterminado:"
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(122, 205)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(157, 13)
        Me.LabelControl24.TabIndex = 59
        Me.LabelControl24.Text = "Punto de Venta Predeterminado:"
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(160, 159)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(119, 13)
        Me.LabelControl23.TabIndex = 59
        Me.LabelControl23.Text = "Bodega Predeterminada:"
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(156, 181)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(123, 13)
        Me.LabelControl22.TabIndex = 59
        Me.LabelControl22.Text = "Sucursal predeterminada:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(188, 86)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl3.TabIndex = 60
        Me.LabelControl3.Text = "Porcentaje de IVA:"
        '
        'LabelControl37
        '
        Me.LabelControl37.Location = New System.Drawing.Point(154, 295)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(125, 13)
        Me.LabelControl37.TabIndex = 59
        Me.LabelControl37.Text = "Skin (piel) de la aplicación:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(109, 273)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(170, 13)
        Me.LabelControl2.TabIndex = 59
        Me.LabelControl2.Text = "Tipo de numeración de las partidas:"
        '
        'deFechaMinima
        '
        Me.deFechaMinima.EditValue = Nothing
        Me.deFechaMinima.Location = New System.Drawing.Point(285, 39)
        Me.deFechaMinima.Name = "deFechaMinima"
        Me.deFechaMinima.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaMinima.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaMinima.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaMinima.Size = New System.Drawing.Size(100, 20)
        Me.deFechaMinima.TabIndex = 2
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.teCta13)
        Me.XtraTabPage3.Controls.Add(Me.beCta13)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl54)
        Me.XtraTabPage3.Controls.Add(Me.teCta12)
        Me.XtraTabPage3.Controls.Add(Me.beCta12)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl51)
        Me.XtraTabPage3.Controls.Add(Me.teCta11)
        Me.XtraTabPage3.Controls.Add(Me.beCta11)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl50)
        Me.XtraTabPage3.Controls.Add(Me.teCta10)
        Me.XtraTabPage3.Controls.Add(Me.beCta10)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl48)
        Me.XtraTabPage3.Controls.Add(Me.teCta09)
        Me.XtraTabPage3.Controls.Add(Me.beCta09)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl46)
        Me.XtraTabPage3.Controls.Add(Me.teCta08)
        Me.XtraTabPage3.Controls.Add(Me.teCta07)
        Me.XtraTabPage3.Controls.Add(Me.teCta06)
        Me.XtraTabPage3.Controls.Add(Me.teCta05)
        Me.XtraTabPage3.Controls.Add(Me.teCta04)
        Me.XtraTabPage3.Controls.Add(Me.teCta03)
        Me.XtraTabPage3.Controls.Add(Me.teCta02)
        Me.XtraTabPage3.Controls.Add(Me.teCta01)
        Me.XtraTabPage3.Controls.Add(Me.beCta08)
        Me.XtraTabPage3.Controls.Add(Me.beCta07)
        Me.XtraTabPage3.Controls.Add(Me.beCta06)
        Me.XtraTabPage3.Controls.Add(Me.beCta05)
        Me.XtraTabPage3.Controls.Add(Me.beCta04)
        Me.XtraTabPage3.Controls.Add(Me.beCta03)
        Me.XtraTabPage3.Controls.Add(Me.beCta02)
        Me.XtraTabPage3.Controls.Add(Me.beCta01)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl32)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl31)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl34)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl33)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl30)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl29)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl28)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl27)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(975, 446)
        Me.XtraTabPage3.Text = "Cuentas Contables"
        '
        'teCta13
        '
        Me.teCta13.Enabled = False
        Me.teCta13.Location = New System.Drawing.Point(371, 324)
        Me.teCta13.Name = "teCta13"
        Me.teCta13.Size = New System.Drawing.Size(331, 20)
        Me.teCta13.TabIndex = 26
        '
        'beCta13
        '
        Me.beCta13.Location = New System.Drawing.Point(205, 324)
        Me.beCta13.Name = "beCta13"
        Me.beCta13.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta13.Size = New System.Drawing.Size(160, 20)
        Me.beCta13.TabIndex = 13
        '
        'LabelControl54
        '
        Me.LabelControl54.Location = New System.Drawing.Point(101, 327)
        Me.LabelControl54.Name = "LabelControl54"
        Me.LabelControl54.Size = New System.Drawing.Size(99, 13)
        Me.LabelControl54.TabIndex = 25
        Me.LabelControl54.Text = "Cuentas por Cobrar:"
        '
        'teCta12
        '
        Me.teCta12.Enabled = False
        Me.teCta12.Location = New System.Drawing.Point(371, 300)
        Me.teCta12.Name = "teCta12"
        Me.teCta12.Size = New System.Drawing.Size(331, 20)
        Me.teCta12.TabIndex = 23
        '
        'beCta12
        '
        Me.beCta12.Location = New System.Drawing.Point(205, 300)
        Me.beCta12.Name = "beCta12"
        Me.beCta12.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta12.Size = New System.Drawing.Size(160, 20)
        Me.beCta12.TabIndex = 12
        '
        'LabelControl51
        '
        Me.LabelControl51.Location = New System.Drawing.Point(74, 303)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(126, 13)
        Me.LabelControl51.TabIndex = 22
        Me.LabelControl51.Text = "Comisión Doc. Liquidación:"
        '
        'teCta11
        '
        Me.teCta11.Enabled = False
        Me.teCta11.Location = New System.Drawing.Point(371, 277)
        Me.teCta11.Name = "teCta11"
        Me.teCta11.Size = New System.Drawing.Size(331, 20)
        Me.teCta11.TabIndex = 20
        '
        'beCta11
        '
        Me.beCta11.Location = New System.Drawing.Point(205, 277)
        Me.beCta11.Name = "beCta11"
        Me.beCta11.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta11.Size = New System.Drawing.Size(160, 20)
        Me.beCta11.TabIndex = 11
        '
        'LabelControl50
        '
        Me.LabelControl50.Location = New System.Drawing.Point(7, 280)
        Me.LabelControl50.Name = "LabelControl50"
        Me.LabelControl50.Size = New System.Drawing.Size(193, 13)
        Me.LabelControl50.TabIndex = 20
        Me.LabelControl50.Text = "Mercadería en Tránsito (Importaciones):"
        '
        'teCta10
        '
        Me.teCta10.Enabled = False
        Me.teCta10.Location = New System.Drawing.Point(371, 254)
        Me.teCta10.Name = "teCta10"
        Me.teCta10.Size = New System.Drawing.Size(331, 20)
        Me.teCta10.TabIndex = 18
        '
        'beCta10
        '
        Me.beCta10.Location = New System.Drawing.Point(205, 254)
        Me.beCta10.Name = "beCta10"
        Me.beCta10.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta10.Size = New System.Drawing.Size(160, 20)
        Me.beCta10.TabIndex = 10
        '
        'LabelControl48
        '
        Me.LabelControl48.Location = New System.Drawing.Point(88, 258)
        Me.LabelControl48.Name = "LabelControl48"
        Me.LabelControl48.Size = New System.Drawing.Size(112, 13)
        Me.LabelControl48.TabIndex = 11
        Me.LabelControl48.Text = "Percepción IVA Ventas:"
        '
        'teCta09
        '
        Me.teCta09.Enabled = False
        Me.teCta09.Location = New System.Drawing.Point(371, 233)
        Me.teCta09.Name = "teCta09"
        Me.teCta09.Size = New System.Drawing.Size(331, 20)
        Me.teCta09.TabIndex = 16
        '
        'beCta09
        '
        Me.beCta09.Location = New System.Drawing.Point(205, 233)
        Me.beCta09.Name = "beCta09"
        Me.beCta09.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta09.Size = New System.Drawing.Size(160, 20)
        Me.beCta09.TabIndex = 9
        '
        'LabelControl46
        '
        Me.LabelControl46.Location = New System.Drawing.Point(92, 237)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(108, 13)
        Me.LabelControl46.TabIndex = 8
        Me.LabelControl46.Text = "Retención IVA Ventas:"
        '
        'teCta08
        '
        Me.teCta08.Enabled = False
        Me.teCta08.Location = New System.Drawing.Point(371, 211)
        Me.teCta08.Name = "teCta08"
        Me.teCta08.Size = New System.Drawing.Size(331, 20)
        Me.teCta08.TabIndex = 14
        '
        'teCta07
        '
        Me.teCta07.Enabled = False
        Me.teCta07.Location = New System.Drawing.Point(371, 187)
        Me.teCta07.Name = "teCta07"
        Me.teCta07.Size = New System.Drawing.Size(331, 20)
        Me.teCta07.TabIndex = 12
        '
        'teCta06
        '
        Me.teCta06.Enabled = False
        Me.teCta06.Location = New System.Drawing.Point(371, 163)
        Me.teCta06.Name = "teCta06"
        Me.teCta06.Size = New System.Drawing.Size(331, 20)
        Me.teCta06.TabIndex = 10
        '
        'teCta05
        '
        Me.teCta05.Enabled = False
        Me.teCta05.Location = New System.Drawing.Point(371, 139)
        Me.teCta05.Name = "teCta05"
        Me.teCta05.Size = New System.Drawing.Size(331, 20)
        Me.teCta05.TabIndex = 8
        '
        'teCta04
        '
        Me.teCta04.Enabled = False
        Me.teCta04.Location = New System.Drawing.Point(371, 115)
        Me.teCta04.Name = "teCta04"
        Me.teCta04.Size = New System.Drawing.Size(331, 20)
        Me.teCta04.TabIndex = 6
        '
        'teCta03
        '
        Me.teCta03.Enabled = False
        Me.teCta03.Location = New System.Drawing.Point(371, 91)
        Me.teCta03.Name = "teCta03"
        Me.teCta03.Size = New System.Drawing.Size(331, 20)
        Me.teCta03.TabIndex = 5
        '
        'teCta02
        '
        Me.teCta02.Enabled = False
        Me.teCta02.Location = New System.Drawing.Point(371, 67)
        Me.teCta02.Name = "teCta02"
        Me.teCta02.Size = New System.Drawing.Size(331, 20)
        Me.teCta02.TabIndex = 3
        '
        'teCta01
        '
        Me.teCta01.Enabled = False
        Me.teCta01.Location = New System.Drawing.Point(371, 42)
        Me.teCta01.Name = "teCta01"
        Me.teCta01.Size = New System.Drawing.Size(331, 20)
        Me.teCta01.TabIndex = 1
        '
        'beCta08
        '
        Me.beCta08.Location = New System.Drawing.Point(205, 211)
        Me.beCta08.Name = "beCta08"
        Me.beCta08.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta08.Size = New System.Drawing.Size(160, 20)
        Me.beCta08.TabIndex = 8
        '
        'beCta07
        '
        Me.beCta07.Location = New System.Drawing.Point(205, 187)
        Me.beCta07.Name = "beCta07"
        Me.beCta07.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta07.Size = New System.Drawing.Size(160, 20)
        Me.beCta07.TabIndex = 7
        '
        'beCta06
        '
        Me.beCta06.Location = New System.Drawing.Point(205, 163)
        Me.beCta06.Name = "beCta06"
        Me.beCta06.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta06.Size = New System.Drawing.Size(160, 20)
        Me.beCta06.TabIndex = 6
        '
        'beCta05
        '
        Me.beCta05.Location = New System.Drawing.Point(205, 139)
        Me.beCta05.Name = "beCta05"
        Me.beCta05.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta05.Size = New System.Drawing.Size(160, 20)
        Me.beCta05.TabIndex = 5
        '
        'beCta04
        '
        Me.beCta04.Location = New System.Drawing.Point(205, 115)
        Me.beCta04.Name = "beCta04"
        Me.beCta04.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta04.Size = New System.Drawing.Size(160, 20)
        Me.beCta04.TabIndex = 4
        '
        'beCta03
        '
        Me.beCta03.Location = New System.Drawing.Point(205, 91)
        Me.beCta03.Name = "beCta03"
        Me.beCta03.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta03.Size = New System.Drawing.Size(160, 20)
        Me.beCta03.TabIndex = 3
        '
        'beCta02
        '
        Me.beCta02.Location = New System.Drawing.Point(205, 67)
        Me.beCta02.Name = "beCta02"
        Me.beCta02.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta02.Size = New System.Drawing.Size(160, 20)
        Me.beCta02.TabIndex = 2
        '
        'beCta01
        '
        Me.beCta01.Location = New System.Drawing.Point(205, 42)
        Me.beCta01.Name = "beCta01"
        Me.beCta01.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta01.Size = New System.Drawing.Size(160, 20)
        Me.beCta01.TabIndex = 0
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(98, 214)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl32.TabIndex = 0
        Me.LabelControl32.Text = "Percepciones de IVA:"
        '
        'LabelControl31
        '
        Me.LabelControl31.Location = New System.Drawing.Point(61, 142)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(139, 13)
        Me.LabelControl31.TabIndex = 0
        Me.LabelControl31.Text = "Crédito Fiscal Importaciones:"
        '
        'LabelControl34
        '
        Me.LabelControl34.Location = New System.Drawing.Point(74, 190)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(126, 13)
        Me.LabelControl34.TabIndex = 0
        Me.LabelControl34.Text = "Retenciones 2% Tarjetas:"
        '
        'LabelControl33
        '
        Me.LabelControl33.Location = New System.Drawing.Point(102, 166)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(98, 13)
        Me.LabelControl33.TabIndex = 0
        Me.LabelControl33.Text = "Retenciones de IVA:"
        '
        'LabelControl30
        '
        Me.LabelControl30.Location = New System.Drawing.Point(49, 118)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(151, 13)
        Me.LabelControl30.TabIndex = 0
        Me.LabelControl30.Text = "Crédito Fiscal Compras Locales:"
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(52, 95)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(148, 13)
        Me.LabelControl29.TabIndex = 0
        Me.LabelControl29.Text = "Débito Fiscal Consumidor Final:"
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(59, 70)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(141, 13)
        Me.LabelControl28.TabIndex = 0
        Me.LabelControl28.Text = "Débito Fiscal Contribuyentes:"
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(134, 45)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl27.TabIndex = 0
        Me.LabelControl27.Text = "Caja General:"
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.sbtnEliminarEmpresaActual)
        Me.XtraTabPage4.Controls.Add(Me.teMontoAcumuladoNoEfectivo)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl59)
        Me.XtraTabPage4.Controls.Add(Me.teMontoDirectoNoEfectivo)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl58)
        Me.XtraTabPage4.Controls.Add(Me.teMontoAcumuladoEfectivo)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl57)
        Me.XtraTabPage4.Controls.Add(Me.teMontoDirectoEfectivo)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl56)
        Me.XtraTabPage4.Controls.Add(Me.peFoto)
        Me.XtraTabPage4.Controls.Add(Me.lcExisteArchivo)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(975, 446)
        Me.XtraTabPage4.Text = "Logo Empresa/Otros Datos"
        '
        'sbtnEliminarEmpresaActual
        '
        Me.sbtnEliminarEmpresaActual.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.sbtnEliminarEmpresaActual.Location = New System.Drawing.Point(815, 391)
        Me.sbtnEliminarEmpresaActual.Name = "sbtnEliminarEmpresaActual"
        Me.sbtnEliminarEmpresaActual.Size = New System.Drawing.Size(142, 38)
        Me.sbtnEliminarEmpresaActual.TabIndex = 51
        Me.sbtnEliminarEmpresaActual.Text = "(EMP) ELIMINARME ?"
        Me.sbtnEliminarEmpresaActual.Visible = False
        '
        'teMontoAcumuladoNoEfectivo
        '
        Me.teMontoAcumuladoNoEfectivo.EditValue = 0
        Me.teMontoAcumuladoNoEfectivo.EnterMoveNextControl = True
        Me.teMontoAcumuladoNoEfectivo.Location = New System.Drawing.Point(793, 84)
        Me.teMontoAcumuladoNoEfectivo.Name = "teMontoAcumuladoNoEfectivo"
        Me.teMontoAcumuladoNoEfectivo.Properties.Appearance.Options.UseTextOptions = True
        Me.teMontoAcumuladoNoEfectivo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teMontoAcumuladoNoEfectivo.Properties.Mask.EditMask = "n2"
        Me.teMontoAcumuladoNoEfectivo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teMontoAcumuladoNoEfectivo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teMontoAcumuladoNoEfectivo.Size = New System.Drawing.Size(90, 20)
        Me.teMontoAcumuladoNoEfectivo.TabIndex = 3
        '
        'LabelControl59
        '
        Me.LabelControl59.Location = New System.Drawing.Point(537, 87)
        Me.LabelControl59.Name = "LabelControl59"
        Me.LabelControl59.Size = New System.Drawing.Size(252, 13)
        Me.LabelControl59.TabIndex = 50
        Me.LabelControl59.Text = "Monto Acumulado Otras Operaciones NO EFECTIVO:"
        '
        'teMontoDirectoNoEfectivo
        '
        Me.teMontoDirectoNoEfectivo.EditValue = 0
        Me.teMontoDirectoNoEfectivo.EnterMoveNextControl = True
        Me.teMontoDirectoNoEfectivo.Location = New System.Drawing.Point(793, 61)
        Me.teMontoDirectoNoEfectivo.Name = "teMontoDirectoNoEfectivo"
        Me.teMontoDirectoNoEfectivo.Properties.Appearance.Options.UseTextOptions = True
        Me.teMontoDirectoNoEfectivo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teMontoDirectoNoEfectivo.Properties.Mask.EditMask = "n2"
        Me.teMontoDirectoNoEfectivo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teMontoDirectoNoEfectivo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teMontoDirectoNoEfectivo.Size = New System.Drawing.Size(90, 20)
        Me.teMontoDirectoNoEfectivo.TabIndex = 2
        '
        'LabelControl58
        '
        Me.LabelControl58.Location = New System.Drawing.Point(537, 64)
        Me.LabelControl58.Name = "LabelControl58"
        Me.LabelControl58.Size = New System.Drawing.Size(234, 13)
        Me.LabelControl58.TabIndex = 48
        Me.LabelControl58.Text = "Monto Directo Otras Operaciones NO EFECTIVO:"
        '
        'teMontoAcumuladoEfectivo
        '
        Me.teMontoAcumuladoEfectivo.EditValue = 0
        Me.teMontoAcumuladoEfectivo.EnterMoveNextControl = True
        Me.teMontoAcumuladoEfectivo.Location = New System.Drawing.Point(793, 37)
        Me.teMontoAcumuladoEfectivo.Name = "teMontoAcumuladoEfectivo"
        Me.teMontoAcumuladoEfectivo.Properties.Appearance.Options.UseTextOptions = True
        Me.teMontoAcumuladoEfectivo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teMontoAcumuladoEfectivo.Properties.Mask.EditMask = "n2"
        Me.teMontoAcumuladoEfectivo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teMontoAcumuladoEfectivo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teMontoAcumuladoEfectivo.Size = New System.Drawing.Size(90, 20)
        Me.teMontoAcumuladoEfectivo.TabIndex = 1
        '
        'LabelControl57
        '
        Me.LabelControl57.Location = New System.Drawing.Point(537, 40)
        Me.LabelControl57.Name = "LabelControl57"
        Me.LabelControl57.Size = New System.Drawing.Size(209, 13)
        Me.LabelControl57.TabIndex = 46
        Me.LabelControl57.Text = "Monto Acumulado Operaciones en Efectivo:"
        '
        'teMontoDirectoEfectivo
        '
        Me.teMontoDirectoEfectivo.EditValue = 0
        Me.teMontoDirectoEfectivo.EnterMoveNextControl = True
        Me.teMontoDirectoEfectivo.Location = New System.Drawing.Point(793, 14)
        Me.teMontoDirectoEfectivo.Name = "teMontoDirectoEfectivo"
        Me.teMontoDirectoEfectivo.Properties.Appearance.Options.UseTextOptions = True
        Me.teMontoDirectoEfectivo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teMontoDirectoEfectivo.Properties.Mask.EditMask = "n2"
        Me.teMontoDirectoEfectivo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teMontoDirectoEfectivo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teMontoDirectoEfectivo.Size = New System.Drawing.Size(90, 20)
        Me.teMontoDirectoEfectivo.TabIndex = 0
        '
        'LabelControl56
        '
        Me.LabelControl56.Location = New System.Drawing.Point(537, 17)
        Me.LabelControl56.Name = "LabelControl56"
        Me.LabelControl56.Size = New System.Drawing.Size(191, 13)
        Me.LabelControl56.TabIndex = 44
        Me.LabelControl56.Text = "Monto Directo Operaciones en Efectivo:"
        '
        'peFoto
        '
        Me.peFoto.Cursor = System.Windows.Forms.Cursors.Default
        Me.peFoto.Location = New System.Drawing.Point(3, 17)
        Me.peFoto.Name = "peFoto"
        Me.peFoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch
        Me.peFoto.Properties.ZoomAccelerationFactor = 1.0R
        Me.peFoto.Size = New System.Drawing.Size(528, 426)
        ToolTipTitleItem1.Text = "Imagen del producto"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        Me.peFoto.SuperTip = SuperToolTip1
        Me.peFoto.TabIndex = 41
        Me.peFoto.ToolTipTitle = "Doble clic para cambiar o cargar imagen"
        '
        'lcExisteArchivo
        '
        Me.lcExisteArchivo.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lcExisteArchivo.Appearance.Options.UseFont = True
        Me.lcExisteArchivo.Location = New System.Drawing.Point(5, 2)
        Me.lcExisteArchivo.Name = "lcExisteArchivo"
        Me.lcExisteArchivo.Size = New System.Drawing.Size(204, 13)
        Me.lcExisteArchivo.TabIndex = 42
        Me.lcExisteArchivo.Text = "-DOBLE CLIC PARA CARGAR IMAGEN-"
        '
        'OpenFile
        '
        Me.OpenFile.FileName = "OpenFile"
        '
        'LabelControl61
        '
        Me.LabelControl61.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl61.Location = New System.Drawing.Point(894, 426)
        Me.LabelControl61.Name = "LabelControl61"
        Me.LabelControl61.Size = New System.Drawing.Size(70, 13)
        Me.LabelControl61.TabIndex = 57
        Me.LabelControl61.Text = "Nexus vs. ERP"
        '
        'adm_frmSetup
        '
        Me.Appearance.BackColor = System.Drawing.Color.White
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(981, 499)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Administración"
        Me.Name = "adm_frmSetup"
        Me.OptionId = "001002"
        Me.Text = "Configuración del sistema"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.XtraTabControl1, 0)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.XtraTabPage1.PerformLayout()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDepartamento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teMunicipio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceAgente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipoContrib.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.teCargo6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCargo5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCargo4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCargo3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCargo2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCargo1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumeroPatronal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teEmpresa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teActividad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        Me.XtraTabPage2.PerformLayout()
        CType(Me.SpiDiasCreditoVencido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFacturarMenosCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seCorrelOC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceBloquearFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceUtilizarFormularioUnico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceSeccionarFormasPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceVistaPrevia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceAutorizaCheques.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceAlertaMinimos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPartidaCompras.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceLineaCompras.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seCorrelVale.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seCorrelNA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seCorrelTraslados.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seCorrelSalidas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seCorrelEntradas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceValidaExistencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seSujetosExcluidos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePrecios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbeSkins.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPrecio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teMoneda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaMaxima.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaMaxima.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sePorcPercep.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sePorcReten.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sePorcIVA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceGuardarDescuadre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaMinima.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaMinima.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        Me.XtraTabPage3.PerformLayout()
        CType(Me.teCta13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta09.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta09.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta08.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta07.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta06.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta05.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta04.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta03.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta08.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta07.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta06.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta05.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta04.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta03.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage4.ResumeLayout(False)
        Me.XtraTabPage4.PerformLayout()
        CType(Me.teMontoAcumuladoNoEfectivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teMontoDirectoNoEfectivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teMontoAcumuladoEfectivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teMontoDirectoEfectivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peFoto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents cboTipoPartida As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents teMoneda As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaMaxima As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sePorcPercep As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents sePorcReten As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents sePorcIVA As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents ceGuardarDescuadre As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaMinima As DevExpress.XtraEditors.DateEdit
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents teNIT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teEmpresa As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCargo6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNombre6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCargo5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNombre5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCargo4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNombre4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCargo3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNombre3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCargo2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNombre2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCargo1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNombre1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoPrecio As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lePuntoVenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leBodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgTipoContrib As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumeroPatronal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teActividad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceAgente As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cbeSkins As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCta08 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCta07 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCta06 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCta05 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCta04 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCta03 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCta02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCta01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCta08 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beCta07 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beCta06 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beCta05 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beCta04 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beCta03 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beCta02 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beCta01 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents lePrecios As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seCorrelativo As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seSujetosExcluidos As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDepartamento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teMunicipio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seCorrelTraslados As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seCorrelSalidas As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seCorrelEntradas As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceValidaExistencia As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents seCorrelNA As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCta09 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCta09 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seCorrelVale As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCta10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCta10 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceLineaCompras As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents leTipoPartidaCompras As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCta11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCta11 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl50 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceAlertaMinimos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceAutorizaCheques As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents teCta12 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCta12 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceVistaPrevia As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceSeccionarFormasPago As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceUtilizarFormularioUnico As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceBloquearFecha As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents seCorrelOC As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl52 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents peFoto As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents lcExisteArchivo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents OpenFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents teTelefonos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCta13 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCta13 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl54 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkFacturarMenosCosto As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl55 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents teMontoAcumuladoNoEfectivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl59 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teMontoDirectoNoEfectivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl58 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teMontoAcumuladoEfectivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl57 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teMontoDirectoEfectivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl56 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SpiDiasCreditoVencido As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl60 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbtnEliminarEmpresaActual As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl61 As DevExpress.XtraEditors.LabelControl
End Class
