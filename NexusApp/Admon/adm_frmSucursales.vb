﻿Imports NexusELL.TableEntities
Public Class adm_frmSucursales
    Dim entSucursal As adm_Sucursales
    Dim entCuentas As con_Cuentas

    Private Sub com_frmImpuestos_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub com_frmImpuestos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.adm_SucursalesSelectAll
        entSucursal = objTablas.adm_SucursalesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdSucursal"))
        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub com_frmImpuestos_Nuevo_Click() Handles Me.Nuevo
        entSucursal = New adm_Sucursales
        entSucursal.IdSucursal = objFunciones.ObtenerUltimoId("ADM_SUCURSALES", "IdSucursal") + 1
        CargaPantalla()
        ActivaControles(True)

    End Sub
    Private Sub com_frmImpuestos_Save_Click() Handles Me.Guardar
        If beCta01.EditValue = "" Or teNombre.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Nombre, Cuenta Contable y Teléfono]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.adm_SucursalesInsert(entSucursal)
        Else
            objTablas.adm_SucursalesUpdate(entSucursal)
        End If
        gc.DataSource = objTablas.adm_SucursalesSelectAll
        entSucursal = objTablas.adm_SucursalesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdSucursal"))
        CargaPantalla()
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub com_frmImpuestos_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el item seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.adm_SucursalesDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.adm_SucursalesSelectAll()
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL IMPUESTO:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub com_frmImpuestos_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entSucursal
            teIdSucursal.EditValue = .IdSucursal
            teNombre.EditValue = .Nombre
            teTelefonos.EditValue = .Telefonos
            teResponsable.EditValue = .Responsable
            seUltimo.EditValue = .UltimoNumero
            seDesde.EditValue = .DesdeNumero
            seHasta.EditValue = .HastaNumero
            beCta01.EditValue = .IdCuentaCostos
            beCta02.EditValue = .IdCuentaCaja

            entCuentas = objTablas.con_CuentasSelectByPK(beCta01.EditValue)
            teCta01.EditValue = entCuentas.Nombre

            entCuentas = objTablas.con_CuentasSelectByPK(beCta02.EditValue)
            teCta02.EditValue = entCuentas.Nombre
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entSucursal
            .IdSucursal = teIdSucursal.EditValue
            .Nombre = teNombre.EditValue
            .Telefonos = teTelefonos.EditValue
            .Responsable = teResponsable.EditValue
            .UltimoNumero = seUltimo.EditValue
            .DesdeNumero = seDesde.EditValue
            .HastaNumero = seHasta.EditValue
            .IdCuentaCostos = beCta01.EditValue
            .IdCuentaCaja = beCta02.EditValue
        End With
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entSucursal = objTablas.adm_SucursalesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdSucursal"))
        CargaPantalla()
    End Sub

    Private Sub facVendedores_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teIdSucursal.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub
    Private Sub beCta01_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta01.ButtonClick
        If teNombre.Properties.ReadOnly = False Then
            beCta01.EditValue = ""
            entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta01.EditValue)
            beCta01.EditValue = entCuentas.IdCuenta
            teCta01.EditValue = entCuentas.Nombre
        End If
    End Sub

    Private Sub beCta01_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCta01.Validated
        entCuentas = objTablas.con_CuentasSelectByPK(beCta01.EditValue)
        beCta01.EditValue = entCuentas.IdCuenta
        teCta01.EditValue = entCuentas.Nombre
    End Sub


    Private Sub beCta02_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta02.ButtonClick
        If teNombre.Properties.ReadOnly = False Then
            beCta02.EditValue = ""
            entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta02.EditValue)
            beCta02.EditValue = entCuentas.IdCuenta
            teCta02.EditValue = entCuentas.Nombre
        End If
    End Sub

    Private Sub beCta02_Validated(sender As Object, e As EventArgs) Handles beCta02.Validated
        entCuentas = objTablas.con_CuentasSelectByPK(beCta02.EditValue)
        beCta02.EditValue = entCuentas.IdCuenta
        teCta02.EditValue = entCuentas.Nombre
    End Sub
End Class
