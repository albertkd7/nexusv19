﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class adm_frmModificaClave
    Dim bl As New AdmonBLL(g_ConnectionString)
    Dim el As New TableBusiness(g_ConnectionString)
    Dim entUser As New adm_Usuarios
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim _IdUsuario As String = 0
    Dim _Acceso As Boolean = False
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Public Property IdUsuario() As String
        Get
            Return _IdUsuario
        End Get
        Set(ByVal value As String)
            _IdUsuario = value
        End Set
    End Property
    Public Property Acceso() As Boolean
        Get
            Return _Acceso
        End Get
        Set(ByVal value As Boolean)
            _Acceso = value
        End Set
    End Property


    Private Sub adm_frmModificaUsuario_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        entUser = el.adm_UsuariosSelectByPK(IdUsuario) 'objTablas.adm_UsuariosSelectByPK(IdUsuario)
        teId.EditValue = entUser.IdUsuario
    End Sub

    Private Sub btGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        If teNewPass.EditValue = "" Then
            MsgBox("Debe de especificar la Nueva contraseña del usuario", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        If teConPass.EditValue = "" Then
            MsgBox("Debe de Confirmar la Nueva contraseña del usuario", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If teConPass.EditValue <> teNewPass.EditValue Then
            MsgBox("La Nueva Contraseña no coincide con la confirmación", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim Password As String = teNewPass.EditValue
        If Password.Length < 8 Then
            MsgBox("La contraseña debe tener 8 caracteres como mínimo", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If Password.Length > 150 Then
            MsgBox("La contraseña debe tener máximo 150 caracteres", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        'VERIFICO QUE LA CONTRASEÑA TENGA UN NUMERO, UNA MAYUSCULA, Y UN CARACTER ESPECIAL
        Dim ContieneNumero As Boolean = False
        Dim ContieneMayus As Boolean = False
        Dim ContineEspecial As Boolean = False
        Dim ClaveUtilizada As Boolean = False

        Dim len As Integer = Password.Length - 1
        For i As Integer = 0 To len
            Dim ch As Char = Password(i)
            If Char.IsNumber(ch) Then   'Si el caracter es un numero
                ContieneNumero = True
            End If
            If Char.IsUpper(ch) Then    'Si es mayuscula
                ContieneMayus = True
            End If
            If InStr(dtParam.Rows(0).Item("Caracteres"), ch) = 0 Then    'Si es mayuscula
                ContineEspecial = True
            End If
        Next

        If Not ContieneMayus Or Not ContieneNumero Or Not ContineEspecial Then
            lblMensaje.Visible = True
            lblMensaje.Text = "Clave Vulnerable"
            Exit Sub
        Else
            lblMensaje.Visible = True
            lblMensaje.Text = "Clave Segura"
        End If

        Dim entUser As adm_Usuarios = el.adm_UsuariosSelectByPK(IdUsuario)
        Dim ProviderNew As New Security.Cryptography.SHA1CryptoServiceProvider
        Dim BytesNew As Byte() = System.Text.Encoding.UTF8.GetBytes(teNewPass.EditValue)
        Dim inArrayNew As Byte() = ProviderNew.ComputeHash(BytesNew)
        ProviderNew.Clear()
        'encripto el password
        Dim sPassWord As String = Convert.ToBase64String(inArrayNew)

        'VERIFICO QUE LA NUEVA CLAVE NO SEA DE LAS ULTIMAS 6 CREADAS
        Dim dtUltimasClaves As DataTable = bl.ObtenerUltimasClaves(teId.EditValue)
        For i = 0 To dtUltimasClaves.Rows.Count - 1
            If (dtUltimasClaves.Rows(i).Item("Clave")) = sPassWord Then
                ClaveUtilizada = True
            End If
        Next

        If ClaveUtilizada Then
            MsgBox("La contraseña debe ser diferente a las ultimas 12 utilizadas", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        entUser.IdUsuario = teId.EditValue
        entUser.Clave = sPassWord
        entUser.FechaUltCambioClave = Today
        'entUser.ModificadoPor = teId.EditValue
        'entUser.FechaHoraModificacion = Now
        Try
            el.adm_UsuariosUpdate(entUser)
            blAdmon.HistorialClaves(teId.EditValue, sPassWord)
            MsgBox("La clave ha sido guardada", MsgBoxStyle.Information, "Nota")
            Acceso = True
        Catch ex As Exception
            MsgBox("No fue posible guardar el cambio de clave" & Chr(13) & ex.Message(), 16, "Verifique el error")
            Acceso = False
            Exit Sub
        End Try
        Me.Close()
    End Sub

    Private Sub SimpleButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SimpleButton2.Click
        Acceso = False
        Me.Close()
    End Sub

End Class