﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adm_frmModificaUsuario
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(adm_frmModificaUsuario))
        Me.sbResetear = New DevExpress.XtraEditors.SimpleButton()
        Me.btGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.leVendedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.leBodega = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.seDias = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.leTipoPrecio = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.teNombres = New DevExpress.XtraEditors.TextEdit()
        Me.teApellidos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teId = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.xtpVarias = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.LeUsuarios = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.seHastaDescto = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.seDesdeDescto = New DevExpress.XtraEditors.SpinEdit()
        Me.ceBloqueoBodega = New DevExpress.XtraEditors.CheckEdit()
        Me.chkRevertirInventarios = New DevExpress.XtraEditors.CheckEdit()
        Me.ceAutorizaDescuentos = New DevExpress.XtraEditors.CheckEdit()
        Me.ceTodasSucursales = New DevExpress.XtraEditors.CheckEdit()
        Me.ceCredito = New DevExpress.XtraEditors.CheckEdit()
        Me.ceActivo = New DevExpress.XtraEditors.CheckEdit()
        Me.chkCambiaDescripciones = New DevExpress.XtraEditors.CheckEdit()
        Me.chkCambiarPrecios = New DevExpress.XtraEditors.CheckEdit()
        Me.xtpAccesoPrecios = New DevExpress.XtraTab.XtraTabPage()
        Me.gcPrecios = New DevExpress.XtraGrid.GridControl()
        Me.gvPrecios = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.IdPrecio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Nombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Acceso = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riceAcceso = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.xtpAccesoSucursales = New DevExpress.XtraTab.XtraTabPage()
        Me.gcSucursales = New DevExpress.XtraGrid.GridControl()
        Me.gvSucursales = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.leSucursalUsuario = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.lePuntoVenta = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.xtpDatos.SuspendLayout()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDias.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPrecio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombres.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teApellidos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpVarias.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.LeUsuarios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seHastaDescto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDesdeDescto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceBloqueoBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRevertirInventarios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceAutorizaDescuentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceTodasSucursales.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCambiaDescripciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCambiarPrecios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpAccesoPrecios.SuspendLayout()
        CType(Me.gcPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riceAcceso, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpAccesoSucursales.SuspendLayout()
        CType(Me.gcSucursales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvSucursales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sbResetear
        '
        Me.sbResetear.Location = New System.Drawing.Point(1, 279)
        Me.sbResetear.Name = "sbResetear"
        Me.sbResetear.Size = New System.Drawing.Size(126, 21)
        Me.sbResetear.TabIndex = 14
        Me.sbResetear.Text = "&Resetear contraseña"
        '
        'btGuardar
        '
        Me.btGuardar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btGuardar.Image = Global.Nexus.My.Resources.Resources.Save
        Me.btGuardar.Location = New System.Drawing.Point(602, 276)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(90, 28)
        Me.btGuardar.TabIndex = 13
        Me.btGuardar.Text = "&Guardar"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.xtpDatos
        Me.XtraTabControl1.Size = New System.Drawing.Size(704, 274)
        Me.XtraTabControl1.TabIndex = 124
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpDatos, Me.xtpVarias, Me.xtpAccesoPrecios, Me.xtpAccesoSucursales})
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.LabelControl11)
        Me.xtpDatos.Controls.Add(Me.lePuntoVenta)
        Me.xtpDatos.Controls.Add(Me.LabelControl21)
        Me.xtpDatos.Controls.Add(Me.leVendedor)
        Me.xtpDatos.Controls.Add(Me.LabelControl20)
        Me.xtpDatos.Controls.Add(Me.leBodega)
        Me.xtpDatos.Controls.Add(Me.LabelControl14)
        Me.xtpDatos.Controls.Add(Me.seDias)
        Me.xtpDatos.Controls.Add(Me.LabelControl8)
        Me.xtpDatos.Controls.Add(Me.deFecha)
        Me.xtpDatos.Controls.Add(Me.leTipoPrecio)
        Me.xtpDatos.Controls.Add(Me.LabelControl2)
        Me.xtpDatos.Controls.Add(Me.LabelControl5)
        Me.xtpDatos.Controls.Add(Me.leSucursal)
        Me.xtpDatos.Controls.Add(Me.teNombres)
        Me.xtpDatos.Controls.Add(Me.teApellidos)
        Me.xtpDatos.Controls.Add(Me.LabelControl4)
        Me.xtpDatos.Controls.Add(Me.LabelControl3)
        Me.xtpDatos.Controls.Add(Me.teId)
        Me.xtpDatos.Controls.Add(Me.LabelControl1)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(698, 246)
        Me.xtpDatos.Text = "Datos Generales"
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(114, 71)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(97, 13)
        Me.LabelControl21.TabIndex = 141
        Me.LabelControl21.Text = "Vendedor Asignado:"
        '
        'leVendedor
        '
        Me.leVendedor.EnterMoveNextControl = True
        Me.leVendedor.Location = New System.Drawing.Point(218, 67)
        Me.leVendedor.Name = "leVendedor"
        Me.leVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leVendedor.Size = New System.Drawing.Size(196, 20)
        Me.leVendedor.TabIndex = 140
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(82, 139)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(133, 13)
        Me.LabelControl20.TabIndex = 139
        Me.LabelControl20.Text = "Bodega a la que pertenece:"
        '
        'leBodega
        '
        Me.leBodega.EnterMoveNextControl = True
        Me.leBodega.Location = New System.Drawing.Point(218, 135)
        Me.leBodega.Name = "leBodega"
        Me.leBodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leBodega.Size = New System.Drawing.Size(196, 20)
        Me.leBodega.TabIndex = 128
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(72, 202)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(143, 13)
        Me.LabelControl14.TabIndex = 138
        Me.LabelControl14.Text = "Período de Cambios de Clave:"
        '
        'seDias
        '
        Me.seDias.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDias.EnterMoveNextControl = True
        Me.seDias.Location = New System.Drawing.Point(218, 200)
        Me.seDias.Name = "seDias"
        Me.seDias.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDias.Properties.Mask.EditMask = "n0"
        Me.seDias.Size = New System.Drawing.Size(112, 20)
        Me.seDias.TabIndex = 131
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(98, 181)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(117, 13)
        Me.LabelControl8.TabIndex = 137
        Me.LabelControl8.Text = "Vencimiento del Usuario:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(218, 178)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Size = New System.Drawing.Size(112, 20)
        Me.deFecha.TabIndex = 130
        '
        'leTipoPrecio
        '
        Me.leTipoPrecio.Location = New System.Drawing.Point(218, 156)
        Me.leTipoPrecio.Name = "leTipoPrecio"
        Me.leTipoPrecio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPrecio.Size = New System.Drawing.Size(196, 20)
        Me.leTipoPrecio.TabIndex = 129
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(4, 159)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(211, 13)
        Me.LabelControl2.TabIndex = 136
        Me.LabelControl2.Text = "Tipo de Precio Predeterminado para Ventas:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(171, 92)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl5.TabIndex = 135
        Me.LabelControl5.Text = "Sucursal:"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(218, 89)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(196, 20)
        Me.leSucursal.TabIndex = 127
        '
        'teNombres
        '
        Me.teNombres.Location = New System.Drawing.Point(218, 24)
        Me.teNombres.Name = "teNombres"
        Me.teNombres.Size = New System.Drawing.Size(196, 20)
        Me.teNombres.TabIndex = 125
        '
        'teApellidos
        '
        Me.teApellidos.Location = New System.Drawing.Point(218, 45)
        Me.teApellidos.Name = "teApellidos"
        Me.teApellidos.Size = New System.Drawing.Size(196, 20)
        Me.teApellidos.TabIndex = 126
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(113, 49)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl4.TabIndex = 134
        Me.LabelControl4.Text = "Apellidos del Usuario:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(118, 27)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(97, 13)
        Me.LabelControl3.TabIndex = 133
        Me.LabelControl3.Text = "Nombre del Usuario:"
        '
        'teId
        '
        Me.teId.Location = New System.Drawing.Point(218, 3)
        Me.teId.Name = "teId"
        Me.teId.Properties.ReadOnly = True
        Me.teId.Size = New System.Drawing.Size(196, 20)
        Me.teId.TabIndex = 124
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(147, 6)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl1.TabIndex = 132
        Me.LabelControl1.Text = "Identificación:"
        '
        'xtpVarias
        '
        Me.xtpVarias.Controls.Add(Me.GroupBox1)
        Me.xtpVarias.Controls.Add(Me.LabelControl19)
        Me.xtpVarias.Controls.Add(Me.seHastaDescto)
        Me.xtpVarias.Controls.Add(Me.LabelControl26)
        Me.xtpVarias.Controls.Add(Me.seDesdeDescto)
        Me.xtpVarias.Controls.Add(Me.ceBloqueoBodega)
        Me.xtpVarias.Controls.Add(Me.chkRevertirInventarios)
        Me.xtpVarias.Controls.Add(Me.ceAutorizaDescuentos)
        Me.xtpVarias.Controls.Add(Me.ceTodasSucursales)
        Me.xtpVarias.Controls.Add(Me.ceCredito)
        Me.xtpVarias.Controls.Add(Me.ceActivo)
        Me.xtpVarias.Controls.Add(Me.chkCambiaDescripciones)
        Me.xtpVarias.Controls.Add(Me.chkCambiarPrecios)
        Me.xtpVarias.Name = "xtpVarias"
        Me.xtpVarias.Size = New System.Drawing.Size(698, 246)
        Me.xtpVarias.Text = "Permisos Varios"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.SimpleButton1)
        Me.GroupBox1.Controls.Add(Me.LeUsuarios)
        Me.GroupBox1.Controls.Add(Me.LabelControl6)
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.Location = New System.Drawing.Point(341, 102)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(334, 100)
        Me.GroupBox1.TabIndex = 144
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Copiar Permisos de Usuario"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.SimpleButton1.Image = Global.Nexus.My.Resources.Resources.autoriza3
        Me.SimpleButton1.Location = New System.Drawing.Point(238, 57)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(90, 28)
        Me.SimpleButton1.TabIndex = 144
        Me.SimpleButton1.Text = "&Procesar"
        '
        'LeUsuarios
        '
        Me.LeUsuarios.EnterMoveNextControl = True
        Me.LeUsuarios.Location = New System.Drawing.Point(71, 31)
        Me.LeUsuarios.Name = "LeUsuarios"
        Me.LeUsuarios.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LeUsuarios.Size = New System.Drawing.Size(257, 20)
        Me.LeUsuarios.TabIndex = 142
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(27, 34)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl6.TabIndex = 143
        Me.LabelControl6.Text = "Usuario:"
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(381, 51)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl19.TabIndex = 133
        Me.LabelControl19.Text = "Hasta % Descto:"
        '
        'seHastaDescto
        '
        Me.seHastaDescto.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seHastaDescto.EnterMoveNextControl = True
        Me.seHastaDescto.Location = New System.Drawing.Point(466, 47)
        Me.seHastaDescto.Name = "seHastaDescto"
        Me.seHastaDescto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seHastaDescto.Properties.Mask.EditMask = "P2"
        Me.seHastaDescto.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seHastaDescto.Size = New System.Drawing.Size(68, 20)
        Me.seHastaDescto.TabIndex = 132
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(379, 30)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl26.TabIndex = 131
        Me.LabelControl26.Text = "Desde % Descto:"
        '
        'seDesdeDescto
        '
        Me.seDesdeDescto.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDesdeDescto.EnterMoveNextControl = True
        Me.seDesdeDescto.Location = New System.Drawing.Point(466, 26)
        Me.seDesdeDescto.Name = "seDesdeDescto"
        Me.seDesdeDescto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDesdeDescto.Properties.Mask.EditMask = "P2"
        Me.seDesdeDescto.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seDesdeDescto.Size = New System.Drawing.Size(68, 20)
        Me.seDesdeDescto.TabIndex = 130
        '
        'ceBloqueoBodega
        '
        Me.ceBloqueoBodega.Location = New System.Drawing.Point(41, 154)
        Me.ceBloqueoBodega.Name = "ceBloqueoBodega"
        Me.ceBloqueoBodega.Properties.Caption = "Bloquear bodega al Facturar"
        Me.ceBloqueoBodega.Size = New System.Drawing.Size(221, 19)
        Me.ceBloqueoBodega.TabIndex = 129
        '
        'chkRevertirInventarios
        '
        Me.chkRevertirInventarios.Location = New System.Drawing.Point(41, 136)
        Me.chkRevertirInventarios.Name = "chkRevertirInventarios"
        Me.chkRevertirInventarios.Properties.Caption = "Permitir Revertir Inventarios en Negativo"
        Me.chkRevertirInventarios.Size = New System.Drawing.Size(221, 19)
        Me.chkRevertirInventarios.TabIndex = 128
        '
        'ceAutorizaDescuentos
        '
        Me.ceAutorizaDescuentos.Location = New System.Drawing.Point(41, 118)
        Me.ceAutorizaDescuentos.Name = "ceAutorizaDescuentos"
        Me.ceAutorizaDescuentos.Properties.Caption = "Podrá Autorizar Descuentos y rebajas"
        Me.ceAutorizaDescuentos.Size = New System.Drawing.Size(221, 19)
        Me.ceAutorizaDescuentos.TabIndex = 127
        '
        'ceTodasSucursales
        '
        Me.ceTodasSucursales.Location = New System.Drawing.Point(41, 99)
        Me.ceTodasSucursales.Name = "ceTodasSucursales"
        Me.ceTodasSucursales.Properties.Caption = "Visualizar Todas las Sucursales"
        Me.ceTodasSucursales.Size = New System.Drawing.Size(243, 19)
        Me.ceTodasSucursales.TabIndex = 126
        '
        'ceCredito
        '
        Me.ceCredito.Location = New System.Drawing.Point(41, 81)
        Me.ceCredito.Name = "ceCredito"
        Me.ceCredito.Properties.Caption = "Podrá Autorizar Créditos"
        Me.ceCredito.Size = New System.Drawing.Size(243, 19)
        Me.ceCredito.TabIndex = 124
        '
        'ceActivo
        '
        Me.ceActivo.Location = New System.Drawing.Point(41, 26)
        Me.ceActivo.Name = "ceActivo"
        Me.ceActivo.Properties.Caption = "Usuario Activo"
        Me.ceActivo.Size = New System.Drawing.Size(94, 19)
        Me.ceActivo.TabIndex = 125
        '
        'chkCambiaDescripciones
        '
        Me.chkCambiaDescripciones.Location = New System.Drawing.Point(41, 62)
        Me.chkCambiaDescripciones.Name = "chkCambiaDescripciones"
        Me.chkCambiaDescripciones.Properties.Caption = "Permitir Cambiar Descripciones de Productos"
        Me.chkCambiaDescripciones.Size = New System.Drawing.Size(243, 19)
        Me.chkCambiaDescripciones.TabIndex = 123
        '
        'chkCambiarPrecios
        '
        Me.chkCambiarPrecios.Location = New System.Drawing.Point(41, 44)
        Me.chkCambiarPrecios.Name = "chkCambiarPrecios"
        Me.chkCambiarPrecios.Properties.Caption = "Podrá Cambiar Precios de Venta al Facturar"
        Me.chkCambiarPrecios.Size = New System.Drawing.Size(243, 19)
        Me.chkCambiarPrecios.TabIndex = 122
        '
        'xtpAccesoPrecios
        '
        Me.xtpAccesoPrecios.Controls.Add(Me.gcPrecios)
        Me.xtpAccesoPrecios.Name = "xtpAccesoPrecios"
        Me.xtpAccesoPrecios.Size = New System.Drawing.Size(698, 246)
        Me.xtpAccesoPrecios.Text = "Acceso a Precios de Venta"
        '
        'gcPrecios
        '
        Me.gcPrecios.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcPrecios.Location = New System.Drawing.Point(0, 0)
        Me.gcPrecios.MainView = Me.gvPrecios
        Me.gcPrecios.Name = "gcPrecios"
        Me.gcPrecios.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riceAcceso})
        Me.gcPrecios.Size = New System.Drawing.Size(698, 246)
        Me.gcPrecios.TabIndex = 75
        Me.gcPrecios.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvPrecios})
        '
        'gvPrecios
        '
        Me.gvPrecios.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.IdPrecio, Me.Nombre, Me.Acceso})
        Me.gvPrecios.GridControl = Me.gcPrecios
        Me.gvPrecios.Name = "gvPrecios"
        Me.gvPrecios.OptionsView.ShowGroupPanel = False
        '
        'IdPrecio
        '
        Me.IdPrecio.Caption = "Precio"
        Me.IdPrecio.FieldName = "IdPrecio"
        Me.IdPrecio.Name = "IdPrecio"
        Me.IdPrecio.OptionsColumn.AllowEdit = False
        Me.IdPrecio.Visible = True
        Me.IdPrecio.VisibleIndex = 0
        Me.IdPrecio.Width = 45
        '
        'Nombre
        '
        Me.Nombre.Caption = "Nombre"
        Me.Nombre.FieldName = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.OptionsColumn.AllowEdit = False
        Me.Nombre.Visible = True
        Me.Nombre.VisibleIndex = 1
        Me.Nombre.Width = 181
        '
        'Acceso
        '
        Me.Acceso.Caption = "Acceso"
        Me.Acceso.ColumnEdit = Me.riceAcceso
        Me.Acceso.FieldName = "Acceso"
        Me.Acceso.Name = "Acceso"
        Me.Acceso.Visible = True
        Me.Acceso.VisibleIndex = 2
        Me.Acceso.Width = 49
        '
        'riceAcceso
        '
        Me.riceAcceso.AutoHeight = False
        Me.riceAcceso.Name = "riceAcceso"
        '
        'xtpAccesoSucursales
        '
        Me.xtpAccesoSucursales.Controls.Add(Me.gcSucursales)
        Me.xtpAccesoSucursales.Name = "xtpAccesoSucursales"
        Me.xtpAccesoSucursales.Size = New System.Drawing.Size(698, 246)
        Me.xtpAccesoSucursales.Text = "Acceso a Sucursales"
        '
        'gcSucursales
        '
        Me.gcSucursales.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcSucursales.Location = New System.Drawing.Point(0, 0)
        Me.gcSucursales.MainView = Me.gvSucursales
        Me.gcSucursales.Name = "gcSucursales"
        Me.gcSucursales.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1, Me.leSucursalUsuario})
        Me.gcSucursales.Size = New System.Drawing.Size(698, 246)
        Me.gcSucursales.TabIndex = 84
        Me.gcSucursales.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvSucursales})
        '
        'gvSucursales
        '
        Me.gvSucursales.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.gvSucursales.GridControl = Me.gcSucursales
        Me.gvSucursales.Name = "gvSucursales"
        Me.gvSucursales.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdSucursal"
        Me.GridColumn1.FieldName = "IdSucursal"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 45
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Sucursal"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 181
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Acceso"
        Me.GridColumn3.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.GridColumn3.FieldName = "Acceso"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 49
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'leSucursalUsuario
        '
        Me.leSucursalUsuario.AutoHeight = False
        Me.leSucursalUsuario.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalUsuario.Name = "leSucursalUsuario"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(135, 117)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl11.TabIndex = 180
        Me.LabelControl11.Text = "Punto de Venta:"
        '
        'lePuntoVenta
        '
        Me.lePuntoVenta.EnterMoveNextControl = True
        Me.lePuntoVenta.Location = New System.Drawing.Point(218, 113)
        Me.lePuntoVenta.Name = "lePuntoVenta"
        Me.lePuntoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePuntoVenta.Size = New System.Drawing.Size(196, 20)
        Me.lePuntoVenta.TabIndex = 179
        '
        'adm_frmModificaUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btGuardar
        Me.ClientSize = New System.Drawing.Size(704, 307)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.sbResetear)
        Me.Controls.Add(Me.btGuardar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "adm_frmModificaUsuario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Modificar Usuarios"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.xtpDatos.ResumeLayout(False)
        Me.xtpDatos.PerformLayout()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDias.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPrecio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombres.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teApellidos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpVarias.ResumeLayout(False)
        Me.xtpVarias.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.LeUsuarios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seHastaDescto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDesdeDescto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceBloqueoBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRevertirInventarios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceAutorizaDescuentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceTodasSucursales.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCambiaDescripciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCambiarPrecios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpAccesoPrecios.ResumeLayout(False)
        CType(Me.gcPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riceAcceso, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpAccesoSucursales.ResumeLayout(False)
        CType(Me.gcSucursales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvSucursales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents sbResetear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leVendedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leBodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seDias As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents leTipoPrecio As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teNombres As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teApellidos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teId As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents xtpVarias As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seHastaDescto As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seDesdeDescto As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents ceBloqueoBodega As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkRevertirInventarios As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceAutorizaDescuentos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceTodasSucursales As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceCredito As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkCambiaDescripciones As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkCambiarPrecios As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents xtpAccesoPrecios As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gcPrecios As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvPrecios As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents IdPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Nombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Acceso As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riceAcceso As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents xtpAccesoSucursales As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gcSucursales As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvSucursales As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents leSucursalUsuario As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LeUsuarios As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lePuntoVenta As DevExpress.XtraEditors.LookUpEdit
End Class
