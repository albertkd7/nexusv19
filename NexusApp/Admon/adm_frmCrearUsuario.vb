Imports NexusBLL
Imports NexusELL.TableEntities
Public Class adm_frmCrearUsuario
    Dim myBL As New TableBusiness(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim blFac As New FacturaBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Private Sub adm_frmCrearUsuario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tePass.EditValue = ""
        teId.EditValue = ""
        teApellidos.EditValue = ""
        teNombres.EditValue = ""
        deFecha.EditValue = DateAdd(DateInterval.Day, 90, Today)
        seDias.EditValue = 90
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalUsuario, objMenu.User, "")
        objCombos.inv_Bodegas(leBodega, "")
        objCombos.inv_Precios(leTipoPrecio, "")
        objCombos.fac_Vendedores(leVendedor, "--NO APLICA--")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "--AUTO-ELEGIBLE--")
        gcPrecios.DataSource = blAdmon.ObtenerPreciosUsuario("")
        gcSucursales.DataSource = blAdmon.ObtenerSucursalesUsuario("")

        leVendedor.EditValue = -1
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        If teId.EditValue = "" Then
            MsgBox("Debe de especificar la identificaci�n del usuario", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        If tePass.EditValue = "" Then
            MsgBox("Debe de especificar la contrase�a del usuario", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        If tePass.EditValue <> tePass2.EditValue Then
            MsgBox("Las contrase�as no co�nciden", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        If seDias.EditValue <= 0 Then
            MsgBox("Debe de especificar el per�odo de cambios de claves", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        If deFecha.EditValue = Nothing Or deFecha.EditValue <= Today Then
            MsgBox("Debe de especificar una fecha de vencimiento correcta al usuario", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim Password As String = tePass.EditValue
        If Password.Length < 6 Then
            MsgBox("La contrase�a debe tener 6 caracteres como m�nimo", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        If Password.Length > 150 Then
            MsgBox("La contrase�a debe tener m�ximo 150 caracteres", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        'VERIFICO QUE LA CONTRASE�A TENGA UN NUMERO, UNA MAYUSCULA, Y UN CARACTER ESPECIAL
        Dim ContieneNumero As Boolean = False, ContieneMayus As Boolean = False, ContieneEspecial As Boolean = False

        Dim len As Integer = Password.Length - 1
        For i As Integer = 0 To len
            Dim ch As Char = Password(i)
            If Char.IsNumber(ch) Then   'Si el caracter es un numero
                ContieneNumero = True
            End If
            If Char.IsUpper(ch) Then    'Si es mayuscula
                ContieneMayus = True
            End If
            If InStr(dtParam.Rows(0).Item("Caracteres"), ch) = 0 Then    'Si es mayuscula
                ContieneEspecial = True
            End If
        Next
        lblMensaje.Text = "Clave vulnerable. No cumple con las politicas"
        If Not ContieneMayus Or Not ContieneNumero Or Not ContieneEspecial Then
            'MsgBox("La contrase�a debe poseer Un numero, Una Letra Mayuscula y un Caracter Especial", MsgBoxStyle.Information, "Nota")
            lblMensaje.Visible = True
            Exit Sub
        Else
            lblMensaje.Visible = True
            lblMensaje.Text = "Clave Segura"
        End If

        Dim entUser As New adm_Usuarios
        Dim Provider As New Security.Cryptography.SHA1CryptoServiceProvider
        Dim Bytes As Byte() = System.Text.Encoding.UTF8.GetBytes(tePass.EditValue)
        Dim inArray As Byte() = provider.ComputeHash(bytes)
        Provider.Clear()

        'encripto el password
        Dim sPassWord As String = Convert.ToBase64String(inArray)
        Dim FechaServer As Date = blFac.fac_FechaServer()

        entUser.IdUsuario = teId.EditValue
        entUser.Clave = sPassWord
        entUser.FechaCreacion = Today
        entUser.FechaVencimiento = deFecha.EditValue
        entUser.FechaUltCambioClave = FechaServer
        entUser.VigenciaClave = seDias.EditValue
        entUser.Estado = 1
        entUser.IdBodega = leBodega.EditValue
        entUser.IdSucursal = leSucursal.EditValue
        entUser.IdTipoPrecio = leTipoPrecio.EditValue
        entUser.CambiarPrecios = ceCambiarPrecios.EditValue
        entUser.AccesoProductos = ceCambiaDescripciones.EditValue
        entUser.AutorizaCredito = ceCredito.EditValue
        entUser.TodasLasSucursales = ceTodasSucursales.EditValue
        entUser.AutorizarDescuentos = ceAutorizaDescuentos.EditValue
        entUser.AutorizarRevertir = chkRevertirInventarios.EditValue
        entUser.BloquearBodega = ceBloqueoBodega.EditValue
        entUser.DesdeDescuentos = seDesdeDescto.EditValue
        entUser.HastaDescuentos = seHastaDescto.EditValue
        entUser.IdVendedor = leVendedor.EditValue
        entUser.Nombre = teNombres.EditValue
        entUser.Apellidos = teApellidos.EditValue
        entUser.IdPunto = lePuntoVenta.EditValue

        myBL.adm_UsuariosInsert(entUser)

        blAdmon.HistorialClaves(teId.EditValue, sPassWord)
        blAdmon.EliminarPreciosUsuario(teId.EditValue)
        blAdmon.EliminarSucursalesUsuario(teId.EditValue)

        For i = 0 To gvPrecios.DataRowCount - 1
            If gvPrecios.GetRowCellValue(i, "Acceso") Then
                blAdmon.InsertarPrecioUsuario(teId.EditValue, gvPrecios.GetRowCellValue(i, "IdPrecio"), objMenu.User)
            End If
        Next
        For i = 0 To gvSucursales.DataRowCount - 1
            If gvSucursales.GetRowCellValue(i, "Acceso") Then
                blAdmon.InsertarSucursalUsuario(teId.EditValue, gvSucursales.GetRowCellValue(i, "IdSucursal"), objMenu.User)
            End If
        Next
        MsgBox("El usuario ha sido guardado", MsgBoxStyle.Information, "Nota")
        Me.Close()
    End Sub

    Private Sub SimpleButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub

    Private Sub leSucursal_EditValueChanged(sender As Object, e As EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "--AUTO-ELEGIBLE--")
    End Sub
End Class