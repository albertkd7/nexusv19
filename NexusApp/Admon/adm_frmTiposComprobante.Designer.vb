﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adm_frmTiposComprobante
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.riteTipoAplicacion = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.riteLibro = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.chkDetalla = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.riteModulo = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteTipoAplicacion1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteLibro1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteModulo1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkFormulario = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.ceFormulario = New DevExpress.XtraEditors.CheckEdit()
        Me.teImpresor = New DevExpress.XtraEditors.TextEdit()
        Me.teCta01 = New DevExpress.XtraEditors.TextEdit()
        Me.beCta01 = New DevExpress.XtraEditors.ButtonEdit()
        Me.seNumLineas = New DevExpress.XtraEditors.SpinEdit()
        Me.leModulo = New DevExpress.XtraEditors.LookUpEdit()
        Me.leLibro = New DevExpress.XtraEditors.LookUpEdit()
        Me.leTipoAplicacion = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdComprobante = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.ceDetallaIva = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.teAbreviatura = New DevExpress.XtraEditors.TextEdit()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        CType(Me.riteTipoAplicacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteLibro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkDetalla, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteModulo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteTipoAplicacion1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteLibro1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteModulo1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFormulario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.ceFormulario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teImpresor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seNumLineas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leModulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leLibro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoAplicacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdComprobante.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceDetallaIva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teAbreviatura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'riteTipoAplicacion
        '
        Me.riteTipoAplicacion.AutoHeight = False
        Me.riteTipoAplicacion.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteTipoAplicacion.LookAndFeel.SkinName = "Money Twins"
        Me.riteTipoAplicacion.Name = "riteTipoAplicacion"
        '
        'riteLibro
        '
        Me.riteLibro.AutoHeight = False
        Me.riteLibro.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteLibro.Name = "riteLibro"
        '
        'chkDetalla
        '
        Me.chkDetalla.AutoHeight = False
        Me.chkDetalla.Name = "chkDetalla"
        '
        'riteModulo
        '
        Me.riteModulo.AutoHeight = False
        Me.riteModulo.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteModulo.Name = "riteModulo"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.gc)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(743, 469)
        Me.PanelControl1.TabIndex = 4
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(2, 2)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteTipoAplicacion1, Me.riteModulo1, Me.riteLibro1, Me.chkFormulario})
        Me.gc.Size = New System.Drawing.Size(739, 465)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn11})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdTipo"
        Me.GridColumn1.FieldName = "IdTipoComprobante"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 42
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombre"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 169
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Abreviatura"
        Me.GridColumn3.FieldName = "Abreviatura"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 53
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "TipoAplicacion"
        Me.GridColumn4.ColumnEdit = Me.riteTipoAplicacion1
        Me.GridColumn4.FieldName = "TipoAplicacion"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 44
        '
        'riteTipoAplicacion1
        '
        Me.riteTipoAplicacion1.AutoHeight = False
        Me.riteTipoAplicacion1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteTipoAplicacion1.Name = "riteTipoAplicacion1"
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Libro"
        Me.GridColumn5.ColumnEdit = Me.riteLibro1
        Me.GridColumn5.FieldName = "IdLibro"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 4
        Me.GridColumn5.Width = 44
        '
        'riteLibro1
        '
        Me.riteLibro1.AutoHeight = False
        Me.riteLibro1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteLibro1.Name = "riteLibro1"
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Detalla IVA?"
        Me.GridColumn6.ColumnEdit = Me.chkDetalla
        Me.GridColumn6.FieldName = "DetallaIVA"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 5
        Me.GridColumn6.Width = 44
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Aplica a Modulo"
        Me.GridColumn7.ColumnEdit = Me.riteModulo1
        Me.GridColumn7.FieldName = "IdModuloAplica"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 6
        Me.GridColumn7.Width = 44
        '
        'riteModulo1
        '
        Me.riteModulo1.AutoHeight = False
        Me.riteModulo1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteModulo1.Name = "riteModulo1"
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Lineas en Documento"
        Me.GridColumn8.FieldName = "NumeroLineas"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 7
        Me.GridColumn8.Width = 44
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Cuenta Inventario"
        Me.GridColumn9.FieldName = "IdCuentaInv"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 8
        Me.GridColumn9.Width = 44
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Imprimir En"
        Me.GridColumn10.FieldName = "NombreImpresor"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 9
        Me.GridColumn10.Width = 66
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Formulario Unico?"
        Me.GridColumn11.ColumnEdit = Me.chkFormulario
        Me.GridColumn11.FieldName = "FormularioUnico"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 10
        '
        'chkFormulario
        '
        Me.chkFormulario.AutoHeight = False
        Me.chkFormulario.Name = "chkFormulario"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.ceFormulario)
        Me.PanelControl2.Controls.Add(Me.teImpresor)
        Me.PanelControl2.Controls.Add(Me.teCta01)
        Me.PanelControl2.Controls.Add(Me.beCta01)
        Me.PanelControl2.Controls.Add(Me.seNumLineas)
        Me.PanelControl2.Controls.Add(Me.leModulo)
        Me.PanelControl2.Controls.Add(Me.leLibro)
        Me.PanelControl2.Controls.Add(Me.leTipoAplicacion)
        Me.PanelControl2.Controls.Add(Me.LabelControl9)
        Me.PanelControl2.Controls.Add(Me.LabelControl8)
        Me.PanelControl2.Controls.Add(Me.LabelControl5)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.teIdComprobante)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.teNombre)
        Me.PanelControl2.Controls.Add(Me.ceDetallaIva)
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.LabelControl4)
        Me.PanelControl2.Controls.Add(Me.LabelControl6)
        Me.PanelControl2.Controls.Add(Me.LabelControl7)
        Me.PanelControl2.Controls.Add(Me.teAbreviatura)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(337, 469)
        Me.PanelControl2.TabIndex = 5
        '
        'ceFormulario
        '
        Me.ceFormulario.Location = New System.Drawing.Point(13, 428)
        Me.ceFormulario.Name = "ceFormulario"
        Me.ceFormulario.Properties.Caption = "Aplica a Formulario Único?"
        Me.ceFormulario.Size = New System.Drawing.Size(153, 19)
        Me.ceFormulario.TabIndex = 119
        '
        'teImpresor
        '
        Me.teImpresor.EnterMoveNextControl = True
        Me.teImpresor.Location = New System.Drawing.Point(13, 402)
        Me.teImpresor.Name = "teImpresor"
        Me.teImpresor.Size = New System.Drawing.Size(327, 20)
        Me.teImpresor.TabIndex = 118
        '
        'teCta01
        '
        Me.teCta01.Enabled = False
        Me.teCta01.Location = New System.Drawing.Point(13, 328)
        Me.teCta01.Name = "teCta01"
        Me.teCta01.Size = New System.Drawing.Size(327, 20)
        Me.teCta01.TabIndex = 117
        '
        'beCta01
        '
        Me.beCta01.Location = New System.Drawing.Point(13, 302)
        Me.beCta01.Name = "beCta01"
        Me.beCta01.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta01.Size = New System.Drawing.Size(160, 20)
        Me.beCta01.TabIndex = 116
        '
        'seNumLineas
        '
        Me.seNumLineas.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seNumLineas.EnterMoveNextControl = True
        Me.seNumLineas.Location = New System.Drawing.Point(137, 357)
        Me.seNumLineas.Name = "seNumLineas"
        Me.seNumLineas.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seNumLineas.Properties.Mask.EditMask = "n0"
        Me.seNumLineas.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seNumLineas.Size = New System.Drawing.Size(95, 20)
        Me.seNumLineas.TabIndex = 115
        '
        'leModulo
        '
        Me.leModulo.Location = New System.Drawing.Point(13, 257)
        Me.leModulo.Name = "leModulo"
        Me.leModulo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leModulo.Size = New System.Drawing.Size(196, 20)
        Me.leModulo.TabIndex = 49
        '
        'leLibro
        '
        Me.leLibro.Location = New System.Drawing.Point(13, 212)
        Me.leLibro.Name = "leLibro"
        Me.leLibro.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leLibro.Size = New System.Drawing.Size(196, 20)
        Me.leLibro.TabIndex = 48
        '
        'leTipoAplicacion
        '
        Me.leTipoAplicacion.Location = New System.Drawing.Point(13, 167)
        Me.leTipoAplicacion.Name = "leTipoAplicacion"
        Me.leTipoAplicacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoAplicacion.Size = New System.Drawing.Size(196, 20)
        Me.leTipoAplicacion.TabIndex = 47
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(13, 383)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(219, 13)
        Me.LabelControl9.TabIndex = 46
        Me.LabelControl9.Text = "Nombre del Impresor (Solo Doc. Facturación):"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(13, 283)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(153, 13)
        Me.LabelControl8.TabIndex = 45
        Me.LabelControl8.Text = "Cuenta Contable de Inventario:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(13, 360)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(117, 13)
        Me.LabelControl5.TabIndex = 44
        Me.LabelControl5.Text = "Líneas en el Documento:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(13, 13)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl1.TabIndex = 34
        Me.LabelControl1.Text = "IdTipo:"
        '
        'teIdComprobante
        '
        Me.teIdComprobante.EnterMoveNextControl = True
        Me.teIdComprobante.Location = New System.Drawing.Point(13, 32)
        Me.teIdComprobante.Name = "teIdComprobante"
        Me.teIdComprobante.Size = New System.Drawing.Size(95, 20)
        Me.teIdComprobante.TabIndex = 27
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(13, 58)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 35
        Me.LabelControl2.Text = "Nombre:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(13, 77)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(327, 20)
        Me.teNombre.TabIndex = 28
        '
        'ceDetallaIva
        '
        Me.ceDetallaIva.Location = New System.Drawing.Point(127, 123)
        Me.ceDetallaIva.Name = "ceDetallaIva"
        Me.ceDetallaIva.Properties.Caption = "Detalla IVA?"
        Me.ceDetallaIva.Size = New System.Drawing.Size(82, 19)
        Me.ceDetallaIva.TabIndex = 36
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(13, 148)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(74, 13)
        Me.LabelControl3.TabIndex = 37
        Me.LabelControl3.Text = "Tipo Aplicación:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(13, 193)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(93, 13)
        Me.LabelControl4.TabIndex = 38
        Me.LabelControl4.Text = "Mostrarse en Libro:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(13, 238)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(101, 13)
        Me.LabelControl6.TabIndex = 40
        Me.LabelControl6.Text = "Módulo al que Aplica:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(13, 103)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl7.TabIndex = 41
        Me.LabelControl7.Text = "Abreviatura:"
        '
        'teAbreviatura
        '
        Me.teAbreviatura.EnterMoveNextControl = True
        Me.teAbreviatura.Location = New System.Drawing.Point(13, 122)
        Me.teAbreviatura.Name = "teAbreviatura"
        Me.teAbreviatura.Size = New System.Drawing.Size(95, 20)
        Me.teAbreviatura.TabIndex = 29
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.PanelControl2)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1085, 469)
        Me.SplitContainerControl1.SplitterPosition = 743
        Me.SplitContainerControl1.TabIndex = 6
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'adm_frmTiposComprobante
        '
        Me.ClientSize = New System.Drawing.Size(1085, 494)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.DbMode = 1
        Me.Modulo = "Administración"
        Me.Name = "adm_frmTiposComprobante"
        Me.OptionId = "001003"
        Me.Text = "Tipos de Comprobantes"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.SplitContainerControl1, 0)
        CType(Me.riteTipoAplicacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteLibro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkDetalla, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteModulo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteTipoAplicacion1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteLibro1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteModulo1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFormulario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.ceFormulario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teImpresor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seNumLineas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leModulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leLibro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoAplicacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdComprobante.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceDetallaIva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teAbreviatura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdComprobante As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ceDetallaIva As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teAbreviatura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leModulo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leLibro As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leTipoAplicacion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents seNumLineas As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents teCta01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCta01 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents teImpresor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents riteTipoAplicacion As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents riteLibro As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents chkDetalla As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents riteModulo As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents riteTipoAplicacion1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents riteModulo1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents riteLibro1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents ceFormulario As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkFormulario As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit

End Class
