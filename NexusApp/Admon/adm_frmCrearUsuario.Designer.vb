<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adm_frmCrearUsuario
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(adm_frmCrearUsuario))
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.lblMensaje = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.leVendedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.leBodega = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.seDias = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.leTipoPrecio = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.teNombres = New DevExpress.XtraEditors.TextEdit()
        Me.teApellidos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.tePass2 = New DevExpress.XtraEditors.TextEdit()
        Me.tePass = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.teId = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.xtpVarias = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.seHastaDescto = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.seDesdeDescto = New DevExpress.XtraEditors.SpinEdit()
        Me.ceBloqueoBodega = New DevExpress.XtraEditors.CheckEdit()
        Me.chkRevertirInventarios = New DevExpress.XtraEditors.CheckEdit()
        Me.ceAutorizaDescuentos = New DevExpress.XtraEditors.CheckEdit()
        Me.ceTodasSucursales = New DevExpress.XtraEditors.CheckEdit()
        Me.ceCredito = New DevExpress.XtraEditors.CheckEdit()
        Me.ceCambiaDescripciones = New DevExpress.XtraEditors.CheckEdit()
        Me.ceCambiarPrecios = New DevExpress.XtraEditors.CheckEdit()
        Me.xtpAccesoPrecios = New DevExpress.XtraTab.XtraTabPage()
        Me.gcPrecios = New DevExpress.XtraGrid.GridControl()
        Me.gvPrecios = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.IdPrecio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Nombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Acceso = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riceAcceso = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.xtpAccesoSucursales = New DevExpress.XtraTab.XtraTabPage()
        Me.gcSucursales = New DevExpress.XtraGrid.GridControl()
        Me.gvSucursales = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.leSucursalUsuario = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.lePuntoVenta = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.xtpDatos.SuspendLayout()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDias.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPrecio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombres.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teApellidos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePass2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpVarias.SuspendLayout()
        CType(Me.seHastaDescto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDesdeDescto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceBloqueoBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRevertirInventarios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceAutorizaDescuentos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceTodasSucursales.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceCambiaDescripciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceCambiarPrecios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpAccesoPrecios.SuspendLayout()
        CType(Me.gcPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPrecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riceAcceso, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpAccesoSucursales.SuspendLayout()
        CType(Me.gcSucursales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvSucursales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Image = Global.Nexus.My.Resources.Resources.Save
        Me.SimpleButton1.Location = New System.Drawing.Point(506, 329)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(90, 28)
        Me.SimpleButton1.TabIndex = 12
        Me.SimpleButton1.Text = "&Guardar"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Image = Global.Nexus.My.Resources.Resources.Undo32x32
        Me.SimpleButton2.Location = New System.Drawing.Point(602, 329)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(90, 28)
        Me.SimpleButton2.TabIndex = 13
        Me.SimpleButton2.Text = "&Regresar"
        '
        'lblMensaje
        '
        Me.lblMensaje.Appearance.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensaje.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblMensaje.Appearance.Options.UseFont = True
        Me.lblMensaje.Appearance.Options.UseForeColor = True
        Me.lblMensaje.Location = New System.Drawing.Point(186, 302)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(162, 23)
        Me.lblMensaje.TabIndex = 32
        Me.lblMensaje.Text = "Clave Vulnerable"
        Me.lblMensaje.Visible = False
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.xtpDatos
        Me.XtraTabControl1.Size = New System.Drawing.Size(714, 302)
        Me.XtraTabControl1.TabIndex = 121
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpDatos, Me.xtpVarias, Me.xtpAccesoPrecios, Me.xtpAccesoSucursales})
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.LabelControl11)
        Me.xtpDatos.Controls.Add(Me.lePuntoVenta)
        Me.xtpDatos.Controls.Add(Me.LabelControl16)
        Me.xtpDatos.Controls.Add(Me.LabelControl21)
        Me.xtpDatos.Controls.Add(Me.leVendedor)
        Me.xtpDatos.Controls.Add(Me.LabelControl20)
        Me.xtpDatos.Controls.Add(Me.leBodega)
        Me.xtpDatos.Controls.Add(Me.LabelControl9)
        Me.xtpDatos.Controls.Add(Me.LabelControl15)
        Me.xtpDatos.Controls.Add(Me.LabelControl13)
        Me.xtpDatos.Controls.Add(Me.LabelControl12)
        Me.xtpDatos.Controls.Add(Me.LabelControl10)
        Me.xtpDatos.Controls.Add(Me.LabelControl14)
        Me.xtpDatos.Controls.Add(Me.seDias)
        Me.xtpDatos.Controls.Add(Me.LabelControl8)
        Me.xtpDatos.Controls.Add(Me.deFecha)
        Me.xtpDatos.Controls.Add(Me.leTipoPrecio)
        Me.xtpDatos.Controls.Add(Me.LabelControl6)
        Me.xtpDatos.Controls.Add(Me.LabelControl5)
        Me.xtpDatos.Controls.Add(Me.leSucursal)
        Me.xtpDatos.Controls.Add(Me.teNombres)
        Me.xtpDatos.Controls.Add(Me.teApellidos)
        Me.xtpDatos.Controls.Add(Me.LabelControl4)
        Me.xtpDatos.Controls.Add(Me.LabelControl3)
        Me.xtpDatos.Controls.Add(Me.tePass2)
        Me.xtpDatos.Controls.Add(Me.tePass)
        Me.xtpDatos.Controls.Add(Me.LabelControl7)
        Me.xtpDatos.Controls.Add(Me.teId)
        Me.xtpDatos.Controls.Add(Me.LabelControl2)
        Me.xtpDatos.Controls.Add(Me.LabelControl1)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(708, 274)
        Me.xtpDatos.Text = "Datos Generales"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl16.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Appearance.Options.UseForeColor = True
        Me.LabelControl16.Location = New System.Drawing.Point(402, 7)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(164, 11)
        Me.LabelControl16.TabIndex = 176
        Me.LabelControl16.Text = "* POLITICAS DE CONTRASE�AS"
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(81, 120)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(97, 13)
        Me.LabelControl21.TabIndex = 175
        Me.LabelControl21.Text = "Vendedor Asignado:"
        '
        'leVendedor
        '
        Me.leVendedor.EnterMoveNextControl = True
        Me.leVendedor.Location = New System.Drawing.Point(185, 116)
        Me.leVendedor.Name = "leVendedor"
        Me.leVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leVendedor.Size = New System.Drawing.Size(196, 20)
        Me.leVendedor.TabIndex = 174
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(50, 187)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(133, 13)
        Me.LabelControl20.TabIndex = 173
        Me.LabelControl20.Text = "Bodega a la que pertenece:"
        '
        'leBodega
        '
        Me.leBodega.EnterMoveNextControl = True
        Me.leBodega.Location = New System.Drawing.Point(185, 183)
        Me.leBodega.Name = "leBodega"
        Me.leBodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leBodega.Size = New System.Drawing.Size(196, 20)
        Me.leBodega.TabIndex = 160
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl9.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Appearance.Options.UseForeColor = True
        Me.LabelControl9.Location = New System.Drawing.Point(402, 100)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(237, 11)
        Me.LabelControl9.TabIndex = 172
        Me.LabelControl9.Text = "5- La contrase�a debe ser diferente a las �ltimas 6 usadas"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl15.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Appearance.Options.UseForeColor = True
        Me.LabelControl15.Location = New System.Drawing.Point(402, 81)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(260, 11)
        Me.LabelControl15.TabIndex = 171
        Me.LabelControl15.Text = "4- La contrase�a debe de tener un caracter especial (#$%&+{? )"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl13.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Appearance.Options.UseForeColor = True
        Me.LabelControl13.Location = New System.Drawing.Point(402, 62)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(174, 11)
        Me.LabelControl13.TabIndex = 170
        Me.LabelControl13.Text = "3- La contrase�a debe de tener un n�mero"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl12.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Appearance.Options.UseForeColor = True
        Me.LabelControl12.Location = New System.Drawing.Point(402, 43)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(213, 11)
        Me.LabelControl12.TabIndex = 169
        Me.LabelControl12.Text = "2- La contrase�a debe de tener una letra May�scula"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl10.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Appearance.Options.UseForeColor = True
        Me.LabelControl10.Location = New System.Drawing.Point(402, 24)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(257, 11)
        Me.LabelControl10.TabIndex = 168
        Me.LabelControl10.Text = "1- La longitud menor de la contrase�a debe ser de 6 caracteres"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(40, 251)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(143, 13)
        Me.LabelControl14.TabIndex = 167
        Me.LabelControl14.Text = "Per�odo de Cambios de Clave:"
        '
        'seDias
        '
        Me.seDias.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDias.EnterMoveNextControl = True
        Me.seDias.Location = New System.Drawing.Point(185, 247)
        Me.seDias.Name = "seDias"
        Me.seDias.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDias.Properties.Mask.EditMask = "n0"
        Me.seDias.Size = New System.Drawing.Size(112, 20)
        Me.seDias.TabIndex = 163
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(66, 228)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(117, 13)
        Me.LabelControl8.TabIndex = 166
        Me.LabelControl8.Text = "Vencimiento del Usuario:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(185, 225)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Size = New System.Drawing.Size(112, 20)
        Me.deFecha.TabIndex = 162
        '
        'leTipoPrecio
        '
        Me.leTipoPrecio.EnterMoveNextControl = True
        Me.leTipoPrecio.Location = New System.Drawing.Point(185, 204)
        Me.leTipoPrecio.Name = "leTipoPrecio"
        Me.leTipoPrecio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPrecio.Size = New System.Drawing.Size(196, 20)
        Me.leTipoPrecio.TabIndex = 161
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(2, 207)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(181, 13)
        Me.LabelControl6.TabIndex = 165
        Me.LabelControl6.Text = "Precio predeterminado para Facturar:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(46, 143)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(137, 13)
        Me.LabelControl5.TabIndex = 164
        Me.LabelControl5.Text = "Sucursal a la que pertenece:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(185, 137)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(196, 20)
        Me.leSucursal.TabIndex = 159
        '
        'teNombres
        '
        Me.teNombres.EnterMoveNextControl = True
        Me.teNombres.Location = New System.Drawing.Point(185, 73)
        Me.teNombres.Name = "teNombres"
        Me.teNombres.Size = New System.Drawing.Size(196, 20)
        Me.teNombres.TabIndex = 155
        '
        'teApellidos
        '
        Me.teApellidos.EnterMoveNextControl = True
        Me.teApellidos.Location = New System.Drawing.Point(185, 94)
        Me.teApellidos.Name = "teApellidos"
        Me.teApellidos.Size = New System.Drawing.Size(196, 20)
        Me.teApellidos.TabIndex = 158
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(81, 98)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl4.TabIndex = 157
        Me.LabelControl4.Text = "Apellidos del Usuario:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(86, 76)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(97, 13)
        Me.LabelControl3.TabIndex = 156
        Me.LabelControl3.Text = "Nombre del Usuario:"
        '
        'tePass2
        '
        Me.tePass2.EditValue = ""
        Me.tePass2.EnterMoveNextControl = True
        Me.tePass2.Location = New System.Drawing.Point(185, 52)
        Me.tePass2.Name = "tePass2"
        Me.tePass2.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tePass2.Size = New System.Drawing.Size(196, 20)
        Me.tePass2.TabIndex = 154
        '
        'tePass
        '
        Me.tePass.EditValue = ""
        Me.tePass.EnterMoveNextControl = True
        Me.tePass.Location = New System.Drawing.Point(185, 31)
        Me.tePass.Name = "tePass"
        Me.tePass.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tePass.Size = New System.Drawing.Size(196, 20)
        Me.tePass.TabIndex = 152
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(85, 55)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(98, 13)
        Me.LabelControl7.TabIndex = 151
        Me.LabelControl7.Text = "Repetir Contrase�a:"
        '
        'teId
        '
        Me.teId.EnterMoveNextControl = True
        Me.teId.Location = New System.Drawing.Point(185, 10)
        Me.teId.Name = "teId"
        Me.teId.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teId.Size = New System.Drawing.Size(196, 20)
        Me.teId.TabIndex = 150
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(123, 34)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl2.TabIndex = 153
        Me.LabelControl2.Text = "Contrase�a:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(115, 13)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(68, 13)
        Me.LabelControl1.TabIndex = 149
        Me.LabelControl1.Text = "Identificaci�n:"
        '
        'xtpVarias
        '
        Me.xtpVarias.Controls.Add(Me.LabelControl19)
        Me.xtpVarias.Controls.Add(Me.seHastaDescto)
        Me.xtpVarias.Controls.Add(Me.LabelControl26)
        Me.xtpVarias.Controls.Add(Me.seDesdeDescto)
        Me.xtpVarias.Controls.Add(Me.ceBloqueoBodega)
        Me.xtpVarias.Controls.Add(Me.chkRevertirInventarios)
        Me.xtpVarias.Controls.Add(Me.ceAutorizaDescuentos)
        Me.xtpVarias.Controls.Add(Me.ceTodasSucursales)
        Me.xtpVarias.Controls.Add(Me.ceCredito)
        Me.xtpVarias.Controls.Add(Me.ceCambiaDescripciones)
        Me.xtpVarias.Controls.Add(Me.ceCambiarPrecios)
        Me.xtpVarias.Name = "xtpVarias"
        Me.xtpVarias.Size = New System.Drawing.Size(698, 246)
        Me.xtpVarias.Text = "Permisos Varios"
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(421, 39)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl19.TabIndex = 128
        Me.LabelControl19.Text = "Hasta % Descto:"
        '
        'seHastaDescto
        '
        Me.seHastaDescto.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seHastaDescto.EnterMoveNextControl = True
        Me.seHastaDescto.Location = New System.Drawing.Point(509, 35)
        Me.seHastaDescto.Name = "seHastaDescto"
        Me.seHastaDescto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seHastaDescto.Properties.Mask.EditMask = "P2"
        Me.seHastaDescto.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seHastaDescto.Size = New System.Drawing.Size(68, 20)
        Me.seHastaDescto.TabIndex = 127
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(421, 18)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl26.TabIndex = 126
        Me.LabelControl26.Text = "Desde % Descto:"
        '
        'seDesdeDescto
        '
        Me.seDesdeDescto.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDesdeDescto.EnterMoveNextControl = True
        Me.seDesdeDescto.Location = New System.Drawing.Point(509, 14)
        Me.seDesdeDescto.Name = "seDesdeDescto"
        Me.seDesdeDescto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDesdeDescto.Properties.Mask.EditMask = "P2"
        Me.seDesdeDescto.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seDesdeDescto.Size = New System.Drawing.Size(68, 20)
        Me.seDesdeDescto.TabIndex = 125
        '
        'ceBloqueoBodega
        '
        Me.ceBloqueoBodega.Location = New System.Drawing.Point(35, 124)
        Me.ceBloqueoBodega.Name = "ceBloqueoBodega"
        Me.ceBloqueoBodega.Properties.Caption = "Bloquear bodega al Facturar"
        Me.ceBloqueoBodega.Size = New System.Drawing.Size(221, 19)
        Me.ceBloqueoBodega.TabIndex = 124
        '
        'chkRevertirInventarios
        '
        Me.chkRevertirInventarios.Location = New System.Drawing.Point(35, 106)
        Me.chkRevertirInventarios.Name = "chkRevertirInventarios"
        Me.chkRevertirInventarios.Properties.Caption = "Permitir Revertir Inventarios en Negativo"
        Me.chkRevertirInventarios.Size = New System.Drawing.Size(221, 19)
        Me.chkRevertirInventarios.TabIndex = 123
        '
        'ceAutorizaDescuentos
        '
        Me.ceAutorizaDescuentos.Location = New System.Drawing.Point(35, 87)
        Me.ceAutorizaDescuentos.Name = "ceAutorizaDescuentos"
        Me.ceAutorizaDescuentos.Properties.Caption = "Podr� Autorizar Descuentos y rebajas"
        Me.ceAutorizaDescuentos.Size = New System.Drawing.Size(236, 19)
        Me.ceAutorizaDescuentos.TabIndex = 122
        '
        'ceTodasSucursales
        '
        Me.ceTodasSucursales.Location = New System.Drawing.Point(35, 68)
        Me.ceTodasSucursales.Name = "ceTodasSucursales"
        Me.ceTodasSucursales.Properties.Caption = "Podr� Visualizar Todas las Sucursales"
        Me.ceTodasSucursales.Size = New System.Drawing.Size(225, 19)
        Me.ceTodasSucursales.TabIndex = 121
        '
        'ceCredito
        '
        Me.ceCredito.Location = New System.Drawing.Point(35, 50)
        Me.ceCredito.Name = "ceCredito"
        Me.ceCredito.Properties.Caption = "El usuario podr� autorizar Cr�ditos"
        Me.ceCredito.Size = New System.Drawing.Size(236, 19)
        Me.ceCredito.TabIndex = 119
        '
        'ceCambiaDescripciones
        '
        Me.ceCambiaDescripciones.Location = New System.Drawing.Point(35, 32)
        Me.ceCambiaDescripciones.Name = "ceCambiaDescripciones"
        Me.ceCambiaDescripciones.Properties.Caption = "Permitir Cambiar Descripciones de Productos"
        Me.ceCambiaDescripciones.Size = New System.Drawing.Size(239, 19)
        Me.ceCambiaDescripciones.TabIndex = 118
        '
        'ceCambiarPrecios
        '
        Me.ceCambiarPrecios.Location = New System.Drawing.Point(35, 14)
        Me.ceCambiarPrecios.Name = "ceCambiarPrecios"
        Me.ceCambiarPrecios.Properties.Caption = "Podr� Cambiar Precios de Venta al facturar"
        Me.ceCambiarPrecios.Size = New System.Drawing.Size(236, 19)
        Me.ceCambiarPrecios.TabIndex = 117
        '
        'xtpAccesoPrecios
        '
        Me.xtpAccesoPrecios.Controls.Add(Me.gcPrecios)
        Me.xtpAccesoPrecios.Name = "xtpAccesoPrecios"
        Me.xtpAccesoPrecios.Size = New System.Drawing.Size(698, 246)
        Me.xtpAccesoPrecios.Text = "Acceso a Precios de Venta"
        '
        'gcPrecios
        '
        Me.gcPrecios.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcPrecios.Location = New System.Drawing.Point(0, 0)
        Me.gcPrecios.MainView = Me.gvPrecios
        Me.gcPrecios.Name = "gcPrecios"
        Me.gcPrecios.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riceAcceso})
        Me.gcPrecios.Size = New System.Drawing.Size(698, 246)
        Me.gcPrecios.TabIndex = 104
        Me.gcPrecios.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvPrecios})
        '
        'gvPrecios
        '
        Me.gvPrecios.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.IdPrecio, Me.Nombre, Me.Acceso})
        Me.gvPrecios.GridControl = Me.gcPrecios
        Me.gvPrecios.Name = "gvPrecios"
        Me.gvPrecios.OptionsView.ShowGroupPanel = False
        '
        'IdPrecio
        '
        Me.IdPrecio.Caption = "Precio"
        Me.IdPrecio.FieldName = "IdPrecio"
        Me.IdPrecio.Name = "IdPrecio"
        Me.IdPrecio.OptionsColumn.AllowEdit = False
        Me.IdPrecio.Visible = True
        Me.IdPrecio.VisibleIndex = 0
        Me.IdPrecio.Width = 45
        '
        'Nombre
        '
        Me.Nombre.Caption = "Nombre"
        Me.Nombre.FieldName = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.OptionsColumn.AllowEdit = False
        Me.Nombre.Visible = True
        Me.Nombre.VisibleIndex = 1
        Me.Nombre.Width = 181
        '
        'Acceso
        '
        Me.Acceso.Caption = "Acceso"
        Me.Acceso.ColumnEdit = Me.riceAcceso
        Me.Acceso.FieldName = "Acceso"
        Me.Acceso.Name = "Acceso"
        Me.Acceso.Visible = True
        Me.Acceso.VisibleIndex = 2
        Me.Acceso.Width = 49
        '
        'riceAcceso
        '
        Me.riceAcceso.AutoHeight = False
        Me.riceAcceso.Name = "riceAcceso"
        '
        'xtpAccesoSucursales
        '
        Me.xtpAccesoSucursales.Controls.Add(Me.gcSucursales)
        Me.xtpAccesoSucursales.Name = "xtpAccesoSucursales"
        Me.xtpAccesoSucursales.Size = New System.Drawing.Size(698, 246)
        Me.xtpAccesoSucursales.Text = "Acceso a Sucursales"
        '
        'gcSucursales
        '
        Me.gcSucursales.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcSucursales.Location = New System.Drawing.Point(0, 0)
        Me.gcSucursales.MainView = Me.gvSucursales
        Me.gcSucursales.Name = "gcSucursales"
        Me.gcSucursales.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1, Me.leSucursalUsuario})
        Me.gcSucursales.Size = New System.Drawing.Size(698, 246)
        Me.gcSucursales.TabIndex = 107
        Me.gcSucursales.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvSucursales})
        '
        'gvSucursales
        '
        Me.gvSucursales.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.gvSucursales.GridControl = Me.gcSucursales
        Me.gvSucursales.Name = "gvSucursales"
        Me.gvSucursales.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdSucursal"
        Me.GridColumn1.FieldName = "IdSucursal"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 45
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Sucursal"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 181
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Acceso"
        Me.GridColumn3.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.GridColumn3.FieldName = "Acceso"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 49
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'leSucursalUsuario
        '
        Me.leSucursalUsuario.AutoHeight = False
        Me.leSucursalUsuario.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalUsuario.Name = "leSucursalUsuario"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(102, 165)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl11.TabIndex = 178
        Me.LabelControl11.Text = "Punto de Venta:"
        '
        'lePuntoVenta
        '
        Me.lePuntoVenta.EnterMoveNextControl = True
        Me.lePuntoVenta.Location = New System.Drawing.Point(185, 161)
        Me.lePuntoVenta.Name = "lePuntoVenta"
        Me.lePuntoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePuntoVenta.Size = New System.Drawing.Size(196, 20)
        Me.lePuntoVenta.TabIndex = 177
        '
        'adm_frmCrearUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(714, 360)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.lblMensaje)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "adm_frmCrearUsuario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Creaci�n de usuarios"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.xtpDatos.ResumeLayout(False)
        Me.xtpDatos.PerformLayout()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDias.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPrecio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombres.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teApellidos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePass2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpVarias.ResumeLayout(False)
        Me.xtpVarias.PerformLayout()
        CType(Me.seHastaDescto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDesdeDescto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceBloqueoBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRevertirInventarios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceAutorizaDescuentos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceTodasSucursales.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceCambiaDescripciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceCambiarPrecios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpAccesoPrecios.ResumeLayout(False)
        CType(Me.gcPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPrecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riceAcceso, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpAccesoSucursales.ResumeLayout(False)
        CType(Me.gcSucursales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvSucursales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblMensaje As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leVendedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leBodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seDias As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents leTipoPrecio As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teNombres As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teApellidos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tePass2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tePass As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teId As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents xtpVarias As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seHastaDescto As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seDesdeDescto As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents ceBloqueoBodega As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkRevertirInventarios As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceAutorizaDescuentos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceTodasSucursales As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceCredito As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceCambiaDescripciones As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceCambiarPrecios As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents xtpAccesoPrecios As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gcPrecios As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvPrecios As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents IdPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Nombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Acceso As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riceAcceso As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents xtpAccesoSucursales As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gcSucursales As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvSucursales As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents leSucursalUsuario As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lePuntoVenta As DevExpress.XtraEditors.LookUpEdit
End Class
