﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adm_frmProfesiones
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(adm_frmProfesiones))
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcAsegurado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkAsegurado = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.gcPeso = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.tePeso = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAsegurado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePeso, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkAsegurado, Me.tePeso})
        Me.gc.Size = New System.Drawing.Size(684, 335)
        Me.gc.TabIndex = 4
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcId, Me.gcNombre, Me.gcAsegurado, Me.gcPeso})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.NewItemRowText = "Click aquí para ingresar nueva forma de pago. Esc para cancelar"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcId
        '
        Me.gcId.Caption = "Id. Profesión"
        Me.gcId.FieldName = "IdProfesion"
        Me.gcId.Name = "gcId"
        Me.gcId.Visible = True
        Me.gcId.VisibleIndex = 0
        Me.gcId.Width = 119
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 1
        Me.gcNombre.Width = 417
        '
        'gcAsegurado
        '
        Me.gcAsegurado.Caption = "Asegurado"
        Me.gcAsegurado.ColumnEdit = Me.chkAsegurado
        Me.gcAsegurado.FieldName = "Asegurado"
        Me.gcAsegurado.Name = "gcAsegurado"
        '
        'chkAsegurado
        '
        Me.chkAsegurado.AutoHeight = False
        Me.chkAsegurado.Name = "chkAsegurado"
        '
        'gcPeso
        '
        Me.gcPeso.Caption = "Peso"
        Me.gcPeso.ColumnEdit = Me.tePeso
        Me.gcPeso.FieldName = "Peso"
        Me.gcPeso.Name = "gcPeso"
        '
        'tePeso
        '
        Me.tePeso.AutoHeight = False
        Me.tePeso.Mask.EditMask = "n2"
        Me.tePeso.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tePeso.Name = "tePeso"
        '
        'adm_frmProfesiones
        '
        Me.ClientSize = New System.Drawing.Size(882, 360)
        Me.Controls.Add(Me.gc)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Administración"
        Me.Name = "adm_frmProfesiones"
        Me.OptionId = "002002"
        Me.Text = "Profesiones"
        Me.TipoFormulario = 2
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAsegurado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePeso, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcAsegurado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkAsegurado As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents gcPeso As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tePeso As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit

End Class
