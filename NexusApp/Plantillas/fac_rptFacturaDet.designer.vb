﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class fac_rptFacturaDet
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.Factura2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.LecturaActual2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ConsumoM32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.FechaCargo2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.LecturaAnterior2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.Dpi = 100.0!
        Me.TopMarginBand1.HeightF = 0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'Factura2
        '
        Me.Factura2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.VentaAfecta", "{0:n2}")})
        Me.Factura2.Dpi = 100.0!
        Me.Factura2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.75!)
        Me.Factura2.LocationFloat = New DevExpress.Utils.PointFloat(706.2912!, 0!)
        Me.Factura2.Name = "Factura2"
        Me.Factura2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.Factura2.SizeF = New System.Drawing.SizeF(52.74994!, 14.37482!)
        Me.Factura2.StylePriority.UseFont = False
        Me.Factura2.StylePriority.UseTextAlignment = False
        Me.Factura2.Text = "Factura2"
        Me.Factura2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'LecturaActual2
        '
        Me.LecturaActual2.CanGrow = False
        Me.LecturaActual2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.Descripcion")})
        Me.LecturaActual2.Dpi = 100.0!
        Me.LecturaActual2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.75!)
        Me.LecturaActual2.LocationFloat = New DevExpress.Utils.PointFloat(446.75!, 0!)
        Me.LecturaActual2.Name = "LecturaActual2"
        Me.LecturaActual2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LecturaActual2.SizeF = New System.Drawing.SizeF(156.2499!, 14.37481!)
        Me.LecturaActual2.StylePriority.UseFont = False
        Me.LecturaActual2.StylePriority.UseTextAlignment = False
        Me.LecturaActual2.Text = "LecturaActual2"
        Me.LecturaActual2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.LecturaActual2.WordWrap = False
        '
        'ConsumoM32
        '
        Me.ConsumoM32.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.PrecioUnitario", "{0:n2}")})
        Me.ConsumoM32.Dpi = 100.0!
        Me.ConsumoM32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.75!)
        Me.ConsumoM32.LocationFloat = New DevExpress.Utils.PointFloat(601.2912!, 0!)
        Me.ConsumoM32.Name = "ConsumoM32"
        Me.ConsumoM32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.ConsumoM32.SizeF = New System.Drawing.SizeF(41.41669!, 14.37482!)
        Me.ConsumoM32.StylePriority.UseFont = False
        Me.ConsumoM32.StylePriority.UseTextAlignment = False
        Me.ConsumoM32.Text = "ConsumoM32"
        Me.ConsumoM32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'FechaCargo2
        '
        Me.FechaCargo2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.IdProducto")})
        Me.FechaCargo2.Dpi = 100.0!
        Me.FechaCargo2.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaCargo2.LocationFloat = New DevExpress.Utils.PointFloat(299.8333!, 0!)
        Me.FechaCargo2.Name = "FechaCargo2"
        Me.FechaCargo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.FechaCargo2.SizeF = New System.Drawing.SizeF(91.16668!, 14.37481!)
        Me.FechaCargo2.StylePriority.UseFont = False
        Me.FechaCargo2.Text = "FechaCargo2"
        Me.FechaCargo2.Visible = False
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.Dpi = 100.0!
        Me.BottomMarginBand1.HeightF = 1.334031!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        Me.BottomMarginBand1.SnapLinePadding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        '
        'LecturaAnterior2
        '
        Me.LecturaAnterior2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.Cantidad", "{0:n2}")})
        Me.LecturaAnterior2.Dpi = 100.0!
        Me.LecturaAnterior2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.75!)
        Me.LecturaAnterior2.LocationFloat = New DevExpress.Utils.PointFloat(391.0!, 0!)
        Me.LecturaAnterior2.Name = "LecturaAnterior2"
        Me.LecturaAnterior2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LecturaAnterior2.SizeF = New System.Drawing.SizeF(44.04132!, 14.37481!)
        Me.LecturaAnterior2.StylePriority.UseFont = False
        Me.LecturaAnterior2.StylePriority.UseTextAlignment = False
        Me.LecturaAnterior2.Text = "LecturaAnterior2"
        Me.LecturaAnterior2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.Factura2, Me.LecturaAnterior2, Me.ConsumoM32, Me.LecturaActual2, Me.FechaCargo2, Me.XrLabel1})
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 17.45817!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel4
        '
        Me.XrLabel4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "VentasDetalle.VentaExenta", "{0:n2}")})
        Me.XrLabel4.Dpi = 100.0!
        Me.XrLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.75!)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(647.875!, 0!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(30.74994!, 14.37482!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "XrLabel4"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel1
        '
        Me.XrLabel1.CanGrow = False
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.UnidadMedida")})
        Me.XrLabel1.Dpi = 100.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(211.8333!, 0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(55.16666!, 14.37481!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.Text = "XrLabel1"
        Me.XrLabel1.Visible = False
        '
        'fac_rptFacturaDet
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.DataMember = "Factura"
        Me.DesignerOptions.ShowExportWarnings = False
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(15, 39, 0, 1)
        Me.PageHeight = 850
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PaperName = "CCF"
        Me.PrinterName = "EPSON FX-890 Ver 2.0"
        Me.SnapGridSize = 2.083333!
        Me.Version = "16.2"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents Factura2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents LecturaActual2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ConsumoM32 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents FechaCargo2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents LecturaAnterior2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
End Class
