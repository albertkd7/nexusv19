﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class fac_rptCCFGS1
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.xrlIva1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlIvaRetenido = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFecha1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDocAfectados = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaNoSujeta = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDirec1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDiasCredito = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDepto = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlComentario = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTelefono = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaACuenta = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.xrlCantLetras1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.xrlNumero = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlMunic = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSubTotal1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotalAfecto1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVendedor = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCesc1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlGiro1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.xrlClient2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDoc2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFec2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel38 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel40 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel41 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel45 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel46 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel49 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel52 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSub2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlImp2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTot2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlArt2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlArt1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTot1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlImp1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSub1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFec1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDoc1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlClient1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel47 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel48 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel56 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel57 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel58 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCantLetras3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel61 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCesc3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel65 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlIva3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlIvaRetenido3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlComent3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel69 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel70 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotalAfecto3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSubTotal3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotalExento3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel74 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel75 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel76 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel77 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel78 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFormaPago3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlGiro3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel81 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel82 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNit3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDirec3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFecha3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNrc3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNombre3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel88 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNombre2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNrc2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFecha2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDirec2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNit2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlGiro2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFormaPago2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotalExento2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSubTotal2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotalAfecto2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlIvaRetenido2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlIva2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel37 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCesc2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel39 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCantLetras2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel42 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel43 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel44 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel50 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel51 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel62 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel63 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotalExento1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPedido = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFormulario = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCodigo = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFormaPago1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNit1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNrc1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNombre1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.DsFactura1 = New Nexus.dsFactura()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel67 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel68 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel89 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel90 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel101 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel102 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio35 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal35 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant35 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta35 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc35 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc36 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta36 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant36 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal36 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio36 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel113 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel114 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPrecio37 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal37 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCant37 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta37 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDesc37 = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me.DsFactura1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xrlIva1
        '
        Me.xrlIva1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIva1.Dpi = 100.0!
        Me.xrlIva1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlIva1.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 159.0!)
        Me.xrlIva1.Name = "xrlIva1"
        Me.xrlIva1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIva1.SizeF = New System.Drawing.SizeF(75.0!, 10.75003!)
        Me.xrlIva1.StylePriority.UseBorders = False
        Me.xrlIva1.StylePriority.UseFont = False
        Me.xrlIva1.StylePriority.UseTextAlignment = False
        Me.xrlIva1.Text = "Iva"
        Me.xrlIva1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlIvaRetenido
        '
        Me.xrlIvaRetenido.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIvaRetenido.Dpi = 100.0!
        Me.xrlIvaRetenido.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlIvaRetenido.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 178.25!)
        Me.xrlIvaRetenido.Name = "xrlIvaRetenido"
        Me.xrlIvaRetenido.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIvaRetenido.SizeF = New System.Drawing.SizeF(75.0!, 10.75003!)
        Me.xrlIvaRetenido.StylePriority.UseBorders = False
        Me.xrlIvaRetenido.StylePriority.UseFont = False
        Me.xrlIvaRetenido.StylePriority.UseTextAlignment = False
        Me.xrlIvaRetenido.Text = "0.00"
        Me.xrlIvaRetenido.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlFecha1
        '
        Me.xrlFecha1.Dpi = 100.0!
        Me.xrlFecha1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlFecha1.LocationFloat = New DevExpress.Utils.PointFloat(554.9999!, 32.99997!)
        Me.xrlFecha1.Name = "xrlFecha1"
        Me.xrlFecha1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha1.SizeF = New System.Drawing.SizeF(75.0!, 9.0!)
        Me.xrlFecha1.StylePriority.UseFont = False
        Me.xrlFecha1.StylePriority.UseTextAlignment = False
        Me.xrlFecha1.Text = "Fecha1"
        Me.xrlFecha1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDocAfectados
        '
        Me.xrlDocAfectados.CanGrow = False
        Me.xrlDocAfectados.Dpi = 100.0!
        Me.xrlDocAfectados.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlDocAfectados.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 51.0!)
        Me.xrlDocAfectados.Name = "xrlDocAfectados"
        Me.xrlDocAfectados.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDocAfectados.SizeF = New System.Drawing.SizeF(57.0!, 9.0!)
        Me.xrlDocAfectados.StylePriority.UseFont = False
        Me.xrlDocAfectados.StylePriority.UseTextAlignment = False
        Me.xrlDocAfectados.Text = "DIRECCIÓN:"
        Me.xrlDocAfectados.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlPrecio11
        '
        Me.xrlPrecio11.CanGrow = False
        Me.xrlPrecio11.Dpi = 100.0!
        Me.xrlPrecio11.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio11.LocationFloat = New DevExpress.Utils.PointFloat(543.0001!, 83.99999!)
        Me.xrlPrecio11.Name = "xrlPrecio11"
        Me.xrlPrecio11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio11.SizeF = New System.Drawing.SizeF(60.0!, 9.000023!)
        Me.xrlPrecio11.StylePriority.UseFont = False
        Me.xrlPrecio11.StylePriority.UseTextAlignment = False
        Me.xrlPrecio11.Text = "Precio11"
        Me.xrlPrecio11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.xrlPrecio11.WordWrap = False
        '
        'xrlVentaNoSujeta
        '
        Me.xrlVentaNoSujeta.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlVentaNoSujeta.Dpi = 100.0!
        Me.xrlVentaNoSujeta.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaNoSujeta.LocationFloat = New DevExpress.Utils.PointFloat(609.0!, 83.99999!)
        Me.xrlVentaNoSujeta.Name = "xrlVentaNoSujeta"
        Me.xrlVentaNoSujeta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaNoSujeta.SizeF = New System.Drawing.SizeF(39.87488!, 9.000023!)
        Me.xrlVentaNoSujeta.StylePriority.UseBorders = False
        Me.xrlVentaNoSujeta.StylePriority.UseFont = False
        Me.xrlVentaNoSujeta.StylePriority.UseTextAlignment = False
        Me.xrlVentaNoSujeta.Text = "0.00"
        Me.xrlVentaNoSujeta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.xrlVentaNoSujeta.Visible = False
        '
        'xrlDirec1
        '
        Me.xrlDirec1.CanGrow = False
        Me.xrlDirec1.Dpi = 100.0!
        Me.xrlDirec1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlDirec1.LocationFloat = New DevExpress.Utils.PointFloat(95.99999!, 51.00001!)
        Me.xrlDirec1.Name = "xrlDirec1"
        Me.xrlDirec1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDirec1.SizeF = New System.Drawing.SizeF(369.0!, 8.999989!)
        Me.xrlDirec1.StylePriority.UseFont = False
        Me.xrlDirec1.StylePriority.UseTextAlignment = False
        Me.xrlDirec1.Text = "Direccion1"
        Me.xrlDirec1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDiasCredito
        '
        Me.xrlDiasCredito.CanGrow = False
        Me.xrlDiasCredito.Dpi = 100.0!
        Me.xrlDiasCredito.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlDiasCredito.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 189.0!)
        Me.xrlDiasCredito.Name = "xrlDiasCredito"
        Me.xrlDiasCredito.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDiasCredito.SizeF = New System.Drawing.SizeF(138.0001!, 10.75003!)
        Me.xrlDiasCredito.StylePriority.UseFont = False
        Me.xrlDiasCredito.StylePriority.UseTextAlignment = False
        Me.xrlDiasCredito.Text = "VENDEDOR"
        Me.xrlDiasCredito.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlDiasCredito.Visible = False
        '
        'xrlCant11
        '
        Me.xrlCant11.Dpi = 100.0!
        Me.xrlCant11.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant11.LocationFloat = New DevExpress.Utils.PointFloat(3.000176!, 83.99999!)
        Me.xrlCant11.Name = "xrlCant11"
        Me.xrlCant11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant11.SizeF = New System.Drawing.SizeF(41.99998!, 9.000023!)
        Me.xrlCant11.StylePriority.UseFont = False
        Me.xrlCant11.StylePriority.UseTextAlignment = False
        Me.xrlCant11.Text = "Cant11"
        Me.xrlCant11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDepto
        '
        Me.xrlDepto.CanGrow = False
        Me.xrlDepto.Dpi = 100.0!
        Me.xrlDepto.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlDepto.LocationFloat = New DevExpress.Utils.PointFloat(537.0!, 5.999995!)
        Me.xrlDepto.Name = "xrlDepto"
        Me.xrlDepto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDepto.SizeF = New System.Drawing.SizeF(135.0!, 13.37481!)
        Me.xrlDepto.StylePriority.UseFont = False
        Me.xrlDepto.StylePriority.UseTextAlignment = False
        Me.xrlDepto.Text = "SAN SALVADOR"
        Me.xrlDepto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlDepto.Visible = False
        Me.xrlDepto.WordWrap = False
        '
        'xrlComentario
        '
        Me.xrlComentario.CanShrink = True
        Me.xrlComentario.Dpi = 100.0!
        Me.xrlComentario.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlComentario.LocationFloat = New DevExpress.Utils.PointFloat(44.08043!, 178.2501!)
        Me.xrlComentario.Name = "xrlComentario"
        Me.xrlComentario.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlComentario.SizeF = New System.Drawing.SizeF(222.2495!, 10.75003!)
        Me.xrlComentario.StylePriority.UseFont = False
        Me.xrlComentario.StylePriority.UseTextAlignment = False
        Me.xrlComentario.Text = "Comentario"
        Me.xrlComentario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlComentario.Visible = False
        '
        'xrlTelefono
        '
        Me.xrlTelefono.Dpi = 100.0!
        Me.xrlTelefono.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlTelefono.LocationFloat = New DevExpress.Utils.PointFloat(471.0!, 41.99995!)
        Me.xrlTelefono.Name = "xrlTelefono"
        Me.xrlTelefono.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTelefono.SizeF = New System.Drawing.SizeF(81.0!, 9.0!)
        Me.xrlTelefono.StylePriority.UseFont = False
        Me.xrlTelefono.StylePriority.UseTextAlignment = False
        Me.xrlTelefono.Text = "NIT:"
        Me.xrlTelefono.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaACuenta
        '
        Me.xrlVentaACuenta.CanGrow = False
        Me.xrlVentaACuenta.Dpi = 100.0!
        Me.xrlVentaACuenta.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlVentaACuenta.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 60.0!)
        Me.xrlVentaACuenta.Name = "xrlVentaACuenta"
        Me.xrlVentaACuenta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaACuenta.SizeF = New System.Drawing.SizeF(57.0!, 8.999996!)
        Me.xrlVentaACuenta.StylePriority.UseFont = False
        Me.xrlVentaACuenta.StylePriority.UseTextAlignment = False
        Me.xrlVentaACuenta.Text = "GIRO:"
        Me.xrlVentaACuenta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.Dpi = 100.0!
        Me.TopMarginBand1.HeightF = 16.0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'xrlCantLetras1
        '
        Me.xrlCantLetras1.Dpi = 100.0!
        Me.xrlCantLetras1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlCantLetras1.LocationFloat = New DevExpress.Utils.PointFloat(56.99998!, 198.0!)
        Me.xrlCantLetras1.Name = "xrlCantLetras1"
        Me.xrlCantLetras1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCantLetras1.SizeF = New System.Drawing.SizeF(471.0!, 10.75003!)
        Me.xrlCantLetras1.StylePriority.UseFont = False
        Me.xrlCantLetras1.StylePriority.UseTextAlignment = False
        Me.xrlCantLetras1.Text = "DIEZ"
        Me.xrlCantLetras1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.Dpi = 100.0!
        Me.BottomMarginBand1.HeightF = 22.79167!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        Me.BottomMarginBand1.SnapLinePadding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        '
        'xrlNumero
        '
        Me.xrlNumero.CanGrow = False
        Me.xrlNumero.Dpi = 100.0!
        Me.xrlNumero.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlNumero.LocationFloat = New DevExpress.Utils.PointFloat(246.5833!, 18.0!)
        Me.xrlNumero.Name = "xrlNumero"
        Me.xrlNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero.SizeF = New System.Drawing.SizeF(76.04175!, 12.0!)
        Me.xrlNumero.StylePriority.UseFont = False
        Me.xrlNumero.StylePriority.UseTextAlignment = False
        Me.xrlNumero.Text = "1945"
        Me.xrlNumero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlNumero.WordWrap = False
        '
        'xrlMunic
        '
        Me.xrlMunic.CanGrow = False
        Me.xrlMunic.Dpi = 100.0!
        Me.xrlMunic.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlMunic.LocationFloat = New DevExpress.Utils.PointFloat(168.0!, 5.999994!)
        Me.xrlMunic.Name = "xrlMunic"
        Me.xrlMunic.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlMunic.SizeF = New System.Drawing.SizeF(135.0!, 13.37481!)
        Me.xrlMunic.StylePriority.UseFont = False
        Me.xrlMunic.StylePriority.UseTextAlignment = False
        Me.xrlMunic.Text = "SAN SALVADOR"
        Me.xrlMunic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlMunic.Visible = False
        Me.xrlMunic.WordWrap = False
        '
        'xrlSubTotal1
        '
        Me.xrlSubTotal1.Dpi = 100.0!
        Me.xrlSubTotal1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlSubTotal1.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 169.25!)
        Me.xrlSubTotal1.Name = "xrlSubTotal1"
        Me.xrlSubTotal1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSubTotal1.SizeF = New System.Drawing.SizeF(75.0!, 10.75003!)
        Me.xrlSubTotal1.StylePriority.UseFont = False
        Me.xrlSubTotal1.StylePriority.UseTextAlignment = False
        Me.xrlSubTotal1.Text = "0.00"
        Me.xrlSubTotal1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotalAfecto1
        '
        Me.xrlTotalAfecto1.Dpi = 100.0!
        Me.xrlTotalAfecto1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlTotalAfecto1.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 148.2499!)
        Me.xrlTotalAfecto1.Name = "xrlTotalAfecto1"
        Me.xrlTotalAfecto1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotalAfecto1.SizeF = New System.Drawing.SizeF(75.0!, 10.75002!)
        Me.xrlTotalAfecto1.StylePriority.UseFont = False
        Me.xrlTotalAfecto1.StylePriority.UseTextAlignment = False
        Me.xrlTotalAfecto1.Text = "TotalAfecto1"
        Me.xrlTotalAfecto1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVendedor
        '
        Me.xrlVendedor.CanGrow = False
        Me.xrlVendedor.Dpi = 100.0!
        Me.xrlVendedor.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlVendedor.LocationFloat = New DevExpress.Utils.PointFloat(130.1668!, 189.0!)
        Me.xrlVendedor.Name = "xrlVendedor"
        Me.xrlVendedor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVendedor.SizeF = New System.Drawing.SizeF(204.4583!, 10.75003!)
        Me.xrlVendedor.StylePriority.UseFont = False
        Me.xrlVendedor.StylePriority.UseTextAlignment = False
        Me.xrlVendedor.Text = "VENDEDOR"
        Me.xrlVendedor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlVendedor.Visible = False
        '
        'xrlCesc1
        '
        Me.xrlCesc1.Dpi = 100.0!
        Me.xrlCesc1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCesc1.LocationFloat = New DevExpress.Utils.PointFloat(468.0!, 177.9999!)
        Me.xrlCesc1.Name = "xrlCesc1"
        Me.xrlCesc1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCesc1.SizeF = New System.Drawing.SizeF(45.0!, 10.75003!)
        Me.xrlCesc1.StylePriority.UseFont = False
        Me.xrlCesc1.StylePriority.UseTextAlignment = False
        Me.xrlCesc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlGiro1
        '
        Me.xrlGiro1.CanGrow = False
        Me.xrlGiro1.Dpi = 100.0!
        Me.xrlGiro1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlGiro1.LocationFloat = New DevExpress.Utils.PointFloat(96.0!, 60.0!)
        Me.xrlGiro1.Name = "xrlGiro1"
        Me.xrlGiro1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlGiro1.SizeF = New System.Drawing.SizeF(369.0!, 9.0!)
        Me.xrlGiro1.StylePriority.UseFont = False
        Me.xrlGiro1.StylePriority.UseTextAlignment = False
        Me.xrlGiro1.Text = "Giro1"
        Me.xrlGiro1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel102, Me.xrlPrecio35, Me.xrlTotal35, Me.xrlCant35, Me.xrlVentaExenta35, Me.xrlDesc35, Me.xrlDesc36, Me.xrlVentaExenta36, Me.xrlCant36, Me.xrlTotal36, Me.xrlPrecio36, Me.XrLabel113, Me.XrLabel114, Me.xrlPrecio37, Me.xrlTotal37, Me.xrlCant37, Me.xrlVentaExenta37, Me.xrlDesc37, Me.xrlDesc27, Me.xrlVentaExenta27, Me.xrlCant27, Me.xrlTotal27, Me.xrlPrecio27, Me.XrLabel89, Me.XrLabel90, Me.xrlPrecio26, Me.xrlTotal26, Me.xrlCant26, Me.xrlVentaExenta26, Me.xrlDesc26, Me.xrlDesc25, Me.xrlVentaExenta25, Me.xrlCant25, Me.xrlTotal25, Me.xrlPrecio25, Me.XrLabel101, Me.XrLabel12, Me.xrlPrecio15, Me.xrlTotal15, Me.xrlCant15, Me.xrlVentaExenta15, Me.xrlDesc15, Me.xrlDesc16, Me.xrlVentaExenta16, Me.xrlCant16, Me.xrlTotal16, Me.xrlPrecio16, Me.XrLabel67, Me.XrLabel68, Me.xrlPrecio17, Me.xrlTotal17, Me.xrlCant17, Me.xrlVentaExenta17, Me.xrlDesc17, Me.xrlClient2, Me.xrlDoc2, Me.xrlFec2, Me.XrLabel38, Me.XrLabel40, Me.XrLabel41, Me.XrLabel45, Me.XrLabel46, Me.XrLabel49, Me.XrLabel52, Me.xrlSub2, Me.xrlImp2, Me.xrlTot2, Me.xrlArt2, Me.xrlArt1, Me.xrlTot1, Me.xrlImp1, Me.xrlSub1, Me.XrLabel30, Me.XrLabel28, Me.XrLabel16, Me.XrLabel11, Me.XrLabel15, Me.XrLabel14, Me.XrLabel13, Me.xrlFec1, Me.xrlDoc1, Me.xrlClient1, Me.xrlDesc34, Me.xrlVentaExenta34, Me.xrlCant34, Me.xrlTotal34, Me.xrlPrecio34, Me.XrLabel21, Me.XrLine3, Me.XrLabel22, Me.xrlPrecio33, Me.xrlTotal33, Me.xrlCant33, Me.xrlVentaExenta33, Me.xrlDesc33, Me.xrlDesc32, Me.xrlVentaExenta32, Me.xrlCant32, Me.xrlTotal32, Me.xrlPrecio32, Me.XrLabel47, Me.XrLabel48, Me.xrlPrecio31, Me.xrlTotal31, Me.xrlCant31, Me.xrlVentaExenta31, Me.xrlDesc31, Me.XrLabel56, Me.XrLabel57, Me.XrLabel58, Me.xrlCantLetras3, Me.xrlTotal3, Me.XrLabel61, Me.xrlCesc3, Me.XrLabel65, Me.xrlIva3, Me.xrlIvaRetenido3, Me.xrlComent3, Me.XrLabel69, Me.XrLabel70, Me.xrlTotalAfecto3, Me.xrlSubTotal3, Me.xrlTotalExento3, Me.XrLabel74, Me.XrLabel75, Me.XrLabel76, Me.XrLabel77, Me.XrLabel78, Me.xrlFormaPago3, Me.xrlGiro3, Me.XrLabel81, Me.XrLabel82, Me.xrlNit3, Me.xrlDirec3, Me.xrlFecha3, Me.xrlNrc3, Me.xrlNombre3, Me.XrLabel88, Me.XrLabel2, Me.xrlNombre2, Me.xrlNrc2, Me.xrlFecha2, Me.xrlDirec2, Me.xrlNit2, Me.XrLabel19, Me.XrLabel20, Me.xrlGiro2, Me.xrlFormaPago2, Me.XrLabel23, Me.XrLabel24, Me.XrLabel25, Me.XrLabel26, Me.XrLabel27, Me.xrlTotalExento2, Me.xrlSubTotal2, Me.xrlTotalAfecto2, Me.XrLabel32, Me.XrLabel33, Me.XrLabel34, Me.xrlIvaRetenido2, Me.xrlIva2, Me.XrLabel37, Me.xrlCesc2, Me.XrLabel39, Me.xrlTotal2, Me.xrlCantLetras2, Me.XrLabel42, Me.XrLabel43, Me.XrLabel44, Me.xrlDesc21, Me.xrlVentaExenta21, Me.xrlCant21, Me.xrlTotal21, Me.xrlPrecio21, Me.XrLabel50, Me.XrLabel51, Me.xrlPrecio22, Me.xrlTotal22, Me.xrlCant22, Me.xrlVentaExenta22, Me.xrlDesc22, Me.xrlDesc23, Me.xrlVentaExenta23, Me.xrlCant23, Me.xrlTotal23, Me.xrlPrecio23, Me.XrLabel62, Me.XrLine2, Me.XrLabel63, Me.xrlPrecio24, Me.xrlTotal24, Me.xrlCant24, Me.xrlVentaExenta24, Me.xrlDesc24, Me.xrlDesc14, Me.xrlVentaExenta14, Me.xrlCant14, Me.xrlTotal14, Me.xrlPrecio14, Me.XrLabel29, Me.XrLine1, Me.XrLabel18, Me.xrlPrecio13, Me.xrlTotal13, Me.xrlCant13, Me.xrlVentaExenta13, Me.xrlDesc13, Me.xrlDesc12, Me.xrlVentaExenta12, Me.xrlCant12, Me.xrlTotal12, Me.xrlPrecio12, Me.XrLabel17, Me.xrlVentaNoSujeta, Me.xrlPrecio11, Me.xrlTotal11, Me.xrlCant11, Me.xrlVentaExenta11, Me.xrlDesc11, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.xrlCantLetras1, Me.xrlTotal1, Me.XrLabel5, Me.xrlCesc1, Me.xrlDiasCredito, Me.xrlIva1, Me.xrlIvaRetenido, Me.xrlComentario, Me.xrlVendedor, Me.XrLabel7, Me.xrlTotalAfecto1, Me.xrlSubTotal1, Me.xrlTotalExento1, Me.XrLabel6, Me.XrLabel4, Me.XrLabel3, Me.xrlPedido, Me.xrlDocAfectados, Me.xrlNumFormulario, Me.xrlCodigo, Me.xrlFormaPago1, Me.xrlGiro1, Me.xrlTelefono, Me.xrlVentaACuenta, Me.xrlDepto, Me.xrlMunic, Me.xrlNit1, Me.xrlDirec1, Me.xrlFecha1, Me.xrlNrc1, Me.xrlNombre1, Me.xrlNumero, Me.XrLabel1})
        Me.PageHeader.Dpi = 100.0!
        Me.PageHeader.HeightF = 1074.987!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlClient2
        '
        Me.xrlClient2.CanGrow = False
        Me.xrlClient2.Dpi = 100.0!
        Me.xrlClient2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlClient2.LocationFloat = New DevExpress.Utils.PointFloat(69.0!, 998.2371!)
        Me.xrlClient2.Name = "xrlClient2"
        Me.xrlClient2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlClient2.SizeF = New System.Drawing.SizeF(324.0001!, 10.75006!)
        Me.xrlClient2.StylePriority.UseFont = False
        Me.xrlClient2.StylePriority.UseTextAlignment = False
        Me.xrlClient2.Text = "CLIENTE"
        Me.xrlClient2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlDoc2
        '
        Me.xrlDoc2.CanGrow = False
        Me.xrlDoc2.Dpi = 100.0!
        Me.xrlDoc2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlDoc2.LocationFloat = New DevExpress.Utils.PointFloat(117.0!, 1010.237!)
        Me.xrlDoc2.Name = "xrlDoc2"
        Me.xrlDoc2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDoc2.SizeF = New System.Drawing.SizeF(84.00002!, 10.75!)
        Me.xrlDoc2.StylePriority.UseFont = False
        Me.xrlDoc2.StylePriority.UseTextAlignment = False
        Me.xrlDoc2.Text = "000000"
        Me.xrlDoc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlFec2
        '
        Me.xrlFec2.CanGrow = False
        Me.xrlFec2.Dpi = 100.0!
        Me.xrlFec2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlFec2.LocationFloat = New DevExpress.Utils.PointFloat(259.625!, 1010.237!)
        Me.xrlFec2.Name = "xrlFec2"
        Me.xrlFec2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFec2.SizeF = New System.Drawing.SizeF(75.0!, 10.75!)
        Me.xrlFec2.StylePriority.UseFont = False
        Me.xrlFec2.StylePriority.UseTextAlignment = False
        Me.xrlFec2.Text = "dd/mm/yyyy"
        Me.xrlFec2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel38
        '
        Me.XrLabel38.CanGrow = False
        Me.XrLabel38.Dpi = 100.0!
        Me.XrLabel38.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel38.LocationFloat = New DevExpress.Utils.PointFloat(20.99997!, 998.2371!)
        Me.XrLabel38.Name = "XrLabel38"
        Me.XrLabel38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel38.SizeF = New System.Drawing.SizeF(45.00004!, 10.75!)
        Me.XrLabel38.StylePriority.UseFont = False
        Me.XrLabel38.StylePriority.UseTextAlignment = False
        Me.XrLabel38.Text = "CLIENTE:"
        Me.XrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel40
        '
        Me.XrLabel40.CanGrow = False
        Me.XrLabel40.Dpi = 100.0!
        Me.XrLabel40.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel40.LocationFloat = New DevExpress.Utils.PointFloat(20.99997!, 1010.237!)
        Me.XrLabel40.Name = "XrLabel40"
        Me.XrLabel40.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel40.SizeF = New System.Drawing.SizeF(93.0!, 10.75!)
        Me.XrLabel40.StylePriority.UseFont = False
        Me.XrLabel40.StylePriority.UseTextAlignment = False
        Me.XrLabel40.Text = "No.DOCUMENTO:"
        Me.XrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel41
        '
        Me.XrLabel41.CanGrow = False
        Me.XrLabel41.Dpi = 100.0!
        Me.XrLabel41.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel41.LocationFloat = New DevExpress.Utils.PointFloat(205.625!, 1010.237!)
        Me.XrLabel41.Name = "XrLabel41"
        Me.XrLabel41.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel41.SizeF = New System.Drawing.SizeF(48.0!, 10.75!)
        Me.XrLabel41.StylePriority.UseFont = False
        Me.XrLabel41.StylePriority.UseTextAlignment = False
        Me.XrLabel41.Text = "FECHA:"
        Me.XrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel45
        '
        Me.XrLabel45.CanGrow = False
        Me.XrLabel45.Dpi = 100.0!
        Me.XrLabel45.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel45.LocationFloat = New DevExpress.Utils.PointFloat(20.99997!, 1022.237!)
        Me.XrLabel45.Name = "XrLabel45"
        Me.XrLabel45.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel45.SizeF = New System.Drawing.SizeF(93.0!, 10.75!)
        Me.XrLabel45.StylePriority.UseFont = False
        Me.XrLabel45.StylePriority.UseTextAlignment = False
        Me.XrLabel45.Text = "SUB-TOTAL:"
        Me.XrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel46
        '
        Me.XrLabel46.CanGrow = False
        Me.XrLabel46.Dpi = 100.0!
        Me.XrLabel46.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel46.LocationFloat = New DevExpress.Utils.PointFloat(204.0!, 1022.237!)
        Me.XrLabel46.Name = "XrLabel46"
        Me.XrLabel46.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel46.SizeF = New System.Drawing.SizeF(63.00002!, 10.75!)
        Me.XrLabel46.StylePriority.UseFont = False
        Me.XrLabel46.StylePriority.UseTextAlignment = False
        Me.XrLabel46.Text = "IMPUESTO:"
        Me.XrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel49
        '
        Me.XrLabel49.CanGrow = False
        Me.XrLabel49.Dpi = 100.0!
        Me.XrLabel49.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel49.LocationFloat = New DevExpress.Utils.PointFloat(322.6251!, 1022.237!)
        Me.XrLabel49.Name = "XrLabel49"
        Me.XrLabel49.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel49.SizeF = New System.Drawing.SizeF(37.37488!, 10.75!)
        Me.XrLabel49.StylePriority.UseFont = False
        Me.XrLabel49.StylePriority.UseTextAlignment = False
        Me.XrLabel49.Text = "TOTAL:"
        Me.XrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel52
        '
        Me.XrLabel52.CanGrow = False
        Me.XrLabel52.Dpi = 100.0!
        Me.XrLabel52.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel52.LocationFloat = New DevExpress.Utils.PointFloat(20.99997!, 1034.237!)
        Me.XrLabel52.Name = "XrLabel52"
        Me.XrLabel52.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel52.SizeF = New System.Drawing.SizeF(93.0!, 10.75!)
        Me.XrLabel52.StylePriority.UseFont = False
        Me.XrLabel52.StylePriority.UseTextAlignment = False
        Me.XrLabel52.Text = "ARTICULO:"
        Me.XrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlSub2
        '
        Me.xrlSub2.CanGrow = False
        Me.xrlSub2.Dpi = 100.0!
        Me.xrlSub2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlSub2.LocationFloat = New DevExpress.Utils.PointFloat(117.0!, 1022.237!)
        Me.xrlSub2.Name = "xrlSub2"
        Me.xrlSub2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSub2.SizeF = New System.Drawing.SizeF(84.0!, 10.75!)
        Me.xrlSub2.StylePriority.UseFont = False
        Me.xrlSub2.StylePriority.UseTextAlignment = False
        Me.xrlSub2.Text = "000000"
        Me.xrlSub2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlImp2
        '
        Me.xrlImp2.CanGrow = False
        Me.xrlImp2.Dpi = 100.0!
        Me.xrlImp2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlImp2.LocationFloat = New DevExpress.Utils.PointFloat(267.0!, 1022.237!)
        Me.xrlImp2.Name = "xrlImp2"
        Me.xrlImp2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlImp2.SizeF = New System.Drawing.SizeF(55.62509!, 10.75!)
        Me.xrlImp2.StylePriority.UseFont = False
        Me.xrlImp2.StylePriority.UseTextAlignment = False
        Me.xrlImp2.Text = "000000"
        Me.xrlImp2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlTot2
        '
        Me.xrlTot2.CanGrow = False
        Me.xrlTot2.Dpi = 100.0!
        Me.xrlTot2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlTot2.LocationFloat = New DevExpress.Utils.PointFloat(360.0002!, 1022.237!)
        Me.xrlTot2.Name = "xrlTot2"
        Me.xrlTot2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTot2.SizeF = New System.Drawing.SizeF(41.99982!, 10.75!)
        Me.xrlTot2.StylePriority.UseFont = False
        Me.xrlTot2.StylePriority.UseTextAlignment = False
        Me.xrlTot2.Text = "000000"
        Me.xrlTot2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlArt2
        '
        Me.xrlArt2.CanGrow = False
        Me.xrlArt2.Dpi = 100.0!
        Me.xrlArt2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlArt2.LocationFloat = New DevExpress.Utils.PointFloat(117.0!, 1034.237!)
        Me.xrlArt2.Name = "xrlArt2"
        Me.xrlArt2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlArt2.SizeF = New System.Drawing.SizeF(294.0001!, 10.75!)
        Me.xrlArt2.StylePriority.UseFont = False
        Me.xrlArt2.StylePriority.UseTextAlignment = False
        Me.xrlArt2.Text = "--"
        Me.xrlArt2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlArt1
        '
        Me.xrlArt1.CanGrow = False
        Me.xrlArt1.Dpi = 100.0!
        Me.xrlArt1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlArt1.LocationFloat = New DevExpress.Utils.PointFloat(114.0!, 944.9999!)
        Me.xrlArt1.Name = "xrlArt1"
        Me.xrlArt1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlArt1.SizeF = New System.Drawing.SizeF(300.0001!, 10.75!)
        Me.xrlArt1.StylePriority.UseFont = False
        Me.xrlArt1.StylePriority.UseTextAlignment = False
        Me.xrlArt1.Text = "--"
        Me.xrlArt1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlTot1
        '
        Me.xrlTot1.CanGrow = False
        Me.xrlTot1.Dpi = 100.0!
        Me.xrlTot1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlTot1.LocationFloat = New DevExpress.Utils.PointFloat(360.0!, 932.9999!)
        Me.xrlTot1.Name = "xrlTot1"
        Me.xrlTot1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTot1.SizeF = New System.Drawing.SizeF(42.00006!, 10.75!)
        Me.xrlTot1.StylePriority.UseFont = False
        Me.xrlTot1.StylePriority.UseTextAlignment = False
        Me.xrlTot1.Text = "000000"
        Me.xrlTot1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlImp1
        '
        Me.xrlImp1.CanGrow = False
        Me.xrlImp1.Dpi = 100.0!
        Me.xrlImp1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlImp1.LocationFloat = New DevExpress.Utils.PointFloat(265.625!, 932.9999!)
        Me.xrlImp1.Name = "xrlImp1"
        Me.xrlImp1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlImp1.SizeF = New System.Drawing.SizeF(57.00003!, 10.75!)
        Me.xrlImp1.StylePriority.UseFont = False
        Me.xrlImp1.StylePriority.UseTextAlignment = False
        Me.xrlImp1.Text = "000000"
        Me.xrlImp1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlSub1
        '
        Me.xrlSub1.CanGrow = False
        Me.xrlSub1.Dpi = 100.0!
        Me.xrlSub1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlSub1.LocationFloat = New DevExpress.Utils.PointFloat(114.0!, 932.9999!)
        Me.xrlSub1.Name = "xrlSub1"
        Me.xrlSub1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSub1.SizeF = New System.Drawing.SizeF(84.0!, 10.75!)
        Me.xrlSub1.StylePriority.UseFont = False
        Me.xrlSub1.StylePriority.UseTextAlignment = False
        Me.xrlSub1.Text = "000000"
        Me.xrlSub1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel30
        '
        Me.XrLabel30.CanGrow = False
        Me.XrLabel30.Dpi = 100.0!
        Me.XrLabel30.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(17.99996!, 944.9999!)
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.SizeF = New System.Drawing.SizeF(93.0!, 10.75!)
        Me.XrLabel30.StylePriority.UseFont = False
        Me.XrLabel30.StylePriority.UseTextAlignment = False
        Me.XrLabel30.Text = "ARTICULO:"
        Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel28
        '
        Me.XrLabel28.CanGrow = False
        Me.XrLabel28.Dpi = 100.0!
        Me.XrLabel28.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(322.6251!, 932.9999!)
        Me.XrLabel28.Name = "XrLabel28"
        Me.XrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel28.SizeF = New System.Drawing.SizeF(37.37491!, 10.75!)
        Me.XrLabel28.StylePriority.UseFont = False
        Me.XrLabel28.StylePriority.UseTextAlignment = False
        Me.XrLabel28.Text = "TOTAL:"
        Me.XrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel16
        '
        Me.XrLabel16.CanGrow = False
        Me.XrLabel16.Dpi = 100.0!
        Me.XrLabel16.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(204.0!, 932.9999!)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(60.00003!, 10.75!)
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.StylePriority.UseTextAlignment = False
        Me.XrLabel16.Text = "IMPUESTO:"
        Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel11
        '
        Me.XrLabel11.CanGrow = False
        Me.XrLabel11.Dpi = 100.0!
        Me.XrLabel11.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(17.99996!, 932.9999!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(93.0!, 10.75!)
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "SUB-TOTAL:"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel15
        '
        Me.XrLabel15.CanGrow = False
        Me.XrLabel15.Dpi = 100.0!
        Me.XrLabel15.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(205.625!, 921.0!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(48.0!, 10.75!)
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        Me.XrLabel15.Text = "FECHA:"
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel14
        '
        Me.XrLabel14.CanGrow = False
        Me.XrLabel14.Dpi = 100.0!
        Me.XrLabel14.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(17.99996!, 921.0!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(93.0!, 10.75!)
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        Me.XrLabel14.Text = "No.DOCUMENTO:"
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel13
        '
        Me.XrLabel13.CanGrow = False
        Me.XrLabel13.Dpi = 100.0!
        Me.XrLabel13.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(17.99996!, 909.0!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(45.00004!, 10.75006!)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "CLIENTE:"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlFec1
        '
        Me.xrlFec1.CanGrow = False
        Me.xrlFec1.Dpi = 100.0!
        Me.xrlFec1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlFec1.LocationFloat = New DevExpress.Utils.PointFloat(259.625!, 921.0!)
        Me.xrlFec1.Name = "xrlFec1"
        Me.xrlFec1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFec1.SizeF = New System.Drawing.SizeF(75.0!, 10.75!)
        Me.xrlFec1.StylePriority.UseFont = False
        Me.xrlFec1.StylePriority.UseTextAlignment = False
        Me.xrlFec1.Text = "dd/mm/yyyy"
        Me.xrlFec1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlDoc1
        '
        Me.xrlDoc1.CanGrow = False
        Me.xrlDoc1.Dpi = 100.0!
        Me.xrlDoc1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlDoc1.LocationFloat = New DevExpress.Utils.PointFloat(114.0!, 921.0!)
        Me.xrlDoc1.Name = "xrlDoc1"
        Me.xrlDoc1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDoc1.SizeF = New System.Drawing.SizeF(83.99999!, 10.75!)
        Me.xrlDoc1.StylePriority.UseFont = False
        Me.xrlDoc1.StylePriority.UseTextAlignment = False
        Me.xrlDoc1.Text = "000000"
        Me.xrlDoc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlClient1
        '
        Me.xrlClient1.CanGrow = False
        Me.xrlClient1.Dpi = 100.0!
        Me.xrlClient1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlClient1.LocationFloat = New DevExpress.Utils.PointFloat(69.0!, 908.9999!)
        Me.xrlClient1.Name = "xrlClient1"
        Me.xrlClient1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlClient1.SizeF = New System.Drawing.SizeF(324.0001!, 10.75!)
        Me.xrlClient1.StylePriority.UseFont = False
        Me.xrlClient1.StylePriority.UseTextAlignment = False
        Me.xrlClient1.Text = "CLIENTE"
        Me.xrlClient1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlDesc34
        '
        Me.xrlDesc34.CanGrow = False
        Me.xrlDesc34.Dpi = 100.0!
        Me.xrlDesc34.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc34.LocationFloat = New DevExpress.Utils.PointFloat(59.99994!, 723.0001!)
        Me.xrlDesc34.Name = "xrlDesc34"
        Me.xrlDesc34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc34.SizeF = New System.Drawing.SizeF(450.0!, 9.000061!)
        Me.xrlDesc34.StylePriority.UseFont = False
        Me.xrlDesc34.StylePriority.UseTextAlignment = False
        Me.xrlDesc34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaExenta34
        '
        Me.xrlVentaExenta34.Dpi = 100.0!
        Me.xrlVentaExenta34.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta34.LocationFloat = New DevExpress.Utils.PointFloat(647.9998!, 723.0001!)
        Me.xrlVentaExenta34.Name = "xrlVentaExenta34"
        Me.xrlVentaExenta34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta34.SizeF = New System.Drawing.SizeF(63.00018!, 9.000061!)
        Me.xrlVentaExenta34.StylePriority.UseFont = False
        Me.xrlVentaExenta34.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant34
        '
        Me.xrlCant34.Dpi = 100.0!
        Me.xrlCant34.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant34.LocationFloat = New DevExpress.Utils.PointFloat(2.999984!, 723.0001!)
        Me.xrlCant34.Name = "xrlCant34"
        Me.xrlCant34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant34.SizeF = New System.Drawing.SizeF(45.0001!, 9.0!)
        Me.xrlCant34.StylePriority.UseFont = False
        Me.xrlCant34.StylePriority.UseTextAlignment = False
        Me.xrlCant34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal34
        '
        Me.xrlTotal34.Dpi = 100.0!
        Me.xrlTotal34.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal34.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 723.0001!)
        Me.xrlTotal34.Name = "xrlTotal34"
        Me.xrlTotal34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal34.SizeF = New System.Drawing.SizeF(75.0!, 9.000061!)
        Me.xrlTotal34.StylePriority.UseFont = False
        Me.xrlTotal34.StylePriority.UseTextAlignment = False
        Me.xrlTotal34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlPrecio34
        '
        Me.xrlPrecio34.Dpi = 100.0!
        Me.xrlPrecio34.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio34.LocationFloat = New DevExpress.Utils.PointFloat(542.9998!, 723.0001!)
        Me.xrlPrecio34.Name = "xrlPrecio34"
        Me.xrlPrecio34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio34.SizeF = New System.Drawing.SizeF(60.0!, 9.000061!)
        Me.xrlPrecio34.StylePriority.UseFont = False
        Me.xrlPrecio34.StylePriority.UseTextAlignment = False
        Me.xrlPrecio34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel21
        '
        Me.XrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel21.Dpi = 100.0!
        Me.XrLabel21.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 723.0001!)
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel21.SizeF = New System.Drawing.SizeF(39.87488!, 9.0!)
        Me.XrLabel21.StylePriority.UseBorders = False
        Me.XrLabel21.StylePriority.UseFont = False
        Me.XrLabel21.StylePriority.UseTextAlignment = False
        Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel21.Visible = False
        '
        'XrLine3
        '
        Me.XrLine3.Dpi = 100.0!
        Me.XrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine3.LocationFloat = New DevExpress.Utils.PointFloat(312.0!, 903.0!)
        Me.XrLine3.Name = "XrLine3"
        Me.XrLine3.SizeF = New System.Drawing.SizeF(219.0!, 2.0!)
        Me.XrLine3.Visible = False
        '
        'XrLabel22
        '
        Me.XrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel22.Dpi = 100.0!
        Me.XrLabel22.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 714.0001!)
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel22.SizeF = New System.Drawing.SizeF(39.87488!, 9.0!)
        Me.XrLabel22.StylePriority.UseBorders = False
        Me.XrLabel22.StylePriority.UseFont = False
        Me.XrLabel22.StylePriority.UseTextAlignment = False
        Me.XrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel22.Visible = False
        '
        'xrlPrecio33
        '
        Me.xrlPrecio33.Dpi = 100.0!
        Me.xrlPrecio33.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio33.LocationFloat = New DevExpress.Utils.PointFloat(542.9998!, 714.0001!)
        Me.xrlPrecio33.Name = "xrlPrecio33"
        Me.xrlPrecio33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio33.SizeF = New System.Drawing.SizeF(60.0!, 9.0!)
        Me.xrlPrecio33.StylePriority.UseFont = False
        Me.xrlPrecio33.StylePriority.UseTextAlignment = False
        Me.xrlPrecio33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal33
        '
        Me.xrlTotal33.Dpi = 100.0!
        Me.xrlTotal33.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal33.LocationFloat = New DevExpress.Utils.PointFloat(710.9999!, 714.0001!)
        Me.xrlTotal33.Name = "xrlTotal33"
        Me.xrlTotal33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal33.SizeF = New System.Drawing.SizeF(75.00006!, 9.0!)
        Me.xrlTotal33.StylePriority.UseFont = False
        Me.xrlTotal33.StylePriority.UseTextAlignment = False
        Me.xrlTotal33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant33
        '
        Me.xrlCant33.Dpi = 100.0!
        Me.xrlCant33.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant33.LocationFloat = New DevExpress.Utils.PointFloat(3.000051!, 714.0001!)
        Me.xrlCant33.Name = "xrlCant33"
        Me.xrlCant33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant33.SizeF = New System.Drawing.SizeF(45.00003!, 9.0!)
        Me.xrlCant33.StylePriority.UseFont = False
        Me.xrlCant33.StylePriority.UseTextAlignment = False
        Me.xrlCant33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta33
        '
        Me.xrlVentaExenta33.Dpi = 100.0!
        Me.xrlVentaExenta33.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta33.LocationFloat = New DevExpress.Utils.PointFloat(647.9999!, 714.0001!)
        Me.xrlVentaExenta33.Name = "xrlVentaExenta33"
        Me.xrlVentaExenta33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta33.SizeF = New System.Drawing.SizeF(63.00006!, 9.0!)
        Me.xrlVentaExenta33.StylePriority.UseFont = False
        Me.xrlVentaExenta33.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDesc33
        '
        Me.xrlDesc33.CanGrow = False
        Me.xrlDesc33.Dpi = 100.0!
        Me.xrlDesc33.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc33.LocationFloat = New DevExpress.Utils.PointFloat(60.0!, 714.0001!)
        Me.xrlDesc33.Name = "xrlDesc33"
        Me.xrlDesc33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc33.SizeF = New System.Drawing.SizeF(450.0!, 9.0!)
        Me.xrlDesc33.StylePriority.UseFont = False
        Me.xrlDesc33.StylePriority.UseTextAlignment = False
        Me.xrlDesc33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDesc32
        '
        Me.xrlDesc32.CanGrow = False
        Me.xrlDesc32.Dpi = 100.0!
        Me.xrlDesc32.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc32.LocationFloat = New DevExpress.Utils.PointFloat(60.0!, 705.0!)
        Me.xrlDesc32.Name = "xrlDesc32"
        Me.xrlDesc32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc32.SizeF = New System.Drawing.SizeF(450.0!, 9.000061!)
        Me.xrlDesc32.StylePriority.UseFont = False
        Me.xrlDesc32.StylePriority.UseTextAlignment = False
        Me.xrlDesc32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaExenta32
        '
        Me.xrlVentaExenta32.Dpi = 100.0!
        Me.xrlVentaExenta32.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta32.LocationFloat = New DevExpress.Utils.PointFloat(647.9999!, 705.0!)
        Me.xrlVentaExenta32.Name = "xrlVentaExenta32"
        Me.xrlVentaExenta32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta32.SizeF = New System.Drawing.SizeF(63.00006!, 9.000061!)
        Me.xrlVentaExenta32.StylePriority.UseFont = False
        Me.xrlVentaExenta32.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant32
        '
        Me.xrlCant32.Dpi = 100.0!
        Me.xrlCant32.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant32.LocationFloat = New DevExpress.Utils.PointFloat(3.000069!, 705.0!)
        Me.xrlCant32.Name = "xrlCant32"
        Me.xrlCant32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant32.SizeF = New System.Drawing.SizeF(45.00001!, 9.000061!)
        Me.xrlCant32.StylePriority.UseFont = False
        Me.xrlCant32.StylePriority.UseTextAlignment = False
        Me.xrlCant32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal32
        '
        Me.xrlTotal32.Dpi = 100.0!
        Me.xrlTotal32.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal32.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 705.0!)
        Me.xrlTotal32.Name = "xrlTotal32"
        Me.xrlTotal32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal32.SizeF = New System.Drawing.SizeF(75.0!, 9.000061!)
        Me.xrlTotal32.StylePriority.UseFont = False
        Me.xrlTotal32.StylePriority.UseTextAlignment = False
        Me.xrlTotal32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlPrecio32
        '
        Me.xrlPrecio32.Dpi = 100.0!
        Me.xrlPrecio32.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio32.LocationFloat = New DevExpress.Utils.PointFloat(542.9998!, 705.0!)
        Me.xrlPrecio32.Name = "xrlPrecio32"
        Me.xrlPrecio32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio32.SizeF = New System.Drawing.SizeF(60.0!, 9.000061!)
        Me.xrlPrecio32.StylePriority.UseFont = False
        Me.xrlPrecio32.StylePriority.UseTextAlignment = False
        Me.xrlPrecio32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel47
        '
        Me.XrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel47.Dpi = 100.0!
        Me.XrLabel47.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel47.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 705.0!)
        Me.XrLabel47.Name = "XrLabel47"
        Me.XrLabel47.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel47.SizeF = New System.Drawing.SizeF(39.87488!, 9.000061!)
        Me.XrLabel47.StylePriority.UseBorders = False
        Me.XrLabel47.StylePriority.UseFont = False
        Me.XrLabel47.StylePriority.UseTextAlignment = False
        Me.XrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel47.Visible = False
        '
        'XrLabel48
        '
        Me.XrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel48.Dpi = 100.0!
        Me.XrLabel48.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel48.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 696.0!)
        Me.XrLabel48.Name = "XrLabel48"
        Me.XrLabel48.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel48.SizeF = New System.Drawing.SizeF(39.87488!, 9.0!)
        Me.XrLabel48.StylePriority.UseBorders = False
        Me.XrLabel48.StylePriority.UseFont = False
        Me.XrLabel48.StylePriority.UseTextAlignment = False
        Me.XrLabel48.Text = "0.00"
        Me.XrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel48.Visible = False
        '
        'xrlPrecio31
        '
        Me.xrlPrecio31.Dpi = 100.0!
        Me.xrlPrecio31.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio31.LocationFloat = New DevExpress.Utils.PointFloat(542.9998!, 696.0!)
        Me.xrlPrecio31.Name = "xrlPrecio31"
        Me.xrlPrecio31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio31.SizeF = New System.Drawing.SizeF(60.0!, 9.0!)
        Me.xrlPrecio31.StylePriority.UseFont = False
        Me.xrlPrecio31.StylePriority.UseTextAlignment = False
        Me.xrlPrecio31.Text = "Precio11"
        Me.xrlPrecio31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal31
        '
        Me.xrlTotal31.Dpi = 100.0!
        Me.xrlTotal31.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal31.LocationFloat = New DevExpress.Utils.PointFloat(710.9999!, 696.0001!)
        Me.xrlTotal31.Name = "xrlTotal31"
        Me.xrlTotal31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal31.SizeF = New System.Drawing.SizeF(75.00006!, 8.999939!)
        Me.xrlTotal31.StylePriority.UseFont = False
        Me.xrlTotal31.StylePriority.UseTextAlignment = False
        Me.xrlTotal31.Text = "Total11"
        Me.xrlTotal31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant31
        '
        Me.xrlCant31.Dpi = 100.0!
        Me.xrlCant31.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant31.LocationFloat = New DevExpress.Utils.PointFloat(3.000077!, 696.0!)
        Me.xrlCant31.Name = "xrlCant31"
        Me.xrlCant31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant31.SizeF = New System.Drawing.SizeF(45.0!, 9.0!)
        Me.xrlCant31.StylePriority.UseFont = False
        Me.xrlCant31.StylePriority.UseTextAlignment = False
        Me.xrlCant31.Text = "Cant11"
        Me.xrlCant31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta31
        '
        Me.xrlVentaExenta31.Dpi = 100.0!
        Me.xrlVentaExenta31.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta31.LocationFloat = New DevExpress.Utils.PointFloat(647.9999!, 696.0!)
        Me.xrlVentaExenta31.Name = "xrlVentaExenta31"
        Me.xrlVentaExenta31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta31.SizeF = New System.Drawing.SizeF(63.00006!, 9.0!)
        Me.xrlVentaExenta31.StylePriority.UseFont = False
        Me.xrlVentaExenta31.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta31.Text = "VentaExenta11"
        Me.xrlVentaExenta31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDesc31
        '
        Me.xrlDesc31.CanGrow = False
        Me.xrlDesc31.Dpi = 100.0!
        Me.xrlDesc31.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc31.LocationFloat = New DevExpress.Utils.PointFloat(60.0!, 696.0!)
        Me.xrlDesc31.Name = "xrlDesc31"
        Me.xrlDesc31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc31.SizeF = New System.Drawing.SizeF(450.0!, 9.0!)
        Me.xrlDesc31.StylePriority.UseFont = False
        Me.xrlDesc31.StylePriority.UseTextAlignment = False
        Me.xrlDesc31.Text = "Desc11"
        Me.xrlDesc31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel56
        '
        Me.XrLabel56.Dpi = 100.0!
        Me.XrLabel56.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel56.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 811.7498!)
        Me.XrLabel56.Name = "XrLabel56"
        Me.XrLabel56.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel56.SizeF = New System.Drawing.SizeF(66.00031!, 8.250061!)
        Me.XrLabel56.StylePriority.UseFont = False
        Me.XrLabel56.StylePriority.UseTextAlignment = False
        Me.XrLabel56.Text = "TOTAL:"
        Me.XrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel57
        '
        Me.XrLabel57.Dpi = 100.0!
        Me.XrLabel57.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel57.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 800.9999!)
        Me.XrLabel57.Name = "XrLabel57"
        Me.XrLabel57.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel57.SizeF = New System.Drawing.SizeF(66.00031!, 10.75!)
        Me.XrLabel57.StylePriority.UseFont = False
        Me.XrLabel57.StylePriority.UseTextAlignment = False
        Me.XrLabel57.Text = "Venta Exenta:"
        Me.XrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel58
        '
        Me.XrLabel58.Dpi = 100.0!
        Me.XrLabel58.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel58.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 790.2498!)
        Me.XrLabel58.Name = "XrLabel58"
        Me.XrLabel58.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel58.SizeF = New System.Drawing.SizeF(66.00031!, 10.75!)
        Me.XrLabel58.StylePriority.UseFont = False
        Me.XrLabel58.StylePriority.UseTextAlignment = False
        Me.XrLabel58.Text = "IVA Retenido:"
        Me.XrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCantLetras3
        '
        Me.xrlCantLetras3.Dpi = 100.0!
        Me.xrlCantLetras3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlCantLetras3.LocationFloat = New DevExpress.Utils.PointFloat(56.99998!, 809.2499!)
        Me.xrlCantLetras3.Name = "xrlCantLetras3"
        Me.xrlCantLetras3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCantLetras3.SizeF = New System.Drawing.SizeF(471.0!, 10.75003!)
        Me.xrlCantLetras3.StylePriority.UseFont = False
        Me.xrlCantLetras3.StylePriority.UseTextAlignment = False
        Me.xrlCantLetras3.Text = "DIEZ"
        Me.xrlCantLetras3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlTotal3
        '
        Me.xrlTotal3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlTotal3.Dpi = 100.0!
        Me.xrlTotal3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlTotal3.LocationFloat = New DevExpress.Utils.PointFloat(708.0002!, 811.7498!)
        Me.xrlTotal3.Name = "xrlTotal3"
        Me.xrlTotal3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal3.SizeF = New System.Drawing.SizeF(77.99982!, 8.250061!)
        Me.xrlTotal3.StylePriority.UseBorders = False
        Me.xrlTotal3.StylePriority.UseFont = False
        Me.xrlTotal3.StylePriority.UseTextAlignment = False
        Me.xrlTotal3.Text = "0.00"
        Me.xrlTotal3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel61
        '
        Me.XrLabel61.Dpi = 100.0!
        Me.XrLabel61.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel61.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 761.0001!)
        Me.XrLabel61.Name = "XrLabel61"
        Me.XrLabel61.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel61.SizeF = New System.Drawing.SizeF(66.00031!, 10.75!)
        Me.XrLabel61.StylePriority.UseFont = False
        Me.XrLabel61.StylePriority.UseTextAlignment = False
        Me.XrLabel61.Text = "Sumas:"
        Me.XrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCesc3
        '
        Me.xrlCesc3.Dpi = 100.0!
        Me.xrlCesc3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCesc3.LocationFloat = New DevExpress.Utils.PointFloat(467.9999!, 783.9999!)
        Me.xrlCesc3.Name = "xrlCesc3"
        Me.xrlCesc3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCesc3.SizeF = New System.Drawing.SizeF(45.0!, 10.75003!)
        Me.xrlCesc3.StylePriority.UseFont = False
        Me.xrlCesc3.StylePriority.UseTextAlignment = False
        Me.xrlCesc3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel65
        '
        Me.XrLabel65.CanGrow = False
        Me.XrLabel65.Dpi = 100.0!
        Me.XrLabel65.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel65.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 795.0!)
        Me.XrLabel65.Name = "XrLabel65"
        Me.XrLabel65.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel65.SizeF = New System.Drawing.SizeF(138.0001!, 10.75003!)
        Me.XrLabel65.StylePriority.UseFont = False
        Me.XrLabel65.StylePriority.UseTextAlignment = False
        Me.XrLabel65.Text = "VENDEDOR"
        Me.XrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel65.Visible = False
        '
        'xrlIva3
        '
        Me.xrlIva3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIva3.Dpi = 100.0!
        Me.xrlIva3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlIva3.LocationFloat = New DevExpress.Utils.PointFloat(708.0002!, 770.9999!)
        Me.xrlIva3.Name = "xrlIva3"
        Me.xrlIva3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIva3.SizeF = New System.Drawing.SizeF(77.99982!, 10.75006!)
        Me.xrlIva3.StylePriority.UseBorders = False
        Me.xrlIva3.StylePriority.UseFont = False
        Me.xrlIva3.StylePriority.UseTextAlignment = False
        Me.xrlIva3.Text = "Iva"
        Me.xrlIva3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlIvaRetenido3
        '
        Me.xrlIvaRetenido3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIvaRetenido3.Dpi = 100.0!
        Me.xrlIvaRetenido3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlIvaRetenido3.LocationFloat = New DevExpress.Utils.PointFloat(708.0002!, 790.2498!)
        Me.xrlIvaRetenido3.Name = "xrlIvaRetenido3"
        Me.xrlIvaRetenido3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIvaRetenido3.SizeF = New System.Drawing.SizeF(77.99982!, 10.75!)
        Me.xrlIvaRetenido3.StylePriority.UseBorders = False
        Me.xrlIvaRetenido3.StylePriority.UseFont = False
        Me.xrlIvaRetenido3.StylePriority.UseTextAlignment = False
        Me.xrlIvaRetenido3.Text = "0.00"
        Me.xrlIvaRetenido3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlComent3
        '
        Me.xrlComent3.CanShrink = True
        Me.xrlComent3.Dpi = 100.0!
        Me.xrlComent3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlComent3.LocationFloat = New DevExpress.Utils.PointFloat(44.08041!, 784.25!)
        Me.xrlComent3.Name = "xrlComent3"
        Me.xrlComent3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlComent3.SizeF = New System.Drawing.SizeF(222.2495!, 10.75003!)
        Me.xrlComent3.StylePriority.UseFont = False
        Me.xrlComent3.StylePriority.UseTextAlignment = False
        Me.xrlComent3.Text = "Comentario"
        Me.xrlComent3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlComent3.Visible = False
        '
        'XrLabel69
        '
        Me.XrLabel69.CanGrow = False
        Me.XrLabel69.Dpi = 100.0!
        Me.XrLabel69.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel69.LocationFloat = New DevExpress.Utils.PointFloat(130.1667!, 795.0!)
        Me.XrLabel69.Name = "XrLabel69"
        Me.XrLabel69.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel69.SizeF = New System.Drawing.SizeF(204.4583!, 10.75003!)
        Me.XrLabel69.StylePriority.UseFont = False
        Me.XrLabel69.StylePriority.UseTextAlignment = False
        Me.XrLabel69.Text = "VENDEDOR"
        Me.XrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel69.Visible = False
        '
        'XrLabel70
        '
        Me.XrLabel70.Dpi = 100.0!
        Me.XrLabel70.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel70.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 781.2498!)
        Me.XrLabel70.Name = "XrLabel70"
        Me.XrLabel70.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel70.SizeF = New System.Drawing.SizeF(66.00031!, 10.75!)
        Me.XrLabel70.StylePriority.UseFont = False
        Me.XrLabel70.StylePriority.UseTextAlignment = False
        Me.XrLabel70.Text = "Sub-Total:"
        Me.XrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTotalAfecto3
        '
        Me.xrlTotalAfecto3.Dpi = 100.0!
        Me.xrlTotalAfecto3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlTotalAfecto3.LocationFloat = New DevExpress.Utils.PointFloat(708.0002!, 761.0001!)
        Me.xrlTotalAfecto3.Name = "xrlTotalAfecto3"
        Me.xrlTotalAfecto3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotalAfecto3.SizeF = New System.Drawing.SizeF(77.99982!, 10.75!)
        Me.xrlTotalAfecto3.StylePriority.UseFont = False
        Me.xrlTotalAfecto3.StylePriority.UseTextAlignment = False
        Me.xrlTotalAfecto3.Text = "TotalAfecto1"
        Me.xrlTotalAfecto3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlSubTotal3
        '
        Me.xrlSubTotal3.Dpi = 100.0!
        Me.xrlSubTotal3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlSubTotal3.LocationFloat = New DevExpress.Utils.PointFloat(708.0002!, 781.2498!)
        Me.xrlSubTotal3.Name = "xrlSubTotal3"
        Me.xrlSubTotal3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSubTotal3.SizeF = New System.Drawing.SizeF(77.99982!, 10.75!)
        Me.xrlSubTotal3.StylePriority.UseFont = False
        Me.xrlSubTotal3.StylePriority.UseTextAlignment = False
        Me.xrlSubTotal3.Text = "0.00"
        Me.xrlSubTotal3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotalExento3
        '
        Me.xrlTotalExento3.Dpi = 100.0!
        Me.xrlTotalExento3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlTotalExento3.LocationFloat = New DevExpress.Utils.PointFloat(708.0002!, 800.9999!)
        Me.xrlTotalExento3.Name = "xrlTotalExento3"
        Me.xrlTotalExento3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotalExento3.SizeF = New System.Drawing.SizeF(77.99982!, 10.75!)
        Me.xrlTotalExento3.StylePriority.UseFont = False
        Me.xrlTotalExento3.StylePriority.UseTextAlignment = False
        Me.xrlTotalExento3.Text = "TotalExento"
        Me.xrlTotalExento3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel74
        '
        Me.XrLabel74.Dpi = 100.0!
        Me.XrLabel74.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel74.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 771.0!)
        Me.XrLabel74.Name = "XrLabel74"
        Me.XrLabel74.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel74.SizeF = New System.Drawing.SizeF(66.00031!, 10.74988!)
        Me.XrLabel74.StylePriority.UseFont = False
        Me.XrLabel74.StylePriority.UseTextAlignment = False
        Me.XrLabel74.Text = "IVA:"
        Me.XrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel75
        '
        Me.XrLabel75.CanGrow = False
        Me.XrLabel75.Dpi = 100.0!
        Me.XrLabel75.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel75.LocationFloat = New DevExpress.Utils.PointFloat(471.0001!, 638.9999!)
        Me.XrLabel75.Name = "XrLabel75"
        Me.XrLabel75.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel75.SizeF = New System.Drawing.SizeF(81.0!, 10.37482!)
        Me.XrLabel75.StylePriority.UseFont = False
        Me.XrLabel75.StylePriority.UseTextAlignment = False
        Me.XrLabel75.Text = "FECHA:"
        Me.XrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel76
        '
        Me.XrLabel76.CanGrow = False
        Me.XrLabel76.Dpi = 100.0!
        Me.XrLabel76.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel76.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 660.0!)
        Me.XrLabel76.Name = "XrLabel76"
        Me.XrLabel76.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel76.SizeF = New System.Drawing.SizeF(56.99998!, 9.0!)
        Me.XrLabel76.StylePriority.UseFont = False
        Me.XrLabel76.StylePriority.UseTextAlignment = False
        Me.XrLabel76.Text = "DIRECCIÓN:"
        Me.XrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel77
        '
        Me.XrLabel77.Dpi = 100.0!
        Me.XrLabel77.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel77.LocationFloat = New DevExpress.Utils.PointFloat(470.9999!, 669.0001!)
        Me.XrLabel77.Name = "XrLabel77"
        Me.XrLabel77.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel77.SizeF = New System.Drawing.SizeF(81.0!, 9.0!)
        Me.XrLabel77.StylePriority.UseFont = False
        Me.XrLabel77.StylePriority.UseTextAlignment = False
        Me.XrLabel77.Text = "FORMA DE PAGO:"
        Me.XrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel78
        '
        Me.XrLabel78.CanGrow = False
        Me.XrLabel78.Dpi = 100.0!
        Me.XrLabel78.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel78.LocationFloat = New DevExpress.Utils.PointFloat(470.9999!, 660.0001!)
        Me.XrLabel78.Name = "XrLabel78"
        Me.XrLabel78.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel78.SizeF = New System.Drawing.SizeF(81.0!, 9.0!)
        Me.XrLabel78.StylePriority.UseFont = False
        Me.XrLabel78.StylePriority.UseTextAlignment = False
        Me.XrLabel78.Text = "NRC:"
        Me.XrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFormaPago3
        '
        Me.xrlFormaPago3.CanGrow = False
        Me.xrlFormaPago3.Dpi = 100.0!
        Me.xrlFormaPago3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlFormaPago3.LocationFloat = New DevExpress.Utils.PointFloat(554.9998!, 669.0001!)
        Me.xrlFormaPago3.Name = "xrlFormaPago3"
        Me.xrlFormaPago3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFormaPago3.SizeF = New System.Drawing.SizeF(100.0833!, 9.0!)
        Me.xrlFormaPago3.StylePriority.UseFont = False
        Me.xrlFormaPago3.StylePriority.UseTextAlignment = False
        Me.xrlFormaPago3.Text = "FormaPago1"
        Me.xrlFormaPago3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlGiro3
        '
        Me.xrlGiro3.CanGrow = False
        Me.xrlGiro3.Dpi = 100.0!
        Me.xrlGiro3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlGiro3.LocationFloat = New DevExpress.Utils.PointFloat(95.99998!, 669.0!)
        Me.xrlGiro3.Name = "xrlGiro3"
        Me.xrlGiro3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlGiro3.SizeF = New System.Drawing.SizeF(369.0!, 9.0!)
        Me.xrlGiro3.StylePriority.UseFont = False
        Me.xrlGiro3.StylePriority.UseTextAlignment = False
        Me.xrlGiro3.Text = "Giro1"
        Me.xrlGiro3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel81
        '
        Me.XrLabel81.Dpi = 100.0!
        Me.XrLabel81.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel81.LocationFloat = New DevExpress.Utils.PointFloat(470.9999!, 651.0001!)
        Me.XrLabel81.Name = "XrLabel81"
        Me.XrLabel81.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel81.SizeF = New System.Drawing.SizeF(81.0!, 9.0!)
        Me.XrLabel81.StylePriority.UseFont = False
        Me.XrLabel81.StylePriority.UseTextAlignment = False
        Me.XrLabel81.Text = "NIT:"
        Me.XrLabel81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel82
        '
        Me.XrLabel82.CanGrow = False
        Me.XrLabel82.Dpi = 100.0!
        Me.XrLabel82.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel82.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 669.0!)
        Me.XrLabel82.Name = "XrLabel82"
        Me.XrLabel82.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel82.SizeF = New System.Drawing.SizeF(56.99998!, 9.0!)
        Me.XrLabel82.StylePriority.UseFont = False
        Me.XrLabel82.StylePriority.UseTextAlignment = False
        Me.XrLabel82.Text = "GIRO:"
        Me.XrLabel82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNit3
        '
        Me.xrlNit3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNit3.CanGrow = False
        Me.xrlNit3.Dpi = 100.0!
        Me.xrlNit3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlNit3.LocationFloat = New DevExpress.Utils.PointFloat(554.9999!, 651.0001!)
        Me.xrlNit3.Name = "xrlNit3"
        Me.xrlNit3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNit3.SizeF = New System.Drawing.SizeF(108.0001!, 9.0!)
        Me.xrlNit3.StylePriority.UseBorders = False
        Me.xrlNit3.StylePriority.UseFont = False
        Me.xrlNit3.StylePriority.UseTextAlignment = False
        Me.xrlNit3.Text = "Nit"
        Me.xrlNit3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDirec3
        '
        Me.xrlDirec3.CanGrow = False
        Me.xrlDirec3.Dpi = 100.0!
        Me.xrlDirec3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlDirec3.LocationFloat = New DevExpress.Utils.PointFloat(95.99998!, 660.0!)
        Me.xrlDirec3.Name = "xrlDirec3"
        Me.xrlDirec3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDirec3.SizeF = New System.Drawing.SizeF(369.0!, 9.000061!)
        Me.xrlDirec3.StylePriority.UseFont = False
        Me.xrlDirec3.StylePriority.UseTextAlignment = False
        Me.xrlDirec3.Text = "Direccion1"
        Me.xrlDirec3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFecha3
        '
        Me.xrlFecha3.Dpi = 100.0!
        Me.xrlFecha3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlFecha3.LocationFloat = New DevExpress.Utils.PointFloat(555.0!, 639.0001!)
        Me.xrlFecha3.Name = "xrlFecha3"
        Me.xrlFecha3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha3.SizeF = New System.Drawing.SizeF(75.0!, 10.37476!)
        Me.xrlFecha3.StylePriority.UseFont = False
        Me.xrlFecha3.StylePriority.UseTextAlignment = False
        Me.xrlFecha3.Text = "Fecha3"
        Me.xrlFecha3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNrc3
        '
        Me.xrlNrc3.Dpi = 100.0!
        Me.xrlNrc3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlNrc3.LocationFloat = New DevExpress.Utils.PointFloat(554.9999!, 660.0001!)
        Me.xrlNrc3.Name = "xrlNrc3"
        Me.xrlNrc3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNrc3.SizeF = New System.Drawing.SizeF(108.0833!, 9.0!)
        Me.xrlNrc3.StylePriority.UseFont = False
        Me.xrlNrc3.StylePriority.UseTextAlignment = False
        Me.xrlNrc3.Text = "Nrc1"
        Me.xrlNrc3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNombre3
        '
        Me.xrlNombre3.CanGrow = False
        Me.xrlNombre3.Dpi = 100.0!
        Me.xrlNombre3.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlNombre3.LocationFloat = New DevExpress.Utils.PointFloat(95.99998!, 651.0!)
        Me.xrlNombre3.Name = "xrlNombre3"
        Me.xrlNombre3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNombre3.SizeF = New System.Drawing.SizeF(369.0!, 9.0!)
        Me.xrlNombre3.StylePriority.UseFont = False
        Me.xrlNombre3.StylePriority.UseTextAlignment = False
        Me.xrlNombre3.Text = "Nombre1"
        Me.xrlNombre3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel88
        '
        Me.XrLabel88.Dpi = 100.0!
        Me.XrLabel88.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel88.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 651.0!)
        Me.XrLabel88.Name = "XrLabel88"
        Me.XrLabel88.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel88.SizeF = New System.Drawing.SizeF(56.99998!, 9.0!)
        Me.XrLabel88.StylePriority.UseFont = False
        Me.XrLabel88.StylePriority.UseTextAlignment = False
        Me.XrLabel88.Text = "CLIENTE:"
        Me.XrLabel88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel2
        '
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 344.4993!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(57.0!, 9.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "CLIENTE:"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNombre2
        '
        Me.xrlNombre2.CanGrow = False
        Me.xrlNombre2.Dpi = 100.0!
        Me.xrlNombre2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlNombre2.LocationFloat = New DevExpress.Utils.PointFloat(95.99999!, 344.4993!)
        Me.xrlNombre2.Name = "xrlNombre2"
        Me.xrlNombre2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNombre2.SizeF = New System.Drawing.SizeF(369.0!, 9.0!)
        Me.xrlNombre2.StylePriority.UseFont = False
        Me.xrlNombre2.StylePriority.UseTextAlignment = False
        Me.xrlNombre2.Text = "Nombre1"
        Me.xrlNombre2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNrc2
        '
        Me.xrlNrc2.Dpi = 100.0!
        Me.xrlNrc2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlNrc2.LocationFloat = New DevExpress.Utils.PointFloat(555.0!, 357.0!)
        Me.xrlNrc2.Name = "xrlNrc2"
        Me.xrlNrc2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNrc2.SizeF = New System.Drawing.SizeF(108.0833!, 10.12448!)
        Me.xrlNrc2.StylePriority.UseFont = False
        Me.xrlNrc2.StylePriority.UseTextAlignment = False
        Me.xrlNrc2.Text = "Nrc1"
        Me.xrlNrc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFecha2
        '
        Me.xrlFecha2.Dpi = 100.0!
        Me.xrlFecha2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlFecha2.LocationFloat = New DevExpress.Utils.PointFloat(554.9999!, 333.0!)
        Me.xrlFecha2.Name = "xrlFecha2"
        Me.xrlFecha2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha2.SizeF = New System.Drawing.SizeF(75.0!, 11.4993!)
        Me.xrlFecha2.StylePriority.UseFont = False
        Me.xrlFecha2.StylePriority.UseTextAlignment = False
        Me.xrlFecha2.Text = "Fecha1"
        Me.xrlFecha2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDirec2
        '
        Me.xrlDirec2.CanGrow = False
        Me.xrlDirec2.Dpi = 100.0!
        Me.xrlDirec2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlDirec2.LocationFloat = New DevExpress.Utils.PointFloat(95.99999!, 353.4993!)
        Me.xrlDirec2.Name = "xrlDirec2"
        Me.xrlDirec2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDirec2.SizeF = New System.Drawing.SizeF(369.0!, 10.12445!)
        Me.xrlDirec2.StylePriority.UseFont = False
        Me.xrlDirec2.StylePriority.UseTextAlignment = False
        Me.xrlDirec2.Text = "Direccion1"
        Me.xrlDirec2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNit2
        '
        Me.xrlNit2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNit2.CanGrow = False
        Me.xrlNit2.Dpi = 100.0!
        Me.xrlNit2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlNit2.LocationFloat = New DevExpress.Utils.PointFloat(555.0!, 344.4993!)
        Me.xrlNit2.Name = "xrlNit2"
        Me.xrlNit2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNit2.SizeF = New System.Drawing.SizeF(108.0001!, 12.0!)
        Me.xrlNit2.StylePriority.UseBorders = False
        Me.xrlNit2.StylePriority.UseFont = False
        Me.xrlNit2.StylePriority.UseTextAlignment = False
        Me.xrlNit2.Text = "Nit"
        Me.xrlNit2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel19
        '
        Me.XrLabel19.CanGrow = False
        Me.XrLabel19.Dpi = 100.0!
        Me.XrLabel19.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 366.8741!)
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.SizeF = New System.Drawing.SizeF(57.0!, 10.62518!)
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        Me.XrLabel19.Text = "GIRO:"
        Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel20
        '
        Me.XrLabel20.Dpi = 100.0!
        Me.XrLabel20.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(471.0!, 344.4993!)
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.SizeF = New System.Drawing.SizeF(81.0!, 12.0!)
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.StylePriority.UseTextAlignment = False
        Me.XrLabel20.Text = "NIT:"
        Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlGiro2
        '
        Me.xrlGiro2.CanGrow = False
        Me.xrlGiro2.Dpi = 100.0!
        Me.xrlGiro2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlGiro2.LocationFloat = New DevExpress.Utils.PointFloat(95.99999!, 366.8741!)
        Me.xrlGiro2.Name = "xrlGiro2"
        Me.xrlGiro2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlGiro2.SizeF = New System.Drawing.SizeF(369.0!, 10.62519!)
        Me.xrlGiro2.StylePriority.UseFont = False
        Me.xrlGiro2.StylePriority.UseTextAlignment = False
        Me.xrlGiro2.Text = "Giro1"
        Me.xrlGiro2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFormaPago2
        '
        Me.xrlFormaPago2.CanGrow = False
        Me.xrlFormaPago2.Dpi = 100.0!
        Me.xrlFormaPago2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlFormaPago2.LocationFloat = New DevExpress.Utils.PointFloat(554.9999!, 367.3748!)
        Me.xrlFormaPago2.Name = "xrlFormaPago2"
        Me.xrlFormaPago2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFormaPago2.SizeF = New System.Drawing.SizeF(100.0833!, 10.12445!)
        Me.xrlFormaPago2.StylePriority.UseFont = False
        Me.xrlFormaPago2.StylePriority.UseTextAlignment = False
        Me.xrlFormaPago2.Text = "FormaPago1"
        Me.xrlFormaPago2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel23
        '
        Me.XrLabel23.CanGrow = False
        Me.XrLabel23.Dpi = 100.0!
        Me.XrLabel23.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(471.0!, 357.0!)
        Me.XrLabel23.Name = "XrLabel23"
        Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel23.SizeF = New System.Drawing.SizeF(81.0!, 10.12448!)
        Me.XrLabel23.StylePriority.UseFont = False
        Me.XrLabel23.StylePriority.UseTextAlignment = False
        Me.XrLabel23.Text = "NRC:"
        Me.XrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel24
        '
        Me.XrLabel24.Dpi = 100.0!
        Me.XrLabel24.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(471.0!, 367.3748!)
        Me.XrLabel24.Name = "XrLabel24"
        Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel24.SizeF = New System.Drawing.SizeF(81.0!, 10.12445!)
        Me.XrLabel24.StylePriority.UseFont = False
        Me.XrLabel24.StylePriority.UseTextAlignment = False
        Me.XrLabel24.Text = "FORMA DE PAGO:"
        Me.XrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel25
        '
        Me.XrLabel25.CanGrow = False
        Me.XrLabel25.Dpi = 100.0!
        Me.XrLabel25.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 353.4993!)
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel25.SizeF = New System.Drawing.SizeF(57.0!, 10.12448!)
        Me.XrLabel25.StylePriority.UseFont = False
        Me.XrLabel25.StylePriority.UseTextAlignment = False
        Me.XrLabel25.Text = "DIRECCIÓN:"
        Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel26
        '
        Me.XrLabel26.CanGrow = False
        Me.XrLabel26.Dpi = 100.0!
        Me.XrLabel26.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(471.0!, 333.0!)
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.SizeF = New System.Drawing.SizeF(81.0!, 11.49933!)
        Me.XrLabel26.StylePriority.UseFont = False
        Me.XrLabel26.StylePriority.UseTextAlignment = False
        Me.XrLabel26.Text = "FECHA:"
        Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel27
        '
        Me.XrLabel27.Dpi = 100.0!
        Me.XrLabel27.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 472.25!)
        Me.XrLabel27.Name = "XrLabel27"
        Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel27.SizeF = New System.Drawing.SizeF(65.99969!, 10.75003!)
        Me.XrLabel27.StylePriority.UseFont = False
        Me.XrLabel27.StylePriority.UseTextAlignment = False
        Me.XrLabel27.Text = "IVA:"
        Me.XrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTotalExento2
        '
        Me.xrlTotalExento2.Dpi = 100.0!
        Me.xrlTotalExento2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlTotalExento2.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 502.2501!)
        Me.xrlTotalExento2.Name = "xrlTotalExento2"
        Me.xrlTotalExento2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotalExento2.SizeF = New System.Drawing.SizeF(75.0!, 10.75003!)
        Me.xrlTotalExento2.StylePriority.UseFont = False
        Me.xrlTotalExento2.StylePriority.UseTextAlignment = False
        Me.xrlTotalExento2.Text = "TotalExento"
        Me.xrlTotalExento2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlSubTotal2
        '
        Me.xrlSubTotal2.Dpi = 100.0!
        Me.xrlSubTotal2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlSubTotal2.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 482.5!)
        Me.xrlSubTotal2.Name = "xrlSubTotal2"
        Me.xrlSubTotal2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSubTotal2.SizeF = New System.Drawing.SizeF(75.0!, 10.75003!)
        Me.xrlSubTotal2.StylePriority.UseFont = False
        Me.xrlSubTotal2.StylePriority.UseTextAlignment = False
        Me.xrlSubTotal2.Text = "0.00"
        Me.xrlSubTotal2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotalAfecto2
        '
        Me.xrlTotalAfecto2.Dpi = 100.0!
        Me.xrlTotalAfecto2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlTotalAfecto2.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 461.5!)
        Me.xrlTotalAfecto2.Name = "xrlTotalAfecto2"
        Me.xrlTotalAfecto2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotalAfecto2.SizeF = New System.Drawing.SizeF(75.0!, 10.75003!)
        Me.xrlTotalAfecto2.StylePriority.UseFont = False
        Me.xrlTotalAfecto2.StylePriority.UseTextAlignment = False
        Me.xrlTotalAfecto2.Text = "TotalAfecto1"
        Me.xrlTotalAfecto2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel32
        '
        Me.XrLabel32.Dpi = 100.0!
        Me.XrLabel32.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel32.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 482.5!)
        Me.XrLabel32.Name = "XrLabel32"
        Me.XrLabel32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel32.SizeF = New System.Drawing.SizeF(65.99969!, 10.75003!)
        Me.XrLabel32.StylePriority.UseFont = False
        Me.XrLabel32.StylePriority.UseTextAlignment = False
        Me.XrLabel32.Text = "Sub-Total:"
        Me.XrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel33
        '
        Me.XrLabel33.CanGrow = False
        Me.XrLabel33.Dpi = 100.0!
        Me.XrLabel33.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel33.LocationFloat = New DevExpress.Utils.PointFloat(130.1668!, 477.4998!)
        Me.XrLabel33.Name = "XrLabel33"
        Me.XrLabel33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel33.SizeF = New System.Drawing.SizeF(204.4583!, 10.75003!)
        Me.XrLabel33.StylePriority.UseFont = False
        Me.XrLabel33.StylePriority.UseTextAlignment = False
        Me.XrLabel33.Text = "VENDEDOR"
        Me.XrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel33.Visible = False
        '
        'XrLabel34
        '
        Me.XrLabel34.CanShrink = True
        Me.XrLabel34.Dpi = 100.0!
        Me.XrLabel34.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel34.LocationFloat = New DevExpress.Utils.PointFloat(44.08044!, 466.7499!)
        Me.XrLabel34.Name = "XrLabel34"
        Me.XrLabel34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel34.SizeF = New System.Drawing.SizeF(222.2495!, 10.75003!)
        Me.XrLabel34.StylePriority.UseFont = False
        Me.XrLabel34.StylePriority.UseTextAlignment = False
        Me.XrLabel34.Text = "Comentario"
        Me.XrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel34.Visible = False
        '
        'xrlIvaRetenido2
        '
        Me.xrlIvaRetenido2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIvaRetenido2.Dpi = 100.0!
        Me.xrlIvaRetenido2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlIvaRetenido2.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 491.5!)
        Me.xrlIvaRetenido2.Name = "xrlIvaRetenido2"
        Me.xrlIvaRetenido2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIvaRetenido2.SizeF = New System.Drawing.SizeF(75.0!, 10.75006!)
        Me.xrlIvaRetenido2.StylePriority.UseBorders = False
        Me.xrlIvaRetenido2.StylePriority.UseFont = False
        Me.xrlIvaRetenido2.StylePriority.UseTextAlignment = False
        Me.xrlIvaRetenido2.Text = "0.00"
        Me.xrlIvaRetenido2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlIva2
        '
        Me.xrlIva2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIva2.Dpi = 100.0!
        Me.xrlIva2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlIva2.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 472.25!)
        Me.xrlIva2.Name = "xrlIva2"
        Me.xrlIva2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIva2.SizeF = New System.Drawing.SizeF(75.0!, 10.75003!)
        Me.xrlIva2.StylePriority.UseBorders = False
        Me.xrlIva2.StylePriority.UseFont = False
        Me.xrlIva2.StylePriority.UseTextAlignment = False
        Me.xrlIva2.Text = "Iva"
        Me.xrlIva2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel37
        '
        Me.XrLabel37.CanGrow = False
        Me.XrLabel37.Dpi = 100.0!
        Me.XrLabel37.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel37.LocationFloat = New DevExpress.Utils.PointFloat(375.0!, 477.4998!)
        Me.XrLabel37.Name = "XrLabel37"
        Me.XrLabel37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel37.SizeF = New System.Drawing.SizeF(138.0001!, 10.75003!)
        Me.XrLabel37.StylePriority.UseFont = False
        Me.XrLabel37.StylePriority.UseTextAlignment = False
        Me.XrLabel37.Text = "VENDEDOR"
        Me.XrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel37.Visible = False
        '
        'xrlCesc2
        '
        Me.xrlCesc2.Dpi = 100.0!
        Me.xrlCesc2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCesc2.LocationFloat = New DevExpress.Utils.PointFloat(468.0!, 466.4998!)
        Me.xrlCesc2.Name = "xrlCesc2"
        Me.xrlCesc2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCesc2.SizeF = New System.Drawing.SizeF(45.0!, 10.75003!)
        Me.xrlCesc2.StylePriority.UseFont = False
        Me.xrlCesc2.StylePriority.UseTextAlignment = False
        Me.xrlCesc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.xrlCesc2.Visible = False
        '
        'XrLabel39
        '
        Me.XrLabel39.Dpi = 100.0!
        Me.XrLabel39.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel39.LocationFloat = New DevExpress.Utils.PointFloat(642.0!, 461.5!)
        Me.XrLabel39.Name = "XrLabel39"
        Me.XrLabel39.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel39.SizeF = New System.Drawing.SizeF(65.99963!, 10.75003!)
        Me.XrLabel39.StylePriority.UseFont = False
        Me.XrLabel39.StylePriority.UseTextAlignment = False
        Me.XrLabel39.Text = "Sumas:"
        Me.XrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTotal2
        '
        Me.xrlTotal2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlTotal2.Dpi = 100.0!
        Me.xrlTotal2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlTotal2.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 513.0!)
        Me.xrlTotal2.Name = "xrlTotal2"
        Me.xrlTotal2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal2.SizeF = New System.Drawing.SizeF(75.0!, 10.75!)
        Me.xrlTotal2.StylePriority.UseBorders = False
        Me.xrlTotal2.StylePriority.UseFont = False
        Me.xrlTotal2.StylePriority.UseTextAlignment = False
        Me.xrlTotal2.Text = "0.00"
        Me.xrlTotal2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCantLetras2
        '
        Me.xrlCantLetras2.Dpi = 100.0!
        Me.xrlCantLetras2.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlCantLetras2.LocationFloat = New DevExpress.Utils.PointFloat(56.99999!, 507.0!)
        Me.xrlCantLetras2.Name = "xrlCantLetras2"
        Me.xrlCantLetras2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCantLetras2.SizeF = New System.Drawing.SizeF(471.0!, 10.75003!)
        Me.xrlCantLetras2.StylePriority.UseFont = False
        Me.xrlCantLetras2.StylePriority.UseTextAlignment = False
        Me.xrlCantLetras2.Text = "DIEZ"
        Me.xrlCantLetras2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel42
        '
        Me.XrLabel42.Dpi = 100.0!
        Me.XrLabel42.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel42.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 491.5!)
        Me.XrLabel42.Name = "XrLabel42"
        Me.XrLabel42.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel42.SizeF = New System.Drawing.SizeF(65.99969!, 10.75006!)
        Me.XrLabel42.StylePriority.UseFont = False
        Me.XrLabel42.StylePriority.UseTextAlignment = False
        Me.XrLabel42.Text = "IVA Retenido:"
        Me.XrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel43
        '
        Me.XrLabel43.Dpi = 100.0!
        Me.XrLabel43.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel43.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 502.2501!)
        Me.XrLabel43.Name = "XrLabel43"
        Me.XrLabel43.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel43.SizeF = New System.Drawing.SizeF(65.99969!, 10.75003!)
        Me.XrLabel43.StylePriority.UseFont = False
        Me.XrLabel43.StylePriority.UseTextAlignment = False
        Me.XrLabel43.Text = "Venta Exenta:"
        Me.XrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel44
        '
        Me.XrLabel44.Dpi = 100.0!
        Me.XrLabel44.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel44.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 513.0!)
        Me.XrLabel44.Name = "XrLabel44"
        Me.XrLabel44.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel44.SizeF = New System.Drawing.SizeF(66.00006!, 10.75!)
        Me.XrLabel44.StylePriority.UseFont = False
        Me.XrLabel44.StylePriority.UseTextAlignment = False
        Me.XrLabel44.Text = "TOTAL:"
        Me.XrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDesc21
        '
        Me.xrlDesc21.CanGrow = False
        Me.xrlDesc21.Dpi = 100.0!
        Me.xrlDesc21.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc21.LocationFloat = New DevExpress.Utils.PointFloat(54.00006!, 394.8755!)
        Me.xrlDesc21.Name = "xrlDesc21"
        Me.xrlDesc21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc21.SizeF = New System.Drawing.SizeF(450.0!, 10.12454!)
        Me.xrlDesc21.StylePriority.UseFont = False
        Me.xrlDesc21.StylePriority.UseTextAlignment = False
        Me.xrlDesc21.Text = "Desc11"
        Me.xrlDesc21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaExenta21
        '
        Me.xrlVentaExenta21.Dpi = 100.0!
        Me.xrlVentaExenta21.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta21.LocationFloat = New DevExpress.Utils.PointFloat(651.0!, 394.8755!)
        Me.xrlVentaExenta21.Name = "xrlVentaExenta21"
        Me.xrlVentaExenta21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta21.SizeF = New System.Drawing.SizeF(60.0!, 10.12457!)
        Me.xrlVentaExenta21.StylePriority.UseFont = False
        Me.xrlVentaExenta21.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta21.Text = "VentaExenta11"
        Me.xrlVentaExenta21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant21
        '
        Me.xrlCant21.Dpi = 100.0!
        Me.xrlCant21.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant21.LocationFloat = New DevExpress.Utils.PointFloat(3.000176!, 394.8755!)
        Me.xrlCant21.Name = "xrlCant21"
        Me.xrlCant21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant21.SizeF = New System.Drawing.SizeF(41.99998!, 10.12454!)
        Me.xrlCant21.StylePriority.UseFont = False
        Me.xrlCant21.StylePriority.UseTextAlignment = False
        Me.xrlCant21.Text = "Cant11"
        Me.xrlCant21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal21
        '
        Me.xrlTotal21.Dpi = 100.0!
        Me.xrlTotal21.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal21.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 394.8755!)
        Me.xrlTotal21.Name = "xrlTotal21"
        Me.xrlTotal21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal21.SizeF = New System.Drawing.SizeF(75.0!, 10.12454!)
        Me.xrlTotal21.StylePriority.UseFont = False
        Me.xrlTotal21.StylePriority.UseTextAlignment = False
        Me.xrlTotal21.Text = "Total11"
        Me.xrlTotal21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlPrecio21
        '
        Me.xrlPrecio21.Dpi = 100.0!
        Me.xrlPrecio21.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio21.LocationFloat = New DevExpress.Utils.PointFloat(543.0001!, 394.8755!)
        Me.xrlPrecio21.Name = "xrlPrecio21"
        Me.xrlPrecio21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio21.SizeF = New System.Drawing.SizeF(60.0!, 10.12454!)
        Me.xrlPrecio21.StylePriority.UseFont = False
        Me.xrlPrecio21.StylePriority.UseTextAlignment = False
        Me.xrlPrecio21.Text = "Precio11"
        Me.xrlPrecio21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel50
        '
        Me.XrLabel50.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel50.Dpi = 100.0!
        Me.XrLabel50.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel50.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 394.8755!)
        Me.XrLabel50.Name = "XrLabel50"
        Me.XrLabel50.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel50.SizeF = New System.Drawing.SizeF(39.87488!, 10.12454!)
        Me.XrLabel50.StylePriority.UseBorders = False
        Me.XrLabel50.StylePriority.UseFont = False
        Me.XrLabel50.StylePriority.UseTextAlignment = False
        Me.XrLabel50.Text = "0.00"
        Me.XrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel50.Visible = False
        '
        'XrLabel51
        '
        Me.XrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel51.Dpi = 100.0!
        Me.XrLabel51.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel51.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 405.2504!)
        Me.XrLabel51.Name = "XrLabel51"
        Me.XrLabel51.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel51.SizeF = New System.Drawing.SizeF(39.87488!, 8.749603!)
        Me.XrLabel51.StylePriority.UseBorders = False
        Me.XrLabel51.StylePriority.UseFont = False
        Me.XrLabel51.StylePriority.UseTextAlignment = False
        Me.XrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel51.Visible = False
        '
        'xrlPrecio22
        '
        Me.xrlPrecio22.Dpi = 100.0!
        Me.xrlPrecio22.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio22.LocationFloat = New DevExpress.Utils.PointFloat(543.0001!, 405.2504!)
        Me.xrlPrecio22.Name = "xrlPrecio22"
        Me.xrlPrecio22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio22.SizeF = New System.Drawing.SizeF(60.0!, 8.749603!)
        Me.xrlPrecio22.StylePriority.UseFont = False
        Me.xrlPrecio22.StylePriority.UseTextAlignment = False
        Me.xrlPrecio22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal22
        '
        Me.xrlTotal22.Dpi = 100.0!
        Me.xrlTotal22.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal22.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 405.2504!)
        Me.xrlTotal22.Name = "xrlTotal22"
        Me.xrlTotal22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal22.SizeF = New System.Drawing.SizeF(75.0!, 8.749603!)
        Me.xrlTotal22.StylePriority.UseFont = False
        Me.xrlTotal22.StylePriority.UseTextAlignment = False
        Me.xrlTotal22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant22
        '
        Me.xrlCant22.Dpi = 100.0!
        Me.xrlCant22.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant22.LocationFloat = New DevExpress.Utils.PointFloat(3.000167!, 405.2504!)
        Me.xrlCant22.Name = "xrlCant22"
        Me.xrlCant22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant22.SizeF = New System.Drawing.SizeF(41.99998!, 8.749603!)
        Me.xrlCant22.StylePriority.UseFont = False
        Me.xrlCant22.StylePriority.UseTextAlignment = False
        Me.xrlCant22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta22
        '
        Me.xrlVentaExenta22.Dpi = 100.0!
        Me.xrlVentaExenta22.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta22.LocationFloat = New DevExpress.Utils.PointFloat(651.0!, 405.2504!)
        Me.xrlVentaExenta22.Name = "xrlVentaExenta22"
        Me.xrlVentaExenta22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta22.SizeF = New System.Drawing.SizeF(60.0!, 8.749603!)
        Me.xrlVentaExenta22.StylePriority.UseFont = False
        Me.xrlVentaExenta22.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDesc22
        '
        Me.xrlDesc22.CanGrow = False
        Me.xrlDesc22.Dpi = 100.0!
        Me.xrlDesc22.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc22.LocationFloat = New DevExpress.Utils.PointFloat(54.00006!, 405.2504!)
        Me.xrlDesc22.Name = "xrlDesc22"
        Me.xrlDesc22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc22.SizeF = New System.Drawing.SizeF(450.0!, 8.749603!)
        Me.xrlDesc22.StylePriority.UseFont = False
        Me.xrlDesc22.StylePriority.UseTextAlignment = False
        Me.xrlDesc22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDesc23
        '
        Me.xrlDesc23.CanGrow = False
        Me.xrlDesc23.Dpi = 100.0!
        Me.xrlDesc23.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc23.LocationFloat = New DevExpress.Utils.PointFloat(54.00006!, 414.0!)
        Me.xrlDesc23.Name = "xrlDesc23"
        Me.xrlDesc23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc23.SizeF = New System.Drawing.SizeF(450.0!, 9.000031!)
        Me.xrlDesc23.StylePriority.UseFont = False
        Me.xrlDesc23.StylePriority.UseTextAlignment = False
        Me.xrlDesc23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaExenta23
        '
        Me.xrlVentaExenta23.Dpi = 100.0!
        Me.xrlVentaExenta23.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta23.LocationFloat = New DevExpress.Utils.PointFloat(651.0!, 414.0!)
        Me.xrlVentaExenta23.Name = "xrlVentaExenta23"
        Me.xrlVentaExenta23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta23.SizeF = New System.Drawing.SizeF(60.0!, 9.000031!)
        Me.xrlVentaExenta23.StylePriority.UseFont = False
        Me.xrlVentaExenta23.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant23
        '
        Me.xrlCant23.Dpi = 100.0!
        Me.xrlCant23.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant23.LocationFloat = New DevExpress.Utils.PointFloat(3.00014!, 414.0!)
        Me.xrlCant23.Name = "xrlCant23"
        Me.xrlCant23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant23.SizeF = New System.Drawing.SizeF(42.00001!, 9.000031!)
        Me.xrlCant23.StylePriority.UseFont = False
        Me.xrlCant23.StylePriority.UseTextAlignment = False
        Me.xrlCant23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal23
        '
        Me.xrlTotal23.Dpi = 100.0!
        Me.xrlTotal23.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal23.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 414.0!)
        Me.xrlTotal23.Name = "xrlTotal23"
        Me.xrlTotal23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal23.SizeF = New System.Drawing.SizeF(75.0!, 9.0!)
        Me.xrlTotal23.StylePriority.UseFont = False
        Me.xrlTotal23.StylePriority.UseTextAlignment = False
        Me.xrlTotal23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlPrecio23
        '
        Me.xrlPrecio23.Dpi = 100.0!
        Me.xrlPrecio23.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio23.LocationFloat = New DevExpress.Utils.PointFloat(543.0001!, 414.0!)
        Me.xrlPrecio23.Name = "xrlPrecio23"
        Me.xrlPrecio23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio23.SizeF = New System.Drawing.SizeF(60.0!, 9.000031!)
        Me.xrlPrecio23.StylePriority.UseFont = False
        Me.xrlPrecio23.StylePriority.UseTextAlignment = False
        Me.xrlPrecio23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel62
        '
        Me.XrLabel62.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel62.Dpi = 100.0!
        Me.XrLabel62.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel62.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 414.0!)
        Me.XrLabel62.Name = "XrLabel62"
        Me.XrLabel62.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel62.SizeF = New System.Drawing.SizeF(39.87488!, 9.000031!)
        Me.XrLabel62.StylePriority.UseBorders = False
        Me.XrLabel62.StylePriority.UseFont = False
        Me.XrLabel62.StylePriority.UseTextAlignment = False
        Me.XrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel62.Visible = False
        '
        'XrLine2
        '
        Me.XrLine2.Dpi = 100.0!
        Me.XrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(321.0!, 606.0!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(219.0!, 2.0!)
        Me.XrLine2.Visible = False
        '
        'XrLabel63
        '
        Me.XrLabel63.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel63.Dpi = 100.0!
        Me.XrLabel63.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel63.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 423.0!)
        Me.XrLabel63.Name = "XrLabel63"
        Me.XrLabel63.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel63.SizeF = New System.Drawing.SizeF(39.87488!, 9.0!)
        Me.XrLabel63.StylePriority.UseBorders = False
        Me.XrLabel63.StylePriority.UseFont = False
        Me.XrLabel63.StylePriority.UseTextAlignment = False
        Me.XrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel63.Visible = False
        '
        'xrlPrecio24
        '
        Me.xrlPrecio24.Dpi = 100.0!
        Me.xrlPrecio24.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio24.LocationFloat = New DevExpress.Utils.PointFloat(543.0001!, 423.0!)
        Me.xrlPrecio24.Name = "xrlPrecio24"
        Me.xrlPrecio24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio24.SizeF = New System.Drawing.SizeF(60.0!, 9.0!)
        Me.xrlPrecio24.StylePriority.UseFont = False
        Me.xrlPrecio24.StylePriority.UseTextAlignment = False
        Me.xrlPrecio24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal24
        '
        Me.xrlTotal24.Dpi = 100.0!
        Me.xrlTotal24.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal24.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 423.0!)
        Me.xrlTotal24.Name = "xrlTotal24"
        Me.xrlTotal24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal24.SizeF = New System.Drawing.SizeF(75.0!, 9.0!)
        Me.xrlTotal24.StylePriority.UseFont = False
        Me.xrlTotal24.StylePriority.UseTextAlignment = False
        Me.xrlTotal24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant24
        '
        Me.xrlCant24.Dpi = 100.0!
        Me.xrlCant24.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant24.LocationFloat = New DevExpress.Utils.PointFloat(3.000077!, 423.0!)
        Me.xrlCant24.Name = "xrlCant24"
        Me.xrlCant24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant24.SizeF = New System.Drawing.SizeF(42.00008!, 9.0!)
        Me.xrlCant24.StylePriority.UseFont = False
        Me.xrlCant24.StylePriority.UseTextAlignment = False
        Me.xrlCant24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta24
        '
        Me.xrlVentaExenta24.Dpi = 100.0!
        Me.xrlVentaExenta24.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta24.LocationFloat = New DevExpress.Utils.PointFloat(650.9999!, 423.0!)
        Me.xrlVentaExenta24.Name = "xrlVentaExenta24"
        Me.xrlVentaExenta24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta24.SizeF = New System.Drawing.SizeF(60.00012!, 9.0!)
        Me.xrlVentaExenta24.StylePriority.UseFont = False
        Me.xrlVentaExenta24.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDesc24
        '
        Me.xrlDesc24.CanGrow = False
        Me.xrlDesc24.Dpi = 100.0!
        Me.xrlDesc24.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc24.LocationFloat = New DevExpress.Utils.PointFloat(54.0!, 423.0!)
        Me.xrlDesc24.Name = "xrlDesc24"
        Me.xrlDesc24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc24.SizeF = New System.Drawing.SizeF(450.0!, 9.0!)
        Me.xrlDesc24.StylePriority.UseFont = False
        Me.xrlDesc24.StylePriority.UseTextAlignment = False
        Me.xrlDesc24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDesc14
        '
        Me.xrlDesc14.CanGrow = False
        Me.xrlDesc14.Dpi = 100.0!
        Me.xrlDesc14.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc14.LocationFloat = New DevExpress.Utils.PointFloat(53.99999!, 111.0!)
        Me.xrlDesc14.Name = "xrlDesc14"
        Me.xrlDesc14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc14.SizeF = New System.Drawing.SizeF(450.0!, 9.000008!)
        Me.xrlDesc14.StylePriority.UseFont = False
        Me.xrlDesc14.StylePriority.UseTextAlignment = False
        Me.xrlDesc14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaExenta14
        '
        Me.xrlVentaExenta14.CanGrow = False
        Me.xrlVentaExenta14.Dpi = 100.0!
        Me.xrlVentaExenta14.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta14.LocationFloat = New DevExpress.Utils.PointFloat(650.9999!, 111.0!)
        Me.xrlVentaExenta14.Name = "xrlVentaExenta14"
        Me.xrlVentaExenta14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta14.SizeF = New System.Drawing.SizeF(63.00006!, 9.000008!)
        Me.xrlVentaExenta14.StylePriority.UseFont = False
        Me.xrlVentaExenta14.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant14
        '
        Me.xrlCant14.Dpi = 100.0!
        Me.xrlCant14.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant14.LocationFloat = New DevExpress.Utils.PointFloat(3.000077!, 111.0!)
        Me.xrlCant14.Name = "xrlCant14"
        Me.xrlCant14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant14.SizeF = New System.Drawing.SizeF(42.00008!, 9.000008!)
        Me.xrlCant14.StylePriority.UseFont = False
        Me.xrlCant14.StylePriority.UseTextAlignment = False
        Me.xrlCant14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal14
        '
        Me.xrlTotal14.CanGrow = False
        Me.xrlTotal14.Dpi = 100.0!
        Me.xrlTotal14.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal14.LocationFloat = New DevExpress.Utils.PointFloat(714.0!, 111.0!)
        Me.xrlTotal14.Name = "xrlTotal14"
        Me.xrlTotal14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal14.SizeF = New System.Drawing.SizeF(72.0!, 9.000008!)
        Me.xrlTotal14.StylePriority.UseFont = False
        Me.xrlTotal14.StylePriority.UseTextAlignment = False
        Me.xrlTotal14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlPrecio14
        '
        Me.xrlPrecio14.CanGrow = False
        Me.xrlPrecio14.Dpi = 100.0!
        Me.xrlPrecio14.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio14.LocationFloat = New DevExpress.Utils.PointFloat(542.9998!, 111.0!)
        Me.xrlPrecio14.Name = "xrlPrecio14"
        Me.xrlPrecio14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio14.SizeF = New System.Drawing.SizeF(60.0!, 9.000008!)
        Me.xrlPrecio14.StylePriority.UseFont = False
        Me.xrlPrecio14.StylePriority.UseTextAlignment = False
        Me.xrlPrecio14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel29
        '
        Me.XrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel29.Dpi = 100.0!
        Me.XrLabel29.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 111.0!)
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.SizeF = New System.Drawing.SizeF(39.87488!, 9.000008!)
        Me.XrLabel29.StylePriority.UseBorders = False
        Me.XrLabel29.StylePriority.UseFont = False
        Me.XrLabel29.StylePriority.UseTextAlignment = False
        Me.XrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel29.Visible = False
        '
        'XrLine1
        '
        Me.XrLine1.Dpi = 100.0!
        Me.XrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(321.0!, 297.0!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(219.0!, 3.0!)
        Me.XrLine1.Visible = False
        '
        'XrLabel18
        '
        Me.XrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel18.Dpi = 100.0!
        Me.XrLabel18.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(609.0!, 102.0!)
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(39.87488!, 8.999992!)
        Me.XrLabel18.StylePriority.UseBorders = False
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.StylePriority.UseTextAlignment = False
        Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel18.Visible = False
        '
        'xrlPrecio13
        '
        Me.xrlPrecio13.CanGrow = False
        Me.xrlPrecio13.Dpi = 100.0!
        Me.xrlPrecio13.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio13.LocationFloat = New DevExpress.Utils.PointFloat(542.9999!, 102.0!)
        Me.xrlPrecio13.Name = "xrlPrecio13"
        Me.xrlPrecio13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio13.SizeF = New System.Drawing.SizeF(60.00006!, 8.999992!)
        Me.xrlPrecio13.StylePriority.UseFont = False
        Me.xrlPrecio13.StylePriority.UseTextAlignment = False
        Me.xrlPrecio13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal13
        '
        Me.xrlTotal13.CanGrow = False
        Me.xrlTotal13.Dpi = 100.0!
        Me.xrlTotal13.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal13.LocationFloat = New DevExpress.Utils.PointFloat(713.9999!, 102.0!)
        Me.xrlTotal13.Name = "xrlTotal13"
        Me.xrlTotal13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal13.SizeF = New System.Drawing.SizeF(72.00006!, 8.999992!)
        Me.xrlTotal13.StylePriority.UseFont = False
        Me.xrlTotal13.StylePriority.UseTextAlignment = False
        Me.xrlTotal13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant13
        '
        Me.xrlCant13.Dpi = 100.0!
        Me.xrlCant13.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant13.LocationFloat = New DevExpress.Utils.PointFloat(3.000144!, 102.0!)
        Me.xrlCant13.Name = "xrlCant13"
        Me.xrlCant13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant13.SizeF = New System.Drawing.SizeF(42.00001!, 8.999992!)
        Me.xrlCant13.StylePriority.UseFont = False
        Me.xrlCant13.StylePriority.UseTextAlignment = False
        Me.xrlCant13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta13
        '
        Me.xrlVentaExenta13.CanGrow = False
        Me.xrlVentaExenta13.Dpi = 100.0!
        Me.xrlVentaExenta13.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta13.LocationFloat = New DevExpress.Utils.PointFloat(651.0!, 102.0!)
        Me.xrlVentaExenta13.Name = "xrlVentaExenta13"
        Me.xrlVentaExenta13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta13.SizeF = New System.Drawing.SizeF(63.0!, 8.999992!)
        Me.xrlVentaExenta13.StylePriority.UseFont = False
        Me.xrlVentaExenta13.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDesc13
        '
        Me.xrlDesc13.CanGrow = False
        Me.xrlDesc13.Dpi = 100.0!
        Me.xrlDesc13.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc13.LocationFloat = New DevExpress.Utils.PointFloat(54.00006!, 102.0!)
        Me.xrlDesc13.Name = "xrlDesc13"
        Me.xrlDesc13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc13.SizeF = New System.Drawing.SizeF(450.0!, 8.999992!)
        Me.xrlDesc13.StylePriority.UseFont = False
        Me.xrlDesc13.StylePriority.UseTextAlignment = False
        Me.xrlDesc13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDesc12
        '
        Me.xrlDesc12.CanGrow = False
        Me.xrlDesc12.Dpi = 100.0!
        Me.xrlDesc12.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc12.LocationFloat = New DevExpress.Utils.PointFloat(54.00005!, 93.00002!)
        Me.xrlDesc12.Name = "xrlDesc12"
        Me.xrlDesc12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc12.SizeF = New System.Drawing.SizeF(450.0!, 8.999992!)
        Me.xrlDesc12.StylePriority.UseFont = False
        Me.xrlDesc12.StylePriority.UseTextAlignment = False
        Me.xrlDesc12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaExenta12
        '
        Me.xrlVentaExenta12.CanGrow = False
        Me.xrlVentaExenta12.Dpi = 100.0!
        Me.xrlVentaExenta12.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta12.LocationFloat = New DevExpress.Utils.PointFloat(651.0!, 93.00002!)
        Me.xrlVentaExenta12.Name = "xrlVentaExenta12"
        Me.xrlVentaExenta12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta12.SizeF = New System.Drawing.SizeF(63.0!, 8.999992!)
        Me.xrlVentaExenta12.StylePriority.UseFont = False
        Me.xrlVentaExenta12.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant12
        '
        Me.xrlCant12.Dpi = 100.0!
        Me.xrlCant12.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant12.LocationFloat = New DevExpress.Utils.PointFloat(3.000167!, 93.00002!)
        Me.xrlCant12.Name = "xrlCant12"
        Me.xrlCant12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant12.SizeF = New System.Drawing.SizeF(41.99998!, 8.999992!)
        Me.xrlCant12.StylePriority.UseFont = False
        Me.xrlCant12.StylePriority.UseTextAlignment = False
        Me.xrlCant12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal12
        '
        Me.xrlTotal12.CanGrow = False
        Me.xrlTotal12.Dpi = 100.0!
        Me.xrlTotal12.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal12.LocationFloat = New DevExpress.Utils.PointFloat(714.0!, 93.00002!)
        Me.xrlTotal12.Name = "xrlTotal12"
        Me.xrlTotal12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal12.SizeF = New System.Drawing.SizeF(72.0!, 8.999992!)
        Me.xrlTotal12.StylePriority.UseFont = False
        Me.xrlTotal12.StylePriority.UseTextAlignment = False
        Me.xrlTotal12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlPrecio12
        '
        Me.xrlPrecio12.CanGrow = False
        Me.xrlPrecio12.Dpi = 100.0!
        Me.xrlPrecio12.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio12.LocationFloat = New DevExpress.Utils.PointFloat(542.9999!, 93.0!)
        Me.xrlPrecio12.Name = "xrlPrecio12"
        Me.xrlPrecio12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio12.SizeF = New System.Drawing.SizeF(60.00006!, 8.999992!)
        Me.xrlPrecio12.StylePriority.UseFont = False
        Me.xrlPrecio12.StylePriority.UseTextAlignment = False
        Me.xrlPrecio12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel17
        '
        Me.XrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel17.Dpi = 100.0!
        Me.XrLabel17.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(609.0!, 93.00002!)
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(39.87488!, 8.999992!)
        Me.XrLabel17.StylePriority.UseBorders = False
        Me.XrLabel17.StylePriority.UseFont = False
        Me.XrLabel17.StylePriority.UseTextAlignment = False
        Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel17.Visible = False
        '
        'xrlTotal11
        '
        Me.xrlTotal11.Dpi = 100.0!
        Me.xrlTotal11.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal11.LocationFloat = New DevExpress.Utils.PointFloat(714.0!, 83.99999!)
        Me.xrlTotal11.Name = "xrlTotal11"
        Me.xrlTotal11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal11.SizeF = New System.Drawing.SizeF(72.0!, 9.000023!)
        Me.xrlTotal11.StylePriority.UseFont = False
        Me.xrlTotal11.StylePriority.UseTextAlignment = False
        Me.xrlTotal11.Text = "Total11"
        Me.xrlTotal11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta11
        '
        Me.xrlVentaExenta11.Dpi = 100.0!
        Me.xrlVentaExenta11.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta11.LocationFloat = New DevExpress.Utils.PointFloat(651.0!, 83.99999!)
        Me.xrlVentaExenta11.Name = "xrlVentaExenta11"
        Me.xrlVentaExenta11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta11.SizeF = New System.Drawing.SizeF(63.0!, 9.000023!)
        Me.xrlVentaExenta11.StylePriority.UseFont = False
        Me.xrlVentaExenta11.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta11.Text = "VentaExenta11"
        Me.xrlVentaExenta11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDesc11
        '
        Me.xrlDesc11.CanGrow = False
        Me.xrlDesc11.Dpi = 100.0!
        Me.xrlDesc11.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc11.LocationFloat = New DevExpress.Utils.PointFloat(54.00006!, 83.99999!)
        Me.xrlDesc11.Name = "xrlDesc11"
        Me.xrlDesc11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc11.SizeF = New System.Drawing.SizeF(450.0!, 9.000023!)
        Me.xrlDesc11.StylePriority.UseFont = False
        Me.xrlDesc11.StylePriority.UseTextAlignment = False
        Me.xrlDesc11.Text = "Desc11"
        Me.xrlDesc11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel10
        '
        Me.XrLabel10.Dpi = 100.0!
        Me.XrLabel10.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 199.75!)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(66.00006!, 10.75003!)
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.StylePriority.UseTextAlignment = False
        Me.XrLabel10.Text = "TOTAL:"
        Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel9
        '
        Me.XrLabel9.Dpi = 100.0!
        Me.XrLabel9.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 189.0001!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(66.00006!, 10.75003!)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.Text = "Venta Exenta:"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel8
        '
        Me.XrLabel8.Dpi = 100.0!
        Me.XrLabel8.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 178.25!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(66.00006!, 10.75003!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "IVA Retenido:"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTotal1
        '
        Me.xrlTotal1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlTotal1.Dpi = 100.0!
        Me.xrlTotal1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlTotal1.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 199.75!)
        Me.xrlTotal1.Name = "xrlTotal1"
        Me.xrlTotal1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal1.SizeF = New System.Drawing.SizeF(75.0!, 10.75003!)
        Me.xrlTotal1.StylePriority.UseBorders = False
        Me.xrlTotal1.StylePriority.UseFont = False
        Me.xrlTotal1.StylePriority.UseTextAlignment = False
        Me.xrlTotal1.Text = "0.00"
        Me.xrlTotal1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel5
        '
        Me.XrLabel5.Dpi = 100.0!
        Me.XrLabel5.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(642.0!, 148.2499!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(66.0!, 10.75002!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "Sumas:"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel7
        '
        Me.XrLabel7.Dpi = 100.0!
        Me.XrLabel7.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(641.9999!, 169.25!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(66.00006!, 10.75003!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "Sub-Total:"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTotalExento1
        '
        Me.xrlTotalExento1.Dpi = 100.0!
        Me.xrlTotalExento1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlTotalExento1.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 189.0001!)
        Me.xrlTotalExento1.Name = "xrlTotalExento1"
        Me.xrlTotalExento1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotalExento1.SizeF = New System.Drawing.SizeF(75.0!, 10.75003!)
        Me.xrlTotalExento1.StylePriority.UseFont = False
        Me.xrlTotalExento1.StylePriority.UseTextAlignment = False
        Me.xrlTotalExento1.Text = "TotalExento"
        Me.xrlTotalExento1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel6
        '
        Me.XrLabel6.Dpi = 100.0!
        Me.XrLabel6.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(642.0!, 159.0!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(66.0!, 10.75003!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.Text = "IVA:"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel4
        '
        Me.XrLabel4.CanGrow = False
        Me.XrLabel4.Dpi = 100.0!
        Me.XrLabel4.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(309.0!, 5.999995!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(108.0!, 13.37481!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "DEPARTAMENTO:"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel4.Visible = False
        Me.XrLabel4.WordWrap = False
        '
        'XrLabel3
        '
        Me.XrLabel3.CanGrow = False
        Me.XrLabel3.Dpi = 100.0!
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(426.0!, 5.999995!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(108.0!, 13.37481!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "MUNICIPIO:"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel3.Visible = False
        Me.XrLabel3.WordWrap = False
        '
        'xrlPedido
        '
        Me.xrlPedido.CanGrow = False
        Me.xrlPedido.Dpi = 100.0!
        Me.xrlPedido.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlPedido.LocationFloat = New DevExpress.Utils.PointFloat(471.0!, 32.99997!)
        Me.xrlPedido.Name = "xrlPedido"
        Me.xrlPedido.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPedido.SizeF = New System.Drawing.SizeF(81.0!, 9.0!)
        Me.xrlPedido.StylePriority.UseFont = False
        Me.xrlPedido.StylePriority.UseTextAlignment = False
        Me.xrlPedido.Text = "FECHA:"
        Me.xrlPedido.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNumFormulario
        '
        Me.xrlNumFormulario.Dpi = 100.0!
        Me.xrlNumFormulario.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlNumFormulario.LocationFloat = New DevExpress.Utils.PointFloat(471.0!, 59.99997!)
        Me.xrlNumFormulario.Name = "xrlNumFormulario"
        Me.xrlNumFormulario.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFormulario.SizeF = New System.Drawing.SizeF(81.0!, 9.0!)
        Me.xrlNumFormulario.StylePriority.UseFont = False
        Me.xrlNumFormulario.StylePriority.UseTextAlignment = False
        Me.xrlNumFormulario.Text = "FORMA DE PAGO:"
        Me.xrlNumFormulario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCodigo
        '
        Me.xrlCodigo.CanGrow = False
        Me.xrlCodigo.Dpi = 100.0!
        Me.xrlCodigo.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlCodigo.LocationFloat = New DevExpress.Utils.PointFloat(471.0!, 50.99997!)
        Me.xrlCodigo.Name = "xrlCodigo"
        Me.xrlCodigo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCodigo.SizeF = New System.Drawing.SizeF(81.0!, 9.0!)
        Me.xrlCodigo.StylePriority.UseFont = False
        Me.xrlCodigo.StylePriority.UseTextAlignment = False
        Me.xrlCodigo.Text = "NRC:"
        Me.xrlCodigo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFormaPago1
        '
        Me.xrlFormaPago1.CanGrow = False
        Me.xrlFormaPago1.Dpi = 100.0!
        Me.xrlFormaPago1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlFormaPago1.LocationFloat = New DevExpress.Utils.PointFloat(554.9999!, 59.99997!)
        Me.xrlFormaPago1.Name = "xrlFormaPago1"
        Me.xrlFormaPago1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFormaPago1.SizeF = New System.Drawing.SizeF(100.0833!, 9.0!)
        Me.xrlFormaPago1.StylePriority.UseFont = False
        Me.xrlFormaPago1.StylePriority.UseTextAlignment = False
        Me.xrlFormaPago1.Text = "FormaPago1"
        Me.xrlFormaPago1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNit1
        '
        Me.xrlNit1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNit1.CanGrow = False
        Me.xrlNit1.Dpi = 100.0!
        Me.xrlNit1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlNit1.LocationFloat = New DevExpress.Utils.PointFloat(555.0!, 41.99994!)
        Me.xrlNit1.Name = "xrlNit1"
        Me.xrlNit1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNit1.SizeF = New System.Drawing.SizeF(131.9999!, 9.0!)
        Me.xrlNit1.StylePriority.UseBorders = False
        Me.xrlNit1.StylePriority.UseFont = False
        Me.xrlNit1.StylePriority.UseTextAlignment = False
        Me.xrlNit1.Text = "Nit"
        Me.xrlNit1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNrc1
        '
        Me.xrlNrc1.Dpi = 100.0!
        Me.xrlNrc1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlNrc1.LocationFloat = New DevExpress.Utils.PointFloat(555.0!, 50.99997!)
        Me.xrlNrc1.Name = "xrlNrc1"
        Me.xrlNrc1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNrc1.SizeF = New System.Drawing.SizeF(108.0833!, 9.0!)
        Me.xrlNrc1.StylePriority.UseFont = False
        Me.xrlNrc1.StylePriority.UseTextAlignment = False
        Me.xrlNrc1.Text = "Nrc1"
        Me.xrlNrc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNombre1
        '
        Me.xrlNombre1.CanGrow = False
        Me.xrlNombre1.Dpi = 100.0!
        Me.xrlNombre1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.xrlNombre1.LocationFloat = New DevExpress.Utils.PointFloat(96.0!, 41.99998!)
        Me.xrlNombre1.Name = "xrlNombre1"
        Me.xrlNombre1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNombre1.SizeF = New System.Drawing.SizeF(369.0!, 9.0!)
        Me.xrlNombre1.StylePriority.UseFont = False
        Me.xrlNombre1.StylePriority.UseTextAlignment = False
        Me.xrlNombre1.Text = "Nombre1"
        Me.xrlNombre1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Dpi = 100.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 6.0!)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 42.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(57.0!, 9.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "CLIENTE:"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'PageFooter
        '
        Me.PageFooter.Dpi = 100.0!
        Me.PageFooter.HeightF = 10.08333!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'Detail
        '
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 0.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ReportFooter
        '
        Me.ReportFooter.Dpi = 100.0!
        Me.ReportFooter.HeightF = 0.0!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'DsFactura1
        '
        Me.DsFactura1.DataSetName = "dsFactura"
        Me.DsFactura1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'XrLabel12
        '
        Me.XrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel12.Dpi = 100.0!
        Me.XrLabel12.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 120.0!)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(39.87488!, 8.999992!)
        Me.XrLabel12.StylePriority.UseBorders = False
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel12.Visible = False
        '
        'xrlPrecio15
        '
        Me.xrlPrecio15.CanGrow = False
        Me.xrlPrecio15.Dpi = 100.0!
        Me.xrlPrecio15.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio15.LocationFloat = New DevExpress.Utils.PointFloat(542.9998!, 120.0!)
        Me.xrlPrecio15.Name = "xrlPrecio15"
        Me.xrlPrecio15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio15.SizeF = New System.Drawing.SizeF(60.00006!, 8.999992!)
        Me.xrlPrecio15.StylePriority.UseFont = False
        Me.xrlPrecio15.StylePriority.UseTextAlignment = False
        Me.xrlPrecio15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal15
        '
        Me.xrlTotal15.CanGrow = False
        Me.xrlTotal15.Dpi = 100.0!
        Me.xrlTotal15.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal15.LocationFloat = New DevExpress.Utils.PointFloat(713.9999!, 120.0!)
        Me.xrlTotal15.Name = "xrlTotal15"
        Me.xrlTotal15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal15.SizeF = New System.Drawing.SizeF(72.00012!, 8.999992!)
        Me.xrlTotal15.StylePriority.UseFont = False
        Me.xrlTotal15.StylePriority.UseTextAlignment = False
        Me.xrlTotal15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant15
        '
        Me.xrlCant15.Dpi = 100.0!
        Me.xrlCant15.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant15.LocationFloat = New DevExpress.Utils.PointFloat(3.000059!, 120.0!)
        Me.xrlCant15.Name = "xrlCant15"
        Me.xrlCant15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant15.SizeF = New System.Drawing.SizeF(41.99998!, 8.999992!)
        Me.xrlCant15.StylePriority.UseFont = False
        Me.xrlCant15.StylePriority.UseTextAlignment = False
        Me.xrlCant15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta15
        '
        Me.xrlVentaExenta15.CanGrow = False
        Me.xrlVentaExenta15.Dpi = 100.0!
        Me.xrlVentaExenta15.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta15.LocationFloat = New DevExpress.Utils.PointFloat(650.9999!, 120.0!)
        Me.xrlVentaExenta15.Name = "xrlVentaExenta15"
        Me.xrlVentaExenta15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta15.SizeF = New System.Drawing.SizeF(63.0!, 8.999992!)
        Me.xrlVentaExenta15.StylePriority.UseFont = False
        Me.xrlVentaExenta15.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDesc15
        '
        Me.xrlDesc15.CanGrow = False
        Me.xrlDesc15.Dpi = 100.0!
        Me.xrlDesc15.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc15.LocationFloat = New DevExpress.Utils.PointFloat(53.99995!, 120.0!)
        Me.xrlDesc15.Name = "xrlDesc15"
        Me.xrlDesc15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc15.SizeF = New System.Drawing.SizeF(450.0!, 8.999992!)
        Me.xrlDesc15.StylePriority.UseFont = False
        Me.xrlDesc15.StylePriority.UseTextAlignment = False
        Me.xrlDesc15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDesc16
        '
        Me.xrlDesc16.CanGrow = False
        Me.xrlDesc16.Dpi = 100.0!
        Me.xrlDesc16.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc16.LocationFloat = New DevExpress.Utils.PointFloat(53.99996!, 129.0!)
        Me.xrlDesc16.Name = "xrlDesc16"
        Me.xrlDesc16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc16.SizeF = New System.Drawing.SizeF(450.0!, 8.999992!)
        Me.xrlDesc16.StylePriority.UseFont = False
        Me.xrlDesc16.StylePriority.UseTextAlignment = False
        Me.xrlDesc16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaExenta16
        '
        Me.xrlVentaExenta16.CanGrow = False
        Me.xrlVentaExenta16.Dpi = 100.0!
        Me.xrlVentaExenta16.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta16.LocationFloat = New DevExpress.Utils.PointFloat(650.9999!, 129.0!)
        Me.xrlVentaExenta16.Name = "xrlVentaExenta16"
        Me.xrlVentaExenta16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta16.SizeF = New System.Drawing.SizeF(63.0!, 8.999992!)
        Me.xrlVentaExenta16.StylePriority.UseFont = False
        Me.xrlVentaExenta16.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant16
        '
        Me.xrlCant16.Dpi = 100.0!
        Me.xrlCant16.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant16.LocationFloat = New DevExpress.Utils.PointFloat(3.000036!, 129.0!)
        Me.xrlCant16.Name = "xrlCant16"
        Me.xrlCant16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant16.SizeF = New System.Drawing.SizeF(42.00001!, 8.999992!)
        Me.xrlCant16.StylePriority.UseFont = False
        Me.xrlCant16.StylePriority.UseTextAlignment = False
        Me.xrlCant16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal16
        '
        Me.xrlTotal16.CanGrow = False
        Me.xrlTotal16.Dpi = 100.0!
        Me.xrlTotal16.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal16.LocationFloat = New DevExpress.Utils.PointFloat(713.9998!, 129.0!)
        Me.xrlTotal16.Name = "xrlTotal16"
        Me.xrlTotal16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal16.SizeF = New System.Drawing.SizeF(72.00018!, 9.0!)
        Me.xrlTotal16.StylePriority.UseFont = False
        Me.xrlTotal16.StylePriority.UseTextAlignment = False
        Me.xrlTotal16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlPrecio16
        '
        Me.xrlPrecio16.CanGrow = False
        Me.xrlPrecio16.Dpi = 100.0!
        Me.xrlPrecio16.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio16.LocationFloat = New DevExpress.Utils.PointFloat(542.9998!, 129.0!)
        Me.xrlPrecio16.Name = "xrlPrecio16"
        Me.xrlPrecio16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio16.SizeF = New System.Drawing.SizeF(60.00006!, 8.999992!)
        Me.xrlPrecio16.StylePriority.UseFont = False
        Me.xrlPrecio16.StylePriority.UseTextAlignment = False
        Me.xrlPrecio16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel67
        '
        Me.XrLabel67.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel67.Dpi = 100.0!
        Me.XrLabel67.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel67.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 129.0!)
        Me.XrLabel67.Name = "XrLabel67"
        Me.XrLabel67.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel67.SizeF = New System.Drawing.SizeF(39.87488!, 8.999992!)
        Me.XrLabel67.StylePriority.UseBorders = False
        Me.XrLabel67.StylePriority.UseFont = False
        Me.XrLabel67.StylePriority.UseTextAlignment = False
        Me.XrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel67.Visible = False
        '
        'XrLabel68
        '
        Me.XrLabel68.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel68.Dpi = 100.0!
        Me.XrLabel68.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel68.LocationFloat = New DevExpress.Utils.PointFloat(608.9998!, 138.0!)
        Me.XrLabel68.Name = "XrLabel68"
        Me.XrLabel68.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel68.SizeF = New System.Drawing.SizeF(39.87488!, 9.000008!)
        Me.XrLabel68.StylePriority.UseBorders = False
        Me.XrLabel68.StylePriority.UseFont = False
        Me.XrLabel68.StylePriority.UseTextAlignment = False
        Me.XrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel68.Visible = False
        '
        'xrlPrecio17
        '
        Me.xrlPrecio17.CanGrow = False
        Me.xrlPrecio17.Dpi = 100.0!
        Me.xrlPrecio17.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio17.LocationFloat = New DevExpress.Utils.PointFloat(542.9997!, 138.0!)
        Me.xrlPrecio17.Name = "xrlPrecio17"
        Me.xrlPrecio17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio17.SizeF = New System.Drawing.SizeF(60.0!, 9.000008!)
        Me.xrlPrecio17.StylePriority.UseFont = False
        Me.xrlPrecio17.StylePriority.UseTextAlignment = False
        Me.xrlPrecio17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal17
        '
        Me.xrlTotal17.CanGrow = False
        Me.xrlTotal17.Dpi = 100.0!
        Me.xrlTotal17.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal17.LocationFloat = New DevExpress.Utils.PointFloat(713.9999!, 138.0!)
        Me.xrlTotal17.Name = "xrlTotal17"
        Me.xrlTotal17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal17.SizeF = New System.Drawing.SizeF(72.00012!, 9.0!)
        Me.xrlTotal17.StylePriority.UseFont = False
        Me.xrlTotal17.StylePriority.UseTextAlignment = False
        Me.xrlTotal17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant17
        '
        Me.xrlCant17.Dpi = 100.0!
        Me.xrlCant17.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant17.LocationFloat = New DevExpress.Utils.PointFloat(2.999969!, 138.0!)
        Me.xrlCant17.Name = "xrlCant17"
        Me.xrlCant17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant17.SizeF = New System.Drawing.SizeF(42.00008!, 9.000008!)
        Me.xrlCant17.StylePriority.UseFont = False
        Me.xrlCant17.StylePriority.UseTextAlignment = False
        Me.xrlCant17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta17
        '
        Me.xrlVentaExenta17.CanGrow = False
        Me.xrlVentaExenta17.Dpi = 100.0!
        Me.xrlVentaExenta17.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta17.LocationFloat = New DevExpress.Utils.PointFloat(650.9998!, 138.0!)
        Me.xrlVentaExenta17.Name = "xrlVentaExenta17"
        Me.xrlVentaExenta17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta17.SizeF = New System.Drawing.SizeF(63.00006!, 9.000008!)
        Me.xrlVentaExenta17.StylePriority.UseFont = False
        Me.xrlVentaExenta17.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDesc17
        '
        Me.xrlDesc17.CanGrow = False
        Me.xrlDesc17.Dpi = 100.0!
        Me.xrlDesc17.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc17.LocationFloat = New DevExpress.Utils.PointFloat(53.99989!, 138.0!)
        Me.xrlDesc17.Name = "xrlDesc17"
        Me.xrlDesc17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc17.SizeF = New System.Drawing.SizeF(450.0!, 9.000008!)
        Me.xrlDesc17.StylePriority.UseFont = False
        Me.xrlDesc17.StylePriority.UseTextAlignment = False
        Me.xrlDesc17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDesc27
        '
        Me.xrlDesc27.CanGrow = False
        Me.xrlDesc27.Dpi = 100.0!
        Me.xrlDesc27.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc27.LocationFloat = New DevExpress.Utils.PointFloat(54.00004!, 449.7496!)
        Me.xrlDesc27.Name = "xrlDesc27"
        Me.xrlDesc27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc27.SizeF = New System.Drawing.SizeF(450.0!, 9.0!)
        Me.xrlDesc27.StylePriority.UseFont = False
        Me.xrlDesc27.StylePriority.UseTextAlignment = False
        Me.xrlDesc27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaExenta27
        '
        Me.xrlVentaExenta27.Dpi = 100.0!
        Me.xrlVentaExenta27.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta27.LocationFloat = New DevExpress.Utils.PointFloat(650.9999!, 449.7496!)
        Me.xrlVentaExenta27.Name = "xrlVentaExenta27"
        Me.xrlVentaExenta27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta27.SizeF = New System.Drawing.SizeF(60.00012!, 9.0!)
        Me.xrlVentaExenta27.StylePriority.UseFont = False
        Me.xrlVentaExenta27.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant27
        '
        Me.xrlCant27.Dpi = 100.0!
        Me.xrlCant27.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant27.LocationFloat = New DevExpress.Utils.PointFloat(3.000113!, 449.7496!)
        Me.xrlCant27.Name = "xrlCant27"
        Me.xrlCant27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant27.SizeF = New System.Drawing.SizeF(42.00008!, 9.0!)
        Me.xrlCant27.StylePriority.UseFont = False
        Me.xrlCant27.StylePriority.UseTextAlignment = False
        Me.xrlCant27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal27
        '
        Me.xrlTotal27.Dpi = 100.0!
        Me.xrlTotal27.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal27.LocationFloat = New DevExpress.Utils.PointFloat(711.0001!, 449.7496!)
        Me.xrlTotal27.Name = "xrlTotal27"
        Me.xrlTotal27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal27.SizeF = New System.Drawing.SizeF(74.99994!, 8.999969!)
        Me.xrlTotal27.StylePriority.UseFont = False
        Me.xrlTotal27.StylePriority.UseTextAlignment = False
        Me.xrlTotal27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlPrecio27
        '
        Me.xrlPrecio27.Dpi = 100.0!
        Me.xrlPrecio27.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio27.LocationFloat = New DevExpress.Utils.PointFloat(543.0001!, 449.7496!)
        Me.xrlPrecio27.Name = "xrlPrecio27"
        Me.xrlPrecio27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio27.SizeF = New System.Drawing.SizeF(60.0!, 9.0!)
        Me.xrlPrecio27.StylePriority.UseFont = False
        Me.xrlPrecio27.StylePriority.UseTextAlignment = False
        Me.xrlPrecio27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel89
        '
        Me.XrLabel89.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel89.Dpi = 100.0!
        Me.XrLabel89.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel89.LocationFloat = New DevExpress.Utils.PointFloat(609.0!, 449.7496!)
        Me.XrLabel89.Name = "XrLabel89"
        Me.XrLabel89.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel89.SizeF = New System.Drawing.SizeF(39.87488!, 9.0!)
        Me.XrLabel89.StylePriority.UseBorders = False
        Me.XrLabel89.StylePriority.UseFont = False
        Me.XrLabel89.StylePriority.UseTextAlignment = False
        Me.XrLabel89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel89.Visible = False
        '
        'XrLabel90
        '
        Me.XrLabel90.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel90.Dpi = 100.0!
        Me.XrLabel90.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel90.LocationFloat = New DevExpress.Utils.PointFloat(609.0!, 440.7496!)
        Me.XrLabel90.Name = "XrLabel90"
        Me.XrLabel90.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel90.SizeF = New System.Drawing.SizeF(39.87488!, 9.000031!)
        Me.XrLabel90.StylePriority.UseBorders = False
        Me.XrLabel90.StylePriority.UseFont = False
        Me.XrLabel90.StylePriority.UseTextAlignment = False
        Me.XrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel90.Visible = False
        '
        'xrlPrecio26
        '
        Me.xrlPrecio26.Dpi = 100.0!
        Me.xrlPrecio26.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio26.LocationFloat = New DevExpress.Utils.PointFloat(543.0002!, 440.7496!)
        Me.xrlPrecio26.Name = "xrlPrecio26"
        Me.xrlPrecio26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio26.SizeF = New System.Drawing.SizeF(60.0!, 9.000031!)
        Me.xrlPrecio26.StylePriority.UseFont = False
        Me.xrlPrecio26.StylePriority.UseTextAlignment = False
        Me.xrlPrecio26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal26
        '
        Me.xrlTotal26.Dpi = 100.0!
        Me.xrlTotal26.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal26.LocationFloat = New DevExpress.Utils.PointFloat(711.0001!, 440.7496!)
        Me.xrlTotal26.Name = "xrlTotal26"
        Me.xrlTotal26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal26.SizeF = New System.Drawing.SizeF(74.99994!, 9.000031!)
        Me.xrlTotal26.StylePriority.UseFont = False
        Me.xrlTotal26.StylePriority.UseTextAlignment = False
        Me.xrlTotal26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant26
        '
        Me.xrlCant26.Dpi = 100.0!
        Me.xrlCant26.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant26.LocationFloat = New DevExpress.Utils.PointFloat(3.000176!, 440.7496!)
        Me.xrlCant26.Name = "xrlCant26"
        Me.xrlCant26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant26.SizeF = New System.Drawing.SizeF(42.00001!, 9.000031!)
        Me.xrlCant26.StylePriority.UseFont = False
        Me.xrlCant26.StylePriority.UseTextAlignment = False
        Me.xrlCant26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta26
        '
        Me.xrlVentaExenta26.Dpi = 100.0!
        Me.xrlVentaExenta26.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta26.LocationFloat = New DevExpress.Utils.PointFloat(651.0001!, 440.7496!)
        Me.xrlVentaExenta26.Name = "xrlVentaExenta26"
        Me.xrlVentaExenta26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta26.SizeF = New System.Drawing.SizeF(60.0!, 9.000031!)
        Me.xrlVentaExenta26.StylePriority.UseFont = False
        Me.xrlVentaExenta26.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDesc26
        '
        Me.xrlDesc26.CanGrow = False
        Me.xrlDesc26.Dpi = 100.0!
        Me.xrlDesc26.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc26.LocationFloat = New DevExpress.Utils.PointFloat(54.0001!, 440.7496!)
        Me.xrlDesc26.Name = "xrlDesc26"
        Me.xrlDesc26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc26.SizeF = New System.Drawing.SizeF(450.0!, 9.000031!)
        Me.xrlDesc26.StylePriority.UseFont = False
        Me.xrlDesc26.StylePriority.UseTextAlignment = False
        Me.xrlDesc26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDesc25
        '
        Me.xrlDesc25.CanGrow = False
        Me.xrlDesc25.Dpi = 100.0!
        Me.xrlDesc25.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc25.LocationFloat = New DevExpress.Utils.PointFloat(54.0001!, 432.0!)
        Me.xrlDesc25.Name = "xrlDesc25"
        Me.xrlDesc25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc25.SizeF = New System.Drawing.SizeF(450.0!, 8.749603!)
        Me.xrlDesc25.StylePriority.UseFont = False
        Me.xrlDesc25.StylePriority.UseTextAlignment = False
        Me.xrlDesc25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaExenta25
        '
        Me.xrlVentaExenta25.Dpi = 100.0!
        Me.xrlVentaExenta25.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta25.LocationFloat = New DevExpress.Utils.PointFloat(651.0001!, 432.0!)
        Me.xrlVentaExenta25.Name = "xrlVentaExenta25"
        Me.xrlVentaExenta25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta25.SizeF = New System.Drawing.SizeF(60.0!, 8.749603!)
        Me.xrlVentaExenta25.StylePriority.UseFont = False
        Me.xrlVentaExenta25.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant25
        '
        Me.xrlCant25.Dpi = 100.0!
        Me.xrlCant25.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant25.LocationFloat = New DevExpress.Utils.PointFloat(3.000203!, 432.0!)
        Me.xrlCant25.Name = "xrlCant25"
        Me.xrlCant25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant25.SizeF = New System.Drawing.SizeF(41.99998!, 8.749603!)
        Me.xrlCant25.StylePriority.UseFont = False
        Me.xrlCant25.StylePriority.UseTextAlignment = False
        Me.xrlCant25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal25
        '
        Me.xrlTotal25.Dpi = 100.0!
        Me.xrlTotal25.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal25.LocationFloat = New DevExpress.Utils.PointFloat(711.0001!, 432.0!)
        Me.xrlTotal25.Name = "xrlTotal25"
        Me.xrlTotal25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal25.SizeF = New System.Drawing.SizeF(74.99994!, 8.749603!)
        Me.xrlTotal25.StylePriority.UseFont = False
        Me.xrlTotal25.StylePriority.UseTextAlignment = False
        Me.xrlTotal25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlPrecio25
        '
        Me.xrlPrecio25.Dpi = 100.0!
        Me.xrlPrecio25.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio25.LocationFloat = New DevExpress.Utils.PointFloat(543.0002!, 432.0!)
        Me.xrlPrecio25.Name = "xrlPrecio25"
        Me.xrlPrecio25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio25.SizeF = New System.Drawing.SizeF(60.0!, 8.749603!)
        Me.xrlPrecio25.StylePriority.UseFont = False
        Me.xrlPrecio25.StylePriority.UseTextAlignment = False
        Me.xrlPrecio25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel101
        '
        Me.XrLabel101.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel101.Dpi = 100.0!
        Me.XrLabel101.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel101.LocationFloat = New DevExpress.Utils.PointFloat(609.0!, 432.0!)
        Me.XrLabel101.Name = "XrLabel101"
        Me.XrLabel101.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel101.SizeF = New System.Drawing.SizeF(39.87488!, 8.749603!)
        Me.XrLabel101.StylePriority.UseBorders = False
        Me.XrLabel101.StylePriority.UseFont = False
        Me.XrLabel101.StylePriority.UseTextAlignment = False
        Me.XrLabel101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel101.Visible = False
        '
        'XrLabel102
        '
        Me.XrLabel102.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel102.Dpi = 100.0!
        Me.XrLabel102.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel102.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 732.0001!)
        Me.XrLabel102.Name = "XrLabel102"
        Me.XrLabel102.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel102.SizeF = New System.Drawing.SizeF(39.87488!, 9.000061!)
        Me.XrLabel102.StylePriority.UseBorders = False
        Me.XrLabel102.StylePriority.UseFont = False
        Me.XrLabel102.StylePriority.UseTextAlignment = False
        Me.XrLabel102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel102.Visible = False
        '
        'xrlPrecio35
        '
        Me.xrlPrecio35.Dpi = 100.0!
        Me.xrlPrecio35.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio35.LocationFloat = New DevExpress.Utils.PointFloat(542.9998!, 732.0001!)
        Me.xrlPrecio35.Name = "xrlPrecio35"
        Me.xrlPrecio35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio35.SizeF = New System.Drawing.SizeF(60.0!, 9.000061!)
        Me.xrlPrecio35.StylePriority.UseFont = False
        Me.xrlPrecio35.StylePriority.UseTextAlignment = False
        Me.xrlPrecio35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal35
        '
        Me.xrlTotal35.Dpi = 100.0!
        Me.xrlTotal35.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal35.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 732.0001!)
        Me.xrlTotal35.Name = "xrlTotal35"
        Me.xrlTotal35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal35.SizeF = New System.Drawing.SizeF(75.0!, 9.000061!)
        Me.xrlTotal35.StylePriority.UseFont = False
        Me.xrlTotal35.StylePriority.UseTextAlignment = False
        Me.xrlTotal35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant35
        '
        Me.xrlCant35.Dpi = 100.0!
        Me.xrlCant35.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant35.LocationFloat = New DevExpress.Utils.PointFloat(3.000062!, 732.0001!)
        Me.xrlCant35.Name = "xrlCant35"
        Me.xrlCant35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant35.SizeF = New System.Drawing.SizeF(45.00001!, 9.000061!)
        Me.xrlCant35.StylePriority.UseFont = False
        Me.xrlCant35.StylePriority.UseTextAlignment = False
        Me.xrlCant35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta35
        '
        Me.xrlVentaExenta35.Dpi = 100.0!
        Me.xrlVentaExenta35.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta35.LocationFloat = New DevExpress.Utils.PointFloat(647.9999!, 732.0001!)
        Me.xrlVentaExenta35.Name = "xrlVentaExenta35"
        Me.xrlVentaExenta35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta35.SizeF = New System.Drawing.SizeF(63.00006!, 9.000061!)
        Me.xrlVentaExenta35.StylePriority.UseFont = False
        Me.xrlVentaExenta35.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDesc35
        '
        Me.xrlDesc35.CanGrow = False
        Me.xrlDesc35.Dpi = 100.0!
        Me.xrlDesc35.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc35.LocationFloat = New DevExpress.Utils.PointFloat(60.0!, 732.0001!)
        Me.xrlDesc35.Name = "xrlDesc35"
        Me.xrlDesc35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc35.SizeF = New System.Drawing.SizeF(450.0!, 9.000061!)
        Me.xrlDesc35.StylePriority.UseFont = False
        Me.xrlDesc35.StylePriority.UseTextAlignment = False
        Me.xrlDesc35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDesc36
        '
        Me.xrlDesc36.CanGrow = False
        Me.xrlDesc36.Dpi = 100.0!
        Me.xrlDesc36.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc36.LocationFloat = New DevExpress.Utils.PointFloat(60.0!, 741.0002!)
        Me.xrlDesc36.Name = "xrlDesc36"
        Me.xrlDesc36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc36.SizeF = New System.Drawing.SizeF(450.0!, 9.0!)
        Me.xrlDesc36.StylePriority.UseFont = False
        Me.xrlDesc36.StylePriority.UseTextAlignment = False
        Me.xrlDesc36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaExenta36
        '
        Me.xrlVentaExenta36.Dpi = 100.0!
        Me.xrlVentaExenta36.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta36.LocationFloat = New DevExpress.Utils.PointFloat(647.9999!, 741.0002!)
        Me.xrlVentaExenta36.Name = "xrlVentaExenta36"
        Me.xrlVentaExenta36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta36.SizeF = New System.Drawing.SizeF(63.00006!, 9.0!)
        Me.xrlVentaExenta36.StylePriority.UseFont = False
        Me.xrlVentaExenta36.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant36
        '
        Me.xrlCant36.Dpi = 100.0!
        Me.xrlCant36.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant36.LocationFloat = New DevExpress.Utils.PointFloat(3.000044!, 741.0002!)
        Me.xrlCant36.Name = "xrlCant36"
        Me.xrlCant36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant36.SizeF = New System.Drawing.SizeF(45.00003!, 9.0!)
        Me.xrlCant36.StylePriority.UseFont = False
        Me.xrlCant36.StylePriority.UseTextAlignment = False
        Me.xrlCant36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal36
        '
        Me.xrlTotal36.Dpi = 100.0!
        Me.xrlTotal36.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal36.LocationFloat = New DevExpress.Utils.PointFloat(710.9999!, 741.0002!)
        Me.xrlTotal36.Name = "xrlTotal36"
        Me.xrlTotal36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal36.SizeF = New System.Drawing.SizeF(75.00006!, 9.0!)
        Me.xrlTotal36.StylePriority.UseFont = False
        Me.xrlTotal36.StylePriority.UseTextAlignment = False
        Me.xrlTotal36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlPrecio36
        '
        Me.xrlPrecio36.Dpi = 100.0!
        Me.xrlPrecio36.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio36.LocationFloat = New DevExpress.Utils.PointFloat(542.9998!, 741.0002!)
        Me.xrlPrecio36.Name = "xrlPrecio36"
        Me.xrlPrecio36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio36.SizeF = New System.Drawing.SizeF(60.0!, 9.0!)
        Me.xrlPrecio36.StylePriority.UseFont = False
        Me.xrlPrecio36.StylePriority.UseTextAlignment = False
        Me.xrlPrecio36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel113
        '
        Me.XrLabel113.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel113.Dpi = 100.0!
        Me.XrLabel113.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel113.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 741.0002!)
        Me.XrLabel113.Name = "XrLabel113"
        Me.XrLabel113.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel113.SizeF = New System.Drawing.SizeF(39.87488!, 9.0!)
        Me.XrLabel113.StylePriority.UseBorders = False
        Me.XrLabel113.StylePriority.UseFont = False
        Me.XrLabel113.StylePriority.UseTextAlignment = False
        Me.XrLabel113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel113.Visible = False
        '
        'XrLabel114
        '
        Me.XrLabel114.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel114.Dpi = 100.0!
        Me.XrLabel114.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.XrLabel114.LocationFloat = New DevExpress.Utils.PointFloat(608.9999!, 750.0002!)
        Me.XrLabel114.Name = "XrLabel114"
        Me.XrLabel114.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel114.SizeF = New System.Drawing.SizeF(39.87488!, 9.0!)
        Me.XrLabel114.StylePriority.UseBorders = False
        Me.XrLabel114.StylePriority.UseFont = False
        Me.XrLabel114.StylePriority.UseTextAlignment = False
        Me.XrLabel114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel114.Visible = False
        '
        'xrlPrecio37
        '
        Me.xrlPrecio37.Dpi = 100.0!
        Me.xrlPrecio37.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlPrecio37.LocationFloat = New DevExpress.Utils.PointFloat(542.9998!, 750.0002!)
        Me.xrlPrecio37.Name = "xrlPrecio37"
        Me.xrlPrecio37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPrecio37.SizeF = New System.Drawing.SizeF(60.0!, 9.000061!)
        Me.xrlPrecio37.StylePriority.UseFont = False
        Me.xrlPrecio37.StylePriority.UseTextAlignment = False
        Me.xrlPrecio37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal37
        '
        Me.xrlTotal37.Dpi = 100.0!
        Me.xrlTotal37.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlTotal37.LocationFloat = New DevExpress.Utils.PointFloat(711.0!, 750.0002!)
        Me.xrlTotal37.Name = "xrlTotal37"
        Me.xrlTotal37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal37.SizeF = New System.Drawing.SizeF(75.0!, 9.000061!)
        Me.xrlTotal37.StylePriority.UseFont = False
        Me.xrlTotal37.StylePriority.UseTextAlignment = False
        Me.xrlTotal37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCant37
        '
        Me.xrlCant37.Dpi = 100.0!
        Me.xrlCant37.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlCant37.LocationFloat = New DevExpress.Utils.PointFloat(2.999977!, 750.0002!)
        Me.xrlCant37.Name = "xrlCant37"
        Me.xrlCant37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCant37.SizeF = New System.Drawing.SizeF(45.0001!, 9.0!)
        Me.xrlCant37.StylePriority.UseFont = False
        Me.xrlCant37.StylePriority.UseTextAlignment = False
        Me.xrlCant37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta37
        '
        Me.xrlVentaExenta37.Dpi = 100.0!
        Me.xrlVentaExenta37.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlVentaExenta37.LocationFloat = New DevExpress.Utils.PointFloat(647.9998!, 750.0002!)
        Me.xrlVentaExenta37.Name = "xrlVentaExenta37"
        Me.xrlVentaExenta37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta37.SizeF = New System.Drawing.SizeF(63.00018!, 9.000061!)
        Me.xrlVentaExenta37.StylePriority.UseFont = False
        Me.xrlVentaExenta37.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDesc37
        '
        Me.xrlDesc37.CanGrow = False
        Me.xrlDesc37.Dpi = 100.0!
        Me.xrlDesc37.Font = New System.Drawing.Font("Arial", 5.5!)
        Me.xrlDesc37.LocationFloat = New DevExpress.Utils.PointFloat(59.99994!, 750.0002!)
        Me.xrlDesc37.Name = "xrlDesc37"
        Me.xrlDesc37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDesc37.SizeF = New System.Drawing.SizeF(450.0!, 9.000061!)
        Me.xrlDesc37.StylePriority.UseFont = False
        Me.xrlDesc37.StylePriority.UseTextAlignment = False
        Me.xrlDesc37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'fac_rptCCFGS1
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
        Me.DataMember = "Factura"
        Me.DataSource = Me.DsFactura1
        Me.DesignerOptions.ShowExportWarnings = False
        Me.DesignerOptions.ShowPrintingWarnings = False
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(20, 20, 16, 23)
        Me.PageHeight = 1300
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.SnapGridSize = 3.0!
        Me.SnappingMode = CType((DevExpress.XtraReports.UI.SnappingMode.SnapLines Or DevExpress.XtraReports.UI.SnappingMode.SnapToGrid), DevExpress.XtraReports.UI.SnappingMode)
        Me.Version = "16.2"
        CType(Me.DsFactura1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents xrlIva1 As XRLabel
    Friend WithEvents xrlIvaRetenido As XRLabel
    Friend WithEvents xrlFecha1 As XRLabel
    Friend WithEvents xrlDocAfectados As XRLabel
    Friend WithEvents xrlPrecio11 As XRLabel
    Friend WithEvents xrlVentaNoSujeta As XRLabel
    Friend WithEvents xrlDirec1 As XRLabel
    Friend WithEvents xrlDiasCredito As XRLabel
    Friend WithEvents xrlCant11 As XRLabel
    Friend WithEvents xrlDepto As XRLabel
    Friend WithEvents xrlComentario As XRLabel
    Friend WithEvents xrlTelefono As XRLabel
    Friend WithEvents xrlVentaACuenta As XRLabel
    Friend WithEvents TopMarginBand1 As TopMarginBand
    Friend WithEvents xrlCantLetras1 As XRLabel
    Friend WithEvents BottomMarginBand1 As BottomMarginBand
    Friend WithEvents xrlNumero As XRLabel
    Friend WithEvents xrlMunic As XRLabel
    Friend WithEvents xrlSubTotal1 As XRLabel
    Friend WithEvents xrlTotalAfecto1 As XRLabel
    Friend WithEvents xrlVendedor As XRLabel
    Friend WithEvents xrlCesc1 As XRLabel
    Friend WithEvents xrlGiro1 As XRLabel
    Friend WithEvents PageHeader As PageHeaderBand
    Friend WithEvents xrlPedido As XRLabel
    Friend WithEvents xrlNumFormulario As XRLabel
    Friend WithEvents xrlCodigo As XRLabel
    Friend WithEvents xrlFormaPago1 As XRLabel
    Friend WithEvents xrlNit1 As XRLabel
    Friend WithEvents xrlNrc1 As XRLabel
    Friend WithEvents xrlNombre1 As XRLabel
    Friend WithEvents XrLabel1 As XRLabel
    Friend WithEvents xrlVentaExenta11 As XRLabel
    Friend WithEvents PageFooter As PageFooterBand
    Friend WithEvents xrlTotal1 As XRLabel
    Friend WithEvents xrlTotalExento1 As XRLabel
    Friend WithEvents xrlDesc11 As XRLabel
    Friend WithEvents xrlTotal11 As XRLabel
    Friend WithEvents Detail As DetailBand
    Friend WithEvents XrLabel4 As XRLabel
    Friend WithEvents XrLabel3 As XRLabel
    Friend WithEvents XrLabel10 As XRLabel
    Friend WithEvents XrLabel9 As XRLabel
    Friend WithEvents XrLabel8 As XRLabel
    Friend WithEvents XrLabel7 As XRLabel
    Friend WithEvents XrLabel6 As XRLabel
    Friend WithEvents XrLabel5 As XRLabel
    Friend WithEvents ReportFooter As ReportFooterBand
    Friend WithEvents DsFactura1 As dsFactura
    Friend WithEvents xrlDesc14 As XRLabel
    Friend WithEvents xrlVentaExenta14 As XRLabel
    Friend WithEvents xrlCant14 As XRLabel
    Friend WithEvents xrlTotal14 As XRLabel
    Friend WithEvents xrlPrecio14 As XRLabel
    Friend WithEvents XrLabel29 As XRLabel
    Friend WithEvents XrLine1 As XRLine
    Friend WithEvents XrLabel18 As XRLabel
    Friend WithEvents xrlPrecio13 As XRLabel
    Friend WithEvents xrlTotal13 As XRLabel
    Friend WithEvents xrlCant13 As XRLabel
    Friend WithEvents xrlVentaExenta13 As XRLabel
    Friend WithEvents xrlDesc13 As XRLabel
    Friend WithEvents xrlDesc12 As XRLabel
    Friend WithEvents xrlVentaExenta12 As XRLabel
    Friend WithEvents xrlCant12 As XRLabel
    Friend WithEvents xrlTotal12 As XRLabel
    Friend WithEvents xrlPrecio12 As XRLabel
    Friend WithEvents XrLabel17 As XRLabel
    Friend WithEvents XrLabel2 As XRLabel
    Friend WithEvents xrlNombre2 As XRLabel
    Friend WithEvents xrlNrc2 As XRLabel
    Friend WithEvents xrlFecha2 As XRLabel
    Friend WithEvents xrlDirec2 As XRLabel
    Friend WithEvents xrlNit2 As XRLabel
    Friend WithEvents XrLabel19 As XRLabel
    Friend WithEvents XrLabel20 As XRLabel
    Friend WithEvents xrlGiro2 As XRLabel
    Friend WithEvents xrlFormaPago2 As XRLabel
    Friend WithEvents XrLabel23 As XRLabel
    Friend WithEvents XrLabel24 As XRLabel
    Friend WithEvents XrLabel25 As XRLabel
    Friend WithEvents XrLabel26 As XRLabel
    Friend WithEvents XrLabel27 As XRLabel
    Friend WithEvents xrlTotalExento2 As XRLabel
    Friend WithEvents xrlSubTotal2 As XRLabel
    Friend WithEvents xrlTotalAfecto2 As XRLabel
    Friend WithEvents XrLabel32 As XRLabel
    Friend WithEvents XrLabel33 As XRLabel
    Friend WithEvents XrLabel34 As XRLabel
    Friend WithEvents xrlIvaRetenido2 As XRLabel
    Friend WithEvents xrlIva2 As XRLabel
    Friend WithEvents XrLabel37 As XRLabel
    Friend WithEvents xrlCesc2 As XRLabel
    Friend WithEvents XrLabel39 As XRLabel
    Friend WithEvents xrlTotal2 As XRLabel
    Friend WithEvents xrlCantLetras2 As XRLabel
    Friend WithEvents XrLabel42 As XRLabel
    Friend WithEvents XrLabel43 As XRLabel
    Friend WithEvents XrLabel44 As XRLabel
    Friend WithEvents xrlDesc21 As XRLabel
    Friend WithEvents xrlVentaExenta21 As XRLabel
    Friend WithEvents xrlCant21 As XRLabel
    Friend WithEvents xrlTotal21 As XRLabel
    Friend WithEvents xrlPrecio21 As XRLabel
    Friend WithEvents XrLabel50 As XRLabel
    Friend WithEvents XrLabel51 As XRLabel
    Friend WithEvents xrlPrecio22 As XRLabel
    Friend WithEvents xrlTotal22 As XRLabel
    Friend WithEvents xrlCant22 As XRLabel
    Friend WithEvents xrlVentaExenta22 As XRLabel
    Friend WithEvents xrlDesc22 As XRLabel
    Friend WithEvents xrlDesc23 As XRLabel
    Friend WithEvents xrlVentaExenta23 As XRLabel
    Friend WithEvents xrlCant23 As XRLabel
    Friend WithEvents xrlTotal23 As XRLabel
    Friend WithEvents xrlPrecio23 As XRLabel
    Friend WithEvents XrLabel62 As XRLabel
    Friend WithEvents XrLine2 As XRLine
    Friend WithEvents XrLabel63 As XRLabel
    Friend WithEvents xrlPrecio24 As XRLabel
    Friend WithEvents xrlTotal24 As XRLabel
    Friend WithEvents xrlCant24 As XRLabel
    Friend WithEvents xrlVentaExenta24 As XRLabel
    Friend WithEvents xrlDesc24 As XRLabel
    Friend WithEvents xrlDesc34 As XRLabel
    Friend WithEvents xrlVentaExenta34 As XRLabel
    Friend WithEvents xrlCant34 As XRLabel
    Friend WithEvents xrlTotal34 As XRLabel
    Friend WithEvents xrlPrecio34 As XRLabel
    Friend WithEvents XrLabel21 As XRLabel
    Friend WithEvents XrLine3 As XRLine
    Friend WithEvents XrLabel22 As XRLabel
    Friend WithEvents xrlPrecio33 As XRLabel
    Friend WithEvents xrlTotal33 As XRLabel
    Friend WithEvents xrlCant33 As XRLabel
    Friend WithEvents xrlVentaExenta33 As XRLabel
    Friend WithEvents xrlDesc33 As XRLabel
    Friend WithEvents xrlDesc32 As XRLabel
    Friend WithEvents xrlVentaExenta32 As XRLabel
    Friend WithEvents xrlCant32 As XRLabel
    Friend WithEvents xrlTotal32 As XRLabel
    Friend WithEvents xrlPrecio32 As XRLabel
    Friend WithEvents XrLabel47 As XRLabel
    Friend WithEvents XrLabel48 As XRLabel
    Friend WithEvents xrlPrecio31 As XRLabel
    Friend WithEvents xrlTotal31 As XRLabel
    Friend WithEvents xrlCant31 As XRLabel
    Friend WithEvents xrlVentaExenta31 As XRLabel
    Friend WithEvents xrlDesc31 As XRLabel
    Friend WithEvents XrLabel56 As XRLabel
    Friend WithEvents XrLabel57 As XRLabel
    Friend WithEvents XrLabel58 As XRLabel
    Friend WithEvents xrlCantLetras3 As XRLabel
    Friend WithEvents xrlTotal3 As XRLabel
    Friend WithEvents XrLabel61 As XRLabel
    Friend WithEvents xrlCesc3 As XRLabel
    Friend WithEvents XrLabel65 As XRLabel
    Friend WithEvents xrlIva3 As XRLabel
    Friend WithEvents xrlIvaRetenido3 As XRLabel
    Friend WithEvents xrlComent3 As XRLabel
    Friend WithEvents XrLabel69 As XRLabel
    Friend WithEvents XrLabel70 As XRLabel
    Friend WithEvents xrlTotalAfecto3 As XRLabel
    Friend WithEvents xrlSubTotal3 As XRLabel
    Friend WithEvents xrlTotalExento3 As XRLabel
    Friend WithEvents XrLabel74 As XRLabel
    Friend WithEvents XrLabel75 As XRLabel
    Friend WithEvents XrLabel76 As XRLabel
    Friend WithEvents XrLabel77 As XRLabel
    Friend WithEvents XrLabel78 As XRLabel
    Friend WithEvents xrlFormaPago3 As XRLabel
    Friend WithEvents xrlGiro3 As XRLabel
    Friend WithEvents XrLabel81 As XRLabel
    Friend WithEvents XrLabel82 As XRLabel
    Friend WithEvents xrlNit3 As XRLabel
    Friend WithEvents xrlDirec3 As XRLabel
    Friend WithEvents xrlFecha3 As XRLabel
    Friend WithEvents xrlNrc3 As XRLabel
    Friend WithEvents xrlNombre3 As XRLabel
    Friend WithEvents XrLabel88 As XRLabel
    Friend WithEvents xrlDoc1 As XRLabel
    Friend WithEvents xrlClient1 As XRLabel
    Friend WithEvents xrlImp1 As XRLabel
    Friend WithEvents xrlSub1 As XRLabel
    Friend WithEvents XrLabel30 As XRLabel
    Friend WithEvents XrLabel28 As XRLabel
    Friend WithEvents XrLabel16 As XRLabel
    Friend WithEvents XrLabel11 As XRLabel
    Friend WithEvents XrLabel15 As XRLabel
    Friend WithEvents XrLabel14 As XRLabel
    Friend WithEvents XrLabel13 As XRLabel
    Friend WithEvents xrlFec1 As XRLabel
    Friend WithEvents xrlTot1 As XRLabel
    Friend WithEvents xrlClient2 As XRLabel
    Friend WithEvents xrlDoc2 As XRLabel
    Friend WithEvents xrlFec2 As XRLabel
    Friend WithEvents XrLabel38 As XRLabel
    Friend WithEvents XrLabel40 As XRLabel
    Friend WithEvents XrLabel41 As XRLabel
    Friend WithEvents XrLabel45 As XRLabel
    Friend WithEvents XrLabel46 As XRLabel
    Friend WithEvents XrLabel49 As XRLabel
    Friend WithEvents XrLabel52 As XRLabel
    Friend WithEvents xrlSub2 As XRLabel
    Friend WithEvents xrlImp2 As XRLabel
    Friend WithEvents xrlTot2 As XRLabel
    Friend WithEvents xrlArt2 As XRLabel
    Friend WithEvents xrlArt1 As XRLabel
    Friend WithEvents XrLabel102 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPrecio35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCant35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDesc35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDesc36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCant36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPrecio36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel113 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel114 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPrecio37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCant37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDesc37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDesc27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCant27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPrecio27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel89 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel90 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPrecio26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCant26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDesc26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDesc25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCant25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPrecio25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel101 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPrecio15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCant15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDesc15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDesc16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCant16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPrecio16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel67 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel68 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPrecio17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCant17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDesc17 As DevExpress.XtraReports.UI.XRLabel
End Class
