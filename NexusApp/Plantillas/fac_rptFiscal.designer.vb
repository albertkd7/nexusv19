﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class fac_rptFiscal
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xrlDireccion = New DevExpress.XtraReports.UI.XRLabel()
        Me.FechaVence4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.xrlDiasCredito = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlIva = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlIvaRetenido = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlComentario = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCantLetras = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaAfecta = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSubTotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVendedor = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumero = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNombre = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCodigo = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Factura2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.LecturaAnterior2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ConsumoM32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.LecturaActual2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlGiro = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFormaPago = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.xrlVentaNoSujeta = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaACuenta = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDepto = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNit = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.xrlFechaImpresion = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPedido = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDocAfectados = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSucursal = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumRemision = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFormulario = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTelefono = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlMunic = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFecha = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNrc = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.DsFactura1 = New Nexus.dsFactura()
        Me.xrlCESC = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me.DsFactura1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xrlDireccion
        '
        Me.xrlDireccion.CanGrow = False
        Me.xrlDireccion.Dpi = 100.0!
        Me.xrlDireccion.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlDireccion.LocationFloat = New DevExpress.Utils.PointFloat(452.0414!, 223.25!)
        Me.xrlDireccion.Name = "xrlDireccion"
        Me.xrlDireccion.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDireccion.SizeF = New System.Drawing.SizeF(214.9581!, 15.00009!)
        Me.xrlDireccion.StylePriority.UseFont = False
        Me.xrlDireccion.StylePriority.UseTextAlignment = False
        Me.xrlDireccion.Text = "RESIDENCIAL CLAUDIA PASAJE AIDA No.19"
        Me.xrlDireccion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'FechaVence4
        '
        Me.FechaVence4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.VentaExenta", "{0:n2}")})
        Me.FechaVence4.Dpi = 100.0!
        Me.FechaVence4.Font = New System.Drawing.Font("Arial", 9.25!)
        Me.FechaVence4.LocationFloat = New DevExpress.Utils.PointFloat(625.4999!, 2.0!)
        Me.FechaVence4.Name = "FechaVence4"
        Me.FechaVence4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.FechaVence4.SizeF = New System.Drawing.SizeF(34.7085!, 13.37481!)
        Me.FechaVence4.StylePriority.UseFont = False
        Me.FechaVence4.StylePriority.UseTextAlignment = False
        Me.FechaVence4.Text = "0.00"
        Me.FechaVence4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlCESC, Me.xrlDiasCredito, Me.xrlIva, Me.xrlIvaRetenido, Me.xrlComentario, Me.xrlCantLetras, Me.xrlTotal, Me.xrlVentaAfecta, Me.xrlSubTotal, Me.xrlVentaExenta, Me.xrlVendedor})
        Me.PageFooter.Dpi = 100.0!
        Me.PageFooter.HeightF = 92.24997!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDiasCredito
        '
        Me.xrlDiasCredito.CanGrow = False
        Me.xrlDiasCredito.Dpi = 100.0!
        Me.xrlDiasCredito.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlDiasCredito.LocationFloat = New DevExpress.Utils.PointFloat(118.1667!, 59.24991!)
        Me.xrlDiasCredito.Name = "xrlDiasCredito"
        Me.xrlDiasCredito.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDiasCredito.SizeF = New System.Drawing.SizeF(204.4583!, 17.00001!)
        Me.xrlDiasCredito.StylePriority.UseFont = False
        Me.xrlDiasCredito.StylePriority.UseTextAlignment = False
        Me.xrlDiasCredito.Text = "VENDEDOR"
        Me.xrlDiasCredito.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlDiasCredito.Visible = False
        '
        'xrlIva
        '
        Me.xrlIva.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIva.Dpi = 100.0!
        Me.xrlIva.Font = New System.Drawing.Font("Arial", 9.5!)
        Me.xrlIva.LocationFloat = New DevExpress.Utils.PointFloat(689.2084!, 19.24999!)
        Me.xrlIva.Name = "xrlIva"
        Me.xrlIva.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIva.SizeF = New System.Drawing.SizeF(44.99997!, 11.99998!)
        Me.xrlIva.StylePriority.UseBorders = False
        Me.xrlIva.StylePriority.UseFont = False
        Me.xrlIva.StylePriority.UseTextAlignment = False
        Me.xrlIva.Text = "0.00"
        Me.xrlIva.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlIvaRetenido
        '
        Me.xrlIvaRetenido.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIvaRetenido.Dpi = 100.0!
        Me.xrlIvaRetenido.Font = New System.Drawing.Font("Arial", 9.5!)
        Me.xrlIvaRetenido.LocationFloat = New DevExpress.Utils.PointFloat(689.2084!, 45.24996!)
        Me.xrlIvaRetenido.Name = "xrlIvaRetenido"
        Me.xrlIvaRetenido.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIvaRetenido.SizeF = New System.Drawing.SizeF(44.99997!, 11.99998!)
        Me.xrlIvaRetenido.StylePriority.UseBorders = False
        Me.xrlIvaRetenido.StylePriority.UseFont = False
        Me.xrlIvaRetenido.StylePriority.UseTextAlignment = False
        Me.xrlIvaRetenido.Text = "0.00"
        Me.xrlIvaRetenido.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlComentario
        '
        Me.xrlComentario.CanShrink = True
        Me.xrlComentario.Dpi = 100.0!
        Me.xrlComentario.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlComentario.LocationFloat = New DevExpress.Utils.PointFloat(85.875!, 20.25!)
        Me.xrlComentario.Name = "xrlComentario"
        Me.xrlComentario.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlComentario.SizeF = New System.Drawing.SizeF(222.2495!, 17.00001!)
        Me.xrlComentario.StylePriority.UseFont = False
        Me.xrlComentario.StylePriority.UseTextAlignment = False
        Me.xrlComentario.Text = "Comentario"
        Me.xrlComentario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlComentario.Visible = False
        '
        'xrlCantLetras
        '
        Me.xrlCantLetras.Dpi = 100.0!
        Me.xrlCantLetras.Font = New System.Drawing.Font("Arial", 9.5!)
        Me.xrlCantLetras.LocationFloat = New DevExpress.Utils.PointFloat(382.4583!, 15.24998!)
        Me.xrlCantLetras.Name = "xrlCantLetras"
        Me.xrlCantLetras.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCantLetras.SizeF = New System.Drawing.SizeF(221.3746!, 17.00001!)
        Me.xrlCantLetras.StylePriority.UseFont = False
        Me.xrlCantLetras.StylePriority.UseTextAlignment = False
        Me.xrlCantLetras.Text = "DIEZ"
        Me.xrlCantLetras.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlTotal
        '
        Me.xrlTotal.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlTotal.Dpi = 100.0!
        Me.xrlTotal.Font = New System.Drawing.Font("Arial", 9.5!)
        Me.xrlTotal.LocationFloat = New DevExpress.Utils.PointFloat(689.2085!, 77.24999!)
        Me.xrlTotal.Name = "xrlTotal"
        Me.xrlTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal.SizeF = New System.Drawing.SizeF(44.99997!, 12.99998!)
        Me.xrlTotal.StylePriority.UseBorders = False
        Me.xrlTotal.StylePriority.UseFont = False
        Me.xrlTotal.StylePriority.UseTextAlignment = False
        Me.xrlTotal.Text = "0.00"
        Me.xrlTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaAfecta
        '
        Me.xrlVentaAfecta.Dpi = 100.0!
        Me.xrlVentaAfecta.Font = New System.Drawing.Font("Arial", 9.5!)
        Me.xrlVentaAfecta.LocationFloat = New DevExpress.Utils.PointFloat(689.2084!, 6.0!)
        Me.xrlVentaAfecta.Name = "xrlVentaAfecta"
        Me.xrlVentaAfecta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaAfecta.SizeF = New System.Drawing.SizeF(44.99997!, 12.875!)
        Me.xrlVentaAfecta.StylePriority.UseFont = False
        Me.xrlVentaAfecta.StylePriority.UseTextAlignment = False
        Me.xrlVentaAfecta.Text = "0.00"
        Me.xrlVentaAfecta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlSubTotal
        '
        Me.xrlSubTotal.Dpi = 100.0!
        Me.xrlSubTotal.Font = New System.Drawing.Font("Arial", 9.5!)
        Me.xrlSubTotal.LocationFloat = New DevExpress.Utils.PointFloat(689.2084!, 32.24997!)
        Me.xrlSubTotal.Name = "xrlSubTotal"
        Me.xrlSubTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSubTotal.SizeF = New System.Drawing.SizeF(44.99997!, 13.0!)
        Me.xrlSubTotal.StylePriority.UseFont = False
        Me.xrlSubTotal.StylePriority.UseTextAlignment = False
        Me.xrlSubTotal.Text = "0.00"
        Me.xrlSubTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta
        '
        Me.xrlVentaExenta.Dpi = 100.0!
        Me.xrlVentaExenta.Font = New System.Drawing.Font("Arial", 9.5!)
        Me.xrlVentaExenta.LocationFloat = New DevExpress.Utils.PointFloat(689.2084!, 63.25001!)
        Me.xrlVentaExenta.Name = "xrlVentaExenta"
        Me.xrlVentaExenta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta.SizeF = New System.Drawing.SizeF(45.0!, 11.99999!)
        Me.xrlVentaExenta.StylePriority.UseFont = False
        Me.xrlVentaExenta.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta.Text = "0.00"
        Me.xrlVentaExenta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVendedor
        '
        Me.xrlVendedor.CanGrow = False
        Me.xrlVendedor.Dpi = 100.0!
        Me.xrlVendedor.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVendedor.LocationFloat = New DevExpress.Utils.PointFloat(118.1667!, 41.24994!)
        Me.xrlVendedor.Name = "xrlVendedor"
        Me.xrlVendedor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVendedor.SizeF = New System.Drawing.SizeF(204.4583!, 17.00001!)
        Me.xrlVendedor.StylePriority.UseFont = False
        Me.xrlVendedor.StylePriority.UseTextAlignment = False
        Me.xrlVendedor.Text = "VENDEDOR"
        Me.xrlVendedor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlVendedor.Visible = False
        '
        'xrlNumero
        '
        Me.xrlNumero.Dpi = 100.0!
        Me.xrlNumero.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlNumero.LocationFloat = New DevExpress.Utils.PointFloat(600.9166!, 181.3333!)
        Me.xrlNumero.Name = "xrlNumero"
        Me.xrlNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlNumero.StylePriority.UseFont = False
        Me.xrlNumero.StylePriority.UseTextAlignment = False
        Me.xrlNumero.Text = "1945"
        Me.xrlNumero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNombre
        '
        Me.xrlNombre.CanGrow = False
        Me.xrlNombre.Dpi = 100.0!
        Me.xrlNombre.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlNombre.LocationFloat = New DevExpress.Utils.PointFloat(449.75!, 206.25!)
        Me.xrlNombre.Name = "xrlNombre"
        Me.xrlNombre.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNombre.SizeF = New System.Drawing.SizeF(280.4584!, 15.99998!)
        Me.xrlNombre.StylePriority.UseFont = False
        Me.xrlNombre.StylePriority.UseTextAlignment = False
        Me.xrlNombre.Text = "IT OUTSOURCING, S.A. DE C.V."
        Me.xrlNombre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCodigo
        '
        Me.xrlCodigo.CanGrow = False
        Me.xrlCodigo.Dpi = 100.0!
        Me.xrlCodigo.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlCodigo.LocationFloat = New DevExpress.Utils.PointFloat(324.4135!, 22.00001!)
        Me.xrlCodigo.Name = "xrlCodigo"
        Me.xrlCodigo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCodigo.SizeF = New System.Drawing.SizeF(104.7948!, 16.00001!)
        Me.xrlCodigo.StylePriority.UseFont = False
        Me.xrlCodigo.StylePriority.UseTextAlignment = False
        Me.xrlCodigo.Text = "CODIGO: 001"
        Me.xrlCodigo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlCodigo.Visible = False
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.Factura2, Me.LecturaAnterior2, Me.ConsumoM32, Me.LecturaActual2, Me.FechaVence4})
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 17.66657!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.PrecioVenta", "{0:n2}")})
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 9.25!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(324.4135!, 4.291757!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(35.4167!, 13.37481!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "XrLabel2"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'Factura2
        '
        Me.Factura2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.VentaAfecta", "{0:n2}")})
        Me.Factura2.Dpi = 100.0!
        Me.Factura2.Font = New System.Drawing.Font("Arial", 9.25!)
        Me.Factura2.LocationFloat = New DevExpress.Utils.PointFloat(695.2084!, 2.0!)
        Me.Factura2.Name = "Factura2"
        Me.Factura2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.Factura2.SizeF = New System.Drawing.SizeF(44.99997!, 13.37481!)
        Me.Factura2.StylePriority.UseFont = False
        Me.Factura2.StylePriority.UseTextAlignment = False
        Me.Factura2.Text = "Factura2"
        Me.Factura2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'LecturaAnterior2
        '
        Me.LecturaAnterior2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.Cantidad", "{0:n0}")})
        Me.LecturaAnterior2.Dpi = 100.0!
        Me.LecturaAnterior2.Font = New System.Drawing.Font("Arial", 9.25!)
        Me.LecturaAnterior2.LocationFloat = New DevExpress.Utils.PointFloat(397.8333!, 2.0!)
        Me.LecturaAnterior2.Name = "LecturaAnterior2"
        Me.LecturaAnterior2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LecturaAnterior2.SizeF = New System.Drawing.SizeF(29.79133!, 13.37481!)
        Me.LecturaAnterior2.StylePriority.UseFont = False
        Me.LecturaAnterior2.StylePriority.UseTextAlignment = False
        Me.LecturaAnterior2.Text = "LecturaAnterior2"
        Me.LecturaAnterior2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ConsumoM32
        '
        Me.ConsumoM32.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.PrecioUnitario", "{0:n2}")})
        Me.ConsumoM32.Dpi = 100.0!
        Me.ConsumoM32.Font = New System.Drawing.Font("Arial", 9.25!)
        Me.ConsumoM32.LocationFloat = New DevExpress.Utils.PointFloat(589.7079!, 2.0!)
        Me.ConsumoM32.Name = "ConsumoM32"
        Me.ConsumoM32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.ConsumoM32.SizeF = New System.Drawing.SizeF(35.4167!, 13.37481!)
        Me.ConsumoM32.StylePriority.UseFont = False
        Me.ConsumoM32.StylePriority.UseTextAlignment = False
        Me.ConsumoM32.Text = "ConsumoM32"
        Me.ConsumoM32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'LecturaActual2
        '
        Me.LecturaActual2.CanGrow = False
        Me.LecturaActual2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.Descripcion")})
        Me.LecturaActual2.Dpi = 100.0!
        Me.LecturaActual2.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.LecturaActual2.LocationFloat = New DevExpress.Utils.PointFloat(428.6246!, 2.000014!)
        Me.LecturaActual2.Name = "LecturaActual2"
        Me.LecturaActual2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LecturaActual2.SizeF = New System.Drawing.SizeF(163.0833!, 13.37481!)
        Me.LecturaActual2.StylePriority.UseFont = False
        Me.LecturaActual2.StylePriority.UseTextAlignment = False
        Me.LecturaActual2.Text = "[Descripcion]"
        Me.LecturaActual2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlGiro
        '
        Me.xrlGiro.CanGrow = False
        Me.xrlGiro.Dpi = 100.0!
        Me.xrlGiro.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlGiro.LocationFloat = New DevExpress.Utils.PointFloat(431.2496!, 269.0!)
        Me.xrlGiro.Name = "xrlGiro"
        Me.xrlGiro.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlGiro.SizeF = New System.Drawing.SizeF(135.1662!, 12.00005!)
        Me.xrlGiro.StylePriority.UseFont = False
        Me.xrlGiro.StylePriority.UseTextAlignment = False
        Me.xrlGiro.Text = "GIRO"
        Me.xrlGiro.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFormaPago
        '
        Me.xrlFormaPago.CanGrow = False
        Me.xrlFormaPago.Dpi = 100.0!
        Me.xrlFormaPago.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlFormaPago.LocationFloat = New DevExpress.Utils.PointFloat(474.2496!, 282.4999!)
        Me.xrlFormaPago.Name = "xrlFormaPago"
        Me.xrlFormaPago.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFormaPago.SizeF = New System.Drawing.SizeF(100.0833!, 14.16666!)
        Me.xrlFormaPago.StylePriority.UseFont = False
        Me.xrlFormaPago.StylePriority.UseTextAlignment = False
        Me.xrlFormaPago.Text = "CONTADO"
        Me.xrlFormaPago.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.Dpi = 100.0!
        Me.TopMarginBand1.HeightF = 0.666666!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'xrlVentaNoSujeta
        '
        Me.xrlVentaNoSujeta.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlVentaNoSujeta.Dpi = 100.0!
        Me.xrlVentaNoSujeta.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlVentaNoSujeta.LocationFloat = New DevExpress.Utils.PointFloat(314.1663!, 244.2501!)
        Me.xrlVentaNoSujeta.Name = "xrlVentaNoSujeta"
        Me.xrlVentaNoSujeta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaNoSujeta.SizeF = New System.Drawing.SizeF(44.99997!, 12.00002!)
        Me.xrlVentaNoSujeta.StylePriority.UseBorders = False
        Me.xrlVentaNoSujeta.StylePriority.UseFont = False
        Me.xrlVentaNoSujeta.StylePriority.UseTextAlignment = False
        Me.xrlVentaNoSujeta.Text = "0.00"
        Me.xrlVentaNoSujeta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.xrlVentaNoSujeta.Visible = False
        '
        'xrlVentaACuenta
        '
        Me.xrlVentaACuenta.CanGrow = False
        Me.xrlVentaACuenta.Dpi = 100.0!
        Me.xrlVentaACuenta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlVentaACuenta.LocationFloat = New DevExpress.Utils.PointFloat(324.4135!, 38.00003!)
        Me.xrlVentaACuenta.Name = "xrlVentaACuenta"
        Me.xrlVentaACuenta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaACuenta.SizeF = New System.Drawing.SizeF(104.7947!, 16.00002!)
        Me.xrlVentaACuenta.StylePriority.UseFont = False
        Me.xrlVentaACuenta.StylePriority.UseTextAlignment = False
        Me.xrlVentaACuenta.Text = "VENTA A CUENTA DE"
        Me.xrlVentaACuenta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlVentaACuenta.Visible = False
        '
        'xrlDepto
        '
        Me.xrlDepto.Dpi = 100.0!
        Me.xrlDepto.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlDepto.LocationFloat = New DevExpress.Utils.PointFloat(313.3746!, 169.5002!)
        Me.xrlDepto.Name = "xrlDepto"
        Me.xrlDepto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDepto.SizeF = New System.Drawing.SizeF(146.2049!, 10.00002!)
        Me.xrlDepto.StylePriority.UseFont = False
        Me.xrlDepto.StylePriority.UseTextAlignment = False
        Me.xrlDepto.Text = "SAN SALVADOR"
        Me.xrlDepto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlDepto.Visible = False
        '
        'xrlNit
        '
        Me.xrlNit.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNit.CanGrow = False
        Me.xrlNit.Dpi = 100.0!
        Me.xrlNit.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlNit.LocationFloat = New DevExpress.Utils.PointFloat(425.75!, 252.0002!)
        Me.xrlNit.Name = "xrlNit"
        Me.xrlNit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNit.SizeF = New System.Drawing.SizeF(163.9579!, 12.99998!)
        Me.xrlNit.StylePriority.UseBorders = False
        Me.xrlNit.StylePriority.UseFont = False
        Me.xrlNit.StylePriority.UseTextAlignment = False
        Me.xrlNit.Text = "0614-251107-107-7"
        Me.xrlNit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlFechaImpresion, Me.xrlPedido, Me.xrlDocAfectados, Me.xrlSucursal, Me.xrlNumRemision, Me.xrlNumFormulario, Me.xrlCodigo, Me.xrlFormaPago, Me.xrlGiro, Me.xrlTelefono, Me.xrlVentaACuenta, Me.xrlDepto, Me.xrlMunic, Me.xrlNit, Me.xrlDireccion, Me.xrlFecha, Me.xrlNrc, Me.xrlNombre, Me.xrlNumero, Me.XrLabel1, Me.xrlVentaNoSujeta})
        Me.PageHeader.Dpi = 100.0!
        Me.PageHeader.HeightF = 323.4999!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFechaImpresion
        '
        Me.xrlFechaImpresion.Dpi = 100.0!
        Me.xrlFechaImpresion.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlFechaImpresion.LocationFloat = New DevExpress.Utils.PointFloat(181.4584!, 4.000012!)
        Me.xrlFechaImpresion.Name = "xrlFechaImpresion"
        Me.xrlFechaImpresion.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFechaImpresion.SizeF = New System.Drawing.SizeF(202.6667!, 16.0!)
        Me.xrlFechaImpresion.StylePriority.UseFont = False
        Me.xrlFechaImpresion.StylePriority.UseTextAlignment = False
        Me.xrlFechaImpresion.Text = "HORA DE IMPRESIÓN"
        Me.xrlFechaImpresion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlPedido
        '
        Me.xrlPedido.CanGrow = False
        Me.xrlPedido.Dpi = 100.0!
        Me.xrlPedido.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlPedido.LocationFloat = New DevExpress.Utils.PointFloat(560.1251!, 38.00004!)
        Me.xrlPedido.Name = "xrlPedido"
        Me.xrlPedido.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPedido.SizeF = New System.Drawing.SizeF(100.0833!, 14.16666!)
        Me.xrlPedido.StylePriority.UseFont = False
        Me.xrlPedido.StylePriority.UseTextAlignment = False
        Me.xrlPedido.Text = "xrlPedido"
        Me.xrlPedido.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDocAfectados
        '
        Me.xrlDocAfectados.CanGrow = False
        Me.xrlDocAfectados.Dpi = 100.0!
        Me.xrlDocAfectados.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDocAfectados.LocationFloat = New DevExpress.Utils.PointFloat(322.8299!, 54.00005!)
        Me.xrlDocAfectados.Name = "xrlDocAfectados"
        Me.xrlDocAfectados.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDocAfectados.SizeF = New System.Drawing.SizeF(104.7947!, 16.00002!)
        Me.xrlDocAfectados.StylePriority.UseFont = False
        Me.xrlDocAfectados.StylePriority.UseTextAlignment = False
        Me.xrlDocAfectados.Text = "VENTA A CUENTA DE"
        Me.xrlDocAfectados.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlDocAfectados.Visible = False
        '
        'xrlSucursal
        '
        Me.xrlSucursal.Dpi = 100.0!
        Me.xrlSucursal.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlSucursal.LocationFloat = New DevExpress.Utils.PointFloat(600.9166!, 107.375!)
        Me.xrlSucursal.Name = "xrlSucursal"
        Me.xrlSucursal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSucursal.SizeF = New System.Drawing.SizeF(111.4584!, 15.0!)
        Me.xrlSucursal.StylePriority.UseFont = False
        Me.xrlSucursal.StylePriority.UseTextAlignment = False
        Me.xrlSucursal.Text = "xrlSucursal"
        Me.xrlSucursal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNumRemision
        '
        Me.xrlNumRemision.Dpi = 100.0!
        Me.xrlNumRemision.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlNumRemision.LocationFloat = New DevExpress.Utils.PointFloat(600.9167!, 133.4166!)
        Me.xrlNumRemision.Name = "xrlNumRemision"
        Me.xrlNumRemision.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumRemision.SizeF = New System.Drawing.SizeF(111.4584!, 15.0!)
        Me.xrlNumRemision.StylePriority.UseFont = False
        Me.xrlNumRemision.StylePriority.UseTextAlignment = False
        Me.xrlNumRemision.Text = "xrlNumRemision"
        Me.xrlNumRemision.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNumFormulario
        '
        Me.xrlNumFormulario.Dpi = 100.0!
        Me.xrlNumFormulario.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlNumFormulario.LocationFloat = New DevExpress.Utils.PointFloat(600.9167!, 158.7501!)
        Me.xrlNumFormulario.Name = "xrlNumFormulario"
        Me.xrlNumFormulario.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFormulario.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlNumFormulario.StylePriority.UseFont = False
        Me.xrlNumFormulario.StylePriority.UseTextAlignment = False
        Me.xrlNumFormulario.Text = "1945"
        Me.xrlNumFormulario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTelefono
        '
        Me.xrlTelefono.Dpi = 100.0!
        Me.xrlTelefono.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlTelefono.LocationFloat = New DevExpress.Utils.PointFloat(38.75005!, 6.0!)
        Me.xrlTelefono.Name = "xrlTelefono"
        Me.xrlTelefono.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTelefono.SizeF = New System.Drawing.SizeF(112.0417!, 16.0!)
        Me.xrlTelefono.StylePriority.UseFont = False
        Me.xrlTelefono.StylePriority.UseTextAlignment = False
        Me.xrlTelefono.Text = "TELEFONO"
        Me.xrlTelefono.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlTelefono.Visible = False
        '
        'xrlMunic
        '
        Me.xrlMunic.Dpi = 100.0!
        Me.xrlMunic.Font = New System.Drawing.Font("Arial", 10.5!, System.Drawing.FontStyle.Bold)
        Me.xrlMunic.LocationFloat = New DevExpress.Utils.PointFloat(314.1663!, 158.7501!)
        Me.xrlMunic.Name = "xrlMunic"
        Me.xrlMunic.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlMunic.SizeF = New System.Drawing.SizeF(155.3299!, 11.00003!)
        Me.xrlMunic.StylePriority.UseFont = False
        Me.xrlMunic.StylePriority.UseTextAlignment = False
        Me.xrlMunic.Text = "SAN SALVADOR"
        Me.xrlMunic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlMunic.Visible = False
        '
        'xrlFecha
        '
        Me.xrlFecha.Dpi = 100.0!
        Me.xrlFecha.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlFecha.LocationFloat = New DevExpress.Utils.PointFloat(632.1246!, 239.2501!)
        Me.xrlFecha.Name = "xrlFecha"
        Me.xrlFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha.SizeF = New System.Drawing.SizeF(98.0838!, 15.00005!)
        Me.xrlFecha.StylePriority.UseFont = False
        Me.xrlFecha.StylePriority.UseTextAlignment = False
        Me.xrlFecha.Text = "25/12/2012"
        Me.xrlFecha.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNrc
        '
        Me.xrlNrc.Dpi = 100.0!
        Me.xrlNrc.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlNrc.LocationFloat = New DevExpress.Utils.PointFloat(631.2073!, 256.0002!)
        Me.xrlNrc.Name = "xrlNrc"
        Me.xrlNrc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNrc.SizeF = New System.Drawing.SizeF(108.0833!, 12.99998!)
        Me.xrlNrc.StylePriority.UseFont = False
        Me.xrlNrc.StylePriority.UseTextAlignment = False
        Me.xrlNrc.Text = "194534-3"
        Me.xrlNrc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.VentaNoSujeta", "{0:n2}")})
        Me.XrLabel1.Dpi = 100.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(445.2496!, 24.62521!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(57.70847!, 13.37481!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "XrLabel1"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrLabel1.Visible = False
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.Dpi = 100.0!
        Me.BottomMarginBand1.HeightF = 68.9594!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        Me.BottomMarginBand1.SnapLinePadding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        '
        'DsFactura1
        '
        Me.DsFactura1.DataSetName = "dsFactura"
        Me.DsFactura1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'xrlCESC
        '
        Me.xrlCESC.Dpi = 100.0!
        Me.xrlCESC.Font = New System.Drawing.Font("Arial", 9.5!)
        Me.xrlCESC.LocationFloat = New DevExpress.Utils.PointFloat(621.9996!, 59.24991!)
        Me.xrlCESC.Name = "xrlCESC"
        Me.xrlCESC.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCESC.SizeF = New System.Drawing.SizeF(45.0!, 11.99999!)
        Me.xrlCESC.StylePriority.UseFont = False
        Me.xrlCESC.StylePriority.UseTextAlignment = False
        Me.xrlCESC.Text = "0.00"
        Me.xrlCESC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'fac_rptFiscal
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.DataMember = "Factura"
        Me.DataSource = Me.DsFactura1
        Me.DesignerOptions.ShowExportWarnings = False
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(23, 40, 1, 69)
        Me.PageHeight = 850
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.SnapGridSize = 2.083333!
        Me.SnapToGrid = False
        Me.Version = "16.1"
        CType(Me.DsFactura1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents xrlDireccion As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents FechaVence4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents xrlDiasCredito As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIva As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIvaRetenido As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlComentario As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCantLetras As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaAfecta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSubTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVendedor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumero As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNombre As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCodigo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents Factura2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents LecturaAnterior2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ConsumoM32 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents LecturaActual2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlGiro As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFormaPago As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents xrlVentaNoSujeta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaACuenta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDepto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNit As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents xrlTelefono As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlMunic As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFecha As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNrc As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents xrlNumFormulario As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumRemision As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSucursal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDocAfectados As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPedido As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DsFactura1 As Nexus.dsFactura
    Friend WithEvents xrlFechaImpresion As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCESC As DevExpress.XtraReports.UI.XRLabel
    'Friend WithEvents DsGas1 As Nexus.dsGas
End Class
