﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class fac_rptFactura
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xrlDiasCredito = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNombre2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDireccion2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label27 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label32 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlMunic2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label44 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label37 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaNoSujeta2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaACuenta = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFechaImpresion2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label28 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDireccion = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFormulario = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNombre = New DevExpress.XtraReports.UI.XRLabel()
        Me.label43 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTelefono = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumRemision2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDepto = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDepto2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlGiro2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFecha = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlComentario2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaAfecta2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPedido = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaAfecta = New DevExpress.XtraReports.UI.XRLabel()
        Me.label20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFechaImpresion = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNrc = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlGiro = New DevExpress.XtraReports.UI.XRLabel()
        Me.label46 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label42 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVendedor = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCESC2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCodigo = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSubTotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlIva = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.label9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaExenta2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFecha2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDiasCredito2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSucursal = New DevExpress.XtraReports.UI.XRLabel()
        Me.label3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label35 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumRemision = New DevExpress.XtraReports.UI.XRLabel()
        Me.label1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlIvaRetenido = New DevExpress.XtraReports.UI.XRLabel()
        Me.label22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTelefono2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNrc2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPedido2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label36 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label45 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVentaACuenta2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label40 = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.xrlVentaNoSujeta = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNit = New DevExpress.XtraReports.UI.XRLabel()
        Me.label5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFormulario2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSubTotal2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVendedor2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFormaPago2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label41 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCon = New DevExpress.XtraReports.UI.XRLabel()
        Me.label14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCESC = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSucursal2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNit2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label38 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCantLetras2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFormaPago = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlIva2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.xrlNumero = New DevExpress.XtraReports.UI.XRLabel()
        Me.label31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlComentario = New DevExpress.XtraReports.UI.XRLabel()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.xrlCantLetras = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.label39 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCodigo2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlIvaRetenido2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumero2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlMunic = New DevExpress.XtraReports.UI.XRLabel()
        Me.label8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.label18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrSubreport2 = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport1 = New DevExpress.XtraReports.UI.XRSubreport()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xrlDiasCredito
        '
        Me.xrlDiasCredito.Dpi = 100.0!
        Me.xrlDiasCredito.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDiasCredito.LocationFloat = New DevExpress.Utils.PointFloat(279.0003!, 17.6666!)
        Me.xrlDiasCredito.Name = "xrlDiasCredito"
        Me.xrlDiasCredito.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDiasCredito.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlDiasCredito.StylePriority.UseFont = False
        Me.xrlDiasCredito.StylePriority.UseTextAlignment = False
        Me.xrlDiasCredito.Text = "xrlDiasCredito"
        Me.xrlDiasCredito.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNombre2
        '
        Me.xrlNombre2.Dpi = 100.0!
        Me.xrlNombre2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlNombre2.LocationFloat = New DevExpress.Utils.PointFloat(419.1052!, 14.31243!)
        Me.xrlNombre2.Name = "xrlNombre2"
        Me.xrlNombre2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNombre2.SizeF = New System.Drawing.SizeF(333.2911!, 12.0!)
        Me.xrlNombre2.StylePriority.UseFont = False
        Me.xrlNombre2.StylePriority.UseTextAlignment = False
        Me.xrlNombre2.Text = "IT OUTSOURCING, S.A. DE C.V."
        Me.xrlNombre2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlNombre2.Visible = False
        '
        'xrlDireccion2
        '
        Me.xrlDireccion2.Dpi = 100.0!
        Me.xrlDireccion2.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlDireccion2.LocationFloat = New DevExpress.Utils.PointFloat(45.23035!, 24.31237!)
        Me.xrlDireccion2.Name = "xrlDireccion2"
        Me.xrlDireccion2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDireccion2.SizeF = New System.Drawing.SizeF(303.8747!, 11.00003!)
        Me.xrlDireccion2.StylePriority.UseFont = False
        Me.xrlDireccion2.StylePriority.UseTextAlignment = False
        Me.xrlDireccion2.Text = "RESIDENCIAL CLAUDIA PASAJE AIDA No.19"
        Me.xrlDireccion2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlDireccion2.Visible = False
        '
        'XrLabel2
        '
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(184.4372!, 22.6666!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(86.20813!, 14.00002!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "FORMA PAGO:"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'label27
        '
        Me.label27.Dpi = 100.0!
        Me.label27.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label27.LocationFloat = New DevExpress.Utils.PointFloat(644.58!, 638.8159!)
        Me.label27.Name = "label27"
        Me.label27.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label27.SizeF = New System.Drawing.SizeF(65.67!, 15.0!)
        Me.label27.StylePriority.UseFont = False
        Me.label27.StylePriority.UseTextAlignment = False
        Me.label27.Text = "[Mes]"
        Me.label27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label32
        '
        Me.label32.Dpi = 100.0!
        Me.label32.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label32.LocationFloat = New DevExpress.Utils.PointFloat(97.88!, 639.8159!)
        Me.label32.Name = "label32"
        Me.label32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label32.SizeF = New System.Drawing.SizeF(333.29!, 15.0!)
        Me.label32.StylePriority.UseFont = False
        Me.label32.StylePriority.UseTextAlignment = False
        Me.label32.Text = "IT OUTSOURCING, S.A. DE C.V."
        Me.label32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label25
        '
        Me.label25.CanGrow = False
        Me.label25.Dpi = 100.0!
        Me.label25.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label25.LocationFloat = New DevExpress.Utils.PointFloat(620.621!, 677.8159!)
        Me.label25.Name = "label25"
        Me.label25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label25.SizeF = New System.Drawing.SizeF(141.379!, 15.0!)
        Me.label25.StylePriority.UseFont = False
        Me.label25.StylePriority.UseTextAlignment = False
        Me.label25.Text = "[VentaAcuentaDe]"
        Me.label25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlMunic2
        '
        Me.xrlMunic2.Dpi = 100.0!
        Me.xrlMunic2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlMunic2.LocationFloat = New DevExpress.Utils.PointFloat(29.60373!, 7.312391!)
        Me.xrlMunic2.Name = "xrlMunic2"
        Me.xrlMunic2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlMunic2.SizeF = New System.Drawing.SizeF(132.0001!, 15.00002!)
        Me.xrlMunic2.StylePriority.UseFont = False
        Me.xrlMunic2.StylePriority.UseTextAlignment = False
        Me.xrlMunic2.Text = "SAN SALVADOR"
        Me.xrlMunic2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlMunic2.Visible = False
        '
        'label44
        '
        Me.label44.Dpi = 100.0!
        Me.label44.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.label44.LocationFloat = New DevExpress.Utils.PointFloat(211.1045!, 17.66663!)
        Me.label44.Name = "label44"
        Me.label44.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label44.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.label44.StylePriority.UseFont = False
        Me.label44.StylePriority.UseTextAlignment = False
        Me.label44.Text = "xrlDiasCredito"
        Me.label44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label37
        '
        Me.label37.Dpi = 100.0!
        Me.label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.label37.LocationFloat = New DevExpress.Utils.PointFloat(637.2915!, 18.0!)
        Me.label37.Name = "label37"
        Me.label37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label37.SizeF = New System.Drawing.SizeF(61.00003!, 13.0!)
        Me.label37.StylePriority.UseFont = False
        Me.label37.StylePriority.UseTextAlignment = False
        Me.label37.Text = "0.00"
        Me.label37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaNoSujeta2
        '
        Me.xrlVentaNoSujeta2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlVentaNoSujeta2.Dpi = 100.0!
        Me.xrlVentaNoSujeta2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaNoSujeta2.LocationFloat = New DevExpress.Utils.PointFloat(685.88!, 389.29!)
        Me.xrlVentaNoSujeta2.Name = "xrlVentaNoSujeta2"
        Me.xrlVentaNoSujeta2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaNoSujeta2.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.xrlVentaNoSujeta2.StylePriority.UseBorders = False
        Me.xrlVentaNoSujeta2.StylePriority.UseFont = False
        Me.xrlVentaNoSujeta2.StylePriority.UseTextAlignment = False
        Me.xrlVentaNoSujeta2.Text = "0.00"
        Me.xrlVentaNoSujeta2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal2
        '
        Me.xrlTotal2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlTotal2.Dpi = 100.0!
        Me.xrlTotal2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlTotal2.LocationFloat = New DevExpress.Utils.PointFloat(685.88!, 419.29!)
        Me.xrlTotal2.Name = "xrlTotal2"
        Me.xrlTotal2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal2.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.xrlTotal2.StylePriority.UseBorders = False
        Me.xrlTotal2.StylePriority.UseFont = False
        Me.xrlTotal2.StylePriority.UseTextAlignment = False
        Me.xrlTotal2.Text = "0.00"
        Me.xrlTotal2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'label11
        '
        Me.label11.Dpi = 100.0!
        Me.label11.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label11.LocationFloat = New DevExpress.Utils.PointFloat(589.621!, 128.0!)
        Me.label11.Name = "label11"
        Me.label11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label11.SizeF = New System.Drawing.SizeF(143.5395!, 15.0!)
        Me.label11.StylePriority.UseFont = False
        Me.label11.StylePriority.UseTextAlignment = False
        Me.label11.Text = "[CorreoElectronico]"
        Me.label11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaACuenta
        '
        Me.xrlVentaACuenta.Dpi = 100.0!
        Me.xrlVentaACuenta.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaACuenta.LocationFloat = New DevExpress.Utils.PointFloat(324.5833!, 23.6666!)
        Me.xrlVentaACuenta.Name = "xrlVentaACuenta"
        Me.xrlVentaACuenta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaACuenta.SizeF = New System.Drawing.SizeF(70.00015!, 12.00002!)
        Me.xrlVentaACuenta.StylePriority.UseFont = False
        Me.xrlVentaACuenta.StylePriority.UseTextAlignment = False
        Me.xrlVentaACuenta.Text = "VENTA A CUENTA DE"
        Me.xrlVentaACuenta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFechaImpresion2
        '
        Me.xrlFechaImpresion2.Dpi = 100.0!
        Me.xrlFechaImpresion2.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlFechaImpresion2.LocationFloat = New DevExpress.Utils.PointFloat(538.0637!, 11.31239!)
        Me.xrlFechaImpresion2.Name = "xrlFechaImpresion2"
        Me.xrlFechaImpresion2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFechaImpresion2.SizeF = New System.Drawing.SizeF(143.6664!, 12.00002!)
        Me.xrlFechaImpresion2.StylePriority.UseFont = False
        Me.xrlFechaImpresion2.StylePriority.UseTextAlignment = False
        Me.xrlFechaImpresion2.Text = "FECHA/HORA IMPRESION"
        Me.xrlFechaImpresion2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlFechaImpresion2.Visible = False
        '
        'label28
        '
        Me.label28.Dpi = 100.0!
        Me.label28.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label28.LocationFloat = New DevExpress.Utils.PointFloat(689.04!, 639.82!)
        Me.label28.Name = "label28"
        Me.label28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label28.SizeF = New System.Drawing.SizeF(65.67!, 15.0!)
        Me.label28.StylePriority.UseFont = False
        Me.label28.StylePriority.UseTextAlignment = False
        Me.label28.Text = "[Anio]"
        Me.label28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDireccion
        '
        Me.xrlDireccion.Dpi = 100.0!
        Me.xrlDireccion.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlDireccion.LocationFloat = New DevExpress.Utils.PointFloat(23.29175!, 19.66678!)
        Me.xrlDireccion.Name = "xrlDireccion"
        Me.xrlDireccion.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDireccion.SizeF = New System.Drawing.SizeF(303.8747!, 11.00003!)
        Me.xrlDireccion.StylePriority.UseFont = False
        Me.xrlDireccion.StylePriority.UseTextAlignment = False
        Me.xrlDireccion.Text = "RESIDENCIAL CLAUDIA PASAJE AIDA No.19"
        Me.xrlDireccion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNumFormulario
        '
        Me.xrlNumFormulario.Dpi = 100.0!
        Me.xrlNumFormulario.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlNumFormulario.LocationFloat = New DevExpress.Utils.PointFloat(564.0004!, 21.12503!)
        Me.xrlNumFormulario.Name = "xrlNumFormulario"
        Me.xrlNumFormulario.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFormulario.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlNumFormulario.StylePriority.UseFont = False
        Me.xrlNumFormulario.StylePriority.UseTextAlignment = False
        Me.xrlNumFormulario.Text = "1945"
        Me.xrlNumFormulario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNombre
        '
        Me.xrlNombre.Dpi = 100.0!
        Me.xrlNombre.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlNombre.LocationFloat = New DevExpress.Utils.PointFloat(413.4171!, 18.66681!)
        Me.xrlNombre.Name = "xrlNombre"
        Me.xrlNombre.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNombre.SizeF = New System.Drawing.SizeF(333.2911!, 12.0!)
        Me.xrlNombre.StylePriority.UseFont = False
        Me.xrlNombre.StylePriority.UseTextAlignment = False
        Me.xrlNombre.Text = "IT OUTSOURCING, S.A. DE C.V."
        Me.xrlNombre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label43
        '
        Me.label43.Dpi = 100.0!
        Me.label43.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.label43.LocationFloat = New DevExpress.Utils.PointFloat(189.1046!, 4.666656!)
        Me.label43.Name = "label43"
        Me.label43.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label43.SizeF = New System.Drawing.SizeF(132.0001!, 15.00002!)
        Me.label43.StylePriority.UseFont = False
        Me.label43.StylePriority.UseTextAlignment = False
        Me.label43.Text = "SAN SALVADOR"
        Me.label43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTelefono
        '
        Me.xrlTelefono.Dpi = 100.0!
        Me.xrlTelefono.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlTelefono.LocationFloat = New DevExpress.Utils.PointFloat(125.0003!, 17.6666!)
        Me.xrlTelefono.Name = "xrlTelefono"
        Me.xrlTelefono.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTelefono.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlTelefono.StylePriority.UseFont = False
        Me.xrlTelefono.StylePriority.UseTextAlignment = False
        Me.xrlTelefono.Text = "25/12/2012"
        Me.xrlTelefono.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNumRemision2
        '
        Me.xrlNumRemision2.Dpi = 100.0!
        Me.xrlNumRemision2.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlNumRemision2.LocationFloat = New DevExpress.Utils.PointFloat(334.2077!, 20.85422!)
        Me.xrlNumRemision2.Name = "xrlNumRemision2"
        Me.xrlNumRemision2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumRemision2.SizeF = New System.Drawing.SizeF(111.4584!, 15.0!)
        Me.xrlNumRemision2.StylePriority.UseFont = False
        Me.xrlNumRemision2.StylePriority.UseTextAlignment = False
        Me.xrlNumRemision2.Text = "xrlNumRemision"
        Me.xrlNumRemision2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlNumRemision2.Visible = False
        '
        'xrlDepto
        '
        Me.xrlDepto.Dpi = 100.0!
        Me.xrlDepto.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDepto.LocationFloat = New DevExpress.Utils.PointFloat(257.0004!, 4.666626!)
        Me.xrlDepto.Name = "xrlDepto"
        Me.xrlDepto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDepto.SizeF = New System.Drawing.SizeF(132.0001!, 15.00002!)
        Me.xrlDepto.StylePriority.UseFont = False
        Me.xrlDepto.StylePriority.UseTextAlignment = False
        Me.xrlDepto.Text = "SAN SALVADOR"
        Me.xrlDepto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDepto2
        '
        Me.xrlDepto2.Dpi = 100.0!
        Me.xrlDepto2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDepto2.LocationFloat = New DevExpress.Utils.PointFloat(161.6039!, 7.312391!)
        Me.xrlDepto2.Name = "xrlDepto2"
        Me.xrlDepto2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDepto2.SizeF = New System.Drawing.SizeF(132.0001!, 15.00002!)
        Me.xrlDepto2.StylePriority.UseFont = False
        Me.xrlDepto2.StylePriority.UseTextAlignment = False
        Me.xrlDepto2.Text = "SAN SALVADOR"
        Me.xrlDepto2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlDepto2.Visible = False
        '
        'label16
        '
        Me.label16.Dpi = 100.0!
        Me.label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.label16.LocationFloat = New DevExpress.Utils.PointFloat(560.2915!, 17.0!)
        Me.label16.Name = "label16"
        Me.label16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label16.SizeF = New System.Drawing.SizeF(61.00003!, 13.0!)
        Me.label16.StylePriority.UseFont = False
        Me.label16.StylePriority.UseTextAlignment = False
        Me.label16.Text = "0.00"
        Me.label16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.label16.Visible = False
        '
        'label4
        '
        Me.label4.Dpi = 100.0!
        Me.label4.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label4.LocationFloat = New DevExpress.Utils.PointFloat(127.88!, 168.0!)
        Me.label4.Name = "label4"
        Me.label4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label4.SizeF = New System.Drawing.SizeF(146.2049!, 15.0!)
        Me.label4.StylePriority.UseFont = False
        Me.label4.StylePriority.UseTextAlignment = False
        Me.label4.Text = "SAN SALVADOR"
        Me.label4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlGiro2
        '
        Me.xrlGiro2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlGiro2.Dpi = 100.0!
        Me.xrlGiro2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlGiro2.LocationFloat = New DevExpress.Utils.PointFloat(254.6456!, 22.31245!)
        Me.xrlGiro2.Name = "xrlGiro2"
        Me.xrlGiro2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlGiro2.SizeF = New System.Drawing.SizeF(141.0827!, 13.0!)
        Me.xrlGiro2.StylePriority.UseBorders = False
        Me.xrlGiro2.StylePriority.UseFont = False
        Me.xrlGiro2.StylePriority.UseTextAlignment = False
        Me.xrlGiro2.Text = "CONSULTORES DE EQUIPO Y PROGRAMAS DE INFORMATICA"
        Me.xrlGiro2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlGiro2.Visible = False
        '
        'xrlFecha
        '
        Me.xrlFecha.Dpi = 100.0!
        Me.xrlFecha.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlFecha.LocationFloat = New DevExpress.Utils.PointFloat(596.9161!, 21.66669!)
        Me.xrlFecha.Name = "xrlFecha"
        Me.xrlFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha.SizeF = New System.Drawing.SizeF(77.0!, 11.0!)
        Me.xrlFecha.StylePriority.UseFont = False
        Me.xrlFecha.StylePriority.UseTextAlignment = False
        Me.xrlFecha.Text = "25/12/2012"
        Me.xrlFecha.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlComentario2
        '
        Me.xrlComentario2.Dpi = 100.0!
        Me.xrlComentario2.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlComentario2.LocationFloat = New DevExpress.Utils.PointFloat(288.1045!, 13.99997!)
        Me.xrlComentario2.Name = "xrlComentario2"
        Me.xrlComentario2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlComentario2.SizeF = New System.Drawing.SizeF(265.0416!, 17.00001!)
        Me.xrlComentario2.StylePriority.UseFont = False
        Me.xrlComentario2.StylePriority.UseTextAlignment = False
        Me.xrlComentario2.Text = "Comentario"
        Me.xrlComentario2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlComentario2.Visible = False
        '
        'label33
        '
        Me.label33.CanGrow = False
        Me.label33.Dpi = 100.0!
        Me.label33.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label33.LocationFloat = New DevExpress.Utils.PointFloat(72.25!, 882.25!)
        Me.label33.Name = "label33"
        Me.label33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label33.SizeF = New System.Drawing.SizeF(262.7916!, 15.0!)
        Me.label33.StylePriority.UseFont = False
        Me.label33.StylePriority.UseTextAlignment = False
        Me.label33.Text = "VENDEDOR"
        Me.label33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlVentaAfecta2
        '
        Me.xrlVentaAfecta2.Dpi = 100.0!
        Me.xrlVentaAfecta2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaAfecta2.LocationFloat = New DevExpress.Utils.PointFloat(685.88!, 353.2917!)
        Me.xrlVentaAfecta2.Name = "xrlVentaAfecta2"
        Me.xrlVentaAfecta2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaAfecta2.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.xrlVentaAfecta2.StylePriority.UseFont = False
        Me.xrlVentaAfecta2.StylePriority.UseTextAlignment = False
        Me.xrlVentaAfecta2.Text = "0.00"
        Me.xrlVentaAfecta2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlPedido
        '
        Me.xrlPedido.Dpi = 100.0!
        Me.xrlPedido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlPedido.LocationFloat = New DevExpress.Utils.PointFloat(81.54185!, 20.66666!)
        Me.xrlPedido.Name = "xrlPedido"
        Me.xrlPedido.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPedido.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlPedido.StylePriority.UseFont = False
        Me.xrlPedido.StylePriority.UseTextAlignment = False
        Me.xrlPedido.Text = "1945"
        Me.xrlPedido.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaAfecta
        '
        Me.xrlVentaAfecta.Dpi = 100.0!
        Me.xrlVentaAfecta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaAfecta.LocationFloat = New DevExpress.Utils.PointFloat(685.88!, 880.25!)
        Me.xrlVentaAfecta.Name = "xrlVentaAfecta"
        Me.xrlVentaAfecta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaAfecta.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.xrlVentaAfecta.StylePriority.UseFont = False
        Me.xrlVentaAfecta.StylePriority.UseTextAlignment = False
        Me.xrlVentaAfecta.Text = "0.00"
        Me.xrlVentaAfecta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'label20
        '
        Me.label20.Dpi = 100.0!
        Me.label20.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label20.LocationFloat = New DevExpress.Utils.PointFloat(629.25!, 538.82!)
        Me.label20.Name = "label20"
        Me.label20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label20.SizeF = New System.Drawing.SizeF(79.04175!, 15.0!)
        Me.label20.StylePriority.UseFont = False
        Me.label20.StylePriority.UseTextAlignment = False
        Me.label20.Text = "1945"
        Me.label20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFechaImpresion
        '
        Me.xrlFechaImpresion.Dpi = 100.0!
        Me.xrlFechaImpresion.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlFechaImpresion.LocationFloat = New DevExpress.Utils.PointFloat(513.542!, 20.66666!)
        Me.xrlFechaImpresion.Name = "xrlFechaImpresion"
        Me.xrlFechaImpresion.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFechaImpresion.SizeF = New System.Drawing.SizeF(143.6664!, 12.00002!)
        Me.xrlFechaImpresion.StylePriority.UseFont = False
        Me.xrlFechaImpresion.StylePriority.UseTextAlignment = False
        Me.xrlFechaImpresion.Text = "FECHA/HORA IMPRESION"
        Me.xrlFechaImpresion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNrc
        '
        Me.xrlNrc.Dpi = 100.0!
        Me.xrlNrc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlNrc.LocationFloat = New DevExpress.Utils.PointFloat(235.5828!, 20.66666!)
        Me.xrlNrc.Name = "xrlNrc"
        Me.xrlNrc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNrc.SizeF = New System.Drawing.SizeF(65.6669!, 13.0!)
        Me.xrlNrc.StylePriority.UseFont = False
        Me.xrlNrc.StylePriority.UseTextAlignment = False
        Me.xrlNrc.Text = "194534-3"
        Me.xrlNrc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlGiro
        '
        Me.xrlGiro.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlGiro.Dpi = 100.0!
        Me.xrlGiro.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlGiro.LocationFloat = New DevExpress.Utils.PointFloat(350.0421!, 19.66663!)
        Me.xrlGiro.Name = "xrlGiro"
        Me.xrlGiro.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlGiro.SizeF = New System.Drawing.SizeF(141.0827!, 13.0!)
        Me.xrlGiro.StylePriority.UseBorders = False
        Me.xrlGiro.StylePriority.UseFont = False
        Me.xrlGiro.StylePriority.UseTextAlignment = False
        Me.xrlGiro.Text = "CONSULTORES DE EQUIPO Y PROGRAMAS DE INFORMATICA"
        Me.xrlGiro.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label46
        '
        Me.label46.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.label46.Dpi = 100.0!
        Me.label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label46.LocationFloat = New DevExpress.Utils.PointFloat(282.1463!, 19.66669!)
        Me.label46.Name = "label46"
        Me.label46.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label46.SizeF = New System.Drawing.SizeF(141.0827!, 13.0!)
        Me.label46.StylePriority.UseBorders = False
        Me.label46.StylePriority.UseFont = False
        Me.label46.StylePriority.UseTextAlignment = False
        Me.label46.Text = "CONSULTORES DE EQUIPO Y PROGRAMAS DE INFORMATICA"
        Me.label46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label42
        '
        Me.label42.Dpi = 100.0!
        Me.label42.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.label42.LocationFloat = New DevExpress.Utils.PointFloat(57.10449!, 4.666656!)
        Me.label42.Name = "label42"
        Me.label42.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label42.SizeF = New System.Drawing.SizeF(132.0001!, 15.00002!)
        Me.label42.StylePriority.UseFont = False
        Me.label42.StylePriority.UseTextAlignment = False
        Me.label42.Text = "SAN SALVADOR"
        Me.label42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label19
        '
        Me.label19.CanGrow = False
        Me.label19.Dpi = 100.0!
        Me.label19.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label19.LocationFloat = New DevExpress.Utils.PointFloat(72.25!, 370.2917!)
        Me.label19.Name = "label19"
        Me.label19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label19.SizeF = New System.Drawing.SizeF(262.7916!, 15.0!)
        Me.label19.StylePriority.UseFont = False
        Me.label19.StylePriority.UseTextAlignment = False
        Me.label19.Text = "VENDEDOR"
        Me.label19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlVendedor
        '
        Me.xrlVendedor.Dpi = 100.0!
        Me.xrlVendedor.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVendedor.LocationFloat = New DevExpress.Utils.PointFloat(84.66724!, 18.66681!)
        Me.xrlVendedor.Name = "xrlVendedor"
        Me.xrlVendedor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVendedor.SizeF = New System.Drawing.SizeF(181.1667!, 12.00002!)
        Me.xrlVendedor.StylePriority.UseFont = False
        Me.xrlVendedor.StylePriority.UseTextAlignment = False
        Me.xrlVendedor.Text = "xrlVendedor"
        Me.xrlVendedor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaExenta
        '
        Me.xrlVentaExenta.Dpi = 100.0!
        Me.xrlVentaExenta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaExenta.LocationFloat = New DevExpress.Utils.PointFloat(685.88!, 934.25!)
        Me.xrlVentaExenta.Name = "xrlVentaExenta"
        Me.xrlVentaExenta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.xrlVentaExenta.StylePriority.UseFont = False
        Me.xrlVentaExenta.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta.Text = "0.00"
        Me.xrlVentaExenta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCESC2
        '
        Me.xrlCESC2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlCESC2.Dpi = 100.0!
        Me.xrlCESC2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlCESC2.LocationFloat = New DevExpress.Utils.PointFloat(275.4795!, 5.166626!)
        Me.xrlCESC2.Name = "xrlCESC2"
        Me.xrlCESC2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCESC2.SizeF = New System.Drawing.SizeF(61.00003!, 15.99998!)
        Me.xrlCESC2.StylePriority.UseBorders = False
        Me.xrlCESC2.StylePriority.UseFont = False
        Me.xrlCESC2.StylePriority.UseTextAlignment = False
        Me.xrlCESC2.Text = "0.00"
        Me.xrlCESC2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCodigo
        '
        Me.xrlCodigo.Dpi = 100.0!
        Me.xrlCodigo.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlCodigo.LocationFloat = New DevExpress.Utils.PointFloat(202.0003!, 17.6666!)
        Me.xrlCodigo.Name = "xrlCodigo"
        Me.xrlCodigo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCodigo.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlCodigo.StylePriority.UseFont = False
        Me.xrlCodigo.StylePriority.UseTextAlignment = False
        Me.xrlCodigo.Text = "xrlCodigo"
        Me.xrlCodigo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlSubTotal
        '
        Me.xrlSubTotal.Dpi = 100.0!
        Me.xrlSubTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlSubTotal.LocationFloat = New DevExpress.Utils.PointFloat(685.88!, 907.25!)
        Me.xrlSubTotal.Name = "xrlSubTotal"
        Me.xrlSubTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSubTotal.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.xrlSubTotal.StylePriority.UseFont = False
        Me.xrlSubTotal.StylePriority.UseTextAlignment = False
        Me.xrlSubTotal.Text = "0.00"
        Me.xrlSubTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlIva
        '
        Me.xrlIva.Dpi = 100.0!
        Me.xrlIva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlIva.LocationFloat = New DevExpress.Utils.PointFloat(482.4166!, 22.6666!)
        Me.xrlIva.Name = "xrlIva"
        Me.xrlIva.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIva.SizeF = New System.Drawing.SizeF(87.75!, 19.0!)
        Me.xrlIva.StylePriority.UseFont = False
        Me.xrlIva.StylePriority.UseTextAlignment = False
        Me.xrlIva.Text = "0.00"
        Me.xrlIva.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.xrlIva.Visible = False
        '
        'Detail
        '
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 23.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label9
        '
        Me.label9.Dpi = 100.0!
        Me.label9.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label9.LocationFloat = New DevExpress.Utils.PointFloat(619.62!, 168.0!)
        Me.label9.Name = "label9"
        Me.label9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label9.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.label9.StylePriority.UseFont = False
        Me.label9.StylePriority.UseTextAlignment = False
        Me.label9.Text = "[Tarjeta]"
        Me.label9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'label2
        '
        Me.label2.Dpi = 100.0!
        Me.label2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.LocationFloat = New DevExpress.Utils.PointFloat(97.88!, 128.0!)
        Me.label2.Name = "label2"
        Me.label2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label2.SizeF = New System.Drawing.SizeF(400.2479!, 15.0!)
        Me.label2.StylePriority.UseFont = False
        Me.label2.StylePriority.UseTextAlignment = False
        Me.label2.Text = "RESIDENCIAL CLAUDIA PASAJE AIDA No.19"
        Me.label2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaExenta2
        '
        Me.xrlVentaExenta2.Dpi = 100.0!
        Me.xrlVentaExenta2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaExenta2.LocationFloat = New DevExpress.Utils.PointFloat(685.88!, 404.29!)
        Me.xrlVentaExenta2.Name = "xrlVentaExenta2"
        Me.xrlVentaExenta2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta2.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.xrlVentaExenta2.StylePriority.UseFont = False
        Me.xrlVentaExenta2.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta2.Text = "0.00"
        Me.xrlVentaExenta2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'label29
        '
        Me.label29.Dpi = 100.0!
        Me.label29.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label29.LocationFloat = New DevExpress.Utils.PointFloat(127.88!, 696.8159!)
        Me.label29.Name = "label29"
        Me.label29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label29.SizeF = New System.Drawing.SizeF(146.2049!, 15.0!)
        Me.label29.StylePriority.UseFont = False
        Me.label29.StylePriority.UseTextAlignment = False
        Me.label29.Text = "SAN SALVADOR"
        Me.label29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFecha2
        '
        Me.xrlFecha2.Dpi = 100.0!
        Me.xrlFecha2.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlFecha2.LocationFloat = New DevExpress.Utils.PointFloat(643.7287!, 22.31245!)
        Me.xrlFecha2.Name = "xrlFecha2"
        Me.xrlFecha2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha2.SizeF = New System.Drawing.SizeF(77.0!, 11.0!)
        Me.xrlFecha2.StylePriority.UseFont = False
        Me.xrlFecha2.StylePriority.UseTextAlignment = False
        Me.xrlFecha2.Text = "25/12/2012"
        Me.xrlFecha2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlFecha2.Visible = False
        '
        'xrlDiasCredito2
        '
        Me.xrlDiasCredito2.Dpi = 100.0!
        Me.xrlDiasCredito2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDiasCredito2.LocationFloat = New DevExpress.Utils.PointFloat(183.1044!, 20.3124!)
        Me.xrlDiasCredito2.Name = "xrlDiasCredito2"
        Me.xrlDiasCredito2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDiasCredito2.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlDiasCredito2.StylePriority.UseFont = False
        Me.xrlDiasCredito2.StylePriority.UseTextAlignment = False
        Me.xrlDiasCredito2.Text = "xrlDiasCredito"
        Me.xrlDiasCredito2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlDiasCredito2.Visible = False
        '
        'label7
        '
        Me.label7.Dpi = 100.0!
        Me.label7.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label7.LocationFloat = New DevExpress.Utils.PointFloat(589.621!, 111.0!)
        Me.label7.Name = "label7"
        Me.label7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label7.SizeF = New System.Drawing.SizeF(65.67!, 15.0!)
        Me.label7.StylePriority.UseFont = False
        Me.label7.StylePriority.UseTextAlignment = False
        Me.label7.Text = "[Dia]"
        Me.label7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label10
        '
        Me.label10.Dpi = 100.0!
        Me.label10.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label10.LocationFloat = New DevExpress.Utils.PointFloat(662.67!, 168.0!)
        Me.label10.Name = "label10"
        Me.label10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label10.SizeF = New System.Drawing.SizeF(37.57831!, 15.0!)
        Me.label10.StylePriority.UseFont = False
        Me.label10.StylePriority.UseTextAlignment = False
        Me.label10.Text = "[Cheque]"
        Me.label10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'label13
        '
        Me.label13.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.label13.Dpi = 100.0!
        Me.label13.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label13.LocationFloat = New DevExpress.Utils.PointFloat(365.938!, 6.791687!)
        Me.label13.Name = "label13"
        Me.label13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label13.SizeF = New System.Drawing.SizeF(61.00003!, 15.99998!)
        Me.label13.StylePriority.UseBorders = False
        Me.label13.StylePriority.UseFont = False
        Me.label13.StylePriority.UseTextAlignment = False
        Me.label13.Text = "0.00"
        Me.label13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.label13.Visible = False
        '
        'xrlSucursal
        '
        Me.xrlSucursal.Dpi = 100.0!
        Me.xrlSucursal.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlSucursal.LocationFloat = New DevExpress.Utils.PointFloat(491.1248!, 22.6666!)
        Me.xrlSucursal.Name = "xrlSucursal"
        Me.xrlSucursal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSucursal.SizeF = New System.Drawing.SizeF(111.4584!, 15.0!)
        Me.xrlSucursal.StylePriority.UseFont = False
        Me.xrlSucursal.StylePriority.UseTextAlignment = False
        Me.xrlSucursal.Text = "xrlSucursal"
        Me.xrlSucursal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label3
        '
        Me.label3.Dpi = 100.0!
        Me.label3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.label3.LocationFloat = New DevExpress.Utils.PointFloat(97.87999!, 149.0!)
        Me.label3.Name = "label3"
        Me.label3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label3.SizeF = New System.Drawing.SizeF(132.0001!, 15.00002!)
        Me.label3.StylePriority.UseFont = False
        Me.label3.StylePriority.UseTextAlignment = False
        Me.label3.Text = "SAN SALVADOR"
        Me.label3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label35
        '
        Me.label35.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.label35.Dpi = 100.0!
        Me.label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.label35.LocationFloat = New DevExpress.Utils.PointFloat(657.2083!, 23.6666!)
        Me.label35.Name = "label35"
        Me.label35.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label35.SizeF = New System.Drawing.SizeF(60.99994!, 13.99998!)
        Me.label35.StylePriority.UseBorders = False
        Me.label35.StylePriority.UseFont = False
        Me.label35.StylePriority.UseTextAlignment = False
        Me.label35.Text = "0.00"
        Me.label35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'label6
        '
        Me.label6.Dpi = 100.0!
        Me.label6.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label6.LocationFloat = New DevExpress.Utils.PointFloat(689.04!, 111.0!)
        Me.label6.Name = "label6"
        Me.label6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label6.SizeF = New System.Drawing.SizeF(65.67!, 15.0!)
        Me.label6.StylePriority.UseFont = False
        Me.label6.StylePriority.UseTextAlignment = False
        Me.label6.Text = "[Anio]"
        Me.label6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label34
        '
        Me.label34.Dpi = 100.0!
        Me.label34.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.label34.LocationFloat = New DevExpress.Utils.PointFloat(179.3959!, 19.66678!)
        Me.label34.Name = "label34"
        Me.label34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label34.SizeF = New System.Drawing.SizeF(272.1669!, 14.0!)
        Me.label34.StylePriority.UseFont = False
        Me.label34.StylePriority.UseTextAlignment = False
        Me.label34.Text = "DIEZ"
        Me.label34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlNumRemision
        '
        Me.xrlNumRemision.Dpi = 100.0!
        Me.xrlNumRemision.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlNumRemision.LocationFloat = New DevExpress.Utils.PointFloat(365.938!, 19.6666!)
        Me.xrlNumRemision.Name = "xrlNumRemision"
        Me.xrlNumRemision.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumRemision.SizeF = New System.Drawing.SizeF(111.4584!, 15.0!)
        Me.xrlNumRemision.StylePriority.UseFont = False
        Me.xrlNumRemision.StylePriority.UseTextAlignment = False
        Me.xrlNumRemision.Text = "xrlNumRemision"
        Me.xrlNumRemision.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label1
        '
        Me.label1.Dpi = 100.0!
        Me.label1.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.LocationFloat = New DevExpress.Utils.PointFloat(97.88!, 111.0!)
        Me.label1.Name = "label1"
        Me.label1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label1.SizeF = New System.Drawing.SizeF(333.29!, 15.0!)
        Me.label1.StylePriority.UseFont = False
        Me.label1.StylePriority.UseTextAlignment = False
        Me.label1.Text = "IT OUTSOURCING, S.A. DE C.V."
        Me.label1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlIvaRetenido
        '
        Me.xrlIvaRetenido.Dpi = 100.0!
        Me.xrlIvaRetenido.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.xrlIvaRetenido.LocationFloat = New DevExpress.Utils.PointFloat(621.2915!, 20.7916!)
        Me.xrlIvaRetenido.Name = "xrlIvaRetenido"
        Me.xrlIvaRetenido.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIvaRetenido.SizeF = New System.Drawing.SizeF(61.00003!, 13.875!)
        Me.xrlIvaRetenido.StylePriority.UseFont = False
        Me.xrlIvaRetenido.StylePriority.UseTextAlignment = False
        Me.xrlIvaRetenido.Text = "0.00"
        Me.xrlIvaRetenido.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'label22
        '
        Me.label22.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.Contado")})
        Me.label22.Dpi = 100.0!
        Me.label22.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label22.LocationFloat = New DevExpress.Utils.PointFloat(659.04!, 696.8159!)
        Me.label22.Name = "label22"
        Me.label22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label22.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.label22.StylePriority.UseFont = False
        Me.label22.StylePriority.UseTextAlignment = False
        Me.label22.Text = "xrlCon"
        Me.label22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel21
        '
        Me.XrLabel21.Dpi = 100.0!
        Me.XrLabel21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(17.60373!, 8.312386!)
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel21.SizeF = New System.Drawing.SizeF(86.20813!, 14.00002!)
        Me.XrLabel21.StylePriority.UseFont = False
        Me.XrLabel21.StylePriority.UseTextAlignment = False
        Me.XrLabel21.Text = "FORMA PAGO:"
        Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrLabel21.Visible = False
        '
        'xrlTelefono2
        '
        Me.xrlTelefono2.Dpi = 100.0!
        Me.xrlTelefono2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlTelefono2.LocationFloat = New DevExpress.Utils.PointFloat(29.60373!, 20.3124!)
        Me.xrlTelefono2.Name = "xrlTelefono2"
        Me.xrlTelefono2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTelefono2.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlTelefono2.StylePriority.UseFont = False
        Me.xrlTelefono2.StylePriority.UseTextAlignment = False
        Me.xrlTelefono2.Text = "25/12/2012"
        Me.xrlTelefono2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlTelefono2.Visible = False
        '
        'xrlNrc2
        '
        Me.xrlNrc2.Dpi = 100.0!
        Me.xrlNrc2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlNrc2.LocationFloat = New DevExpress.Utils.PointFloat(260.1044!, 11.31239!)
        Me.xrlNrc2.Name = "xrlNrc2"
        Me.xrlNrc2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNrc2.SizeF = New System.Drawing.SizeF(65.6669!, 13.0!)
        Me.xrlNrc2.StylePriority.UseFont = False
        Me.xrlNrc2.StylePriority.UseTextAlignment = False
        Me.xrlNrc2.Text = "194534-3"
        Me.xrlNrc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlNrc2.Visible = False
        '
        'label24
        '
        Me.label24.Dpi = 100.0!
        Me.label24.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label24.LocationFloat = New DevExpress.Utils.PointFloat(589.621!, 696.8159!)
        Me.label24.Name = "label24"
        Me.label24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label24.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.label24.StylePriority.UseFont = False
        Me.label24.StylePriority.UseTextAlignment = False
        Me.label24.Text = "[Tarjeta]"
        Me.label24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlPedido2
        '
        Me.xrlPedido2.Dpi = 100.0!
        Me.xrlPedido2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlPedido2.LocationFloat = New DevExpress.Utils.PointFloat(106.0635!, 11.31239!)
        Me.xrlPedido2.Name = "xrlPedido2"
        Me.xrlPedido2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPedido2.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlPedido2.StylePriority.UseFont = False
        Me.xrlPedido2.StylePriority.UseTextAlignment = False
        Me.xrlPedido2.Text = "1945"
        Me.xrlPedido2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlPedido2.Visible = False
        '
        'label36
        '
        Me.label36.Dpi = 100.0!
        Me.label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.label36.LocationFloat = New DevExpress.Utils.PointFloat(602.5831!, 30.00003!)
        Me.label36.Name = "label36"
        Me.label36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label36.SizeF = New System.Drawing.SizeF(61.00003!, 12.875!)
        Me.label36.StylePriority.UseFont = False
        Me.label36.StylePriority.UseTextAlignment = False
        Me.label36.Text = "0.00"
        Me.label36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'label45
        '
        Me.label45.Dpi = 100.0!
        Me.label45.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.label45.LocationFloat = New DevExpress.Utils.PointFloat(134.1045!, 17.66663!)
        Me.label45.Name = "label45"
        Me.label45.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label45.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.label45.StylePriority.UseFont = False
        Me.label45.StylePriority.UseTextAlignment = False
        Me.label45.Text = "xrlCodigo"
        Me.label45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label12
        '
        Me.label12.Dpi = 100.0!
        Me.label12.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label12.LocationFloat = New DevExpress.Utils.PointFloat(629.25!, 10.0!)
        Me.label12.Name = "label12"
        Me.label12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label12.SizeF = New System.Drawing.SizeF(79.04175!, 15.0!)
        Me.label12.StylePriority.UseFont = False
        Me.label12.StylePriority.UseTextAlignment = False
        Me.label12.Text = "1945"
        Me.label12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaACuenta2
        '
        Me.xrlVentaACuenta2.Dpi = 100.0!
        Me.xrlVentaACuenta2.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaACuenta2.LocationFloat = New DevExpress.Utils.PointFloat(349.105!, 14.3124!)
        Me.xrlVentaACuenta2.Name = "xrlVentaACuenta2"
        Me.xrlVentaACuenta2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaACuenta2.SizeF = New System.Drawing.SizeF(70.00015!, 12.00002!)
        Me.xrlVentaACuenta2.StylePriority.UseFont = False
        Me.xrlVentaACuenta2.StylePriority.UseTextAlignment = False
        Me.xrlVentaACuenta2.Text = "VENTA A CUENTA DE"
        Me.xrlVentaACuenta2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlVentaACuenta2.Visible = False
        '
        'label40
        '
        Me.label40.Dpi = 100.0!
        Me.label40.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label40.LocationFloat = New DevExpress.Utils.PointFloat(554.6251!, 21.54166!)
        Me.label40.Name = "label40"
        Me.label40.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label40.SizeF = New System.Drawing.SizeF(143.6664!, 12.00002!)
        Me.label40.StylePriority.UseFont = False
        Me.label40.StylePriority.UseTextAlignment = False
        Me.label40.Text = "FECHA/HORA IMPRESION"
        Me.label40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlFechaImpresion, Me.xrlSucursal, Me.xrlNumRemision, Me.xrlNumFormulario, Me.XrLabel2, Me.xrlVentaACuenta, Me.xrlGiro, Me.xrlPedido, Me.xrlCodigo, Me.xrlDiasCredito, Me.xrlDepto, Me.xrlMunic, Me.xrlNit, Me.xrlDireccion, Me.xrlFecha, Me.xrlNrc, Me.xrlNombre, Me.xrlFormaPago, Me.xrlNumero, Me.xrlVendedor, Me.xrlCESC2, Me.xrlIvaRetenido2, Me.xrlIva2, Me.label13, Me.xrlCantLetras2, Me.label14, Me.label15, Me.label16, Me.label17, Me.xrlComentario2, Me.xrlTelefono, Me.xrlCESC, Me.xrlIvaRetenido, Me.xrlIva, Me.label34, Me.label35, Me.label36, Me.label37, Me.label38, Me.label39, Me.xrlComentario, Me.label40, Me.label41, Me.label42, Me.label43, Me.label44, Me.label45, Me.label46, Me.xrlDireccion2, Me.xrlSucursal2, Me.xrlMunic2, Me.xrlFormaPago2, Me.xrlVendedor2, Me.xrlTelefono2, Me.xrlNombre2, Me.xrlNrc2, Me.xrlFecha2, Me.xrlNit2, Me.xrlDiasCredito2, Me.xrlCodigo2, Me.xrlPedido2, Me.xrlGiro2, Me.xrlVentaACuenta2, Me.xrlNumFormulario2, Me.xrlNumRemision2, Me.xrlFechaImpresion2, Me.xrlDepto2, Me.XrLabel21, Me.xrlNumero2})
        Me.TopMargin.Dpi = 100.0!
        Me.TopMargin.HeightF = 48.66669!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaNoSujeta
        '
        Me.xrlVentaNoSujeta.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlVentaNoSujeta.Dpi = 100.0!
        Me.xrlVentaNoSujeta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaNoSujeta.LocationFloat = New DevExpress.Utils.PointFloat(685.88!, 919.25!)
        Me.xrlVentaNoSujeta.Name = "xrlVentaNoSujeta"
        Me.xrlVentaNoSujeta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaNoSujeta.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.xrlVentaNoSujeta.StylePriority.UseBorders = False
        Me.xrlVentaNoSujeta.StylePriority.UseFont = False
        Me.xrlVentaNoSujeta.StylePriority.UseTextAlignment = False
        Me.xrlVentaNoSujeta.Text = "0.00"
        Me.xrlVentaNoSujeta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlNit
        '
        Me.xrlNit.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNit.Dpi = 100.0!
        Me.xrlNit.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.xrlNit.LocationFloat = New DevExpress.Utils.PointFloat(138.334!, 21.12503!)
        Me.xrlNit.Name = "xrlNit"
        Me.xrlNit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNit.SizeF = New System.Drawing.SizeF(133.0!, 12.0!)
        Me.xrlNit.StylePriority.UseBorders = False
        Me.xrlNit.StylePriority.UseFont = False
        Me.xrlNit.StylePriority.UseTextAlignment = False
        Me.xrlNit.Text = "0614-251107-107-7"
        Me.xrlNit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label5
        '
        Me.label5.Dpi = 100.0!
        Me.label5.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.LocationFloat = New DevExpress.Utils.PointFloat(644.58!, 111.0!)
        Me.label5.Name = "label5"
        Me.label5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label5.SizeF = New System.Drawing.SizeF(65.67!, 15.0!)
        Me.label5.StylePriority.UseFont = False
        Me.label5.StylePriority.UseTextAlignment = False
        Me.label5.Text = "[Mes]"
        Me.label5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label30
        '
        Me.label30.Dpi = 100.0!
        Me.label30.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.label30.LocationFloat = New DevExpress.Utils.PointFloat(97.88!, 677.82!)
        Me.label30.Name = "label30"
        Me.label30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label30.SizeF = New System.Drawing.SizeF(132.0001!, 15.00002!)
        Me.label30.StylePriority.UseFont = False
        Me.label30.StylePriority.UseTextAlignment = False
        Me.label30.Text = "SAN SALVADOR"
        Me.label30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNumFormulario2
        '
        Me.xrlNumFormulario2.Dpi = 100.0!
        Me.xrlNumFormulario2.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlNumFormulario2.LocationFloat = New DevExpress.Utils.PointFloat(532.2703!, 22.31258!)
        Me.xrlNumFormulario2.Name = "xrlNumFormulario2"
        Me.xrlNumFormulario2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFormulario2.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlNumFormulario2.StylePriority.UseFont = False
        Me.xrlNumFormulario2.StylePriority.UseTextAlignment = False
        Me.xrlNumFormulario2.Text = "1945"
        Me.xrlNumFormulario2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlNumFormulario2.Visible = False
        '
        'xrlSubTotal2
        '
        Me.xrlSubTotal2.Dpi = 100.0!
        Me.xrlSubTotal2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlSubTotal2.LocationFloat = New DevExpress.Utils.PointFloat(685.88!, 371.29!)
        Me.xrlSubTotal2.Name = "xrlSubTotal2"
        Me.xrlSubTotal2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSubTotal2.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.xrlSubTotal2.StylePriority.UseFont = False
        Me.xrlSubTotal2.StylePriority.UseTextAlignment = False
        Me.xrlSubTotal2.Text = "0.00"
        Me.xrlSubTotal2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'label26
        '
        Me.label26.Dpi = 100.0!
        Me.label26.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label26.LocationFloat = New DevExpress.Utils.PointFloat(589.621!, 639.8159!)
        Me.label26.Name = "label26"
        Me.label26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label26.SizeF = New System.Drawing.SizeF(65.67!, 15.0!)
        Me.label26.StylePriority.UseFont = False
        Me.label26.StylePriority.UseTextAlignment = False
        Me.label26.Text = "[Dia]"
        Me.label26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVendedor2
        '
        Me.xrlVendedor2.Dpi = 100.0!
        Me.xrlVendedor2.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVendedor2.LocationFloat = New DevExpress.Utils.PointFloat(133.2917!, 23.85419!)
        Me.xrlVendedor2.Name = "xrlVendedor2"
        Me.xrlVendedor2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVendedor2.SizeF = New System.Drawing.SizeF(181.1667!, 12.00002!)
        Me.xrlVendedor2.StylePriority.UseFont = False
        Me.xrlVendedor2.StylePriority.UseTextAlignment = False
        Me.xrlVendedor2.Text = "xrlVendedor"
        Me.xrlVendedor2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlVendedor2.Visible = False
        '
        'xrlFormaPago2
        '
        Me.xrlFormaPago2.Dpi = 100.0!
        Me.xrlFormaPago2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlFormaPago2.LocationFloat = New DevExpress.Utils.PointFloat(108.6461!, 8.312386!)
        Me.xrlFormaPago2.Name = "xrlFormaPago2"
        Me.xrlFormaPago2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFormaPago2.SizeF = New System.Drawing.SizeF(64.62469!, 13.00002!)
        Me.xrlFormaPago2.StylePriority.UseFont = False
        Me.xrlFormaPago2.StylePriority.UseTextAlignment = False
        Me.xrlFormaPago2.Text = "CONTADO"
        Me.xrlFormaPago2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlFormaPago2.Visible = False
        '
        'label41
        '
        Me.label41.Dpi = 100.0!
        Me.label41.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.label41.LocationFloat = New DevExpress.Utils.PointFloat(57.10449!, 17.66663!)
        Me.label41.Name = "label41"
        Me.label41.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label41.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.label41.StylePriority.UseFont = False
        Me.label41.StylePriority.UseTextAlignment = False
        Me.label41.Text = "25/12/2012"
        Me.label41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCon
        '
        Me.xrlCon.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.Contado")})
        Me.xrlCon.Dpi = 100.0!
        Me.xrlCon.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlCon.LocationFloat = New DevExpress.Utils.PointFloat(659.04!, 168.0!)
        Me.xrlCon.Name = "xrlCon"
        Me.xrlCon.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCon.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.xrlCon.StylePriority.UseFont = False
        Me.xrlCon.StylePriority.UseTextAlignment = False
        Me.xrlCon.Text = "xrlCon"
        Me.xrlCon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'label14
        '
        Me.label14.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.label14.Dpi = 100.0!
        Me.label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.label14.LocationFloat = New DevExpress.Utils.PointFloat(513.5421!, 17.0!)
        Me.label14.Name = "label14"
        Me.label14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label14.SizeF = New System.Drawing.SizeF(60.99994!, 13.99998!)
        Me.label14.StylePriority.UseBorders = False
        Me.label14.StylePriority.UseFont = False
        Me.label14.StylePriority.UseTextAlignment = False
        Me.label14.Text = "0.00"
        Me.label14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.label14.Visible = False
        '
        'label21
        '
        Me.label21.Dpi = 100.0!
        Me.label21.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label21.LocationFloat = New DevExpress.Utils.PointFloat(589.621!, 656.82!)
        Me.label21.Name = "label21"
        Me.label21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label21.SizeF = New System.Drawing.SizeF(143.5395!, 15.0!)
        Me.label21.StylePriority.UseFont = False
        Me.label21.StylePriority.UseTextAlignment = False
        Me.label21.Text = "[CorreoElectronico]"
        Me.label21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label23
        '
        Me.label23.Dpi = 100.0!
        Me.label23.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label23.LocationFloat = New DevExpress.Utils.PointFloat(662.67!, 696.816!)
        Me.label23.Name = "label23"
        Me.label23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label23.SizeF = New System.Drawing.SizeF(37.57831!, 15.0!)
        Me.label23.StylePriority.UseFont = False
        Me.label23.StylePriority.UseTextAlignment = False
        Me.label23.Text = "[Cheque]"
        Me.label23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'xrlCESC
        '
        Me.xrlCESC.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlCESC.Dpi = 100.0!
        Me.xrlCESC.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlCESC.LocationFloat = New DevExpress.Utils.PointFloat(191.5417!, 25.66666!)
        Me.xrlCESC.Name = "xrlCESC"
        Me.xrlCESC.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCESC.SizeF = New System.Drawing.SizeF(61.00003!, 15.99998!)
        Me.xrlCESC.StylePriority.UseBorders = False
        Me.xrlCESC.StylePriority.UseFont = False
        Me.xrlCESC.StylePriority.UseTextAlignment = False
        Me.xrlCESC.Text = "0.00"
        Me.xrlCESC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlSucursal2
        '
        Me.xrlSucursal2.Dpi = 100.0!
        Me.xrlSucursal2.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlSucursal2.LocationFloat = New DevExpress.Utils.PointFloat(532.2703!, 7.312554!)
        Me.xrlSucursal2.Name = "xrlSucursal2"
        Me.xrlSucursal2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSucursal2.SizeF = New System.Drawing.SizeF(111.4584!, 15.0!)
        Me.xrlSucursal2.StylePriority.UseFont = False
        Me.xrlSucursal2.StylePriority.UseTextAlignment = False
        Me.xrlSucursal2.Text = "xrlSucursal"
        Me.xrlSucursal2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlSucursal2.Visible = False
        '
        'xrlNit2
        '
        Me.xrlNit2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNit2.Dpi = 100.0!
        Me.xrlNit2.Font = New System.Drawing.Font("Arial", 7.5!)
        Me.xrlNit2.LocationFloat = New DevExpress.Utils.PointFloat(395.7283!, 14.3124!)
        Me.xrlNit2.Name = "xrlNit2"
        Me.xrlNit2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNit2.SizeF = New System.Drawing.SizeF(133.0!, 12.0!)
        Me.xrlNit2.StylePriority.UseBorders = False
        Me.xrlNit2.StylePriority.UseFont = False
        Me.xrlNit2.StylePriority.UseTextAlignment = False
        Me.xrlNit2.Text = "0614-251107-107-7"
        Me.xrlNit2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlNit2.Visible = False
        '
        'label17
        '
        Me.label17.Dpi = 100.0!
        Me.label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.label17.LocationFloat = New DevExpress.Utils.PointFloat(356.0003!, 16.66663!)
        Me.label17.Name = "label17"
        Me.label17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label17.SizeF = New System.Drawing.SizeF(61.00003!, 16.0!)
        Me.label17.StylePriority.UseFont = False
        Me.label17.StylePriority.UseTextAlignment = False
        Me.label17.Text = "0.00"
        Me.label17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.label17.Visible = False
        '
        'label38
        '
        Me.label38.Dpi = 100.0!
        Me.label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.label38.LocationFloat = New DevExpress.Utils.PointFloat(668.959!, 32.66669!)
        Me.label38.Name = "label38"
        Me.label38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label38.SizeF = New System.Drawing.SizeF(61.00003!, 16.0!)
        Me.label38.StylePriority.UseFont = False
        Me.label38.StylePriority.UseTextAlignment = False
        Me.label38.Text = "0.00"
        Me.label38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCantLetras2
        '
        Me.xrlCantLetras2.Dpi = 100.0!
        Me.xrlCantLetras2.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlCantLetras2.LocationFloat = New DevExpress.Utils.PointFloat(69.64687!, 0.0!)
        Me.xrlCantLetras2.Name = "xrlCantLetras2"
        Me.xrlCantLetras2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCantLetras2.SizeF = New System.Drawing.SizeF(272.1669!, 14.0!)
        Me.xrlCantLetras2.StylePriority.UseFont = False
        Me.xrlCantLetras2.StylePriority.UseTextAlignment = False
        Me.xrlCantLetras2.Text = "DIEZ"
        Me.xrlCantLetras2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlFormaPago
        '
        Me.xrlFormaPago.Dpi = 100.0!
        Me.xrlFormaPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlFormaPago.LocationFloat = New DevExpress.Utils.PointFloat(275.4795!, 22.6666!)
        Me.xrlFormaPago.Name = "xrlFormaPago"
        Me.xrlFormaPago.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFormaPago.SizeF = New System.Drawing.SizeF(64.62469!, 13.00002!)
        Me.xrlFormaPago.StylePriority.UseFont = False
        Me.xrlFormaPago.StylePriority.UseTextAlignment = False
        Me.xrlFormaPago.Text = "CONTADO"
        Me.xrlFormaPago.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlIva2
        '
        Me.xrlIva2.Dpi = 100.0!
        Me.xrlIva2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlIva2.LocationFloat = New DevExpress.Utils.PointFloat(564.6874!, 13.66663!)
        Me.xrlIva2.Name = "xrlIva2"
        Me.xrlIva2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIva2.SizeF = New System.Drawing.SizeF(87.75!, 19.0!)
        Me.xrlIva2.StylePriority.UseFont = False
        Me.xrlIva2.StylePriority.UseTextAlignment = False
        Me.xrlIva2.Text = "0.00"
        Me.xrlIva2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.xrlIva2.Visible = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport2, Me.XrSubreport1, Me.label30, Me.label3, Me.xrlVentaNoSujeta, Me.xrlVentaExenta, Me.xrlSubTotal, Me.xrlVentaAfecta, Me.xrlTotal, Me.xrlCantLetras, Me.label33, Me.label20, Me.label21, Me.label22, Me.label23, Me.label24, Me.label25, Me.label26, Me.label27, Me.label28, Me.label29, Me.label31, Me.label32, Me.xrlVentaNoSujeta2, Me.xrlVentaExenta2, Me.xrlSubTotal2, Me.xrlVentaAfecta2, Me.xrlTotal2, Me.label18, Me.label19, Me.label1, Me.label2, Me.label4, Me.label6, Me.label5, Me.label7, Me.label8, Me.label9, Me.label10, Me.xrlCon, Me.label11, Me.label12})
        Me.ReportHeader.Dpi = 100.0!
        Me.ReportHeader.HeightF = 996.7501!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xrlNumero
        '
        Me.xrlNumero.Dpi = 100.0!
        Me.xrlNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlNumero.LocationFloat = New DevExpress.Utils.PointFloat(491.1248!, 19.66672!)
        Me.xrlNumero.Name = "xrlNumero"
        Me.xrlNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero.SizeF = New System.Drawing.SizeF(79.04175!, 15.0!)
        Me.xrlNumero.StylePriority.UseFont = False
        Me.xrlNumero.StylePriority.UseTextAlignment = False
        Me.xrlNumero.Text = "1945"
        Me.xrlNumero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label31
        '
        Me.label31.Dpi = 100.0!
        Me.label31.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label31.LocationFloat = New DevExpress.Utils.PointFloat(97.88!, 656.82!)
        Me.label31.Name = "label31"
        Me.label31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label31.SizeF = New System.Drawing.SizeF(400.2479!, 15.0!)
        Me.label31.StylePriority.UseFont = False
        Me.label31.StylePriority.UseTextAlignment = False
        Me.label31.Text = "RESIDENCIAL CLAUDIA PASAJE AIDA No.19"
        Me.label31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlComentario
        '
        Me.xrlComentario.Dpi = 100.0!
        Me.xrlComentario.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlComentario.LocationFloat = New DevExpress.Utils.PointFloat(197.8542!, 13.99997!)
        Me.xrlComentario.Name = "xrlComentario"
        Me.xrlComentario.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlComentario.SizeF = New System.Drawing.SizeF(265.0416!, 17.00001!)
        Me.xrlComentario.StylePriority.UseFont = False
        Me.xrlComentario.StylePriority.UseTextAlignment = False
        Me.xrlComentario.Text = "Comentario"
        Me.xrlComentario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlComentario.Visible = False
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 100.0!
        Me.BottomMargin.HeightF = 23.20836!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCantLetras
        '
        Me.xrlCantLetras.Dpi = 100.0!
        Me.xrlCantLetras.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlCantLetras.LocationFloat = New DevExpress.Utils.PointFloat(189.25!, 927.2501!)
        Me.xrlCantLetras.Name = "xrlCantLetras"
        Me.xrlCantLetras.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCantLetras.SizeF = New System.Drawing.SizeF(409.7072!, 15.0!)
        Me.xrlCantLetras.StylePriority.UseFont = False
        Me.xrlCantLetras.StylePriority.UseTextAlignment = False
        Me.xrlCantLetras.Text = "DIEZ"
        Me.xrlCantLetras.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlTotal
        '
        Me.xrlTotal.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlTotal.Dpi = 100.0!
        Me.xrlTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlTotal.LocationFloat = New DevExpress.Utils.PointFloat(685.88!, 952.25!)
        Me.xrlTotal.Name = "xrlTotal"
        Me.xrlTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal.SizeF = New System.Drawing.SizeF(76.12!, 15.0!)
        Me.xrlTotal.StylePriority.UseBorders = False
        Me.xrlTotal.StylePriority.UseFont = False
        Me.xrlTotal.StylePriority.UseTextAlignment = False
        Me.xrlTotal.Text = "0.00"
        Me.xrlTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'label39
        '
        Me.label39.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.label39.Dpi = 100.0!
        Me.label39.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label39.LocationFloat = New DevExpress.Utils.PointFloat(336.3333!, 27.29178!)
        Me.label39.Name = "label39"
        Me.label39.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label39.SizeF = New System.Drawing.SizeF(61.00003!, 15.99998!)
        Me.label39.StylePriority.UseBorders = False
        Me.label39.StylePriority.UseFont = False
        Me.label39.StylePriority.UseTextAlignment = False
        Me.label39.Text = "0.00"
        Me.label39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.label39.Visible = False
        '
        'xrlCodigo2
        '
        Me.xrlCodigo2.Dpi = 100.0!
        Me.xrlCodigo2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlCodigo2.LocationFloat = New DevExpress.Utils.PointFloat(106.1043!, 20.3124!)
        Me.xrlCodigo2.Name = "xrlCodigo2"
        Me.xrlCodigo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCodigo2.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlCodigo2.StylePriority.UseFont = False
        Me.xrlCodigo2.StylePriority.UseTextAlignment = False
        Me.xrlCodigo2.Text = "xrlCodigo"
        Me.xrlCodigo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlCodigo2.Visible = False
        '
        'xrlIvaRetenido2
        '
        Me.xrlIvaRetenido2.Dpi = 100.0!
        Me.xrlIvaRetenido2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.xrlIvaRetenido2.LocationFloat = New DevExpress.Utils.PointFloat(476.1458!, 19.66669!)
        Me.xrlIvaRetenido2.Name = "xrlIvaRetenido2"
        Me.xrlIvaRetenido2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIvaRetenido2.SizeF = New System.Drawing.SizeF(61.00003!, 13.875!)
        Me.xrlIvaRetenido2.StylePriority.UseFont = False
        Me.xrlIvaRetenido2.StylePriority.UseTextAlignment = False
        Me.xrlIvaRetenido2.Text = "0.00"
        Me.xrlIvaRetenido2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.xrlIvaRetenido2.Visible = False
        '
        'xrlNumero2
        '
        Me.xrlNumero2.Dpi = 100.0!
        Me.xrlNumero2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlNumero2.LocationFloat = New DevExpress.Utils.PointFloat(334.2078!, 9.312393!)
        Me.xrlNumero2.Name = "xrlNumero2"
        Me.xrlNumero2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero2.SizeF = New System.Drawing.SizeF(79.04175!, 15.0!)
        Me.xrlNumero2.StylePriority.UseFont = False
        Me.xrlNumero2.StylePriority.UseTextAlignment = False
        Me.xrlNumero2.Text = "1945"
        Me.xrlNumero2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlNumero2.Visible = False
        '
        'xrlMunic
        '
        Me.xrlMunic.Dpi = 100.0!
        Me.xrlMunic.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlMunic.LocationFloat = New DevExpress.Utils.PointFloat(125.0003!, 4.666626!)
        Me.xrlMunic.Name = "xrlMunic"
        Me.xrlMunic.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlMunic.SizeF = New System.Drawing.SizeF(132.0001!, 15.00002!)
        Me.xrlMunic.StylePriority.UseFont = False
        Me.xrlMunic.StylePriority.UseTextAlignment = False
        Me.xrlMunic.Text = "SAN SALVADOR"
        Me.xrlMunic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label8
        '
        Me.label8.CanGrow = False
        Me.label8.Dpi = 100.0!
        Me.label8.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label8.LocationFloat = New DevExpress.Utils.PointFloat(620.621!, 149.0!)
        Me.label8.Name = "label8"
        Me.label8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label8.SizeF = New System.Drawing.SizeF(141.379!, 15.0!)
        Me.label8.StylePriority.UseFont = False
        Me.label8.StylePriority.UseTextAlignment = False
        Me.label8.Text = "[VentaAcuentaDe]"
        Me.label8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'label15
        '
        Me.label15.Dpi = 100.0!
        Me.label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.label15.LocationFloat = New DevExpress.Utils.PointFloat(383.4374!, 6.791687!)
        Me.label15.Name = "label15"
        Me.label15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label15.SizeF = New System.Drawing.SizeF(61.00003!, 12.875!)
        Me.label15.StylePriority.UseFont = False
        Me.label15.StylePriority.UseTextAlignment = False
        Me.label15.Text = "0.00"
        Me.label15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'label18
        '
        Me.label18.Dpi = 100.0!
        Me.label18.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label18.LocationFloat = New DevExpress.Utils.PointFloat(189.25!, 415.2917!)
        Me.label18.Name = "label18"
        Me.label18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.label18.SizeF = New System.Drawing.SizeF(409.7072!, 15.0!)
        Me.label18.StylePriority.UseFont = False
        Me.label18.StylePriority.UseTextAlignment = False
        Me.label18.Text = "DIEZ"
        Me.label18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrSubreport2
        '
        Me.XrSubreport2.Dpi = 100.0!
        Me.XrSubreport2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 733.6249!)
        Me.XrSubreport2.Name = "XrSubreport2"
        Me.XrSubreport2.ReportSource = New Nexus.fac_rptPlanFacturaDetalle()
        Me.XrSubreport2.SizeF = New System.Drawing.SizeF(770.0!, 150.6251!)
        '
        'XrSubreport1
        '
        Me.XrSubreport1.Dpi = 100.0!
        Me.XrSubreport1.LocationFloat = New DevExpress.Utils.PointFloat(0.0000186995!, 209.3817!)
        Me.XrSubreport1.Name = "XrSubreport1"
        Me.XrSubreport1.ReportSource = New Nexus.fac_rptPlanFacturaDetalle()
        Me.XrSubreport1.SizeF = New System.Drawing.SizeF(770.0!, 148.91!)
        '
        'fac_rptFactura
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader})
        Me.DataMember = "VentasDetalle"
        Me.DrawGrid = False
        Me.Margins = New System.Drawing.Printing.Margins(40, 40, 49, 23)
        Me.SnapToGrid = False
        Me.Version = "16.1"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents xrlDiasCredito As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNombre2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDireccion2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label27 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label32 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlMunic2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label44 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrSubreport2 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents label37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaNoSujeta2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaACuenta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFechaImpresion2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label28 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDireccion As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFormulario As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNombre As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label43 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTelefono As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumRemision2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDepto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDepto2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlGiro2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFecha As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlComentario2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label33 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaAfecta2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPedido As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaAfecta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFechaImpresion As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNrc As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlGiro As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label46 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label42 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVendedor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCESC2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCodigo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSubTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIva As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents label9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFecha2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDiasCredito2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSucursal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label35 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label34 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumRemision As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIvaRetenido As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTelefono2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNrc2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label24 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPedido2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrSubreport1 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents label45 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaACuenta2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label40 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents xrlMunic As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNit As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFormaPago As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumero As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIvaRetenido2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIva2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCantLetras2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCESC As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label38 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label39 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlComentario As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label41 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSucursal2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFormaPago2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVendedor2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNit2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCodigo2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFormulario2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumero2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaNoSujeta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSubTotal2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCon As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label23 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents xrlTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCantLetras As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label31 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents label8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
End Class
