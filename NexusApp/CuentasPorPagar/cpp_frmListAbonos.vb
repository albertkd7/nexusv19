﻿Imports NexusBLL

Public Class cpp_frmListAbonos
    Dim bl As New CuentasPPBLL(g_ConnectionString)

    Private Sub cpp_frmListAbonos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = CDate(String.Format("01/{0}/{1}", Today.Month, Today.Year))
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        BeProveedor1.teNombre.EditValue = "-- DEJAR EN BLANCO EL CÓDIGO DE PROVEEDOR PARA GENERAR TODOS --"
    End Sub

    Private Sub cpp_frmListAbonos_Reporte() Handles Me.Reporte
        Dim dt = bl.cpp_ListadoAbonosPorProveedor(deDesde.EditValue, deHasta.EditValue, BeProveedor1.beCodigo.EditValue, rgTipo.EditValue, leSucursal.EditValue)
        Dim rpt As New cpp_rptListAbonos() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue + " SUCURSAL: " + leSucursal.Text
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.ShowPreviewDialog()
    End Sub
End Class
