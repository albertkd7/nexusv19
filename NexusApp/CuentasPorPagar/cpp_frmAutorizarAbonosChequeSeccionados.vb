﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class cpp_frmAutorizarAbonosChequeSeccionados
    Dim bl As New CuentasPPBLL(g_ConnectionString)
    Dim blBank As New BancosBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim ValorCheque As Decimal = 0
    Dim SumAbonar As Decimal = 0

    Private Sub cpp_frmAutorizarAbonos_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        objCombos.conTiposPartida(leTipoPartida, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.banCuentasBancarias(leCtaBancaria)
        leSucursal.EditValue = piIdSucursalUsuario
        gc.DataSource = bl.cpp_ObtenerAbonosPendientes(leSucursal.EditValue)
        deFecha.EditValue = Today
    End Sub

    Private Sub gc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc.Click
        'teANombreDe.EditValue = gv.GetRowCellValue(gv.FocusedRowHandle, "Nombre")
        'meConcepto.EditValue = gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto")    '"ABONO REALIZADO ÉSTE DÍA, " &
        'chkAplica_CheckedChanged("", New EventArgs)
    End Sub



    Private Sub btAplicar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btAplicar.Click

        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "pagar") = True Then
                ValorCheque = ValorCheque + gv.GetRowCellValue(i, "TotalAbono")
            End If
        Next

        If teANombreDe.EditValue = "" Or meConcepto.EditValue = "" Or ValorCheque = 0 Then
            If ValorCheque = 0 Then
                MsgBox("Debe autorizar a menos un abono para poder aplicar" + Chr(13) + "Verifique columna [¿Pagar?]", MsgBoxStyle.Critical, "Nota")
            Else
                MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Nombre], [Concepto]", MsgBoxStyle.Critical, "Nota")
            End If
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de aplicar y autorizar este abono?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        If rgFormaPago.EditValue = 0 Then
            InsertarCheque()
        End If

    End Sub

    Private Sub InsertarCheque()
        Dim Cheque = New ban_Cheques, sConcepto As String = "", ChequeDetalle As New List(Of ban_ChequesDetalle)
        sConcepto = " Comprobante " & bl.cpp_ObtenerDocumentosAbonados(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProveedor"), gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"))
        With Cheque
            .IdCuentaBancaria = leCtaBancaria.EditValue
            .Numero = blBank.GetNumeroCheque(leCtaBancaria.EditValue)
            .Fecha = deFecha.DateTime
            .Valor = ValorCheque
            .AnombreDe = teANombreDe.EditValue
            .Concepto = meConcepto.EditValue
            .IdProveedor = gv.GetRowCellValue(gv.FocusedRowHandle, "IdProveedor")
            .IdSucursal = gv.GetRowCellValue(gv.FocusedRowHandle, "IdSucursal")
            .IdTipoPartida = leTipoPartida.EditValue
            .Impreso = False
            .Conciliado = False
            .Anulado = False
            .MotivoAnulacion = ""
            .PagadoBanco = True
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .FechaHoraModificacion = Nothing
            .ModificadoPor = ""
        End With
        Dim entCta As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(Cheque.IdCuentaBancaria)
        Dim entPro As com_Proveedores = objTablas.com_ProveedoresSelectByPK(Cheque.IdProveedor)

        Dim entDet As New ban_ChequesDetalle

        Dim correl As Integer = 1
        For j = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(j, "pagar") = True Then
                entDet = New ban_ChequesDetalle
                With entDet
                    .IdCheque = 0
                    .IdDetalle = correl
                    .IdCuenta = entPro.IdCuentaContable   'la cuenta por pagar al proveedor
                    .Referencia = gv.GetRowCellValue(j, "NumeroComprobante")
                    .Concepto = "Cheque " + Cheque.Numero + ". " + teANombreDe.EditValue + " " + gv.GetRowCellValue(j, "Concepto")
                    .Debe = gv.GetRowCellValue(j, "TotalAbono") 'el total a pagar al proveedor
                    .Haber = 0.0
                    .IdCentro = ""
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                End With
                ChequeDetalle.Add(entDet)
                correl += 1
            End If
        Next

        entDet = New ban_ChequesDetalle  'agregando el haber al banco
        With entDet
            .IdCheque = 0
            .IdDetalle = correl
            .IdCuenta = entCta.IdCuentaContable 'la cuenta por pagar al proveedor
            .Referencia = gv.GetRowCellValue(gv.FocusedRowHandle, "NumeroComprobante")
            .Concepto = "Cheque " + Cheque.Numero + ". " + teANombreDe.EditValue + " " + meConcepto.EditValue
            .Debe = 0.0
            .Haber = ValorCheque  'el total a pagar al proveedor
            .IdCentro = ""
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        ChequeDetalle.Add(entDet)
        Dim Contabilizar As Boolean = True 'gsNombre_Empresa.StartsWith("PITUTA")
        Dim msj As String = blBank.ban_InsertarCheque(Cheque, ChequeDetalle, Contabilizar)
        If msj = "Ok" Then
            For i = 0 To gv.DataRowCount - 1
                If gv.GetRowCellValue(i, "pagar") = True Then
                    bl.cpp_AutorizaAbonosCheque(gv.GetRowCellValue(i, "IdComprobante"), Cheque.IdCheque, objMenu.User)
                End If
            Next
            MsgBox("El cheque fue guardado con éxito", 64, "Nota")
        Else
            MsgBox("NO FUE POSIBLE GUARDAR EL CHEQUE" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
        End If
        Close()
    End Sub


    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        gc.DataSource = bl.cpp_ObtenerAbonosPendientes(leSucursal.EditValue)
    End Sub

    Private Sub chkAplica_CheckedChanged(sender As Object, e As EventArgs) Handles chkAplica.CheckedChanged
        If gv.FocusedColumn.FieldName = "pagar" Then
            gv.SetFocusedRowCellValue("pagar", SiEsNulo(gv.EditingValue, False))
        End If
    End Sub

    Private Sub chkAplica_CheckStateChanged(sender As Object, e As EventArgs) Handles chkAplica.CheckStateChanged
        SumAbonar = 0
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "pagar") = True Then
                SumAbonar += gv.GetRowCellValue(i, "TotalAbono")
            End If
        Next
        txtTotalAbono.EditValue = String.Format("{0:###,###,##0.00}", SumAbonar)
    End Sub
End Class
