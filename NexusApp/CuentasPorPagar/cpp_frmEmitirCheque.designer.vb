﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpp_frmEmitirCheque
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cpp_frmEmitirCheque))
        Me.btAplicar = New DevExpress.XtraEditors.SimpleButton
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.leCtaBancaria = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.meConcepto = New DevExpress.XtraEditors.MemoEdit
        Me.teANombreDe = New DevExpress.XtraEditors.TextEdit
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teANombreDe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btAplicar
        '
        Me.btAplicar.Location = New System.Drawing.Point(107, 130)
        Me.btAplicar.Name = "btAplicar"
        Me.btAplicar.Size = New System.Drawing.Size(98, 23)
        Me.btAplicar.TabIndex = 26
        Me.btAplicar.Text = "Proceder"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(2, 27)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl1.TabIndex = 21
        Me.LabelControl1.Text = "Banco/Cta. Bancaria:"
        '
        'leCtaBancaria
        '
        Me.leCtaBancaria.EnterMoveNextControl = True
        Me.leCtaBancaria.Location = New System.Drawing.Point(107, 24)
        Me.leCtaBancaria.Name = "leCtaBancaria"
        Me.leCtaBancaria.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCtaBancaria.Size = New System.Drawing.Size(474, 20)
        Me.leCtaBancaria.TabIndex = 20
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(37, 48)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(67, 13)
        Me.LabelControl2.TabIndex = 24
        Me.LabelControl2.Text = "A Nombre De:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(54, 70)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl3.TabIndex = 25
        Me.LabelControl3.Text = "Concepto:"
        '
        'meConcepto
        '
        Me.meConcepto.EnterMoveNextControl = True
        Me.meConcepto.Location = New System.Drawing.Point(107, 66)
        Me.meConcepto.Name = "meConcepto"
        Me.meConcepto.Size = New System.Drawing.Size(474, 48)
        Me.meConcepto.TabIndex = 23
        '
        'teANombreDe
        '
        Me.teANombreDe.EnterMoveNextControl = True
        Me.teANombreDe.Location = New System.Drawing.Point(107, 45)
        Me.teANombreDe.Name = "teANombreDe"
        Me.teANombreDe.Size = New System.Drawing.Size(474, 20)
        Me.teANombreDe.TabIndex = 22
        '
        'cpp_frmEmitirCheque
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(602, 185)
        Me.Controls.Add(Me.btAplicar)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.leCtaBancaria)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.meConcepto)
        Me.Controls.Add(Me.teANombreDe)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "cpp_frmEmitirCheque"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Emitir Cheque para proveedor"
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teANombreDe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btAplicar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCtaBancaria As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents teANombreDe As DevExpress.XtraEditors.TextEdit
End Class
