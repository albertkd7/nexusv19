﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpp_frmTraslado
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.teTotalTraslado = New DevExpress.XtraEditors.TextEdit()
        Me.txtConcepto = New DevExpress.XtraEditors.TextEdit()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colAbonar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkAbonar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.colNumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFechaCompra = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFechaVencto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTotalCompra = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSaldo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcMontoAbonar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIdComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIdImportacion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutConverter1 = New DevExpress.XtraLayout.Converter.LayoutConverter(Me.components)
        Me.gcHeader = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.BeProveedor1 = New Nexus.beProveedor()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.btObtenerData = New DevExpress.XtraEditors.SimpleButton()
        Me.beProveedor = New Nexus.beProveedor()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.leTipoPartida = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.teTotalTraslado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAbonar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcHeader.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'teTotalTraslado
        '
        Me.teTotalTraslado.EditValue = "0.0"
        Me.teTotalTraslado.Location = New System.Drawing.Point(622, 6)
        Me.teTotalTraslado.Name = "teTotalTraslado"
        Me.teTotalTraslado.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotalTraslado.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotalTraslado.Properties.Mask.EditMask = "n2"
        Me.teTotalTraslado.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotalTraslado.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTotalTraslado.Properties.ReadOnly = True
        Me.teTotalTraslado.Size = New System.Drawing.Size(118, 20)
        Me.teTotalTraslado.TabIndex = 22
        '
        'txtConcepto
        '
        Me.txtConcepto.EnterMoveNextControl = True
        Me.txtConcepto.Location = New System.Drawing.Point(124, 97)
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.Size = New System.Drawing.Size(535, 20)
        Me.txtConcepto.TabIndex = 7
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 122)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkAbonar})
        Me.gc.Size = New System.Drawing.Size(745, 225)
        Me.gc.TabIndex = 2
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colAbonar, Me.colNumero, Me.colFechaCompra, Me.colFechaVencto, Me.colTotalCompra, Me.colSaldo, Me.gcMontoAbonar, Me.colIdComprobante, Me.colIdImportacion})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'colAbonar
        '
        Me.colAbonar.Caption = "Trasladar?"
        Me.colAbonar.ColumnEdit = Me.chkAbonar
        Me.colAbonar.FieldName = "Abonar"
        Me.colAbonar.Name = "colAbonar"
        Me.colAbonar.Visible = True
        Me.colAbonar.VisibleIndex = 0
        Me.colAbonar.Width = 64
        '
        'chkAbonar
        '
        Me.chkAbonar.AutoHeight = False
        Me.chkAbonar.Name = "chkAbonar"
        '
        'colNumero
        '
        Me.colNumero.Caption = "No. Dcto.Compra"
        Me.colNumero.FieldName = "Numero"
        Me.colNumero.Name = "colNumero"
        Me.colNumero.OptionsColumn.AllowEdit = False
        Me.colNumero.OptionsColumn.AllowFocus = False
        Me.colNumero.Visible = True
        Me.colNumero.VisibleIndex = 1
        Me.colNumero.Width = 106
        '
        'colFechaCompra
        '
        Me.colFechaCompra.Caption = "Fecha de Docto."
        Me.colFechaCompra.FieldName = "FechaCompra"
        Me.colFechaCompra.Name = "colFechaCompra"
        Me.colFechaCompra.OptionsColumn.AllowEdit = False
        Me.colFechaCompra.OptionsColumn.AllowFocus = False
        Me.colFechaCompra.Visible = True
        Me.colFechaCompra.VisibleIndex = 2
        Me.colFechaCompra.Width = 107
        '
        'colFechaVencto
        '
        Me.colFechaVencto.Caption = "Fecha de Vencto."
        Me.colFechaVencto.FieldName = "FechaVencto"
        Me.colFechaVencto.Name = "colFechaVencto"
        Me.colFechaVencto.OptionsColumn.AllowEdit = False
        Me.colFechaVencto.OptionsColumn.AllowFocus = False
        Me.colFechaVencto.Visible = True
        Me.colFechaVencto.VisibleIndex = 3
        Me.colFechaVencto.Width = 107
        '
        'colTotalCompra
        '
        Me.colTotalCompra.Caption = "Total del Docto."
        Me.colTotalCompra.FieldName = "TotalCompra"
        Me.colTotalCompra.Name = "colTotalCompra"
        Me.colTotalCompra.OptionsColumn.AllowEdit = False
        Me.colTotalCompra.OptionsColumn.AllowFocus = False
        Me.colTotalCompra.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCompra", "{0:n2}")})
        Me.colTotalCompra.Visible = True
        Me.colTotalCompra.VisibleIndex = 4
        Me.colTotalCompra.Width = 107
        '
        'colSaldo
        '
        Me.colSaldo.Caption = "Saldo del Docto."
        Me.colSaldo.FieldName = "SaldoCompra"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.OptionsColumn.AllowEdit = False
        Me.colSaldo.OptionsColumn.AllowFocus = False
        Me.colSaldo.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SaldoCompra", "{0:n2}")})
        Me.colSaldo.Visible = True
        Me.colSaldo.VisibleIndex = 5
        Me.colSaldo.Width = 116
        '
        'gcMontoAbonar
        '
        Me.gcMontoAbonar.Caption = "Monto a Abonar"
        Me.gcMontoAbonar.FieldName = "MontoAbonar"
        Me.gcMontoAbonar.Name = "gcMontoAbonar"
        Me.gcMontoAbonar.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontoAbonar", "{0:n2}")})
        Me.gcMontoAbonar.Width = 117
        '
        'colIdComprobante
        '
        Me.colIdComprobante.Caption = "Id. Docto.Compra"
        Me.colIdComprobante.FieldName = "IdComprobante"
        Me.colIdComprobante.Name = "colIdComprobante"
        '
        'colIdImportacion
        '
        Me.colIdImportacion.Caption = "Id. Docto.Importacion"
        Me.colIdImportacion.FieldName = "IdImportacion"
        Me.colIdImportacion.Name = "colIdImportacion"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(124, 3)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(124, 20)
        Me.deFecha.TabIndex = 1
        '
        'gcHeader
        '
        Me.gcHeader.CaptionLocation = DevExpress.Utils.Locations.Top
        Me.gcHeader.Controls.Add(Me.leTipoPartida)
        Me.gcHeader.Controls.Add(Me.LabelControl7)
        Me.gcHeader.Controls.Add(Me.LabelControl1)
        Me.gcHeader.Controls.Add(Me.BeProveedor1)
        Me.gcHeader.Controls.Add(Me.leSucursal)
        Me.gcHeader.Controls.Add(Me.LabelControl16)
        Me.gcHeader.Controls.Add(Me.btObtenerData)
        Me.gcHeader.Controls.Add(Me.beProveedor)
        Me.gcHeader.Controls.Add(Me.LabelControl2)
        Me.gcHeader.Controls.Add(Me.deFecha)
        Me.gcHeader.Controls.Add(Me.LabelControl5)
        Me.gcHeader.Controls.Add(Me.txtConcepto)
        Me.gcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.gcHeader.Location = New System.Drawing.Point(0, 0)
        Me.gcHeader.Name = "gcHeader"
        Me.gcHeader.ShowCaption = False
        Me.gcHeader.Size = New System.Drawing.Size(758, 122)
        Me.gcHeader.TabIndex = 1
        Me.gcHeader.Text = "Root"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(8, 77)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl1.TabIndex = 88
        Me.LabelControl1.Text = "Trasladar a"
        '
        'BeProveedor1
        '
        Me.BeProveedor1.Location = New System.Drawing.Point(63, 74)
        Me.BeProveedor1.Name = "BeProveedor1"
        Me.BeProveedor1.Size = New System.Drawing.Size(608, 20)
        Me.BeProveedor1.TabIndex = 87
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(303, 3)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(208, 20)
        Me.leSucursal.TabIndex = 2
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(253, 6)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl16.TabIndex = 86
        Me.LabelControl16.Text = "Sucursal:"
        '
        'btObtenerData
        '
        Me.btObtenerData.Location = New System.Drawing.Point(124, 47)
        Me.btObtenerData.Name = "btObtenerData"
        Me.btObtenerData.Size = New System.Drawing.Size(124, 23)
        Me.btObtenerData.TabIndex = 3
        Me.btObtenerData.Text = "Obtener documentos"
        '
        'beProveedor
        '
        Me.beProveedor.Location = New System.Drawing.Point(63, 26)
        Me.beProveedor.Name = "beProveedor"
        Me.beProveedor.Size = New System.Drawing.Size(608, 20)
        Me.beProveedor.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(88, 6)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "Fecha:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(71, 100)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl5.TabIndex = 18
        Me.LabelControl5.Text = "Concepto:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(545, 8)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl6.TabIndex = 22
        Me.LabelControl6.Text = "Total Traslado:"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.teTotalTraslado)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl1.Location = New System.Drawing.Point(0, 347)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(758, 39)
        Me.PanelControl1.TabIndex = 23
        '
        'leTipoPartida
        '
        Me.leTipoPartida.EnterMoveNextControl = True
        Me.leTipoPartida.Location = New System.Drawing.Point(372, 51)
        Me.leTipoPartida.Name = "leTipoPartida"
        Me.leTipoPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPartida.Size = New System.Drawing.Size(287, 20)
        Me.leTipoPartida.TabIndex = 89
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(293, 54)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl7.TabIndex = 90
        Me.LabelControl7.Text = "Tipo de Partida:"
        '
        'cpp_frmTraslado
        '
        Me.ClientSize = New System.Drawing.Size(758, 411)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.gcHeader)
        Me.Modulo = "Cuentas por Pagar"
        Me.Name = "cpp_frmTraslado"
        Me.OptionId = "001003"
        Me.Text = "Traslado de Deuda a Proveedores"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.gcHeader, 0)
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.teTotalTraslado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAbonar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcHeader.ResumeLayout(False)
        Me.gcHeader.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents colAbonar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFechaCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFechaVencto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTotalCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcMontoAbonar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkAbonar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents teTotalTraslado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents colIdComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutConverter1 As DevExpress.XtraLayout.Converter.LayoutConverter
    Friend WithEvents gcHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents beProveedor As Nexus.beProveedor
    Friend WithEvents btObtenerData As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colIdImportacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BeProveedor1 As Nexus.beProveedor
    Friend WithEvents leTipoPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl

End Class
