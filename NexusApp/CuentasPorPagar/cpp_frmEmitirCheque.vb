﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class cpp_frmEmitirCheque
    Dim bl As New CuentasPPBLL(g_ConnectionString)
    Dim blBank As New BancosBLL(g_ConnectionString)
    Dim ChequeHeader As ban_Cheques
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Private Sub btAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAplicar.Click

        Dim entCta As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(leCtaBancaria.EditValue)

        'If MsgBox("Está seguro(a) de emitir el cheque para el proveedor?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then

        'End If

        Dim DatosCheques As String = "" & Chr(13)
        DatosCheques += "Banco: " & entCta.Nombre & Chr(13) & "# Cuenta: " & entCta.NumeroCuenta & Chr(13)
        DatosCheques += "Numero de Cheque: " & CInt(blBank.GetNumeroCheque(leCtaBancaria.EditValue))

        If MsgBox("¿Está seguro(a) de Emitir el Cheque con estos Datos? " + DatosCheques, MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim Cheque = New ban_Cheques
        Dim sConcepto As String = ", Comp. # " & bl.cpp_ObtenerDocumentosAbonados(IdProveedor, IdComprobante)
        Dim ChequeDetalle As New List(Of ban_ChequesDetalle)

        With Cheque
            .IdCuentaBancaria = leCtaBancaria.EditValue
            .Numero = blBank.GetNumeroCheque(leCtaBancaria.EditValue)
            .Fecha = Fecha
            .IdSucursal = IdSucursal
            .Valor = Valor
            .AnombreDe = teANombreDe.EditValue
            .Concepto = meConcepto.EditValue & sConcepto
            .IdProveedor = IdProveedor
            .IdTipoPartida = entcta.IdTipoPartida
            .Impreso = False
            .Conciliado = False
            .Anulado = False
            .MotivoAnulacion = ""
            .PagadoBanco = True
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .FechaHoraModificacion = Nothing
            .ModificadoPor = ""
        End With
        Dim entPro As com_Proveedores = objTablas.com_ProveedoresSelectByPK(Cheque.IdProveedor)
        Dim entDet As New ban_ChequesDetalle
        With entDet
            .IdCheque = 0
            .IdDetalle = 1
            .IdCuenta = entPro.IdCuentaContable   'la cuenta por pagar al proveedor
            .Referencia = ""
            .Concepto = entPro.Nombre + " " + sConcepto + ", Cheque No.  " + Cheque.Numero.ToString
            .Debe = Cheque.Valor  'el total a pagar al proveedor
            .Haber = 0.0
            .IdCentro = ""
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        ChequeDetalle.Add(entDet)
        entDet = New ban_ChequesDetalle  'agregando el haber al banco
        With entDet
            .IdCheque = 0
            .IdDetalle = 2
            .IdCuenta = entCta.IdCuentaContable 'la cuenta por pagar al proveedor
            .Referencia = Cheque.Numero
            .Concepto = "Cheque: " + Cheque.Numero + ". " + entPro.Nombre + " " + sConcepto
            .Debe = 0.0
            .Haber = Cheque.Valor  'el total a pagar al proveedor
            .IdCentro = ""
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        ChequeDetalle.Add(entDet)
        Dim Contabilizar As Boolean = True
        Dim msj As String = blBank.ban_InsertarCheque(Cheque, ChequeDetalle, Contabilizar)
        If msj = "Ok" Then
            bl.cpp_AutorizaAbonosCheque(IdComprobante, Cheque.IdCheque, objMenu.User)
            MsgBox("El cheque fue guardado con éxito" & Chr(13), 64, "Nota")

            If dtParam.Rows(0).Item("AutorizarCheques") Then
                MsgBox("Debe autorizar el cheque en el módulo de bancos para poder realizar la impresión", MsgBoxStyle.Information, "Nota")
                Close()
                Exit Sub
            End If

            ' & "Proceda a imprimir en el módulo de bancos"
            ChequeHeader = objTablas.ban_ChequesSelectByPK(Cheque.IdCheque)
            If entCta.TipoImpresion = 1 Then  'solo vaucher impreso
                ImprimeVaucher(entCta)
            End If
            If entCta.TipoImpresion = 2 Then  'cheque vaucher
                ImprimeCheque(entCta)
            End If
            If entCta.TipoImpresion = 3 Then  'vaucher y cheque
                ImprimeVaucher(entCta)
                ImprimeCheque(entCta)
            End If


        Else
            MsgBox("NO FUE POSIBLE GUARDAR EL CHEQUE" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
        End If
        Close()

    End Sub

    Private _IdProveedor As String = ""
    Public Property IdProveedor() As String
        Get
            Return _IdProveedor
        End Get
        Set(ByVal value As String)
            _IdProveedor = value
        End Set
    End Property


    Private _IdComprobante As Integer
    Public Property IdComprobante() As Integer
        Get
            Return _IdComprobante
        End Get
        Set(ByVal value As Integer)
            _IdComprobante = value
        End Set
    End Property

    Private _Valor As Decimal
    Public Property Valor() As Decimal
        Get
            Return _Valor
        End Get
        Set(ByVal value As Decimal)
            _Valor = value
        End Set
    End Property
    Private _Fecha As Date
    Public Property Fecha() As Date
        Get
            Return _Fecha
        End Get
        Set(ByVal value As Date)
            _Fecha = value
        End Set
    End Property

    Private _IdSucursal As Integer
    Public Property IdSucursal() As Integer
        Get
            Return _IdSucursal
        End Get
        Set(ByVal value As Integer)
            _IdSucursal = value
        End Set
    End Property

    Private Sub cpp_frmEmitirCheque_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.banCuentasBancarias(leCtaBancaria)
    End Sub

    Private Sub ImprimeVaucher(ByRef entCta As ban_CuentasBancarias)

        Dim dt As DataTable = blBank.ReporteCheques(ChequeHeader.IdCheque, 0)
        Dim sDecimal As String = ""
        sDecimal = String.Format("{0:c}", ChequeHeader.Valor)

        sDecimal = sDecimal.Substring(sDecimal.Length - 2) & "/100"

        Dim rpt As New Vaucher() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlNumeroCheque.Text = ChequeHeader.Numero
        rpt.xrlNumeroCuenta.Text = entCta.NumeroCuenta
        rpt.xrlValorCheque.Text = "***" & Format(ChequeHeader.Valor, "###,###,###.00")
        rpt.xrlFecha.Text = dtParam.Rows(0).Item("Domicilio") & ", " & (FechaToString(ChequeHeader.Fecha, ChequeHeader.Fecha)).ToUpper
        rpt.xrlANombreDe.Text = ChequeHeader.AnombreDe
        rpt.xrlCantidad.Text = Num2Text(Int(ChequeHeader.Valor)) & " " & sDecimal & " DÓLARES"
        rpt.xrlIdPartida.Text = ChequeHeader.IdCheque
        rpt.xrlNoPartida.Text = ChequeHeader.IdTipoPartida & "-" & ChequeHeader.NumeroPartida
        rpt.xrlConcepto.Text = ChequeHeader.Concepto
        rpt.xrlBanco.Text = entCta.Nombre
        rpt.xrlHecho.Text = entCta.NombreDigitador
        rpt.xrlRevisado.Text = entCta.NombreRevisa
        rpt.xrlAutorizado.Text = entCta.NombreAutoriza
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub ImprimeCheque(ByRef entCta As ban_CuentasBancarias)
        Dim Tipo As Integer = 0 'indica si la columna del debe y el haber se sumna
        If entCta.IdFormatoVaucher = 2 Then 'citi
            Tipo = 1
        End If
        Dim dt As DataTable = blBank.ReporteCheques(ChequeHeader.IdCheque, Tipo)
        Dim sDecimal As String = ""
        '1=Agricola, 2=Citi, 3=HSBC, 4=Scotiabank, 5=America Central, 6=Promerica, 7=Hipotecario, 8=Procredit, 9=Industrial/GYT
        Dim rpt As New ban_rptCheque01
        Dim Template = Application.StartupPath & "\Plantillas\cheque" & entCta.IdFormatoVaucher.ToString.PadLeft(2, "0") & ".repx"
        If Not FileIO.FileSystem.FileExists(Template) Then
            MsgBox("No existe la plantilla necesaria para imprimir el cheque", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If


        sDecimal = String.Format("{0:c}", ChequeHeader.Valor)
        sDecimal = sDecimal.Substring(sDecimal.Length - 2) & "/100" & "********************************"
        Dim Valor As String = "********************" + Format(ChequeHeader.Valor, "###,###,##0.00")
        Valor = Valor.Substring(Valor.Length - 15, 15)

        rpt.LoadLayout(Template)
        rpt.DataSource = dt
        rpt.DataMember = ""
        rpt.PrinterName = dtParam.Rows(0).Item("ImpresorCheques")
        If entCta.IdFormatoVaucher = 1 Or entCta.IdFormatoVaucher = 5 Then 'AGRICOLA Y AMERICA CENTRAL
            Dim LugarFecha As String = dtParam.Rows(0).Item("Domicilio") & ", " & CDate(ChequeHeader.Fecha).Day.ToString() & " de "
            LugarFecha += ObtieneMesString(CDate(ChequeHeader.Fecha).Month)
            rpt.xrlLugarFecha.Text = LugarFecha
            rpt.xrlAnio.Text = CDate(ChequeHeader.Fecha).Year.ToString()
        Else
            If entCta.IdFormatoVaucher = 4 Then 'ScotiaBank
                rpt.xrlLugarFecha.Text = String.Format("{0} ", dtParam.Rows(0).Item("Domicilio"))
                rpt.xrlDia.Text = CDate(ChequeHeader.Fecha).Day.ToString()
                rpt.xrlMes.Text = ObtieneMesString(CDate(ChequeHeader.Fecha).Month)
                rpt.xrlAnio.Text = CDate(ChequeHeader.Fecha).Year.ToString()
            Else 'Cheque 2, 3, 6, 7, 8 y 9
                rpt.xrlLugarFecha.Text = String.Format("{0}, {1}", dtParam.Rows(0).Item("Domicilio"), FechaToString(ChequeHeader.Fecha, ChequeHeader.Fecha))
            End If
        End If

        'rpt.xrlValorCheque.Text = Format(ChequeHeader.Valor, "###,###,##0.00") 'ChequeHeader.Valor
        rpt.xrlValorCheque.Text = Valor 'Format(ChequeHeader.Valor, "###,###,##0.00") 'ChequeHeader.Valor

        rpt.xrlANombreDe.Text = ChequeHeader.AnombreDe
        rpt.xrlCantidad.Text = String.Format("{0} {1}", Num2Text(Int(ChequeHeader.Valor)), sDecimal)
        rpt.xrlConcepto.Text = ChequeHeader.Concepto
        If dtParam.Rows(0).Item("PreviewCheques") Then
            rpt.ShowPreviewDialog()
        Else
            rpt.PrintDialog()
        End If
    End Sub

End Class