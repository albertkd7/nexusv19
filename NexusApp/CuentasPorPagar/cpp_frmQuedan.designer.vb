﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpp_frmQuedan
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xtcQuedan = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage()
        Me.gcList = New DevExpress.XtraGrid.GridControl()
        Me.gvList = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.btAnular = New DevExpress.XtraEditors.SimpleButton()
        Me.btAgregarProveedor = New DevExpress.XtraEditors.SimpleButton()
        Me.btEliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.teNRC = New DevExpress.XtraEditors.TextEdit()
        Me.btAgregar = New DevExpress.XtraEditors.SimpleButton()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.beProveedor = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaPago = New DevExpress.XtraEditors.DateEdit()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teConcepto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcolNumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gColTipo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.releTipoDocto = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gColFecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.redeFecha = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.gColVencto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.redeFecVence = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.gcolValorGravado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcolIdCompra = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcColValorExento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colValorIva = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSubTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colRetencion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colValorTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.xtcQuedan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcQuedan.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gcList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaPago.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.releTipoDocto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.redeFecha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.redeFecha.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.redeFecVence, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.redeFecVence.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcQuedan
        '
        Me.xtcQuedan.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcQuedan.Location = New System.Drawing.Point(0, 0)
        Me.xtcQuedan.Name = "xtcQuedan"
        Me.xtcQuedan.SelectedTabPage = Me.xtpLista
        Me.xtcQuedan.Size = New System.Drawing.Size(1010, 547)
        Me.xtcQuedan.TabIndex = 29
        Me.xtcQuedan.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gcList)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(1004, 519)
        Me.xtpLista.Text = "Lista (Doble clic para Editar)"
        '
        'gcList
        '
        Me.gcList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcList.Location = New System.Drawing.Point(0, 0)
        Me.gcList.MainView = Me.gvList
        Me.gcList.Name = "gcList"
        Me.gcList.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.leSucursalDetalle})
        Me.gcList.Size = New System.Drawing.Size(1004, 519)
        Me.gcList.TabIndex = 0
        Me.gcList.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvList})
        '
        'gvList
        '
        Me.gvList.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn9, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn8, Me.GridColumn6, Me.GridColumn7})
        Me.gvList.GridControl = Me.gcList
        Me.gvList.Name = "gvList"
        Me.gvList.OptionsBehavior.Editable = False
        Me.gvList.OptionsView.ShowAutoFilterRow = True
        Me.gvList.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Correlativo"
        Me.GridColumn1.FieldName = "IdComprobante"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Width = 59
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Periodo"
        Me.GridColumn2.FieldName = "Periodo"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        Me.GridColumn2.Width = 52
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Sucursal de Emisión"
        Me.GridColumn9.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn9.FieldName = "IdSucursal"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 1
        Me.GridColumn9.Width = 141
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Fecha Elaboración"
        Me.GridColumn3.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.GridColumn3.FieldName = "FechaHoraCreacion"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 101
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatString = "{dd-MM-yyyy 00:00:00 am}"
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Fecha Quedan"
        Me.GridColumn4.FieldName = "Fecha"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 81
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "No. Quedan"
        Me.GridColumn5.FieldName = "Numero"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 4
        Me.GridColumn5.Width = 71
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Proveedor"
        Me.GridColumn8.FieldName = "Proveedor"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 5
        Me.GridColumn8.Width = 260
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Monto Total"
        Me.GridColumn6.FieldName = "TotalPago"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 6
        Me.GridColumn6.Width = 64
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Concepto"
        Me.GridColumn7.FieldName = "Concepto"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 7
        Me.GridColumn7.Width = 200
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.pcHeader)
        Me.xtpDatos.Controls.Add(Me.gc)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(1004, 519)
        Me.xtpDatos.Text = "Datos del Quedan"
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.LabelControl9)
        Me.pcHeader.Controls.Add(Me.btAnular)
        Me.pcHeader.Controls.Add(Me.btAgregarProveedor)
        Me.pcHeader.Controls.Add(Me.btEliminar)
        Me.pcHeader.Controls.Add(Me.teNRC)
        Me.pcHeader.Controls.Add(Me.btAgregar)
        Me.pcHeader.Controls.Add(Me.teNombre)
        Me.pcHeader.Controls.Add(Me.beProveedor)
        Me.pcHeader.Controls.Add(Me.LabelControl4)
        Me.pcHeader.Controls.Add(Me.LabelControl7)
        Me.pcHeader.Controls.Add(Me.LabelControl6)
        Me.pcHeader.Controls.Add(Me.LabelControl3)
        Me.pcHeader.Controls.Add(Me.deFechaPago)
        Me.pcHeader.Controls.Add(Me.deFecha)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.teConcepto)
        Me.pcHeader.Controls.Add(Me.LabelControl5)
        Me.pcHeader.Controls.Add(Me.teCorrelativo)
        Me.pcHeader.Controls.Add(Me.LabelControl8)
        Me.pcHeader.Controls.Add(Me.teNumero)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(1004, 135)
        Me.pcHeader.TabIndex = 3
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(570, 3)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(228, 20)
        Me.leSucursal.TabIndex = 30
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(522, 7)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl9.TabIndex = 31
        Me.LabelControl9.Text = "Sucursal:"
        '
        'btAnular
        '
        Me.btAnular.Image = Global.Nexus.My.Resources.Resources.Delete
        Me.btAnular.Location = New System.Drawing.Point(557, 78)
        Me.btAnular.Name = "btAnular"
        Me.btAnular.Size = New System.Drawing.Size(99, 29)
        Me.btAnular.TabIndex = 29
        Me.btAnular.Text = "Anular"
        '
        'btAgregarProveedor
        '
        Me.btAgregarProveedor.Location = New System.Drawing.Point(557, 49)
        Me.btAgregarProveedor.Name = "btAgregarProveedor"
        Me.btAgregarProveedor.Size = New System.Drawing.Size(99, 23)
        Me.btAgregarProveedor.TabIndex = 28
        Me.btAgregarProveedor.Text = "Agregar Proveedor"
        '
        'btEliminar
        '
        Me.btEliminar.Location = New System.Drawing.Point(788, 110)
        Me.btEliminar.Name = "btEliminar"
        Me.btEliminar.Size = New System.Drawing.Size(150, 23)
        Me.btEliminar.TabIndex = 4
        Me.btEliminar.Text = "Eliminar Docto. de Compra"
        '
        'teNRC
        '
        Me.teNRC.Enabled = False
        Me.teNRC.EnterMoveNextControl = True
        Me.teNRC.Location = New System.Drawing.Point(140, 67)
        Me.teNRC.Name = "teNRC"
        Me.teNRC.Properties.ReadOnly = True
        Me.teNRC.Size = New System.Drawing.Size(100, 20)
        Me.teNRC.TabIndex = 5
        '
        'btAgregar
        '
        Me.btAgregar.Location = New System.Drawing.Point(788, 86)
        Me.btAgregar.Name = "btAgregar"
        Me.btAgregar.Size = New System.Drawing.Size(150, 23)
        Me.btAgregar.TabIndex = 4
        Me.btAgregar.Text = "Agregar Docto. de Compra"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(140, 46)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.ReadOnly = True
        Me.teNombre.Size = New System.Drawing.Size(379, 20)
        Me.teNombre.TabIndex = 4
        '
        'beProveedor
        '
        Me.beProveedor.EnterMoveNextControl = True
        Me.beProveedor.Location = New System.Drawing.Point(140, 25)
        Me.beProveedor.Name = "beProveedor"
        Me.beProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beProveedor.Size = New System.Drawing.Size(100, 20)
        Me.beProveedor.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(60, 91)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl4.TabIndex = 26
        Me.LabelControl4.Text = "Fecha de Pago:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(110, 70)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl7.TabIndex = 27
        Me.LabelControl7.Text = "NRC:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(94, 49)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl6.TabIndex = 27
        Me.LabelControl6.Text = "Nombre:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(55, 28)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(80, 13)
        Me.LabelControl3.TabIndex = 27
        Me.LabelControl3.Text = "Cód. Proveedor:"
        '
        'deFechaPago
        '
        Me.deFechaPago.EditValue = Nothing
        Me.deFechaPago.EnterMoveNextControl = True
        Me.deFechaPago.Location = New System.Drawing.Point(140, 88)
        Me.deFechaPago.Name = "deFechaPago"
        Me.deFechaPago.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaPago.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaPago.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaPago.Size = New System.Drawing.Size(100, 20)
        Me.deFechaPago.TabIndex = 6
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(419, 25)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(324, 29)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl2.TabIndex = 24
        Me.LabelControl2.Text = "Fecha del Quedan:"
        '
        'teConcepto
        '
        Me.teConcepto.EnterMoveNextControl = True
        Me.teConcepto.Location = New System.Drawing.Point(140, 109)
        Me.teConcepto.Name = "teConcepto"
        Me.teConcepto.Size = New System.Drawing.Size(516, 20)
        Me.teConcepto.TabIndex = 7
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(10, 113)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(125, 13)
        Me.LabelControl5.TabIndex = 20
        Me.LabelControl5.Text = "Concepto/Observaciones:"
        '
        'teCorrelativo
        '
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(140, 3)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Properties.ReadOnly = True
        Me.teCorrelativo.Size = New System.Drawing.Size(100, 20)
        Me.teCorrelativo.TabIndex = 0
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(78, 6)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl8.TabIndex = 21
        Me.LabelControl8.Text = "Correlativo:"
        '
        'teNumero
        '
        Me.teNumero.Enabled = False
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(419, 3)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(100, 20)
        Me.teNumero.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(338, 6)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl1.TabIndex = 21
        Me.LabelControl1.Text = "No. de Quedan:"
        '
        'gc
        '
        Me.gc.Location = New System.Drawing.Point(-1, 135)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.releTipoDocto, Me.redeFecha, Me.redeFecVence})
        Me.gc.Size = New System.Drawing.Size(989, 382)
        Me.gc.TabIndex = 2
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcolNumero, Me.gColTipo, Me.gColFecha, Me.gColVencto, Me.gcolValorGravado, Me.gcolIdCompra, Me.gcColValorExento, Me.colValorIva, Me.colSubTotal, Me.colRetencion, Me.colValorTotal})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.NewItemRowText = "Click aquí para agregar documento. <Esc> para salir"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcolNumero
        '
        Me.gcolNumero.Caption = "No. de Documento"
        Me.gcolNumero.FieldName = "Numero"
        Me.gcolNumero.Name = "gcolNumero"
        Me.gcolNumero.Visible = True
        Me.gcolNumero.VisibleIndex = 0
        Me.gcolNumero.Width = 93
        '
        'gColTipo
        '
        Me.gColTipo.Caption = "Tipo Docto."
        Me.gColTipo.ColumnEdit = Me.releTipoDocto
        Me.gColTipo.FieldName = "IdTipo"
        Me.gColTipo.Name = "gColTipo"
        Me.gColTipo.Visible = True
        Me.gColTipo.VisibleIndex = 1
        Me.gColTipo.Width = 173
        '
        'releTipoDocto
        '
        Me.releTipoDocto.AutoHeight = False
        Me.releTipoDocto.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.releTipoDocto.Name = "releTipoDocto"
        '
        'gColFecha
        '
        Me.gColFecha.Caption = "Fecha de Emisión"
        Me.gColFecha.ColumnEdit = Me.redeFecha
        Me.gColFecha.FieldName = "Fecha"
        Me.gColFecha.Name = "gColFecha"
        Me.gColFecha.Visible = True
        Me.gColFecha.VisibleIndex = 2
        Me.gColFecha.Width = 93
        '
        'redeFecha
        '
        Me.redeFecha.AutoHeight = False
        Me.redeFecha.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.redeFecha.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.redeFecha.DisplayFormat.FormatString = "{dd-MM-dd-yyyy}"
        Me.redeFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.redeFecha.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.redeFecha.Name = "redeFecha"
        '
        'gColVencto
        '
        Me.gColVencto.Caption = "Fecha de Vencto."
        Me.gColVencto.ColumnEdit = Me.redeFecVence
        Me.gColVencto.FieldName = "FechaVence"
        Me.gColVencto.Name = "gColVencto"
        Me.gColVencto.Visible = True
        Me.gColVencto.VisibleIndex = 3
        Me.gColVencto.Width = 94
        '
        'redeFecVence
        '
        Me.redeFecVence.AutoHeight = False
        Me.redeFecVence.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.redeFecVence.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.redeFecVence.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.redeFecVence.Name = "redeFecVence"
        '
        'gcolValorGravado
        '
        Me.gcolValorGravado.Caption = "Valor Gravado"
        Me.gcolValorGravado.DisplayFormat.FormatString = "{0:c2}"
        Me.gcolValorGravado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcolValorGravado.FieldName = "ValorGravado"
        Me.gcolValorGravado.Name = "gcolValorGravado"
        Me.gcolValorGravado.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorGravado", "{0:n2}")})
        Me.gcolValorGravado.Visible = True
        Me.gcolValorGravado.VisibleIndex = 4
        Me.gcolValorGravado.Width = 88
        '
        'gcolIdCompra
        '
        Me.gcolIdCompra.Caption = "GridColumn1"
        Me.gcolIdCompra.FieldName = "IdCompra"
        Me.gcolIdCompra.Name = "gcolIdCompra"
        '
        'gcColValorExento
        '
        Me.gcColValorExento.Caption = "Valor Exento"
        Me.gcColValorExento.DisplayFormat.FormatString = "{0:c2}"
        Me.gcColValorExento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcColValorExento.FieldName = "ValorExento"
        Me.gcColValorExento.Name = "gcColValorExento"
        Me.gcColValorExento.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorExento", "{0:n2}")})
        Me.gcColValorExento.Visible = True
        Me.gcColValorExento.VisibleIndex = 5
        Me.gcColValorExento.Width = 73
        '
        'colValorIva
        '
        Me.colValorIva.Caption = "Valor IVA"
        Me.colValorIva.DisplayFormat.FormatString = "{0:c2}"
        Me.colValorIva.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colValorIva.FieldName = "ValorIva"
        Me.colValorIva.Name = "colValorIva"
        Me.colValorIva.OptionsColumn.AllowEdit = False
        Me.colValorIva.OptionsColumn.AllowFocus = False
        Me.colValorIva.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorIva", "{0:n2}")})
        Me.colValorIva.Visible = True
        Me.colValorIva.VisibleIndex = 6
        Me.colValorIva.Width = 52
        '
        'colSubTotal
        '
        Me.colSubTotal.Caption = "Sub-Total"
        Me.colSubTotal.DisplayFormat.FormatString = "{0:c2}"
        Me.colSubTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colSubTotal.FieldName = "SubTotal"
        Me.colSubTotal.Name = "colSubTotal"
        Me.colSubTotal.OptionsColumn.AllowEdit = False
        Me.colSubTotal.OptionsColumn.AllowFocus = False
        Me.colSubTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SubTotal", "{0:n2}")})
        Me.colSubTotal.Visible = True
        Me.colSubTotal.VisibleIndex = 7
        Me.colSubTotal.Width = 78
        '
        'colRetencion
        '
        Me.colRetencion.Caption = "Percepcion/Retencion"
        Me.colRetencion.DisplayFormat.FormatString = "{0:c2}"
        Me.colRetencion.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colRetencion.FieldName = "Retencion"
        Me.colRetencion.Name = "colRetencion"
        Me.colRetencion.OptionsColumn.AllowEdit = False
        Me.colRetencion.OptionsColumn.AllowFocus = False
        Me.colRetencion.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Retencion", "{0:n2}")})
        Me.colRetencion.Visible = True
        Me.colRetencion.VisibleIndex = 8
        Me.colRetencion.Width = 123
        '
        'colValorTotal
        '
        Me.colValorTotal.Caption = "Total Documento"
        Me.colValorTotal.DisplayFormat.FormatString = "{0:c2}"
        Me.colValorTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colValorTotal.FieldName = "ValorTotal"
        Me.colValorTotal.Name = "colValorTotal"
        Me.colValorTotal.OptionsColumn.AllowEdit = False
        Me.colValorTotal.OptionsColumn.AllowFocus = False
        Me.colValorTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorTotal", "{0:n2}")})
        Me.colValorTotal.Visible = True
        Me.colValorTotal.VisibleIndex = 9
        Me.colValorTotal.Width = 104
        '
        'cpp_frmQuedan
        '
        Me.ClientSize = New System.Drawing.Size(1010, 572)
        Me.Controls.Add(Me.xtcQuedan)
        Me.Modulo = "Cuentas por Pagar"
        Me.Name = "cpp_frmQuedan"
        Me.OptionId = "001001"
        Me.Text = "Quedan"
        Me.Controls.SetChildIndex(Me.xtcQuedan, 0)
        CType(Me.xtcQuedan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcQuedan.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gcList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaPago.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.releTipoDocto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.redeFecha.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.redeFecha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.redeFecVence.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.redeFecVence, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents xtcQuedan As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gcList As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvList As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btAgregarProveedor As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btEliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btAgregar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beProveedor As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaPago As DevExpress.XtraEditors.DateEdit
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcolNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gColTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents releTipoDocto As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents gColFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents redeFecha As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents gColVencto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents redeFecVence As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents gcolValorGravado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcolIdCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcColValorExento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colValorIva As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSubTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRetencion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colValorTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents btAnular As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit

End Class
