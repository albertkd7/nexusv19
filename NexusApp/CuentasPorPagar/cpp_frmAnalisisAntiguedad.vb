﻿
Imports NexusBLL
Public Class cpp_frmAnalisisAntiguedad
    Dim myBL As New CuentasPPBLL(g_ConnectionString)

    Private Sub cpp_frmAnalisis_Antiguedad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        deHasta.EditValue = Today
    End Sub
    Private Sub cpp_frmAnalisis_Antiguedad_Report_Click() Handles Me.Reporte
        Dim dt = myBL.getAnalisis(BeProveedor2.beCodigo.EditValue, deHasta.EditValue, leSucursal.EditValue)
        Dim rpt As New cpp_rptAnalisis() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = (FechaToString(deHasta.EditValue, deHasta.EditValue)).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda

        rpt.ShowPreviewDialog()

    End Sub

    Private Sub sbResumen_Click(sender As Object, e As EventArgs) Handles sbResumen.Click

        Dim dt = myBL.cpp_ResumenSaldosProveedor(leSucursal.EditValue, deHasta.EditValue)
        Dim rpt As New cpp_rptSaldosProveedor() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = "AL " & (FechaToString(deHasta.EditValue, deHasta.EditValue)).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda

        rpt.ShowPreviewDialog()

    End Sub

End Class
