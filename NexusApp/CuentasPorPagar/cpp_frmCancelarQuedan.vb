﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class cpp_frmCancelarQuedan
    Dim bl As New CuentasPPBLL(g_ConnectionString)

    Private Sub cpp_frmCancelarQuedan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFecha.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        leSucursal.EditValue = piIdSucursalUsuario
        gc.DataSource = bl.cpp_ObtenerQuedanPendientes(leSucursal.EditValue)
    End Sub


    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click

        For I = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(I, "Cancelar") = True Then
                bl.cpp_CancelarQuedan(gv.GetRowCellValue(I, "Correlativo"), deFecha.EditValue)
            End If
        Next

        MsgBox("La cancelación ha sido aplicada con éxito", MsgBoxStyle.Information, "Nota")
        Close()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        gc.DataSource = bl.cpp_ObtenerQuedanPendientes(leSucursal.EditValue)
    End Sub
End Class
