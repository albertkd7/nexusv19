﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class cpp_frmAutorizarAbonos2

    Dim bl As New CuentasPPBLL(g_ConnectionString)
    Dim blBank As New BancosBLL(g_ConnectionString)
    Dim entAbonos As New cpp_Abonos
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub cpp_frmAutorizarAbonos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.banTiposTransaccion(leTipoTransaccion, "")
        objCombos.conTiposPartida(leTipoPartida, "")
        objCombos.banCuentasBancarias(leCtaBancaria)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        leTipoTransaccion.EditValue = 3
        leSucursal.EditValue = piIdSucursalUsuario
        gc.DataSource = bl.cpp_ObtenerEstruturaPendientes()
        deFecha.EditValue = Today
    End Sub


    Private Sub rgFormaPago_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgFormaPago.EditValueChanged
        lblTipoTransaccion.Visible = rgFormaPago.EditValue = 1
        leTipoTransaccion.Visible = rgFormaPago.EditValue = 1
    End Sub

    Private Sub btAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAplicar.Click
        gv.UpdateTotalSummary()

        If teMontoAbonar.EditValue <> Me.gclMontoCheque.SummaryItem.SummaryValue Then
            MsgBox("El valor de los cheques ó transferencias es diferente al monto del abono", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If

        Dim msj As String = ""

        gv.Columns("IdCuentaBancaria").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "IdCuentaBancaria") = gv.GetRowCellValue(i + 1, "IdCuentaBancaria") Then
                msj = "La cuenta bancaria " & gv.GetRowCellValue(i, "IdCuentaBancaria") & ",  está repetida"
                Exit For
            End If
        Next

        gv.Columns("IdCuentaBancaria").SortOrder = DevExpress.Data.ColumnSortOrder.None
        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If

        If rgFormaPago.EditValue = 0 Then
            InsertarCheque()
        Else
            InsertarTransaccion()
        End If
    End Sub

    Private Sub InsertarCheque()


        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "Monto") > 0.0 Then
                Dim Cheque = New ban_Cheques
                Dim sConcepto As String = ""
                Dim ChequeDetalle As New List(Of ban_ChequesDetalle)
                sConcepto = " Comprobante " & bl.cpp_ObtenerDocumentosAbonados(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProveedor"), gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"))

                With Cheque
                    .IdCuentaBancaria = gv.GetRowCellValue(i, "IdCuentaBancaria")
                    .Numero = blBank.GetNumeroCheque(gv.GetRowCellValue(i, "IdCuentaBancaria"))
                    .Fecha = deFecha.DateTime
                    .Valor = gv.GetRowCellValue(i, "Monto")
                    .AnombreDe = teANombreDe.EditValue
                    .Concepto = meConcepto.EditValue & sConcepto
                    .IdProveedor = entAbonos.IdProveedor
                    .IdTipoPartida = leTipoPartida.EditValue
                    .IdSucursal = leSucursal.EditValue
                    .Impreso = False
                    .Conciliado = False
                    .Anulado = False
                    .MotivoAnulacion = ""
                    .PagadoBanco = True
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                    .FechaHoraModificacion = Nothing
                    .ModificadoPor = ""
                End With

                Dim entCta As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(Cheque.IdCuentaBancaria)
                Dim entPro As com_Proveedores = objTablas.com_ProveedoresSelectByPK(Cheque.IdProveedor)
                Dim entDet As New ban_ChequesDetalle

                With entDet
                    .IdCheque = 0
                    .IdDetalle = 1
                    .IdCuenta = entPro.IdCuentaContable   'la cuenta por pagar al proveedor
                    .Referencia = entAbonos.NumeroComprobante
                    .Concepto = entAbonos.Concepto + " " & sConcepto
                    .Debe = Cheque.Valor  'el total a pagar al proveedor
                    .Haber = 0.0
                    .IdCentro = ""
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                End With

                ChequeDetalle.Add(entDet)
                entDet = New ban_ChequesDetalle  'agregando el haber al banco

                With entDet
                    .IdCheque = 0
                    .IdDetalle = 2
                    .IdCuenta = entCta.IdCuentaContable 'la cuenta por pagar al proveedor
                    .Referencia = entAbonos.NumeroComprobante
                    .Concepto = entAbonos.Concepto + " " & sConcepto
                    .Debe = 0.0
                    .Haber = Cheque.Valor  'el total a pagar al proveedor
                    .IdCentro = ""
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                End With

                ChequeDetalle.Add(entDet)
                Dim Contabilizar As Boolean = True
                Dim msj As String = blBank.ban_InsertarCheque(Cheque, ChequeDetalle, Contabilizar)

                If msj = "" Then
                    bl.cpp_AutorizaAbonosCheque(entAbonos.IdComprobante, Cheque.IdCheque, objMenu.User)
                Else
                    MsgBox("NO FUE POSIBLE GUARDAR EL CHEQUE" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
                End If
            End If
        Next

        Close()
    End Sub


    Private Sub InsertarTransaccion()
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "Monto") > 0.0 Then
                Dim Transacciones As New ban_Transacciones
                Dim sConcepto As String = ""
                Dim TransaccionesDetalle As New List(Of ban_TransaccionesDetalle)
                sConcepto = " Comprobante " & bl.cpp_ObtenerDocumentosAbonados(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProveedor"), gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"))

                With Transacciones
                    .IdCuentaBancaria = gv.GetRowCellValue(i, "IdCuentaBancaria")
                    .Fecha = deFecha.EditValue
                    .FechaContable = deFecha.EditValue
                    .Valor = gv.GetRowCellValue(i, "Monto")
                    .IdTipo = leTipoTransaccion.EditValue
                    .IdSucursal = leSucursal.EditValue
                    .Concepto = meConcepto.EditValue & sConcepto
                    '.IdPartida = 0  'El Id partida se llena en la capa de datos
                    '.NumeroPartida = ""   'el numero de partida se llena en la capa de datos
                    .IdTipoPartida = leTipoPartida.EditValue
                    .Contabilizar = True
                    .IncluirConciliacion = True
                    .ProcesadaBanco = True
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                    .FechaHoraModificacion = Nothing
                    .ModificadoPor = ""
                End With
                Dim entCta As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(Transacciones.IdCuentaBancaria)
                Dim entPro As com_Proveedores = objTablas.com_ProveedoresSelectByPK(entAbonos.IdProveedor)

                Dim entDet As New ban_TransaccionesDetalle
                With entDet
                    .IdTransaccion = 0
                    .IdDetalle = 1
                    .IdCuenta = entPro.IdCuentaContable   'la cuenta por pagar al proveedor
                    .Referencia = entAbonos.NumeroComprobante
                    .Concepto = entAbonos.Concepto + " " & sConcepto
                    .Debe = Transacciones.Valor  'el total a pagar al proveedor
                    .Haber = 0.0
                    .IdCentro = ""
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                End With
                TransaccionesDetalle.Add(entDet)
                entDet = New ban_TransaccionesDetalle
                'agregando el haber al banco
                With entDet
                    .IdTransaccion = 0
                    .IdDetalle = 2
                    .IdCuenta = entCta.IdCuentaContable 'la cuenta por pagar al proveedor
                    .Referencia = entAbonos.NumeroComprobante
                    .Concepto = entAbonos.Concepto + " " & sConcepto
                    .Debe = 0.0
                    .Haber = Transacciones.Valor  'el total a pagar al proveedor
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                End With
                TransaccionesDetalle.Add(entDet)
                Dim Contabilizar As Boolean = True ' gsNombre_Empresa.StartsWith("PITUTA")
                Dim msj As String = blBank.InsertarTransaccion(Transacciones, TransaccionesDetalle, Contabilizar)
                If msj = "" Then
                    bl.cpp_AutorizaAbonosTransaccion(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), Transacciones.IdTransaccion, objMenu.User)
                    'MsgBox("La transacción ha sido registrada con éxito", 64, "Nota")
                Else
                    MsgBox("No fue posible guardar la transacción" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
                End If
            End If
        Next
        Close()
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        Dim IdAbono = SiEsNulo(objConsultas.cnsAbonosCPP(frmConsultas, leSucursal.EditValue), 0)
        If IdAbono.ToString = "" Then
            Exit Sub
        End If
        entAbonos = objTablas.cpp_AbonosSelectByPK(IdAbono)

        If entAbonos.IdComprobante = 0 Then
            MsgBox("El abono no existe", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        If entAbonos.IdSucursal <> piIdSucursalUsuario And piIdSucursalUsuario <> 1 Then
            MsgBox("El abono corresponde a otra sucursal", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If

        Dim entPorv As com_Proveedores = objTablas.com_ProveedoresSelectByPK(entAbonos.IdProveedor)

        deFechaAbono.EditValue = entAbonos.Fecha
        teMontoAbonar.EditValue = entAbonos.TotalAbono
        teNumeroComprobante.EditValue = entAbonos.NumeroComprobante
        teANombreDe.EditValue = entPorv.Nombre
        meConcepto.EditValue = entAbonos.Concepto
        gc.DataSource = bl.cpp_ObtenerEstruturaPendientes() ' limpio de nuevo el grid, por si mete abonos en el grid de otro abono o proveedor diferente al inicial
    End Sub

    Private Sub gv_InitNewRow(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Monto", 0.0)
    End Sub
End Class
