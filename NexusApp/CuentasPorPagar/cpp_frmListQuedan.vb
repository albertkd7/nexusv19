﻿
Imports NexusBLL

Public Class cpp_frmListQuedan
    Dim myBL As New CuentasPPBLL(g_ConnectionString)

    Private Sub cpp_frmListQuedan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
    End Sub

    Private Sub cpp_frmListQuedan_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable

        dt = myBL.getList_Quedan(rgTipo.EditValue, rgTipo2.EditValue, deDesde.EditValue, deHasta.EditValue)
        Dim rpt As New cpp_rptListQuedan() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.ShowPreview()
    End Sub
End Class
