﻿
Imports NexusBLL
Public Class cpp_frmAuxiliarAntiguedad
    ReadOnly myBL As New CuentasPPBLL(g_ConnectionString)

    Private Sub cpp_frmAuxiliar_Antiguedad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub cpp_frmAuxiliar_Antiguedad_Report_Click() Handles Me.Reporte
        Dim dt = myBL.getAuxiliar(BeProveedor2.beCodigo.EditValue, deHasta.EditValue, leSucursal.EditValue)
        Dim rpt As New cpp_rptAuxiliarAntiguedad() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue + " SUCURSAL: " + leSucursal.Text
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.xrlPeriodo.Text = (FechaToString(deHasta.EditValue, deHasta.EditValue)).ToUpper
        rpt.ShowPreviewDialog()
    End Sub


End Class
