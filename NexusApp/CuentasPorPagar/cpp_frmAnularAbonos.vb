﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class cpp_frmAnularAbonos
    Dim dtDetalle As New DataTable
    Dim bl As New CuentasPPBLL(g_ConnectionString), blAdmon As New AdmonBLL(g_ConnectionString)
    Dim AbonoHeader As cpp_Abonos, AbonoDetalle As List(Of cpp_AbonosDetalle), entCheque As ban_Cheques, entPartidas As con_Partidas
    Dim blBancos As New BancosBLL(g_ConnectionString)
    Dim dtPar As DataTable = blAdmon.ObtieneParametros()

    Private Sub cpp_frmAnularAbonos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        LimpiaPantalla()
    End Sub

    Private Sub LimpiaPantalla()
        beProveedor.beCodigo.EditValue = ""
        beProveedor.teNombre.EditValue = ""
       
        beProveedor.Focus()
        gc.DataSource = Nothing
    End Sub

    Private Sub btObtener_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btObtener.Click
        If SiEsNulo(beProveedor.beCodigo.EditValue, "") = "" Then
            MsgBox("Debe de especificar el código de proveedor", MsgBoxStyle.Critical)
            Exit Sub
        End If
        Dim Fecha As Date = CDate(dtPar.Rows(0).Item("FechaCierre"))
        gc.DataSource = bl.cpp_ObtenerAbonosPorProveedor(Fecha, Today, beProveedor.beCodigo.EditValue)
        If gv.RowCount = 0 Then
            MsgBox("No se han encontrado abonos para el proveedor seleccionado", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
    End Sub

    Private Sub btAnular_Click(sender As Object, e As EventArgs) Handles btAnular.Click

        If SiEsNulo(gv.GetFocusedRowCellValue("IdComprobante"), 0) = 0 Then
            MsgBox("No ha seleccionado ningún abono", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        If MsgBox("Está seguro(a) de Anular éste abono?" & Chr(13) & "Ya no podrá revertir la anulación", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If

        AbonoHeader = objTablas.cpp_AbonosSelectByPK(gv.GetFocusedRowCellValue("IdComprobante"))

        Dim EsOk As Boolean = ValidarFechaCierre(AbonoHeader.Fecha)
        If Not EsOk Then
            MsgBox("La fecha del Abono corresponde a un período ya Cerrado", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        If AbonoHeader.IdCheque = -9999 Then
            MsgBox("El abono ya ha sido anulado anteriormente.", MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If

        Dim Eliminar As Integer = 0
        Dim bAnulado = False
        Try
            AbonoHeader.CreadoPor = objMenu.User
            objTablas.cpp_AbonosUpdate(AbonoHeader)
            If AbonoHeader.IdCheque > 0 Then
                entCheque = objTablas.ban_ChequesSelectByPK(AbonoHeader.IdCheque)
                entPartidas = objTablas.con_PartidasSelectByPK(entCheque.IdPartida)
                If MsgBox("Desea Eliminar el Cheque ?" & Chr(13) & "Ya no podrá revertir la Eliminación, de lo Contrario se Anulara ", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    Eliminar = 1
                Else
                    Eliminar = 0
                    Dim EsOkCierre As Boolean = ValidarFechaCierre(entCheque.Fecha)
                    If EsOkCierre Then
                        bAnulado = blBancos.AnulacionChequeAbierto(entCheque.IdCheque, objMenu.User, entCheque.Fecha, "Anulación de Abono N° " & AbonoHeader.NumeroComprobante) = 0
                    Else
                        If MsgBox("La fecha del cheque no corresponde al período vigente!" & Chr(13) &
                               "Está seguro(a) de continuar con la anulación?", MsgBoxStyle.YesNo, "Nota") = MsgBoxResult.No Then
                            Exit Sub
                        End If
                        Dim iIdTransaccion = objFunciones.ObtenerUltimoId("BAN_TRANSACCIONES", "IdTransaccion") + 1
                        Dim iIdPartida = objFunciones.ObtenerUltimoId("CON_PARTIDAS", "IdPartida") + 1
                        bAnulado = blBancos.AnulacionChequeCerrado(entCheque.IdCheque, iIdTransaccion, 2, entPartidas.IdTipo, iIdPartida, entCheque.Fecha, "Anulación de Abono N° " & AbonoHeader.NumeroComprobante, objMenu.User) = 0
                    End If
                End If
            End If
            bl.cpp_AnulaAbono(AbonoHeader.IdComprobante, AbonoHeader.IdCheque, AbonoHeader.IdTransaccion, objMenu.User, Eliminar)
            gv.DeleteRow(gv.FocusedRowHandle)
            MsgBox("El Abono ha sido anulado con éxito", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox("No fue posible anular el Abono" & Chr(13) & ex.Message, MsgBoxStyle.Critical, "Error al anular el registro")
        End Try
         
    End Sub
End Class
