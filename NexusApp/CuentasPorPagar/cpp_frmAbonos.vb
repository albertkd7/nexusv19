﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Imports System.Xml
Public Class cpp_frmAbonos
    Dim bl As New CuentasPPBLL(g_ConnectionString)
    Dim AbonoHeader As cpp_Abonos
    Dim AbonoDetalle As List(Of cpp_AbonosDetalle)
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub cpp_frmAbonos_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        deFecha.EditValue = Today
        objCombos.cpp_TiposAbono(leTipoAbono)
        objCombos.Com_CajasChicaCompras(leCaja, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        LimpiaPantalla()
    End Sub
    
    Private Sub cpp_frmAbonos_Guardar() Handles Me.Guardar
        If SiEsNulo(beProveedor.beCodigo.EditValue, "") = "" Then
            MsgBox("Debe de especificar el código de proveedor al que desea aplicar el abono", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If teMontoAbonar.EditValue = 0 Then
            MsgBox("Debe de especificar el monto que desea abonar", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If leSucursal.EditValue = 0 Or leSucursal.EditValue = Nothing Or leSucursal.Text = "" Then
            MsgBox("Debe de especificar una sucursal", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If leTipoAbono.EditValue = 6 Then
            MsgBox("Tipo de Abono no Valido para Abonos a Proveedores", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If txtConcepto.EditValue = "" Then
            MsgBox("Debe de especificar el Concepto del Abono", MsgBoxStyle.Critical)
            Exit Sub
        End If

        gv.UpdateTotalSummary()
        If teDiferencia.EditValue <> 0 Or Decimal.Round(gcMontoAbonar.SummaryItem.SummaryValue, 2) = 0 Then
            MsgBox("No puede aplicar el abono, hay saldo disponible", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If MsgBox("Está seguro(a) de aplicar éste abono?" & Chr(13) & "Ya no podrá editar los datos", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        LlenarEntidades()
        Dim msj As String = bl.cpp_AplicarAbono(AbonoHeader, AbonoDetalle)
        If msj = "" Then

            MsgBox("El abono ha sido aplicado con éxito", MsgBoxStyle.Information, "Nota")

            cpp_frmEmitirCheque.IdComprobante = AbonoHeader.IdComprobante
            cpp_frmEmitirCheque.IdProveedor = beProveedor.beCodigo.EditValue
            cpp_frmEmitirCheque.teANombreDe.EditValue = beProveedor.teNombre.EditValue
            cpp_frmEmitirCheque.Valor = teMontoAbonar.EditValue
            cpp_frmEmitirCheque.meConcepto.EditValue = AbonoHeader.Concepto
            cpp_frmEmitirCheque.Fecha = AbonoHeader.Fecha
            cpp_frmEmitirCheque.IdSucursal = AbonoHeader.IdSucursal
            LimpiaPantalla()

            If leTipoAbono.EditValue = 1 Then
                If MsgBox("¿Va a aplicar el cheque en éste momento?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
                    If MsgBox("¿Está seguro(a) que no desea emitir el cheque?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
                        Exit Sub
                    End If
                End If
                cpp_frmEmitirCheque.ShowDialog()
            End If
        Else
            MsgBox("NO FUE POSIBLE APLICAR EL ABONO" + Chr(13) + msj, MsgBoxStyle.Critical, "Nota")
        End If
    End Sub
    Private Sub LlenarEntidades()
        With AbonoHeader
            .IdComprobante = 0 'El id se asigna en la capa de datos
            .Fecha = deFecha.EditValue
            .NumeroComprobante = teNumeroComprobante.EditValue
            .Concepto = txtConcepto.EditValue
            .IdTipo = leTipoAbono.EditValue
            .IdSucursal = leSucursal.EditValue
            .IdProveedor = beProveedor.beCodigo.EditValue
            .TotalAbono = teMontoAbonar.EditValue
            .IdCaja = IIf(leTipoAbono.EditValue = 3, leCaja.EditValue, 1)
            .FechaHoraCancelacion = Now
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With


        AbonoDetalle = New List(Of cpp_AbonosDetalle)
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "MontoAbonar") > 0 Then  'inserto solo los que aplicaron abono
                Dim entDetalle As New cpp_AbonosDetalle
                With entDetalle
                    .IdComprobante = AbonoHeader.IdComprobante
                    .IdDetalle = i
                    .NumComprobanteCompra = gv.GetRowCellValue(i, "Numero")
                    .MontoAbonado = gv.GetRowCellValue(i, "MontoAbonar")
                    .SaldoActual = gv.GetRowCellValue(i, "SaldoCompra")
                    .IdComprobanteCompra = gv.GetRowCellValue(i, "IdComprobante")                    
                    .TipoComprobanteCompra = gv.GetRowCellValue(i, "TipoComprobanteCompra")
                End With
                AbonoDetalle.Add(entDetalle)
            End If
        Next

    End Sub
    Private Sub CalculaDiferencia()
        gv.UpdateTotalSummary()
        teDiferencia.EditValue = teMontoAbonar.EditValue - Decimal.Round(gcMontoAbonar.SummaryItem.SummaryValue, 2)
    End Sub
    Private Sub teMontoAbonar_Validated(ByVal sender As Object, ByVal e As EventArgs) Handles teMontoAbonar.Validated
        Dim Monto As Decimal = 0.0
        teDiferencia.EditValue = teMontoAbonar.EditValue
        For i = 0 To gv.RowCount - 1
            Monto = gv.GetRowCellValue(i, "SaldoCompra")
            If Monto > teDiferencia.EditValue Then Monto = teDiferencia.EditValue
            If teDiferencia.EditValue > 0 Then
                gv.SetRowCellValue(i, "Abonar", True)
                gv.SetRowCellValue(i, "MontoAbonar", Monto)
            Else
                gv.SetRowCellValue(i, "Abonar", False)
                gv.SetRowCellValue(i, "MontoAbonar", 0)
            End If
            CalculaDiferencia()
        Next
    End Sub
    Private Sub chkAbonar_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkAbonar.CheckedChanged
        Dim Monto As Decimal = 0.0
        If gv.EditingValue Then
            Monto = gv.GetRowCellValue(gv.FocusedRowHandle, "SaldoCompra")
            If Monto > teDiferencia.EditValue Then Monto = teDiferencia.EditValue
        End If
        gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", Monto)
        CalculaDiferencia()
    End Sub

    Private Sub LimpiaPantalla()
        AbonoHeader = New cpp_Abonos
        teNumeroComprobante.EditValue = (objFunciones.ObtenerUltimoId("CPP_ABONOS", "IdComprobante") + 1).ToString.PadLeft(6, "0")
       
        beProveedor.beCodigo.EditValue = ""
        beProveedor.teNombre.EditValue = ""
        deFecha.EditValue = Today
        txtConcepto.EditValue = ""
        teDiferencia.EditValue = 0.0
        teMontoAbonar.EditValue = 0.0
        teNumeroComprobante.Focus()
        leCaja.EditValue = 1
        leSucursal.EditValue = piIdSucursalUsuario
        gc.DataSource = Nothing
    End Sub

    Private Sub btObtenerData_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btObtenerData.Click
        gc.DataSource = bl.ObtenerSaldosCompras(beProveedor.beCodigo.EditValue, deFecha.EditValue, leSucursal.EditValue)
    End Sub

    Private Sub leTipoAbono_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leTipoAbono.EditValueChanged
        If leTipoAbono.EditValue = 3 Then
            LabelCaja.Visible = True
            leCaja.Visible = True
        Else
            LabelCaja.Visible = False
            leCaja.Visible = False
        End If
    End Sub
End Class
