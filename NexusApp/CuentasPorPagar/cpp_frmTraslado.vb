﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Imports System.Xml
Public Class cpp_frmTraslado
    Dim bl As New CuentasPPBLL(g_ConnectionString)
    Dim Header As cpp_Traslado
    Dim Detalle As List(Of cpp_TrasladoDetalle)

    Dim AbonoHeader As cpp_Abonos
    Dim AbonoDetalle As List(Of cpp_AbonosDetalle)

    Dim ComprasListas As List(Of com_Compras)

    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub cpp_frmAbonos_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        deFecha.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.conTiposPartida(leTipoPartida, "")
        LimpiaPantalla()
    End Sub

    Private Sub cpp_frmAbonos_Guardar() Handles Me.Guardar
        If SiEsNulo(beProveedor.beCodigo.EditValue, "") = "" Then
            MsgBox("Debe de especificar el código de proveedor Origen Documento", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If SiEsNulo(BeProveedor1.beCodigo.EditValue, "") = "" Then
            MsgBox("Debe de especificar el código de proveedor Destino Documento", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If leSucursal.EditValue = 0 Or leSucursal.EditValue = Nothing Or leSucursal.Text = "" Then
            MsgBox("Debe de especificar una sucursal", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If txtConcepto.EditValue = "" Then
            MsgBox("Debe de especificar el Concepto del Traslado", MsgBoxStyle.Critical)
            Exit Sub
        End If


        If MsgBox("Está seguro(a) de aplicar éste traslado?" & Chr(13) & "Ya no podrá revertir la aplicación", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        LlenarEntidades()
        Dim msj As String = bl.cpp_AplicarTraslado(AbonoHeader, AbonoDetalle, Header, Detalle, ComprasListas)
        If msj = "" Then
            MsgBox("El Traslado de saldo ha sido aplicado con éxito", MsgBoxStyle.Information, "Nota")
            InsertaPartidaTraslado(AbonoHeader.IdComprobante, Header.NumeroComprobante)
            LimpiaPantalla()
        Else
            MsgBox("NO FUE POSIBLE APLICAR EL ABONO" + Chr(13) + msj, MsgBoxStyle.Critical, "Nota")
        End If
    End Sub
    Private Sub LlenarEntidades()
        With AbonoHeader
            .IdComprobante = 0 'El id se asigna en la capa de datos
            .Fecha = deFecha.EditValue
            .NumeroComprobante = ""
            .Concepto = txtConcepto.EditValue
            .IdTipo = 6 ' Especial para Traslados de Deuda
            .IdSucursal = leSucursal.EditValue
            .IdProveedor = beProveedor.beCodigo.EditValue
            .TotalAbono = teTotalTraslado.EditValue
            .IdCaja = 1
            .FechaHoraCancelacion = Now
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .Cancelado = True
            .CanceladoPor = objMenu.User
            .FechaHoraCancelacion = Now
            .IdCaja = -1
            .IdCheque = -1
            .IdTransaccion = -1
        End With

        With Header
            .IdComprobante = 0 'El id se asigna en la capa de datos
            .Fecha = deFecha.EditValue
            .NumeroComprobante = ""
            .Concepto = txtConcepto.EditValue
            .IdProveedorOrigen = beProveedor.beCodigo.EditValue
            .IdProveedorDestino = BeProveedor1.beCodigo.EditValue
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With

        AbonoDetalle = New List(Of cpp_AbonosDetalle)
        Detalle = New List(Of cpp_TrasladoDetalle)
        ComprasListas = New List(Of com_Compras)
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "MontoAbonar") > 0 Then  'inserto solo los que aplicaron abono

                ' El detalle del Abono de la compras que se esta Trasladando
                Dim entDetalle As New cpp_AbonosDetalle
                With entDetalle
                    .IdComprobante = AbonoHeader.IdComprobante
                    .IdDetalle = i
                    .NumComprobanteCompra = gv.GetRowCellValue(i, "Numero")
                    .MontoAbonado = gv.GetRowCellValue(i, "MontoAbonar")
                    .SaldoActual = gv.GetRowCellValue(i, "SaldoCompra")
                    .IdComprobanteCompra = gv.GetRowCellValue(i, "IdComprobante")
                    .TipoComprobanteCompra = gv.GetRowCellValue(i, "TipoComprobanteCompra")
                End With
                AbonoDetalle.Add(entDetalle)

                ' El detalle del Traslado que se esta Aplicando 
                Dim detalleTras As New cpp_TrasladoDetalle
                With detalleTras
                    .IdComprobante = AbonoHeader.IdComprobante
                    .IdDetalle = i
                    .NumComprobanteCompra = gv.GetRowCellValue(i, "Numero")
                    .SaldoActual = gv.GetRowCellValue(i, "SaldoCompra")
                    .IdComprobanteCompra = gv.GetRowCellValue(i, "IdComprobante")
                    .TipoComprobanteCompra = gv.GetRowCellValue(i, "TipoComprobanteCompra")
                End With
                Detalle.Add(detalleTras)

                'El nuevo cargo que pasara al otro Proveeedor
                Dim EntPro As com_Proveedores = objTablas.com_ProveedoresSelectByPK(BeProveedor1.beCodigo.EditValue)

                Dim entCom As New com_Compras
                With entCom
                    .Serie = "TRD"
                    .Numero = "T-" & gv.GetRowCellValue(i, "Numero")
                    .IdProveedor = EntPro.IdProveedor
                    .Nombre = EntPro.Nombre
                    .Nrc = EntPro.Nrc
                    .Nit = EntPro.Nit
                    .Fecha = deFecha.EditValue
                    .FechaContable = deFecha.EditValue
                    .DiasCredito = EntPro.DiasCredito
                    .FechaVencimiento = DateAdd(DateInterval.Day, EntPro.DiasCredito, deFecha.EditValue)
                    .IdFormaPago = 2
                    .IdSucursal = leSucursal.EditValue
                    .IdBodega = 1
                    .NumOrdenCompra = gv.GetRowCellValue(i, "IdComprobante").ToString
                    .IdOrigenCompra = 1
                    .IdTipoCompra = 1
                    .IdTipoComprobante = gv.GetRowCellValue(i, "TipoComprobanteCompra")
                    .CompraExcluido = False
                    .NumDocExcluido = ""
                    .AplicadaInventario = False
                    .TotalExento = 0
                    .TotalAfecto = gv.GetRowCellValue(i, "SaldoCompra")
                    .TotalIva = 0
                    .TipoImpuestoAdicional = 1
                    .TotalImpuesto1 = 0
                    .TotalImpuesto2 = 0
                    .IdPartida = 0
                    .IdCheque = 0
                    .TotalComprobante = gv.GetRowCellValue(i, "SaldoCompra")

                    .ExcluirLibro = True
                    .IdCaja = -1
                    .Contabilizada = True
                    .AfectaCosto = False

                    .Correlativo = 0
                    .Giro = EntPro.Giro
                    .Direccion = ""
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                    .ModificadoPor = ""
                End With
                ComprasListas.Add(entCom)

            End If
        Next

    End Sub
    Private Sub CalculaDiferencia()
        gv.UpdateTotalSummary()
        teTotalTraslado.EditValue = Decimal.Round(gcMontoAbonar.SummaryItem.SummaryValue, 2)
    End Sub

    Private Sub chkAbonar_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkAbonar.CheckedChanged
        Dim Monto As Decimal = 0.0
        If gv.EditingValue Then
            Monto = gv.GetRowCellValue(gv.FocusedRowHandle, "SaldoCompra")
        End If
        gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", Monto)
        CalculaDiferencia()
    End Sub
    Private Sub InsertaPartidaTraslado(ByVal IdAbonoTraslado As Integer, ByVal NumTraslado As String)

        Dim sConcepto As String = "Traslado de Saldo de Comprobante " & bl.cpp_ObtenerDocumentosAbonados(beProveedor.beCodigo.EditValue, IdAbonoTraslado)
        Dim Partidas As New con_Partidas, PartidasDetalle As New List(Of con_PartidasDetalle)

        Dim entProOrigen As com_Proveedores = objTablas.com_ProveedoresSelectByPK(beProveedor.beCodigo.EditValue)
        Dim entProDestino As com_Proveedores = objTablas.com_ProveedoresSelectByPK(BeProveedor1.beCodigo.EditValue)

        With Partidas
            .IdPartida = 0
            .IdSucursal = leSucursal.EditValue
            .IdTipo = leTipoPartida.EditValue
            .Numero = ""
            .Fecha = deFecha.EditValue
            .Concepto = sConcepto
            .Actualizada = 0
            .IdModuloOrigen = 1   'PARA EL CASO DE ABONOS EN EFECTIVO SI PODRÁ ELIMINAR LA PARTIDA
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .ModificadoPor = ""
            .FechaHoraModificacion = Nothing
        End With

        Dim entPartidaDetalle As New con_PartidasDetalle
        With entPartidaDetalle
            .IdPartida = 0
            .IdDetalle = 1
            .IdCuenta = entProOrigen.IdCuentaContable
            .Referencia = NumTraslado
            .Concepto = entProOrigen.Nombre + " " + sConcepto
            .Debe = teTotalTraslado.EditValue
            .Haber = 0.0
            .IdCentro = ""
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        PartidasDetalle.Add(entPartidaDetalle)

        Dim entPartidaDetalle1 As New con_PartidasDetalle
        With entPartidaDetalle1
            .IdPartida = 0
            .IdDetalle = 2
            .IdCuenta = entProDestino.IdCuentaContable
            .Referencia = NumTraslado
            .Concepto = entProDestino.Nombre + " " + sConcepto
            .Debe = 0.0
            .Haber = teTotalTraslado.EditValue
            .IdCentro = ""
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        PartidasDetalle.Add(entPartidaDetalle1)


        Dim msj As String = bl.cpp_InsertaPartida(Partidas, PartidasDetalle)
        If msj = "" Then
            MsgBox("El registro contable se aplico con exito", 64, "Nota")
        Else
            MsgBox("No fue posible guardar el regsitro contable" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
        End If
    End Sub
    Private Sub LimpiaPantalla()
        AbonoHeader = New cpp_Abonos
        Header = New cpp_Traslado
        beProveedor.beCodigo.EditValue = ""
        beProveedor.teNombre.EditValue = ""
        BeProveedor1.beCodigo.EditValue = ""
        BeProveedor1.teNombre.EditValue = ""
        deFecha.EditValue = Today
        txtConcepto.EditValue = ""
        teTotalTraslado.EditValue = 0.0
        leSucursal.EditValue = piIdSucursalUsuario
        gc.DataSource = Nothing
    End Sub

    Private Sub btObtenerData_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btObtenerData.Click
        gc.DataSource = bl.ObtenerSaldosCompras(beProveedor.beCodigo.EditValue, deFecha.EditValue, leSucursal.EditValue)
    End Sub
End Class
