﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpp_frmAbonos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.teDiferencia = New DevExpress.XtraEditors.TextEdit()
        Me.txtConcepto = New DevExpress.XtraEditors.TextEdit()
        Me.teMontoAbonar = New DevExpress.XtraEditors.TextEdit()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colAbonar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkAbonar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.colNumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFechaCompra = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFechaVencto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTotalCompra = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSaldo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcMontoAbonar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIdComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIdImportacion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leTipoAbono = New DevExpress.XtraEditors.LookUpEdit()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.teNumeroComprobante = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutConverter1 = New DevExpress.XtraLayout.Converter.LayoutConverter(Me.components)
        Me.gcHeader = New DevExpress.XtraEditors.GroupControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelCaja = New DevExpress.XtraEditors.LabelControl()
        Me.leCaja = New DevExpress.XtraEditors.LookUpEdit()
        Me.btObtenerData = New DevExpress.XtraEditors.SimpleButton()
        Me.beProveedor = New Nexus.beProveedor()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        CType(Me.teDiferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teMontoAbonar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAbonar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumeroComprobante.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcHeader.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'teDiferencia
        '
        Me.teDiferencia.EditValue = "0.0"
        Me.teDiferencia.Location = New System.Drawing.Point(622, 6)
        Me.teDiferencia.Name = "teDiferencia"
        Me.teDiferencia.Properties.Appearance.Options.UseTextOptions = True
        Me.teDiferencia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teDiferencia.Properties.Mask.EditMask = "n2"
        Me.teDiferencia.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teDiferencia.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teDiferencia.Properties.ReadOnly = True
        Me.teDiferencia.Size = New System.Drawing.Size(118, 20)
        Me.teDiferencia.TabIndex = 22
        '
        'txtConcepto
        '
        Me.txtConcepto.EnterMoveNextControl = True
        Me.txtConcepto.Location = New System.Drawing.Point(102, 111)
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.Size = New System.Drawing.Size(608, 20)
        Me.txtConcepto.TabIndex = 7
        '
        'teMontoAbonar
        '
        Me.teMontoAbonar.EditValue = "0"
        Me.teMontoAbonar.EnterMoveNextControl = True
        Me.teMontoAbonar.Location = New System.Drawing.Point(102, 90)
        Me.teMontoAbonar.Name = "teMontoAbonar"
        Me.teMontoAbonar.Properties.Appearance.Options.UseTextOptions = True
        Me.teMontoAbonar.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teMontoAbonar.Properties.Mask.EditMask = "n2"
        Me.teMontoAbonar.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teMontoAbonar.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teMontoAbonar.Size = New System.Drawing.Size(124, 20)
        Me.teMontoAbonar.TabIndex = 5
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 133)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkAbonar})
        Me.gc.Size = New System.Drawing.Size(745, 211)
        Me.gc.TabIndex = 2
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colAbonar, Me.colNumero, Me.colFechaCompra, Me.colFechaVencto, Me.colTotalCompra, Me.colSaldo, Me.gcMontoAbonar, Me.colIdComprobante, Me.colIdImportacion})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'colAbonar
        '
        Me.colAbonar.Caption = "Abonar?"
        Me.colAbonar.ColumnEdit = Me.chkAbonar
        Me.colAbonar.FieldName = "Abonar"
        Me.colAbonar.Name = "colAbonar"
        Me.colAbonar.Visible = True
        Me.colAbonar.VisibleIndex = 0
        Me.colAbonar.Width = 64
        '
        'chkAbonar
        '
        Me.chkAbonar.AutoHeight = False
        Me.chkAbonar.Name = "chkAbonar"
        '
        'colNumero
        '
        Me.colNumero.Caption = "No. Dcto.Compra"
        Me.colNumero.FieldName = "Numero"
        Me.colNumero.Name = "colNumero"
        Me.colNumero.OptionsColumn.AllowEdit = False
        Me.colNumero.OptionsColumn.AllowFocus = False
        Me.colNumero.Visible = True
        Me.colNumero.VisibleIndex = 1
        Me.colNumero.Width = 106
        '
        'colFechaCompra
        '
        Me.colFechaCompra.Caption = "Fecha de Docto."
        Me.colFechaCompra.FieldName = "FechaCompra"
        Me.colFechaCompra.Name = "colFechaCompra"
        Me.colFechaCompra.OptionsColumn.AllowEdit = False
        Me.colFechaCompra.OptionsColumn.AllowFocus = False
        Me.colFechaCompra.Visible = True
        Me.colFechaCompra.VisibleIndex = 2
        Me.colFechaCompra.Width = 107
        '
        'colFechaVencto
        '
        Me.colFechaVencto.Caption = "Fecha de Vencto."
        Me.colFechaVencto.FieldName = "FechaVencto"
        Me.colFechaVencto.Name = "colFechaVencto"
        Me.colFechaVencto.OptionsColumn.AllowEdit = False
        Me.colFechaVencto.OptionsColumn.AllowFocus = False
        Me.colFechaVencto.Visible = True
        Me.colFechaVencto.VisibleIndex = 3
        Me.colFechaVencto.Width = 107
        '
        'colTotalCompra
        '
        Me.colTotalCompra.Caption = "Total del Docto."
        Me.colTotalCompra.FieldName = "TotalCompra"
        Me.colTotalCompra.Name = "colTotalCompra"
        Me.colTotalCompra.OptionsColumn.AllowEdit = False
        Me.colTotalCompra.OptionsColumn.AllowFocus = False
        Me.colTotalCompra.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCompra", "{0:n2}")})
        Me.colTotalCompra.Visible = True
        Me.colTotalCompra.VisibleIndex = 4
        Me.colTotalCompra.Width = 107
        '
        'colSaldo
        '
        Me.colSaldo.Caption = "Saldo del Docto."
        Me.colSaldo.FieldName = "SaldoCompra"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.OptionsColumn.AllowEdit = False
        Me.colSaldo.OptionsColumn.AllowFocus = False
        Me.colSaldo.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SaldoCompra", "{0:n2}")})
        Me.colSaldo.Visible = True
        Me.colSaldo.VisibleIndex = 5
        Me.colSaldo.Width = 116
        '
        'gcMontoAbonar
        '
        Me.gcMontoAbonar.Caption = "Monto a Abonar"
        Me.gcMontoAbonar.FieldName = "MontoAbonar"
        Me.gcMontoAbonar.Name = "gcMontoAbonar"
        Me.gcMontoAbonar.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontoAbonar", "{0:n2}")})
        Me.gcMontoAbonar.Visible = True
        Me.gcMontoAbonar.VisibleIndex = 6
        Me.gcMontoAbonar.Width = 117
        '
        'colIdComprobante
        '
        Me.colIdComprobante.Caption = "Id. Docto.Compra"
        Me.colIdComprobante.FieldName = "IdComprobante"
        Me.colIdComprobante.Name = "colIdComprobante"
        '
        'colIdImportacion
        '
        Me.colIdImportacion.Caption = "Id. Docto.Importacion"
        Me.colIdImportacion.FieldName = "IdImportacion"
        Me.colIdImportacion.Name = "colIdImportacion"
        '
        'leTipoAbono
        '
        Me.leTipoAbono.EnterMoveNextControl = True
        Me.leTipoAbono.Location = New System.Drawing.Point(102, 69)
        Me.leTipoAbono.Name = "leTipoAbono"
        Me.leTipoAbono.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoAbono.Size = New System.Drawing.Size(353, 20)
        Me.leTipoAbono.TabIndex = 4
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(267, 3)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(89, 20)
        Me.deFecha.TabIndex = 1
        '
        'teNumeroComprobante
        '
        Me.teNumeroComprobante.EnterMoveNextControl = True
        Me.teNumeroComprobante.Location = New System.Drawing.Point(102, 3)
        Me.teNumeroComprobante.Name = "teNumeroComprobante"
        Me.teNumeroComprobante.Size = New System.Drawing.Size(124, 20)
        Me.teNumeroComprobante.TabIndex = 0
        '
        'gcHeader
        '
        Me.gcHeader.CaptionLocation = DevExpress.Utils.Locations.Top
        Me.gcHeader.Controls.Add(Me.leSucursal)
        Me.gcHeader.Controls.Add(Me.LabelControl16)
        Me.gcHeader.Controls.Add(Me.LabelCaja)
        Me.gcHeader.Controls.Add(Me.leCaja)
        Me.gcHeader.Controls.Add(Me.btObtenerData)
        Me.gcHeader.Controls.Add(Me.beProveedor)
        Me.gcHeader.Controls.Add(Me.LabelControl1)
        Me.gcHeader.Controls.Add(Me.teNumeroComprobante)
        Me.gcHeader.Controls.Add(Me.LabelControl2)
        Me.gcHeader.Controls.Add(Me.deFecha)
        Me.gcHeader.Controls.Add(Me.LabelControl3)
        Me.gcHeader.Controls.Add(Me.leTipoAbono)
        Me.gcHeader.Controls.Add(Me.LabelControl4)
        Me.gcHeader.Controls.Add(Me.teMontoAbonar)
        Me.gcHeader.Controls.Add(Me.LabelControl5)
        Me.gcHeader.Controls.Add(Me.txtConcepto)
        Me.gcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.gcHeader.Location = New System.Drawing.Point(0, 0)
        Me.gcHeader.Name = "gcHeader"
        Me.gcHeader.ShowCaption = False
        Me.gcHeader.Size = New System.Drawing.Size(864, 133)
        Me.gcHeader.TabIndex = 1
        Me.gcHeader.Text = "Root"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(430, 3)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(208, 20)
        Me.leSucursal.TabIndex = 2
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(380, 6)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl16.TabIndex = 86
        Me.LabelControl16.Text = "Sucursal:"
        '
        'LabelCaja
        '
        Me.LabelCaja.Location = New System.Drawing.Point(227, 94)
        Me.LabelCaja.Name = "LabelCaja"
        Me.LabelCaja.Size = New System.Drawing.Size(55, 13)
        Me.LabelCaja.TabIndex = 84
        Me.LabelCaja.Text = "Caja Chica:"
        Me.LabelCaja.Visible = False
        '
        'leCaja
        '
        Me.leCaja.EnterMoveNextControl = True
        Me.leCaja.Location = New System.Drawing.Point(284, 89)
        Me.leCaja.Name = "leCaja"
        Me.leCaja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCaja.Size = New System.Drawing.Size(171, 20)
        Me.leCaja.TabIndex = 6
        Me.leCaja.Visible = False
        '
        'btObtenerData
        '
        Me.btObtenerData.Location = New System.Drawing.Point(102, 45)
        Me.btObtenerData.Name = "btObtenerData"
        Me.btObtenerData.Size = New System.Drawing.Size(124, 23)
        Me.btObtenerData.TabIndex = 3
        Me.btObtenerData.Text = "Obtener documentos"
        '
        'beProveedor
        '
        Me.beProveedor.Location = New System.Drawing.Point(41, 24)
        Me.beProveedor.Name = "beProveedor"
        Me.beProveedor.Size = New System.Drawing.Size(600, 20)
        Me.beProveedor.TabIndex = 3
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(28, 6)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "No. de Recibo:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(231, 6)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "Fecha:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(29, 72)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(70, 13)
        Me.LabelControl3.TabIndex = 6
        Me.LabelControl3.Text = "Tipo deAbono:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(18, 92)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(81, 13)
        Me.LabelControl4.TabIndex = 17
        Me.LabelControl4.Text = "Monto a Abonar:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(49, 114)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl5.TabIndex = 18
        Me.LabelControl5.Text = "Concepto:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(545, 8)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl6.TabIndex = 22
        Me.LabelControl6.Text = "Diferencia:"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.teDiferencia)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl1.Location = New System.Drawing.Point(0, 344)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(864, 39)
        Me.PanelControl1.TabIndex = 23
        '
        'cpp_frmAbonos
        '
        Me.ClientSize = New System.Drawing.Size(864, 411)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.gcHeader)
        Me.Modulo = "Cuentas por Pagar"
        Me.Name = "cpp_frmAbonos"
        Me.OptionId = "001003"
        Me.Text = "Aplicación de Abonos a proveedores"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.gcHeader, 0)
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.teDiferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teMontoAbonar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAbonar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumeroComprobante.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcHeader.ResumeLayout(False)
        Me.gcHeader.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents leTipoAbono As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents teNumeroComprobante As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teMontoAbonar As DevExpress.XtraEditors.TextEdit
    Friend WithEvents colAbonar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFechaCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFechaVencto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTotalCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcMontoAbonar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkAbonar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents teDiferencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents colIdComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutConverter1 As DevExpress.XtraLayout.Converter.LayoutConverter
    Friend WithEvents gcHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents beProveedor As Nexus.beProveedor
    Friend WithEvents btObtenerData As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colIdImportacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelCaja As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCaja As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl

End Class
