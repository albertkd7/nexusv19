﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class cpp_frmQuedan
    Dim myBL As New CuentasPPBLL(g_ConnectionString)
    Dim myBLAd As New AdmonBLL(g_ConnectionString)
    Dim Quedan As cpp_Quedan
    Dim Usuarios As adm_Usuarios
    Dim TiposDoc As adm_TiposComprobante, entCom As com_Compras
    Dim QuedanDetalle As List(Of cpp_QuedanDetalle), _DataCompra As String, AplicaRetencion As Boolean
    Dim dtParametros As DataTable
    Dim fd As New FuncionesBLL(g_ConnectionString)

    Private Sub cpp_frmQuedan_RefreshConsulta() Handles Me.RefreshConsulta
        gcList.DataSource = myBL.GetQuedan()
        gvList.BestFitColumns()
    End Sub

    Private Sub cpp_frmQuedan_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        objCombos.com_TiposComprobante(releTipoDocto)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        dtParametros = myBLAd.ObtieneParametros()
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("CPP_QUEDAN", "IdComprobante")
        CargaControles(0)
        ActivarControles(False)
        gcList.DataSource = myBL.GetQuedan()
    End Sub
    Private Sub cpp_frmQuedan_New_Click() Handles Me.Nuevo
        ActivarControles(True)
        Quedan = New cpp_Quedan
        deFecha.EditValue = Today
        deFechaPago.EditValue = Nothing
        teConcepto.EditValue = ""
        beProveedor.EditValue = ""
        teNombre.EditValue = ""
        teNRC.EditValue = ""
        AplicaRetencion = False
        leSucursal.EditValue = piIdSucursalUsuario
        teNumero.EditValue = fd.ObtenerCorrelativoManual("QUEDAN").ToString.PadLeft(4, "0") 'SiEsNulo(.Numero, UltimoNumero.ToString.PadLeft(4, "0"))

        'teNumero.EditValue = (objFunciones.ObtenerUltimoId("CPP_QUEDAN", "IdComprobante") + 1).ToString.PadLeft(6, "0")

        gc.DataSource = myBL.GetQuedanDetalle(-1)
        gv.CancelUpdateCurrentRow()
        gv.AddNewRow()
        gv.FocusedColumn = gv.Columns(0)
        CargaPantalla()
        teNumero.Focus()
    End Sub
    Private Sub cpp_frmQuedan_Guardar() Handles Me.Guardar
        If Not DatosValidos() Then
            Exit Sub
        End If

        If myBL.ExisteNumeroQuedan(teNumero.EditValue) > 0 Then
            If DbMode = DbModeType.insert Then
                MsgBox("Ya existe un número de Quedan con el número asignado", MsgBoxStyle.Critical)
                If MsgBox("¿Está seguro(a) Tomar un nuevo Correlativo de Quedan?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
                    GeneraNumero()
                End If
                Exit Sub
            Else
                If myBL.ExisteNumeroQuedan(teNumero.EditValue) <> teCorrelativo.EditValue Then
                    MsgBox("Ya existe un número de Quedan con el número asignado", MsgBoxStyle.Critical)
                    Exit Sub
                End If
            End If
        End If

        CargaEntidades()
        Dim msj As String = ""
        If DbMode = DbModeType.insert Then
            msj = myBL.cpp_InsertarQuedan(Quedan, QuedanDetalle)
            If msj = "" Then
                MsgBox("El quedan fue guardado con éxito", 64, "Nota")
            Else
                MsgBox("No fue posible guardar el quedan " + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        Else
            msj = myBL.cpp_ActualizarQuedan(Quedan, QuedanDetalle)
            If msj = "" Then
                MsgBox("El quedan fue actualizado con éxito", 64, "Nota")
            Else
                MsgBox(String.Format("No fue posible actualizar el quedan{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If

        MostrarModoInicial()
        ActivarControles(False)

        xtcQuedan.SelectedTabPage = xtpLista
        gcList.DataSource = myBL.GetQuedan()
        gcList.Focus()
        MostrarModoInicial()
    End Sub
    Private Sub GeneraNumero()
        If Not DbMode = DbModeType.insert Then
            Exit Sub
        End If
        teNumero.EditValue = fd.ObtenerCorrelativoManual("QUEDAN").ToString.PadLeft(4, "0")
    End Sub
    Private Sub cpp_frmQuedan_Consulta_Click() Handles Me.Consulta
        teCorrelativo.EditValue = objConsultas.ConsultaQuedan(frmConsultaDetalle)
        CargaControles(0)
    End Sub
    Private Sub cpp_frmQuedan_Editar_Click() Handles Me.Editar

        If Quedan.Anulado Then
            MsgBox("El quedán esta anulado." + Chr(13) + "Imposible Modificar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        ActivarControles(True)
        teNumero.Focus()
    End Sub
    Private Sub cpp_frmQuedan_Revertir() Handles Me.Revertir
        ActivarControles(False)
        xtcQuedan.SelectedTabPage = xtpLista
        gcList.Focus()
    End Sub
    Private Sub gcList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gcList.DoubleClick
        Quedan = objTablas.cpp_QuedanSelectByPK(gvList.GetRowCellValue(gvList.FocusedRowHandle, "IdComprobante"))
        teCorrelativo.EditValue = gvList.GetRowCellValue(gvList.FocusedRowHandle, "IdComprobante")
        CargaPantalla()
        DbMode = DbModeType.update
        teNumero.Focus()
    End Sub
    Private Sub CargaPantalla()
        Dim dt As DataTable
        ' Dim UltimoNumero As Integer
        dt = myBLAd.ObtieneParametros()

        xtcQuedan.SelectedTabPage = xtpDatos
        teNumero.Focus()
        With Quedan
            teCorrelativo.EditValue = .IdComprobante
            deFecha.EditValue = SiEsNulo(.Fecha, Today)
            deFechaPago.EditValue = .FechaPago
            teConcepto.EditValue = .Concepto
            beProveedor.EditValue = .IdProveedor
            teNombre.EditValue = .NombreProveedor
            teNRC.EditValue = .NrcProveedor
            teNumero.EditValue = .Numero
            leSucursal.EditValue = IIf(.IdSucursal = 0, piIdSucursalUsuario, SiEsNulo(.IdSucursal, 1))

            'UltimoNumero = fd.ObtenerCorrelativoManual("QUEDAN") ' dt.Rows(0).Item("CorrelativoQuedan") + 1

            If DbMode = DbModeType.insert Then
                teNumero.EditValue = fd.ObtenerCorrelativoManual("QUEDAN").ToString.PadLeft(4, "0") 'SiEsNulo(.Numero, UltimoNumero.ToString.PadLeft(4, "0"))
            End If

            'If teNumero.EditValue = "" Then
            '    teNumero.EditValue = UltimoNumero.ToString.PadLeft(4, "0")
            'End If

            gc.DataSource = myBL.GetQuedanDetalle(.IdComprobante)

        End With
    End Sub
    Private Sub cpp_frmQuedan_Reporte() Handles Me.Reporte
        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)
        Dim Template = Application.StartupPath & "\Plantillas\quedan" & NumFormato & ".repx"

        Dim rpt As New cpp_rptQuedan
        If FileIO.FileSystem.FileExists(Template) Then
            'MsgBox("No existe la plantilla necesaria para el quedan", MsgBoxStyle.Critical, "Nota")
            'Exit Sub
            rpt.LoadLayout(Template)
        End If



        Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

        Dim Fact As String = ""
        Dim Fact2 As String = ""
        Dim Fact3 As String = ""
        Dim Fact4 As String = ""
        Dim dt As DataTable
        Dim Corre As Integer = 0
        Dim IdDoc As Integer = gvList.GetRowCellValue(gvList.FocusedRowHandle, "IdComprobante")

        Try
            dt = myBL.GetQuedanDetalleStore(IdDoc)
            rpt.DataSource = dt
            rpt.DataMember = ""
        Catch ex As Exception
            dt = myBL.GetQuedanDetalle(IdDoc)
        End Try

        For i = 0 To dt.Rows.Count - 1
            Corre = i

            If Corre >= 0 And Corre <= 2 Then
                TiposDoc = objTablas.adm_TiposComprobanteSelectByPK(dt.Rows(i).Item("IdTipo"))
                If TiposDoc.IdTipoComprobante > 0 Then
                    Fact += TiposDoc.Abreviatura + " "
                    Fact += dt.Rows(i).Item("Numero") + "; " 'gv.GetRowCellValue(i, ) + "; "
                    Fact += dt.Rows(i).Item("Fecha") & Chr(13) 'gv.GetRowCellValue(i, "Fecha") & Chr(13)
                End If
            End If
            If Corre >= 3 And Corre <= 5 Then
                TiposDoc = objTablas.adm_TiposComprobanteSelectByPK(dt.Rows(i).Item("IdTipo"))
                If TiposDoc.IdTipoComprobante > 0 Then
                    Fact2 += TiposDoc.Abreviatura + " "
                    Fact2 += dt.Rows(i).Item("Numero") + "; " 'gv.GetRowCellValue(i, ) + "; "
                    Fact2 += dt.Rows(i).Item("Fecha") & Chr(13) 'gv.GetRowCellValue(i, "Fecha") & Chr(13)
                End If
            End If
            If Corre >= 6 And Corre <= 8 Then
                TiposDoc = objTablas.adm_TiposComprobanteSelectByPK(dt.Rows(i).Item("IdTipo"))
                If TiposDoc.IdTipoComprobante > 0 Then
                    Fact3 += TiposDoc.Abreviatura + " "
                    Fact3 += dt.Rows(i).Item("Numero") + "; "  'gv.GetRowCellValue(i, ) + "; "
                    Fact3 += dt.Rows(i).Item("Fecha") & Chr(13) 'gv.GetRowCellValue(i, "Fecha") & Chr(13)
                End If
            End If
            If Corre >= 9 And Corre <= 11 Then
                TiposDoc = objTablas.adm_TiposComprobanteSelectByPK(dt.Rows(i).Item("IdTipo"))
                If TiposDoc.IdTipoComprobante > 0 Then
                    Fact3 += TiposDoc.Abreviatura + " "
                    Fact3 += dt.Rows(i).Item("Numero") + "; "  'gv.GetRowCellValue(i, ) + "; "
                    Fact3 += dt.Rows(i).Item("Fecha") & Chr(13) 'gv.GetRowCellValue(i, "Fecha") & Chr(13)
                End If
            End If
        Next

        Quedan = objTablas.cpp_QuedanSelectByPK(IdDoc)
        With Quedan
            teNumero.EditValue = .Numero
            deFecha.EditValue = .Fecha
            beProveedor.EditValue = .IdProveedor
            teNombre.EditValue = .NombreProveedor
            teNRC.EditValue = .NrcProveedor
            deFechaPago.EditValue = .FechaPago
            teConcepto.EditValue = .Concepto
            leSucursal.EditValue = .IdSucursal
            gc.DataSource = myBL.GetQuedanDetalle(.IdComprobante)
        End With

        rpt.xrlEmpresa1.Text = gsNombre_Empresa
        rpt.xrlEmpresa2.Text = gsNombre_Empresa
        rpt.xrlEmpresa3.Text = gsNombre_Empresa
        rpt.xrlNumero1.Text = teNumero.EditValue
        rpt.xrlNumero2.Text = teNumero.EditValue
        rpt.xrlNumero3.Text = teNumero.EditValue
        rpt.xrlAFavorDe1.Text = teNombre.EditValue
        rpt.xrlAFavorDe2.Text = teNombre.EditValue
        rpt.xrlAFavorDe3.Text = teNombre.EditValue

        rpt.xrlNumFacturas1.Text = Fact
        rpt.xrlNumFacturas2.Text = Fact2
        rpt.xrlNumFacturas3.Text = Fact3
        rpt.xrlNumFacturas4.Text = Fact4

        rpt.xrlNumFacturas5.Text = Fact
        rpt.xrlNumFacturas6.Text = Fact2
        rpt.xrlNumFacturas7.Text = Fact3
        rpt.xrlNumFacturas8.Text = Fact4

        rpt.xrlNumFacturas9.Text = Fact
        rpt.xrlNumFacturas10.Text = Fact2
        rpt.xrlNumFacturas11.Text = Fact3
        rpt.xrlNumFacturas12.Text = Fact4


        rpt.xrlFecha1.Text = FechaToString(deFecha.EditValue, deFecha.EditValue)
        rpt.xrlFecha2.Text = FechaToString(deFecha.EditValue, deFecha.EditValue)
        rpt.xrlFecha3.Text = FechaToString(deFecha.EditValue, deFecha.EditValue)
        rpt.xrlValor1.Text = Format(Me.colValorTotal.SummaryItem.SummaryValue, "$###,###,##0.00")
        rpt.xrlValor2.Text = Format(Me.colValorTotal.SummaryItem.SummaryValue, "$###,###,##0.00")
        rpt.xrlValor3.Text = Format(Me.colValorTotal.SummaryItem.SummaryValue, "$###,###,##0.00")
        rpt.xrlConcepto1.Text = teConcepto.EditValue
        rpt.xrlConcepto2.Text = teConcepto.EditValue
        rpt.xrlConcepto3.Text = teConcepto.EditValue

        rpt.xrlRecibio1.Text = Usuarios.Nombre + " " + Usuarios.Apellidos
        rpt.xrlRecibio2.Text = Usuarios.Nombre + " " + Usuarios.Apellidos
        rpt.xrlRecibio3.Text = Usuarios.Nombre + " " + Usuarios.Apellidos

        'rpt.xrLogo1.Image = ByteToImagen(dtParametros.Rows(0).Item("LogoEmpresa"))
        'rpt.xrLogo2.Image = ByteToImagen(dtParametros.Rows(0).Item("LogoEmpresa"))
        'rpt.xrLogo3.Image = ByteToImagen(dtParametros.Rows(0).Item("LogoEmpresa"))

        If deFechaPago.EditValue Is Nothing Then
            rpt.xrlFechaPago1.Text = "-- SE AVISARÁ --"
            rpt.xrlFechaPago2.Text = "-- SE AVISARÁ --"
            rpt.xrlFechaPago3.Text = "-- SE AVISARÁ --"
        Else
            rpt.xrlFechaPago1.Text = FechaToString(deFechaPago.EditValue, deFechaPago.EditValue)
            rpt.xrlFechaPago2.Text = FechaToString(deFechaPago.EditValue, deFechaPago.EditValue)
            rpt.xrlFechaPago3.Text = FechaToString(deFechaPago.EditValue, deFechaPago.EditValue)
        End If

        rpt.ShowPrintMarginsWarning = False
        rpt.ShowPreviewDialog()
    End Sub
    Private Sub cpp_frmQuedan_Eliminar() Handles Me.Eliminar
        Quedan = objTablas.cpp_QuedanSelectByPK(gvList.GetRowCellValue(gvList.FocusedRowHandle, "IdComprobante"))
        If Quedan.Anulado Then
            MsgBox("Quedán Anulado." + Chr(13) + "Imposible Modificar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If

        If MsgBox("Está seguro(a) de eliminar el quedán?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        Quedan.ModificadoPor = objMenu.User
        objTablas.cpp_QuedanUpdate(Quedan)

        objTablas.cpp_QuedanDeleteByPK(Quedan.IdComprobante)
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("CPP_QUEDAN", "IdComprobante")
        teCorrelativo.EditValue -= 1
        CargaControles(0)
        gcList.DataSource = myBL.GetQuedan()
    End Sub
    Private Sub CargaEntidades()

        With Quedan
            .Numero = teNumero.EditValue
            .Fecha = deFecha.EditValue
            .TotalPago = Me.colValorTotal.SummaryItem.SummaryValue  'Me.gcolValorGravado.SummaryItem.SummaryValue

            .IdProveedor = beProveedor.EditValue
            .NombreProveedor = teNombre.EditValue
            .Concepto = teConcepto.EditValue
            .FechaPago = deFechaPago.EditValue
            .IdProveedor = beProveedor.EditValue
            .SaldoActual = Me.gcolValorGravado.SummaryItem.SummaryValue
            .Anulado = False
            .IdSucursal = leSucursal.EditValue
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .FechaHoraModificacion = Nothing
                .ModificadoPor = ""
                .AnuladoPor = ""
                .FechaHoraAnulacion = Nothing
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If
        End With

        QuedanDetalle = New List(Of cpp_QuedanDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New cpp_QuedanDetalle
            With entDetalle
                .IdComprobante = Quedan.IdComprobante
                .IdDetalle = i + 1
                .IdCompra = SiEsNulo(gv.GetRowCellValue(i, "IdCompra"), 0)
                .Numero = gv.GetRowCellValue(i, "Numero")
                .IdTipo = gv.GetRowCellValue(i, "IdTipo")
                .Fecha = gv.GetRowCellValue(i, "Fecha")
                .FechaVence = gv.GetRowCellValue(i, "FechaVence")
                .ValorGravado = gv.GetRowCellValue(i, "ValorGravado")
                .ValorExento = gv.GetRowCellValue(i, "ValorExento")
                .ValorIva = gv.GetRowCellValue(i, "ValorIva")
                .SubTotal = gv.GetRowCellValue(i, "SubTotal")
                .Retencion = gv.GetRowCellValue(i, "Retencion")
                .ValorTotal = gv.GetRowCellValue(i, "ValorTotal")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            QuedanDetalle.Add(entDetalle)
        Next
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        'gv.SetRowCellValue(gv.FocusedRowHandle, "Numero", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdTipo", 1)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Fecha", Today())
        gv.SetRowCellValue(gv.FocusedRowHandle, "FechaVence", Today)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorGravado", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorExento", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorIva", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "SubTotal", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Retencion", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorTotal", 0.0)
    End Sub

    Private Sub gc_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc.Enter
        If gv.DataRowCount = 0 Then
            If gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
                gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
            End If
        End If
    End Sub
    Private Sub beProveedor_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beProveedor.ButtonClick
        beProveedor.EditValue = ""
        beProveedor_Validated(sender, New EventArgs)
    End Sub
    Private Sub beProveedor_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beProveedor.Validated
        Dim entProveedor As com_Proveedores = objConsultas.cnsProveedores(frmConsultas, beProveedor.EditValue)
        beProveedor.EditValue = entProveedor.IdProveedor
        teNombre.EditValue = entProveedor.Nombre
        teNRC.EditValue = entProveedor.Nrc
        AplicaRetencion = entProveedor.AplicaRetencion
    End Sub
    Public Sub CargaControles(ByVal TipoAvance As Integer)
        If TipoAvance = 0 Then 'no es necesario obtener el Id
        Else
            teCorrelativo.EditValue = myBL.cpp_ObtenerIdQuedan(teCorrelativo.EditValue, TipoAvance)
        End If
        Quedan = objTablas.cpp_QuedanSelectByPK(teCorrelativo.EditValue)
        With Quedan
            teNumero.EditValue = .Numero
            deFecha.EditValue = .Fecha
            beProveedor.EditValue = .IdProveedor
            teNombre.EditValue = .NombreProveedor
            teNRC.EditValue = .NrcProveedor
            deFechaPago.EditValue = .FechaPago
            teConcepto.EditValue = .Concepto
            leSucursal.EditValue = .IdSucursal
            gc.DataSource = myBL.GetQuedanDetalle(.IdComprobante)
        End With
        teNumero.Focus()
    End Sub
    Private Function DatosValidos()
        If beProveedor.EditValue = "" Then
            MsgBox("No ha especificado el proveedor", MsgBoxStyle.Information, "Nota")
            Return False
        End If

        gv.UpdateTotalSummary()
        If Me.colValorTotal.SummaryItem.SummaryValue = 0.0 Then
            MsgBox("Debe de especificar el valor del quedan", MsgBoxStyle.Information, "Nota")
            Return False
        End If

        Return True
    End Function
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
        teCorrelativo.Properties.ReadOnly = True
        teNombre.Properties.ReadOnly = True
        teNRC.Properties.ReadOnly = True
    End Sub

    Private Sub btAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAgregar.Click
        If rbMenu.qbSave.Visibility = DevExpress.XtraBars.BarItemVisibility.Never Then
            Exit Sub
        End If
        com_frmCompras.sbGuardar.Visible = True
        DataCompra = ""

        com_frmCompras.teNumero.EditValue = gv.GetFocusedRowCellValue("Numero")
        com_frmCompras.deFechaCompra.EditValue = deFecha.EditValue
        com_frmCompras.deFechaContable.EditValue = deFecha.EditValue
        com_frmCompras.beProveedor.EditValue = beProveedor.EditValue
        com_frmCompras.OrigenCompra = "Quedan"
        com_frmCompras.xtcCompras.SelectedTabPage = com_frmCompras.xtpDatos1
        com_frmCompras.xtpLista1.PageVisible = False
        com_frmCompras.ShowDialog()

        If DataCompra = "" Then
            Exit Sub
        End If

        ' IdCompra, Numero, IdTipo, Fecha, Valor, TipoComprobante = tp.Nombre
        Dim aData As Array = DataCompra.Split("|")

        gv.AddNewRow()

        gv.SetFocusedRowCellValue("IdCompra", aData(0))
        gv.SetFocusedRowCellValue("Numero", aData(1))
        gv.SetFocusedRowCellValue("IdTipo", aData(2))
        gv.SetFocusedRowCellValue("Fecha", aData(3))
        gv.SetFocusedRowCellValue("FechaVence", aData(3))
        gv.SetFocusedRowCellValue("Retencion", aData(5))
        gv.SetFocusedRowCellValue("ValorIva", aData(6))
        gv.SetFocusedRowCellValue("ValorGravado", aData(7))
        gv.SetFocusedRowCellValue("ValorExento", aData(8))
        gv.SetFocusedRowCellValue("SubTotal", aData(6) + aData(7) + aData(8))
        gv.SetFocusedRowCellValue("ValorTotal", aData(4))

        'If aData(2) = 1 Then
        '    gv.SetFocusedRowCellValue("TipoDocumento", "CRÉDITO FISCAL")
        'End If
        'If aData(2) = 2 Then
        '    gv.SetFocusedRowCellValue("TipoDocumento", "FACTURA CORRIENTE")
        'End If
        'If aData(2) = 3 Then
        '    gv.SetFocusedRowCellValue("TipoDocumento", "RECIBO")
        'End If
        gv.UpdateCurrentRow()
    End Sub



    Public Property DataCompra() As String
        Get
            Return _DataCompra
        End Get
        Set(ByVal value As String)
            _DataCompra = value
        End Set
    End Property

    Private Sub sbEliminar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btEliminar.Click

        If rbMenu.qbSave.Visibility = DevExpress.XtraBars.BarItemVisibility.Never Then
            Exit Sub
        End If
        gv.DeleteRow(gv.FocusedRowHandle)
    End Sub

    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Select Case gv.FocusedColumn.FieldName
            Case "ValorGravado"
                gv.SetFocusedRowCellValue("ValorGravado", e.Value)
                CalculaTotalFila(e.Value, gv.GetFocusedRowCellValue(gv.Columns("ValorExento")))
            Case "ValorExento"
                gv.SetFocusedRowCellValue("ValorExento", e.Value)
                CalculaTotalFila(gv.GetFocusedRowCellValue(gv.Columns("ValorGravado")), e.Value)
        End Select
    End Sub

    Private Sub CalculaTotalFila(ByVal ValorGravado As Decimal, ByVal ValorExento As Decimal)

        gv.SetFocusedRowCellValue("ValorIva", Decimal.Round(ValorGravado * pnIVA, 2))
        Dim nSubTotal As Decimal = gv.GetFocusedRowCellValue(gv.Columns("ValorIva")) + ValorGravado + ValorExento
        gv.UpdateTotalSummary()


        gv.SetFocusedRowCellValue("SubTotal", nSubTotal)
        gv.SetFocusedRowCellValue("Retencion", 0.0)

        If AplicaRetencion Then
            If gv.GetFocusedRowCellValue(gv.Columns("IdTipo")) <> 6 Then ' las facturas es aparte
                If ValorGravado >= 100 Then
                    gv.SetFocusedRowCellValue("Retencion", Decimal.Round(ValorGravado * CDec(dtParametros.Rows(0).Item("PorcRetencion") / 100), 2))
                Else
                    gv.SetFocusedRowCellValue("Retencion", 0.0)
                End If
            Else
                If ValorGravado >= 113 Then
                    gv.SetFocusedRowCellValue("Retencion", Decimal.Round(ValorGravado * CDec(dtParametros.Rows(0).Item("PorcRetencion") / 100), 2))
                Else
                    gv.SetFocusedRowCellValue("Retencion", 0.0)
                End If
            End If
        Else
            gv.SetFocusedRowCellValue("Retencion", 0.0)
        End If

        If dtParametros.Rows(0).Item("TipoContribuyente") = 3 Then
            gv.SetFocusedRowCellValue("ValorTotal", gv.GetFocusedRowCellValue(gv.Columns("SubTotal")) - gv.GetFocusedRowCellValue(gv.Columns("Retencion")))
        Else
            gv.SetFocusedRowCellValue("ValorTotal", gv.GetFocusedRowCellValue(gv.Columns("SubTotal")) + gv.GetFocusedRowCellValue(gv.Columns("Retencion")))
        End If

        gv.UpdateTotalSummary()
    End Sub

    Private Sub btAgregarProveedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAgregarProveedor.Click
        If rbMenu.qbSave.Visibility = DevExpress.XtraBars.BarItemVisibility.Never Then
            Exit Sub
        End If
        com_frmProveedores.sbGuardar.Visible = True
        com_frmProveedores.ShowDialog()
    End Sub

    Private Sub btAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAnular.Click

        If Not rbMenu.qbSave.Visibility = DevExpress.XtraBars.BarItemVisibility.Never Then
            Exit Sub
        End If

        If Quedan.Anulado Then
            MsgBox("El Quedán ya se encuentra Anulado." + Chr(13) + "Imposible Anular", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If

        If MsgBox("Está seguro(a) de querer anular el Quedán?", MsgBoxStyle.YesNo + vbDefaultButton2, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim msj As String = myBL.AnulaQuedan(teCorrelativo.EditValue, objMenu.User)
        If msj = "" Then
            MsgBox("El quedan fue anulado con éxito", 64, "Nota")
        Else
            MsgBox(String.Format("No fue posible actualizar el quedan{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
            Exit Sub
        End If

        MostrarModoInicial()
        ActivarControles(False)

        xtcQuedan.SelectedTabPage = xtpLista
        gcList.DataSource = myBL.GetQuedan()
        gcList.Focus()
        MostrarModoInicial()
    End Sub

    Private Sub gv_KeyDown(sender As Object, e As KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "Numero" Then
                If gv.EditingValue = Nothing Then
                    gv.EditingValue = ""
                End If
                If SiEsNulo(gv.EditingValue.ToString, "") = "" Then
                    Dim IdProd = objConsultas.com_ConsultaComprasQuedan(frmConsultas, beProveedor.EditValue)
                    If IdProd = "" Then
                        IdProd = Nothing
                    End If
                    gv.EditingValue = SiEsNulo(IdProd, 0)
                End If
                entCom = objTablas.com_ComprasSelectByPK(gv.EditingValue)
                If entCom.Numero = "" Then

                Else
                    gv.SetFocusedRowCellValue("IdCompra", entCom.IdComprobante)
                    gv.SetFocusedRowCellValue("Numero", entCom.Numero)
                    gv.SetFocusedRowCellValue("IdTipo", entCom.IdTipoComprobante)
                    gv.SetFocusedRowCellValue("Fecha", entCom.Fecha)
                    gv.SetFocusedRowCellValue("FechaVence", entCom.Fecha)
                    gv.SetFocusedRowCellValue("ValorGravado", entCom.TotalAfecto)
                    gv.SetFocusedRowCellValue("ValorExento", entCom.TotalExento)
                    gv.SetFocusedRowCellValue("ValorIva", entCom.TotalIva)
                    gv.SetFocusedRowCellValue("SubTotal", entCom.TotalIva + entCom.TotalAfecto + entCom.TotalExento)
                    gv.SetFocusedRowCellValue("Retencion", entCom.TotalImpuesto1)
                    gv.SetFocusedRowCellValue("ValorTotal", entCom.TotalComprobante)
                End If
            End If

        End If
    End Sub
End Class
