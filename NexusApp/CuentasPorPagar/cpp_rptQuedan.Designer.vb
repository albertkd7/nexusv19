<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class cpp_rptQuedan
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cpp_rptQuedan))
        Me.xrlFechaPago3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.xrlEmpresa3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel37 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPageInfo3 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.xrlNumFacturas1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlConcepto1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlValor1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlFecha1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.xrlRecibio3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlRecibio2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFacturas12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFacturas11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFacturas10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFacturas9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFacturas7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFacturas8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFacturas5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFacturas6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFacturas4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFacturas3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumFacturas2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlRecibio1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine5 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel34 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFecha3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel31 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumero3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel36 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLogo3 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlValor3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine4 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlAFavorDe3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlConcepto3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumero2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlAFavorDe2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFecha2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlConcepto2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlValor2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLogo2 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFechaPago2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLogo1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.xrlEmpresa2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlEmpresa1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNumero1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFechaPago1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlAFavorDe1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xrlFechaPago3
        '
        Me.xrlFechaPago3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlFechaPago3.Dpi = 100.0!
        Me.xrlFechaPago3.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlFechaPago3.LocationFloat = New DevExpress.Utils.PointFloat(462.802!, 896.6459!)
        Me.xrlFechaPago3.Name = "xrlFechaPago3"
        Me.xrlFechaPago3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFechaPago3.SizeF = New System.Drawing.SizeF(293.5417!, 18.99997!)
        Me.xrlFechaPago3.StylePriority.UseBorders = False
        Me.xrlFechaPago3.StylePriority.UseFont = False
        Me.xrlFechaPago3.Text = "xrlFechaPago"
        '
        'XrLabel8
        '
        Me.XrLabel8.Dpi = 100.0!
        Me.XrLabel8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(633.2084!, 29.00001!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(41.66669!, 21.0!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.Text = "No."
        '
        'XrLabel17
        '
        Me.XrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel17.Dpi = 100.0!
        Me.XrLabel17.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(633.5209!, 338.9375!)
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(125.0!, 29.0!)
        Me.XrLabel17.StylePriority.UseBorders = False
        Me.XrLabel17.StylePriority.UseFont = False
        Me.XrLabel17.Text = "QUEDAN"
        '
        'PageFooter
        '
        Me.PageFooter.Dpi = 100.0!
        Me.PageFooter.HeightF = 30.0!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlEmpresa3
        '
        Me.xrlEmpresa3.Dpi = 100.0!
        Me.xrlEmpresa3.Font = New System.Drawing.Font("Arial", 14.0!)
        Me.xrlEmpresa3.LocationFloat = New DevExpress.Utils.PointFloat(268.0312!, 666.7709!)
        Me.xrlEmpresa3.Name = "xrlEmpresa3"
        Me.xrlEmpresa3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlEmpresa3.SizeF = New System.Drawing.SizeF(325.2917!, 25.0!)
        Me.xrlEmpresa3.StylePriority.UseFont = False
        Me.xrlEmpresa3.StylePriority.UseTextAlignment = False
        Me.xrlEmpresa3.Text = "SU EMPRESA"
        Me.xrlEmpresa3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrlEmpresa3.Visible = False
        '
        'XrLabel37
        '
        Me.XrLabel37.Dpi = 100.0!
        Me.XrLabel37.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel37.LocationFloat = New DevExpress.Utils.PointFloat(631.1771!, 693.7292!)
        Me.XrLabel37.Name = "XrLabel37"
        Me.XrLabel37.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel37.SizeF = New System.Drawing.SizeF(41.66669!, 21.0!)
        Me.XrLabel37.StylePriority.UseFont = False
        Me.XrLabel37.Text = "No."
        '
        'XrPageInfo3
        '
        Me.XrPageInfo3.Dpi = 100.0!
        Me.XrPageInfo3.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.XrPageInfo3.Format = "{0:dd/MM/yyyy hh:mm tt}"
        Me.XrPageInfo3.LocationFloat = New DevExpress.Utils.PointFloat(206.3437!, 918.792!)
        Me.XrPageInfo3.Name = "XrPageInfo3"
        Me.XrPageInfo3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo3.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo3.SizeF = New System.Drawing.SizeF(166.6667!, 19.0!)
        Me.XrPageInfo3.StylePriority.UseFont = False
        '
        'xrlNumFacturas1
        '
        Me.xrlNumFacturas1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumFacturas1.Dpi = 100.0!
        Me.xrlNumFacturas1.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlNumFacturas1.LocationFloat = New DevExpress.Utils.PointFloat(25.95831!, 76.75001!)
        Me.xrlNumFacturas1.Multiline = True
        Me.xrlNumFacturas1.Name = "xrlNumFacturas1"
        Me.xrlNumFacturas1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFacturas1.SizeF = New System.Drawing.SizeF(188.3333!, 19.99999!)
        Me.xrlNumFacturas1.StylePriority.UseBorders = False
        Me.xrlNumFacturas1.StylePriority.UseFont = False
        Me.xrlNumFacturas1.Text = "IT OUTSOURCING, S.A. DE C.V."
        '
        'xrlConcepto1
        '
        Me.xrlConcepto1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrlConcepto1.Dpi = 100.0!
        Me.xrlConcepto1.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlConcepto1.LocationFloat = New DevExpress.Utils.PointFloat(208.375!, 174.1667!)
        Me.xrlConcepto1.Name = "xrlConcepto1"
        Me.xrlConcepto1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlConcepto1.SizeF = New System.Drawing.SizeF(550.0!, 50.0!)
        Me.xrlConcepto1.StylePriority.UseBorders = False
        Me.xrlConcepto1.StylePriority.UseFont = False
        Me.xrlConcepto1.Text = "PAGO DE FACTURAS"
        '
        'xrlValor1
        '
        Me.xrlValor1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlValor1.Dpi = 100.0!
        Me.xrlValor1.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold)
        Me.xrlValor1.LocationFloat = New DevExpress.Utils.PointFloat(208.375!, 145.9583!)
        Me.xrlValor1.Name = "xrlValor1"
        Me.xrlValor1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlValor1.SizeF = New System.Drawing.SizeF(122.9167!, 25.0!)
        Me.xrlValor1.StylePriority.UseBorders = False
        Me.xrlValor1.StylePriority.UseFont = False
        Me.xrlValor1.StylePriority.UseTextAlignment = False
        Me.xrlValor1.Text = "1"
        Me.xrlValor1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel18
        '
        Me.XrLabel18.Dpi = 100.0!
        Me.XrLabel18.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(633.5209!, 367.9375!)
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(41.66669!, 21.0!)
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.Text = "No."
        '
        'XrLine3
        '
        Me.XrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine3.Dpi = 100.0!
        Me.XrLine3.LocationFloat = New DevExpress.Utils.PointFloat(468.625!, 623.2292!)
        Me.XrLine3.Name = "XrLine3"
        Me.XrLine3.SizeF = New System.Drawing.SizeF(283.0!, 2.0!)
        Me.XrLine3.StylePriority.UseBorders = False
        '
        'xrlFecha1
        '
        Me.xrlFecha1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlFecha1.Dpi = 100.0!
        Me.xrlFecha1.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlFecha1.LocationFloat = New DevExpress.Utils.PointFloat(208.375!, 98.5417!)
        Me.xrlFecha1.Name = "xrlFecha1"
        Me.xrlFecha1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha1.SizeF = New System.Drawing.SizeF(400.0!, 19.00001!)
        Me.xrlFecha1.StylePriority.UseBorders = False
        Me.xrlFecha1.StylePriority.UseFont = False
        Me.xrlFecha1.Text = "xrlFecha1"
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlRecibio3, Me.xrlRecibio2, Me.xrlNumFacturas12, Me.xrlNumFacturas11, Me.xrlNumFacturas10, Me.xrlNumFacturas9, Me.xrlNumFacturas7, Me.xrlNumFacturas8, Me.xrlNumFacturas5, Me.xrlNumFacturas6, Me.xrlNumFacturas4, Me.xrlNumFacturas3, Me.xrlNumFacturas2, Me.xrlRecibio1, Me.XrLine5, Me.XrLabel34, Me.xrlFecha3, Me.XrPageInfo3, Me.XrLabel31, Me.XrLabel37, Me.xrlNumero3, Me.XrLabel36, Me.xrLogo3, Me.xrlEmpresa3, Me.XrLabel30, Me.xrlValor3, Me.XrLine4, Me.XrLabel21, Me.xrlFechaPago3, Me.XrLabel19, Me.xrlAFavorDe3, Me.XrLabel29, Me.xrlConcepto3, Me.XrLabel25, Me.XrLabel26, Me.XrPageInfo2, Me.XrLabel13, Me.XrPageInfo1, Me.XrLabel10, Me.xrlNumero2, Me.XrLabel20, Me.XrLabel17, Me.XrLabel18, Me.XrLabel24, Me.xrlAFavorDe2, Me.XrLabel22, Me.xrlFecha2, Me.xrlConcepto2, Me.XrLabel12, Me.XrLabel9, Me.XrLine3, Me.xrlValor2, Me.XrLabel16, Me.xrLogo2, Me.XrLabel14, Me.xrlFechaPago2, Me.XrLabel8, Me.xrLogo1, Me.xrlEmpresa2, Me.XrLine2, Me.xrlEmpresa1, Me.XrLabel1, Me.xrlNumero1, Me.XrLabel3, Me.XrLabel6, Me.xrlFechaPago1, Me.XrLabel2, Me.xrlFecha1, Me.xrlNumFacturas1, Me.XrLabel4, Me.xrlAFavorDe1, Me.XrLabel5, Me.xrlValor1, Me.XrLabel11, Me.xrlConcepto1, Me.XrLabel7, Me.XrLine1})
        Me.PageHeader.Dpi = 100.0!
        Me.PageHeader.HeightF = 1006.312!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlRecibio3
        '
        Me.xrlRecibio3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlRecibio3.Dpi = 100.0!
        Me.xrlRecibio3.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlRecibio3.LocationFloat = New DevExpress.Utils.PointFloat(466.2812!, 940.0211!)
        Me.xrlRecibio3.Name = "xrlRecibio3"
        Me.xrlRecibio3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlRecibio3.SizeF = New System.Drawing.SizeF(293.5417!, 18.99997!)
        Me.xrlRecibio3.StylePriority.UseBorders = False
        Me.xrlRecibio3.StylePriority.UseFont = False
        Me.xrlRecibio3.StylePriority.UseTextAlignment = False
        Me.xrlRecibio3.Text = "xrlRecibio1"
        Me.xrlRecibio3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlRecibio2
        '
        Me.xrlRecibio2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlRecibio2.Dpi = 100.0!
        Me.xrlRecibio2.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlRecibio2.LocationFloat = New DevExpress.Utils.PointFloat(464.6667!, 604.2292!)
        Me.xrlRecibio2.Name = "xrlRecibio2"
        Me.xrlRecibio2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlRecibio2.SizeF = New System.Drawing.SizeF(293.5417!, 18.99997!)
        Me.xrlRecibio2.StylePriority.UseBorders = False
        Me.xrlRecibio2.StylePriority.UseFont = False
        Me.xrlRecibio2.StylePriority.UseTextAlignment = False
        Me.xrlRecibio2.Text = "xrlRecibio1"
        Me.xrlRecibio2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlNumFacturas12
        '
        Me.xrlNumFacturas12.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumFacturas12.Dpi = 100.0!
        Me.xrlNumFacturas12.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlNumFacturas12.LocationFloat = New DevExpress.Utils.PointFloat(589.7084!, 743.4792!)
        Me.xrlNumFacturas12.Multiline = True
        Me.xrlNumFacturas12.Name = "xrlNumFacturas12"
        Me.xrlNumFacturas12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFacturas12.SizeF = New System.Drawing.SizeF(175.3333!, 19.99999!)
        Me.xrlNumFacturas12.StylePriority.UseBorders = False
        Me.xrlNumFacturas12.StylePriority.UseFont = False
        Me.xrlNumFacturas12.Text = "IT OUTSOURCING, S.A. DE C.V."
        '
        'xrlNumFacturas11
        '
        Me.xrlNumFacturas11.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumFacturas11.Dpi = 100.0!
        Me.xrlNumFacturas11.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlNumFacturas11.LocationFloat = New DevExpress.Utils.PointFloat(405.9895!, 743.4792!)
        Me.xrlNumFacturas11.Multiline = True
        Me.xrlNumFacturas11.Name = "xrlNumFacturas11"
        Me.xrlNumFacturas11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFacturas11.SizeF = New System.Drawing.SizeF(177.3333!, 19.99999!)
        Me.xrlNumFacturas11.StylePriority.UseBorders = False
        Me.xrlNumFacturas11.StylePriority.UseFont = False
        Me.xrlNumFacturas11.Text = "IT OUTSOURCING, S.A. DE C.V."
        '
        'xrlNumFacturas10
        '
        Me.xrlNumFacturas10.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumFacturas10.Dpi = 100.0!
        Me.xrlNumFacturas10.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlNumFacturas10.LocationFloat = New DevExpress.Utils.PointFloat(214.4167!, 743.4792!)
        Me.xrlNumFacturas10.Multiline = True
        Me.xrlNumFacturas10.Name = "xrlNumFacturas10"
        Me.xrlNumFacturas10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFacturas10.SizeF = New System.Drawing.SizeF(190.3333!, 19.99999!)
        Me.xrlNumFacturas10.StylePriority.UseBorders = False
        Me.xrlNumFacturas10.StylePriority.UseFont = False
        Me.xrlNumFacturas10.Text = "IT OUTSOURCING, S.A. DE C.V."
        '
        'xrlNumFacturas9
        '
        Me.xrlNumFacturas9.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumFacturas9.Dpi = 100.0!
        Me.xrlNumFacturas9.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlNumFacturas9.LocationFloat = New DevExpress.Utils.PointFloat(24.0!, 743.4792!)
        Me.xrlNumFacturas9.Multiline = True
        Me.xrlNumFacturas9.Name = "xrlNumFacturas9"
        Me.xrlNumFacturas9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFacturas9.SizeF = New System.Drawing.SizeF(188.3333!, 19.99999!)
        Me.xrlNumFacturas9.StylePriority.UseBorders = False
        Me.xrlNumFacturas9.StylePriority.UseFont = False
        Me.xrlNumFacturas9.Text = "IT OUTSOURCING, S.A. DE C.V."
        '
        'xrlNumFacturas7
        '
        Me.xrlNumFacturas7.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumFacturas7.Dpi = 100.0!
        Me.xrlNumFacturas7.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlNumFacturas7.LocationFloat = New DevExpress.Utils.PointFloat(408.2603!, 411.6875!)
        Me.xrlNumFacturas7.Multiline = True
        Me.xrlNumFacturas7.Name = "xrlNumFacturas7"
        Me.xrlNumFacturas7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFacturas7.SizeF = New System.Drawing.SizeF(177.3333!, 19.99999!)
        Me.xrlNumFacturas7.StylePriority.UseBorders = False
        Me.xrlNumFacturas7.StylePriority.UseFont = False
        Me.xrlNumFacturas7.Text = "IT OUTSOURCING, S.A. DE C.V."
        '
        'xrlNumFacturas8
        '
        Me.xrlNumFacturas8.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumFacturas8.Dpi = 100.0!
        Me.xrlNumFacturas8.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlNumFacturas8.LocationFloat = New DevExpress.Utils.PointFloat(591.9792!, 411.6875!)
        Me.xrlNumFacturas8.Multiline = True
        Me.xrlNumFacturas8.Name = "xrlNumFacturas8"
        Me.xrlNumFacturas8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFacturas8.SizeF = New System.Drawing.SizeF(175.3333!, 19.99999!)
        Me.xrlNumFacturas8.StylePriority.UseBorders = False
        Me.xrlNumFacturas8.StylePriority.UseFont = False
        Me.xrlNumFacturas8.Text = "IT OUTSOURCING, S.A. DE C.V."
        '
        'xrlNumFacturas5
        '
        Me.xrlNumFacturas5.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumFacturas5.Dpi = 100.0!
        Me.xrlNumFacturas5.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlNumFacturas5.LocationFloat = New DevExpress.Utils.PointFloat(26.2708!, 411.6875!)
        Me.xrlNumFacturas5.Multiline = True
        Me.xrlNumFacturas5.Name = "xrlNumFacturas5"
        Me.xrlNumFacturas5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFacturas5.SizeF = New System.Drawing.SizeF(188.3333!, 19.99999!)
        Me.xrlNumFacturas5.StylePriority.UseBorders = False
        Me.xrlNumFacturas5.StylePriority.UseFont = False
        Me.xrlNumFacturas5.Text = "IT OUTSOURCING, S.A. DE C.V."
        '
        'xrlNumFacturas6
        '
        Me.xrlNumFacturas6.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumFacturas6.Dpi = 100.0!
        Me.xrlNumFacturas6.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlNumFacturas6.LocationFloat = New DevExpress.Utils.PointFloat(216.6875!, 411.6875!)
        Me.xrlNumFacturas6.Multiline = True
        Me.xrlNumFacturas6.Name = "xrlNumFacturas6"
        Me.xrlNumFacturas6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFacturas6.SizeF = New System.Drawing.SizeF(190.3333!, 19.99999!)
        Me.xrlNumFacturas6.StylePriority.UseBorders = False
        Me.xrlNumFacturas6.StylePriority.UseFont = False
        Me.xrlNumFacturas6.Text = "IT OUTSOURCING, S.A. DE C.V."
        '
        'xrlNumFacturas4
        '
        Me.xrlNumFacturas4.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumFacturas4.Dpi = 100.0!
        Me.xrlNumFacturas4.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlNumFacturas4.LocationFloat = New DevExpress.Utils.PointFloat(591.6667!, 76.75001!)
        Me.xrlNumFacturas4.Multiline = True
        Me.xrlNumFacturas4.Name = "xrlNumFacturas4"
        Me.xrlNumFacturas4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFacturas4.SizeF = New System.Drawing.SizeF(175.3333!, 19.99999!)
        Me.xrlNumFacturas4.StylePriority.UseBorders = False
        Me.xrlNumFacturas4.StylePriority.UseFont = False
        Me.xrlNumFacturas4.Text = "IT OUTSOURCING, S.A. DE C.V."
        '
        'xrlNumFacturas3
        '
        Me.xrlNumFacturas3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumFacturas3.Dpi = 100.0!
        Me.xrlNumFacturas3.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlNumFacturas3.LocationFloat = New DevExpress.Utils.PointFloat(407.9478!, 76.75001!)
        Me.xrlNumFacturas3.Multiline = True
        Me.xrlNumFacturas3.Name = "xrlNumFacturas3"
        Me.xrlNumFacturas3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFacturas3.SizeF = New System.Drawing.SizeF(177.3333!, 19.99999!)
        Me.xrlNumFacturas3.StylePriority.UseBorders = False
        Me.xrlNumFacturas3.StylePriority.UseFont = False
        Me.xrlNumFacturas3.Text = "IT OUTSOURCING, S.A. DE C.V."
        '
        'xrlNumFacturas2
        '
        Me.xrlNumFacturas2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumFacturas2.Dpi = 100.0!
        Me.xrlNumFacturas2.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlNumFacturas2.LocationFloat = New DevExpress.Utils.PointFloat(216.375!, 76.75001!)
        Me.xrlNumFacturas2.Multiline = True
        Me.xrlNumFacturas2.Name = "xrlNumFacturas2"
        Me.xrlNumFacturas2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFacturas2.SizeF = New System.Drawing.SizeF(190.3333!, 19.99999!)
        Me.xrlNumFacturas2.StylePriority.UseBorders = False
        Me.xrlNumFacturas2.StylePriority.UseFont = False
        Me.xrlNumFacturas2.Text = "IT OUTSOURCING, S.A. DE C.V."
        '
        'xrlRecibio1
        '
        Me.xrlRecibio1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlRecibio1.Dpi = 100.0!
        Me.xrlRecibio1.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlRecibio1.LocationFloat = New DevExpress.Utils.PointFloat(470.375!, 279.1667!)
        Me.xrlRecibio1.Name = "xrlRecibio1"
        Me.xrlRecibio1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlRecibio1.SizeF = New System.Drawing.SizeF(293.5417!, 18.99997!)
        Me.xrlRecibio1.StylePriority.UseBorders = False
        Me.xrlRecibio1.StylePriority.UseFont = False
        Me.xrlRecibio1.StylePriority.UseTextAlignment = False
        Me.xrlRecibio1.Text = "xrlRecibio1"
        Me.xrlRecibio1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine5
        '
        Me.XrLine5.Dpi = 100.0!
        Me.XrLine5.LineStyle = System.Drawing.Drawing2D.DashStyle.DashDot
        Me.XrLine5.LocationFloat = New DevExpress.Utils.PointFloat(3.0!, 651.5104!)
        Me.XrLine5.Name = "XrLine5"
        Me.XrLine5.SizeF = New System.Drawing.SizeF(767.0!, 2.0!)
        '
        'XrLabel34
        '
        Me.XrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel34.Dpi = 100.0!
        Me.XrLabel34.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel34.LocationFloat = New DevExpress.Utils.PointFloat(23.92699!, 724.4792!)
        Me.XrLabel34.Name = "XrLabel34"
        Me.XrLabel34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel34.SizeF = New System.Drawing.SizeF(501.8334!, 18.99999!)
        Me.XrLabel34.StylePriority.UseBorders = False
        Me.XrLabel34.StylePriority.UseFont = False
        Me.XrLabel34.Text = "Quedan en nuestro poder los siguientes documentos:"
        '
        'xrlFecha3
        '
        Me.xrlFecha3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlFecha3.Dpi = 100.0!
        Me.xrlFecha3.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlFecha3.LocationFloat = New DevExpress.Utils.PointFloat(206.3437!, 764.7292!)
        Me.xrlFecha3.Name = "xrlFecha3"
        Me.xrlFecha3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha3.SizeF = New System.Drawing.SizeF(400.0!, 19.0!)
        Me.xrlFecha3.StylePriority.UseBorders = False
        Me.xrlFecha3.StylePriority.UseFont = False
        Me.xrlFecha3.Text = "xrlFecha"
        '
        'XrLabel31
        '
        Me.XrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel31.Dpi = 100.0!
        Me.XrLabel31.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel31.LocationFloat = New DevExpress.Utils.PointFloat(33.96866!, 764.2709!)
        Me.XrLabel31.Name = "XrLabel31"
        Me.XrLabel31.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel31.SizeF = New System.Drawing.SizeF(153.0!, 19.00001!)
        Me.XrLabel31.StylePriority.UseBorders = False
        Me.XrLabel31.StylePriority.UseFont = False
        Me.XrLabel31.StylePriority.UseTextAlignment = False
        Me.XrLabel31.Text = "Fecha:"
        Me.XrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlNumero3
        '
        Me.xrlNumero3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumero3.Dpi = 100.0!
        Me.xrlNumero3.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold)
        Me.xrlNumero3.LocationFloat = New DevExpress.Utils.PointFloat(679.0937!, 693.7292!)
        Me.xrlNumero3.Name = "xrlNumero3"
        Me.xrlNumero3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero3.SizeF = New System.Drawing.SizeF(77.08325!, 21.0!)
        Me.xrlNumero3.StylePriority.UseBorders = False
        Me.xrlNumero3.StylePriority.UseFont = False
        Me.xrlNumero3.Text = "xrlNumero"
        '
        'XrLabel36
        '
        Me.XrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel36.Dpi = 100.0!
        Me.XrLabel36.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel36.LocationFloat = New DevExpress.Utils.PointFloat(631.1771!, 664.7292!)
        Me.XrLabel36.Name = "XrLabel36"
        Me.XrLabel36.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel36.SizeF = New System.Drawing.SizeF(125.0!, 29.0!)
        Me.XrLabel36.StylePriority.UseBorders = False
        Me.XrLabel36.StylePriority.UseFont = False
        Me.XrLabel36.Text = "QUEDAN"
        '
        'xrLogo3
        '
        Me.xrLogo3.Dpi = 100.0!
        Me.xrLogo3.Image = CType(resources.GetObject("xrLogo3.Image"), System.Drawing.Image)
        Me.xrLogo3.LocationFloat = New DevExpress.Utils.PointFloat(22.96867!, 665.7292!)
        Me.xrLogo3.Name = "xrLogo3"
        Me.xrLogo3.SizeF = New System.Drawing.SizeF(210.0!, 54.00006!)
        Me.xrLogo3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'XrLabel30
        '
        Me.XrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel30.Dpi = 100.0!
        Me.XrLabel30.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(7.656189!, 918.792!)
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.SizeF = New System.Drawing.SizeF(179.0!, 19.0!)
        Me.XrLabel30.StylePriority.UseBorders = False
        Me.XrLabel30.StylePriority.UseFont = False
        Me.XrLabel30.StylePriority.UseTextAlignment = False
        Me.XrLabel30.Text = "Recibido:"
        Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlValor3
        '
        Me.xrlValor3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlValor3.Dpi = 100.0!
        Me.xrlValor3.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold)
        Me.xrlValor3.LocationFloat = New DevExpress.Utils.PointFloat(206.3437!, 812.6876!)
        Me.xrlValor3.Name = "xrlValor3"
        Me.xrlValor3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlValor3.SizeF = New System.Drawing.SizeF(122.9167!, 25.0!)
        Me.xrlValor3.StylePriority.UseBorders = False
        Me.xrlValor3.StylePriority.UseFont = False
        Me.xrlValor3.StylePriority.UseTextAlignment = False
        Me.xrlValor3.Text = "1"
        Me.xrlValor3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLine4
        '
        Me.XrLine4.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine4.Dpi = 100.0!
        Me.XrLine4.LocationFloat = New DevExpress.Utils.PointFloat(466.2812!, 959.021!)
        Me.XrLine4.Name = "XrLine4"
        Me.XrLine4.SizeF = New System.Drawing.SizeF(283.0!, 2.0!)
        Me.XrLine4.StylePriority.UseBorders = False
        '
        'XrLabel21
        '
        Me.XrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel21.Dpi = 100.0!
        Me.XrLabel21.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(206.3437!, 896.6459!)
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel21.SizeF = New System.Drawing.SizeF(250.0!, 19.0!)
        Me.XrLabel21.StylePriority.UseBorders = False
        Me.XrLabel21.StylePriority.UseFont = False
        Me.XrLabel21.Text = "Para su revision y pago el d�a:"
        '
        'XrLabel19
        '
        Me.XrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel19.Dpi = 100.0!
        Me.XrLabel19.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(33.96866!, 812.6876!)
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.SizeF = New System.Drawing.SizeF(153.0!, 25.0!)
        Me.XrLabel19.StylePriority.UseBorders = False
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        Me.XrLabel19.Text = "Por un valor de:"
        Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'xrlAFavorDe3
        '
        Me.xrlAFavorDe3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlAFavorDe3.Dpi = 100.0!
        Me.xrlAFavorDe3.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlAFavorDe3.LocationFloat = New DevExpress.Utils.PointFloat(206.3437!, 789.2709!)
        Me.xrlAFavorDe3.Name = "xrlAFavorDe3"
        Me.xrlAFavorDe3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlAFavorDe3.SizeF = New System.Drawing.SizeF(550.0001!, 18.0!)
        Me.xrlAFavorDe3.StylePriority.UseBorders = False
        Me.xrlAFavorDe3.StylePriority.UseFont = False
        Me.xrlAFavorDe3.Text = "xrlAFavorDe"
        '
        'XrLabel29
        '
        Me.XrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel29.Dpi = 100.0!
        Me.XrLabel29.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(33.96866!, 789.2709!)
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.SizeF = New System.Drawing.SizeF(153.0!, 18.0!)
        Me.XrLabel29.StylePriority.UseBorders = False
        Me.XrLabel29.StylePriority.UseFont = False
        Me.XrLabel29.StylePriority.UseTextAlignment = False
        Me.XrLabel29.Text = "A favor de:"
        Me.XrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlConcepto3
        '
        Me.xrlConcepto3.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrlConcepto3.Dpi = 100.0!
        Me.xrlConcepto3.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlConcepto3.LocationFloat = New DevExpress.Utils.PointFloat(206.3437!, 842.6459!)
        Me.xrlConcepto3.Name = "xrlConcepto3"
        Me.xrlConcepto3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlConcepto3.SizeF = New System.Drawing.SizeF(550.0!, 50.0!)
        Me.xrlConcepto3.StylePriority.UseBorders = False
        Me.xrlConcepto3.StylePriority.UseFont = False
        Me.xrlConcepto3.Text = "PAGO DE FACTURAS"
        '
        'XrLabel25
        '
        Me.XrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel25.Dpi = 100.0!
        Me.XrLabel25.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(549.6563!, 961.021!)
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel25.SizeF = New System.Drawing.SizeF(123.1875!, 20.0!)
        Me.XrLabel25.StylePriority.UseBorders = False
        Me.XrLabel25.StylePriority.UseFont = False
        Me.XrLabel25.StylePriority.UseTextAlignment = False
        Me.XrLabel25.Text = "Recibido por"
        Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel26
        '
        Me.XrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel26.Dpi = 100.0!
        Me.XrLabel26.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(33.96866!, 841.8959!)
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.SizeF = New System.Drawing.SizeF(153.0!, 19.0!)
        Me.XrLabel26.StylePriority.UseBorders = False
        Me.XrLabel26.StylePriority.UseFont = False
        Me.XrLabel26.StylePriority.UseTextAlignment = False
        Me.XrLabel26.Text = "En concepto de:"
        Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.Dpi = 100.0!
        Me.XrPageInfo2.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.XrPageInfo2.Format = "{0:dd/MM/yyyy hh:mm tt}"
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(208.6875!, 587.0!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(166.6667!, 19.0!)
        Me.XrPageInfo2.StylePriority.UseFont = False
        '
        'XrLabel13
        '
        Me.XrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel13.Dpi = 100.0!
        Me.XrLabel13.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 587.0!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(179.0!, 19.0!)
        Me.XrLabel13.StylePriority.UseBorders = False
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "Recibido:"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Dpi = 100.0!
        Me.XrPageInfo1.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.XrPageInfo1.Format = "{0:dd/MM/yyyy hh:mm tt}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(208.6875!, 249.1667!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(166.6667!, 19.0!)
        Me.XrPageInfo1.StylePriority.UseFont = False
        '
        'XrLabel10
        '
        Me.XrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel10.Dpi = 100.0!
        Me.XrLabel10.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 249.1667!)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(179.0!, 19.0!)
        Me.XrLabel10.StylePriority.UseBorders = False
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.StylePriority.UseTextAlignment = False
        Me.XrLabel10.Text = "Recibido:"
        Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlNumero2
        '
        Me.xrlNumero2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumero2.Dpi = 100.0!
        Me.xrlNumero2.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold)
        Me.xrlNumero2.LocationFloat = New DevExpress.Utils.PointFloat(681.4375!, 367.9375!)
        Me.xrlNumero2.Name = "xrlNumero2"
        Me.xrlNumero2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero2.SizeF = New System.Drawing.SizeF(77.08325!, 21.0!)
        Me.xrlNumero2.StylePriority.UseBorders = False
        Me.xrlNumero2.StylePriority.UseFont = False
        Me.xrlNumero2.Text = "xrlNumero"
        '
        'XrLabel20
        '
        Me.XrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel20.Dpi = 100.0!
        Me.XrLabel20.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(26.2708!, 391.6875!)
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.SizeF = New System.Drawing.SizeF(501.8334!, 18.99999!)
        Me.XrLabel20.StylePriority.UseBorders = False
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.Text = "Quedan en nuestro poder los siguientes documentos:"
        '
        'XrLabel24
        '
        Me.XrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel24.Dpi = 100.0!
        Me.XrLabel24.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(36.31247!, 460.4792!)
        Me.XrLabel24.Name = "XrLabel24"
        Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel24.SizeF = New System.Drawing.SizeF(153.0!, 18.0!)
        Me.XrLabel24.StylePriority.UseBorders = False
        Me.XrLabel24.StylePriority.UseFont = False
        Me.XrLabel24.StylePriority.UseTextAlignment = False
        Me.XrLabel24.Text = "A favor de:"
        Me.XrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlAFavorDe2
        '
        Me.xrlAFavorDe2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlAFavorDe2.Dpi = 100.0!
        Me.xrlAFavorDe2.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlAFavorDe2.LocationFloat = New DevExpress.Utils.PointFloat(208.6875!, 460.4792!)
        Me.xrlAFavorDe2.Name = "xrlAFavorDe2"
        Me.xrlAFavorDe2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlAFavorDe2.SizeF = New System.Drawing.SizeF(550.0001!, 18.0!)
        Me.xrlAFavorDe2.StylePriority.UseBorders = False
        Me.xrlAFavorDe2.StylePriority.UseFont = False
        Me.xrlAFavorDe2.Text = "xrlAFavorDe"
        '
        'XrLabel22
        '
        Me.XrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel22.Dpi = 100.0!
        Me.XrLabel22.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(36.31247!, 435.4792!)
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel22.SizeF = New System.Drawing.SizeF(153.0!, 19.00001!)
        Me.XrLabel22.StylePriority.UseBorders = False
        Me.XrLabel22.StylePriority.UseFont = False
        Me.XrLabel22.StylePriority.UseTextAlignment = False
        Me.XrLabel22.Text = "Fecha:"
        Me.XrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlFecha2
        '
        Me.xrlFecha2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlFecha2.Dpi = 100.0!
        Me.xrlFecha2.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlFecha2.LocationFloat = New DevExpress.Utils.PointFloat(208.6875!, 435.9375!)
        Me.xrlFecha2.Name = "xrlFecha2"
        Me.xrlFecha2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha2.SizeF = New System.Drawing.SizeF(400.0!, 19.0!)
        Me.xrlFecha2.StylePriority.UseBorders = False
        Me.xrlFecha2.StylePriority.UseFont = False
        Me.xrlFecha2.Text = "xrlFecha"
        '
        'xrlConcepto2
        '
        Me.xrlConcepto2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.xrlConcepto2.Dpi = 100.0!
        Me.xrlConcepto2.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlConcepto2.LocationFloat = New DevExpress.Utils.PointFloat(208.6875!, 512.8542!)
        Me.xrlConcepto2.Name = "xrlConcepto2"
        Me.xrlConcepto2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlConcepto2.SizeF = New System.Drawing.SizeF(550.0!, 50.0!)
        Me.xrlConcepto2.StylePriority.UseBorders = False
        Me.xrlConcepto2.StylePriority.UseFont = False
        Me.xrlConcepto2.Text = "PAGO DE FACTURAS"
        '
        'XrLabel12
        '
        Me.XrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel12.Dpi = 100.0!
        Me.XrLabel12.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(36.31247!, 512.1042!)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(153.0!, 19.0!)
        Me.XrLabel12.StylePriority.UseBorders = False
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "En concepto de:"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel9
        '
        Me.XrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel9.Dpi = 100.0!
        Me.XrLabel9.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(552.0001!, 625.2291!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(123.1875!, 20.0!)
        Me.XrLabel9.StylePriority.UseBorders = False
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.Text = "Recibido por"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlValor2
        '
        Me.xrlValor2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlValor2.Dpi = 100.0!
        Me.xrlValor2.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold)
        Me.xrlValor2.LocationFloat = New DevExpress.Utils.PointFloat(208.6875!, 481.8959!)
        Me.xrlValor2.Name = "xrlValor2"
        Me.xrlValor2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlValor2.SizeF = New System.Drawing.SizeF(122.9167!, 25.0!)
        Me.xrlValor2.StylePriority.UseBorders = False
        Me.xrlValor2.StylePriority.UseFont = False
        Me.xrlValor2.StylePriority.UseTextAlignment = False
        Me.xrlValor2.Text = "1"
        Me.xrlValor2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel16
        '
        Me.XrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel16.Dpi = 100.0!
        Me.XrLabel16.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(208.6875!, 566.8542!)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(250.0!, 19.0!)
        Me.XrLabel16.StylePriority.UseBorders = False
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.Text = "Para su revision y pago el d�a:"
        '
        'xrLogo2
        '
        Me.xrLogo2.Dpi = 100.0!
        Me.xrLogo2.Image = CType(resources.GetObject("xrLogo2.Image"), System.Drawing.Image)
        Me.xrLogo2.LocationFloat = New DevExpress.Utils.PointFloat(25.31249!, 334.9375!)
        Me.xrLogo2.Name = "xrLogo2"
        Me.xrLogo2.SizeF = New System.Drawing.SizeF(210.0!, 54.00003!)
        Me.xrLogo2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'XrLabel14
        '
        Me.XrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel14.Dpi = 100.0!
        Me.XrLabel14.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(36.31247!, 481.8959!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(153.0!, 25.0!)
        Me.XrLabel14.StylePriority.UseBorders = False
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        Me.XrLabel14.Text = "Por un valor de:"
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'xrlFechaPago2
        '
        Me.xrlFechaPago2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlFechaPago2.Dpi = 100.0!
        Me.xrlFechaPago2.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlFechaPago2.LocationFloat = New DevExpress.Utils.PointFloat(465.1458!, 566.8542!)
        Me.xrlFechaPago2.Name = "xrlFechaPago2"
        Me.xrlFechaPago2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFechaPago2.SizeF = New System.Drawing.SizeF(293.5417!, 18.99997!)
        Me.xrlFechaPago2.StylePriority.UseBorders = False
        Me.xrlFechaPago2.StylePriority.UseFont = False
        Me.xrlFechaPago2.Text = "xrlFechaPago"
        '
        'xrLogo1
        '
        Me.xrLogo1.Dpi = 100.0!
        Me.xrLogo1.Image = CType(resources.GetObject("xrLogo1.Image"), System.Drawing.Image)
        Me.xrLogo1.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 0.000008106232!)
        Me.xrLogo1.Name = "xrLogo1"
        Me.xrLogo1.SizeF = New System.Drawing.SizeF(210.0!, 54.0!)
        Me.xrLogo1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'xrlEmpresa2
        '
        Me.xrlEmpresa2.Dpi = 100.0!
        Me.xrlEmpresa2.Font = New System.Drawing.Font("Arial", 14.0!)
        Me.xrlEmpresa2.LocationFloat = New DevExpress.Utils.PointFloat(268.375!, 336.9792!)
        Me.xrlEmpresa2.Name = "xrlEmpresa2"
        Me.xrlEmpresa2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlEmpresa2.SizeF = New System.Drawing.SizeF(325.2917!, 25.0!)
        Me.xrlEmpresa2.StylePriority.UseFont = False
        Me.xrlEmpresa2.StylePriority.UseTextAlignment = False
        Me.xrlEmpresa2.Text = "SU EMPRESA"
        Me.xrlEmpresa2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrlEmpresa2.Visible = False
        '
        'XrLine2
        '
        Me.XrLine2.Dpi = 100.0!
        Me.XrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.DashDot
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(0!, 328.1667!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(767.0!, 2.0!)
        '
        'xrlEmpresa1
        '
        Me.xrlEmpresa1.Dpi = 100.0!
        Me.xrlEmpresa1.Font = New System.Drawing.Font("Arial", 14.0!)
        Me.xrlEmpresa1.LocationFloat = New DevExpress.Utils.PointFloat(303.7501!, 23.0!)
        Me.xrlEmpresa1.Name = "xrlEmpresa1"
        Me.xrlEmpresa1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlEmpresa1.SizeF = New System.Drawing.SizeF(274.2499!, 25.0!)
        Me.xrlEmpresa1.StylePriority.UseFont = False
        Me.xrlEmpresa1.StylePriority.UseTextAlignment = False
        Me.xrlEmpresa1.Text = "SU EMPRESA, S.A. DE C.V."
        Me.xrlEmpresa1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrlEmpresa1.Visible = False
        '
        'XrLabel1
        '
        Me.XrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel1.Dpi = 100.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(633.2084!, 0.000008106232!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(125.0!, 29.0!)
        Me.XrLabel1.StylePriority.UseBorders = False
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.Text = "QUEDAN"
        '
        'xrlNumero1
        '
        Me.xrlNumero1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNumero1.Dpi = 100.0!
        Me.xrlNumero1.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold)
        Me.xrlNumero1.LocationFloat = New DevExpress.Utils.PointFloat(681.1251!, 29.00001!)
        Me.xrlNumero1.Name = "xrlNumero1"
        Me.xrlNumero1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero1.SizeF = New System.Drawing.SizeF(77.08325!, 21.0!)
        Me.xrlNumero1.StylePriority.UseBorders = False
        Me.xrlNumero1.StylePriority.UseFont = False
        Me.xrlNumero1.Text = "xrlNumero1"
        '
        'XrLabel3
        '
        Me.XrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel3.Dpi = 100.0!
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(25.95833!, 57.75002!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(501.8334!, 17.99999!)
        Me.XrLabel3.StylePriority.UseBorders = False
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.Text = "Quedan en nuestro poder los siguientes documentos:"
        '
        'XrLabel6
        '
        Me.XrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel6.Dpi = 100.0!
        Me.XrLabel6.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(208.375!, 226.7501!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(250.0!, 19.0!)
        Me.XrLabel6.StylePriority.UseBorders = False
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.Text = "Para su revision y pago el d�a:"
        '
        'xrlFechaPago1
        '
        Me.xrlFechaPago1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlFechaPago1.Dpi = 100.0!
        Me.xrlFechaPago1.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlFechaPago1.LocationFloat = New DevExpress.Utils.PointFloat(464.8334!, 226.7501!)
        Me.xrlFechaPago1.Name = "xrlFechaPago1"
        Me.xrlFechaPago1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFechaPago1.SizeF = New System.Drawing.SizeF(293.5417!, 18.99997!)
        Me.xrlFechaPago1.StylePriority.UseBorders = False
        Me.xrlFechaPago1.StylePriority.UseFont = False
        Me.xrlFechaPago1.Text = "xrlFechaPago1"
        '
        'XrLabel2
        '
        Me.XrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 97.5417!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(153.0!, 19.00001!)
        Me.XrLabel2.StylePriority.UseBorders = False
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "Fecha:"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel4
        '
        Me.XrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel4.Dpi = 100.0!
        Me.XrLabel4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 122.5417!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(153.0!, 18.0!)
        Me.XrLabel4.StylePriority.UseBorders = False
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "A favor de:"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlAFavorDe1
        '
        Me.xrlAFavorDe1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlAFavorDe1.Dpi = 100.0!
        Me.xrlAFavorDe1.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.xrlAFavorDe1.LocationFloat = New DevExpress.Utils.PointFloat(208.375!, 122.5417!)
        Me.xrlAFavorDe1.Name = "xrlAFavorDe1"
        Me.xrlAFavorDe1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlAFavorDe1.SizeF = New System.Drawing.SizeF(550.0001!, 18.0!)
        Me.xrlAFavorDe1.StylePriority.UseBorders = False
        Me.xrlAFavorDe1.StylePriority.UseFont = False
        Me.xrlAFavorDe1.Text = "xrlAFavorDe1"
        '
        'XrLabel5
        '
        Me.XrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel5.Dpi = 100.0!
        Me.XrLabel5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 145.9583!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(153.0!, 25.0!)
        Me.XrLabel5.StylePriority.UseBorders = False
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "Por un valor de:"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel11
        '
        Me.XrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel11.Dpi = 100.0!
        Me.XrLabel11.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 174.1667!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(153.0!, 19.0!)
        Me.XrLabel11.StylePriority.UseBorders = False
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "En concepto de:"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel7
        '
        Me.XrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLabel7.Dpi = 100.0!
        Me.XrLabel7.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(541.125!, 300.1667!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(115.625!, 20.0!)
        Me.XrLabel7.StylePriority.UseBorders = False
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "Recibido por"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine1
        '
        Me.XrLine1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine1.Dpi = 100.0!
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(470.375!, 298.1667!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(283.0!, 2.0!)
        Me.XrLine1.StylePriority.UseBorders = False
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.Dpi = 100.0!
        Me.TopMarginBand1.HeightF = 25.0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.Dpi = 100.0!
        Me.BottomMarginBand1.HeightF = 0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'Detail
        '
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'cpp_rptQuedan
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.DrawGrid = False
        Me.Margins = New System.Drawing.Printing.Margins(35, 35, 25, 0)
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.SnapGridSize = 5.0!
        Me.Version = "16.2"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents xrlFechaPago3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents xrlEmpresa3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel37 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo3 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents xrlNumFacturas1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlConcepto1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlValor1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlFecha1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents XrLine5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel34 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFecha3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel31 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumero3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel36 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLogo3 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlValor3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine4 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlAFavorDe3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlConcepto3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumero2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlAFavorDe2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFecha2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlConcepto2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlValor2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLogo2 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFechaPago2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLogo1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents xrlEmpresa2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlEmpresa1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumero1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFechaPago1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlAFavorDe1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents xrlRecibio1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFacturas3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFacturas2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFacturas4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlRecibio3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlRecibio2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFacturas12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFacturas11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFacturas10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFacturas9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFacturas7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFacturas8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFacturas5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFacturas6 As DevExpress.XtraReports.UI.XRLabel
End Class
