﻿
Imports NexusBLL
Public Class cpp_frmEstadoCuenta
    Dim bl As New CuentasPPBLL(g_ConnectionString)
    Private Sub cpc_frmDocsPendiente_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deHasta.EditValue = Today
        BeProveedor1.beCodigo.EditValue = ""

        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub cpc_frmDocsPendiente_Reporte() Handles Me.Reporte
        Dim dt = bl.cpp_EstadoCuenta(leSucursal.EditValue, deHasta.EditValue, BeProveedor1.beCodigo.EditValue, BeProveedor2.beCodigo.EditValue)
        Dim rpt As New cpp_rptEstadoCuenta() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "ESTADO DE CUENTA"
        rpt.xrlCliente.Text = BeProveedor1.teNombre.EditValue
        rpt.xrlPeriodo.Text = "AL " + (FechaToString(deHasta.EditValue, deHasta.EditValue)).ToUpper
        rpt.ShowPreviewDialog()
    End Sub


End Class
