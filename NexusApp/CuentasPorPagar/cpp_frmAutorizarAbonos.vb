﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class cpp_frmAutorizarAbonos
    Dim bl As New CuentasPPBLL(g_ConnectionString)
    Dim blBank As New BancosBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub cpp_frmAutorizarAbonos_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        objCombos.banTiposTransaccion(leTipoTransaccion, "")
        objCombos.conTiposPartida(leTipoPartida, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.banCuentasBancarias(leCtaBancaria)
        leTipoTransaccion.EditValue = 3
        leSucursal.EditValue = piIdSucursalUsuario
        gc.DataSource = bl.cpp_ObtenerAbonosPendientes(leSucursal.EditValue)
        deFecha.EditValue = Today
    End Sub

    Private Sub gc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc.Click
        teANombreDe.EditValue = gv.GetRowCellValue(gv.FocusedRowHandle, "Nombre")
        meConcepto.EditValue = gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto")    '"ABONO REALIZADO ÉSTE DÍA, " &
        'leSucursal.EditValue = gv.GetRowCellValue(
    End Sub

    Private Sub rgFormaPago_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgFormaPago.EditValueChanged
        lblTipoTransaccion.Visible = rgFormaPago.EditValue = 1
        leTipoTransaccion.Visible = rgFormaPago.EditValue = 1

        If rgFormaPago.EditValue = 2 Then
            btAplicar.Text = "Aplicar Abono"
        Else
            btAplicar.Text = "Aplicar al Banco"
        End If
    End Sub

    Private Sub btAplicar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btAplicar.Click
        If MsgBox("¿Está seguro(a) de aplicar y autorizar este abono?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        If rgFormaPago.EditValue = 0 Then
            InsertarCheque()
        End If
        If rgFormaPago.EditValue = 1 Then
            InsertarTransaccion()
        End If
        If rgFormaPago.EditValue = 2 Then
            InsertarEfectivo()
        End If
        If rgFormaPago.EditValue = 3 Then
            If MsgBox("Está seguro(a) que este abono será autorizado sin transacción?" & Chr(13) & _
            "La autorización no generará ningun documento contable" & Chr(13) & Chr(13) & "FAVOR CONFIRME NUEVAMENTE...", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
                Exit Sub
            End If

            bl.cpp_AutorizaAbonosSinTransaccion(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), objMenu.User)
            MsgBox("El documento fue autorizado con éxito" & Chr(13) & "No se generó ningún documento", 64, "Nota")
            gc.DataSource = bl.cpp_ObtenerAbonosPendientes(leSucursal.EditValue)
        End If
    End Sub
    Private Sub InsertarCheque()
        Dim Cheque = New ban_Cheques, sConcepto As String = "", ChequeDetalle As New List(Of ban_ChequesDetalle)
        sConcepto = " Comprobante " & bl.cpp_ObtenerDocumentosAbonados(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProveedor"), gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"))
        With Cheque
            .IdCuentaBancaria = leCtaBancaria.EditValue            
            .Numero = blBank.GetNumeroCheque(leCtaBancaria.EditValue)
            .Fecha = deFecha.DateTime
            .Valor = gv.GetRowCellValue(gv.FocusedRowHandle, "TotalAbono")
            .AnombreDe = teANombreDe.EditValue
            .Concepto = meConcepto.EditValue & sConcepto
            .IdProveedor = gv.GetRowCellValue(gv.FocusedRowHandle, "IdProveedor")
            .IdSucursal = gv.GetRowCellValue(gv.FocusedRowHandle, "IdSucursal")
            .IdTipoPartida = leTipoPartida.EditValue
            .Impreso = False
            .Conciliado = False
            .Anulado = False
            .MotivoAnulacion = ""
            .PagadoBanco = True
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .FechaHoraModificacion = Nothing
            .ModificadoPor = ""
        End With
        Dim entCta As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(Cheque.IdCuentaBancaria)
        Dim entPro As com_Proveedores = objTablas.com_proveedoresSelectByPK(Cheque.IdProveedor)
        Dim entDet As New ban_ChequesDetalle
        With entDet
            .IdCheque = 0
            .IdDetalle = 1
            .IdCuenta = entPro.IdCuentaContable   'la cuenta por pagar al proveedor
            .Referencia = gv.GetRowCellValue(gv.FocusedRowHandle, "NumeroComprobante")
            .Concepto = "Cheque " + Cheque.Numero + ". " + teANombreDe.EditValue + gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto") & sConcepto
            .Debe = Cheque.Valor  'el total a pagar al proveedor
            .Haber = 0.0
            .IdCentro = ""
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        ChequeDetalle.Add(entDet)
        entDet = New ban_ChequesDetalle  'agregando el haber al banco
        With entDet
            .IdCheque = 0
            .IdDetalle = 2
            .IdCuenta = entCta.IdCuentaContable 'la cuenta por pagar al proveedor
            .Referencia = gv.GetRowCellValue(gv.FocusedRowHandle, "NumeroComprobante")
            .Concepto = "Cheque " + Cheque.Numero + ". " + teANombreDe.EditValue + gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto") & sConcepto
            .Debe = 0.0
            .Haber = Cheque.Valor  'el total a pagar al proveedor
            .IdCentro = ""
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        ChequeDetalle.Add(entDet)
        Dim Contabilizar As Boolean = True 'gsNombre_Empresa.StartsWith("PITUTA")
        Dim msj As String = blBank.ban_InsertarCheque(Cheque, ChequeDetalle, Contabilizar)
        If msj = "Ok" Then
            bl.cpp_AutorizaAbonosCheque(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), Cheque.IdCheque, objMenu.User)
            MsgBox("El cheque fue guardado con éxito", 64, "Nota")
        Else
            MsgBox("NO FUE POSIBLE GUARDAR EL CHEQUE" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
        End If
        Close()
    End Sub
    Private Sub InsertarTransaccion()
        Dim Transacciones As New ban_Transacciones
        Dim sConcepto As String = ""
        Dim TransaccionesDetalle As New List(Of ban_TransaccionesDetalle)
        sConcepto = " Comprobante " & bl.cpp_ObtenerDocumentosAbonados(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProveedor"), gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"))
        With Transacciones
            .IdCuentaBancaria = leCtaBancaria.EditValue
            .Fecha = deFecha.EditValue
            .FechaContable = deFecha.EditValue
            .Valor = gv.GetRowCellValue(gv.FocusedRowHandle, "TotalAbono")
            .IdTipo = leTipoTransaccion.EditValue
            .Concepto = meConcepto.EditValue & sConcepto
            .IdSucursal = gv.GetRowCellValue(gv.FocusedRowHandle, "IdSucursal")
            .IdTipoPartida = leTipoPartida.EditValue
            .Contabilizar = True
            .IncluirConciliacion = True
            .ProcesadaBanco = True
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .FechaHoraModificacion = Nothing
            .ModificadoPor = ""
        End With
        Dim entCta As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(Transacciones.IdCuentaBancaria)
        Dim entPro As com_Proveedores = objTablas.com_proveedoresSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProveedor"))

        Dim entDet As New ban_TransaccionesDetalle
        With entDet
            .IdTransaccion = 0
            .IdDetalle = 1
            .IdCuenta = entPro.IdCuentaContable   'la cuenta por pagar al proveedor
            .Referencia = gv.GetRowCellValue(gv.FocusedRowHandle, "NumeroComprobante")
            .Concepto = entPro.Nombre + " " + gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto") & sConcepto
            .Debe = Transacciones.Valor  'el total a pagar al proveedor
            .Haber = 0.0
            .IdCentro = ""
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        TransaccionesDetalle.Add(entDet)
        entDet = New ban_TransaccionesDetalle
        'agregando el haber al banco
        With entDet
            .IdTransaccion = 0
            .IdDetalle = 2
            .IdCuenta = entCta.IdCuentaContable 'la cuenta por pagar al proveedor
            .Referencia = gv.GetRowCellValue(gv.FocusedRowHandle, "NumeroComprobante")
            .Concepto = entPro.Nombre + " " + gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto") & sConcepto
            .Debe = 0.0
            .Haber = Transacciones.Valor  'el total a pagar al proveedor
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        TransaccionesDetalle.Add(entDet)

        Dim Contabilizar As Boolean = True ' gsNombre_Empresa.StartsWith("PITUTA")

        Dim msj As String = blBank.InsertarTransaccion(Transacciones, TransaccionesDetalle, Contabilizar)
        If msj = "Ok" Then
            bl.cpp_AutorizaAbonosTransaccion(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), Transacciones.IdTransaccion, objMenu.User)
            MsgBox("La transacción ha sido registrada con éxito", 64, "Nota")
        Else
            MsgBox("No fue posible guardar la transacción" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
        End If
        Close()
    End Sub
    Private Sub InsertarEfectivo()

        Dim sConcepto As String = " Comprobante " & bl.cpp_ObtenerDocumentosAbonados(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProveedor"), gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"))
        Dim Partidas As New con_Partidas, PartidasDetalle As New List(Of con_PartidasDetalle)

        Dim entPro As com_Proveedores = objTablas.com_ProveedoresSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProveedor"))
        Dim entCaja As com_CajasChica = objTablas.com_CajasChicaSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCaja"))
        With Partidas
            .IdPartida = 0
            .IdSucursal = leSucursal.EditValue
            .IdTipo = leTipoPartida.EditValue
            .Numero = ""
            .Fecha = deFecha.EditValue
            .Concepto = sConcepto
            .Actualizada = 0
            .IdModuloOrigen = 1   'PARA EL CASO DE ABONOS EN EFECTIVO SI PODRÁ ELIMINAR LA PARTIDA
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .ModificadoPor = ""
            .FechaHoraModificacion = Nothing
        End With

        Dim entPartidaDetalle As New con_PartidasDetalle
        With entPartidaDetalle
            .IdPartida = 0
            .IdDetalle = 1
            .IdCuenta = entPro.IdCuentaContable
            .Referencia = gv.GetRowCellValue(gv.FocusedRowHandle, "NumeroComprobante")
            .Concepto = entPro.Nombre + " " + gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto") & sConcepto
            .Debe = gv.GetRowCellValue(gv.FocusedRowHandle, "TotalAbono")
            .Haber = 0.0
            .IdCentro = ""
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        PartidasDetalle.Add(entPartidaDetalle)

        Dim entPartidaDetalle1 As New con_PartidasDetalle
        With entPartidaDetalle1
            .IdPartida = 0
            .IdDetalle = 2
            .IdCuenta = IIf(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCaja") > 1, entCaja.IdCuentaContable, dtParam.Rows(0).Item("IdCuentaCaja"))
            .Referencia = gv.GetRowCellValue(gv.FocusedRowHandle, "NumeroComprobante")
            .Concepto = entPro.Nombre + " " + gv.GetRowCellValue(gv.FocusedRowHandle, "Concepto") & sConcepto
            .Debe = 0.0
            .Haber = gv.GetRowCellValue(gv.FocusedRowHandle, "TotalAbono")
            .IdCentro = ""
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        PartidasDetalle.Add(entPartidaDetalle1)
 

        Dim msj As String = bl.cpp_InsertaPartida(Partidas, PartidasDetalle)
        If msj = "" Then
            bl.cpp_AutorizaAbonosTransaccion(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), -999, objMenu.User)
            MsgBox("La transacción ha sido registrada con éxito", 64, "Nota")
        Else
            MsgBox("No fue posible guardar la transacción" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
        End If
        Close()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        gc.DataSource = bl.cpp_ObtenerAbonosPendientes(leSucursal.EditValue)
    End Sub
    
End Class
