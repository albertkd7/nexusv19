﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class cpp_frmHistoricoMovimientos
    Dim myBL As New CuentasPPBLL(g_ConnectionString)

    Private Sub cpp_frmEstado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        deDesde.EditValue = Today
        deHasta.EditValue = Today
    End Sub

    Private Sub cpp_frmMovimientosProveedor_Reporte() Handles Me.Reporte
        If BeProveedor1.beCodigo.EditValue = "" Then
            MsgBox("Debe de especificar el proveedor", 64, "Nota")
            Exit Sub
        End If
        Dim dt = myBL.cpp_MovimientosProveedor(BeProveedor1.beCodigo.EditValue, deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        If dt.Rows.Count = 0 Then
            MsgBox("No se encontraron transacciones para mostrar informe", 64, "Nota")
            Exit Sub
        End If
        'VERIFICAR SALDO EN EL REPORTE
        Dim rpt As New cpp_rptMovimientosProveedor() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "HISTORICO DE MOVIMIENTOS DEL PROVEEDOR " & (BeProveedor1.teNombre.EditValue).ToString.ToUpper
        rpt.xrlSucursal.Text = leSucursal.Text
        rpt.xrlPeriodo.Text = (FechaToString(deDesde.EditValue, deHasta.EditValue)).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda
        'rpt.ShowDesignerDialog()
        rpt.ShowPreviewDialog()


    End Sub
End Class
