﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpp_frmAutorizarAbonos2
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumeroComprobante = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaAbono = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.teMontoAbonar = New DevExpress.XtraEditors.TextEdit()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.rgFormaPago = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoTransaccion = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.lblTipoTransaccion = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoPartida = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.btAplicar = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.meConcepto = New DevExpress.XtraEditors.MemoEdit()
        Me.teANombreDe = New DevExpress.XtraEditors.TextEdit()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcBanco = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leCtaBancaria = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gclMontoCheque = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumeroComprobante.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaAbono.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teMontoAbonar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgFormaPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoTransaccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teANombreDe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCtaBancaria, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.leSucursal)
        Me.PanelControl1.Controls.Add(Me.LabelControl16)
        Me.PanelControl1.Controls.Add(Me.SimpleButton1)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.teNumeroComprobante)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.deFechaAbono)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.teMontoAbonar)
        Me.PanelControl1.Controls.Add(Me.deFecha)
        Me.PanelControl1.Controls.Add(Me.rgFormaPago)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.leTipoTransaccion)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.lblTipoTransaccion)
        Me.PanelControl1.Controls.Add(Me.leTipoPartida)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.btAplicar)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.meConcepto)
        Me.PanelControl1.Controls.Add(Me.teANombreDe)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(893, 145)
        Me.PanelControl1.TabIndex = 3
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(642, 10)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(218, 20)
        Me.leSucursal.TabIndex = 89
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(597, 14)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl16.TabIndex = 90
        Me.LabelControl16.Text = "Sucursal:"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(191, 10)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(25, 23)
        Me.SimpleButton1.TabIndex = 26
        Me.SimpleButton1.Text = "..."
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(12, 15)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl1.TabIndex = 21
        Me.LabelControl1.Text = "No. de Recibo:"
        '
        'teNumeroComprobante
        '
        Me.teNumeroComprobante.Enabled = False
        Me.teNumeroComprobante.EnterMoveNextControl = True
        Me.teNumeroComprobante.Location = New System.Drawing.Point(86, 12)
        Me.teNumeroComprobante.Name = "teNumeroComprobante"
        Me.teNumeroComprobante.Size = New System.Drawing.Size(104, 20)
        Me.teNumeroComprobante.TabIndex = 20
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(220, 14)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl6.TabIndex = 24
        Me.LabelControl6.Text = "Fecha del Abono:"
        '
        'deFechaAbono
        '
        Me.deFechaAbono.EditValue = Nothing
        Me.deFechaAbono.Enabled = False
        Me.deFechaAbono.EnterMoveNextControl = True
        Me.deFechaAbono.Location = New System.Drawing.Point(311, 12)
        Me.deFechaAbono.Name = "deFechaAbono"
        Me.deFechaAbono.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaAbono.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaAbono.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaAbono.Size = New System.Drawing.Size(89, 20)
        Me.deFechaAbono.TabIndex = 22
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(404, 14)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl8.TabIndex = 25
        Me.LabelControl8.Text = "Monto del Abono:"
        '
        'teMontoAbonar
        '
        Me.teMontoAbonar.EditValue = "0"
        Me.teMontoAbonar.Enabled = False
        Me.teMontoAbonar.EnterMoveNextControl = True
        Me.teMontoAbonar.Location = New System.Drawing.Point(493, 12)
        Me.teMontoAbonar.Name = "teMontoAbonar"
        Me.teMontoAbonar.Properties.Appearance.Options.UseTextOptions = True
        Me.teMontoAbonar.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teMontoAbonar.Properties.Mask.EditMask = "n2"
        Me.teMontoAbonar.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teMontoAbonar.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teMontoAbonar.Size = New System.Drawing.Size(92, 20)
        Me.teMontoAbonar.TabIndex = 23
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.Location = New System.Drawing.Point(485, 106)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 19
        '
        'rgFormaPago
        '
        Me.rgFormaPago.EditValue = 0
        Me.rgFormaPago.Location = New System.Drawing.Point(597, 48)
        Me.rgFormaPago.Name = "rgFormaPago"
        Me.rgFormaPago.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Con cheque"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Transacción Bancaria")})
        Me.rgFormaPago.Size = New System.Drawing.Size(262, 22)
        Me.rgFormaPago.TabIndex = 18
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(598, 34)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(148, 13)
        Me.LabelControl4.TabIndex = 17
        Me.LabelControl4.Text = "Forma de Pago de éste abono:"
        '
        'leTipoTransaccion
        '
        Me.leTipoTransaccion.EnterMoveNextControl = True
        Me.leTipoTransaccion.Location = New System.Drawing.Point(597, 85)
        Me.leTipoTransaccion.Name = "leTipoTransaccion"
        Me.leTipoTransaccion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoTransaccion.Size = New System.Drawing.Size(262, 20)
        Me.leTipoTransaccion.TabIndex = 16
        Me.leTipoTransaccion.Visible = False
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(405, 109)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl5.TabIndex = 17
        Me.LabelControl5.Text = "Fecha del pago:"
        '
        'lblTipoTransaccion
        '
        Me.lblTipoTransaccion.Location = New System.Drawing.Point(599, 70)
        Me.lblTipoTransaccion.Name = "lblTipoTransaccion"
        Me.lblTipoTransaccion.Size = New System.Drawing.Size(99, 13)
        Me.lblTipoTransaccion.TabIndex = 17
        Me.lblTipoTransaccion.Text = "Tipo de Transacción:"
        Me.lblTipoTransaccion.Visible = False
        '
        'leTipoPartida
        '
        Me.leTipoPartida.EnterMoveNextControl = True
        Me.leTipoPartida.Location = New System.Drawing.Point(86, 106)
        Me.leTipoPartida.Name = "leTipoPartida"
        Me.leTipoPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPartida.Size = New System.Drawing.Size(238, 20)
        Me.leTipoPartida.TabIndex = 16
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(7, 109)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl7.TabIndex = 17
        Me.LabelControl7.Text = "Tipo de Partida:"
        '
        'btAplicar
        '
        Me.btAplicar.Location = New System.Drawing.Point(598, 105)
        Me.btAplicar.Name = "btAplicar"
        Me.btAplicar.Size = New System.Drawing.Size(98, 23)
        Me.btAplicar.TabIndex = 15
        Me.btAplicar.Text = "Aplicar al Banco..."
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(16, 39)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(67, 13)
        Me.LabelControl2.TabIndex = 13
        Me.LabelControl2.Text = "A Nombre De:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(33, 61)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl3.TabIndex = 14
        Me.LabelControl3.Text = "Concepto:"
        '
        'meConcepto
        '
        Me.meConcepto.Enabled = False
        Me.meConcepto.EnterMoveNextControl = True
        Me.meConcepto.Location = New System.Drawing.Point(86, 57)
        Me.meConcepto.Name = "meConcepto"
        Me.meConcepto.Size = New System.Drawing.Size(499, 48)
        Me.meConcepto.TabIndex = 12
        '
        'teANombreDe
        '
        Me.teANombreDe.Enabled = False
        Me.teANombreDe.EnterMoveNextControl = True
        Me.teANombreDe.Location = New System.Drawing.Point(86, 36)
        Me.teANombreDe.Name = "teANombreDe"
        Me.teANombreDe.Size = New System.Drawing.Size(499, 20)
        Me.teANombreDe.TabIndex = 11
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 145)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.leCtaBancaria})
        Me.gc.Size = New System.Drawing.Size(885, 277)
        Me.gc.TabIndex = 4
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcBanco, Me.gclMontoCheque, Me.GridColumn1})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcBanco
        '
        Me.gcBanco.Caption = "Banco/Cta. Bancaria"
        Me.gcBanco.ColumnEdit = Me.leCtaBancaria
        Me.gcBanco.FieldName = "IdCuentaBancaria"
        Me.gcBanco.Name = "gcBanco"
        Me.gcBanco.Visible = True
        Me.gcBanco.VisibleIndex = 0
        Me.gcBanco.Width = 246
        '
        'leCtaBancaria
        '
        Me.leCtaBancaria.AutoHeight = False
        Me.leCtaBancaria.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCtaBancaria.Name = "leCtaBancaria"
        '
        'gclMontoCheque
        '
        Me.gclMontoCheque.Caption = "Monto "
        Me.gclMontoCheque.DisplayFormat.FormatString = "{0:c}"
        Me.gclMontoCheque.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gclMontoCheque.FieldName = "Monto"
        Me.gclMontoCheque.Name = "gclMontoCheque"
        Me.gclMontoCheque.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Monto", "{0:c}")})
        Me.gclMontoCheque.Visible = True
        Me.gclMontoCheque.VisibleIndex = 1
        Me.gclMontoCheque.Width = 89
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdSucursal"
        Me.GridColumn1.FieldName = "IdSucursal"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'cpp_frmAutorizarAbonos2
        '
        Me.ClientSize = New System.Drawing.Size(893, 447)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.PanelControl1)
        Me.Modulo = "Cuentas por Pagar"
        Me.Name = "cpp_frmAutorizarAbonos2"
        Me.OptionId = "001006"
        Me.Text = "Emitir pagos a proveedores"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumeroComprobante.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaAbono.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teMontoAbonar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgFormaPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoTransaccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teANombreDe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCtaBancaria, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents teANombreDe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btAplicar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcBanco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gclMontoCheque As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leTipoPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgFormaPago As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents leTipoTransaccion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lblTipoTransaccion As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCtaBancaria As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumeroComprobante As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaAbono As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teMontoAbonar As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl

End Class
