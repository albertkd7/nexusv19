﻿Imports NexusBLL
Public Class cpp_frmAbonosAplicados
    Dim bl As New CuentasPPBLL(g_ConnectionString)

    Private Sub cpp_frmAbonosAplicados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = CDate(String.Format("01/{0}/{1}", Today.Month, Today.Year))
        deHasta.EditValue = Today
    End Sub

    Private Sub btObtener_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btObtener.Click
        gc.DataSource = bl.cpp_ConsultaAbonosProveedor(BeProveedor1.beCodigo.EditValue, deDesde.EditValue, deHasta.EditValue).Tables(0)
        gv.BestFitColumns()
    End Sub


    Private Sub cpp_frmAbonosAplicados_Reporte() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub

End Class
