﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpp_frmAnularAbonos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colNumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcConcepto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcTotalAbono = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTotalCompra = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkAbonar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.gcHeader = New DevExpress.XtraEditors.GroupControl()
        Me.btObtener = New DevExpress.XtraEditors.SimpleButton()
        Me.beProveedor = New Nexus.beProveedor()
        Me.btAnular = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAbonar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcHeader.SuspendLayout()
        Me.SuspendLayout()
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 82)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkAbonar})
        Me.gc.Size = New System.Drawing.Size(745, 278)
        Me.gc.TabIndex = 5
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colNumero, Me.gcFecha, Me.gcConcepto, Me.gcTotalAbono, Me.gcIdComprobante, Me.colTotalCompra})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'colNumero
        '
        Me.colNumero.Caption = "No. Comprobante"
        Me.colNumero.FieldName = "NumeroComprobante"
        Me.colNumero.Name = "colNumero"
        Me.colNumero.OptionsColumn.AllowEdit = False
        Me.colNumero.OptionsColumn.AllowFocus = False
        Me.colNumero.OptionsColumn.ReadOnly = True
        Me.colNumero.Visible = True
        Me.colNumero.VisibleIndex = 0
        Me.colNumero.Width = 159
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha"
        Me.gcFecha.FieldName = "Fecha"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.OptionsColumn.AllowEdit = False
        Me.gcFecha.OptionsColumn.AllowFocus = False
        Me.gcFecha.OptionsColumn.ReadOnly = True
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 1
        Me.gcFecha.Width = 88
        '
        'gcConcepto
        '
        Me.gcConcepto.Caption = "Concepto"
        Me.gcConcepto.FieldName = "Concepto"
        Me.gcConcepto.Name = "gcConcepto"
        Me.gcConcepto.OptionsColumn.AllowEdit = False
        Me.gcConcepto.OptionsColumn.AllowFocus = False
        Me.gcConcepto.OptionsColumn.ReadOnly = True
        Me.gcConcepto.Visible = True
        Me.gcConcepto.VisibleIndex = 2
        Me.gcConcepto.Width = 303
        '
        'gcTotalAbono
        '
        Me.gcTotalAbono.Caption = "Monto Abonado"
        Me.gcTotalAbono.DisplayFormat.FormatString = "{0:c2}"
        Me.gcTotalAbono.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcTotalAbono.FieldName = "TotalAbono"
        Me.gcTotalAbono.Name = "gcTotalAbono"
        Me.gcTotalAbono.OptionsColumn.ReadOnly = True
        Me.gcTotalAbono.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalAbono", "{0:n2}")})
        Me.gcTotalAbono.Visible = True
        Me.gcTotalAbono.VisibleIndex = 3
        Me.gcTotalAbono.Width = 105
        '
        'gcIdComprobante
        '
        Me.gcIdComprobante.Caption = "Correlativo"
        Me.gcIdComprobante.FieldName = "IdComprobante"
        Me.gcIdComprobante.Name = "gcIdComprobante"
        Me.gcIdComprobante.OptionsColumn.ReadOnly = True
        Me.gcIdComprobante.Visible = True
        Me.gcIdComprobante.VisibleIndex = 4
        Me.gcIdComprobante.Width = 72
        '
        'colTotalCompra
        '
        Me.colTotalCompra.Caption = "Total Compra"
        Me.colTotalCompra.DisplayFormat.FormatString = "n2"
        Me.colTotalCompra.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colTotalCompra.FieldName = "TotalCompra"
        Me.colTotalCompra.Name = "colTotalCompra"
        Me.colTotalCompra.OptionsColumn.AllowEdit = False
        Me.colTotalCompra.OptionsColumn.AllowFocus = False
        Me.colTotalCompra.OptionsColumn.ReadOnly = True
        Me.colTotalCompra.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCompra", "{0:n2}")})
        Me.colTotalCompra.Width = 107
        '
        'chkAbonar
        '
        Me.chkAbonar.AutoHeight = False
        Me.chkAbonar.Name = "chkAbonar"
        '
        'gcHeader
        '
        Me.gcHeader.CaptionLocation = DevExpress.Utils.Locations.Top
        Me.gcHeader.Controls.Add(Me.btObtener)
        Me.gcHeader.Controls.Add(Me.beProveedor)
        Me.gcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.gcHeader.Location = New System.Drawing.Point(0, 0)
        Me.gcHeader.Name = "gcHeader"
        Me.gcHeader.ShowCaption = False
        Me.gcHeader.Size = New System.Drawing.Size(900, 82)
        Me.gcHeader.TabIndex = 4
        Me.gcHeader.Text = "Root"
        '
        'btObtener
        '
        Me.btObtener.Location = New System.Drawing.Point(103, 47)
        Me.btObtener.Name = "btObtener"
        Me.btObtener.Size = New System.Drawing.Size(114, 24)
        Me.btObtener.TabIndex = 25
        Me.btObtener.Text = "Obtener Abonos"
        '
        'beProveedor
        '
        Me.beProveedor.Location = New System.Drawing.Point(41, 24)
        Me.beProveedor.Name = "beProveedor"
        Me.beProveedor.Size = New System.Drawing.Size(669, 20)
        Me.beProveedor.TabIndex = 2
        '
        'btAnular
        '
        Me.btAnular.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.btAnular.Appearance.Options.UseFont = True
        Me.btAnular.Location = New System.Drawing.Point(751, 124)
        Me.btAnular.Name = "btAnular"
        Me.btAnular.Size = New System.Drawing.Size(75, 24)
        Me.btAnular.TabIndex = 25
        Me.btAnular.Text = "Anular"
        '
        'cpp_frmAnularAbonos
        '
        Me.ClientSize = New System.Drawing.Size(900, 385)
        Me.Controls.Add(Me.btAnular)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.gcHeader)
        Me.Modulo = "Cuentas por Pagar"
        Me.Name = "cpp_frmAnularAbonos"
        Me.OptionId = "001004"
        Me.Text = "Anular Abonos de proveedores"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.gcHeader, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        Me.Controls.SetChildIndex(Me.btAnular, 0)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAbonar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcHeader.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents chkAbonar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents colNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTotalCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTotalAbono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents beProveedor As Nexus.beProveedor
    Friend WithEvents btObtener As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btAnular As DevExpress.XtraEditors.SimpleButton

End Class
