﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class acf_frmMantenimientos
    Dim entMantenimiento As acf_Mantenimientos
    Dim entActivo As acf_Activos
    Dim IdMantenimiento As Integer = 0
    Dim bl As New ActivosBusiness(g_ConnectionString)


    Private Sub acf_frmMantenimientos_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el Mantenimiento Activo Fijo?", MsgBoxStyle.YesNo + MsgBoxStyle.Information) = MsgBoxResult.Yes Then
            Dim msj As String = ""
            Try
                objTablas.acf_ActivosDeleteByPK(IdMantenimiento)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If msj = "" Then
                MsgBox("El Mantenimiento ha sido eliminado con éxito", MsgBoxStyle.Information)
                IdMantenimiento = teIdMantenimiento.EditValue - 1
                CargaControles(-1)
            Else
                MsgBox("No fue posible eliminar el Mantenimiento" + Chr(13) + msj, MsgBoxStyle.Critical, "Error al elminar el registro")
            End If
        End If
    End Sub

    Private Sub acf_frmMantenimientos_Edit_Click() Handles Me.Editar
        ActivarControles(True)
        teIdMantenimiento.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub

    Private Sub pre_frmLineas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objCombos.acf_Fallas(leFalla, "")
        objCombos.acf_Tecnicos(leTecnico, "")
        IdMantenimiento = objFunciones.ObtenerUltimoId("acf_Mantenimientos", "Id")
        CargaControles(0)
        ActivarControles(False)
    End Sub

    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl1.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teIdActivo.Properties.ReadOnly = True
    End Sub

    Public Sub CargaControles(ByVal TipoAvance As Integer)
        Dim IdMantenimientoStart As Integer = 0
        Dim IdMantenimientoEnd As Integer = 0
        If TipoAvance = -1 Then
            'IdMantenimientoStart = objFunciones.ObtenerPrimerId("acf_Mantenimientos", "Id")
            If IdMantenimiento = IdMantenimientoStart Then
                IdMantenimiento = IdMantenimiento + 1
            End If
        End If
        If TipoAvance = 1 Then
            IdMantenimientoEnd = objFunciones.ObtenerUltimoId("acf_Mantenimientos", "Id")
            If IdMantenimiento = IdMantenimientoEnd Then
                IdMantenimiento = IdMantenimiento - 1
            End If
        End If
        If IdMantenimiento = 0 Then
            Exit Sub
        End If
        IdMantenimiento = bl.ObtenerIdMantenimiento(IdMantenimiento, TipoAvance)
        If IdMantenimiento = 0 Then
            Exit Sub
        End If
        entMantenimiento = objTablas.acf_MantenimientosSelectByPK(IdMantenimiento)
        entActivo = objTablas.acf_ActivosSelectByPK(entMantenimiento.IdActivo)
        With entMantenimiento
            teIdMantenimiento.EditValue = .Id
            teIdActivo.EditValue = .IdActivo
            teNumActivo_Validated(teNumActivo, New EventArgs)
            teNombre.EditValue = entActivo.Nombre
            deFecha.EditValue = .Fecha
            seValor.EditValue = .Valor
            leFalla.EditValue = .IdFalla
            leTecnico.EditValue = .IdTecnico
            ceGarantia.EditValue = .IncluyeGarantia
            deFechaVence.EditValue = .FechaVenceGarantia
            deFechaProximoServicio.EditValue = .FechaProximoServicio
            meDetalleProblema.EditValue = .DetalleProblema
            meSolucionProblema.EditValue = .DetalleSolucion
        End With
    End Sub

    Private Sub acf_frmMantenimientos_New_Click() Handles Me.Nuevo
        ActivarControles(True)
        entMantenimiento = New acf_Mantenimientos
        teIdMantenimiento.EditValue = objFunciones.ObtenerUltimoId("acf_Mantenimientos", "Id") + 1
        teIdMantenimiento.Properties.ReadOnly = True
        teNumActivo.EditValue = ""
        teIdActivo.EditValue = 0
        teNombre.EditValue = ""
        deFecha.EditValue = Today
        seValor.EditValue = 0.0
        ceGarantia.EditValue = False
        deFechaVence.EditValue = Today
        deFechaProximoServicio.EditValue = Today
        meDetalleProblema.EditValue = ""
        meSolucionProblema.EditValue = ""
        teNombre.Focus()
    End Sub

    Function DatosValidos() As Boolean
        If teNombre.EditValue = "" Then
            MsgBox("Debe especificar el activo Fijo" + Chr(13) + "Imposible Continuar", MsgBoxStyle.Critical, "Error al Guardar")
            teNombre.Focus()
            Return False
        End If
        If meDetalleProblema.EditValue = "" Then
            MsgBox("Es necesario especificar el detalle del problema" + Chr(13) + "Imposible Continuar", MsgBoxStyle.Critical, "Error al Guardar")
            teNombre.Focus()
            Return False
        End If
        Return True
    End Function

    Private Sub acf_frmMantenimientos_Query_Click() Handles Me.Consulta
        entMantenimiento = objConsultas.cnsMantenimientos(frmConsultas, 0)
        teIdMantenimiento.EditValue = entMantenimiento.Id
        IdMantenimiento = entMantenimiento.Id
        CargaControles(0)
    End Sub

    Private Sub pre_frmLineas_Report_Click() Handles Me.Reporte
        Dim rpt As New acf_rptComprobanteMantenimiento
        rpt.DataSource = bl.rptComprobanteMantenimiento(teIdMantenimiento.EditValue)
        rpt.DataMember = ""
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub pre_frmLineas_Save_Click() Handles Me.Guardar
        If Not DatosValidos() Then
            Exit Sub
        End If
        CargaEntidades()
        Dim msj As String = ""
        If DbMode = DbModeType.insert Then
            objTablas.acf_MantenimientosInsert(entMantenimiento)
            MsgBox("El mantenimiento ha sido guardado con éxito", MsgBoxStyle.Information, "Nota")
        Else
            objTablas.acf_MantenimientosUpdate(entMantenimiento)
            MsgBox("El mantenimiento ha sido actualizada con éxito", MsgBoxStyle.Information, "Nota")
        End If
        MostrarModoInicial()
        ActivarControles(False)
    End Sub

    Private Sub CargaEntidades()
        With entMantenimiento
            .Id = teIdMantenimiento.EditValue
            .IdActivo = teIdActivo.EditValue
            .Fecha = deFecha.EditValue
            .Valor = seValor.EditValue
            .IdFalla = leFalla.EditValue
            .IdTecnico = leTecnico.EditValue
            .IncluyeGarantia = ceGarantia.EditValue
            .FechaVenceGarantia = deFechaVence.EditValue
            .FechaProximoServicio = deFechaProximoServicio.EditValue
            .DetalleProblema = meDetalleProblema.EditValue
            .DetalleSolucion = meSolucionProblema.EditValue
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
    End Sub

    Private Sub pre_frmLineas_Undo_Click() Handles Me.Revertir
        ActivarControles(False)
        CargaControles(0)
    End Sub

    Private Sub teIdActivo_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles teNumActivo.ButtonClick
        frmConsultas.Dispose()
        teIdActivo.EditValue = objConsultas.ConsultaActivoFijo(frmConsultas, piIdSucursalUsuario)
        Dim Activo As acf_Activos = objTablas.acf_ActivosSelectByPK(teIdActivo.EditValue)

        If Activo.IdSucursal <> piIdSucursalUsuario And piIdSucursalUsuario <> 1 Then
            MsgBox("El activo no corresponde a su sucursal", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        teNumActivo.EditValue = Activo.Codigo
        teNumActivo_Validated(e, New EventArgs)
    End Sub

    Private Sub teNumActivo_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles teNumActivo.Validated, teIdActivo.Validated
        If SiEsNulo(teNumActivo.EditValue, "") = "" Then
            'Exit Sub
        Else
            If teIdActivo.EditValue = 0 Then
                Dim dt As DataTable = bl.DatosGeneralesActivoFijo(teNumActivo.EditValue)
                If dt.Rows.Count = 0 Then
                    MsgBox("No existe el código del activo ingresado", MsgBoxStyle.Critical, "Nota")
                    Exit Sub
                End If
                teIdActivo.EditValue = dt.Rows(0).Item("IdActivo")
            End If
        End If
        Dim ActivoFijo As acf_Activos = objTablas.acf_ActivosSelectByPK(teIdActivo.EditValue)
        teNumActivo.EditValue = ActivoFijo.Codigo
        teNombre.EditValue = ActivoFijo.Nombre
    End Sub

End Class
