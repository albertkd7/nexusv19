﻿Imports NexusELL.TableEntities
Public Class acf_frmTecnicos2


    Private Sub acf_frmTecnicos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.acf_TecnicosSelectAll
    End Sub
    Private Sub acf_frmTecnicos_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Return
        End If

        Dim Id As Integer = gv.GetFocusedRowCellValue(gv.Columns(0))
        objTablas.acf_TecnicosDeleteByPK(Id)
        gc.DataSource = objTablas.acf_TecnicosSelectAll
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        Dim entidad As New acf_Tecnicos
        Dim numFila As Integer

        If e.RowHandle < 0 Then
            DbMode = DbModeType.insert
            numFila = gv.RowCount - 1
        Else
            DbMode = DbModeType.update
            numFila = e.RowHandle
        End If

        If Not AllowInsert Or Not AllowEdit Then
            MsgBox("No le está permitido ingresar o editar información", MsgBoxStyle.Exclamation, Me.Text)
            gc.DataSource = objTablas.acf_TecnicosSelectAll
            Exit Sub
        End If

        With entidad
            .IdTecnico = gv.GetRowCellValue(numFila, gv.Columns(0).FieldName)
            .Nombre = gv.GetRowCellValue(numFila, gv.Columns(1).FieldName)
            .Empresa = gv.GetRowCellValue(numFila, gv.Columns(2).FieldName)
            .Telefono = SiEsNulo(gv.GetRowCellValue(numFila, gv.Columns(3).FieldName), "")
            .Email = SiEsNulo(gv.GetRowCellValue(numFila, gv.Columns(4).FieldName), "")
        End With
        If DbMode = DbModeType.insert Then
            objTablas.acf_TecnicosInsert(entidad)
        Else
            objTablas.acf_TecnicosUpdate(entidad)
        End If
    End Sub

    Private Sub acf_frmTecnicos_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub

End Class
