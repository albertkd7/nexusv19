﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class acf_frmDepreciacionIndividual
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcColumna1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcColumna2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcColumna3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcColumna4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.XtraTabControl1)
        Me.GroupControl1.Size = New System.Drawing.Size(1123, 316)
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.leSucursal)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl32)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl2)
        Me.XtraTabPage1.Controls.Add(Me.RadioGroup1)
        Me.XtraTabPage1.Controls.Add(Me.gc)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1113, 266)
        Me.XtraTabPage1.Text = "Listado de Activos(click para seleccionar)"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(901, 28)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(198, 20)
        Me.leSucursal.TabIndex = 54
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(900, 12)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl32.TabIndex = 55
        Me.LabelControl32.Text = "Sucursal:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl2.Location = New System.Drawing.Point(901, 51)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(103, 13)
        Me.LabelControl2.TabIndex = 13
        Me.LabelControl2.Text = "Tipo Depreciación:"
        '
        'RadioGroup1
        '
        Me.RadioGroup1.Location = New System.Drawing.Point(899, 68)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Mensual"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Anual")})
        Me.RadioGroup1.Size = New System.Drawing.Size(147, 48)
        Me.RadioGroup1.TabIndex = 1
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.leSucursalDetalle})
        Me.gc.Size = New System.Drawing.Size(893, 266)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv, Me.GridView1})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcColumna1, Me.gcColumna2, Me.GridColumn3, Me.gcColumna3, Me.gcColumna4, Me.GridColumn1, Me.GridColumn2})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowAutoFilterRow = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcColumna1
        '
        Me.gcColumna1.Caption = "Clase Activo"
        Me.gcColumna1.FieldName = "ClaseActivo"
        Me.gcColumna1.Name = "gcColumna1"
        Me.gcColumna1.Visible = True
        Me.gcColumna1.VisibleIndex = 0
        Me.gcColumna1.Width = 153
        '
        'gcColumna2
        '
        Me.gcColumna2.Caption = "Codigo Activo"
        Me.gcColumna2.FieldName = "Codigo"
        Me.gcColumna2.Name = "gcColumna2"
        Me.gcColumna2.Visible = True
        Me.gcColumna2.VisibleIndex = 1
        Me.gcColumna2.Width = 90
        '
        'gcColumna3
        '
        Me.gcColumna3.Caption = "Nombre Activo"
        Me.gcColumna3.FieldName = "NombreActivo"
        Me.gcColumna3.Name = "gcColumna3"
        Me.gcColumna3.Visible = True
        Me.gcColumna3.VisibleIndex = 3
        Me.gcColumna3.Width = 272
        '
        'gcColumna4
        '
        Me.gcColumna4.Caption = "Fecha Adquisicion"
        Me.gcColumna4.FieldName = "FechaAdquisicion"
        Me.gcColumna4.Name = "gcColumna4"
        Me.gcColumna4.Visible = True
        Me.gcColumna4.VisibleIndex = 4
        Me.gcColumna4.Width = 104
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Valor Adquisición"
        Me.GridColumn1.FieldName = "ValorAdquisicion"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 5
        Me.GridColumn1.Width = 123
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Id Activo"
        Me.GridColumn2.FieldName = "IdActivo"
        Me.GridColumn2.Name = "GridColumn2"
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.gc
        Me.GridView1.Name = "GridView1"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(2, 22)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(1119, 292)
        Me.XtraTabControl1.TabIndex = 3
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Sucursal"
        Me.GridColumn3.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn3.FieldName = "IdSucursal"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 133
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'acf_frmDepreciacionIndividual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(1123, 341)
        Me.Modulo = "Activo Fijo"
        Me.Name = "acf_frmDepreciacionIndividual"
        Me.Text = "Depreciacion Individual(Kardex)"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.XtraTabPage1.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcColumna1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcColumna2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcColumna3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcColumna4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit

End Class
