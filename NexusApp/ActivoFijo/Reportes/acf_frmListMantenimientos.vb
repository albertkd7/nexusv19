﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class acf_frmListMantenimientos
    Dim myBL As New ActivosBusiness(g_ConnectionString)
    Private Sub acf_frmListMantenimientos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub acf_frmListMantenimientos_Report_Click() Handles Me.Reporte
        Dim IdActivo As Integer = -1
        If beActivo.EditValue <> Nothing Then
            If beActivo.EditValue <> 0 Then
                IdActivo = beActivo.EditValue
            End If
        End If
        Dim dt As DataTable = myBL.ListMantenimientos(IdActivo, deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        Dim rpt As New acf_rptListMantenimientos() With {.DataSource = dt, .DataMember = ""}
        If teActivo.EditValue = "" Then
            rpt.xrlResponsable.Text = "--TODOS LOS ACTIVOS"
        Else
            rpt.xrlResponsable.Text = teActivo.EditValue
        End If
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Listado de Mantemientos " + FechaToString(deDesde.EditValue, deHasta.EditValue) + " | SUCURSAL: " & leSucursal.Text
        rpt.ShowPreview()
    End Sub

    Private Sub beIdActivo_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beActivo.Validated
        If beActivo.EditValue = Nothing Then
            teActivo.EditValue = ""
        Else
            Dim entActivo As New acf_Activos
            entActivo = objTablas.acf_ActivosSelectByPK(beActivo.EditValue)
            With entActivo
                beActivo.EditValue = .IdActivo
                teActivo.EditValue = .Nombre
            End With
        End If
    End Sub

    Private Sub beIdEmpleado_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beActivo.ButtonClick
        beActivo.EditValue = Nothing
        Dim entActivo As New acf_Activos
        entActivo = objConsultas.cnsActivos(frmConsultas, entActivo.IdActivo)
        With entActivo
            beActivo.EditValue = .IdActivo
            teActivo.EditValue = .Nombre
        End With
        beIdActivo_Validated(beActivo, New EventArgs)
    End Sub

End Class
