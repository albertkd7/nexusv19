﻿Imports NexusBLL


Public Class acf_frmListActivosRetirados
    Dim myBL As New ActivosBusiness(g_ConnectionString)
    Private Sub acf_frmListActivosRetirados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.acf_Clases(leClase, "--TODAS LAS CLASIFICACIONES--")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        deDesde.EditValue = Today
        deHasta.EditValue = Today
    End Sub

    Private Sub acf_frmListActivosRetirados_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = myBL.ListActivosRetirados(leClase.EditValue, deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        Dim rpt As New acf_rptListActivosRetirados() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Listado de Activos Retirados del " + FechaToString(deDesde.EditValue, deHasta.EditValue) + " | SUCURSAL: " & leSucursal.Text
        rpt.ShowPreview()
    End Sub
End Class
