﻿Imports NexusBLL
Public Class acf_frmDepreciacionIndividual
    Dim bl As New NexusBLL.ActivosBusiness(g_ConnectionString)
    Dim blTablas As New TableBusiness(g_ConnectionString)
    Dim funciones As New FuncionesBLL(g_ConnectionString)
    Dim cn As New NexusBLL.AdmonBLL(g_ConnectionString)



    Private Sub acf_frmDepreciacionIndividual_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        RadioGroup1.EditValue = 1
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        gc.DataSource = bl.TodosActivos(leSucursal.EditValue)
    End Sub

    Private Sub acf_frmDepreciacionIndividual_Reporte() Handles Me.Reporte
        Dim IdActivo As Integer
        Dim rpt As New acf_rptListDepreciacionIndividual
        IdActivo = gv.GetRowCellValue(gv.FocusedRowHandle, "IdActivo")
        rpt.DataSource = bl.ListDepreciacionIndividual(IdActivo, RadioGroup1.EditValue)
        rpt.DataMember = ""
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        gc.DataSource = bl.TodosActivos(leSucursal.EditValue)
    End Sub
End Class
