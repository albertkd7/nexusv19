﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class acf_frmListActivosCuenta
    Dim myBL As New ActivosBusiness(g_ConnectionString)
    Private Sub acf_frmListCuentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.acf_CuentaGasto(leCuenta, "--TODAS LAS CUENTAS--")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        deDesde.EditValue = Today
        deHasta.EditValue = Today
    End Sub

    Private Sub acf_frmListCuentas_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = myBL.ListActivosCuentas(leCuenta.EditValue, deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        Dim rpt As New acf_rptListActivosCuentas() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Listado de Activos por Cuenta de Gasto " + FechaToString(deDesde.EditValue, deHasta.EditValue) & " | SUCURSAL: " + leSucursal.Text
        rpt.ShowPreview()
    End Sub

    Private Sub GroupControl1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles GroupControl1.Paint

    End Sub
End Class
