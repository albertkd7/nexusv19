﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class acf_frmListCuadroDiferencias
    Dim myBL As New ActivosBusiness(g_ConnectionString)
    Private Sub acf_frmListCuadroDiferencias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SpinEdit1.Value = Today.Year
        MonthEdit1.Month = Today.Month
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub acf_frmListCuadroDiferencias_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = myBL.ListCuadroDiferencias(SpinEdit1.EditValue, MonthEdit1.EditValue, leSucursal.EditValue)
        Dim rpt As New acf_rptListCuadroDiferencias() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Cuadro de Diferencias a " + MonthEdit1.Text + " " + SpinEdit1.Text + "| SUCURSAL: " & leSucursal.Text
        rpt.ShowPreview()
    End Sub


End Class
