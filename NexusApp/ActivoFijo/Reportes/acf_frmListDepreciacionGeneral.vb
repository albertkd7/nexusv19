﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class acf_frmListDepreciacionGeneral
    Dim myBL As New ActivosBusiness(g_ConnectionString)
    Private Sub acf_frmListActivosGeneral_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.acf_Clases(leClase, "-- TODAS LAS CLASIFICACIONES --")
        objCombos.acf_Ubicacion(leUbicacion, " -- TODAS LAS UBICACIONES --")
        objCombos.acf_CuentaGasto(leCuenta, " -- TODAS LAS CUENTAS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        objCombos.acf_ListGrupoGeneral(leAgrupado)
        SpinEdit1.Value = Today.Year
        MonthEdit1.Month = Today.Month
    End Sub

    Private Sub acf_frmListActivosGeneral_Report_Click() Handles Me.Reporte
        Dim dFechaFinal As DateTime
        dFechaFinal = CDate(CStr(SpinEdit1.EditValue) & "/" & (MonthEdit1.EditValue).ToString.PadLeft(2, "0") & "/01")
        dFechaFinal = DateAdd(DateInterval.Month, 1, dFechaFinal)
        dFechaFinal = DateAdd(DateInterval.Day, -1, dFechaFinal)
        Dim dt As DataTable = myBL.ListActivosGeneral(leClase.EditValue, leUbicacion.EditValue, leCuenta.EditValue, leAgrupado.EditValue, ceDepreciable.EditValue, ceDepreciado.EditValue, dFechaFinal, leSucursal.EditValue)
        If leAgrupado.EditValue = 1 Then
            Dim rpt As New acf_rptListDepreciacionGeneralClase() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = "Cuadro de Depreciaciones de Activo Fijo a " + MonthEdit1.Text + " " + SpinEdit1.Text
            rpt.cTitle1.Text = leUbicacion.Text
            rpt.cTitle2.Text = leCuenta.Text
            rpt.xrlSucursal.Text = leSucursal.Text
            rpt.ShowPreview()
        Else
            Dim rpt As New acf_rptListDepreciacionGeneralUbicacion() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = "Cuadro de Depreciaciones de Activo Fijo a " + MonthEdit1.Text + " " + SpinEdit1.Text
            rpt.cTitle1.Text = leClase.Text
            rpt.cTitle2.Text = leCuenta.Text
            rpt.xrlSucursal.Text = leSucursal.Text
            rpt.ShowPreview()
        End If

    End Sub

End Class
