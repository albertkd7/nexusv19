﻿Imports NexusBLL
Public Class acf_frmListActivos
    Dim myBL As New ActivosBusiness(g_ConnectionString)
    Private Sub acf_frmListActivos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.acf_Clases(leClase, "-- TODAS LAS CLASIFICACIONES --")
        objCombos.acf_Ubicacion(leUbicacion, "--TODAS LAS UBICACIONES--")
        objCombos.acf_TiposDepreciacion(leTipoDep, "-- TODOS LOS TIPOS DE DEPRECIACIÓN --")
        objCombos.acf_Estados(leEstado, "-- TODOS LOS ESTADOS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        deDesde.EditValue = Today
        deHasta.EditValue = Today
    End Sub
    
    Private Sub acf_frmListActivos_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = myBL.ListActivos(leClase.EditValue, leUbicacion.EditValue, leTipoDep.EditValue, leEstado.EditValue, deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        Dim rpt As New acf_rptListActivos() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Listado de Activos del " + FechaToString(deDesde.EditValue, deHasta.EditValue) & " | SUCURSAL: " + leSucursal.Text
        rpt.xrlTitulo2.Text = "Estado del los Activos: " + leEstado.Text
        rpt.ShowPreview()
    End Sub
End Class
