﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class acf_frmOtrosReportes
    Dim myBL As New ActivosBusiness(g_ConnectionString)
    Private Sub acf_frmListMarcas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.acf_Ubicacion(leUbicacion, "-- TODAS LAS UBICACIONES --")
        objCombos.acf_Marcas(leMarca, "-- TODAS LAS MARCAS --")
        objCombos.acf_Estados(leEstado, "-- TODOS LOS ESTADOS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        deDesde.EditValue = Today
        deHasta.EditValue = Today
    End Sub

    Private Sub acf_frmListActivos_Report_Click() Handles Me.Reporte
        Select Case rgTipo.EditValue
            Case 0
                Dim dt As DataTable = myBL.ListActivosUbicaciones(leUbicacion.EditValue, -1, deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
                Dim rpt As New acf_rptListActivosUbicacion() With {.DataSource = dt, .DataMember = ""}

                rpt.xrlResponsable.Text = "--TODOS LOS RESPONSABLES --"
                rpt.xrlEmpresa.Text = gsNombre_Empresa
                rpt.xrlTitulo.Text = "Listado de Activos por Ubicación " + FechaToString(deDesde.EditValue, deHasta.EditValue) + " | SUCURSAL: " + leSucursal.Text
                rpt.ShowPreview()
            Case 1
                Dim dt As DataTable = myBL.ListActivosMarca(leMarca.EditValue, deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
                Dim rpt As New acf_rptListActivosMarca() With {.DataSource = dt, .DataMember = ""}
                rpt.xrlEmpresa.Text = gsNombre_Empresa
                rpt.xrlTitulo.Text = "Listado de Activos por Marca " + FechaToString(deDesde.EditValue, deHasta.EditValue) + " | SUCURSAL: " + leSucursal.Text
                rpt.ShowPreview()
            Case 2
                Dim dt As DataTable = myBL.ListActivosEstado(leEstado.EditValue, deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
                Dim rpt As New acf_rptListActivosEstado() With {.DataSource = dt, .DataMember = ""}
                rpt.xrlEmpresa.Text = gsNombre_Empresa
                rpt.xrlTitulo.Text = "Listado de Activos por Estado " + FechaToString(deDesde.EditValue, deHasta.EditValue) + " | SUCURSAL: " + leSucursal.Text
                rpt.ShowPreview()
        End Select
        
    End Sub

End Class
