﻿Imports NexusELL.TableEntities
Public Class acf_frmTiposDepreciacion

    Private Sub acf_frmTiposDepreciacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.acf_TiposDepreciacionSelectAll
    End Sub
    
    Private Sub acf_frmTiposDepreciacion_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Return
        End If
        Dim Id As String = gv.GetFocusedRowCellValue(gv.Columns(0))
        Dim msj As String = ""
        Try
            objTablas.acf_TiposDepreciacionDeleteByPK(Id)
        Catch ex As Exception
            msj = ex.Message()
        End Try
        If msj = "" Then
            MsgBox("El Tipo de Depreciación ha sido eliminada con éxito", MsgBoxStyle.Information)
        Else
            MsgBox("No fue posible eliminar el Tipo de Depreciación" + Chr(13) + msj, MsgBoxStyle.Critical, "Error al elminar el registro")
        End If
        gc.DataSource = objTablas.acf_TiposDepreciacionSelectAll
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        Dim entidad As New acf_TiposDepreciacion
        Dim numFila As Integer
        Dim msj As String = ""

        If e.RowHandle < 0 Then
            DbMode = DbModeType.insert
            numFila = gv.RowCount - 1
        Else
            DbMode = DbModeType.update
            numFila = e.RowHandle
        End If

        If Not AllowInsert Or Not AllowEdit Then
            MsgBox("No le está permitido ingresar o editar información", MsgBoxStyle.Exclamation, Me.Text)
            gc.DataSource = objTablas.acf_TiposDepreciacionSelectAll
            Exit Sub
        End If

        With entidad
            .IdTipo = gv.GetRowCellValue(numFila, gv.Columns(0).FieldName)
            .Nombre = gv.GetRowCellValue(numFila, gv.Columns(1).FieldName)
        End With

        If DbMode = DbModeType.insert Then
            Try
                objTablas.acf_TiposDepreciacionInsert(entidad)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If Not msj = "" Then
                MsgBox(String.Format("No fue posible Guardar  El tipo de Depreciación{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error al Guardar el registro")
            End If
        Else
            Try
                objTablas.acf_TiposDepreciacionUpdate(entidad)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If Not msj = "" Then
                MsgBox(String.Format("No fue posible actualizar el tipo de Depreciación{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error al Guardar el registro")
            End If
        End If
        gc.DataSource = objTablas.acf_TiposDepreciacionSelectAll
    End Sub

    Private Sub acf_frmTiposDepreciacion_Reporte() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
End Class
