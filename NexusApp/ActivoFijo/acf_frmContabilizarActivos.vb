﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class acf_frmContabilizarActivos
    Dim iId = 0
    Dim myBL As New ActivosBusiness(g_ConnectionString)
    Dim blContable As New ContabilidadBLL(g_ConnectionString)
    Dim blTabla As New TableBusiness(g_ConnectionString)
    Dim entCuentas As New con_Cuentas

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        Dim dFechaFinal As DateTime
        dFechaFinal = CDate(CStr(SpinEdit1.EditValue) & "/" & (MonthEdit1.EditValue).ToString.PadLeft(2, "0") & "/01")
        dFechaFinal = DateAdd(DateInterval.Month, 1, dFechaFinal)
        dFechaFinal = DateAdd(DateInterval.Day, -1, dFechaFinal)

        Dim EsOk As Boolean = ValidarFechaCierre(dFechaFinal)
        If Not EsOk Then
            MsgBox("La fecha de la partida debe ser de un período válido", 16, "Error de datos")
            Return
        End If

        Dim IdPartida As Integer = myBL.ExistePartidaActivo(dFechaFinal, leSucursal.EditValue)
        If IdPartida > 0 Then
            MsgBox("El periodo ya fue Contabilizado", 16, "Error de datos")
            Return
        End If

        If MsgBox("¿Está seguro(a) de realizar la Contabilización del Periodo?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Return
        End If
        If myBL.ContabilizarActivoFijo(SpinEdit1.Value, MonthEdit1.Month, leTipoPartida.EditValue, meConcepto.EditValue, objMenu.User, leSucursal.EditValue) = "Ok" Then
            MsgBox("Cierre realizado con éxito", MsgBoxStyle.Information, "Nota")
        Else
            MsgBox("No fue posible realizar el cierre", MsgBoxStyle.Information, "Nota")
        End If

        Me.Close()
    End Sub

    Private Sub frmCierreMes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        CambiaDatos()
    End Sub

    Private Sub CambiaDatos()
        Dim dt As New DataTable
        Dim MesCerrado As Integer
        Dim PeriodoCerrado As Integer
        dt = myBL.ObtienePeriodoCerrado(leSucursal.EditValue)
        If dt.Rows.Count > 0 Then
            MesCerrado = SiEsNulo(dt.Rows(0).Item(0), Now.Month)
            PeriodoCerrado = SiEsNulo(dt.Rows(0).Item(1), Now.Year)
            If MesCerrado = 12 Then
                SpinEdit1.Value = PeriodoCerrado + 1
                MonthEdit1.Month = 1
            Else
                SpinEdit1.Value = PeriodoCerrado
                MonthEdit1.Month = MesCerrado + 1
            End If
        Else
            MesCerrado = Today.Month
            PeriodoCerrado = Today.Year
            SpinEdit1.Value = PeriodoCerrado
            SpinEdit1.Enabled = True
            MonthEdit1.Month = MesCerrado
            MonthEdit1.Enabled = True
            lbMsg1.Visible = True
            lbMsg2.Visible = True
        End If
        If PeriodoCerrado = 12 Then
            MonthEdit1.Month = 1
            SpinEdit1.Value = PeriodoCerrado + 1
        End If
        meConcepto.EditValue = "DEPRECIACION DE ACTIVO FIJO DEL MES DE " & MonthEdit1.Text.ToString.ToUpper & " DE " & SpinEdit1.EditValue.ToString & "SUCURSAL: " & leSucursal.Text
        objCombos.conTiposPartida(leTipoPartida)

    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        CambiaDatos()
    End Sub
End Class