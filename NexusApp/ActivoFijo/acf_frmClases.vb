﻿Imports NexusELL.TableEntities
Public Class acf_frmClases
    Dim entClase As acf_Clases

    Private Sub acf_frmClases_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub acfClases_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' FilterControl1.SourceControl = objTablas.acf_ClasesSelectAll
        gc.DataSource = objTablas.acf_ClasesSelectAll
        entClase = objTablas.acf_ClasesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdClase"))
        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub acfClases_Nuevo_Click() Handles Me.Nuevo
        entClase = New acf_Clases
        entClase.IdClase = objFunciones.ObtenerUltimoId("ACF_CLASES", "IdClase") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub acfClases_Save_Click() Handles Me.Guardar
        Dim msj As String = ""
        If teNombre.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Nombre]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        CargaEntidad()
        If DbMode = DbModeType.insert Then
            Try
                objTablas.acf_ClasesInsert(entClase)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If msj = "" Then
                MsgBox("La Clasificación ha sido guardado con éxito", MsgBoxStyle.Information)
                CargaPantalla()
            Else
                MsgBox(String.Format("No fue posible Guardar la Clasificación{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error al Guardar el registro")
            End If
        Else
            Try
                objTablas.acf_ClasesUpdate(entClase)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If msj = "" Then
                MsgBox("La Clasificación ha sido actualizado con éxito", MsgBoxStyle.Information)
                CargaPantalla()
            Else
                MsgBox(String.Format("No fue posible actualizar la Clasificación{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error al Guardar el registro")
            End If
        End If
        gc.DataSource = objTablas.acf_ClasesSelectAll
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub acf_frmClases_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar la Clasificación seleccionada?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Dim msj As String = ""
            Try
                objTablas.acf_ClasesDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If msj = "" Then
                MsgBox("La Clasificación ha sido eliminada con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("No fue posible eliminar la Clasificación de Activo Fijo" + Chr(13) + msj, MsgBoxStyle.Critical, "Error al elminar el registro")
            End If
            gc.DataSource = objTablas.acf_ClasesSelectAll
        End If
    End Sub
    Private Sub acf_frmClases_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entClase
            teIdClase.EditValue = .IdClase
            teNombre.EditValue = .Nombre
            ceDepreciable.EditValue = .Depreciable
            beCta01.beIdCuenta.EditValue = .IdCtaActivo
            If .IdCtaActivo = "" Then
                beCta01.teNombreCuenta.EditValue = ""
            End If
            beCta01.ValidateChildren()
            beCta02.beIdCuenta.EditValue = .IdCtaDepreciacion
            If beCta02.beIdCuenta.EditValue = "" Then
                beCta02.teNombreCuenta.EditValue = ""
            End If
            beCta02.ValidateChildren()
            beCta03.beIdCuenta.EditValue = .IdCtaGasto
            If beCta03.beIdCuenta.EditValue = "" Then
                beCta03.teNombreCuenta.EditValue = ""
            End If
            beCta03.ValidateChildren()
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entClase
            .IdClase = teIdClase.EditValue
            .Nombre = teNombre.EditValue
            .Depreciable = ceDepreciable.EditValue
            .IdCtaActivo = beCta01.beIdCuenta.EditValue
            .IdCtaDepreciacion = beCta02.beIdCuenta.EditValue
            .IdCtaGasto = beCta03.beIdCuenta.EditValue
        End With
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entClase = objTablas.acf_ClasesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdClase"))
        CargaPantalla()
    End Sub

    Private Sub acf_Clases_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        beCta01.beIdCuenta.Properties.ReadOnly = Not Tipo
        beCta02.beIdCuenta.Properties.ReadOnly = Not Tipo
        beCta03.beIdCuenta.Properties.ReadOnly = Not Tipo
        teIdClase.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub
End Class
