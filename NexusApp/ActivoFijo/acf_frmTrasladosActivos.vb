﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns
Imports System.IO

Public Class acf_frmTrasladosActivos

    Dim bl As New ActivosBusiness(g_ConnectionString)
    Dim Header As New acf_Traslados
    Dim Detalle As List(Of acf_TrasladosDetalle)

    Dim entUbicaciones As New acf_Ubicaciones
    Dim entActivos As New acf_Activos
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim IdSucUser As Integer = SiEsNulo(EntUsuario.IdSucursal, 1)


    Private Sub inv_frmEntradas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cargarcombos()
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("ACF_TRASLADOS", "IdComprobante")
        CargaControles(0)
        ActivarControles(False)
    End Sub
    Private Sub inv_frmEntradas_Nuevo() Handles MyBase.Nuevo
        Header = New acf_Traslados
        ActivarControles(True)

        gc.DataSource = bl.ObtenerDetalleDocumento(-1, "acf_TrasladosDetalle")
        gv.CancelUpdateCurrentRow()
        gv.AddNewRow()

        teNumero.EditValue = ""
        deFecha.EditValue = Today
        meConcepto.EditValue = ""
        leUbicacionAct.EditValue = 1
        leUbicacionAct.EditValue = 1

        teNumero.Focus()
    End Sub
    Private Sub inv_frmEntradas_Guardar() Handles MyBase.Guardar

        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            MsgBox("La fecha del documento debe ser de un período válido", 16, "Error de datos")
            Exit Sub
        End If

        CargarEntidades()
        Dim msj = ""

        If DbMode = DbModeType.insert Then
            msj = bl.GuardarTaslado(Header, Detalle, True)

            If msj = "" Then
                MsgBox("El comprobante ha sido registrado con éxito", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox("SE DETECTÓ UN ERROR AL CREAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If
        Else
            msj = bl.GuardarTaslado(Header, Detalle, False)
            If msj = "" Then
                MsgBox("El documento ha sido actualizado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("SE DETECTÓ UN ERROR AL ACTUALIZAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If
        teCorrelativo.EditValue = Header.IdComprobante
        MostrarModoInicial()
        teCorrelativo.Focus()
        ActivarControles(False)
    End Sub
    Private Sub CargarCombos()
        objCombos.pla_Empleados(leResponsableAnt, "")
        objCombos.pla_Empleados(leResponsableAct, "")

        objCombos.acf_Ubicacion(leUbicacionAnt, "")
        objCombos.acf_Ubicacion(leUbicacionAct, "")
    End Sub
    Private Sub inv_frmEntradas_Editar() Handles MyBase.Editar
        ActivarControles(True)
        teNumero.Focus()
    End Sub
    Private Sub inv_frmEntradas_Consulta() Handles MyBase.Consulta
        teCorrelativo.EditValue = objConsultas.ConsultaTrasladosActivoFijo(frmConsultas)
        ' objConsultas.cnsActivos(frmConsultas, 0)
        CargaControles(0)
    End Sub
    Private Sub inv_frmEntradas_Revertir() Handles MyBase.Revertir
        ActivarControles(False)
    End Sub
    Private Sub inv_frmEntradas_Eliminar() Handles MyBase.Eliminar
        If MsgBox("¿Está seguro(a) de eliminar la transacción?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            objTablas.acf_TrasladosDeleteByPK(Header.IdComprobante)
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("ACF_TRASLADOS", "IdComprobante")
            CargaControles(0)
            ActivarControles(False)
        End If
    End Sub

#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdResponsableAnt", 0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdResponsableAct", 0)
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdActivo"), 0) = 0 Then
            e.Valid = False
        End If
    End Sub
    'Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
    '    Dim Cantidad As Integer = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
    '    Dim PrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")

    '    Select Case gv.FocusedColumn.FieldName
    '        Case "Cantidad"
    '            Cantidad = e.Value

    '        Case "PrecioUnitario"
    '            PrecioUnitario = e.Value
    '    End Select

    '    Dim Total As Decimal = Decimal.Round(Cantidad * PrecioUnitario, 2)
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", Total)
    'End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            If gv.FocusedColumn.FieldName = "IdActivo" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProd = objConsultas.ConsultaActivoFijo(frmConsultas, piIdSucursalUsuario)
                    gv.EditingValue = IdProd
                End If
                entActivos = objTablas.acf_ActivosSelectByPK(gv.EditingValue)

                If entActivos.IdActivo = 0 Then
                    MsgBox("Código de activo no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Descripcion", "-- ACTIVO NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("Descripcion", entActivos.Nombre)
                    gv.SetFocusedRowCellValue("IdResponsableAnt", entActivos.IdEmpleado)
                End If
            End If
        End If

    End Sub
#End Region

    Public Sub CargaControles(ByVal TipoAvance As Integer)
        If TipoAvance = 0 Then 'no es necesario obtener el Id del comprobante
        Else
            teCorrelativo.EditValue = bl.ObtenerIdDocumento(teCorrelativo.EditValue, TipoAvance, "acf_Traslados")
        End If
        If teCorrelativo.EditValue = 0 Then
            Exit Sub
        End If
        Header = objTablas.acf_TrasladosSelectByPK(teCorrelativo.EditValue)
        If Header Is Nothing Then
            Exit Sub
        End If
        With Header
            teCorrelativo.EditValue = .IdComprobante
            teNumero.EditValue = .NumeroComprobante
            leUbicacionAnt.EditValue = .IdUbicacionAnt
            leUbicacionAct.EditValue = .IdUbicacionAct
            deFecha.EditValue = .Fecha
            meConcepto.EditValue = .Concepto

            gc.DataSource = bl.ObtenerDetalleDocumento(.IdComprobante, "acf_TrasladosDetalle")
        End With
    End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
    End Sub
    Private Sub CargarEntidades()
        With Header
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
            Else
                .ModificadoPor = objMenu.User
                .FechaHoraModificacion = Now
            End If

            .IdComprobante = teCorrelativo.EditValue  'el id se asignará en la capa de datos
            .NumeroComprobante = teNumero.EditValue
            .Fecha = deFecha.EditValue
            .Concepto = meConcepto.EditValue
            .IdUbicacionAnt = leUbicacionAnt.EditValue
            .IdUbicacionAct = leUbicacionAct.EditValue
        End With

        Detalle = New List(Of acf_TrasladosDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New acf_TrasladosDetalle
            With entDetalle
                .IdComprobante = Header.IdComprobante
                .IdDetalle = i + 1
                .IdActivo = gv.GetRowCellValue(i, "IdActivo")
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .IdResponsableAnt = gv.GetRowCellValue(i, "IdResponsableAnt")
                .IdResponsableAct = gv.GetRowCellValue(i, "IdResponsableAct")
            End With
            Detalle.Add(entDetalle)
        Next

    End Sub

    Private Sub inv_frmEntradas_Reporte() Handles Me.Reporte
        Dim dt As DataTable = bl.ImpresionTraslado(teCorrelativo.EditValue)
        Dim rpt As New acf_rptTraslados() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlCorrel.Text = teCorrelativo.EditValue
        rpt.xrlNumero.Text = teNumero.EditValue
        rpt.xrlProcedencia.Text = leUbicacionAnt.Text
        rpt.xrlNuevaUnidad.Text = leUbicacionAct.Text
        rpt.xrlMotivoTraslado.Text = meConcepto.Text
        rpt.xrlFecha.Text = deFecha.EditValue
        rpt.xrlFecha.Text = deFecha.EditValue
        rpt.ShowPreviewDialog()
    End Sub



    'Private Sub beUbicacion01_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beUbicacion01.ButtonClick
    '    beUbicacion01.EditValue = ""
    '    beUbicacion01_Validated(beUbicacion01, New System.EventArgs)
    'End Sub

    'Private Sub beUbicacion01_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beUbicacion01.Validated
    '    entUbicaciones = objTablas.acf_UbicacionesSelectByPK(objConsultas.cnsUbicaciones(frmConsultas))
    '    beUbicacion01.EditValue = entUbicaciones.IdUbicacion
    '    teUbicacion01.EditValue = entUbicaciones.Nombre
    'End Sub
End Class
