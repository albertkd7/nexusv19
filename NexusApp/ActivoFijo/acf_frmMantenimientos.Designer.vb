﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class acf_frmMantenimientos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
        Me.teIdActivo = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.meSolucionProblema = New DevExpress.XtraEditors.MemoEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.meDetalleProblema = New DevExpress.XtraEditors.MemoEdit
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl
        Me.deFechaProximoServicio = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl
        Me.deFechaVence = New DevExpress.XtraEditors.DateEdit
        Me.ceGarantia = New DevExpress.XtraEditors.CheckEdit
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.leTecnico = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.leFalla = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.seValor = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.deFecha = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.teNombre = New DevExpress.XtraEditors.TextEdit
        Me.teNumActivo = New DevExpress.XtraEditors.ButtonEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.teIdMantenimiento = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl59 = New DevExpress.XtraEditors.LabelControl
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.teIdActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meSolucionProblema.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meDetalleProblema.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaProximoServicio.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaProximoServicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaVence.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaVence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceGarantia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTecnico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leFalla.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seValor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdMantenimiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.teIdActivo)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.meSolucionProblema)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Controls.Add(Me.meDetalleProblema)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.deFechaProximoServicio)
        Me.PanelControl1.Controls.Add(Me.LabelControl9)
        Me.PanelControl1.Controls.Add(Me.deFechaVence)
        Me.PanelControl1.Controls.Add(Me.ceGarantia)
        Me.PanelControl1.Controls.Add(Me.LabelControl8)
        Me.PanelControl1.Controls.Add(Me.leTecnico)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.leFalla)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.seValor)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.deFecha)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.teNombre)
        Me.PanelControl1.Controls.Add(Me.teNumActivo)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.teIdMantenimiento)
        Me.PanelControl1.Controls.Add(Me.LabelControl59)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(601, 330)
        Me.PanelControl1.TabIndex = 0
        '
        'teIdActivo
        '
        Me.teIdActivo.EnterMoveNextControl = True
        Me.teIdActivo.Location = New System.Drawing.Point(486, 31)
        Me.teIdActivo.Name = "teIdActivo"
        Me.teIdActivo.Properties.ReadOnly = True
        Me.teIdActivo.Size = New System.Drawing.Size(87, 20)
        Me.teIdActivo.TabIndex = 104
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(431, 34)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl11.TabIndex = 105
        Me.LabelControl11.Text = "Id. Activo:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(72, 259)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(101, 13)
        Me.LabelControl2.TabIndex = 103
        Me.LabelControl2.Text = "Solucion al Problema:"
        '
        'meSolucionProblema
        '
        Me.meSolucionProblema.EnterMoveNextControl = True
        Me.meSolucionProblema.Location = New System.Drawing.Point(176, 257)
        Me.meSolucionProblema.Name = "meSolucionProblema"
        Me.meSolucionProblema.Size = New System.Drawing.Size(397, 51)
        Me.meSolucionProblema.TabIndex = 102
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(5, 203)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(168, 13)
        Me.LabelControl1.TabIndex = 101
        Me.LabelControl1.Text = "Detalle o Diagnostico del Problema:"
        '
        'meDetalleProblema
        '
        Me.meDetalleProblema.EnterMoveNextControl = True
        Me.meDetalleProblema.Location = New System.Drawing.Point(176, 200)
        Me.meDetalleProblema.Name = "meDetalleProblema"
        Me.meDetalleProblema.Size = New System.Drawing.Size(397, 51)
        Me.meDetalleProblema.TabIndex = 100
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(59, 177)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(114, 13)
        Me.LabelControl10.TabIndex = 99
        Me.LabelControl10.Text = "Fecha Proximo Servicio:"
        '
        'deFechaProximoServicio
        '
        Me.deFechaProximoServicio.EditValue = Nothing
        Me.deFechaProximoServicio.EnterMoveNextControl = True
        Me.deFechaProximoServicio.Location = New System.Drawing.Point(176, 174)
        Me.deFechaProximoServicio.Name = "deFechaProximoServicio"
        Me.deFechaProximoServicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaProximoServicio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaProximoServicio.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFechaProximoServicio.Size = New System.Drawing.Size(87, 20)
        Me.deFechaProximoServicio.TabIndex = 98
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(347, 151)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(137, 13)
        Me.LabelControl9.TabIndex = 97
        Me.LabelControl9.Text = "Fecha Vencimiento Garantia:"
        '
        'deFechaVence
        '
        Me.deFechaVence.EditValue = Nothing
        Me.deFechaVence.EnterMoveNextControl = True
        Me.deFechaVence.Location = New System.Drawing.Point(486, 148)
        Me.deFechaVence.Name = "deFechaVence"
        Me.deFechaVence.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaVence.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaVence.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFechaVence.Size = New System.Drawing.Size(87, 20)
        Me.deFechaVence.TabIndex = 96
        '
        'ceGarantia
        '
        Me.ceGarantia.EnterMoveNextControl = True
        Me.ceGarantia.Location = New System.Drawing.Point(176, 148)
        Me.ceGarantia.Name = "ceGarantia"
        Me.ceGarantia.Properties.Caption = "Este servicio tiene garantía"
        Me.ceGarantia.Size = New System.Drawing.Size(153, 19)
        Me.ceGarantia.TabIndex = 95
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(16, 126)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(157, 13)
        Me.LabelControl8.TabIndex = 94
        Me.LabelControl8.Text = "Tecnico o Empresa Responsable:"
        '
        'leTecnico
        '
        Me.leTecnico.EnterMoveNextControl = True
        Me.leTecnico.Location = New System.Drawing.Point(176, 123)
        Me.leTecnico.Name = "leTecnico"
        Me.leTecnico.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTecnico.Size = New System.Drawing.Size(397, 20)
        Me.leTecnico.TabIndex = 93
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(2, 104)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(171, 13)
        Me.LabelControl7.TabIndex = 92
        Me.LabelControl7.Text = "Tipo de Falla Detectada o Resuelta:"
        '
        'leFalla
        '
        Me.leFalla.EnterMoveNextControl = True
        Me.leFalla.Location = New System.Drawing.Point(176, 101)
        Me.leFalla.Name = "leFalla"
        Me.leFalla.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leFalla.Size = New System.Drawing.Size(397, 20)
        Me.leFalla.TabIndex = 91
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(397, 82)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl6.TabIndex = 90
        Me.LabelControl6.Text = "Valor del Servicio:"
        '
        'seValor
        '
        Me.seValor.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seValor.EnterMoveNextControl = True
        Me.seValor.Location = New System.Drawing.Point(486, 79)
        Me.seValor.Name = "seValor"
        Me.seValor.Properties.Mask.EditMask = "n2"
        Me.seValor.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seValor.Size = New System.Drawing.Size(87, 20)
        Me.seValor.TabIndex = 89
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(140, 78)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl5.TabIndex = 88
        Me.LabelControl5.Text = "Fecha:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(176, 75)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFecha.Size = New System.Drawing.Size(87, 20)
        Me.deFecha.TabIndex = 87
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(86, 34)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl4.TabIndex = 86
        Me.LabelControl4.Text = "Codigo del Activo:"
        '
        'teNombre
        '
        Me.teNombre.Enabled = False
        Me.teNombre.Location = New System.Drawing.Point(176, 53)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(397, 20)
        Me.teNombre.TabIndex = 80
        '
        'teNumActivo
        '
        Me.teNumActivo.Location = New System.Drawing.Point(176, 31)
        Me.teNumActivo.Name = "teNumActivo"
        Me.teNumActivo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.teNumActivo.Size = New System.Drawing.Size(123, 20)
        Me.teNumActivo.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(132, 56)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl3.TabIndex = 74
        Me.LabelControl3.Text = "Nombre:"
        '
        'teIdMantenimiento
        '
        Me.teIdMantenimiento.EnterMoveNextControl = True
        Me.teIdMantenimiento.Location = New System.Drawing.Point(176, 9)
        Me.teIdMantenimiento.Name = "teIdMantenimiento"
        Me.teIdMantenimiento.Size = New System.Drawing.Size(87, 20)
        Me.teIdMantenimiento.TabIndex = 0
        '
        'LabelControl59
        '
        Me.LabelControl59.Location = New System.Drawing.Point(34, 12)
        Me.LabelControl59.Name = "LabelControl59"
        Me.LabelControl59.Size = New System.Drawing.Size(139, 13)
        Me.LabelControl59.TabIndex = 73
        Me.LabelControl59.Text = "Correlativo o No. de Control:"
        '
        'acf_frmMantenimientos
        '
        Me.ClientSize = New System.Drawing.Size(601, 355)
        Me.Controls.Add(Me.PanelControl1)
        Me.Modulo = "Activo Fijo"
        Me.Name = "acf_frmMantenimientos"
        Me.OptionId = "002003"
        Me.Text = "Mantenimientos de Activo Fijo"
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.teIdActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meSolucionProblema.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meDetalleProblema.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaProximoServicio.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaProximoServicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaVence.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaVence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceGarantia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTecnico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leFalla.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seValor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdMantenimiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdMantenimiento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl59 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNumActivo As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents seValor As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leFalla As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTecnico As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents ceGarantia As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaVence As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaProximoServicio As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meDetalleProblema As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meSolucionProblema As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents teIdActivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl

End Class
