﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class acf_frmVentaActivos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.sbContabilizar = New DevExpress.XtraEditors.SimpleButton
        Me.sbSalir = New DevExpress.XtraEditors.SimpleButton
        Me.cmdImprimir = New DevExpress.XtraEditors.SimpleButton
        Me.GrupoHistoria = New DevExpress.XtraLayout.LayoutControlGroup
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup
        Me.gcIdActivo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcCodigo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcFechaAdquisicion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcValor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcVenta = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcValorContable = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcValorDepreciar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcDepreciacionAcumulada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl
        Me.gcActivos = New DevExpress.XtraGrid.GridControl
        Me.gvActivos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcRetira = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcPerdida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkRetirar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.leClase = New DevExpress.XtraEditors.LookUpEdit
        Me.deFechaVenta = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.leTipoPartida = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.leMotivo = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.beCtaPerdida = New DevExpress.XtraEditors.ButtonEdit
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.teCtaGasto = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.beCtaIngreso = New DevExpress.XtraEditors.ButtonEdit
        Me.teCtaIngreso = New DevExpress.XtraEditors.TextEdit
        Me.beCtaEfectivo = New DevExpress.XtraEditors.ButtonEdit
        Me.teCtaEfectivo = New DevExpress.XtraEditors.TextEdit
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl
        CType(Me.GrupoHistoria, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.gcActivos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvActivos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRetirar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leClase.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaVenta.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leMotivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCtaPerdida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCtaGasto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCtaIngreso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCtaIngreso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCtaEfectivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCtaEfectivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sbContabilizar
        '
        Me.sbContabilizar.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.sbContabilizar.Location = New System.Drawing.Point(253, 457)
        Me.sbContabilizar.Name = "sbContabilizar"
        Me.sbContabilizar.Size = New System.Drawing.Size(125, 22)
        Me.sbContabilizar.TabIndex = 35
        Me.sbContabilizar.Text = "Contabilizar"
        '
        'sbSalir
        '
        Me.sbSalir.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.sbSalir.Location = New System.Drawing.Point(392, 457)
        Me.sbSalir.Name = "sbSalir"
        Me.sbSalir.Size = New System.Drawing.Size(95, 22)
        Me.sbSalir.TabIndex = 14
        Me.sbSalir.Text = "Salir"
        '
        'cmdImprimir
        '
        Me.cmdImprimir.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.cmdImprimir.Location = New System.Drawing.Point(126, 457)
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Size = New System.Drawing.Size(112, 22)
        Me.cmdImprimir.TabIndex = 12
        Me.cmdImprimir.Text = "Imprimir Reporte"
        '
        'GrupoHistoria
        '
        Me.GrupoHistoria.CustomizationFormText = "GrupoHistoria"
        Me.GrupoHistoria.Location = New System.Drawing.Point(0, 416)
        Me.GrupoHistoria.Name = "GrupoHistoria"
        Me.GrupoHistoria.OptionsItemText.TextToControlDistance = 5
        Me.GrupoHistoria.Size = New System.Drawing.Size(337, 84)
        Me.GrupoHistoria.Text = "GrupoHistoria"
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.CustomizationFormText = "GrupoHistoria"
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 416)
        Me.LayoutControlGroup2.Name = "GrupoHistoria"
        Me.LayoutControlGroup2.OptionsItemText.TextToControlDistance = 5
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(337, 84)
        Me.LayoutControlGroup2.Text = "GrupoHistoria"
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.CustomizationFormText = "GrupoHistoria"
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 416)
        Me.LayoutControlGroup3.Name = "GrupoHistoria"
        Me.LayoutControlGroup3.OptionsItemText.TextToControlDistance = 5
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(337, 84)
        Me.LayoutControlGroup3.Text = "GrupoHistoria"
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.CustomizationFormText = "GrupoHistoria"
        Me.LayoutControlGroup4.Location = New System.Drawing.Point(0, 416)
        Me.LayoutControlGroup4.Name = "GrupoHistoria"
        Me.LayoutControlGroup4.OptionsItemText.TextToControlDistance = 5
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(337, 84)
        Me.LayoutControlGroup4.Text = "GrupoHistoria"
        '
        'gcIdActivo
        '
        Me.gcIdActivo.Caption = "Id. Activo"
        Me.gcIdActivo.FieldName = "IdActivo"
        Me.gcIdActivo.Name = "gcIdActivo"
        Me.gcIdActivo.OptionsColumn.ReadOnly = True
        Me.gcIdActivo.Visible = True
        Me.gcIdActivo.VisibleIndex = 0
        Me.gcIdActivo.Width = 60
        '
        'gcCodigo
        '
        Me.gcCodigo.Caption = "Codigo"
        Me.gcCodigo.FieldName = "Codigo"
        Me.gcCodigo.Name = "gcCodigo"
        Me.gcCodigo.OptionsColumn.ReadOnly = True
        Me.gcCodigo.Visible = True
        Me.gcCodigo.VisibleIndex = 1
        Me.gcCodigo.Width = 84
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.OptionsColumn.ReadOnly = True
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 2
        Me.gcNombre.Width = 224
        '
        'gcFechaAdquisicion
        '
        Me.gcFechaAdquisicion.Caption = "Fecha Adquisicion"
        Me.gcFechaAdquisicion.FieldName = "FechaAdquisicion"
        Me.gcFechaAdquisicion.Name = "gcFechaAdquisicion"
        Me.gcFechaAdquisicion.OptionsColumn.ReadOnly = True
        Me.gcFechaAdquisicion.Visible = True
        Me.gcFechaAdquisicion.VisibleIndex = 3
        Me.gcFechaAdquisicion.Width = 96
        '
        'gcValor
        '
        Me.gcValor.Caption = "Valor Adquisicion"
        Me.gcValor.FieldName = "ValorAdquisicion"
        Me.gcValor.Name = "gcValor"
        Me.gcValor.OptionsColumn.ReadOnly = True
        Me.gcValor.Visible = True
        Me.gcValor.VisibleIndex = 4
        Me.gcValor.Width = 93
        '
        'gcVenta
        '
        Me.gcVenta.Caption = "Valor Venta"
        Me.gcVenta.FieldName = "ValorVenta"
        Me.gcVenta.Name = "gcVenta"
        Me.gcVenta.Visible = True
        Me.gcVenta.VisibleIndex = 5
        Me.gcVenta.Width = 86
        '
        'gcValorContable
        '
        Me.gcValorContable.Caption = "Valor Contable"
        Me.gcValorContable.FieldName = "ValorContable"
        Me.gcValorContable.Name = "gcValorContable"
        '
        'gcValorDepreciar
        '
        Me.gcValorDepreciar.Caption = "Valor a Depreciar"
        Me.gcValorDepreciar.FieldName = "ValorDepreciar"
        Me.gcValorDepreciar.Name = "gcValorDepreciar"
        '
        'gcDepreciacionAcumulada
        '
        Me.gcDepreciacionAcumulada.Caption = "Depreciacion Acumulada"
        Me.gcDepreciacionAcumulada.FieldName = "DepreciacionAcumulada"
        Me.gcDepreciacionAcumulada.Name = "gcDepreciacionAcumulada"
        '
        'GroupControl2
        '
        Me.GroupControl2.CaptionLocation = DevExpress.Utils.Locations.Top
        Me.GroupControl2.Controls.Add(Me.gcActivos)
        Me.GroupControl2.Location = New System.Drawing.Point(7, 37)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(840, 274)
        Me.GroupControl2.TabIndex = 38
        Me.GroupControl2.Text = "Lista de Activos"
        '
        'gcActivos
        '
        Me.gcActivos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcActivos.Location = New System.Drawing.Point(2, 22)
        Me.gcActivos.MainView = Me.gvActivos
        Me.gcActivos.Name = "gcActivos"
        Me.gcActivos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkRetirar})
        Me.gcActivos.Size = New System.Drawing.Size(836, 250)
        Me.gcActivos.TabIndex = 5
        Me.gcActivos.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvActivos})
        '
        'gvActivos
        '
        Me.gvActivos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.gcRetira, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.gcPerdida})
        Me.gvActivos.GridControl = Me.gcActivos
        Me.gvActivos.Name = "gvActivos"
        Me.gvActivos.OptionsView.ShowAutoFilterRow = True
        Me.gvActivos.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Cód. Activo"
        Me.GridColumn1.FieldName = "IdActivo"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.ReadOnly = True
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 60
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Codigo"
        Me.GridColumn2.FieldName = "Codigo"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.ReadOnly = True
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 84
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Nombre"
        Me.GridColumn3.FieldName = "Nombre"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.ReadOnly = True
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 260
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Fecha Adquisicion"
        Me.GridColumn4.FieldName = "FechaAdquisicion"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.OptionsColumn.ReadOnly = True
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 96
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Valor Adquisicion"
        Me.GridColumn5.FieldName = "ValorAdquisicion"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.OptionsColumn.ReadOnly = True
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 4
        Me.GridColumn5.Width = 88
        '
        'gcRetira
        '
        Me.gcRetira.Caption = "Valor Venta"
        Me.gcRetira.FieldName = "ValorVenta"
        Me.gcRetira.Name = "gcRetira"
        Me.gcRetira.Visible = True
        Me.gcRetira.VisibleIndex = 5
        Me.gcRetira.Width = 55
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Valor Contable"
        Me.GridColumn6.FieldName = "ValorContable"
        Me.GridColumn6.Name = "GridColumn6"
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Valor a Depreciar"
        Me.GridColumn7.FieldName = "ValorDepreciar"
        Me.GridColumn7.Name = "GridColumn7"
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Depreciacion Acumulada"
        Me.GridColumn8.FieldName = "DepreciacionAcumulada"
        Me.GridColumn8.Name = "GridColumn8"
        '
        'gcPerdida
        '
        Me.gcPerdida.Caption = "Perdida"
        Me.gcPerdida.FieldName = "Perdida"
        Me.gcPerdida.Name = "gcPerdida"
        '
        'chkRetirar
        '
        Me.chkRetirar.AutoHeight = False
        Me.chkRetirar.Name = "chkRetirar"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(10, 15)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl1.TabIndex = 36
        Me.LabelControl1.Text = "Clase de Activo:"
        '
        'leClase
        '
        Me.leClase.Location = New System.Drawing.Point(91, 12)
        Me.leClase.Name = "leClase"
        Me.leClase.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leClase.Size = New System.Drawing.Size(386, 20)
        Me.leClase.TabIndex = 37
        '
        'deFechaVenta
        '
        Me.deFechaVenta.EditValue = Nothing
        Me.deFechaVenta.Location = New System.Drawing.Point(672, 317)
        Me.deFechaVenta.Name = "deFechaVenta"
        Me.deFechaVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaVenta.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFechaVenta.Size = New System.Drawing.Size(99, 20)
        Me.deFechaVenta.TabIndex = 44
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(604, 321)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl6.TabIndex = 47
        Me.LabelControl6.Text = "Fecha Venta:"
        '
        'leTipoPartida
        '
        Me.leTipoPartida.Location = New System.Drawing.Point(419, 317)
        Me.leTipoPartida.Name = "leTipoPartida"
        Me.leTipoPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPartida.Size = New System.Drawing.Size(164, 20)
        Me.leTipoPartida.TabIndex = 40
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(296, 320)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl2.TabIndex = 39
        Me.LabelControl2.Text = "Tipo de Partida a Utilizar:"
        '
        'leMotivo
        '
        Me.leMotivo.Location = New System.Drawing.Point(126, 317)
        Me.leMotivo.Name = "leMotivo"
        Me.leMotivo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leMotivo.Size = New System.Drawing.Size(164, 20)
        Me.leMotivo.TabIndex = 43
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(48, 320)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl7.TabIndex = 48
        Me.LabelControl7.Text = "Motivo de Baja:"
        '
        'beCtaPerdida
        '
        Me.beCtaPerdida.EnterMoveNextControl = True
        Me.beCtaPerdida.Location = New System.Drawing.Point(126, 365)
        Me.beCtaPerdida.Name = "beCtaPerdida"
        Me.beCtaPerdida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beCtaPerdida.Size = New System.Drawing.Size(164, 20)
        Me.beCtaPerdida.TabIndex = 45
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(126, 339)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(646, 20)
        Me.txtDescripcion.TabIndex = 41
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(10, 343)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(113, 13)
        Me.LabelControl3.TabIndex = 42
        Me.LabelControl3.Text = "Concepto de la Partida:"
        '
        'teCtaGasto
        '
        Me.teCtaGasto.Enabled = False
        Me.teCtaGasto.Location = New System.Drawing.Point(296, 365)
        Me.teCtaGasto.Name = "teCtaGasto"
        Me.teCtaGasto.Properties.ReadOnly = True
        Me.teCtaGasto.Size = New System.Drawing.Size(368, 20)
        Me.teCtaGasto.TabIndex = 46
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(45, 368)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl4.TabIndex = 49
        Me.LabelControl4.Text = "Cuenta Perdida:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(44, 395)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl5.TabIndex = 50
        Me.LabelControl5.Text = "Cuenta Ingreso:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(42, 422)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(81, 13)
        Me.LabelControl8.TabIndex = 51
        Me.LabelControl8.Text = "Cuenta Efectivo:"
        '
        'beCtaIngreso
        '
        Me.beCtaIngreso.EnterMoveNextControl = True
        Me.beCtaIngreso.Location = New System.Drawing.Point(126, 390)
        Me.beCtaIngreso.Name = "beCtaIngreso"
        Me.beCtaIngreso.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beCtaIngreso.Size = New System.Drawing.Size(164, 20)
        Me.beCtaIngreso.TabIndex = 52
        '
        'teCtaIngreso
        '
        Me.teCtaIngreso.Enabled = False
        Me.teCtaIngreso.Location = New System.Drawing.Point(296, 390)
        Me.teCtaIngreso.Name = "teCtaIngreso"
        Me.teCtaIngreso.Properties.ReadOnly = True
        Me.teCtaIngreso.Size = New System.Drawing.Size(368, 20)
        Me.teCtaIngreso.TabIndex = 53
        '
        'beCtaEfectivo
        '
        Me.beCtaEfectivo.EnterMoveNextControl = True
        Me.beCtaEfectivo.Location = New System.Drawing.Point(126, 415)
        Me.beCtaEfectivo.Name = "beCtaEfectivo"
        Me.beCtaEfectivo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beCtaEfectivo.Size = New System.Drawing.Size(164, 20)
        Me.beCtaEfectivo.TabIndex = 54
        '
        'teCtaEfectivo
        '
        Me.teCtaEfectivo.Enabled = False
        Me.teCtaEfectivo.Location = New System.Drawing.Point(296, 415)
        Me.teCtaEfectivo.Name = "teCtaEfectivo"
        Me.teCtaEfectivo.Properties.ReadOnly = True
        Me.teCtaEfectivo.Size = New System.Drawing.Size(368, 20)
        Me.teCtaEfectivo.TabIndex = 55
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(585, 11)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(260, 20)
        Me.leSucursal.TabIndex = 56
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(538, 15)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl32.TabIndex = 57
        Me.LabelControl32.Text = "Sucursal:"
        '
        'acf_frmVentaActivos
        '
        Me.ClientSize = New System.Drawing.Size(863, 518)
        Me.Controls.Add(Me.leSucursal)
        Me.Controls.Add(Me.LabelControl32)
        Me.Controls.Add(Me.beCtaEfectivo)
        Me.Controls.Add(Me.teCtaEfectivo)
        Me.Controls.Add(Me.beCtaIngreso)
        Me.Controls.Add(Me.teCtaIngreso)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.deFechaVenta)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.leTipoPartida)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.leMotivo)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.beCtaPerdida)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.teCtaGasto)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.leClase)
        Me.Controls.Add(Me.sbContabilizar)
        Me.Controls.Add(Me.sbSalir)
        Me.Controls.Add(Me.cmdImprimir)
        Me.DbMode = 1
        Me.Modulo = "Activo Fijo"
        Me.Name = "acf_frmVentaActivos"
        Me.OptionId = "002004"
        Me.Text = "Venta de Activos Fijos"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.cmdImprimir, 0)
        Me.Controls.SetChildIndex(Me.sbSalir, 0)
        Me.Controls.SetChildIndex(Me.sbContabilizar, 0)
        Me.Controls.SetChildIndex(Me.leClase, 0)
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.GroupControl2, 0)
        Me.Controls.SetChildIndex(Me.teCtaGasto, 0)
        Me.Controls.SetChildIndex(Me.LabelControl3, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.beCtaPerdida, 0)
        Me.Controls.SetChildIndex(Me.LabelControl7, 0)
        Me.Controls.SetChildIndex(Me.leMotivo, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        Me.Controls.SetChildIndex(Me.leTipoPartida, 0)
        Me.Controls.SetChildIndex(Me.LabelControl6, 0)
        Me.Controls.SetChildIndex(Me.deFechaVenta, 0)
        Me.Controls.SetChildIndex(Me.LabelControl4, 0)
        Me.Controls.SetChildIndex(Me.LabelControl5, 0)
        Me.Controls.SetChildIndex(Me.LabelControl8, 0)
        Me.Controls.SetChildIndex(Me.teCtaIngreso, 0)
        Me.Controls.SetChildIndex(Me.beCtaIngreso, 0)
        Me.Controls.SetChildIndex(Me.teCtaEfectivo, 0)
        Me.Controls.SetChildIndex(Me.beCtaEfectivo, 0)
        Me.Controls.SetChildIndex(Me.LabelControl32, 0)
        Me.Controls.SetChildIndex(Me.leSucursal, 0)
        CType(Me.GrupoHistoria, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.gcActivos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvActivos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRetirar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leClase.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaVenta.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leMotivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCtaPerdida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCtaGasto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCtaIngreso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCtaIngreso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCtaEfectivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCtaEfectivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdImprimir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbContabilizar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GrupoHistoria As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents gcIdActivo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCodigo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFechaAdquisicion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcVenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorContable As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorDepreciar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDepreciacionAcumulada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gcActivos As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvActivos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcRetira As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPerdida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkRetirar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leClase As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents deFechaVenta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leMotivo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCtaPerdida As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCtaGasto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCtaIngreso As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents teCtaIngreso As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCtaEfectivo As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents teCtaEfectivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl

End Class
