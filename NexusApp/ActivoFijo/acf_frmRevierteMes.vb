﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class acf_frmRevierteMes
    Dim iId = 0
    Dim myBL As New ActivosBusiness(g_ConnectionString)
    Dim blContable As New ContabilidadBLL(g_ConnectionString)
    Dim blTabla As New TableBusiness(g_ConnectionString)
    Dim entCuentas As New con_Cuentas

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        Dim dFechaFinal As DateTime
        dFechaFinal = CDate(CStr(SpinEdit1.EditValue) & "/" & (MonthEdit1.EditValue).ToString.PadLeft(2, "0") & "/01")
        dFechaFinal = DateAdd(DateInterval.Month, 1, dFechaFinal)
        dFechaFinal = DateAdd(DateInterval.Day, -1, dFechaFinal)
        Dim EsOk As Boolean = ValidarFechaCierre(dFechaFinal)
        If Not EsOk Then
            MsgBox("el periodo contable ya esta cerrado, No es posible revertir", 16, "Error de datos")
            Return
        End If
        If MsgBox("Está seguro(a) de revertir esta contabilización?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Return
        End If
        If myBL.RevertirContabilizarActivoFijo(SpinEdit1.Value, MonthEdit1.Month, leSucursal.EditValue) = "Ok" Then
            MsgBox("El cierre ha sido revertido con éxito")
        Else
            MsgBox("No fue posible revertir el cierre")
        End If
        Me.Close()
    End Sub

    Private Sub frmCierreMes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        CambiaDatos()
    End Sub

    Private Sub CambiaDatos()
        Dim dt As New DataTable
        Dim MesCerrado As Integer
        Dim PeriodoCerrado As Integer
        dt = myBL.ObtienePeriodoCerrado(leSucursal.EditValue)
        If dt.Rows.Count > 0 Then
            MesCerrado = SiEsNulo(dt.Rows(0).Item(0), Now.Month)
            PeriodoCerrado = SiEsNulo(dt.Rows(0).Item(1), Now.Year)
            SpinEdit1.Value = PeriodoCerrado
            MonthEdit1.Month = MesCerrado
        Else
            MesCerrado = Today.Month
            PeriodoCerrado = Today.Year
            SpinEdit1.Value = PeriodoCerrado
            MonthEdit1.Month = MesCerrado
        End If
        If PeriodoCerrado = 12 Then
            MonthEdit1.Month = 1
            SpinEdit1.Value = PeriodoCerrado + 1
        End If
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        CambiaDatos()
    End Sub
End Class