﻿Imports NexusELL.TableEntities
Public Class acf_frmFallas

    Private Sub acf_frmFallas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.acf_FallasSelectAll
    End Sub
    Private Sub acf_frmFallas_Eliminar() Handles MyBase.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Return
        End If

        Dim Id As Integer = gv.GetFocusedRowCellValue(gv.Columns(0))
        Dim msj As String = ""
        Try
            objTablas.acf_FallasDeleteByPK(Id)
        Catch ex As Exception
            msj = ex.Message()
        End Try
        If msj = "" Then
            MsgBox("La Falla ha sido eliminada con éxito", MsgBoxStyle.Information)
        Else
            MsgBox("No fue posible eliminar la Falla" + Chr(13) + msj, MsgBoxStyle.Critical, "Error al eliminar el registro")
        End If
        gc.DataSource = objTablas.acf_FallasSelectAll
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        Dim msj As String = ""
        Dim entidad As New acf_Fallas
        Dim numFila As Integer

        If e.RowHandle < 0 Then
            DbMode = DbModeType.insert
            numFila = gv.RowCount - 1
        Else
            DbMode = DbModeType.update
            numFila = e.RowHandle
        End If

        If Not AllowInsert Or Not AllowEdit Then
            MsgBox("No le está permitido ingresar o editar información", MsgBoxStyle.Exclamation, Me.Text)
            gc.DataSource = objTablas.acf_FallasSelectAll
            Exit Sub
        End If

        With entidad
            .IdFalla = gv.GetRowCellValue(numFila, gv.Columns(0).FieldName)
            .Nombre = gv.GetRowCellValue(numFila, gv.Columns(1).FieldName)

        End With

        If DbMode = DbModeType.insert Then
            Try
                objTablas.acf_FallasInsert(entidad)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If Not msj = "" Then
                MsgBox(String.Format("No fue posible Guardar la Falla{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error al Guardar el registro")
            End If
        Else
            Try
                objTablas.acf_FallasUpdate(entidad)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If Not msj = "" Then
                MsgBox(String.Format("No fue posible actualizar la Falla{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error al Guardar el registro")
            End If
        End If
        gc.DataSource = objTablas.acf_FallasSelectAll
    End Sub

    Private Sub acf_frmFallas_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub

End Class
