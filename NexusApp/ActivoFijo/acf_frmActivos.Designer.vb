﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class acf_frmActivos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xtcActivos = New DevExpress.XtraTab.XtraTabControl
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage
        Me.gc2 = New DevExpress.XtraGrid.GridControl
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl
        Me.sbVitacoraTraslados = New DevExpress.XtraEditors.SimpleButton
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl
        Me.deVtoGarantia = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl
        Me.beEmpleado = New DevExpress.XtraEditors.ButtonEdit
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl
        Me.meInfo = New DevExpress.XtraEditors.MemoExEdit
        Me.teEmpleado = New DevExpress.XtraEditors.TextEdit
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl
        Me.seValorDepreciar = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl
        Me.seVidaMeses = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl
        Me.seVidaAnios = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl
        Me.seCuotaMensual = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl
        Me.seCuotaAnual = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl
        Me.deInicioDepreciacion = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl
        Me.seValorContable = New DevExpress.XtraEditors.SpinEdit
        Me.ceDepreciable = New DevExpress.XtraEditors.CheckEdit
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl
        Me.beCtaActivo = New DevExpress.XtraEditors.ButtonEdit
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl
        Me.beCtaDepreciacion = New DevExpress.XtraEditors.ButtonEdit
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl
        Me.beCtaGasto = New DevExpress.XtraEditors.ButtonEdit
        Me.teCtaActivo = New DevExpress.XtraEditors.TextEdit
        Me.teCtaDepreciacion = New DevExpress.XtraEditors.TextEdit
        Me.teCtaGasto = New DevExpress.XtraEditors.TextEdit
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl
        Me.beProveedor = New DevExpress.XtraEditors.ButtonEdit
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl
        Me.seCostoAdquisicion = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl
        Me.teNoFactura = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl
        Me.teNumCheque = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl
        Me.deFechaAdquisicion = New DevExpress.XtraEditors.DateEdit
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.leClase = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.leUbicacion = New DevExpress.XtraEditors.LookUpEdit
        Me.meDescripcion = New DevExpress.XtraEditors.MemoEdit
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.teCodigo = New DevExpress.XtraEditors.TextEdit
        Me.leMarca = New DevExpress.XtraEditors.LookUpEdit
        Me.teNombre = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl
        Me.leEstado = New DevExpress.XtraEditors.LookUpEdit
        Me.seAnio = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl
        Me.leTiposDepreciacion = New DevExpress.XtraEditors.LookUpEdit
        Me.teSerie = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl
        Me.leModelo = New DevExpress.XtraEditors.LookUpEdit
        Me.teColor = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl
        Me.leEstilo = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl
        CType(Me.xtcActivos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcActivos.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.deVtoGarantia.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deVtoGarantia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beEmpleado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meInfo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teEmpleado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.seValorDepreciar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seVidaMeses.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seVidaAnios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seCuotaMensual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seCuotaAnual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deInicioDepreciacion.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deInicioDepreciacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seValorContable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceDepreciable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.beCtaActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCtaDepreciacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCtaGasto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCtaActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCtaDepreciacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCtaGasto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.beProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seCostoAdquisicion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNoFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaAdquisicion.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaAdquisicion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leClase.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leUbicacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leMarca.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTiposDepreciacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leModelo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teColor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leEstilo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcActivos
        '
        Me.xtcActivos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcActivos.Location = New System.Drawing.Point(0, 0)
        Me.xtcActivos.Name = "xtcActivos"
        Me.xtcActivos.SelectedTabPage = Me.xtpLista
        Me.xtcActivos.Size = New System.Drawing.Size(830, 548)
        Me.xtcActivos.TabIndex = 4
        Me.xtcActivos.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc2)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(824, 522)
        Me.xtpLista.Text = "Consulta Activos Fijos"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(824, 522)
        Me.gc2.TabIndex = 4
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn3, Me.GridColumn8, Me.GridColumn5, Me.GridColumn2, Me.GridColumn9, Me.GridColumn6, Me.GridColumn7})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdActivo"
        Me.GridColumn1.FieldName = "IdActivo"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 59
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Cód. Activo"
        Me.GridColumn3.FieldName = "Codigo"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        Me.GridColumn3.Width = 85
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Sucursal"
        Me.GridColumn8.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn8.FieldName = "IdSucursal"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 2
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Nombre Activo"
        Me.GridColumn5.FieldName = "Nombre"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 3
        Me.GridColumn5.Width = 440
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Valor Adquisición"
        Me.GridColumn2.FieldName = "ValorAdquisicion"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 4
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Valor a Depreciar"
        Me.GridColumn9.FieldName = "ValorDepreciar"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 5
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Creado Por"
        Me.GridColumn6.FieldName = "CreadoPor"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 6
        Me.GridColumn6.Width = 98
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Fecha Hora Creacion"
        Me.GridColumn7.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.GridColumn7.FieldName = "FechaHoraCreacion"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 7
        Me.GridColumn7.Width = 114
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        Me.RepositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.GroupControl5)
        Me.xtpDatos.Controls.Add(Me.GroupControl3)
        Me.xtpDatos.Controls.Add(Me.GroupControl2)
        Me.xtpDatos.Controls.Add(Me.GroupControl4)
        Me.xtpDatos.Controls.Add(Me.pcHeader)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(824, 522)
        Me.xtpDatos.Text = "Activos Fijos"
        '
        'GroupControl5
        '
        Me.GroupControl5.CaptionLocation = DevExpress.Utils.Locations.Top
        Me.GroupControl5.Controls.Add(Me.sbVitacoraTraslados)
        Me.GroupControl5.Controls.Add(Me.LabelControl24)
        Me.GroupControl5.Controls.Add(Me.deVtoGarantia)
        Me.GroupControl5.Controls.Add(Me.LabelControl25)
        Me.GroupControl5.Controls.Add(Me.beEmpleado)
        Me.GroupControl5.Controls.Add(Me.LabelControl26)
        Me.GroupControl5.Controls.Add(Me.meInfo)
        Me.GroupControl5.Controls.Add(Me.teEmpleado)
        Me.GroupControl5.Location = New System.Drawing.Point(0, 406)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(823, 75)
        Me.GroupControl5.TabIndex = 58
        Me.GroupControl5.Text = "Otros Datos"
        '
        'sbVitacoraTraslados
        '
        Me.sbVitacoraTraslados.Location = New System.Drawing.Point(348, 47)
        Me.sbVitacoraTraslados.Name = "sbVitacoraTraslados"
        Me.sbVitacoraTraslados.Size = New System.Drawing.Size(104, 23)
        Me.sbVitacoraTraslados.TabIndex = 31
        Me.sbVitacoraTraslados.Text = "Historico Traslados"
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(554, 52)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(131, 13)
        Me.LabelControl24.TabIndex = 0
        Me.LabelControl24.Text = "Vencimiento de la Garantía:"
        '
        'deVtoGarantia
        '
        Me.deVtoGarantia.EditValue = Nothing
        Me.deVtoGarantia.EnterMoveNextControl = True
        Me.deVtoGarantia.Location = New System.Drawing.Point(692, 49)
        Me.deVtoGarantia.Name = "deVtoGarantia"
        Me.deVtoGarantia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deVtoGarantia.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deVtoGarantia.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deVtoGarantia.Size = New System.Drawing.Size(121, 20)
        Me.deVtoGarantia.TabIndex = 3
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(30, 27)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(114, 13)
        Me.LabelControl25.TabIndex = 21
        Me.LabelControl25.Text = "Empleado Responsable:"
        '
        'beEmpleado
        '
        Me.beEmpleado.EnterMoveNextControl = True
        Me.beEmpleado.Location = New System.Drawing.Point(151, 25)
        Me.beEmpleado.Name = "beEmpleado"
        Me.beEmpleado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beEmpleado.Size = New System.Drawing.Size(175, 20)
        Me.beEmpleado.TabIndex = 0
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(41, 52)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl26.TabIndex = 22
        Me.LabelControl26.Text = "Información Adicional:"
        '
        'meInfo
        '
        Me.meInfo.EnterMoveNextControl = True
        Me.meInfo.Location = New System.Drawing.Point(151, 48)
        Me.meInfo.Name = "meInfo"
        Me.meInfo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meInfo.Size = New System.Drawing.Size(175, 20)
        Me.meInfo.TabIndex = 2
        '
        'teEmpleado
        '
        Me.teEmpleado.EnterMoveNextControl = True
        Me.teEmpleado.Location = New System.Drawing.Point(348, 25)
        Me.teEmpleado.Name = "teEmpleado"
        Me.teEmpleado.Properties.ReadOnly = True
        Me.teEmpleado.Size = New System.Drawing.Size(467, 20)
        Me.teEmpleado.TabIndex = 1
        '
        'GroupControl3
        '
        Me.GroupControl3.CaptionLocation = DevExpress.Utils.Locations.Top
        Me.GroupControl3.Controls.Add(Me.LabelControl12)
        Me.GroupControl3.Controls.Add(Me.seValorDepreciar)
        Me.GroupControl3.Controls.Add(Me.LabelControl13)
        Me.GroupControl3.Controls.Add(Me.seVidaMeses)
        Me.GroupControl3.Controls.Add(Me.LabelControl14)
        Me.GroupControl3.Controls.Add(Me.seVidaAnios)
        Me.GroupControl3.Controls.Add(Me.LabelControl15)
        Me.GroupControl3.Controls.Add(Me.seCuotaMensual)
        Me.GroupControl3.Controls.Add(Me.LabelControl16)
        Me.GroupControl3.Controls.Add(Me.seCuotaAnual)
        Me.GroupControl3.Controls.Add(Me.LabelControl17)
        Me.GroupControl3.Controls.Add(Me.deInicioDepreciacion)
        Me.GroupControl3.Controls.Add(Me.LabelControl18)
        Me.GroupControl3.Controls.Add(Me.seValorContable)
        Me.GroupControl3.Controls.Add(Me.ceDepreciable)
        Me.GroupControl3.Location = New System.Drawing.Point(348, 258)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(475, 146)
        Me.GroupControl3.TabIndex = 56
        Me.GroupControl3.Text = "Datos de Depreciación"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(16, 26)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl12.TabIndex = 0
        Me.LabelControl12.Text = "Valor a Depreciar:"
        '
        'seValorDepreciar
        '
        Me.seValorDepreciar.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seValorDepreciar.EnterMoveNextControl = True
        Me.seValorDepreciar.Location = New System.Drawing.Point(106, 23)
        Me.seValorDepreciar.Name = "seValorDepreciar"
        Me.seValorDepreciar.Properties.Mask.EditMask = "n2"
        Me.seValorDepreciar.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seValorDepreciar.Size = New System.Drawing.Size(107, 20)
        Me.seValorDepreciar.TabIndex = 0
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(12, 51)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl13.TabIndex = 26
        Me.LabelControl13.Text = "Vida Util en Meses:"
        '
        'seVidaMeses
        '
        Me.seVidaMeses.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seVidaMeses.EnterMoveNextControl = True
        Me.seVidaMeses.Location = New System.Drawing.Point(106, 47)
        Me.seVidaMeses.Name = "seVidaMeses"
        Me.seVidaMeses.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seVidaMeses.Properties.Mask.EditMask = "n0"
        Me.seVidaMeses.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seVidaMeses.Size = New System.Drawing.Size(66, 20)
        Me.seVidaMeses.TabIndex = 1
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(275, 50)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl14.TabIndex = 27
        Me.LabelControl14.Text = "Vida Util en años:"
        '
        'seVidaAnios
        '
        Me.seVidaAnios.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seVidaAnios.Location = New System.Drawing.Point(361, 47)
        Me.seVidaAnios.Name = "seVidaAnios"
        Me.seVidaAnios.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seVidaAnios.Properties.Mask.EditMask = "n2"
        Me.seVidaAnios.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seVidaAnios.Properties.ReadOnly = True
        Me.seVidaAnios.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.seVidaAnios.Size = New System.Drawing.Size(66, 20)
        Me.seVidaAnios.TabIndex = 27
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(27, 74)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl15.TabIndex = 28
        Me.LabelControl15.Text = "Cuota Mensual:"
        '
        'seCuotaMensual
        '
        Me.seCuotaMensual.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seCuotaMensual.EnterMoveNextControl = True
        Me.seCuotaMensual.Location = New System.Drawing.Point(106, 69)
        Me.seCuotaMensual.Name = "seCuotaMensual"
        Me.seCuotaMensual.Properties.Mask.EditMask = "n2"
        Me.seCuotaMensual.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seCuotaMensual.Size = New System.Drawing.Size(107, 20)
        Me.seCuotaMensual.TabIndex = 2
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(39, 96)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(63, 13)
        Me.LabelControl16.TabIndex = 29
        Me.LabelControl16.Text = "Cuota Anual:"
        '
        'seCuotaAnual
        '
        Me.seCuotaAnual.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seCuotaAnual.Location = New System.Drawing.Point(106, 90)
        Me.seCuotaAnual.Name = "seCuotaAnual"
        Me.seCuotaAnual.Properties.Mask.EditMask = "n2"
        Me.seCuotaAnual.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seCuotaAnual.Properties.ReadOnly = True
        Me.seCuotaAnual.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.seCuotaAnual.Size = New System.Drawing.Size(107, 20)
        Me.seCuotaAnual.TabIndex = 3
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(217, 26)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(142, 13)
        Me.LabelControl17.TabIndex = 30
        Me.LabelControl17.Text = "Fecha Inicial de Depreciación:"
        '
        'deInicioDepreciacion
        '
        Me.deInicioDepreciacion.EditValue = Nothing
        Me.deInicioDepreciacion.EnterMoveNextControl = True
        Me.deInicioDepreciacion.Location = New System.Drawing.Point(361, 23)
        Me.deInicioDepreciacion.Name = "deInicioDepreciacion"
        Me.deInicioDepreciacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deInicioDepreciacion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deInicioDepreciacion.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deInicioDepreciacion.Size = New System.Drawing.Size(107, 20)
        Me.deInicioDepreciacion.TabIndex = 4
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(233, 74)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(126, 13)
        Me.LabelControl18.TabIndex = 31
        Me.LabelControl18.Text = "Valor Contable o Residual:"
        '
        'seValorContable
        '
        Me.seValorContable.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seValorContable.EnterMoveNextControl = True
        Me.seValorContable.Location = New System.Drawing.Point(361, 71)
        Me.seValorContable.Name = "seValorContable"
        Me.seValorContable.Properties.Mask.EditMask = "n2"
        Me.seValorContable.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seValorContable.Size = New System.Drawing.Size(107, 20)
        Me.seValorContable.TabIndex = 5
        '
        'ceDepreciable
        '
        Me.ceDepreciable.EnterMoveNextControl = True
        Me.ceDepreciable.Location = New System.Drawing.Point(262, 97)
        Me.ceDepreciable.Name = "ceDepreciable"
        Me.ceDepreciable.Properties.Caption = "Genera depreciación o es depreciable?"
        Me.ceDepreciable.Size = New System.Drawing.Size(207, 19)
        Me.ceDepreciable.TabIndex = 20
        '
        'GroupControl2
        '
        Me.GroupControl2.CaptionLocation = DevExpress.Utils.Locations.Top
        Me.GroupControl2.Controls.Add(Me.LabelControl9)
        Me.GroupControl2.Controls.Add(Me.beCtaActivo)
        Me.GroupControl2.Controls.Add(Me.LabelControl10)
        Me.GroupControl2.Controls.Add(Me.beCtaDepreciacion)
        Me.GroupControl2.Controls.Add(Me.LabelControl11)
        Me.GroupControl2.Controls.Add(Me.beCtaGasto)
        Me.GroupControl2.Controls.Add(Me.teCtaActivo)
        Me.GroupControl2.Controls.Add(Me.teCtaDepreciacion)
        Me.GroupControl2.Controls.Add(Me.teCtaGasto)
        Me.GroupControl2.Location = New System.Drawing.Point(0, 258)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(341, 146)
        Me.GroupControl2.TabIndex = 55
        Me.GroupControl2.Text = "Datos Contables"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(9, 26)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl9.TabIndex = 0
        Me.LabelControl9.Text = "Cuenta de Activo:"
        '
        'beCtaActivo
        '
        Me.beCtaActivo.EnterMoveNextControl = True
        Me.beCtaActivo.Location = New System.Drawing.Point(137, 21)
        Me.beCtaActivo.Name = "beCtaActivo"
        Me.beCtaActivo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beCtaActivo.Size = New System.Drawing.Size(189, 20)
        Me.beCtaActivo.TabIndex = 0
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(9, 67)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(118, 13)
        Me.LabelControl10.TabIndex = 23
        Me.LabelControl10.Text = "Cuenta de Depreciación:"
        '
        'beCtaDepreciacion
        '
        Me.beCtaDepreciacion.EnterMoveNextControl = True
        Me.beCtaDepreciacion.Location = New System.Drawing.Point(137, 62)
        Me.beCtaDepreciacion.Name = "beCtaDepreciacion"
        Me.beCtaDepreciacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beCtaDepreciacion.Size = New System.Drawing.Size(189, 20)
        Me.beCtaDepreciacion.TabIndex = 2
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(9, 106)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl11.TabIndex = 24
        Me.LabelControl11.Text = "Cuenta de Gasto:"
        '
        'beCtaGasto
        '
        Me.beCtaGasto.EnterMoveNextControl = True
        Me.beCtaGasto.Location = New System.Drawing.Point(137, 103)
        Me.beCtaGasto.Name = "beCtaGasto"
        Me.beCtaGasto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beCtaGasto.Size = New System.Drawing.Size(189, 20)
        Me.beCtaGasto.TabIndex = 4
        '
        'teCtaActivo
        '
        Me.teCtaActivo.Enabled = False
        Me.teCtaActivo.Location = New System.Drawing.Point(5, 42)
        Me.teCtaActivo.Name = "teCtaActivo"
        Me.teCtaActivo.Properties.AllowFocused = False
        Me.teCtaActivo.Properties.ReadOnly = True
        Me.teCtaActivo.Size = New System.Drawing.Size(321, 20)
        Me.teCtaActivo.TabIndex = 1
        '
        'teCtaDepreciacion
        '
        Me.teCtaDepreciacion.Enabled = False
        Me.teCtaDepreciacion.Location = New System.Drawing.Point(5, 83)
        Me.teCtaDepreciacion.Name = "teCtaDepreciacion"
        Me.teCtaDepreciacion.Properties.AllowFocused = False
        Me.teCtaDepreciacion.Properties.ReadOnly = True
        Me.teCtaDepreciacion.Size = New System.Drawing.Size(321, 20)
        Me.teCtaDepreciacion.TabIndex = 3
        '
        'teCtaGasto
        '
        Me.teCtaGasto.Enabled = False
        Me.teCtaGasto.Location = New System.Drawing.Point(5, 124)
        Me.teCtaGasto.Name = "teCtaGasto"
        Me.teCtaGasto.Properties.ReadOnly = True
        Me.teCtaGasto.Size = New System.Drawing.Size(321, 20)
        Me.teCtaGasto.TabIndex = 5
        '
        'GroupControl4
        '
        Me.GroupControl4.CaptionLocation = DevExpress.Utils.Locations.Top
        Me.GroupControl4.Controls.Add(Me.LabelControl19)
        Me.GroupControl4.Controls.Add(Me.beProveedor)
        Me.GroupControl4.Controls.Add(Me.TextEdit1)
        Me.GroupControl4.Controls.Add(Me.LabelControl20)
        Me.GroupControl4.Controls.Add(Me.seCostoAdquisicion)
        Me.GroupControl4.Controls.Add(Me.LabelControl21)
        Me.GroupControl4.Controls.Add(Me.teNoFactura)
        Me.GroupControl4.Controls.Add(Me.LabelControl22)
        Me.GroupControl4.Controls.Add(Me.teNumCheque)
        Me.GroupControl4.Controls.Add(Me.LabelControl23)
        Me.GroupControl4.Controls.Add(Me.deFechaAdquisicion)
        Me.GroupControl4.Location = New System.Drawing.Point(0, 188)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(823, 67)
        Me.GroupControl4.TabIndex = 57
        Me.GroupControl4.Text = "Datos de Adquisición"
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(67, 25)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl19.TabIndex = 0
        Me.LabelControl19.Text = "Proveedor:"
        '
        'beProveedor
        '
        Me.beProveedor.EnterMoveNextControl = True
        Me.beProveedor.Location = New System.Drawing.Point(125, 21)
        Me.beProveedor.Name = "beProveedor"
        Me.beProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beProveedor.Size = New System.Drawing.Size(90, 20)
        Me.beProveedor.TabIndex = 0
        '
        'TextEdit1
        '
        Me.TextEdit1.EnterMoveNextControl = True
        Me.TextEdit1.Location = New System.Drawing.Point(221, 21)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.ReadOnly = True
        Me.TextEdit1.Size = New System.Drawing.Size(392, 20)
        Me.TextEdit1.TabIndex = 14
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(18, 46)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(103, 13)
        Me.LabelControl20.TabIndex = 15
        Me.LabelControl20.Text = "Costo de Adquisición:"
        '
        'seCostoAdquisicion
        '
        Me.seCostoAdquisicion.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seCostoAdquisicion.EnterMoveNextControl = True
        Me.seCostoAdquisicion.Location = New System.Drawing.Point(125, 43)
        Me.seCostoAdquisicion.Name = "seCostoAdquisicion"
        Me.seCostoAdquisicion.Properties.Mask.EditMask = "n2"
        Me.seCostoAdquisicion.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seCostoAdquisicion.Size = New System.Drawing.Size(90, 20)
        Me.seCostoAdquisicion.TabIndex = 1
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(232, 46)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl21.TabIndex = 16
        Me.LabelControl21.Text = "No. de Factura:"
        '
        'teNoFactura
        '
        Me.teNoFactura.EnterMoveNextControl = True
        Me.teNoFactura.Location = New System.Drawing.Point(314, 43)
        Me.teNoFactura.Name = "teNoFactura"
        Me.teNoFactura.Size = New System.Drawing.Size(104, 20)
        Me.teNoFactura.TabIndex = 2
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(443, 46)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl22.TabIndex = 17
        Me.LabelControl22.Text = "No. de Cheque:"
        '
        'teNumCheque
        '
        Me.teNumCheque.EnterMoveNextControl = True
        Me.teNumCheque.Location = New System.Drawing.Point(523, 43)
        Me.teNumCheque.Name = "teNumCheque"
        Me.teNumCheque.Size = New System.Drawing.Size(90, 20)
        Me.teNumCheque.TabIndex = 3
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(630, 46)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(104, 13)
        Me.LabelControl23.TabIndex = 18
        Me.LabelControl23.Text = "Fecha de Adquisición:"
        '
        'deFechaAdquisicion
        '
        Me.deFechaAdquisicion.EditValue = Nothing
        Me.deFechaAdquisicion.EnterMoveNextControl = True
        Me.deFechaAdquisicion.Location = New System.Drawing.Point(738, 43)
        Me.deFechaAdquisicion.Name = "deFechaAdquisicion"
        Me.deFechaAdquisicion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaAdquisicion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaAdquisicion.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFechaAdquisicion.Size = New System.Drawing.Size(78, 20)
        Me.deFechaAdquisicion.TabIndex = 4
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.LabelControl32)
        Me.pcHeader.Controls.Add(Me.LabelControl3)
        Me.pcHeader.Controls.Add(Me.leClase)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.LabelControl4)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Controls.Add(Me.leUbicacion)
        Me.pcHeader.Controls.Add(Me.meDescripcion)
        Me.pcHeader.Controls.Add(Me.LabelControl5)
        Me.pcHeader.Controls.Add(Me.teCodigo)
        Me.pcHeader.Controls.Add(Me.leMarca)
        Me.pcHeader.Controls.Add(Me.teNombre)
        Me.pcHeader.Controls.Add(Me.LabelControl6)
        Me.pcHeader.Controls.Add(Me.LabelControl31)
        Me.pcHeader.Controls.Add(Me.leEstado)
        Me.pcHeader.Controls.Add(Me.seAnio)
        Me.pcHeader.Controls.Add(Me.LabelControl7)
        Me.pcHeader.Controls.Add(Me.LabelControl30)
        Me.pcHeader.Controls.Add(Me.leTiposDepreciacion)
        Me.pcHeader.Controls.Add(Me.teSerie)
        Me.pcHeader.Controls.Add(Me.LabelControl8)
        Me.pcHeader.Controls.Add(Me.LabelControl29)
        Me.pcHeader.Controls.Add(Me.leModelo)
        Me.pcHeader.Controls.Add(Me.teColor)
        Me.pcHeader.Controls.Add(Me.LabelControl28)
        Me.pcHeader.Controls.Add(Me.leEstilo)
        Me.pcHeader.Controls.Add(Me.LabelControl27)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(824, 186)
        Me.pcHeader.TabIndex = 54
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(526, 10)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(260, 20)
        Me.leSucursal.TabIndex = 0
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(479, 14)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl32.TabIndex = 47
        Me.LabelControl32.Text = "Sucursal:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(18, 101)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(110, 13)
        Me.LabelControl3.TabIndex = 25
        Me.LabelControl3.Text = "Tipo o Clase de Activo:"
        '
        'leClase
        '
        Me.leClase.EnterMoveNextControl = True
        Me.leClase.Location = New System.Drawing.Point(130, 96)
        Me.leClase.Name = "leClase"
        Me.leClase.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leClase.Size = New System.Drawing.Size(281, 20)
        Me.leClase.TabIndex = 5
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(23, 58)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(105, 13)
        Me.LabelControl2.TabIndex = 22
        Me.LabelControl2.Text = "Descripción detallada:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(71, 122)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl4.TabIndex = 27
        Me.LabelControl4.Text = "Ubicado en:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(41, 13)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl1.TabIndex = 20
        Me.LabelControl1.Text = "Código del Activo:"
        '
        'leUbicacion
        '
        Me.leUbicacion.EnterMoveNextControl = True
        Me.leUbicacion.Location = New System.Drawing.Point(130, 117)
        Me.leUbicacion.Name = "leUbicacion"
        Me.leUbicacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leUbicacion.Size = New System.Drawing.Size(281, 20)
        Me.leUbicacion.TabIndex = 7
        '
        'meDescripcion
        '
        Me.meDescripcion.EnterMoveNextControl = True
        Me.meDescripcion.Location = New System.Drawing.Point(130, 52)
        Me.meDescripcion.Name = "meDescripcion"
        Me.meDescripcion.Size = New System.Drawing.Size(656, 43)
        Me.meDescripcion.TabIndex = 4
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(95, 142)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl5.TabIndex = 30
        Me.LabelControl5.Text = "Marca:"
        '
        'teCodigo
        '
        Me.teCodigo.EnterMoveNextControl = True
        Me.teCodigo.Location = New System.Drawing.Point(130, 10)
        Me.teCodigo.Name = "teCodigo"
        Me.teCodigo.Size = New System.Drawing.Size(280, 20)
        Me.teCodigo.TabIndex = 1
        '
        'leMarca
        '
        Me.leMarca.EnterMoveNextControl = True
        Me.leMarca.Location = New System.Drawing.Point(130, 138)
        Me.leMarca.Name = "leMarca"
        Me.leMarca.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leMarca.Size = New System.Drawing.Size(281, 20)
        Me.leMarca.TabIndex = 9
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(130, 31)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(280, 20)
        Me.teNombre.TabIndex = 3
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(436, 101)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl6.TabIndex = 33
        Me.LabelControl6.Text = "Estado del Activo:"
        '
        'LabelControl31
        '
        Me.LabelControl31.Location = New System.Drawing.Point(40, 34)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(88, 13)
        Me.LabelControl31.TabIndex = 45
        Me.LabelControl31.Text = "Descripción Corta:"
        '
        'leEstado
        '
        Me.leEstado.EnterMoveNextControl = True
        Me.leEstado.Location = New System.Drawing.Point(526, 96)
        Me.leEstado.Name = "leEstado"
        Me.leEstado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leEstado.Size = New System.Drawing.Size(260, 20)
        Me.leEstado.TabIndex = 6
        '
        'seAnio
        '
        Me.seAnio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seAnio.EnterMoveNextControl = True
        Me.seAnio.Location = New System.Drawing.Point(719, 159)
        Me.seAnio.Name = "seAnio"
        Me.seAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seAnio.Properties.Mask.EditMask = "n0"
        Me.seAnio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seAnio.Size = New System.Drawing.Size(67, 20)
        Me.seAnio.TabIndex = 13
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(420, 122)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(103, 13)
        Me.LabelControl7.TabIndex = 36
        Me.LabelControl7.Text = "Tipo de Depreciación:"
        '
        'LabelControl30
        '
        Me.LabelControl30.Location = New System.Drawing.Point(692, 164)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(23, 13)
        Me.LabelControl30.TabIndex = 44
        Me.LabelControl30.Text = "Año:"
        '
        'leTiposDepreciacion
        '
        Me.leTiposDepreciacion.EnterMoveNextControl = True
        Me.leTiposDepreciacion.Location = New System.Drawing.Point(526, 117)
        Me.leTiposDepreciacion.Name = "leTiposDepreciacion"
        Me.leTiposDepreciacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTiposDepreciacion.Size = New System.Drawing.Size(260, 20)
        Me.leTiposDepreciacion.TabIndex = 8
        '
        'teSerie
        '
        Me.teSerie.EnterMoveNextControl = True
        Me.teSerie.Location = New System.Drawing.Point(526, 31)
        Me.teSerie.Name = "teSerie"
        Me.teSerie.Size = New System.Drawing.Size(260, 20)
        Me.teSerie.TabIndex = 2
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(485, 142)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl8.TabIndex = 37
        Me.LabelControl8.Text = "Modelo:"
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(495, 33)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl29.TabIndex = 43
        Me.LabelControl29.Text = "Serie:"
        '
        'leModelo
        '
        Me.leModelo.EnterMoveNextControl = True
        Me.leModelo.Location = New System.Drawing.Point(526, 138)
        Me.leModelo.Name = "leModelo"
        Me.leModelo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leModelo.Size = New System.Drawing.Size(260, 20)
        Me.leModelo.TabIndex = 10
        '
        'teColor
        '
        Me.teColor.EnterMoveNextControl = True
        Me.teColor.Location = New System.Drawing.Point(526, 159)
        Me.teColor.Name = "teColor"
        Me.teColor.Size = New System.Drawing.Size(136, 20)
        Me.teColor.TabIndex = 12
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(494, 164)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl28.TabIndex = 42
        Me.LabelControl28.Text = "Color:"
        '
        'leEstilo
        '
        Me.leEstilo.EnterMoveNextControl = True
        Me.leEstilo.Location = New System.Drawing.Point(130, 159)
        Me.leEstilo.Name = "leEstilo"
        Me.leEstilo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leEstilo.Size = New System.Drawing.Size(281, 20)
        Me.leEstilo.TabIndex = 11
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(99, 164)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl27.TabIndex = 41
        Me.LabelControl27.Text = "Estilo:"
        '
        'acf_frmActivos
        '
        Me.ClientSize = New System.Drawing.Size(830, 573)
        Me.Controls.Add(Me.xtcActivos)
        Me.Modulo = "Activo Fijo"
        Me.Name = "acf_frmActivos"
        Me.OptionId = "002001"
        Me.Text = "Activos Fijos"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.xtcActivos, 0)
        CType(Me.xtcActivos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcActivos.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.deVtoGarantia.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deVtoGarantia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beEmpleado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meInfo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teEmpleado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.seValorDepreciar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seVidaMeses.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seVidaAnios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seCuotaMensual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seCuotaAnual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deInicioDepreciacion.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deInicioDepreciacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seValorContable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceDepreciable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.beCtaActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCtaDepreciacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCtaGasto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCtaActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCtaDepreciacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCtaGasto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.beProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seCostoAdquisicion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNoFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaAdquisicion.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaAdquisicion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leClase.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leUbicacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leMarca.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTiposDepreciacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leModelo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teColor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leEstilo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents xtcActivos As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents sbVitacoraTraslados As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deVtoGarantia As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beEmpleado As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meInfo As DevExpress.XtraEditors.MemoExEdit
    Friend WithEvents teEmpleado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seValorDepreciar As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seVidaMeses As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seVidaAnios As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seCuotaMensual As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seCuotaAnual As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deInicioDepreciacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seValorContable As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents ceDepreciable As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCtaActivo As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCtaDepreciacion As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCtaGasto As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents teCtaActivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCtaDepreciacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCtaGasto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beProveedor As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seCostoAdquisicion As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNoFactura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumCheque As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaAdquisicion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leClase As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leUbicacion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents meDescripcion As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leMarca As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leEstado As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents seAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTiposDepreciacion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leModelo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teColor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leEstilo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit

End Class
