﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class acf_frmActivos
    Dim iId = 0
    Dim entActivo As acf_Activos
    Dim entDetalle As List(Of acf_DepreciacionesAjustes)
    Dim bl As New ActivosBusiness(g_ConnectionString)
    Dim blContable As New ContabilidadBLL(g_ConnectionString)
    Dim blTabla As New TableBusiness(g_ConnectionString)
    Dim entCuentas As New con_Cuentas
    Dim entEmpleados As New pla_Empleados
    

    Private Sub acf_frmActivos_Edit_Click() Handles Me.Editar
        'LayoutPrincipal.Enabled = True

        If entActivo.IdEstado = 7 Then
            MsgBox("Este activo esta retirado", MsgBoxStyle.Critical, "Imposible continuar")
            MostrarModoInicial()
            Exit Sub
        End If
        If entActivo.IdEstado = 8 Then
            MsgBox("Este activo esta vendido", MsgBoxStyle.Critical, "Imposible continuar")
            MostrarModoInicial()
            Exit Sub
        End If

        teCodigo.Focus()
        ActivarControles(True)
    End Sub

    Private Sub acf_frmActivos_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = bl.ConsultaActivos(piIdSucursalUsuario)
        gv2.BestFitColumns()
    End Sub

    Private Sub acf_frmActivos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargaCombos()
        iId = objFunciones.ObtenerUltimoId("ACF_ACTIVOS", "IdActivo")
        entActivo = objTablas.acf_ActivosSelectByPK(iId)
        CargaControles()
        ActivarControles(False)
        gc2.DataSource = bl.ConsultaActivos(piIdSucursalUsuario)
        'LayoutPrincipal.Enabled = False
    End Sub
    Private Sub acf_frmActivos_Nuevo_Click() Handles Me.Nuevo
        ActivarControles(True)
        CargaControles()  'se invoca para que limpie todos los controles con la entidad vacía

        entActivo = objTablas.acf_ActivosSelectByPK(0)
        CargaPantalla()
        DbMode = DbModeType.update
        teCodigo.Focus()

        entActivo = New acf_Activos
        DbMode = DbModeType.insert

        CargaCombos()
        deFechaAdquisicion.EditValue = Today
        teCtaActivo.EditValue = ""
        teCtaGasto.EditValue = ""
        teCtaDepreciacion.EditValue = ""
        TextEdit1.EditValue = ""
        teEmpleado.EditValue = ""
        leSucursal.EditValue = piIdSucursalUsuario
        leTiposDepreciacion.EditValue = 1
        leModelo.EditValue = 1
        leMarca.EditValue = 1
        leUbicacion.EditValue = 1
        leEstilo.EditValue = 1
        leEstado.EditValue = 1
        leClase.EditValue = 1
        deInicioDepreciacion.EditValue = Today
        'LayoutPrincipal.Enabled = True
        teCodigo.Focus()
        beEmpleado.EditValue = Nothing
        xtcActivos.SelectedTabPage = xtpDatos
    End Sub
    Private Sub acf_frmActivos_Save_Click() Handles Me.Guardar
        CargaEntidad()
        If Not DataEntryIsCorrect() Then
            Exit Sub
        End If
        Dim msj As String = ""
        If DbMode = DbModeType.insert Then

            If Not VerificaCierre() Then
                Exit Sub
            End If

            msj = bl.GuardarActivo(entActivo, entDetalle, True)
            If msj = "" Then
                MsgBox("Activo ingresado correctamente", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox("El activo no pudo ser guardado" + Chr(13) + msj, MsgBoxStyle.Critical, "Error DB")
                Exit Sub
            End If
        Else
            msj = bl.GuardarActivo(entActivo, entDetalle, False)
            If msj = "" Then
                MsgBox("El activo se ha actualizado correctamente", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox("No se pudo actualizar el activo" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If
        End If

        xtcActivos.SelectedTabPage = xtpLista
        gc2.DataSource = bl.ConsultaActivos(piIdSucursalUsuario)
        gc2.Focus()

        MostrarModoInicial()
        ActivarControles(False)
        'LayoutPrincipal.Enabled = False
    End Sub
    Function VerificaCierre() As Boolean
        Dim dt As New DataTable
        Dim MesCerrado As Integer
        Dim PeriodoCerrado As Integer

        dt = bl.ObtienePeriodoCerrado(leSucursal.EditValue)

        If dt.Rows.Count > 0 Then
            MesCerrado = SiEsNulo(dt.Rows(0).Item(0), Now.Month)
            PeriodoCerrado = SiEsNulo(dt.Rows(0).Item(1), Now.Year)

            Dim dFechaFinal As DateTime
            dFechaFinal = CDate(PeriodoCerrado & "/" & (MesCerrado).ToString.PadLeft(2, "0") & "/01")
            dFechaFinal = DateAdd(DateInterval.Month, 1, dFechaFinal)
            dFechaFinal = DateAdd(DateInterval.Day, -1, dFechaFinal)


            If deInicioDepreciacion.EditValue < dFechaFinal Then
                MsgBox("La Fecha de Inicial de depreciación es menor al último mes contabilizado", MsgBoxStyle.Critical, "Error de usuario")
                Return False
            End If

        End If

        Return True

    End Function
    Private Sub acf_frmActivos_Consulta_Click() Handles Me.Consulta
        'entActivo = objConsultas.cnsActivos(frmConsultas, 0)
        'If entActivo.IdActivo > 0 Then
        '    CargaControles()  'carga todos los controles con la entidad llena retornada desde la consulta
        'End If
    End Sub

    Private Sub CargaControles()
        'With entActivo
        '    teCodigo.EditValue = .Codigo
        '    teNombre.EditValue = .Nombre
        '    meDescripcion.EditValue = .Descripcion
        '    leClase.EditValue = .IdClase
        '    leMarca.EditValue = .IdMarca
        '    leModelo.EditValue = .IdModelo
        '    leEstado.EditValue = .IdEstado
        '    teColor.EditValue = .Color
        '    beProveedor.EditValue = .IdProveedor
        '    beProveedor_Validated(beProveedor, New EventArgs)
        '    teNoFactura.EditValue = .NumCompra
        '    teNumCheque.EditValue = .NumCheque
        '    ceDepreciable.EditValue = .Depreciable
        '    deFechaAdquisicion.EditValue = .FechaAdquisicion
        '    seCostoAdquisicion.EditValue = .ValorAdquisicion
        '    seValorDepreciar.EditValue = .ValorDepreciar
        '    deInicioDepreciacion.EditValue = .FechaInicio
        '    seValorContable.EditValue = .ValorContable
        '    seCuotaAnual.EditValue = .CuotaAnual
        '    seCuotaMensual.EditValue = .CuotaMensual
        '    seVidaMeses.EditValue = .MesesDepreciacion
        '    seVidaAnios.EditValue = .MesesDepreciacion / 12
        '    beCtaActivo.EditValue = .IdCuentaActivo
        '    beCtaActivo_Validated(beCtaActivo, New EventArgs)
        '    beCtaDepreciacion.EditValue = .IdCuentaDepreciacion
        '    beCtaDepreciacion_Validated(beCtaDepreciacion, New EventArgs)
        '    beCtaGasto.EditValue = .IdCuentaGasto
        '    beCtaGasto_Validated(beCtaGasto, New EventArgs)
        '    beEmpleado.EditValue = .IdEmpleado
        '    beIdEmpleado_Validated(beEmpleado, New EventArgs)
        '    deVtoGarantia.EditValue = .VectoGarantia
        '    meInfo.EditValue = .InfoAdicional
        '    teSerie.EditValue = .Serie
        '    seAnio.EditValue = .Anio
        'End With
    End Sub

    Sub CargaEntidad()
        With entActivo
            .Codigo = teCodigo.EditValue
            .Nombre = teNombre.EditValue
            .Descripcion = meDescripcion.EditValue
            .FechaAdquisicion = deFechaAdquisicion.EditValue
            .IdMarca = leMarca.EditValue
            .IdModelo = leModelo.EditValue
            .IdEstilo = leEstilo.EditValue
            .Color = teColor.EditValue
            .Serie = teSerie.EditValue
            .Anio = seAnio.EditValue
            .NumCompra = teNoFactura.EditValue
            .NumCheque = teNumCheque.EditValue
            .Depreciable = ceDepreciable.EditValue
            .ValorAdquisicion = seCostoAdquisicion.EditValue
            .ValorDepreciar = seValorDepreciar.EditValue
            .ValorContable = seValorContable.EditValue
            .MesesDepreciacion = seVidaMeses.EditValue
            .CuotaMensual = seCuotaMensual.EditValue
            .CuotaMensual = Decimal.Round(seCuotaMensual.EditValue, 2)
            .CuotaAnual = seCuotaAnual.EditValue
            .FechaInicio = deInicioDepreciacion.EditValue
            .IdTipoDepreciacion = leTiposDepreciacion.EditValue
            .ValorRetiro = 0
            .IdClase = leClase.EditValue
            .IdProveedor = beProveedor.EditValue
            .IdEmpleado = beEmpleado.EditValue
            .IdUbicacion = leUbicacion.EditValue
            .IdCuentaActivo = beCtaActivo.EditValue
            .IdCuentaDepreciacion = beCtaDepreciacion.EditValue
            .IdCuentaGasto = beCtaGasto.EditValue
            .IdEstado = leEstado.EditValue
            .IdMotivoBaja = Nothing
            .InfoAdicional = meInfo.EditValue
            .VectoGarantia = deVtoGarantia.EditValue
            .IdSucursal = leSucursal.EditValue

            If DbMode = DbModeType.insert Then
                .FechaHoraCreacion = Now
                .CreadoPor = objMenu.User
                .ModificadoPor = ""
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If

            entDetalle = New List(Of acf_DepreciacionesAjustes)

            If Not ceDepreciable.EditValue Then 'el activo no genera depreciacion
                Dim entDep As New acf_DepreciacionesAjustes
                With entDep
                    .NumCuota = 1
                    .Fecha = deFechaAdquisicion.EditValue
                    .Valor = 0
                    .IdActivo = entActivo.IdActivo
                    .IdTipo = 0
                End With
                entDetalle.Add(entDep)

                Exit Sub
            End If


            Dim dFechaInicial As DateTime
            dFechaInicial = CDate(CStr(deInicioDepreciacion.EditValue.ToString.Substring(6, 4)) & "/" & (deInicioDepreciacion.EditValue.ToString.Substring(3, 2)).ToString.PadLeft(2, "0") & "/01")
            dFechaInicial = DateAdd(DateInterval.Month, 1, dFechaInicial)
            dFechaInicial = DateAdd(DateInterval.Day, -1, dFechaInicial)
            Dim dTotal As Decimal = seValorDepreciar.EditValue



            For i = 1 To .MesesDepreciacion
                Dim entDep As New acf_DepreciacionesAjustes
                With entDep
                    .NumCuota = i

                    If i <> 1 Then
                        Dim FechaInicioMes_ As String = "01/" + DateAdd(DateInterval.Month, 1, dFechaInicial).Month.ToString.PadLeft(2, "0") + "/" + DateAdd(DateInterval.Month, 1, dFechaInicial).Year.ToString.PadLeft(4, "0")
                        Dim FecIniMes As Date = CDate(FechaInicioMes_)
                        FecIniMes = DateAdd(DateInterval.Month, 1, FecIniMes)
                        FecIniMes = DateAdd(DateInterval.Day, -1, FecIniMes)
                        .Fecha = FecIniMes
                        dFechaInicial = FecIniMes
                    Else
                        .Fecha = dFechaInicial
                    End If

                    If entActivo.MesesDepreciacion = i Then
                        .Valor = dTotal
                    Else
                        .Valor = entActivo.CuotaMensual
                    End If

                    .IdActivo = entActivo.IdActivo
                    .IdTipo = 1  '1 es depreciacion
                    dTotal -= entActivo.CuotaMensual
                End With
                entDetalle.Add(entDep)
            Next

        End With
    End Sub
    Function DataEntryIsCorrect() As Boolean

        If ceDepreciable.EditValue = True Then
            If SiEsNulo(beCtaActivo.EditValue, "") = "" Or SiEsNulo(beCtaDepreciacion.EditValue, "") = "" Or SiEsNulo(beCtaGasto.EditValue, "") = "" Then
                MsgBox("Debe de especificar todas las cuentas contables del activo", MsgBoxStyle.Critical, "Error de usuario")
                Return False
            End If

            If seVidaMeses.EditValue <= 0 Then
                MsgBox("Debe de especificar la vida útil", MsgBoxStyle.Critical, "Error de usuario")
                Return False
            End If

            If ceDepreciable.EditValue = True Then
                If seValorDepreciar.EditValue <= 0 Then
                    MsgBox("Debe de especificar al valor a depreciar", MsgBoxStyle.Critical, "Error de usuario")
                    Return False
                End If
            End If

            If seCostoAdquisicion.EditValue = Nothing Then
                MsgBox("Debe de especificar el Costo de Adquisicion del Activo Fijo", MsgBoxStyle.Critical, "Error de usuario")
                Return False
            End If

            If seCuotaMensual.EditValue <= 0 Then
                MsgBox("Debe de especificar la Cuota Mensual del Activo Fijo", MsgBoxStyle.Critical, "Error de usuario")
                Return False
            End If
        End If





        If deInicioDepreciacion.EditValue Is Nothing Then
            MsgBox("Debe de especificar la fecha inicial de depreciación", MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If

        If deVtoGarantia.EditValue Is Nothing Then
            MsgBox("Debe de especificar el vencimiento de la garantía", MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If
        If teCodigo.EditValue = "" Then
            MsgBox("Debe de especificar el codigo del Activo Fijo", MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If
        If teNombre.EditValue = "" Then
            MsgBox("Debe de especificar el Nombre del Activo Fijo", MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If
        If leClase.Text = "" Then
            MsgBox("Debe de especificar la Clasificación del Activo Fijo", MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If
        If beProveedor.EditValue = "" Then
            MsgBox("Debe de especificar el Proveedor del Activo Fijo", MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If


        If beEmpleado.EditValue = Nothing Or beEmpleado.EditValue = 0 Then
            MsgBox("Debe de especificar el Responsable del Activo Fijo", MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If

        If DbMode = DbModeType.insert Then
            If bl.SiExisteActivo(teCodigo.EditValue) Then
                MsgBox("Ya existe un activo con este código. Imposible continuar", MsgBoxStyle.Critical, "Nota")
                Return False
            End If
        End If

        Return True
    End Function
    Sub seCostoAdquisicion_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles seCostoAdquisicion.EditValueChanged
        seValorDepreciar.EditValue = seCostoAdquisicion.EditValue
        CalculaDepreciacion()
    End Sub

    Private Sub seValorDepreciar_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles seValorDepreciar.Validated
        CalculaDepreciacion()
    End Sub

    Sub CalculaDepreciacion()
        If seVidaMeses.EditValue > 0 Then
            seCuotaMensual.EditValue = seValorDepreciar.EditValue / seVidaMeses.EditValue
        End If
        seVidaAnios.EditValue = seVidaMeses.EditValue / 12
        seCuotaAnual.EditValue = seCuotaMensual.EditValue * 12
    End Sub
    Private Sub CargaCombos()
        objCombos.acf_Clases(leClase, "")
        objCombos.acf_Estados(leEstado, "")
        objCombos.acf_Estilos(leEstilo, "")
        objCombos.acf_Marcas(leMarca, "")
        objCombos.acf_Modelos(leModelo, "")
        objCombos.acf_TiposDepreciacion(leTiposDepreciacion, "")
        objCombos.acf_Ubicacion(leUbicacion, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
    End Sub

    Private Sub acf_frmActivos_Undo_Click() Handles Me.Revertir
        'LayoutPrincipal.Enabled = False
        xtcActivos.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub

    Private Sub beCtaActivo_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCtaActivo.ButtonClick
        If teNombre.Properties.ReadOnly = False Then
            beCtaActivo.EditValue = ""
            entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCtaActivo.EditValue)
            beCtaActivo.EditValue = entCuentas.IdCuenta
            teCtaActivo.EditValue = entCuentas.Nombre
        End If
    End Sub

    Private Sub beCtaActivo_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCtaActivo.EditValueChanged
        If beCtaActivo.EditValue <> Nothing Then
            entCuentas = blTabla.con_CuentasSelectByPK(beCtaActivo.EditValue)
            beCtaActivo.EditValue = entCuentas.IdCuenta
            teCtaActivo.EditValue = entCuentas.Nombre
        End If
    End Sub

    Private Sub beCtaActivo_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCtaActivo.Validated
        entCuentas = blTabla.con_CuentasSelectByPK(beCtaActivo.EditValue)
        beCtaActivo.EditValue = entCuentas.IdCuenta
        teCtaActivo.EditValue = entCuentas.Nombre
    End Sub

    Private Sub beCtaDepreciacion_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCtaDepreciacion.ButtonClick
        If teNombre.Properties.ReadOnly = False Then
            beCtaDepreciacion.EditValue = ""
            entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCtaDepreciacion.EditValue)
            beCtaDepreciacion.EditValue = entCuentas.IdCuenta
            teCtaDepreciacion.EditValue = entCuentas.Nombre
        End If
    End Sub

    Private Sub beCtaDepreciacion_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCtaDepreciacion.EditValueChanged
        If beCtaDepreciacion.EditValue <> Nothing Then
            entCuentas = blTabla.con_CuentasSelectByPK(beCtaDepreciacion.EditValue)
            beCtaDepreciacion.EditValue = entCuentas.IdCuenta
            teCtaDepreciacion.EditValue = entCuentas.Nombre
        End If
    End Sub

    Private Sub beCtaDepreciacion_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCtaDepreciacion.EditValueChanged
        entCuentas = blTabla.con_CuentasSelectByPK(beCtaDepreciacion.EditValue)
        beCtaDepreciacion.EditValue = entCuentas.IdCuenta
        teCtaDepreciacion.EditValue = entCuentas.Nombre
    End Sub

    Private Sub beCtaGasto_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCtaGasto.ButtonClick
        If teNombre.Properties.ReadOnly = False Then
            beCtaGasto.EditValue = ""
            entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCtaGasto.EditValue)
            beCtaGasto.EditValue = entCuentas.IdCuenta
            teCtaGasto.EditValue = entCuentas.Nombre
        End If
    End Sub

    Private Sub beCtaGasto_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCtaGasto.EditValueChanged
        If beCtaGasto.EditValue <> Nothing Then
            entCuentas = blTabla.con_CuentasSelectByPK(beCtaGasto.EditValue)
            beCtaGasto.EditValue = entCuentas.IdCuenta
            teCtaGasto.EditValue = entCuentas.Nombre
        End If
    End Sub

    Private Sub beCtaGasto_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCtaGasto.Validated
        entCuentas = blTabla.con_CuentasSelectByPK(beCtaGasto.EditValue)
        beCtaGasto.EditValue = entCuentas.IdCuenta
        teCtaGasto.EditValue = entCuentas.Nombre
    End Sub

    Private Sub seVidaMeses_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles seVidaMeses.EditValueChanged
        If seVidaMeses.EditValue > 0.0 Then
            seVidaAnios.EditValue = Int(Decimal.Round(seVidaMeses.EditValue / 12, 0))
        Else
            seVidaAnios.EditValue = 0
        End If
        CalculaDepreciacion()
    End Sub

    Private Sub beEmpleado_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beEmpleado.ButtonClick
        If beEmpleado.Properties.ReadOnly = False Then
            beEmpleado.EditValue = 0
            beEmpleado.EditValue = objConsultas.cnsEmpleados(frmConsultas)
        End If
    End Sub

    Private Sub beEmpleado_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles beEmpleado.EditValueChanged
        If beEmpleado.EditValue <> Nothing Then
            beIdEmpleado_Validated(beEmpleado.EditValue, New EventArgs)
        End If
    End Sub

    Private Sub beIdEmpleado_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beEmpleado.Validated
        If beEmpleado.EditValue = Nothing Then

        Else
            entEmpleados = blTabla.pla_EmpleadosSelectByPK(beEmpleado.EditValue)
            With entEmpleados
                beEmpleado.EditValue = .IdEmpleado
                teEmpleado.EditValue = .Nombres & " " & .Apellidos
            End With
        End If
    End Sub



    Private Sub deFechaAdquisicion_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles deFechaAdquisicion.Validated
        deInicioDepreciacion.EditValue = deFechaAdquisicion.EditValue
    End Sub

    Private Sub beProveedor_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beProveedor.ButtonClick
        beProveedor.EditValue = Nothing
        Dim entProveedor As com_Proveedores = objConsultas.cnsProveedores(frmConsultas, beProveedor.EditValue)
        beProveedor.EditValue = entProveedor.IdProveedor
        TextEdit1.EditValue = entProveedor.Nombre
        beProveedor_Validated(sender, New System.EventArgs)
    End Sub

    Private Sub beProveedor_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles beProveedor.EditValueChanged
        If beProveedor.EditValue <> Nothing Then
            Dim entProveedor As com_Proveedores = blTabla.com_ProveedoresSelectByPK(beProveedor.EditValue)
            beProveedor.EditValue = entProveedor.IdProveedor
            TextEdit1.EditValue = entProveedor.Nombre
        End If
    End Sub

    Private Sub beProveedor_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beProveedor.Validated
        If beProveedor.EditValue = Nothing Then

        Else
            Dim entProveedor As com_Proveedores = blTabla.com_ProveedoresSelectByPK(beProveedor.EditValue)
            beProveedor.EditValue = entProveedor.IdProveedor
            TextEdit1.EditValue = entProveedor.Nombre
        End If
    End Sub

    Private Sub leClase_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles leClase.Validated
        Dim entClase As New acf_Clases
        entClase = blTabla.acf_ClasesSelectByPK(leClase.EditValue)
        If leClase.Properties.ReadOnly = False Then
            If entClase.Depreciable = True Then
                ceDepreciable.EditValue = entClase.Depreciable
            Else
                ceDepreciable.EditValue = entClase.Depreciable
            End If

            If MsgBox("Desea Cargar las Cuentas Contables que corresponde a esta Clasificación", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Imprimir") = MsgBoxResult.Yes Then
                Dim entCuentas As con_Cuentas
                beCtaActivo.EditValue = entClase.IdCtaActivo
                entCuentas = blTabla.con_CuentasSelectByPK(beCtaActivo.EditValue)
                teCtaActivo.EditValue = entCuentas.Nombre
                beCtaGasto.EditValue = entClase.IdCtaGasto
                entCuentas = blTabla.con_CuentasSelectByPK(beCtaGasto.EditValue)
                teCtaGasto.EditValue = entCuentas.Nombre
                beCtaDepreciacion.EditValue = entClase.IdCtaDepreciacion
                entCuentas = blTabla.con_CuentasSelectByPK(beCtaDepreciacion.EditValue)
                teCtaDepreciacion.EditValue = entCuentas.Nombre
            End If
        End If
    End Sub

    Private Sub acf_frmActivos_Eliminar() Handles Me.Eliminar

        Dim IdActivo As Integer
        IdActivo = bl.SiExisteActivo(gv2.GetRowCellValue(gv2.FocusedRowHandle, "Codigo"))
        entActivo = objTablas.acf_ActivosSelectByPK(IdActivo)

        If IdActivo > 0 Then

            If entActivo.IdEstado = 7 Then
                MsgBox("Este activo esta retirado", MsgBoxStyle.Critical, "Imposible continuar")
                MostrarModoInicial()
                Exit Sub
            End If
            If entActivo.IdEstado = 8 Then
                MsgBox("Este activo esta vendido", MsgBoxStyle.Critical, "Imposible continuar")
                MostrarModoInicial()
                Exit Sub
            End If

            If MsgBox("Está seguro(a) de eliminar el Activo Fijo?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
                Dim msj As String = ""
                Try
                    entActivo.ModificadoPor = objMenu.User
                    objTablas.acf_ActivosUpdate(entActivo)

                    objTablas.acf_ActivosDeleteByPK(IdActivo)
                Catch ex As Exception
                    msj = ex.Message()
                End Try
                If msj = "" Then
                    MsgBox("El Activo Fijo ha sido eliminado con éxito", MsgBoxStyle.Information)
                    'Close()
                Else
                    MsgBox("No fue posible eliminar el Activo Fijo" + Chr(13) + msj, MsgBoxStyle.Critical, "Error al elminar el registro")
                End If

                xtcActivos.SelectedTabPage = xtpLista
                gc2.DataSource = bl.ConsultaActivos(piIdSucursalUsuario)
                gc2.Focus()

            End If
        End If
    End Sub

    Private Sub acf_frmActivos_Reporte() Handles Me.Reporte
        'Dim IdActivo As Integer = bl.SiExisteActivo(teCodigo.EditValue)
        entActivo = objTablas.acf_ActivosSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdActivo"))
        Dim dt As DataTable = bl.ImprimeActivoFijo(entActivo.IdActivo)
        Dim rpt As New acf_rptImprimeActivoFijo() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.ShowPreviewDialog()
    End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        For Each ctrl In GroupControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        For Each ctrl In GroupControl3.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        For Each ctrl In GroupControl4.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        For Each ctrl In GroupControl5.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
    End Sub


    Private Sub sbVitacoraTraslados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbVitacoraTraslados.Click

        If teCodigo.Properties.ReadOnly = False Then
            Exit Sub
        End If

        Dim frmHistorial As New acf_frmHistoricoTraslados
        frmHistorial.IdActivo = entActivo.IdActivo
        frmHistorial.Text = "Historial de Traslados de Activo: " + teNombre.EditValue
        frmHistorial.ShowDialog()

    End Sub

    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        entActivo = objTablas.acf_ActivosSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdActivo"))
        CargaPantalla()
        DbMode = DbModeType.update
        teCodigo.Focus()
    End Sub
    Private Sub CargaPantalla()
        xtcActivos.SelectedTabPage = xtpDatos
        teCodigo.Focus()

        With entActivo
            teCodigo.EditValue = .Codigo
            teNombre.EditValue = .Nombre
            meDescripcion.EditValue = .Descripcion
            leClase.EditValue = .IdClase
            leMarca.EditValue = .IdMarca
            leModelo.EditValue = .IdModelo
            leEstado.EditValue = .IdEstado
            teColor.EditValue = .Color
            beProveedor.EditValue = .IdProveedor
            beProveedor_Validated(beProveedor, New EventArgs)
            teNoFactura.EditValue = .NumCompra
            teNumCheque.EditValue = .NumCheque
            ceDepreciable.EditValue = .Depreciable
            deFechaAdquisicion.EditValue = .FechaAdquisicion
            seCostoAdquisicion.EditValue = .ValorAdquisicion
            seValorDepreciar.EditValue = .ValorDepreciar
            deInicioDepreciacion.EditValue = .FechaInicio
            seValorContable.EditValue = .ValorContable
            seCuotaAnual.EditValue = .CuotaAnual
            seCuotaMensual.EditValue = .CuotaMensual
            seVidaMeses.EditValue = .MesesDepreciacion
            seVidaAnios.EditValue = .MesesDepreciacion / 12
            beCtaActivo.EditValue = .IdCuentaActivo
            beCtaActivo_Validated(beCtaActivo, New EventArgs)
            beCtaDepreciacion.EditValue = .IdCuentaDepreciacion
            beCtaDepreciacion_Validated(beCtaDepreciacion, New EventArgs)
            beCtaGasto.EditValue = .IdCuentaGasto
            beCtaGasto_Validated(beCtaGasto, New EventArgs)
            beEmpleado.EditValue = .IdEmpleado
            beIdEmpleado_Validated(beEmpleado, New EventArgs)
            deVtoGarantia.EditValue = .VectoGarantia
            meInfo.EditValue = .InfoAdicional
            teSerie.EditValue = .Serie
            seAnio.EditValue = .Anio
            leSucursal.EditValue = .IdSucursal
            leUbicacion.EditValue = .IdUbicacion
            leTiposDepreciacion.EditValue = .IdTipoDepreciacion
            leEstilo.EditValue = .IdEstilo
        End With
    End Sub


End Class
