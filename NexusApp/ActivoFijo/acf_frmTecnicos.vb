﻿Imports NexusELL.TableEntities
Public Class acf_frmTecnicos
    Dim entTecnico As acf_Tecnicos

    Private Sub acf_frmTecnicos_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub acfTecnicos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.acf_TecnicosSelectAll
        entTecnico = objTablas.acf_TecnicosSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTecnico"))
        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub acfTecnicos_Nuevo_Click() Handles Me.Nuevo
        entTecnico = New acf_Tecnicos
        entTecnico.IdTecnico = objFunciones.ObtenerUltimoId("ACF_TECNICOS", "IdTecnico") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub acfTecnicos_Save_Click() Handles Me.Guardar
        Dim msj As String = ""
        If teNombre.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Nombre]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        CargaEntidad()
        If DbMode = DbModeType.insert Then
            Try
                objTablas.acf_TecnicosInsert(entTecnico)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If msj = "" Then
                MsgBox("El Tecnico ha sido guardado con éxito", MsgBoxStyle.Information)
                CargaPantalla()
            Else
                MsgBox(String.Format("No fue posible Guardar el Tecnico{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error al Guardar el registro")
            End If
        Else
            Try
                objTablas.acf_TecnicosUpdate(entTecnico)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If msj = "" Then
                MsgBox("El Tecnico ha sido actualizado con éxito", MsgBoxStyle.Information)
                CargaPantalla()
            Else
                MsgBox(String.Format("No fue posible actualizar el Tecnico{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error al Guardar el registro")
            End If
        End If
        gc.DataSource = objTablas.acf_TecnicosSelectAll
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub acf_frmTecnicos_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el Tecnico seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Dim msj As String = ""
            Try
                objTablas.acf_TecnicosDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If msj = "" Then
                MsgBox("El Tecnico ha sido eliminado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("No fue posible eliminar el Tecnico" + Chr(13) + msj, MsgBoxStyle.Critical, "Error al elminar el registro")
            End If
            gc.DataSource = objTablas.acf_TecnicosSelectAll
        End If
    End Sub
    Private Sub acf_frmTecnicos_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entTecnico
            teIdTecnico.EditValue = .IdTecnico
            teNombre.EditValue = .Nombre
            teEmpresa.EditValue = .Empresa
            teTelefono.EditValue = .Telefono
            teCorreo.EditValue = .Email
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entTecnico
            .IdTecnico = teIdTecnico.EditValue
            .Nombre = teNombre.EditValue
            .Empresa = teEmpresa.EditValue
            .Telefono = teTelefono.EditValue
            .Email = teCorreo.EditValue
        End With
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entTecnico = objTablas.acf_TecnicosSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTecnico"))
        CargaPantalla()
    End Sub

    Private Sub acf_Tecnicos_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teIdTecnico.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub

End Class
