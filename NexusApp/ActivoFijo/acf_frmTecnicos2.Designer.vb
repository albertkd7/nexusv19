﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class acf_frmTecnicos2
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcId = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcEmpresa = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcTelefono = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcEmail = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(853, 363)
        Me.gc.TabIndex = 4
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcId, Me.gcNombre, Me.gcEmpresa, Me.gcTelefono, Me.gcEmail})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcId
        '
        Me.gcId.Caption = "Id. Técnico"
        Me.gcId.FieldName = "IdTecnico"
        Me.gcId.Name = "gcId"
        Me.gcId.Visible = True
        Me.gcId.VisibleIndex = 0
        Me.gcId.Width = 100
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 1
        Me.gcNombre.Width = 255
        '
        'gcEmpresa
        '
        Me.gcEmpresa.Caption = "Empresa Responsable"
        Me.gcEmpresa.FieldName = "Empresa"
        Me.gcEmpresa.Name = "gcEmpresa"
        Me.gcEmpresa.Visible = True
        Me.gcEmpresa.VisibleIndex = 2
        Me.gcEmpresa.Width = 204
        '
        'gcTelefono
        '
        Me.gcTelefono.Caption = "Teléfono"
        Me.gcTelefono.FieldName = "Telefono"
        Me.gcTelefono.Name = "gcTelefono"
        Me.gcTelefono.Visible = True
        Me.gcTelefono.VisibleIndex = 3
        Me.gcTelefono.Width = 98
        '
        'gcEmail
        '
        Me.gcEmail.Caption = "E-Mail"
        Me.gcEmail.FieldName = "Email"
        Me.gcEmail.Name = "gcEmail"
        Me.gcEmail.Visible = True
        Me.gcEmail.VisibleIndex = 4
        Me.gcEmail.Width = 175
        '
        'acf_frmTecnicos
        '
        Me.ClientSize = New System.Drawing.Size(865, 388)
        Me.Controls.Add(Me.gc)
        Me.DbMode = 1
        Me.Modulo = "Activo Fijo"
        Me.Name = "acf_frmTecnicos"
        Me.OptionId = "001007"
        Me.Text = "Técnicos"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcEmpresa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTelefono As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcEmail As DevExpress.XtraGrid.Columns.GridColumn

End Class
