﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class acf_frmHistoricoTraslados
    Dim bl As New ActivosBusiness(g_ConnectionString)

    Private _IdActivo As Integer
    Public Property IdActivo() As Integer
        Get
            Return _IdActivo
        End Get
        Set(ByVal value As Integer)
            _IdActivo = value
        End Set
    End Property

    Private Sub com_frmDetallarGastosImportaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Llenar()
    End Sub

    Private Sub sbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGuardar.Click
        Me.Close()
    End Sub

    Private Sub Llenar()
        gc.DataSource = bl.HistorialTraslado(IdActivo)
    End Sub
End Class
