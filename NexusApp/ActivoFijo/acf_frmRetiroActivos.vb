﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class acf_frmRetiroActivos
    Dim blActivos As New ActivosBusiness(g_ConnectionString)
    Dim blConta As New ContabilidadBLL(g_ConnectionString)
    Dim fb As New FuncionesBLL(g_ConnectionString)
    Dim entCuentas As New con_Cuentas
    Dim entPartida As New con_Partidas
    Dim entDetalle As New List(Of con_PartidasDetalle)


    Private Sub RetiroActivos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFechaRetiro.EditValue = Today
        objCombos.acf_Clases(leClase, "")
        objCombos.acf_Motivos(leMotivo, "")
        objCombos.conTiposPartida(leTipoPartida)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        DbMode = DbModeType.insert

    End Sub

    Private Sub leClase_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leClase.EditValueChanged
        If deFechaRetiro.EditValue = Nothing Then
            gcActivos.DataSource = blActivos.GetActivos(leClase.EditValue, Today, leSucursal.EditValue)
        Else
            gcActivos.DataSource = blActivos.GetActivos(leClase.EditValue, deFechaRetiro.EditValue, leSucursal.EditValue)
        End If

        gvActivos.BestFitColumns()
    End Sub

    Private Sub ClearScreen()
        txtDescripcion.EditValue = ""
        deFechaRetiro.EditValue = Today
        beCtaActivo.EditValue = ""
    End Sub

    Private Sub beCtaActivo_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCtaActivo.ButtonClick
        beCtaActivo.EditValue = ""
        entCuentas = objConsultas.cnsCuentas(frmConsultas, beCtaActivo.EditValue)
        beCtaActivo.EditValue = entCuentas.IdCuenta
        teCtaGasto.EditValue = entCuentas.Nombre
    End Sub

    Private Sub beCtaActivo_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCtaActivo.EditValueChanged
        If beCtaActivo.EditValue <> Nothing Then
            entCuentas = objTablas.con_CuentasSelectByPK(beCtaActivo.EditValue)
            beCtaActivo.EditValue = entCuentas.IdCuenta
            teCtaGasto.EditValue = entCuentas.Nombre
        End If
    End Sub

    Private Sub beCtaActivo_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCtaActivo.Validated
        entCuentas = objTablas.con_CuentasSelectByPK(beCtaActivo.EditValue)
        beCtaActivo.EditValue = entCuentas.IdCuenta
        teCtaGasto.EditValue = entCuentas.Nombre
    End Sub

    Private Sub deFechaRetiro_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles deFechaRetiro.EditValueChanged
        If deFechaRetiro.EditValue = Nothing Then
            gcActivos.DataSource = blActivos.GetActivos(leClase.EditValue, Today, leSucursal.EditValue)
        Else
            gcActivos.DataSource = blActivos.GetActivos(leClase.EditValue, deFechaRetiro.EditValue, leSucursal.EditValue)
        End If

        gvActivos.BestFitColumns()
    End Sub


    Private Sub cmdImprimir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdImprimir.Click
        Dim dtReporte As New DataTable
        dtReporte.Columns.Add("IdActivo").DataType = System.Type.GetType("System.Int32")
        dtReporte.Columns.Add("Codigo").DataType = System.Type.GetType("System.String")
        dtReporte.Columns.Add("Nombre").DataType = System.Type.GetType("System.String")
        dtReporte.Columns.Add("FechaAdquisicion").DataType = System.Type.GetType("System.DateTime")
        dtReporte.Columns.Add("ValorAdquisicion").DataType = System.Type.GetType("System.Decimal")
        dtReporte.Columns.Add("ValorContable").DataType = System.Type.GetType("System.Decimal")
        dtReporte.Columns.Add("ValorDepreciar").DataType = System.Type.GetType("System.Decimal")
        dtReporte.Columns.Add("DepreciacionAcumulada").DataType = System.Type.GetType("System.Decimal")
        dtReporte.Columns.Add("Perdida").DataType = System.Type.GetType("System.Decimal")
        dtReporte.Columns.Add("Retira").DataType = System.Type.GetType("System.Boolean")

        Dim workRow As DataRow

        For i = 0 To gvActivos.DataRowCount - 1
            If gvActivos.GetRowCellValue(i, "Retira") = True Then
                workRow = dtReporte.NewRow()
                workRow("IdActivo") = gvActivos.GetRowCellValue(i, "IdActivo")
                workRow("Codigo") = gvActivos.GetRowCellValue(i, "Codigo")
                workRow("Nombre") = gvActivos.GetRowCellValue(i, "Nombre")
                workRow("FechaAdquisicion") = gvActivos.GetRowCellValue(i, "FechaAdquisicion")
                workRow("ValorAdquisicion") = gvActivos.GetRowCellValue(i, "ValorAdquisicion")
                workRow("ValorContable") = gvActivos.GetRowCellValue(i, "ValorContable")
                workRow("ValorDepreciar") = gvActivos.GetRowCellValue(i, "ValorDepreciar")
                workRow("DepreciacionAcumulada") = gvActivos.GetRowCellValue(i, "DepreciacionAcumulada")
                workRow("Perdida") = gvActivos.GetRowCellValue(i, "Perdida")
                workRow("Retira") = gvActivos.GetRowCellValue(i, "Retira")
                dtReporte.Rows.Add(workRow)
            End If
        Next
        Dim rpt As New acf_rptListActivosRetirados
        rpt.DataSource = dtReporte
        rpt.DataMember = ""
        rpt.xrlTitulo.Text = "LISTADO DE ACTIVOS RETIRADOS AL " & FechaToString(deFechaRetiro.EditValue, deFechaRetiro.EditValue)
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub sbSalir_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Close()
    End Sub


    Private Sub sbContabilizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles sbContabilizar.Click
        Dim msj As String = ""
        If leTipoPartida.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Tipo de Partida]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        Dim EsOk As Boolean = ValidarFechaCierre(deFechaRetiro.EditValue)
        If Not EsOk Then
            msj = "La fecha de Retiro no corresponde al período activo"
            Exit Sub
        End If
        If txtDescripcion.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Concepto]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        If beCtaActivo.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Cuenta]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        Dim tmpCuenta As con_Cuentas
        tmpCuenta = objTablas.con_CuentasSelectByPK(beCtaActivo.EditValue)
        If tmpCuenta.IdCuenta = "" Then
            MsgBox("El codigo de Cuenta no Existe" + Chr(13) + "Verifique [Cuenta]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        CargaPartida()

        Try
            For i = 0 To gvActivos.DataRowCount - 1
                If gvActivos.GetRowCellValue(i, "Retira") = True Then
                    blActivos.ActualizarRetiro(gvActivos.GetRowCellValue(i, "IdActivo"), deFechaRetiro.EditValue, leMotivo.EditValue, 7, gvActivos.GetRowCellValue(i, "Retira"))
                End If
            Next
            msj = blConta.InsertaPartidas(entPartida, entDetalle)
        Catch ex As Exception
            msj = ex.Message()
        End Try

        If msj = "" Then
            MsgBox("La Partida del Retiro fue Generada Exitosamente", MsgBoxStyle.Information)
            Close()
        Else
            MsgBox(String.Format("ERROR AL GENERALA PARTIDA DEL RETIRO{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
            Exit Sub
        End If
    End Sub

    Public Sub CargaPartida()
        With entPartida
            .IdPartida = objFunciones.ObtenerUltimoId("CON_PARTIDAS", "IdPartida") + 1
            .IdTipo = leTipoPartida.EditValue
            .Numero = ""
            .Fecha = deFechaRetiro.EditValue
            .Concepto = txtDescripcion.EditValue
            .Actualizada = 1
            .IdSucursal = piIdSucursalUsuario
            .IdModuloOrigen = 13
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .ModificadoPor = objMenu.User
            .FechaHoraModificacion = Now
        End With

        Dim j As Integer = 1
        Dim tmpActivo As New acf_Activos
        For i = 0 To gvActivos.DataRowCount - 1
            If gvActivos.GetRowCellValue(i, "Retira") = True Then
                tmpActivo = objTablas.acf_ActivosSelectByPK(gvActivos.GetRowCellValue(i, "IdActivo"))
                Dim tmpDetalle As New con_PartidasDetalle
                With tmpDetalle
                    .IdPartida = entPartida.IdPartida
                    .IdCentro = ""
                    .IdDetalle = j
                    .IdCuenta = tmpActivo.IdCuentaActivo
                    .Referencia = ""
                    .Concepto = txtDescripcion.EditValue & ", " & tmpActivo.Nombre
                    .Debe = 0.0
                    .Haber = tmpActivo.ValorDepreciar
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                    j = j + 1
                End With
                entDetalle.Add(tmpDetalle)

                Dim tmpDetalle2 As New con_PartidasDetalle
                With tmpDetalle2
                    .IdPartida = entPartida.IdPartida
                    .IdCentro = ""
                    .IdDetalle = j
                    .IdCuenta = tmpActivo.IdCuentaDepreciacion
                    .Referencia = ""
                    .Concepto = txtDescripcion.EditValue & ", " & tmpActivo.Nombre
                    .Debe = gvActivos.GetRowCellValue(i, "DepreciacionAcumulada")
                    .Haber = 0.0
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                    j = j + 1
                End With
                entDetalle.Add(tmpDetalle2)

                If gvActivos.GetRowCellValue(i, "Perdida") > 0 Then
                    Dim tmpDetalle3 As New con_PartidasDetalle
                    With tmpDetalle3
                        .IdPartida = entPartida.IdPartida
                        .IdCentro = ""
                        .IdDetalle = j
                        .IdCuenta = beCtaActivo.EditValue
                        .Referencia = ""
                        .Concepto = txtDescripcion.EditValue & ", " & tmpActivo.Nombre
                        .Debe = gvActivos.GetRowCellValue(i, "Perdida")
                        .Haber = 0.0
                        .CreadoPor = objMenu.User
                        .FechaHoraCreacion = Now
                        j = j + 1
                    End With
                    entDetalle.Add(tmpDetalle3)
                End If
            End If
        Next
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        If deFechaRetiro.EditValue = Nothing Then
            gcActivos.DataSource = blActivos.GetActivos(leClase.EditValue, Today, leSucursal.EditValue)
        Else
            gcActivos.DataSource = blActivos.GetActivos(leClase.EditValue, deFechaRetiro.EditValue, leSucursal.EditValue)
        End If

        gvActivos.BestFitColumns()
    End Sub
End Class
