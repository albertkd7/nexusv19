﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class acf_frmUbicaciones

    Private Sub acf_frmUbicaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.acf_UbicacionesSelectAll
    End Sub
    
    Private Sub acf_frmUbicaciones_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Return
        End If

        Dim IdUbic As String = gv.GetFocusedRowCellValue(gv.Columns(0))

        Dim msj As String = ""
        Try
            objTablas.acf_UbicacionesDeleteByPK(IdUbic)
        Catch ex As Exception
            msj = ex.Message()
        End Try
        If msj = "" Then
            MsgBox("La Ubicación ha sido eliminada con éxito", MsgBoxStyle.Information)
        Else
            MsgBox("No fue posible eliminar la Ubicación" + Chr(13) + msj, MsgBoxStyle.Critical, "Error al eliminar el registro")
        End If
        gc.DataSource = objTablas.acf_UbicacionesSelectAll
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        Dim entidad As New acf_Ubicaciones
        Dim msj As String = ""
        Dim numFila As Integer
        If e.RowHandle < 0 Then
            DbMode = DbModeType.insert
            numFila = gv.RowCount - 1
        Else
            DbMode = DbModeType.update
            numFila = e.RowHandle
        End If


        If Not AllowInsert Or Not AllowEdit Then
            MsgBox("No le está permitido ingresar o editar información", MsgBoxStyle.Exclamation, Me.Text)
            gc.DataSource = objTablas.acf_UbicacionesSelectAll
            Exit Sub
        End If

        With entidad
            .IdUbicacion = gv.GetRowCellValue(numFila, gv.Columns(0).FieldName)
            .Nombre = gv.GetRowCellValue(numFila, gv.Columns(1).FieldName)
        End With
        If DbMode = DbModeType.insert Then
            Try
                objTablas.acf_UbicacionesInsert(entidad)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If Not msj = "" Then
                MsgBox(String.Format("No fue posible Guardar la Ubicación{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error al Guardar el registro")
            End If
        Else
            Try
                objTablas.acf_UbicacionesUpdate(entidad)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If Not msj = "" Then
                MsgBox(String.Format("No fue posible actualizar la Ubicación{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error al Guardar el registro")
            End If
        End If
        gc.DataSource = objTablas.acf_UbicacionesSelectAll
    End Sub

    Private Sub acf_frmUbicaciones_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
End Class
