﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class acf_frmClases
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.beCta03 = New Nexus.beCtaContable()
        Me.beCta02 = New Nexus.beCtaContable()
        Me.beCta01 = New Nexus.beCtaContable()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdClase = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.ceDepreciable = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.teIdClase.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceDepreciable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.gc)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(843, 172)
        Me.PanelControl1.TabIndex = 4
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(2, 2)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(605, 168)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Id. Clase"
        Me.GridColumn1.FieldName = "IdClase"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 90
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombre"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 372
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.beCta03)
        Me.PanelControl2.Controls.Add(Me.beCta02)
        Me.PanelControl2.Controls.Add(Me.beCta01)
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.LabelControl28)
        Me.PanelControl2.Controls.Add(Me.LabelControl27)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.teIdClase)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.teNombre)
        Me.PanelControl2.Controls.Add(Me.ceDepreciable)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(0, 172)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(843, 146)
        Me.PanelControl2.TabIndex = 5
        '
        'beCta03
        '
        Me.beCta03.Location = New System.Drawing.Point(136, 103)
        Me.beCta03.Name = "beCta03"
        Me.beCta03.Size = New System.Drawing.Size(594, 20)
        Me.beCta03.TabIndex = 95
        Me.beCta03.ValidarMayor = False
        '
        'beCta02
        '
        Me.beCta02.Location = New System.Drawing.Point(136, 78)
        Me.beCta02.Name = "beCta02"
        Me.beCta02.Size = New System.Drawing.Size(594, 20)
        Me.beCta02.TabIndex = 94
        Me.beCta02.ValidarMayor = False
        '
        'beCta01
        '
        Me.beCta01.Location = New System.Drawing.Point(136, 53)
        Me.beCta01.Name = "beCta01"
        Me.beCta01.Size = New System.Drawing.Size(594, 20)
        Me.beCta01.TabIndex = 93
        Me.beCta01.ValidarMayor = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(51, 106)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl3.TabIndex = 91
        Me.LabelControl3.Text = "Cuenta de Gasto:"
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(18, 80)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(118, 13)
        Me.LabelControl28.TabIndex = 87
        Me.LabelControl28.Text = "Cuenta de Depreciación:"
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(49, 57)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl27.TabIndex = 88
        Me.LabelControl27.Text = "Cuenta de Activo:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(74, 10)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(62, 13)
        Me.LabelControl1.TabIndex = 34
        Me.LabelControl1.Text = "Id. de Clase:"
        '
        'teIdClase
        '
        Me.teIdClase.EnterMoveNextControl = True
        Me.teIdClase.Location = New System.Drawing.Point(139, 6)
        Me.teIdClase.Name = "teIdClase"
        Me.teIdClase.Size = New System.Drawing.Size(95, 20)
        Me.teIdClase.TabIndex = 27
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(95, 32)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 35
        Me.LabelControl2.Text = "Nombre:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(139, 29)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(589, 20)
        Me.teNombre.TabIndex = 28
        '
        'ceDepreciable
        '
        Me.ceDepreciable.Location = New System.Drawing.Point(274, 6)
        Me.ceDepreciable.Name = "ceDepreciable"
        Me.ceDepreciable.Properties.Caption = "Depreciable"
        Me.ceDepreciable.Size = New System.Drawing.Size(82, 19)
        Me.ceDepreciable.TabIndex = 36
        '
        'acf_frmClases
        '
        Me.ClientSize = New System.Drawing.Size(843, 343)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.DbMode = 1
        Me.Modulo = "Activo Fijo"
        Me.Name = "acf_frmClases"
        Me.OptionId = "001001"
        Me.Text = "Clases de Activos Fijo"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.PanelControl2, 0)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.teIdClase.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceDepreciable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdClase As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ceDepreciable As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCta03 As Nexus.beCtaContable
    Friend WithEvents beCta02 As Nexus.beCtaContable
    Friend WithEvents beCta01 As Nexus.beCtaContable

End Class
