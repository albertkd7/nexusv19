﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class acf_frmRetiroActivos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.leMotivo = New DevExpress.XtraEditors.LookUpEdit
        Me.sbContabilizar = New DevExpress.XtraEditors.SimpleButton
        Me.teCtaGasto = New DevExpress.XtraEditors.TextEdit
        Me.cmdImprimir = New DevExpress.XtraEditors.SimpleButton
        Me.leClase = New DevExpress.XtraEditors.LookUpEdit
        Me.beCtaActivo = New DevExpress.XtraEditors.ButtonEdit
        Me.gcActivos = New DevExpress.XtraGrid.GridControl
        Me.gvActivos = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcIdActivo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcCodigo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcFechaAdquisicion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcValor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcRetira = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkRetirar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.gcValorContable = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcValorDepreciar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcDepreciacionAcumulada = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcPerdida = New DevExpress.XtraGrid.Columns.GridColumn
        Me.deFechaRetiro = New DevExpress.XtraEditors.DateEdit
        Me.txtDescripcion = New DevExpress.XtraEditors.TextEdit
        Me.leTipoPartida = New DevExpress.XtraEditors.LookUpEdit
        Me.GrupoHistoria = New DevExpress.XtraLayout.LayoutControlGroup
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup
        Me.LayoutConverter1 = New DevExpress.XtraLayout.Converter.LayoutConverter(Me.components)
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl
        CType(Me.leMotivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCtaGasto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leClase.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCtaActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcActivos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvActivos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRetirar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaRetiro.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaRetiro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrupoHistoria, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'leMotivo
        '
        Me.leMotivo.Location = New System.Drawing.Point(126, 307)
        Me.leMotivo.Name = "leMotivo"
        Me.leMotivo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leMotivo.Size = New System.Drawing.Size(441, 20)
        Me.leMotivo.TabIndex = 7
        '
        'sbContabilizar
        '
        Me.sbContabilizar.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.sbContabilizar.Location = New System.Drawing.Point(241, 426)
        Me.sbContabilizar.Name = "sbContabilizar"
        Me.sbContabilizar.Size = New System.Drawing.Size(111, 22)
        Me.sbContabilizar.TabIndex = 35
        Me.sbContabilizar.Text = "Contabilizar"
        '
        'teCtaGasto
        '
        Me.teCtaGasto.Enabled = False
        Me.teCtaGasto.Location = New System.Drawing.Point(405, 385)
        Me.teCtaGasto.Name = "teCtaGasto"
        Me.teCtaGasto.Properties.ReadOnly = True
        Me.teCtaGasto.Size = New System.Drawing.Size(368, 20)
        Me.teCtaGasto.TabIndex = 34
        '
        'cmdImprimir
        '
        Me.cmdImprimir.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.cmdImprimir.Location = New System.Drawing.Point(126, 426)
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Size = New System.Drawing.Size(109, 22)
        Me.cmdImprimir.TabIndex = 12
        Me.cmdImprimir.Text = "Generar Reporte"
        '
        'leClase
        '
        Me.leClase.Location = New System.Drawing.Point(97, 7)
        Me.leClase.Name = "leClase"
        Me.leClase.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leClase.Size = New System.Drawing.Size(386, 20)
        Me.leClase.TabIndex = 4
        '
        'beCtaActivo
        '
        Me.beCtaActivo.EnterMoveNextControl = True
        Me.beCtaActivo.Location = New System.Drawing.Point(126, 385)
        Me.beCtaActivo.Name = "beCtaActivo"
        Me.beCtaActivo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beCtaActivo.Size = New System.Drawing.Size(164, 20)
        Me.beCtaActivo.TabIndex = 24
        '
        'gcActivos
        '
        Me.gcActivos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcActivos.Location = New System.Drawing.Point(2, 22)
        Me.gcActivos.MainView = Me.gvActivos
        Me.gcActivos.Name = "gcActivos"
        Me.gcActivos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkRetirar})
        Me.gcActivos.Size = New System.Drawing.Size(836, 240)
        Me.gcActivos.TabIndex = 5
        Me.gcActivos.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvActivos})
        '
        'gvActivos
        '
        Me.gvActivos.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdActivo, Me.gcCodigo, Me.gcNombre, Me.gcFechaAdquisicion, Me.gcValor, Me.gcRetira, Me.gcValorContable, Me.gcValorDepreciar, Me.gcDepreciacionAcumulada, Me.gcPerdida})
        Me.gvActivos.GridControl = Me.gcActivos
        Me.gvActivos.Name = "gvActivos"
        Me.gvActivos.OptionsView.ShowAutoFilterRow = True
        Me.gvActivos.OptionsView.ShowGroupPanel = False
        '
        'gcIdActivo
        '
        Me.gcIdActivo.Caption = "Cód. Activo"
        Me.gcIdActivo.FieldName = "IdActivo"
        Me.gcIdActivo.Name = "gcIdActivo"
        Me.gcIdActivo.OptionsColumn.ReadOnly = True
        Me.gcIdActivo.Visible = True
        Me.gcIdActivo.VisibleIndex = 0
        Me.gcIdActivo.Width = 60
        '
        'gcCodigo
        '
        Me.gcCodigo.Caption = "Codigo"
        Me.gcCodigo.FieldName = "Codigo"
        Me.gcCodigo.Name = "gcCodigo"
        Me.gcCodigo.OptionsColumn.ReadOnly = True
        Me.gcCodigo.Visible = True
        Me.gcCodigo.VisibleIndex = 1
        Me.gcCodigo.Width = 84
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.OptionsColumn.ReadOnly = True
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 2
        Me.gcNombre.Width = 260
        '
        'gcFechaAdquisicion
        '
        Me.gcFechaAdquisicion.Caption = "Fecha Adquisicion"
        Me.gcFechaAdquisicion.FieldName = "FechaAdquisicion"
        Me.gcFechaAdquisicion.Name = "gcFechaAdquisicion"
        Me.gcFechaAdquisicion.OptionsColumn.ReadOnly = True
        Me.gcFechaAdquisicion.Visible = True
        Me.gcFechaAdquisicion.VisibleIndex = 3
        Me.gcFechaAdquisicion.Width = 96
        '
        'gcValor
        '
        Me.gcValor.Caption = "Valor Adquisicion"
        Me.gcValor.FieldName = "ValorAdquisicion"
        Me.gcValor.Name = "gcValor"
        Me.gcValor.OptionsColumn.ReadOnly = True
        Me.gcValor.Visible = True
        Me.gcValor.VisibleIndex = 4
        Me.gcValor.Width = 88
        '
        'gcRetira
        '
        Me.gcRetira.Caption = "Retirar?"
        Me.gcRetira.ColumnEdit = Me.chkRetirar
        Me.gcRetira.FieldName = "Retira"
        Me.gcRetira.Name = "gcRetira"
        Me.gcRetira.Visible = True
        Me.gcRetira.VisibleIndex = 5
        Me.gcRetira.Width = 55
        '
        'chkRetirar
        '
        Me.chkRetirar.AutoHeight = False
        Me.chkRetirar.Name = "chkRetirar"
        '
        'gcValorContable
        '
        Me.gcValorContable.Caption = "Valor Contable"
        Me.gcValorContable.FieldName = "ValorContable"
        Me.gcValorContable.Name = "gcValorContable"
        '
        'gcValorDepreciar
        '
        Me.gcValorDepreciar.Caption = "Valor a Depreciar"
        Me.gcValorDepreciar.FieldName = "ValorDepreciar"
        Me.gcValorDepreciar.Name = "gcValorDepreciar"
        '
        'gcDepreciacionAcumulada
        '
        Me.gcDepreciacionAcumulada.Caption = "Depreciacion Acumulada"
        Me.gcDepreciacionAcumulada.FieldName = "DepreciacionAcumulada"
        Me.gcDepreciacionAcumulada.Name = "gcDepreciacionAcumulada"
        '
        'gcPerdida
        '
        Me.gcPerdida.Caption = "Perdida"
        Me.gcPerdida.FieldName = "Perdida"
        Me.gcPerdida.Name = "gcPerdida"
        '
        'deFechaRetiro
        '
        Me.deFechaRetiro.EditValue = Nothing
        Me.deFechaRetiro.Location = New System.Drawing.Point(672, 307)
        Me.deFechaRetiro.Name = "deFechaRetiro"
        Me.deFechaRetiro.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaRetiro.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFechaRetiro.Size = New System.Drawing.Size(99, 20)
        Me.deFechaRetiro.TabIndex = 8
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(126, 359)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(647, 20)
        Me.txtDescripcion.TabIndex = 7
        '
        'leTipoPartida
        '
        Me.leTipoPartida.Location = New System.Drawing.Point(126, 333)
        Me.leTipoPartida.Name = "leTipoPartida"
        Me.leTipoPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPartida.Size = New System.Drawing.Size(441, 20)
        Me.leTipoPartida.TabIndex = 6
        '
        'GrupoHistoria
        '
        Me.GrupoHistoria.CustomizationFormText = "GrupoHistoria"
        Me.GrupoHistoria.Location = New System.Drawing.Point(0, 416)
        Me.GrupoHistoria.Name = "GrupoHistoria"
        Me.GrupoHistoria.OptionsItemText.TextToControlDistance = 5
        Me.GrupoHistoria.Size = New System.Drawing.Size(337, 84)
        Me.GrupoHistoria.Text = "GrupoHistoria"
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.CustomizationFormText = "GrupoHistoria"
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 416)
        Me.LayoutControlGroup2.Name = "GrupoHistoria"
        Me.LayoutControlGroup2.OptionsItemText.TextToControlDistance = 5
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(337, 84)
        Me.LayoutControlGroup2.Text = "GrupoHistoria"
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.CustomizationFormText = "GrupoHistoria"
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 416)
        Me.LayoutControlGroup3.Name = "GrupoHistoria"
        Me.LayoutControlGroup3.OptionsItemText.TextToControlDistance = 5
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(337, 84)
        Me.LayoutControlGroup3.Text = "GrupoHistoria"
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.CustomizationFormText = "GrupoHistoria"
        Me.LayoutControlGroup4.Location = New System.Drawing.Point(0, 416)
        Me.LayoutControlGroup4.Name = "GrupoHistoria"
        Me.LayoutControlGroup4.OptionsItemText.TextToControlDistance = 5
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(337, 84)
        Me.LayoutControlGroup4.Text = "GrupoHistoria"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(16, 10)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Clase de Activo:"
        '
        'GroupControl2
        '
        Me.GroupControl2.CaptionLocation = DevExpress.Utils.Locations.Top
        Me.GroupControl2.Controls.Add(Me.gcActivos)
        Me.GroupControl2.Location = New System.Drawing.Point(13, 32)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(840, 264)
        Me.GroupControl2.TabIndex = 5
        Me.GroupControl2.Text = "Lista de Activos"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(2, 336)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl2.TabIndex = 6
        Me.LabelControl2.Text = "Tipo de Partida a Utilizar:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(9, 363)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(113, 13)
        Me.LabelControl3.TabIndex = 7
        Me.LabelControl3.Text = "Concepto de la Partida:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(37, 389)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl4.TabIndex = 8
        Me.LabelControl4.Text = "Cuenta Contable:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(308, 388)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl5.TabIndex = 25
        Me.LabelControl5.Text = "Nombre de Cuenta:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(604, 311)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl6.TabIndex = 36
        Me.LabelControl6.Text = "Fecha Retiro:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(47, 310)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl7.TabIndex = 37
        Me.LabelControl7.Text = "Motivo de Baja:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(593, 7)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(260, 20)
        Me.leSucursal.TabIndex = 48
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(546, 11)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl32.TabIndex = 49
        Me.LabelControl32.Text = "Sucursal:"
        '
        'acf_frmRetiroActivos
        '
        Me.ClientSize = New System.Drawing.Size(865, 679)
        Me.Controls.Add(Me.leSucursal)
        Me.Controls.Add(Me.LabelControl32)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.leClase)
        Me.Controls.Add(Me.cmdImprimir)
        Me.Controls.Add(Me.sbContabilizar)
        Me.Controls.Add(Me.deFechaRetiro)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.leTipoPartida)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.leMotivo)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.beCtaActivo)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.teCtaGasto)
        Me.DbMode = 1
        Me.Modulo = "Activo Fijo"
        Me.Name = "acf_frmRetiroActivos"
        Me.OptionId = "002004"
        Me.Text = "Retiro de Activos Fijos"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.teCtaGasto, 0)
        Me.Controls.SetChildIndex(Me.LabelControl5, 0)
        Me.Controls.SetChildIndex(Me.LabelControl3, 0)
        Me.Controls.SetChildIndex(Me.txtDescripcion, 0)
        Me.Controls.SetChildIndex(Me.beCtaActivo, 0)
        Me.Controls.SetChildIndex(Me.LabelControl4, 0)
        Me.Controls.SetChildIndex(Me.LabelControl7, 0)
        Me.Controls.SetChildIndex(Me.leMotivo, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        Me.Controls.SetChildIndex(Me.leTipoPartida, 0)
        Me.Controls.SetChildIndex(Me.LabelControl6, 0)
        Me.Controls.SetChildIndex(Me.deFechaRetiro, 0)
        Me.Controls.SetChildIndex(Me.sbContabilizar, 0)
        Me.Controls.SetChildIndex(Me.cmdImprimir, 0)
        Me.Controls.SetChildIndex(Me.leClase, 0)
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.GroupControl2, 0)
        Me.Controls.SetChildIndex(Me.LabelControl32, 0)
        Me.Controls.SetChildIndex(Me.leSucursal, 0)
        CType(Me.leMotivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCtaGasto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leClase.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCtaActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcActivos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvActivos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRetirar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaRetiro.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaRetiro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrupoHistoria, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gcActivos As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvActivos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents leClase As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leTipoPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtDescripcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents deFechaRetiro As DevExpress.XtraEditors.DateEdit
    Friend WithEvents beCtaActivo As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents teCtaGasto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cmdImprimir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbContabilizar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GrupoHistoria As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents gcIdActivo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCodigo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFechaAdquisicion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcRetira As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkRetirar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents gcValorContable As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorDepreciar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDepreciacionAcumulada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPerdida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leMotivo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutConverter1 As DevExpress.XtraLayout.Converter.LayoutConverter
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl

End Class
