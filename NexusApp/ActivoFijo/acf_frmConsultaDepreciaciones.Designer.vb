﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class acf_frmConsultaDepreciaciones
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.leClase = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcCodigo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcCosto = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leClase.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Size = New System.Drawing.Size(811, 400)
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Horizontal = False
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.RadioGroup1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.leClase)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.gc)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(811, 400)
        Me.SplitContainerControl1.SplitterPosition = 77
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'RadioGroup1
        '
        Me.RadioGroup1.EditValue = 0
        Me.RadioGroup1.Location = New System.Drawing.Point(142, 37)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Mensual"), New DevExpress.XtraEditors.Controls.RadioGroupItem(New Decimal(New Integer() {1, 0, 0, 0}), "Anual")})
        Me.RadioGroup1.Size = New System.Drawing.Size(172, 27)
        Me.RadioGroup1.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(37, 43)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Tipo de depreciación:"
        '
        'leClase
        '
        Me.leClase.Location = New System.Drawing.Point(142, 11)
        Me.leClase.Name = "leClase"
        Me.leClase.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leClase.Size = New System.Drawing.Size(470, 20)
        Me.leClase.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(61, 14)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Clase de Activo:"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(767, 318)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcCodigo, Me.gcDescripcion, Me.gcFecha, Me.gcCosto})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowAutoFilterRow = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcCodigo
        '
        Me.gcCodigo.Caption = "Código"
        Me.gcCodigo.FieldName = "Codigo"
        Me.gcCodigo.Name = "gcCodigo"
        Me.gcCodigo.Visible = True
        Me.gcCodigo.VisibleIndex = 0
        Me.gcCodigo.Width = 136
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Descripción del Activo"
        Me.gcDescripcion.FieldName = "Nombre"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 1
        Me.gcDescripcion.Width = 366
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha Adquisición"
        Me.gcFecha.FieldName = "FechaAdquisicion"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 2
        Me.gcFecha.Width = 103
        '
        'gcCosto
        '
        Me.gcCosto.Caption = "Costo de Adquisición"
        Me.gcCosto.DisplayFormat.FormatString = "n2"
        Me.gcCosto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcCosto.FieldName = "ValorAdquisicion"
        Me.gcCosto.Name = "gcCosto"
        Me.gcCosto.Visible = True
        Me.gcCosto.VisibleIndex = 3
        Me.gcCosto.Width = 110
        '
        'acf_frmConsultaDepreciaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(811, 425)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Modulo = "Activo Fijo"
        Me.Name = "acf_frmConsultaDepreciaciones"
        Me.Text = "Consulta de Depreciaciones"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.SplitContainerControl1, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leClase.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leClase As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcCodigo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCosto As DevExpress.XtraGrid.Columns.GridColumn

End Class
