﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class acf_frmContabilizarActivos
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(acf_frmContabilizarActivos))
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.MonthEdit1 = New DevExpress.XtraScheduler.UI.MonthEdit()
        Me.SpinEdit1 = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.meConcepto = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoPartida = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.lbMsg2 = New DevExpress.XtraEditors.LabelControl()
        Me.lbMsg1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.MonthEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl32)
        Me.GroupControl1.Controls.Add(Me.meConcepto)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.leTipoPartida)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.SimpleButton1)
        Me.GroupControl1.Controls.Add(Me.lbMsg2)
        Me.GroupControl1.Controls.Add(Me.lbMsg1)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.SpinEdit1)
        Me.GroupControl1.Controls.Add(Me.MonthEdit1)
        Me.GroupControl1.Size = New System.Drawing.Size(709, 266)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Contabilizar Periodo"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(158, 207)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(129, 33)
        Me.SimpleButton1.TabIndex = 5
        Me.SimpleButton1.Text = "Contabilizar..."
        '
        'MonthEdit1
        '
        Me.MonthEdit1.Enabled = False
        Me.MonthEdit1.Location = New System.Drawing.Point(157, 38)
        Me.MonthEdit1.Name = "MonthEdit1"
        Me.MonthEdit1.Properties.AllowFocused = False
        Me.MonthEdit1.Properties.AppearanceDisabled.BorderColor = System.Drawing.Color.Black
        Me.MonthEdit1.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.MonthEdit1.Properties.AppearanceDisabled.Options.UseBorderColor = True
        Me.MonthEdit1.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.MonthEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.MonthEdit1.Size = New System.Drawing.Size(92, 20)
        Me.MonthEdit1.TabIndex = 0
        '
        'SpinEdit1
        '
        Me.SpinEdit1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.SpinEdit1.Enabled = False
        Me.SpinEdit1.Location = New System.Drawing.Point(255, 38)
        Me.SpinEdit1.Name = "SpinEdit1"
        Me.SpinEdit1.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.SpinEdit1.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.SpinEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.SpinEdit1.Properties.DisplayFormat.FormatString = "n0"
        Me.SpinEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SpinEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.SpinEdit1.Properties.Mask.EditMask = "n0"
        Me.SpinEdit1.Size = New System.Drawing.Size(73, 20)
        Me.SpinEdit1.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(23, 40)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(128, 13)
        Me.LabelControl1.TabIndex = 26
        Me.LabelControl1.Text = "Período que se contabiliza:"
        '
        'meConcepto
        '
        Me.meConcepto.EditValue = "DEPRECIACION DE ACTIVO FIJO DEL MES DE"
        Me.meConcepto.Location = New System.Drawing.Point(158, 119)
        Me.meConcepto.Name = "meConcepto"
        Me.meConcepto.Size = New System.Drawing.Size(302, 76)
        Me.meConcepto.TabIndex = 4
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(42, 118)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(113, 13)
        Me.LabelControl5.TabIndex = 45
        Me.LabelControl5.Text = "Concepto de la partida:"
        '
        'leTipoPartida
        '
        Me.leTipoPartida.Location = New System.Drawing.Point(158, 63)
        Me.leTipoPartida.Name = "leTipoPartida"
        Me.leTipoPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPartida.Size = New System.Drawing.Size(302, 20)
        Me.leTipoPartida.TabIndex = 2
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(79, 66)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl4.TabIndex = 44
        Me.LabelControl4.Text = "Tipo de partida:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(158, 89)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(302, 20)
        Me.leSucursal.TabIndex = 3
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(111, 92)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl32.TabIndex = 51
        Me.LabelControl32.Text = "Sucursal:"
        '
        'lbMsg2
        '
        Me.lbMsg2.Location = New System.Drawing.Point(476, 40)
        Me.lbMsg2.Name = "lbMsg2"
        Me.lbMsg2.Size = New System.Drawing.Size(225, 39)
        Me.lbMsg2.TabIndex = 26
        Me.lbMsg2.Text = "No existen registros de periodos contabilizados" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Favor indique el periodo de inic" &
    "io contable" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "-- Nexus ERP --"
        Me.lbMsg2.Visible = False
        '
        'lbMsg1
        '
        Me.lbMsg1.Location = New System.Drawing.Point(334, 41)
        Me.lbMsg1.Name = "lbMsg1"
        Me.lbMsg1.Size = New System.Drawing.Size(136, 13)
        Me.lbMsg1.TabIndex = 26
        Me.lbMsg1.Text = "<--------------------------------"
        Me.lbMsg1.Visible = False
        '
        'acf_frmContabilizarActivos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(709, 291)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "ActivoFijo"
        Me.Name = "acf_frmContabilizarActivos"
        Me.Text = "Contabilizar Depreciaciones"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.MonthEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MonthEdit1 As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents SpinEdit1 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbMsg2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbMsg1 As DevExpress.XtraEditors.LabelControl
End Class
