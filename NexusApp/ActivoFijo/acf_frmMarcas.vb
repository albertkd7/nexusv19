﻿Imports NexusELL.TableEntities
Public Class acf_frmMarcas

    Private Sub acf_frmMarcas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.acf_MarcasSelectAll
    End Sub
    Private Sub acf_frmMarcas_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Return
        End If

        Dim Id As String = gv.GetFocusedRowCellValue(gv.Columns(0))
        Dim msj As String = ""
        Try
            objTablas.acf_MarcasDeleteByPK(Id)
        Catch ex As Exception
            msj = ex.Message()
        End Try
        If msj = "" Then
            MsgBox("La Marca ha sido eliminada con éxito", MsgBoxStyle.Information)
        Else
            MsgBox("No fue posible eliminar la Marca" + Chr(13) + msj, MsgBoxStyle.Critical, "Error al eliminar el registro")
        End If
        gc.DataSource = objTablas.acf_MarcasSelectAll
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        Dim entidad As New acf_Marcas
        Dim numFila As Integer
        Dim msj As String = ""
        If e.RowHandle < 0 Then
            DbMode = DbModeType.insert
            numFila = gv.RowCount - 1
        Else
            DbMode = DbModeType.update
            numFila = e.RowHandle
        End If

        If Not AllowInsert Or Not AllowEdit Then
            MsgBox("No le está permitido ingresar o editar información", MsgBoxStyle.Exclamation, Me.Text)
            gc.DataSource = objTablas.acf_MarcasSelectAll
            Exit Sub
        End If

        With entidad
            .IdMarca = gv.GetRowCellValue(numFila, gv.Columns(0).FieldName)
            .Nombre = gv.GetRowCellValue(numFila, gv.Columns(1).FieldName)
        End With

        If DbMode = DbModeType.insert Then
            Try
                objTablas.acf_MarcasInsert(entidad)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If Not msj = "" Then
                MsgBox(String.Format("No fue posible Guardar la Marca{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error al Guardar el registro")
            End If
        Else
            Try
                objTablas.acf_MarcasUpdate(entidad)
            Catch ex As Exception
                msj = ex.Message()
            End Try
            If Not msj = "" Then
                MsgBox(String.Format("No fue posible actualizar la Marca{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error al Guardar el registro")
            End If
        End If
        gc.DataSource = objTablas.acf_MarcasSelectAll
    End Sub
    Private Sub acf_frmMarcas_Reporte() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub

End Class
