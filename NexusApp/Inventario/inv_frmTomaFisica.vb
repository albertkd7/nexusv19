﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Imports System.IO
Public Class inv_frmTomaFisica
    Dim blInv As New InventarioBLL(g_ConnectionString)

    Declare Sub Sleep Lib "kernel32" Alias "Sleep" _
(ByVal dwMilliseconds As Long)

    Private Sub inv_frmTomaFisica_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFecha.EditValue = Today
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        leBodega.EditValue = 1
        deFechaAplicacion.EditValue = Today
        objCombos.invGrupos(leGrupo, "-- TODOS --")
        objCombos.inv_TiposSalida(leTipoSalida, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.inv_TiposEntrada(leTipoEntrada, "")
        objCombos.invSubGrupos(leSubGrupo, -1, "-- TODOS LOS SUB-GRUPOS --")
        gc.DataSource = blInv.inv_ObtieneTomaFisica(leGrupo.EditValue, leSubGrupo.EditValue, leBodega.EditValue, deFecha.EditValue)
        gv.BestFitColumns()
    End Sub

    Private Sub btActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btActualizar.Click
        Dim IdProducto As String
        Dim Existencias As Decimal
        For i = 0 To gv.DataRowCount - 1
            IdProducto = gv.GetRowCellValue(i, "IdProducto")
            Existencias = blInv.inv_ObtieneExistencia(IdProducto, deFecha.EditValue, leBodega.EditValue)
            gv.SetRowCellValue(i, "Existencias", Existencias)
        Next
    End Sub

    Private Sub btAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAplicar.Click
        If teNumero.EditValue = "" Then
            MsgBox("Debe especificar el Numero de Comprobante para hacer la aplicación" + Chr(13), MsgBoxStyle.Critical)
            Exit Sub
        End If

        Dim filtrado As Integer = SiEsNulo(gv.ActiveFilter.Count, 0)

        If filtrado > 0 Then
            MsgBox("Debe quitar los filtros en la consulta para aplicar la toma física", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If

        If MsgBox("¿Está seguro(a) de aplicar los ajustes a los inventarios?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim msj As String = ""
        Dim Existencias As Decimal = 0.0
        Dim Acumulado As Decimal = 0.0
        Dim Diferencia As Decimal = 0.0

        Dim entEntrada As New inv_Entradas
        Dim entSalida As New inv_Salidas
        Dim entConteo As New inv_Entradas

        Dim entDetSalida As New List(Of inv_SalidasDetalle)
        Dim entDetEntrada As New List(Of inv_EntradasDetalle)
        Dim entDetConteo As New List(Of inv_EntradasDetalle)


        For i = 0 To gv.DataRowCount - 1
            Dim entDetalleE As New inv_EntradasDetalle
            Dim entDetalleS As New inv_SalidasDetalle
            Existencias = gv.GetRowCellValue(i, "Existencias")
            Diferencia = gv.GetRowCellValue(i, "Diferencia")

            If Diferencia <> 0.0 Then 'Existencias <> 0.0
                If Diferencia < 0.0 Then
                    entDetalleS.Cantidad = 0.0 - Diferencia
                    entDetalleS.IdDetalle = i + 1
                    entDetalleS.CreadoPor = objMenu.User
                    entDetalleS.Descripcion = gv.GetRowCellValue(i, "Nombre")
                    entDetalleS.FechaHoraCreacion = Now
                    entDetalleS.IdProducto = gv.GetRowCellValue(i, "IdProducto")
                    entDetalleS.PrecioCosto = SiEsNulo(blInv.inv_ObtienePrecioCosto(gv.GetRowCellValue(i, "IdProducto"), deFecha.EditValue), 0.0)
                    entDetalleS.PrecioUnitario = entDetalleS.PrecioCosto
                    entDetalleS.PrecioTotal = Decimal.Round(entDetalleS.Cantidad * entDetalleS.PrecioCosto, 2)
                    entDetSalida.Add(entDetalleS)
                Else
                    entDetalleE.Cantidad = Diferencia
                    entDetalleE.IdDetalle = i + 1
                    entDetalleE.CreadoPor = objMenu.User
                    entDetalleE.Descripcion = gv.GetRowCellValue(i, "Nombre")
                    entDetalleE.FechaHoraCreacion = Now
                    entDetalleE.IdProducto = gv.GetRowCellValue(i, "IdProducto")
                    entDetalleE.PrecioCosto = SiEsNulo(blInv.inv_ObtienePrecioCosto(gv.GetRowCellValue(i, "IdProducto"), deFecha.EditValue), 0.0)
                    entDetalleE.PrecioUnitario = entDetalleE.PrecioCosto
                    entDetalleE.PrecioTotal = Decimal.Round(entDetalleE.Cantidad * entDetalleE.PrecioCosto, 2)
                    entDetEntrada.Add(entDetalleE)
                End If
            End If
        Next

        If entDetSalida.Count > 0 Then
            entSalida.Concepto = "AJUSTE DE SALIDA DE INVENTARIO POR TOMA FISICA"
            entSalida.CreadoPor = objMenu.User
            entSalida.Fecha = deFechaAplicacion.EditValue
            entSalida.FechaHoraCreacion = Now
            entSalida.IdBodega = leBodega.EditValue
            entSalida.IdSucursal = leSucursal.EditValue
            entSalida.IdTipoComprobante = leTipoSalida.EditValue
            entSalida.Numero = "S-" & teNumero.EditValue
        End If

        If entDetEntrada.Count > 0 Then
            entEntrada.Concepto = "AJUSTE DE ENTRADA DE INVENTARIO POR TOMA FISICA"
            entEntrada.CreadoPor = objMenu.User
            entEntrada.Fecha = deFechaAplicacion.EditValue
            entEntrada.FechaHoraCreacion = Now
            entEntrada.IdBodega = leBodega.EditValue
            entEntrada.IdSucursal = leSucursal.EditValue
            entEntrada.IdTipoComprobante = leTipoEntrada.EditValue
            entEntrada.Numero = "E-" & teNumero.EditValue
        End If

        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New inv_EntradasDetalle
            Acumulado = gv.GetRowCellValue(i, "Acumulado")
            If Acumulado > 0 Then
                entDetalle.IdDetalle = i + 1
                entDetalle.Cantidad = Acumulado
                entDetalle.CreadoPor = objMenu.User
                entDetalle.Descripcion = gv.GetRowCellValue(i, "Nombre")
                entDetalle.FechaHoraCreacion = Now
                entDetalle.IdProducto = gv.GetRowCellValue(i, "IdProducto")
                entDetalle.PrecioCosto = gv.GetRowCellValue(i, "PrecioCosto")
                entDetalle.PrecioUnitario = gv.GetRowCellValue(i, "PrecioCosto")
                entDetalle.PrecioTotal = Decimal.Round(Acumulado * gv.GetRowCellValue(i, "PrecioCosto"), 2)
                entDetConteo.Add(entDetalle)
            End If
        Next

        If entDetConteo.Count > 0 Then
            entConteo.Concepto = "INGRESO DE CONTEO TOMA FISICA"
            entConteo.CreadoPor = objMenu.User
            entConteo.Fecha = deFechaAplicacion.EditValue
            entConteo.FechaHoraCreacion = Now
            entConteo.IdBodega = leBodega.EditValue
            entConteo.IdSucursal = leSucursal.EditValue
            entConteo.IdTipoComprobante = leTipoEntrada.EditValue
            entConteo.Numero = "C-" & teNumero.EditValue
        End If

        msj = ""
        If entDetSalida.Count > 0 Then
            msj = blInv.inv_InsertarSalida(entSalida, entDetSalida, "")
        End If


        If msj = "" Then

        Else
            MsgBox("SE DETECTÓ UN ERROR AL APLICAR LA SALIDA DE INVENTARIO DE AJUSTE" + Chr(13) + msj, MsgBoxStyle.Critical)
            Exit Sub
        End If
        Sleep(500)
        msj = ""

        If entDetEntrada.Count > 0 Then
            msj = blInv.inv_InsertarEntrada(entEntrada, entDetEntrada)
        End If

        If msj = "" Then

        Else
            MsgBox("SE DETECTÓ UN ERROR AL APLICAR LA ENTRADA DE INVENTARIO DE AJUSTE" + Chr(13) + msj, MsgBoxStyle.Critical)
            Exit Sub
        End If

        'Sleep(500)
        'msj = blInv.inv_InsertarEntrada(entConteo, entDetConteo)
        'If msj = "" Then
        'Else
        '    MsgBox("SE DETECTÓ UN ERROR AL APLICAR EL CONTEO FISICO" + Chr(13) + msj, MsgBoxStyle.Critical)
        '    Exit Sub
        'End If
        ''blInv.inv_RecalculaExistencias(leBodega.EditValue, "-1")

        MsgBox("La Toma Fisica ha sido aplicada con éxito", MsgBoxStyle.Information)
        Me.Close()
    End Sub
    

    Private Sub leGrupo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leGrupo.EditValueChanged
        objCombos.invSubGrupos(leSubGrupo, leGrupo.EditValue, "-- TODOS LOS SUB-GRUPOS --")
        gc.DataSource = blInv.inv_ObtieneTomaFisica(leGrupo.EditValue, leSubGrupo.EditValue, leBodega.EditValue, deFecha.EditValue)
    End Sub

    Private Sub leSubGrupo_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles leSubGrupo.EditValueChanged
        gc.DataSource = blInv.inv_ObtieneTomaFisica(leGrupo.EditValue, leSubGrupo.EditValue, leBodega.EditValue, deFecha.EditValue)
    End Sub


    Private Sub leBodega_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles leBodega.EditValueChanged
        gc.DataSource = blInv.inv_ObtieneTomaFisica(leGrupo.EditValue, leSubGrupo.EditValue, leBodega.EditValue, deFecha.EditValue)
    End Sub

    Private Sub deFecha_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles deFecha.LostFocus
        gc.DataSource = blInv.inv_ObtieneTomaFisica(leGrupo.EditValue, leSubGrupo.EditValue, leBodega.EditValue, deFecha.EditValue)
    End Sub


    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        Dim Acumulado As Decimal = 0.0
        Dim Conteo As Decimal = 0.0
        'If gv.GetRowCellValue(gv.FocusedRowHandle, "Conteo") >= 0.0 Then
        Acumulado = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "Acumulado"), 0)
        Conteo = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "Conteo"), 0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Acumulado", SiEsNulo(Acumulado, 0) + SiEsNulo(Conteo, 0))
        gv.SetRowCellValue(gv.FocusedRowHandle, "Conteo", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Diferencia", SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "Acumulado"), 0) - SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "Existencias"), 0))
        ' End If
    End Sub


    Private Sub btListado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btListado.Click
        Dim dt As DataTable = gc.DataSource

        Dim rpt As New inv_rptInventarioTomaFisica() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlPeriodo.Text = (FechaToString(deFecha.EditValue, deFecha.EditValue)).ToUpper
        rpt.xrlTitulo.Text = "LISTADO PARA TOMA FISICA DE INVENTARIO"
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub btComparativo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btComparativo.Click
        Dim dt As DataTable = gc.DataSource

        Dim rpt As New inv_rptInventarioComparativoTomaFisica() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlPeriodo.Text = (FechaToString(deFecha.EditValue, deFecha.EditValue)).ToUpper
        rpt.xrlTitulo.Text = "LISTADO COMPARATIVO DE INVENTARIO FISICO"
        rpt.ShowPreviewDialog()
    End Sub


    Private Sub btCosteo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCosteo.Click
        Dim dt As DataTable = gc.DataSource

        Dim rpt As New inv_rptInventarioTomaFisicaDiferencia() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlPeriodo.Text = (FechaToString(deFecha.EditValue, deFecha.EditValue)).ToUpper
        rpt.xrlTitulo.Text = "LISTADO COMPARATIVO DE INVENTARIO FISICO COSTO"
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub sbImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbImportar.Click
        Dim Delimitador As String = InputBox("Especifique el deliminator del archivo, el cual puede ser coma , * |", "Delimitador", "|")
        If Delimitador = "" Then
            Return
        End If
        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            Return
        End If
        Dim csv_file As System.IO.StreamReader = File.OpenText(ofd.FileName)

        Try
            gc.DataSource = blInv.inv_ObtieneTomaFisicaEstructura()

            While csv_file.Peek() > 0
                Dim sLine As String = csv_file.ReadLine()
                Dim aData As Array = sLine.Split(Delimitador)
                Dim entPro As inv_Productos = objTablas.inv_ProductosSelectByPK(aData(0))

                If entPro.IdProducto <> "" Then
                    gv.AddNewRow()
                    Dim entunidad As inv_UnidadesMedida = objTablas.inv_UnidadesMedidaSelectByPK(entPro.IdUnidad)
                    Dim dtSaldo As DataTable = blInv.inv_ObtieneExistenciaCostoProducto(SiEsNulo(aData(0), ""), leBodega.EditValue)

                    gv.SetRowCellValue(gv.FocusedRowHandle, "IdProducto", aData(0))
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Nombre", entPro.Nombre)
                    gv.SetRowCellValue(gv.FocusedRowHandle, "UnidadMedida", entunidad.Nombre)
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Conteo", 0)
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Acumulado", SiEsNulo(aData(1), 0))
                    If dtSaldo.Rows.Count > 0 Then
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Existencias", dtSaldo.Rows(0).Item("SaldoExistencias"))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Diferencia", SiEsNulo(aData(1), 0) - dtSaldo.Rows(0).Item("SaldoExistencias"))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioCosto", dtSaldo.Rows(0).Item("PrecioCosto"))
                    Else
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Existencias", 0.0)
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Diferencia", SiEsNulo(aData(1), 0) - 0)
                        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioCosto", 0.0)
                    End If

                    gv.UpdateCurrentRow()
                End If


            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error al importar")
            Return
        End Try

        csv_file.Close()
    End Sub
End Class
