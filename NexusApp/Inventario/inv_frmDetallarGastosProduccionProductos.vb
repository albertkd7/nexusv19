﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class inv_frmDetallarGastosProduccionProductos
    Dim dtDetalle As New DataTable
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim dtGastosProd As DataTable

    Private _IdProduccion As Integer
    Public Property IdProduccion() As Integer
        Get
            Return _IdProduccion
        End Get
        Set(ByVal value As Integer)
            _IdProduccion = value
        End Set
    End Property

    Private _IdProducto As String
    Public Property IdProducto() As String
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As String)
            _IdProducto = value
        End Set
    End Property


    Private Sub com_frmDetallarGastosImportaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.inv_GastosProduccion(leGasto)
        LlenarGastosNuevo()
    End Sub

    Private Sub LlenarEntidades()
        Dim frmProd As inv_frmProducciones = Me.Owner
        Dim dt As DataTable = frmProd.dtGastosProd

        'PRIMERO BORRO DEL DATATABLE ORIGINAL EL REGISTRO DE ESTE PRODUCTO SI YA SE ENCUENTRA INGRESADO
        'PARA QUE TOME COMO DATOS VALIDOS LOS QUE A MARCADO AQUI

        'For j = 0 To dt.Rows.Count - 1
        '    If dt.Rows(j).Item("IdProducto") = IdProducto Then
        '        frmImp.dtGastosImpProd.Rows(j).Delete()
        '    End If
        'Next
        'frmImp.dtGastosImpProd.AcceptChanges()

        Dim filas As DataRow() = frmProd.dtGastosProd.Select("IdProducto ='" & IdProducto + "'")

        For Each filaActual As DataRow In filas
            filaActual.Delete()
        Next
        frmProd.dtGastosProd.AcceptChanges()

        For i = 0 To gv.DataRowCount - 1
            Dim dr As DataRow = frmProd.dtGastosProd.NewRow()
            dr("IdProduccion") = gv.GetRowCellValue(i, "IdProduccion")
            dr("IdProducto") = gv.GetRowCellValue(i, "IdProducto")
            dr("Descripcion") = gv.GetRowCellValue(i, "Descripcion")
            dr("IdGasto") = gv.GetRowCellValue(i, "IdGasto")
            dr("AplicaGasto") = gv.GetRowCellValue(i, "AplicaGasto")
            frmProd.dtGastosProd.Rows.Add(dr)
        Next

        Me.Close()
    End Sub

    Private Sub sbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGuardar.Click
        LlenarEntidades()
    End Sub

    Private Sub LlenarGastosNuevo()
        Dim frmProd As inv_frmProducciones = Me.Owner
        Dim dt As DataTable = frmProd.dtGastosProd

        If dt.Rows.Count > 0 Then

            Dim dt2 As New DataTable
            dt2 = bl.inv_ObtenerGastosProduccionProdEstructura()


            For i = 0 To dt.Rows.Count - 1
                If SiEsNulo(dt.Rows(i).Item("IdProducto"), "") = IdProducto Then
                    Dim dr As DataRow = dt2.NewRow()
                    dr("IdProduccion") = dt.Rows(i).Item("IdProduccion")
                    dr("IdProducto") = dt.Rows(i).Item("IdProducto")
                    dr("Descripcion") = dt.Rows(i).Item("Descripcion")
                    dr("IdGasto") = dt.Rows(i).Item("IdGasto")
                    dr("AplicaGasto") = dt.Rows(i).Item("AplicaGasto")
                    dt2.Rows.Add(dr)
                End If
            Next

            gc.DataSource = dt2
        Else
            gc.DataSource = dt
        End If

    End Sub
End Class
