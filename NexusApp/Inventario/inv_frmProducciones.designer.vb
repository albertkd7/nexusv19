﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmProducciones
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem
        Me.xtcProducciones = New DevExpress.XtraTab.XtraTabControl
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage
        Me.gc2 = New DevExpress.XtraGrid.GridControl
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcIdProducto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.riteCodigo = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.gcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.riteCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ritePrecio = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.gcPrecioTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcCantidadPosible = New DevExpress.XtraGrid.Columns.GridColumn
        Me.riteCantidadPosible = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.pcFooter = New DevExpress.XtraEditors.PanelControl
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl
        Me.cmdDetallar = New DevExpress.XtraEditors.SimpleButton
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl
        Me.sbGastos = New DevExpress.XtraEditors.SimpleButton
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl
        Me.leTipoTransaccion = New DevExpress.XtraEditors.LookUpEdit
        Me.btRevertir = New DevExpress.XtraEditors.SimpleButton
        Me.btAplicar = New DevExpress.XtraEditors.SimpleButton
        Me.btSaveAs = New DevExpress.XtraEditors.SimpleButton
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.meConcepto = New DevExpress.XtraEditors.MemoEdit
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit
        Me.teNumero = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.deFecha = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl
        Me.leBodega = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl
        CType(Me.xtcProducciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcProducciones.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCantidadPosible, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcFooter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.leTipoTransaccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcProducciones
        '
        Me.xtcProducciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcProducciones.Location = New System.Drawing.Point(0, 0)
        Me.xtcProducciones.Name = "xtcProducciones"
        Me.xtcProducciones.SelectedTabPage = Me.xtpLista
        Me.xtcProducciones.Size = New System.Drawing.Size(974, 335)
        Me.xtcProducciones.TabIndex = 4
        Me.xtcProducciones.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc2)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(968, 309)
        Me.xtpLista.Text = "Consulta de Producciones"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit2, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(968, 309)
        Me.gc2.TabIndex = 6
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12, Me.GridColumn1, Me.GridColumn14, Me.GridColumn15})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "IdComprobante"
        Me.GridColumn7.FieldName = "IdComprobante"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 0
        Me.GridColumn7.Width = 59
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Numero"
        Me.GridColumn10.FieldName = "Numero"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 1
        Me.GridColumn10.Width = 85
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Fecha"
        Me.GridColumn11.FieldName = "Fecha"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 2
        Me.GridColumn11.Width = 81
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Sucursal"
        Me.GridColumn12.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn12.FieldName = "IdSucursal"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 3
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Concepto"
        Me.GridColumn1.FieldName = "Concepto"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 4
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "Creado Por"
        Me.GridColumn14.FieldName = "CreadoPor"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 5
        Me.GridColumn14.Width = 98
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "Fecha Hora Creacion"
        Me.GridColumn15.ColumnEdit = Me.RepositoryItemDateEdit2
        Me.GridColumn15.FieldName = "FechaHoraCreacion"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 6
        Me.GridColumn15.Width = 114
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        Me.RepositoryItemDateEdit2.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.gc)
        Me.xtpDatos.Controls.Add(Me.pcFooter)
        Me.xtpDatos.Controls.Add(Me.pcBotones)
        Me.xtpDatos.Controls.Add(Me.pcHeader)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(968, 309)
        Me.xtpDatos.Text = "Producciones"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 117)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteCantidad, Me.ritePrecio, Me.riteCodigo, Me.riteCantidadPosible})
        Me.gc.Size = New System.Drawing.Size(927, 164)
        Me.gc.TabIndex = 8
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdProducto, Me.gcCantidad, Me.gcDescripcion, Me.gcPrecioUnitario, Me.gcPrecioTotal, Me.gcCantidadPosible})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdProducto
        '
        Me.gcIdProducto.Caption = "Id. Producto"
        Me.gcIdProducto.ColumnEdit = Me.riteCodigo
        Me.gcIdProducto.FieldName = "IdProducto"
        Me.gcIdProducto.Name = "gcIdProducto"
        Me.gcIdProducto.Visible = True
        Me.gcIdProducto.VisibleIndex = 0
        Me.gcIdProducto.Width = 109
        '
        'riteCodigo
        '
        Me.riteCodigo.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCodigo.AutoHeight = False
        Me.riteCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.riteCodigo.Name = "riteCodigo"
        '
        'gcCantidad
        '
        Me.gcCantidad.Caption = "Cantidad"
        Me.gcCantidad.ColumnEdit = Me.riteCantidad
        Me.gcCantidad.DisplayFormat.FormatString = "n2"
        Me.gcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcCantidad.FieldName = "Cantidad"
        Me.gcCantidad.Name = "gcCantidad"
        Me.gcCantidad.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.gcCantidad.Visible = True
        Me.gcCantidad.VisibleIndex = 1
        Me.gcCantidad.Width = 81
        '
        'riteCantidad
        '
        Me.riteCantidad.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCantidad.Appearance.Options.UseTextOptions = True
        Me.riteCantidad.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.riteCantidad.AutoHeight = False
        Me.riteCantidad.EditFormat.FormatString = "n2"
        Me.riteCantidad.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteCantidad.Mask.EditMask = "n2"
        Me.riteCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.riteCantidad.Name = "riteCantidad"
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Concepto/ Descripción del artículo"
        Me.gcDescripcion.FieldName = "Descripcion"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 3
        Me.gcDescripcion.Width = 366
        '
        'gcPrecioUnitario
        '
        Me.gcPrecioUnitario.Caption = "Precio Unitario"
        Me.gcPrecioUnitario.ColumnEdit = Me.ritePrecio
        Me.gcPrecioUnitario.FieldName = "PrecioUnitario"
        Me.gcPrecioUnitario.Name = "gcPrecioUnitario"
        Me.gcPrecioUnitario.Visible = True
        Me.gcPrecioUnitario.VisibleIndex = 4
        Me.gcPrecioUnitario.Width = 107
        '
        'ritePrecio
        '
        Me.ritePrecio.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.ritePrecio.AutoHeight = False
        Me.ritePrecio.Mask.EditMask = "n4"
        Me.ritePrecio.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePrecio.Mask.UseMaskAsDisplayFormat = True
        Me.ritePrecio.Name = "ritePrecio"
        '
        'gcPrecioTotal
        '
        Me.gcPrecioTotal.Caption = "Precio Total"
        Me.gcPrecioTotal.DisplayFormat.FormatString = "n2"
        Me.gcPrecioTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioTotal.FieldName = "PrecioTotal"
        Me.gcPrecioTotal.Name = "gcPrecioTotal"
        Me.gcPrecioTotal.OptionsColumn.AllowEdit = False
        Me.gcPrecioTotal.OptionsColumn.AllowFocus = False
        Me.gcPrecioTotal.Visible = True
        Me.gcPrecioTotal.VisibleIndex = 5
        Me.gcPrecioTotal.Width = 125
        '
        'gcCantidadPosible
        '
        Me.gcCantidadPosible.Caption = "Cantidad Posible"
        Me.gcCantidadPosible.ColumnEdit = Me.riteCantidadPosible
        Me.gcCantidadPosible.FieldName = "CantidadPosible"
        Me.gcCantidadPosible.Name = "gcCantidadPosible"
        Me.gcCantidadPosible.OptionsColumn.ReadOnly = True
        Me.gcCantidadPosible.Visible = True
        Me.gcCantidadPosible.VisibleIndex = 2
        Me.gcCantidadPosible.Width = 91
        '
        'riteCantidadPosible
        '
        Me.riteCantidadPosible.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCantidadPosible.AutoHeight = False
        Me.riteCantidadPosible.EditFormat.FormatString = "n2"
        Me.riteCantidadPosible.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteCantidadPosible.Mask.EditMask = "n2"
        Me.riteCantidadPosible.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteCantidadPosible.Mask.UseMaskAsDisplayFormat = True
        Me.riteCantidadPosible.Name = "riteCantidadPosible"
        '
        'pcFooter
        '
        Me.pcFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pcFooter.Location = New System.Drawing.Point(0, 281)
        Me.pcFooter.Name = "pcFooter"
        Me.pcFooter.Size = New System.Drawing.Size(927, 28)
        Me.pcFooter.TabIndex = 6
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdDetallar)
        Me.pcBotones.Controls.Add(Me.cmdUp)
        Me.pcBotones.Controls.Add(Me.cmdDown)
        Me.pcBotones.Controls.Add(Me.cmdBorrar)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(927, 117)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(41, 192)
        Me.pcBotones.TabIndex = 9
        '
        'cmdDetallar
        '
        Me.cmdDetallar.Image = Global.Nexus.My.Resources.Resources.autoriza3
        Me.cmdDetallar.Location = New System.Drawing.Point(7, 85)
        Me.cmdDetallar.Name = "cmdDetallar"
        Me.cmdDetallar.Size = New System.Drawing.Size(27, 26)
        ToolTipTitleItem1.Text = "Detallar" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Detallar Gastos a Productos"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.cmdDetallar.SuperTip = SuperToolTip1
        Me.cmdDetallar.TabIndex = 3
        Me.cmdDetallar.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(7, 4)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(27, 24)
        Me.cmdUp.TabIndex = 0
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(7, 30)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(27, 24)
        Me.cmdDown.TabIndex = 1
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(7, 55)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 24)
        Me.cmdBorrar.TabIndex = 2
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.sbGastos)
        Me.pcHeader.Controls.Add(Me.LabelControl17)
        Me.pcHeader.Controls.Add(Me.leTipoTransaccion)
        Me.pcHeader.Controls.Add(Me.btRevertir)
        Me.pcHeader.Controls.Add(Me.btAplicar)
        Me.pcHeader.Controls.Add(Me.btSaveAs)
        Me.pcHeader.Controls.Add(Me.LabelControl14)
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.meConcepto)
        Me.pcHeader.Controls.Add(Me.LabelControl6)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Controls.Add(Me.teCorrelativo)
        Me.pcHeader.Controls.Add(Me.teNumero)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.deFecha)
        Me.pcHeader.Controls.Add(Me.LabelControl10)
        Me.pcHeader.Controls.Add(Me.leBodega)
        Me.pcHeader.Controls.Add(Me.LabelControl13)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(968, 117)
        Me.pcHeader.TabIndex = 7
        '
        'sbGastos
        '
        Me.sbGastos.AllowFocus = False
        Me.sbGastos.Image = Global.Nexus.My.Resources.Resources.MoneyBagUSDollar32
        Me.sbGastos.Location = New System.Drawing.Point(790, 0)
        Me.sbGastos.Name = "sbGastos"
        Me.sbGastos.Size = New System.Drawing.Size(179, 33)
        Me.sbGastos.TabIndex = 83
        Me.sbGastos.Text = "&Gastos/Costos Fabricación "
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(13, 27)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(99, 13)
        Me.LabelControl17.TabIndex = 81
        Me.LabelControl17.Text = "Tipo de Transacción:"
        '
        'leTipoTransaccion
        '
        Me.leTipoTransaccion.EnterMoveNextControl = True
        Me.leTipoTransaccion.Location = New System.Drawing.Point(115, 26)
        Me.leTipoTransaccion.Name = "leTipoTransaccion"
        Me.leTipoTransaccion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoTransaccion.Size = New System.Drawing.Size(329, 20)
        Me.leTipoTransaccion.TabIndex = 80
        '
        'btRevertir
        '
        Me.btRevertir.AllowFocus = False
        Me.btRevertir.Location = New System.Drawing.Point(826, 86)
        Me.btRevertir.Name = "btRevertir"
        Me.btRevertir.Size = New System.Drawing.Size(107, 24)
        Me.btRevertir.TabIndex = 79
        Me.btRevertir.Text = "&Revertir"
        '
        'btAplicar
        '
        Me.btAplicar.AllowFocus = False
        Me.btAplicar.Location = New System.Drawing.Point(826, 62)
        Me.btAplicar.Name = "btAplicar"
        Me.btAplicar.Size = New System.Drawing.Size(107, 24)
        Me.btAplicar.TabIndex = 78
        Me.btAplicar.Text = "&Aplicar"
        '
        'btSaveAs
        '
        Me.btSaveAs.AllowFocus = False
        Me.btSaveAs.Location = New System.Drawing.Point(826, 38)
        Me.btSaveAs.Name = "btSaveAs"
        Me.btSaveAs.Size = New System.Drawing.Size(107, 24)
        Me.btSaveAs.TabIndex = 75
        Me.btSaveAs.Text = "Guardar como..."
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(467, 94)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl14.TabIndex = 73
        Me.LabelControl14.Text = "Sucursal:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(513, 91)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(273, 20)
        Me.leSucursal.TabIndex = 6
        '
        'meConcepto
        '
        Me.meConcepto.EnterMoveNextControl = True
        Me.meConcepto.Location = New System.Drawing.Point(115, 48)
        Me.meConcepto.Name = "meConcepto"
        Me.meConcepto.Size = New System.Drawing.Size(671, 41)
        Me.meConcepto.TabIndex = 4
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(55, 8)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl6.TabIndex = 66
        Me.LabelControl6.Text = "Correlativo:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(226, 7)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(104, 13)
        Me.LabelControl1.TabIndex = 65
        Me.LabelControl1.Text = "No. de Comprobante:"
        '
        'teCorrelativo
        '
        Me.teCorrelativo.Enabled = False
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(115, 5)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Properties.ReadOnly = True
        Me.teCorrelativo.Size = New System.Drawing.Size(98, 20)
        Me.teCorrelativo.TabIndex = 0
        '
        'teNumero
        '
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(332, 5)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(112, 20)
        Me.teNumero.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(639, 7)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl2.TabIndex = 67
        Me.LabelControl2.Text = "Fecha:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(674, 5)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFecha.Size = New System.Drawing.Size(112, 20)
        Me.deFecha.TabIndex = 3
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(16, 94)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(96, 13)
        Me.LabelControl10.TabIndex = 68
        Me.LabelControl10.Text = "Bodega de Entrada:"
        '
        'leBodega
        '
        Me.leBodega.EnterMoveNextControl = True
        Me.leBodega.Location = New System.Drawing.Point(115, 91)
        Me.leBodega.Name = "leBodega"
        Me.leBodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leBodega.Size = New System.Drawing.Size(329, 20)
        Me.leBodega.TabIndex = 5
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(62, 49)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl13.TabIndex = 69
        Me.LabelControl13.Text = "Concepto:"
        '
        'inv_frmProducciones
        '
        Me.ClientSize = New System.Drawing.Size(974, 360)
        Me.Controls.Add(Me.xtcProducciones)
        Me.Modulo = "Inventario"
        Me.Name = "inv_frmProducciones"
        Me.OptionId = "002007"
        Me.Text = "Producciones"
        Me.Controls.SetChildIndex(Me.xtcProducciones, 0)
        CType(Me.xtcProducciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcProducciones.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCantidadPosible, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcFooter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.leTipoTransaccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents xtcProducciones As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCodigo As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ritePrecio As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcPrecioTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCantidadPosible As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCantidadPosible As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents pcFooter As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdDetallar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents sbGastos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoTransaccion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents btRevertir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btAplicar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btSaveAs As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents meConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leBodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit

End Class
