﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports System.Math
Public Class inv_frmConsultaProductosPrecios
    Dim blFac As New FacturaBLL(g_ConnectionString)
    Dim blInventa As New InventarioBLL(g_ConnectionString)
    Dim Prod As DataTable, ProdVista As DataTable
    Dim IdProducto As String
    Dim PreciosDetalle As List(Of inv_ProductosPrecios)


    Private Sub fac_frmConsultaProductosBase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'txtCodigo.EditValue = ""
        'txtNombre.EditValue = ""
        CargaCombos()
        gvProd1.Columns.Clear()

        CargaDatos()
        txtCodigo.Focus()
    End Sub

    Private Sub CargaCombos()
        objCombos.inv_Precios(rileIdPrecio1)
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
    End Sub


    Private Sub CargaDatos()
        gcProductos.DataSource = blFac.fac_ConsultaProductosPrecios(Today, leGrupo.EditValue)
        ProdVista = gcProductos.DataSource
        IdProducto = ""
        gvProd1.BestFitColumns()
        ' gvProd1.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle
        txtCodigo.Focus()
    End Sub



    Private Sub gvProd1_FocusedRowChanged_1(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gvProd1.FocusedRowChanged
        IdProducto = gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "IdProducto")
        Prod = blInventa.inv_GeneralesProducto(IdProducto)

        gcPr1.DataSource = blInventa.inv_ObtienePrecios(IdProducto, objMenu.User, pnIVA)
        gcEx1.DataSource = blInventa.inv_ObtieneSaldosPorProducto(-1, IdProducto)
        lblProducto.Text = gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "IdProducto") & " - " & gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "Nombre")

        If Prod.Rows.Count = 0 Then
            Exit Sub
        End If

    End Sub

    Private Sub leGrupo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leGrupo.EditValueChanged
        CargaDatos()
    End Sub

    Private Sub gvPr1_ValidatingEditor(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gvPr1.ValidatingEditor
        Dim Margen As Decimal = gvPr1.GetRowCellValue(gvProd1.FocusedRowHandle, "Margen")
        Dim PorcDescuento As Decimal = gvPr1.GetRowCellValue(gvProd1.FocusedRowHandle, "Descuento")

        Select Case gvPr1.FocusedColumn.FieldName
            Case "Margen"
                Margen = e.Value
                gvPr1.SetFocusedRowCellValue("Margen", Margen)
            Case "Descuento"
                PorcDescuento = e.Value
                gvPr1.SetFocusedRowCellValue("Descuento", PorcDescuento)
                If PorcDescuento = 0.0 Then
                    gvPr1.SetFocusedRowCellValue("Margen", 0.0)
                End If
        End Select



        CalcularPrecios()
    End Sub

    Private Sub CalcularPrecios()

        Dim Costo As Decimal = gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "PrecioCosto")
        Dim PrecioBase As Decimal = 0.0, Precio As Decimal = 0.0, Margen As Decimal = 0.0, MargenProdBase As Decimal = 0.0

        For i = 0 To gvPr1.DataRowCount - 1
            If gvPr1.GetRowCellValue(i, "IdPrecio") = 1 Then
                MargenProdBase = gvPr1.GetRowCellValue(i, "Margen")
                If gvPr1.GetRowCellValue(i, "Margen") > 0.0 Then
                    PrecioBase = Costo / (1 - (gvPr1.GetRowCellValue(i, "Margen") / 100))
                    PrecioBase = PrecioBase * (pnIVA + 1)


                    gvPr1.SetRowCellValue(i, "Precio", PrecioBase)
                    gvPr1.SetRowCellValue(i, "Descuento", 0)
                End If
            Else
                If gvPr1.GetRowCellValue(i, "Descuento") > 0.0 Then
                    Precio = Round((1 - (gvPr1.GetRowCellValue(i, "Descuento") / 100)) * PrecioBase, 4)
                    gvPr1.SetRowCellValue(i, "Precio", Precio)

                    If Costo > 0.0 Then
                        Margen = 0 - Round((Costo / (Precio / (pnIVA + 1)) - 1) * 100, 2)
                    Else
                        Margen = 0.0
                    End If
                    gvPr1.SetRowCellValue(i, "Margen", Margen)
                End If
            End If
            gvPr1.SetRowCellValue(i, "PrecioNeto", Decimal.Round(Precio / (pnIVA + 1), 4))
        Next

    End Sub

    Private Sub btAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAplicar.Click

        Dim Mensaje As String = " a este Grupo?", Mensaje2 As String = leGrupo.Text

        If leGrupo.EditValue = -1 Then
            Mensaje = " a este Producto?"
            Mensaje2 = gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "Nombre")
        End If
        If MsgBox("¿Está seguro(a) de aplicar estos cambios de precios" + Mensaje + Chr(13) + Mensaje2, MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)

        If leGrupo.EditValue <> -1 Then ' si tiene un grupo seleccionado, actualiza todo el grupo
            For i = 0 To gvProd1.DataRowCount - 1
                Dim IdPro As String = ""
                IdPro = gvProd1.GetRowCellValue(i, "IdProducto")
                PreciosDetalle = New List(Of inv_ProductosPrecios)
                For j = 0 To gvPr1.DataRowCount - 1
                    Dim entPrecio As New inv_ProductosPrecios
                    With entPrecio
                        .IdProducto = IdPro
                        .IdPrecio = gvPr1.GetRowCellValue(j, "IdPrecio")
                        .Precio = SiEsNulo(gvPr1.GetRowCellValue(j, "Precio"), 0)
                        .Descuento = SiEsNulo(gvPr1.GetRowCellValue(j, "Descuento"), 0)
                        .Margen = SiEsNulo(gvPr1.GetRowCellValue(j, "Margen"), 0)
                        .CreadoPor = objMenu.User
                        .FechaHoraCreacion = Now
                    End With
                    PreciosDetalle.Add(entPrecio)
                Next
                blInventa.inv_ActualizaPrecios(IdPro, PreciosDetalle)
            Next
        Else ' sino solo actualiza el producto seleccionado
            Dim IdPro As String = gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "IdProducto")
            PreciosDetalle = New List(Of inv_ProductosPrecios)
            For k = 0 To gvPr1.DataRowCount - 1
                Dim entPrecio As New inv_ProductosPrecios
                With entPrecio
                    .IdProducto = IdPro
                    .IdPrecio = gvPr1.GetRowCellValue(k, "IdPrecio")
                    .Precio = SiEsNulo(gvPr1.GetRowCellValue(k, "Precio"), 0)
                    .Descuento = SiEsNulo(gvPr1.GetRowCellValue(k, "Descuento"), 0)
                    .Margen = SiEsNulo(gvPr1.GetRowCellValue(k, "Margen"), 0)
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                End With
                PreciosDetalle.Add(entPrecio)
            Next
            blInventa.inv_ActualizaPrecios(IdPro, PreciosDetalle)

        End If

        MsgBox("La transacción fue realizada con éxito.", MsgBoxStyle.Information, "Nota")

    End Sub


#Region "Tipo de Busqueda"
    Private Sub txtNombre_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombre.EditValueChanged

        'If rgTipoBusqueda.SelectedIndex = 0 Then
        '    ProdVista.DefaultView.RowFilter = ("Nombre like '%" + txtNombre.EditValue + "%'")
        'Else
        '    ProdVista.DefaultView.RowFilter = ("Nombre like '" + txtNombre.EditValue + "%'")
        'End If
        If rgTipoBusqueda.SelectedIndex = 0 Then
            Dim Transaccion As String
            Dim CadenaPrincipal As String = txtNombre.EditValue

            Transaccion = txtNombre.EditValue
            If txtNombre.EditValue <> "" And Transaccion.IndexOf("%") > 0 Then
                Dim V() As String
                Dim Cadena As String
                Dim SentenciaBusqueda As String = "Nombre like '%"
                Dim x As Integer

                'Cadena a tratar 
                Cadena = txtNombre.EditValue

                'Remplazando todos los espaciadores incorrectos por ";" 
                For x = 0 To 1
                    Cadena = Replace(Cadena, "%", ";")
                Next x

                'Separo la cadena gracias al delimitador ";" 
                V = Split(Cadena, ";")

                'Muestro la cadena 
                For x = 0 To UBound(V)
                    If x = 0 Then
                        SentenciaBusqueda += Trim(V(x)) + "%'"
                    Else
                        SentenciaBusqueda += IIf(Trim(V(x)) <> "", " and Nombre like '%" + Trim(V(x)) + "%'", Trim(V(x)))
                    End If
                Next x
                ProdVista.DefaultView.RowFilter = (SentenciaBusqueda)
            Else
                ProdVista.DefaultView.RowFilter = ("Nombre like '%" + txtNombre.EditValue + "%'")
            End If
        Else
            ProdVista.DefaultView.RowFilter = ("Nombre like '" + txtNombre.EditValue + "%'")
        End If

        gcProductos.DataSource = ProdVista.DefaultView
        lblProducto.Text = gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "IdProducto") & " - " & gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "Nombre")

        IdProducto = gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "IdProducto")
        gcPr1.DataSource = blInventa.inv_ObtienePrecios(IdProducto, objMenu.User, pnIVA)
        gcEx1.DataSource = blInventa.inv_ObtieneSaldosPorProducto(-1, IdProducto)

    End Sub

    Private Sub txtCodigo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigo.EditValueChanged
        If rgTipoBusqueda.SelectedIndex = 0 Then
            ProdVista.DefaultView.RowFilter = ("IdProducto like '%" + txtCodigo.EditValue + "%'")
        Else
            ProdVista.DefaultView.RowFilter = ("IdProducto like '" + txtCodigo.EditValue + "%'")
        End If
        'IdProducto = gvProd.GetRowCellValue(gvProd.FocusedRowHandle, "IdProducto")
        gcProductos.DataSource = ProdVista.DefaultView
        lblProducto.Text = gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "IdProducto") & " - " & gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "Nombre")
        IdProducto = gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "IdProducto")
        gcPr1.DataSource = blInventa.inv_ObtienePrecios(IdProducto, objMenu.User, pnIVA)
        gcEx1.DataSource = blInventa.inv_ObtieneSaldosPorProducto(-1, IdProducto)
    End Sub

    Private Sub rgTipoBusqueda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgTipoBusqueda.SelectedIndexChanged
        If txtCodigo.EditValue <> "" Then
            txtCodigo_EditValueChanged("", New EventArgs)
        Else
            If txtNombre.EditValue <> "" Then
                txtNombre_EditValueChanged("", New EventArgs)
            End If
        End If
    End Sub
#End Region

End Class
