﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmIngresoKit
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.teProducto = New DevExpress.XtraEditors.TextEdit
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton
        Me.teIdProducto = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton
        Me.pcTotales = New DevExpress.XtraEditors.PanelControl
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcIdProducto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.riteCodigo = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.gcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.riteCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ritePrecio = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        CType(Me.teProducto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdProducto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcTotales.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'teProducto
        '
        Me.teProducto.EnterMoveNextControl = True
        Me.teProducto.Location = New System.Drawing.Point(164, 5)
        Me.teProducto.Name = "teProducto"
        Me.teProducto.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teProducto.Properties.ReadOnly = True
        Me.teProducto.Size = New System.Drawing.Size(320, 20)
        Me.teProducto.TabIndex = 1
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(3, 35)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(28, 24)
        Me.cmdUp.TabIndex = 35
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(3, 6)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(28, 24)
        Me.cmdDown.TabIndex = 33
        '
        'teIdProducto
        '
        Me.teIdProducto.EnterMoveNextControl = True
        Me.teIdProducto.Location = New System.Drawing.Point(79, 5)
        Me.teIdProducto.Name = "teIdProducto"
        Me.teIdProducto.Properties.ReadOnly = True
        Me.teIdProducto.Size = New System.Drawing.Size(81, 20)
        Me.teIdProducto.TabIndex = 0
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(11, 10)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(62, 13)
        Me.LabelControl15.TabIndex = 48
        Me.LabelControl15.Text = "Producto Kit:"
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.teIdProducto)
        Me.pcHeader.Controls.Add(Me.LabelControl15)
        Me.pcHeader.Controls.Add(Me.teProducto)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(503, 48)
        Me.pcHeader.TabIndex = 0
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdBorrar)
        Me.pcBotones.Controls.Add(Me.cmdUp)
        Me.pcBotones.Controls.Add(Me.cmdDown)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(472, 48)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(31, 269)
        Me.pcBotones.TabIndex = 56
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(3, 65)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(28, 23)
        Me.cmdBorrar.TabIndex = 36
        '
        'pcTotales
        '
        Me.pcTotales.Controls.Add(Me.sbGuardar)
        Me.pcTotales.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pcTotales.Location = New System.Drawing.Point(0, 264)
        Me.pcTotales.Name = "pcTotales"
        Me.pcTotales.Size = New System.Drawing.Size(472, 53)
        Me.pcTotales.TabIndex = 57
        '
        'sbGuardar
        '
        Me.sbGuardar.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sbGuardar.Appearance.Options.UseFont = True
        Me.sbGuardar.Location = New System.Drawing.Point(27, 4)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(133, 30)
        Me.sbGuardar.TabIndex = 0
        Me.sbGuardar.Text = "&Guardar Kit"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 48)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteCantidad, Me.ritePrecio, Me.riteCodigo})
        Me.gc.Size = New System.Drawing.Size(472, 216)
        Me.gc.TabIndex = 58
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdProducto, Me.gcCantidad, Me.gcNombre})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdProducto
        '
        Me.gcIdProducto.Caption = "Id. Producto"
        Me.gcIdProducto.ColumnEdit = Me.riteCodigo
        Me.gcIdProducto.FieldName = "IdProducto"
        Me.gcIdProducto.Name = "gcIdProducto"
        Me.gcIdProducto.OptionsColumn.ReadOnly = True
        Me.gcIdProducto.Visible = True
        Me.gcIdProducto.VisibleIndex = 0
        Me.gcIdProducto.Width = 77
        '
        'riteCodigo
        '
        Me.riteCodigo.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCodigo.AutoHeight = False
        Me.riteCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.riteCodigo.Name = "riteCodigo"
        '
        'gcCantidad
        '
        Me.gcCantidad.Caption = "Cantidad"
        Me.gcCantidad.ColumnEdit = Me.riteCantidad
        Me.gcCantidad.DisplayFormat.FormatString = "n6"
        Me.gcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcCantidad.FieldName = "Cantidad"
        Me.gcCantidad.Name = "gcCantidad"
        Me.gcCantidad.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)})
        Me.gcCantidad.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.gcCantidad.Visible = True
        Me.gcCantidad.VisibleIndex = 2
        Me.gcCantidad.Width = 100
        '
        'riteCantidad
        '
        Me.riteCantidad.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCantidad.Appearance.Options.UseTextOptions = True
        Me.riteCantidad.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.riteCantidad.AutoHeight = False
        Me.riteCantidad.EditFormat.FormatString = "n6"
        Me.riteCantidad.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteCantidad.Mask.EditMask = "n6"
        Me.riteCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.riteCantidad.Name = "riteCantidad"
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.OptionsColumn.ReadOnly = True
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 1
        Me.gcNombre.Width = 263
        '
        'ritePrecio
        '
        Me.ritePrecio.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.ritePrecio.AutoHeight = False
        Me.ritePrecio.Mask.EditMask = "n4"
        Me.ritePrecio.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePrecio.Mask.UseMaskAsDisplayFormat = True
        Me.ritePrecio.Name = "ritePrecio"
        '
        'inv_frmIngresoKit
        '
        Me.ClientSize = New System.Drawing.Size(503, 342)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.pcTotales)
        Me.Controls.Add(Me.pcBotones)
        Me.Controls.Add(Me.pcHeader)
        Me.Name = "inv_frmIngresoKit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ingreso de Kit de Productos"
        Me.Controls.SetChildIndex(Me.pcHeader, 0)
        Me.Controls.SetChildIndex(Me.pcBotones, 0)
        Me.Controls.SetChildIndex(Me.pcTotales, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.teProducto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdProducto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcTotales.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teProducto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pcTotales As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Public WithEvents teIdProducto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCodigo As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ritePrecio As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit

End Class
