﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmProductos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Me.myStyle = New DevExpress.XtraEditors.StyleController(Me.components)
        Me.OpenFile = New System.Windows.Forms.OpenFileDialog()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.dn = New DevExpress.XtraEditors.DataNavigator()
        Me.xtcProductos = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.teCuantia = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.teOrigen = New DevExpress.XtraEditors.TextEdit()
        Me.teTraduccion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.sbCentroCosto = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.leCentroCosto = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.leDepartamento = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.teCodBarra = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.leCategoria = New DevExpress.XtraEditors.LookUpEdit()
        Me.btnDetallarKit = New System.Windows.Forms.Button()
        Me.chkCompuesto = New DevExpress.XtraEditors.CheckEdit()
        Me.teEstilo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.teColor = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.leGrupo = New DevExpress.XtraEditors.LookUpEdit()
        Me.teTalla = New DevExpress.XtraEditors.TextEdit()
        Me.leSubGrupo = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.teCodigo = New DevExpress.XtraEditors.TextEdit()
        Me.leMarca = New DevExpress.XtraEditors.LookUpEdit()
        Me.leEstado = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoInventario = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.teUnidadesPres = New DevExpress.XtraEditors.TextEdit()
        Me.deFechaIngreso = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.leProveedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.teExistenciaMinima = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.teExistenciaMaxima = New DevExpress.XtraEditors.TextEdit()
        Me.leUnidadMedida = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.ceProductoEsp = New DevExpress.XtraEditors.CheckEdit()
        Me.cePermiteFact = New DevExpress.XtraEditors.CheckEdit()
        Me.ceExento = New DevExpress.XtraEditors.CheckEdit()
        Me.leUbicacion = New DevExpress.XtraEditors.LookUpEdit()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.peFoto = New DevExpress.XtraEditors.PictureEdit()
        Me.lcExisteArchivo = New DevExpress.XtraEditors.LabelControl()
        Me.BeCtaContable3 = New Nexus.beCtaContable()
        Me.BeCtaContable2 = New Nexus.beCtaContable()
        Me.BeCtaContable1 = New Nexus.beCtaContable()
        Me.lcCtaCosto = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.beImagen = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.meInfoAdicional = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.SplitContainerControl2 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.gcPr1 = New DevExpress.XtraGrid.GridControl()
        Me.gvPr1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdPrecio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.rileIdPrecio1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDescuento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcMargen = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.myStyle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.xtcProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcProductos.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.teCuantia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teOrigen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTraduccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCentroCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leDepartamento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCodBarra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCategoria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCompuesto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teEstilo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teColor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTalla.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSubGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leMarca.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoInventario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teUnidadesPres.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaIngreso.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaIngreso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teExistenciaMinima.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teExistenciaMaxima.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leUnidadMedida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceProductoEsp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cePermiteFact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceExento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leUbicacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.peFoto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beImagen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meInfoAdicional.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl2.SuspendLayout()
        CType(Me.gcPr1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPr1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rileIdPrecio1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'myStyle
        '
        Me.myStyle.Appearance.Options.UseTextOptions = True
        Me.myStyle.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.myStyle.AppearanceFocused.Options.UseTextOptions = True
        Me.myStyle.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        '
        'OpenFile
        '
        Me.OpenFile.FileName = "OpenFile"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(51, 51)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "LabelControl1"
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.dn)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(1253, 28)
        Me.pcHeader.TabIndex = 95
        '
        'dn
        '
        Me.dn.Buttons.Append.Visible = False
        Me.dn.Buttons.CancelEdit.Visible = False
        Me.dn.Buttons.EndEdit.Visible = False
        Me.dn.Buttons.NextPage.Visible = False
        Me.dn.Buttons.PrevPage.Visible = False
        Me.dn.Buttons.Remove.Visible = False
        Me.dn.Location = New System.Drawing.Point(3, 3)
        Me.dn.Name = "dn"
        Me.dn.Size = New System.Drawing.Size(165, 23)
        Me.dn.TabIndex = 1
        Me.dn.Text = "DataNavigator1"
        Me.dn.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Begin
        Me.dn.TextStringFormat = "Producto {0} de {1}"
        '
        'xtcProductos
        '
        Me.xtcProductos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcProductos.Location = New System.Drawing.Point(0, 28)
        Me.xtcProductos.Name = "xtcProductos"
        Me.xtcProductos.SelectedTabPage = Me.XtraTabPage1
        Me.xtcProductos.Size = New System.Drawing.Size(1253, 559)
        Me.xtcProductos.TabIndex = 96
        Me.xtcProductos.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.teCuantia)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl29)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl26)
        Me.XtraTabPage1.Controls.Add(Me.teOrigen)
        Me.XtraTabPage1.Controls.Add(Me.teTraduccion)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl7)
        Me.XtraTabPage1.Controls.Add(Me.sbCentroCosto)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl28)
        Me.XtraTabPage1.Controls.Add(Me.leCentroCosto)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl27)
        Me.XtraTabPage1.Controls.Add(Me.leDepartamento)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl25)
        Me.XtraTabPage1.Controls.Add(Me.teCodBarra)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl24)
        Me.XtraTabPage1.Controls.Add(Me.leCategoria)
        Me.XtraTabPage1.Controls.Add(Me.btnDetallarKit)
        Me.XtraTabPage1.Controls.Add(Me.chkCompuesto)
        Me.XtraTabPage1.Controls.Add(Me.teEstilo)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl22)
        Me.XtraTabPage1.Controls.Add(Me.teNombre)
        Me.XtraTabPage1.Controls.Add(Me.teColor)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl2)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl15)
        Me.XtraTabPage1.Controls.Add(Me.leGrupo)
        Me.XtraTabPage1.Controls.Add(Me.teTalla)
        Me.XtraTabPage1.Controls.Add(Me.leSubGrupo)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl14)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl3)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl23)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl20)
        Me.XtraTabPage1.Controls.Add(Me.teCodigo)
        Me.XtraTabPage1.Controls.Add(Me.leMarca)
        Me.XtraTabPage1.Controls.Add(Me.leEstado)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl4)
        Me.XtraTabPage1.Controls.Add(Me.leTipoInventario)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl5)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl21)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl8)
        Me.XtraTabPage1.Controls.Add(Me.teUnidadesPres)
        Me.XtraTabPage1.Controls.Add(Me.deFechaIngreso)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl12)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl6)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl16)
        Me.XtraTabPage1.Controls.Add(Me.leProveedor)
        Me.XtraTabPage1.Controls.Add(Me.teExistenciaMinima)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl11)
        Me.XtraTabPage1.Controls.Add(Me.teExistenciaMaxima)
        Me.XtraTabPage1.Controls.Add(Me.leUnidadMedida)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl10)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl13)
        Me.XtraTabPage1.Controls.Add(Me.ceProductoEsp)
        Me.XtraTabPage1.Controls.Add(Me.cePermiteFact)
        Me.XtraTabPage1.Controls.Add(Me.ceExento)
        Me.XtraTabPage1.Controls.Add(Me.leUbicacion)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1247, 531)
        Me.XtraTabPage1.Text = "Producto"
        '
        'teCuantia
        '
        Me.teCuantia.EnterMoveNextControl = True
        Me.teCuantia.Location = New System.Drawing.Point(757, 219)
        Me.teCuantia.Name = "teCuantia"
        Me.teCuantia.Size = New System.Drawing.Size(148, 20)
        Me.teCuantia.TabIndex = 22
        Me.teCuantia.Visible = False
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(713, 222)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl29.TabIndex = 155
        Me.LabelControl29.Text = "Cuantía:"
        Me.LabelControl29.Visible = False
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(716, 116)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl26.TabIndex = 153
        Me.LabelControl26.Text = "Origen:"
        '
        'teOrigen
        '
        Me.teOrigen.EnterMoveNextControl = True
        Me.teOrigen.Location = New System.Drawing.Point(757, 113)
        Me.teOrigen.Name = "teOrigen"
        Me.teOrigen.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teOrigen.Size = New System.Drawing.Size(148, 20)
        Me.teOrigen.TabIndex = 8
        '
        'teTraduccion
        '
        Me.teTraduccion.EnterMoveNextControl = True
        Me.teTraduccion.Location = New System.Drawing.Point(179, 114)
        Me.teTraduccion.Name = "teTraduccion"
        Me.teTraduccion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teTraduccion.Size = New System.Drawing.Size(527, 20)
        Me.teTraduccion.TabIndex = 7
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(120, 118)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl7.TabIndex = 151
        Me.LabelControl7.Text = "Traducción:"
        '
        'sbCentroCosto
        '
        Me.sbCentroCosto.Location = New System.Drawing.Point(528, 239)
        Me.sbCentroCosto.Name = "sbCentroCosto"
        Me.sbCentroCosto.Size = New System.Drawing.Size(75, 23)
        Me.sbCentroCosto.TabIndex = 149
        Me.sbCentroCosto.Text = "Centro Costo"
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(93, 243)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl28.TabIndex = 148
        Me.LabelControl28.Text = "Centro de Costo:"
        '
        'leCentroCosto
        '
        Me.leCentroCosto.EnterMoveNextControl = True
        Me.leCentroCosto.Location = New System.Drawing.Point(179, 241)
        Me.leCentroCosto.Name = "leCentroCosto"
        Me.leCentroCosto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCentroCosto.Size = New System.Drawing.Size(341, 20)
        Me.leCentroCosto.TabIndex = 23
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(103, 11)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl27.TabIndex = 147
        Me.LabelControl27.Text = "Departamento:"
        '
        'leDepartamento
        '
        Me.leDepartamento.EnterMoveNextControl = True
        Me.leDepartamento.Location = New System.Drawing.Point(179, 7)
        Me.leDepartamento.Name = "leDepartamento"
        Me.leDepartamento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leDepartamento.Size = New System.Drawing.Size(283, 20)
        Me.leDepartamento.TabIndex = 0
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(459, 72)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(144, 13)
        Me.LabelControl25.TabIndex = 145
        Me.LabelControl25.Text = "Código de barra del producto:"
        '
        'teCodBarra
        '
        Me.teCodBarra.EnterMoveNextControl = True
        Me.teCodBarra.Location = New System.Drawing.Point(606, 70)
        Me.teCodBarra.Name = "teCodBarra"
        Me.teCodBarra.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teCodBarra.Size = New System.Drawing.Size(300, 20)
        Me.teCodBarra.TabIndex = 5
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(272, 221)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl24.TabIndex = 144
        Me.LabelControl24.Text = "Categoría f983:"
        '
        'leCategoria
        '
        Me.leCategoria.EnterMoveNextControl = True
        Me.leCategoria.Location = New System.Drawing.Point(351, 219)
        Me.leCategoria.Name = "leCategoria"
        Me.leCategoria.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCategoria.Size = New System.Drawing.Size(252, 20)
        Me.leCategoria.TabIndex = 21
        '
        'btnDetallarKit
        '
        Me.btnDetallarKit.Location = New System.Drawing.Point(179, 352)
        Me.btnDetallarKit.Name = "btnDetallarKit"
        Me.btnDetallarKit.Size = New System.Drawing.Size(167, 23)
        Me.btnDetallarKit.TabIndex = 143
        Me.btnDetallarKit.Text = "Detallar kit"
        Me.btnDetallarKit.UseVisualStyleBackColor = True
        '
        'chkCompuesto
        '
        Me.chkCompuesto.Location = New System.Drawing.Point(179, 287)
        Me.chkCompuesto.Name = "chkCompuesto"
        Me.chkCompuesto.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.chkCompuesto.Properties.Appearance.Options.UseFont = True
        Me.chkCompuesto.Properties.Caption = "PRODUCTO KIT(COMPUESTO)"
        Me.chkCompuesto.Size = New System.Drawing.Size(189, 19)
        Me.chkCompuesto.TabIndex = 142
        '
        'teEstilo
        '
        Me.teEstilo.EnterMoveNextControl = True
        Me.teEstilo.Location = New System.Drawing.Point(757, 198)
        Me.teEstilo.Name = "teEstilo"
        Me.teEstilo.Size = New System.Drawing.Size(148, 20)
        Me.teEstilo.TabIndex = 19
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(725, 201)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl22.TabIndex = 141
        Me.LabelControl22.Text = "Estilo:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(179, 91)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teNombre.Size = New System.Drawing.Size(726, 20)
        Me.teNombre.TabIndex = 6
        '
        'teColor
        '
        Me.teColor.EnterMoveNextControl = True
        Me.teColor.Location = New System.Drawing.Point(757, 177)
        Me.teColor.Name = "teColor"
        Me.teColor.Size = New System.Drawing.Size(148, 20)
        Me.teColor.TabIndex = 16
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(6, 32)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(170, 13)
        Me.LabelControl2.TabIndex = 93
        Me.LabelControl2.Text = "Grupo o Familia a la que pertenece:"
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(725, 180)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl15.TabIndex = 140
        Me.LabelControl15.Text = "Color:"
        '
        'leGrupo
        '
        Me.leGrupo.EnterMoveNextControl = True
        Me.leGrupo.Location = New System.Drawing.Point(179, 28)
        Me.leGrupo.Name = "leGrupo"
        Me.leGrupo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leGrupo.Size = New System.Drawing.Size(283, 20)
        Me.leGrupo.TabIndex = 1
        '
        'teTalla
        '
        Me.teTalla.EnterMoveNextControl = True
        Me.teTalla.Location = New System.Drawing.Point(757, 156)
        Me.teTalla.Name = "teTalla"
        Me.teTalla.Size = New System.Drawing.Size(148, 20)
        Me.teTalla.TabIndex = 13
        '
        'leSubGrupo
        '
        Me.leSubGrupo.EnterMoveNextControl = True
        Me.leSubGrupo.Location = New System.Drawing.Point(179, 49)
        Me.leSubGrupo.Name = "leSubGrupo"
        Me.leSubGrupo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSubGrupo.Size = New System.Drawing.Size(283, 20)
        Me.leSubGrupo.TabIndex = 2
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(728, 159)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl14.TabIndex = 139
        Me.LabelControl14.Text = "Talla:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(121, 53)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl3.TabIndex = 98
        Me.LabelControl3.Text = "Sub-Grupo:"
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(570, 139)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl23.TabIndex = 136
        Me.LabelControl23.Text = "Marca:"
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(139, 200)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl20.TabIndex = 135
        Me.LabelControl20.Text = "Estado:"
        '
        'teCodigo
        '
        Me.teCodigo.EnterMoveNextControl = True
        Me.teCodigo.Location = New System.Drawing.Point(179, 70)
        Me.teCodigo.Name = "teCodigo"
        Me.teCodigo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teCodigo.Size = New System.Drawing.Size(177, 20)
        Me.teCodigo.TabIndex = 4
        '
        'leMarca
        '
        Me.leMarca.EnterMoveNextControl = True
        Me.leMarca.Location = New System.Drawing.Point(606, 135)
        Me.leMarca.Name = "leMarca"
        Me.leMarca.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leMarca.Size = New System.Drawing.Size(299, 20)
        Me.leMarca.TabIndex = 10
        '
        'leEstado
        '
        Me.leEstado.EnterMoveNextControl = True
        Me.leEstado.Location = New System.Drawing.Point(179, 198)
        Me.leEstado.Name = "leEstado"
        Me.leEstado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leEstado.Size = New System.Drawing.Size(283, 20)
        Me.leEstado.TabIndex = 17
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(29, 73)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(147, 13)
        Me.LabelControl4.TabIndex = 100
        Me.LabelControl4.Text = "Código comercial del producto:"
        '
        'leTipoInventario
        '
        Me.leTipoInventario.EnterMoveNextControl = True
        Me.leTipoInventario.Location = New System.Drawing.Point(606, 49)
        Me.leTipoInventario.Name = "leTipoInventario"
        Me.leTipoInventario.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoInventario.Size = New System.Drawing.Size(299, 20)
        Me.leTipoInventario.TabIndex = 3
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(135, 93)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl5.TabIndex = 103
        Me.LabelControl5.Text = "Nombre:"
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(477, 52)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(126, 13)
        Me.LabelControl21.TabIndex = 134
        Me.LabelControl21.Text = "Tipo de Producto/Servicio:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(122, 180)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl8.TabIndex = 137
        Me.LabelControl8.Text = "Proveedor:"
        '
        'teUnidadesPres
        '
        Me.teUnidadesPres.EnterMoveNextControl = True
        Me.teUnidadesPres.Location = New System.Drawing.Point(606, 198)
        Me.teUnidadesPres.Name = "teUnidadesPres"
        Me.teUnidadesPres.Size = New System.Drawing.Size(100, 20)
        Me.teUnidadesPres.TabIndex = 18
        '
        'deFechaIngreso
        '
        Me.deFechaIngreso.EditValue = Nothing
        Me.deFechaIngreso.EnterMoveNextControl = True
        Me.deFechaIngreso.Location = New System.Drawing.Point(179, 219)
        Me.deFechaIngreso.Name = "deFechaIngreso"
        Me.deFechaIngreso.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaIngreso.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaIngreso.Size = New System.Drawing.Size(92, 20)
        Me.deFechaIngreso.TabIndex = 20
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(471, 201)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(132, 13)
        Me.LabelControl12.TabIndex = 129
        Me.LabelControl12.Text = "Unidades por Presentación:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(88, 222)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(88, 13)
        Me.LabelControl6.TabIndex = 107
        Me.LabelControl6.Text = "Fecha de Ingreso:"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(512, 161)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl16.TabIndex = 130
        Me.LabelControl16.Text = "Existencia Máxima:"
        '
        'leProveedor
        '
        Me.leProveedor.EnterMoveNextControl = True
        Me.leProveedor.Location = New System.Drawing.Point(179, 177)
        Me.leProveedor.Name = "leProveedor"
        Me.leProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leProveedor.Size = New System.Drawing.Size(283, 20)
        Me.leProveedor.TabIndex = 14
        '
        'teExistenciaMinima
        '
        Me.teExistenciaMinima.EnterMoveNextControl = True
        Me.teExistenciaMinima.Location = New System.Drawing.Point(606, 177)
        Me.teExistenciaMinima.Name = "teExistenciaMinima"
        Me.teExistenciaMinima.Size = New System.Drawing.Size(100, 20)
        Me.teExistenciaMinima.TabIndex = 15
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(87, 140)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl11.TabIndex = 106
        Me.LabelControl11.Text = "Unidad de Medida:"
        '
        'teExistenciaMaxima
        '
        Me.teExistenciaMaxima.EnterMoveNextControl = True
        Me.teExistenciaMaxima.Location = New System.Drawing.Point(606, 156)
        Me.teExistenciaMaxima.Name = "teExistenciaMaxima"
        Me.teExistenciaMaxima.Size = New System.Drawing.Size(100, 20)
        Me.teExistenciaMaxima.TabIndex = 12
        '
        'leUnidadMedida
        '
        Me.leUnidadMedida.EnterMoveNextControl = True
        Me.leUnidadMedida.Location = New System.Drawing.Point(179, 135)
        Me.leUnidadMedida.Name = "leUnidadMedida"
        Me.leUnidadMedida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leUnidadMedida.Size = New System.Drawing.Size(283, 20)
        Me.leUnidadMedida.TabIndex = 9
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(516, 180)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl10.TabIndex = 115
        Me.LabelControl10.Text = "Existencia Mínima:"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(78, 159)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(98, 13)
        Me.LabelControl13.TabIndex = 108
        Me.LabelControl13.Text = "Ubicación o Estante:"
        '
        'ceProductoEsp
        '
        Me.ceProductoEsp.Location = New System.Drawing.Point(179, 267)
        Me.ceProductoEsp.Name = "ceProductoEsp"
        Me.ceProductoEsp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ceProductoEsp.Properties.Appearance.Options.UseFont = True
        Me.ceProductoEsp.Properties.Caption = "PRODUCTO ESPECIAL"
        Me.ceProductoEsp.Size = New System.Drawing.Size(189, 19)
        Me.ceProductoEsp.TabIndex = 124
        '
        'cePermiteFact
        '
        Me.cePermiteFact.Location = New System.Drawing.Point(179, 327)
        Me.cePermiteFact.Name = "cePermiteFact"
        Me.cePermiteFact.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.cePermiteFact.Properties.Appearance.Options.UseFont = True
        Me.cePermiteFact.Properties.Caption = "PERMITE FACTURACIÓN?"
        Me.cePermiteFact.Size = New System.Drawing.Size(189, 19)
        Me.cePermiteFact.TabIndex = 123
        '
        'ceExento
        '
        Me.ceExento.Location = New System.Drawing.Point(179, 307)
        Me.ceExento.Name = "ceExento"
        Me.ceExento.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ceExento.Properties.Appearance.Options.UseFont = True
        Me.ceExento.Properties.Caption = "PRODUCTO EXENTO?"
        Me.ceExento.Size = New System.Drawing.Size(189, 19)
        Me.ceExento.TabIndex = 125
        '
        'leUbicacion
        '
        Me.leUbicacion.EnterMoveNextControl = True
        Me.leUbicacion.Location = New System.Drawing.Point(179, 156)
        Me.leUbicacion.Name = "leUbicacion"
        Me.leUbicacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leUbicacion.Size = New System.Drawing.Size(283, 20)
        Me.leUbicacion.TabIndex = 11
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.SplitContainerControl1)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1247, 531)
        Me.XtraTabPage2.Text = "Información Complementaria"
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.peFoto)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl19)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl18)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.meInfoAdicional)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.beImagen)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lcExisteArchivo)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.SplitContainerControl2)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1247, 531)
        Me.SplitContainerControl1.SplitterPosition = 400
        Me.SplitContainerControl1.TabIndex = 146
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'peFoto
        '
        Me.peFoto.Cursor = System.Windows.Forms.Cursors.Default
        Me.peFoto.Dock = System.Windows.Forms.DockStyle.Top
        Me.peFoto.Location = New System.Drawing.Point(0, 0)
        Me.peFoto.Name = "peFoto"
        Me.peFoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch
        Me.peFoto.Properties.ZoomAccelerationFactor = 1.0R
        Me.peFoto.Size = New System.Drawing.Size(400, 268)
        ToolTipTitleItem1.Text = "Imagen del producto"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        Me.peFoto.SuperTip = SuperToolTip1
        Me.peFoto.TabIndex = 40
        Me.peFoto.ToolTipTitle = "Doble clic para cambiar o cargar imagen"
        '
        'lcExisteArchivo
        '
        Me.lcExisteArchivo.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lcExisteArchivo.Appearance.Options.UseFont = True
        Me.lcExisteArchivo.Location = New System.Drawing.Point(11, 274)
        Me.lcExisteArchivo.Name = "lcExisteArchivo"
        Me.lcExisteArchivo.Size = New System.Drawing.Size(204, 13)
        Me.lcExisteArchivo.TabIndex = 139
        Me.lcExisteArchivo.Text = "-DOBLE CLIC PARA CARGAR IMAGEN-"
        '
        'BeCtaContable3
        '
        Me.BeCtaContable3.Location = New System.Drawing.Point(197, 77)
        Me.BeCtaContable3.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.BeCtaContable3.Name = "BeCtaContable3"
        Me.BeCtaContable3.Size = New System.Drawing.Size(617, 20)
        Me.BeCtaContable3.TabIndex = 2
        Me.BeCtaContable3.ValidarMayor = True
        '
        'BeCtaContable2
        '
        Me.BeCtaContable2.Location = New System.Drawing.Point(197, 56)
        Me.BeCtaContable2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.BeCtaContable2.Name = "BeCtaContable2"
        Me.BeCtaContable2.Size = New System.Drawing.Size(617, 20)
        Me.BeCtaContable2.TabIndex = 1
        Me.BeCtaContable2.ValidarMayor = True
        '
        'BeCtaContable1
        '
        Me.BeCtaContable1.Location = New System.Drawing.Point(197, 35)
        Me.BeCtaContable1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.BeCtaContable1.Name = "BeCtaContable1"
        Me.BeCtaContable1.Size = New System.Drawing.Size(616, 20)
        Me.BeCtaContable1.TabIndex = 0
        Me.BeCtaContable1.ValidarMayor = True
        '
        'lcCtaCosto
        '
        Me.lcCtaCosto.Location = New System.Drawing.Point(20, 80)
        Me.lcCtaCosto.Name = "lcCtaCosto"
        Me.lcCtaCosto.Size = New System.Drawing.Size(166, 13)
        Me.lcCtaCosto.TabIndex = 144
        Me.lcCtaCosto.Text = "Cuenta Contable p/Aplicar Costos:"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(20, 60)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(171, 13)
        Me.LabelControl9.TabIndex = 143
        Me.LabelControl9.Text = "Cta.Contable p/Inventario o Gasto:"
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(20, 38)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(161, 13)
        Me.LabelControl17.TabIndex = 145
        Me.LabelControl17.Text = "Cuenta Contable p/Aplicar Venta:"
        '
        'beImagen
        '
        Me.beImagen.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.beImagen.Location = New System.Drawing.Point(11, 321)
        Me.beImagen.Name = "beImagen"
        Me.beImagen.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beImagen.Size = New System.Drawing.Size(379, 20)
        Me.beImagen.TabIndex = 4
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(12, 302)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(155, 13)
        Me.LabelControl19.TabIndex = 39
        Me.LabelControl19.Text = "Carpeta y archivo de la Imagen:"
        '
        'meInfoAdicional
        '
        Me.meInfoAdicional.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.meInfoAdicional.EnterMoveNextControl = True
        Me.meInfoAdicional.Location = New System.Drawing.Point(12, 368)
        Me.meInfoAdicional.Name = "meInfoAdicional"
        Me.meInfoAdicional.Size = New System.Drawing.Size(378, 97)
        Me.meInfoAdicional.TabIndex = 3
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(12, 349)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl18.TabIndex = 38
        Me.LabelControl18.Text = "Información Adicional:"
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Location = New System.Drawing.Point(20, 19)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl30.TabIndex = 139
        Me.LabelControl30.Text = "CONTABILIDAD"
        '
        'SplitContainerControl2
        '
        Me.SplitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl2.Horizontal = False
        Me.SplitContainerControl2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl2.Name = "SplitContainerControl2"
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.LabelControl30)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.LabelControl17)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.BeCtaContable1)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.BeCtaContable3)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.lcCtaCosto)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.LabelControl9)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.BeCtaContable2)
        Me.SplitContainerControl2.Panel1.Text = "Panel1"
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.gcPr1)
        Me.SplitContainerControl2.Panel2.Text = "Panel2"
        Me.SplitContainerControl2.Size = New System.Drawing.Size(842, 531)
        Me.SplitContainerControl2.SplitterPosition = 214
        Me.SplitContainerControl2.TabIndex = 146
        Me.SplitContainerControl2.Text = "SplitContainerControl2"
        '
        'gcPr1
        '
        Me.gcPr1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcPr1.Location = New System.Drawing.Point(0, 0)
        Me.gcPr1.MainView = Me.gvPr1
        Me.gcPr1.Name = "gcPr1"
        Me.gcPr1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rileIdPrecio1})
        Me.gcPr1.Size = New System.Drawing.Size(842, 312)
        Me.gcPr1.TabIndex = 60
        Me.gcPr1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvPr1})
        '
        'gvPr1
        '
        Me.gvPr1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcId, Me.gcIdPrecio, Me.GridColumn9, Me.GridColumn1, Me.gcDescuento, Me.gcMargen})
        Me.gvPr1.GridControl = Me.gcPr1
        Me.gvPr1.Name = "gvPr1"
        Me.gvPr1.OptionsView.ShowGroupPanel = False
        '
        'gcId
        '
        Me.gcId.Caption = "IdPrecio"
        Me.gcId.FieldName = "IdPrecio"
        Me.gcId.Name = "gcId"
        Me.gcId.OptionsColumn.ReadOnly = True
        Me.gcId.Visible = True
        Me.gcId.VisibleIndex = 0
        Me.gcId.Width = 57
        '
        'gcIdPrecio
        '
        Me.gcIdPrecio.Caption = "Descripcion"
        Me.gcIdPrecio.ColumnEdit = Me.rileIdPrecio1
        Me.gcIdPrecio.FieldName = "IdPrecio"
        Me.gcIdPrecio.Name = "gcIdPrecio"
        Me.gcIdPrecio.OptionsColumn.AllowEdit = False
        Me.gcIdPrecio.OptionsColumn.ReadOnly = True
        Me.gcIdPrecio.Visible = True
        Me.gcIdPrecio.VisibleIndex = 1
        Me.gcIdPrecio.Width = 194
        '
        'rileIdPrecio1
        '
        Me.rileIdPrecio1.AutoHeight = False
        Me.rileIdPrecio1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rileIdPrecio1.Name = "rileIdPrecio1"
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Precio Venta"
        Me.GridColumn9.DisplayFormat.FormatString = "###,##0.0000"
        Me.GridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn9.FieldName = "Precio"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 2
        Me.GridColumn9.Width = 96
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Precio Neto"
        Me.GridColumn1.DisplayFormat.FormatString = "###,##0.0000"
        Me.GridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn1.FieldName = "PrecioNeto"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 3
        Me.GridColumn1.Width = 105
        '
        'gcDescuento
        '
        Me.gcDescuento.Caption = "Descuento (%)"
        Me.gcDescuento.DisplayFormat.FormatString = "###,##0.00"
        Me.gcDescuento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcDescuento.FieldName = "Descuento"
        Me.gcDescuento.Name = "gcDescuento"
        Me.gcDescuento.Visible = True
        Me.gcDescuento.VisibleIndex = 4
        Me.gcDescuento.Width = 82
        '
        'gcMargen
        '
        Me.gcMargen.Caption = "Margen (%)"
        Me.gcMargen.DisplayFormat.FormatString = "###,##0.00"
        Me.gcMargen.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcMargen.FieldName = "Margen"
        Me.gcMargen.Name = "gcMargen"
        Me.gcMargen.Visible = True
        Me.gcMargen.VisibleIndex = 5
        Me.gcMargen.Width = 96
        '
        'inv_frmProductos
        '
        Me.ClientSize = New System.Drawing.Size(1253, 612)
        Me.Controls.Add(Me.xtcProductos)
        Me.Controls.Add(Me.pcHeader)
        Me.Modulo = "Inventario"
        Me.Name = "inv_frmProductos"
        Me.OptionId = "001004"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Productos y servicios"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.pcHeader, 0)
        Me.Controls.SetChildIndex(Me.xtcProductos, 0)
        CType(Me.myStyle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        CType(Me.xtcProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcProductos.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.XtraTabPage1.PerformLayout()
        CType(Me.teCuantia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teOrigen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTraduccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCentroCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leDepartamento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCodBarra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCategoria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCompuesto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teEstilo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teColor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTalla.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSubGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leMarca.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoInventario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teUnidadesPres.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaIngreso.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaIngreso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teExistenciaMinima.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teExistenciaMaxima.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leUnidadMedida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceProductoEsp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cePermiteFact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceExento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leUbicacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.peFoto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beImagen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meInfoAdicional.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl2.ResumeLayout(False)
        CType(Me.gcPr1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPr1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rileIdPrecio1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OpenFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents myStyle As DevExpress.XtraEditors.StyleController
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents dn As DevExpress.XtraEditors.DataNavigator
    Friend WithEvents xtcProductos As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents sbCentroCosto As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCentroCosto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leDepartamento As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCodBarra As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCategoria As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents btnDetallarKit As System.Windows.Forms.Button
    Friend WithEvents chkCompuesto As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents teEstilo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teColor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leGrupo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teTalla As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leSubGrupo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leMarca As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leEstado As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoInventario As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teUnidadesPres As DevExpress.XtraEditors.TextEdit
    Friend WithEvents deFechaIngreso As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leProveedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teExistenciaMinima As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teExistenciaMaxima As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leUnidadMedida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceProductoEsp As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cePermiteFact As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceExento As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents leUbicacion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents BeCtaContable3 As Nexus.beCtaContable
    Friend WithEvents BeCtaContable2 As Nexus.beCtaContable
    Friend WithEvents BeCtaContable1 As Nexus.beCtaContable
    Friend WithEvents lcCtaCosto As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lcExisteArchivo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents peFoto As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents beImagen As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meInfoAdicional As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCuantia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teOrigen As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teTraduccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SplitContainerControl2 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents gcPr1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvPr1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rileIdPrecio1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcMargen As DevExpress.XtraGrid.Columns.GridColumn
End Class
