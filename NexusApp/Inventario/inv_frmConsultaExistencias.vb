Public Class inv_frmConsultaExistencias

    Private Sub inv_frmConsultaExistencias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gcEx.DataSource = New DataView(dtSaldos)
    End Sub

    Private _dtSaldos As DataTable
    Public Property dtSaldos() As DataTable
        Get
            Return _dtSaldos
        End Get
        Set(ByVal value As DataTable)
            _dtSaldos = value
        End Set
    End Property
End Class