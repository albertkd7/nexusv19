﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmGrupos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.seUlt = New DevExpress.XtraEditors.SpinEdit()
        Me.teCta05 = New DevExpress.XtraEditors.TextEdit()
        Me.teCta04 = New DevExpress.XtraEditors.TextEdit()
        Me.teCta03 = New DevExpress.XtraEditors.TextEdit()
        Me.teCta02 = New DevExpress.XtraEditors.TextEdit()
        Me.teCta01 = New DevExpress.XtraEditors.TextEdit()
        Me.beCta05 = New DevExpress.XtraEditors.ButtonEdit()
        Me.beCta04 = New DevExpress.XtraEditors.ButtonEdit()
        Me.beCta03 = New DevExpress.XtraEditors.ButtonEdit()
        Me.beCta02 = New DevExpress.XtraEditors.ButtonEdit()
        Me.beCta01 = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teId = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.leCategoria = New DevExpress.XtraEditors.LookUpEdit()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCategoria = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.seUlt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta05.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta04.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta03.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCta01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta05.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta04.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta03.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta02.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCta01.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCategoria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCategoria, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.gc)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(471, 469)
        Me.PanelControl1.TabIndex = 4
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(2, 2)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteCategoria})
        Me.gc.Size = New System.Drawing.Size(467, 465)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn8})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdGrupo"
        Me.GridColumn1.FieldName = "IdGrupo"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 67
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombre"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 155
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Ult. Producto Ingresado"
        Me.GridColumn8.FieldName = "UltCodProducto"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 3
        Me.GridColumn8.Width = 125
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl9)
        Me.PanelControl2.Controls.Add(Me.leCategoria)
        Me.PanelControl2.Controls.Add(Me.LabelControl8)
        Me.PanelControl2.Controls.Add(Me.LabelControl7)
        Me.PanelControl2.Controls.Add(Me.LabelControl6)
        Me.PanelControl2.Controls.Add(Me.LabelControl5)
        Me.PanelControl2.Controls.Add(Me.LabelControl4)
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.seUlt)
        Me.PanelControl2.Controls.Add(Me.teCta05)
        Me.PanelControl2.Controls.Add(Me.teCta04)
        Me.PanelControl2.Controls.Add(Me.teCta03)
        Me.PanelControl2.Controls.Add(Me.teCta02)
        Me.PanelControl2.Controls.Add(Me.teCta01)
        Me.PanelControl2.Controls.Add(Me.beCta05)
        Me.PanelControl2.Controls.Add(Me.beCta04)
        Me.PanelControl2.Controls.Add(Me.beCta03)
        Me.PanelControl2.Controls.Add(Me.beCta02)
        Me.PanelControl2.Controls.Add(Me.beCta01)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.teId)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.teNombre)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(471, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(830, 469)
        Me.PanelControl2.TabIndex = 5
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(12, 354)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(180, 13)
        Me.LabelControl8.TabIndex = 52
        Me.LabelControl8.Text = "Productos Ingresados en este Grupo:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(12, 309)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(151, 13)
        Me.LabelControl7.TabIndex = 51
        Me.LabelControl7.Text = "Cuenta Contable Devoluciones:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(12, 268)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(127, 13)
        Me.LabelControl6.TabIndex = 50
        Me.LabelControl6.Text = "Cuenta Contable Rebajas:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(12, 225)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(121, 13)
        Me.LabelControl5.TabIndex = 49
        Me.LabelControl5.Text = "Cuenta Contable Costos:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(12, 185)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(143, 13)
        Me.LabelControl4.TabIndex = 48
        Me.LabelControl4.Text = "Cuenta Contable Inventarios:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(12, 142)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(130, 13)
        Me.LabelControl3.TabIndex = 47
        Me.LabelControl3.Text = "Cuenta Contable Ingresos:"
        '
        'seUlt
        '
        Me.seUlt.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seUlt.EnterMoveNextControl = True
        Me.seUlt.Location = New System.Drawing.Point(12, 373)
        Me.seUlt.Name = "seUlt"
        Me.seUlt.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seUlt.Properties.Mask.EditMask = "n0"
        Me.seUlt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seUlt.Size = New System.Drawing.Size(160, 20)
        Me.seUlt.TabIndex = 46
        '
        'teCta05
        '
        Me.teCta05.Enabled = False
        Me.teCta05.Location = New System.Drawing.Point(178, 328)
        Me.teCta05.Name = "teCta05"
        Me.teCta05.Size = New System.Drawing.Size(331, 20)
        Me.teCta05.TabIndex = 45
        '
        'teCta04
        '
        Me.teCta04.Enabled = False
        Me.teCta04.Location = New System.Drawing.Point(178, 283)
        Me.teCta04.Name = "teCta04"
        Me.teCta04.Size = New System.Drawing.Size(331, 20)
        Me.teCta04.TabIndex = 43
        '
        'teCta03
        '
        Me.teCta03.Enabled = False
        Me.teCta03.Location = New System.Drawing.Point(178, 242)
        Me.teCta03.Name = "teCta03"
        Me.teCta03.Size = New System.Drawing.Size(331, 20)
        Me.teCta03.TabIndex = 42
        '
        'teCta02
        '
        Me.teCta02.Enabled = False
        Me.teCta02.Location = New System.Drawing.Point(178, 200)
        Me.teCta02.Name = "teCta02"
        Me.teCta02.Size = New System.Drawing.Size(331, 20)
        Me.teCta02.TabIndex = 39
        '
        'teCta01
        '
        Me.teCta01.Enabled = False
        Me.teCta01.Location = New System.Drawing.Point(178, 160)
        Me.teCta01.Name = "teCta01"
        Me.teCta01.Size = New System.Drawing.Size(331, 20)
        Me.teCta01.TabIndex = 37
        '
        'beCta05
        '
        Me.beCta05.Location = New System.Drawing.Point(12, 328)
        Me.beCta05.Name = "beCta05"
        Me.beCta05.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta05.Size = New System.Drawing.Size(160, 20)
        Me.beCta05.TabIndex = 44
        '
        'beCta04
        '
        Me.beCta04.Location = New System.Drawing.Point(12, 283)
        Me.beCta04.Name = "beCta04"
        Me.beCta04.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta04.Size = New System.Drawing.Size(160, 20)
        Me.beCta04.TabIndex = 41
        '
        'beCta03
        '
        Me.beCta03.Location = New System.Drawing.Point(12, 242)
        Me.beCta03.Name = "beCta03"
        Me.beCta03.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta03.Size = New System.Drawing.Size(160, 20)
        Me.beCta03.TabIndex = 40
        '
        'beCta02
        '
        Me.beCta02.Location = New System.Drawing.Point(12, 200)
        Me.beCta02.Name = "beCta02"
        Me.beCta02.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta02.Size = New System.Drawing.Size(160, 20)
        Me.beCta02.TabIndex = 38
        '
        'beCta01
        '
        Me.beCta01.Location = New System.Drawing.Point(12, 160)
        Me.beCta01.Name = "beCta01"
        Me.beCta01.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCta01.Size = New System.Drawing.Size(160, 20)
        Me.beCta01.TabIndex = 36
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(12, 20)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl1.TabIndex = 34
        Me.LabelControl1.Text = "Id Grupo:"
        '
        'teId
        '
        Me.teId.EnterMoveNextControl = True
        Me.teId.Location = New System.Drawing.Point(12, 39)
        Me.teId.Name = "teId"
        Me.teId.Size = New System.Drawing.Size(95, 20)
        Me.teId.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(12, 64)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 35
        Me.LabelControl2.Text = "Nombre:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(12, 78)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(497, 20)
        Me.teNombre.TabIndex = 1
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(12, 102)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl9.TabIndex = 54
        Me.LabelControl9.Text = "Categoría:"
        '
        'leCategoria
        '
        Me.leCategoria.EnterMoveNextControl = True
        Me.leCategoria.Location = New System.Drawing.Point(12, 118)
        Me.leCategoria.Name = "leCategoria"
        Me.leCategoria.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCategoria.Size = New System.Drawing.Size(497, 20)
        Me.leCategoria.TabIndex = 53
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Categoría"
        Me.GridColumn3.ColumnEdit = Me.riteCategoria
        Me.GridColumn3.FieldName = "IdCategoria"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 102
        '
        'riteCategoria
        '
        Me.riteCategoria.AutoHeight = False
        Me.riteCategoria.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteCategoria.Name = "riteCategoria"
        '
        'inv_frmGrupos
        '
        Me.ClientSize = New System.Drawing.Size(1301, 494)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.DbMode = 1
        Me.Modulo = "Inventario"
        Me.Name = "inv_frmGrupos"
        Me.OptionId = "001001"
        Me.Text = "Grupos"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.PanelControl2, 0)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.seUlt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta05.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta04.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta03.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCta01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta05.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta04.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta03.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta02.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCta01.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCategoria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCategoria, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teId As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teCta05 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCta04 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCta03 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCta02 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCta01 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCta05 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beCta04 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beCta03 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beCta02 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents beCta01 As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seUlt As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCategoria As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCategoria As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit

End Class
