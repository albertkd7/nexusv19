﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class inv_frmCategorias
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim entCategoria As inv_Categorias
    Dim entCategoriaDetalle As List(Of inv_CategoriasDetalle)
    Dim entCuentas As con_Cuentas

    Private Sub com_frmImpuestos_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub com_frmImpuestos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        gc.DataSource = objTablas.inv_CategoriasSelectAll
        gcSucursales.DataSource = bl.inv_ObtenerSucursalesCategorias(-1)
        entCategoria = objTablas.inv_CategoriasSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCategoria"))
        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub com_frmImpuestos_Nuevo_Click() Handles Me.Nuevo
        entCategoria = New inv_Categorias
        entCategoria.IdCategoria = objFunciones.ObtenerUltimoId("INV_CATEGORIAS", "IdCategoria") + 1
        CargaPantalla()
        ActivaControles(True)

    End Sub
    Private Sub com_frmImpuestos_Save_Click() Handles Me.Guardar

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            bl.inv_InsertarCategoria(entCategoria, entCategoriaDetalle)
        Else
            bl.inv_ActualizarCategoria(entCategoria, entCategoriaDetalle)
        End If
        teIdCategoria.EditValue = entCategoria.IdCategoria
        gc.DataSource = objTablas.inv_CategoriasSelectAll
        entCategoria = objTablas.inv_CategoriasSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCategoria"))
        CargaPantalla()
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub com_frmImpuestos_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el item seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.inv_CategoriasDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.inv_CategoriasSelectAll
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL IMPUESTO:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub com_frmImpuestos_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entCategoria
            teIdCategoria.EditValue = .IdCategoria
            teNombre.EditValue = .Nombre
            gcSucursales.DataSource = bl.inv_ObtenerSucursalesCategorias(.IdCategoria)
        End With
        teNombre.Focus()

    End Sub
    Private Sub CargaEntidad()
        With entCategoria
            .IdCategoria = teIdCategoria.EditValue
            .Nombre = teNombre.EditValue
        End With


        entCategoriaDetalle = New List(Of inv_CategoriasDetalle)
        For i = 0 To gvSucursales.DataRowCount - 1
            Dim entDetalle As New inv_CategoriasDetalle
            With entDetalle
                .IdCategoria = teIdCategoria.EditValue
                .IdSucursal = gvSucursales.GetRowCellValue(i, "IdSucursal")
                .IdCuentaIngreso = gvSucursales.GetRowCellValue(i, "IdCuentaIngreso")
            End With
            entCategoriaDetalle.Add(entDetalle)
        Next
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entCategoria = objTablas.inv_CategoriasSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCategoria"))
        CargaPantalla()
    End Sub

    Private Sub facVendedores_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teIdCategoria.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub

    Private Sub riteIdCuenta_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles riteIdCuenta.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            If gvSucursales.FocusedColumn.FieldName = "IdCuentaIngreso" Then
                Dim entCuenta As con_Cuentas
                If SiEsNulo(gvSucursales.EditingValue, "") = "" Then
                    entCuenta = objConsultas.cnsCuentas(con_frmConsultaCuentas, SiEsNulo(gvSucursales.EditingValue, ""))
                    gvSucursales.EditingValue = entCuenta.IdCuenta
                Else
                    entCuenta = objTablas.con_CuentasSelectByPK(SiEsNulo(gvSucursales.EditingValue, ""))
                End If
                entCuenta = objTablas.con_CuentasSelectByPK(entCuenta.IdCuenta)
                If Not entCuenta.EsTransaccional And entCuenta.IdCuenta <> "" Then
                    MsgBox("La cuenta no permite aceptar transacciones" & Chr(13) & _
                           "Es de mayor o tiene subcuentas", 64, "Nota")
                    gvSucursales.SetFocusedRowCellValue("IdCuentaIngreso", "")
                Else
                    gvSucursales.SetFocusedRowCellValue("IdCuentaIngreso", entCuenta.IdCuenta)
                End If


            End If
        End If
    End Sub

    'Private Sub riteIdCuenta_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles riteIdCuenta.Validating
    '    gvSucursales.EditingValue = SiEsNulo(gvSucursales.EditingValue, "")
    '    Dim entCuenta As con_Cuentas = objConsultas.cnsCuentas(frmConsultas, gvSucursales.ActiveEditor.EditValue) 'CORREGIR
    '    If Not entCuenta.EsTransaccional And entCuenta.IdCuenta <> "" Then
    '        MsgBox("La cuenta no permite aceptar transacciones" & Chr(13) & _
    '               "Es de mayor o tiene subcuentas", 64, "Nota")
    '        e.Cancel = True
    '    End If

    '    gvSucursales.ActiveEditor.EditValue = entCuenta.IdCuenta
    'End Sub
End Class
