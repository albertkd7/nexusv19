﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns
Imports System.IO


Public Class inv_frmTraslados
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim entProducto As inv_Productos
    Dim Header As New inv_Traslados
    Dim Detalle As List(Of inv_TrasladosDetalle)
    Dim dt As DataTable = blAdmon.ObtieneParametros()
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim IdSucUser As Integer = SiEsNulo(EntUsuario.IdSucursal, 1)
    Dim dtParametros As DataTable = blAdmon.ObtieneParametros()
    Dim AutorizoReversion As String = "", Existencia As Decimal
    Dim _dtPedidos As DataTable

    Property dtPedidos() As DataTable
        Get
            Return _dtPedidos
        End Get
        Set(ByVal value As DataTable)
            _dtPedidos = value
        End Set
    End Property

    Private Sub inv_frmTraslados_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = bl.inv_ConsultaTraslados(objMenu.User).Tables(0)
        gv2.BestFitColumns()
    End Sub

    Private Sub inv_frmTraslados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.inv_TiposTraslado(leTipoTransaccion, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_SucursalesFac(LeSucuralRecibe, "")
        objCombos.adm_SucursalesFac(LeSucursalRecibe2, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        objCombos.inv_Bodegas(leBodegaSalida, "")
        objCombos.inv_Bodegas(leBodegaIngreso, "")
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("INV_TRASLADOS", "IdComprobante")
        ActivarControles(False)
        Me.gcDescripcion.OptionsColumn.ReadOnly = Not SiEsNulo(EntUsuario.AccesoProductos, True)

        gc2.DataSource = bl.inv_ConsultaTraslados(objMenu.User).Tables(0)

        If gsNombre_Empresa.StartsWith("CARBAZEL") Then
            If SiEsNulo(EntUsuario.IdBodega, 0) <> piIdBodega Then 'si no es la bodega princiapal se bloquean
                leBodegaIngreso.Enabled = False
                leBodegaSalida.Enabled = False
                gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None
            End If
        End If

        RefrescaGrid()
    End Sub
    Private Sub inv_frmTraslados_Nuevo() Handles MyBase.Nuevo
        Header = New inv_Traslados
        ActivarControles(True)
        gc.DataSource = bl.inv_ObtenerDetalleDocumento(-1, "inv_TrasladosDetalle")
        gv.CancelUpdateCurrentRow()
        'gv.AddNewRow()

        teNumero.EditValue = ""
        deFecha.EditValue = Today
        meConcepto.EditValue = ""
        leBodegaIngreso.EditValue = 1
        LeSucuralRecibe.EditValue = 1
        leBodegaSalida.EditValue = SiEsNulo(EntUsuario.IdBodega, 1)
        leTipoTransaccion.EditValue = 14
        leSucursal.EditValue = piIdSucursalUsuario
        ceImprimir.EditValue = False
        btAplicar.Enabled = False
        teIdPedido.EditValue = 0
        teNumPedido.EditValue = ""


        Dim UltimoNumero As Integer = SiEsNulo(blAdmon.ObtieneCorrelativos("TRASLADOS_INV"), 0) + 1
        teNumero.EditValue = UltimoNumero.ToString.PadLeft(6, "0")

        teNumero.Focus()
        xtcTraslados.SelectedTabPage = xtpDatos
        RefrescaGrid()
    End Sub
    Private Sub inv_frmTraslados_Guardar() Handles MyBase.Guardar
        Dim msj As String = "", Existencia As Decimal = 0.0, ExistenciaAct As Decimal = 0.0
        Dim dtExistencia As DataTable
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "IdProducto") = gv.GetRowCellValue(i + 1, "IdProducto") Then
                msj = "El producto " & gv.GetRowCellValue(i, "IdProducto") & ",  está repetido"
                Exit For
            End If
            dtExistencia = bl.inv_ObtieneExistenciaProductoTraslado(gv.GetRowCellValue(i, "IdProducto"), leBodegaSalida.EditValue, deFecha.EditValue)
            If dtExistencia.Rows.Count = 0 Then
                Existencia = 0
            Else
                Existencia = dtExistencia.Rows(0).Item("SaldoExistencias")
            End If
            dtExistencia = bl.inv_ObtieneExistenciaProductoTraslado(gv.GetRowCellValue(i, "IdProducto"), leBodegaSalida.EditValue, Today)
            If dtExistencia.Rows.Count = 0 Then
                ExistenciaAct = 0
            Else
                ExistenciaAct = dtExistencia.Rows(0).Item("SaldoExistencias")
            End If

            If dtParametros.Rows(0).Item("ValidarExistencias") = True Then
                If Existencia < gv.GetRowCellValue(i, "Cantidad") Then
                    msj = "El producto --> " & gv.GetRowCellValue(i, "IdProducto") & ", no tiene existencia suficiente a la fecha " + Format(deFecha.EditValue, "dd/MM/yyyy")
                    Exit For
                End If
                If deFecha.EditValue < Today Then
                    If Existencia < gv.GetRowCellValue(i, "Cantidad") Then
                        msj = "El producto --> " & gv.GetRowCellValue(i, "IdProducto") & ", no tiene existencia suficiente a la fecha " + Format(Today, "dd/MM/yyyy")
                        Exit For
                    End If
                End If
            End If

        Next
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.None

        If SiEsNulo(leSucursal.Text, "") = "" Or SiEsNulo(leBodegaSalida.EditValue, 0) = 0 Or SiEsNulo(leBodegaIngreso.EditValue, 0) = 0 Or SiEsNulo(leTipoTransaccion.Text, "") = "" Or SiEsNulo(leTipoTransaccion.EditValue, 0) = 0 Then
            msj = "Los datos de Bodega, Sucursal ó Tipo de Transacción no pueden quedar en blanco"
        End If

        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If
        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            MsgBox("La fecha del documento debe ser de un período válido", 16, "Error de datos")
            Exit Sub
        End If
        If teNumero.EditValue = "" Then
            MsgBox("Debes especificar un Numero de Comprobante", 16, "Error de datos")
            Exit Sub
        End If

        If leBodegaIngreso.EditValue = leBodegaSalida.EditValue Then
            MsgBox("La bodega de salida y entrada deben ser diferentes", 16, "Error de datos")
            Exit Sub
        End If
        If bl.ExisteNumeroTraslado(teNumero.EditValue) > 0 Then
            If DbMode = DbModeType.insert Then
                MsgBox("Ya existe un número de Traslado con el número asignado", MsgBoxStyle.Critical)
                If MsgBox("¿Está seguro(a) Tomar un nuevo Correlativo de Traslado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
                    Dim UltimoNumero As Integer
                    UltimoNumero = SiEsNulo(blAdmon.ObtieneCorrelativos("TRASLADOS_INV"), 0) + 1
                    teNumero.EditValue = UltimoNumero.ToString.PadLeft(6, "0")
                End If
                Exit Sub
            Else
                If bl.ExisteNumeroTraslado(teNumero.EditValue) <> teCorrelativo.EditValue Then
                    MsgBox("Ya existe un número de Traslado con el número asignado", MsgBoxStyle.Critical)
                    Exit Sub
                End If
            End If
        End If
        CargarEntidades()
        If DbMode = DbModeType.insert Then
            msj = bl.inv_InsertarTraslado(Header, Detalle)

            If msj = "" Then
                MsgBox("El comprobante ha sido registrado con éxito", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox("SE DETECTÓ UN ERROR AL CREAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If
            teNumero.EditValue = Header.Numero
        Else
            msj = bl.inv_ActualizarTraslado(Header, Detalle)
            If msj = "" Then
                MsgBox("El documento ha sido actualizado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("SE DETECTÓ UN ERROR AL ACTUALIZAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If

        'xtcTraslados.SelectedTabPage = xtpLista
        gc2.DataSource = bl.inv_ConsultaTraslados(objMenu.User).Tables(0)
        'gc2.Focus()

        teCorrelativo.EditValue = Header.IdComprobante
        MostrarModoInicial()
        teCorrelativo.Focus()
        ActivarControles(False)
    End Sub
    Private Sub inv_frmTraslados_Editar() Handles MyBase.Editar
        If EntUsuario.IdSucursal <> Header.IdSucursal Then
            MsgBox("No puede editar ésta transacción." + Chr(13) + "No pertenece a la Sucursal de Envio ", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        If Header.AplicadaInventario Then
            MsgBox("No puede editar ésta transacción." + Chr(13) + "Debe revertirla antes de modificar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        ActivarControles(True)
        teNumero.Focus()
        RefrescaGrid()
    End Sub
    Private Sub inv_frmTraslados_Consulta() Handles MyBase.Consulta
        'teCorrelativo.EditValue = objConsultas.ConsultaTrasladosInv(frmConsultaDetalle)
        'CargaControles(0)
    End Sub
    Private Sub inv_frmTraslados_Revertir() Handles MyBase.Revertir
        xtcTraslados.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
        RefrescaGrid()
    End Sub
    Private Sub inv_frmTraslados_Eliminar() Handles MyBase.Eliminar
        Header = objTablas.inv_TrasladosSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))

        If Header.AplicadaInventario Then
            MsgBox("Es necesario que revierta la transacción. No se puede eliminar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de eliminar la transacción?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Header.ModificadoPor = objMenu.User
            objTablas.inv_TrasladosUpdate(Header)

            objTablas.inv_TrasladosDeleteByPK(Header.IdComprobante)
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("INV_TRASLADOS", "IdComprobante")

            ActivarControles(False)

            xtcTraslados.SelectedTabPage = xtpLista
            gc2.DataSource = bl.inv_ConsultaTraslados(objMenu.User).Tables(0)
            gc2.Focus()
        End If
    End Sub

#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", 0.0)
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), "") = "" Then
            e.Valid = False
        End If
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Dim Cantidad As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
        Dim PrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")
        Dim dtSaldo As DataTable = bl.inv_ObtieneExistenciaCostoProducto(SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), ""), leBodegaSalida.EditValue)
        Select Case gv.FocusedColumn.FieldName
            Case "Cantidad"
                Cantidad = e.Value
                ''gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", SiEsNulo(dtSaldo.Rows(0).Item("PrecioCosto"), 0.0))
                If dtParametros.Rows(0).Item("ValidarExistencias") = True Then
                    If dtSaldo.Rows.Count > 0 Then
                        If e.Value > dtSaldo.Rows(0).Item("SaldoExistencias") Then
                            MsgBox("Esta cantidad sobregira el inventario. No podrá guardar la factura", MsgBoxStyle.Critical, "Error")
                        End If
                    Else
                        MsgBox("Esta cantidad sobregira el inventario. No podrá guardar la factura", MsgBoxStyle.Critical, "Error")
                    End If
                End If
            Case "PrecioUnitario"
                PrecioUnitario = e.Value
        End Select

        Dim Total As Decimal = Decimal.Round(Cantidad * PrecioUnitario, 2)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", Total)
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gv.EditingValue = IdProd
                End If
                Dim dtSaldo As DataTable = bl.inv_ObtieneExistenciaCostoProducto(gv.EditingValue, leBodegaSalida.EditValue)
                If dtSaldo.Rows.Count > 0 Then
                    entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                    Existencia = CDec(bl.inv_ObtieneExistenciaProducto(entProducto.IdProducto, leBodegaSalida.EditValue, deFecha.EditValue).Rows(0).Item("SaldoExistencias"))
                    TeCodigoSaldo.EditValue = entProducto.IdProducto
                    teSaldo.EditValue = Format(Existencia, "###,##0.00")

                    If dtParametros.Rows(0).Item("AlertaMinimos") Then
                        If dtSaldo.Rows(0).Item("SaldoExistencias") < entProducto.ExistenciaMinima Then
                            MsgBox("El producto " + entProducto.Nombre + " se encuentra con existencia menor al mínimo", MsgBoxStyle.Critical, "Error")
                        End If
                    End If
                    gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", SiEsNulo(dtSaldo.Rows(0).Item("PrecioCosto"), 0.0))
                    Dim cantidad As Integer = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
                    Dim PrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")
                    Dim total As Decimal = Decimal.Round(cantidad * PrecioUnitario, 2)
                    gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", total)
                End If
                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                End If
            End If
        End If

    End Sub
#End Region


    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If

        Next
        'teNumero.Properties.ReadOnly = Not DbModeType.insert
        gv.OptionsBehavior.Editable = Tipo
        btAplicar.Enabled = Not Header.AplicadaInventario
        btRevertir.Enabled = Header.AplicadaInventario
    End Sub
    Private Sub CargarEntidades()
        With Header
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
            Else
                .ModificadoPor = objMenu.User
                .FechaHoraModificacion = Now
            End If
            .IdComprobante = teCorrelativo.EditValue  'el id se asignará en la capa de datos
            .IdTipoComprobante = leTipoTransaccion.EditValue
            .Numero = teNumero.EditValue
            .Fecha = deFecha.EditValue
            .Concepto = meConcepto.EditValue
            .IdBodegaSalida = leBodegaSalida.EditValue
            .IdBodegaIngreso = leBodegaIngreso.EditValue
            .IdSucursal = leSucursal.EditValue
            .IdSucursalRecibe = LeSucuralRecibe.EditValue
            .ImprimirValores = ceImprimir.EditValue
            .AplicadaInventario = False
            .IdPedido = teIdPedido.EditValue
        End With

        Detalle = New List(Of inv_TrasladosDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New inv_TrasladosDetalle
            With entDetalle
                .IdComprobante = Header.IdComprobante
                .IdDetalle = i + 1
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .PrecioTotal = gv.GetRowCellValue(i, "PrecioTotal")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            Detalle.Add(entDetalle)
        Next

    End Sub
    Private Sub sbImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbImportar.Click
        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            Return
        End If
        Dim csv_file As System.IO.StreamReader = File.OpenText(ofd.FileName)
        Try
            While csv_file.Peek() > 0
                Dim sLine As String = csv_file.ReadLine()
                Dim aData As Array = sLine.Split(",")
                gv.AddNewRow()
                gv.SetRowCellValue(gv.FocusedRowHandle, "IdProducto", aData(0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", aData(1))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", aData(2))
                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", SiEsNulo(aData(3), 0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", SiEsNulo(aData(4), 0))
                gv.UpdateCurrentRow()

            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Return
        End Try
        csv_file.Close()
    End Sub
    Private Sub inv_frmTraslados_Reporte() Handles Me.Reporte

        Header = objTablas.inv_TrasladosSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Dim entBodI As inv_Bodegas = objTablas.inv_BodegasSelectByPK(Header.IdBodegaIngreso)
        Dim entBodS As inv_Bodegas = objTablas.inv_BodegasSelectByPK(Header.IdBodegaSalida)
        Dim entTipos As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(Header.IdTipoComprobante)
        Dim dt As DataTable = bl.inv_ObtenerDetalleDocumento(Header.IdComprobante, "inv_TrasladosDetalle")

        Dim rpt As New inv_rptEntradaSalida() With {.DataSource = dt, .DataMember = ""}


        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "HOJA DE TRASLADO DE MERCADERÍA"
        rpt.xrlNumero.Text = Header.Numero
        rpt.xrlTipo.Text = entTipos.Nombre
        rpt.xrlConcepto.Text = Header.Concepto
        rpt.xrlFecha.Text = Header.Fecha
        rpt.xrlBodegaSal.Text = "BODEGA DE SALIDA:"
        rpt.xrlBodega.Text = entBodS.Nombre
        rpt.xrlBodegaIng.Text = "BODEGA DE INGRESO: " + entBodI.Nombre
        rpt.XrLSucEnvia.Text = leSucursal.Text
        rpt.XrLSucRecibe.Text = LeSucuralRecibe.Text

        If ceImprimir.EditValue = False Then
            rpt.XrLabel3.Visible = False
            rpt.XrLabel28.Visible = False
            rpt.xrlTotal.Visible = False
        End If
        rpt.ShowPreviewDialog()


    End Sub

    Private Sub btSaveAs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSaveAs.Click
        Dim Delimitador As String = InputBox("Deliminator del archivo:", "Especifique el delimitador", "|")
        If Delimitador = "" Then
            Return
        End If

        Dim fbd As New FolderBrowserDialog

        fbd.ShowDialog()
        If fbd.SelectedPath = "" Then
            Return
        End If

        Dim Archivo As String = String.Format("{0}\TRASLADO{1}{2}.txt", fbd.SelectedPath, teCorrelativo.EditValue, Format(deFecha.EditValue, "yyyyMMdd"))

        Try
            Using Arc As StreamWriter = New StreamWriter(Archivo)
                Dim linea As String = String.Empty

                With gv
                    For fila As Integer = 0 To .RowCount - 1
                        linea = String.Empty

                        For col As Integer = 0 To .Columns.Count - 1
                            linea &= gv.GetRowCellValue(fila, gv.Columns(col)) & Delimitador
                        Next

                        With Arc
                            linea = linea.Remove(linea.Length - 1).ToString
                            .WriteLine(linea.ToString)
                        End With
                    Next
                End With
            End Using

            Process.Start(Archivo)

            'si se genera un error
        Catch ex As Exception
            MsgBox("NO SE PUDO GUARDAR EL ARCHIVO" + Chr(13) + ex.Message.ToString, MsgBoxStyle.Critical, "Nota")
        End Try
    End Sub

    Private Sub btAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAplicar.Click
        'If meConcepto.Properties.ReadOnly = False Then
        '    MsgBox("Debe guardar antes el documento", MsgBoxStyle.Exclamation, "Error")
        '    Exit Sub
        'End If
        'If EntUsuario.IdSucursal <> Header.IdSucursal Then
        '    MsgBox("No puede editar ésta transacción." + Chr(13) + "No pertenece a la Sucursal de Envio ", MsgBoxStyle.Exclamation, "Nota")
        '    MostrarModoInicial()
        '    Exit Sub
        'End If
        If meConcepto.Properties.ReadOnly = False Then
            MsgBox("Debe guardar antes el documento", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de aplicar al inventario la transacción?" + Chr(13) + "YA NO SE PODRÁ EDITAR", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        Header = objTablas.inv_TrasladosSelectByPK(teCorrelativo.EditValue)
        If Header.AplicadaInventario Then
            MsgBox("Esta transacción ya fue aplicada al inventario", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If

        ' VALIDO LA  EXISTENCIA EN EL MOMENTO QUE DECIDE APLICAR LA OPERACION AL INVENTARIO
        Dim msj1 As String = ""
        Dim Existencia As Decimal = 0.0, ExistenciaAct As Decimal = 0.0
        Dim dtExistencia As DataTable

        For i = 0 To gv.DataRowCount - 1
            dtExistencia = bl.inv_ObtieneExistenciaProductoTraslado(gv.GetRowCellValue(i, "IdProducto"), leBodegaSalida.EditValue, deFecha.EditValue)
            If dtExistencia.Rows.Count = 0 Then
                Existencia = 0
            Else
                Existencia = dtExistencia.Rows(0).Item("SaldoExistencias")
            End If
            dtExistencia = bl.inv_ObtieneExistenciaProductoTraslado(gv.GetRowCellValue(i, "IdProducto"), leBodegaSalida.EditValue, Today)
            If dtExistencia.Rows.Count = 0 Then
                ExistenciaAct = 0
            Else
                ExistenciaAct = dtExistencia.Rows(0).Item("SaldoExistencias")
            End If
            If dtParametros.Rows(0).Item("ValidarExistencias") = True Then
                If Existencia < gv.GetRowCellValue(i, "Cantidad") Then
                    msj1 = "El producto --> " & gv.GetRowCellValue(i, "IdProducto") & ", no tiene existencia suficiente a la fecha " + Format(deFecha.EditValue, "dd/MM/yyyy")
                    Exit For
                End If
                If deFecha.EditValue < Today Then
                    If Existencia < gv.GetRowCellValue(i, "Cantidad") Then
                        msj1 = "El producto --> " & gv.GetRowCellValue(i, "IdProducto") & ", no tiene existencia suficiente a la fecha " + Format(Today, "dd/MM/yyyy")
                        Exit For
                    End If
                End If
            End If
        Next

        If msj1 <> "" Then
            MsgBox(msj1, MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If

        Dim msj As String = bl.inv_AplicarTransaccionInventario(Header.IdComprobante, "Traslado", "I", Header.IdTipoComprobante, 1)
        If msj = "Ok" Then
            MsgBox("La transacción ha sido aplicada con éxito. Ya no puede editarse", MsgBoxStyle.Information, "Nota")
            bl.inv_AutorizoReversionInventario(AutorizoReversion, "Traslados", Header.Numero, Header.IdComprobante)
            If Header.IdPedido > 0 Then
                bl.inv_ActualizaPedidoTraslado(Header.IdComprobante, Header.IdPedido, 1)
            End If
            Header.AplicadaInventario = True
            btAplicar.Enabled = False
            btRevertir.Enabled = True
        Else
            MsgBox("Sucedió algún error al aplicar" + Chr(13) + msj + "Favor reporte éste mensaje a ITO, S.A. DE C.V.", MsgBoxStyle.Critical, "Error")
        End If

        xtcTraslados.SelectedTabPage = xtpLista
        gc2.DataSource = bl.inv_ConsultaTraslados(objMenu.User).Tables(0)
        gc2.Focus()
    End Sub

    Private Sub btRevertir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRevertir.Click
        AutorizoReversion = ""
        If meConcepto.Properties.ReadOnly = False Then
            MsgBox("Debes Guardar antes la Traslado", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        If meConcepto.Properties.ReadOnly = False Then
            MsgBox("Deebes Guardar antes el Traslado", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If

        If MsgBox("¿Está seguro(a) de revertir la transacción?" + Chr(13) + "ESTO PUEDE DESESTABILIZAR SUS INVENTARIOS", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        If Not Header.AplicadaInventario Then
            MsgBox("La transacción NO ha sido aplicada aún", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim msj1 As String = "", lDummy As Boolean = False

        For i = 0 To gv.DataRowCount - 1
            If bl.inv_ObtieneExistenciaProductoTraslado(gv.GetRowCellValue(i, "IdProducto"), leBodegaIngreso.EditValue, deFecha.EditValue).Rows(0).Item("SaldoExistencias") - gv.GetRowCellValue(i, "Cantidad") < 0 Then
                If msj1 = "" Then
                    msj1 = "Productos que resultaran con Saldo Negativos en Fecha " + Format(deFecha.EditValue, "dd/MM/yyyy") + Chr(13)
                End If
                msj1 &= gv.GetRowCellValue(i, "IdProducto") + ","
            End If
        Next

        For i = 0 To gv.DataRowCount - 1
            If bl.inv_ObtieneExistenciaProductoTraslado(gv.GetRowCellValue(i, "IdProducto"), leBodegaIngreso.EditValue, Today).Rows(0).Item("SaldoExistencias") - gv.GetRowCellValue(i, "Cantidad") < 0 Then
                If Not lDummy Then
                    msj1 &= Chr(13) + "Productos que resultaran con Saldo Negativos en Fecha " + Format(Today, "dd/MM/yyyy") + Chr(13)
                End If
                msj1 &= gv.GetRowCellValue(i, "IdProducto") + ","
                lDummy = True
            End If
        Next

        If msj1 <> "" Then
            If MsgBox(msj1 + Chr(13) + "Desea Autorizar para continuar?", 292, "Confirme") = MsgBoxResult.No Then
                Exit Sub
            End If
            frmObtienePassword.Text = "AUTORIZACIÓN PARA REVERTIR INVENTARIOS"
            frmObtienePassword.Usuario = objMenu.User
            frmObtienePassword.TipoAcceso = 4
            frmObtienePassword.ShowDialog()

            If frmObtienePassword.Acceso = False Then
                MsgBox("Usuario no Autorizado para Revertir Inventarios", MsgBoxStyle.Critical, "Nota")
                Exit Sub
            End If
            AutorizoReversion = frmObtienePassword.Usuario
            frmObtienePassword.Dispose()
        End If

        Dim msj As String = bl.inv_AplicarTransaccionInventario(Header.IdComprobante, "Traslado", "D", Header.IdTipoComprobante, 0)
        If msj = "Ok" Then
            MsgBox("La transacción ha sido revertida con éxito. Ahora puede editarse", MsgBoxStyle.Information, "Nota")
            bl.inv_AutorizoReversionInventario(AutorizoReversion, "Revertir Traslado de ", Header.Numero, Header.IdComprobante)
            If Header.IdPedido > 0 Then
                bl.inv_ActualizaPedidoTraslado(Header.IdComprobante, Header.IdPedido, 0)
            End If

            Header.AplicadaInventario = False
            btAplicar.Enabled = True
            btRevertir.Enabled = False
        Else
            MsgBox("Sucedió algún error al revertir" + Chr(13) + msj + Chr(13) + "Favor reporte éste mensaje a IT O", MsgBoxStyle.Critical, "Error")
        End If
    End Sub


    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        Header = objTablas.inv_TrasladosSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        'DbMode = DbModeType.update
        teNumero.Focus()
    End Sub
    Private Sub CargaPantalla()
        If DbMode <> DbModeType.cargar Then
            Exit Sub
        End If
        xtcTraslados.SelectedTabPage = xtpDatos
        teNumero.Focus()

        With Header
            teCorrelativo.EditValue = .IdComprobante
            teNumero.EditValue = .Numero
            leTipoTransaccion.EditValue = .IdTipoComprobante
            deFecha.EditValue = .Fecha
            meConcepto.EditValue = .Concepto
            leBodegaSalida.EditValue = .IdBodegaSalida
            leBodegaIngreso.EditValue = .IdBodegaIngreso
            leSucursal.EditValue = .IdSucursal
            LeSucuralRecibe.EditValue = .IdSucursalRecibe
            ceImprimir.EditValue = .ImprimirValores
            teIdPedido.EditValue = .IdPedido
            gc.DataSource = bl.inv_ObtenerDetalleDocumento(.IdComprobante, "inv_TrasladosDetalle")
        End With
        btAplicar.Enabled = Not Header.AplicadaInventario
        btRevertir.Enabled = Header.AplicadaInventario
        RefrescaGrid()
    End Sub

    Private Sub sbPedido_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbPedido.Click
        If deFecha.Properties.ReadOnly Then
            Exit Sub
        End If

        _dtPedidos = bl.inv_ObtenerDetalleDocumento(-1, "inv_TrasladosDetalle")

        Dim frmTrasladosGarantia As New inv_frmTrasladosPedidos
        Me.AddOwnedForm(frmTrasladosGarantia)

        frmTrasladosGarantia.IdSucursal = piIdSucursalUsuario
        frmTrasladosGarantia.IdBodegaVenta = EntUsuario.IdBodega
        frmTrasladosGarantia.ShowDialog()

        teIdPedido.EditValue = frmTrasladosGarantia.IdComprobante
        teNumPedido.EditValue = frmTrasladosGarantia.Numero
        leBodegaSalida.EditValue = frmTrasladosGarantia.IdBodegaVenta
        Dim entUsuarioSolicito As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(frmTrasladosGarantia.CreadoPor)
        leBodegaIngreso.EditValue = SiEsNulo(entUsuarioSolicito.IdBodega, 0)
        gc.DataSource = _dtPedidos
        RefrescaGrid()
    End Sub
    Private Sub RefrescaGrid()
        Dim entPedidos As fac_Pedidos = objTablas.fac_PedidosSelectByPK(teIdPedido.EditValue)
        teNumPedido.EditValue = entPedidos.Numero

        leBodegaSalida.Enabled = Not teIdPedido.EditValue > 0
        leBodegaIngreso.Enabled = Not teIdPedido.EditValue > 0
        If teIdPedido.EditValue > 0 Then
            gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None
        Else
            gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        End If
        Me.gcIdProducto.OptionsColumn.ReadOnly = teIdPedido.EditValue > 0
        Me.gcDescripcion.OptionsColumn.ReadOnly = teIdPedido.EditValue > 0
        Me.gcPrecioUnitario.OptionsColumn.ReadOnly = teIdPedido.EditValue > 0
        Me.gcPrecioTotal.OptionsColumn.ReadOnly = teIdPedido.EditValue > 0

        'If gsNombre_Empresa.StartsWith("CARBAZEL") Then
        '    If SiEsNulo(EntUsuario.IdBodega, 0) <> piIdBodega Then 'si no es la bodega princiapal se bloquean
        '        leBodegaIngreso.Enabled = False
        '        leBodegaSalida.Enabled = False
        '        gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None
        '    End If
        'End If

    End Sub
End Class
