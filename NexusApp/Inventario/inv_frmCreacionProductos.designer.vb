﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmCreacionProductos
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btAplicar = New DevExpress.XtraEditors.SimpleButton()
        Me.sbImportar = New DevExpress.XtraEditors.SimpleButton()
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdProducto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcUnidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcExistencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDiferencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPrecioCosto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcGrupo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LeGrupo = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcSubGrupo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LeSubGrupo = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LeGrupo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LeSubGrupo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.sbImportar)
        Me.GroupControl1.Controls.Add(Me.btAplicar)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Size = New System.Drawing.Size(1067, 60)
        Me.GroupControl1.Text = ""
        '
        'btAplicar
        '
        Me.btAplicar.Image = Global.Nexus.My.Resources.Resources.autoriza3
        Me.btAplicar.Location = New System.Drawing.Point(184, 25)
        Me.btAplicar.Name = "btAplicar"
        Me.btAplicar.Size = New System.Drawing.Size(119, 23)
        Me.btAplicar.TabIndex = 10
        Me.btAplicar.Text = "Aplicar"
        '
        'sbImportar
        '
        Me.sbImportar.Image = Global.Nexus.My.Resources.Resources.bajar
        Me.sbImportar.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.sbImportar.Location = New System.Drawing.Point(25, 25)
        Me.sbImportar.Name = "sbImportar"
        Me.sbImportar.Size = New System.Drawing.Size(119, 23)
        Me.sbImportar.TabIndex = 86
        Me.sbImportar.Text = "Importar Archivo"
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdBorrar)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(1036, 60)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(31, 355)
        Me.pcBotones.TabIndex = 7
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(1, 75)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 23)
        Me.cmdBorrar.TabIndex = 34
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 60)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.LeGrupo, Me.LeSubGrupo})
        Me.gc.Size = New System.Drawing.Size(1036, 355)
        Me.gc.TabIndex = 8
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv, Me.GridView2})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdProducto, Me.gcDescripcion, Me.gcUnidad, Me.gcExistencia, Me.gcDiferencia, Me.gcPrecioCosto, Me.gcGrupo, Me.gcSubGrupo, Me.GridColumn1, Me.GridColumn2})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowAutoFilterRow = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdProducto
        '
        Me.gcIdProducto.Caption = "Codigo"
        Me.gcIdProducto.FieldName = "IdProducto"
        Me.gcIdProducto.Name = "gcIdProducto"
        Me.gcIdProducto.OptionsColumn.ReadOnly = True
        Me.gcIdProducto.Visible = True
        Me.gcIdProducto.VisibleIndex = 0
        Me.gcIdProducto.Width = 91
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Descripcion"
        Me.gcDescripcion.FieldName = "Nombre"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.OptionsColumn.ReadOnly = True
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 1
        Me.gcDescripcion.Width = 293
        '
        'gcUnidad
        '
        Me.gcUnidad.Caption = "Unidad M."
        Me.gcUnidad.FieldName = "UnidadMedida"
        Me.gcUnidad.Name = "gcUnidad"
        Me.gcUnidad.OptionsColumn.ReadOnly = True
        Me.gcUnidad.Visible = True
        Me.gcUnidad.VisibleIndex = 2
        Me.gcUnidad.Width = 79
        '
        'gcExistencia
        '
        Me.gcExistencia.Caption = "Precio 1"
        Me.gcExistencia.DisplayFormat.FormatString = "n2"
        Me.gcExistencia.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcExistencia.FieldName = "Precio1"
        Me.gcExistencia.Name = "gcExistencia"
        Me.gcExistencia.Visible = True
        Me.gcExistencia.VisibleIndex = 6
        Me.gcExistencia.Width = 70
        '
        'gcDiferencia
        '
        Me.gcDiferencia.Caption = "Precio Final"
        Me.gcDiferencia.DisplayFormat.FormatString = "n2"
        Me.gcDiferencia.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcDiferencia.FieldName = "PrecioFinal"
        Me.gcDiferencia.Name = "gcDiferencia"
        Me.gcDiferencia.Visible = True
        Me.gcDiferencia.VisibleIndex = 7
        Me.gcDiferencia.Width = 85
        '
        'gcPrecioCosto
        '
        Me.gcPrecioCosto.Caption = "Precio Costo"
        Me.gcPrecioCosto.DisplayFormat.FormatString = "n2"
        Me.gcPrecioCosto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioCosto.FieldName = "PrecioCosto"
        Me.gcPrecioCosto.Name = "gcPrecioCosto"
        Me.gcPrecioCosto.Visible = True
        Me.gcPrecioCosto.VisibleIndex = 5
        Me.gcPrecioCosto.Width = 91
        '
        'gcGrupo
        '
        Me.gcGrupo.Caption = "Grupo"
        Me.gcGrupo.ColumnEdit = Me.LeGrupo
        Me.gcGrupo.FieldName = "IdGrupo"
        Me.gcGrupo.Name = "gcGrupo"
        Me.gcGrupo.Visible = True
        Me.gcGrupo.VisibleIndex = 3
        '
        'LeGrupo
        '
        Me.LeGrupo.AutoHeight = False
        Me.LeGrupo.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LeGrupo.Name = "LeGrupo"
        '
        'gcSubGrupo
        '
        Me.gcSubGrupo.Caption = "SubGrupo"
        Me.gcSubGrupo.ColumnEdit = Me.LeSubGrupo
        Me.gcSubGrupo.FieldName = "IdSubGrupo"
        Me.gcSubGrupo.Name = "gcSubGrupo"
        Me.gcSubGrupo.Visible = True
        Me.gcSubGrupo.VisibleIndex = 4
        '
        'LeSubGrupo
        '
        Me.LeSubGrupo.AutoHeight = False
        Me.LeSubGrupo.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LeSubGrupo.Name = "LeSubGrupo"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Existe"
        Me.GridColumn1.FieldName = "Existe"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.gc
        Me.GridView2.Name = "GridView2"
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Precio Mayoreo"
        Me.GridColumn2.DisplayFormat.FormatString = "n2"
        Me.GridColumn2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn2.FieldName = "PrecioMayoreo"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 8
        '
        'inv_frmCreacionProductos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(1067, 440)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.pcBotones)
        Me.Modulo = "Inventario"
        Me.Name = "inv_frmCreacionProductos"
        Me.Text = "Creacion Productos Masivos"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.pcBotones, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LeGrupo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LeSubGrupo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btAplicar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbImportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcUnidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcExistencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDiferencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LeGrupo As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents gcSubGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LeSubGrupo As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn

End Class
