﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class inv_frmDetallarGastosProduccion
    Dim dtDetalle As New DataTable
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim dtGastosTMP As DataTable

    Private _IdProduccion As Integer
    Public Property IdProduccion() As Integer
        Get
            Return _IdProduccion
        End Get
        Set(ByVal value As Integer)
            _IdProduccion = value
        End Set
    End Property

    Private Sub com_frmDetallarGastosImportaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LlenarGastosNuevo()
    End Sub

    Private Sub LlenarEntidades()
        Dim frmProd As inv_frmProducciones = Me.Owner
        Dim dt As DataTable = frmProd.dtGastos

        For i = 0 To gv.DataRowCount - 1
            Dim dr As DataRow = frmProd.dtGastos.NewRow()
            dr("IdGasto") = gv.GetRowCellValue(i, "IdGasto")
            dr("Concepto") = gv.GetRowCellValue(i, "Concepto")
            dr("Valor") = gv.GetRowCellValue(i, "Valor")
            dr("IdProduccion") = gv.GetRowCellValue(i, "IdProduccion")
            dr("Contabilizar") = gv.GetRowCellValue(i, "Contabilizar")
            frmProd.dtGastos.Rows.Add(dr)
        Next

        Me.Close()
    End Sub

    Private Sub sbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGuardar.Click
        LlenarEntidades()
    End Sub

    Private Sub LlenarGastosNuevo()
        Dim frmProd As inv_frmProducciones = Me.Owner
        Dim dt As DataTable = frmProd.dtGastos

        If dt.Rows.Count > 0 Then
            gc.DataSource = dt
        Else
            gc.DataSource = bl.inv_ObtenerGastosProduccion(IdProduccion)
        End If
        ' se asigno este valor para el dt quede sin datos para que cuando se guardar aki lo rellene nuevamente
        frmProd.dtGastos = bl.inv_ObtenerGastosProduccion(-1)
    End Sub
End Class
