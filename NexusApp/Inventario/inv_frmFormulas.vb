﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns
Imports System.IO

Public Class inv_frmFormulas
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim entProducto As inv_Productos
    Dim Header As inv_Formulas
    Dim Detalle As List(Of inv_FormulasDetalle)

    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
    End Sub

    Public Sub CargaControles(ByVal TipoAvance As Integer)

        If TipoAvance = 0 Then 'no es necesario obtener el Id del comprobante
        Else
            teCorrelativo.EditValue = bl.inv_ObtenerIdDocumento(teCorrelativo.EditValue, TipoAvance, "INV_FORMULAS")
        End If
        If teCorrelativo.EditValue = 0 Then
            Exit Sub
        End If
        Header = objTablas.inv_FormulasSelectByPK(teCorrelativo.EditValue)
        If Header Is Nothing Then
            Exit Sub
        End If
        With Header
            teCorrelativo.EditValue = .IdFormula
            teCodigo.EditValue = .IdProducto
            teNombre.EditValue = .Nombre
            deFecha.EditValue = .Fecha
            meConcepto.EditValue = .Concepto
            gc.DataSource = bl.inv_ObtenerDetalleDocumentoFormula(.IdFormula, "inv_formulasDetalle")
        End With
    End Sub

    Private Sub inv_frmFormulas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("INV_FORMULAS", "IdFormula")
        CargaControles(0)
        ActivarControles(False)
    End Sub
    Private Sub inv_frmProducciones_Nuevo() Handles MyBase.Nuevo
        Header = New inv_Formulas
        ActivarControles(True)

        gc.DataSource = bl.inv_ObtenerDetalleDocumentoFormula(-1, "inv_FormulasDetalle")
        gv.CancelUpdateCurrentRow()
        gv.AddNewRow()

        teCorrelativo.EditValue = 0
        teCodigo.EditValue = ""
        teNombre.EditValue = ""
        deFecha.EditValue = Today
        meConcepto.EditValue = ""
        teCodigo.Focus()
    End Sub
    Private Sub inv_frmProducciones_Editar() Handles MyBase.Editar
        ActivarControles(True)
        teCodigo.Focus()
    End Sub
    Private Sub inv_frmProducciones_Consulta() Handles MyBase.Consulta
        teCorrelativo.EditValue = objConsultas.ConsultaFormulasInv(frmConsultaDetalle)
        CargaControles(0)
    End Sub
    Private Sub inv_frmProducciones_Revertir() Handles MyBase.Revertir
        ActivarControles(False)
    End Sub
    Private Sub inv_frmProducciones_Eliminar() Handles MyBase.Eliminar

        If MsgBox("¿Está seguro(a) de eliminar la transacción?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            objTablas.inv_FormulasDeleteByPK(Header.IdFormula)
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("INV_FORMULAS", "IdFormula")
            CargaControles(0)
            ActivarControles(False)
        End If
    End Sub
    Private Sub inv_frmProducciones_Guardar() Handles MyBase.Guardar
        Dim msj As String = ""

        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "IdProducto") = gv.GetRowCellValue(i + 1, "IdProducto") Then
                msj = "El producto " & gv.GetRowCellValue(i, "IdProducto") & ",  está repetido"
                Exit For
            End If

            ' si el producto es encabezado de formula
            If bl.inv_VerificaProductoFormula(gv.GetRowCellValue(i, "IdProducto"), 1) > 0 Then
                msj = "El producto " & gv.GetRowCellValue(i, "IdProducto") & ",  es producto terminado de producción"
                Exit For
            End If
        Next
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.None

        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If

        If teCodigo.EditValue = "" Then
            MsgBox("Debe especificar un código de producto para la formula", 16, "Error de datos")
            Exit Sub
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            MsgBox("La fecha del documento debe ser de un período válido", 16, "Error de datos")
            Exit Sub
        End If


        CargarEntidades()

        If DbMode = DbModeType.insert Then
            msj = bl.inv_InsertarFormulas(Header, Detalle)
            If msj = "" Then
                MsgBox("El comprobante ha sido registrado con éxito", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox("SE DETECTÓ UN ERROR AL CREAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If
        Else
            msj = bl.inv_ActualizarFormulas(Header, Detalle)
            If msj = "" Then
                MsgBox("El documento ha sido actualizado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("SE DETECTÓ UN ERROR AL ACTUALIZAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If
        teCorrelativo.EditValue = Header.IdFormula
        MostrarModoInicial()
        teCorrelativo.Focus()
        ActivarControles(False)
    End Sub


#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Nombre", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioCosto", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", 0.0)
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), "") = "" Then
            e.Valid = False
        End If
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Dim Cantidad As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
        Dim PrecioCosto As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioCosto")

        Select Case gv.FocusedColumn.FieldName
            Case "Cantidad"
                Cantidad = e.Value

            Case "PrecioCosto"
                PrecioCosto = e.Value
        End Select

        Dim Total As Decimal = Decimal.Round(Cantidad * PrecioCosto, 2)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", Total)
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab OrElse e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.Down OrElse e.KeyCode = Keys.Right OrElse e.KeyCode = Keys.Left Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gv.EditingValue = IdProd
                End If
                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                ' le e dejado 1 en el parametro de la bodega, solo para que asigne un costo nada mas, no afecta
                ' en el kardex tomara el costo del momento

                Dim dtSaldo As DataTable = bl.inv_ObtieneExistenciaKit(gv.EditingValue, 1, deFecha.EditValue)
                If dtSaldo.Rows.Count > 0 Then
                    gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioCosto", SiEsNulo(dtSaldo.Rows(0).Item("PrecioCosto"), 0.0))
                    Dim cantidad As Integer = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
                    Dim PrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioCosto")
                    Dim total As Decimal = Decimal.Round(cantidad * PrecioUnitario, 2)
                    gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", total)
                End If

                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Nombre", "-- PRODUCTO NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("Nombre", entProducto.Nombre)
                End If

                If bl.inv_VerificaProductoFormula(gv.EditingValue, 2) > 0 Then
                    MsgBox("Este artículo es producto terminado de producción", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Nombre", "-- PRODUCTO INVALIDO--")
                End If
            End If
        End If

    End Sub

    Private Sub CargarEntidades()
        With Header
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End If

            .IdFormula = teCorrelativo.EditValue  'el id se asignará en la capa de datos
            .IdProducto = teCodigo.EditValue
            .Nombre = teNombre.EditValue
            .Fecha = deFecha.EditValue
            .Concepto = meConcepto.EditValue
        End With

        Detalle = New List(Of inv_FormulasDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New inv_FormulasDetalle
            With entDetalle
                .IdFormula = Header.IdFormula
                .IdDetalle = i + 1
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Nombre = gv.GetRowCellValue(i, "Nombre")
                .PrecioCosto = gv.GetRowCellValue(i, "PrecioCosto")
                .PrecioTotal = gv.GetRowCellValue(i, "PrecioTotal")
            End With
            Detalle.Add(entDetalle)
        Next
    End Sub

#End Region

    Private Sub teCodigo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles teCodigo.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab OrElse e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.Down OrElse e.KeyCode = Keys.Right OrElse e.KeyCode = Keys.Left Then
            'validar columna codigo de producto
            If SiEsNulo(teCodigo.EditValue, "") = "" Then
                Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                teCodigo.EditValue = IdProd
            End If

            entProducto = objTablas.inv_ProductosSelectByPK(teCodigo.EditValue)
            If entProducto.IdProducto = "" Then
                MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                teNombre.EditValue = "-- PRODUCTO NO REGISTRADO --"
            Else
                teNombre.EditValue = entProducto.Nombre
            End If
        End If
    End Sub

    Private Sub sbImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbImportar.Click
        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            Return
        End If
        Dim Archivo As System.IO.StreamReader = File.OpenText(ofd.FileName)
        Dim separador As String = InputBox("Separador:", "Defina el delimitador del archivo", "|")
        If separador = "" Then
            separador = "|"
        End If
        Try
            While Archivo.Peek() > 0
                Dim sLine As String = Archivo.ReadLine()
                Dim aData As Array = sLine.Split(separador)
                gv.AddNewRow()
                gv.SetRowCellValue(gv.FocusedRowHandle, "IdProducto", aData(0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", aData(1))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Nombre", aData(2))
                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioCosto", SiEsNulo(aData(3), 0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", SiEsNulo(aData(4), 0))
                gv.UpdateCurrentRow()

            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Return
        End Try
        Archivo.Close()
    End Sub

    Private Sub btSaveAs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSaveAs.Click
        Dim Delimitador As String = InputBox("Deliminator del archivo:", "Especifique el delimitador", "|")
        If Delimitador = "" Then
            Return
        End If

        Dim fbd As New FolderBrowserDialog

        fbd.ShowDialog()
        If fbd.SelectedPath = "" Then
            Return
        End If

        Dim Archivo As String = String.Format("{0}\FORMULA{1}{2}.txt", fbd.SelectedPath, teCorrelativo.EditValue, Format(deFecha.EditValue, "yyyyMMdd"))

        Try
            Using Arc As StreamWriter = New StreamWriter(Archivo)
                Dim linea As String = String.Empty

                With gv
                    For fila As Integer = 0 To .RowCount - 1
                        linea = String.Empty

                        For col As Integer = 0 To .Columns.Count - 1
                            linea &= gv.GetRowCellValue(fila, gv.Columns(col)) & Delimitador
                        Next

                        With Arc
                            linea = linea.Remove(linea.Length - 1).ToString
                            .WriteLine(linea.ToString)
                        End With
                    Next
                End With
            End Using

            Process.Start(Archivo)

            'si se genera un error
        Catch ex As Exception
            MsgBox("NO SE PUDO GUARDAR EL ARCHIVO" + Chr(13) + ex.Message.ToString, MsgBoxStyle.Critical, "Nota")
        End Try
    End Sub
End Class