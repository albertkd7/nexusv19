﻿Imports NexusELL.TableEntities
Public Class inv_frmSubGrupos
    Dim entSubGrupo As inv_SubGrupos

    Private Sub inv_frmSubGrupos_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub inv_SubGrupos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.inv_SubGruposSelectAll
        entSubGrupo = objTablas.inv_SubGruposSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdSubGrupo"))
        objCombos.invGrupos(leGrupo)
        objCombos.invGrupos(leGrupo1)
        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub InvSubGrupos_Nuevo_Click() Handles Me.Nuevo
        entSubGrupo = New inv_SubGrupos
        entSubGrupo.IdSubGrupo = objFunciones.ObtenerUltimoId("Inv_SubGrupos", "IdSubGrupo") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub InvSubGrupos_Save_Click() Handles Me.Guardar
        If teIdSubGrupo.EditValue = 0 Or teNombre.EditValue = "" Or SiEsNulo(leGrupo.EditValue, 0) = 0 Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Nombre y Grupo]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.inv_SubGruposInsert(entSubGrupo)
        Else
            objTablas.inv_SubGruposUpdate(entSubGrupo)
        End If
        gc.DataSource = objTablas.inv_SubGruposSelectAll
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub inv_frmSubGrupos_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar al SubGrupo seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.inv_SubGruposDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.inv_SubGruposSelectAll
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL SUB GRUPO:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub Inv_SubGrupos_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entSubGrupo
            teIdSubGrupo.EditValue = .IdSubGrupo
            teNombre.EditValue = .Nombre
            leGrupo.EditValue = .IdGrupo
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entSubGrupo
            .IdSubGrupo = teIdSubGrupo.EditValue
            .Nombre = teNombre.EditValue
            .IdGrupo = leGrupo.EditValue
        End With
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entSubGrupo = objTablas.inv_SubGruposSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdSubGrupo"))
        CargaPantalla()
    End Sub

    Private Sub invSubGrupos_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teIdSubGrupo.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub

    Private Sub PanelControl2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PanelControl2.Paint

    End Sub
End Class
