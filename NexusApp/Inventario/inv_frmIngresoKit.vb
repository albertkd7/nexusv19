﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class inv_frmIngresoKit
    Dim entProducto As New inv_Productos
    Dim Detalle As List(Of inv_ProductosKit)
    Dim bl As New InventarioBLL(g_ConnectionString), IdCuenta As String

    Private _IdKit As String
    Public Property IdKit() As String
        Get
            Return _IdKit
        End Get
        Set(ByVal value As String)
            _IdKit = value
        End Set
    End Property


    Private Sub inv_frmIngresoKit_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        inv_frmIngresoKit_Nuevo()
        teIdProducto.Properties.ReadOnly = True
        teProducto.Properties.ReadOnly = True
    End Sub
    Private Sub inv_frmIngresoKit_Nuevo()
        entProducto = objTablas.inv_ProductosSelectByPK(_IdKit)
        teIdProducto.EditValue = entProducto.IdProducto
        teProducto.EditValue = entProducto.Nombre
        ActivarControles(True)
        gc.DataSource = bl.inv_ObtenerProductosKit(teIdProducto.EditValue)
        teProducto.Focus()
    End Sub
    Private Sub inv_frmIngresokit_Guardar() Handles sbGuardar.Click
        If Not DatosValidos() Then
            Exit Sub
        End If
        CargaEntidades()

        Dim msj As String = bl.inv_InsertaProductoKit(teIdProducto.EditValue, Detalle)
        If msj = "" Then
            MsgBox("El Kit ha sido guardado con éxito", MsgBoxStyle.Information, "Nota")
        Else
            msj = String.Format("Verifique que no esté ingresando registros duplicados{0}{1}{2}{3}", Chr(13), "Favor reporte el siguiente mensaje al departamento de IT", Chr(13), msj)
            MsgBox(msj, MsgBoxStyle.Critical, "NO FUE POSIBLE GUARDAR EL DOCUMENTO")
            Return
        End If
        Close()
    End Sub

    Function DatosValidos() As Boolean
        Dim msj As String = ""
        If teIdProducto.EditValue = "" Then
            msj = "Debe de especificar el código del producto"
        End If
        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If
        Return True
    End Function

    Private Sub CargaEntidades()
        Dim Cantidad As Decimal
        Detalle = New List(Of inv_ProductosKit)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New inv_ProductosKit
            With entDetalle
                .IdKit = teIdProducto.EditValue
                .IdDetalle = i + 1
                .IdProducto = SiEsNulo(gv.GetRowCellValue(i, "IdProducto"), -1)
                Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            Detalle.Add(entDetalle)
        Next
    End Sub

    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
        teIdProducto.Properties.ReadOnly = True
    End Sub

#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdProducto", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Nombre", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), "") = "" Then
            e.Valid = False
        End If
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Dim Cantidad As Integer = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
        Select Case gv.FocusedColumn.FieldName
            Case "Cantidad"
                Cantidad = e.Value
        End Select
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            Dim IdProd As String = ""
            If gv.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gv.EditingValue = IdProd
                    gv.SetRowCellValue(gv.FocusedRowHandle, "IdProducto", IdProd)
                End If
                entProducto = objTablas.inv_ProductosSelectByPK(IdProd)
                gv.SetRowCellValue(gv.FocusedRowHandle, "Nombre", entProducto.Nombre)
                If entProducto.Compuesto = True Then
                    MsgBox("Código de producto es Kit", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Nombre", "-- PRODUCTO ES KIT --")
                    gv.SetFocusedRowCellValue("IdProducto", "")
                Else
                    gv.SetFocusedRowCellValue("Nombre", entProducto.Nombre)
                End If
                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Nombre", "-- PRODUCTO NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("Nombre", entProducto.Nombre)
                End If
            End If
        End If
    End Sub
#End Region

    Private Sub inv_frmIngresoKit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        sbGuardar.Visible = False
        Me.Dispose()
    End Sub

End Class
