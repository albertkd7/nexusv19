﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns
Imports System.IO

Public Class inv_frmProducciones
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim entProducto As inv_Productos
    Dim Header As New inv_Producciones
    Dim Detalle As List(Of inv_ProduccionesDetalle)
    Dim DetalleGastosDet As List(Of inv_ProduccionesGastosDetalle)
    Dim DetalleGastosProd As List(Of inv_ProduccionesGastosProductos)
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim IdSucUser As Integer = SiEsNulo(EntUsuario.IdSucursal, 1)
    Dim _dtGastos As DataTable
    Dim _dtGastosProd As DataTable
    Dim dioClick As Boolean = False
    Dim TotalGastos As Decimal = 0.0

    Property dtGastos() As DataTable
        Get
            Return _dtGastos
        End Get
        Set(ByVal value As DataTable)
            _dtGastos = value
        End Set
    End Property

    Property dtGastosProd() As DataTable
        Get
            Return _dtGastosProd
        End Get
        Set(ByVal value As DataTable)
            _dtGastosProd = value
        End Set
    End Property

    Private Sub inv_frmProducciones_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = bl.inv_ConsultaProducciones(objMenu.User).Tables(0)
        gv2.BestFitColumns()
    End Sub

    Private Sub inv_frmProducciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        objCombos.inv_TiposProduccion(leTipoTransaccion, "")
        objCombos.inv_Bodegas(leBodega, "")
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("INV_PRODUCCIONES", "IdComprobante")
        CargaControles(0)
        ActivarControles(False)
        Me.gcDescripcion.OptionsColumn.ReadOnly = Not SiEsNulo(EntUsuario.AccesoProductos, True)
        gc2.DataSource = bl.inv_ConsultaProducciones(objMenu.User).Tables(0)
    End Sub
    Private Sub inv_frmProducciones_Nuevo() Handles MyBase.Nuevo
        Header = New inv_Producciones
        ActivarControles(True)

        gc.DataSource = bl.inv_ObtenerDetalleDocumento(-1, "inv_ProduccionesDetalle")
        gv.CancelUpdateCurrentRow()
        ' gv.AddNewRow()
        _dtGastos = bl.inv_ObtenerGastosProduccion(-1)
        _dtGastosProd = bl.inv_ObtenerGastosProduccionProd(Header.IdComprobante, "")
        teNumero.EditValue = ""
        deFecha.EditValue = Today
        meConcepto.EditValue = ""
        leBodega.EditValue = 1
        leTipoTransaccion.ItemIndex = 0
        leSucursal.EditValue = piIdSucursalUsuario
        btAplicar.Enabled = False
        teNumero.Focus()
        xtcProducciones.SelectedTabPage = xtpDatos
    End Sub
    Private Sub inv_frmProducciones_Guardar() Handles MyBase.Guardar
        Dim msj As String = ""
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "IdProducto") = gv.GetRowCellValue(i + 1, "IdProducto") Then
                msj = "El producto " & gv.GetRowCellValue(i, "IdProducto") & ",  está repetido"
                Exit For
            End If
        Next
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.None
        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If
        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            MsgBox("La fecha del documento debe ser de un período válido", 16, "Error de datos")
            Exit Sub
        End If
        If SiEsNulo(leSucursal.Text, "") = "" Or SiEsNulo(leBodega.EditValue, 0) = 0 Or SiEsNulo(leTipoTransaccion.Text, "") = "" Then
            MsgBox("Los datos de Bodega, Sucursal ó Tipo de Transacción no pueden quedar en blanco", 16, "Error de datos")
            Exit Sub
        End If
        CargarEntidades()

        If DbMode = DbModeType.insert Then
            msj = bl.inv_InsertarProducciones(Header, Detalle, DetalleGastosDet, DetalleGastosProd)
            If msj = "" Then
                MsgBox("El comprobante ha sido registrado con éxito", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox("SE DETECTÓ UN ERROR AL CREAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If
        Else
            msj = bl.inv_ActualizarProducciones(Header, Detalle, DetalleGastosDet, DetalleGastosProd)
            If msj = "" Then
                MsgBox("El documento ha sido actualizado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("SE DETECTÓ UN ERROR AL ACTUALIZAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If

        xtcProducciones.SelectedTabPage = xtpLista
        gc2.DataSource = bl.inv_ConsultaProducciones(objMenu.User).Tables(0)
        gc2.Focus()

        teCorrelativo.EditValue = Header.IdComprobante
        MostrarModoInicial()
        teCorrelativo.Focus()
        ActivarControles(False)
    End Sub
    Private Sub inv_frmProducciones_Editar() Handles MyBase.Editar
        If Header.AplicadaInventario Then
            MsgBox("No puede editar ésta transacción." + Chr(13) + "Debe revertirla antes de modificar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        ActivarControles(True)
        teNumero.Focus()
    End Sub
    Private Sub inv_frmProducciones_Consulta() Handles MyBase.Consulta
        'teCorrelativo.EditValue = objConsultas.ConsultaProduccionesInv(frmConsultaDetalle)
        'CargaControles(0)
    End Sub

    Private Sub inv_frmProducciones_Revertir() Handles MyBase.Revertir
        xtcProducciones.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub
    Private Sub inv_frmProducciones_Eliminar() Handles MyBase.Eliminar

        Header = objTablas.inv_ProduccionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        If Header.AplicadaInventario Then
            MsgBox("Es necesario que revierta la transacción. No se puede eliminar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de eliminar la transacción?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Header.ModificadoPor = objMenu.User
            objTablas.inv_ProduccionesUpdate(Header)

            objTablas.inv_ProduccionesDeleteByPK(Header.IdComprobante)
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("INV_PRODUCCIONES", "IdComprobante")
            CargaControles(0)
            ActivarControles(False)

            xtcProducciones.SelectedTabPage = xtpLista
            gc2.DataSource = bl.inv_ConsultaProducciones(objMenu.User).Tables(0)
            gc2.Focus()

        End If
    End Sub

#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "CantidadPosible", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", 0.0)
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), "") = "" Then
            e.Valid = False
        End If
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Dim Cantidad As Integer = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
        Dim PrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")

        Select Case gv.FocusedColumn.FieldName
            Case "Cantidad"
                Cantidad = e.Value

            Case "PrecioUnitario"
                PrecioUnitario = e.Value
        End Select

        Dim Total As Decimal = Decimal.Round(Cantidad * PrecioUnitario, 2)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", Total)
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then

                    Dim Condicion As String = ""
                    Condicion = "AND IdProducto IN (SELECT IdProducto FROM inv_Formulas )"

                    Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, Condicion, "")
                    gv.EditingValue = IdProd
                End If
                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                Dim dtSaldo As DataTable = bl.inv_ObtieneExistenciaFormulas(gv.EditingValue, leBodega.EditValue, deFecha.EditValue)
                If dtSaldo.Rows.Count > 0 Then
                    gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", SiEsNulo(dtSaldo.Rows(0).Item("PrecioCosto"), 0.0))
                    gv.SetRowCellValue(gv.FocusedRowHandle, "CantidadPosible", SiEsNulo(dtSaldo.Rows(0).Item("CantidadDisponible"), 0.0))
                    Dim cantidad As Integer = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
                    Dim PrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")
                    Dim total As Decimal = Decimal.Round(cantidad * PrecioUnitario, 2)
                    gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", total)
                End If
                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                End If
            End If
        End If

    End Sub
#End Region

    Public Sub CargaControles(ByVal TipoAvance As Integer)
        'If TipoAvance = 0 Then 'no es necesario obtener el Id del comprobante
        'Else
        '    teCorrelativo.EditValue = bl.inv_ObtenerIdDocumento(teCorrelativo.EditValue, TipoAvance, "inv_Producciones")
        'End If
        'If teCorrelativo.EditValue = 0 Then
        '    Exit Sub
        'End If
        'Header = objTablas.inv_ProduccionesSelectByPK(teCorrelativo.EditValue)
        'If Header Is Nothing Then
        '    Exit Sub
        'End If
        'With Header
        '    teCorrelativo.EditValue = .IdComprobante
        '    teNumero.EditValue = .Numero
        '    deFecha.EditValue = .Fecha
        '    meConcepto.EditValue = .Concepto
        '    leBodega.EditValue = .IdBodega
        '    leSucursal.EditValue = .IdSucursal
        '    leTipoTransaccion.EditValue = .IdTipoComprobante
        '    gc.DataSource = bl.inv_ObtenerDetalleDocumento(.IdComprobante, "inv_ProduccionesDetalle")
        '    _dtGastos = bl.inv_ObtenerGastosProduccion(.IdComprobante)
        '    _dtGastosProd = bl.inv_ObtenerGastosProduccionProd(.IdComprobante, "") 'LLAMO TODOS LOS PRODUCTOS PARA VER QUE GASTOS APLICAN A ELLOS
        'End With
        'LlenarDetalleGastos()
        'LlenarDetalleGastosProd()
        'btAplicar.Enabled = Not Header.AplicadaInventario
        'btRevertir.Enabled = Header.AplicadaInventario
    End Sub

    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
        btAplicar.Enabled = Not Header.AplicadaInventario
        btRevertir.Enabled = Header.AplicadaInventario
        btRevertir.Enabled = Header.AplicadaInventario
    End Sub
    Private Sub CargarEntidades()
        With Header
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
            Else
                .ModificadoPor = objMenu.User
                .FechaHoraModificacion = Now
            End If
            .IdComprobante = teCorrelativo.EditValue  'el id se asignará en la capa de datos
            .Numero = teNumero.EditValue
            .Fecha = deFecha.EditValue
            .Concepto = meConcepto.EditValue
            .IdBodega = leBodega.EditValue
            .IdSucursal = leSucursal.EditValue
            .AplicadaInventario = False
            .IdTipoComprobante = leTipoTransaccion.EditValue

        End With

        Detalle = New List(Of inv_ProduccionesDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New inv_ProduccionesDetalle
            With entDetalle
                .IdComprobante = Header.IdComprobante
                .IdDetalle = i + 1
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .CantidadPosible = gv.GetRowCellValue(i, "CantidadPosible")
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .PrecioTotal = gv.GetRowCellValue(i, "PrecioTotal")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            Detalle.Add(entDetalle)
        Next
    End Sub
    Private Sub inv_frmProduciones_Reporte() Handles Me.Reporte
        Header = objTablas.inv_ProduccionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))

        Dim dt As DataTable = bl.inv_ProrrateoProduccion(Header.IdComprobante)
        Dim rpt As New inv_rptProduccion With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlNumero.Text = Header.Numero
        'rpt.xrlConcepto.Text = meConcepto.EditValue
        rpt.xrlFecha.Text = Header.Fecha
        'rpt.xrlBodega.Text = leBodega.Text
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub btAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAplicar.Click
        If meConcepto.Properties.ReadOnly = False Then
            MsgBox("Debes Guardar antes la Producción", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de aplicar al inventario la transacción?" + Chr(13) + "YA NO SE PODRÁ EDITAR", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim msj As String = ""
        For i = 0 To gv.DataRowCount - 1
            Dim dtExistencia As DataTable
            dtExistencia = bl.inv_ObtieneExistenciaProductoFormulas(gv.GetRowCellValue(i, "IdProducto"), leBodega.EditValue, deFecha.EditValue)
            For Each drow As DataRow In dtExistencia.Rows
                If gv.GetRowCellValue(i, "Cantidad") > drow("CantidadDisponible") Then
                    msj = "El producto --> " & drow("IdProducto") & ", no tiene existencia en composición"
                    Exit For
                End If
            Next
        Next
        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Information, "Nota")
            msj = ""
            Exit Sub
        End If
        Header = objTablas.inv_ProduccionesSelectByPK(teCorrelativo.EditValue)
        If Header.AplicadaInventario Then
            MsgBox("Esta transacción ya fue aplicada al inventario", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        msj = bl.inv_AplicarTransaccionInventario(Header.IdComprobante, "Produccion", "I", Header.IdTipoComprobante, 0)
        If msj = "Ok" Then
            MsgBox("La transacción ha sido aplicada con éxito. Ya no puede editarse", MsgBoxStyle.Information, "Nota")
            Header.AplicadaInventario = True
            btAplicar.Enabled = False
            btRevertir.Enabled = True
        Else
            MsgBox("Sucedió algún error al aplicar" + Chr(13) + msj + "Favor reporte éste mensaje a ITO, S.A. DE C.V.", MsgBoxStyle.Critical, "Error")
        End If
    End Sub

    Private Sub btRevertir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRevertir.Click
        If meConcepto.Properties.ReadOnly = False Then
            MsgBox("Debes Guardar antes la Produccion", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de revertir la transacción?" + Chr(13) + "ESTO PUEDE DESESTABILIZAR SUS INVENTARIOS", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        If Not Header.AplicadaInventario Then
            MsgBox("La transacción NO ha sido aplicada aún", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        'Dim dt As DataTable = bl.inv_ObtenerProductosAfectados(teCorrelativo.EditValue, "SALIDAS")
        Dim msj As String = bl.inv_AplicarTransaccionInventario(Header.IdComprobante, "Produccion", "D", Header.IdTipoComprobante, 0)
        If msj = "Ok" Then
            MsgBox("La transacción ha sido revertida con éxito. Ahora puede editarse", MsgBoxStyle.Information, "Nota")
            Header.AplicadaInventario = False
            btAplicar.Enabled = True
            btRevertir.Enabled = False
        Else
            MsgBox("Sucedió algún error al revertir" + Chr(13) + msj + Chr(13) + "Favor reporte éste mensaje a IT O", MsgBoxStyle.Critical, "Error")
        End If
    End Sub

    Private Sub sbGastos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGastos.Click

        If teNumero.Properties.ReadOnly = True Then
            Exit Sub
        End If

        If dioClick = False Then
            _dtGastos = bl.inv_ObtenerGastosProduccion(-1)
        End If

        Dim frmGastos As New inv_frmDetallarGastosProduccion
        Me.AddOwnedForm(frmGastos)
        frmGastos.IdProduccion = Header.IdComprobante
        frmGastos.ShowDialog()

        LlenarDetalleGastos()
        dioClick = True
    End Sub
    Private Sub LlenarDetalleGastos()
        ' LLENO LA LISTA CON LOS GASTOS SELECCIONADOS DEL GRID ANTERIOR
        TotalGastos = 0.0
        DetalleGastosDet = New List(Of inv_ProduccionesGastosDetalle)
        For i = 0 To _dtGastos.Rows.Count - 1
            Dim entDetalle As New inv_ProduccionesGastosDetalle
            With entDetalle
                .IdProduccion = _dtGastos.Rows(i).Item("IdProduccion")
                .IdGasto = _dtGastos.Rows(i).Item("IdGasto")
                .Concepto = _dtGastos.Rows(i).Item("Concepto")
                .Valor = _dtGastos.Rows(i).Item("Valor")
                .Contabilizar = _dtGastos.Rows(i).Item("Contabilizar")

                TotalGastos += _dtGastos.Rows(i).Item("Valor")
                .IdDetalle = i + 1

            End With
            DetalleGastosDet.Add(entDetalle)
        Next
    End Sub

    Private Sub cmdDetallar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDetallar.Click
        If rbMenu.qbSave.Visibility = DevExpress.XtraBars.BarItemVisibility.Never Then
            Exit Sub
        End If
        If TotalGastos <= 0.0 Then
            MsgBox("No ha definido los gastos de la producción", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If gv.DataRowCount = 0 Then
            MsgBox("No ha definido los productos de la producción", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim IdProdSelecciono As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdProducto")), "")
        If IdProdSelecciono = "" Then
            MsgBox("Debe seleccionar el producto a definir gastos", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim ProductoYaDetallado As Boolean = False

        For i = 0 To _dtGastosProd.Rows.Count - 1
            If SiEsNulo(_dtGastosProd.Rows(i).Item("IdProducto"), "") = IdProdSelecciono Then
                ProductoYaDetallado = True
            End If
        Next

        If Not ProductoYaDetallado Then
            Dim dtTemp As New DataTable
            dtTemp = bl.inv_ObtenerGastosProduccionProd(0, IdProdSelecciono)
            For i = 0 To dtTemp.Rows.Count - 1
                Dim dr As DataRow = _dtGastosProd.NewRow()
                dr("IdProduccion") = dtTemp.Rows(i).Item("IdProduccion")
                dr("IdProducto") = dtTemp.Rows(i).Item("IdProducto")
                dr("Descripcion") = dtTemp.Rows(i).Item("Descripcion")
                dr("IdGasto") = dtTemp.Rows(i).Item("IdGasto")
                dr("AplicaGasto") = dtTemp.Rows(i).Item("AplicaGasto")
                _dtGastosProd.Rows.Add(dr)
            Next
        End If

        Dim frmGastosProd As New inv_frmDetallarGastosProduccionProductos
        Me.AddOwnedForm(frmGastosProd)
        frmGastosProd.IdProduccion = Header.IdComprobante
        frmGastosProd.IdProducto = IdProdSelecciono
        frmGastosProd.ShowDialog()

        LlenarDetalleGastosProd()
    End Sub

    Private Sub LlenarDetalleGastosProd()
        ' LLENO LA LISTA DE PRODUCTO PARA QUE LE COLOQUEN LOS GASTOS QUE APLICARAN
        DetalleGastosProd = New List(Of inv_ProduccionesGastosProductos)
        For i = 0 To _dtGastosProd.Rows.Count - 1
            If SiEsNulo(_dtGastosProd.Rows(i).Item("IdProducto"), "") <> "" Then
                Dim entDetalle As New inv_ProduccionesGastosProductos
                With entDetalle
                    .IdProduccion = SiEsNulo(_dtGastosProd.Rows(i).Item("IdProduccion"), 0)
                    .IdProducto = SiEsNulo(_dtGastosProd.Rows(i).Item("IdProducto"), "")
                    .Descripcion = SiEsNulo(_dtGastosProd.Rows(i).Item("Descripcion"), "")
                    .IdGasto = SiEsNulo(_dtGastosProd.Rows(i).Item("IdGasto"), 0)
                    .AplicaGasto = SiEsNulo(_dtGastosProd.Rows(i).Item("AplicaGasto"), 0)
                End With
                DetalleGastosProd.Add(entDetalle)
            End If
        Next
    End Sub


    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        Header = objTablas.inv_ProduccionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        DbMode = DbModeType.update
        teNumero.Focus()
    End Sub
    Private Sub CargaPantalla()
        xtcProducciones.SelectedTabPage = xtpDatos
        teNumero.Focus()

        With Header
            teCorrelativo.EditValue = .IdComprobante
            teNumero.EditValue = .Numero
            deFecha.EditValue = .Fecha
            meConcepto.EditValue = .Concepto
            leBodega.EditValue = .IdBodega
            leSucursal.EditValue = .IdSucursal
            leTipoTransaccion.EditValue = .IdTipoComprobante
            gc.DataSource = bl.inv_ObtenerDetalleDocumento(.IdComprobante, "inv_ProduccionesDetalle")
            _dtGastos = bl.inv_ObtenerGastosProduccion(.IdComprobante)
            _dtGastosProd = bl.inv_ObtenerGastosProduccionProd(.IdComprobante, "") 'LLAMO TODOS LOS PRODUCTOS PARA VER QUE GASTOS APLICAN A ELLOS
        End With
        LlenarDetalleGastos()
        LlenarDetalleGastosProd()
        btAplicar.Enabled = Not Header.AplicadaInventario
        btRevertir.Enabled = Header.AplicadaInventario
    End Sub
End Class
