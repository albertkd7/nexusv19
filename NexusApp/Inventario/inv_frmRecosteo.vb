﻿Imports NexusBLL
Public Class inv_frmRecosteo
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Dim bl As New InventarioBLL(g_ConnectionString)
    Private Sub inv_frmRecosteo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        deHasta.EditValue = Today
        objCombos.inv_Bodegas(leBodega, "")
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles sbProceder.Click
        If dtParam.Rows(0).Item("FechaCierre") > deHasta.EditValue Then
            MsgBox("Fecha de recalculo no puede ser menor al cierre contable", MsgBoxStyle.Critical, "Nota para el usuario")
            Exit Sub
        End If
        If MsgBox("Está seguro(a) de realiza el recalculo?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        Dim msj As String = bl.inv_RecalculaCostosExistencias(leBodega.EditValue, deHasta.EditValue, rgTipo.EditValue)
        If msj = "Ok" Then
            MsgBox("El recalcuo se ha realizado con éxito", MsgBoxStyle.Information, "Nota para el usuario")
        Else
            MsgBox("Se generó un error al realizar el proceso" & Chr(13) & "Favor reporte el siguiente mensaje al personal de Nexus" & Chr(13) & msj, MsgBoxStyle.Critical, "Nota para el usuario")
        End If
    End Sub
End Class
