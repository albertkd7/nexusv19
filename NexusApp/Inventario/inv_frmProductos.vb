﻿
Imports NexusELL.TableEntities
Imports NexusBLL
Imports System.Drawing
Imports System.Web
Imports System.Reflection
Imports System.Configuration
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Web.Script.Serialization

Public Class inv_frmProductos
    Dim entProducto As inv_Productos
    Dim FileName As String
    Dim nxcloud As nxcloudfiles.NxFilesCloud = New nxcloudfiles.NxFilesCloud()
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim entCuenta As con_Cuentas, IdProd As String 'PreciosDetalle As List(Of inv_ProductosPrecios),
    Dim nxDirectory As String = ConfigurationManager.AppSettings("NxAPICloudFiles:Directory")
    Dim nxDisabled As Boolean = IIf(ConfigurationManager.AppSettings("NxAPICloudFiles:Disabled") = "True", True, False)
    Dim jss As New JavaScriptSerializer()
    Dim PreciosDetalle As List(Of inv_ProductosPrecios)

    Private Sub inv_frmProductos_Consulta() Handles Me.Consulta
        IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
        CargaControles(-1)
    End Sub
    Private Sub inv_frmProductos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargaCombos()
        dn.DataSource = bl.inv_ObtenerListaProductos()
        ActivaControles(False)
        CargaControles(dn.Position)
    End Sub
    Private Sub inv_frmProductos_Nuevo() Handles Me.Nuevo
        IdProd = "-1x"  'código tonto para que se limpien los controles
        CargaControles(-1)
        ActivaControles(True)
        leGrupo.Focus()


    End Sub
    Private Sub inv_frmClientes_Reporte() Handles Me.Reporte
        Dim rpt As New inv_rptProductos() With {.DataSource = bl.inv_ListaDeProductos(-1, -1), .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Listado de productos"
        rpt.ShowPreview()
    End Sub
    Private Sub inv_frmProductos_Guardar() Handles Me.Guardar
        If Not DatosValidos() Then
            Exit Sub
        End If
        CargaEntidad()
        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)

        If DbMode = DbModeType.insert Then
            objTablas.inv_ProductosInsert(entProducto)
            bl.inv_ActualizaUltimoCorrelativoProducto(entProducto.IdGrupo)
            ActulizarPrecios()
            MsgBox("El producto se creo correctamente", 64, "Nota")
        Else
            objTablas.inv_ProductosUpdate(entProducto)
            ActulizarPrecios()
            MsgBox("Se ha guardado el producto", 64, "Nota")
        End If

        'bl.inv_ActualizaPrecios(entProducto.IdProducto, PreciosDetalle)
        MostrarModoInicial()
        ActivaControles(False)
    End Sub
    Private Sub inv_frmProductos_Revertir() Handles Me.Revertir
        CargaControles(dn.Position)
        ActivaControles(False)
    End Sub
    Private Sub inv_frmProductos_Eliminar() Handles Me.Eliminar
        If MsgBox("¿Está seguro de eliminar éste producto?", 292, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        objTablas.inv_ProductosDeleteByPK(teCodigo.EditValue)
        MsgBox("El producto se elimino correctamente", 64, "Nota")
        CargaControles(dn.Position)
        ActivaControles(False)
    End Sub
    Public Sub CargaControles(ByVal Position As Integer)
        If Position >= 0 Then
            IdProd = CType(dn.DataSource, DataTable).Rows(Position).Item(0)
        End If
        entProducto = objTablas.inv_ProductosSelectByPK(IdProd)
        With entProducto
            teCodigo.EditValue = .IdProducto
            teCodBarra.EditValue = .CodBarra
            leDepartamento.EditValue = .IdClasificacion
            leGrupo.EditValue = .IdGrupo
            leSubGrupo.EditValue = .IdSubGrupo
            teNombre.EditValue = .Nombre
            deFechaIngreso.DateTime = Now
            leUnidadMedida.EditValue = .IdUnidad
            leTipoInventario.EditValue = .TipoProducto
            leEstado.EditValue = .EstadoProducto
            leMarca.EditValue = .IdMarca
            leUbicacion.EditValue = .IdUbicacion
            leProveedor.EditValue = .IdProveedor
            'tePrecioCosto.EditValue = .PrecioCosto
            ceExento.Checked = .EsExento
            ceProductoEsp.Checked = .EsEspecial
            cePermiteFact.Checked = .PermiteFacturar
            teUnidadesPres.EditValue = .UnidadesPresentacion
            teTalla.EditValue = .Talla
            teColor.EditValue = .Color
            teEstilo.EditValue = .Estilo
            leProveedor.EditValue = .IdMarca
            teExistenciaMaxima.EditValue = .ExistenciaMaxima
            teExistenciaMinima.EditValue = .ExistenciaMinima
            meInfoAdicional.EditValue = .InformacionAdicional
            BeCtaContable1.beIdCuenta.EditValue = .IdCuentaIng
            BeCtaContable2.beIdCuenta.EditValue = .IdCuentaInv
            BeCtaContable3.beIdCuenta.EditValue = .IdCuentaCos
            beImagen.EditValue = .ArchivoImagen
            chkCompuesto.EditValue = .Compuesto
            leCategoria.EditValue = .IdCategoria
            'seComision.EditValue = .PorcComision
            leCentroCosto.EditValue = .IdCentro
            teTraduccion.EditValue = .Traduccion
            teCuantia.EditValue = .Cuantia
            teOrigen.EditValue = .Origen

            If leTipoInventario.EditValue = 0 Or leTipoInventario.EditValue = Nothing Then
                leTipoInventario.EditValue = 1
            End If
            If leCategoria.EditValue = 0 Or leCategoria.EditValue = Nothing Then
                leCategoria.EditValue = 1
            End If

            If .Compuesto = True Then
                btnDetallarKit.Visible = True
            Else
                btnDetallarKit.Visible = False
            End If

            CargaImagen(.ArchivoImagen)
            gcPr1.DataSource = bl.inv_ObtienePrecios(teCodigo.EditValue, objMenu.User, pnIVA)
            'gcp.DataSource = bl.inv_ObtienePrecios(.IdProducto)
        End With
    End Sub
    Private Sub CargaEntidad()
        With entProducto
            .IdProducto = teCodigo.EditValue
            .CodBarra = teCodBarra.EditValue
            .Nombre = teNombre.EditValue
            .IdGrupo = leGrupo.EditValue
            .IdSubGrupo = leSubGrupo.EditValue
            .IdMarca = leMarca.EditValue
            .IdUnidad = leUnidadMedida.EditValue
            .IdProveedor = leProveedor.EditValue
            .IdUbicacion = leUbicacion.EditValue
            .Talla = teTalla.EditValue
            .Color = teColor.EditValue
            .Estilo = teEstilo.EditValue
            .UnidadesPresentacion = teUnidadesPres.EditValue
            .TipoProducto = leTipoInventario.EditValue
            .EstadoProducto = leEstado.EditValue
            .PrecioCosto = 0 ' tePrecioCosto.EditValue
            .EsExento = ceExento.Checked
            .EsEspecial = ceProductoEsp.Checked
            .PermiteFacturar = cePermiteFact.Checked
            .IdCuentaIng = BeCtaContable1.beIdCuenta.EditValue
            .IdCuentaInv = BeCtaContable2.beIdCuenta.EditValue
            .IdCuentaCos = BeCtaContable3.beIdCuenta.EditValue
            .ArchivoImagen = beImagen.EditValue
            .ExistenciaMaxima = teExistenciaMaxima.EditValue
            .ExistenciaMinima = teExistenciaMinima.EditValue
            .InformacionAdicional = meInfoAdicional.EditValue
            .IdCategoria = leCategoria.EditValue
            .IdClasificacion = leDepartamento.EditValue
            .PorcComision = 0 ' seComision.EditValue
            .IdCentro = SiEsNulo(leCentroCosto.EditValue, "")
            .Origen = teOrigen.EditValue
            .Cuantia = teCuantia.EditValue
            .Traduccion = teTraduccion.EditValue

            Dim dtCom As New DataTable
            dtCom = bl.inv_ObtenerEsComponente(teCodigo.EditValue)

            If chkCompuesto.EditValue = True Then
                If dtCom.Rows.Count > 0 Then
                    .Compuesto = False
                Else
                    .Compuesto = True
                End If
            Else
                .Compuesto = chkCompuesto.EditValue
            End If

            If DbMode = DbModeType.insert Then
                .FechaIngreso = Now
                .FechaHoraCreacion = Now
                .CreadoPor = objMenu.User
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If
        End With


    End Sub
    Private Sub CargaCombos()
        With objCombos
            .invUnidadesMedida(leUnidadMedida)
            .invTiposInventario(leTipoInventario)
            .invEstadosInventario(leEstado)
            .invMarcas(leMarca)
            .invUbicaciones(leUbicacion)
            .com_Proveedores(leProveedor)
            .invCategoriaProductof983(leCategoria)
            .invCategoriaProducto(leDepartamento)
            .con_CentrosCosto(leCentroCosto, "", "")
            .inv_Precios(rileIdPrecio1)
        End With
    End Sub
    Private Sub cboGrupo_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles leGrupo.EditValueChanged
        objCombos.invSubGrupos(leSubGrupo, leGrupo.EditValue)
        If DbMode = DbModeType.insert Then 'And teCodigo.EditValue = "" 
            teCodigo.EditValue = bl.inv_ObtenieCodigoDeProducto(leGrupo.EditValue)
        End If
    End Sub
    Private Sub leCategoria2_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leDepartamento.EditValueChanged
        objCombos.invGrupos(leGrupo, leDepartamento.EditValue)
    End Sub

    Private Sub CargaImagen(ByVal FileName As String)
        If String.IsNullOrEmpty(nxDirectory) = False And nxDisabled Then
            peFoto.Image = FileName.BuscarImagenEnServidor()
        Else
            If Not FileIO.FileSystem.FileExists(FileName) Or FileName = "" Then
                peFoto.Image = Nothing
                lcExisteArchivo.Text = "-DOBLE CLIC PARA CARGAR IMAGEN-"
            Else
                Dim bm As New Bitmap(FileName)
                peFoto.Image = bm
                lcExisteArchivo.Text = "-DOBLE CLIC PARA CAMBIAR IMAGEN-"
            End If
        End If
    End Sub
    Private Sub beImagen_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beImagen.ButtonClick
        SubirImagen()
    End Sub
    Private Sub peFoto_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles peFoto.DoubleClick
        SubirImagen()
    End Sub

    Private Function SubirImagen()
        If OpenFile.ShowDialog() = DialogResult.OK Then
            If String.IsNullOrEmpty(nxDirectory) = False And nxDisabled Then

                Dim x = Path.GetFileName(OpenFile.FileName)
                Dim y = System.IO.File.ReadAllBytes(OpenFile.FileName)
                Dim u = "ITO".Encriptar()
                Dim p = "Ito1945343$".Encriptar()

                Dim ResponseHttp As String = nxcloud.UploadFile("ITO".Encriptar(), "Ito1945343$".Encriptar(), nxDirectory, Path.GetFileName(OpenFile.FileName), System.IO.File.ReadAllBytes(OpenFile.FileName))
                Dim rws As ResponseWS = jss.Deserialize(Of ResponseWS)(ResponseHttp)
                If rws.Completado = False Then
                    MessageBox.Show(rws.Mensaje, "404!: ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return False
                Else
                    beImagen.Text = OpenFile.FileName
                    CargaImagen(beImagen.Text)
                End If
            Else
                beImagen.Text = OpenFile.FileName
                CargaImagen(beImagen.Text)
            End If
        End If
        Return True
    End Function
    Private Function DatosValidos()
        If leGrupo.EditValue = 0 Then
            MsgBox("Debe de especificar el grupo al que pertenece el artículo", MsgBoxStyle.Critical)
            Return False
        End If
        If leSubGrupo.EditValue = 0 Then
            MsgBox("Debe de especificar el sub-grupo al que pertenece el producto", MsgBoxStyle.Critical)
            Return False
        End If
        If teCodigo.EditValue = "" Then
            MsgBox("Debe de especificar el código del producto", MsgBoxStyle.Critical)
            Return False
        End If
        If teNombre.EditValue = "" Then
            MsgBox("Debe de especificar el nombre del producto", MsgBoxStyle.Critical)
            Return False
        End If
        If leMarca.EditValue = 0 Then
            MsgBox("Debe de especificar la marca", MsgBoxStyle.Critical)
            Return False
        End If
        If leUnidadMedida.EditValue = 0 Then
            MsgBox("Debe de especificar la unidad de medida del producto", MsgBoxStyle.Critical)
            Return False
        End If

        If leTipoInventario.EditValue = 0 Or leTipoInventario.EditValue = Nothing Then
            MsgBox("Debe de especificar el tipo de producto", MsgBoxStyle.Critical)
            Return False
        End If
        If leCategoria.EditValue = 0 Or leCategoria.EditValue = Nothing Then
            MsgBox("Debe de especificar la categoría f983 del producto", MsgBoxStyle.Critical)
            Return False
        End If
        If leDepartamento.EditValue = 0 Or leDepartamento.EditValue = Nothing Then
            MsgBox("Debe de especificar el departamento del producto", MsgBoxStyle.Critical)
            Return False
        End If
        If leUbicacion.EditValue = 0 Then
            MsgBox("Debe de especificar la ubicación del artículo", MsgBoxStyle.Critical)
            Return False
        End If
        Return True
    End Function
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In XtraTabPage1.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        For Each ctrl In XtraTabPage2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        leCentroCosto.Properties.ReadOnly = True
    End Sub

    Private Sub dn_PositionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dn.PositionChanged
        CargaControles(dn.Position)
    End Sub

    Private Sub inv_frmProductos_Editar() Handles Me.Editar
        ActivaControles(True)
        leGrupo.Focus()
    End Sub
    Private Sub btnDetallarKit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetallarKit.Click
        Dim dtCom As New DataTable
        dtCom = bl.inv_ObtenerEsComponente(teCodigo.EditValue)
        If chkCompuesto.EditValue = True Then
            If dtCom.Rows.Count = 0 Then
                inv_frmIngresoKit.IdKit = teCodigo.EditValue
                inv_frmIngresoKit.ShowDialog()
            Else
                MsgBox("El Producto es Componente, Imposible Continuar", 64, "Nota")
            End If
        End If
    End Sub
    Private Sub chkCompuesto_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCompuesto.EditValueChanged
        If chkCompuesto.EditValue = True Then
            btnDetallarKit.Visible = True
        Else
            btnDetallarKit.Visible = False
        End If
    End Sub

    Private Sub sbCentroCosto_Click(sender As Object, e As EventArgs) Handles sbCentroCosto.Click
        If teNombre.Properties.ReadOnly Then
            Exit Sub
        End If

        Dim IdCentro As String = SiEsNulo(leCentroCosto.EditValue, "")
        Dim frmSeleccionaCC As New frmDetallarCentroCosto
        Me.AddOwnedForm(frmSeleccionaCC)
        frmSeleccionaCC.IdCentro = IdCentro
        frmSeleccionaCC.ShowDialog()
        leCentroCosto.EditValue = SiEsNulo(frmSeleccionaCC.IdCentro, "")
        frmSeleccionaCC.Dispose()
    End Sub

    Private Sub ActulizarPrecios()
        Dim IdPro As String = teCodigo.EditValue
        PreciosDetalle = New List(Of inv_ProductosPrecios)
        For k = 0 To gvPr1.DataRowCount - 1
            Dim entPrecio As New inv_ProductosPrecios
            With entPrecio
                .IdProducto = IdPro
                .IdPrecio = gvPr1.GetRowCellValue(k, "IdPrecio")
                .Precio = SiEsNulo(gvPr1.GetRowCellValue(k, "Precio"), 0)
                .Descuento = SiEsNulo(gvPr1.GetRowCellValue(k, "Descuento"), 0)
                .Margen = SiEsNulo(gvPr1.GetRowCellValue(k, "Margen"), 0)
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            PreciosDetalle.Add(entPrecio)
        Next
        bl.inv_ActualizaPrecios(IdPro, PreciosDetalle)
    End Sub
End Class
