﻿Imports NexusELL.TableEntities
Public Class inv_frmProductosMargenes

    Private Sub inv_frmProductosMargenes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.inv_ProductosMargenesSelectAll
    End Sub
    Private Sub inv_frmProductosMargenes_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar este registro?", MsgBoxStyle.YesNo, "Nexus") = MsgBoxResult.No Then
            Return
        End If

        Dim Id As Integer = gv.GetFocusedRowCellValue(gv.Columns(0))
        objTablas.inv_ProductosMargenesDeleteByPK(Id)

        gc.DataSource = objTablas.inv_ProductosMargenesSelectAll
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        Dim Ent As New inv_ProductosMargenes
        Dim numFila As Integer
        If e.RowHandle < 0 Then
            DbMode = DbModeType.Insert
            numFila = gv.RowCount - 1
        Else
            DbMode = DbModeType.Update
            numFila = e.RowHandle
        End If
        If Not AllowInsert Or Not AllowEdit Then
            MsgBox("No le está permitido ingresar o editar información", MsgBoxStyle.Exclamation, Me.Text)
            gc.DataSource = objTablas.inv_ProductosMargenesSelectAll
            Exit Sub
        End If

        With Ent
            .IdMargen = gv.GetRowCellValue(numFila, gv.Columns(0).FieldName)
            .Desde = gv.GetRowCellValue(numFila, gv.Columns(1).FieldName)
            .Hasta = SiEsNulo(gv.GetRowCellValue(numFila, gv.Columns(2).FieldName), False)
            .Valor = SiEsNulo(gv.GetRowCellValue(numFila, gv.Columns(3).FieldName), False)
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        If DbMode = DbModeType.Insert Then
            objTablas.inv_ProductosMargenesInsert(Ent)
        Else
            objTablas.inv_ProductosMargenesUpdate(Ent)
        End If

    End Sub

    Private Sub inv_frmProductosMargenes_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
End Class
