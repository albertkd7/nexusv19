﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns
Imports System.IO

Public Class inv_frmEntradas
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim MyBL As New AdmonBLL(g_ConnectionString)
    Dim entProducto As inv_Productos
    Dim Header As New inv_Entradas
    Dim Detalle As List(Of inv_EntradasDetalle)
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim IdSucUser As Integer = SiEsNulo(EntUsuario.IdSucursal, 1), AutorizoReversion As String = ""
    Dim _dtRemision As DataTable

    Property dtRemision() As DataTable
        Get
            Return _dtRemision
        End Get
        Set(ByVal value As DataTable)
            _dtRemision = value
        End Set
    End Property

    Private Sub inv_frmEntradas_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = bl.inv_ConsultaEntradas(objMenu.User).Tables(0)
        gv2.BestFitColumns()
    End Sub

    Private Sub inv_frmEntradas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.inv_TiposEntrada(leTipoTransaccion, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        objCombos.con_CentrosCosto(leCentro, "", "")
        objCombos.inv_Bodegas(leBodega, "")
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("INV_ENTRADAS", "IdComprobante")

        ActivarControles(False)

        gc2.DataSource = bl.inv_ConsultaEntradas(objMenu.User).Tables(0)
        RefrescaGrid()
    End Sub
    Private Sub inv_frmEntradas_Nuevo() Handles MyBase.Nuevo
        Header = New inv_Entradas
        ActivarControles(True)

        gc.DataSource = bl.inv_ObtenerDetalleDocumento(-1, "inv_EntradasDetalle")
        gv.CancelUpdateCurrentRow()
        gv.AddNewRow()

        teNumero.EditValue = ""
        deFecha.EditValue = Today
        meConcepto.EditValue = ""
        leBodega.EditValue = EntUsuario.IdBodega
        leSucursal.EditValue = piIdSucursalUsuario
        leTipoTransaccion.EditValue = 12
        teIdRemision.EditValue = 0
        teNumRemision.EditValue = ""
        btAplicar.Enabled = False
        teNumero.Focus()

        xtcEntradas.SelectedTabPage = xtpDatos
        RefrescaGrid()
    End Sub
    Private Sub inv_frmEntradas_Guardar() Handles MyBase.Guardar
        Dim msj As String = ""
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "IdProducto") = gv.GetRowCellValue(i + 1, "IdProducto") Then
                msj = "El producto " & gv.GetRowCellValue(i, "IdProducto") & ",  está repetido"
                Exit For
            End If
        Next
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.None
        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If
        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            MsgBox("La fecha del documento debe ser de un período válido", 16, "Error de datos")
            Exit Sub
        End If

        If SiEsNulo(leSucursal.Text, "") = "" Or SiEsNulo(leBodega.EditValue, 0) = 0 Or SiEsNulo(leTipoTransaccion.Text, "") = "" Or SiEsNulo(leTipoTransaccion.EditValue, 0) = 0 Then
            MsgBox("Los datos de Bodega, Sucursal ó Tipo de Transacción no pueden quedar en blanco", 16, "Error de datos")
            Exit Sub
        End If

        CargarEntidades()

        If DbMode = DbModeType.insert Then
            msj = bl.inv_InsertarEntrada(Header, Detalle)

            If msj = "" Then
                MsgBox("El comprobante ha sido registrado con éxito", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox("SE DETECTÓ UN ERROR AL CREAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If

            If teNumero.EditValue = "" Then
                teNumero.EditValue = Header.Numero
            End If

        Else
            msj = bl.inv_ActualizarEntrada(Header, Detalle)
            If msj = "" Then
                MsgBox("El documento ha sido actualizado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("SE DETECTÓ UN ERROR AL ACTUALIZAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If

        'xtcEntradas.SelectedTabPage = xtpLista
        'gc2.DataSource = bl.inv_ConsultaEntradas(piIdSucursalUsuario).Tables(0)
        'gc2.Focus()

        teCorrelativo.EditValue = Header.IdComprobante
        MostrarModoInicial()
        teCorrelativo.Focus()
        ActivarControles(False)
        RefrescaGrid()
    End Sub
    Private Sub inv_frmEntradas_Editar() Handles MyBase.Editar
        If Header.AplicadaInventario Then
            MsgBox("No puede editar ésta transacción." + Chr(13) + "Debe revertirla antes de modificar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        ActivarControles(True)
        teNumero.Focus()
        RefrescaGrid()
    End Sub
    Private Sub inv_frmEntradas_Consulta() Handles MyBase.Consulta
        'teCorrelativo.EditValue = objConsultas.ConsultaEntradasInv(frmConsultaDetalle)
        'CargaControles(0)
    End Sub
    Private Sub inv_frmEntradas_Revertir() Handles MyBase.Revertir
        xtcEntradas.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub
    Private Sub inv_frmEntradas_Eliminar() Handles MyBase.Eliminar
        If xtcEntradas.SelectedTabPage.Name = "xtpLista" Then
            Header = objTablas.inv_EntradasSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Else
            Header = objTablas.inv_EntradasSelectByPK(teCorrelativo.EditValue)
        End If

        If Header.AplicadaInventario Then
            MsgBox("Es necesario que revierta la transacción. No se puede eliminar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de eliminar la transacción?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Header.ModificadoPor = objMenu.User
            objTablas.inv_EntradasUpdate(Header)

            objTablas.inv_EntradasDeleteByPK(Header.IdComprobante)
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("INV_ENTRADAS", "IdComprobante")

            ActivarControles(False)

            xtcEntradas.SelectedTabPage = xtpLista
            gc2.DataSource = bl.inv_ConsultaEntradas(objMenu.User).Tables(0)
            gc2.Focus()

        End If
    End Sub

#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        If teIdRemision.EditValue > 0 Then
            Exit Sub
        End If
        gv.DeleteSelectedRows()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", 0.0)
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), "") = "" Then
            e.Valid = False
        End If
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Dim Cantidad As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
        Dim PrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")

        Select Case gv.FocusedColumn.FieldName
            Case "Cantidad"
                Cantidad = e.Value

            Case "PrecioUnitario"
                PrecioUnitario = e.Value
        End Select

        Dim Total As Decimal = Decimal.Round(Cantidad * PrecioUnitario, 2)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", Total)
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gv.EditingValue = IdProd
                End If
                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                Dim dtSaldo As DataTable = bl.inv_ObtieneExistenciaCostoProducto(gv.EditingValue, leBodega.EditValue)
                If dtSaldo.Rows.Count > 0 Then
                    gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", SiEsNulo(dtSaldo.Rows(0).Item("PrecioCosto"), 0.0))
                    Dim cantidad As Integer = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
                    Dim PrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")
                    Dim total As Decimal = Decimal.Round(cantidad * PrecioUnitario, 2)
                    gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", total)
                End If
                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                    gv.SetFocusedRowCellValue("IdCentro", entProducto.IdCentro)
                End If
            End If
        End If

    End Sub
#End Region




    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
        btAplicar.Enabled = Not Header.AplicadaInventario
        btRevertir.Enabled = Header.AplicadaInventario
        btRevertir.Enabled = Header.AplicadaInventario
    End Sub
    Private Sub CargarEntidades()
        With Header


            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing

            Else
                .ModificadoPor = objMenu.User
                .FechaHoraModificacion = Now
            End If

            .Numero = teNumero.EditValue
            .IdComprobante = teCorrelativo.EditValue  'el id se asignará en la capa de datos
            .IdTipoComprobante = leTipoTransaccion.EditValue
            .Fecha = deFecha.EditValue
            .Concepto = meConcepto.EditValue
            .IdBodega = leBodega.EditValue
            .IdSucursal = leSucursal.EditValue
            .AplicadaInventario = False
            .IdComprobVenta = teIdRemision.EditValue
        End With

        Detalle = New List(Of inv_EntradasDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New inv_EntradasDetalle
            With entDetalle
                .IdComprobante = Header.IdComprobante
                .IdDetalle = i + 1
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .PrecioTotal = gv.GetRowCellValue(i, "PrecioTotal")
                .IdCentro = gv.GetRowCellValue(i, "IdCentro")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            Detalle.Add(entDetalle)
        Next

    End Sub
    Private Sub sbImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbImportar.Click
        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            Return
        End If
        Dim Archivo As System.IO.StreamReader = File.OpenText(ofd.FileName)
        Dim separador As String = InputBox("Separador:", "Defina el delimitador del archivo", "|")
        If separador = "" Then
            separador = "|"
        End If
        Try
            While Archivo.Peek() > 0
                Dim sLine As String = Archivo.ReadLine()
                Dim aData As Array = sLine.Split(separador)
                entProducto = objTablas.inv_ProductosSelectByPK(aData(0))

                gv.AddNewRow()
                gv.SetRowCellValue(gv.FocusedRowHandle, "IdProducto", aData(0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", aData(1))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", aData(2))
                gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", entProducto.IdCentro)
                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", SiEsNulo(aData(3), 0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", SiEsNulo(aData(4), 0))
                gv.UpdateCurrentRow()

            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Return
        End Try
        Archivo.Close()
    End Sub
    Private Sub inv_frmEntradas_Reporte() Handles Me.Reporte
        If xtcEntradas.SelectedTabPage.Name = "xtpLista" Then
            Header = objTablas.inv_EntradasSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Else
            Header = objTablas.inv_EntradasSelectByPK(teCorrelativo.EditValue)
        End If

        Dim entBod As inv_Bodegas = objTablas.inv_BodegasSelectByPK(Header.IdBodega)
        Dim entTipos As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(Header.IdTipoComprobante)
        Dim dt As DataTable = bl.inv_ObtenerDetalleDocumento(Header.IdComprobante, "inv_EntradasDetalle")


            Dim rpt As New inv_rptEntradaSalida() With {.DataSource = dt, .DataMember = ""}

            rpt.DataMember = ""
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlNumero.Text = Header.Numero
            rpt.xrlTipo.Text = entTipos.Nombre
            rpt.xrlConcepto.Text = Header.Concepto
            rpt.xrlFecha.Text = Header.Fecha
            rpt.xrlBodega.Text = entBod.Nombre
            rpt.ShowPreviewDialog()


    End Sub
    Private Sub btSaveAs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSaveAs.Click
        Dim Delimitador As String = InputBox("Deliminator del archivo:", "Especifique el delimitador", "|")
        If Delimitador = "" Then
            Return
        End If

        Dim fbd As New FolderBrowserDialog

        fbd.ShowDialog()
        If fbd.SelectedPath = "" Then
            Return
        End If

        Dim Archivo As String = String.Format("{0}\ENTRADA{1}{2}.txt", fbd.SelectedPath, teCorrelativo.EditValue, Format(deFecha.EditValue, "yyyyMMdd"))

        Try
            Using Arc As StreamWriter = New StreamWriter(Archivo)
                Dim linea As String = String.Empty

                With gv
                    For fila As Integer = 0 To .RowCount - 1
                        linea = String.Empty

                        For col As Integer = 0 To .Columns.Count - 1
                            linea &= gv.GetRowCellValue(fila, gv.Columns(col)) & Delimitador
                        Next

                        With Arc
                            linea = linea.Remove(linea.Length - 1).ToString
                            .WriteLine(linea.ToString)
                        End With
                    Next
                End With
            End Using

            Process.Start(Archivo)

            'si se genera un error
        Catch ex As Exception
            MsgBox("NO SE PUDO GUARDAR EL ARCHIVO" + Chr(13) + ex.Message.ToString, MsgBoxStyle.Critical, "Nota")
        End Try
    End Sub
    Private Sub btAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAplicar.Click
        If meConcepto.Properties.ReadOnly = False Then
            MsgBox("Debes Guardar antes la entrada", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de aplicar al inventario la transacción?" + Chr(13) + "YA NO SE PODRÁ EDITAR", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        Header = objTablas.inv_EntradasSelectByPK(teCorrelativo.EditValue)
        If Header.AplicadaInventario Then
            MsgBox("Esta transacción ya fue aplicada al inventario", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        Dim msj As String = bl.inv_AplicarTransaccionInventario(Header.IdComprobante, "Entrada", "I", Header.IdTipoComprobante, 0)
        If msj = "Ok" Then
            MsgBox("La transacción ha sido aplicada con éxito. Ya no puede editarse", MsgBoxStyle.Information, "Nota")
            Header.AplicadaInventario = True
            btAplicar.Enabled = False
            btRevertir.Enabled = True
        Else
            MsgBox("Sucedió algún error al aplicar" + Chr(13) + msj + "Favor reporte éste mensaje a ITO, S.A. DE C.V.", MsgBoxStyle.Critical, "Error")
        End If

        xtcEntradas.SelectedTabPage = xtpLista
        gc2.DataSource = bl.inv_ConsultaEntradas(objMenu.User).Tables(0)
        gc2.Focus()

    End Sub
    Private Sub btRevertir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRevertir.Click
        AutorizoReversion = ""
        If meConcepto.Properties.ReadOnly = False Then
            MsgBox("Debes Guardar antes la entrada", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de revertir la transacción?" + Chr(13) + "ESTO PUEDE DESESTABILIZAR SUS INVENTARIOS", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        If Not Header.AplicadaInventario Then
            MsgBox("La transacción NO ha sido aplicada aún", MsgBoxStyle.Exclamation, "Error")
        End If

        Dim msj1 As String = "", lDummy As Boolean = False

        For i = 0 To gv.DataRowCount - 1
            If bl.inv_ObtieneExistenciaProducto(gv.GetRowCellValue(i, "IdProducto"), leBodega.EditValue, deFecha.EditValue).Rows(0).Item("SaldoExistencias") - gv.GetRowCellValue(i, "Cantidad") < 0 Then
                If msj1 = "" Then
                    msj1 = "Productos que resultaran con Saldo Negativos en Fecha " + Format(deFecha.EditValue, "dd/MM/yyyy") + Chr(13)
                End If
                msj1 &= gv.GetRowCellValue(i, "IdProducto") + ","
            End If
        Next

        For i = 0 To gv.DataRowCount - 1
            If bl.inv_ObtieneExistenciaProducto(gv.GetRowCellValue(i, "IdProducto"), leBodega.EditValue, Today).Rows(0).Item("SaldoExistencias") - gv.GetRowCellValue(i, "Cantidad") < 0 Then
                If Not lDummy Then
                    msj1 &= Chr(13) + "Productos que resultaran con Saldo Negativos en Fecha " + Format(Today, "dd/MM/yyyy") + Chr(13)
                End If
                msj1 &= gv.GetRowCellValue(i, "IdProducto") + ","
                lDummy = True
            End If
        Next

        If msj1 <> "" Then
            If MsgBox(msj1 + Chr(13) + "Desea Autorizar para continuar?", 292, "Confirme") = MsgBoxResult.No Then
                Exit Sub
            End If
            frmObtienePassword.Text = "AUTORIZACIÓN PARA REVERTIR INVENTARIOS"
            frmObtienePassword.Usuario = objMenu.User
            frmObtienePassword.TipoAcceso = 4
            frmObtienePassword.ShowDialog()

            If frmObtienePassword.Acceso = False Then
                MsgBox("Usuario no Autorizado para Revertir Inventarios", MsgBoxStyle.Critical, "Nota")
                Exit Sub
            End If
            AutorizoReversion = frmObtienePassword.Usuario
            frmObtienePassword.Dispose()
        End If


        Dim msj As String = bl.inv_AplicarTransaccionInventario(Header.IdComprobante, "Entrada", "D", Header.IdTipoComprobante, 0)

            If msj = "Ok" Then
            MsgBox("La transacción ha sido revertida con éxito. Ahora puede editarse", MsgBoxStyle.Information, "Nota")
            bl.inv_AutorizoReversionInventario(AutorizoReversion, "Revertir Entrada de ", Header.Numero, Header.IdComprobante)
                Header.AplicadaInventario = False
                btAplicar.Enabled = True
                btRevertir.Enabled = False
            Else
                MsgBox("Sucedió algún error al revertir" + Chr(13) + msj + Chr(13) + "Favor reporte éste mensaje a IT O", MsgBoxStyle.Critical, "Error")
            End If
    End Sub
    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        Header = objTablas.inv_EntradasSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        'DbMode = DbModeType.update
        teNumero.Focus()
    End Sub
    Private Sub CargaPantalla()
        If DbMode <> DbModeType.cargar Then
            Exit Sub
        End If
        xtcEntradas.SelectedTabPage = xtpDatos
        teNumero.Focus()

        With Header
            teCorrelativo.EditValue = .IdComprobante
            teNumero.EditValue = .Numero
            leTipoTransaccion.EditValue = CInt(.IdTipoComprobante)
            deFecha.EditValue = .Fecha
            meConcepto.EditValue = .Concepto
            leBodega.EditValue = .IdBodega
            leSucursal.EditValue = .IdSucursal
            teIdRemision.EditValue = .IdComprobVenta
            Dim entVenta As fac_Ventas = objTablas.fac_VentasSelectByPK(.IdComprobVenta)
            teNumRemision.EditValue = entVenta.Numero
            gc.DataSource = bl.inv_ObtenerDetalleDocumento(.IdComprobante, "inv_entradasDetalle")
        End With
        btAplicar.Enabled = Not Header.AplicadaInventario
        btRevertir.Enabled = Header.AplicadaInventario

    End Sub

    Private Sub sbPedido_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbPedido.Click
        If deFecha.Properties.ReadOnly Then
            Exit Sub
        End If

        _dtRemision = bl.inv_ObtenerDetalleDocumento(-1, "inv_EntradasDetalle")

        Dim frmTrasladosGarantia As New inv_frmTrasladosPedidos
        Me.AddOwnedForm(frmTrasladosGarantia)

        frmTrasladosGarantia.IdSucursal = piIdSucursalUsuario
        frmTrasladosGarantia.IdBodegaVenta = EntUsuario.IdBodega
        frmTrasladosGarantia.ShowDialog()

        teIdRemision.EditValue = frmTrasladosGarantia.IdComprobante
        teNumRemision.EditValue = frmTrasladosGarantia.Numero
        leBodega.EditValue = frmTrasladosGarantia.IdBodegaVenta
        gc.DataSource = _dtRemision
        RefrescaGrid()
    End Sub
    Private Sub RefrescaGrid()
        Dim entVenta As fac_Ventas = objTablas.fac_VentasSelectByPK(teIdRemision.EditValue)
        teNumRemision.EditValue = entVenta.Numero

        leBodega.Enabled = Not teIdRemision.EditValue > 0
        If teIdRemision.EditValue > 0 Then
            gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None
        Else
            gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        End If
        Me.gcIdProducto.OptionsColumn.ReadOnly = teIdRemision.EditValue > 0
        Me.gcCantidad.OptionsColumn.ReadOnly = teIdRemision.EditValue > 0
        Me.gcDescripcion.OptionsColumn.ReadOnly = teIdRemision.EditValue > 0
        Me.gcPrecioUnitario.OptionsColumn.ReadOnly = teIdRemision.EditValue > 0
        Me.gcPrecioTotal.OptionsColumn.ReadOnly = teIdRemision.EditValue > 0

        If gsNombre_Empresa.StartsWith("CARBAZEL") Then
            If SiEsNulo(EntUsuario.IdBodega, 0) <> piIdBodega Then 'si no es la bodega princiapal se bloquean
                leBodega.Enabled = False
                gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None
            End If
        End If

    End Sub
    Private Sub gv_DoubleClick(sender As Object, e As EventArgs) Handles gv.DoubleClick
        If meConcepto.Properties.ReadOnly Then
            Exit Sub
        End If

        Dim IdCentro As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdCentro")), "")
        Dim frmSeleccionaCC As New frmDetallarCentroCosto
        Me.AddOwnedForm(frmSeleccionaCC)
        frmSeleccionaCC.IdCentro = IdCentro
        frmSeleccionaCC.ShowDialog()

        IdCentro = SiEsNulo(frmSeleccionaCC.IdCentro, "")
        gv.SetFocusedRowCellValue("IdCentro", IdCentro)
        frmSeleccionaCC.Dispose()
    End Sub
End Class
