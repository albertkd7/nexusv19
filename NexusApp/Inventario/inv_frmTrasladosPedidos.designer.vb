﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmTrasladosPedidos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(inv_frmTrasladosPedidos))
        Me.gcRM = New DevExpress.XtraGrid.GridControl
        Me.gvRM = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.NroComprobante = New DevExpress.XtraGrid.Columns.GridColumn
        Me.FechaComp = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        CType(Me.gcRM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvRM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcRM
        '
        Me.gcRM.Dock = System.Windows.Forms.DockStyle.Left
        Me.gcRM.Location = New System.Drawing.Point(0, 0)
        Me.gcRM.MainView = Me.gvRM
        Me.gcRM.Name = "gcRM"
        Me.gcRM.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1})
        Me.gcRM.Size = New System.Drawing.Size(839, 349)
        Me.gcRM.TabIndex = 3
        Me.gcRM.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvRM})
        '
        'gvRM
        '
        Me.gvRM.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.NroComprobante, Me.FechaComp, Me.GridColumn6, Me.GridColumn3, Me.GridColumn2, Me.GridColumn1, Me.GridColumn5})
        Me.gvRM.GridControl = Me.gcRM
        Me.gvRM.Name = "gvRM"
        Me.gvRM.OptionsView.ShowFooter = True
        Me.gvRM.OptionsView.ShowGroupPanel = False
        '
        'NroComprobante
        '
        Me.NroComprobante.Caption = "No. Remisión"
        Me.NroComprobante.FieldName = "Numero"
        Me.NroComprobante.Name = "NroComprobante"
        Me.NroComprobante.OptionsColumn.AllowEdit = False
        Me.NroComprobante.OptionsColumn.AllowFocus = False
        Me.NroComprobante.Visible = True
        Me.NroComprobante.VisibleIndex = 0
        Me.NroComprobante.Width = 107
        '
        'FechaComp
        '
        Me.FechaComp.Caption = "Fecha Comprob."
        Me.FechaComp.FieldName = "Fecha"
        Me.FechaComp.Name = "FechaComp"
        Me.FechaComp.OptionsColumn.AllowEdit = False
        Me.FechaComp.OptionsColumn.AllowFocus = False
        Me.FechaComp.Visible = True
        Me.FechaComp.VisibleIndex = 1
        Me.FechaComp.Width = 100
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Sucursal Envía"
        Me.GridColumn6.FieldName = "Sucursal"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.OptionsColumn.AllowEdit = False
        Me.GridColumn6.OptionsColumn.AllowFocus = False
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 2
        Me.GridColumn6.Width = 160
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Cliente"
        Me.GridColumn3.FieldName = "Nombre"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.AllowEdit = False
        Me.GridColumn3.OptionsColumn.AllowFocus = False
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 3
        Me.GridColumn3.Width = 281
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Creado Por"
        Me.GridColumn2.FieldName = "CreadoPor"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.OptionsColumn.AllowFocus = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 4
        Me.GridColumn2.Width = 85
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "GridColumn1"
        Me.GridColumn1.FieldName = "IdComprobante"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "IdBodega"
        Me.GridColumn5.FieldName = "IdBodega"
        Me.GridColumn5.Name = "GridColumn5"
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatString = "dt"
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEdit1.EditFormat.FormatString = "dt"
        Me.RepositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        Me.RepositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        '
        'inv_frmTrasladosPedidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(840, 349)
        Me.Controls.Add(Me.gcRM)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "inv_frmTrasladosPedidos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Remisiones Pendientes de Ingreso"
        CType(Me.gcRM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvRM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gcRM As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvRM As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents NroComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents FechaComp As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
End Class
