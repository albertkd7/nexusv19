﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class inv_frmGrupos
    Dim entidad As inv_Grupos
    Dim entCuentas As con_Cuentas

    Private Sub ban_frmTiposTransaccion_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub ban_frmTiposTransaccion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.inv_GruposSelectAll
        entidad = objTablas.inv_GruposSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdGrupo"))
        objCombos.invCategoriaProducto(leCategoria, "")
        objCombos.invCategoriaProducto(riteCategoria, "")
        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub cban_frmTiposTransaccion_Nuevo_Click() Handles Me.Nuevo
        entidad = New inv_Grupos
        entidad.IdGrupo = objFunciones.ObtenerUltimoId("INV_GRUPOS", "IdGrupo") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub ban_frmTiposTransaccion_Save_Click() Handles Me.Guardar
        If teNombre.EditValue = "" Or SiEsNulo(leCategoria.EditValue, 0) = 0 Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [ Nombre, Categoría]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.inv_GruposInsert(entidad)
        Else
            objTablas.inv_GruposUpdate(entidad)
        End If
        gc.DataSource = objTablas.inv_GruposSelectAll
        entidad = objTablas.inv_GruposSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdGrupo"))
        CargaPantalla()
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub ban_frmTiposTransaccion_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.inv_GruposDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.inv_GruposSelectAll
                entidad = objTablas.inv_GruposSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdGrupo"))
                CargaPantalla()
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL REGISTRO:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub ban_frmTiposTransaccion_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entidad
            teId.EditValue = .IdGrupo
            teNombre.EditValue = .Nombre
            beCta01.EditValue = .IdCuentaIngreso
            beCta02.EditValue = .IdCuentaInventario
            beCta03.EditValue = .IdCuentaCostos
            beCta04.EditValue = .IdCuentaRebajas
            beCta05.EditValue = .IdCuentaDevolucion
            seUlt.EditValue = .UltCodProducto
            leCategoria.EditValue = .IdCategoria
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entidad
            .IdGrupo = teId.EditValue
            .Nombre = teNombre.EditValue
            .IdCuentaIngreso = beCta01.EditValue
            .IdCuentaInventario = beCta02.EditValue
            .IdCuentaCostos = beCta03.EditValue
            .IdCuentaRebajas = beCta04.EditValue
            .IdCuentaDevolucion = beCta05.EditValue
            .UltCodProducto = seUlt.EditValue
            .pje1 = 0.0
            .pje2 = 0.0
            .pje3 = 0.0
            .pje4 = 0.0
            .IdCategoria = leCategoria.EditValue
        End With
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entidad = objTablas.inv_GruposSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdGrupo"))
        CargaPantalla()
    End Sub

    Private Sub cban_frmTiposTransaccion_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teId.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub
    Private Sub beCta01_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta01.ButtonClick
        beCta01.EditValue = ""
        beCta01_Validated(beCta01, New System.EventArgs)
    End Sub
    Private Sub beCta02_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta02.ButtonClick
        beCta02.EditValue = ""
        beCta02_Validated(beCta01, New System.EventArgs)
    End Sub
    Private Sub beCta03_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta03.ButtonClick
        beCta03.EditValue = ""
        beCta03_Validated(beCta03, New System.EventArgs)
    End Sub
    Private Sub beCta04_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta04.ButtonClick
        beCta04.EditValue = ""
        beCta04_Validated(beCta04, New System.EventArgs)
    End Sub
    Private Sub beCta05_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta05.ButtonClick
        beCta05.EditValue = ""
        beCta05_Validated(beCta05, New System.EventArgs)
    End Sub
    Private Sub beCta01_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta01.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta01.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta01.EditValue = ""
            teCta01.EditValue = ""
            Exit Sub
        End If
        beCta01.EditValue = entCuentas.IdCuenta
        teCta01.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta02_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta02.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta02.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta02.EditValue = ""
            teCta02.EditValue = ""
            Exit Sub
        End If
        beCta02.EditValue = entCuentas.IdCuenta
        teCta02.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta03_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta03.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta03.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta03.EditValue = ""
            teCta03.EditValue = ""
            Exit Sub
        End If
        beCta03.EditValue = entCuentas.IdCuenta
        teCta03.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta04_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta04.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta04.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta04.EditValue = ""
            teCta04.EditValue = ""
            Exit Sub
        End If
        beCta04.EditValue = entCuentas.IdCuenta
        teCta04.EditValue = entCuentas.Nombre
    End Sub
    Private Sub beCta05_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta05.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta05.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta05.EditValue = ""
            teCta05.EditValue = ""
            Exit Sub
        End If
        beCta05.EditValue = entCuentas.IdCuenta
        teCta05.EditValue = entCuentas.Nombre
    End Sub
End Class
