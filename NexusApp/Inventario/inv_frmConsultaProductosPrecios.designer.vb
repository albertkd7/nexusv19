﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmConsultaProductosPrecios
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gcEx1 = New DevExpress.XtraGrid.GridControl()
        Me.gvEx1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPr1 = New DevExpress.XtraGrid.GridControl()
        Me.gvPr1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdPrecio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.rileIdPrecio1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDescuento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcMargen = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcProductos = New DevExpress.XtraGrid.GridControl()
        Me.gvProd1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcUnidadMedida = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPrecioCosto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCostoUltCompra = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.lblProducto = New DevExpress.XtraEditors.LabelControl()
        Me.leGrupo = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.btAplicar = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.rgTipoBusqueda = New DevExpress.XtraEditors.RadioGroup()
        Me.txtCodigo = New DevExpress.XtraEditors.TextEdit()
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit()
        CType(Me.gcEx1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvEx1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcPr1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPr1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rileIdPrecio1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvProd1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipoBusqueda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcEx1
        '
        Me.gcEx1.Location = New System.Drawing.Point(662, 67)
        Me.gcEx1.MainView = Me.gvEx1
        Me.gcEx1.Name = "gcEx1"
        Me.gcEx1.Size = New System.Drawing.Size(284, 165)
        Me.gcEx1.TabIndex = 61
        Me.gcEx1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvEx1})
        '
        'gvEx1
        '
        Me.gvEx1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn8})
        Me.gvEx1.GridControl = Me.gcEx1
        Me.gvEx1.Name = "gvEx1"
        Me.gvEx1.OptionsBehavior.Editable = False
        Me.gvEx1.OptionsView.ShowFooter = True
        Me.gvEx1.OptionsView.ShowGroupPanel = False
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Bodega"
        Me.GridColumn7.FieldName = "Bodega"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.OptionsColumn.AllowEdit = False
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 0
        Me.GridColumn7.Width = 167
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Saldo"
        Me.GridColumn8.DisplayFormat.FormatString = "###,##0.00"
        Me.GridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn8.FieldName = "Saldo"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)})
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 1
        Me.GridColumn8.Width = 112
        '
        'gcPr1
        '
        Me.gcPr1.Location = New System.Drawing.Point(12, 367)
        Me.gcPr1.MainView = Me.gvPr1
        Me.gcPr1.Name = "gcPr1"
        Me.gcPr1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rileIdPrecio1})
        Me.gcPr1.Size = New System.Drawing.Size(648, 134)
        Me.gcPr1.TabIndex = 59
        Me.gcPr1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvPr1})
        '
        'gvPr1
        '
        Me.gvPr1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcId, Me.gcIdPrecio, Me.GridColumn9, Me.GridColumn1, Me.gcDescuento, Me.gcMargen})
        Me.gvPr1.GridControl = Me.gcPr1
        Me.gvPr1.Name = "gvPr1"
        Me.gvPr1.OptionsView.ShowGroupPanel = False
        '
        'gcId
        '
        Me.gcId.Caption = "IdPrecio"
        Me.gcId.FieldName = "IdPrecio"
        Me.gcId.Name = "gcId"
        Me.gcId.OptionsColumn.ReadOnly = True
        Me.gcId.Visible = True
        Me.gcId.VisibleIndex = 0
        Me.gcId.Width = 57
        '
        'gcIdPrecio
        '
        Me.gcIdPrecio.Caption = "Descripcion"
        Me.gcIdPrecio.ColumnEdit = Me.rileIdPrecio1
        Me.gcIdPrecio.FieldName = "IdPrecio"
        Me.gcIdPrecio.Name = "gcIdPrecio"
        Me.gcIdPrecio.OptionsColumn.AllowEdit = False
        Me.gcIdPrecio.OptionsColumn.ReadOnly = True
        Me.gcIdPrecio.Visible = True
        Me.gcIdPrecio.VisibleIndex = 1
        Me.gcIdPrecio.Width = 194
        '
        'rileIdPrecio1
        '
        Me.rileIdPrecio1.AutoHeight = False
        Me.rileIdPrecio1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rileIdPrecio1.Name = "rileIdPrecio1"
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Precio Venta"
        Me.GridColumn9.DisplayFormat.FormatString = "###,##0.0000"
        Me.GridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn9.FieldName = "Precio"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 2
        Me.GridColumn9.Width = 96
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Precio Neto"
        Me.GridColumn1.DisplayFormat.FormatString = "###,##0.0000"
        Me.GridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn1.FieldName = "PrecioNeto"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 3
        Me.GridColumn1.Width = 105
        '
        'gcDescuento
        '
        Me.gcDescuento.Caption = "Descuento (%)"
        Me.gcDescuento.DisplayFormat.FormatString = "###,##0.00"
        Me.gcDescuento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcDescuento.FieldName = "Descuento"
        Me.gcDescuento.Name = "gcDescuento"
        Me.gcDescuento.Visible = True
        Me.gcDescuento.VisibleIndex = 4
        Me.gcDescuento.Width = 82
        '
        'gcMargen
        '
        Me.gcMargen.Caption = "Margen (%)"
        Me.gcMargen.DisplayFormat.FormatString = "###,##0.00"
        Me.gcMargen.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcMargen.FieldName = "Margen"
        Me.gcMargen.Name = "gcMargen"
        Me.gcMargen.Visible = True
        Me.gcMargen.VisibleIndex = 5
        Me.gcMargen.Width = 96
        '
        'gcProductos
        '
        Me.gcProductos.Location = New System.Drawing.Point(12, 66)
        Me.gcProductos.MainView = Me.gvProd1
        Me.gcProductos.Name = "gcProductos"
        Me.gcProductos.Size = New System.Drawing.Size(648, 299)
        Me.gcProductos.TabIndex = 38
        Me.gcProductos.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvProd1})
        '
        'gvProd1
        '
        Me.gvProd1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn10, Me.GridColumn11, Me.gcUnidadMedida, Me.gcPrecioCosto, Me.gcCostoUltCompra})
        Me.gvProd1.GridControl = Me.gcProductos
        Me.gvProd1.Name = "gvProd1"
        Me.gvProd1.OptionsBehavior.Editable = False
        Me.gvProd1.OptionsView.ShowGroupPanel = False
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Cód.Producto"
        Me.GridColumn10.FieldName = "IdProducto"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 0
        Me.GridColumn10.Width = 104
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Nombre"
        Me.GridColumn11.FieldName = "Nombre"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 1
        Me.GridColumn11.Width = 295
        '
        'gcUnidadMedida
        '
        Me.gcUnidadMedida.Caption = "Unidad Medida"
        Me.gcUnidadMedida.FieldName = "UnidadMedida"
        Me.gcUnidadMedida.Name = "gcUnidadMedida"
        Me.gcUnidadMedida.Visible = True
        Me.gcUnidadMedida.VisibleIndex = 2
        Me.gcUnidadMedida.Width = 73
        '
        'gcPrecioCosto
        '
        Me.gcPrecioCosto.Caption = "Precio Costo"
        Me.gcPrecioCosto.FieldName = "PrecioCosto"
        Me.gcPrecioCosto.GroupFormat.FormatString = "n6"
        Me.gcPrecioCosto.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioCosto.Name = "gcPrecioCosto"
        Me.gcPrecioCosto.Visible = True
        Me.gcPrecioCosto.VisibleIndex = 3
        Me.gcPrecioCosto.Width = 81
        '
        'gcCostoUltCompra
        '
        Me.gcCostoUltCompra.Caption = "Costo Ult Compra"
        Me.gcCostoUltCompra.FieldName = "CostoUltCompra"
        Me.gcCostoUltCompra.Name = "gcCostoUltCompra"
        Me.gcCostoUltCompra.Visible = True
        Me.gcCostoUltCompra.VisibleIndex = 4
        Me.gcCostoUltCompra.Width = 77
        '
        'lblProducto
        '
        Me.lblProducto.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProducto.Appearance.ForeColor = System.Drawing.Color.Black
        Me.lblProducto.Appearance.Options.UseFont = True
        Me.lblProducto.Appearance.Options.UseForeColor = True
        Me.lblProducto.Location = New System.Drawing.Point(110, 5)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(0, 14)
        Me.lblProducto.TabIndex = 62
        '
        'leGrupo
        '
        Me.leGrupo.EnterMoveNextControl = True
        Me.leGrupo.Location = New System.Drawing.Point(338, 24)
        Me.leGrupo.Name = "leGrupo"
        Me.leGrupo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leGrupo.Size = New System.Drawing.Size(322, 20)
        Me.leGrupo.TabIndex = 63
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(303, 27)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl4.TabIndex = 64
        Me.LabelControl4.Text = "Grupo:"
        '
        'btAplicar
        '
        Me.btAplicar.Image = Global.Nexus.My.Resources.Resources.autoriza
        Me.btAplicar.Location = New System.Drawing.Point(664, 367)
        Me.btAplicar.Name = "btAplicar"
        Me.btAplicar.Size = New System.Drawing.Size(127, 34)
        Me.btAplicar.TabIndex = 65
        Me.btAplicar.Text = "Aplicar Cambios"
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(55, 26)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl24.TabIndex = 69
        Me.LabelControl24.Text = "Tipo de Busqueda:"
        '
        'rgTipoBusqueda
        '
        Me.rgTipoBusqueda.Location = New System.Drawing.Point(146, 23)
        Me.rgTipoBusqueda.Name = "rgTipoBusqueda"
        Me.rgTipoBusqueda.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Contiene"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Inicia Con")})
        Me.rgTipoBusqueda.Size = New System.Drawing.Size(153, 21)
        Me.rgTipoBusqueda.TabIndex = 68
        '
        'txtCodigo
        '
        Me.txtCodigo.Location = New System.Drawing.Point(13, 45)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(131, 20)
        Me.txtCodigo.TabIndex = 0
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(146, 45)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(514, 20)
        Me.txtNombre.TabIndex = 1
        '
        'inv_frmConsultaProductosPrecios
        '
        Me.ClientSize = New System.Drawing.Size(949, 527)
        Me.Controls.Add(Me.LabelControl24)
        Me.Controls.Add(Me.rgTipoBusqueda)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.btAplicar)
        Me.Controls.Add(Me.leGrupo)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.lblProducto)
        Me.Controls.Add(Me.gcEx1)
        Me.Controls.Add(Me.gcPr1)
        Me.Controls.Add(Me.gcProductos)
        Me.Modulo = "Inventario"
        Me.Name = "inv_frmConsultaProductosPrecios"
        Me.OptionId = "002008"
        Me.Text = "Asignar Precios de Venta a Productos"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.gcProductos, 0)
        Me.Controls.SetChildIndex(Me.gcPr1, 0)
        Me.Controls.SetChildIndex(Me.gcEx1, 0)
        Me.Controls.SetChildIndex(Me.lblProducto, 0)
        Me.Controls.SetChildIndex(Me.LabelControl4, 0)
        Me.Controls.SetChildIndex(Me.leGrupo, 0)
        Me.Controls.SetChildIndex(Me.btAplicar, 0)
        Me.Controls.SetChildIndex(Me.txtNombre, 0)
        Me.Controls.SetChildIndex(Me.txtCodigo, 0)
        Me.Controls.SetChildIndex(Me.rgTipoBusqueda, 0)
        Me.Controls.SetChildIndex(Me.LabelControl24, 0)
        CType(Me.gcEx1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvEx1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcPr1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPr1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rileIdPrecio1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvProd1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipoBusqueda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gcEx1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvEx1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPr1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvPr1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rileIdPrecio1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcMargen As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcProductos As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvProd1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblProducto As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcPrecioCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leGrupo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btAplicar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcUnidadMedida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgTipoBusqueda As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents txtCodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gcCostoUltCompra As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
End Class
