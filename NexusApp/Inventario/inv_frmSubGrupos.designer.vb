﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmSubGrupos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdSubGrupo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdGrupo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leGrupo1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.leGrupo = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdSubGrupo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leGrupo1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.leGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdSubGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.gc)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(387, 258)
        Me.PanelControl1.TabIndex = 4
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(2, 2)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.leGrupo1})
        Me.gc.Size = New System.Drawing.Size(383, 254)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdSubGrupo, Me.gcIdGrupo, Me.gcNombre})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdSubGrupo
        '
        Me.gcIdSubGrupo.Caption = "IdSubGrupo"
        Me.gcIdSubGrupo.FieldName = "IdSubGrupo"
        Me.gcIdSubGrupo.Name = "gcIdSubGrupo"
        Me.gcIdSubGrupo.Visible = True
        Me.gcIdSubGrupo.VisibleIndex = 0
        Me.gcIdSubGrupo.Width = 151
        '
        'gcIdGrupo
        '
        Me.gcIdGrupo.Caption = "Grupo"
        Me.gcIdGrupo.ColumnEdit = Me.leGrupo1
        Me.gcIdGrupo.FieldName = "IdGrupo"
        Me.gcIdGrupo.Name = "gcIdGrupo"
        Me.gcIdGrupo.Visible = True
        Me.gcIdGrupo.VisibleIndex = 1
        Me.gcIdGrupo.Width = 313
        '
        'leGrupo1
        '
        Me.leGrupo1.AutoHeight = False
        Me.leGrupo1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leGrupo1.Name = "leGrupo1"
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 2
        Me.gcNombre.Width = 306
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.leGrupo)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.teIdSubGrupo)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.teNombre)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(387, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(616, 258)
        Me.PanelControl2.TabIndex = 5
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(14, 90)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(170, 13)
        Me.LabelControl3.TabIndex = 37
        Me.LabelControl3.Text = "Grupo o Familia a la que pertenece:"
        '
        'leGrupo
        '
        Me.leGrupo.EnterMoveNextControl = True
        Me.leGrupo.Location = New System.Drawing.Point(14, 109)
        Me.leGrupo.Name = "leGrupo"
        Me.leGrupo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leGrupo.Size = New System.Drawing.Size(273, 20)
        Me.leGrupo.TabIndex = 36
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(14, 9)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(67, 13)
        Me.LabelControl1.TabIndex = 34
        Me.LabelControl1.Text = "Id Sub Grupo:"
        '
        'teIdSubGrupo
        '
        Me.teIdSubGrupo.EnterMoveNextControl = True
        Me.teIdSubGrupo.Location = New System.Drawing.Point(14, 25)
        Me.teIdSubGrupo.Name = "teIdSubGrupo"
        Me.teIdSubGrupo.Size = New System.Drawing.Size(95, 20)
        Me.teIdSubGrupo.TabIndex = 27
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(14, 49)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 35
        Me.LabelControl2.Text = "Nombre:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(14, 66)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(274, 20)
        Me.teNombre.TabIndex = 28
        '
        'inv_frmSubGrupos
        '
        Me.ClientSize = New System.Drawing.Size(1003, 283)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.DbMode = 1
        Me.Modulo = "Inventario"
        Me.Name = "inv_frmSubGrupos"
        Me.OptionId = "001002"
        Me.Text = "Sub Grupos"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.PanelControl2, 0)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leGrupo1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.leGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdSubGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdSubGrupo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gcIdSubGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leGrupo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents gcIdGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leGrupo1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit

End Class
