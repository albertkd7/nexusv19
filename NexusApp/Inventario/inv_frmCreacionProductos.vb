﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Imports System.IO
Public Class inv_frmCreacionProductos
    Dim blInv As New InventarioBLL(g_ConnectionString)
    Dim ProDetalle As List(Of inv_Productos), PreciosDetalle As List(Of inv_ProductosPrecios)
    Declare Sub Sleep Lib "kernel32" Alias "Sleep" _
(ByVal dwMilliseconds As Long)

    Private Sub inv_frmTomaFisica_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.invGrupos(LeGrupo)
        objCombos.invSubGruposTodos(LeSubGrupo)
        gv.BestFitColumns()
    End Sub
    Private Sub btAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAplicar.Click
        Dim filtrado As Integer = SiEsNulo(gv.ActiveFilter.Count, 0)
        If filtrado > 0 Then
            MsgBox("Debe quitar los filtros en la consulta para aplicar ", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        ProDetalle = New List(Of inv_Productos)
        PreciosDetalle = New List(Of inv_ProductosPrecios)
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "Existe") = 0 Then
                Dim entPro As New inv_Productos
                With entPro
                    .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                    .CodBarra = gv.GetRowCellValue(i, "IdProducto")
                    .Nombre = gv.GetRowCellValue(i, "Nombre")
                    .IdGrupo = gv.GetRowCellValue(i, "IdGrupo")
                    .IdSubGrupo = gv.GetRowCellValue(i, "IdSubGrupo")
                    .IdMarca = 1
                    .IdUnidad = gv.GetRowCellValue(i, "UnidadMedida")
                    .IdProveedor = ""
                    .IdUbicacion = 1
                    .Talla = ""
                    .Color = ""
                    .Estilo = ""
                    .UnidadesPresentacion = 1
                    .TipoProducto = 1
                    .EstadoProducto = 1
                    .PrecioCosto = SiEsNulo(gv.GetRowCellValue(i, "PrecioCosto"), 0)
                    .EsExento = 0
                    .EsEspecial = 0
                    .PermiteFacturar = 1
                    .IdCuentaIng = "51010101"
                    .IdCuentaInv = "110601"
                    .IdCuentaCos = "41010101"
                    .ArchivoImagen = ""
                    .ExistenciaMaxima = 0
                    .ExistenciaMinima = 0
                    .InformacionAdicional = ""
                    .IdCategoria = 1
                    .IdClasificacion = 1
                    .PorcComision = 0 ' seComision.EditValue
                    .IdCentro = ""
                    .Origen = 1
                    .Cuantia = ""
                    .Traduccion = ""
                    .FechaIngreso = Now
                    .FechaHoraCreacion = Now
                    .CreadoPor = objMenu.User
                End With
                'Creo la entidad del Producto
                ProDetalle.Add(entPro)


                Dim entPrecio As New inv_ProductosPrecios
                With entPrecio
                    .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                    .IdPrecio = 1
                    .Precio = SiEsNulo(gv.GetRowCellValue(i, "Precio1"), 0)
                    .Descuento = 0
                    .Margen = 0
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                End With
                PreciosDetalle.Add(entPrecio)
                Dim entPrecio1 As New inv_ProductosPrecios
                With entPrecio1
                    .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                    .IdPrecio = 2
                    .Precio = SiEsNulo(gv.GetRowCellValue(i, "PrecioFinal"), 0)
                    .Descuento = 0
                    .Margen = 0
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                End With
                PreciosDetalle.Add(entPrecio1)
                Dim entPrecio2 As New inv_ProductosPrecios
                With entPrecio2
                    .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                    .IdPrecio = 3
                    .Precio = SiEsNulo(gv.GetRowCellValue(i, "PrecioMayoreo"), 0)
                    .Descuento = 0
                    .Margen = 0
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                End With
                PreciosDetalle.Add(entPrecio2)
            End If
        Next

        Dim msj As String = ""
        msj = blInv.inv_InsertaProductosMasivos(ProDetalle, PreciosDetalle)
        If msj = "" Then
        Else
            MsgBox("SE DETECTÓ UN ERROR AL APLICAR LA ENTRADA DE INVENTARIO DE AJUSTE" + Chr(13) + msj, MsgBoxStyle.Critical)
            Exit Sub
        End If
        MsgBox("Los productos se crearon con éxito", MsgBoxStyle.Information)
        Me.Close()
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        ' gv.SetRowCellValue(gv.FocusedRowHandle, "Conteo", 0.0)
    End Sub

    Private Sub sbImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbImportar.Click
        Dim Delimitador As String = InputBox("Especifique el deliminator del archivo, el cual puede ser coma , * |", "Delimitador", "|")
        If Delimitador = "" Then
            Return
        End If
        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            Return
        End If
        Dim csv_file As System.IO.StreamReader = File.OpenText(ofd.FileName)
        Try
            gc.DataSource = blInv.inv_ObtieneCreaProdcutosEstructura()
            Dim IDPR As String

            While csv_file.Peek() > 0

                Dim sLine As String = csv_file.ReadLine()
                Dim aData As Array = sLine.Split(Delimitador)
                IDPR = blInv.inv_VerificaProductoExiste(aData(0))
                gv.AddNewRow()
                gv.SetRowCellValue(gv.FocusedRowHandle, "IdProducto", aData(0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Nombre", SiEsNulo(aData(1), "-S/N-"))
                gv.SetRowCellValue(gv.FocusedRowHandle, "UnidadMedida", SiEsNulo(aData(2), 1))
                gv.SetRowCellValue(gv.FocusedRowHandle, "IdGrupo", SiEsNulo(aData(3), 1))
                gv.SetRowCellValue(gv.FocusedRowHandle, "IdSubGrupo", SiEsNulo(aData(4), 1))
                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioCosto", SiEsNulo(aData(5), 0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Precio1", SiEsNulo(aData(6), 0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioFinal", SiEsNulo(aData(7), 0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioMayoreo", SiEsNulo(aData(8), 0))
                If IDPR <> "" Then
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Existe", 1)
                Else
                    gv.SetRowCellValue(gv.FocusedRowHandle, "Existe", 0)
                End If
                gv.UpdateCurrentRow()
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error al importar")
            Return
        End Try
        csv_file.Close()
    End Sub

    Private Sub cmdBorrar_Click(sender As Object, e As EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub
    Private Sub gv_RowCellStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles gv.RowCellStyle
        Dim TG As Integer = SiEsNulo(gv.GetRowCellValue(e.RowHandle, "Existe"), 0)
        If TG = 0 Then
            e.Appearance.ForeColor = Color.Red
        End If
    End Sub


End Class
