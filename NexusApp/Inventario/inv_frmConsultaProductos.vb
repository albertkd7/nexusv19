Imports NexusBLL
Public Class inv_frmConsultaProductos
    Dim blInventa As New InventarioBLL(g_ConnectionString)
    Dim Prod As DataTable, lDummy As Boolean = False
    Dim nxDirectory As String = System.Configuration.ConfigurationManager.AppSettings("NxAPICloudFiles:Directory")
    Dim nxDisabled As Boolean = IIf(System.Configuration.ConfigurationManager.AppSettings("NxAPICloudFiles:Disabled") = "True", True, False)

    Private Sub inv_frmConsultaProductos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        teNombre.Focus()
    End Sub
    Private Sub inv_frmConsultaProductos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'gvProd.Columns.Clear()
        gcProductos.DataSource = New DataView(cnsDataTable)
        IdProducto = ""
        teCodigo.EditValue = ""
        teNombre.EditValue = ""
        lDummy = False
        'gvProd.BestFitColumns()
        'gvProd.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle
        objCombos.inv_Precios(rileIdPrecio)
        teNombre.Focus()
    End Sub

    Private _cnsDatatable As DataTable
    Public Property cnsDataTable() As DataTable
        Get
            Return _cnsDatatable
        End Get
        Set(ByVal value As DataTable)
            _cnsDatatable = value
        End Set
    End Property
    Private _IdProducto As String = ""
    Public Property IdProducto() As String
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As String)
            _IdProducto = value
        End Set
    End Property

    Private Sub gvProd_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gvProd.FocusedRowChanged
        'IdProducto = gvProd.GetRowCellValue(gvProd.FocusedRowHandle, "IdProducto")
        'Prod = blInventa.inv_GeneralesProducto(IdProducto)

        'gcPr.DataSource = blInventa.inv_ObtienePrecios(IdProducto, objMenu.User)
        'gcEx.DataSource = blInventa.inv_ObtieneSaldosPorProducto(-1, IdProducto)
        'If Prod.Rows.Count = 0 Then
        '    Exit Sub
        'End If

        'teGrupo.EditValue = Prod.Rows(0).Item("Grupo")
        'teSubGrupo.EditValue = Prod.Rows(0).Item("SubGrupo")
        'teUnidad.EditValue = Prod.Rows(0).Item("UnidadMedida")
        'teMarca.EditValue = Prod.Rows(0).Item("Marca")
        'teProveedor.EditValue = Prod.Rows(0).Item("Proveedor")
        'teColor.EditValue = Prod.Rows(0).Item("Color")
        'teTalla.EditValue = Prod.Rows(0).Item("Talla")
        'teEstilo.EditValue = Prod.Rows(0).Item("Estilo")
        'teUnidadesPre.EditValue = Prod.Rows(0).Item("UnidadesPresentacion")
        'meInfo.EditValue = Prod.Rows(0).Item("InformacionAdicional")
        'Dim ArchivoImagen As String = Prod.Rows(0).Item("ArchivoImagen")
        'If String.IsNullOrEmpty(nxDirectory) = False And nxDisabled Then
        '    peFoto.Image = ArchivoImagen.BuscarImagenEnServidor()
        'ElseIf Not FileIO.FileSystem.FileExists(ArchivoImagen) Or ArchivoImagen = "" Then
        '    peFoto.Image = Nothing
        'Else
        '    Dim bm As New Bitmap(ArchivoImagen)
        '    peFoto.Image = bm
        'End If
    End Sub
    Public Sub GeneralesPro(ByVal Idproducto As String)
        Prod = blInventa.inv_GeneralesProducto(Idproducto)
        If Prod.Rows.Count = 0 Then
            Exit Sub
        End If
        teGrupo.EditValue = Prod.Rows(0).Item("Grupo")
        teSubGrupo.EditValue = Prod.Rows(0).Item("SubGrupo")
        teUnidad.EditValue = Prod.Rows(0).Item("UnidadMedida")
        teMarca.EditValue = Prod.Rows(0).Item("Marca")
        teProveedor.EditValue = Prod.Rows(0).Item("Proveedor")
        teColor.EditValue = Prod.Rows(0).Item("Color")
        teTalla.EditValue = Prod.Rows(0).Item("Talla")
        teEstilo.EditValue = Prod.Rows(0).Item("Estilo")
        teUnidadesPre.EditValue = Prod.Rows(0).Item("UnidadesPresentacion")
        meInfo.EditValue = Prod.Rows(0).Item("InformacionAdicional")
        TeCuentaContableIng.EditValue = Prod.Rows(0).Item("CuentaContableIngre")
        TeCuentaContableInv.EditValue = Prod.Rows(0).Item("CuentaContableInv")
        TeCuentaContableCos.EditValue = Prod.Rows(0).Item("CuentaContableCos")

        Dim ArchivoImagen As String = Prod.Rows(0).Item("ArchivoImagen")
        If String.IsNullOrEmpty(nxDirectory) = False And nxDisabled Then
            peFoto.Image = ArchivoImagen.BuscarImagenEnServidor()
        ElseIf Not FileIO.FileSystem.FileExists(ArchivoImagen) Or ArchivoImagen = "" Then
            peFoto.Image = Nothing
        Else
            Dim bm As New Bitmap(ArchivoImagen)
            peFoto.Image = bm
        End If
    End Sub
    Private Sub gvProd_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvProd.KeyDown
        If e.KeyCode = Keys.Enter Then
            IdProducto = gvProd.GetRowCellValue(gvProd.FocusedRowHandle, "IdProducto")
            lDummy = True
            ''Me.Dispose()
            Me.Close()
        End If
    End Sub

    Private Sub gcProductos_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gcProductos.DoubleClick
        IdProducto = gvProd.GetRowCellValue(gvProd.FocusedRowHandle, "IdProducto")
        lDummy = True
        ''   Me.Dispose()
        Me.Close()
    End Sub
    Private Sub inv_frmConsultaProductos_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            IdProducto = ""
            Me.Close()
        End If
    End Sub

#Region "Tipo de Busqueda"
    Private Sub txtNombre_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles teNombre.EditValueChanged
        If rgTipoBusqueda.SelectedIndex = 0 Then
            Dim Transaccion As String
            Dim CadenaPrincipal As String = teNombre.EditValue

            Transaccion = teNombre.EditValue
            If teNombre.EditValue <> "" And Transaccion.IndexOf("%") > 0 Then
                Dim V() As String
                Dim Cadena As String
                Dim SentenciaBusqueda As String = "Nombre like '%"
                Dim x As Integer

                'Cadena a tratar 
                Cadena = teNombre.EditValue

                'Remplazando todos los espaciadores incorrectos por ";" 
                For x = 0 To 1
                    Cadena = Replace(Cadena, "%", ";")
                Next x

                'Separo la cadena gracias al delimitador ";" 
                V = Split(Cadena, ";")

                'Muestro la cadena 
                For x = 0 To UBound(V)
                    If x = 0 Then
                        SentenciaBusqueda += Trim(V(x)) + "%'"
                    Else
                        SentenciaBusqueda += IIf(Trim(V(x)) <> "", " and Nombre like '%" + Trim(V(x)) + "%'", Trim(V(x)))
                    End If
                Next x
                cnsDataTable.DefaultView.RowFilter = (SentenciaBusqueda)
            Else
                cnsDataTable.DefaultView.RowFilter = ("Nombre like '%" + teNombre.EditValue + "%'")
            End If
        Else
            cnsDataTable.DefaultView.RowFilter = ("Nombre like '" + teNombre.EditValue + "%'")
        End If

        gcProductos.DataSource = cnsDataTable.DefaultView
        'IdProducto = gvProd.GetRowCellValue(gvProd.FocusedRowHandle, "IdProducto")
        'gcPr.DataSource = blInventa.inv_ObtienePrecios(IdProducto, objMenu.User)
        'gcEx.DataSource = blInventa.inv_ObtieneSaldosPorProducto(-1, IdProducto)
        'GeneralesPro(IdProducto)
    End Sub

    Private Sub txtCodigo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles teCodigo.EditValueChanged
        If rgTipoBusqueda.SelectedIndex = 0 Then
            cnsDataTable.DefaultView.RowFilter = ("IdProducto like '%" + teCodigo.EditValue + "%'")
        Else
            cnsDataTable.DefaultView.RowFilter = ("IdProducto like '" + teCodigo.EditValue + "%'")
        End If
        IdProducto = gvProd.GetRowCellValue(gvProd.FocusedRowHandle, "IdProducto")
        ' gcPr.DataSource = blInventa.inv_ObtienePrecios(IdProducto, objMenu.User)
        'gcEx.DataSource = blInventa.inv_ObtieneSaldosPorProducto(-1, IdProducto)
        gcProductos.DataSource = cnsDataTable.DefaultView
        'GeneralesPro(IdProducto)
    End Sub

    Private Sub rgTipoBusqueda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgTipoBusqueda.SelectedIndexChanged
        If teCodigo.EditValue <> "" Then
            txtCodigo_EditValueChanged("", New EventArgs)
        Else
            If teNombre.EditValue <> "" Then
                txtNombre_EditValueChanged("", New EventArgs)
            End If
        End If
    End Sub
#End Region

    Private Sub inv_frmConsultaProductos_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' si solo cerro el formulario, no selecciono codigo de producto
        If Not lDummy Then
            IdProducto = ""
        End If
    End Sub


    Private Sub gcProductos_Click(sender As Object, e As EventArgs) Handles gcProductos.Click
        IdProducto = gvProd.GetRowCellValue(gvProd.FocusedRowHandle, "IdProducto")
        Prod = blInventa.inv_GeneralesProducto(IdProducto)

        gcPr.DataSource = blInventa.inv_ObtienePrecios(IdProducto, objMenu.User, pnIVA)
        gcEx.DataSource = blInventa.inv_ObtieneSaldosPorProducto(-1, IdProducto)

        GeneralesPro(IdProducto)
    End Sub
End Class