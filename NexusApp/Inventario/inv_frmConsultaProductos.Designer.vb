<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmConsultaProductos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ColumnDefinition1 As DevExpress.XtraLayout.ColumnDefinition = New DevExpress.XtraLayout.ColumnDefinition()
        Dim ColumnDefinition2 As DevExpress.XtraLayout.ColumnDefinition = New DevExpress.XtraLayout.ColumnDefinition()
        Dim RowDefinition1 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition2 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition3 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition4 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition5 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition6 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition7 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition8 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition9 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition10 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition11 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition12 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition13 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition14 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition15 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition16 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim RowDefinition17 As DevExpress.XtraLayout.RowDefinition = New DevExpress.XtraLayout.RowDefinition()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(inv_frmConsultaProductos))
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.teMarca = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TeCuentaContableCos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.TeCuentaContableInv = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.TeCuentaContableIng = New DevExpress.XtraEditors.TextEdit()
        Me.teSubGrupo = New DevExpress.XtraEditors.TextEdit()
        Me.teGrupo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.meInfo = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.teUnidadesPre = New DevExpress.XtraEditors.TextEdit()
        Me.teProveedor = New DevExpress.XtraEditors.TextEdit()
        Me.teEstilo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.teTalla = New DevExpress.XtraEditors.TextEdit()
        Me.teUnidad = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.teColor = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.peFoto = New DevExpress.XtraEditors.PictureEdit()
        Me.gcProductos = New DevExpress.XtraGrid.GridControl()
        Me.gvProd = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPr = New DevExpress.XtraGrid.GridControl()
        Me.gvPr = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdPrecio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.rileIdPrecio = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcValor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.cgValorNeto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcEx = New DevExpress.XtraGrid.GridControl()
        Me.gvEx = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcBodega = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcSaldo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.rgTipoBusqueda = New DevExpress.XtraEditors.RadioGroup()
        Me.teCodigo = New DevExpress.XtraEditors.TextEdit()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SplitContainerControl2 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SplitContainerControl3 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SplitContainerControl4 = New DevExpress.XtraEditors.SplitContainerControl()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.teMarca.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeCuentaContableCos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeCuentaContableInv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeCuentaContableIng.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSubGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meInfo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teUnidadesPre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teEstilo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTalla.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teUnidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teColor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peFoto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcPr, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPr, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rileIdPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcEx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvEx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipoBusqueda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.SplitContainerControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl2.SuspendLayout()
        CType(Me.SplitContainerControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl3.SuspendLayout()
        CType(Me.SplitContainerControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl4.SuspendLayout()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.LabelControl1.LineVisible = True
        Me.LabelControl1.Location = New System.Drawing.Point(147, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl1.StyleController = Me.LayoutControl1
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Grupo:"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.teMarca)
        Me.LayoutControl1.Controls.Add(Me.LabelControl4)
        Me.LayoutControl1.Controls.Add(Me.TeCuentaContableCos)
        Me.LayoutControl1.Controls.Add(Me.LabelControl13)
        Me.LayoutControl1.Controls.Add(Me.TeCuentaContableInv)
        Me.LayoutControl1.Controls.Add(Me.LabelControl2)
        Me.LayoutControl1.Controls.Add(Me.LabelControl12)
        Me.LayoutControl1.Controls.Add(Me.LabelControl1)
        Me.LayoutControl1.Controls.Add(Me.TeCuentaContableIng)
        Me.LayoutControl1.Controls.Add(Me.teSubGrupo)
        Me.LayoutControl1.Controls.Add(Me.teGrupo)
        Me.LayoutControl1.Controls.Add(Me.LabelControl11)
        Me.LayoutControl1.Controls.Add(Me.LabelControl3)
        Me.LayoutControl1.Controls.Add(Me.meInfo)
        Me.LayoutControl1.Controls.Add(Me.LabelControl6)
        Me.LayoutControl1.Controls.Add(Me.LabelControl10)
        Me.LayoutControl1.Controls.Add(Me.teUnidadesPre)
        Me.LayoutControl1.Controls.Add(Me.teProveedor)
        Me.LayoutControl1.Controls.Add(Me.teEstilo)
        Me.LayoutControl1.Controls.Add(Me.LabelControl5)
        Me.LayoutControl1.Controls.Add(Me.LabelControl9)
        Me.LayoutControl1.Controls.Add(Me.LabelControl8)
        Me.LayoutControl1.Controls.Add(Me.teTalla)
        Me.LayoutControl1.Controls.Add(Me.teUnidad)
        Me.LayoutControl1.Controls.Add(Me.LabelControl7)
        Me.LayoutControl1.Controls.Add(Me.teColor)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(685, 271, 450, 400)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(610, 259)
        Me.LayoutControl1.TabIndex = 56
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'teMarca
        '
        Me.teMarca.Location = New System.Drawing.Point(184, 276)
        Me.teMarca.Name = "teMarca"
        Me.teMarca.Properties.ReadOnly = True
        Me.teMarca.Size = New System.Drawing.Size(397, 20)
        Me.teMarca.StyleController = Me.LayoutControl1
        Me.teMarca.TabIndex = 4
        Me.teMarca.Visible = False
        '
        'LabelControl4
        '
        Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.LabelControl4.Location = New System.Drawing.Point(147, 276)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl4.StyleController = Me.LayoutControl1
        Me.LabelControl4.TabIndex = 1
        Me.LabelControl4.Text = "Marca:"
        Me.LabelControl4.Visible = False
        '
        'TeCuentaContableCos
        '
        Me.TeCuentaContableCos.Location = New System.Drawing.Point(184, 252)
        Me.TeCuentaContableCos.Name = "TeCuentaContableCos"
        Me.TeCuentaContableCos.Properties.ReadOnly = True
        Me.TeCuentaContableCos.Size = New System.Drawing.Size(397, 20)
        Me.TeCuentaContableCos.StyleController = Me.LayoutControl1
        Me.TeCuentaContableCos.TabIndex = 55
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(64, 252)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(116, 13)
        Me.LabelControl13.StyleController = Me.LayoutControl1
        Me.LabelControl13.TabIndex = 54
        Me.LabelControl13.Text = "Cuenta Contable Costo:"
        '
        'TeCuentaContableInv
        '
        Me.TeCuentaContableInv.Location = New System.Drawing.Point(184, 228)
        Me.TeCuentaContableInv.Name = "TeCuentaContableInv"
        Me.TeCuentaContableInv.Properties.ReadOnly = True
        Me.TeCuentaContableInv.Size = New System.Drawing.Size(397, 20)
        Me.TeCuentaContableInv.StyleController = Me.LayoutControl1
        Me.TeCuentaContableInv.TabIndex = 53
        '
        'LabelControl2
        '
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.LabelControl2.LineVisible = True
        Me.LabelControl2.Location = New System.Drawing.Point(125, 36)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl2.StyleController = Me.LayoutControl1
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Sub-Grupo:"
        '
        'LabelControl12
        '
        Me.LabelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.LabelControl12.Location = New System.Drawing.Point(39, 228)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(141, 13)
        Me.LabelControl12.StyleController = Me.LayoutControl1
        Me.LabelControl12.TabIndex = 52
        Me.LabelControl12.Text = "Cuenta Contable Inventario: "
        '
        'TeCuentaContableIng
        '
        Me.TeCuentaContableIng.Location = New System.Drawing.Point(184, 204)
        Me.TeCuentaContableIng.Name = "TeCuentaContableIng"
        Me.TeCuentaContableIng.Properties.ReadOnly = True
        Me.TeCuentaContableIng.Size = New System.Drawing.Size(397, 20)
        Me.TeCuentaContableIng.StyleController = Me.LayoutControl1
        Me.TeCuentaContableIng.TabIndex = 51
        '
        'teSubGrupo
        '
        Me.teSubGrupo.Location = New System.Drawing.Point(184, 36)
        Me.teSubGrupo.Name = "teSubGrupo"
        Me.teSubGrupo.Properties.ReadOnly = True
        Me.teSubGrupo.Size = New System.Drawing.Size(397, 20)
        Me.teSubGrupo.StyleController = Me.LayoutControl1
        Me.teSubGrupo.TabIndex = 3
        '
        'teGrupo
        '
        Me.teGrupo.Location = New System.Drawing.Point(184, 12)
        Me.teGrupo.Name = "teGrupo"
        Me.teGrupo.Properties.ReadOnly = True
        Me.teGrupo.Size = New System.Drawing.Size(397, 20)
        Me.teGrupo.StyleController = Me.LayoutControl1
        Me.teGrupo.TabIndex = 2
        '
        'LabelControl11
        '
        Me.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.LabelControl11.Location = New System.Drawing.Point(55, 204)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(125, 13)
        Me.LabelControl11.StyleController = Me.LayoutControl1
        Me.LabelControl11.TabIndex = 50
        Me.LabelControl11.Text = "Cuenta Contable Ingreso:"
        '
        'LabelControl3
        '
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.LabelControl3.LineVisible = True
        Me.LabelControl3.Location = New System.Drawing.Point(91, 60)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl3.StyleController = Me.LayoutControl1
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Unidad de Medida:"
        '
        'meInfo
        '
        Me.meInfo.Location = New System.Drawing.Point(184, 300)
        Me.meInfo.Name = "meInfo"
        Me.meInfo.Properties.ReadOnly = True
        Me.meInfo.Size = New System.Drawing.Size(397, 52)
        Me.meInfo.StyleController = Me.LayoutControl1
        Me.meInfo.TabIndex = 10
        '
        'LabelControl6
        '
        Me.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.LabelControl6.LineVisible = True
        Me.LabelControl6.Location = New System.Drawing.Point(154, 84)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl6.StyleController = Me.LayoutControl1
        Me.LabelControl6.TabIndex = 1
        Me.LabelControl6.Text = "Talla:"
        '
        'LabelControl10
        '
        Me.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.LabelControl10.Location = New System.Drawing.Point(62, 312)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl10.StyleController = Me.LayoutControl1
        Me.LabelControl10.TabIndex = 1
        Me.LabelControl10.Text = "Informaci�n Adicional:"
        '
        'teUnidadesPre
        '
        Me.teUnidadesPre.Location = New System.Drawing.Point(184, 156)
        Me.teUnidadesPre.Name = "teUnidadesPre"
        Me.teUnidadesPre.Properties.ReadOnly = True
        Me.teUnidadesPre.Size = New System.Drawing.Size(397, 20)
        Me.teUnidadesPre.StyleController = Me.LayoutControl1
        Me.teUnidadesPre.TabIndex = 8
        '
        'teProveedor
        '
        Me.teProveedor.Location = New System.Drawing.Point(184, 180)
        Me.teProveedor.Name = "teProveedor"
        Me.teProveedor.Properties.ReadOnly = True
        Me.teProveedor.Size = New System.Drawing.Size(397, 20)
        Me.teProveedor.StyleController = Me.LayoutControl1
        Me.teProveedor.TabIndex = 9
        '
        'teEstilo
        '
        Me.teEstilo.Location = New System.Drawing.Point(184, 108)
        Me.teEstilo.Name = "teEstilo"
        Me.teEstilo.Properties.ReadOnly = True
        Me.teEstilo.Size = New System.Drawing.Size(397, 20)
        Me.teEstilo.StyleController = Me.LayoutControl1
        Me.teEstilo.TabIndex = 6
        '
        'LabelControl5
        '
        Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.LabelControl5.Location = New System.Drawing.Point(126, 180)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl5.StyleController = Me.LayoutControl1
        Me.LabelControl5.TabIndex = 1
        Me.LabelControl5.Text = "Proveedor:"
        '
        'LabelControl9
        '
        Me.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.LabelControl9.Location = New System.Drawing.Point(48, 156)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(132, 13)
        Me.LabelControl9.StyleController = Me.LayoutControl1
        Me.LabelControl9.TabIndex = 1
        Me.LabelControl9.Text = "Unidades por Presentaci�n:"
        '
        'LabelControl8
        '
        Me.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.LabelControl8.Location = New System.Drawing.Point(151, 108)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl8.StyleController = Me.LayoutControl1
        Me.LabelControl8.TabIndex = 1
        Me.LabelControl8.Text = "Estilo:"
        '
        'teTalla
        '
        Me.teTalla.Location = New System.Drawing.Point(184, 84)
        Me.teTalla.Name = "teTalla"
        Me.teTalla.Properties.ReadOnly = True
        Me.teTalla.Size = New System.Drawing.Size(397, 20)
        Me.teTalla.StyleController = Me.LayoutControl1
        Me.teTalla.TabIndex = 5
        '
        'teUnidad
        '
        Me.teUnidad.Location = New System.Drawing.Point(184, 60)
        Me.teUnidad.Name = "teUnidad"
        Me.teUnidad.Properties.ReadOnly = True
        Me.teUnidad.Size = New System.Drawing.Size(397, 20)
        Me.teUnidad.StyleController = Me.LayoutControl1
        Me.teUnidad.TabIndex = 4
        '
        'LabelControl7
        '
        Me.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
        Me.LabelControl7.Location = New System.Drawing.Point(151, 132)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl7.StyleController = Me.LayoutControl1
        Me.LabelControl7.TabIndex = 1
        Me.LabelControl7.Text = "Color:"
        '
        'teColor
        '
        Me.teColor.Location = New System.Drawing.Point(184, 132)
        Me.teColor.Name = "teColor"
        Me.teColor.Properties.ReadOnly = True
        Me.teColor.Size = New System.Drawing.Size(397, 20)
        Me.teColor.StyleController = Me.LayoutControl1
        Me.teColor.TabIndex = 7
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem1, Me.LayoutControlItem4, Me.LayoutControlItem3, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem20, Me.LayoutControlItem19, Me.LayoutControlItem21, Me.LayoutControlItem23, Me.LayoutControlItem22, Me.LayoutControlItem24, Me.LayoutControlItem26, Me.LayoutControlItem25, Me.LayoutControlItem18, Me.LayoutControlGroup2})
        Me.LayoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        ColumnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent
        ColumnDefinition1.Width = 30.0R
        ColumnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent
        ColumnDefinition2.Width = 70.0R
        Me.LayoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(New DevExpress.XtraLayout.ColumnDefinition() {ColumnDefinition1, ColumnDefinition2})
        RowDefinition1.Height = 24.0R
        RowDefinition1.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition2.Height = 24.0R
        RowDefinition2.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition3.Height = 24.0R
        RowDefinition3.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition4.Height = 24.0R
        RowDefinition4.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition5.Height = 24.0R
        RowDefinition5.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition6.Height = 24.0R
        RowDefinition6.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition7.Height = 24.0R
        RowDefinition7.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition8.Height = 24.0R
        RowDefinition8.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition9.Height = 24.0R
        RowDefinition9.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition10.Height = 24.0R
        RowDefinition10.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition11.Height = 24.0R
        RowDefinition11.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition12.Height = 24.0R
        RowDefinition12.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition13.Height = 41.0R
        RowDefinition13.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition14.Height = 5.0R
        RowDefinition14.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition15.Height = 5.0R
        RowDefinition15.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition16.Height = 5.0R
        RowDefinition16.SizeType = System.Windows.Forms.SizeType.AutoSize
        RowDefinition17.Height = 20.0R
        RowDefinition17.SizeType = System.Windows.Forms.SizeType.AutoSize
        Me.LayoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(New DevExpress.XtraLayout.RowDefinition() {RowDefinition1, RowDefinition2, RowDefinition3, RowDefinition4, RowDefinition5, RowDefinition6, RowDefinition7, RowDefinition8, RowDefinition9, RowDefinition10, RowDefinition11, RowDefinition12, RowDefinition13, RowDefinition14, RowDefinition15, RowDefinition16, RowDefinition17})
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(593, 384)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.LabelControl1
        Me.LayoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(172, 24)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.teGrupo
        Me.LayoutControlItem1.Location = New System.Drawing.Point(172, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem1.Size = New System.Drawing.Size(401, 24)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.LabelControl2
        Me.LayoutControlItem4.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.OptionsTableLayoutItem.RowIndex = 1
        Me.LayoutControlItem4.Size = New System.Drawing.Size(172, 24)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.teSubGrupo
        Me.LayoutControlItem3.Location = New System.Drawing.Point(172, 24)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem3.OptionsTableLayoutItem.RowIndex = 1
        Me.LayoutControlItem3.Size = New System.Drawing.Size(401, 24)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.LabelControl3
        Me.LayoutControlItem5.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.OptionsTableLayoutItem.RowIndex = 2
        Me.LayoutControlItem5.Size = New System.Drawing.Size(172, 24)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.teUnidad
        Me.LayoutControlItem6.Location = New System.Drawing.Point(172, 48)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem6.OptionsTableLayoutItem.RowIndex = 2
        Me.LayoutControlItem6.Size = New System.Drawing.Size(401, 24)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.LabelControl6
        Me.LayoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.OptionsTableLayoutItem.RowIndex = 3
        Me.LayoutControlItem7.Size = New System.Drawing.Size(172, 24)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.teTalla
        Me.LayoutControlItem8.Location = New System.Drawing.Point(172, 72)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem8.OptionsTableLayoutItem.RowIndex = 3
        Me.LayoutControlItem8.Size = New System.Drawing.Size(401, 24)
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextVisible = False
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.LabelControl8
        Me.LayoutControlItem9.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.OptionsTableLayoutItem.RowIndex = 4
        Me.LayoutControlItem9.Size = New System.Drawing.Size(172, 24)
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = False
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.teEstilo
        Me.LayoutControlItem10.Location = New System.Drawing.Point(172, 96)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem10.OptionsTableLayoutItem.RowIndex = 4
        Me.LayoutControlItem10.Size = New System.Drawing.Size(401, 24)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.LabelControl7
        Me.LayoutControlItem11.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.OptionsTableLayoutItem.RowIndex = 5
        Me.LayoutControlItem11.Size = New System.Drawing.Size(172, 24)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.teColor
        Me.LayoutControlItem12.Location = New System.Drawing.Point(172, 120)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem12.OptionsTableLayoutItem.RowIndex = 5
        Me.LayoutControlItem12.Size = New System.Drawing.Size(401, 24)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = False
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.LabelControl9
        Me.LayoutControlItem13.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 144)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.OptionsTableLayoutItem.RowIndex = 6
        Me.LayoutControlItem13.Size = New System.Drawing.Size(172, 24)
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = False
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.teUnidadesPre
        Me.LayoutControlItem14.Location = New System.Drawing.Point(172, 144)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem14.OptionsTableLayoutItem.RowIndex = 6
        Me.LayoutControlItem14.Size = New System.Drawing.Size(401, 24)
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem14.TextVisible = False
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.LabelControl5
        Me.LayoutControlItem15.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 168)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.OptionsTableLayoutItem.RowIndex = 7
        Me.LayoutControlItem15.Size = New System.Drawing.Size(172, 24)
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem15.TextVisible = False
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.teProveedor
        Me.LayoutControlItem16.Location = New System.Drawing.Point(172, 168)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem16.OptionsTableLayoutItem.RowIndex = 7
        Me.LayoutControlItem16.Size = New System.Drawing.Size(401, 24)
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem16.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.TeCuentaContableIng
        Me.LayoutControlItem20.Location = New System.Drawing.Point(172, 192)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem20.OptionsTableLayoutItem.RowIndex = 8
        Me.LayoutControlItem20.Size = New System.Drawing.Size(401, 24)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = False
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.LabelControl11
        Me.LayoutControlItem19.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 192)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.OptionsTableLayoutItem.RowIndex = 8
        Me.LayoutControlItem19.Size = New System.Drawing.Size(172, 24)
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextVisible = False
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.LabelControl12
        Me.LayoutControlItem21.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 216)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.OptionsTableLayoutItem.RowIndex = 9
        Me.LayoutControlItem21.Size = New System.Drawing.Size(172, 24)
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = False
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.LabelControl13
        Me.LayoutControlItem23.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 240)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.OptionsTableLayoutItem.RowIndex = 10
        Me.LayoutControlItem23.Size = New System.Drawing.Size(172, 24)
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem23.TextVisible = False
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.TeCuentaContableInv
        Me.LayoutControlItem22.Location = New System.Drawing.Point(172, 216)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem22.OptionsTableLayoutItem.RowIndex = 9
        Me.LayoutControlItem22.Size = New System.Drawing.Size(401, 24)
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem22.TextVisible = False
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.TeCuentaContableCos
        Me.LayoutControlItem24.Location = New System.Drawing.Point(172, 240)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem24.OptionsTableLayoutItem.RowIndex = 10
        Me.LayoutControlItem24.Size = New System.Drawing.Size(401, 24)
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem24.TextVisible = False
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.teMarca
        Me.LayoutControlItem26.Location = New System.Drawing.Point(172, 264)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem26.OptionsTableLayoutItem.RowIndex = 11
        Me.LayoutControlItem26.Size = New System.Drawing.Size(401, 24)
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem26.TextVisible = False
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.LabelControl4
        Me.LayoutControlItem25.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem25.Location = New System.Drawing.Point(0, 264)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.OptionsTableLayoutItem.RowIndex = 11
        Me.LayoutControlItem25.Size = New System.Drawing.Size(172, 24)
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextVisible = False
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.meInfo
        Me.LayoutControlItem18.Location = New System.Drawing.Point(172, 288)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.OptionsTableLayoutItem.ColumnIndex = 1
        Me.LayoutControlItem18.OptionsTableLayoutItem.RowIndex = 12
        Me.LayoutControlItem18.OptionsTableLayoutItem.RowSpan = 4
        Me.LayoutControlItem18.Size = New System.Drawing.Size(401, 56)
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextVisible = False
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem17})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 288)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.OptionsTableLayoutItem.RowIndex = 12
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(172, 41)
        Me.LayoutControlGroup2.TextVisible = False
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.LabelControl10
        Me.LayoutControlItem17.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.OptionsTableLayoutItem.RowIndex = 12
        Me.LayoutControlItem17.OptionsTableLayoutItem.RowSpan = 4
        Me.LayoutControlItem17.Size = New System.Drawing.Size(148, 17)
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextVisible = False
        '
        'peFoto
        '
        Me.peFoto.Cursor = System.Windows.Forms.Cursors.Default
        Me.peFoto.Dock = System.Windows.Forms.DockStyle.Fill
        Me.peFoto.Location = New System.Drawing.Point(0, 0)
        Me.peFoto.Name = "peFoto"
        Me.peFoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch
        Me.peFoto.Properties.ZoomAccelerationFactor = 1.0R
        Me.peFoto.Size = New System.Drawing.Size(389, 216)
        ToolTipTitleItem1.Text = "Imagen del producto"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        Me.peFoto.SuperTip = SuperToolTip1
        Me.peFoto.TabIndex = 12
        Me.peFoto.ToolTipTitle = "Doble clic para cambiar o cargar imagen"
        '
        'gcProductos
        '
        Me.gcProductos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gcProductos.Location = New System.Drawing.Point(0, 60)
        Me.gcProductos.MainView = Me.gvProd
        Me.gcProductos.Name = "gcProductos"
        Me.gcProductos.Size = New System.Drawing.Size(610, 268)
        Me.gcProductos.TabIndex = 2
        Me.gcProductos.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvProd})
        '
        'gvProd
        '
        Me.gvProd.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.gvProd.GridControl = Me.gcProductos
        Me.gvProd.Name = "gvProd"
        Me.gvProd.OptionsBehavior.Editable = False
        Me.gvProd.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "C�d.Producto"
        Me.GridColumn1.FieldName = "IdProducto"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 190
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombre"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 401
        '
        'gcPr
        '
        Me.gcPr.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcPr.Location = New System.Drawing.Point(0, 0)
        Me.gcPr.MainView = Me.gvPr
        Me.gcPr.Name = "gcPr"
        Me.gcPr.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rileIdPrecio})
        Me.gcPr.Size = New System.Drawing.Size(389, 201)
        Me.gcPr.TabIndex = 11
        Me.gcPr.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvPr})
        '
        'gvPr
        '
        Me.gvPr.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdPrecio, Me.gcValor, Me.cgValorNeto})
        Me.gvPr.GridControl = Me.gcPr
        Me.gvPr.Name = "gvPr"
        Me.gvPr.OptionsBehavior.Editable = False
        Me.gvPr.OptionsView.ShowGroupPanel = False
        '
        'gcIdPrecio
        '
        Me.gcIdPrecio.Caption = "Precio"
        Me.gcIdPrecio.ColumnEdit = Me.rileIdPrecio
        Me.gcIdPrecio.FieldName = "IdPrecio"
        Me.gcIdPrecio.Name = "gcIdPrecio"
        Me.gcIdPrecio.OptionsColumn.AllowEdit = False
        Me.gcIdPrecio.Visible = True
        Me.gcIdPrecio.VisibleIndex = 0
        Me.gcIdPrecio.Width = 166
        '
        'rileIdPrecio
        '
        Me.rileIdPrecio.AutoHeight = False
        Me.rileIdPrecio.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rileIdPrecio.Name = "rileIdPrecio"
        '
        'gcValor
        '
        Me.gcValor.Caption = "Valor"
        Me.gcValor.DisplayFormat.FormatString = "###,##0.0000"
        Me.gcValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcValor.FieldName = "Precio"
        Me.gcValor.Name = "gcValor"
        Me.gcValor.Visible = True
        Me.gcValor.VisibleIndex = 1
        Me.gcValor.Width = 93
        '
        'cgValorNeto
        '
        Me.cgValorNeto.Caption = "ValorNeto"
        Me.cgValorNeto.DisplayFormat.FormatString = "###,##0.0000"
        Me.cgValorNeto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.cgValorNeto.FieldName = "PrecioNeto"
        Me.cgValorNeto.Name = "cgValorNeto"
        Me.cgValorNeto.Visible = True
        Me.cgValorNeto.VisibleIndex = 2
        Me.cgValorNeto.Width = 94
        '
        'gcEx
        '
        Me.gcEx.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcEx.Location = New System.Drawing.Point(0, 0)
        Me.gcEx.MainView = Me.gvEx
        Me.gcEx.Name = "gcEx"
        Me.gcEx.Size = New System.Drawing.Size(389, 165)
        Me.gcEx.TabIndex = 13
        Me.gcEx.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvEx})
        '
        'gvEx
        '
        Me.gvEx.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcBodega, Me.gcSaldo})
        Me.gvEx.GridControl = Me.gcEx
        Me.gvEx.Name = "gvEx"
        Me.gvEx.OptionsBehavior.Editable = False
        Me.gvEx.OptionsView.ShowFooter = True
        Me.gvEx.OptionsView.ShowGroupPanel = False
        '
        'gcBodega
        '
        Me.gcBodega.Caption = "Bodega"
        Me.gcBodega.FieldName = "Bodega"
        Me.gcBodega.Name = "gcBodega"
        Me.gcBodega.OptionsColumn.AllowEdit = False
        Me.gcBodega.Visible = True
        Me.gcBodega.VisibleIndex = 0
        Me.gcBodega.Width = 167
        '
        'gcSaldo
        '
        Me.gcSaldo.Caption = "Saldo"
        Me.gcSaldo.DisplayFormat.FormatString = "###,##0.00"
        Me.gcSaldo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcSaldo.FieldName = "Saldo"
        Me.gcSaldo.Name = "gcSaldo"
        Me.gcSaldo.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)})
        Me.gcSaldo.Visible = True
        Me.gcSaldo.VisibleIndex = 1
        Me.gcSaldo.Width = 112
        '
        'LabelControl24
        '
        Me.LabelControl24.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl24.Location = New System.Drawing.Point(362, 10)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl24.TabIndex = 49
        Me.LabelControl24.Text = "Tipo de Busqueda:"
        '
        'rgTipoBusqueda
        '
        Me.rgTipoBusqueda.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rgTipoBusqueda.Location = New System.Drawing.Point(453, 7)
        Me.rgTipoBusqueda.Name = "rgTipoBusqueda"
        Me.rgTipoBusqueda.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Contiene"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Inicia Con")})
        Me.rgTipoBusqueda.Size = New System.Drawing.Size(153, 21)
        Me.rgTipoBusqueda.TabIndex = 48
        '
        'teCodigo
        '
        Me.teCodigo.Location = New System.Drawing.Point(12, 34)
        Me.teCodigo.Name = "teCodigo"
        Me.teCodigo.Size = New System.Drawing.Size(193, 20)
        Me.teCodigo.TabIndex = 1
        '
        'teNombre
        '
        Me.teNombre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.teNombre.Location = New System.Drawing.Point(207, 34)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(400, 20)
        Me.teNombre.TabIndex = 0
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SplitContainerControl2)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.SplitContainerControl3)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1004, 592)
        Me.SplitContainerControl1.SplitterPosition = 610
        Me.SplitContainerControl1.TabIndex = 56
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'SplitContainerControl2
        '
        Me.SplitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl2.Horizontal = False
        Me.SplitContainerControl2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl2.Name = "SplitContainerControl2"
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.LabelControl24)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.gcProductos)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.teNombre)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.teCodigo)
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.rgTipoBusqueda)
        Me.SplitContainerControl2.Panel1.Text = "Panel1"
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.LayoutControl1)
        Me.SplitContainerControl2.Panel2.Text = "Panel2"
        Me.SplitContainerControl2.Size = New System.Drawing.Size(610, 592)
        Me.SplitContainerControl2.SplitterPosition = 328
        Me.SplitContainerControl2.TabIndex = 0
        Me.SplitContainerControl2.Text = "SplitContainerControl2"
        '
        'SplitContainerControl3
        '
        Me.SplitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl3.Horizontal = False
        Me.SplitContainerControl3.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl3.Name = "SplitContainerControl3"
        Me.SplitContainerControl3.Panel1.Controls.Add(Me.peFoto)
        Me.SplitContainerControl3.Panel1.Text = "Panel1"
        Me.SplitContainerControl3.Panel2.Controls.Add(Me.SplitContainerControl4)
        Me.SplitContainerControl3.Panel2.Text = "Panel2"
        Me.SplitContainerControl3.Size = New System.Drawing.Size(389, 592)
        Me.SplitContainerControl3.SplitterPosition = 216
        Me.SplitContainerControl3.TabIndex = 14
        Me.SplitContainerControl3.Text = "SplitContainerControl3"
        '
        'SplitContainerControl4
        '
        Me.SplitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl4.Horizontal = False
        Me.SplitContainerControl4.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl4.Name = "SplitContainerControl4"
        Me.SplitContainerControl4.Panel1.Controls.Add(Me.gcPr)
        Me.SplitContainerControl4.Panel1.Text = "Panel1"
        Me.SplitContainerControl4.Panel2.Controls.Add(Me.gcEx)
        Me.SplitContainerControl4.Panel2.Text = "Panel2"
        Me.SplitContainerControl4.Size = New System.Drawing.Size(389, 371)
        Me.SplitContainerControl4.SplitterPosition = 201
        Me.SplitContainerControl4.TabIndex = 0
        Me.SplitContainerControl4.Text = "SplitContainerControl4"
        '
        'inv_frmConsultaProductos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1004, 592)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "inv_frmConsultaProductos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Consulta de productos"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.teMarca.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeCuentaContableCos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeCuentaContableInv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeCuentaContableIng.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSubGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meInfo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teUnidadesPre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teEstilo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTalla.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teUnidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teColor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peFoto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcPr, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPr, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rileIdPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcEx, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvEx, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipoBusqueda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.SplitContainerControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl2.ResumeLayout(False)
        CType(Me.SplitContainerControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl3.ResumeLayout(False)
        CType(Me.SplitContainerControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents peFoto As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teSubGrupo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teUnidad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teMarca As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teUnidadesPre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTalla As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teColor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teEstilo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teProveedor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meInfo As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents gcProductos As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvProd As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPr As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvPr As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rileIdPrecio As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents gcValor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcEx As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvEx As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgTipoBusqueda As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents teCodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TeCuentaContableIng As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TeCuentaContableInv As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TeCuentaContableCos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cgValorNeto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents teGrupo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents SplitContainerControl2 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents SplitContainerControl3 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents SplitContainerControl4 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
End Class
