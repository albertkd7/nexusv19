﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class inv_frmPrecios
    Dim entidad As inv_Precios

    Private Sub ban_frmTiposTransaccion_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub ban_frmTiposTransaccion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.inv_PreciosSelectAll
        entidad = objTablas.inv_PreciosSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdPrecio"))

        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub cban_frmTiposTransaccion_Nuevo_Click() Handles Me.Nuevo
        entidad = New inv_Precios
        entidad.IdPrecio = objFunciones.ObtenerUltimoId("INV_PRECIOS", "IdPrecio") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub ban_frmTiposTransaccion_Save_Click() Handles Me.Guardar
        If teNombre.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [ Nombre]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.inv_PreciosInsert(entidad)
        Else
            objTablas.inv_PreciosUpdate(entidad)
        End If
        gc.DataSource = objTablas.inv_PreciosSelectAll
        entidad = objTablas.inv_PreciosSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdPrecio"))
        CargaPantalla()
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub ban_frmTiposTransaccion_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.inv_PreciosDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.inv_PreciosSelectAll
                entidad = objTablas.inv_PreciosSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdPrecio"))
                CargaPantalla()
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL REGISTRO:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub ban_frmTiposTransaccion_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entidad
            teId.EditValue = .IdPrecio
            teNombre.EditValue = .Nombre
            ceRequiereAutorizacion.EditValue = .RequiereAutorizacion

        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entidad
            .IdPrecio = teId.EditValue
            .Nombre = teNombre.EditValue
            .RequiereAutorizacion = ceRequiereAutorizacion.EditValue
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entidad = objTablas.inv_PreciosSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdPrecio"))
        CargaPantalla()
    End Sub

    Private Sub cban_frmTiposTransaccion_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teId.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub
  
End Class
