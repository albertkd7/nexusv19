﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns
Imports System.IO
Public Class inv_frmSalidas
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim entProducto As inv_Productos, Header As New inv_Salidas
    Dim Detalle As List(Of inv_SalidasDetalle)
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim IdSucUser As Integer = SiEsNulo(EntUsuario.IdSucursal, 1)
    Dim dtParametros As DataTable = blAdmon.ObtieneParametros()
    Dim AutorizoReversion As String = ""



    Private Sub inv_frmSalidas_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = bl.inv_ConsultaSalidas(objMenu.User).Tables(0)
        gv2.BestFitColumns()
    End Sub

    Private Sub inv_frmSalidas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.inv_TiposSalida(leTipoTransaccion, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        objCombos.con_CentrosCosto(leCentroCosto, "", "")
        objCombos.inv_Bodegas(leBodega, "")
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("INV_SALIDAS", "IdComprobante")
        CargaControles(0)
        ActivarControles(False)

        gc2.DataSource = bl.inv_ConsultaSalidas(objMenu.User).Tables(0)
    End Sub
    Private Sub inv_frmSalidas_Nuevo() Handles Me.Nuevo
        Header = New inv_Salidas
        ActivarControles(True)

        gc.DataSource = bl.inv_ObtenerDetalleDocumento(-1, "inv_SalidasDetalle")
        gv.CancelUpdateCurrentRow()
        'gv.AddNewRow()

        teNumero.EditValue = ""
        deFecha.EditValue = Today
        meConcepto.EditValue = ""
        leBodega.EditValue = EntUsuario.IdBodega
        leSucursal.EditValue = piIdSucursalUsuario
        leTipoTransaccion.EditValue = 13

        AutorizoReversion = ""
        teNumero.Focus()
        xtcSalidas.SelectedTabPage = xtpDatos
    End Sub
    Private Sub inv_frmSalidas_Guardar() Handles Me.Guardar
        If Not DatosValidos() Then
            Exit Sub
        End If
        CargarEntidades()
        Dim msj As String = ""

        If DbMode = DbModeType.insert Then
            msj = bl.inv_InsertarSalida(Header, Detalle, AutorizoReversion)
            If msj = "" Then
                MsgBox("El comprobante ha sido registrado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("SE DETECTÓ UN ERROR AL CREAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If

            If teNumero.EditValue = "" Then
                teNumero.EditValue = Header.Numero
            End If

        Else
            msj = bl.inv_ActualizarSalida(Header, Detalle, AutorizoReversion)
            If msj = "" Then
                MsgBox("El documento ha sido actualizado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("SE DETECTÓ UN ERROR AL ACTUALIZAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If

        teCorrelativo.EditValue = Header.IdComprobante


        MostrarModoInicial()
        teCorrelativo.Focus()
        ActivarControles(False)
    End Sub
    Private Sub inv_frmSalidas_Editar() Handles Me.Editar
        If Header.AplicadaInventario Then
            MsgBox("No puede editar ésta transacción." + Chr(13) + "Debe revertirla antes de modificar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        ActivarControles(True)
        teNumero.Focus()
    End Sub
    Private Sub inv_frmSalidas_Consulta() Handles Me.Consulta
        'teCorrelativo.EditValue = objConsultas.ConsultaSalidasInv(frmConsultaDetalle)
        'CargaControles(0)
    End Sub

    Private Sub inv_frmSalidas_Revertir() Handles Me.Revertir
        xtcSalidas.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub
    Private Sub inv_frmSalidas_Eliminar() Handles Me.Eliminar
        If xtcSalidas.SelectedTabPage.Name = "xtpLista" Then
            Header = objTablas.inv_SalidasSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Else
            Header = objTablas.inv_SalidasSelectByPK(teCorrelativo.EditValue)
        End If

        If Header.AplicadaInventario Then
            MsgBox("Es necesario que revierta la transacción. No se puede eliminar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de eliminar el documento?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Header.ModificadoPor = objMenu.User
            objTablas.inv_SalidasUpdate(Header)

            objTablas.inv_SalidasDeleteByPK(Header.IdComprobante)
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("INV_SALIDAS", "IdComprobante")
            CargaControles(0)
            ActivarControles(False)
            gc2.DataSource = bl.inv_ConsultaSalidas(objMenu.User).Tables(0)
        End If
    End Sub

#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", 0.0)
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), "") = "" Then
            e.Valid = False
        End If
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Dim cantidad As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
        Dim PrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")
        Dim dtSaldo As DataTable = bl.inv_ObtieneExistenciaCostoProducto(SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), ""), leBodega.EditValue)
        Select Case gv.FocusedColumn.FieldName
            Case "Cantidad"
                cantidad = e.Value
                ''gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", SiEsNulo(dtSaldo.Rows(0).Item("PrecioCosto"), 0.0))
                If e.Value > dtSaldo.Rows(0).Item("SaldoExistencias") Then
                    MsgBox("Esta cantidad sobregira el inventario a la fecha actual. No podrá guardar la Salida", MsgBoxStyle.Critical, "Error")
                End If
            Case "PrecioUnitario"
                PrecioUnitario = e.Value
        End Select

        Dim total As Decimal = Decimal.Round(cantidad * PrecioUnitario, 2)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", total)
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gv.EditingValue = IdProd
                End If
                Dim dtSaldo As DataTable = bl.inv_ObtieneExistenciaCostoProducto(gv.EditingValue, leBodega.EditValue)
                If dtSaldo.Rows.Count > 0 Then
                    entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                    If dtParametros.Rows(0).Item("AlertaMinimos") Then
                        If dtSaldo.Rows(0).Item("SaldoExistencias") < entProducto.ExistenciaMinima Then
                            MsgBox("El producto " + entProducto.Nombre + " se encuentra con existencia menor al mínimo", MsgBoxStyle.Critical, "Error")
                        End If
                    End If

                    gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", SiEsNulo(dtSaldo.Rows(0).Item("PrecioCosto"), 0.0))
                    Dim cantidad As Integer = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
                    Dim PrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")
                    Dim total As Decimal = Decimal.Round(cantidad * PrecioUnitario, 2)
                    gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", total)
                End If

                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                    gv.SetFocusedRowCellValue("IdCentro", entProducto.IdCentro)
                End If
            End If
        End If

    End Sub
#End Region

    Public Sub CargaControles(ByVal TipoAvance As Integer)

    End Sub

    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
        'btAplicar.Enabled = (Not Header.AplicadaInventario Or Not DbMode = DbModeType.insert)
        'btRevertir.Enabled = Header.AplicadaInventario
        btAplicar.Enabled = Not Header.AplicadaInventario
        btRevertir.Enabled = Header.AplicadaInventario
        btRevertir.Enabled = Header.AplicadaInventario
    End Sub
    Private Sub CargarEntidades()
        With Header
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
            Else
                .ModificadoPor = objMenu.User
                .FechaHoraModificacion = Now
            End If
            .IdComprobante = teCorrelativo.EditValue  'el id se asignará en la capa de datos
            .IdTipoComprobante = leTipoTransaccion.EditValue
            .Numero = teNumero.EditValue
            .Fecha = deFecha.EditValue
            .Concepto = meConcepto.EditValue
            .IdBodega = leBodega.EditValue
            .IdSucursal = leSucursal.EditValue

        End With

        Detalle = New List(Of inv_SalidasDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New inv_SalidasDetalle
            With entDetalle
                .IdComprobante = Header.IdComprobante
                .IdDetalle = i + 1
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .PrecioTotal = gv.GetRowCellValue(i, "PrecioTotal")
                .IdCentro = gv.GetRowCellValue(i, "IdCentro")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            Detalle.Add(entDetalle)
        Next

    End Sub
    Private Sub sbImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbImportar.Click
        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            Return
        End If
        Dim csv_file As System.IO.StreamReader = File.OpenText(ofd.FileName)
        Try
            While csv_file.Peek() > 0
                Dim sLine As String = csv_file.ReadLine()
                Dim aData As Array = sLine.Split(",")
                entProducto = objTablas.inv_ProductosSelectByPK(aData(0))
                gv.AddNewRow()
                gv.SetRowCellValue(gv.FocusedRowHandle, "IdProducto", aData(0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", aData(1))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", aData(2))
                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", SiEsNulo(aData(3), 0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", SiEsNulo(aData(4), 0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", SiEsNulo(entProducto.IdCentro, ""))
                gv.UpdateCurrentRow()

            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Return
        End Try

        csv_file.Close()
    End Sub

    Private Sub inv_frmSalidas_Reporte() Handles Me.Reporte
        If xtcSalidas.SelectedTabPage.Name = "xtpLista" Then
            Header = objTablas.inv_SalidasSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Else
            Header = objTablas.inv_SalidasSelectByPK(teCorrelativo.EditValue)
        End If

        Dim dt As DataTable = bl.inv_ObtenerDetalleDocumento(Header.IdComprobante, "inv_SalidasDetalle")

        Dim rpt As New inv_rptEntradaSalida() With {.DataSource = dt, .DataMember = ""}


        rpt.DataMember = ""
        Dim entBod As inv_Bodegas = objTablas.inv_BodegasSelectByPK(Header.IdBodega)
        Dim entTipos As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(Header.IdTipoComprobante)


        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "HOJA DE SALIDA DE MERCADERÍA DEL INVENTARIO"
        rpt.xrlNumero.Text = Header.Numero
        rpt.xrlTipo.Text = entTipos.Nombre
        rpt.xrlConcepto.Text = Header.Concepto
        rpt.xrlFecha.Text = Header.Fecha
        rpt.xrlBodega.Text = entBod.Nombre


        rpt.ShowPreviewDialog()


    End Sub

    Private Sub btSaveAs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSaveAs.Click
        Dim Delimitador As String = InputBox("Deliminator del archivo:", "Especifique el delimitador", "|")
        If Delimitador = "" Then
            Return
        End If

        Dim fbd As New FolderBrowserDialog

        fbd.ShowDialog()
        If fbd.SelectedPath = "" Then
            Return
        End If

        Dim Archivo As String = String.Format("{0}\SALIDA{1}{2}.txt", fbd.SelectedPath, teCorrelativo.EditValue, Format(deFecha.EditValue, "yyyyMMdd"))

        Try
            Using Arc As StreamWriter = New StreamWriter(Archivo)
                Dim linea As String = String.Empty

                With gv
                    For fila As Integer = 0 To .RowCount - 1
                        linea = String.Empty

                        For col As Integer = 0 To .Columns.Count - 1
                            linea &= gv.GetRowCellValue(fila, gv.Columns(col)) & Delimitador
                        Next

                        With Arc
                            linea = linea.Remove(linea.Length - 1).ToString
                            .WriteLine(linea.ToString)
                        End With
                    Next
                End With
            End Using

            Process.Start(Archivo)

        Catch ex As Exception
            MsgBox("NO SE PUDO GUARDAR EL ARCHIVO" + Chr(13) + ex.Message.ToString, MsgBoxStyle.Critical, "Nota")
        End Try
    End Sub
    Private Function DatosValidos()
        Dim msj As String = "", msj1 As String = "", lDummy As Boolean = False
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "IdProducto") = gv.GetRowCellValue(i + 1, "IdProducto") Then
                msj = "El producto " & gv.GetRowCellValue(i, "IdProducto") & ",  está repetido"
                Exit For
            End If
            If bl.inv_ObtieneExistenciaProducto(gv.GetRowCellValue(i, "IdProducto"), leBodega.EditValue, deFecha.EditValue).Rows(0).Item("SaldoExistencias") - gv.GetRowCellValue(i, "Cantidad") < 0 Then
                If i = 0 Then
                    msj1 = "Productos sin existencias en Fecha " + Format(deFecha.EditValue, "dd/MM/yyyy") + Chr(13)
                End If
                msj1 &= gv.GetRowCellValue(i, "IdProducto") + ","

            End If
        Next
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.None
        For i = 0 To gv.DataRowCount - 1
            If bl.inv_ObtieneExistenciaProducto(gv.GetRowCellValue(i, "IdProducto"), leBodega.EditValue, Today).Rows(0).Item("SaldoExistencias") - gv.GetRowCellValue(i, "Cantidad") < 0 Then
                If Not lDummy Then
                    msj1 &= "Productos sin existencias en Fecha " + Format(Today, "dd/MM/yyyy") + Chr(13)
                End If
                msj1 &= gv.GetRowCellValue(i, "IdProducto") + ","
                lDummy = True
            End If
        Next

        If SiEsNulo(leSucursal.Text, "") = "" Or SiEsNulo(leBodega.EditValue, 0) = 0 Or SiEsNulo(leTipoTransaccion.Text, "") = "" Or SiEsNulo(leTipoTransaccion.EditValue, 0) = 0 Then
            msj = "Los datos de Bodega, Sucursal ó Tipo de Transacción no pueden quedar en blanco"
        End If

        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If

        If msj1 <> "" Then
            MsgBox(msj1, MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            msj = "No puede crear el documento" & Chr(13) & "La fecha corresponde a un período ya cerrado"
            Return False
        End If
        Return True
    End Function

    Private Sub btAplicar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btAplicar.Click
        If meConcepto.Properties.ReadOnly = False Then
            MsgBox("Debes Guardar antes la Salida", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        Header = objTablas.inv_SalidasSelectByPK(teCorrelativo.EditValue)
        If Header.AplicadaInventario Then
            MsgBox("Esta transacción ya ha sido aplicada anteriormente", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de querer aplicar la transacción?" + Chr(13) + "YA NO PODRÁ EDITAR LOS DATOS", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        Dim msj As String = ""
        If dtParametros.Rows(0).Item("ValidarExistencias") = True Then
            For i = 0 To gv.DataRowCount - 1
                If bl.inv_ObtieneExistenciaProducto(gv.GetRowCellValue(i, "IdProducto"), Header.IdBodega, Header.Fecha).Rows(0).Item("SaldoExistencias") <= 0 Then
                    msj = "El producto --> " & gv.GetRowCellValue(i, "IdProducto") & ", no tiene existencia a la fecha " + Format(deFecha.EditValue, "dd/MM/yyyy")
                    Exit For
                End If
                If Header.Fecha < Today Then
                    If bl.inv_ObtieneExistenciaProducto(gv.GetRowCellValue(i, "IdProducto"), Header.IdBodega, Today).Rows(0).Item("SaldoExistencias") <= 0 Then
                        msj = "El producto --> " & gv.GetRowCellValue(i, "IdProducto") & ", no tiene existencia a la fecha " + Format(Today, "dd/MM/yyyy")
                        Exit For
                    End If
                End If
            Next
        End If
        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        msj = bl.inv_AplicarTransaccionInventario(Header.IdComprobante, "Salida", "I", Header.IdTipoComprobante, 0)
        If msj = "Ok" Then
            MsgBox("La transacción ha sido aplicada", MsgBoxStyle.Information, "Nota")
            Header.AplicadaInventario = True
            btAplicar.Enabled = False
            btRevertir.Enabled = True
        Else
            MsgBox("Sucedió algún error al aplicar" + Chr(13) + msj + Chr(13) + "Favor reporte éste mensaje a ITO, S.A. DE C.V.", MsgBoxStyle.Critical, "Error")
        End If
        xtcSalidas.SelectedTabPage = xtpLista
        gc2.DataSource = bl.inv_ConsultaSalidas(objMenu.User).Tables(0)
        gc2.Focus()
    End Sub

    Private Sub btRevertir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRevertir.Click
        If meConcepto.Properties.ReadOnly = False Then
            MsgBox("Debe Guardar antes la transacción", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        If MsgBox("Está seguro(a) de querer revertir ésta transacción?" + Chr(13) + "ESTO PUEDE DESESTABILIZAR SUS INVENTARIOS", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        If Not Header.AplicadaInventario Then
            MsgBox("La transacción NO ha sido aplicada aún", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        'Dim msj1 As String = ""
        'Dim lDummy As Boolean = False
        'For i = 0 To gv.DataRowCount - 1
        '    Dim dt As New DataTable
        '    dt = bl.inv_ObtieneExistenciaProducto(gv.GetRowCellValue(i, "IdProducto"), Header.IdBodega, Header.Fecha)
        '    If dt.Rows.Count > 0 Then
        '        If SiEsNulo(dt.Rows(i).Item("SaldoExistencias"), 0) - gv.GetRowCellValue(i, "Cantidad") < 0 Then
        '            If i = 0 Then
        '                msj1 = "Productos que resultaran con Saldo Negativos en Fecha " + Header.Fecha.ToString + Chr(13)
        '            End If
        '            msj1 &= gv.GetRowCellValue(i, "IdProducto") + Chr(13)
        '            Exit For
        '        End If
        '    End If

        '    dt = bl.inv_ObtieneExistenciaProducto(gv.GetRowCellValue(i, "IdProducto"), Header.IdBodega, Today)
        '    If dt.Rows.Count > 0 Then
        '        If SiEsNulo(dt.Rows(i).Item("SaldoExistencias"), 0) - gv.GetRowCellValue(i, "Cantidad") < 0 Then
        '            If i = 0 Or Not lDummy Then
        '                msj1 &= "Productos que resultaran con Saldo Negativos en Fecha " + Format(Today, "dd/MM/yyyy") + Chr(13)
        '            End If
        '            msj1 &= gv.GetRowCellValue(i, "IdProducto") + Chr(13)
        '            lDummy = True
        '            Exit For
        '        End If
        '    End If
        'Next

        If MsgBox("Debe autorizar para continuar?", 292, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        frmObtienePassword.Text = "AUTORIZACIÓN PARA REVERTIR INVENTARIOS"
        frmObtienePassword.Usuario = objMenu.User
        frmObtienePassword.TipoAcceso = 4
        frmObtienePassword.ShowDialog()

        If frmObtienePassword.Acceso = False Then
            MsgBox("Usuario no Autorizado para Revertir Inventarios", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        Dim msj As String = bl.inv_AplicarTransaccionInventario(Header.IdComprobante, "Salida", "D", Header.IdTipoComprobante, 0)
        If msj = "Ok" Then
            MsgBox("La transacción ha sido revertida con éxito. Ya puede editarse", MsgBoxStyle.Information, "Nota")
            Header.AplicadaInventario = False
            btAplicar.Enabled = True
            btRevertir.Enabled = False
        Else
            MsgBox("Sucedió algún error al intentar revertir" + Chr(13) + msj + Chr(13) + "Favor reporte éste mensaje a ITO, S.A. DE C.V.", MsgBoxStyle.Critical, "Error")
        End If
    End Sub

    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        Header = objTablas.inv_SalidasSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        'DbMode = DbModeType.update
        teNumero.Focus()
    End Sub
    Private Sub CargaPantalla()
        If DbMode <> DbModeType.cargar Then
            Exit Sub
        End If
        xtcSalidas.SelectedTabPage = xtpDatos
        teNumero.Focus()

        With Header
            teCorrelativo.EditValue = .IdComprobante
            teNumero.EditValue = .Numero
            leTipoTransaccion.EditValue = CInt(.IdTipoComprobante)
            deFecha.EditValue = .Fecha
            meConcepto.EditValue = .Concepto
            leBodega.EditValue = .IdBodega
            leSucursal.EditValue = .IdSucursal
            gc.DataSource = bl.inv_ObtenerDetalleDocumento(.IdComprobante, "inv_salidasDetalle")
        End With
        btAplicar.Enabled = Not Header.AplicadaInventario
        btRevertir.Enabled = Header.AplicadaInventario

    End Sub

    Private Sub gv_DoubleClick(sender As Object, e As EventArgs) Handles gv.DoubleClick
        If meConcepto.Properties.ReadOnly Then
            Exit Sub
        End If

        Dim IdCentro As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdCentro")), "")
        Dim frmSeleccionaCC As New frmDetallarCentroCosto
        Me.AddOwnedForm(frmSeleccionaCC)
        frmSeleccionaCC.IdCentro = IdCentro
        frmSeleccionaCC.ShowDialog()

        IdCentro = SiEsNulo(frmSeleccionaCC.IdCentro, "")
        gv.SetFocusedRowCellValue("IdCentro", IdCentro)
        frmSeleccionaCC.Dispose()
    End Sub

End Class
