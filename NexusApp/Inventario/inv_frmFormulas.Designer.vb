﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmFormulas
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl
        Me.teCodigo = New DevExpress.XtraEditors.TextEdit
        Me.btSaveAs = New DevExpress.XtraEditors.SimpleButton
        Me.sbImportar = New DevExpress.XtraEditors.SimpleButton
        Me.meConcepto = New DevExpress.XtraEditors.MemoEdit
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.teNombre = New DevExpress.XtraEditors.TextEdit
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.deFecha = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcIdProducto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.riteCodigo = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.gcCantidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.riteCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn
        Me.ritePrecio = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.gcPrecioTotal = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.teCodigo)
        Me.pcHeader.Controls.Add(Me.btSaveAs)
        Me.pcHeader.Controls.Add(Me.sbImportar)
        Me.pcHeader.Controls.Add(Me.meConcepto)
        Me.pcHeader.Controls.Add(Me.LabelControl6)
        Me.pcHeader.Controls.Add(Me.teNombre)
        Me.pcHeader.Controls.Add(Me.teCorrelativo)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.deFecha)
        Me.pcHeader.Controls.Add(Me.LabelControl13)
        Me.pcHeader.Controls.Add(Me.LabelControl17)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(834, 111)
        Me.pcHeader.TabIndex = 4
        '
        'teCodigo
        '
        Me.teCodigo.Location = New System.Drawing.Point(160, 25)
        Me.teCodigo.Name = "teCodigo"
        Me.teCodigo.Size = New System.Drawing.Size(112, 20)
        Me.teCodigo.TabIndex = 76
        '
        'btSaveAs
        '
        Me.btSaveAs.AllowFocus = False
        Me.btSaveAs.Location = New System.Drawing.Point(675, 80)
        Me.btSaveAs.Name = "btSaveAs"
        Me.btSaveAs.Size = New System.Drawing.Size(93, 23)
        Me.btSaveAs.TabIndex = 75
        Me.btSaveAs.Text = "Guardar como..."
        '
        'sbImportar
        '
        Me.sbImportar.AllowFocus = False
        Me.sbImportar.Location = New System.Drawing.Point(675, 55)
        Me.sbImportar.Name = "sbImportar"
        Me.sbImportar.Size = New System.Drawing.Size(93, 23)
        Me.sbImportar.TabIndex = 75
        Me.sbImportar.Text = "Importar de CSV"
        '
        'meConcepto
        '
        Me.meConcepto.EnterMoveNextControl = True
        Me.meConcepto.Location = New System.Drawing.Point(160, 47)
        Me.meConcepto.Name = "meConcepto"
        Me.meConcepto.Size = New System.Drawing.Size(509, 61)
        Me.meConcepto.TabIndex = 4
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(33, 7)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(124, 13)
        Me.LabelControl6.TabIndex = 66
        Me.LabelControl6.Text = "Correlativo de la Formula:"
        '
        'teNombre
        '
        Me.teNombre.Enabled = False
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(278, 25)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.ReadOnly = True
        Me.teNombre.Size = New System.Drawing.Size(391, 20)
        Me.teNombre.TabIndex = 0
        '
        'teCorrelativo
        '
        Me.teCorrelativo.Enabled = False
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(160, 4)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Properties.ReadOnly = True
        Me.teCorrelativo.Size = New System.Drawing.Size(112, 20)
        Me.teCorrelativo.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(475, 7)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(93, 13)
        Me.LabelControl2.TabIndex = 67
        Me.LabelControl2.Text = "Fecha de Creación:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(570, 4)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFecha.Size = New System.Drawing.Size(99, 20)
        Me.deFecha.TabIndex = 3
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(26, 53)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(131, 13)
        Me.LabelControl13.TabIndex = 69
        Me.LabelControl13.Text = "Concepto / Observaciones:"
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(13, 28)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(144, 13)
        Me.LabelControl17.TabIndex = 70
        Me.LabelControl17.Text = "Código de Producto Asociado:"
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdUp)
        Me.pcBotones.Controls.Add(Me.cmdDown)
        Me.pcBotones.Controls.Add(Me.cmdBorrar)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(793, 111)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(41, 167)
        Me.pcBotones.TabIndex = 7
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(7, 8)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(27, 24)
        Me.cmdUp.TabIndex = 0
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(7, 43)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(27, 24)
        Me.cmdDown.TabIndex = 1
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(7, 78)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 24)
        Me.cmdBorrar.TabIndex = 2
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 111)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteCantidad, Me.ritePrecio, Me.riteCodigo})
        Me.gc.Size = New System.Drawing.Size(793, 167)
        Me.gc.TabIndex = 8
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdProducto, Me.gcCantidad, Me.gcDescripcion, Me.gcPrecioUnitario, Me.gcPrecioTotal})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdProducto
        '
        Me.gcIdProducto.Caption = "Cód. Producto"
        Me.gcIdProducto.ColumnEdit = Me.riteCodigo
        Me.gcIdProducto.FieldName = "IdProducto"
        Me.gcIdProducto.Name = "gcIdProducto"
        Me.gcIdProducto.Visible = True
        Me.gcIdProducto.VisibleIndex = 0
        Me.gcIdProducto.Width = 118
        '
        'riteCodigo
        '
        Me.riteCodigo.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCodigo.AutoHeight = False
        Me.riteCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.riteCodigo.Name = "riteCodigo"
        '
        'gcCantidad
        '
        Me.gcCantidad.Caption = "Cantidad"
        Me.gcCantidad.ColumnEdit = Me.riteCantidad
        Me.gcCantidad.DisplayFormat.FormatString = "n2"
        Me.gcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcCantidad.FieldName = "Cantidad"
        Me.gcCantidad.Name = "gcCantidad"
        Me.gcCantidad.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.gcCantidad.Visible = True
        Me.gcCantidad.VisibleIndex = 1
        Me.gcCantidad.Width = 88
        '
        'riteCantidad
        '
        Me.riteCantidad.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCantidad.Appearance.Options.UseTextOptions = True
        Me.riteCantidad.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.riteCantidad.AutoHeight = False
        Me.riteCantidad.EditFormat.FormatString = "n2"
        Me.riteCantidad.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteCantidad.Mask.EditMask = "n2"
        Me.riteCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.riteCantidad.Name = "riteCantidad"
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Concepto/ Descripción del artículo"
        Me.gcDescripcion.FieldName = "Nombre"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 2
        Me.gcDescripcion.Width = 412
        '
        'gcPrecioUnitario
        '
        Me.gcPrecioUnitario.Caption = "Precio de Costo"
        Me.gcPrecioUnitario.ColumnEdit = Me.ritePrecio
        Me.gcPrecioUnitario.FieldName = "PrecioCosto"
        Me.gcPrecioUnitario.Name = "gcPrecioUnitario"
        Me.gcPrecioUnitario.Visible = True
        Me.gcPrecioUnitario.VisibleIndex = 3
        Me.gcPrecioUnitario.Width = 123
        '
        'ritePrecio
        '
        Me.ritePrecio.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.ritePrecio.AutoHeight = False
        Me.ritePrecio.Mask.EditMask = "n4"
        Me.ritePrecio.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePrecio.Mask.UseMaskAsDisplayFormat = True
        Me.ritePrecio.Name = "ritePrecio"
        '
        'gcPrecioTotal
        '
        Me.gcPrecioTotal.Caption = "Precio Total"
        Me.gcPrecioTotal.FieldName = "PrecioTotal"
        Me.gcPrecioTotal.Name = "gcPrecioTotal"
        Me.gcPrecioTotal.OptionsColumn.AllowEdit = False
        Me.gcPrecioTotal.OptionsColumn.AllowFocus = False
        Me.gcPrecioTotal.Visible = True
        Me.gcPrecioTotal.VisibleIndex = 4
        Me.gcPrecioTotal.Width = 135
        '
        'inv_frmFormulas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(834, 303)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.pcBotones)
        Me.Controls.Add(Me.pcHeader)
        Me.Modulo = "Inventario"
        Me.Name = "inv_frmFormulas"
        Me.OptionId = "002005"
        Me.Text = "Creación de formulas para producción"
        Me.Controls.SetChildIndex(Me.pcHeader, 0)
        Me.Controls.SetChildIndex(Me.pcBotones, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btSaveAs As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbImportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents meConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCodigo As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ritePrecio As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcPrecioTotal As DevExpress.XtraGrid.Columns.GridColumn
End Class
