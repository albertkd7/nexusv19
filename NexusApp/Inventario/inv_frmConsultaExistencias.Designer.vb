<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmConsultaExistencias
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(inv_frmConsultaExistencias))
        Me.gcEx = New DevExpress.XtraGrid.GridControl
        Me.gvEx = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcProducto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcSaldo = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.gcEx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvEx, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcEx
        '
        Me.gcEx.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcEx.Location = New System.Drawing.Point(0, 0)
        Me.gcEx.MainView = Me.gvEx
        Me.gcEx.Name = "gcEx"
        Me.gcEx.Size = New System.Drawing.Size(590, 303)
        Me.gcEx.TabIndex = 0
        Me.gcEx.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvEx})
        '
        'gvEx
        '
        Me.gvEx.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcBodega, Me.gcProducto, Me.gcSaldo})
        Me.gvEx.GridControl = Me.gcEx
        Me.gvEx.Name = "gvEx"
        Me.gvEx.OptionsBehavior.Editable = False
        '
        'gcBodega
        '
        Me.gcBodega.Caption = "Bodega"
        Me.gcBodega.FieldName = "Bodega"
        Me.gcBodega.Name = "gcBodega"
        Me.gcBodega.Visible = True
        Me.gcBodega.VisibleIndex = 0
        Me.gcBodega.Width = 218
        '
        'gcProducto
        '
        Me.gcProducto.Caption = "Producto"
        Me.gcProducto.FieldName = "Producto"
        Me.gcProducto.Name = "gcProducto"
        Me.gcProducto.Visible = True
        Me.gcProducto.VisibleIndex = 1
        Me.gcProducto.Width = 220
        '
        'gcSaldo
        '
        Me.gcSaldo.Caption = "Saldo"
        Me.gcSaldo.FieldName = "Saldo"
        Me.gcSaldo.Name = "gcSaldo"
        Me.gcSaldo.Visible = True
        Me.gcSaldo.VisibleIndex = 2
        Me.gcSaldo.Width = 131
        '
        'inv_frmConsultaExistencias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(590, 303)
        Me.Controls.Add(Me.gcEx)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "inv_frmConsultaExistencias"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Consultar Existencias por bodega"
        CType(Me.gcEx, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvEx, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gcEx As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvEx As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcSaldo As DevExpress.XtraGrid.Columns.GridColumn
End Class
