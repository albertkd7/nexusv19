﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmDetallarGastosProduccionProductos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(inv_frmDetallarGastosProduccionProductos))
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colIdProducto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colDescripcion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.colGasto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.leGasto = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.colAplica = New DevExpress.XtraGrid.Columns.GridColumn
        Me.chkAplica = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.codIdImportacion = New DevExpress.XtraGrid.Columns.GridColumn
        Me.LayoutConverter1 = New DevExpress.XtraLayout.Converter.LayoutConverter(Me.components)
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leGasto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAplica, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkAplica, Me.leGasto})
        Me.gc.Size = New System.Drawing.Size(733, 372)
        Me.gc.TabIndex = 2
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIdProducto, Me.colDescripcion, Me.colGasto, Me.colAplica, Me.codIdImportacion})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'colIdProducto
        '
        Me.colIdProducto.Caption = "IdProducto"
        Me.colIdProducto.FieldName = "IdProducto"
        Me.colIdProducto.Name = "colIdProducto"
        Me.colIdProducto.OptionsColumn.AllowEdit = False
        Me.colIdProducto.OptionsColumn.AllowFocus = False
        Me.colIdProducto.Visible = True
        Me.colIdProducto.VisibleIndex = 0
        Me.colIdProducto.Width = 90
        '
        'colDescripcion
        '
        Me.colDescripcion.Caption = "Descripción del Producto"
        Me.colDescripcion.FieldName = "Descripcion"
        Me.colDescripcion.Name = "colDescripcion"
        Me.colDescripcion.OptionsColumn.AllowEdit = False
        Me.colDescripcion.OptionsColumn.AllowFocus = False
        Me.colDescripcion.Visible = True
        Me.colDescripcion.VisibleIndex = 1
        Me.colDescripcion.Width = 361
        '
        'colGasto
        '
        Me.colGasto.Caption = "Gasto Producción"
        Me.colGasto.ColumnEdit = Me.leGasto
        Me.colGasto.FieldName = "IdGasto"
        Me.colGasto.Name = "colGasto"
        Me.colGasto.OptionsColumn.AllowEdit = False
        Me.colGasto.OptionsColumn.AllowFocus = False
        Me.colGasto.Visible = True
        Me.colGasto.VisibleIndex = 2
        Me.colGasto.Width = 177
        '
        'leGasto
        '
        Me.leGasto.AutoHeight = False
        Me.leGasto.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leGasto.Name = "leGasto"
        '
        'colAplica
        '
        Me.colAplica.Caption = "Aplica Gasto ?"
        Me.colAplica.ColumnEdit = Me.chkAplica
        Me.colAplica.FieldName = "AplicaGasto"
        Me.colAplica.Name = "colAplica"
        Me.colAplica.Visible = True
        Me.colAplica.VisibleIndex = 3
        Me.colAplica.Width = 87
        '
        'chkAplica
        '
        Me.chkAplica.AutoHeight = False
        Me.chkAplica.Name = "chkAplica"
        '
        'codIdImportacion
        '
        Me.codIdImportacion.Caption = "IdImportacion"
        Me.codIdImportacion.FieldName = "IdImportacion"
        Me.codIdImportacion.Name = "codIdImportacion"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.sbGuardar)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl1.Location = New System.Drawing.Point(0, 372)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(733, 39)
        Me.PanelControl1.TabIndex = 23
        '
        'sbGuardar
        '
        Me.sbGuardar.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbGuardar.Appearance.Options.UseFont = True
        Me.sbGuardar.Location = New System.Drawing.Point(247, 3)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(153, 33)
        Me.sbGuardar.TabIndex = 23
        Me.sbGuardar.Text = "Aceptar"
        '
        'inv_frmDetallarGastosProduccionProductos
        '
        Me.ClientSize = New System.Drawing.Size(733, 411)
        Me.ControlBox = False
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.PanelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "inv_frmDetallarGastosProduccionProductos"
        Me.Text = "Detallar Gastos por Productos"
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leGasto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAplica, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGasto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutConverter1 As DevExpress.XtraLayout.Converter.LayoutConverter
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents codIdImportacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAplica As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkAplica As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents leGasto As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit

End Class
