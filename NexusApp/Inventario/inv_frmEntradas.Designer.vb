﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmEntradas
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.xtcEntradas = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage()
        Me.gc2 = New DevExpress.XtraGrid.GridControl()
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcCorrelativo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcSucursal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcConcepto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcAplicada = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCreadoPor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdProducto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCodigo = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcCantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leCentro = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ritePrecio = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcPrecioTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCantidad2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.pcFooter = New DevExpress.XtraEditors.PanelControl()
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.sbPedido = New DevExpress.XtraEditors.SimpleButton()
        Me.teNumRemision = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdRemision = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.btRevertir = New DevExpress.XtraEditors.SimpleButton()
        Me.btAplicar = New DevExpress.XtraEditors.SimpleButton()
        Me.btSaveAs = New DevExpress.XtraEditors.SimpleButton()
        Me.sbImportar = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.meConcepto = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.leBodega = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoTransaccion = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.xtcEntradas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcEntradas.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCentro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCantidad2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcFooter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.teNumRemision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdRemision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoTransaccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcEntradas
        '
        Me.xtcEntradas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcEntradas.Location = New System.Drawing.Point(0, 0)
        Me.xtcEntradas.Name = "xtcEntradas"
        Me.xtcEntradas.SelectedTabPage = Me.xtpLista
        Me.xtcEntradas.Size = New System.Drawing.Size(938, 335)
        Me.xtcEntradas.TabIndex = 4
        Me.xtcEntradas.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc2)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(932, 307)
        Me.xtpLista.Text = "Consulta de Entradas"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        GridLevelNode1.RelationName = "Level1"
        Me.gc2.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit2, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(932, 307)
        Me.gc2.TabIndex = 5
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcCorrelativo, Me.gcNumero, Me.gcFecha, Me.gcSucursal, Me.gcConcepto, Me.gcAplicada, Me.gcCreadoPor, Me.GridColumn15})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'gcCorrelativo
        '
        Me.gcCorrelativo.Caption = "Correlativo"
        Me.gcCorrelativo.FieldName = "IdComprobante"
        Me.gcCorrelativo.Name = "gcCorrelativo"
        Me.gcCorrelativo.Visible = True
        Me.gcCorrelativo.VisibleIndex = 0
        Me.gcCorrelativo.Width = 72
        '
        'gcNumero
        '
        Me.gcNumero.Caption = "Numero"
        Me.gcNumero.FieldName = "Numero"
        Me.gcNumero.Name = "gcNumero"
        Me.gcNumero.Visible = True
        Me.gcNumero.VisibleIndex = 1
        Me.gcNumero.Width = 59
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha"
        Me.gcFecha.FieldName = "Fecha"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 2
        Me.gcFecha.Width = 56
        '
        'gcSucursal
        '
        Me.gcSucursal.Caption = "Sucursal"
        Me.gcSucursal.ColumnEdit = Me.leSucursalDetalle
        Me.gcSucursal.FieldName = "IdSucursal"
        Me.gcSucursal.Name = "gcSucursal"
        Me.gcSucursal.Visible = True
        Me.gcSucursal.VisibleIndex = 3
        Me.gcSucursal.Width = 150
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'gcConcepto
        '
        Me.gcConcepto.Caption = "Concepto"
        Me.gcConcepto.FieldName = "Concepto"
        Me.gcConcepto.Name = "gcConcepto"
        Me.gcConcepto.Visible = True
        Me.gcConcepto.VisibleIndex = 4
        Me.gcConcepto.Width = 215
        '
        'gcAplicada
        '
        Me.gcAplicada.Caption = "Aplicada?"
        Me.gcAplicada.FieldName = "AplicadaInventario"
        Me.gcAplicada.Name = "gcAplicada"
        Me.gcAplicada.Visible = True
        Me.gcAplicada.VisibleIndex = 5
        Me.gcAplicada.Width = 66
        '
        'gcCreadoPor
        '
        Me.gcCreadoPor.Caption = "Creado Por"
        Me.gcCreadoPor.FieldName = "CreadoPor"
        Me.gcCreadoPor.Name = "gcCreadoPor"
        Me.gcCreadoPor.Visible = True
        Me.gcCreadoPor.VisibleIndex = 6
        Me.gcCreadoPor.Width = 93
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "Fecha Hora Creacion"
        Me.GridColumn15.ColumnEdit = Me.RepositoryItemDateEdit2
        Me.GridColumn15.FieldName = "FechaHoraCreacion"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 7
        Me.GridColumn15.Width = 203
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemDateEdit2.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.gc)
        Me.xtpDatos.Controls.Add(Me.pcFooter)
        Me.xtpDatos.Controls.Add(Me.pcBotones)
        Me.xtpDatos.Controls.Add(Me.pcHeader)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(932, 307)
        Me.xtpDatos.Text = "Entradas al Inventario"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 132)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteCantidad, Me.ritePrecio, Me.riteCodigo, Me.riteCantidad2, Me.leCentro})
        Me.gc.Size = New System.Drawing.Size(891, 147)
        Me.gc.TabIndex = 8
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdProducto, Me.gcCantidad, Me.gcDescripcion, Me.GridColumn1, Me.gcPrecioUnitario, Me.gcPrecioTotal})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdProducto
        '
        Me.gcIdProducto.Caption = "Id. Producto"
        Me.gcIdProducto.ColumnEdit = Me.riteCodigo
        Me.gcIdProducto.FieldName = "IdProducto"
        Me.gcIdProducto.Name = "gcIdProducto"
        Me.gcIdProducto.Visible = True
        Me.gcIdProducto.VisibleIndex = 0
        Me.gcIdProducto.Width = 118
        '
        'riteCodigo
        '
        Me.riteCodigo.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCodigo.AutoHeight = False
        Me.riteCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.riteCodigo.Name = "riteCodigo"
        '
        'gcCantidad
        '
        Me.gcCantidad.Caption = "Cantidad"
        Me.gcCantidad.ColumnEdit = Me.riteCantidad
        Me.gcCantidad.DisplayFormat.FormatString = "n2"
        Me.gcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcCantidad.FieldName = "Cantidad"
        Me.gcCantidad.Name = "gcCantidad"
        Me.gcCantidad.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.gcCantidad.Visible = True
        Me.gcCantidad.VisibleIndex = 1
        Me.gcCantidad.Width = 88
        '
        'riteCantidad
        '
        Me.riteCantidad.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCantidad.Appearance.Options.UseTextOptions = True
        Me.riteCantidad.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.riteCantidad.AutoHeight = False
        Me.riteCantidad.EditFormat.FormatString = "n2"
        Me.riteCantidad.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteCantidad.Mask.EditMask = "n2"
        Me.riteCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.riteCantidad.Name = "riteCantidad"
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Concepto/ Descripción del artículo"
        Me.gcDescripcion.FieldName = "Descripcion"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 2
        Me.gcDescripcion.Width = 412
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Centro Costo"
        Me.GridColumn1.ColumnEdit = Me.leCentro
        Me.GridColumn1.FieldName = "IdCentro"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.AllowFocus = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 3
        '
        'leCentro
        '
        Me.leCentro.AutoHeight = False
        Me.leCentro.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCentro.Name = "leCentro"
        '
        'gcPrecioUnitario
        '
        Me.gcPrecioUnitario.Caption = "Precio Unitario"
        Me.gcPrecioUnitario.ColumnEdit = Me.ritePrecio
        Me.gcPrecioUnitario.FieldName = "PrecioUnitario"
        Me.gcPrecioUnitario.Name = "gcPrecioUnitario"
        Me.gcPrecioUnitario.Visible = True
        Me.gcPrecioUnitario.VisibleIndex = 4
        Me.gcPrecioUnitario.Width = 123
        '
        'ritePrecio
        '
        Me.ritePrecio.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.ritePrecio.AutoHeight = False
        Me.ritePrecio.Mask.EditMask = "n4"
        Me.ritePrecio.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePrecio.Mask.UseMaskAsDisplayFormat = True
        Me.ritePrecio.Name = "ritePrecio"
        '
        'gcPrecioTotal
        '
        Me.gcPrecioTotal.Caption = "Precio Total"
        Me.gcPrecioTotal.DisplayFormat.FormatString = "n2"
        Me.gcPrecioTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioTotal.FieldName = "PrecioTotal"
        Me.gcPrecioTotal.Name = "gcPrecioTotal"
        Me.gcPrecioTotal.OptionsColumn.AllowEdit = False
        Me.gcPrecioTotal.OptionsColumn.AllowFocus = False
        Me.gcPrecioTotal.Visible = True
        Me.gcPrecioTotal.VisibleIndex = 5
        Me.gcPrecioTotal.Width = 135
        '
        'riteCantidad2
        '
        Me.riteCantidad2.AutoHeight = False
        Me.riteCantidad2.EditFormat.FormatString = "n2"
        Me.riteCantidad2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteCantidad2.Mask.EditMask = "n2"
        Me.riteCantidad2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteCantidad2.Mask.UseMaskAsDisplayFormat = True
        Me.riteCantidad2.Name = "riteCantidad2"
        '
        'pcFooter
        '
        Me.pcFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pcFooter.Location = New System.Drawing.Point(0, 279)
        Me.pcFooter.Name = "pcFooter"
        Me.pcFooter.Size = New System.Drawing.Size(891, 28)
        Me.pcFooter.TabIndex = 6
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdUp)
        Me.pcBotones.Controls.Add(Me.cmdDown)
        Me.pcBotones.Controls.Add(Me.cmdBorrar)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(891, 132)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(41, 175)
        Me.pcBotones.TabIndex = 9
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(7, 8)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(27, 24)
        Me.cmdUp.TabIndex = 0
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(7, 43)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(27, 24)
        Me.cmdDown.TabIndex = 1
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(7, 78)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 24)
        Me.cmdBorrar.TabIndex = 2
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.sbPedido)
        Me.pcHeader.Controls.Add(Me.teNumRemision)
        Me.pcHeader.Controls.Add(Me.LabelControl4)
        Me.pcHeader.Controls.Add(Me.teIdRemision)
        Me.pcHeader.Controls.Add(Me.LabelControl7)
        Me.pcHeader.Controls.Add(Me.btRevertir)
        Me.pcHeader.Controls.Add(Me.btAplicar)
        Me.pcHeader.Controls.Add(Me.btSaveAs)
        Me.pcHeader.Controls.Add(Me.sbImportar)
        Me.pcHeader.Controls.Add(Me.LabelControl14)
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.meConcepto)
        Me.pcHeader.Controls.Add(Me.LabelControl6)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Controls.Add(Me.teCorrelativo)
        Me.pcHeader.Controls.Add(Me.teNumero)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.deFecha)
        Me.pcHeader.Controls.Add(Me.LabelControl10)
        Me.pcHeader.Controls.Add(Me.leBodega)
        Me.pcHeader.Controls.Add(Me.LabelControl13)
        Me.pcHeader.Controls.Add(Me.LabelControl17)
        Me.pcHeader.Controls.Add(Me.leTipoTransaccion)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(932, 132)
        Me.pcHeader.TabIndex = 7
        '
        'sbPedido
        '
        Me.sbPedido.AllowFocus = False
        Me.sbPedido.Location = New System.Drawing.Point(791, 5)
        Me.sbPedido.Name = "sbPedido"
        Me.sbPedido.Size = New System.Drawing.Size(20, 21)
        Me.sbPedido.TabIndex = 101
        Me.sbPedido.Text = "..."
        '
        'teNumRemision
        '
        Me.teNumRemision.Enabled = False
        Me.teNumRemision.EnterMoveNextControl = True
        Me.teNumRemision.Location = New System.Drawing.Point(674, 26)
        Me.teNumRemision.Name = "teNumRemision"
        Me.teNumRemision.Properties.ReadOnly = True
        Me.teNumRemision.Size = New System.Drawing.Size(112, 20)
        Me.teNumRemision.TabIndex = 100
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(622, 29)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(49, 13)
        Me.LabelControl4.TabIndex = 99
        Me.LabelControl4.Text = "No. Rem.:"
        '
        'teIdRemision
        '
        Me.teIdRemision.Enabled = False
        Me.teIdRemision.EnterMoveNextControl = True
        Me.teIdRemision.Location = New System.Drawing.Point(674, 5)
        Me.teIdRemision.Name = "teIdRemision"
        Me.teIdRemision.Properties.ReadOnly = True
        Me.teIdRemision.Size = New System.Drawing.Size(112, 20)
        Me.teIdRemision.TabIndex = 98
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(629, 8)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(42, 13)
        Me.LabelControl7.TabIndex = 97
        Me.LabelControl7.Text = "Id Rem.:"
        '
        'btRevertir
        '
        Me.btRevertir.AllowFocus = False
        Me.btRevertir.Location = New System.Drawing.Point(837, 101)
        Me.btRevertir.Name = "btRevertir"
        Me.btRevertir.Size = New System.Drawing.Size(93, 24)
        Me.btRevertir.TabIndex = 79
        Me.btRevertir.Text = "&Revertir"
        '
        'btAplicar
        '
        Me.btAplicar.AllowFocus = False
        Me.btAplicar.Location = New System.Drawing.Point(837, 75)
        Me.btAplicar.Name = "btAplicar"
        Me.btAplicar.Size = New System.Drawing.Size(93, 24)
        Me.btAplicar.TabIndex = 78
        Me.btAplicar.Text = "&Aplicar"
        '
        'btSaveAs
        '
        Me.btSaveAs.AllowFocus = False
        Me.btSaveAs.Location = New System.Drawing.Point(837, 38)
        Me.btSaveAs.Name = "btSaveAs"
        Me.btSaveAs.Size = New System.Drawing.Size(93, 24)
        Me.btSaveAs.TabIndex = 75
        Me.btSaveAs.Text = "Guardar como..."
        '
        'sbImportar
        '
        Me.sbImportar.AllowFocus = False
        Me.sbImportar.Location = New System.Drawing.Point(837, 12)
        Me.sbImportar.Name = "sbImportar"
        Me.sbImportar.Size = New System.Drawing.Size(93, 24)
        Me.sbImportar.TabIndex = 75
        Me.sbImportar.Text = "Importar..."
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(455, 113)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl14.TabIndex = 73
        Me.LabelControl14.Text = "Sucursal:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(501, 109)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(285, 20)
        Me.leSucursal.TabIndex = 6
        '
        'meConcepto
        '
        Me.meConcepto.EnterMoveNextControl = True
        Me.meConcepto.Location = New System.Drawing.Point(115, 47)
        Me.meConcepto.Name = "meConcepto"
        Me.meConcepto.Size = New System.Drawing.Size(671, 61)
        Me.meConcepto.TabIndex = 4
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(56, 9)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl6.TabIndex = 66
        Me.LabelControl6.Text = "Correlativo:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(396, 9)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(104, 13)
        Me.LabelControl1.TabIndex = 65
        Me.LabelControl1.Text = "No. de Comprobante:"
        '
        'teCorrelativo
        '
        Me.teCorrelativo.Enabled = False
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(115, 5)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Properties.ReadOnly = True
        Me.teCorrelativo.Size = New System.Drawing.Size(112, 20)
        Me.teCorrelativo.TabIndex = 0
        '
        'teNumero
        '
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(501, 5)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(112, 20)
        Me.teNumero.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(467, 31)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl2.TabIndex = 67
        Me.LabelControl2.Text = "Fecha:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(501, 26)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(112, 20)
        Me.deFecha.TabIndex = 3
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(17, 113)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(96, 13)
        Me.LabelControl10.TabIndex = 68
        Me.LabelControl10.Text = "Bodega de Entrada:"
        '
        'leBodega
        '
        Me.leBodega.EnterMoveNextControl = True
        Me.leBodega.Location = New System.Drawing.Point(115, 109)
        Me.leBodega.Name = "leBodega"
        Me.leBodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leBodega.Size = New System.Drawing.Size(273, 20)
        Me.leBodega.TabIndex = 5
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(63, 54)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl13.TabIndex = 69
        Me.LabelControl13.Text = "Concepto:"
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(14, 31)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(99, 13)
        Me.LabelControl17.TabIndex = 70
        Me.LabelControl17.Text = "Tipo de Transacción:"
        '
        'leTipoTransaccion
        '
        Me.leTipoTransaccion.EnterMoveNextControl = True
        Me.leTipoTransaccion.Location = New System.Drawing.Point(115, 26)
        Me.leTipoTransaccion.Name = "leTipoTransaccion"
        Me.leTipoTransaccion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoTransaccion.Size = New System.Drawing.Size(273, 20)
        Me.leTipoTransaccion.TabIndex = 2
        '
        'inv_frmEntradas
        '
        Me.ClientSize = New System.Drawing.Size(938, 360)
        Me.Controls.Add(Me.xtcEntradas)
        Me.Modulo = "Inventario"
        Me.Name = "inv_frmEntradas"
        Me.OptionId = "002001"
        Me.Text = "Entradas de Inventario"
        Me.Controls.SetChildIndex(Me.xtcEntradas, 0)
        CType(Me.xtcEntradas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcEntradas.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCentro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCantidad2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcFooter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.teNumRemision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdRemision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoTransaccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents xtcEntradas As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCodigo As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ritePrecio As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcPrecioTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents pcFooter As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btRevertir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btAplicar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btSaveAs As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbImportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents meConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leBodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoTransaccion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcCorrelativo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcSucursal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents gcCreadoPor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents gcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCantidad2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents sbPedido As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teNumRemision As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdRemision As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcAplicada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leCentro As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit

End Class
