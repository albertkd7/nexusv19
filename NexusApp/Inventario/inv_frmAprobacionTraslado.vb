﻿Imports DevExpress.XtraReports.UI
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class inv_frmAprobacionTraslado
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Dim entUsu As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub fac_frmAprobacionCotizacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        gc.DataSource = bl.ConsultaTraslados(Today, Today, objMenu.User)
        gv.BestFitColumns()
    End Sub



    Private Sub btAprobar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAprobar.Click
        Dim IdDoc As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
        If IdDoc = 0 Then
            MsgBox("Es necesario selecionar un traslado", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If MsgBox("¿Está seguro(a) de aprobar el Traslado entre Bodegas?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Dim EntTra As inv_Traslados
            EntTra = objTablas.inv_TrasladosSelectByPK(IdDoc)
            bl.inv_AprobarTraslado(IdDoc, 1, "", objMenu.User)
            Dim Mj As String
            Mj = bl.inv_AplicarTransaccionInventario(IdDoc, "Traslado", "I", EntTra.IdTipoComprobante, 2)
            If Mj = "Ok" Then
                MsgBox("La transacción ha sido aplicada con éxito. Ya no puede editarse", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox("SE DETECTÓ UN ERROR AL RESERVAR" + Chr(13) + Mj, MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If

            gc.DataSource = bl.ConsultaTraslados(deDesde.EditValue, deHasta.EditValue, objMenu.User)
            gv.BestFitColumns()
        End If

    End Sub

  
    Private Sub deDesde_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deDesde.Validated
        gc.DataSource = bl.ConsultaTraslados(deDesde.EditValue, deHasta.EditValue, objMenu.User)
        gv.BestFitColumns()
    End Sub

    Private Sub deHasta_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deHasta.Validated
        gc.DataSource = bl.ConsultaTraslados(deDesde.EditValue, deHasta.EditValue, objMenu.User)
        gv.BestFitColumns()
    End Sub

    Private Sub btDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDetalle.Click
        Dim IdDoc As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
        If IdDoc = 0 Then
            MsgBox("Es necesario selecionar un Traslado", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim entTraslado As inv_Traslados = objTablas.inv_TrasladosSelectByPK(IdDoc)
        Dim entBodegaI As inv_Bodegas = objTablas.inv_BodegasSelectByPK(entTraslado.IdBodegaIngreso)
        Dim entBodegaS As inv_Bodegas = objTablas.inv_BodegasSelectByPK(entTraslado.IdBodegaSalida)
        Dim entTipoTrassaccion As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(entTraslado.IdTipoComprobante)

        Dim dt As DataTable = bl.inv_ObtenerDetalleDocumento(IdDoc, "inv_TrasladosDetalle")
        Dim rpt As New inv_rptEntradaSalida() With {.DataSource = dt, .DataMember = ""}

        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "HOJA DE TRASLADO DE MERCADERÍA"
        rpt.xrlNumero.Text = entTraslado.Numero
        rpt.xrlTipo.Text = entTipoTrassaccion.Nombre
        rpt.xrlConcepto.Text = entTraslado.Concepto
        rpt.xrlFecha.Text = entTraslado.Fecha
        rpt.xrlBodegaSal.Text = "BODEGA DE SALIDA:"
        rpt.xrlBodega.Text = entBodegaS.Nombre
        rpt.xrlBodegaIng.Text = "BODEGA DE INGRESO: " + entBodegaI.Nombre

        If entTraslado.ImprimirValores = False Then
            rpt.XrLabel3.Visible = False
            rpt.XrLabel28.Visible = False
            rpt.xrlTotal.Visible = False
        End If
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        Dim IdDoc As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
        If IdDoc = 0 Then
            MsgBox("Es necesario selecionar una Traslado", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If MsgBox("¿Está seguro(a) de reprobar el Traslado entre Bodegas?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Dim Comentario As String = InputBox("Comentario:", "Reprobar Traslado", "")
            Dim entTras As inv_Traslados
            entTras = objTablas.inv_TrasladosSelectByPK(IdDoc)
            bl.inv_AprobarTraslado(IdDoc, 2, Comentario, objMenu.User)

            Dim msj As String = bl.inv_AplicarTransaccionInventario(entTras.IdComprobante, "Traslado", "D", entTras.IdTipoComprobante, 0)
            If msj = "" Then
                MsgBox("El comprobante ha sido Revertido con éxito", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox("SE DETECTÓ UN ERROR AL REVERTIR" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If
            gc.DataSource = bl.ConsultaTraslados(deDesde.EditValue, deHasta.EditValue, objMenu.User)
            gv.BestFitColumns()
        End If
    End Sub

    Private Sub sbReprobar_Click(sender As Object, e As EventArgs) Handles sbReprobar.Click
        gc.DataSource = bl.ConsultaTraslados(deDesde.EditValue, deHasta.EditValue, objMenu.User)
        gv.BestFitColumns()
    End Sub
End Class
