﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class inv_frmUbicaciones
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim entidad As inv_Ubicaciones

    Private Sub inv_frmUbicaciones_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub inv_frmUbicaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.inv_UbicacionesSelectAll
        entidad = objTablas.inv_UbicacionesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdUbicacion"))

        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub inv_frmUbicaciones_Nuevo_Click() Handles Me.Nuevo
        entidad = New inv_Ubicaciones
        entidad.IdUbicacion = objFunciones.ObtenerUltimoId("INV_UBICACIONES", "IdUbicacion") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub inv_frmUbicaciones_Save_Click() Handles Me.Guardar
        If teNombre.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Nombre]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.inv_UbicacionesInsert(entidad)
        Else
            objTablas.inv_UbicacionesUpdate(entidad)
        End If
        gc.DataSource = objTablas.inv_UbicacionesSelectAll

        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub inv_frmUbicaciones_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar la ubicacion seleccionada?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.inv_UbicacionesDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.inv_UbicacionesSelectAll
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR LA UBICACION:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub inv_frmUbicaciones_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entidad
            teId.EditValue = .IdUbicacion
            teNombre.EditValue = .Nombre
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entidad
            .IdUbicacion = teId.EditValue
            .Nombre = teNombre.EditValue
        End With


    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entidad = objTablas.inv_UbicacionesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdUbicacion"))
        CargaPantalla()
    End Sub

    Private Sub inv_frmUbicaciones_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teId.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub

End Class
