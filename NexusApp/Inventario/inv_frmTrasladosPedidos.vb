﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class inv_frmTrasladosPedidos

    Dim dtDetalle As New DataTable
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blInv As New InventarioBLL(g_ConnectionString)

    Private _IdSucursal As Integer
    Public Property IdSucursal() As Integer
        Get
            Return _IdSucursal
        End Get
        Set(ByVal value As Integer)
            _IdSucursal = value
        End Set
    End Property

    Private _IdComprobante As Integer
    Public Property IdComprobante() As Integer
        Get
            Return _IdComprobante
        End Get
        Set(ByVal value As Integer)
            _IdComprobante = value
        End Set
    End Property

    Private _IdBodegaVenta As Integer
    Public Property IdBodegaVenta() As Integer
        Get
            Return _IdBodegaVenta
        End Get
        Set(ByVal value As Integer)
            _IdBodegaVenta = value
        End Set
    End Property


    Private _Numero As String
    Public Property Numero() As String
        Get
            Return _Numero
        End Get
        Set(ByVal value As String)
            _Numero = value
        End Set
    End Property

    Private _CreadoPor As String
    Public Property CreadoPor() As String
        Get
            Return _CreadoPor
        End Get
        Set(ByVal value As String)
            _CreadoPor = value
        End Set
    End Property

    Private Sub fac_frmAplicacionNotaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gcRM.DataSource = bl.fac_PedidosParaTraslados(IdSucursal)
    End Sub

    Private Sub gcRM_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gcRM.DoubleClick
        IdComprobante = gvRM.GetRowCellValue(gvRM.FocusedRowHandle, "IdComprobante")
        Numero = gvRM.GetRowCellValue(gvRM.FocusedRowHandle, "Numero")
        IdBodegaVenta = gvRM.GetRowCellValue(gvRM.FocusedRowHandle, "IdBodega")
        CreadoPor = gvRM.GetRowCellValue(gvRM.FocusedRowHandle, "CreadoPor")
        AgregaDetalle(IdComprobante)

    End Sub

    Private Sub gvRM_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvRM.KeyDown
        If e.KeyCode = Keys.Enter Then
            IdComprobante = gvRM.GetRowCellValue(gvRM.FocusedRowHandle, "IdComprobante")
            Numero = gvRM.GetRowCellValue(gvRM.FocusedRowHandle, "Numero")
            IdBodegaVenta = gvRM.GetRowCellValue(gvRM.FocusedRowHandle, "IdBodega")
            AgregaDetalle(IdComprobante)
            'Me.Close()
        End If
    End Sub

    Private Sub AgregaDetalle(ByVal IdComprobante As Integer)
        Dim frmEnt As inv_frmEntradas = Me.Owner
        Dim dt As DataTable = frmEnt.dtRemision

        Dim dtDetalleGarantia As DataTable = bl.fac_ObtenerDetalleDocumento("fac_VentasDetalle", IdComprobante)


        For j = 0 To dtDetalleGarantia.Rows.Count - 1

            If dtDetalleGarantia.Rows(j).Item("IdProducto") = "N/A" Then
                Exit For
            End If

            Dim dr As DataRow = frmEnt.dtRemision.NewRow()
            Dim dtSaldo As DataTable
            dtSaldo = blInv.inv_ObtieneExistenciaCostoProducto(dtDetalleGarantia.Rows(j).Item("IdProducto"), IdBodegaVenta)

            dr("IdProducto") = dtDetalleGarantia.Rows(j).Item("IdProducto")
            dr("Cantidad") = dtDetalleGarantia.Rows(j).Item("Cantidad")
            dr("Descripcion") = dtDetalleGarantia.Rows(j).Item("Descripcion")

            If dtSaldo.Rows.Count > 0 Then
                dr("PrecioUnitario") = SiEsNulo(dtSaldo.Rows(0).Item("PrecioCosto"), 0.0)
                dr("PrecioTotal") = Decimal.Round(SiEsNulo(dtSaldo.Rows(0).Item("PrecioCosto"), 0.0) * dtDetalleGarantia.Rows(j).Item("Cantidad"), 2)
            Else
                dr("PrecioUnitario") = 0.0
                dr("PrecioTotal") = 0.0
            End If

            frmEnt.dtRemision.Rows.Add(dr)
        Next
        Dispose()
    End Sub
End Class