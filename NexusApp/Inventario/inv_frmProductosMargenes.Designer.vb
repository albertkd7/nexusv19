﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmProductosMargenes
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdMargen = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDesde = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcHasta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcValor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.gc.Size = New System.Drawing.Size(431, 282)
        Me.gc.TabIndex = 3
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdMargen, Me.gcDesde, Me.gcHasta, Me.gcValor})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdMargen
        '
        Me.gcIdMargen.Caption = "Id Margen"
        Me.gcIdMargen.FieldName = "IdMargen"
        Me.gcIdMargen.Name = "gcIdMargen"
        Me.gcIdMargen.Visible = True
        Me.gcIdMargen.VisibleIndex = 0
        Me.gcIdMargen.Width = 91
        '
        'gcDesde
        '
        Me.gcDesde.Caption = "Desde"
        Me.gcDesde.FieldName = "Desde"
        Me.gcDesde.Name = "gcDesde"
        Me.gcDesde.Visible = True
        Me.gcDesde.VisibleIndex = 1
        Me.gcDesde.Width = 101
        '
        'gcHasta
        '
        Me.gcHasta.Caption = "Hasta"
        Me.gcHasta.FieldName = "Hasta"
        Me.gcHasta.Name = "gcHasta"
        Me.gcHasta.Visible = True
        Me.gcHasta.VisibleIndex = 2
        Me.gcHasta.Width = 99
        '
        'gcValor
        '
        Me.gcValor.Caption = "Valor"
        Me.gcValor.FieldName = "Valor"
        Me.gcValor.Name = "gcValor"
        Me.gcValor.Visible = True
        Me.gcValor.VisibleIndex = 3
        Me.gcValor.Width = 122
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'inv_frmProductosMargenes
        '
        Me.ClientSize = New System.Drawing.Size(706, 307)
        Me.Controls.Add(Me.gc)
        Me.Modulo = "Inventario"
        Me.Name = "inv_frmProductosMargenes"
        Me.OptionId = "001005"
        Me.Text = "Productos Margenes"
        Me.TipoFormulario = 2
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdMargen As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDesde As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcHasta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents gcValor As DevExpress.XtraGrid.Columns.GridColumn

End Class
