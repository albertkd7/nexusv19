﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmCategorias
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.gcSucursales = New DevExpress.XtraGrid.GridControl()
        Me.gvSucursales = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcDesde = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leSucursal = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcHasta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteIdCuenta = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdCategoria = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.gcSucursales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvSucursales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteIdCuenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdCategoria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.gc)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(491, 398)
        Me.PanelControl1.TabIndex = 4
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(2, 2)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(487, 394)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Correlativo"
        Me.GridColumn1.FieldName = "IdCategoria"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 78
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Categoría/Departamento"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 496
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.gcSucursales)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.teIdCategoria)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.teNombre)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(491, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(980, 398)
        Me.PanelControl2.TabIndex = 5
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(24, 119)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(161, 13)
        Me.LabelControl3.TabIndex = 44
        Me.LabelControl3.Text = "Cuentas de Ingreso por Sucursal:"
        '
        'gcSucursales
        '
        Me.gcSucursales.Location = New System.Drawing.Point(24, 137)
        Me.gcSucursales.MainView = Me.gvSucursales
        Me.gcSucursales.Name = "gcSucursales"
        Me.gcSucursales.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.leSucursal, Me.riteIdCuenta})
        Me.gcSucursales.Size = New System.Drawing.Size(447, 147)
        Me.gcSucursales.TabIndex = 43
        Me.gcSucursales.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvSucursales})
        '
        'gvSucursales
        '
        Me.gvSucursales.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcDesde, Me.gcHasta})
        Me.gvSucursales.GridControl = Me.gcSucursales
        Me.gvSucursales.Name = "gvSucursales"
        Me.gvSucursales.OptionsView.ShowGroupPanel = False
        '
        'gcDesde
        '
        Me.gcDesde.Caption = "Sucursal"
        Me.gcDesde.ColumnEdit = Me.leSucursal
        Me.gcDesde.FieldName = "IdSucursal"
        Me.gcDesde.Name = "gcDesde"
        Me.gcDesde.OptionsColumn.AllowEdit = False
        Me.gcDesde.OptionsColumn.AllowFocus = False
        Me.gcDesde.Visible = True
        Me.gcDesde.VisibleIndex = 0
        Me.gcDesde.Width = 242
        '
        'leSucursal
        '
        Me.leSucursal.AutoHeight = False
        Me.leSucursal.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Name = "leSucursal"
        '
        'gcHasta
        '
        Me.gcHasta.Caption = "Cuenta de Ingreso"
        Me.gcHasta.ColumnEdit = Me.riteIdCuenta
        Me.gcHasta.FieldName = "IdCuentaIngreso"
        Me.gcHasta.Name = "gcHasta"
        Me.gcHasta.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.gcHasta.Visible = True
        Me.gcHasta.VisibleIndex = 1
        Me.gcHasta.Width = 187
        '
        'riteIdCuenta
        '
        Me.riteIdCuenta.AutoHeight = False
        Me.riteIdCuenta.Name = "riteIdCuenta"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(24, 24)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl1.TabIndex = 34
        Me.LabelControl1.Text = "Correlativo:"
        '
        'teIdCategoria
        '
        Me.teIdCategoria.EnterMoveNextControl = True
        Me.teIdCategoria.Location = New System.Drawing.Point(24, 42)
        Me.teIdCategoria.Name = "teIdCategoria"
        Me.teIdCategoria.Size = New System.Drawing.Size(95, 20)
        Me.teIdCategoria.TabIndex = 27
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(24, 66)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl2.TabIndex = 35
        Me.LabelControl2.Text = "Descripción:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(24, 85)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(447, 20)
        Me.teNombre.TabIndex = 28
        '
        'inv_frmCategorias
        '
        Me.ClientSize = New System.Drawing.Size(1471, 423)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.DbMode = 1
        Me.Modulo = "Inventario"
        Me.Name = "inv_frmCategorias"
        Me.OptionId = "001006"
        Me.Text = "Categorías/Departamentos"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.PanelControl2, 0)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.gcSucursales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvSucursales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteIdCuenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdCategoria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdCategoria As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcSucursales As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvSucursales As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcDesde As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursal As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents gcHasta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteIdCuenta As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl

End Class
