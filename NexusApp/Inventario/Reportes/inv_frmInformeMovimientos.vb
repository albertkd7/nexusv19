﻿
Imports NexusBLL
Public Class inv_frmInformeMovimientos
    Dim bl As New InventarioBLL(g_ConnectionString)

    Private Sub inv_frmInformeMovimientos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.con_CentrosCosto(leCentro, "", "-- TODOS LOS CENTROS DE COSTOS --")
        leCentro.EditValue = "-1"
    End Sub

    Private Sub inv_frmInformeMovimientos_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = bl.inv_ObtenerMovimientos(deDesde.EditValue, deHasta.EditValue, RadioGroup1.EditValue, leCentro.EditValue)
        Dim rpt As New inv_rptMovimientosInventario() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)

        rpt.ShowPreviewDialog()

    End Sub
End Class
