﻿
Imports NexusBLL
Public Class inv_frmInformeVentasUtilidadPorDoc
    Dim bl As New InventarioBLL(g_ConnectionString)


    Private Sub inv_frmInformeVentasUtilidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
    End Sub

    Private Sub inv_frmInformeVentasUtilidad_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable

        dt = bl.inv_spVentasCostosPorDoc(deDesde.EditValue, deHasta.EditValue, leBodega.EditValue)
        Dim rpt As New inv_rptUtilidadesVentasPorDoc() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.XrLBodega.Text = "BODEGA: " + leBodega.Text
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.ShowPreview()
    End Sub
End Class
