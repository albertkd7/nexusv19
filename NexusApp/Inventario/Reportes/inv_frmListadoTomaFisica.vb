﻿
Imports NexusBLL

Public Class inv_frmListadoTomaFisica
    Dim bl As New InventarioBLL(g_ConnectionString)
    Private Sub inv_frmListadoTomaFisica_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
        objCombos.invSubGrupos(leSubGrupo, -1, "-- TODOS LOS SUB-GRUPOS --")
        objCombos.com_Proveedores(leProveedor)
        deHasta.EditValue = Today
    End Sub

    Private Sub leGrupo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leGrupo.EditValueChanged
        objCombos.invSubGrupos(leSubGrupo, leGrupo.EditValue, "-- TODOS LOS SUB-GRUPOS --")
    End Sub

    Private Sub inv_frmListadoTomaFisica_Reporte() Handles Me.Reporte
        If rgFiltro.EditValue = 0 Then
            Dim dt As DataTable = bl.inv_InventarioTomaFisica(deHasta.EditValue, leBodega.EditValue, leGrupo.EditValue, leSubGrupo.EditValue)
            Dim rpt As New inv_rptInventarioTomaFisica() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.XrLBodega.Text = "BODEGA: " + leBodega.Text
            rpt.xrlPeriodo.Text = (FechaToString(deHasta.EditValue, deHasta.EditValue)).ToUpper
            rpt.xrlTitulo.Text = teTitulo.EditValue
            rpt.ShowPreviewDialog()
        Else
            Dim dt As DataTable = bl.inv_InventarioTomaFisica(deHasta.EditValue, leBodega.EditValue, leProveedor.EditValue)

            Dim rpt As New inv_rptInventarioTomaFisicaProveedor() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlPeriodo.Text = (FechaToString(deHasta.EditValue, deHasta.EditValue)).ToUpper
            rpt.xrlTitulo.Text = teTitulo.EditValue
            rpt.ShowPreviewDialog()
        End If
       
    End Sub

    Private Sub rgFiltro_EditValueChanged(sender As Object, e As EventArgs) Handles rgFiltro.EditValueChanged
        lcProveedor.Visible = rgFiltro.EditValue = 1
        leProveedor.Visible = rgFiltro.EditValue = 1
        lcGrupo.Visible = rgFiltro.EditValue = 0
        leGrupo.Visible = rgFiltro.EditValue = 0
        lcSubGrupo.Visible = rgFiltro.EditValue = 0
        leSubGrupo.Visible = rgFiltro.EditValue = 0

    End Sub
End Class
