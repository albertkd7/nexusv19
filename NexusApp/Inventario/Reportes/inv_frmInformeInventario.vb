﻿
Imports NexusBLL
Public Class inv_frmInformeInventario
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim dt As New DataTable
    
    Private Sub inv_frmInventarioValuado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
        objCombos.invSubGrupos(leSubGrupo, -1, "-- TODOS LOS SUB-GRUPOS --")
        objCombos.invCategoriaProducto(leCategoria, "-- TODAS LAS CATEGORÍAS --")
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        BeProveedor1.beCodigo.EditValue = ""

    End Sub

    Private Sub inv_frmInventarioValuado_Report_Click() Handles Me.Reporte
        If rgTipoReporte.EditValue = 2 Then
            dt = bl.inv_InventarioUnidades(deDesde.EditValue, deHasta.EditValue, leBodega.EditValue, leGrupo.EditValue, leSubGrupo.EditValue, leCategoria.EditValue)

            Dim rpt As New inv_rptInventarioUnidades() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlPeriodo.Text = (FechaToString(deDesde.EditValue, deHasta.EditValue)).ToUpper
            rpt.XrlBodega.Text = "BODEGA: " + leBodega.Text
            rpt.xrlTitulo.Text = teTitulo.EditValue
            rpt.xrlMoneda.Text = gsDesc_Moneda
            rpt.ShowPreviewDialog()
        ElseIf rgTipoReporte.EditValue = 1 Then
            dt = bl.inv_InventarioValuado(deDesde.EditValue, deHasta.EditValue, leBodega.EditValue, leGrupo.EditValue, leSubGrupo.EditValue, False, leCategoria.EditValue)
            Dim rpt As New inv_rptInventarioValuado() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.XrLBodega.Text = "BODEGA: " + leBodega.Text
            rpt.xrlPeriodo.Text = (FechaToString(deDesde.EditValue, deHasta.EditValue)).ToUpper
            rpt.xrlTitulo.Text = teTitulo.EditValue
            rpt.xrlMoneda.Text = gsDesc_Moneda
            rpt.ShowPreviewDialog()
        Else
            Try
                dt = bl.inv_InventarioValuadoProveedor(deDesde.EditValue, deHasta.EditValue, leBodega.EditValue, leGrupo.EditValue, leSubGrupo.EditValue, False, leCategoria.EditValue, BeProveedor1.beCodigo.EditValue)
                Dim rpt As New inv_rptInventarioValuadoProveedor() With {.DataSource = dt, .DataMember = ""}
                rpt.xrlEmpresa.Text = gsNombre_Empresa
                rpt.XrLBodega.Text = "BODEGA: " + leBodega.Text
                rpt.xrlPeriodo.Text = (FechaToString(deDesde.EditValue, deHasta.EditValue)).ToUpper
                rpt.xrlTitulo.Text = teTitulo.EditValue
                rpt.xrlMoneda.Text = gsDesc_Moneda
                rpt.ShowPreviewDialog()
            Catch ex As Exception
                MsgBox("Reporte no activado", MsgBoxStyle.Exclamation, "NOTA")
            End Try

        End If
    End Sub
    Private Sub leCategoria_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leCategoria.EditValueChanged
        objCombos.invGrupos(leGrupo, leCategoria.EditValue, "-- TODOS LOS GRUPOS --")
    End Sub
    Private Sub leGrupo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leGrupo.EditValueChanged
        objCombos.invSubGrupos(leSubGrupo, leGrupo.EditValue, "-- TODOS LOS SUB-GRUPOS --")
    End Sub

    Private Sub sbGenera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGenera.Click
        dt = bl.inv_InventarioValuado(deDesde.EditValue, deHasta.EditValue, leBodega.EditValue, leGrupo.EditValue, leSubGrupo.EditValue, True, leCategoria.EditValue)

        Dim fbd As New FolderBrowserDialog

        fbd.ShowDialog()
        If fbd.SelectedPath = "" Then
            MsgBox("No se seleccionó ninguna carpeta", MsgBoxStyle.Critical, "Nota")
            Return
        End If

        Try
            GenerarArchivo(String.Format("{0}\{1}.txt", fbd.SelectedPath, "F983"), dt)
            MsgBox("Los archivos han sido generado con éxito." + Chr(13) + "Y fueron colocados en " + fbd.SelectedPath, MsgBoxStyle.Information, "Nota")

        Catch ex As Exception
            MsgBox("NO FUE POSIBLE GENERAR LOS ARCHIVOS" + Chr(13) + ex.Message.ToString, MsgBoxStyle.Critical, "Nota")
        End Try
    End Sub
    Private Sub GenerarArchivo(ByVal Archivo As String, ByVal dt As DataTable)
        FileOpen(1, Archivo, OpenMode.Output)
        Dim sb As String = ""
        Dim dc As DataColumn

        Dim i As Integer = 0
        Dim dr As DataRow
        For Each dr In dt.Rows
            i = 0 : sb = ""
            For Each dc In dt.Columns
                If Not IsDBNull(dr(i)) Then
                    sb &= CStr(dr(i))
                End If
                i += 1
            Next
            PrintLine(1, sb)
        Next
        FileClose(1)
    End Sub


    Private Sub sbGeneraExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGeneraExcel.Click
        ExportarLibroExcel()
    End Sub
    Private Sub ExportarLibroExcel()
        If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Inventario.XLSX") Then
            If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Inventario.XLS") Then
                MsgBox("No existe la plantilla necesaria para poder exportar", MsgBoxStyle.Critical, "Nota")
                Exit Sub
            End If
        End If

        Dim Template As String
        teProgreso.EditValue = "PREPARANDO LOS DATOS. ESPERE..."
        teProgreso.Visible = True
        If FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Inventario.XLSX") Then
            Template = Application.StartupPath & "\Plantillas\InventarioTmp.XLSX"
            FileCopy(Application.StartupPath & "\Plantillas\Inventario.XLSX", Template)
        Else
            Template = Application.StartupPath & "\Plantillas\InventarioTmp.XLS"
            FileCopy(Application.StartupPath & "\Plantillas\Inventario.XLS", Template)
        End If

        Dim oApp As Object = CreateObject("Excel.Application")

        'verifico si no está en una cultura diferente, por el momento de Inglés a Español, pendiente de verificar cuando sea a la inversa
        Dim oldCI As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        Try
            oApp.DisplayAlerts = False
        Catch ex As Exception
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")
            oApp.DisplayAlerts = False
        End Try

        oApp.workbooks.Open(Template)
        oApp.Cells(2, 1).Value = gsNombre_Empresa
        oApp.Cells(3, 1).Value = (FechaToString(deDesde.EditValue, deHasta.EditValue)).ToUpper
        oApp.Cells(4, 1).Value = teTitulo.EditValue
        oApp.Cells(5, 1).Value = gsDesc_Moneda

        Dim Linea As Integer = 7, i As Integer = 0
        Dim dt As New DataTable
        dt = bl.inv_InventarioValuado(deDesde.EditValue, deHasta.EditValue, leBodega.EditValue, leGrupo.EditValue, leSubGrupo.EditValue, False, leCategoria.EditValue)

        While i <= dt.Rows.Count - 1
            oApp.Cells(Linea, 1).Value = dt.Rows(i).Item("IdClasificacion")
            oApp.Cells(Linea, 2).Value = dt.Rows(i).Item("IdGrupo")
            oApp.Cells(Linea, 3).Value = dt.Rows(i).Item("IdSubGrupo")
            oApp.Cells(Linea, 4).Value = dt.Rows(i).Item("IdProducto")
            oApp.Cells(Linea, 5).Value = dt.Rows(i).Item("Nombre")
            oApp.Cells(Linea, 6).Value = dt.Rows(i).Item("UnidadMedida")
            oApp.Cells(Linea, 7).Value = dt.Rows(i).Item("Precio")
            oApp.Cells(Linea, 8).Value = dt.Rows(i).Item("PrecioSIva")
            oApp.Cells(Linea, 9).Value = dt.Rows(i).Item("Existencias")
            oApp.Cells(Linea, 10).Value = dt.Rows(i).Item("CostoUnitario")
            oApp.Cells(Linea, 11).Value = dt.Rows(i).Item("CostoUltCompra")
            oApp.Cells(Linea, 12).Value = dt.Rows(i).Item("CostoFOB")
            i += 1
            Linea += 1
        End While
        teProgreso.Visible = False
        oApp.Visible = True
        System.Threading.Thread.CurrentThread.CurrentCulture = oldCI

    End Sub
End Class
