Public Class inv_rptKardexBodega
    Dim SaldoProducto As Decimal = 0.0
    Dim SaldoProductoTot As Decimal = 0.0
    Private Sub GroupHeader1_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles GroupHeader1.BeforePrint
        If GetCurrentColumnValue("Documento") = "Saldo anterior -->" Then
            SaldoProducto = GetCurrentColumnValue("SaldoUnidades")
            SaldoProductoTot += SaldoProducto
        End If
    End Sub

    Private Sub Detail_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Detail.BeforePrint
        If GetCurrentColumnValue("TipoAplicacion") = 1 Then
            SaldoProducto += GetCurrentColumnValue("EntradasUnidades")
            SaldoProductoTot += GetCurrentColumnValue("EntradasUnidades")
        Else
            SaldoProducto -= GetCurrentColumnValue("SalidasUnidades")
            SaldoProductoTot -= GetCurrentColumnValue("SalidasUnidades")
        End If

    End Sub

    Private Sub XrLabel18_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel18.BeforePrint
        XrLabel18.Text = Format(SaldoProducto, "##,###,##0.00")
    End Sub

    Private Sub XrLabel33_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel33.BeforePrint
        XrLabel33.Text = Format(SaldoProductoTot, "##,###,##0.00")
    End Sub
End Class