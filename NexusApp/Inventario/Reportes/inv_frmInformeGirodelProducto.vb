﻿
Imports NexusBLL
Public Class inv_frmInformeGirodelProducto
    Dim bl As New InventarioBLL(g_ConnectionString)


    Private Sub inv_frmInformeVentasUtilidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        spAnio.EditValue = Today.Year
        meMes.EditValue = Today.Month
    End Sub

    Private Sub inv_frmInformeVentasUtilidad_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable

        dt = bl.inv_spRptGiroProducto(BeProducto1.beCodigo.EditValue, spAnio.EditValue, meMes.EditValue)
        Dim rpt As New inv_rptGiroDelProducto() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.ShowPreview()
    End Sub
End Class
