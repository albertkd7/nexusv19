﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmListadoTomaFisica
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.leSubGrupo = New DevExpress.XtraEditors.LookUpEdit()
        Me.lcSubGrupo = New DevExpress.XtraEditors.LabelControl()
        Me.leGrupo = New DevExpress.XtraEditors.LookUpEdit()
        Me.lcGrupo = New DevExpress.XtraEditors.LabelControl()
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit()
        Me.leBodega = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.deHasta = New DevExpress.XtraEditors.DateEdit()
        Me.rgFiltro = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.lcProveedor = New DevExpress.XtraEditors.LabelControl()
        Me.leProveedor = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.leSubGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgFiltro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.rgFiltro)
        Me.GroupControl1.Controls.Add(Me.leProveedor)
        Me.GroupControl1.Controls.Add(Me.lcProveedor)
        Me.GroupControl1.Controls.Add(Me.leSubGrupo)
        Me.GroupControl1.Controls.Add(Me.lcSubGrupo)
        Me.GroupControl1.Controls.Add(Me.leGrupo)
        Me.GroupControl1.Controls.Add(Me.lcGrupo)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.leBodega)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.deHasta)
        Me.GroupControl1.TabIndex = 0
        '
        'leSubGrupo
        '
        Me.leSubGrupo.EnterMoveNextControl = True
        Me.leSubGrupo.Location = New System.Drawing.Point(100, 145)
        Me.leSubGrupo.Name = "leSubGrupo"
        Me.leSubGrupo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSubGrupo.Size = New System.Drawing.Size(338, 20)
        Me.leSubGrupo.TabIndex = 4
        '
        'lcSubGrupo
        '
        Me.lcSubGrupo.Location = New System.Drawing.Point(42, 148)
        Me.lcSubGrupo.Name = "lcSubGrupo"
        Me.lcSubGrupo.Size = New System.Drawing.Size(55, 13)
        Me.lcSubGrupo.TabIndex = 21
        Me.lcSubGrupo.Text = "Sub-Grupo:"
        '
        'leGrupo
        '
        Me.leGrupo.EnterMoveNextControl = True
        Me.leGrupo.Location = New System.Drawing.Point(100, 119)
        Me.leGrupo.Name = "leGrupo"
        Me.leGrupo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leGrupo.Size = New System.Drawing.Size(338, 20)
        Me.leGrupo.TabIndex = 3
        '
        'lcGrupo
        '
        Me.lcGrupo.Location = New System.Drawing.Point(64, 123)
        Me.lcGrupo.Name = "lcGrupo"
        Me.lcGrupo.Size = New System.Drawing.Size(33, 13)
        Me.lcGrupo.TabIndex = 20
        Me.lcGrupo.Text = "Grupo:"
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "LISTADO PARA TOMA FISICA DE INVENTARIO"
        Me.teTitulo.Location = New System.Drawing.Point(100, 169)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(338, 20)
        Me.teTitulo.TabIndex = 5
        '
        'leBodega
        '
        Me.leBodega.EnterMoveNextControl = True
        Me.leBodega.Location = New System.Drawing.Point(100, 64)
        Me.leBodega.Name = "leBodega"
        Me.leBodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leBodega.Size = New System.Drawing.Size(338, 20)
        Me.leBodega.TabIndex = 1
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(13, 173)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl5.TabIndex = 13
        Me.LabelControl5.Text = "Título de reporte:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(57, 67)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl3.TabIndex = 16
        Me.LabelControl3.Text = "Bodega:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(33, 46)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 14
        Me.LabelControl2.Text = "Hasta Fecha:"
        '
        'deHasta
        '
        Me.deHasta.EditValue = Nothing
        Me.deHasta.EnterMoveNextControl = True
        Me.deHasta.Location = New System.Drawing.Point(100, 43)
        Me.deHasta.Name = "deHasta"
        Me.deHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deHasta.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deHasta.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deHasta.Size = New System.Drawing.Size(100, 20)
        Me.deHasta.TabIndex = 0
        '
        'rgFiltro
        '
        Me.rgFiltro.EditValue = CType(0, Short)
        Me.rgFiltro.EnterMoveNextControl = True
        Me.rgFiltro.Location = New System.Drawing.Point(100, 89)
        Me.rgFiltro.Name = "rgFiltro"
        Me.rgFiltro.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(0, Short), "Grupo y Sub-Grupo"), New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(1, Short), "Proveedor")})
        Me.rgFiltro.Size = New System.Drawing.Size(310, 25)
        Me.rgFiltro.TabIndex = 2
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(46, 97)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl1.TabIndex = 16
        Me.LabelControl1.Text = "Filtrar por:"
        '
        'lcProveedor
        '
        Me.lcProveedor.Location = New System.Drawing.Point(43, 123)
        Me.lcProveedor.Name = "lcProveedor"
        Me.lcProveedor.Size = New System.Drawing.Size(54, 13)
        Me.lcProveedor.TabIndex = 21
        Me.lcProveedor.Text = "Proveedor:"
        '
        'leProveedor
        '
        Me.leProveedor.EnterMoveNextControl = True
        Me.leProveedor.Location = New System.Drawing.Point(100, 119)
        Me.leProveedor.Name = "leProveedor"
        Me.leProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leProveedor.Size = New System.Drawing.Size(624, 20)
        Me.leProveedor.TabIndex = 4
        '
        'inv_frmListadoTomaFisica
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(839, 314)
        Me.Modulo = "Inventario"
        Me.Name = "inv_frmListadoTomaFisica"
        Me.Text = "Listado para Toma Física de Inventario"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.leSubGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgFiltro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents leSubGrupo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lcSubGrupo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leGrupo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lcGrupo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leBodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents rgFiltro As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents leProveedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lcProveedor As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl

End Class
