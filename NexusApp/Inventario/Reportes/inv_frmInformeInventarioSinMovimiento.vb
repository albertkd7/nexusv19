﻿
Imports NexusBLL
Public Class inv_frmInformeInventarioSinMovimiento
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim dt As New DataTable

    Private Sub inv_frmInventarioValuado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
        objCombos.invSubGrupos(leSubGrupo, -1, "-- TODOS LOS SUB-GRUPOS --")
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        BeProducto1.beCodigo.EditValue = ""
    End Sub

    Private Sub inv_frmInventarioValuado_Report_Click() Handles Me.Reporte

        dt = bl.inv_InventarioSinMovimiento(deDesde.EditValue, deHasta.EditValue, BeProducto1.beCodigo.EditValue, leGrupo.EditValue, leSubGrupo.EditValue)
        Dim rpt As New inv_rptInventarioSinMovimiento With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.XrLBodega.Text = "BODEGA: " + leBodega.Text
        rpt.xrlPeriodo.Text = (FechaToString(deDesde.EditValue, deHasta.EditValue)).ToUpper
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.ShowPreviewDialog()

    End Sub

    Private Sub leGrupo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leGrupo.EditValueChanged
        objCombos.invSubGrupos(leSubGrupo, leGrupo.EditValue, "-- TODOS LOS SUB-GRUPOS --")
    End Sub
End Class
