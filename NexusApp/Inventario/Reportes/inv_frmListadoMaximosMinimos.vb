﻿
Imports NexusBLL
Public Class inv_frmListadoMaximosMinimos
    Dim bl As New InventarioBLL(g_ConnectionString)


    Private Sub inv_frmListadoMaximosMinimos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
        objCombos.invSubGrupos(leSubGrupo, -1, "-- TODOS LOS SUB-GRUPOS --")
        objCombos.invMaximosMinimos(leMaximoMinimo, "")
        deHasta.EditValue = Today
    End Sub

    Private Sub inv_frmListadoMaximosMinimos_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = bl.inv_ListaDeProductosMaximosMinimos(leGrupo.EditValue, leSubGrupo.EditValue, deHasta.EditValue, leMaximoMinimo.EditValue, ceAplican.EditValue)
        Dim rpt As New inv_rptMaximosMinimos() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue

        If leMaximoMinimo.EditValue = 2 Then
            rpt.xrlMaxMin.Text = "Existencia Mínima"
        Else
            rpt.xrlMaxMin.Text = "Existencia Máxima"
        End If
        rpt.ShowPreview()
    End Sub

    Private Sub leGrupo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leGrupo.EditValueChanged
        objCombos.invSubGrupos(leSubGrupo, leGrupo.EditValue)
    End Sub

    Private Sub leMaximoMinimo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leMaximoMinimo.EditValueChanged
        If leMaximoMinimo.EditValue = 2 Then
            teTitulo.EditValue = "LISTADO DE PRODUCTOS EN EXISTENCIAS MINIMAS"
            ceAplican.Text = "Solo Productos en Existencia Mínima"
        Else
            teTitulo.EditValue = "LISTADO DE PRODUCTOS EN EXISTENCIAS MAXIMAS"
            ceAplican.Text = "Solo Productos en Existencia Máxima"
        End If
    End Sub
End Class
