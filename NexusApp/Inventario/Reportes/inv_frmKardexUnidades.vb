﻿
Imports NexusBLL
Public Class inv_frmKardexUnidades
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim entProducto As New NexusELL.TableEntities.inv_Productos
    
    Private Sub frmKardex_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        deDesde.EditValue = Today
        deHasta.EditValue = Today
    End Sub

    Private Sub inv_frmKardexValuado_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = bl.inv_KardexBodega(deDesde.EditValue, deHasta.EditValue, BeProducto1.beCodigo.EditValue, leBodega.EditValue)

        Dim rpt As New inv_rptKardexBodega() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue).ToUpper
        rpt.xrlBodega.Text = "BODEGA: " + leBodega.Text
        rpt.ShowPreviewDialog()
    End Sub

End Class
