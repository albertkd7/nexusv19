﻿Imports System.Math

Public Class inv_rptInventarioTomaFisicaDiferencia
    Private TotalMenos As Decimal
    Private TotalMas As Decimal
    Private DiferenciaTotal As Decimal = 0.0

    Private Sub inv_rptInventarioTomaFisicaDiferencia_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        ' Create a new rule and add it to a report.
        Dim ruleDif As New FormattingRule()
        'Dim ruleCero As New FormattingRule()
        Me.FormattingRuleSheet.Add(ruleDif)
        'Me.FormattingRuleSheet.Add(ruleCero)

        ' Specify the rule's properties.
        ruleDif.DataSource = Me.DataSource
        ruleDif.DataMember = Me.DataMember
        ruleDif.Condition = "[Diferencia] <> 0"
        ruleDif.Formatting.BackColor = Color.WhiteSmoke
        ruleDif.Formatting.ForeColor = Color.IndianRed
        ruleDif.Formatting.Font = New Font("Arial", 10, FontStyle.Bold)

        ''ruleCero.Condition = "[ValorMenos] = 0"
        ''ruleCero.Formatting.Visible = DevExpress.Utils.DefaultBoolean.False

        ' Apply this rule to the detail band.
        Me.Detail.FormattingRules.Add(ruleDif)
        'Me.Detail.FormattingRules.Add(ruleCero)
    End Sub

    Private Sub XrLabel1_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel1.BeforePrint
        Dim Diferencia As Decimal = 0.0
        Dim PrecioCosto As Decimal = 0.0
        Diferencia = SiEsNulo(GetCurrentColumnValue("Diferencia"), 0)
        PrecioCosto = SiEsNulo(GetCurrentColumnValue("PrecioCosto"), 0)
        If Diferencia > 0 Then
            XrLabel11.Text = Decimal.Round(Diferencia * PrecioCosto, 2)
            TotalMas = TotalMas + Decimal.Round(Diferencia * PrecioCosto, 2)
            DiferenciaTotal = DiferenciaTotal + Decimal.Round(Diferencia * PrecioCosto, 2)
            XrLabel8.Text = 0.0
        Else
            XrLabel8.Text = Decimal.Round((Diferencia * -1) * PrecioCosto, 2)
            TotalMenos = TotalMenos + Decimal.Round((Diferencia * -1) * PrecioCosto, 2)
            DiferenciaTotal = DiferenciaTotal - Decimal.Round((Diferencia * -1) * PrecioCosto, 2)
            XrLabel11.Text = 0.0
        End If
    End Sub

    Private Sub XrLabel19_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel19.BeforePrint
        XrLabel19.Text = TotalMenos
        TotalMenos = 0.0
    End Sub

    Private Sub XrLabel20_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel20.BeforePrint
        XrLabel20.Text = TotalMas
        TotalMas = 0.0
    End Sub

    Private Sub XrLabel24_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel24.BeforePrint
        XrLabel24.Text = DiferenciaTotal
        DiferenciaTotal = 0.0
    End Sub
End Class