﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class inv_frmInformeGirodelProducto
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.BeProducto1 = New Nexus.beProducto()
        Me.spAnio = New DevExpress.XtraEditors.SpinEdit()
        Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.spAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.spAnio)
        Me.GroupControl1.Controls.Add(Me.meMes)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.BeProducto1)
        Me.GroupControl1.Size = New System.Drawing.Size(679, 173)
        '
        'BeProducto1
        '
        Me.BeProducto1.Location = New System.Drawing.Point(45, 91)
        Me.BeProducto1.Name = "BeProducto1"
        Me.BeProducto1.Size = New System.Drawing.Size(612, 25)
        Me.BeProducto1.TabIndex = 11
        '
        'spAnio
        '
        Me.spAnio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spAnio.Location = New System.Drawing.Point(101, 39)
        Me.spAnio.Name = "spAnio"
        Me.spAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.spAnio.Properties.Mask.EditMask = "####"
        Me.spAnio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.spAnio.Size = New System.Drawing.Size(61, 20)
        Me.spAnio.TabIndex = 30
        '
        'meMes
        '
        Me.meMes.Location = New System.Drawing.Point(101, 65)
        Me.meMes.Name = "meMes"
        Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes.Size = New System.Drawing.Size(100, 20)
        Me.meMes.TabIndex = 29
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(41, 68)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl3.TabIndex = 31
        Me.LabelControl3.Text = "Hasta Mes:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(72, 42)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(23, 13)
        Me.LabelControl1.TabIndex = 31
        Me.LabelControl1.Text = "Año:"
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "INFORME DE GIRO DEL PRODUCTO"
        Me.teTitulo.Location = New System.Drawing.Point(101, 122)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(338, 20)
        Me.teTitulo.TabIndex = 32
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(65, 125)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(30, 13)
        Me.LabelControl5.TabIndex = 33
        Me.LabelControl5.Text = "Título:"
        '
        'inv_frmInformeGirodelProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(679, 198)
        Me.Name = "inv_frmInformeGirodelProducto"
        Me.Text = "Informe de Giro del Producto"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.spAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BeProducto1 As beProducto
    Friend WithEvents spAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
End Class
