﻿
Imports NexusBLL
Public Class inv_frmInformeCostos
    Dim bl As New InventarioBLL(g_ConnectionString)
    
    Private Sub inv_frmInformeCostos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        deDesde.EditValue = Today
        deHasta.EditValue = Today
    End Sub

    Private Sub inv_frmInformeCostos_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable

        dt = bl.inv_UtilidadesVentas(deDesde.EditValue, deHasta.EditValue, leBodega.EditValue)
        Dim rpt As New inv_rptUtilidadesVentas() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.XrLBodega.Text = "BODEGA: " + leBodega.Text
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.xrlTitulo.Text = "INFORME DE COSTOS"
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)

        rpt.ShowPreviewDialog()
    End Sub
End Class
