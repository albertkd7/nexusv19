Public Class inv_rptKardexValuado
    Dim Corre As Integer = 1
    Dim SaldoProducto As Decimal = 0.0
    Dim SaldoTotal As Decimal = 0.0
    Dim SaldoExistencias As Decimal = 0.0
    Dim SaldoCostoProducto As Decimal = 0.0

    Private Sub xrlCorrel_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlCorrel.BeforePrint
        If GetCurrentColumnValue("Documento") <> "Saldo anterior -->       " Then
            xrlCorrel.Text = Corre.ToString
            Corre += 1
        Else
            xrlCorrel.Text = ""
        End If
    End Sub


    Private Sub GroupHeader1_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles GroupHeader1.BeforePrint
        If GetCurrentColumnValue("Documento") = "Saldo anterior -->       " Then
            SaldoProducto = GetCurrentColumnValue("SaldoUnidades")
        End If
    End Sub

    Private Sub XrLabel18_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel18.BeforePrint
        XrLabel18.Text = Format(SaldoProducto, "##,###,##0.00")
        'XrLabel7.Text = Format((SaldoProducto * GetCurrentColumnValue("CostoPromedio")), "##,###,##0.00")
    End Sub

    Private Sub Detail_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Detail.BeforePrint
        If GetCurrentColumnValue("TipoAplicacion") = 1 Then
            SaldoProducto += GetCurrentColumnValue("EntradasUnidades")
        Else
            SaldoProducto -= GetCurrentColumnValue("SalidasUnidades")
        End If
    End Sub

    Private Sub XrLabel7_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel7.BeforePrint
        If GetCurrentColumnValue("Documento") <> "Saldo anterior -->       " Then
            XrLabel7.Text = Format((SaldoProducto * GetCurrentColumnValue("CostoPromedio")), "##,###,##0.00")
        Else
            XrLabel7.Text = Format(GetCurrentColumnValue("SaldoValor"), "##,###,##0.00")
        End If
    End Sub

    Private Sub XrLabel10_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrLabel10.BeforePrint
        XrLabel10.Text = Format((SaldoExistencias), "##,###,##0.00")
    End Sub

    Private Sub XrLabel26_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrLabel26.BeforePrint
        SaldoExistencias = SaldoExistencias + SaldoProducto
        SaldoTotal = SaldoTotal + (SaldoProducto * GetCurrentColumnValue("CostoPromedio"))
    End Sub

    Private Sub XrLabel9_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrLabel9.BeforePrint
        XrLabel9.Text = Format(SaldoTotal, "##,###,##0.00")
    End Sub
End Class