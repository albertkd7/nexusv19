Public Class inv_rptInventarioComparativoTomaFisica
    Dim Diferencia As Decimal = 0.0
    Dim PrecioCosto As Decimal = 0.0
    Dim CostoTotal As Decimal = 0.0

    Private Sub XrLabel14_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel14.BeforePrint
        Diferencia = SiEsNulo(GetCurrentColumnValue("Diferencia"), 0)
        PrecioCosto = SiEsNulo(GetCurrentColumnValue("PrecioCosto"), 0)


        If Diferencia > 0 Then
            PrecioCosto = Decimal.Round(Diferencia * PrecioCosto, 2)
            XrLabel14.Text = Format(PrecioCosto, "###,###,##0.00")
            CostoTotal += PrecioCosto
        Else
            'PrecioCosto = Decimal.Round((Diferencia * -1) * PrecioCosto, 2)
            PrecioCosto = Decimal.Round(Diferencia * PrecioCosto, 2)
            XrLabel14.Text = Format(PrecioCosto, "###,###,##0.00")
            CostoTotal += PrecioCosto
        End If

    End Sub

    Private Sub XrLabel16_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel16.BeforePrint
        XrLabel16.Text = Format(CostoTotal, "###,###,##0.00")
    End Sub
End Class