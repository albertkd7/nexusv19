﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmVineta
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdProducto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCodigo = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ritePrecio = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.riteCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.riteMarca = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.riteUnidad = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.teLineas = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.seCantidad = New DevExpress.XtraEditors.SpinEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.ceFormato = New DevExpress.XtraEditors.CheckEdit()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteMarca, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteUnidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teLineas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceFormato.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.ceFormato)
        Me.GroupControl1.Controls.Add(Me.SimpleButton1)
        Me.GroupControl1.Controls.Add(Me.LabelControl26)
        Me.GroupControl1.Controls.Add(Me.seCantidad)
        Me.GroupControl1.Controls.Add(Me.teLineas)
        Me.GroupControl1.Controls.Add(Me.LabelControl28)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Size = New System.Drawing.Size(918, 69)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Parámetros de la consulta"
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdUp)
        Me.pcBotones.Controls.Add(Me.cmdDown)
        Me.pcBotones.Controls.Add(Me.cmdBorrar)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(877, 69)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(41, 297)
        Me.pcBotones.TabIndex = 10
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(7, 8)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(27, 24)
        Me.cmdUp.TabIndex = 0
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(7, 43)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(27, 24)
        Me.cmdDown.TabIndex = 1
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(7, 78)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 24)
        Me.cmdBorrar.TabIndex = 2
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 69)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteCantidad, Me.ritePrecio, Me.riteCodigo, Me.riteMarca, Me.riteUnidad, Me.RepositoryItemDateEdit1})
        Me.gc.Size = New System.Drawing.Size(877, 297)
        Me.gc.TabIndex = 11
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdProducto, Me.gcDescripcion, Me.gcPrecioUnitario, Me.GridColumn1})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdProducto
        '
        Me.gcIdProducto.Caption = "Id. Producto"
        Me.gcIdProducto.ColumnEdit = Me.riteCodigo
        Me.gcIdProducto.FieldName = "IdProducto"
        Me.gcIdProducto.Name = "gcIdProducto"
        Me.gcIdProducto.Visible = True
        Me.gcIdProducto.VisibleIndex = 0
        Me.gcIdProducto.Width = 91
        '
        'riteCodigo
        '
        Me.riteCodigo.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCodigo.AutoHeight = False
        Me.riteCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.riteCodigo.Name = "riteCodigo"
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Concepto/ Descripción del artículo"
        Me.gcDescripcion.FieldName = "Descripcion"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.OptionsColumn.AllowEdit = False
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 2
        Me.gcDescripcion.Width = 184
        '
        'gcPrecioUnitario
        '
        Me.gcPrecioUnitario.Caption = "Precio Unitario"
        Me.gcPrecioUnitario.ColumnEdit = Me.ritePrecio
        Me.gcPrecioUnitario.FieldName = "PrecioUnitario"
        Me.gcPrecioUnitario.Name = "gcPrecioUnitario"
        Me.gcPrecioUnitario.Visible = True
        Me.gcPrecioUnitario.VisibleIndex = 3
        Me.gcPrecioUnitario.Width = 112
        '
        'ritePrecio
        '
        Me.ritePrecio.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.ritePrecio.AutoHeight = False
        Me.ritePrecio.Mask.EditMask = "n4"
        Me.ritePrecio.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePrecio.Mask.UseMaskAsDisplayFormat = True
        Me.ritePrecio.Name = "ritePrecio"
        '
        'riteCantidad
        '
        Me.riteCantidad.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCantidad.Appearance.Options.UseTextOptions = True
        Me.riteCantidad.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.riteCantidad.AutoHeight = False
        Me.riteCantidad.EditFormat.FormatString = "n2"
        Me.riteCantidad.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteCantidad.Mask.EditMask = "n2"
        Me.riteCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.riteCantidad.Name = "riteCantidad"
        '
        'riteMarca
        '
        Me.riteMarca.AutoHeight = False
        Me.riteMarca.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteMarca.Name = "riteMarca"
        '
        'riteUnidad
        '
        Me.riteUnidad.AutoHeight = False
        Me.riteUnidad.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteUnidad.Name = "riteUnidad"
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'teLineas
        '
        Me.teLineas.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teLineas.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.teLineas.Enabled = False
        Me.teLineas.Location = New System.Drawing.Point(816, 31)
        Me.teLineas.Name = "teLineas"
        Me.teLineas.Properties.AllowFocused = False
        Me.teLineas.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.teLineas.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.teLineas.Properties.Appearance.Options.UseFont = True
        Me.teLineas.Properties.Appearance.Options.UseForeColor = True
        Me.teLineas.Properties.Appearance.Options.UseTextOptions = True
        Me.teLineas.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teLineas.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teLineas.Properties.ReadOnly = True
        Me.teLineas.Size = New System.Drawing.Size(61, 26)
        Me.teLineas.TabIndex = 95
        '
        'LabelControl28
        '
        Me.LabelControl28.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl28.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl28.Appearance.ForeColor = System.Drawing.Color.Black
        Me.LabelControl28.Appearance.Options.UseFont = True
        Me.LabelControl28.Appearance.Options.UseForeColor = True
        Me.LabelControl28.Location = New System.Drawing.Point(722, 34)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(85, 19)
        Me.LabelControl28.TabIndex = 94
        Me.LabelControl28.Text = "No. Items:"
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl26.Appearance.Options.UseFont = True
        Me.LabelControl26.Location = New System.Drawing.Point(24, 26)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl26.TabIndex = 99
        Me.LabelControl26.Text = "Cantidad:"
        '
        'seCantidad
        '
        Me.seCantidad.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seCantidad.EnterMoveNextControl = True
        Me.seCantidad.Location = New System.Drawing.Point(24, 45)
        Me.seCantidad.Name = "seCantidad"
        Me.seCantidad.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seCantidad.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
        Me.seCantidad.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seCantidad.Size = New System.Drawing.Size(68, 20)
        Me.seCantidad.TabIndex = 98
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Image = Global.Nexus.My.Resources.Resources.Printer
        Me.SimpleButton1.Location = New System.Drawing.Point(622, 26)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(67, 32)
        Me.SimpleButton1.TabIndex = 100
        '
        'ceFormato
        '
        Me.ceFormato.Location = New System.Drawing.Point(129, 45)
        Me.ceFormato.Name = "ceFormato"
        Me.ceFormato.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ceFormato.Properties.Appearance.Options.UseFont = True
        Me.ceFormato.Properties.Caption = "2 Formato"
        Me.ceFormato.Size = New System.Drawing.Size(99, 20)
        Me.ceFormato.TabIndex = 101
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "SKU"
        Me.GridColumn1.FieldName = "CreadoPor"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        '
        'inv_frmVineta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(918, 391)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.pcBotones)
        Me.Modulo = "Inventarios"
        Me.Name = "inv_frmVineta"
        Me.Text = "Impresión de Viñetas por Entradas"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.pcBotones, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteMarca, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteUnidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teLineas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceFormato.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout

End Sub
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCodigo As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents riteCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ritePrecio As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents riteMarca As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents riteUnidad As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents teLineas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seCantidad As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ceFormato As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn

End Class
