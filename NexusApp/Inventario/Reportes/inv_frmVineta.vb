﻿Imports DevExpress.XtraReports.UI
Imports NexusBLL
Imports NexusELL.TableEntities
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Columns
Public Class inv_frmVineta
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim blF As New FacturaBLL(g_ConnectionString)
    Dim fd As New FuncionesBLL(g_ConnectionString)
    Dim Detalle As List(Of inv_Vinetas)
    Dim entProducto As inv_Productos
    Private Sub fac_frmPreFacturacion_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        objCombos.invUnidadesMedida(riteUnidad)
        objCombos.invMarcas(riteMarca)
        gc.DataSource = bl.inv_ObtenerDetalleDocumento(-1, "inv_Vinetas")
        gv.CancelUpdateCurrentRow()
        gv.AddNewRow()
        teLineas.EditValue = 0
        seCantidad.EditValue = 1
    End Sub


    Private Sub fac_frmPreFacturacion_Reporte() Handles Me.Reporte
        If gv.RowCount <= 0 Then
            MsgBox("Debe especificar los productos a generar", MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If
        LlenarEntidad()


        bl.inv_ActualizarVineta(Detalle)

        Dim dt As DataTable = bl.inv_ObtenerProductosEtiquetas()
        If ceFormato.Checked = True Then
            LlenarEntidad()
            Dim rpt As New inv_rptVinietas2() With {.DataSource = dt, .DataMember = ""}
            rpt.ShowPrintMarginsWarning = False
            rpt.ShowPreviewDialog()
        Else
            LlenarEntidad()
            Dim rpt As New inv_rptVinietas() With {.DataSource = dt, .DataMember = ""}
            rpt.ShowPrintMarginsWarning = False
            rpt.ShowPreviewDialog()
        End If
    End Sub
    
#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteRow(gv.FocusedRowHandle)
        CalcularTotales()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdMarca", -1)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdUnidad", 0)
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), "") = "" Then
            e.Valid = False
        End If
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Dim Cantidad As Decimal = fd.EsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad"), 0)
        Dim PrecioUnitario As Decimal = fd.EsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario"), 0)

        Select Case gv.FocusedColumn.FieldName
            Case "Cantidad"
                Cantidad = e.Value

            Case "PrecioUnitario"
                PrecioUnitario = e.Value
        End Select

        Dim Total As Decimal = Decimal.Round(Cantidad * PrecioUnitario, 2)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", Total)
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gv.EditingValue = IdProd
                End If
                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                If entProducto.IdProducto = "" Then
                    entProducto = objTablas.inv_ProductosSelectByPK(blF.inv_ObtieneProductoAlias(gv.EditingValue))
                    gv.EditingValue = entProducto.IdProducto
                End If

                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
                Else

                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                    gv.SetFocusedRowCellValue("IdUnidad", entProducto.IdUnidad)
                    gv.SetFocusedRowCellValue("IdMarca", entProducto.IdMarca)

                End If
            End If
            If gv.FocusedColumn.FieldName = "IdUnidad" Then
                Dim Producto As String = gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto")
                Dim Uni As Integer
                If SiEsNulo(gv.EditingValue, 0) = 0 Then
                    '    Dim IdUnidad = objConsultas.ConsultaProductosPreciosUnidad(inv_frmConsultaUnidadesPrecio, Producto, 1, LePuntoVenta.EditValue, 1, "")
                    '   inv_frmConsultaUnidadesPrecio.Dispose()
                    '   Uni = IdUnidad
                    gv.SetFocusedRowCellValue("IdUnidad", Uni)
                End If
                Dim ID As Integer
                ID = gv.GetFocusedRowCellValue("IdUnidad")
                Dim Medida As String
                '  Medida = fd.ObtenerNombreMedida(ID)
                gv.SetFocusedRowCellValue("MedidaNombre", Medida)

                Dim IDPro As String = gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto")
                '   gv.SetFocusedRowCellValue("PrecioUnitario", bl.inv_ObtienePrecioProducto(SiEsNulo(IDPro, ""), gv.EditingValue, LePuntoVenta.EditValue))
            End If
        End If

    End Sub
#End Region
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        CalcularTotales()
    End Sub
    Private Sub CalcularTotales()
        teLineas.EditValue = 0
        teLineas.EditValue = gv.RowCount()
    End Sub
    Private Sub LlenarEntidad()
        Detalle = New List(Of inv_Vinetas)
        For C = 0 To seCantidad.EditValue - 1
            For i = 0 To gv.DataRowCount - 1
                Dim entDetalle As New inv_Vinetas
                With entDetalle
                    .IdComprobante = C
                    .IdDetalle = i + 1
                    .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                    .Cantidad = 1 'SiEsNulo(gv.GetRowCellValue(i, "Cantidad"), 0)
                    .Descripcion = SiEsNulo(gv.GetRowCellValue(i, "Descripcion"), 0)
                    .PrecioUnitario = SiEsNulo(gv.GetRowCellValue(i, "PrecioUnitario"), 0)
                    .IdUnidad = 1 'SiEsNulo(gv.GetRowCellValue(i, "IdUnidad"), 0)
                    .IdMarca = 1 'SiEsNulo(gv.GetRowCellValue(i, "IdMarca"), 0)
                    .CreadoPor = SiEsNulo(gv.GetRowCellValue(i, "CreadoPor"), "")
                    .FechaHoraCreacion = Now 'SiEsNulo(gv.GetRowCellValue(i, "FechaHoraCreacion"), Nothing)
                End With
                Detalle.Add(entDetalle)

            Next
        Next
    End Sub

    Private Sub seCantidad_EditValueChanged(sender As Object, e As EventArgs) Handles seCantidad.EditValueChanged
        If seCantidad.EditValue = 0 Then
            MsgBox("No se Generara Ninguna Viñeta", MsgBoxStyle.Critical, "Error De Usuario")
        End If
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        If gv.RowCount <= 0 Then
            MsgBox("Debe especificar los productos a generar", MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If
        LlenarEntidad()
        bl.inv_ActualizarVineta(Detalle)
        Dim dt As DataTable = bl.inv_ObtenerProductosEtiquetas()
        If ceFormato.Checked = True Then
            LlenarEntidad()
            Dim rpt As New inv_rptVinietas2() With {.DataSource = dt, .DataMember = ""}
            rpt.ShowPrintMarginsWarning = False
            rpt.ShowPreviewDialog()
        Else
            LlenarEntidad()
            Dim rpt As New inv_rptVinietas() With {.DataSource = dt, .DataMember = ""}
            rpt.ShowPrintMarginsWarning = False
            rpt.ShowPreviewDialog()
        End If
    End Sub


End Class
