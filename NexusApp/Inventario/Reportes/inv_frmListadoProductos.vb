﻿
Imports NexusBLL
Public Class inv_frmListadoProductos
    Dim bl As New InventarioBLL(g_ConnectionString)

    
    Private Sub inv_frmListadoProductos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
        objCombos.invSubGrupos(leSubGrupo, -1, "-- TODOS LOS SUB-GRUPOS --")
    End Sub

    Private Sub inv_frmListadoProductos_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = bl.inv_ListaDeProductos(leGrupo.EditValue, leSubGrupo.EditValue)
        Dim rpt As New inv_rptProductos() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue & " (PRECIOS INCLUYEN IVA)"
        rpt.ShowPreview()
    End Sub

    Private Sub leGrupo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leGrupo.EditValueChanged
        objCombos.invSubGrupos(leSubGrupo, leGrupo.EditValue)
    End Sub
End Class
