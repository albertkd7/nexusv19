﻿
Imports NexusBLL
Public Class inv_frmListadoProductosCategorias
    Dim bl As New InventarioBLL(g_ConnectionString)


    Private Sub inv_frmListadoProductosCategorias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.invCategoriaProducto(leCategoria, "-- TODAS LAS CATEGORIAS --")
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
        objCombos.invSubGrupos(leSubGrupo, -1, "-- TODOS LOS SUB-GRUPOS --")
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
    End Sub

    Private Sub inv_frmListadoProductosCategorias_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = bl.inv_ListaDeProductosCategorias(leCategoria.EditValue, leGrupo.EditValue, leSubGrupo.EditValue, leBodega.EditValue)
        Dim rpt As New inv_rptProductosCategoria() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.ShowPreview()
    End Sub

    Private Sub leGrupo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leGrupo.EditValueChanged
        objCombos.invSubGrupos(leSubGrupo, leGrupo.EditValue)
    End Sub
End Class
