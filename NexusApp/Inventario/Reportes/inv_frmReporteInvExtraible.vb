﻿Imports NexusBLL
Imports DevExpress.XtraGrid.Columns

Imports DevExpress.XtraGrid.Menu
Imports DevExpress.Utils.Menu
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraSplashScreen

Public Class inv_frmReporteInvExtraible
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim dt As New DataTable

    Public Sub New()
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        AddHandler gv.PopupMenuShowing, AddressOf GridPopupMenuShowing
    End Sub

    Private Sub inv_frmInventarioValuado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
        objCombos.invSubGrupos(leSubGrupo, -1, "-- TODOS LOS SUB-GRUPOS --")
        deHasta.EditValue = Today
    End Sub

    Private Sub sbtnGenerar_Click(sender As Object, e As EventArgs) Handles sbtnGenerar.Click
        SplashScreenManager.ShowForm(Me, WaitForm.GetType(), True, True, False)
        SplashScreenManager.Default.SetWaitFormCaption("Procesando")
        SplashScreenManager.Default.SetWaitFormDescription("Consultado Datos...")

        gc.DataMember = ""
        gc.DataSource = bl.inv_ConsultaExportableExistenciasYprecios(leGrupo.EditValue, leSubGrupo.EditValue, deHasta.EditValue)
        gv.BestFitColumns()

        SplashScreenManager.Default.SetWaitFormCaption("Completado!")
        SplashScreenManager.Default.SetWaitFormDescription("Datos obtenidos con exito")
        SplashScreenManager.CloseForm()
    End Sub

#Region "StripOptions/XtraGrid"

    Private Sub GridPopupMenuShowing(sender As Object, e As PopupMenuShowingEventArgs)
        If e.MenuType = GridMenuType.Column Then
            Dim menu As GridViewColumnMenu = TryCast(e.Menu, GridViewColumnMenu)
            menu.Items.Clear()
            If menu.Column IsNot Nothing Then
                Dim item1 As New DXMenuItem("Exportar a CSV", New EventHandler(AddressOf OnExportOptionCSV_Click), Global.Nexus.My.Resources.Resources.fvcsv)
                item1.Tag = menu.Column
                menu.Items.Add(item1)

                Dim item2 As New DXMenuItem("Exportar a Excel", New EventHandler(AddressOf OnExportOptionExcel_Click), Global.Nexus.My.Resources.Resources.fvexcel)
                item2.Tag = menu.Column
                menu.Items.Add(item2)
            End If
        End If
    End Sub

    ' Menu item click handler - columns Header. 
    Private Sub OnExportOptionCSV_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim Sfd As New SaveFileDialog()
        Sfd.Filter = "CSV Files (*.csv)|*.csv"
        Sfd.Title = "Por favor indique ubicacion para guardar exportacion..."
        Sfd.FileName = "Datos-Inv-Export"

        If Sfd.ShowDialog() = DialogResult.OK Then
            gc.ExportToCsv(Sfd.FileName)
        End If
    End Sub

    Private Sub OnExportOptionExcel_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim Sfd As New SaveFileDialog()
        Sfd.Filter = "Excel Files (*.xlsx)|*.xlsx"
        Sfd.Title = "Por favor indique ubicacion para guardar exportacion..."
        Sfd.FileName = "Datos-Inv-Export"

        If Sfd.ShowDialog() = DialogResult.OK Then
            gc.ExportToXlsx(Sfd.FileName)
        End If
    End Sub

    Private Sub leGrupo_EditValueChanged(sender As Object, e As EventArgs) Handles leGrupo.EditValueChanged
        objCombos.invSubGrupos(leSubGrupo, leGrupo.EditValue, "-- TODOS LOS SUB-GRUPOS --")
    End Sub
#End Region
End Class
