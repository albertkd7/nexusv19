﻿
Imports NexusBLL
Public Class inv_frmKardex
    ReadOnly bl As New InventarioBLL(g_ConnectionString)
    ReadOnly blAdmon As New AdmonBLL(g_ConnectionString)
    ReadOnly entProducto As New NexusELL.TableEntities.inv_Productos()
    
    Private Sub frmKardex_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
        objCombos.invSubGrupos(leSubGrupo, -1, "-- TODOS LOS SUB-GRUPOS --")

        deDesde.EditValue = Today
        deHasta.EditValue = Today
    End Sub

    Private Sub inv_frmKardexValuado_Reporte() Handles Me.Reporte
        Dim dt As DataTable = bl.inv_ReporteKardex(deDesde.EditValue, deHasta.EditValue, BeProducto1.beCodigo.EditValue, leBodega.EditValue, leGrupo.EditValue, leSubGrupo.EditValue)
        Dim dtp As DataTable = blAdmon.ObtieneParametros()
        Dim rpt As New inv_rptKardexValuado() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue).ToUpper
        rpt.xrlNit.Text = "NIT: " + dtp.Rows(0).Item("NitEmpresa")
        rpt.xrlNRC.Text = "NRC: " + dtp.Rows(0).Item("NrcEmpresa")
        rpt.xrlBodega.Text = "BODEGA: " + leBodega.Text
        rpt.ShowPreview()
    End Sub

    Private Sub leGrupo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leGrupo.EditValueChanged
        objCombos.invSubGrupos(leSubGrupo, leGrupo.EditValue, "-- TODOS LOS SUB-GRUPOS --")
        If leGrupo.EditValue = "-1" Then
            BeProducto1.Visible = True
        Else
            BeProducto1.Visible = False
        End If
    End Sub
End Class