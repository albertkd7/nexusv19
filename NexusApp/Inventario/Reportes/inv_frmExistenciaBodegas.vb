﻿Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraSplashScreen
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class inv_frmExistenciaBodegas
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim blInventa As New InventarioBLL(g_ConnectionString)
    Dim Prod As DataTable, ProdVista As DataTable
    Dim dt As New DataTable


    Private Sub fac_frmConsultaProductosBase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deHasta.EditValue = Today
        CargaCombos()
        gvProd1.Columns.Clear()
    End Sub

    Private Sub CargaCombos()
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
        objCombos.invSubGrupos(leSubGrupo, -1, "-- TODOS LOS SUB-GRUPOS --")
    End Sub


    Private Sub btGenerar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGenerar.Click

        SplashScreenManager.ShowForm(Me, WaitForm.GetType(), True, True, False)
        SplashScreenManager.Default.SetWaitFormCaption("Procesando")
        SplashScreenManager.Default.SetWaitFormDescription("Consultado Existencias...")

        Dim dt As DataTable
        dt = bl.inv_ExistenciasBodegasProductos(deHasta.EditValue, leGrupo.EditValue, leSubGrupo.EditValue)
        gvProd1.Columns.Clear()
        gcProductos.DataSource = dt
        ' DE QUI HACIA ABAJO FUNCIONA PARA GENERAR LOS TOTALES EN EL GRID DE MANERA DINAMICA
        Dim Colum As String, Columnas As Integer = dt.Columns.Count()
        For i = 0 To Columnas - 1
            Colum = gvProd1.Columns(i).FieldName()
            gvProd1.Columns(i).Summary.Add(DevExpress.Data.SummaryItemType.Sum, Colum)
        Next
        ProdVista = gcProductos.DataSource

        SplashScreenManager.Default.SetWaitFormCaption("Completado!")
        SplashScreenManager.Default.SetWaitFormDescription("Existencias obtenidas con exito")
        SplashScreenManager.CloseForm()
    End Sub

    Private Sub btExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btExportar.Click
        Dim NombreArchivo As String = ""
        NombreArchivo = ObtieneNombreArchivo()
        gcProductos.ExportToXls(NombreArchivo)

        MsgBox("El documento ha sido exportado con éxito en formato Excel", 64, "Nota")

    End Sub
    Private Function ObtieneNombreArchivo() As String
        Dim NombreArchivo As String = "Existencias Bodegas " & Today.Year.ToString.PadLeft(4, "0") & Today.Month.ToString.PadLeft(2, "0") & Today.Day.ToString.PadLeft(2, "0")
        NombreArchivo = InputBox("Nombre del archivo", "Defina el nombre del archivo", NombreArchivo)

        Dim myFolderBrowserDialog As New FolderBrowserDialog

        With myFolderBrowserDialog

            .RootFolder = Environment.SpecialFolder.Desktop
            .SelectedPath = "c:\"
            .Description = "Seleccione la carpeta destino"
            If .ShowDialog = DialogResult.OK Then
                NombreArchivo = .SelectedPath & "\" & NombreArchivo & ".xls"
            End If
        End With
        Return NombreArchivo
    End Function

    Private Sub leGrupo_EditValueChanged(sender As Object, e As EventArgs) Handles leGrupo.EditValueChanged
        objCombos.invSubGrupos(leSubGrupo, leGrupo.EditValue, "-- TODOS LOS SUBGRUPOS--")
    End Sub
End Class
