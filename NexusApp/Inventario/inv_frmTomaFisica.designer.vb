﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inv_frmTomaFisica
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaAplicacion = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.leGrupo = New DevExpress.XtraEditors.LookUpEdit()
        Me.btActualizar = New DevExpress.XtraEditors.SimpleButton()
        Me.btAplicar = New DevExpress.XtraEditors.SimpleButton()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdProducto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcUnidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcConteo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcAcumulado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcExistencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDiferencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPrecioCosto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcGrupo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcSubGrupo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.leSubGrupo = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.leBodega = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoSalida = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoEntrada = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.btListado = New DevExpress.XtraEditors.SimpleButton()
        Me.btComparativo = New DevExpress.XtraEditors.SimpleButton()
        Me.btCosteo = New DevExpress.XtraEditors.SimpleButton()
        Me.sbImportar = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaAplicacion.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaAplicacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSubGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoSalida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoEntrada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.sbImportar)
        Me.GroupControl1.Controls.Add(Me.btCosteo)
        Me.GroupControl1.Controls.Add(Me.btComparativo)
        Me.GroupControl1.Controls.Add(Me.btListado)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.teNumero)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.leTipoEntrada)
        Me.GroupControl1.Controls.Add(Me.LabelControl17)
        Me.GroupControl1.Controls.Add(Me.leTipoSalida)
        Me.GroupControl1.Controls.Add(Me.LabelControl14)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.leBodega)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.leSubGrupo)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.btAplicar)
        Me.GroupControl1.Controls.Add(Me.btActualizar)
        Me.GroupControl1.Controls.Add(Me.leGrupo)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.deFechaAplicacion)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.deFecha)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Size = New System.Drawing.Size(959, 143)
        Me.GroupControl1.Text = "Toma Fisica"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(33, 101)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Fecha:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.Location = New System.Drawing.Point(69, 98)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(622, 122)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Fecha Aplicación:"
        '
        'deFechaAplicacion
        '
        Me.deFechaAplicacion.EditValue = Nothing
        Me.deFechaAplicacion.Location = New System.Drawing.Point(708, 119)
        Me.deFechaAplicacion.Name = "deFechaAplicacion"
        Me.deFechaAplicacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaAplicacion.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaAplicacion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaAplicacion.Size = New System.Drawing.Size(100, 20)
        Me.deFechaAplicacion.TabIndex = 5
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(33, 30)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Grupo:"
        '
        'leGrupo
        '
        Me.leGrupo.Location = New System.Drawing.Point(69, 28)
        Me.leGrupo.Name = "leGrupo"
        Me.leGrupo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leGrupo.Size = New System.Drawing.Size(246, 20)
        Me.leGrupo.TabIndex = 0
        '
        'btActualizar
        '
        Me.btActualizar.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.btActualizar.Location = New System.Drawing.Point(175, 97)
        Me.btActualizar.Name = "btActualizar"
        Me.btActualizar.Size = New System.Drawing.Size(139, 23)
        Me.btActualizar.TabIndex = 4
        Me.btActualizar.Text = "Actualizar Existencia"
        '
        'btAplicar
        '
        Me.btAplicar.Image = Global.Nexus.My.Resources.Resources.compra___copia
        Me.btAplicar.Location = New System.Drawing.Point(824, 95)
        Me.btAplicar.Name = "btAplicar"
        Me.btAplicar.Size = New System.Drawing.Size(127, 47)
        Me.btAplicar.TabIndex = 10
        Me.btAplicar.Text = "Aplicar"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 143)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(959, 272)
        Me.gc.TabIndex = 6
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv, Me.GridView2})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdProducto, Me.gcDescripcion, Me.gcUnidad, Me.gcConteo, Me.gcAcumulado, Me.gcExistencia, Me.gcDiferencia, Me.gcPrecioCosto, Me.gcGrupo, Me.gcSubGrupo})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowAutoFilterRow = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdProducto
        '
        Me.gcIdProducto.Caption = "Codigo"
        Me.gcIdProducto.FieldName = "IdProducto"
        Me.gcIdProducto.Name = "gcIdProducto"
        Me.gcIdProducto.OptionsColumn.ReadOnly = True
        Me.gcIdProducto.Visible = True
        Me.gcIdProducto.VisibleIndex = 0
        Me.gcIdProducto.Width = 91
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Descripcion"
        Me.gcDescripcion.FieldName = "Nombre"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.OptionsColumn.ReadOnly = True
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 1
        Me.gcDescripcion.Width = 293
        '
        'gcUnidad
        '
        Me.gcUnidad.Caption = "Unidad M."
        Me.gcUnidad.FieldName = "UnidadMedida"
        Me.gcUnidad.Name = "gcUnidad"
        Me.gcUnidad.OptionsColumn.ReadOnly = True
        Me.gcUnidad.Visible = True
        Me.gcUnidad.VisibleIndex = 2
        Me.gcUnidad.Width = 79
        '
        'gcConteo
        '
        Me.gcConteo.Caption = "Conteo"
        Me.gcConteo.FieldName = "Conteo"
        Me.gcConteo.Name = "gcConteo"
        Me.gcConteo.Visible = True
        Me.gcConteo.VisibleIndex = 4
        Me.gcConteo.Width = 73
        '
        'gcAcumulado
        '
        Me.gcAcumulado.Caption = "Acumulado"
        Me.gcAcumulado.FieldName = "Acumulado"
        Me.gcAcumulado.Name = "gcAcumulado"
        Me.gcAcumulado.OptionsColumn.ReadOnly = True
        Me.gcAcumulado.Visible = True
        Me.gcAcumulado.VisibleIndex = 5
        Me.gcAcumulado.Width = 72
        '
        'gcExistencia
        '
        Me.gcExistencia.Caption = "Existencia"
        Me.gcExistencia.FieldName = "Existencias"
        Me.gcExistencia.Name = "gcExistencia"
        Me.gcExistencia.OptionsColumn.ReadOnly = True
        Me.gcExistencia.Visible = True
        Me.gcExistencia.VisibleIndex = 6
        Me.gcExistencia.Width = 70
        '
        'gcDiferencia
        '
        Me.gcDiferencia.Caption = "Diferencia"
        Me.gcDiferencia.FieldName = "Diferencia"
        Me.gcDiferencia.Name = "gcDiferencia"
        Me.gcDiferencia.OptionsColumn.ReadOnly = True
        Me.gcDiferencia.Visible = True
        Me.gcDiferencia.VisibleIndex = 7
        Me.gcDiferencia.Width = 85
        '
        'gcPrecioCosto
        '
        Me.gcPrecioCosto.Caption = "Precio Costo"
        Me.gcPrecioCosto.FieldName = "PrecioCosto"
        Me.gcPrecioCosto.Name = "gcPrecioCosto"
        Me.gcPrecioCosto.Visible = True
        Me.gcPrecioCosto.VisibleIndex = 3
        Me.gcPrecioCosto.Width = 91
        '
        'gcGrupo
        '
        Me.gcGrupo.Caption = "Grupo"
        Me.gcGrupo.FieldName = "Grupo"
        Me.gcGrupo.Name = "gcGrupo"
        '
        'gcSubGrupo
        '
        Me.gcSubGrupo.Caption = "SubGrupo"
        Me.gcSubGrupo.FieldName = "SubGrupo"
        Me.gcSubGrupo.Name = "gcSubGrupo"
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.gc
        Me.GridView2.Name = "GridView2"
        '
        'leSubGrupo
        '
        Me.leSubGrupo.Location = New System.Drawing.Point(69, 52)
        Me.leSubGrupo.Name = "leSubGrupo"
        Me.leSubGrupo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSubGrupo.Size = New System.Drawing.Size(246, 20)
        Me.leSubGrupo.TabIndex = 1
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(11, 55)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl4.TabIndex = 5
        Me.LabelControl4.Text = "Sub-Grupo:"
        '
        'leBodega
        '
        Me.leBodega.Location = New System.Drawing.Point(69, 74)
        Me.leBodega.Name = "leBodega"
        Me.leBodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leBodega.Size = New System.Drawing.Size(246, 20)
        Me.leBodega.TabIndex = 3
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(26, 77)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl5.TabIndex = 7
        Me.LabelControl5.Text = "Bodega:"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(661, 29)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl14.TabIndex = 75
        Me.LabelControl14.Text = "Sucursal:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(708, 26)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(244, 20)
        Me.leSucursal.TabIndex = 6
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(635, 54)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(70, 13)
        Me.LabelControl17.TabIndex = 77
        Me.LabelControl17.Text = "Tipo de Salida:"
        '
        'leTipoSalida
        '
        Me.leTipoSalida.EnterMoveNextControl = True
        Me.leTipoSalida.Location = New System.Drawing.Point(708, 51)
        Me.leTipoSalida.Name = "leTipoSalida"
        Me.leTipoSalida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoSalida.Size = New System.Drawing.Size(244, 20)
        Me.leTipoSalida.TabIndex = 7
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(625, 77)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(80, 13)
        Me.LabelControl6.TabIndex = 79
        Me.LabelControl6.Text = "Tipo de Entrada:"
        '
        'leTipoEntrada
        '
        Me.leTipoEntrada.EnterMoveNextControl = True
        Me.leTipoEntrada.Location = New System.Drawing.Point(708, 74)
        Me.leTipoEntrada.Name = "leTipoEntrada"
        Me.leTipoEntrada.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoEntrada.Size = New System.Drawing.Size(244, 20)
        Me.leTipoEntrada.TabIndex = 8
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(601, 100)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(104, 13)
        Me.LabelControl7.TabIndex = 81
        Me.LabelControl7.Text = "No. de Comprobante:"
        '
        'teNumero
        '
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(708, 96)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(101, 20)
        Me.teNumero.TabIndex = 9
        '
        'btListado
        '
        Me.btListado.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.btListado.Location = New System.Drawing.Point(321, 25)
        Me.btListado.Name = "btListado"
        Me.btListado.Size = New System.Drawing.Size(139, 23)
        Me.btListado.TabIndex = 82
        Me.btListado.Text = "Listado"
        '
        'btComparativo
        '
        Me.btComparativo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.btComparativo.Location = New System.Drawing.Point(321, 51)
        Me.btComparativo.Name = "btComparativo"
        Me.btComparativo.Size = New System.Drawing.Size(139, 23)
        Me.btComparativo.TabIndex = 83
        Me.btComparativo.Text = "Listado Comparativo"
        '
        'btCosteo
        '
        Me.btCosteo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.btCosteo.Location = New System.Drawing.Point(321, 76)
        Me.btCosteo.Name = "btCosteo"
        Me.btCosteo.Size = New System.Drawing.Size(139, 23)
        Me.btCosteo.TabIndex = 84
        Me.btCosteo.Text = "Listado Costeo"
        '
        'sbImportar
        '
        Me.sbImportar.Image = Global.Nexus.My.Resources.Resources.bajar
        Me.sbImportar.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.sbImportar.Location = New System.Drawing.Point(466, 25)
        Me.sbImportar.Name = "sbImportar"
        Me.sbImportar.Size = New System.Drawing.Size(119, 23)
        Me.sbImportar.TabIndex = 86
        Me.sbImportar.Text = "Importar Archivo"
        '
        'inv_frmTomaFisica
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(959, 440)
        Me.Controls.Add(Me.gc)
        Me.Modulo = "Inventario"
        Me.Name = "inv_frmTomaFisica"
        Me.Text = "Toma Fisica"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaAplicacion.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaAplicacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSubGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoSalida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoEntrada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btAplicar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btActualizar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents leGrupo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaAplicacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents leSubGrupo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leBodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcIdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcUnidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcConteo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcAcumulado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcExistencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDiferencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoSalida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoEntrada As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btCosteo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btComparativo As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btListado As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcSubGrupo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents sbImportar As DevExpress.XtraEditors.SimpleButton

End Class
