Imports NexusELL.TableEntities
Imports NexusBLL
Public Class fac_frmMostrarFormasPago
    Dim dtDetalle As New DataTable
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim DetallePago As List(Of fac_VentasDetallePago)

    Private _Imprimir As Boolean = False
    Public Property Imprimir() As Boolean
        Get
            Return _Imprimir
        End Get
        Set(ByVal value As Boolean)
            _Imprimir = value
        End Set
    End Property

    Private _IdComprob As Integer

    Public Property IdComprob() As Integer
        Get
            Return _IdComprob
        End Get
        Set(ByVal value As Integer)
            _IdComprob = value
        End Set
    End Property
    Private _Tipo As Integer

    Public Property Tipo() As Integer
        Get
            Return _Tipo
        End Get
        Set(ByVal value As Integer)
            _Tipo = value
        End Set
    End Property
    Private _IdFormaPago As Integer

    Public Property IdFormaPago() As Integer
        Get
            Return _IdFormaPago
        End Get
        Set(ByVal value As Integer)
            _IdFormaPago = value
        End Set
    End Property
    Private _TotalComprob As Decimal

    Public Property TotalComprob() As Decimal
        Get
            Return _TotalComprob
        End Get
        Set(ByVal value As Decimal)
            _TotalComprob = value
        End Set
    End Property
    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        gvFP.UpdateTotalSummary()
        gvFP.UpdateSummary()

        Dim TotalForma As Decimal = Decimal.Round(SiEsNulo(gcTotal.SummaryItem.SummaryValue, 0), 2)
        If (TotalForma) < teTotalFactura.EditValue Then
            MsgBox("No puede recibir menos  que el total del documento", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        If teCambio.EditValue = 0 Then
            If (TotalForma) > teTotalFactura.EditValue Then
                MsgBox("Ha ingresado mas Dinero que el total del documento", MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If
        End If
        Tipo = 1
        Imprimir = True
        LlenarEntidades()
        Close()
    End Sub


    Private Sub sbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCancel.Click
        gvFP.UpdateTotalSummary()
        gvFP.UpdateSummary()

        Dim TotalForma As Decimal = Decimal.Round(SiEsNulo(gcTotal.SummaryItem.SummaryValue, 0), 2)

        If (TotalForma) < teTotalFactura.EditValue Then
            MsgBox("No puede recibir menos  que el total del documento", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        If (TotalForma) > teTotalFactura.EditValue Then
            MsgBox("Ha ingresado mas Dinero que el total del documento", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        'If teCambio.EditValue = 0 Then

        'End If
        Tipo = 1
        Imprimir = False
        LlenarEntidades()
        Close()
    End Sub

    Private Sub fac_frmMostrarFormasPago_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        teEfectivo.EditValue = TotalComprob
        teEfectivo.Focus()
    End Sub

    Private Sub fac_frmMostrarCambio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        objCombos.fac_FormasPago(leFormaPago, "")
        gcFP.DataSource = bl.fac_ObtenerFormasPago(IdFormaPago, TotalComprob)
        teEfectivo.Focus()
    End Sub

    Private Sub LlenarEntidades()
        Dim frmFacturacion As fac_frmFacturacion = Me.Owner
        Dim dt As DataTable = frmFacturacion.dtDetallePago

        For i = 0 To gvFP.DataRowCount - 1
            If gvFP.GetRowCellValue(i, "Total") <> 0.0 Then
                Dim dr As DataRow = frmFacturacion.dtDetallePago.NewRow()
                dr("IdFormaPago") = gvFP.GetRowCellValue(i, "IdFormaPago")
                dr("Total") = gvFP.GetRowCellValue(i, "Total")
                dr("IdComprobVenta") = 0
                frmFacturacion.dtDetallePago.Rows.Add(dr)
            End If
        Next
    End Sub



    Private Sub teEfectivo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If gvFP.RowCount = 0 Then
            Exit Sub
        End If
        gvFP.UpdateTotalSummary()
        Dim TotalForma As Decimal = Decimal.Round(SiEsNulo(gcTotal.SummaryItem.SummaryValue, 0), 2)
        teEfectivo.EditValue = SiEsNulo(gvFP.GetRowCellValue(0, "Total"), 0)
        If teEfectivo.EditValue > teTotalFactura.EditValue Then
            teCambio.EditValue = (teEfectivo.EditValue) - teTotalFactura.EditValue
        Else
            teCambio.EditValue = 0
        End If
        fac_frmFacturacion.lblUltimoCambio.Text = "ULTIMO CAMBIO: " & Format(teCambio.EditValue, "$##,##0.00")
    End Sub
    Private Sub teEfectivo_KeyDown(sender As Object, e As KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            SimpleButton1.Focus()
        End If
    End Sub
    Private Sub gvFP_RowUpdated(sender As Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gvFP.RowUpdated
        gvFP.UpdateTotalSummary()
        gvFP.UpdateSummary()
        gvFP.UpdateTotalSummary()

        teEfectivo.EditValue = SiEsNulo(gvFP.GetRowCellValue(0, "Total"), 0)
        If teEfectivo.EditValue > teTotalFactura.EditValue Then
            teCambio.EditValue = (teEfectivo.EditValue) - teTotalFactura.EditValue
        Else
            teCambio.EditValue = 0
        End If
        fac_frmFacturacion.lblUltimoCambio.Text = "ULTIMO CAMBIO: " & Format(teCambio.EditValue, "$##,##0.00")
    End Sub
    Private Sub BtRegresar_Click(sender As Object, e As EventArgs) Handles BtRegresar.Click
        Tipo = 0
        Me.Close()
    End Sub

    Private Sub teEfectivo_EditValueChanged_1(sender As Object, e As EventArgs) Handles teEfectivo.EditValueChanged
        'gvFP.SetRowCellValue(0, "Total", teEfectivo.EditValue)
        If teEfectivo.EditValue > teTotalFactura.EditValue Then
            teCambio.EditValue = (teEfectivo.EditValue) - teTotalFactura.EditValue
        Else
            teCambio.EditValue = 0
        End If
        gvFP.UpdateSummary()
        fac_frmFacturacion.lblUltimoCambio.Text = "ULTIMO CAMBIO: " & Format(teCambio.EditValue, "$##,##0.00")
    End Sub
End Class