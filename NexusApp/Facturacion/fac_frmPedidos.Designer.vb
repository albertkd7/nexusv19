﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmPedidos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Me.xtcPedidos = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage()
        Me.gc2 = New DevExpress.XtraGrid.GridControl()
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdProducto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riCodProd = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcIdPrecio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteIdPrecio = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcCantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteDescripcion = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcPrecioVenta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPorcDescto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riPorcDescto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riPrecioUnitario = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcPrecioTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcValorIVA = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcVentaNeta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPrecioCosto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcEsEspecial = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcTipoProducto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcEsCompuesto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.pcTotales = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.teIVA = New DevExpress.XtraEditors.TextEdit()
        Me.lblPercReten = New DevExpress.XtraEditors.LabelControl()
        Me.teRetencionPercepcion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.teTotal = New DevExpress.XtraEditors.TextEdit()
        Me.teAnticipo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.gcHeader = New DevExpress.XtraEditors.GroupControl()
        Me.sbRevertirInventario = New DevExpress.XtraEditors.SimpleButton()
        Me.teTelefonos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.ceReservar = New DevExpress.XtraEditors.CheckEdit()
        Me.sbObtienePedido = New DevExpress.XtraEditors.SimpleButton()
        Me.sbBuscaClientePedido = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoVenta = New DevExpress.XtraEditors.LookUpEdit()
        Me.sbObtieneCotizacion = New DevExpress.XtraEditors.SimpleButton()
        Me.ceMarcarDevolucion = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.seDiasCredito = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.sePjeDescuento = New DevExpress.XtraEditors.SpinEdit()
        Me.ceRetencionPercepcion = New DevExpress.XtraEditors.CheckEdit()
        Me.peFoto = New DevExpress.XtraEditors.PictureEdit()
        Me.gcEx = New DevExpress.XtraGrid.GridControl()
        Me.gvEx = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcBodega = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcSaldo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leVendedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoDoc = New DevExpress.XtraEditors.LookUpEdit()
        Me.leBodega = New DevExpress.XtraEditors.LookUpEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.leFormaPago = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.teNit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.teNRC = New DevExpress.XtraEditors.TextEdit()
        Me.lblNRC_DUI = New DevExpress.XtraEditors.LabelControl()
        Me.teDireccion = New DevExpress.XtraEditors.TextEdit()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.teNotas = New DevExpress.XtraEditors.TextEdit()
        Me.teGiro = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.beCodigo = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.SbCargarHoras = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.xtcPedidos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcPedidos.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riCodProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteIdPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteDescripcion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riPorcDescto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riPrecioUnitario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcTotales.SuspendLayout()
        CType(Me.teIVA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teRetencionPercepcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teAnticipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcHeader.SuspendLayout()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceReservar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceMarcarDevolucion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sePjeDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceRetencionPercepcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peFoto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcEx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvEx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoDoc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNotas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teGiro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcPedidos
        '
        Me.xtcPedidos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcPedidos.Location = New System.Drawing.Point(0, 0)
        Me.xtcPedidos.Name = "xtcPedidos"
        Me.xtcPedidos.SelectedTabPage = Me.xtpLista
        Me.xtcPedidos.Size = New System.Drawing.Size(1247, 508)
        Me.xtcPedidos.TabIndex = 6
        Me.xtcPedidos.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc2)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(1241, 480)
        Me.xtpLista.Text = "Consulta de Pedidos"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit2, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(1241, 480)
        Me.gc2.TabIndex = 5
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemDateEdit2.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.gc)
        Me.xtpDatos.Controls.Add(Me.pcTotales)
        Me.xtpDatos.Controls.Add(Me.PanelControl1)
        Me.xtpDatos.Controls.Add(Me.gcHeader)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(1241, 480)
        Me.xtpDatos.Text = "Pedidos"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 204)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riCantidad, Me.riPorcDescto, Me.riPrecioUnitario, Me.riCodProd, Me.riteIdPrecio, Me.riteDescripcion})
        Me.gc.Size = New System.Drawing.Size(1203, 211)
        Me.gc.TabIndex = 6
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdProducto, Me.gcIdPrecio, Me.gcCantidad, Me.gcDescripcion, Me.gcPrecioVenta, Me.gcPorcDescto, Me.gcPrecioUnitario, Me.gcPrecioTotal, Me.gcValorIVA, Me.gcVentaNeta, Me.gcPrecioCosto, Me.gcEsEspecial, Me.gcTipoProducto, Me.gcEsCompuesto})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsNavigation.EnterMoveNextColumn = True
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdProducto
        '
        Me.gcIdProducto.Caption = "Cód.Producto"
        Me.gcIdProducto.ColumnEdit = Me.riCodProd
        Me.gcIdProducto.FieldName = "IdProducto"
        Me.gcIdProducto.Name = "gcIdProducto"
        Me.gcIdProducto.Visible = True
        Me.gcIdProducto.VisibleIndex = 0
        Me.gcIdProducto.Width = 104
        '
        'riCodProd
        '
        Me.riCodProd.AutoHeight = False
        Me.riCodProd.Mask.EditMask = "\P{Ll}+"
        Me.riCodProd.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.riCodProd.Mask.UseMaskAsDisplayFormat = True
        Me.riCodProd.Name = "riCodProd"
        '
        'gcIdPrecio
        '
        Me.gcIdPrecio.Caption = "Precio"
        Me.gcIdPrecio.ColumnEdit = Me.riteIdPrecio
        Me.gcIdPrecio.FieldName = "IdPrecio"
        Me.gcIdPrecio.Name = "gcIdPrecio"
        Me.gcIdPrecio.Visible = True
        Me.gcIdPrecio.VisibleIndex = 2
        Me.gcIdPrecio.Width = 52
        '
        'riteIdPrecio
        '
        Me.riteIdPrecio.AutoHeight = False
        Me.riteIdPrecio.Mask.EditMask = "n0"
        Me.riteIdPrecio.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteIdPrecio.Mask.UseMaskAsDisplayFormat = True
        Me.riteIdPrecio.Name = "riteIdPrecio"
        '
        'gcCantidad
        '
        Me.gcCantidad.Caption = "Cantidad"
        Me.gcCantidad.ColumnEdit = Me.riCantidad
        Me.gcCantidad.DisplayFormat.FormatString = "{0:n2}"
        Me.gcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcCantidad.FieldName = "Cantidad"
        Me.gcCantidad.Name = "gcCantidad"
        Me.gcCantidad.Visible = True
        Me.gcCantidad.VisibleIndex = 1
        Me.gcCantidad.Width = 68
        '
        'riCantidad
        '
        Me.riCantidad.AutoHeight = False
        Me.riCantidad.Mask.EditMask = "n2"
        Me.riCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.riCantidad.Name = "riCantidad"
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Descripción / Concepto"
        Me.gcDescripcion.ColumnEdit = Me.riteDescripcion
        Me.gcDescripcion.FieldName = "Descripcion"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 3
        Me.gcDescripcion.Width = 394
        '
        'riteDescripcion
        '
        Me.riteDescripcion.AutoHeight = False
        Me.riteDescripcion.Mask.EditMask = "\P{Ll}+"
        Me.riteDescripcion.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.riteDescripcion.Mask.UseMaskAsDisplayFormat = True
        Me.riteDescripcion.Name = "riteDescripcion"
        '
        'gcPrecioVenta
        '
        Me.gcPrecioVenta.Caption = "Precio de Venta"
        Me.gcPrecioVenta.DisplayFormat.FormatString = "{0:n4}"
        Me.gcPrecioVenta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioVenta.FieldName = "PrecioVenta"
        Me.gcPrecioVenta.Name = "gcPrecioVenta"
        Me.gcPrecioVenta.OptionsColumn.AllowEdit = False
        Me.gcPrecioVenta.OptionsColumn.AllowFocus = False
        Me.gcPrecioVenta.Visible = True
        Me.gcPrecioVenta.VisibleIndex = 4
        Me.gcPrecioVenta.Width = 112
        '
        'gcPorcDescto
        '
        Me.gcPorcDescto.Caption = "% de Descto."
        Me.gcPorcDescto.ColumnEdit = Me.riPorcDescto
        Me.gcPorcDescto.FieldName = "PorcDescuento"
        Me.gcPorcDescto.Name = "gcPorcDescto"
        Me.gcPorcDescto.Visible = True
        Me.gcPorcDescto.VisibleIndex = 5
        Me.gcPorcDescto.Width = 79
        '
        'riPorcDescto
        '
        Me.riPorcDescto.AutoHeight = False
        Me.riPorcDescto.Mask.EditMask = "n2"
        Me.riPorcDescto.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riPorcDescto.Mask.UseMaskAsDisplayFormat = True
        Me.riPorcDescto.Name = "riPorcDescto"
        '
        'gcPrecioUnitario
        '
        Me.gcPrecioUnitario.Caption = "Precio Unitario"
        Me.gcPrecioUnitario.ColumnEdit = Me.riPrecioUnitario
        Me.gcPrecioUnitario.FieldName = "PrecioUnitario"
        Me.gcPrecioUnitario.Name = "gcPrecioUnitario"
        Me.gcPrecioUnitario.Visible = True
        Me.gcPrecioUnitario.VisibleIndex = 6
        Me.gcPrecioUnitario.Width = 113
        '
        'riPrecioUnitario
        '
        Me.riPrecioUnitario.AutoHeight = False
        Me.riPrecioUnitario.Mask.EditMask = "n6"
        Me.riPrecioUnitario.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riPrecioUnitario.Mask.UseMaskAsDisplayFormat = True
        Me.riPrecioUnitario.Name = "riPrecioUnitario"
        '
        'gcPrecioTotal
        '
        Me.gcPrecioTotal.Caption = "Precio Total"
        Me.gcPrecioTotal.DisplayFormat.FormatString = "{0:c}"
        Me.gcPrecioTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioTotal.FieldName = "PrecioTotal"
        Me.gcPrecioTotal.Name = "gcPrecioTotal"
        Me.gcPrecioTotal.OptionsColumn.AllowEdit = False
        Me.gcPrecioTotal.OptionsColumn.AllowFocus = False
        Me.gcPrecioTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PrecioTotal", "{0:c}")})
        Me.gcPrecioTotal.Visible = True
        Me.gcPrecioTotal.VisibleIndex = 7
        Me.gcPrecioTotal.Width = 112
        '
        'gcValorIVA
        '
        Me.gcValorIVA.Caption = "ValorIVA"
        Me.gcValorIVA.FieldName = "ValorIva"
        Me.gcValorIVA.Name = "gcValorIVA"
        Me.gcValorIVA.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorIva", "{0:c}")})
        '
        'gcVentaNeta
        '
        Me.gcVentaNeta.Caption = "VentaNeta"
        Me.gcVentaNeta.FieldName = "VentaNeta"
        Me.gcVentaNeta.Name = "gcVentaNeta"
        Me.gcVentaNeta.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VentaNeta", "{0:c}")})
        '
        'gcPrecioCosto
        '
        Me.gcPrecioCosto.Caption = "PrecioCosto"
        Me.gcPrecioCosto.FieldName = "PrecioCosto"
        Me.gcPrecioCosto.Name = "gcPrecioCosto"
        '
        'gcEsEspecial
        '
        Me.gcEsEspecial.Caption = "EsEspecial"
        Me.gcEsEspecial.FieldName = "EsEspecial"
        Me.gcEsEspecial.Name = "gcEsEspecial"
        '
        'gcTipoProducto
        '
        Me.gcTipoProducto.Caption = "TipoProducto"
        Me.gcTipoProducto.FieldName = "TipoProducto"
        Me.gcTipoProducto.Name = "gcTipoProducto"
        '
        'gcEsCompuesto
        '
        Me.gcEsCompuesto.Caption = "gcEsCompuesto"
        Me.gcEsCompuesto.FieldName = "EsCompuesto"
        Me.gcEsCompuesto.Name = "gcEsCompuesto"
        '
        'pcTotales
        '
        Me.pcTotales.Controls.Add(Me.LabelControl20)
        Me.pcTotales.Controls.Add(Me.teIVA)
        Me.pcTotales.Controls.Add(Me.lblPercReten)
        Me.pcTotales.Controls.Add(Me.teRetencionPercepcion)
        Me.pcTotales.Controls.Add(Me.LabelControl21)
        Me.pcTotales.Controls.Add(Me.teTotal)
        Me.pcTotales.Controls.Add(Me.teAnticipo)
        Me.pcTotales.Controls.Add(Me.LabelControl16)
        Me.pcTotales.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pcTotales.Location = New System.Drawing.Point(0, 415)
        Me.pcTotales.Name = "pcTotales"
        Me.pcTotales.Size = New System.Drawing.Size(1203, 65)
        Me.pcTotales.TabIndex = 8
        '
        'LabelControl20
        '
        Me.LabelControl20.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl20.Location = New System.Drawing.Point(1072, 4)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl20.TabIndex = 39
        Me.LabelControl20.Text = "IVA:"
        '
        'teIVA
        '
        Me.teIVA.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teIVA.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.teIVA.Location = New System.Drawing.Point(1098, 1)
        Me.teIVA.Name = "teIVA"
        Me.teIVA.Properties.Appearance.Options.UseTextOptions = True
        Me.teIVA.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teIVA.Properties.DisplayFormat.FormatString = "c"
        Me.teIVA.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teIVA.Properties.Mask.EditMask = "c"
        Me.teIVA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teIVA.Properties.ReadOnly = True
        Me.teIVA.Size = New System.Drawing.Size(98, 20)
        Me.teIVA.TabIndex = 38
        '
        'lblPercReten
        '
        Me.lblPercReten.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblPercReten.Location = New System.Drawing.Point(1005, 25)
        Me.lblPercReten.Name = "lblPercReten"
        Me.lblPercReten.Size = New System.Drawing.Size(90, 13)
        Me.lblPercReten.TabIndex = 41
        Me.lblPercReten.Text = "(-) IVA RETENIDO:"
        '
        'teRetencionPercepcion
        '
        Me.teRetencionPercepcion.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teRetencionPercepcion.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.teRetencionPercepcion.Location = New System.Drawing.Point(1098, 22)
        Me.teRetencionPercepcion.Name = "teRetencionPercepcion"
        Me.teRetencionPercepcion.Properties.Appearance.Options.UseTextOptions = True
        Me.teRetencionPercepcion.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teRetencionPercepcion.Properties.DisplayFormat.FormatString = "c"
        Me.teRetencionPercepcion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teRetencionPercepcion.Properties.Mask.EditMask = "c"
        Me.teRetencionPercepcion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teRetencionPercepcion.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teRetencionPercepcion.Properties.ReadOnly = True
        Me.teRetencionPercepcion.Size = New System.Drawing.Size(98, 20)
        Me.teRetencionPercepcion.TabIndex = 40
        '
        'LabelControl21
        '
        Me.LabelControl21.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(1054, 47)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl21.TabIndex = 43
        Me.LabelControl21.Text = "TOTAL:"
        '
        'teTotal
        '
        Me.teTotal.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teTotal.EditValue = New Decimal(New Integer() {0, 0, 0, 65536})
        Me.teTotal.Location = New System.Drawing.Point(1098, 43)
        Me.teTotal.Name = "teTotal"
        Me.teTotal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.teTotal.Properties.Appearance.Options.UseFont = True
        Me.teTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotal.Properties.DisplayFormat.FormatString = "c"
        Me.teTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teTotal.Properties.ReadOnly = True
        Me.teTotal.Size = New System.Drawing.Size(98, 20)
        Me.teTotal.TabIndex = 42
        '
        'teAnticipo
        '
        Me.teAnticipo.EditValue = 0
        Me.teAnticipo.EnterMoveNextControl = True
        Me.teAnticipo.Location = New System.Drawing.Point(376, 32)
        Me.teAnticipo.Name = "teAnticipo"
        Me.teAnticipo.Properties.Appearance.Options.UseTextOptions = True
        Me.teAnticipo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teAnticipo.Properties.Mask.EditMask = "n2"
        Me.teAnticipo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teAnticipo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teAnticipo.Size = New System.Drawing.Size(78, 20)
        Me.teAnticipo.TabIndex = 15
        Me.teAnticipo.Visible = False
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(331, 36)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(42, 13)
        Me.LabelControl16.TabIndex = 34
        Me.LabelControl16.Text = "Anticipo:"
        Me.LabelControl16.Visible = False
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cmdBorrar)
        Me.PanelControl1.Controls.Add(Me.cmdDown)
        Me.PanelControl1.Controls.Add(Me.cmdUp)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelControl1.Location = New System.Drawing.Point(1203, 204)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(38, 276)
        Me.PanelControl1.TabIndex = 7
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(6, 76)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(28, 24)
        Me.cmdBorrar.TabIndex = 34
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(6, 41)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(28, 24)
        Me.cmdDown.TabIndex = 33
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(6, 6)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(28, 24)
        Me.cmdUp.TabIndex = 32
        '
        'gcHeader
        '
        Me.gcHeader.Controls.Add(Me.SbCargarHoras)
        Me.gcHeader.Controls.Add(Me.sbRevertirInventario)
        Me.gcHeader.Controls.Add(Me.teTelefonos)
        Me.gcHeader.Controls.Add(Me.LabelControl18)
        Me.gcHeader.Controls.Add(Me.ceReservar)
        Me.gcHeader.Controls.Add(Me.sbObtienePedido)
        Me.gcHeader.Controls.Add(Me.sbBuscaClientePedido)
        Me.gcHeader.Controls.Add(Me.LabelControl24)
        Me.gcHeader.Controls.Add(Me.leTipoVenta)
        Me.gcHeader.Controls.Add(Me.sbObtieneCotizacion)
        Me.gcHeader.Controls.Add(Me.ceMarcarDevolucion)
        Me.gcHeader.Controls.Add(Me.LabelControl17)
        Me.gcHeader.Controls.Add(Me.seDiasCredito)
        Me.gcHeader.Controls.Add(Me.LabelControl26)
        Me.gcHeader.Controls.Add(Me.sePjeDescuento)
        Me.gcHeader.Controls.Add(Me.ceRetencionPercepcion)
        Me.gcHeader.Controls.Add(Me.peFoto)
        Me.gcHeader.Controls.Add(Me.gcEx)
        Me.gcHeader.Controls.Add(Me.leVendedor)
        Me.gcHeader.Controls.Add(Me.LabelControl9)
        Me.gcHeader.Controls.Add(Me.leTipoDoc)
        Me.gcHeader.Controls.Add(Me.leBodega)
        Me.gcHeader.Controls.Add(Me.leSucursal)
        Me.gcHeader.Controls.Add(Me.LabelControl15)
        Me.gcHeader.Controls.Add(Me.LabelControl12)
        Me.gcHeader.Controls.Add(Me.LabelControl11)
        Me.gcHeader.Controls.Add(Me.leFormaPago)
        Me.gcHeader.Controls.Add(Me.LabelControl10)
        Me.gcHeader.Controls.Add(Me.teNit)
        Me.gcHeader.Controls.Add(Me.LabelControl13)
        Me.gcHeader.Controls.Add(Me.LabelControl6)
        Me.gcHeader.Controls.Add(Me.teNRC)
        Me.gcHeader.Controls.Add(Me.lblNRC_DUI)
        Me.gcHeader.Controls.Add(Me.teDireccion)
        Me.gcHeader.Controls.Add(Me.teNombre)
        Me.gcHeader.Controls.Add(Me.teNotas)
        Me.gcHeader.Controls.Add(Me.teGiro)
        Me.gcHeader.Controls.Add(Me.LabelControl4)
        Me.gcHeader.Controls.Add(Me.LabelControl7)
        Me.gcHeader.Controls.Add(Me.LabelControl8)
        Me.gcHeader.Controls.Add(Me.beCodigo)
        Me.gcHeader.Controls.Add(Me.LabelControl3)
        Me.gcHeader.Controls.Add(Me.deFecha)
        Me.gcHeader.Controls.Add(Me.LabelControl2)
        Me.gcHeader.Controls.Add(Me.teCorrelativo)
        Me.gcHeader.Controls.Add(Me.LabelControl14)
        Me.gcHeader.Controls.Add(Me.teNumero)
        Me.gcHeader.Controls.Add(Me.LabelControl1)
        Me.gcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.gcHeader.Location = New System.Drawing.Point(0, 0)
        Me.gcHeader.Name = "gcHeader"
        Me.gcHeader.Size = New System.Drawing.Size(1241, 204)
        Me.gcHeader.TabIndex = 1
        Me.gcHeader.Text = "Datos del pedido"
        '
        'sbRevertirInventario
        '
        Me.sbRevertirInventario.AllowFocus = False
        Me.sbRevertirInventario.Location = New System.Drawing.Point(1038, 173)
        Me.sbRevertirInventario.Name = "sbRevertirInventario"
        Me.sbRevertirInventario.Size = New System.Drawing.Size(101, 23)
        Me.sbRevertirInventario.TabIndex = 110
        Me.sbRevertirInventario.Text = "&Revertir"
        Me.sbRevertirInventario.ToolTipTitle = "Revertir el descargo de Inventario"
        '
        'teTelefonos
        '
        Me.teTelefonos.EnterMoveNextControl = True
        Me.teTelefonos.Location = New System.Drawing.Point(102, 170)
        Me.teTelefonos.Name = "teTelefonos"
        Me.teTelefonos.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teTelefonos.Size = New System.Drawing.Size(100, 20)
        Me.teTelefonos.TabIndex = 117
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(24, 173)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(74, 13)
        Me.LabelControl18.TabIndex = 116
        Me.LabelControl18.Text = "Orden Compra:"
        '
        'ceReservar
        '
        Me.ceReservar.Location = New System.Drawing.Point(374, 23)
        Me.ceReservar.Name = "ceReservar"
        Me.ceReservar.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ceReservar.Properties.Appearance.Options.UseFont = True
        Me.ceReservar.Properties.Caption = "SOLICITAR MERCADERIA A BODEGA"
        Me.ceReservar.Size = New System.Drawing.Size(221, 19)
        Me.ceReservar.TabIndex = 115
        Me.ceReservar.Visible = False
        '
        'sbObtienePedido
        '
        Me.sbObtienePedido.AllowFocus = False
        Me.sbObtienePedido.Location = New System.Drawing.Point(564, 170)
        Me.sbObtienePedido.Name = "sbObtienePedido"
        Me.sbObtienePedido.Size = New System.Drawing.Size(110, 23)
        Me.sbObtienePedido.TabIndex = 114
        Me.sbObtienePedido.Text = "&Obtener Pedido Dev"
        Me.sbObtienePedido.ToolTipTitle = "Llama un pedido para hacer devolucion"
        '
        'sbBuscaClientePedido
        '
        Me.sbBuscaClientePedido.AllowFocus = False
        Me.sbBuscaClientePedido.Location = New System.Drawing.Point(202, 44)
        Me.sbBuscaClientePedido.Name = "sbBuscaClientePedido"
        Me.sbBuscaClientePedido.Size = New System.Drawing.Size(17, 21)
        Me.sbBuscaClientePedido.TabIndex = 112
        Me.sbBuscaClientePedido.Text = "&B"
        Me.sbBuscaClientePedido.ToolTipTitle = "Muestra las existencias por bodega"
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(568, 152)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl24.TabIndex = 111
        Me.LabelControl24.Text = "Tipo de Impuesto:"
        '
        'leTipoVenta
        '
        Me.leTipoVenta.EnterMoveNextControl = True
        Me.leTipoVenta.Location = New System.Drawing.Point(658, 149)
        Me.leTipoVenta.Name = "leTipoVenta"
        Me.leTipoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoVenta.Size = New System.Drawing.Size(133, 20)
        Me.leTipoVenta.TabIndex = 15
        '
        'sbObtieneCotizacion
        '
        Me.sbObtieneCotizacion.AllowFocus = False
        Me.sbObtieneCotizacion.Location = New System.Drawing.Point(688, 170)
        Me.sbObtieneCotizacion.Name = "sbObtieneCotizacion"
        Me.sbObtieneCotizacion.Size = New System.Drawing.Size(101, 23)
        Me.sbObtieneCotizacion.TabIndex = 109
        Me.sbObtieneCotizacion.Text = "&Obtener Cotización"
        Me.sbObtieneCotizacion.ToolTipTitle = "Muestra las existencias por bodega"
        '
        'ceMarcarDevolucion
        '
        Me.ceMarcarDevolucion.Location = New System.Drawing.Point(795, 174)
        Me.ceMarcarDevolucion.Name = "ceMarcarDevolucion"
        Me.ceMarcarDevolucion.Properties.Caption = "(-) MARCAR FACTURA COMO DEVOLUCION"
        Me.ceMarcarDevolucion.Size = New System.Drawing.Size(235, 19)
        Me.ceMarcarDevolucion.TabIndex = 108
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(224, 132)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl17.TabIndex = 107
        Me.LabelControl17.Text = "Días de Crédito:"
        '
        'seDiasCredito
        '
        Me.seDiasCredito.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDiasCredito.EnterMoveNextControl = True
        Me.seDiasCredito.Location = New System.Drawing.Point(304, 128)
        Me.seDiasCredito.Name = "seDiasCredito"
        Me.seDiasCredito.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDiasCredito.Properties.Mask.EditMask = "n0"
        Me.seDiasCredito.Size = New System.Drawing.Size(97, 20)
        Me.seDiasCredito.TabIndex = 12
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(425, 175)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl26.TabIndex = 105
        Me.LabelControl26.Text = "% de Descto:"
        '
        'sePjeDescuento
        '
        Me.sePjeDescuento.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.sePjeDescuento.EnterMoveNextControl = True
        Me.sePjeDescuento.Location = New System.Drawing.Point(492, 170)
        Me.sePjeDescuento.Name = "sePjeDescuento"
        Me.sePjeDescuento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.sePjeDescuento.Properties.Mask.EditMask = "P2"
        Me.sePjeDescuento.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.sePjeDescuento.Size = New System.Drawing.Size(64, 20)
        Me.sePjeDescuento.TabIndex = 16
        '
        'ceRetencionPercepcion
        '
        Me.ceRetencionPercepcion.Location = New System.Drawing.Point(203, 170)
        Me.ceRetencionPercepcion.Name = "ceRetencionPercepcion"
        Me.ceRetencionPercepcion.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ceRetencionPercepcion.Properties.Appearance.Options.UseFont = True
        Me.ceRetencionPercepcion.Properties.Caption = "APLICAR RETENCIÓN DEL 1 %"
        Me.ceRetencionPercepcion.Size = New System.Drawing.Size(216, 19)
        Me.ceRetencionPercepcion.TabIndex = 103
        '
        'peFoto
        '
        Me.peFoto.Cursor = System.Windows.Forms.Cursors.Default
        Me.peFoto.Location = New System.Drawing.Point(1038, 26)
        Me.peFoto.Name = "peFoto"
        Me.peFoto.Properties.AllowFocused = False
        Me.peFoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch
        Me.peFoto.Properties.ZoomAccelerationFactor = 1.0R
        Me.peFoto.Size = New System.Drawing.Size(204, 145)
        ToolTipTitleItem2.Text = "Imagen del producto"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        Me.peFoto.SuperTip = SuperToolTip2
        Me.peFoto.TabIndex = 38
        Me.peFoto.ToolTipTitle = "Doble clic para cambiar o cargar imagen"
        '
        'gcEx
        '
        Me.gcEx.EmbeddedNavigator.Text = "Existencias"
        Me.gcEx.Enabled = False
        Me.gcEx.Location = New System.Drawing.Point(797, 26)
        Me.gcEx.MainView = Me.gvEx
        Me.gcEx.Name = "gcEx"
        Me.gcEx.Size = New System.Drawing.Size(236, 145)
        Me.gcEx.TabIndex = 35
        Me.gcEx.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvEx})
        '
        'gvEx
        '
        Me.gvEx.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcBodega, Me.gcSaldo})
        Me.gvEx.GridControl = Me.gcEx
        Me.gvEx.Name = "gvEx"
        Me.gvEx.OptionsBehavior.Editable = False
        Me.gvEx.OptionsView.ShowGroupPanel = False
        '
        'gcBodega
        '
        Me.gcBodega.Caption = "Bodega"
        Me.gcBodega.FieldName = "Bodega"
        Me.gcBodega.Name = "gcBodega"
        Me.gcBodega.Visible = True
        Me.gcBodega.VisibleIndex = 0
        Me.gcBodega.Width = 218
        '
        'gcSaldo
        '
        Me.gcSaldo.Caption = "Saldo"
        Me.gcSaldo.FieldName = "Saldo"
        Me.gcSaldo.Name = "gcSaldo"
        Me.gcSaldo.Visible = True
        Me.gcSaldo.VisibleIndex = 1
        Me.gcSaldo.Width = 131
        '
        'leVendedor
        '
        Me.leVendedor.EnterMoveNextControl = True
        Me.leVendedor.Location = New System.Drawing.Point(102, 107)
        Me.leVendedor.Name = "leVendedor"
        Me.leVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leVendedor.Size = New System.Drawing.Size(148, 20)
        Me.leVendedor.TabIndex = 8
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(22, 133)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl9.TabIndex = 33
        Me.LabelControl9.Text = "Forma de Pago:"
        '
        'leTipoDoc
        '
        Me.leTipoDoc.EnterMoveNextControl = True
        Me.leTipoDoc.Location = New System.Drawing.Point(563, 128)
        Me.leTipoDoc.Name = "leTipoDoc"
        Me.leTipoDoc.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoDoc.Size = New System.Drawing.Size(228, 20)
        Me.leTipoDoc.TabIndex = 13
        '
        'leBodega
        '
        Me.leBodega.EnterMoveNextControl = True
        Me.leBodega.Location = New System.Drawing.Point(563, 107)
        Me.leBodega.Name = "leBodega"
        Me.leBodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leBodega.Size = New System.Drawing.Size(228, 20)
        Me.leBodega.TabIndex = 10
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(304, 107)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(209, 20)
        Me.leSucursal.TabIndex = 9
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(502, 133)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl15.TabIndex = 34
        Me.LabelControl15.Text = "Documento:"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(66, 154)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl12.TabIndex = 34
        Me.LabelControl12.Text = "Notas:"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(520, 113)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl11.TabIndex = 34
        Me.LabelControl11.Text = "Bodega:"
        '
        'leFormaPago
        '
        Me.leFormaPago.EnterMoveNextControl = True
        Me.leFormaPago.Location = New System.Drawing.Point(102, 128)
        Me.leFormaPago.Name = "leFormaPago"
        Me.leFormaPago.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leFormaPago.Size = New System.Drawing.Size(114, 20)
        Me.leFormaPago.TabIndex = 11
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(257, 112)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl10.TabIndex = 34
        Me.LabelControl10.Text = "Sucursal:"
        '
        'teNit
        '
        Me.teNit.EnterMoveNextControl = True
        Me.teNit.Location = New System.Drawing.Point(268, 65)
        Me.teNit.Name = "teNit"
        Me.teNit.Size = New System.Drawing.Size(127, 20)
        Me.teNit.TabIndex = 5
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(411, 70)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(23, 13)
        Me.LabelControl13.TabIndex = 29
        Me.LabelControl13.Text = "Giro:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(223, 70)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(43, 13)
        Me.LabelControl6.TabIndex = 29
        Me.LabelControl6.Text = "NIT/DUI:"
        '
        'teNRC
        '
        Me.teNRC.EnterMoveNextControl = True
        Me.teNRC.Location = New System.Drawing.Point(102, 65)
        Me.teNRC.Name = "teNRC"
        Me.teNRC.Size = New System.Drawing.Size(100, 20)
        Me.teNRC.TabIndex = 4
        '
        'lblNRC_DUI
        '
        Me.lblNRC_DUI.Location = New System.Drawing.Point(73, 68)
        Me.lblNRC_DUI.Name = "lblNRC_DUI"
        Me.lblNRC_DUI.Size = New System.Drawing.Size(25, 13)
        Me.lblNRC_DUI.TabIndex = 25
        Me.lblNRC_DUI.Text = "NRC:"
        '
        'teDireccion
        '
        Me.teDireccion.Enabled = False
        Me.teDireccion.EnterMoveNextControl = True
        Me.teDireccion.Location = New System.Drawing.Point(102, 86)
        Me.teDireccion.Name = "teDireccion"
        Me.teDireccion.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teDireccion.Size = New System.Drawing.Size(689, 20)
        Me.teDireccion.TabIndex = 7
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(268, 44)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teNombre.Size = New System.Drawing.Size(523, 20)
        Me.teNombre.TabIndex = 3
        '
        'teNotas
        '
        Me.teNotas.EnterMoveNextControl = True
        Me.teNotas.Location = New System.Drawing.Point(102, 149)
        Me.teNotas.Name = "teNotas"
        Me.teNotas.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teNotas.Size = New System.Drawing.Size(454, 20)
        Me.teNotas.TabIndex = 14
        '
        'teGiro
        '
        Me.teGiro.EnterMoveNextControl = True
        Me.teGiro.Location = New System.Drawing.Point(438, 65)
        Me.teGiro.Name = "teGiro"
        Me.teGiro.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teGiro.Size = New System.Drawing.Size(353, 20)
        Me.teGiro.TabIndex = 6
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(224, 49)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl4.TabIndex = 23
        Me.LabelControl4.Text = "Nombre:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(51, 92)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl7.TabIndex = 27
        Me.LabelControl7.Text = "Dirección:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(48, 113)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl8.TabIndex = 28
        Me.LabelControl8.Text = "Vendedor:"
        '
        'beCodigo
        '
        Me.beCodigo.EnterMoveNextControl = True
        Me.beCodigo.Location = New System.Drawing.Point(102, 44)
        Me.beCodigo.Name = "beCodigo"
        Me.beCodigo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCodigo.Size = New System.Drawing.Size(100, 20)
        Me.beCodigo.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(38, 49)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl3.TabIndex = 21
        Me.LabelControl3.Text = "Cód.Cliente:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.Enabled = False
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(268, 23)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.deFecha.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(232, 28)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl2.TabIndex = 19
        Me.LabelControl2.Text = "Fecha:"
        '
        'teCorrelativo
        '
        Me.teCorrelativo.Enabled = False
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(709, 23)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.teCorrelativo.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.teCorrelativo.Properties.ReadOnly = True
        Me.teCorrelativo.Size = New System.Drawing.Size(82, 20)
        Me.teCorrelativo.TabIndex = 1
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(649, 28)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl14.TabIndex = 17
        Me.LabelControl14.Text = "Correlativo:"
        '
        'teNumero
        '
        Me.teNumero.Enabled = False
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(102, 23)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.teNumero.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.teNumero.Size = New System.Drawing.Size(100, 20)
        Me.teNumero.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(7, 28)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl1.TabIndex = 17
        Me.LabelControl1.Text = "Número de Pedido:"
        '
        'SbCargarHoras
        '
        Me.SbCargarHoras.AllowFocus = False
        Me.SbCargarHoras.Location = New System.Drawing.Point(1036, 173)
        Me.SbCargarHoras.Name = "SbCargarHoras"
        Me.SbCargarHoras.Size = New System.Drawing.Size(103, 23)
        Me.SbCargarHoras.TabIndex = 118
        Me.SbCargarHoras.Text = "&Cargar Horas"
        Me.SbCargarHoras.ToolTipTitle = "Revertir el descargo de Inventario"
        '
        'fac_frmPedidos
        '
        Me.ClientSize = New System.Drawing.Size(1247, 533)
        Me.Controls.Add(Me.xtcPedidos)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmPedidos"
        Me.OptionId = "002001"
        Me.Text = "Pedidos de mercadería"
        Me.Controls.SetChildIndex(Me.xtcPedidos, 0)
        CType(Me.xtcPedidos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcPedidos.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riCodProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteIdPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteDescripcion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riPorcDescto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riPrecioUnitario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcTotales.ResumeLayout(False)
        Me.pcTotales.PerformLayout()
        CType(Me.teIVA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teRetencionPercepcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teAnticipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcHeader.ResumeLayout(False)
        Me.gcHeader.PerformLayout()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceReservar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceMarcarDevolucion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sePjeDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceRetencionPercepcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peFoto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcEx, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvEx, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoDoc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNotas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teGiro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents xtcPedidos As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gcHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents sbObtienePedido As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbBuscaClientePedido As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoVenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents sbObtieneCotizacion As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ceMarcarDevolucion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seDiasCredito As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sePjeDescuento As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents ceRetencionPercepcion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents peFoto As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents gcEx As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvEx As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leVendedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoDoc As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leBodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leFormaPago As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblNRC_DUI As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teAnticipo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNotas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teGiro As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCodigo As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents pcTotales As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIVA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPercReten As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teRetencionPercepcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riCodProd As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcIdPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioVenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPorcDescto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riPorcDescto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riPrecioUnitario As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcPrecioTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorIVA As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcVentaNeta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents ceReservar As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents riteIdPrecio As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents teTelefonos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents riteDescripcion As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcPrecioCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcEsEspecial As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTipoProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcEsCompuesto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents sbRevertirInventario As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SbCargarHoras As DevExpress.XtraEditors.SimpleButton
End Class
