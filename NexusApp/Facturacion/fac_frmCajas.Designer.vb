﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmCajas
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcNumero = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcResponsable = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcUltimoCorte = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcFechaUC = New DevExpress.XtraGrid.Columns.GridColumn
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(774, 391)
        Me.gc.TabIndex = 4
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcNumero, Me.gcNombre, Me.gcResponsable, Me.gcUltimoCorte, Me.gcFechaUC})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcNumero
        '
        Me.gcNumero.Caption = "No. de Caja"
        Me.gcNumero.FieldName = "NumeroCaja"
        Me.gcNumero.Name = "gcNumero"
        Me.gcNumero.Visible = True
        Me.gcNumero.VisibleIndex = 0
        Me.gcNumero.Width = 100
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "NombreCaja"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 1
        Me.gcNombre.Width = 231
        '
        'gcResponsable
        '
        Me.gcResponsable.Caption = "Responsable"
        Me.gcResponsable.FieldName = "Responsable"
        Me.gcResponsable.Name = "gcResponsable"
        Me.gcResponsable.Visible = True
        Me.gcResponsable.VisibleIndex = 2
        Me.gcResponsable.Width = 222
        '
        'gcUltimoCorte
        '
        Me.gcUltimoCorte.Caption = "Ultimo Corte"
        Me.gcUltimoCorte.FieldName = "UltimoCorte"
        Me.gcUltimoCorte.Name = "gcUltimoCorte"
        Me.gcUltimoCorte.Visible = True
        Me.gcUltimoCorte.VisibleIndex = 3
        Me.gcUltimoCorte.Width = 93
        '
        'gcFechaUC
        '
        Me.gcFechaUC.Caption = "Fecha Ult.Corte"
        Me.gcFechaUC.FieldName = "FechaUltimoCorte"
        Me.gcFechaUC.Name = "gcFechaUC"
        Me.gcFechaUC.Visible = True
        Me.gcFechaUC.VisibleIndex = 4
        Me.gcFechaUC.Width = 107
        '
        'fac_frmCajas
        '
        Me.ClientSize = New System.Drawing.Size(859, 416)
        Me.Controls.Add(Me.gc)
        Me.DbMode = 1
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmCajas"
        Me.OptionId = "001002"
        Me.Text = "Cajas Registradoras"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcUltimoCorte As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFechaUC As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcResponsable As DevExpress.XtraGrid.Columns.GridColumn

End Class
