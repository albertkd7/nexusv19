Public Class fac_frmTipoFacturaEvento

    Private _TipoFacturacion As System.Int32
    Public Property TipoFacturacion() As System.Int32
        Get
            Return _TipoFacturacion
        End Get
        Set(ByVal value As System.Int32)
            _TipoFacturacion = value
        End Set
    End Property
    Private _NumCuota As System.Int32
    Public Property NumCuota() As System.Int32
        Get
            Return _NumCuota
        End Get
        Set(ByVal value As System.Int32)
            _NumCuota = value
        End Set
    End Property



    Private Sub sbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCancel.Click
        TipoFacturacion = rgOpcion.EditValue
        NumCuota = teNumCuota.EditValue
        Close()
    End Sub

    Private Sub fac_frmMostrarCambio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        rgOpcion.Focus()
    End Sub

    Private Sub rgOpcion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgOpcion.SelectedIndexChanged
        If rgOpcion.EditValue = 1 Then
            lblCuota.Visible = False
            teNumCuota.Visible = False
        Else
            lblCuota.Visible = True
            teNumCuota.Visible = True
        End If
    End Sub
End Class