Imports System.Drawing
Public Class fac_frmEspecificaciones
    Dim FileName As String

    Private _Salir As Boolean = False
    Public Property Salir() As Boolean
        Get
            Return _Salir
        End Get
        Set(ByVal value As Boolean)
            _Salir = value
        End Set
    End Property
    Private _Aceptar As Boolean = False
    Public Property Aceptar() As Boolean
        Get
            Return _Aceptar
        End Get
        Set(ByVal value As Boolean)
            _Aceptar = value
        End Set
    End Property

    Private _Tipo As Boolean = False
    Public Property Tipo() As Boolean
        Get
            Return _Tipo
        End Get
        Set(ByVal value As Boolean)
            _Tipo = value
        End Set
    End Property

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbAceptar.Click
        Aceptar = True
        Close()
    End Sub

    Private Sub sbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCancel.Click
        Salir = False
        Close()
    End Sub

    Private Sub fac_frmEspecificaciones_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        ActivaControles(Tipo)
        CargaImagen(beImagen.EditValue)
    End Sub

    Private Sub fac_frmMostrarCambio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        ActivaControles(Tipo)
        CargaImagen(beImagen.EditValue)
    End Sub

    Private Sub CargaImagen(ByVal FileName As String)
        If Not FileIO.FileSystem.FileExists(FileName) Or FileName = "" Then
            peFoto.Image = Nothing
            lcExisteArchivo.Text = "-DOBLE CLIC PARA CARGAR IMAGEN-"
        Else
            Dim bm As New Bitmap(FileName)
            peFoto.Image = bm
            lcExisteArchivo.Text = "-DOBLE CLIC PARA CAMBIAR IMAGEN-"
        End If
    End Sub
    Private Sub beImagen_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beImagen.ButtonClick
        If Not Tipo Then
            Exit Sub
        End If
        OpenFile.ShowDialog()
        beImagen.Text = OpenFile.FileName
        CargaImagen(beImagen.Text)
    End Sub
    Private Sub peFoto_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles peFoto.DoubleClick

        If Not Tipo Then
            Exit Sub
        End If

        OpenFile.ShowDialog()
        FileName = OpenFile.FileName
        beImagen.Text = OpenFile.FileName
        Dim bm As New Bitmap(FileName)
        peFoto.Image = bm
    End Sub

    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In Me.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.MemoEdit Then
                CType(ctrl, DevExpress.XtraEditors.MemoEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
    End Sub
End Class