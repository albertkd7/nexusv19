﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmAsignacionDocumentos
    Dim myBL As New FacturaBLL(g_ConnectionString)
    Dim ent As adm_SeriesDocumentos
    Dim IdSucursal As Integer = 1
    

    Private Sub adm_frmAsignacionDocumentos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = myBL.fac_ObtenerSeriesDocumentos(objMenu.User)
        IdSucursal = gv.GetFocusedRowCellValue(gv.Columns(0))
        objCombos.adm_Sucursales(rileSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(rilePunto, -1, "")
        objCombos.fac_TiposComprobante(rileTipo, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePunto, IdSucursal, "")
        objCombos.fac_TiposComprobante(leTipoDoc, "")
    End Sub
    Private Sub frmTiposPartida_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar esta asignación?", MsgBoxStyle.YesNo, "Nexus") = MsgBoxResult.No Then
            Return
        End If

        IdSucursal = gv.GetFocusedRowCellValue(gv.Columns(0))
        Dim IdPunto As Integer = gv.GetFocusedRowCellValue(gv.Columns(1))
        Dim IdTipo As Integer = gv.GetFocusedRowCellValue(gv.Columns(2))
        Dim Serie As String = gv.GetFocusedRowCellValue(gv.Columns(3))
        Dim Resolucion As String = gv.GetFocusedRowCellValue(gv.Columns(4))
        objTablas.adm_SeriesDocumentosDeleteByPK(IdSucursal, IdPunto, IdTipo, Serie, Resolucion)
        gc.DataSource = myBL.fac_ObtenerSeriesDocumentos(objMenu.User)
    End Sub
    
    Private Sub facGrupos_Nuevo_Click() Handles Me.Nuevo
        ent = New adm_SeriesDocumentos
        CargaPantalla()
    End Sub
    Private Sub facGrupos_Guardar() Handles Me.Guardar
        If XtraTabControl1.SelectedTabPageIndex = 0 Then
            Exit Sub
        End If
        If deFecha.Text = "" Then
            MsgBox("Debe de especificar la fecha", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        With ent
            .IdSucursal = leSucursal.EditValue
            .IdPunto = lePunto.EditValue
            .IdTipoComprobante = leTipoDoc.EditValue
            .Serie = teSerie.EditValue
            .UltimoNumero = seUltimo.EditValue
            .DesdeNumero = seDesde.EditValue
            .HastaNumero = seHasta.EditValue
            .CreadoPor = objMenu.User
            .EsActivo = ceActivo.EditValue
            .FechaAsignacion = deFecha.EditValue
            .Resolucion = TeResolucion.EditValue
        End With

        If DbMode = DbModeType.insert Then
            objTablas.adm_SeriesDocumentosInsert(ent)
        Else
            objTablas.adm_SeriesDocumentosUpdate(ent)
        End If

        DbMode = DbModeType.insert
        MostrarModoInicial()
        XtraTabControl1.SelectedTabPage = XtraTabPage1
        gc.DataSource = myBL.fac_ObtenerSeriesDocumentos(objMenu.User)
    End Sub
    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc.DoubleClick
        IdSucursal = gv.GetFocusedRowCellValue(gv.Columns(0))
        Dim IdPunto As Integer = gv.GetFocusedRowCellValue(gv.Columns(1))
        Dim IdTipo As Integer = gv.GetFocusedRowCellValue(gv.Columns(2))
        Dim Serie As String = gv.GetFocusedRowCellValue(gv.Columns(3))
        Dim Resolucion As String = gv.GetFocusedRowCellValue(gv.Columns(4))
        ent = objTablas.adm_SeriesDocumentosSelectByPK(IdSucursal, IdPunto, IdTipo, Serie, Resolucion)
        DbMode = DbModeType.update
        MostrarModoEditar()
        CargaPantalla()
        leSucursal.Focus()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePunto, leSucursal.EditValue, "")
    End Sub
    Private Sub CargaPantalla()
        XtraTabControl1.SelectedTabPage = XtraTabPage2
        If DbMode = DbModeType.Update Then
            leSucursal.Properties.ReadOnly = True
            lePunto.Properties.ReadOnly = True
            leTipoDoc.Properties.ReadOnly = True
            teSerie.Properties.ReadOnly = True
            If objMenu.User <> "ADMINISTRADOR" Then
                TeResolucion.Properties.ReadOnly = True
            Else
                TeResolucion.Properties.ReadOnly = False
            End If
        Else
            leSucursal.Properties.ReadOnly = False
            lePunto.Properties.ReadOnly = False
            leTipoDoc.Properties.ReadOnly = False
            teSerie.Properties.ReadOnly = False
        End If

        With ent
            leSucursal.EditValue = .IdSucursal
            lePunto.EditValue = .IdPunto
            leTipoDoc.EditValue = .IdTipoComprobante
            teSerie.EditValue = .Serie
            seUltimo.EditValue = .UltimoNumero
            seDesde.EditValue = .DesdeNumero
            seHasta.EditValue = .HastaNumero
            ceActivo.EditValue = .EsActivo
            deFecha.EditValue = .FechaAsignacion
            TeResolucion.EditValue = .Resolucion

            If DbMode = DbModeType.insert Then
                deFecha.EditValue = Today()
            End If
        End With
    End Sub

    Private Sub fac_frmAsignacionDocumentos_Revertir() Handles Me.Revertir
        XtraTabControl1.SelectedTabPage = XtraTabPage1
        MostrarModoInicial()
    End Sub
End Class
