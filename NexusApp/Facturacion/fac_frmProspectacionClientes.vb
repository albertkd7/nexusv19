﻿Imports NexusBLL 
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns


Public Class fac_frmProspectacionClientes
	Dim entCliente As fac_ProspectacionClientes
	Dim FlagIdCuenta As Boolean = False
	Dim bl As New InventarioBLL(g_ConnectionString)
	Dim fd As New FuncionesBLL(g_ConnectionString)
	Dim blAdmon As New AdmonBLL(g_ConnectionString)
	Dim blCli As New FacturaBLL(g_ConnectionString)
	Dim dtParam As DataTable = blAdmon.ObtieneParametros()


	Private Sub fac_frmClientes_Eliminar() Handles Me.Eliminar
		Try
			If MsgBox("Está seguro(a) de eliminar a éste prospecto de cliente?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
				objTablas.fac_ProspectacionClientesDeleteByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"))
			End If
		Catch ex As Exception

			MsgBox("SE DETECTÓ UN ERROR AL TRATAR DE ELIMINAR:" + Chr(13) + ex.Message(), MsgBoxStyle.Critical, "Error")
		End Try


	End Sub

	Private Sub acf_frmActivos_RefreshConsulta() Handles Me.RefreshConsulta
		gc.DataSource = objTablas.fac_ProspectacionClientesSelectAll
		gv.BestFitColumns()
	End Sub

	Private Sub facClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		objCombos.admDepartamentos(leDepartamento)
		objCombos.admMunicipios(leMunicipio, leDepartamento.EditValue)
		objCombos.fac_Vendedores(leVendedor)
		objCombos.fac_Vendedores(riteVendedor)
		gc.DataSource = objTablas.fac_ProspectacionClientesSelectAll
		ActivaControles(False)
	End Sub

	Private Sub fac_frmClientes_Nuevo() Handles Me.Nuevo
		dtParam = blAdmon.ObtieneParametros()


		entCliente = New fac_ProspectacionClientes
		entCliente.IdVendedor = leVendedor.EditValue
		entCliente.IdMunicipio = "0614"
		entCliente.IdDepartamento = "06"
		entCliente.IdVendedor = IIf(piIdVendedor <> -1, piIdVendedor, 1)
		CargaPantalla()


		deFechaContacto.EditValue = Today
		ActivaControles(True)
	End Sub
	Private Sub fac_frmClientes_Guardar() Handles Me.Guardar
		If Not DatosValidos() Then
			Exit Sub
		End If

		CargaEntidad()


		If DbMode = DbModeType.insert Then
			Try
				entCliente.IdComprobante = fd.ObtenerUltimoId("fac_ProspectacionClientes", "IdComprobante") + 1
				objTablas.fac_ProspectacionClientesInsert(entCliente)
			Catch ex As Exception
				MsgBox("Ocurrio un problema al crear la ficha del prospecto de cliente" + Chr(13) + ex.Message, MsgBoxStyle.Critical, "Error")
				Exit Sub
			End Try
		Else
			Try
				objTablas.fac_ProspectacionClientesUpdate(entCliente)
			Catch ex As Exception
				MsgBox("Ocurrio un problema al actualizar la ficha del prospecto de cliente" + Chr(13) + ex.Message, MsgBoxStyle.Critical, "Error")
				Exit Sub
			End Try
		End If

		MsgBox("Registro guardado exitosamente", MsgBoxStyle.Information, "NOTA")

		xtcClientes.SelectedTabPage = xtpLista
		gc.DataSource = objTablas.fac_ProspectacionClientesSelectAll
		gc.Focus()
		MostrarModoInicial()
		ActivaControles(False)
	End Sub
	Private Sub fac_frmClientes_Revertir() Handles Me.Revertir
		xtcClientes.SelectedTabPage = xtpLista
		gc.Focus()
		ActivaControles(False)
	End Sub
	Private Sub fac_frmClientes_Edit_Click() Handles Me.Editar
		entCliente = objTablas.fac_ProspectacionClientesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"))
		CargaPantalla()
		Me.Text = "Cliente: " & entCliente.NombreCliente
		DbMode = DbModeType.update
		ActivaControles(True)

	End Sub
	Private Sub ActivaControles(ByVal Tipo As Boolean)
		For Each ctrl In xtpDatos.Controls
			If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
				CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
			End If
		Next
		gv.OptionsBehavior.Editable = Tipo
		teCorrelativo.Properties.ReadOnly = True
	End Sub
	Private Sub CargaPantalla()

		xtcClientes.SelectedTabPage = xtpDatos
		teNombre.Focus()
		With entCliente
			teCorrelativo.EditValue = .IdComprobante
			teNombre.EditValue = .NombreCliente
			teRazonSocial.EditValue = .RazonSocial
			teNRC.EditValue = .Nrc
			teNIT.EditValue = .Nit
			teGiro.EditValue = .Giro
			teTelefonos.EditValue = .Telefonos
			teDireccion.EditValue = .Direccion
			teCorreoElectronico.EditValue = .Email
			leDepartamento.EditValue = .IdDepartamento
			leMunicipio.EditValue = .IdMunicipio
			leVendedor.EditValue = .IdVendedor
			teOtroDocumento.EditValue = .Dui
			teAsunto.EditValue = .Asunto
			teReferidoPor.EditValue = .ReferidoPor
			deFechaContacto.EditValue = .FechaContacto
			teLugarContacto.EditValue = .LugarContacto

		End With
	End Sub
	Private Sub CargaEntidad()
		With entCliente
			.IdComprobante = teCorrelativo.EditValue
			.NombreCliente = teNombre.EditValue
			.RazonSocial = teRazonSocial.EditValue
			.Nrc = teNRC.EditValue
			.Nit = teNIT.EditValue
			.Giro = teGiro.EditValue
			.Telefonos = teTelefonos.EditValue
			.Direccion = teDireccion.EditValue
			.IdDepartamento = leDepartamento.EditValue
			.IdMunicipio = leMunicipio.EditValue
			.IdVendedor = leVendedor.EditValue
			.Dui = teOtroDocumento.EditValue
			.LugarContacto = teLugarContacto.EditValue
			.Asunto = teAsunto.EditValue
			.FechaContacto = deFechaContacto.EditValue
			.ReferidoPor = teReferidoPor.EditValue

			If DbMode = DbModeType.insert Then
				.FechaHoraCreacion = Now
				.CreadoPor = objMenu.User
				.ModificadoPor = ""
			Else
				.FechaHoraModificacion = Now
				.ModificadoPor = objMenu.User
			End If



		End With
	End Sub
	Private Sub leDepto_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leDepartamento.EditValueChanged
		objCombos.admMunicipios(leMunicipio, leDepartamento.EditValue)
	End Sub
	Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc.DoubleClick
		entCliente = objTablas.fac_ProspectacionClientesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"))
		CargaPantalla()
		Me.Text = "Cliente: " & entCliente.NombreCliente
		DbMode = DbModeType.update

	End Sub
	Function DatosValidos() As Boolean
		If teNombre.EditValue = "" OrElse leVendedor.EditValue = 0 OrElse teTelefonos.EditValue = "" Then
			MsgBox("Existen datos que no pueden quedar en blanco" & Chr(13) & "Verifique [Nombre, Vendedor, teléfonos]", MsgBoxStyle.Critical, "Nota")
			Return False
		End If

		Return True
	End Function
	Private Sub fac_frmClientes_Report_Click() Handles Me.Reporte
		Dim rpt As New cpc_rptListClientes
		rpt.xrlEmpresa.Text = gsNombre_Empresa
		rpt.xrlTitulo.Text = "Listado de Prospectos de Clientes"
		rpt.DataSource = objTablas.fac_ProspectacionClientesSelectAll
		rpt.DataMember = ""
		rpt.ShowPreview()
	End Sub






End Class
