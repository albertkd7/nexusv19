﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmInfoAdicionalCliente
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmInfoAdicionalCliente))
        Me.LayoutConverter1 = New DevExpress.XtraLayout.Converter.LayoutConverter(Me.components)
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.sbGuardarCambios = New DevExpress.XtraEditors.SimpleButton()
        Me.sbSalir = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpGeneral = New DevExpress.XtraTab.XtraTabPage()
        Me.chkPersonaJuridica = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.chkApnfd = New DevExpress.XtraEditors.CheckEdit()
        Me.meObservaciones = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teIngresosBrutos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl91 = New DevExpress.XtraEditors.LabelControl()
        Me.teEstimadoCompras = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.xtpDatosCliente = New DevExpress.XtraTab.XtraTabPage()
        Me.leRubro = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl68 = New DevExpress.XtraEditors.LabelControl()
        Me.leActividad = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.leProfesion = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.xtpRepLegal = New DevExpress.XtraTab.XtraTabPage()
        Me.teTelefonoMovilRL = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl80 = New DevExpress.XtraEditors.LabelControl()
        Me.teCorreoElectronicoRL = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl62 = New DevExpress.XtraEditors.LabelControl()
        Me.teTelefonoRL = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.teDireccionRL = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoDocumento = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.leMunicipioRL = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.leDepartamentoRL = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl60 = New DevExpress.XtraEditors.LabelControl()
        Me.leProfesionRL = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.teNITRL = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumDocRL = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombreRepLegal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.xtpGeneral.SuspendLayout()
        CType(Me.chkPersonaJuridica.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkApnfd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meObservaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIngresosBrutos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teEstimadoCompras.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatosCliente.SuspendLayout()
        CType(Me.leRubro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leActividad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leProfesion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpRepLegal.SuspendLayout()
        CType(Me.teTelefonoMovilRL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorreoElectronicoRL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTelefonoRL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDireccionRL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoDocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leMunicipioRL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leDepartamentoRL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leProfesionRL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNITRL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumDocRL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombreRepLegal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.sbGuardarCambios)
        Me.PanelControl1.Controls.Add(Me.sbSalir)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl1.Location = New System.Drawing.Point(0, 473)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(821, 33)
        Me.PanelControl1.TabIndex = 23
        '
        'sbGuardarCambios
        '
        Me.sbGuardarCambios.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbGuardarCambios.Appearance.Options.UseFont = True
        Me.sbGuardarCambios.Location = New System.Drawing.Point(287, 6)
        Me.sbGuardarCambios.Name = "sbGuardarCambios"
        Me.sbGuardarCambios.Size = New System.Drawing.Size(120, 23)
        Me.sbGuardarCambios.TabIndex = 25
        Me.sbGuardarCambios.Text = "Guardar Cambios"
        '
        'sbSalir
        '
        Me.sbSalir.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbSalir.Appearance.Options.UseFont = True
        Me.sbSalir.Location = New System.Drawing.Point(450, 6)
        Me.sbSalir.Name = "sbSalir"
        Me.sbSalir.Size = New System.Drawing.Size(69, 23)
        Me.sbSalir.TabIndex = 24
        Me.sbSalir.Text = "Salir"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.xtpGeneral
        Me.XtraTabControl1.Size = New System.Drawing.Size(821, 473)
        Me.XtraTabControl1.TabIndex = 124
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpGeneral, Me.xtpDatosCliente, Me.xtpRepLegal})
        '
        'xtpGeneral
        '
        Me.xtpGeneral.Controls.Add(Me.chkPersonaJuridica)
        Me.xtpGeneral.Controls.Add(Me.LabelControl3)
        Me.xtpGeneral.Controls.Add(Me.chkApnfd)
        Me.xtpGeneral.Controls.Add(Me.meObservaciones)
        Me.xtpGeneral.Controls.Add(Me.LabelControl2)
        Me.xtpGeneral.Controls.Add(Me.LabelControl1)
        Me.xtpGeneral.Controls.Add(Me.teIngresosBrutos)
        Me.xtpGeneral.Controls.Add(Me.LabelControl91)
        Me.xtpGeneral.Controls.Add(Me.teEstimadoCompras)
        Me.xtpGeneral.Controls.Add(Me.LabelControl40)
        Me.xtpGeneral.Name = "xtpGeneral"
        Me.xtpGeneral.Size = New System.Drawing.Size(815, 445)
        Me.xtpGeneral.Text = "Información General"
        '
        'chkPersonaJuridica
        '
        Me.chkPersonaJuridica.Location = New System.Drawing.Point(393, 4)
        Me.chkPersonaJuridica.Name = "chkPersonaJuridica"
        Me.chkPersonaJuridica.Properties.Caption = "Persona Jurídica?"
        Me.chkPersonaJuridica.Size = New System.Drawing.Size(104, 19)
        Me.chkPersonaJuridica.TabIndex = 148
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(180, 45)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(225, 11)
        Me.LabelControl3.TabIndex = 147
        Me.LabelControl3.Text = "(Actividades y Profesiones no Financieras Designadas)"
        '
        'chkApnfd
        '
        Me.chkApnfd.Location = New System.Drawing.Point(178, 25)
        Me.chkApnfd.Name = "chkApnfd"
        Me.chkApnfd.Properties.Caption = "APNFD?"
        Me.chkApnfd.Size = New System.Drawing.Size(65, 19)
        Me.chkApnfd.TabIndex = 146
        '
        'meObservaciones
        '
        Me.meObservaciones.Location = New System.Drawing.Point(178, 125)
        Me.meObservaciones.Name = "meObservaciones"
        Me.meObservaciones.Size = New System.Drawing.Size(319, 96)
        Me.meObservaciones.TabIndex = 145
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(98, 126)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl2.TabIndex = 144
        Me.LabelControl2.Text = "Observaciones:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(4, 7)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(169, 13)
        Me.LabelControl1.TabIndex = 143
        Me.LabelControl1.Text = "Ingresos Brutos Promedio Mensual:"
        '
        'teIngresosBrutos
        '
        Me.teIngresosBrutos.EditValue = 0
        Me.teIngresosBrutos.EnterMoveNextControl = True
        Me.teIngresosBrutos.Location = New System.Drawing.Point(177, 3)
        Me.teIngresosBrutos.Name = "teIngresosBrutos"
        Me.teIngresosBrutos.Properties.Appearance.Options.UseTextOptions = True
        Me.teIngresosBrutos.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teIngresosBrutos.Properties.Mask.EditMask = "n2"
        Me.teIngresosBrutos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teIngresosBrutos.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teIngresosBrutos.Size = New System.Drawing.Size(101, 20)
        Me.teIngresosBrutos.TabIndex = 142
        '
        'LabelControl91
        '
        Me.LabelControl91.Location = New System.Drawing.Point(33, 95)
        Me.LabelControl91.Name = "LabelControl91"
        Me.LabelControl91.Size = New System.Drawing.Size(140, 13)
        Me.LabelControl91.TabIndex = 141
        Me.LabelControl91.Text = "Monto Estimado en Compras:"
        '
        'teEstimadoCompras
        '
        Me.teEstimadoCompras.EditValue = 0
        Me.teEstimadoCompras.EnterMoveNextControl = True
        Me.teEstimadoCompras.Location = New System.Drawing.Point(177, 90)
        Me.teEstimadoCompras.Name = "teEstimadoCompras"
        Me.teEstimadoCompras.Properties.Appearance.Options.UseTextOptions = True
        Me.teEstimadoCompras.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teEstimadoCompras.Properties.Mask.EditMask = "n2"
        Me.teEstimadoCompras.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teEstimadoCompras.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teEstimadoCompras.Size = New System.Drawing.Size(101, 20)
        Me.teEstimadoCompras.TabIndex = 139
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl40.Appearance.Options.UseFont = True
        Me.LabelControl40.Location = New System.Drawing.Point(177, 68)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(228, 16)
        Me.LabelControl40.TabIndex = 140
        Me.LabelControl40.Text = "COMPRAS PROYECTADAS EN EL MES"
        '
        'xtpDatosCliente
        '
        Me.xtpDatosCliente.Controls.Add(Me.leRubro)
        Me.xtpDatosCliente.Controls.Add(Me.LabelControl68)
        Me.xtpDatosCliente.Controls.Add(Me.leActividad)
        Me.xtpDatosCliente.Controls.Add(Me.LabelControl42)
        Me.xtpDatosCliente.Controls.Add(Me.leProfesion)
        Me.xtpDatosCliente.Controls.Add(Me.LabelControl33)
        Me.xtpDatosCliente.Name = "xtpDatosCliente"
        Me.xtpDatosCliente.Size = New System.Drawing.Size(815, 445)
        Me.xtpDatosCliente.Text = "Datos Persona Natural"
        '
        'leRubro
        '
        Me.leRubro.EnterMoveNextControl = True
        Me.leRubro.Location = New System.Drawing.Point(167, 28)
        Me.leRubro.Name = "leRubro"
        Me.leRubro.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leRubro.Size = New System.Drawing.Size(320, 20)
        Me.leRubro.TabIndex = 125
        '
        'LabelControl68
        '
        Me.LabelControl68.Location = New System.Drawing.Point(4, 31)
        Me.LabelControl68.Name = "LabelControl68"
        Me.LabelControl68.Size = New System.Drawing.Size(159, 13)
        Me.LabelControl68.TabIndex = 129
        Me.LabelControl68.Text = "Rubro de la Actividad Económica:"
        '
        'leActividad
        '
        Me.leActividad.EnterMoveNextControl = True
        Me.leActividad.Location = New System.Drawing.Point(167, 50)
        Me.leActividad.Name = "leActividad"
        Me.leActividad.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leActividad.Size = New System.Drawing.Size(320, 20)
        Me.leActividad.TabIndex = 126
        '
        'LabelControl42
        '
        Me.LabelControl42.Location = New System.Drawing.Point(20, 53)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(143, 13)
        Me.LabelControl42.TabIndex = 128
        Me.LabelControl42.Text = "Actividad Económica Principal:"
        '
        'leProfesion
        '
        Me.leProfesion.EnterMoveNextControl = True
        Me.leProfesion.Location = New System.Drawing.Point(167, 7)
        Me.leProfesion.Name = "leProfesion"
        Me.leProfesion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leProfesion.Size = New System.Drawing.Size(320, 20)
        Me.leProfesion.TabIndex = 124
        '
        'LabelControl33
        '
        Me.LabelControl33.Location = New System.Drawing.Point(114, 11)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(49, 13)
        Me.LabelControl33.TabIndex = 127
        Me.LabelControl33.Text = "Profesión:"
        '
        'xtpRepLegal
        '
        Me.xtpRepLegal.Controls.Add(Me.teTelefonoMovilRL)
        Me.xtpRepLegal.Controls.Add(Me.LabelControl80)
        Me.xtpRepLegal.Controls.Add(Me.teCorreoElectronicoRL)
        Me.xtpRepLegal.Controls.Add(Me.LabelControl62)
        Me.xtpRepLegal.Controls.Add(Me.teTelefonoRL)
        Me.xtpRepLegal.Controls.Add(Me.LabelControl25)
        Me.xtpRepLegal.Controls.Add(Me.teDireccionRL)
        Me.xtpRepLegal.Controls.Add(Me.LabelControl20)
        Me.xtpRepLegal.Controls.Add(Me.leTipoDocumento)
        Me.xtpRepLegal.Controls.Add(Me.LabelControl8)
        Me.xtpRepLegal.Controls.Add(Me.leMunicipioRL)
        Me.xtpRepLegal.Controls.Add(Me.LabelControl11)
        Me.xtpRepLegal.Controls.Add(Me.leDepartamentoRL)
        Me.xtpRepLegal.Controls.Add(Me.LabelControl60)
        Me.xtpRepLegal.Controls.Add(Me.leProfesionRL)
        Me.xtpRepLegal.Controls.Add(Me.LabelControl4)
        Me.xtpRepLegal.Controls.Add(Me.teNITRL)
        Me.xtpRepLegal.Controls.Add(Me.LabelControl49)
        Me.xtpRepLegal.Controls.Add(Me.teNumDocRL)
        Me.xtpRepLegal.Controls.Add(Me.LabelControl30)
        Me.xtpRepLegal.Controls.Add(Me.teNombreRepLegal)
        Me.xtpRepLegal.Controls.Add(Me.LabelControl26)
        Me.xtpRepLegal.Name = "xtpRepLegal"
        Me.xtpRepLegal.Size = New System.Drawing.Size(815, 445)
        Me.xtpRepLegal.Text = "Datos del Representante Legal"
        '
        'teTelefonoMovilRL
        '
        Me.teTelefonoMovilRL.EnterMoveNextControl = True
        Me.teTelefonoMovilRL.Location = New System.Drawing.Point(132, 142)
        Me.teTelefonoMovilRL.Name = "teTelefonoMovilRL"
        Me.teTelefonoMovilRL.Properties.Mask.EditMask = "0000-0000"
        Me.teTelefonoMovilRL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.teTelefonoMovilRL.Size = New System.Drawing.Size(183, 20)
        Me.teTelefonoMovilRL.TabIndex = 213
        '
        'LabelControl80
        '
        Me.LabelControl80.Location = New System.Drawing.Point(56, 145)
        Me.LabelControl80.Name = "LabelControl80"
        Me.LabelControl80.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl80.TabIndex = 218
        Me.LabelControl80.Text = "Teléfono movil:"
        '
        'teCorreoElectronicoRL
        '
        Me.teCorreoElectronicoRL.EnterMoveNextControl = True
        Me.teCorreoElectronicoRL.Location = New System.Drawing.Point(518, 164)
        Me.teCorreoElectronicoRL.Name = "teCorreoElectronicoRL"
        Me.teCorreoElectronicoRL.Size = New System.Drawing.Size(224, 20)
        Me.teCorreoElectronicoRL.TabIndex = 214
        '
        'LabelControl62
        '
        Me.LabelControl62.Location = New System.Drawing.Point(422, 167)
        Me.LabelControl62.Name = "LabelControl62"
        Me.LabelControl62.Size = New System.Drawing.Size(92, 13)
        Me.LabelControl62.TabIndex = 217
        Me.LabelControl62.Text = "Correo Electrónico:"
        '
        'teTelefonoRL
        '
        Me.teTelefonoRL.EnterMoveNextControl = True
        Me.teTelefonoRL.Location = New System.Drawing.Point(518, 142)
        Me.teTelefonoRL.Name = "teTelefonoRL"
        Me.teTelefonoRL.Properties.Mask.EditMask = "0000-0000"
        Me.teTelefonoRL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.teTelefonoRL.Size = New System.Drawing.Size(224, 20)
        Me.teTelefonoRL.TabIndex = 212
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(450, 145)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl25.TabIndex = 216
        Me.LabelControl25.Text = "Teléfono fijo:"
        '
        'teDireccionRL
        '
        Me.teDireccionRL.EnterMoveNextControl = True
        Me.teDireccionRL.Location = New System.Drawing.Point(132, 119)
        Me.teDireccionRL.Name = "teDireccionRL"
        Me.teDireccionRL.Size = New System.Drawing.Size(610, 20)
        Me.teDireccionRL.TabIndex = 211
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(82, 122)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl20.TabIndex = 215
        Me.LabelControl20.Text = "Dirección:"
        '
        'leTipoDocumento
        '
        Me.leTipoDocumento.EnterMoveNextControl = True
        Me.leTipoDocumento.Location = New System.Drawing.Point(132, 30)
        Me.leTipoDocumento.Name = "leTipoDocumento"
        Me.leTipoDocumento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoDocumento.Size = New System.Drawing.Size(183, 20)
        Me.leTipoDocumento.TabIndex = 197
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(34, 33)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(96, 13)
        Me.LabelControl8.TabIndex = 210
        Me.LabelControl8.Text = "Tipo de Documento:"
        '
        'leMunicipioRL
        '
        Me.leMunicipioRL.EnterMoveNextControl = True
        Me.leMunicipioRL.Location = New System.Drawing.Point(518, 97)
        Me.leMunicipioRL.Name = "leMunicipioRL"
        Me.leMunicipioRL.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leMunicipioRL.Size = New System.Drawing.Size(224, 20)
        Me.leMunicipioRL.TabIndex = 203
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(467, 99)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl11.TabIndex = 209
        Me.LabelControl11.Text = "Municipio:"
        '
        'leDepartamentoRL
        '
        Me.leDepartamentoRL.EnterMoveNextControl = True
        Me.leDepartamentoRL.Location = New System.Drawing.Point(132, 97)
        Me.leDepartamentoRL.Name = "leDepartamentoRL"
        Me.leDepartamentoRL.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leDepartamentoRL.Size = New System.Drawing.Size(183, 20)
        Me.leDepartamentoRL.TabIndex = 202
        '
        'LabelControl60
        '
        Me.LabelControl60.Location = New System.Drawing.Point(56, 100)
        Me.LabelControl60.Name = "LabelControl60"
        Me.LabelControl60.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl60.TabIndex = 208
        Me.LabelControl60.Text = "Departamento:"
        '
        'leProfesionRL
        '
        Me.leProfesionRL.EnterMoveNextControl = True
        Me.leProfesionRL.Location = New System.Drawing.Point(132, 52)
        Me.leProfesionRL.Name = "leProfesionRL"
        Me.leProfesionRL.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leProfesionRL.Size = New System.Drawing.Size(610, 20)
        Me.leProfesionRL.TabIndex = 201
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(41, 55)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(88, 13)
        Me.LabelControl4.TabIndex = 207
        Me.LabelControl4.Text = "Profesión u Oficio:"
        '
        'teNITRL
        '
        Me.teNITRL.EnterMoveNextControl = True
        Me.teNITRL.Location = New System.Drawing.Point(132, 74)
        Me.teNITRL.Name = "teNITRL"
        Me.teNITRL.Properties.Mask.EditMask = "0000-000000-000-0"
        Me.teNITRL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.teNITRL.Size = New System.Drawing.Size(183, 20)
        Me.teNITRL.TabIndex = 200
        '
        'LabelControl49
        '
        Me.LabelControl49.Location = New System.Drawing.Point(107, 77)
        Me.LabelControl49.Name = "LabelControl49"
        Me.LabelControl49.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl49.TabIndex = 204
        Me.LabelControl49.Text = "NIT:"
        '
        'teNumDocRL
        '
        Me.teNumDocRL.EnterMoveNextControl = True
        Me.teNumDocRL.Location = New System.Drawing.Point(608, 30)
        Me.teNumDocRL.Name = "teNumDocRL"
        Me.teNumDocRL.Size = New System.Drawing.Size(134, 20)
        Me.teNumDocRL.TabIndex = 199
        '
        'LabelControl30
        '
        Me.LabelControl30.Location = New System.Drawing.Point(530, 33)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl30.TabIndex = 205
        Me.LabelControl30.Text = "No. Documento:"
        '
        'teNombreRepLegal
        '
        Me.teNombreRepLegal.EnterMoveNextControl = True
        Me.teNombreRepLegal.Location = New System.Drawing.Point(132, 7)
        Me.teNombreRepLegal.Name = "teNombreRepLegal"
        Me.teNombreRepLegal.Size = New System.Drawing.Size(610, 20)
        Me.teNombreRepLegal.TabIndex = 196
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(16, 10)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(112, 13)
        Me.LabelControl26.TabIndex = 206
        Me.LabelControl26.Text = "Nombre del Rep. Legal:"
        '
        'fac_frmInfoAdicionalCliente
        '
        Me.ClientSize = New System.Drawing.Size(821, 506)
        Me.ControlBox = False
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fac_frmInfoAdicionalCliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Información Adicional del Cliente"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.xtpGeneral.ResumeLayout(False)
        Me.xtpGeneral.PerformLayout()
        CType(Me.chkPersonaJuridica.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkApnfd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meObservaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIngresosBrutos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teEstimadoCompras.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatosCliente.ResumeLayout(False)
        Me.xtpDatosCliente.PerformLayout()
        CType(Me.leRubro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leActividad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leProfesion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpRepLegal.ResumeLayout(False)
        Me.xtpRepLegal.PerformLayout()
        CType(Me.teTelefonoMovilRL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorreoElectronicoRL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTelefonoRL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDireccionRL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoDocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leMunicipioRL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leDepartamentoRL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leProfesionRL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNITRL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumDocRL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombreRepLegal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutConverter1 As DevExpress.XtraLayout.Converter.LayoutConverter
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents sbSalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbGuardarCambios As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpGeneral As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkApnfd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents meObservaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIngresosBrutos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl91 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teEstimadoCompras As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents xtpDatosCliente As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents leRubro As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl68 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leActividad As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leProfesion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents xtpRepLegal As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents teTelefonoMovilRL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl80 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCorreoElectronicoRL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl62 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTelefonoRL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDireccionRL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoDocumento As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leMunicipioRL As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leDepartamentoRL As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl60 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leProfesionRL As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNITRL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumDocRL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombreRepLegal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkPersonaJuridica As DevExpress.XtraEditors.CheckEdit

End Class
