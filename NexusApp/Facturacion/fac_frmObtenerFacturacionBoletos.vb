Imports NexusELL.TableEntities
Imports NexusBLL
Imports System.IO
Public Class fac_frmObtenerFacturacionBoletos
    Dim dtDetalle As New DataTable
    'Dim bl As New CuentasPCBLL(g_ConnectionString)
    Dim blfac As New FacturaBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParametros As DataTable = blAdmon.ObtieneParametros()
    Dim FacturaHeader As fac_Ventas, FacturaDetalle As List(Of fac_VentasDetalle)
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim IdSucUser As Integer = SiEsNulo(EntUsuario.IdSucursal, 1)
    Dim bl As New FacturaBLL(g_ConnectionString), fd As New FuncionesBLL(g_ConnectionString)

    Private _IdCliente As System.String
    Public Property IdCliente() As System.String
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As System.String)
            _IdCliente = value
        End Set
    End Property

    Private _TotalAnticipos As System.Decimal
    Public Property TotalAnticipos() As System.Decimal
        Get
            Return _TotalAnticipos
        End Get
        Set(ByVal value As System.Decimal)
            _TotalAnticipos = value
        End Set
    End Property

    Private _TipoVenta As System.Int32
    Public Property TipoVenta() As System.Int32
        Get
            Return _TipoVenta
        End Get
        Set(ByVal value As System.Int32)
            _TipoVenta = value
        End Set
    End Property


    Private _Aceptar As Boolean = False

    Public Property Aceptar() As Boolean
        Get
            Return _Aceptar
        End Get
        Set(ByVal value As Boolean)
            _Aceptar = value
        End Set
    End Property

    Private _DocumentoDetallaIva As Boolean = False

    Public Property DocumentoDetallaIva() As Boolean
        Get
            Return _DocumentoDetallaIva
        End Get
        Set(ByVal value As Boolean)
            _DocumentoDetallaIva = value
        End Set
    End Property


    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        LlenarEntidades()
        Aceptar = True
        Close()
    End Sub


    Private Sub sbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCancel.Click
        TotalAnticipos = 0.0
        Aceptar = False
        Close()
    End Sub


    Private Sub fac_frmMostrarCambio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        InicializaGrid()
    End Sub


    Private Sub LlenarEntidades()
        Dim frmFacturacion As fac_frmFacturacion = Me.Owner
        Dim ldummy As Boolean = False
        Dim Concepto As String = ""
        Dim AsignarNumeroAutomatico As Boolean = objMenu.User.StartsWith("DIGITADOR") = False
        FacturaHeader = New fac_Ventas
        'FacturaDetalle = New List(Of fac_VentasDetalle)

        For i = 0 To gvFP.DataRowCount - 1
            'Dim DetalleFac As New fac_Ventas
            With FacturaHeader

                'If IdSucUser = 0 Then
                '    .IdSucursal = piIdSucursal
                'Else
                '    .IdSucursal = IdSucUser
                'End If

                .Numero = gvFP.GetRowCellValue(i, "Numero") 'teNumero.EditValue
                .NumFormularioUnico = "" ' SiEsNulo(teNumeroUnico.EditValue, "")
                .Serie = "" 'Serie
                .IdSucursal = piIdSucursal
                If gvFP.GetRowCellValue(i, "IdPunto") = "BSP" Then
                    .IdPunto = 2
                ElseIf gvFP.GetRowCellValue(i, "IdPunto") = "ATO" Then
                    .IdPunto = 1
                ElseIf gvFP.GetRowCellValue(i, "IdPunto") = "CAG" Then
                    .IdPunto = 3
                ElseIf gvFP.GetRowCellValue(i, "IdPunto") = "CTO" Then
                    .IdPunto = 4
                ElseIf gvFP.GetRowCellValue(i, "IdPunto") = "OTR" Then
                    .IdPunto = 5
                End If

                .IdTipoComprobante = 6 'leTipoComprobante.EditValue
                Dim fec As String = gvFP.GetRowCellValue(i, "Fecha")
                Dim fec2 As Date = CDate(fec.Substring(6, 2) + "/" + fec.Substring(4, 2) + "/" + fec.Substring(0, 4))
                .Fecha = fec2
                .IdCliente = ".." 'beCodCliente.EditValue
                .Nombre = "CLIENTE COMUN AEROPUERTO" 'teNombre.EditValue
                .Nrc = "" 'teNRC.EditValue
                .Nit = gvFP.GetRowCellValue(i, "Nit") 'teNit.EditValue
                .Giro = "" 'teGiro.EditValue
                .Direccion = "SS" 'teDireccion.EditValue
                .VentaAcuentaDe = "" 'teVentaCuentaDe.EditValue
                .IdMunicipio = "0614" 'leMunicipio.EditValue
                .IdDepartamento = "06" 'leDepartamento.EditValue
                .IdFormaPago = 1 'leFormaPago.EditValue
                .DiasCredito = 0 'seDiasCredito.Value
                .IdVendedor = 1 'leVendedor.EditValue
                .TipoVenta = 1 'leTipoImpuesto.EditValue
                .PorcDescto = 0 'sePjeDescuento.EditValue
                .EsDevolucion = 0 'ceDevolucion.EditValue
                .Telefono = "" 'teNotaRemision.EditValue
                .TipoImpuesto = 0 'rgTipo.EditValue
                .Telefono = "" 'teTelefonos.EditValue

                ' If TipoAplicacion <> 1 Or ceDevolucion.EditValue = True Then  'cuando es nota de cr�dito, fac. de devolucion y nota de envio de devolucion
                .EsDevolucion = False
                'End If
                .TotalDescto = 0 'teDescuento.EditValue * TipoAplicacion
                .TotalComprobante = gvFP.GetRowCellValue(i, "TotalAfecto") 'teTotal.EditValue * TipoAplicacion
                .TotalIva = gvFP.GetRowCellValue(i, "TotalIva") 'teIVA.EditValue * TipoAplicacion
                .TotalImpuesto1 = 0 ' teRetencionPercepcion.EditValue * TipoAplicacion
                .TotalImpuesto2 = 0 ' teCESC.EditValue
                .TotalNoSujeto = gvFP.GetRowCellValue(i, "TotalNoSujeto") 'gcVentaNoSujeta.SummaryItem.SummaryValue * TipoAplicacion
                .TotalExento = gvFP.GetRowCellValue(i, "TotalExento") ' gcVentaExenta.SummaryItem.SummaryValue * TipoAplicacion
                .TotalAfecto = gvFP.GetRowCellValue(i, "TotalAfecto") 'gcVentaAfecta.SummaryItem.SummaryValue * TipoAplicacion
                .TotalNeto = gvFP.GetRowCellValue(i, "TotalAfecto") - gvFP.GetRowCellValue(i, "TotalIva")  'gcVentaNeta.SummaryItem.SummaryValue * TipoAplicacion
                .TotalPagado = 0.0
                .SaldoActual = gvFP.GetRowCellValue(i, "TotalNoSujeto") 'teTotal.EditValue * TipoAplicacion
                .IdSucursalEnvio = piIdSucursal 'leSucursalEnvio.EditValue
                'TODO Falta llenar estos valores
                .IdComprobanteNota = 0
                .OrdenCompra = "" 'teNumPedido.EditValue
                .IdBodega = 1 'leBodega.EditValue
                .MotivoAnulacion = ""
                .AnuladoPor = ""
                .AutorizoDescuento = ""
                .Comentario = "BOLETO IMPORTADO MEDIANTE CARGA DE ARCHIVO CSV" 'meComentario.EditValue
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
            End With
            'FacturaHeader.Add(DetalleFac)

            'objTablas.fac_VentasInsert(FacturaHeader)

            FacturaDetalle = New List(Of fac_VentasDetalle)
            'FacturaDetalle = New fac_VentasDetalle

            ' For j = 0 To gvFP.DataRowCount - 1
            Dim entDetalle As New fac_VentasDetalle
            With entDetalle
                .IdComprobante = FacturaHeader.IdComprobante
                .IdProducto = "01" 'gv.GetRowCellValue(i, "IdProducto")
                .IdPrecio = 1 'gv.GetRowCellValue(i, "IdPrecio")
                .Cantidad = 1 'gv.GetRowCellValue(i, "Cantidad") * TipoAplicacion
                .Descripcion = "CARGA DE BOLETOS" 'gv.GetRowCellValue(i, "Descripcion")
                .PrecioVenta = 0 'gv.GetRowCellValue(i, "PrecioVenta") * TipoAplicacion
                .PorcDescuento = 0 'gv.GetRowCellValue(i, "PorcDescuento")
                .ValorDescuento = 0 ' gv.GetRowCellValue(i, "ValorDescuento") * TipoAplicacion
                .PrecioUnitario = 0 'gv.GetRowCellValue(i, "PrecioUnitario") * TipoAplicacion
                .VentaNoSujeta = gvFP.GetRowCellValue(i, "TotalNoSujeto") ' * TipoAplicacion
                .VentaExenta = gvFP.GetRowCellValue(i, "TotalExento") ' * TipoAplicacion
                .VentaAfecta = gvFP.GetRowCellValue(i, "TotalAfecto") ' * TipoAplicacion
                .VentaNeta = gvFP.GetRowCellValue(i, "TotalAfecto") ' * TipoAplicacion
                .TipoImpuesto = 0 ' leTipoImpuesto.EditValue
                .ValorIVA = gvFP.GetRowCellValue(i, "TotalIva") ' * TipoAplicacion
                .PrecioCosto = 0.0 'SiEsNulo(gv.GetRowCellValue(i, "PrecioCosto"), 0.0)
                .PorcComision = 0.0 'SiEsNulo(blInve.inv_ObtenerComisionProducto(.IdProducto), 0)
                .DescuentoAutorizadoPor = "" 'SiEsNulo(gv.GetRowCellValue(i, "DescuentoAutorizadoPor"), "")
                .IdCentro = "" 'SiEsNulo(gv.GetRowCellValue(i, "IdCentro"), "")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .IdDetalle = i + 1
            End With
            FacturaDetalle.Add(entDetalle)
            'objTablas.fac_VentasDetalleInsert(entDetalle)
            'Next

            Dim msj As String = bl.fac_InsertaFacturaBoletos(FacturaHeader, FacturaDetalle, AsignarNumeroAutomatico)
        Next

        MsgBox("Boletos importados exit�samente", MsgBoxStyle.Information, "Nota")

    End Sub

    Private Sub sbImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbImportar.Click
        
        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            Return
        End If
        Dim csv_file As StreamReader = File.OpenText(ofd.FileName)
        Try
            While csv_file.Peek() > 0
                Dim sLine As String = csv_file.ReadLine()
                Dim aData As Array = sLine.Split(",")

                gvFP.AddNewRow()
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Fecha", aData(0))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "IdPunto", aData(1))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Numero", aData(2))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalNoSujeto", aData(3))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalExento", SiEsNulo(aData(4), 0))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalAfecto", SiEsNulo(aData(5), 0))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalIva", SiEsNulo(aData(6), 0))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Nit", SiEsNulo(aData(7), ""))
                gvFP.UpdateCurrentRow()

            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Return
        End Try

        csv_file.Close()

    End Sub

    Private Sub InicializaGrid()
        dtDetalle = New DataTable
        dtDetalle.Columns.Add(New DataColumn("Fecha", GetType(String)))
        dtDetalle.Columns.Add(New DataColumn("IdPunto", GetType(String)))
        dtDetalle.Columns.Add(New DataColumn("Numero", GetType(String)))
        dtDetalle.Columns.Add(New DataColumn("TotalNoSujeto", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("TotalExento", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("TotalAfecto", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("TotalIva", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("Nit", GetType(String)))
        'dtDetalle.Columns.Add(New DataColumn("IdComprobante", GetType(Integer)))
        gcFP.DataSource = dtDetalle
    End Sub

End Class