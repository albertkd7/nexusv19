﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmAutorizarDescuento
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.beProducto = New Nexus.beProducto()
        Me.txtContraseña = New DevExpress.XtraEditors.TextEdit()
        Me.lblContraseña = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.seHastaDescto = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.seDesdeDescto = New DevExpress.XtraEditors.SpinEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.sePjeDescuento = New DevExpress.XtraEditors.SpinEdit()
        Me.sbGenerar = New DevExpress.XtraEditors.SimpleButton()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtContraseña.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seHastaDescto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDesdeDescto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sePjeDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.beProducto)
        Me.GroupControl1.Controls.Add(Me.txtContraseña)
        Me.GroupControl1.Controls.Add(Me.lblContraseña)
        Me.GroupControl1.Controls.Add(Me.LabelControl19)
        Me.GroupControl1.Controls.Add(Me.seHastaDescto)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.seDesdeDescto)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.LabelControl26)
        Me.GroupControl1.Controls.Add(Me.sePjeDescuento)
        Me.GroupControl1.Controls.Add(Me.sbGenerar)
        Me.GroupControl1.Controls.Add(Me.deFecha)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(695, 418)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Generar Contraseña de Descuento"
        '
        'beProducto
        '
        Me.beProducto.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.beProducto.Appearance.Options.UseFont = True
        Me.beProducto.Location = New System.Drawing.Point(139, 157)
        Me.beProducto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.beProducto.Name = "beProducto"
        Me.beProducto.Size = New System.Drawing.Size(761, 25)
        Me.beProducto.TabIndex = 166
        Me.beProducto.Visible = False
        '
        'txtContraseña
        '
        Me.txtContraseña.Location = New System.Drawing.Point(223, 288)
        Me.txtContraseña.Name = "txtContraseña"
        Me.txtContraseña.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContraseña.Properties.Appearance.Options.UseFont = True
        Me.txtContraseña.Properties.ReadOnly = True
        Me.txtContraseña.Size = New System.Drawing.Size(230, 52)
        Me.txtContraseña.TabIndex = 165
        '
        'lblContraseña
        '
        Me.lblContraseña.Appearance.Font = New System.Drawing.Font("Tahoma", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContraseña.Appearance.Options.UseFont = True
        Me.lblContraseña.Location = New System.Drawing.Point(89, 292)
        Me.lblContraseña.Name = "lblContraseña"
        Me.lblContraseña.Size = New System.Drawing.Size(129, 45)
        Me.lblContraseña.TabIndex = 164
        Me.lblContraseña.Text = "TOKEN:"
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(365, 87)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(45, 19)
        Me.LabelControl19.TabIndex = 163
        Me.LabelControl19.Text = "Hasta:"
        Me.LabelControl19.Visible = False
        '
        'seHastaDescto
        '
        Me.seHastaDescto.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seHastaDescto.EnterMoveNextControl = True
        Me.seHastaDescto.Location = New System.Drawing.Point(424, 84)
        Me.seHastaDescto.Name = "seHastaDescto"
        Me.seHastaDescto.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seHastaDescto.Properties.Appearance.Options.UseFont = True
        Me.seHastaDescto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seHastaDescto.Properties.Mask.EditMask = "P2"
        Me.seHastaDescto.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seHastaDescto.Properties.ReadOnly = True
        Me.seHastaDescto.Size = New System.Drawing.Size(127, 26)
        Me.seHastaDescto.TabIndex = 161
        Me.seHastaDescto.Visible = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(38, 87)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(181, 19)
        Me.LabelControl2.TabIndex = 162
        Me.LabelControl2.Text = "Rango Autorizado Desde:"
        Me.LabelControl2.Visible = False
        '
        'seDesdeDescto
        '
        Me.seDesdeDescto.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDesdeDescto.EnterMoveNextControl = True
        Me.seDesdeDescto.Location = New System.Drawing.Point(223, 84)
        Me.seDesdeDescto.Name = "seDesdeDescto"
        Me.seDesdeDescto.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.seDesdeDescto.Properties.Appearance.Options.UseFont = True
        Me.seDesdeDescto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDesdeDescto.Properties.Mask.EditMask = "P2"
        Me.seDesdeDescto.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seDesdeDescto.Properties.ReadOnly = True
        Me.seDesdeDescto.Size = New System.Drawing.Size(127, 26)
        Me.seDesdeDescto.TabIndex = 160
        Me.seDesdeDescto.Visible = False
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(223, 186)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.leSucursal.Properties.Appearance.Options.UseFont = True
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(328, 26)
        Me.leSucursal.TabIndex = 156
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(71, 189)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(148, 19)
        Me.LabelControl10.TabIndex = 159
        Me.LabelControl10.Text = "Sucursal a Autorizar:"
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl26.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl26.Appearance.Options.UseFont = True
        Me.LabelControl26.Appearance.Options.UseForeColor = True
        Me.LabelControl26.Location = New System.Drawing.Point(40, 132)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(176, 19)
        Me.LabelControl26.TabIndex = 158
        Me.LabelControl26.Text = "ULTIMO PRECIO VENTA"
        Me.LabelControl26.Visible = False
        '
        'sePjeDescuento
        '
        Me.sePjeDescuento.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.sePjeDescuento.EnterMoveNextControl = True
        Me.sePjeDescuento.Location = New System.Drawing.Point(223, 129)
        Me.sePjeDescuento.Name = "sePjeDescuento"
        Me.sePjeDescuento.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sePjeDescuento.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.sePjeDescuento.Properties.Appearance.Options.UseFont = True
        Me.sePjeDescuento.Properties.Appearance.Options.UseForeColor = True
        Me.sePjeDescuento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.sePjeDescuento.Properties.Mask.EditMask = "n2"
        Me.sePjeDescuento.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.sePjeDescuento.Size = New System.Drawing.Size(127, 26)
        Me.sePjeDescuento.TabIndex = 155
        Me.sePjeDescuento.Visible = False
        '
        'sbGenerar
        '
        Me.sbGenerar.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sbGenerar.Appearance.Options.UseFont = True
        Me.sbGenerar.Location = New System.Drawing.Point(223, 238)
        Me.sbGenerar.Name = "sbGenerar"
        Me.sbGenerar.Size = New System.Drawing.Size(116, 44)
        Me.sbGenerar.TabIndex = 157
        Me.sbGenerar.Text = "Generar"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(223, 52)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.deFecha.Properties.Appearance.Options.UseFont = True
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(127, 26)
        Me.deFecha.TabIndex = 153
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(173, 55)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(46, 19)
        Me.LabelControl1.TabIndex = 154
        Me.LabelControl1.Text = "Fecha:"
        '
        'fac_frmAutorizarDescuento
        '
        Me.ClientSize = New System.Drawing.Size(695, 443)
        Me.Controls.Add(Me.GroupControl1)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmAutorizarDescuento"
        Me.OptionId = "002009"
        Me.Text = "Autorización de Descuentos"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.txtContraseña.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seHastaDescto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDesdeDescto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sePjeDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents beProducto As Nexus.beProducto
    Friend WithEvents txtContraseña As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblContraseña As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seHastaDescto As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seDesdeDescto As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sePjeDescuento As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents sbGenerar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl

End Class
