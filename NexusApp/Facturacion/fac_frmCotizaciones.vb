﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Imports System.Math
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo

Public Class fac_frmCotizaciones
    Dim myBL As New FacturaBLL(g_ConnectionString)
    Dim blInve As New InventarioBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim entCotiza As fac_Cotizaciones
    Dim entCotizaDetalle As List(Of fac_CotizacionesDetalle)
    Dim Precio As Decimal = 0.0, entProducto As inv_Productos, dtParametros As DataTable = blAdmon.ObtieneParametros, TipoPrecio As Integer = 1
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User), DocumentoDetallaIva As Boolean, TipoVenta As Integer = 1
    Dim EntVendedor As fac_Vendedores = objTablas.fac_VendedoresSelectByPK(EntUsuario.IdVendedor)
    Dim IdTipoPrecioUser As Integer = SiEsNulo(EntUsuario.IdTipoPrecio, 0), TotalGral As Decimal = 0.0, LimiteLineas As Integer = 0, TipoAplicacion = 0
    Dim nxDirectory As String = System.Configuration.ConfigurationManager.AppSettings("NxAPICloudFiles:Directory")
    Dim nxDisabled As Boolean = IIf(System.Configuration.ConfigurationManager.AppSettings("NxAPICloudFiles:Disabled") = "True", True, False)
    Private editor As BaseEdit
    Dim ExisteRCV As Boolean = True

    Private Sub fac_frmCotizaciones_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        objCombos.fac_Vendedores(leVendedor)
        objCombos.fac_FormasPago(leFormaPago)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        objCombos.fac_TiposImpuesto(leTipoVenta)
        objCombos.fac_TiposComprobante(leTipoDoc, "")

        ActivaControles(False)
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("FAC_COTIZACIONES", "IdComprobante")
        If dtParametros.Rows(0).Item("TipoContribuyente") = 3 And dtParametros.Rows(0).Item("EsRetenedor") Then
            ceRetencionPercepcion.Text = "APLICAR PERCEPCIÓN DEL 1 %"
            lblPercReten.Text = "(+) IVA PERCIBIDO: "
        End If
        ' para el caso de los clientes que no tenia este cambio, estara nulo este campo
        Me.gcDescripcion.OptionsColumn.ReadOnly = Not SiEsNulo(EntUsuario.AccesoProductos, True)
        gc2.DataSource = myBL.fac_ConsultaCotizaciones(objMenu.User, IIf(EntVendedor.AllRCV = False, EntVendedor.IdVendedor, 0)).Tables(0)

        ExisteRCV = myBL.fac_ExisteRCV()
    End Sub

    Private Sub fac_frmPedidos_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = myBL.fac_ConsultaCotizaciones(objMenu.User, IIf(EntVendedor.AllRCV = False, EntVendedor.IdVendedor, 0)).Tables(0)
        gv2.BestFitColumns()
    End Sub

    Private Sub fac_frmCotizaciones_Nuevo() Handles Me.Nuevo
        ' este codigo se debe de poner siempre al dar nuevo
        entCotiza = New fac_Cotizaciones
        gv.CancelUpdateCurrentRow()
        deFecha.EditValue = Today
        beCliente.EditValue = ""
        meNombre.EditValue = ""
        teAtencion.EditValue = ""
        teDireccion.EditValue = ""
        teNRC.EditValue = ""
        teTiempo.EditValue = ""
        teProyecto.EditValue = ""
        teGarantia.EditValue = ""
        meConcepto.EditValue = ""
        ceImprimePrecios.EditValue = True
        leTipoVenta.EditValue = 1
        leSucursal.EditValue = piIdSucursalUsuario
        leTipoDoc.EditValue = 6
        ceRetencionPercepcion.EditValue = False
        teTotal.EditValue = 0.0
        teIVA.EditValue = 0.0
        teRetencionPercepcion.EditValue = 0.0
        leFormaPago.EditValue = 1
        leVendedor.EditValue = 1

        teNumero.EditValue = (objFunciones.ObtenerUltimoId("FAC_COTIZACIONES", "IdComprobante") + 1).ToString.PadLeft(6, "0")
        gc.DataSource = myBL.fac_ObtenerDetalleDocumento("Fac_CotizacionesDetalle", -1)
        gv.CancelUpdateCurrentRow()
        'gv.AddNewRow()
        gv.FocusedColumn = gv.Columns(0)

        ActivaControles(True)
        teCorrelativo.Focus()

        xtcCotizaciones.SelectedTabPage = xtpDatos
    End Sub
    Private Sub beIdCliente_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCliente.ButtonClick
        beCliente.EditValue = ""
        beIdCliente_Validated(beCliente, New EventArgs)
    End Sub
    Private Sub beIdCliente_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCliente.Validated

        Dim cquery As String = IIf(ExisteRCV And EntVendedor.AllRCV = False And EntUsuario.IdVendedor > 0, " IdCliente IN(SELECT IdCliente FROM fac_VendedoresClientes   WHERE IdVendedor = " & EntUsuario.IdVendedor & ") ", "")

        Dim entCliente As fac_Clientes = objConsultas.cnsClientes(fac_frmConsultaClientes, beCliente.EditValue, cquery)
        If entCliente.IdCliente = "" Then
            beCliente.EditValue = ""
            beCliente.Focus()
            Exit Sub
        End If

        Dim _AplicaRetencion As Boolean = If(entCliente.TipoImpuestoAdicional >= 2, True, False)

        With entCliente
            beCliente.EditValue = .IdCliente
            meNombre.EditValue = .Nombre
            teDireccion.EditValue = .Direccion
            teNRC.EditValue = .Nrc
            seDiasCredito.EditValue = .DiasCredito
            leVendedor.EditValue = .IdVendedor
            leFormaPago.EditValue = .IdFormaPago
            leTipoVenta.EditValue = .IdTipoImpuesto
            leTipoDoc.EditValue = .IdTipoComprobante
            ceRetencionPercepcion.EditValue = _AplicaRetencion
            ceRetencionPercepcion.Checked = _AplicaRetencion
            If .IdPrecio > 0 Then 'si el cliente tiene definido un precio, se tomará como predeterminado
                TipoPrecio = .IdPrecio
            Else ' de lo contrario se tomará el precio global de facturación
                If IdTipoPrecioUser = 0 Then
                    TipoPrecio = dtParametros.Rows(0).Item("IdTipoPrecio")
                Else
                    TipoPrecio = IdTipoPrecioUser
                End If

            End If
        End With
    End Sub
    Private Sub fac_frmCotizaciones_Guardar() Handles Me.Guardar
        If Not DatosValidos() Then
            Exit Sub
        End If
        CargaEntidades()
        Dim msj As String = ""
        If DbMode = DbModeType.insert Then
            msj = myBL.fac_InsertarCotizacion(entCotiza, entCotizaDetalle)
            If msj = "" Then
                MsgBox("La cotización ha sido guardada con éxito", 64, "Nota")
            Else
                MsgBox(String.Format("NO FUE POSIBLE GUARDAR LA COTIZACIÓN{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
                Exit Sub
            End If
            teCorrelativo.EditValue = entCotiza.IdComprobante
        Else
            msj = myBL.fac_ActualizarCotizacion(entCotiza, entCotizaDetalle)
            If msj = "" Then
                MsgBox("La cotización ha sido actualizada con éxito", 64, "Nota")
            Else
                MsgBox(String.Format("NO FUE POSIBLE ACTUALIZAR LA COTIZACIÓN{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If
        xtcCotizaciones.SelectedTabPage = xtpLista
        gc2.DataSource = myBL.fac_ConsultaCotizaciones(objMenu.User).Tables(0)
        gc2.Focus()
        MostrarModoInicial()
        ActivaControles(False)
    End Sub
    'Private Sub fac_frmCotizaciones_Consulta_Click() Handles Me.Consulta
    '    'teCorrelativo.EditValue = objConsultas.ConsultaCotizaciones(frmConsultaDetalle)
    '    'CargaControles(0)
    'End Sub

    Private Sub fac_frmCotizaciones_Editar() Handles Me.Editar
        ActivaControles(True)
        teNumero.Focus()
    End Sub
    Private Sub fac_frmCotizaciones_Delete_Click() Handles Me.Eliminar
        entCotiza = objTablas.fac_CotizacionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        If MsgBox("Está seguro(a) de eliminar ésta cotización?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            entCotiza.ModificadoPor = objMenu.User
            objTablas.fac_CotizacionesUpdate(entCotiza)
            objTablas.fac_CotizacionesDeleteByPK(entCotiza.IdComprobante)
            gc2.DataSource = myBL.fac_ConsultaCotizaciones(objMenu.User).Tables(0)
        End If
    End Sub

    Private Sub fac_frmCotizaciones_Undo_Click() Handles Me.Revertir
        xtcCotizaciones.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivaControles(False)
    End Sub


    Private Sub CargaEntidades()
        gv.UpdateTotalSummary()

        With entCotiza
            .Numero = teNumero.EditValue
            .Fecha = deFecha.EditValue
            .IdVendedor = leVendedor.EditValue
            .Nombre = meNombre.EditValue
            .AtencionA = teAtencion.Text
            .IdFormaPago = leFormaPago.EditValue
            .IdCliente = beCliente.EditValue
            .Direccion = teDireccion.Text
            .Nrc = teNRC.EditValue
            .DiasCredito = seDiasCredito.EditValue
            .TiempoEntrega = teTiempo.Text
            .Proyecto = teProyecto.EditValue
            .Garantia = teGarantia.EditValue
            .Concepto = meConcepto.EditValue
            .IdSucursal = leSucursal.EditValue
            .ImprimirPrecios = ceImprimePrecios.EditValue
            .TipoVenta = leTipoVenta.EditValue
            .IdTipoComprobante = leTipoDoc.EditValue
            .AplicaRetencionPercepcion = ceRetencionPercepcion.EditValue
            .TotalAfecto = Me.gcPrecioTotal.SummaryItem.SummaryValue
            .TotalIva = teIVA.EditValue
            .TotalImpuesto1 = teRetencionPercepcion.EditValue
            .TotalComprobante = teTotal.EditValue

            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .FechaHoraModificacion = Nothing
                .ModificadoPor = ""
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If
        End With

        entCotizaDetalle = New List(Of fac_CotizacionesDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New fac_CotizacionesDetalle
            With entDetalle
                entProducto = objTablas.inv_ProductosSelectByPK(gv.GetRowCellValue(i, "IdProducto"))
                .IdComprobante = entCotiza.IdComprobante
                .IdDetalle = i + 1
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .CodigoPublico = gv.GetRowCellValue(i, "IdProducto")
                .IdPrecio = gv.GetRowCellValue(i, "IdPrecio")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Especificaciones = SiEsNulo(gv.GetRowCellValue(i, "Especificaciones"), "")
                .ArchivoImagen = SiEsNulo(gv.GetRowCellValue(i, "ArchivoImagen"), "")
                If gv.GetRowCellValue(i, "Descripcion") = "-- PRODUCTO NO REGISTRADO --" Or gv.GetRowCellValue(i, "Descripcion") = ", PRODUCTO SIN SALDO --" Then
                    .Descripcion = entProducto.Nombre
                Else
                    .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                End If
                .PrecioVenta = gv.GetRowCellValue(i, "PrecioVenta")
                .PorcDescuento = gv.GetRowCellValue(i, "PorcDescuento")
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .PrecioTotal = gv.GetRowCellValue(i, "PrecioTotal")
                .VentaNeta = gv.GetRowCellValue(i, "VentaNeta")
                .ValorIva = gv.GetRowCellValue(i, "ValorIva")
                .DescuentoAutorizadoPor = gv.GetRowCellValue(i, "DescuentoAutorizadoPor")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            entCotizaDetalle.Add(entDetalle)
        Next

    End Sub

#Region "grid"

    Private Sub gv_HiddenEditor(sender As Object, e As EventArgs) Handles gv.HiddenEditor
        RemoveHandler editor.DoubleClick, AddressOf editor_DoubleClick
        editor = Nothing
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        ' gv.SetRowCellValue(gv.FocusedRowHandle, "IdProducto", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "CodigoPublico", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdPrecio", dtParametros.Rows(0).Item("IdTipoPrecio"))
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioVenta", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PorcDescuento", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "VentaNeta", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorIva", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "DescuentoAutorizadoPor", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Especificaciones", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "ArchivoImagen", "")
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_FocusedColumnChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs) Handles gv.FocusedColumnChanged
        gv.ClearColumnErrors()
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab OrElse e.KeyCode = Keys.Down OrElse e.KeyCode = Keys.Right OrElse e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.Left Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdProducto" Then

                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProduc = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gv.EditingValue = IdProduc
                End If

                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                '''''' valido los precios, por si cambian un codigo, que se refresque el precio automaticamente
                If blInve.inv_VerificaPrecioUsuario(objMenu.User, gv.GetFocusedRowCellValue("IdPrecio")) Then
                    'MsgBox("Tipo de precio no definido o no autorizado para éste usuario", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("IdPrecio", 1)
                End If

                Precio = blInve.inv_ObtienePrecioProducto(gv.EditingValue, gv.GetFocusedRowCellValue("IdPrecio"))

                If leTipoVenta.EditValue <> 1 Or DocumentoDetallaIva Then
                    Precio = Round(Precio / (pnIVA + 1), 4)
                End If

                gv.SetFocusedRowCellValue("PrecioVenta", Precio)
                Dim Descto As Decimal = Decimal.Round(Precio * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)
                gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescuento", Decimal.Round((Descto * gv.GetFocusedRowCellValue("Cantidad")), 2))

                Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Descto
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)

                CalculaTotal(gv.GetFocusedRowCellValue("Cantidad"), Precio)
                'CalcularTotalesGenerales()


                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                    gv.SetFocusedRowCellValue("Especificaciones", entProducto.InformacionAdicional)
                    gv.SetFocusedRowCellValue("ArchivoImagen", entProducto.ArchivoImagen)
                End If

            End If

            If gv.FocusedColumn.FieldName = "IdPrecio" Then

                If SiEsNulo(gv.EditingValue, 0) = 0 Then
                    Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), gv.GetFocusedRowCellValue("IdPrecio")) 'gv.GetFocusedRowCellValue(gv.Columns("IdProducto"))
                Else
                    Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), gv.EditingValue) 'gv.GetFocusedRowCellValue(gv.Columns("IdProducto"))
                End If

                If leTipoVenta.EditValue <> 1 Or DocumentoDetallaIva Then
                    Precio = Round(Precio / (pnIVA + 1), 4)
                End If

                gv.SetFocusedRowCellValue("PrecioVenta", Precio)
                Dim Descto As Decimal = Decimal.Round(Precio * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)
                gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescuento", Decimal.Round((Descto * gv.GetFocusedRowCellValue("Cantidad")), 2))

                Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Descto
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)

                CalculaTotal(gv.GetFocusedRowCellValue("Cantidad"), Precio)
                'CalcularTotalesGenerales()
            End If

            CalcularTotalesGenerales()
        End If
    End Sub

    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        'SOLAMENTE SE VALIDAN VALORES OBLIGATORIOS QUE NO PUEDEN QUEDAR VACIOS AL TRATAR DE ABANDONAR LA FILA
        If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), "") = "" Then
            e.Valid = False
        End If
        Dim IdProduc As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdProducto")), "")
        Dim IdPrecio As Integer = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdPrecio")), 0)

        If IdProduc = "" OrElse Not blInve.inv_VerificaCodigoProducto(IdProduc) Then
            e.Valid = False
            gv.SetColumnError(gv.Columns("IdProducto"), "Código de producto no válido o no existe")
        End If

        If IdPrecio = 0 Then
            e.Valid = False
            gv.SetColumnError(gv.Columns("IdPrecio"), "Debe de especificar el precio")
        End If
        CalcularTotalesGenerales()
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        'gv.SetFocusedRowCellValue(gv.Columns("Cantidad"), SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("Cantidad")), 1.0))

        Select Case gv.FocusedColumn.FieldName
            Case "IdPrecio"
                Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue(gv.Columns("IdProducto")), e.Value) '

                If leTipoVenta.EditValue <> 1 Or DocumentoDetallaIva Then
                    Dim PrecioSinIva As Decimal = Round(Precio / (pnIVA + 1), 4)
                    gv.SetFocusedRowCellValue("PrecioVenta", PrecioSinIva)
                    gv.SetFocusedRowCellValue("PrecioUnitario", PrecioSinIva)
                    CalculaTotal(gv.GetFocusedRowCellValue(gv.Columns("Cantidad")), PrecioSinIva)
                Else ' exento y exportacion
                    gv.SetFocusedRowCellValue("PrecioVenta", Precio)
                    gv.SetFocusedRowCellValue("PrecioUnitario", Precio)
                    CalculaTotal(gv.GetFocusedRowCellValue(gv.Columns("Cantidad")), Precio)
                End If

                Dim Descto As Decimal = Decimal.Round(gv.GetFocusedRowCellValue("PrecioVenta") * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)
                gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescuento", Decimal.Round((Descto * gv.GetFocusedRowCellValue("Cantidad")), 2))

                Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Descto
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)

                CalculaTotal(gv.GetFocusedRowCellValue("Cantidad"), Precio)
                CalcularTotalesGenerales()

            Case "Cantidad"

                Precio = gv.GetFocusedRowCellValue("PrecioVenta")
                Dim Descto As Decimal = Decimal.Round(Precio * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)
                gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescuento", Decimal.Round((Descto * e.Value), 2))
                Precio = Precio - Descto
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)

                CalculaTotal(e.Value, gv.GetFocusedRowCellValue(gv.Columns("PrecioUnitario")))
                CalcularTotalesGenerales()

            Case "PrecioVenta"
                Precio = e.Value
                Dim Descto As Decimal = Decimal.Round(e.Value * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)
                gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescuento", Decimal.Round((Descto * e.Value), 2))
                Precio = Precio - Descto
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)
                CalculaTotal(gv.GetFocusedRowCellValue(gv.Columns("Cantidad")), Precio)
                CalcularTotalesGenerales()
            Case "PorcDescuento"

                If e.Value > 0.0 Then
                    ' si el usuario pone el % mayor o menor del establecido pide autorizacion
                    If e.Value < EntUsuario.DesdeDescuentos Or e.Value > EntUsuario.HastaDescuentos Then
                        frmObtienePassword.Text = "AUTORIZACIÓN DE DESCUENTOS"
                        frmObtienePassword.Usuario = objMenu.User
                        frmObtienePassword.TipoAcceso = 3
                        frmObtienePassword.ShowDialog()

                        If frmObtienePassword.Acceso = False Then
                            MsgBox("Usuario no Autorizado para Aplicar Descuentos", MsgBoxStyle.Critical, "Nota")
                            gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", "")
                            e.Value = 0
                        Else
                            Dim entUsuarioAutoriza As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(SiEsNulo(frmObtienePassword.teUserName.EditValue, ""))
                            If e.Value > entUsuarioAutoriza.HastaDescuentos Then
                                MsgBox("El Usuario no puede autorizar descuento mayor al establecido ", MsgBoxStyle.Critical, "Nota")
                                gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", "")
                                e.Value = 0
                                frmObtienePassword.Usuario = ""
                            End If
                            If e.Value < entUsuarioAutoriza.DesdeDescuentos And e.Value > 0.0 Then
                                MsgBox("El Usuario no puede autorizar descuento menor al establecido ", MsgBoxStyle.Critical, "Nota")
                                gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", "")
                                e.Value = 0
                                frmObtienePassword.Usuario = ""
                            End If
                            'AutorizoDescto = frmObtienePassword.Usuario
                        End If
                        gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", frmObtienePassword.Usuario)
                        frmObtienePassword.Dispose()
                    Else
                        gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", objMenu.User)
                    End If
                End If

                Dim Descto As Decimal = Decimal.Round(gv.GetFocusedRowCellValue("PrecioVenta") * e.Value / 100, 4)
                gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescuento", Decimal.Round((Descto * gv.GetFocusedRowCellValue("Cantidad")), 2))

                Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Descto
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)

                CalculaTotal(gv.GetFocusedRowCellValue(gv.Columns("Cantidad")), Precio)
                CalcularTotalesGenerales()
            Case "PrecioUnitario"
                CalculaTotal(gv.GetFocusedRowCellValue(gv.Columns("Cantidad")), e.Value)
                CalcularTotalesGenerales()
        End Select
    End Sub
    Private Sub CalculaTotal(ByVal Cantidad As Decimal, ByVal Precio As Decimal)
        Dim Total As Decimal = Decimal.Round(Cantidad * Precio, 2)
        gv.SetFocusedRowCellValue("PrecioTotal", Total)

        TipoVenta = leTipoVenta.EditValue
        'cuando el iva esta incluído en la facturacion. El precio del producto está guardado con IVA incluido

        Precio = Decimal.Round(Cantidad * Precio, 2)
        If TipoVenta = 1 Or TipoVenta = 3 Then 'precio gravado o con tasa cero
            gv.SetFocusedRowCellValue("PrecioTotal", Precio)

            If DocumentoDetallaIva Then 'crédito fiscal, nota de crédito, nota de débito
                gv.SetFocusedRowCellValue("VentaNeta", Precio)
                gv.SetFocusedRowCellValue("ValorIva", Decimal.Round(Precio * pnIVA, 2))
            Else 'ticket, consumidor final, factura de exportación
                If leTipoDoc.EditValue = 7 Then
                    gv.SetFocusedRowCellValue("VentaNeta", gv.GetFocusedRowCellValue(gv.Columns("PrecioTotal")))
                    gv.SetFocusedRowCellValue("ValorIva", 0.0)
                Else
                    Precio = Decimal.Round(Precio / (pnIVA + 1), 2) '4
                    gv.SetFocusedRowCellValue("VentaNeta", Precio)
                    gv.SetFocusedRowCellValue("ValorIva", gv.GetFocusedRowCellValue(gv.Columns("PrecioTotal")) - Precio)
                End If

            End If

            If TipoVenta = 3 Then
                gv.SetFocusedRowCellValue("ValorIva", 0.0)
            End If

        End If

        If TipoVenta = 2 Then 'precio exento
            gv.SetFocusedRowCellValue("PrecioTotal", Precio)
            gv.SetFocusedRowCellValue("VentaNeta", Precio)
            gv.SetFocusedRowCellValue("ValorIva", 0.0)
        End If

    End Sub
    Private Sub CalcularTotalesGenerales()
        gv.UpdateTotalSummary()
        TotalGral = gcPrecioTotal.SummaryItem.SummaryValue

        'obtener el tipo de documentos para efectos de calcular el IVA
        If DocumentoDetallaIva Then
            teIVA.EditValue = Me.gcValorIva.SummaryItem.SummaryValue 'Decimal.Round(Me.gcPrecioTotal.SummaryItem.SummaryValue * pnIVA, 2)
        Else
            If leTipoDoc.EditValue = 7 Then  'FACTURACION DE EXPORTACION 
                teIVA.EditValue = 0.0
            Else
                teIVA.EditValue = Me.gcValorIva.SummaryItem.SummaryValue
            End If
        End If

        Dim IvaRetPer As Decimal = 0.0
        If ceRetencionPercepcion.Checked Then
            If dtParametros.Rows(0).Item("TipoContribuyente") = 3 And dtParametros.Rows(0).Item("EsRetenedor") Then
                If IIf(DocumentoDetallaIva, Me.gcPrecioTotal.SummaryItem.SummaryValue >= 100, Me.gcVentaNeta.SummaryItem.SummaryValue >= 100) Then
                    If DocumentoDetallaIva Then
                        teRetencionPercepcion.EditValue = Decimal.Round(Me.gcPrecioTotal.SummaryItem.SummaryValue * CDec(dtParametros.Rows(0).Item("PorcPercepcion") / 100), 2)
                    Else
                        teRetencionPercepcion.EditValue = Decimal.Round(Me.gcVentaNeta.SummaryItem.SummaryValue * CDec(dtParametros.Rows(0).Item("PorcPercepcion") / 100), 2)
                    End If

                    IvaRetPer = teRetencionPercepcion.EditValue
                Else
                    teRetencionPercepcion.EditValue = 0.0
                End If
            Else
                If IIf(DocumentoDetallaIva, Me.gcPrecioTotal.SummaryItem.SummaryValue >= 100, Me.gcVentaNeta.SummaryItem.SummaryValue >= 100) Then
                    If DocumentoDetallaIva Then
                        teRetencionPercepcion.EditValue = Decimal.Round(Me.gcPrecioTotal.SummaryItem.SummaryValue * CDec(dtParametros.Rows(0).Item("PorcRetencion") / 100), 2)
                    Else
                        teRetencionPercepcion.EditValue = Decimal.Round(Me.gcVentaNeta.SummaryItem.SummaryValue * CDec(dtParametros.Rows(0).Item("PorcRetencion") / 100), 2)
                    End If

                    IvaRetPer = -teRetencionPercepcion.EditValue
                Else
                    teRetencionPercepcion.EditValue = 0.0
                End If
            End If
        Else
            teRetencionPercepcion.EditValue = 0.0
        End If

        If DocumentoDetallaIva Then 'crédito fiscal, nota de crédito, nota de débito
            teTotal.EditValue = Me.gcPrecioTotal.SummaryItem.SummaryValue + teIVA.EditValue + IvaRetPer
            TotalGral = teTotal.EditValue
        Else
            teTotal.EditValue = Me.gcPrecioTotal.SummaryItem.SummaryValue + IvaRetPer
            TotalGral = teTotal.EditValue
        End If

        If EntUsuario.CambiarPrecios Then
            leTipoDoc.Properties.ReadOnly = TotalGral <> 0.0
        End If

        leTipoVenta.Properties.ReadOnly = TotalGral <> 0.0
        'leTipoDoc.Properties.ReadOnly = TotalGral <> 0.0
    End Sub
#End Region

    Private Function DatosValidos() As Boolean

        Dim msg As String = ""

        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        For i = 0 To gv.DataRowCount - 1
            If blInve.inv_ObtieneTipoProducto(SiEsNulo(gv.GetRowCellValue(i, "IdProducto"), "")) > 1 Then 'PARA TODOS LOS SERVICIOS NO APLICA
                Exit For
            End If

            ' valido que no se valla un producto sin precio unitario
            If gv.GetRowCellValue(i, "PrecioUnitario") = 0.0 Then
                msg = "El producto " & gv.GetRowCellValue(i, "IdProducto") & ", Tiene Precio Unitario Cero"
                Exit For
            End If
        Next
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.None

        If teNumero.EditValue = "" Then
            msg = "Debe de especificar el numero de cotización"
            Return False
        End If
        If meNombre.EditValue = "" Then
            msg = "Debe de especificar el nombre del cliente"
            Return False
        End If

        If msg = "" Then
            Return True
        Else
            MsgBox(msg, MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If
    End Function

    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
        teCorrelativo.Properties.ReadOnly = True
    End Sub

    Private Sub fac_frmCotizaciones_Reporte() Handles Me.Reporte
        entCotiza = objTablas.fac_CotizacionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))

        Dim dt As DataTable = myBL.fac_ObtenerDetalleCotizacion(entCotiza.IdComprobante)
        Dim entCliente As fac_Clientes = objTablas.fac_ClientesSelectByPK(entCotiza.IdCliente)
        Dim entSucursal As adm_Sucursales = objTablas.adm_SucursalesSelectByPK(entCotiza.IdSucursal)
        Dim entForma As fac_FormasPago = objTablas.fac_FormasPagoSelectByPK(entCotiza.IdFormaPago)
        Dim entVendedor As fac_Vendedores = objTablas.fac_VendedoresSelectByPK(entCotiza.IdVendedor)
        Dim entTipos As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(entCotiza.IdTipoComprobante)
        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)
        Dim Template = Application.StartupPath & "\Plantillas\ctNexus" & NumFormato & ".repx"
        Dim rpt As New fac_rptCotizacion
        If Not FileIO.FileSystem.FileExists(Template) Then
            MsgBox("No existe la plantilla necesaria para el documento" & Chr(13) & "Se imprimirá en formato estándar", MsgBoxStyle.Critical, "Nota")
        Else
            rpt.LoadLayout(Template)
        End If

        rpt.DataSource = dt
        rpt.DataMember = ""

        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlNumero.Text = teNumero.Text
        rpt.xrlCodigo.Text = entCotiza.IdCliente
        rpt.xrlNombre.Text = entCotiza.Nombre
        rpt.xrlAtencionA.Text = entCotiza.AtencionA
        rpt.xrlSucursal.Text = entSucursal.Nombre
        rpt.xrlFecha.Text = entCotiza.Fecha
        rpt.xrlNit.Text = entCotiza.Nit
        rpt.xrlNrc.Text = entCotiza.Nrc
        rpt.xrlForma.Text = entForma.Nombre
        rpt.xrlDireccion.Text = entCotiza.Direccion
        rpt.xrlDias.Text = entCotiza.DiasCredito
        rpt.xrlTiempo.Text = entCotiza.TiempoEntrega
        rpt.xrlVendedor.Text = entVendedor.Nombre
        rpt.xrlProyecto.Text = entCotiza.Proyecto
        rpt.xrlGarantia.Text = entCotiza.Garantia
        rpt.xrlConcepto.Text = entCotiza.Concepto
        rpt.xrlTelefono.Text = entCliente.Telefonos

        If entTipos.DetallaIVA Then
            rpt.xrlIva.Text = Format(entCotiza.TotalIva, "$###,###.00")
        Else
            rpt.xrlIva.Text = Format(0, "$###,###.00")
            rpt.xrlIva.Visible = False
        End If

        rpt.xrlRetencion.Text = Format(entCotiza.TotalImpuesto1, "$###,###.00")
        rpt.xrlTotalFinal.Text = Format(entCotiza.TotalComprobante, "$###,###.00")
        rpt.xrlDetallaIVA.Visible = entTipos.DetallaIVA

        If Not entCotiza.ImprimirPrecios Then
            rpt.XrLabel3.Visible = False
            rpt.XrLabel28.Visible = False
        End If

        rpt.xrlDias.Visible = leFormaPago.EditValue > 1
        rpt.XrLabel54.Visible = leFormaPago.EditValue > 1
        ''rpt.xrLogo.Image = ByteToImagen(dtParametros.Rows(0).Item("LogoEmpresa"))

        Dim LoadFromServer As Boolean = IIf(String.IsNullOrEmpty(nxDirectory) = False And nxDisabled, True, False)

        If dt.Rows.Count > 0 Then
            If LoadFromServer Then
                rpt.XrPict1.Image = dt.Rows(0).Item("ArchivoImagen").ToString().BuscarImagenEnServidor(False)
            Else
                rpt.XrPict1.ImageUrl = dt.Rows(0).Item("ArchivoImagen")
            End If
        End If
        If dt.Rows.Count > 1 Then
            If LoadFromServer Then
                rpt.XrPict2.Image = dt.Rows(1).Item("ArchivoImagen").ToString().BuscarImagenEnServidor(False)
            Else
                rpt.XrPict2.ImageUrl = dt.Rows(1).Item("ArchivoImagen")
            End If
        End If
        If dt.Rows.Count > 2 Then
            If LoadFromServer Then
                rpt.XrPict3.Image = dt.Rows(2).Item("ArchivoImagen").ToString().BuscarImagenEnServidor(False)
            Else
                rpt.XrPict3.ImageUrl = dt.Rows(2).Item("ArchivoImagen")
            End If
        End If
        If dt.Rows.Count > 3 Then
            If LoadFromServer Then
                rpt.XrPict4.Image = dt.Rows(3).Item("ArchivoImagen").ToString().BuscarImagenEnServidor(False)
            Else
                rpt.XrPict4.ImageUrl = dt.Rows(3).Item("ArchivoImagen")
            End If
        End If
        If dt.Rows.Count > 4 Then
            If LoadFromServer Then
                rpt.XrPict5.Image = dt.Rows(4).Item("ArchivoImagen").ToString().BuscarImagenEnServidor(False)
            Else
                rpt.XrPict5.ImageUrl = dt.Rows(4).Item("ArchivoImagen")
            End If
        End If
        If dt.Rows.Count > 5 Then
            If LoadFromServer Then
                rpt.XrPict6.Image = dt.Rows(5).Item("ArchivoImagen").ToString().BuscarImagenEnServidor(False)
            Else
                rpt.XrPict6.ImageUrl = dt.Rows(5).Item("ArchivoImagen")
            End If
        End If
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteRow(gv.FocusedRowHandle)
        gv.UpdateTotalSummary()
        CalcularTotalesGenerales()
    End Sub

    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        CalcularTotalesGenerales()
    End Sub

    Private Sub leFormaPago_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leFormaPago.EditValueChanged
        If leFormaPago.EditValue = 1 Then
            seDiasCredito.EditValue = 0
            seDiasCredito.Enabled = False
        Else
            seDiasCredito.Enabled = True
        End If
    End Sub

    Private Sub leTipoDoc_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles leTipoDoc.EditValueChanged
        myBL.fac_TipoAplicacionComprobante(leTipoDoc.EditValue, DocumentoDetallaIva, TipoAplicacion, LimiteLineas)

        If leTipoDoc.EditValue = 7 Then
            leTipoVenta.EditValue = 3
        Else
            leTipoVenta.EditValue = 1
        End If
        If DocumentoDetallaIva Then
            lblNRC_DUI.Text = "NRC:"
        Else
            lblNRC_DUI.Text = "DUI:"
        End If
        For i = 0 To gv.DataRowCount - 1
            entProducto = objTablas.inv_ProductosSelectByPK(gv.GetRowCellValue(i, "IdProducto"))

            If EntUsuario.CambiarPrecios = False And entProducto.TipoProducto = 1 Then
                Dim Precio As Decimal = 0.0
                Precio = blInve.inv_ObtienePrecioProducto(gv.GetRowCellValue(i, "IdProducto"), gv.GetRowCellValue(i, "IdPrecio"))

                If leTipoVenta.EditValue <> 1 Or DocumentoDetallaIva Then
                    Precio = Round(Precio / (pnIVA + 1), 4)
                End If

                gv.SetRowCellValue(i, "PrecioUnitario", Precio)
                Precio = Decimal.Round(gv.GetRowCellValue(i, "Cantidad") * Precio, 2)
                gv.SetRowCellValue(i, "PrecioTotal", Precio)

                If DocumentoDetallaIva Then 'crédito fiscal, nota de crédito, nota de débito
                    gv.SetRowCellValue(i, "VentaNeta", Precio)
                    gv.SetRowCellValue(i, "ValorIva", Decimal.Round(Precio * pnIVA, 2))
                Else 'ticket, consumidor final, factura de exportación
                    If leTipoDoc.EditValue = 7 Then
                        gv.SetRowCellValue(i, "VentaNeta", gv.GetRowCellValue(i, gv.Columns("PrecioTotal")))
                        gv.SetRowCellValue(i, "ValorIva", 0.0)
                    Else
                        Precio = Decimal.Round(Precio / (pnIVA + 1), 2) ' tenia 4
                        gv.SetRowCellValue(i, "VentaNeta", Precio)
                        gv.SetRowCellValue(i, "ValorIva", gv.GetRowCellValue(i, gv.Columns("PrecioTotal")) - Precio)
                    End If

                End If
                CalcularTotalesGenerales()
            End If
        Next
    End Sub


    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        entCotiza = objTablas.fac_CotizacionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        DbMode = DbModeType.update
        teNumero.Focus()
    End Sub
    Private Sub CargaPantalla()
        xtcCotizaciones.SelectedTabPage = xtpDatos
        teNumero.Focus()

        With entCotiza
            teCorrelativo.EditValue = .IdComprobante
            teNumero.EditValue = .Numero
            deFecha.EditValue = .Fecha
            beCliente.EditValue = .IdCliente
            meNombre.EditValue = .Nombre
            teNRC.EditValue = .Nrc
            teAtencion.EditValue = .AtencionA
            teDireccion.EditValue = .Direccion
            seDiasCredito.EditValue = .DiasCredito
            teTiempo.EditValue = .TiempoEntrega
            leFormaPago.EditValue = .IdFormaPago
            leVendedor.EditValue = .IdVendedor
            teTiempo.EditValue = .TiempoEntrega
            teProyecto.EditValue = .Proyecto
            teGarantia.EditValue = .Garantia
            meConcepto.EditValue = .Concepto
            ceImprimePrecios.EditValue = .ImprimirPrecios
            leTipoVenta.EditValue = .TipoVenta
            leTipoDoc.EditValue = .IdTipoComprobante
            ceRetencionPercepcion.EditValue = .AplicaRetencionPercepcion
            teIVA.EditValue = .TotalIva
            teRetencionPercepcion.EditValue = .TotalImpuesto1
            teTotal.EditValue = teTotal.EditValue
            leSucursal.EditValue = .IdSucursal
            leFormaPago.EditValue = .IdFormaPago
            gc.DataSource = myBL.fac_ObtenerDetalleDocumento("fac_CotizacionesDetalle", .IdComprobante)
        End With
        myBL.fac_TipoAplicacionComprobante(leTipoDoc.EditValue, DocumentoDetallaIva, TipoAplicacion, LimiteLineas)
        CalcularTotalesGenerales()
    End Sub

    Private Sub ceRetencionPercepcion_CheckedChanged(sender As Object, e As EventArgs) Handles ceRetencionPercepcion.CheckedChanged
        CalcularTotalesGenerales()
    End Sub



    Private Sub gv_ShownEditor(sender As Object, e As EventArgs) Handles gv.ShownEditor
        Dim view As GridView = TryCast(sender, GridView)
        editor = view.ActiveEditor
        AddHandler editor.DoubleClick, AddressOf editor_DoubleClick
    End Sub

    Private Sub editor_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim editor As BaseEdit = DirectCast(sender, BaseEdit)
        Dim grid As GridControl = TryCast(editor.Parent, GridControl)
        Dim view As GridView = TryCast(grid.FocusedView, GridView)
        'Dim pt As Point = grid.PointToClient(Control.MousePosition)
        'Dim info As GridHitInfo = view.CalcHitInfo(pt)

        'If info.InRow OrElse info.InRowCell Then
        '    Dim colCaption As String = If(info.Column Is Nothing, "N/A", info.Column.GetCaption())
        '    MessageBox.Show(String.Format("DoubleClick on row: {0}, column: {1}.", info.RowHandle, colCaption))
        'End If

        fac_frmEspecificaciones.Tipo = Not deFecha.Properties.ReadOnly
        fac_frmEspecificaciones.lbProducto.Text = gv.GetFocusedRowCellValue("Descripcion")
        fac_frmEspecificaciones.meEspecificaciones.EditValue = gv.GetFocusedRowCellValue("Especificaciones")
        fac_frmEspecificaciones.beImagen.EditValue = gv.GetFocusedRowCellValue("ArchivoImagen")
        fac_frmEspecificaciones.ShowDialog()

        If fac_frmEspecificaciones.Aceptar Then
            gv.SetRowCellValue(gv.FocusedRowHandle, "Especificaciones", fac_frmEspecificaciones.meEspecificaciones.EditValue)
            gv.SetRowCellValue(gv.FocusedRowHandle, "ArchivoImagen", fac_frmEspecificaciones.beImagen.EditValue)
        End If

    End Sub

    Private Sub gc_DoubleClick(sender As Object, e As EventArgs) Handles gc.DoubleClick
        fac_frmEspecificaciones.Tipo = Not deFecha.Properties.ReadOnly
        fac_frmEspecificaciones.lbProducto.Text = gv.GetFocusedRowCellValue("Descripcion")
        fac_frmEspecificaciones.meEspecificaciones.EditValue = gv.GetFocusedRowCellValue("Especificaciones")
        fac_frmEspecificaciones.beImagen.EditValue = gv.GetFocusedRowCellValue("ArchivoImagen")
        fac_frmEspecificaciones.ShowDialog()

        If fac_frmEspecificaciones.Aceptar Then
            gv.SetRowCellValue(gv.FocusedRowHandle, "Especificaciones", fac_frmEspecificaciones.meEspecificaciones.EditValue)
            gv.SetRowCellValue(gv.FocusedRowHandle, "ArchivoImagen", fac_frmEspecificaciones.beImagen.EditValue)
        End If
    End Sub
End Class
