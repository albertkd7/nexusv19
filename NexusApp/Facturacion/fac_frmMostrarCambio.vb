Public Class fac_frmMostrarCambio 

    Private _Imprimir As Boolean = False
    Public Property Imprimir() As Boolean
        Get
            Return _Imprimir
        End Get
        Set(ByVal value As Boolean)
            _Imprimir = value
        End Set
    End Property


    Private Sub teEfectivo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles teEfectivo.EditValueChanged
        teCambio.EditValue = teEfectivo.EditValue - teTotalFactura.EditValue
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        If teEfectivo.EditValue < teTotalFactura.EditValue Then
            MsgBox("No puede recibir menos efectivo que el total del documento", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        Imprimir = True
        Close()
    End Sub

    Private Sub sbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCancel.Click
        Imprimir = False
        Close()
    End Sub

    Private Sub fac_frmMostrarCambio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        teEfectivo.Focus()
    End Sub
End Class