<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmEspecificaciones
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmEspecificaciones))
        Me.OpenFile = New System.Windows.Forms.OpenFileDialog()
        Me.sbAceptar = New DevExpress.XtraEditors.SimpleButton()
        Me.sbCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.meEspecificaciones = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.peFoto = New DevExpress.XtraEditors.PictureEdit()
        Me.beImagen = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.lbProducto = New DevExpress.XtraEditors.LabelControl()
        Me.lcExisteArchivo = New DevExpress.XtraEditors.LabelControl()
        CType(Me.meEspecificaciones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peFoto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beImagen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OpenFile
        '
        Me.OpenFile.FileName = "OpenFile"
        '
        'sbAceptar
        '
        Me.sbAceptar.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sbAceptar.Appearance.Options.UseFont = True
        Me.sbAceptar.Location = New System.Drawing.Point(8, 313)
        Me.sbAceptar.Name = "sbAceptar"
        Me.sbAceptar.Size = New System.Drawing.Size(95, 29)
        Me.sbAceptar.TabIndex = 1
        Me.sbAceptar.Text = "&Aceptar"
        '
        'sbCancel
        '
        Me.sbCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sbCancel.Appearance.Options.UseFont = True
        Me.sbCancel.Location = New System.Drawing.Point(108, 313)
        Me.sbCancel.Name = "sbCancel"
        Me.sbCancel.Size = New System.Drawing.Size(94, 29)
        Me.sbCancel.TabIndex = 2
        Me.sbCancel.Text = "Salir"
        '
        'meEspecificaciones
        '
        Me.meEspecificaciones.EditValue = ""
        Me.meEspecificaciones.Location = New System.Drawing.Point(4, 45)
        Me.meEspecificaciones.Name = "meEspecificaciones"
        Me.meEspecificaciones.Size = New System.Drawing.Size(501, 263)
        Me.meEspecificaciones.TabIndex = 36
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(5, 9)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl14.TabIndex = 37
        Me.LabelControl14.Text = "Especificaciones Para:"
        '
        'peFoto
        '
        Me.peFoto.Location = New System.Drawing.Point(509, 46)
        Me.peFoto.Name = "peFoto"
        Me.peFoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch
        Me.peFoto.Size = New System.Drawing.Size(310, 297)
        ToolTipTitleItem1.Text = "Imagen del producto"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        Me.peFoto.SuperTip = SuperToolTip1
        Me.peFoto.TabIndex = 40
        Me.peFoto.ToolTipTitle = "Doble clic para cambiar o cargar imagen"
        '
        'beImagen
        '
        Me.beImagen.Location = New System.Drawing.Point(165, 25)
        Me.beImagen.Name = "beImagen"
        Me.beImagen.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beImagen.Size = New System.Drawing.Size(655, 20)
        Me.beImagen.TabIndex = 38
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(4, 28)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(155, 13)
        Me.LabelControl19.TabIndex = 39
        Me.LabelControl19.Text = "Carpeta y archivo de la Imagen:"
        '
        'lbProducto
        '
        Me.lbProducto.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbProducto.Location = New System.Drawing.Point(165, 6)
        Me.lbProducto.Name = "lbProducto"
        Me.lbProducto.Size = New System.Drawing.Size(204, 13)
        Me.lbProducto.TabIndex = 41
        Me.lbProducto.Text = "-DOBLE CLIC PARA CARGAR IMAGEN-"
        '
        'lcExisteArchivo
        '
        Me.lcExisteArchivo.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lcExisteArchivo.Location = New System.Drawing.Point(299, 314)
        Me.lcExisteArchivo.Name = "lcExisteArchivo"
        Me.lcExisteArchivo.Size = New System.Drawing.Size(204, 13)
        Me.lcExisteArchivo.TabIndex = 42
        Me.lcExisteArchivo.Text = "-DOBLE CLIC PARA CARGAR IMAGEN-"
        '
        'fac_frmEspecificaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(824, 346)
        Me.ControlBox = False
        Me.Controls.Add(Me.lcExisteArchivo)
        Me.Controls.Add(Me.lbProducto)
        Me.Controls.Add(Me.peFoto)
        Me.Controls.Add(Me.beImagen)
        Me.Controls.Add(Me.LabelControl19)
        Me.Controls.Add(Me.meEspecificaciones)
        Me.Controls.Add(Me.LabelControl14)
        Me.Controls.Add(Me.sbCancel)
        Me.Controls.Add(Me.sbAceptar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fac_frmEspecificaciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Especificaciones T�cnicas"
        CType(Me.meEspecificaciones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peFoto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beImagen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OpenFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents sbAceptar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents meEspecificaciones As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents peFoto As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents beImagen As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbProducto As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lcExisteArchivo As DevExpress.XtraEditors.LabelControl
End Class
