﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmAnularDocumentos
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim entFactura As New fac_Ventas
    Dim CreaDocumento As Integer = 0
    
    Private Sub fac_frmAnularDocumentos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePunto, leSucursal.EditValue, "")
        leSucursal.EditValue = piIdSucursal
        objCombos.fac_TiposComprobante(leTipo, "")
        sbAnular.Enabled = False
    End Sub

    Private Sub sbObtener_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbObtener.Click
        CreaDocumento = 0
        Dim IdComprobante As Integer = bl.getIdComprobante(leSucursal.EditValue, lePunto.EditValue, leTipo.EditValue, teSerie.EditValue, teNumero.EditValue)
        If IdComprobante = 0 Then
            MsgBox("No se ha emitido ningún documento con ese número", 64, "Nota")

            If MsgBox("¿Desea crear éste documento como anulado?", 292, "Confirme") = MsgBoxResult.No Then
                Exit Sub
            Else
                CreaDocumento = 1
            End If
        End If

        If CreaDocumento = 0 Then
            deFecha.Properties.ReadOnly = True
            entFactura = objTablas.fac_VentasSelectByPK(IdComprobante)
            deFecha.EditValue = entFactura.Fecha
            teNombre.EditValue = entFactura.Nombre
            seTotal.EditValue = entFactura.TotalComprobante
            sbAnular.Enabled = True
        Else
            deFecha.Properties.ReadOnly = False
            LlenaEntidadCreaDoc()
            deFecha.EditValue = entFactura.Fecha
            teNombre.EditValue = entFactura.Nombre
            seTotal.EditValue = entFactura.TotalComprobante
            sbAnular.Enabled = True
        End If

    End Sub

    Private Sub sbAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbAnular.Click
        sbAnular.Enabled = False
        Dim EsOk As Boolean = ValidarFechaCierre(entFactura.Fecha)
        If Not EsOk Then
            MsgBox("Fecha del comprobante fuera del período activo", 16, "Imposible anular")
            Exit Sub
        End If
        If teMotivo.EditValue = "" Then
            MsgBox("Debe de especificar el motivo de anulación del documento", 16, "Nota")
            Exit Sub
        End If
        '*-- verifico si no hay alguna nota de credito o debito que afecte al documento
        Dim sNumero_Nota = SiEsNulo(bl.Verificar_Nota(entFactura.IdComprobante), "")
        If sNumero_Nota <> "" Then
            MsgBox("Debe de anular la nota de crédito que afecta a éste documento" & Chr(13) & _
             "Número de la Nota de Crédito: " + sNumero_Nota, 16, "Imposible Anular")
            Exit Sub
        End If

        '*-- verifico si no hay abonos aplicados
        Dim sFecha = SiEsNulo(bl.Verificar_Abonos(entFactura.IdComprobante), "")
        If sFecha <> "" Then
            MsgBox("Imposible anular éste documento. Ya tiene abonos aplicados" + Chr(13) + _
            "Fecha del abono: " + sFecha, 16, "Imposible Anular")
            Exit Sub
        End If

        '*-- Mensaje de confirmación para anular comprobante
        If entFactura.Anulado Then
            If MsgBox("El Documento esta anulado, ¿Desea eliminarlo totalmente? " + teNumero.EditValue, 292, "Confirme") = MsgBoxResult.No Then
                sbAnular.Enabled = True
                Exit Sub
            End If
        Else
            If MsgBox("¿Está seguro(a) de anular éste documento?  " + teNumero.EditValue, 292, "Confirme") = MsgBoxResult.No Then
                sbAnular.Enabled = True
                Exit Sub
            End If
        End If

        Dim msj As String = ""
        entFactura.Fecha = deFecha.EditValue

        If entFactura.Anulado Then
            msj = bl.fac_AnulaDocumentoVenta(entFactura.IdComprobante, teMotivo.EditValue, Today, objMenu.User, 1, CreaDocumento, entFactura)
        Else
            msj = bl.fac_AnulaDocumentoVenta(entFactura.IdComprobante, teMotivo.EditValue, Today, objMenu.User, 0, CreaDocumento, entFactura)
        End If

        If msj = "" Then
            MsgBox("El documento ha sido anulado", MsgBoxStyle.Information, "Nota")
        Else
            MsgBox("No fue posible anular el documento" + Chr(13) + _
            msj, 16, "Verifique el error")
            Exit Sub
        End If

        teNumero.EditValue = ""
        teNombre.EditValue = ""
        seTotal.EditValue = 0.0
        teMotivo.EditValue = ""
        deFecha.Properties.ReadOnly = True
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePunto, leSucursal.EditValue)
        CargaSerie()
    End Sub
    Private Sub CargaSerie()
        teSerie.EditValue = bl.fac_ObtenerSerieDocumento(leSucursal.EditValue, lePunto.EditValue, leTipo.EditValue)
    End Sub

    Private Sub lePunto_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lePunto.EditValueChanged
        CargaSerie()
    End Sub
    Private Sub LlenaEntidadCreaDoc()
        Dim dtCLiente As New DataTable
        dtCLiente = objTablas.fac_ClientesSelectAll

        With entFactura
            .IdSucursal = leSucursal.EditValue
            .IdComprobante = -99999999
            .IdPunto = lePunto.EditValue
            .IdTipoComprobante = leTipo.EditValue
            .Numero = teNumero.EditValue
            .Fecha = Today
            .Serie = teSerie.EditValue
            .IdCliente = dtCLiente.Rows(0).Item("IdCliente") 'para efectos de anular tomo el primer cliente
            .Nombre = "<< Documento Anulado >>"
            .Nrc = ""
            .Nit = ""
            .Giro = ""
            .Direccion = ""
            .Telefono = ""
            .VentaAcuentaDe = ""
            .IdMunicipio = "0614"
            .IdDepartamento = "06"
            .IdFormaPago = 1
            .DiasCredito = 0
            .IdVendedor = 1
            .TipoVenta = 1
            .PorcDescto = 0
            .EsDevolucion = False
            .TotalDescto = 0
            .TotalComprobante = 0
            .TotalIva = 0
            .TotalImpuesto1 = 0
            .TotalImpuesto2 = 0.0
            .TotalNoSujeto = 0
            .TotalExento = 0
            .TotalAfecto = 0
            .TotalNeto = 0
            .TotalPagado = 0.0
            .SaldoActual = 0
            .Anulado = 1
            .IdComprobanteNota = 0
            .OrdenCompra = ""
            .IdBodega = 1
            .MotivoAnulacion = ""
            .AnuladoPor = "ANULACION DIRECTA"

            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .ModificadoPor = ""
            .FechaHoraModificacion = Nothing
        End With
    End Sub
    Private Sub leTipo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leTipo.EditValueChanged
        CargaSerie()
    End Sub
End Class
