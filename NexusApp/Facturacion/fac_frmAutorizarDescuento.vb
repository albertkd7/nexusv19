﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmAutorizarDescuento
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim fd As New FuncionesBLL(g_ConnectionString)
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim entFactura As New fac_Ventas
    Dim dtParametros As DataTable = blAdmon.ObtieneParametros()
    Dim dt As DataTable
    Dim IdTurno As Integer

    Private Sub fac_frmAutorizarDescuento_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        sePjeDescuento.Focus()
    End Sub

    Private Sub fac_frmAutorizarDescuento_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        deFecha.EditValue = fd.GetFechaContable(piIdSucursalUsuario)  'Today
        sePjeDescuento.EditValue = 0.0
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        leSucursal.EditValue = piIdSucursalUsuario

        seDesdeDescto.EditValue = entUsuario.DesdeDescuentos
        seHastaDescto.EditValue = entUsuario.HastaDescuentos

        If dtParametros.Rows(0).Item("BloquearFechaFacturar") Then
            deFecha.Properties.ReadOnly = True
        End If

    End Sub


    Private Sub sbGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGenerar.Click
        '   If sePjeDescuento.EditValue <> 0.0 Then
        'If sePjeDescuento.EditValue > seHastaDescto.EditValue Or sePjeDescuento.EditValue < seDesdeDescto.EditValue Then
        '    MsgBox("El descuento a autorizar no se encuentra en su rango autorizado", MsgBoxStyle.Information, "NOTA")
        '    Exit Sub
        'End If

        'If beProducto.beCodigo.EditValue = "" Then
        '    MsgBox("Debe especificar el código de producto", MsgBoxStyle.Information, "NOTA")
        '    beProducto.beCodigo.Focus()
        '    Exit Sub
        'End If

        If MsgBox("Está seguro(a) de generar el Token de descuento?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        txtContraseña.EditValue = bl.fac_ObtenerContrasenaDescuento(deFecha.EditValue, leSucursal.EditValue, sePjeDescuento.EditValue, objMenu.User, beProducto.beCodigo.EditValue)
        'Else
        '    MsgBox("Debe Digitar el Porcentaje de Descuento ", MsgBoxStyle.OkOnly)
        '    sePjeDescuento.Focus()
        'End If
    End Sub

    Private Sub beProducto_Load(sender As Object, e As EventArgs) Handles beProducto.Load

    End Sub
End Class
