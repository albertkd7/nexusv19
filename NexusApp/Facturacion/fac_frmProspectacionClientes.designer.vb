﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class fac_frmProspectacionClientes
	Inherits Nexus.gen_frmBase

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing AndAlso components IsNot Nothing Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Me.xtcClientes = New DevExpress.XtraTab.XtraTabControl()
		Me.xtpLista = New DevExpress.XtraTab.XtraTabPage()
		Me.gc = New DevExpress.XtraGrid.GridControl()
		Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
		Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.gcNrc = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.gcNit = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.gcTelefonos = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.gcDireccion = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.riteVendedor = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
		Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
		Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
		Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit()
		Me.teAsunto = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
		Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
		Me.deFechaContacto = New DevExpress.XtraEditors.DateEdit()
		Me.teLugarContacto = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
		Me.teReferidoPor = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
		Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
		Me.teOtroDocumento = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
		Me.leMunicipio = New DevExpress.XtraEditors.LookUpEdit()
		Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
		Me.leVendedor = New DevExpress.XtraEditors.LookUpEdit()
		Me.leDepartamento = New DevExpress.XtraEditors.LookUpEdit()
		Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
		Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
		Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
		Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
		Me.teNIT = New DevExpress.XtraEditors.TextEdit()
		Me.teCorreoElectronico = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
		Me.teTelefonos = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
		Me.teNRC = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
		Me.teDireccion = New DevExpress.XtraEditors.TextEdit()
		Me.teGiro = New DevExpress.XtraEditors.TextEdit()
		Me.teRazonSocial = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
		Me.teNombre = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
		CType(Me.xtcClientes, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.xtcClientes.SuspendLayout()
		Me.xtpLista.SuspendLayout()
		CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.riteVendedor, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.xtpDatos.SuspendLayout()
		CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teAsunto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaContacto.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaContacto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teLugarContacto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teReferidoPor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teOtroDocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.leMunicipio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.leDepartamento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teCorreoElectronico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teGiro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teRazonSocial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'xtcClientes
		'
		Me.xtcClientes.Dock = System.Windows.Forms.DockStyle.Fill
		Me.xtcClientes.Location = New System.Drawing.Point(0, 0)
		Me.xtcClientes.Name = "xtcClientes"
		Me.xtcClientes.SelectedTabPage = Me.xtpLista
		Me.xtcClientes.Size = New System.Drawing.Size(856, 473)
		Me.xtcClientes.TabIndex = 0
		Me.xtcClientes.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
		'
		'xtpLista
		'
		Me.xtpLista.Controls.Add(Me.gc)
		Me.xtpLista.Name = "xtpLista"
		Me.xtpLista.Size = New System.Drawing.Size(850, 445)
		Me.xtpLista.Text = "Lista (Doble clic para Editar)"
		'
		'gc
		'
		Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
		Me.gc.Location = New System.Drawing.Point(0, 0)
		Me.gc.MainView = Me.gv
		Me.gc.Name = "gc"
		Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteVendedor})
		Me.gc.Size = New System.Drawing.Size(850, 445)
		Me.gc.TabIndex = 0
		Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
		'
		'gv
		'
		Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn3, Me.gcNombre, Me.gcNrc, Me.gcNit, Me.gcTelefonos, Me.gcDireccion, Me.GridColumn2, Me.GridColumn1})
		Me.gv.GridControl = Me.gc
		Me.gv.Name = "gv"
		Me.gv.OptionsBehavior.Editable = False
		Me.gv.OptionsView.ShowAutoFilterRow = True
		Me.gv.OptionsView.ShowGroupPanel = False
		'
		'GridColumn3
		'
		Me.GridColumn3.Caption = "Correlativo"
		Me.GridColumn3.FieldName = "IdComprobante"
		Me.GridColumn3.Name = "GridColumn3"
		Me.GridColumn3.Visible = True
		Me.GridColumn3.VisibleIndex = 0
		'
		'gcNombre
		'
		Me.gcNombre.Caption = "Nombre"
		Me.gcNombre.FieldName = "NombreCliente"
		Me.gcNombre.Name = "gcNombre"
		Me.gcNombre.Visible = True
		Me.gcNombre.VisibleIndex = 1
		Me.gcNombre.Width = 419
		'
		'gcNrc
		'
		Me.gcNrc.Caption = "Nrc"
		Me.gcNrc.FieldName = "Nrc"
		Me.gcNrc.Name = "gcNrc"
		Me.gcNrc.Visible = True
		Me.gcNrc.VisibleIndex = 2
		Me.gcNrc.Width = 143
		'
		'gcNit
		'
		Me.gcNit.Caption = "Nit"
		Me.gcNit.FieldName = "Nit"
		Me.gcNit.Name = "gcNit"
		Me.gcNit.Visible = True
		Me.gcNit.VisibleIndex = 3
		Me.gcNit.Width = 148
		'
		'gcTelefonos
		'
		Me.gcTelefonos.Caption = "Teléfonos"
		Me.gcTelefonos.FieldName = "Telefonos"
		Me.gcTelefonos.Name = "gcTelefonos"
		Me.gcTelefonos.Visible = True
		Me.gcTelefonos.VisibleIndex = 4
		Me.gcTelefonos.Width = 101
		'
		'gcDireccion
		'
		Me.gcDireccion.Caption = "Dirección"
		Me.gcDireccion.FieldName = "Direccion"
		Me.gcDireccion.Name = "gcDireccion"
		Me.gcDireccion.Visible = True
		Me.gcDireccion.VisibleIndex = 5
		'
		'GridColumn2
		'
		Me.GridColumn2.Caption = "Fecha Contacto"
		Me.GridColumn2.FieldName = "FechaContacto"
		Me.GridColumn2.Name = "GridColumn2"
		Me.GridColumn2.Visible = True
		Me.GridColumn2.VisibleIndex = 6
		'
		'GridColumn1
		'
		Me.GridColumn1.Caption = "Vendedor"
		Me.GridColumn1.ColumnEdit = Me.riteVendedor
		Me.GridColumn1.FieldName = "IdVendedor"
		Me.GridColumn1.Name = "GridColumn1"
		Me.GridColumn1.Visible = True
		Me.GridColumn1.VisibleIndex = 7
		'
		'riteVendedor
		'
		Me.riteVendedor.AutoHeight = False
		Me.riteVendedor.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.riteVendedor.Name = "riteVendedor"
		'
		'xtpDatos
		'
		Me.xtpDatos.Controls.Add(Me.LabelControl15)
		Me.xtpDatos.Controls.Add(Me.teCorrelativo)
		Me.xtpDatos.Controls.Add(Me.teAsunto)
		Me.xtpDatos.Controls.Add(Me.LabelControl13)
		Me.xtpDatos.Controls.Add(Me.LabelControl19)
		Me.xtpDatos.Controls.Add(Me.deFechaContacto)
		Me.xtpDatos.Controls.Add(Me.teLugarContacto)
		Me.xtpDatos.Controls.Add(Me.LabelControl9)
		Me.xtpDatos.Controls.Add(Me.teReferidoPor)
		Me.xtpDatos.Controls.Add(Me.LabelControl1)
		Me.xtpDatos.Controls.Add(Me.LabelControl17)
		Me.xtpDatos.Controls.Add(Me.teOtroDocumento)
		Me.xtpDatos.Controls.Add(Me.LabelControl14)
		Me.xtpDatos.Controls.Add(Me.leMunicipio)
		Me.xtpDatos.Controls.Add(Me.LabelControl12)
		Me.xtpDatos.Controls.Add(Me.leVendedor)
		Me.xtpDatos.Controls.Add(Me.leDepartamento)
		Me.xtpDatos.Controls.Add(Me.LabelControl11)
		Me.xtpDatos.Controls.Add(Me.LabelControl7)
		Me.xtpDatos.Controls.Add(Me.LabelControl6)
		Me.xtpDatos.Controls.Add(Me.LabelControl5)
		Me.xtpDatos.Controls.Add(Me.teNIT)
		Me.xtpDatos.Controls.Add(Me.teCorreoElectronico)
		Me.xtpDatos.Controls.Add(Me.LabelControl10)
		Me.xtpDatos.Controls.Add(Me.teTelefonos)
		Me.xtpDatos.Controls.Add(Me.LabelControl8)
		Me.xtpDatos.Controls.Add(Me.teNRC)
		Me.xtpDatos.Controls.Add(Me.LabelControl4)
		Me.xtpDatos.Controls.Add(Me.teDireccion)
		Me.xtpDatos.Controls.Add(Me.teGiro)
		Me.xtpDatos.Controls.Add(Me.teRazonSocial)
		Me.xtpDatos.Controls.Add(Me.LabelControl3)
		Me.xtpDatos.Controls.Add(Me.teNombre)
		Me.xtpDatos.Controls.Add(Me.LabelControl2)
		Me.xtpDatos.Name = "xtpDatos"
		Me.xtpDatos.Size = New System.Drawing.Size(850, 445)
		Me.xtpDatos.Text = "Prospectación de Clientes"
		'
		'LabelControl15
		'
		Me.LabelControl15.Location = New System.Drawing.Point(102, 18)
		Me.LabelControl15.Name = "LabelControl15"
		Me.LabelControl15.Size = New System.Drawing.Size(57, 13)
		Me.LabelControl15.TabIndex = 105
		Me.LabelControl15.Text = "Correlativo:"
		'
		'teCorrelativo
		'
		Me.teCorrelativo.EnterMoveNextControl = True
		Me.teCorrelativo.Location = New System.Drawing.Point(162, 15)
		Me.teCorrelativo.Name = "teCorrelativo"
		Me.teCorrelativo.Properties.ReadOnly = True
		Me.teCorrelativo.Size = New System.Drawing.Size(100, 20)
		Me.teCorrelativo.TabIndex = 0
		'
		'teAsunto
		'
		Me.teAsunto.EnterMoveNextControl = True
		Me.teAsunto.Location = New System.Drawing.Point(162, 190)
		Me.teAsunto.Name = "teAsunto"
		Me.teAsunto.Size = New System.Drawing.Size(476, 20)
		Me.teAsunto.TabIndex = 12
		'
		'LabelControl13
		'
		Me.LabelControl13.Location = New System.Drawing.Point(122, 193)
		Me.LabelControl13.Name = "LabelControl13"
		Me.LabelControl13.Size = New System.Drawing.Size(38, 13)
		Me.LabelControl13.TabIndex = 103
		Me.LabelControl13.Text = "Asunto:"
		'
		'LabelControl19
		'
		Me.LabelControl19.Location = New System.Drawing.Point(269, 18)
		Me.LabelControl19.Name = "LabelControl19"
		Me.LabelControl19.Size = New System.Drawing.Size(95, 13)
		Me.LabelControl19.TabIndex = 101
		Me.LabelControl19.Text = "Fecha de Contacto:"
		'
		'deFechaContacto
		'
		Me.deFechaContacto.EditValue = Nothing
		Me.deFechaContacto.EnterMoveNextControl = True
		Me.deFechaContacto.Location = New System.Drawing.Point(368, 15)
		Me.deFechaContacto.Name = "deFechaContacto"
		Me.deFechaContacto.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
		Me.deFechaContacto.Properties.AppearanceDisabled.Options.UseForeColor = True
		Me.deFechaContacto.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
		Me.deFechaContacto.Properties.AppearanceReadOnly.Options.UseForeColor = True
		Me.deFechaContacto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.deFechaContacto.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.deFechaContacto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
		Me.deFechaContacto.Size = New System.Drawing.Size(100, 20)
		Me.deFechaContacto.TabIndex = 1
		'
		'teLugarContacto
		'
		Me.teLugarContacto.EnterMoveNextControl = True
		Me.teLugarContacto.Location = New System.Drawing.Point(162, 211)
		Me.teLugarContacto.Name = "teLugarContacto"
		Me.teLugarContacto.Size = New System.Drawing.Size(476, 20)
		Me.teLugarContacto.TabIndex = 13
		'
		'LabelControl9
		'
		Me.LabelControl9.Location = New System.Drawing.Point(67, 214)
		Me.LabelControl9.Name = "LabelControl9"
		Me.LabelControl9.Size = New System.Drawing.Size(93, 13)
		Me.LabelControl9.TabIndex = 99
		Me.LabelControl9.Text = "Lugar de Contacto:"
		'
		'teReferidoPor
		'
		Me.teReferidoPor.EnterMoveNextControl = True
		Me.teReferidoPor.Location = New System.Drawing.Point(162, 233)
		Me.teReferidoPor.Name = "teReferidoPor"
		Me.teReferidoPor.Size = New System.Drawing.Size(476, 20)
		Me.teReferidoPor.TabIndex = 14
		'
		'LabelControl1
		'
		Me.LabelControl1.Location = New System.Drawing.Point(96, 236)
		Me.LabelControl1.Name = "LabelControl1"
		Me.LabelControl1.Size = New System.Drawing.Size(64, 13)
		Me.LabelControl1.TabIndex = 97
		Me.LabelControl1.Text = "Referido Por:"
		'
		'LabelControl17
		'
		Me.LabelControl17.Location = New System.Drawing.Point(639, 85)
		Me.LabelControl17.Name = "LabelControl17"
		Me.LabelControl17.Size = New System.Drawing.Size(22, 13)
		Me.LabelControl17.TabIndex = 95
		Me.LabelControl17.Text = "DUI:"
		'
		'teOtroDocumento
		'
		Me.teOtroDocumento.EnterMoveNextControl = True
		Me.teOtroDocumento.Location = New System.Drawing.Point(662, 81)
		Me.teOtroDocumento.Name = "teOtroDocumento"
		Me.teOtroDocumento.Properties.Mask.EditMask = "99999999-9"
		Me.teOtroDocumento.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
		Me.teOtroDocumento.Size = New System.Drawing.Size(170, 20)
		Me.teOtroDocumento.TabIndex = 6
		'
		'LabelControl14
		'
		Me.LabelControl14.Location = New System.Drawing.Point(38, 280)
		Me.LabelControl14.Name = "LabelControl14"
		Me.LabelControl14.Size = New System.Drawing.Size(122, 13)
		Me.LabelControl14.TabIndex = 16
		Me.LabelControl14.Text = "Vendedor que lo Atiende:"
		'
		'leMunicipio
		'
		Me.leMunicipio.EnterMoveNextControl = True
		Me.leMunicipio.Location = New System.Drawing.Point(448, 146)
		Me.leMunicipio.Name = "leMunicipio"
		Me.leMunicipio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.leMunicipio.Size = New System.Drawing.Size(190, 20)
		Me.leMunicipio.TabIndex = 10
		'
		'LabelControl12
		'
		Me.LabelControl12.Location = New System.Drawing.Point(398, 150)
		Me.LabelControl12.Name = "LabelControl12"
		Me.LabelControl12.Size = New System.Drawing.Size(47, 13)
		Me.LabelControl12.TabIndex = 11
		Me.LabelControl12.Text = "Municipio:"
		'
		'leVendedor
		'
		Me.leVendedor.EnterMoveNextControl = True
		Me.leVendedor.Location = New System.Drawing.Point(162, 277)
		Me.leVendedor.Name = "leVendedor"
		Me.leVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.leVendedor.Size = New System.Drawing.Size(209, 20)
		Me.leVendedor.TabIndex = 16
		'
		'leDepartamento
		'
		Me.leDepartamento.EnterMoveNextControl = True
		Me.leDepartamento.Location = New System.Drawing.Point(162, 146)
		Me.leDepartamento.Name = "leDepartamento"
		Me.leDepartamento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.leDepartamento.Size = New System.Drawing.Size(209, 20)
		Me.leDepartamento.TabIndex = 9
		'
		'LabelControl11
		'
		Me.LabelControl11.Location = New System.Drawing.Point(87, 150)
		Me.LabelControl11.Name = "LabelControl11"
		Me.LabelControl11.Size = New System.Drawing.Size(73, 13)
		Me.LabelControl11.TabIndex = 11
		Me.LabelControl11.Text = "Departamento:"
		'
		'LabelControl7
		'
		Me.LabelControl7.Location = New System.Drawing.Point(113, 126)
		Me.LabelControl7.Name = "LabelControl7"
		Me.LabelControl7.Size = New System.Drawing.Size(47, 13)
		Me.LabelControl7.TabIndex = 10
		Me.LabelControl7.Text = "Dirección:"
		'
		'LabelControl6
		'
		Me.LabelControl6.Location = New System.Drawing.Point(137, 105)
		Me.LabelControl6.Name = "LabelControl6"
		Me.LabelControl6.Size = New System.Drawing.Size(23, 13)
		Me.LabelControl6.TabIndex = 10
		Me.LabelControl6.Text = "Giro:"
		'
		'LabelControl5
		'
		Me.LabelControl5.Location = New System.Drawing.Point(344, 83)
		Me.LabelControl5.Name = "LabelControl5"
		Me.LabelControl5.Size = New System.Drawing.Size(21, 13)
		Me.LabelControl5.TabIndex = 9
		Me.LabelControl5.Text = "NIT:"
		'
		'teNIT
		'
		Me.teNIT.EnterMoveNextControl = True
		Me.teNIT.Location = New System.Drawing.Point(368, 81)
		Me.teNIT.Name = "teNIT"
		Me.teNIT.Properties.Mask.EditMask = "9999-999999-999-9"
		Me.teNIT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
		Me.teNIT.Size = New System.Drawing.Size(190, 20)
		Me.teNIT.TabIndex = 5
		'
		'teCorreoElectronico
		'
		Me.teCorreoElectronico.EnterMoveNextControl = True
		Me.teCorreoElectronico.Location = New System.Drawing.Point(162, 255)
		Me.teCorreoElectronico.Name = "teCorreoElectronico"
		Me.teCorreoElectronico.Size = New System.Drawing.Size(476, 20)
		Me.teCorreoElectronico.TabIndex = 15
		'
		'LabelControl10
		'
		Me.LabelControl10.Location = New System.Drawing.Point(13, 258)
		Me.LabelControl10.Name = "LabelControl10"
		Me.LabelControl10.Size = New System.Drawing.Size(147, 13)
		Me.LabelControl10.TabIndex = 6
		Me.LabelControl10.Text = "Correo Electrónico / Sitio Web:"
		'
		'teTelefonos
		'
		Me.teTelefonos.EnterMoveNextControl = True
		Me.teTelefonos.Location = New System.Drawing.Point(162, 168)
		Me.teTelefonos.Name = "teTelefonos"
		Me.teTelefonos.Size = New System.Drawing.Size(209, 20)
		Me.teTelefonos.TabIndex = 11
		'
		'LabelControl8
		'
		Me.LabelControl8.Location = New System.Drawing.Point(109, 171)
		Me.LabelControl8.Name = "LabelControl8"
		Me.LabelControl8.Size = New System.Drawing.Size(51, 13)
		Me.LabelControl8.TabIndex = 6
		Me.LabelControl8.Text = "Teléfonos:"
		'
		'teNRC
		'
		Me.teNRC.EnterMoveNextControl = True
		Me.teNRC.Location = New System.Drawing.Point(162, 81)
		Me.teNRC.Name = "teNRC"
		Me.teNRC.Size = New System.Drawing.Size(100, 20)
		Me.teNRC.TabIndex = 4
		'
		'LabelControl4
		'
		Me.LabelControl4.Location = New System.Drawing.Point(135, 85)
		Me.LabelControl4.Name = "LabelControl4"
		Me.LabelControl4.Size = New System.Drawing.Size(25, 13)
		Me.LabelControl4.TabIndex = 6
		Me.LabelControl4.Text = "NRC:"
		'
		'teDireccion
		'
		Me.teDireccion.EnterMoveNextControl = True
		Me.teDireccion.Location = New System.Drawing.Point(162, 123)
		Me.teDireccion.Name = "teDireccion"
		Me.teDireccion.Size = New System.Drawing.Size(670, 20)
		Me.teDireccion.TabIndex = 8
		'
		'teGiro
		'
		Me.teGiro.EnterMoveNextControl = True
		Me.teGiro.Location = New System.Drawing.Point(162, 102)
		Me.teGiro.Name = "teGiro"
		Me.teGiro.Size = New System.Drawing.Size(670, 20)
		Me.teGiro.TabIndex = 7
		'
		'teRazonSocial
		'
		Me.teRazonSocial.EnterMoveNextControl = True
		Me.teRazonSocial.Location = New System.Drawing.Point(162, 59)
		Me.teRazonSocial.Name = "teRazonSocial"
		Me.teRazonSocial.Size = New System.Drawing.Size(670, 20)
		Me.teRazonSocial.TabIndex = 3
		'
		'LabelControl3
		'
		Me.LabelControl3.Location = New System.Drawing.Point(96, 62)
		Me.LabelControl3.Name = "LabelControl3"
		Me.LabelControl3.Size = New System.Drawing.Size(64, 13)
		Me.LabelControl3.TabIndex = 4
		Me.LabelControl3.Text = "Razón Social:"
		'
		'teNombre
		'
		Me.teNombre.EnterMoveNextControl = True
		Me.teNombre.Location = New System.Drawing.Point(162, 38)
		Me.teNombre.Name = "teNombre"
		Me.teNombre.Size = New System.Drawing.Size(670, 20)
		Me.teNombre.TabIndex = 2
		'
		'LabelControl2
		'
		Me.LabelControl2.Location = New System.Drawing.Point(119, 43)
		Me.LabelControl2.Name = "LabelControl2"
		Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
		Me.LabelControl2.TabIndex = 2
		Me.LabelControl2.Text = "Nombre:"
		'
		'fac_frmProspectacionClientes
		'
		Me.ClientSize = New System.Drawing.Size(856, 498)
		Me.Controls.Add(Me.xtcClientes)
		Me.Modulo = "Facturación"
		Me.Name = "fac_frmProspectacionClientes"
		Me.OptionId = "004003"
		Me.Text = "Prospectación de Clientes"
		Me.Controls.SetChildIndex(Me.xtcClientes, 0)
		CType(Me.xtcClientes, System.ComponentModel.ISupportInitialize).EndInit()
		Me.xtcClientes.ResumeLayout(False)
		Me.xtpLista.ResumeLayout(False)
		CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.riteVendedor, System.ComponentModel.ISupportInitialize).EndInit()
		Me.xtpDatos.ResumeLayout(False)
		Me.xtpDatos.PerformLayout()
		CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teAsunto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaContacto.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaContacto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teLugarContacto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teReferidoPor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teOtroDocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.leMunicipio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.leDepartamento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teCorreoElectronico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teGiro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teRazonSocial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents xtcClientes As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNIT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teGiro As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teRazonSocial As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents teTelefonos As DevExpress.XtraEditors.TextEdit
	Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents teCorreoElectronico As DevExpress.XtraEditors.TextEdit
	Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents leMunicipio As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents leDepartamento As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents leVendedor As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents gc As DevExpress.XtraGrid.GridControl
	Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
	Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents gcNrc As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents gcNit As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents gcTelefonos As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents gcDireccion As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents teOtroDocumento As DevExpress.XtraEditors.TextEdit
	Friend WithEvents teLugarContacto As DevExpress.XtraEditors.TextEdit
	Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents teReferidoPor As DevExpress.XtraEditors.TextEdit
	Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents teAsunto As DevExpress.XtraEditors.TextEdit
	Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents deFechaContacto As DevExpress.XtraEditors.DateEdit
	Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents riteVendedor As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
	Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
	Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
End Class
