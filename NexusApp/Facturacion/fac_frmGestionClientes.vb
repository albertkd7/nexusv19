﻿Imports DevExpress.XtraReports.UI
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmGestionClientes
    Dim blFac As New FacturaBLL(g_ConnectionString)
	Dim UsuarioDioClic As Boolean
	Dim UsuarioDioClicProspecto As Boolean
	Dim entGestion As New fac_Gestiones
	Dim entCliente As New fac_Clientes
	Dim entProspecto As New fac_ProspectacionClientes
	Dim fd As New FuncionesBLL(g_ConnectionString)

    Private Sub fac_frmConsultaFacturacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LimpiaControles()
        gc.DataSource = blFac.fac_GestionesbyCliente("", -999)
        gv.BestFitColumns()
    End Sub

    Private Sub LimpiaControles()

        deFecha.EditValue = Today
        deFechaProxima.EditValue = Nothing
		beCodCliente.EditValue = ""
		beCodProspecto.EditValue = 0
		teNombre.EditValue = ""
		teNombreProspecto.EditValue = ""
		meComentarioCliente.EditValue = ""
        meComentarioGeneral.EditValue = ""
        teGestion.EditValue = Now
        teMotivoGestion.EditValue = ""
        deFechaProxima.EditValue = Nothing
        deFechaVisita.EditValue = Nothing
        teVisita.EditValue = Nothing
        teIdCotizacion.EditValue = 0
        teIdGestion.EditValue = 0
        teTelefonos.EditValue = ""
        teNumCotizacion.EditValue = ""
        beCodCliente.Enabled = True
        teNombre.Enabled = True
        deFecha.Enabled = True
        teMotivoGestion.Enabled = True

        gc.DataSource = blFac.fac_GestionesbyCliente("", -999)
        gv.BestFitColumns()
    End Sub

    Private Sub beIdCliente_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCodCliente.ButtonClick
        beCodCliente.EditValue = ""
        UsuarioDioClic = True
        beIdCliente_Validated(beCodCliente, New EventArgs)
    End Sub
	Private Sub beCodProspecto_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCodProspecto.ButtonClick
		beCodProspecto.EditValue = 0
		UsuarioDioClicProspecto = True
		beCodProspecto_Validated(beCodProspecto, New EventArgs)
	End Sub
	Private Sub beIdCliente_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCodCliente.Validated
		If Not UsuarioDioClic And beCodCliente.EditValue = "" Then
			Exit Sub
		End If
		If beCodProspecto.EditValue <> 0 Then
			Exit Sub
		End If
		UsuarioDioClic = False

        entCliente = objConsultas.cnsClientes(fac_frmConsultaClientes, beCodCliente.EditValue)
        If entCliente.IdCliente = "" Then
            beCodCliente.EditValue = ""
            beCodCliente.Focus()
            Exit Sub
        End If


		LimpiaControles()
        beCodCliente.EditValue = entCliente.IdCliente
		teNombre.EditValue = entCliente.Nombre
		teTelefonos.EditValue = entCliente.Telefonos

        teGestion.EditValue = Now
        gc.DataSource = blFac.fac_GestionesbyCliente(entCliente.IdCliente, piIdVendedor)
        gv.BestFitColumns()
        'objCombos.fac_ContactosCliente(leContacto, "IdCliente='" & entCliente.IdCliente & "'")
    End Sub
	Private Sub beCodProspecto_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCodProspecto.Validated
		If Not UsuarioDioClicProspecto And beCodProspecto.EditValue = 0 Then
			teNombreProspecto.EditValue = ""
			Exit Sub
		End If
		If beCodCliente.EditValue <> "" Then
			Exit Sub
		End If
		UsuarioDioClicProspecto = False
		entProspecto = objConsultas.cnsProspectosClientes(frmConsultas, beCodProspecto.EditValue)

		If entProspecto.IdComprobante = 0 Then
			beCodProspecto.EditValue = 0
			teNombreProspecto.EditValue = ""
			beCodProspecto.Focus()
			Exit Sub
		End If


		LimpiaControles()
		beCodProspecto.EditValue = entProspecto.IdComprobante
		teNombreProspecto.EditValue = entProspecto.NombreCliente
		teTelefonos.EditValue = entProspecto.Telefonos

		teGestion.EditValue = Now
		gc.DataSource = blFac.fac_GestionesbyIdProspecto(entProspecto.IdComprobante, piIdVendedor)
		gv.BestFitColumns()
		'objCombos.fac_ContactosCliente(leContacto, "IdCliente='" & entCliente.IdCliente & "'")
	End Sub
	Private Sub sbGuardar_Click(sender As Object, e As EventArgs) Handles sbGuardar.Click
        If MsgBox("Está seguro(a) de guardar la gestión?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        If MsgBox("Confirme Nuevamente", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
		If beCodCliente.EditValue = "" And beCodProspecto.EditValue = 0 Then
			MsgBox("Debe especificar un cliente ó un código de prospecto", MsgBoxStyle.Exclamation, "Nota")
			Exit Sub
		End If
		entGestion = New fac_Gestiones
        entGestion.IdGestion = fd.ObtenerUltimoId("fac_Gestiones", "IdGestion") + 1
        entGestion.IdCliente = beCodCliente.EditValue
		entGestion.NombreCliente = IIf(beCodProspecto.EditValue = 0, teNombre.EditValue, teNombreProspecto.EditValue)
		entGestion.Motivo = teMotivoGestion.EditValue
        entGestion.Fecha = deFecha.EditValue
        entGestion.HoraLlamada = teGestion.EditValue
        entGestion.ComentarioCliente = meComentarioCliente.EditValue
        entGestion.ComentarioGeneral = meComentarioGeneral.EditValue
        entGestion.ProximaGestion = SiEsNulo(deFechaProxima.EditValue, Nothing)
        entGestion.FechaVisita = SiEsNulo(deFechaVisita.EditValue, Nothing)
        entGestion.HoraVisita = SiEsNulo(teVisita.EditValue, Nothing)
		entGestion.IdOrden = beCodProspecto.EditValue
		entGestion.CreadoPor = objMenu.User
        entGestion.IdGestionProgramada = teIdGestion.EditValue
        entGestion.FechaHoraCreacion = Now
		entGestion.IdCotizacion = teIdCotizacion.EditValue

		If piIdVendedor > 0 Then
            entGestion.IdVendedor = piIdVendedor
        Else
			entGestion.IdVendedor = IIf(beCodProspecto.EditValue = 0, entCliente.IdVendedor, entProspecto.IdVendedor)
		End If

        Try
            objTablas.fac_GestionesInsert(entGestion)
            MsgBox("Gestión guardada con éxito ", MsgBoxStyle.Information)
            LimpiaControles()
        Catch ex As Exception
            MsgBox("Sucedio un error al guardar la gestión " + ex.Message, MsgBoxStyle.Critical)
        End Try


    End Sub



    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles sbObtenerGestion.Click

        Dim entGEstion As Appointments
        teIdGestion.EditValue = ""
        teIdGestion.EditValue = objConsultas.ConsultaGEstionesProgramadas(frmConsultas, piIdVendedor)

        If teIdGestion.EditValue = "" Then
            MsgBox("No ha Selecciono Ninguna Gestión", MsgBoxStyle.Exclamation, "Nota")
            teIdGestion.EditValue = 0
            Exit Sub
        Else
            entGEstion = objTablas.AppointmentsSelectByPK(teIdGestion.EditValue)
        End If

        If SiEsNulo(entGEstion.UniqueID, 0) = 0 Then
            LimpiaControles()
            Exit Sub
        End If

        entCliente = objTablas.fac_ClientesSelectByPK(entGEstion.CustomField1)
        beCodCliente.EditValue = entGEstion.CustomField1
        teNombre.EditValue = entCliente.Nombre
        teTelefonos.EditValue = entCliente.Telefonos
        deFecha.EditValue = Today
        teMotivoGestion.EditValue = entGEstion.Subject
        teGestion.EditValue = Now
        beCodCliente.Enabled = False
        teNombre.Enabled = False
        deFecha.Enabled = False
        teMotivoGestion.Enabled = False

    End Sub

    Private Sub sbLlamaCotizacion_Click(sender As Object, e As EventArgs) Handles sbLlamaCotizacion.Click
        Dim entCotizacion As fac_Cotizaciones
        teIdCotizacion.EditValue = 0
        teIdCotizacion.EditValue = objConsultas.ConsultaCotizaciones(frmConsultaDetalle, piIdVendedor)


        If teIdCotizacion.EditValue = 0 Then
            MsgBox("No ha Selecciono Ninguna Cotización", MsgBoxStyle.Exclamation, "Nota")
            teIdCotizacion.EditValue = 0
            Exit Sub
        Else
            entCotizacion = objTablas.fac_CotizacionesSelectByPK(teIdCotizacion.EditValue)
            teNumCotizacion.EditValue = entCotizacion.Numero
        End If

    End Sub


End Class
