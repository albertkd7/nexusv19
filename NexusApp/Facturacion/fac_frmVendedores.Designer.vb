﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmVendedores
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.gcComision = New DevExpress.XtraGrid.GridControl()
        Me.gvComision = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcDesde = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcHasta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPorcentaje = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdVendedor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ritePrecio = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.riteCodigo = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdVendedor = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.ceActivo = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teTelefonos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.tePorcComision = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teLimite = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.beIdEmpleado = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.teDireccion = New DevExpress.XtraEditors.TextEdit()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SplitContainerControl2 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.sbtnImprimirCRV = New DevExpress.XtraEditors.SimpleButton()
        Me.gcVenClientes = New DevExpress.XtraGrid.GridControl()
        Me.gvClientesVendedor = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gccIdVendedor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gccIdCliente = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gccNombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.lbInfoRCV = New DevExpress.XtraEditors.LabelControl()
        Me.ceTodosClientes = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.gcComision, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvComision, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePorcComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teLimite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beIdEmpleado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.SplitContainerControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl2.SuspendLayout()
        CType(Me.gcVenClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvClientesVendedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceTodosClientes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.gc)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(421, 564)
        Me.PanelControl1.TabIndex = 4
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(3, 3)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(415, 558)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Cód. Vendedor"
        Me.GridColumn1.FieldName = "IdVendedor"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 151
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombre"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 372
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Teléfonos"
        Me.GridColumn3.FieldName = "Telefonos"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 306
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.leSucursal)
        Me.PanelControl2.Controls.Add(Me.LabelControl10)
        Me.PanelControl2.Controls.Add(Me.PanelControl3)
        Me.PanelControl2.Controls.Add(Me.gcComision)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Controls.Add(Me.teIdVendedor)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.teNombre)
        Me.PanelControl2.Controls.Add(Me.ceActivo)
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.teTelefonos)
        Me.PanelControl2.Controls.Add(Me.LabelControl4)
        Me.PanelControl2.Controls.Add(Me.tePorcComision)
        Me.PanelControl2.Controls.Add(Me.LabelControl5)
        Me.PanelControl2.Controls.Add(Me.teLimite)
        Me.PanelControl2.Controls.Add(Me.LabelControl6)
        Me.PanelControl2.Controls.Add(Me.beIdEmpleado)
        Me.PanelControl2.Controls.Add(Me.LabelControl7)
        Me.PanelControl2.Controls.Add(Me.teDireccion)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(528, 564)
        Me.PanelControl2.TabIndex = 5
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(10, 326)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(209, 20)
        Me.leSucursal.TabIndex = 46
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(11, 308)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl10.TabIndex = 47
        Me.LabelControl10.Text = "Sucursal:"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.cmdUp)
        Me.PanelControl3.Controls.Add(Me.cmdDown)
        Me.PanelControl3.Controls.Add(Me.cmdBorrar)
        Me.PanelControl3.Location = New System.Drawing.Point(484, 364)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(33, 188)
        Me.PanelControl3.TabIndex = 43
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(3, 7)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(27, 24)
        Me.cmdUp.TabIndex = 28
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(3, 42)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(27, 24)
        Me.cmdDown.TabIndex = 29
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(3, 77)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 24)
        Me.cmdBorrar.TabIndex = 30
        '
        'gcComision
        '
        Me.gcComision.Location = New System.Drawing.Point(8, 364)
        Me.gcComision.MainView = Me.gvComision
        Me.gcComision.Name = "gcComision"
        Me.gcComision.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteCantidad, Me.ritePrecio, Me.riteCodigo})
        Me.gcComision.Size = New System.Drawing.Size(470, 188)
        Me.gcComision.TabIndex = 42
        Me.gcComision.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvComision})
        '
        'gvComision
        '
        Me.gvComision.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcDesde, Me.gcHasta, Me.gcPorcentaje, Me.gcIdVendedor})
        Me.gvComision.GridControl = Me.gcComision
        Me.gvComision.Name = "gvComision"
        Me.gvComision.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gvComision.OptionsView.ShowGroupPanel = False
        '
        'gcDesde
        '
        Me.gcDesde.Caption = "Desde Rango"
        Me.gcDesde.ColumnEdit = Me.riteCantidad
        Me.gcDesde.FieldName = "RangoDesde"
        Me.gcDesde.Name = "gcDesde"
        Me.gcDesde.Visible = True
        Me.gcDesde.VisibleIndex = 0
        Me.gcDesde.Width = 184
        '
        'riteCantidad
        '
        Me.riteCantidad.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCantidad.Appearance.Options.UseTextOptions = True
        Me.riteCantidad.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.riteCantidad.AutoHeight = False
        Me.riteCantidad.EditFormat.FormatString = "n2"
        Me.riteCantidad.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteCantidad.Mask.EditMask = "n2"
        Me.riteCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.riteCantidad.Name = "riteCantidad"
        '
        'gcHasta
        '
        Me.gcHasta.Caption = "Hasta Rango"
        Me.gcHasta.ColumnEdit = Me.riteCantidad
        Me.gcHasta.DisplayFormat.FormatString = "n2"
        Me.gcHasta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcHasta.FieldName = "RangoHasta"
        Me.gcHasta.Name = "gcHasta"
        Me.gcHasta.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.gcHasta.Visible = True
        Me.gcHasta.VisibleIndex = 1
        Me.gcHasta.Width = 143
        '
        'gcPorcentaje
        '
        Me.gcPorcentaje.Caption = "(%) Porcentaje"
        Me.gcPorcentaje.FieldName = "Porcentaje"
        Me.gcPorcentaje.Name = "gcPorcentaje"
        Me.gcPorcentaje.Visible = True
        Me.gcPorcentaje.VisibleIndex = 2
        Me.gcPorcentaje.Width = 125
        '
        'gcIdVendedor
        '
        Me.gcIdVendedor.Caption = "IdVendedor"
        Me.gcIdVendedor.FieldName = "IdVendedor"
        Me.gcIdVendedor.Name = "gcIdVendedor"
        Me.gcIdVendedor.Width = 123
        '
        'ritePrecio
        '
        Me.ritePrecio.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.ritePrecio.AutoHeight = False
        Me.ritePrecio.Mask.EditMask = "n4"
        Me.ritePrecio.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePrecio.Mask.UseMaskAsDisplayFormat = True
        Me.ritePrecio.Name = "ritePrecio"
        '
        'riteCodigo
        '
        Me.riteCodigo.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCodigo.AutoHeight = False
        Me.riteCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.riteCodigo.Name = "riteCodigo"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(11, 10)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl1.TabIndex = 34
        Me.LabelControl1.Text = "Cód. Vendedor:"
        '
        'teIdVendedor
        '
        Me.teIdVendedor.EnterMoveNextControl = True
        Me.teIdVendedor.Location = New System.Drawing.Point(10, 28)
        Me.teIdVendedor.Name = "teIdVendedor"
        Me.teIdVendedor.Size = New System.Drawing.Size(95, 20)
        Me.teIdVendedor.TabIndex = 27
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(11, 53)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 35
        Me.LabelControl2.Text = "Nombre:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(10, 66)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(447, 20)
        Me.teNombre.TabIndex = 28
        '
        'ceActivo
        '
        Me.ceActivo.Location = New System.Drawing.Point(350, 29)
        Me.ceActivo.Name = "ceActivo"
        Me.ceActivo.Properties.Caption = "Vendedor Activo?"
        Me.ceActivo.Size = New System.Drawing.Size(128, 19)
        Me.ceActivo.TabIndex = 36
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(11, 131)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl3.TabIndex = 37
        Me.LabelControl3.Text = "Teléfonos personales:"
        '
        'teTelefonos
        '
        Me.teTelefonos.EnterMoveNextControl = True
        Me.teTelefonos.Location = New System.Drawing.Point(10, 146)
        Me.teTelefonos.Name = "teTelefonos"
        Me.teTelefonos.Size = New System.Drawing.Size(211, 20)
        Me.teTelefonos.TabIndex = 30
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(11, 175)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl4.TabIndex = 38
        Me.LabelControl4.Text = "Porc. Comisión asignado:"
        '
        'tePorcComision
        '
        Me.tePorcComision.EditValue = 0
        Me.tePorcComision.EnterMoveNextControl = True
        Me.tePorcComision.Location = New System.Drawing.Point(10, 192)
        Me.tePorcComision.Name = "tePorcComision"
        Me.tePorcComision.Size = New System.Drawing.Size(95, 20)
        Me.tePorcComision.TabIndex = 31
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(11, 220)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl5.TabIndex = 39
        Me.LabelControl5.Text = "Límite de Venta:"
        '
        'teLimite
        '
        Me.teLimite.EditValue = 0
        Me.teLimite.EnterMoveNextControl = True
        Me.teLimite.Location = New System.Drawing.Point(10, 234)
        Me.teLimite.Name = "teLimite"
        Me.teLimite.Size = New System.Drawing.Size(95, 20)
        Me.teLimite.TabIndex = 32
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(11, 263)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(125, 13)
        Me.LabelControl6.TabIndex = 40
        Me.LabelControl6.Text = "Id. Empleado (Según RH):"
        '
        'beIdEmpleado
        '
        Me.beIdEmpleado.EnterMoveNextControl = True
        Me.beIdEmpleado.Location = New System.Drawing.Point(10, 281)
        Me.beIdEmpleado.Name = "beIdEmpleado"
        Me.beIdEmpleado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beIdEmpleado.Size = New System.Drawing.Size(211, 20)
        Me.beIdEmpleado.TabIndex = 33
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(11, 91)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl7.TabIndex = 41
        Me.LabelControl7.Text = "Dirección:"
        '
        'teDireccion
        '
        Me.teDireccion.EnterMoveNextControl = True
        Me.teDireccion.Location = New System.Drawing.Point(10, 105)
        Me.teDireccion.Name = "teDireccion"
        Me.teDireccion.Size = New System.Drawing.Size(446, 20)
        Me.teDireccion.TabIndex = 29
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.SplitContainerControl2)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1370, 564)
        Me.SplitContainerControl1.SplitterPosition = 421
        Me.SplitContainerControl1.TabIndex = 6
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'SplitContainerControl2
        '
        Me.SplitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl2.Name = "SplitContainerControl2"
        Me.SplitContainerControl2.Panel1.Controls.Add(Me.PanelControl2)
        Me.SplitContainerControl2.Panel1.Text = "Panel1"
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.sbtnImprimirCRV)
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.gcVenClientes)
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.lbInfoRCV)
        Me.SplitContainerControl2.Panel2.Controls.Add(Me.ceTodosClientes)
        Me.SplitContainerControl2.Panel2.Text = "Panel2"
        Me.SplitContainerControl2.Size = New System.Drawing.Size(943, 564)
        Me.SplitContainerControl2.SplitterPosition = 528
        Me.SplitContainerControl2.TabIndex = 6
        Me.SplitContainerControl2.Text = "SplitContainerControl2"
        '
        'sbtnImprimirCRV
        '
        Me.sbtnImprimirCRV.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.sbtnImprimirCRV.Location = New System.Drawing.Point(331, 9)
        Me.sbtnImprimirCRV.Name = "sbtnImprimirCRV"
        Me.sbtnImprimirCRV.Size = New System.Drawing.Size(74, 23)
        Me.sbtnImprimirCRV.TabIndex = 35
        Me.sbtnImprimirCRV.Text = "[Imprimir]"
        '
        'gcVenClientes
        '
        Me.gcVenClientes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gcVenClientes.Location = New System.Drawing.Point(0, 63)
        Me.gcVenClientes.MainView = Me.gvClientesVendedor
        Me.gcVenClientes.Name = "gcVenClientes"
        Me.gcVenClientes.Size = New System.Drawing.Size(408, 501)
        Me.gcVenClientes.TabIndex = 0
        Me.gcVenClientes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvClientesVendedor})
        '
        'gvClientesVendedor
        '
        Me.gvClientesVendedor.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gccIdVendedor, Me.gccIdCliente, Me.gccNombre})
        Me.gvClientesVendedor.GridControl = Me.gcVenClientes
        Me.gvClientesVendedor.Name = "gvClientesVendedor"
        Me.gvClientesVendedor.OptionsCustomization.AllowGroup = False
        Me.gvClientesVendedor.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gvClientesVendedor.OptionsView.ShowGroupPanel = False
        '
        'gccIdVendedor
        '
        Me.gccIdVendedor.Caption = "IdVendedor"
        Me.gccIdVendedor.FieldName = "IdVendedor"
        Me.gccIdVendedor.Name = "gccIdVendedor"
        '
        'gccIdCliente
        '
        Me.gccIdCliente.Caption = "Cod. Cliente"
        Me.gccIdCliente.FieldName = "IdCliente"
        Me.gccIdCliente.Name = "gccIdCliente"
        Me.gccIdCliente.Visible = True
        Me.gccIdCliente.VisibleIndex = 0
        Me.gccIdCliente.Width = 115
        '
        'gccNombre
        '
        Me.gccNombre.Caption = "Nombre Cliente"
        Me.gccNombre.FieldName = "Nombre"
        Me.gccNombre.Name = "gccNombre"
        Me.gccNombre.OptionsColumn.ReadOnly = True
        Me.gccNombre.Visible = True
        Me.gccNombre.VisibleIndex = 1
        Me.gccNombre.Width = 274
        '
        'lbInfoRCV
        '
        Me.lbInfoRCV.Location = New System.Drawing.Point(18, 9)
        Me.lbInfoRCV.Name = "lbInfoRCV"
        Me.lbInfoRCV.Size = New System.Drawing.Size(226, 39)
        Me.lbInfoRCV.TabIndex = 34
        Me.lbInfoRCV.Text = "Listado de Clientes asignados por vendedor" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Para desvincular un cliente, por favo" & _
    "r ubiquese" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "en la fila deseada y presione ""Suprimir""."
        '
        'ceTodosClientes
        '
        Me.ceTodosClientes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ceTodosClientes.Location = New System.Drawing.Point(256, 38)
        Me.ceTodosClientes.Name = "ceTodosClientes"
        Me.ceTodosClientes.Properties.Caption = "Cotizar a todos los Clientes"
        Me.ceTodosClientes.Size = New System.Drawing.Size(149, 19)
        Me.ceTodosClientes.TabIndex = 36
        '
        'fac_frmVendedores
        '
        Me.ClientSize = New System.Drawing.Size(1370, 592)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.DbMode = 1
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmVendedores"
        Me.OptionId = "001003"
        Me.Text = "Vendedores"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.SplitContainerControl1, 0)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        CType(Me.gcComision, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvComision, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePorcComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teLimite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beIdEmpleado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.SplitContainerControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl2.ResumeLayout(False)
        CType(Me.gcVenClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvClientesVendedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceTodosClientes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdVendedor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ceActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTelefonos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tePorcComision As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teLimite As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beIdEmpleado As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcComision As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvComision As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcDesde As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcHasta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPorcentaje As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdVendedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ritePrecio As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents riteCodigo As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents SplitContainerControl2 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents gcVenClientes As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvClientesVendedor As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gccIdCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gccNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gccIdVendedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lbInfoRCV As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbtnImprimirCRV As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ceTodosClientes As DevExpress.XtraEditors.CheckEdit
End Class
