﻿
Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.GridControl
Imports System.Math
Imports System.Reflection

Public Class fac_frmPedidos
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blInve As New InventarioBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim blCxC As New CuentasPCBLL(g_ConnectionString)
    Dim Header As New fac_Pedidos, Detalle As List(Of fac_PedidosDetalle)
    Dim entProducto As inv_Productos, dtParametros As DataTable, TipoPrecio As Integer = 1, Existencia As Decimal, TipoAplicacion As Integer = 1
    Dim entCliente As New fac_Clientes, EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim UsuarioPuedeCambiarPrecios As Boolean = SiEsNulo(EntUsuario.CambiarPrecios, False), IdTipoPrecioUser As Integer = SiEsNulo(EntUsuario.IdTipoPrecio, 0)
    Dim entComprobante As adm_TiposComprobante, OtroModulo As String = "", IdCotizacion As Integer = 0, IdPedidoAnular As Integer = 0
    Dim DocumentoDetallaIva As Boolean, _AutorizadoPor As String = ""
    Dim LimiteLineas As Integer = 0, Precio As Decimal = 0.0, PrecioCosto As Decimal = 0.0, EsCompuesto As Boolean = False, TipoProducto As Integer = 1
    Dim TipoVenta As Integer = 1, lMensaje As Boolean, EsEspecial As Boolean
    Dim dtPermisos As DataTable = objMenu.ObtenerPermisosForma(objMenu.User, Me.Modulo, "002001")

    Private Sub fac_frmPedidos_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = bl.fac_ConsultaPedidos(objMenu.User)
        gv2.BestFitColumns()
    End Sub

    Private Sub fac_frmPedidos_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        CargaCombos()
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("FAC_PEDIDOS", "IdComprobante")
        dtParametros = blAdmon.ObtieneParametros()
        If dtParametros.Rows(0).Item("TipoContribuyente") = 3 And dtParametros.Rows(0).Item("EsRetenedor") Then
            ceRetencionPercepcion.Text = "APLICAR PERCEPCIÓN DEL 1 %"
            lblPercReten.Text = "(+) IVA PERCIBIDO:"
        End If
        If UsuarioPuedeCambiarPrecios Then
            Me.gcPorcDescto.OptionsColumn.ReadOnly = True
            'Me.gcPrecioUnitario.OptionsColumn.ReadOnly = True
            sePjeDescuento.Enabled = False
        Else
            Me.gcPorcDescto.OptionsColumn.ReadOnly = False
            'Me.gcPrecioUnitario.OptionsColumn.ReadOnly = False
            sePjeDescuento.Enabled = True
        End If
        ' para el caso de los clientes que no tenia este cambio, estara nulo este campo
        Me.gcDescripcion.OptionsColumn.ReadOnly = Not SiEsNulo(EntUsuario.AccesoProductos, True)

        If IdTipoPrecioUser = 0 Then
            TipoPrecio = dtParametros.Rows(0).Item("IdTipoPrecio")
        Else
            TipoPrecio = IdTipoPrecioUser
        End If

        gc2.DataSource = bl.fac_ConsultaPedidos(objMenu.User)
        gv.BestFitColumns()

        ActivarControles(False)
        'CargaControles(0)

        sbRevertirInventario.Visible = gsNombre_Empresa.StartsWith("KEIRAN, SOCIEDAD ANONIMA DE CAPITAL VARIABLE")
        SbCargarHoras.Visible = gsNombre_Empresa.StartsWith("SKYCOM")
        riteDescripcion.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        riteDescripcion.Mask.UseMaskAsDisplayFormat = True
        riCodProd.Mask.UseMaskAsDisplayFormat = True
        riCodProd.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx

    End Sub
    Private Sub fac_frmPedidos_Eliminar() Handles Me.Eliminar

        If xtcPedidos.SelectedTabPage.Name = "xtpLista" Then
            Header = objTablas.fac_PedidosSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Else
            Header = objTablas.fac_PedidosSelectByPK(teCorrelativo.EditValue)
        End If

        If Header.Reservado Then
            MsgBox("Es necesario que revierta el pedido. No se puede eliminar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If


        Try
            If MsgBox("Está seguro(a) de eliminar el pedido?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
                Header.ModificadoPor = objMenu.User
                objTablas.fac_PedidosUpdate(Header)

                objTablas.fac_PedidosDeleteByPK(Header.IdComprobante)
            End If
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("FAC_PEDIDOS", "IdComprobante")
            'CargaControles(0)
            gc2.DataSource = bl.fac_ConsultaPedidos(objMenu.User)
        Catch ex As Exception
            MsgBox("No se pudo eliminar el pedido: " & ex.Message, MsgBoxStyle.Information, "Nota")
        End Try
    End Sub

    Private Sub fac_frmPedidos_Nuevo() Handles Me.Nuevo

        Header = New fac_Pedidos
        gc.DataSource = bl.fac_ObtenerPedidoDetalle(-1, pnIVA, False, 1)
        gv.CancelUpdateCurrentRow()
        'gv.AddNewRow()

        beCodigo.EditValue = ""

        teNumero.EditValue = ""
        deFecha.EditValue = Today 'objFunciones.GetFechaContable(dtParametros.Rows(0).Item("IdSucursal")) ' 
        teDireccion.EditValue = ""
        teNombre.EditValue = ""
        teGiro.EditValue = ""
        teNit.EditValue = ""
        teNRC.EditValue = ""
        teNotas.EditValue = ""
        teTelefonos.EditValue = ""
        teAnticipo.EditValue = 0.0
        leSucursal.EditValue = piIdSucursalUsuario
        leBodega.EditValue = SiEsNulo(EntUsuario.IdBodega, 0)
        ceRetencionPercepcion.EditValue = False
        ceMarcarDevolucion.EditValue = False
        ceReservar.EditValue = False
        sePjeDescuento.EditValue = 0.0
        seDiasCredito.EditValue = 0
        leFormaPago.EditValue = 1
        leVendedor.EditValue = 0
        leTipoVenta.EditValue = 1
        leTipoDoc.EditValue = 6
        OtroModulo = ""
        IdCotizacion = 0
        IdPedidoAnular = 0
        CalcularTotalesGenerales()

        ActivarControles(True)

        xtcPedidos.SelectedTabPage = xtpDatos
        beCodigo.Focus()
    End Sub
    Private Sub fac_frmPedidos_Guardar() Handles Me.Guardar

        ' VERIFICO SI ESTAN MODIFICANDO Y A LA VEZ LO FACTURAN , NO PERMITIRA GUARDAR SI EN ESE TIEMPO LO FACTURAN
        If Not DbMode = DbModeType.insert Then
            Dim Facturado As Integer = bl.fac_VerificaFacturadoPedido(teCorrelativo.EditValue)
            If Facturado <> 0 Then
                MsgBox("No es posible editar el pedido. Ya fue facturado", MsgBoxStyle.Critical, "Nota")
                Exit Sub
            End If
        End If
        If Not EntUsuario.CambiarPrecios Then 'EL USUARIO NO PUEDE CAMBIAR LOS PRECIOS, POR LO TANTO REQUIERE AUTORIZACION... 
            If Not PreciosVerificados() Then ' SE VERIFICAN LOS PRECIOS QUE HA FACTURADO
                Return
            End If
        End If

        If Not DatosValidos() Then
            Return
        End If

        ' VERIFICO EL SALDO DEL CLIENTES CUANDO GUARDA
        If Not ValidaSaldoCliente() Then
            Return
        End If

        CargaEntidades()
        Dim msj As String = ""
        If DbMode = DbModeType.insert Then
            msj = bl.fac_InsertarPedido(Header, Detalle)
            teCorrelativo.EditValue = Header.IdComprobante
            teNumero.EditValue = Header.Numero
            If msj = "" Then
                MsgBox("El pedido fue guardado exitosamente", MsgBoxStyle.Information, "Nexus")
            Else
                MsgBox("SE GENERÓ UN ERROR AL TRATAR DE GUARDAR EL PEDIDO" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        Else
            msj = bl.fac_ActualizaPedido(Header, Detalle)
            If msj = "" Then
                MsgBox("El pedido fue guardado exitosamente", MsgBoxStyle.Information, "Nexus")
            Else
                MsgBox("SE GENERÓ UN ERROR AL TRATAR DE ACTUALIZAR EL PEDIDO" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If

        xtcPedidos.SelectedTabPage = xtpLista
        gc2.DataSource = bl.fac_ConsultaPedidos(objMenu.User)
        gc2.Focus()

        MostrarModoInicial()
        teCorrelativo.EditValue = Header.IdComprobante
        'CargaControles(0)
        ActivarControles(False)
        beCodigo.Focus()
        CalcularTotalesGenerales()
        gv.CancelUpdateCurrentRow()
        gv.AddNewRow()
        IdPedidoAnular = 0
    End Sub

    Private Sub fac_frmPedidos_Editar() Handles Me.Editar

        If xtcPedidos.SelectedTabPage.Name = "xtpLista" Then
            Header = objTablas.fac_PedidosSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Else
            Header = objTablas.fac_PedidosSelectByPK(teCorrelativo.EditValue)
        End If

        If Header.Reservado Then
            MsgBox("Es necesario que revierta el pedido. No se puede editar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If

        If Header.Facturado Then
            MsgBox("No es posible editar la orden. Ya fue facturada", MsgBoxStyle.Critical, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        ActivarControles(True)
        CalcularTotalesGenerales()
    End Sub

    Private Sub fac_frmPedidos_Reporte() Handles Me.Reporte
        Header = objTablas.fac_PedidosSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Dim dt As DataTable = bl.fac_ObtenerPedido(Header.IdComprobante)
        Dim rpt As New fac_rptPedido() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.ShowPreviewDialog()
    End Sub
    
    Private Function DatosValidos() As Boolean

        Dim msj As String = ""
        If beCodigo.EditValue = "" Then
            msj = "Debe de especificar el código del cliente"
        End If
        If teNombre.EditValue = "" Then
            msj = "Debe de especificar el nombre del cliente"
        End If

        If leVendedor.EditValue = 0 Then
            msj = "Debe de especificar el vendedor"
        End If

        If leTipoVenta.EditValue = 0 Then
            msj = "Debe especificar el tipo de venta"
        End If

        If beCodigo.EditValue = "123" And leFormaPago.EditValue = 2 Then
            msj = "No puede realizar el pedido al crédito para este cliente"
        End If

        If leTipoDoc.EditValue = 7 And leTipoVenta.EditValue <> 3 Then
            msj = "La exportación debe ser con el tipo de impuesto Tasa Cero"
        End If

        If leTipoVenta.EditValue = 3 And leTipoDoc.EditValue <> 7 Then
            msj = "El tipo de impuesto para el documento seleccionado no puede ser Tasa Cero"
        End If

        entCliente = objTablas.fac_ClientesSelectByPK(beCodigo.EditValue)
        If entCliente.BloquearFacturacion = True And leFormaPago.EditValue = 2 Then
            msj = "El cliente se encuentra bloqueado, Consulte con el Departamento de Cobros"
        End If

        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        entComprobante = objTablas.adm_TiposComprobanteSelectByPK(leTipoDoc.EditValue)
        TipoAplicacion = entComprobante.TipoAplicacion

        Dim dtRenu As DataTable = bl.fac_ObtenerPedidoDetalle(-1, pnIVA, False, 1)
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

        Dim i As Integer = 0
        If gsNombre_Empresa.StartsWith("PITUTA") Then
            For i = 0 To gv.DataRowCount - 1
                If blInve.inv_ObtieneTipoProducto(SiEsNulo(gv.GetRowCellValue(i, "IdProducto"), "")) = 1 Then 'REVISANDO SI SON PRODUCTOS INVENTARIADOS
                    If gv.GetRowCellValue(i, "IdProducto") = gv.GetRowCellValue(i + 1, "IdProducto") Then
                        msj = "El producto " & gv.GetRowCellValue(i, "IdProducto") & ", está repetido"
                        Exit For
                    End If
                End If
            Next
        End If

        For l = 0 To gv.DataRowCount - 1
            Dim dr1 As DataRow = dtRenu.NewRow()
            dr1("IdProducto") = gv.GetRowCellValue(l, "IdProducto")
            dr1("Cantidad") = gv.GetRowCellValue(l, "Cantidad")
            dtRenu.Rows.Add(dr1)
        Next

        ' AGRUPACION DE PRODUCTOS PARA QUE SE PUEDAN REPETIR CODIGOS
        Dim query = From q In dtRenu.AsEnumerable() Select q Order By q.Item("IdProducto")
        Dim dtResultado As New DataTable
        dtResultado.Columns.Add("IdProducto")
        dtResultado.Columns.Add("Cantidad")

        Dim dtCopy As New DataTable 'query.CopyToDataTable()
        dtCopy = query.CopyToDataTable()
        dtCopy.Rows.Add()
        Dim dr As DataRow = dtCopy.NewRow()
        Dim value As Integer
        i = 0
        For j As Integer = 0 To dtCopy.Rows.Count - 2
            Dim item = dtCopy.Rows(j)
            Dim IdPro = Convert.ToString(item("IdProducto")).ToUpper
            Dim cant = Convert.ToDecimal(item("Cantidad"))
            Dim drr As DataRow = dtResultado.NewRow()
            drr.Item(0) = IdPro

            'dtResultado.ImportRow(item)
            Dim filaSig As String = Convert.ToString(dtCopy.Rows(i + 1).Item("IdProducto")).ToUpper 'fila siguiente
            If (IdPro = filaSig) Then 'producto actual es igual a la siguiente zona
                value += cant
            Else 'cuando cambie el producto insertar nueva fila 
                drr.Item(1) = value + cant
                dtResultado.Rows.Add(drr)
                value = 0
            End If
            i += 1 'indice
        Next

        'FIN DE AGRUPACION DE PRODUCTOS
        For h = 0 To dtResultado.Rows.Count - 1

            If blInve.inv_ObtieneTipoProducto(SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), "")) > 1 Then 'PARA TODOS LOS SERVICIOS NO APLICA
                Exit For
            End If

            EsCompuesto = blInve.inv_ProductoEsCompuesto(SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), ""))

            If dtParametros.Rows(0).Item("ValidarExistencias") = True Then
                If TipoAplicacion = 1 And Not ceMarcarDevolucion.EditValue Then 'es una venta, no devolución
                    If Not EsCompuesto Then
                        Dim dtExistencia As DataTable = blInve.inv_ObtieneExistenciaProducto(SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), ""), leBodega.EditValue, deFecha.EditValue)
                        Existencia = 0.0
                        PrecioCosto = 0.0
                        If dtExistencia.Rows.Count > 0 Then
                            Existencia = dtExistencia.Rows(0).Item("SaldoExistencias")
                            PrecioCosto = dtExistencia.Rows(0).Item("PrecioCosto")
                        End If

                        'Existencia = CDec(blInve.inv_ObtieneExistenciaProducto(SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), ""), leBodega.EditValue).Rows(0).Item("SaldoExistencias"))
                        If Existencia < SiEsNulo(dtResultado.Rows(h).Item("Cantidad"), 0) And Not ceMarcarDevolucion.EditValue Then
                            msj = "El producto --> " & SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), "") & ", no tiene existencia suficiente a fecha " + Format(deFecha.EditValue, "dd/MM/yyyy")
                            Exit For
                        End If
                        If deFecha.EditValue < Today Then
                            dtExistencia = blInve.inv_ObtieneExistenciaProducto(SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), ""), leBodega.EditValue, Today)
                            Existencia = 0.0
                            PrecioCosto = 0.0
                            If dtExistencia.Rows.Count > 0 Then
                                Existencia = dtExistencia.Rows(0).Item("SaldoExistencias")
                                PrecioCosto = dtExistencia.Rows(0).Item("PrecioCosto")
                            End If
                            If Existencia < SiEsNulo(dtResultado.Rows(h).Item("Cantidad"), 0) And Not ceMarcarDevolucion.EditValue Then
                                msj = "El producto --> " & SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), "") & ", no tiene existencia suficiente a fecha " + Format(Today, "dd/MM/yyyy")
                                Exit For
                            End If
                        End If
                    Else
                        Dim dtExistenciaKit As DataTable = blInve.inv_ObtieneExistenciaKit(dtResultado.Rows(h).Item("IdProducto"), leBodega.EditValue, deFecha.EditValue)
                        If SiEsNulo(dtExistenciaKit.Rows(0).Item("CantidadDisponible"), 0.0) < SiEsNulo(dtResultado.Rows(h).Item("Cantidad"), 0) And TipoAplicacion = 1 And Not ceMarcarDevolucion.EditValue Then
                            msj = "Código de producto no registrado o sin existencia suficiente en la composición del KIT"
                            Exit For
                        End If
                    End If
                Else

                End If
            End If
        Next

        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.None


        Dim NumDocumento As String = teNit.EditValue
        If leTipoDoc.EditValue = 5 Or leTipoDoc.EditValue = 8 Then
            If teNRC.EditValue = "" And teNit.EditValue = "" Then
                msj = "Debe colocar número de Registro ó NIT "
            End If
            If teNit.EditValue <> "" And NumDocumento.Length <> 17 Then
                msj = "Debe colocar un número de NIT con el formato 9999-999999-999-9"
            End If
        Else
            If teNit.EditValue = "" And teTotal.EditValue >= 200.0 Then
                msj = "Debe colocar número de DUI ó NIT"
            End If
            If teNit.EditValue <> "" And (NumDocumento.Length <> 10 And NumDocumento.Length <> 17) And teTotal.EditValue >= 200.0 Then
                msj = "Debe colocar un número de DUI con formato 99999999-9 o NIT 9999-999999-999-6"
            End If
        End If

        If msj = "" Then
            Return True
        Else
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If

    End Function
    Private Sub CargaCombos()
        objCombos.inv_Bodegas(leBodega)
       
        objCombos.fac_FormasPago(leFormaPago)
        objCombos.fac_TiposComprobante(leTipoDoc, "")
       
        objCombos.fac_Vendedores(leVendedor)
        objCombos.inv_Bodegas(leBodega)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_TiposImpuesto(leTipoVenta)
        leSucursal.EditValue = bl.GetIdSucursal
    End Sub
    Private Sub CargaEntidades()
        Header = New fac_Pedidos
        CalcularTotalesGenerales()
        gv.UpdateTotalSummary()

        ''Dim entCli As fac_Clientes = objTablas.fac_ClientesSelectByPK(beCodigo.EditValue)  'esto no es necesario, ya estaba entCliente
        With Header
            .IdComprobante = teCorrelativo.EditValue
            .Numero = teNumero.EditValue
            .Fecha = deFecha.EditValue 'objFunciones.GetFechaContable(dtParametros.Rows(0).Item("IdSucursal")) 
            .Nombre = teNombre.EditValue
            .Nrc = teNRC.EditValue
            .Nit = teNit.EditValue
            .Giro = teGiro.EditValue
            .Direccion = teDireccion.EditValue
            .IdCliente = beCodigo.EditValue
            .IdSucursal = leSucursal.EditValue
            .IdFormaPago = leFormaPago.EditValue
            .DiasCredito = seDiasCredito.EditValue
            .Notas = teNotas.EditValue
            .IdTipoComprobante = leTipoDoc.EditValue
            .IdVendedor = leVendedor.EditValue
            .IdBodega = leBodega.EditValue
            .Telefono = teTelefonos.EditValue
            .Facturado = False
            .Devolucion = ceMarcarDevolucion.EditValue
            .IdComprobanteFactura = 0
            .Anticipo = teAnticipo.EditValue
            .TotalAfecto = Me.gcPrecioTotal.SummaryItem.SummaryValue
            .TotalIva = teIVA.EditValue
            .TotalImpuesto1 = teRetencionPercepcion.EditValue
            .TotalComprobante = teTotal.EditValue
            .AplicaRetencionPercepcion = ceRetencionPercepcion.EditValue
            .PorcDescuento = sePjeDescuento.EditValue
            .TipoVenta = leTipoVenta.EditValue
            .TipoImpuesto = -1  'SE ESTÁ PONIENDO -1 PARA DEMOSTRAR QUE YA NO SE OCUPA ESTA COLUMNA, SE ESTÁ USANDO TipoVenta
            .Reservado = ceReservar.EditValue
            .IdDepartamento = entCliente.IdDepartamento
            .IdMunicipio = entCliente.IdMunicipio
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .FechaHoraModificacion = Nothing
            .ModificadoPor = ""
        End With

        Detalle = New List(Of fac_PedidosDetalle)

        entComprobante = objTablas.adm_TiposComprobanteSelectByPK(leTipoDoc.EditValue)
        DocumentoDetallaIva = entComprobante.DetallaIVA

        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New fac_PedidosDetalle
            With entDetalle
                .IdComprobante = Header.IdComprobante
                .IdDetalle = i + 1
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .IdPrecio = gv.GetRowCellValue(i, "IdPrecio")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .TipoProducto = gv.GetRowCellValue(i, "TipoProducto")
                .PrecioVenta = gv.GetRowCellValue(i, "PrecioVenta")
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .PrecioTotal = gv.GetRowCellValue(i, "PrecioTotal")

                .PrecioVenta = gv.GetRowCellValue(i, "PrecioVenta")
                .PorcDescuento = gv.GetRowCellValue(i, "PorcDescuento")
                .ValorDescuento = .PrecioVenta - gv.GetRowCellValue(i, "PrecioUnitario")
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .PrecioTotal = gv.GetRowCellValue(i, "PrecioTotal")
                .VentaNeta = gv.GetRowCellValue(i, "VentaNeta")
                .ValorIva = gv.GetRowCellValue(i, "ValorIva")
                .PrecioCosto = gv.GetRowCellValue(i, "PrecioCosto")
                .EsEspecial = gv.GetRowCellValue(i, "EsEspecial")
                .EsCompuesto = gv.GetRowCellValue(i, "EsCompuesto")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With

            Detalle.Add(entDetalle)
        Next
    End Sub

#Region "Grid"
    Private Sub gv_FocusedColumnChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs) Handles gv.FocusedColumnChanged
        gv.ClearColumnErrors()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        'gv.SetRowCellValue(gv.FocusedRowHandle, "IdProducto", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdPrecio", IdTipoPrecioUser)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioVenta", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PorcDescuento", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescuento", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "VentaNeta", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorIva", 0.0)
    End Sub
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteRow(gv.FocusedRowHandle)
        CalcularTotalesGenerales()
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        CalcularTotalesGenerales()
    End Sub
    Private Sub CalcularTotalesGenerales()
        gv.UpdateTotalSummary()
        'PREPARO EL VALOR NETO
        Dim totalNeto As Decimal = Decimal.Round(Me.gcPrecioTotal.SummaryItem.SummaryValue, 2)
        If leTipoVenta.EditValue = 1 And DocumentoDetallaIva = False Then  'SI LA VENTA ES GRAVADA
            totalNeto = Decimal.Round(totalNeto / (pnIVA + 1), 2)
        End If

        If leTipoDoc.EditValue = 7 Or leTipoVenta.EditValue > 1 Then  'FACTURA DE EXPORTACION, EXENTA O TASA CERO
            teIVA.EditValue = 0.0
        Else
            If DocumentoDetallaIva = False Then
                teIVA.EditValue = Me.gcPrecioTotal.SummaryItem.SummaryValue - totalNeto
            Else
                teIVA.EditValue = Decimal.Round(totalNeto * pnIVA, 2)
            End If
        End If

        Dim IvaRetPer As Decimal = teIVA.EditValue
        If ceRetencionPercepcion.Checked And totalNeto > 100 And leTipoVenta.EditValue = 1 And leTipoDoc.EditValue <> 7 Then
            If dtParametros.Rows(0).Item("TipoContribuyente") = 3 And dtParametros.Rows(0).Item("EsRetenedor") Then
                teRetencionPercepcion.EditValue = Decimal.Round(totalNeto * CDec(dtParametros.Rows(0).Item("PorcPercepcion") / 100), 2)
                IvaRetPer += teRetencionPercepcion.EditValue
            Else
                teRetencionPercepcion.EditValue = Decimal.Round(totalNeto * CDec(dtParametros.Rows(0).Item("PorcRetencion") / 100), 2)
                IvaRetPer -= teRetencionPercepcion.EditValue
            End If
        Else
            teRetencionPercepcion.EditValue = 0.0
        End If

        'PENDIENTE DE CONFIRMAR CUANDO LA RETENCION VA RESTADA
        teTotal.EditValue = Me.gcPrecioTotal.SummaryItem.SummaryValue + IvaRetPer

        If EntUsuario.CambiarPrecios Then
            leTipoDoc.Properties.ReadOnly = teTotal.EditValue <> 0.0
        End If

        'beCodigo.Properties.ReadOnly = TotalGral <> 0.0
        leBodega.Properties.ReadOnly = teTotal.EditValue <> 0.0
        leTipoVenta.Properties.ReadOnly = teTotal.EditValue <> 0.0

        leVendedor.Enabled = Not (teTotal.EditValue <> 0.0 And (ceMarcarDevolucion.EditValue Or leTipoDoc.EditValue = 8))

        If (teTotal.EditValue > 200) And leTipoDoc.EditValue = 6 And Not lMensaje And deFecha.Properties.ReadOnly = False Then
            MsgBox("LA VENTA SOBREPASA LOS $200.00, DEBERÁ DE SOLICITAR DUI O NIT", MsgBoxStyle.Information, "NOTA")
            lMensaje = True
        End If

    End Sub

    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab OrElse e.KeyCode = Keys.Down OrElse e.KeyCode = Keys.Right OrElse e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.Left Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProduc = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gv.EditingValue = IdProduc
                End If

                '-- valido los precios, por si cambian un codigo, que se refresque el precio automaticamente
                If blInve.inv_VerificaPrecioUsuario(objMenu.User, gv.GetFocusedRowCellValue("IdPrecio")) Then
                    MsgBox("Tipo de precio no definido o no autorizado para éste usuario", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("IdPrecio", 1)
                End If

                'EXTRAIGO EL PRECIO SETEADO, CON IVA INCLUIDO
                Precio = blInve.inv_ObtienePrecioProducto(gv.EditingValue, gv.GetFocusedRowCellValue("IdPrecio"))

                'ACA SE LLAMA LA ENTIDAD DE PRODUCTO Y SE SETEAN LOS DATOS QUE SE REQUIEREN VALIDAR SIEMPRE
                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                entComprobante = objTablas.adm_TiposComprobanteSelectByPK(leTipoDoc.EditValue)
                TipoAplicacion = entComprobante.TipoAplicacion

                'DETERMINO SI ES UNA VENTA EXENTA Y SI EL PRODUCTO NO ES EXENTO PARA EXTRAERLE EL IVA
                If leTipoVenta.EditValue > 1 And Not entProducto.EsExento Then 'SOLO PARA LOS PRODUCTOS EXENTOS O VENTA EXENTA SE LE EXTRAE EL IVA
                    Precio = Round(Precio / (pnIVA + 1), 4)
                End If

                gv.SetFocusedRowCellValue("PrecioVenta", Precio)
                Precio = gv.GetFocusedRowCellValue("PrecioVenta") * gv.GetFocusedRowCellValue("PorcDescuento") / 100

                gv.SetFocusedRowCellValue("ValorDescuento", Precio)
                Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Precio
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)

                'LOS TOTALES SON CON IVA INCLUIDO
                gv.SetFocusedRowCellValue("PrecioTotal", Decimal.Round(gv.GetFocusedRowCellValue("Cantidad") * gv.GetFocusedRowCellValue("PrecioUnitario"), 2))
                CalcularTotalesGenerales()

                'SETEO EL PRECIO DE COSTO Y EL TIPO DE PRODUCTO AL ROW ACTUAL PARA NO VOLVER A LLAMAR A LA BDD
                PrecioCosto = SiEsNulo(blInve.inv_ObtienePrecioCosto(entProducto.IdProducto, deFecha.EditValue), 0.0)
                gv.SetFocusedRowCellValue("PrecioCosto", PrecioCosto)
                gv.SetFocusedRowCellValue("TipoProducto", entProducto.TipoProducto)

                'SI EL PRODUCTO ES ESPECIAL O COMPUESTO, para efectos de no validar el costo
                gv.SetFocusedRowCellValue("EsEspecial", entProducto.EsEspecial)
                gv.SetFocusedRowCellValue("EsCompuesto", entProducto.Compuesto)
                TipoProducto = entProducto.TipoProducto
                EsEspecial = entProducto.EsEspecial
                EsCompuesto = entProducto.Compuesto

                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
                Else
                    If dtParametros.Rows(0).Item("ValidarExistencias") = True Then

                        Existencia = CDec(blInve.inv_ObtieneExistenciaProducto(gv.EditingValue, leBodega.EditValue, deFecha.EditValue).Rows(0).Item("SaldoExistencias"))
                        If Existencia < gv.GetFocusedRowCellValue("Cantidad") And TipoAplicacion = 1 And entProducto.TipoProducto = 1 And Not ceMarcarDevolucion.EditValue Then
                            MsgBox("Código de producto no registrado o sin existencia suficiente a fecha " + deFecha.EditValue.ToString(), MsgBoxStyle.Critical, "Error")
                            gv.SetFocusedRowCellValue("Descripcion", ", PRODUCTO SIN SALDO --")
                        Else
                            If deFecha.EditValue < Today Then
                                Existencia = CDec(blInve.inv_ObtieneExistenciaProducto(gv.EditingValue, leBodega.EditValue, Today.ToString()).Rows(0).Item("SaldoExistencias"))
                                If Existencia < gv.GetFocusedRowCellValue("Cantidad") And TipoAplicacion = 1 And entProducto.TipoProducto = 1 And Not ceMarcarDevolucion.EditValue Then
                                    MsgBox("Código de producto no registrado o sin existencia suficiente a fecha " + +Format(Today, "dd/MM/yyyy"), MsgBoxStyle.Critical, "Error")
                                    gv.SetFocusedRowCellValue("Descripcion", ", PRODUCTO SIN SALDO --")
                                Else
                                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                                End If
                            Else
                                gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                            End If
                        End If
                    Else
                        gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                    End If

                    gcEx.DataSource = blInve.inv_ObtieneSaldosPorProducto(-1, gv.EditingValue)
                    Dim ArchivoImagen As String = entProducto.ArchivoImagen
                    If Not FileIO.FileSystem.FileExists(ArchivoImagen) Or ArchivoImagen = "" Then
                        peFoto.Image = Nothing
                    Else
                        Dim bm As New Bitmap(ArchivoImagen)
                        peFoto.Image = bm
                    End If
                End If
                lMensaje = False
            End If

            If gv.FocusedColumn.FieldName = "IdPrecio" Then
                Precio = blInve.inv_ObtienePrecioProducto(SiEsNulo(gv.GetFocusedRowCellValue("IdProducto"), ""), gv.EditingValue)

                If leTipoVenta.EditValue > 1 Then 'si el producto es exento , PENDIENTE DE VER QUE EL PRODUCTO SEA EXENTO EN LA FICHA
                    Precio = Round(Precio / (pnIVA + 1), 4)
                End If
                gv.SetFocusedRowCellValue("PrecioVenta", Precio)
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)
                gv.SetFocusedRowCellValue("PrecioTotal", Decimal.Round(gv.GetFocusedRowCellValue("Cantidad") * gv.GetFocusedRowCellValue("PrecioUnitario"), 2))


                CalcularTotalesGenerales()
            End If
        End If
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow

        'SOLAMENTE SE VALIDAN VALORES OBLIGATORIOS QUE NO PUEDEN QUEDAR VACIOS AL TRATAR DE ABANDONAR LA FILA
        Dim IdProduc As String = SiEsNulo(gv.GetFocusedRowCellValue("IdProducto"), "")
        Dim IdPrecio As Integer = SiEsNulo(gv.GetFocusedRowCellValue("IdPrecio"), 0)
        Precio = SiEsNulo(gv.GetFocusedRowCellValue("PrecioUnitario"), 0.0)

        'ESTA LLAMADA YA NO SE REQUIERE PORQUE YA ESTÁ SETEADO EN EL ROW ACTUAL, REALIZADO EN EL KEYDOWN AL MOMENTO DE SELECCIONAR EL PRODUCTO
        'Dim PrecioCosto As Decimal = SiEsNulo(blInve.inv_ObtienePrecioCosto(IdProduc, deFecha.EditValue), 0.0)
        PrecioCosto = SiEsNulo(gv.GetFocusedRowCellValue("PrecioCosto"), 0.0)
        TipoProducto = SiEsNulo(gv.GetFocusedRowCellValue("TipoProducto"), 0)
        EsEspecial = SiEsNulo(gv.GetFocusedRowCellValue("EsEspecial"), False)
        EsCompuesto = SiEsNulo(gv.GetFocusedRowCellValue("EsCompuesto"), False)
        If IdProduc = "" OrElse Not blInve.inv_VerificaCodigoProducto(IdProduc) Then
            e.Valid = False
            gv.SetColumnError(gv.Columns("IdProducto"), "Código de producto no válido o no existe")
        End If

        If IdPrecio = 0 Then
            e.Valid = False
            gv.SetColumnError(gv.Columns("IdPrecio"), "Debe de especificar el precio")
        End If

        lMensaje = False
        CalcularTotalesGenerales()
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Select Case gv.FocusedColumn.FieldName
            Case "IdPrecio"

                If blInve.inv_VerificaPrecioUsuario(objMenu.User, e.Value) Then
                    MsgBox("Tipo de precio no definido o no autorizado para éste usuario", MsgBoxStyle.Critical, "Error")
                    e.Value = 1
                End If

                Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), e.Value)
                If leTipoVenta.EditValue > 1 Then
                    Precio = Round(Precio / (pnIVA + 1), 4)
                End If

                gv.SetFocusedRowCellValue("PrecioVenta", Precio)
                Precio = gv.GetFocusedRowCellValue("PrecioVenta") * gv.GetFocusedRowCellValue("PorcDescuento") / 100
                Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Precio

                'VALIDO QUE EL PRECIO DEL PRODUCTO NO SEA MENOR AL COSTO
                PrecioCosto = gv.GetFocusedRowCellValue("PrecioCosto")
                TipoProducto = gv.GetFocusedRowCellValue("TipoProducto")
                EsEspecial = gv.GetFocusedRowCellValue("EsEspecial")

                If Precio < PrecioCosto And Not ceReservar.EditValue And TipoProducto = 1 And Not EsEspecial Then
                    MsgBox("El precio del producto no puede ser menor al precio de costo", MsgBoxStyle.Critical, "Error")
                    e.Value = 1

                    Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), e.Value)
                    If leTipoVenta.EditValue > 1 Or DocumentoDetallaIva Then
                        Precio = Round(Precio / (pnIVA + 1), 4)
                    End If

                    gv.SetFocusedRowCellValue("PrecioVenta", Precio)
                    Precio = gv.GetFocusedRowCellValue("PrecioVenta") * gv.GetFocusedRowCellValue("PorcDescuento") / 100
                    Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Precio
                End If
                gv.SetFocusedRowCellValue("ValorDescuento", 0.0)
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)
                gv.SetFocusedRowCellValue("PrecioTotal", Decimal.Round(gv.GetFocusedRowCellValue("Cantidad") * gv.GetFocusedRowCellValue("PrecioUnitario"), 2))
                CalcularTotalesGenerales()

            Case "PorcDescuento"
                
                Precio = gv.GetFocusedRowCellValue("PrecioVenta") * e.Value / 100
                gv.SetFocusedRowCellValue("VaorDescuento", Precio)

                Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Precio
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)
                gv.SetFocusedRowCellValue("PrecioTotal", Decimal.Round(gv.GetFocusedRowCellValue("Cantidad") * gv.GetFocusedRowCellValue("PrecioUnitario"), 2))
                CalcularTotalesGenerales()

            Case "Cantidad"
                gv.SetFocusedRowCellValue("PrecioTotal", Decimal.Round(e.Value * gv.GetFocusedRowCellValue("PrecioUnitario"), 2))
                CalcularTotalesGenerales()

                'YA NO SE REQUIERE HACER LLAMADA A LA BDD DE INV_PRODUCTOS
                'Dim entProducto As inv_Productos = objTablas.inv_ProductosSelectByPK(gv.GetFocusedRowCellValue("IdProducto"))
                entComprobante = objTablas.adm_TiposComprobanteSelectByPK(leTipoDoc.EditValue)
                TipoAplicacion = entComprobante.TipoAplicacion
                TipoProducto = gv.GetFocusedRowCellValue("TipoProducto")
                EsEspecial = gv.GetFocusedRowCellValue("EsEspecial")
                EsCompuesto = gv.GetFocusedRowCellValue("EsCompuesto")

                If dtParametros.Rows(0).Item("ValidarExistencias") = True Then
                    If EsCompuesto Then
                        Dim dtExistenciaKit As DataTable = blInve.inv_ObtieneExistenciaKit(gv.GetFocusedRowCellValue("IdProducto"), leBodega.EditValue, deFecha.EditValue)
                        If SiEsNulo(dtExistenciaKit.Rows(0).Item("CantidadDisponible"), 0.0) < e.Value And TipoAplicacion = 1 And Not ceMarcarDevolucion.EditValue Then
                            MsgBox("Esta cantidad del KIT sobregira el inventario. No podrá guardar el Documento", MsgBoxStyle.Critical, "Error")
                        End If
                    Else
                        If e.Value > Existencia And TipoAplicacion = 1 And TipoProducto = 1 And Not ceMarcarDevolucion.EditValue Then
                            MsgBox("Esta cantidad sobregira el inventario. No podrá guardar el Documento", MsgBoxStyle.Critical, "Error")
                        End If
                    End If
                End If

            Case "PrecioUnitario"

                'VALIDO QUE EL PRECIO DEL PRODUCTO NO SEA MENOR AL COSTO
                PrecioCosto = SiEsNulo(gv.GetFocusedRowCellValue("PrecioCosto"), 0.0)
                TipoProducto = SiEsNulo(gv.GetFocusedRowCellValue("TipoProducto"), 0)
                EsEspecial = gv.GetFocusedRowCellValue("EsEspecial")

                If e.Value < PrecioCosto And Not ceReservar.EditValue And TipoProducto = 1 And Not EsEspecial Then
                    MsgBox("El precio del producto no puede ser menor al precio de costo", MsgBoxStyle.Critical, "Error")
                End If

                'If e.Value < SiEsNulo(gv.GetFocusedRowCellValue("PrecioVenta"), 0.0) And Not PuedeCambiarPrecios Then
                '    frmObtienePassword.Text = "AUTORIZACIÓN POR REBAJA"
                '    frmObtienePassword.Usuario = objMenu.User
                '    frmObtienePassword.TipoAcceso = 3
                '    frmObtienePassword.ShowDialog()
                '    If frmObtienePassword.Acceso = False Then
                '        MsgBox("No fue posible autorizar esta rebaja de precio", MsgBoxStyle.Critical, "Nota")
                '        gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", "")
                '        e.Value = SiEsNulo(gv.GetFocusedRowCellValue("PrecioVenta"), 0.0)
                '    End If

                '    frmObtienePassword.Dispose()
                'End If
                gv.SetFocusedRowCellValue("PrecioTotal", Decimal.Round(gv.GetFocusedRowCellValue("Cantidad") * e.Value, 2))
                CalcularTotalesGenerales()
        End Select
    End Sub


#End Region
    Private Sub teNotas_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles teNotas.LostFocus
        If gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
        End If
    End Sub
    Private Sub CalculaTotal(ByVal Cantidad As Decimal, ByVal Precio As Decimal)
        'revisar este codigo detenidamente
        TipoVenta = leTipoVenta.EditValue
        'cuando el iva esta incluído en la facturacion. El precio del producto está guardado con IVA incluido
        Precio = Decimal.Round(Cantidad * Precio, 2)
        If TipoVenta = 1 Or TipoVenta = 3 Then 'precio gravado o con tasa cero
            gv.SetFocusedRowCellValue("PrecioTotal", Precio)

            If DocumentoDetallaIva Then 'crédito fiscal, nota de crédito, nota de débito
                gv.SetFocusedRowCellValue("VentaNeta", Precio)
                gv.SetFocusedRowCellValue("ValorIva", Decimal.Round(Precio * pnIVA, 2))
            Else 'ticket, consumidor final, factura de exportación
                If leTipoDoc.EditValue = 7 Then
                    gv.SetFocusedRowCellValue("VentaNeta", gv.GetFocusedRowCellValue("PrecioTotal"))
                    gv.SetFocusedRowCellValue("ValorIva", 0.0)
                Else
                    Precio = Decimal.Round(Precio / (pnIVA + 1), 2) '4
                    gv.SetFocusedRowCellValue("VentaNeta", Precio)
                    gv.SetFocusedRowCellValue("ValorIva", gv.GetFocusedRowCellValue("PrecioTotal") - Precio)
                End If
            End If

            If TipoVenta = 3 Then  'ES UNA VENTA EXENTA
                gv.SetFocusedRowCellValue("ValorIva", 0.0)
            End If

        End If
        If TipoVenta = 2 Then 'precio exento
            gv.SetFocusedRowCellValue("PrecioTotal", Precio)
            gv.SetFocusedRowCellValue("VentaNeta", Precio)
            gv.SetFocusedRowCellValue("ValorIva", 0.0)
        End If
    End Sub
    Private Sub beIdCliente_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCodigo.ButtonClick
        ' si ya tiene valor la venta no puede cambiar el clientes, porque al llamar el cliente cambio en documento
        ' y puede haber problemas de cambiar de ccf a fa o biceversa
        'If TotalGral <> 0.0 Then  ' SE ACTIVO DE NUEVO, CUANDO CAMBIEN DE CLIENTE RECALCULARA EL DOCUMENTO
        '    Exit Sub
        'End If

        beCodigo.EditValue = ""
        entCliente.CreadoPor = "clic"
        OtroModulo = ""
        beIdCliente_Validated(beCodigo, New System.EventArgs)
    End Sub

    Private Sub beIdCliente_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCodigo.Validated
        If entCliente.CreadoPor <> "clic" And beCodigo.EditValue = "" Then
            Exit Sub
        End If

        If OtroModulo <> "" Then
            Exit Sub
        End If

        If beCodigo.Properties.ReadOnly Then
            Exit Sub
        End If

        ' SI EL CLIENTE ES DIFERENTE AL CODIGO ACTUAL, TOMA LOS DATOS DE LA FICHA DEL CLIENTE
        If Not Header.IdCliente <> beCodigo.EditValue And beCodigo.EditValue <> "" Then
            Exit Sub
        End If
        entCliente.IdCliente = beCodigo.EditValue
        entCliente = objConsultas.cnsClientes(fac_frmConsultaClientes, entCliente.IdCliente)
        beCodigo.EditValue = entCliente.IdCliente

        If Not ValidaSaldoCliente() Then
            Return
        End If

        With entCliente
            teNombre.EditValue = .Nombre
            teDireccion.EditValue = .Direccion
            teNit.EditValue = .Nit
            teNRC.EditValue = .Nrc
            teGiro.EditValue = .Giro
            leTipoDoc.EditValue = IIf(.IdTipoComprobante = 0 Or .IdTipoComprobante = Nothing, 6, .IdTipoComprobante)
            leFormaPago.EditValue = .IdFormaPago
            leVendedor.EditValue = .IdVendedor         
            ceRetencionPercepcion.EditValue = .AplicaRetencion
            sePjeDescuento.EditValue = .PorcDescuento
            seDiasCredito.EditValue = .DiasCredito
            leTipoVenta.EditValue = IIf(.IdTipoImpuesto = 0 Or .IdTipoImpuesto = Nothing, 1, .IdTipoImpuesto)
            .CreadoPor = "usuario"
        End With
        teNombre.Focus()
        'leTipoDoc_EditValueChanged("", New EventArgs)

        If entCliente.BloquearFacturacion = True And leFormaPago.EditValue = 2 Then
            MsgBox("El cliente se encuentra bloqueado, Consulte con el Departamento de Cobros", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        OtroModulo = ""
    End Sub

    Private Function ValidaSaldoCliente() As Boolean

        'entCliente = objTablas.fac_ClientesSelectByPK(beCodigo.EditValue)


        Dim SaldoCliente As Decimal = 0.0, TotalPedido As Decimal = 0.0, DocumentosVencidos As Integer = 0
        Dim msg As String = ""

        gv.UpdateTotalSummary()
        TotalPedido = gcPrecioTotal.SummaryItem.SummaryValue
        SaldoCliente = SiEsNulo(blCxC.SaldoCliente(beCodigo.EditValue, deFecha.EditValue, leSucursal.EditValue), 0.0)
        DocumentosVencidos = SiEsNulo(blCxC.DocumentosVencidos(beCodigo.EditValue, deFecha.EditValue), 0.0)


        SaldoCliente = SaldoCliente + TotalPedido
        If leTipoDoc.EditValue <> 8 And Not ceMarcarDevolucion.EditValue And leFormaPago.EditValue = 2 And beCodigo.EditValue <> "" Then
            If SiEsNulo(entCliente.LimiteCredito, 0) > 0 Then
                If SaldoCliente > SiEsNulo(entCliente.LimiteCredito, 0) Then
                    msg = "No puede crear el documento" & _
                    ", El Saldo por Cobrar al cliente excede su limite"
                End If
            End If

            If DocumentosVencidos > 0 Then
                msg = "No puede crear el documento" & _
                ", El Cliente Tiene documentos de cobros vencidos"
            End If
        End If

        If msg = "" Then
            Return True
        Else
            If MsgBox(msg + Chr(13) + " ¿Desea autorizar para continuar?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
                frmObtienePassword.Usuario = objMenu.User
                frmObtienePassword.TipoAcceso = 1
                frmObtienePassword.ShowDialog()

                If frmObtienePassword.Acceso = False Then
                    Return False
                Else
                    Return True
                End If
            Else
                Return False
            End If

            'MsgBox(msg, MsgBoxStyle.Critical, "Error de usuario")
            'Return False
        End If

    End Function
   
    
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In gcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.MemoEdit Then
                CType(ctrl, DevExpress.XtraEditors.MemoEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        For Each ctrl In pcTotales.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.MemoEdit Then
                CType(ctrl, DevExpress.XtraEditors.MemoEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teIVA.Properties.ReadOnly = True
        teRetencionPercepcion.Properties.ReadOnly = True
        teTotal.Properties.ReadOnly = True
        gv.OptionsBehavior.Editable = Tipo
        sbRevertirInventario.Enabled = Header.Reservado
    End Sub

    'Public Sub CargaControles(ByVal TipoAvance As Integer)
    'If TipoAvance = 0 Then
    'Else
    '    teCorrelativo.EditValue = bl.fac_ObtenerIdPedido(teCorrelativo.EditValue, TipoAvance)
    'End If

    'If teCorrelativo.EditValue = 0 Then
    '    Exit Sub
    'End If

    'Header = objTablas.fac_PedidosSelectByPK(teCorrelativo.EditValue)
    'With Header
    '    teCorrelativo.EditValue = .IdComprobante
    '    leTipoDoc.EditValue = .IdTipoComprobante
    '    teNumero.EditValue = .Numero
    '    deFecha.EditValue = .Fecha
    '    beCodigo.EditValue = .IdCliente
    '    teNombre.EditValue = .Nombre
    '    teNRC.EditValue = .Nrc
    '    teNit.EditValue = .Nit
    '    teGiro.EditValue = .Giro
    '    teDireccion.EditValue = .Direccion
    '    leVendedor.EditValue = .IdVendedor
    '    leTipoDoc.EditValue = .IdTipoComprobante
    '    leFormaPago.EditValue = .IdFormaPago
    '    seDiasCredito.EditValue = .DiasCredito
    '    leBodega.EditValue = .IdBodega
    '    teNotas.EditValue = .Notas
    '    teAnticipo.EditValue = .Anticipo
    '    teIVA.EditValue = .TotalIva
    '    teRetencionPercepcion.EditValue = .TotalImpuesto1
    '    teTotal.EditValue = .TotalComprobante
    '    ceRetencionPercepcion.EditValue = .AplicaRetencionPercepcion
    '    sePjeDescuento.EditValue = .PorcDescuento
    '    ceMarcarDevolucion.EditValue = .Devolucion
    '    leTipoVenta.EditValue = .TipoVenta
    '    gc.DataSource = bl.fac_ObtenerPedidoDetalle(.IdComprobante, pnIVA, False, 1)
    'End With
    'leVendedor.Enabled = True
    'bl.fac_TipoAplicacionComprobante(leTipoDoc.EditValue, DocumentoDetallaIva, TipoAplicacion, LimiteLineas)
    'CalcularTotalesGenerales()
    'End Sub

    'Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    If deFecha.Properties.ReadOnly = False Then
    '        Exit Sub
    '    End If

    '    Dim dt As DataTable = bl.fac_ObtenerPedido(teCorrelativo.EditValue)

    '    Dim entCliente As fac_Clientes = objTablas.fac_ClientesSelectByPK(beCodigo.EditValue)
    '    Dim entSucursal As adm_Sucursales = objTablas.adm_SucursalesSelectByPK(dtParametros.Rows(0).Item("IdSucursal"))

    '    Dim Template = Application.StartupPath & "\Plantillas\ctNexus.repx"
    '    If Not FileIO.FileSystem.FileExists(Template) Then
    '        MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
    '        Exit Sub
    '    End If

    '    Dim rpt As New fac_rptCotizacion() With {.DataSource = dt, .DataMember = ""}
    '    rpt.LoadLayout(Template)

    '    rpt.xrlEmpresa.Text = gsNombre_Empresa
    '    rpt.xrlCodigo.Text = beCodigo.EditValue
    '    rpt.xrlNombre.Text = teNombre.EditValue
    '    rpt.xrlAtencionA.Text = teNombre.EditValue
    '    rpt.xrlSucursal.Text = entSucursal.Nombre
    '    rpt.xrlFecha.Text = deFecha.EditValue
    '    rpt.xrlNit.Text = teNit.EditValue
    '    rpt.xrlNrc.Text = teNRC.EditValue
    '    rpt.xrlForma.Text = leFormaPago.Text
    '    rpt.xrlDireccion.Text = teDireccion.EditValue
    '    rpt.xrlDias.Text = 0
    '    rpt.xrlTiempo.Text = ""
    '    rpt.xrlVendedor.Text = leVendedor.Text
    '    rpt.xrlGarantia.Text = ""
    '    rpt.xrlConcepto.Text = teNotas.EditValue

    '    Dim Iva As Decimal = 0.0

    '    If entCliente.IdTipoImpuesto = 1 Then
    '        Iva = Round(gcPrecioTotal.SummaryItem.SummaryValue * pnIVA, 2)
    '    Else
    '        Iva = 0.0
    '    End If

    '    rpt.xrlIva.Text = Iva
    '    rpt.xrlRetencion.Text = Round(gcPrecioTotal.SummaryItem.SummaryValue + Iva, 2)

    '    If dtParametros.Rows(0).Item("RutaLogo") <> "" Then
    '        rpt.xrLogo.ImageUrl = dtParametros.Rows(0).Item("RutaLogo")
    '    End If

    '    rpt.ShowPreviewDialog()
    'End Sub

    'Private Sub leTipoDoc_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leTipoDoc.EditValueChanged
    '    bl.fac_TipoAplicacionComprobante(leTipoDoc.EditValue, DocumentoDetallaIva, TipoAplicacion, LimiteLineas)

    '    entComprobante = objTablas.adm_TiposComprobanteSelectByPK(leTipoDoc.EditValue)
    '    If leTipoDoc.EditValue <> 6 And leTipoDoc.EditValue <> 24 And leTipoDoc.EditValue <> 7 Then
    '        ceMarcarDevolucion.EditValue = False
    '    End If

    '    If leTipoDoc.EditValue = 7 Then
    '        leTipoVenta.EditValue = 3
    '    Else
    '        leTipoVenta.EditValue = 1
    '    End If

    '    For i = 0 To gv.DataRowCount - 1
    '        entProducto = objTablas.inv_ProductosSelectByPK(gv.GetRowCellValue(i, "IdProducto"))
    '        ''''''' ACTUALIZO LOS PRECIOS DE PRODUCTOS, PORQUE HAY CASOS QUE NO DAN ENTER
    '        ''''''' Y NO SE REFRESCAN PRECIOS, DESCRIPCIONES ETC. solo lo hara cuando no tenga acceso a modificar
    '        ''''''' descripciones y que no sea una cotizacion la que este llamando

    '        If EntUsuario.CambiarPrecios = False And entProducto.TipoProducto = 1 Then
    '            Dim Precio As Decimal = 0.0
    '            Precio = blInve.inv_ObtienePrecioProducto(gv.GetRowCellValue(i, "IdProducto"), gv.GetRowCellValue(i, "IdPrecio"))
    '            If leTipoVenta.EditValue <> 1 Or DocumentoDetallaIva Then
    '                Precio = Round(Precio / (pnIVA + 1), 4)
    '            End If

    '            gv.SetRowCellValue(i, "PrecioVenta", Precio)
    '            Precio = gv.GetRowCellValue(i, "PrecioVenta") * gv.GetRowCellValue(i, "PorcDescuento") / 100
    '            Precio = gv.GetRowCellValue(i, "PrecioVenta") - Precio
    '            gv.SetRowCellValue(i, "PrecioUnitario", Precio)
    '            Precio = Decimal.Round(gv.GetRowCellValue(i, "Cantidad") * Precio, 2)
    '            gv.SetRowCellValue(i, "PrecioTotal", Precio)

    '            If DocumentoDetallaIva Then 'crédito fiscal, nota de crédito, nota de débito
    '                gv.SetRowCellValue(i, "VentaNeta", Precio)
    '                gv.SetRowCellValue(i, "ValorIva", Decimal.Round(Precio * pnIVA, 2))
    '            Else 'ticket, consumidor final, factura de exportación
    '                If leTipoDoc.EditValue = 7 Then
    '                    gv.SetRowCellValue(i, "VentaNeta", gv.GetRowCellValue(i, gv.Columns("PrecioTotal")))
    '                    gv.SetRowCellValue(i, "ValorIva", 0.0)
    '                Else
    '                    Precio = Decimal.Round(Precio / (pnIVA + 1), 2) ' tenia 4
    '                    gv.SetRowCellValue(i, "VentaNeta", Precio)
    '                    gv.SetRowCellValue(i, "ValorIva", gv.GetRowCellValue(i, gv.Columns("PrecioTotal")) - Precio)
    '                End If

    '            End If
    '            CalcularTotalesGenerales()
    '        End If
    '    Next
    'End Sub

    Private Sub leFormaPago_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leFormaPago.EditValueChanged
        seDiasCredito.EditValue = 0
    End Sub

    Private Sub ceMarcarDevolucion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ceMarcarDevolucion.CheckedChanged
        If leTipoDoc.EditValue <> 6 And leTipoDoc.EditValue <> 24 And leTipoDoc.EditValue <> 7 Then
            ceMarcarDevolucion.EditValue = False
        End If
    End Sub

    Private Sub sbObtieneCotizacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbObtieneCotizacion.Click
        If deFecha.Properties.ReadOnly = True Then
            Exit Sub
        End If

        IdCotizacion = objConsultas.ConsultaCotizaciones(frmConsultaDetalle, piIdVendedor)
        Dim entCotiza As fac_Cotizaciones = objTablas.fac_CotizacionesSelectByPK(IdCotizacion)

        beCodigo.EditValue = entCotiza.IdCliente
        teNombre.EditValue = entCotiza.Nombre
        teNRC.EditValue = entCotiza.Nrc
        teNit.EditValue = entCotiza.Nit
        teGiro.EditValue = ""
        teDireccion.EditValue = entCotiza.Direccion
        leFormaPago.EditValue = entCotiza.IdFormaPago
        seDiasCredito.EditValue = entCotiza.DiasCredito
        teNotas.EditValue = entCotiza.Concepto

        beIdCliente_Validated(beCodigo, New System.EventArgs)

        leTipoDoc.EditValue = IIf(entCotiza.IdTipoComprobante = 0 Or entCotiza.IdTipoComprobante = Nothing, 6, entCotiza.IdTipoComprobante)
        leTipoVenta.EditValue = IIf(entCotiza.TipoVenta = 0 Or entCotiza.TipoVenta = Nothing, 1, entCotiza.TipoVenta)
        ceRetencionPercepcion.EditValue = IIf(entCotiza.AplicaRetencionPercepcion = 0 Or entCotiza.AplicaRetencionPercepcion = Nothing, False, entCotiza.AplicaRetencionPercepcion)

        If leTipoVenta.EditValue = 3 Then
            leTipoDoc.EditValue = 7 ' fact. exportacion 
        End If

        gc.DataSource = bl.fac_ObtenerCotizacionPedido(IdCotizacion, leTipoDoc.EditValue)
        CalcularTotalesGenerales()
        OtroModulo = ""
    End Sub

    Private Sub sbBuscaClientePedido_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbBuscaClientePedido.Click
        'SE COMENTARIO PORQUE NECESITAN CAMBIAR CLIENTES, CON EL CAMBIO DE RECALCULO DEL COMPROBANTE SE PODRA
        'If TotalGral <> 0.0 Then
        '    Exit Sub
        'End If

        'If entCliente.CreadoPor <> "clic" And beCodigo.EditValue = "" Then
        '    Exit Sub
        'End If
        'entCliente.IdCliente = beCodigo.EditValue

        If deFecha.Properties.ReadOnly = True Then
            Exit Sub
        End If

        Dim entPedBusca As fac_Pedidos = objConsultas.cnsClientesPedidos(fac_frmConsultaClientesPorPedidos, 0)
        With entPedBusca
            OtroModulo = "Clientes Pedidos"
            beCodigo.EditValue = .IdCliente
            teNombre.EditValue = .Nombre
            teDireccion.EditValue = .Direccion
            teNit.EditValue = .Nit
            teNRC.EditValue = .Nrc
            teGiro.EditValue = .Giro
            leTipoDoc.EditValue = IIf(.IdTipoComprobante = 0 Or .IdTipoComprobante = Nothing, 6, .IdTipoComprobante)
            ceRetencionPercepcion.EditValue = .AplicaRetencionPercepcion
            sePjeDescuento.EditValue = .PorcDescuento
            seDiasCredito.EditValue = .DiasCredito
            leTipoVenta.EditValue = IIf(.TipoVenta = 0 Or .TipoVenta = Nothing, 1, .TipoVenta)
        End With


    End Sub

    'Private Sub riCodProd_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles riCodProd.KeyDown

    '    'If gv.FocusedColumn.FieldName = "IdProducto" And e.KeyCode = Keys.F4 And gsNombre_Empresa.StartsWith("ELECTRO") Then
    '    '    If SiEsNulo(gv.EditingValue, "") = "" Then
    '    '        Dim IdProduc = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
    '    '        gv.EditingValue = IdProduc
    '    '    End If
    '    'End If

    '    'If gv.FocusedColumn.FieldName = "IdProducto" And e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab OrElse e.KeyCode = Keys.Down OrElse e.KeyCode = Keys.Right OrElse e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.Left And gsNombre_Empresa.StartsWith("ELECTRO") Then
    '    '    If leTipoVenta.EditValue = 0 Or leTipoVenta.EditValue = Nothing Then
    '    '        MsgBox("Tiene que definir el tipo de impuesto del pedido, se colocara automaticamente gravado", MsgBoxStyle.Critical, "Error")
    '    '        leTipoVenta.EditValue = 1
    '    '    End If

    '    '    '''''' valido los precios, por si cambian un codigo, que se refresque el precio automaticamente
    '    '    If blInve.inv_VerificaPrecioUsuario(objMenu.User, gv.GetFocusedRowCellValue("IdPrecio")) Then
    '    '        MsgBox("Tipo de precio no definido o no autorizado para éste usuario", MsgBoxStyle.Critical, "Error")
    '    '        gv.SetFocusedRowCellValue("IdPrecio", 1)
    '    '    End If

    '    '    Precio = blInve.inv_ObtienePrecioProducto(SiEsNulo(gv.EditingValue, ""), gv.GetFocusedRowCellValue("IdPrecio"))
    '    '    If leTipoVenta.EditValue <> 1 Or DocumentoDetallaIva Then
    '    '        Precio = Round(Precio / (pnIVA + 1), 4)
    '    '    End If

    '    '    gv.SetFocusedRowCellValue("PrecioVenta", Precio)
    '    '    Precio = gv.GetFocusedRowCellValue("PrecioVenta") * gv.GetFocusedRowCellValue("PorcDescuento") / 100
    '    '    Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Precio
    '    '    gv.SetFocusedRowCellValue("PrecioUnitario", Precio)
    '    '    CalculaTotal(gv.GetFocusedRowCellValue("Cantidad"), Precio)
    '    '    CalcularTotalesGenerales()
    '    '    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


    '    '    entProducto = objTablas.inv_ProductosSelectByPK(SiEsNulo(gv.EditingValue, ""))
    '    '    entComprobante = objTablas.adm_TiposComprobanteSelectByPK(leTipoDoc.EditValue)
    '    '    TipoAplicacion = entComprobante.TipoAplicacion

    '    '    If entProducto.IdProducto = "" Then
    '    '        MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
    '    '        gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
    '    '    Else
    '    '        If dtParametros.Rows(0).Item("ValidarExistencias") = True Then

    '    '            Existencia = CDec(blInve.inv_ObtieneExistenciaProducto(gv.EditingValue, leBodega.EditValue).Rows(0).Item("SaldoExistencias"))
    '    '            'PORQUE SE VUELVE A INVOCAR LA CONSULTA ??
    '    '            entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)

    '    '            If Not entProducto.Compuesto Then
    '    '                If Existencia < gv.GetFocusedRowCellValue("Cantidad") And TipoAplicacion = 1 And entProducto.TipoProducto = 1 And Not ceMarcarDevolucion.EditValue Then
    '    '                    MsgBox("Código de producto no registrado o sin existencia suficiente", MsgBoxStyle.Critical, "Error")
    '    '                    gv.SetFocusedRowCellValue("Descripcion", ", PRODUCTO SIN SALDO --")
    '    '                Else
    '    '                    'Y ACÁ OTRA VEZ ??
    '    '                    entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
    '    '                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
    '    '                End If
    '    '            Else
    '    '                Dim dtExistenciaKit As DataTable
    '    '                dtExistenciaKit = blInve.inv_ObtieneExistenciaKit(gv.EditingValue, leBodega.EditValue, deFecha.EditValue)

    '    '                If SiEsNulo(dtExistenciaKit.Rows(0).Item("CantidadDisponible"), 0.0) < gv.GetFocusedRowCellValue("Cantidad") And TipoAplicacion = 1 And Not ceMarcarDevolucion.EditValue Then
    '    '                    MsgBox("Código de producto no registrado o sin existencia suficiente en la composición del KIT", MsgBoxStyle.Critical, "Error")
    '    '                    gv.SetFocusedRowCellValue("Descripcion", ", PRODUCTO SIN SALDO --")
    '    '                Else
    '    '                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
    '    '                End If
    '    '            End If
    '    '        Else
    '    '            gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
    '    '        End If

    '    '        'gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
    '    '        gcEx.DataSource = blInve.inv_ObtieneSaldosPorProducto(-1, gv.EditingValue)
    '    '        Dim ArchivoImagen As String = entProducto.ArchivoImagen
    '    '        If Not FileIO.FileSystem.FileExists(ArchivoImagen) Or ArchivoImagen = "" Then
    '    '            peFoto.Image = Nothing
    '    '        Else
    '    '            Dim bm As New Bitmap(ArchivoImagen)
    '    '            peFoto.Image = bm
    '    '        End If
    '    '    End If
    '    'End If
    'End Sub

    Private Sub fac_frmPedidos_Revertir() Handles Me.Revertir
        xtcPedidos.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
        beCodigo.Focus()
    End Sub

    Private Sub sbObtienePedido_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbObtienePedido.Click
        If deFecha.Properties.ReadOnly = True Then
            Exit Sub
        End If

        If beCodigo.EditValue = "" Then
            MsgBox("Debe seleccionar el cliente, para buscar el pedido", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If


        IdPedidoAnular = objConsultas.ConsultaPedidosClientes(frmConsultas, beCodigo.EditValue)
        Dim dtPedidoAnular As DataTable = bl.fac_ExtraePedidoAnular(IdPedidoAnular)

        With dtPedidoAnular
            'OtroModulo = "Clientes Pedidos"
            beCodigo.EditValue = .Rows(0).Item("IdCliente")
            teNombre.EditValue = .Rows(0).Item("Nombre")
            teDireccion.EditValue = .Rows(0).Item("Direccion")
            teNit.EditValue = .Rows(0).Item("Nit")
            teNRC.EditValue = .Rows(0).Item("Nrc")
            teGiro.EditValue = .Rows(0).Item("Giro")
            leVendedor.EditValue = .Rows(0).Item("IdVendedor")
            leVendedor.Enabled = False
            leTipoDoc.EditValue = IIf(.Rows(0).Item("IdTipoComprobante") = 5, 8, .Rows(0).Item("IdTipoComprobante")) ' si es ccf, sera nc, sino el tipo que trae

            If .Rows(0).Item("IdTipoComprobante") <> 5 Or .Rows(0).Item("IdTipoComprobante") <> 8 Then
                ceMarcarDevolucion.EditValue = True
            Else
                ceMarcarDevolucion.EditValue = False
            End If

            ceRetencionPercepcion.EditValue = .Rows(0).Item("AplicaRetencionPercepcion")
            sePjeDescuento.EditValue = 0.0
            seDiasCredito.EditValue = .Rows(0).Item("DiasCredito")
            leTipoVenta.EditValue = IIf(.Rows(0).Item("TipoVenta") = 0, 1, .Rows(0).Item("TipoVenta")) ' si el tipo de venta es cero, pone 1
        End With

        Dim entDocs As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(dtPedidoAnular.Rows(0).Item("IdTipoComprobante"))
        TipoAplicacion = entDocs.TipoAplicacion
        DocumentoDetallaIva = entDocs.DetallaIVA

        gc.DataSource = dtPedidoAnular
        CalcularTotalesGenerales()

    End Sub
    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        Header = objTablas.fac_PedidosSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        DbMode = DbModeType.update
        teNumero.Focus()
    End Sub
    Private Sub CargaPantalla()
        xtcPedidos.SelectedTabPage = xtpDatos

        With Header
            teCorrelativo.EditValue = .IdComprobante
            leTipoDoc.EditValue = .IdTipoComprobante
            teNumero.EditValue = .Numero
            deFecha.EditValue = .Fecha
            beCodigo.EditValue = .IdCliente
            teNombre.EditValue = .Nombre
            teNRC.EditValue = .Nrc
            teNit.EditValue = .Nit
            teGiro.EditValue = .Giro
            teDireccion.EditValue = .Direccion
            leVendedor.EditValue = .IdVendedor
            leTipoDoc.EditValue = .IdTipoComprobante
            leFormaPago.EditValue = .IdFormaPago
            seDiasCredito.EditValue = .DiasCredito
            leBodega.EditValue = .IdBodega
            teNotas.EditValue = .Notas
            teTelefonos.EditValue = .Telefono
            teAnticipo.EditValue = .Anticipo
            teIVA.EditValue = .TotalIva
            teRetencionPercepcion.EditValue = .TotalImpuesto1
            teTotal.EditValue = .TotalComprobante
            ceRetencionPercepcion.EditValue = .AplicaRetencionPercepcion
            sePjeDescuento.EditValue = .PorcDescuento
            ceMarcarDevolucion.EditValue = .Devolucion
            leTipoVenta.EditValue = .TipoVenta
            ceReservar.EditValue = .Reservado
            gc.DataSource = bl.fac_ObtenerPedidoDetalle(.IdComprobante, pnIVA, False, 1)
        End With
        leVendedor.Enabled = True
        bl.fac_TipoAplicacionComprobante(leTipoDoc.EditValue, DocumentoDetallaIva, TipoAplicacion, LimiteLineas)
        CalcularTotalesGenerales()
        sbRevertirInventario.Enabled = Header.Reservado
    End Sub

    Private Sub leTipoDoc_EditValueChanged(sender As Object, e As EventArgs) Handles leTipoDoc.EditValueChanged
        bl.fac_TipoAplicacionComprobante(leTipoDoc.EditValue, DocumentoDetallaIva, TipoAplicacion, LimiteLineas)

        If leTipoDoc.EditValue = 7 Then  'SE CONVIERTE A TASA CERO
            leTipoVenta.EditValue = 2
        End If
        If DocumentoDetallaIva Then
            lblNRC_DUI.Text = "NRC:"
        Else
            lblNRC_DUI.Text = "DUI:"
        End If
    End Sub
    Private Function PreciosVerificados() As Boolean

        Dim ListaDescto As New List(Of fac_PedidosDescuento)
        Dim AplicaDescto As Boolean = False

        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New fac_PedidosDescuento
            With entDetalle
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .Nombre = gv.GetRowCellValue(i, "Descripcion")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .PrecioVenta = gv.GetRowCellValue(i, "PrecioVenta")
                .TotalVenta = Decimal.Round(.Cantidad * gv.GetRowCellValue(i, "PrecioVenta"), 2)
                .PorcDescuento = gv.GetRowCellValue(i, "PorcDescuento")
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .TotalFacturado = gv.GetRowCellValue(i, "PrecioTotal")
                .Diferencia = .TotalVenta - .TotalFacturado
                'SE VERIFICA QUE EL PORCENTAJE DE DESCUENTO OTORGADO ESTÉ EN EL RANGO AUTORIZADO EN EL PERFIL DEL USUARIO
                'TAMBIÉN SE VERIFICA QUE NO DE PASO CON PRECIOS A CERO
                If .Diferencia > 0 And (.PorcDescuento < EntUsuario.DesdeDescuentos Or .PorcDescuento > EntUsuario.HastaDescuentos) Then
                    ListaDescto.Add(entDetalle)
                    AplicaDescto = True
                Else
                    If .TotalFacturado <= 0 Then 'SE HA COLOCADO ESTA VALIDACION, YA QUE SI POR ERROR ESTÁ FACTURANDO CON PRECIO 0
                        ListaDescto.Add(entDetalle)
                        AplicaDescto = True
                    End If
                End If
                '    If .PorcDescuento < EntUsuario.DesdeDescuentos Or .PorcDescuento > EntUsuario.HastaDescuentos Then
                '        frmObtienePassword.Text = "AUTORIZACIÓN DE DESCUENTOS"
                '        frmObtienePassword.Usuario = objMenu.User
                '        frmObtienePassword.TipoAcceso = 3
                '        frmObtienePassword.ShowDialog()

                '        If frmObtienePassword.Acceso = False Then
                '            MsgBox("Usuario no autorizado para aplicar descuentos", MsgBoxStyle.Critical, "Nota")
                '            gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", "")
                '            E.Value = 0
                '            'AutorizoDescto = ""
                '        Else
                '            Dim entUsuarioAutoriza As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(SiEsNulo(frmObtienePassword.teUserName.EditValue, ""))
                '            If E.Value > entUsuarioAutoriza.HastaDescuentos Then
                '                MsgBox("El Usuario no puede autorizar descuento mayor al establecido ", MsgBoxStyle.Critical, "Nota")
                '                gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", "")
                '                E.Value = 0
                '                'AutorizoDescto = ""
                '            End If
                '            If E.Value < entUsuarioAutoriza.DesdeDescuentos And E.Value > 0.0 Then
                '                MsgBox("El Usuario no puede autorizar descuento menor al establecido ", MsgBoxStyle.Critical, "Nota")
                '                gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", "")
                '                E.Value = 0
                '            End If
                '            'AutorizoDescto = frmObtienePassword.Usuario
                '        End If
                '        'gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", AutorizoDescto)
                '        frmObtienePassword.Dispose()
                '    Else
                '        'gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", objMenu.User)
                '    End If
                'End If
 
            End With
        Next

        If AplicaDescto Then
            fac_frmProductosConDescuento.gcProdDesc.DataSource = ListaDescto
            fac_frmProductosConDescuento.IdClientePed = beCodigo.EditValue
            fac_frmProductosConDescuento.NombreCliente = teNombre.EditValue
            fac_frmProductosConDescuento.ShowDialog()
            If Not fac_frmProductosConDescuento.Aceptado Then
                Return False
            End If
            fac_frmProductosConDescuento.Dispose()
        End If

        'If leFormaPago.EditValue > 1 Then
        '    fac_frmAutorizaCrearCredito.tePassWord.EditValue = ""
        '    fac_frmAutorizaCrearCredito.teUsuario.EditValue = ""
        '    fac_frmAutorizaCrearCredito.Text = "Autorización de Creación/Modificación Clientes de Crédito"
        '    fac_frmAutorizaCrearCredito.ShowDialog()

        '    If Not fac_frmAutorizaCrearCredito.Aceptado Then
        '        Return False
        '    End If

        'End If

        Return True
    End Function

    Public Property AutorizadoPor() As String
        Get
            Return _AutorizadoPor
        End Get
        Set(ByVal value As String)
            _AutorizadoPor = value
        End Set
    End Property

    Private Sub ceRetencionPercepcion_CheckedChanged(sender As Object, e As EventArgs) Handles ceRetencionPercepcion.CheckedChanged
        CalcularTotalesGenerales()
    End Sub

    Private Sub sbRevertirInventario_Click(sender As Object, e As EventArgs) Handles sbRevertirInventario.Click

        dtPermisos = objMenu.ObtenerPermisosForma(objMenu.User, Me.Modulo, "002001")
        If dtPermisos.Rows.Count > 0 Then
            If Not dtPermisos.Rows(0).Item("snDelete") Then
                MsgBox("Usuario sin autorización para este proceso", MsgBoxStyle.Information, "NOTA")
                Exit Sub
            End If
        Else
                MsgBox("Usuario sin autorización para este proceso", MsgBoxStyle.Information, "NOTA")
            Exit Sub
        End If
        If Not Header.Reservado Then
            MsgBox("No puede revertir del pedido, no esta aplicado aún.", MsgBoxStyle.Exclamation, "NOTA")
            Exit Sub
        End If

        If Not teNombre.Properties.ReadOnly Then
            MsgBox("Debe guardar el pedido para revertir", MsgBoxStyle.Exclamation, "NOTA")
            Exit Sub
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            MsgBox("La fecha del documento debe ser de un período válido", 16, "Error de datos")
            Exit Sub
        End If

        If MsgBox("¿Esta seguro de revertir del inventario el pedido?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "CONFIRME") = MsgBoxResult.No Then
            Exit Sub
        End If


        Dim msj As String = ""
        msj = bl.fac_EliminaKardexPedido(Header.IdComprobante)

        If msj = "Ok" Then
            MsgBox("EL pedido fue eliminado exitosamente", MsgBoxStyle.Information, "Nota")
            sbRevertirInventario.Enabled = False
            Exit Sub
        Else
            MsgBox("No se pudo eliminar el pedido del kardex: " & msj, MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If

    End Sub

    Private Sub SbCargarHoras_Click(sender As Object, e As EventArgs) Handles SbCargarHoras.Click
        If deFecha.Properties.ReadOnly Then
            Exit Sub
        End If
        If beCodigo.EditValue = "" Then
            MsgBox("Debe de Seleccionar el cliente ", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        Dim frmdocV As New fac_frmPideFechas
        Me.AddOwnedForm(frmdocV)
        frmdocV.ShowDialog()
        Dim dt As DataTable
        dt = bl.fac_ObtenerHorasClientes(frmdocV.deDesde.EditValue, frmdocV.deHasta.EditValue, beCodigo.EditValue)
        gc.DataSource = dt
    End Sub
End Class
