﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns
Imports System.IO

Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraEditors.ViewInfo

Public Class fac_frmFacturacion
    Dim bl As New FacturaBLL(g_ConnectionString), fd As New FuncionesBLL(g_ConnectionString)
    Dim blInve As New InventarioBLL(g_ConnectionString), blCxc As New CuentasPCBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim FacturaDetallePago As List(Of fac_VentasDetallePago)
    Dim FacturaHeader As fac_Ventas, FacturaDetalle As List(Of fac_VentasDetalle), entComprobantes As adm_TiposComprobante, Sucursales As adm_Sucursales
    Dim Precio As Decimal = 0.0, ultPrecio As Decimal = 0.0, UsuarioDioClic As Boolean, DatosEncabezado As String, DatosPie As String, Existencia As Decimal
    Dim TipoVenta As Integer = 1, DocumentoDetallaIva As Boolean, TipoAplicacion As Integer = 1, TipoPrecio As Integer = 1, LimiteLineas = 0
    Dim entProducto As inv_Productos, dtParametros As DataTable = blAdmon.ObtieneParametros(), entidadTicket As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(10), EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim IdSucUser As Integer = SiEsNulo(EntUsuario.IdSucursal, 1), CambiarPreciosUser As Integer = SiEsNulo(EntUsuario.CambiarPrecios, 0), IdTipoPrecioUser As Integer = SiEsNulo(EntUsuario.IdTipoPrecio, 0), TipoPrecioConfiguracion As Integer = 1
    Dim EsCompuesto As Boolean = False, TipoFacturacion As Integer = 0, NumCuotass As Integer = 0, AutorizoDescto As String = "", Serie As String
    Dim _dtDetallePago As DataTable, CescEditado As Boolean, PrecioCosto As Decimal = 0.0, TipoProducto As Integer = 1
    Dim AsignarNumeroAutomatico As Boolean = True

    Property dtDetallePago() As DataTable
        Get
            Return _dtDetallePago
        End Get
        Set(ByVal value As DataTable)
            _dtDetallePago = value
        End Set
    End Property

    Private Sub fac_frmFactura_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        CargaListas()
        leBodega.EditValue = piIdBodega
        Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

        ' La entidad de la sucursal nunca cambiara desde el momente que se apertura la ventana de la venta para cargarlo en cada imprecion , cuando solo se puede cargar una vez
        Sucursales = objTablas.adm_SucursalesSelectByPK(piIdSucursalUsuario)
        LimpiaLaPantalla()
        If Not objMenu.User.StartsWith("DIGITADOR") Then
            If EntUsuario.IdPunto <> -1 Then
                teNumero.Properties.ReadOnly = True
                teNumero.Properties.AllowFocused = False
                teNumeroUnico.Properties.ReadOnly = True
                teNumeroUnico.Properties.AllowFocused = False
                AsignarNumeroAutomatico = True
            Else
                cboPuntoVenta.ReadOnly = False
                cboPuntoVenta.Enabled = True
                AsignarNumeroAutomatico = False
            End If
        Else
            AsignarNumeroAutomatico = False
        End If
        If dtParametros.Rows(0).Item("BloquearFechaFacturar") Then
            deFecha.Enabled = False
        End If

        Me.IdPrecio.OptionsColumn.ReadOnly = CambiarPreciosUser = 0
        'Me.PrecioVenta.OptionsColumn.ReadOnly = CambiarPreciosUser = 0
        'Me.PrecioVenta.OptionsColumn.AllowEdit = CambiarPreciosUser <> 0
        'Me.PrecioVenta.OptionsColumn.AllowFocus = CambiarPreciosUser <> 0
        Me.PrecioUnitario.OptionsColumn.ReadOnly = CambiarPreciosUser = 0
        Me.PrecioUnitario.OptionsColumn.AllowEdit = CambiarPreciosUser <> 0
        Me.PrecioUnitario.OptionsColumn.AllowFocus = CambiarPreciosUser <> 0

        ' para el caso de los clientes que no tenia este cambio, estara nulo este campo
        Me.Descripcion.OptionsColumn.ReadOnly = Not SiEsNulo(EntUsuario.AccesoProductos, True)
        Me.Descripcion.OptionsColumn.AllowEdit = SiEsNulo(EntUsuario.AccesoProductos, True)
        Me.Descripcion.OptionsColumn.AllowFocus = SiEsNulo(EntUsuario.AccesoProductos, True)
        leBodega.Enabled = Not EntUsuario.BloquearBodega

        cboPuntoVenta.EditValue = EntUsuario.IdPunto
        deFecha.EditValue = SiEsNulo(objFunciones.GetFechaContableCaja(piIdSucursalUsuario, cboPuntoVenta.EditValue), Today)
        If IdTipoPrecioUser = 0 Then
            TipoPrecio = dtParametros.Rows(0).Item("IdTipoPrecio")
        Else
            TipoPrecio = IdTipoPrecioUser
        End If

        TipoPrecioConfiguracion = dtParametros.Rows(0).Item("TipoPrecio")

        If dtParametros.Rows(0).Item("TipoContribuyente") = 3 And dtParametros.Rows(0).Item("EsRetenedor") = True Then
            ceRetencionPercepcion.Text = "APLICAR PERCEPCIÓN DEL 1 %"
            ceRetencionPercepcion.EditValue = True
            lblPercReten.Text = "(+) IVA Percibido:"
        End If
        'If EntUsuario.CambiarPrecios = True Then
        '    deFecha.Properties.ReadOnly = False
        'Else
        '    deFecha.Properties.ReadOnly = True
        'End If

        TipoVenta = leTipoImpuesto.EditValue
        bl.fac_TipoAplicacionComprobante(leTipoComprobante.EditValue, DocumentoDetallaIva, TipoAplicacion, LimiteLineas)
        bl.fac_ObtenerDatosEncabezadoPieTicket(DatosEncabezado, DatosPie, cboPuntoVenta.EditValue)
        teLimiteLineas.EditValue = LimiteLineas

        Dim Template = Application.StartupPath & "\Copa.Ini"

        If FileIO.FileSystem.FileExists(Template) Then
            sbImportar.Visible = True
            sbMarcarBoletos.Visible = True
        Else
            sbImportar.Visible = False
            sbMarcarBoletos.Visible = False
        End If
   
    End Sub

    Private Sub LimpiaLaPantalla()
        beCodCliente.EditValue = ""
        teNombre.EditValue = ""
        teNRC.EditValue = ""
        teNit.EditValue = ""
        teGiro.EditValue = ""
        teDireccion.EditValue = ""
        teVentaCuentaDe.EditValue = ""
        leFormaPago.EditValue = 2
        seDiasCredito.EditValue = 0
        leTipoImpuesto.EditValue = 1
        sePjeDescuento.EditValue = 0.0
        teDescuento.EditValue = 0.0
        teIVA.EditValue = 0.0
        teRetencionPercepcion.EditValue = 0.0
        teTotal.EditValue = 0.0
        TipoFacturacion = 0
        teNumPedido.EditValue = ""
        teNotaRemision.EditValue = ""
        AutorizoDescto = ""
        TipoFacturacion = 1
        teLineas.EditValue = 0
        FacturaHeader = New fac_Ventas
        FacturaDetalle = New List(Of fac_VentasDetalle)
        leTipoComprobante.Focus()
        gv.CancelUpdateCurrentRow()
        sbImportarBoletos.DataSource = bl.fac_ObtenerDetalleDocumento("fac_VentasDetalle", -1)
        leTipoComprobante.Properties.ReadOnly = False
        beCodCliente.Properties.ReadOnly = False
        meComentario.EditValue = ""
        If dtParametros.Rows(0).Item("BloquearFechaFacturar") Then
            deFecha.EditValue = SiEsNulo(objFunciones.GetFechaContableCaja(piIdSucursalUsuario, cboPuntoVenta.EditValue), Today)
        Else
            deFecha.EditValue = Today
        End If
        'deFecha.EditValue = IIf(SiEsNulo(dtParametros.Rows(0).Item("BloquearFechaFacturar"), False), SiEsNulo(objFunciones.GetFechaContable(piIdSucursalUsuario), Nothing), Today)
        Serie = ""
        leBodega.EditValue = SiEsNulo(EntUsuario.IdBodega, 0)
        teTelefonos.EditValue = ""
        teCESC.EditValue = 0.0
        CescEditado = False
        'If beCodCliente.EditValue = "1" Then
        '    teNombre.Properties.ReadOnly = True
        'Else
        '    teNombre.Properties.ReadOnly = False
        'End If
    End Sub

    Private Sub CargaListas()
        With objCombos

            If IdSucUser = 0 Then
                .fac_PuntosVenta(cboPuntoVenta, piIdSucursal)
            Else
                .fac_PuntosVenta(cboPuntoVenta, IdSucUser)
            End If

            .fac_TiposComprobante(leTipoComprobante)
            .fac_TiposImpuesto(leTipoImpuesto)
            .admDepartamentos(leDepartamento)
            .fac_FormasPago(leFormaPago)
            .fac_VendedoresFacturacion(leVendedor, EntUsuario.IdSucursal)
            .adm_SucursalesFac(leSucursalEnvio, "")
            .inv_Bodegas(leBodega)
            .con_CentrosCosto(riteCentroCosto, "", "")
        End With
    End Sub
    Private Sub cboDepartamento_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leDepartamento.EditValueChanged
        objCombos.admMunicipios(leMunicipio, leDepartamento.EditValue)
    End Sub
    Private Sub btGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btGuardar.Click
        If Not DatosValidos() Then
            Exit Sub
        End If
        CargaEntidades()
        Dim entForma As fac_FormasPago = objTablas.fac_FormasPagoSelectByPK(leFormaPago.EditValue)
        Dim Imprimir As Boolean = False
        If entForma.DiasCredito = 0 And SiEsNulo(dtParametros.Rows(0).Item("SeccionarFormaPago"), False) Then
            _dtDetallePago = bl.fac_ObtenerEstructuraPago()
            Dim frmDetallePago As New fac_frmMostrarFormasPago
            Me.AddOwnedForm(frmDetallePago)
            frmDetallePago.teTotalFactura.EditValue = teTotal.EditValue
            frmDetallePago.IdFormaPago = leFormaPago.EditValue
            frmDetallePago.TotalComprob = teTotal.EditValue
            frmDetallePago.ShowDialog()
            'Retorno de las formas de pago cancelando el cierre de la venta
            If frmDetallePago.Tipo = 0 Then
                Exit Sub
            End If
            FacturaHeader.TotalPagado = SiEsNulo(frmDetallePago.gcTotal.SummaryItem.SummaryValue, 0)
            Imprimir = frmDetallePago.Imprimir
            LlenarFormaPago()
        End If

        If entForma.DiasCredito > 0 And SiEsNulo(dtParametros.Rows(0).Item("SeccionarFormaPago"), False) Then
            _dtDetallePago = bl.fac_ObtenerEstructuraPago()
            Dim dr As DataRow = dtDetallePago.NewRow()
            dr("IdFormaPago") = leFormaPago.EditValue
            dr("Total") = 0.0 'teTotal.EditValue
            dr("IdComprobVenta") = 0
            dtDetallePago.Rows.Add(dr)
            Imprimir = True
            LlenarFormaPago()
        End If

        If Not SiEsNulo(dtParametros.Rows(0).Item("SeccionarFormaPago"), False) Then
            fac_frmMostrarCambio.teTotalFactura.EditValue = FacturaHeader.TotalComprobante
            fac_frmMostrarCambio.teEfectivo.EditValue = FacturaHeader.TotalComprobante
            fac_frmMostrarCambio.teEfectivo.Focus()
            fac_frmMostrarCambio.ShowDialog()
            FacturaHeader.TotalPagado = fac_frmMostrarCambio.teEfectivo.EditValue
            Imprimir = fac_frmMostrarCambio.Imprimir
            _dtDetallePago = bl.fac_ObtenerEstructuraPago()
            Dim dr As DataRow = dtDetallePago.NewRow()
            dr("IdFormaPago") = leFormaPago.EditValue
            dr("Total") = 0.0
            dr("IdComprobVenta") = 0
            dtDetallePago.Rows.Add(dr)
            LlenarFormaPago()
        End If

        Dim msj As String = bl.fac_InsertaFactura(FacturaHeader, FacturaDetalle, FacturaDetallePago, AsignarNumeroAutomatico)
        If msj = "" Then
            teNumero.EditValue = FacturaHeader.Numero
            If gsNombre_Empresa = "JURO, S. A. DE C. V." Then
                If FacturaHeader.IdFormaPago = 2 Then
                    MsgBox("DEBE DE AUTORIZAR LA VENTA PARA SU IMPRECION", MsgBoxStyle.Exclamation, "Nota")
                    Imprimir = False
                End If
            End If
            If Imprimir Then
                Try
                    ImprimeFactura()

                Catch ex As Exception
                    MsgBox("Hubo algún error al imprimir el documento" + Chr(13) + ex.Message, MsgBoxStyle.Critical)
                End Try
            End If

            If Not SiEsNulo(dtParametros.Rows(0).Item("SeccionarFormaPago"), False) Then
                fac_frmMostrarCambio.Dispose()
            End If
        Else
            MsgBox("NO SE PUDO CREAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical, "Error de BD")
            Exit Sub
        End If

        Try
            If TipoAplicacion <> 1 And leFormaPago.EditValue = 2 Then  'verificar la nota de crédito

                Dim frmNc As New fac_frmAplicacionNotaCredito
                frmNc.IdComprob = FacturaHeader.IdComprobante
                frmNc.NumeroNota = teNumero.EditValue
                frmNc.FechaNota = deFecha.EditValue
                frmNc.IdCliente = beCodCliente.EditValue
                frmNc.Nombre = teNombre.EditValue
                frmNc.MontoAbonado = teTotal.EditValue
                frmNc.ShowDialog()
            End If
        Catch ex As Exception

        End Try

        ceDevolucion.EditValue = False
        beCodCliente.Focus()
        LimpiaLaPantalla()
        NumeroComprobante()


    End Sub
    Private Sub CargaEntidades()

        If ceDevolucion.EditValue = True Then
            TipoAplicacion = -1
        End If

        With FacturaHeader

            If IdSucUser = 0 Then
                .IdSucursal = piIdSucursal
            Else
                .IdSucursal = IdSucUser
            End If

            .Numero = teNumero.EditValue
            .NumFormularioUnico = SiEsNulo(teNumeroUnico.EditValue, "")
            .Serie = Serie
            .IdPunto = cboPuntoVenta.EditValue
            .IdTipoComprobante = leTipoComprobante.EditValue
            .Fecha = deFecha.EditValue
            .IdCliente = beCodCliente.EditValue
            .Nombre = teNombre.EditValue
            .Nrc = teNRC.EditValue
            .Nit = teNit.EditValue
            .Giro = teGiro.EditValue
            .Direccion = teDireccion.EditValue
            .VentaAcuentaDe = teVentaCuentaDe.EditValue
            .IdMunicipio = leMunicipio.EditValue
            .IdDepartamento = leDepartamento.EditValue
            .IdFormaPago = leFormaPago.EditValue
            .DiasCredito = seDiasCredito.Value
            .IdVendedor = leVendedor.EditValue
            .TipoVenta = leTipoImpuesto.EditValue
            .PorcDescto = sePjeDescuento.EditValue
            .EsDevolucion = ceDevolucion.EditValue
            .Telefono = teNotaRemision.EditValue
            .TipoImpuesto = rgTipo.EditValue
            .Telefono = teTelefonos.EditValue

            If TipoAplicacion <> 1 Or ceDevolucion.EditValue = True Then  'cuando es nota de crédito, fac. de devolucion y nota de envio de devolucion
                .EsDevolucion = True
            End If

            .TotalDescto = teDescuento.EditValue * TipoAplicacion
            .TotalComprobante = teTotal.EditValue * TipoAplicacion
            .TotalIva = teIVA.EditValue * TipoAplicacion
            .TotalImpuesto1 = teRetencionPercepcion.EditValue * TipoAplicacion
            .TotalImpuesto2 = teCESC.EditValue
            .TotalNoSujeto = gcVentaNoSujeta.SummaryItem.SummaryValue * TipoAplicacion
            .TotalExento = gcVentaExenta.SummaryItem.SummaryValue * TipoAplicacion
            .TotalAfecto = gcVentaAfecta.SummaryItem.SummaryValue * TipoAplicacion
            .TotalNeto = gcVentaNeta.SummaryItem.SummaryValue * TipoAplicacion
            .TotalPagado = 0.0
            .SaldoActual = teTotal.EditValue * TipoAplicacion
            .IdSucursalEnvio = leSucursalEnvio.EditValue
            'TODO Falta llenar estos valores
            .IdComprobanteNota = 0
            .OrdenCompra = teNumPedido.EditValue
            .IdBodega = leBodega.EditValue
            .MotivoAnulacion = ""
            .AnuladoPor = ""
            .AutorizoDescuento = ""
            .Comentario = meComentario.EditValue
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .ModificadoPor = ""
            .FechaHoraModificacion = Nothing
        End With

        FacturaDetalle = New List(Of fac_VentasDetalle)

        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New fac_VentasDetalle
            With entDetalle
                .IdComprobante = FacturaHeader.IdComprobante
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .IdPrecio = gv.GetRowCellValue(i, "IdPrecio")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad") * TipoAplicacion
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .PrecioVenta = gv.GetRowCellValue(i, "PrecioVenta") * TipoAplicacion
                .PorcDescuento = gv.GetRowCellValue(i, "PorcDescuento")
                .ValorDescuento = gv.GetRowCellValue(i, "ValorDescuento") * TipoAplicacion
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario") * TipoAplicacion
                .VentaNoSujeta = gv.GetRowCellValue(i, "VentaNoSujeta") * TipoAplicacion
                .VentaExenta = gv.GetRowCellValue(i, "VentaExenta") * TipoAplicacion
                .VentaAfecta = gv.GetRowCellValue(i, "VentaAfecta") * TipoAplicacion
                .VentaNeta = gv.GetRowCellValue(i, "VentaNeta") * TipoAplicacion
                .TipoImpuesto = leTipoImpuesto.EditValue
                .ValorIVA = gv.GetRowCellValue(i, "ValorIVA") * TipoAplicacion
                .PrecioCosto = SiEsNulo(gv.GetRowCellValue(i, "PrecioCosto"), 0.0)
                .PorcComision = SiEsNulo(blInve.inv_ObtenerComisionProducto(.IdProducto), 0)
                .DescuentoAutorizadoPor = SiEsNulo(gv.GetRowCellValue(i, "DescuentoAutorizadoPor"), "")
                .IdCentro = SiEsNulo(gv.GetRowCellValue(i, "IdCentro"), "")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .IdDetalle = i + 1
            End With
            FacturaDetalle.Add(entDetalle)
        Next
    End Sub

#Region "Grid"

    Private Sub cmdUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteRow(gv.FocusedRowHandle)
        CalcularTotales()
    End Sub



    Private Sub gv_FocusedColumnChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedColumnChangedEventArgs) Handles gv.FocusedColumnChanged
        gv.ClearColumnErrors()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdPrecio", TipoPrecio)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdParticipante", 0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioVenta", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PorcDescuento", sePjeDescuento.Value)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescuento", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "VentaNoSujeta", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "VentaExenta", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "VentaAfecta", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "VentaNeta", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorIVA", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "DescuentoAutorizadoPor", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "TipoProducto", 1)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdComprobante", 1)
    End Sub

    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Select Case gv.FocusedColumn.FieldName
            Case "IdPrecio"
                entProducto = objTablas.inv_ProductosSelectByPK(gv.GetFocusedRowCellValue("IdProducto"))
                Dim ValorDescuento As Decimal = blInve.inv_ObtieneDescuentoMargen(gv.GetFocusedRowCellValue("Cantidad"))

                'VALIDO SI LOS PRECIOS SON POR PRODUCTO Ó POR LISTA DE PRECIOS DE CLIENTES
                If TipoPrecioConfiguracion <> -999 Then '---> 2
                    If chkMargen.EditValue = True And entProducto.IdSubGrupo = 1 Then
                        Precio = SiEsNulo(bl.fac_ObtienePrecioCliente(gv.GetFocusedRowCellValue("IdProducto"), beCodCliente.EditValue), 0.0) - ValorDescuento
                    Else
                        Precio = SiEsNulo(bl.fac_ObtienePrecioCliente(gv.GetFocusedRowCellValue("IdProducto"), beCodCliente.EditValue), 0.0)
                    End If
                End If

                If Precio = 0 Then
                    If chkMargen.EditValue = True And entProducto.IdSubGrupo = 1 Then
                        Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), e.Value) - ValorDescuento
                    Else
                        Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), e.Value)
                    End If
                End If

                If entProducto.EsExento = False And (DocumentoDetallaIva Or leTipoImpuesto.EditValue > 1) Then 'SE EXTRAE EL IVA CUANDO EL PRODUCTO ESTÁ GRAVADO
                    Precio = Decimal.Round(Precio / (pnIVA + 1), 4)
                End If
                gv.SetFocusedRowCellValue("PrecioVenta", Precio)

                Dim Descto As Decimal = Decimal.Round(Precio * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)
                gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescuento", Decimal.Round((Descto * gv.GetFocusedRowCellValue("Cantidad")), 2))

                Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Descto

                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)

                'VALIDO QUE EL PRECIO DEL PRODUCTO NO SEA MENOR AL COSTO ////////////////////////////////////////////////////////////////////////////////////////////
                PrecioCosto = gv.GetFocusedRowCellValue("PrecioCosto")
                TipoProducto = gv.GetFocusedRowCellValue("TipoProducto")

                If Precio < PrecioCosto And TipoProducto = 1 And SiEsNulo(dtParametros.Rows(0).Item("FacturarMenosCosto"), False) = False Then
                    MsgBox("El precio del producto no puede ser menor al precio de costo", MsgBoxStyle.Critical, "Error")
                    e.Value = 1

                    Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), e.Value)
                    If entProducto.EsExento = False And (DocumentoDetallaIva Or leTipoImpuesto.EditValue > 1) Then 'SE EXTRAE EL IVA CUANDO EL PRODUCTO ESTÁ GRAVADO
                        Precio = Decimal.Round(Precio / (pnIVA + 1), 4)
                    End If

                    gv.SetFocusedRowCellValue("PrecioVenta", Precio)
                    Descto = Decimal.Round(Precio * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)
                    Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Descto
                    gv.SetFocusedRowCellValue("PrecioUnitario", Precio)
                End If
                '///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                CalculaTotalFila(gv.GetFocusedRowCellValue("Cantidad"), Precio)
            Case "PorcDescuento"
                If e.Value > 0.0 Then
                    ' si el usuario pone el % mayor o menor del establecido pide autorizacion
                    'If e.Value < EntUsuario.DesdeDescuentos Or e.Value > EntUsuario.HastaDescuentos Then
                    '    frmValidaToken.Text = "AUTORIZACIÓN DE DESCUENTOS"
                    '    frmValidaToken.Usuario = objMenu.User
                    '    frmValidaToken.Descuento = e.Value
                    '    frmValidaToken.IdProducto = SiEsNulo(gv.GetFocusedRowCellValue("IdProducto"), "")
                    '    frmValidaToken.Fecha = deFecha.EditValue
                    '    frmValidaToken.ShowDialog()
                    '    If frmValidaToken.Acceso = False Then
                    '        MsgBox("Token No Valido", MsgBoxStyle.Critical, "Nota")
                    '        gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", "")
                    '        e.Value = 0
                    '        AutorizoDescto = ""
                    '    Else
                    '        'Dim entUsuarioAutoriza As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(SiEsNulo(frmObtienePassword.teUserName.EditValue, ""))
                    '        'If e.Value > entUsuarioAutoriza.HastaDescuentos Then
                    '        '    MsgBox("El Usuario no puede autorizar descuento mayor al establecido ", MsgBoxStyle.Critical, "Nota")
                    '        '    gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", "")
                    '        '    e.Value = 0
                    '        '    AutorizoDescto = ""
                    '        'End If
                    '        'If e.Value < entUsuarioAutoriza.DesdeDescuentos And e.Value > 0.0 Then
                    '        '    MsgBox("El Usuario no puede autorizar descuento menor al establecido ", MsgBoxStyle.Critical, "Nota")
                    '        '    gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", "")
                    '        '    e.Value = 0
                    '        '    AutorizoDescto = ""
                    '        'End If
                    '        AutorizoDescto = frmValidaToken.teToken.EditValue
                    '    End If
                    '    gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", AutorizoDescto)
                    '    frmValidaToken.Dispose()
                    'Else
                    '    gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", objMenu.User)
                    'End If
                    e.Value = 0
                    AutorizoDescto = ""
                End If

                Dim Descto As Decimal = Decimal.Round(gv.GetFocusedRowCellValue("PrecioVenta") * e.Value / 100, 4)
                gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescuento", Decimal.Round((Descto * gv.GetFocusedRowCellValue("Cantidad")), 2))
                Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Descto
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)
                CalculaTotalFila(gv.GetFocusedRowCellValue("Cantidad"), Precio)
            Case "Cantidad"
                'SE VUELVE A LLAMAR LA ENTIDAD, NO ENTIENDO PORQUE ?? WTF, PORQUE NO DEJAR GUARDADO EL DATO DESDE QUE SE CAMBIA EN EL CODIGO DE PRODUCTO?
                entProducto = objTablas.inv_ProductosSelectByPK(gv.GetFocusedRowCellValue("IdProducto"))
                Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), gv.GetFocusedRowCellValue("IdPrecio"))

                'VALIDO SI LOS PRECIOS SON POR PRODUCTO Ó POR LISTA DE PRECIOS DE CLIENTES
                If TipoPrecioConfiguracion = 2 Then
                    Precio = SiEsNulo(bl.fac_ObtienePrecioCliente(gv.GetFocusedRowCellValue("IdProducto"), beCodCliente.EditValue), 0.0)
                End If

                If entProducto.EsExento = False And (DocumentoDetallaIva Or leTipoImpuesto.EditValue > 1) Then 'SE EXTRAE EL IVA CUANDO EL PRODUCTO ESTÁ GRAVADO
                    Precio = Decimal.Round(Precio / (pnIVA + 1), 4)
                End If
                gv.SetFocusedRowCellValue("PrecioVenta", Precio)

                Dim Descto As Decimal = Decimal.Round(Precio * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)
                gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescuento", Decimal.Round((Descto * e.Value), 2))
                Precio = Precio - Descto
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)
                CalculaTotalFila(e.Value, Precio)

                'ACÁ OTRA VEZ SE VUELVE A LLAMAR LA ENTIDAD DE PRODUCTOS?? WTF, desactivado para ver que pasa
                '              entProducto = objTablas.inv_ProductosSelectByPK(gv.GetFocusedRowCellValue("IdProducto"))

                If dtParametros.Rows(0).Item("ValidarExistencias") = True Then
                    If Not entProducto.Compuesto Then
                        If e.Value > teSaldo.EditValue And TipoAplicacion = 1 And entProducto.TipoProducto = 1 And ceDevolucion.EditValue = False Then
                            MsgBox("Esta cantidad sobregira el inventario. No podrá guardar la factura", MsgBoxStyle.Critical, "Error")
                        End If
                    Else
                        Dim dtExistenciaKit As DataTable = blInve.inv_ObtieneExistenciaKit(entProducto.IdProducto, leBodega.EditValue, deFecha.EditValue)
                        If SiEsNulo(dtExistenciaKit.Rows(0).Item("CantidadDisponible"), 0.0) < e.Value And TipoAplicacion = 1 And Not ceDevolucion.EditValue Then
                            MsgBox("Esta cantidad del KIT sobregira el inventario. No podrá guardar el Documento", MsgBoxStyle.Critical, "Error")
                        End If
                    End If
                End If

            Case "PrecioVenta"
                Precio = e.Value
                Dim Descto As Decimal = Decimal.Round(Precio * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)

                'VALIDO QUE EL PRECIO DEL PRODUCTO NO SEA MENOR AL COSTO ////////////////////////////////////////////////////////////////////////////////////////////
                PrecioCosto = gv.GetFocusedRowCellValue("PrecioCosto")
                TipoProducto = gv.GetFocusedRowCellValue("TipoProducto")

                If Precio < PrecioCosto And TipoProducto = 1 And SiEsNulo(dtParametros.Rows(0).Item("FacturarMenosCosto"), False) = False Then
                    MsgBox("El precio del producto no puede ser menor al precio de costo", MsgBoxStyle.Critical, "Error")

                    Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), gv.GetFocusedRowCellValue("IdPrecio"))
                    If entProducto.EsExento = False And (DocumentoDetallaIva Or leTipoImpuesto.EditValue > 1) Then 'SE EXTRAE EL IVA CUANDO EL PRODUCTO ESTÁ GRAVADO
                        Precio = Decimal.Round(Precio / (pnIVA + 1), 4)
                    End If

                    e.Value = Precio
                    Descto = Decimal.Round(Precio * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)
                    Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Descto
                    gv.SetFocusedRowCellValue("PrecioUnitario", Precio)
                End If
                '///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)
                CalculaTotalFila(gv.GetFocusedRowCellValue("Cantidad"), Precio)

            Case "PrecioUnitario"
                Precio = e.Value
                Dim Descto As Decimal = Decimal.Round(Precio * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)

                'VALIDO QUE EL PRECIO DEL PRODUCTO NO SEA MENOR AL COSTO ////////////////////////////////////////////////////////////////////////////////////////////
                PrecioCosto = gv.GetFocusedRowCellValue("PrecioCosto")
                TipoProducto = gv.GetFocusedRowCellValue("TipoProducto")
                ultPrecio = gv.GetFocusedRowCellValue("UltPrecio")

                If Precio < ultPrecio And TipoProducto = 1 Then
                    frmValidaToken.Text = "AUTORIZACIÓN DE DESCUENTOS"
                    frmValidaToken.Usuario = objMenu.User
                    frmValidaToken.Descuento = Precio 'e.Value
                    frmValidaToken.IdProducto = SiEsNulo(gv.GetFocusedRowCellValue("IdProducto"), "")
                    frmValidaToken.Fecha = deFecha.EditValue
                    frmValidaToken.ShowDialog()
                    If frmValidaToken.Acceso = False Then
                        MsgBox("El precio del producto no puede ser menor al ultimo precio", MsgBoxStyle.Critical, "Error")
                        Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), gv.GetFocusedRowCellValue("IdPrecio"))
                        If entProducto.EsExento = False And (DocumentoDetallaIva Or leTipoImpuesto.EditValue > 1) Then 'SE EXTRAE EL IVA CUANDO EL PRODUCTO ESTÁ GRAVADO
                            Precio = Decimal.Round(Precio / (pnIVA + 1), 4)
                        End If
                        e.Value = Precio
                    Else
                        AutorizoDescto = frmValidaToken.teToken.EditValue
                    End If
                    gv.SetFocusedRowCellValue("DescuentoAutorizadoPor", AutorizoDescto)
                    frmValidaToken.Dispose()
                End If

                If Precio < PrecioCosto And TipoProducto = 1 And SiEsNulo(dtParametros.Rows(0).Item("FacturarMenosCosto"), False) = False Then
                    MsgBox("El precio del producto no puede ser menor al precio de costo", MsgBoxStyle.Critical, "Error")

                    Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), gv.GetFocusedRowCellValue("IdPrecio"))
                    If entProducto.EsExento = False And (DocumentoDetallaIva Or leTipoImpuesto.EditValue > 1) Then 'SE EXTRAE EL IVA CUANDO EL PRODUCTO ESTÁ GRAVADO
                        Precio = Decimal.Round(Precio / (pnIVA + 1), 4)
                    End If

                    e.Value = Precio
                End If
                '///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                CalculaTotalFila(gv.GetFocusedRowCellValue("Cantidad"), e.Value)
        End Select
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        Dim IdProduc As String = SiEsNulo(gv.GetFocusedRowCellValue("IdProducto"), "")
        Dim IdPrecio As Integer = SiEsNulo(gv.GetFocusedRowCellValue("IdPrecio"), 0)

        If IdProduc = "" OrElse Not blInve.inv_VerificaCodigoProducto(IdProduc) Then
            e.Valid = False
            gv.SetColumnError(gv.Columns("IdProducto"), "Código de producto no válido, no existe o no permite facturación")
        End If

        If IdPrecio = 0 Then
            e.Valid = False
            gv.SetColumnError(gv.Columns("IdPrecio"), "Debe de especificar el precio")
        End If

        ' SI ES TIKETE QUE META TODOS LOS PRODUCTOS QUE QUIERA
        If leTipoComprobante.EditValue <> 10 Then
            If teLineas.EditValue > teLimiteLineas.EditValue Then
                MsgBox("Ya no puede ingresar mas lineas en el documento", MsgBoxStyle.Exclamation, "Nota")
                e.Valid = False
                Exit Sub
            End If
        End If
        CalculaTotalFila(gv.GetFocusedRowCellValue("Cantidad"), gv.GetFocusedRowCellValue("PrecioUnitario"))
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        CalcularTotales()
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab OrElse e.KeyCode = Keys.Down OrElse e.KeyCode = Keys.Right Then

            If gv.FocusedColumn.FieldName = "IdProducto" Then

                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProduc = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "and PermiteFacturar=1", "")
                    inv_frmConsultaProductos.Dispose()
                    gv.EditingValue = IdProduc
                End If

                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                If entProducto.IdProducto = "" Then
                    entProducto = objTablas.inv_ProductosSelectByPK(bl.inv_ObtieneProductoAlias(gv.EditingValue))
                    gv.EditingValue = entProducto.IdProducto
                End If

                Dim dtSaldo As DataTable = blInve.inv_ObtieneExistenciaCostoProducto(gv.EditingValue, leBodega.EditValue)
                gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                gv.SetFocusedRowCellValue("TipoProducto", entProducto.TipoProducto)
                If dtSaldo.Rows.Count > 0 Then
                    gv.SetFocusedRowCellValue("PrecioCosto", entProducto.PrecioCosto)
                Else
                    gv.SetFocusedRowCellValue("PrecioCosto", SiEsNulo(dtSaldo.Rows(0).Item("PrecioCosto"), 0.0))
                End If
                If entProducto.TipoProducto > 1 Then 'PARA TODOS LOS SERVICIOS NO APLICA
                    Exit Sub
                End If
                If dtParametros.Rows(0).Item("ValidarExistencias") = True Then
                    Existencia = CDec(blInve.inv_ObtieneExistenciaProducto(gv.EditingValue, leBodega.EditValue, deFecha.EditValue).Rows(0).Item("SaldoExistencias"))
                    teSaldo.EditValue = Format(Existencia, "###,##0.00")
                    'entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)

                    If dtParametros.Rows(0).Item("AlertaMinimos") Then
                        If Existencia < entProducto.ExistenciaMinima Then
                            MsgBox("El producto " + entProducto.Nombre + " se encuentra con existencia menor al mínimo", MsgBoxStyle.Critical, "Error")
                        End If
                    End If

                    If Not entProducto.Compuesto Then
                        If Existencia < gv.GetFocusedRowCellValue("Cantidad") And TipoAplicacion = 1 And entProducto.TipoProducto = 1 And Not ceDevolucion.EditValue Then
                            MsgBox("Código de producto no registrado o sin existencia suficiente", MsgBoxStyle.Critical, "Error")
                        Else
                            entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                            gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                        End If
                    Else
                        Dim dtExistenciaKit As DataTable = blInve.inv_ObtieneExistenciaKit(gv.EditingValue, leBodega.EditValue, deFecha.EditValue)

                        If SiEsNulo(dtExistenciaKit.Rows(0).Item("CantidadDisponible"), 0.0) < gv.GetFocusedRowCellValue("Cantidad") And TipoAplicacion = 1 And Not ceDevolucion.EditValue Then
                            MsgBox("Código de producto no registrado o sin existencia suficiente en la composición del KIT", MsgBoxStyle.Critical, "Error")
                        Else
                            gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                        End If
                    End If
                Else
                    If dtParametros.Rows(0).Item("AlertaMinimos") Then
                        If Existencia < entProducto.ExistenciaMinima Then
                            MsgBox("El producto " + entProducto.Nombre + " se encuentra con existencia menor al mínimo", MsgBoxStyle.Critical, "Error")
                        End If
                    End If

                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                End If
            End If
            If gv.FocusedColumn.FieldName = "IdPrecio" Then
                entProducto = objTablas.inv_ProductosSelectByPK(gv.GetFocusedRowCellValue("IdProducto"))
                Dim ValorDescuento As Decimal = ValorDescuento = blInve.inv_ObtieneDescuentoMargen(gv.GetFocusedRowCellValue("Cantidad"))

                'VALIDO SI LOS PRECIOS SON POR PRODUCTO Ó POR LISTA DE PRECIOS DE CLIENTES
                If TipoPrecioConfiguracion <> -999 Then '---> 2
                    If chkMargen.EditValue = True And entProducto.IdSubGrupo = 1 Then
                        Precio = SiEsNulo(bl.fac_ObtienePrecioCliente(gv.GetFocusedRowCellValue("IdProducto"), beCodCliente.EditValue), 0.0) - ValorDescuento
                    Else
                        Precio = SiEsNulo(bl.fac_ObtienePrecioCliente(gv.GetFocusedRowCellValue("IdProducto"), beCodCliente.EditValue), 0.0)
                    End If
                End If

                If Precio = 0 Then
                    If chkMargen.EditValue = True And entProducto.IdSubGrupo = 1 Then
                        ultPrecio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), 2) - ValorDescuento
                        Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), gv.EditingValue) - ValorDescuento
                    Else
                        Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), gv.EditingValue)
                        ultPrecio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), 2)
                    End If
                End If

                If entProducto.EsExento = False And (DocumentoDetallaIva Or leTipoImpuesto.EditValue > 1) Then 'SE EXTRAE EL IVA CUANDO EL PRODUCTO ESTÁ GRAVADO
                    Precio = Decimal.Round(Precio / (pnIVA + 1), 4)
                    ultPrecio = Decimal.Round(ultPrecio / (pnIVA + 1), 4)
                End If
                gv.SetFocusedRowCellValue("PrecioVenta", Precio)
                gv.SetFocusedRowCellValue("UltPrecio", ultPrecio)

                Dim Descto As Decimal = Decimal.Round(Precio * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)
                gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescuento", Decimal.Round((Descto * gv.GetFocusedRowCellValue("Cantidad")), 2))
                Precio = Precio - Descto
                gv.SetFocusedRowCellValue("PrecioUnitario", Precio)

                'VALIDO QUE EL PRECIO DEL PRODUCTO NO SEA MENOR AL COSTO ////////////////////////////////////////////////////////////////////////////////////////////
                PrecioCosto = gv.GetFocusedRowCellValue("PrecioCosto")
                TipoProducto = gv.GetFocusedRowCellValue("TipoProducto")

                If Precio < PrecioCosto And TipoProducto = 1 And SiEsNulo(dtParametros.Rows(0).Item("FacturarMenosCosto"), False) = False Then
                    MsgBox("El precio del producto no puede ser menor al precio de costo", MsgBoxStyle.Critical, "Error")

                    Precio = blInve.inv_ObtienePrecioProducto(gv.GetFocusedRowCellValue("IdProducto"), gv.EditingValue)
                    If entProducto.EsExento = False And (DocumentoDetallaIva Or leTipoImpuesto.EditValue > 1) Then 'SE EXTRAE EL IVA CUANDO EL PRODUCTO ESTÁ GRAVADO
                        Precio = Decimal.Round(Precio / (pnIVA + 1), 4)
                    End If

                    gv.SetFocusedRowCellValue("PrecioVenta", Precio)
                    Descto = Decimal.Round(Precio * gv.GetFocusedRowCellValue("PorcDescuento") / 100, 4)
                    Precio = gv.GetFocusedRowCellValue("PrecioVenta") - Descto
                    gv.SetFocusedRowCellValue("PrecioUnitario", Precio)
                End If
                '///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                CalculaTotalFila(gv.GetFocusedRowCellValue("Cantidad"), Precio)

            End If
        End If
    End Sub
#End Region

    Private Sub CalculaTotalFila(ByVal Cantidad As Decimal, ByVal Precio As Decimal)
        'revisar este codigo detenidamente
        TipoVenta = leTipoImpuesto.EditValue
        entProducto = objTablas.inv_ProductosSelectByPK(gv.GetFocusedRowCellValue("IdProducto"))
        If entProducto.EsExento Then
            TipoVenta = 2
        End If
        'cuando el iva esta incluído en la facturacion. El precio del producto está guardado con IVA incluido
        Precio = Decimal.Round(Cantidad * Precio, 2)
        If TipoVenta = 1 Or TipoVenta = 3 Then 'precio gravado o con tasa cero
            gv.SetFocusedRowCellValue("VentaExenta", 0.0)
            gv.SetFocusedRowCellValue("VentaNoSujeta", 0.0)
            gv.SetFocusedRowCellValue("VentaAfecta", Precio)
            If DocumentoDetallaIva Then 'crédito fiscal, nota de crédito, nota de débito
                gv.SetFocusedRowCellValue("VentaNeta", Precio)
                gv.SetFocusedRowCellValue("ValorIVA", Decimal.Round(Precio * pnIVA, 2))
            Else 'ticket, consumidor final, factura de exportación
                Precio = Decimal.Round(Precio / (pnIVA + 1), 2)
                gv.SetFocusedRowCellValue("VentaNeta", Precio)
                gv.SetFocusedRowCellValue("ValorIVA", gv.GetFocusedRowCellValue(gv.Columns("VentaAfecta")) - Precio)
            End If
            If TipoVenta = 3 Then
                gv.SetFocusedRowCellValue("ValorIVA", 0.0)
            End If
        End If
        If TipoVenta = 2 Then 'precio exento
            gv.SetFocusedRowCellValue("VentaExenta", Precio)
            gv.SetFocusedRowCellValue("VentaNoSujeta", 0.0)
            gv.SetFocusedRowCellValue("VentaAfecta", 0.0)
            gv.SetFocusedRowCellValue("VentaNeta", Precio)
            gv.SetFocusedRowCellValue("ValorIVA", 0.0)
        End If
        If TipoVenta = 4 Then 'precio no sujeto
            gv.SetFocusedRowCellValue("VentaExenta", 0.0)
            gv.SetFocusedRowCellValue("VentaNoSujeta", Precio)
            gv.SetFocusedRowCellValue("VentaAfecta", 0.0)
            gv.SetFocusedRowCellValue("VentaNeta", Precio)
            gv.SetFocusedRowCellValue("ValorIVA", 0.0)
        End If

        If leTipoComprobante.EditValue = 19 Or leTipoComprobante.EditValue = 24 Then
            gv.SetFocusedRowCellValue("ValorIVA", 0.0)
        End If
        'gv.UpdateTotalSummary()

        CalcularTotales()
    End Sub

    Private Sub CalcularTotales()
        gv.UpdateTotalSummary()
        If ceCESC.Checked Then
            If Not CescEditado Then
                teCESC.EditValue = Decimal.Round(Me.gcVentaAfecta.SummaryItem.SummaryValue * 0.05, 2)
            End If
        Else
            teCESC.EditValue = 0.0
        End If
        teDescuento.EditValue = Me.gcValorDescuento.SummaryItem.SummaryValue
        'obtener el tipo de documentos para efectos de calcular el IVA

        If DocumentoDetallaIva Then
            teIVA.EditValue = Decimal.Round(Me.gcVentaAfecta.SummaryItem.SummaryValue * pnIVA, 2)
        Else
            teIVA.EditValue = Me.gcValorIVA.SummaryItem.SummaryValue
        End If

        Dim IvaRetPer As Decimal = 0.0
        If ceRetencionPercepcion.Checked Then
            '' dtParametros.Rows(0).Item("TipoContribuyente") = 3 And dtParametros.Rows(0).Item("EsRetenedor")
            If rgTipo.EditValue = 3 Then
                If IIf(DocumentoDetallaIva, Me.gcVentaAfecta.SummaryItem.SummaryValue >= 100, Me.gcVentaNeta.SummaryItem.SummaryValue >= 100) Then
                    teRetencionPercepcion.EditValue = Decimal.Round(IIf(DocumentoDetallaIva, Me.gcVentaAfecta.SummaryItem.SummaryValue, Me.gcVentaNeta.SummaryItem.SummaryValue) * CDec(dtParametros.Rows(0).Item("PorcPercepcion") / 100), 2)
                    IvaRetPer = teRetencionPercepcion.EditValue
                Else
                    teRetencionPercepcion.EditValue = 0.0
                End If
            Else
                If IIf(DocumentoDetallaIva, Me.gcVentaAfecta.SummaryItem.SummaryValue >= 100, Me.gcVentaNeta.SummaryItem.SummaryValue >= 100) Then
                    teRetencionPercepcion.EditValue = Decimal.Round(IIf(DocumentoDetallaIva, Me.gcVentaAfecta.SummaryItem.SummaryValue, Me.gcVentaNeta.SummaryItem.SummaryValue) * CDec(dtParametros.Rows(0).Item("PorcRetencion") / 100), 2)
                    IvaRetPer = -teRetencionPercepcion.EditValue
                Else
                    teRetencionPercepcion.EditValue = 0.0
                End If
            End If
        Else
            teRetencionPercepcion.EditValue = 0.0
        End If
        If leTipoImpuesto.EditValue > 1 Then   'para los exentos, tasas ceros y cualquier otro
            teIVA.EditValue = 0.0
            teRetencionPercepcion.EditValue = 0.0
            IvaRetPer = 0.0
        End If
        If DocumentoDetallaIva Then 'crédito fiscal, nota de crédito, nota de débito
            teTotal.EditValue = Me.gcVentaNoSujeta.SummaryItem.SummaryValue + Me.gcVentaExenta.SummaryItem.SummaryValue + Me.gcVentaAfecta.SummaryItem.SummaryValue + teIVA.EditValue + IvaRetPer + teCESC.EditValue
        Else
            teTotal.EditValue = Me.gcVentaNoSujeta.SummaryItem.SummaryValue + Me.gcVentaExenta.SummaryItem.SummaryValue + Me.gcVentaAfecta.SummaryItem.SummaryValue + IvaRetPer + teCESC.EditValue
        End If

        leTipoComprobante.Properties.ReadOnly = teTotal.EditValue <> 0.0
        beCodCliente.Properties.ReadOnly = teTotal.EditValue <> 0.0
        leTipoImpuesto.Properties.ReadOnly = teTotal.EditValue <> 0.0
        teLineas.EditValue = gv.RowCount()
    End Sub

    Private Sub beIdCliente_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCodCliente.ButtonClick
        If teTotal.EditValue <> 0 Then
            MsgBox("Ya no puede cambiar el cliente", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        beCodCliente.EditValue = ""
        UsuarioDioClic = True
        beIdCliente_Validated(beCodCliente, New EventArgs)
    End Sub

    Private Sub beIdCliente_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCodCliente.Validated
        If Not UsuarioDioClic And beCodCliente.EditValue = "" Then
            Exit Sub
        End If
        UsuarioDioClic = False

        Dim entCliente As fac_Clientes = objConsultas.cnsClientes(fac_frmConsultaClientes, beCodCliente.EditValue)
        If entCliente.IdCliente = "" Then
            beCodCliente.EditValue = ""
            beCodCliente.Focus()
            Exit Sub
        End If

        If entCliente.IdTipoImpuesto <= 0 Then
            MsgBox("El Cliente seleccionado no esta configurado completamente. No se reconoce el Tipo Impuesto Aplica", MsgBoxStyle.Exclamation, "Nota")
            Exit Sub
        End If

        ValidaSaldoCliente()

        With entCliente
            beCodCliente.EditValue = .IdCliente
            teNombre.EditValue = .RazonSocial
            teDireccion.EditValue = .Direccion
            teNit.EditValue = .Nit
            teNRC.EditValue = .Nrc
            teGiro.EditValue = .Giro
            leDepartamento.EditValue = .IdDepartamento
            leMunicipio.EditValue = .IdMunicipio
            seDiasCredito.EditValue = .DiasCredito
            leVendedor.EditValue = .IdVendedor
            TelimiteSaldo.EditValue = .LimiteCredito
            leTipoComprobante.EditValue = .IdTipoComprobante
            leFormaPago.EditValue = .IdFormaPago
            leTipoImpuesto.EditValue = .IdTipoImpuesto
            rgTipo.EditValue = .TipoImpuestoAdicional
            If .TipoImpuestoAdicional > 1 Then
                ceRetencionPercepcion.EditValue = True
                If .TipoImpuestoAdicional = 2 Then
                    ceRetencionPercepcion.Text = "Aplicar retención"
                    ceRetencionPercepcion.EditValue = True
                    lblPercReten.Text = "(-) IVA Retenido:"
                Else
                    ceRetencionPercepcion.Text = "APLICAR PERCEPCIÓN"
                    ceRetencionPercepcion.EditValue = True
                    lblPercReten.Text = "(+) IVA Percibido:"
                End If
            Else
                ceRetencionPercepcion.EditValue = False
            End If
            If .IdTipoComprobante = 6 Or .IdTipoComprobante = 10 Then
                lblNRC_DUI.Text = "DUI:"
            End If
            If .IdTipoComprobante = 5 Or .IdTipoComprobante = 8 Or .IdTipoComprobante = 9 Then
                lblNRC_DUI.Text = "NRC:"
            End If
            ''ceRetencionPercepcion.EditValue = .AplicaRetencion

            'If beCodCliente.EditValue = "1" Then
            '    teNombre.Properties.ReadOnly = True
            'Else
            '    teNombre.Properties.ReadOnly = False
            'End If

            If .IdPrecio > 0 Then 'si el cliente tiene definido un precio, se tomará como predeterminado
                TipoPrecio = .IdPrecio
            Else ' de lo contrario se tomará el precio global de facturación

                If IdTipoPrecioUser = 0 Then
                    TipoPrecio = dtParametros.Rows(0).Item("IdTipoPrecio")
                Else
                    TipoPrecio = IdTipoPrecioUser
                End If

            End If
        End With
    End Sub

    Private Sub ValidaSaldoCliente()
        If leTipoComprobante.EditValue = 8 Or ceDevolucion.EditValue Then 'SOLO PARA ESTOS COMPROBANTES ES NECESARIA LA VALIDACIÓN
            Dim SaldoCliente As Decimal = SiEsNulo(blCxc.SaldoCliente(beCodCliente.EditValue, deFecha.EditValue, piIdSucursalUsuario), 0.0)
            If SaldoCliente <= 0.0 And leFormaPago.EditValue = 2 Then
                MsgBox("El cliente no tiene saldo pendiente para aplicar la NC o Devolución", MsgBoxStyle.Critical, "Error de Datos")
                beCodCliente.EditValue = ""
                teNombre.EditValue = ""
                teNRC.EditValue = ""
                teNit.EditValue = ""
                teDireccion.EditValue = ""
                teGiro.EditValue = ""
            End If
        Else
            Exit Sub
        End If
    End Sub

    Private Function DatosValidos()
        CalcularTotales()
        If beCodCliente.EditValue = "" Then
            MsgBox("Debe de especificar el código de cliente", MsgBoxStyle.Critical, "Nota")
            beCodCliente.Focus()
            Return False
        End If
        ' LA DEVOLUCION LA PUEDE HACER CON VALOR A CERO
        If teTotal.EditValue = 0.0 And leTipoComprobante.EditValue <> 19 Then
            MsgBox("No ha registrado ninguna venta", MsgBoxStyle.Critical, "Nota")
            beCodCliente.Focus()
            Return False
        End If
        If ceDevolucion.EditValue And (teNombre.EditValue = "" Or teNit.EditValue = "") Then
            MsgBox("La venta es una devolución, debe de especificar el nombre y NIT", MsgBoxStyle.Critical, "Nota")
            teNombre.Focus()
            Return False
        End If
        If teTotal.EditValue >= 200 And (teNombre.EditValue = "" Or teNit.EditValue = "") Then
            MsgBox("La venta es mayor a $200.00, debe de especificar el nombre y NIT", MsgBoxStyle.Critical, "Nota")
            teNit.Focus()
            Return False
        End If
        If teTotal.EditValue >= 11428.57 And (teNombre.EditValue = "" Or teNit.EditValue = "") Then
            MsgBox("La venta es mayor a $11,428.57, debe de especificar el nombre y NIT", MsgBoxStyle.Critical, "Nota")
            teNit.Focus()
            Return False
        End If
        If leFormaPago.Text = "" Then
            MsgBox("Debe asignar la forma de Pago", MsgBoxStyle.Critical, "Nota")
            leFormaPago.Focus()
            Return False
        End If
        If teLineas.EditValue > teLimiteLineas.EditValue Then
            MsgBox("El número de lineas en el documento excede el límite establecido", MsgBoxStyle.Critical, "Nota")
            Return False
        End If
        Dim entTipoComprob As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(leTipoComprobante.EditValue)
        If dtParametros.Rows(0).Item("UtilizarFormularioUnico") And entTipoComprob.FormularioUnico Then
            If teNumero.EditValue = "" Or teNumeroUnico.EditValue = "" Then
                MsgBox("El número de comprobante o Formulario Único esta en blanco", MsgBoxStyle.Critical, "Nota")
                Return False
            End If
        End If
        If leTipoComprobante.EditValue = 5 Or leTipoComprobante.EditValue = 8 Or leTipoComprobante.EditValue = 9 Then
            If teNRC.EditValue = "" Or teNit.EditValue = "" Or teGiro.EditValue = "" Then
                MsgBox("El NRC, NIT o Giro esta en blanco", MsgBoxStyle.Critical, "Nota")
                Return False
            End If
        End If

        'COLOCAR A PARTIR DE ACÁ LA VALIDACIÓN DE LOS DATOS CRÍTICOS. POR FAVOR RESPETAR ESO
        'Solo cuando es Credito se asume que se valida su Saldo Vencido (2 debe de ser siempre al Credito)
        If leFormaPago.EditValue = 2 Then
            Dim DTSaldoCliente As DataTable = bl.fac_ConsultaSaldoCliente(beCodCliente.EditValue, dtParametros.Rows(0).Item("DiasMaximoCredito"))
            If DTSaldoCliente.Rows(0).Item("Vencido") > 0.0 Then
                MsgBox("El saldo vencido del cliente por $: " & DTSaldoCliente.Rows(0).Item("Vencido") & " Excede los días permitidos", MsgBoxStyle.Critical, "Nota")
                Return False
            End If
        End If
        Dim msj As String = "", SaldoCliente As Decimal = 0.0, IdComprobante As Integer = 0, Existencia As Decimal = 0.0
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        Dim dtRenu As DataTable = bl.fac_ObtenerDetalleDocumento("fac_VentasDetalle", -99999)
        For l = 0 To gv.DataRowCount - 1
            Dim dr1 As DataRow = dtRenu.NewRow()
            dr1("IdProducto") = gv.GetRowCellValue(l, "IdProducto")
            dr1("Cantidad") = gv.GetRowCellValue(l, "Cantidad")
            dtRenu.Rows.Add(dr1)
        Next
        '-- AGRUPACION DE PRODUCTOS PARA QUE SE PUEDAN REPETIR CODIGOS
        Dim query = From q In dtRenu.AsEnumerable() Select q Order By q.Item("IdProducto")
        Dim dtResultado As New DataTable
        dtResultado.Columns.Add("IdProducto")
        dtResultado.Columns.Add("Cantidad")
        ''
        Dim dtCopy As New DataTable 'query.CopyToDataTable()
        dtCopy = query.CopyToDataTable()
        dtCopy.Rows.Add()
        Dim dr As DataRow = dtCopy.NewRow()
        Dim i, value As Integer
        For j As Integer = 0 To dtCopy.Rows.Count - 2
            Dim item = dtCopy.Rows(j)
            Dim IdPro = Convert.ToString(item("IdProducto")).ToUpper
            Dim cant = Convert.ToDecimal(item("Cantidad"))
            Dim drr As DataRow = dtResultado.NewRow()
            drr.Item(0) = IdPro
            Dim filaSig As String = Convert.ToString(dtCopy.Rows(i + 1).Item("IdProducto")).ToUpper 'fila siguiente
            If (IdPro = filaSig) Then 'producto actual es igual a la siguiente zona
                value += cant
            Else 'cuando cambie el producto insertar nueva fila 
                drr.Item(1) = value + cant
                dtResultado.Rows.Add(drr)
                value = 0
            End If
            i += 1 'indice
        Next
        'FIN DE AGRUPACION DE PRODUCTOS

        For h = 0 To dtResultado.Rows.Count - 1
            'PORQUE NO DEJARLO EN EL DATATABLE DEL GRID??
            If blInve.inv_ObtieneTipoProducto(SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), "")) > 1 Then 'PARA TODOS LOS SERVICIOS NO APLICA
                Exit For
            End If

            'porque no dejarlo en el datatable del grid ??
            EsCompuesto = blInve.inv_ProductoEsCompuesto(SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), ""))

            If dtParametros.Rows(0).Item("ValidarExistencias") = True Then
                If TipoAplicacion = 1 And Not ceDevolucion.EditValue Then 'es una venta, no devolución
                    If Not EsCompuesto Then
                        Dim dtExistencia As DataTable = blInve.inv_ObtieneExistenciaProducto(SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), ""), leBodega.EditValue, deFecha.EditValue)
                        Existencia = 0.0
                        If dtExistencia.Rows.Count > 0 Then
                            Existencia = dtExistencia.Rows(0).Item("SaldoExistencias")
                        End If

                        'Existencia = CDec(blInve.inv_ObtieneExistenciaProducto(SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), ""), leBodega.EditValue).Rows(0).Item("SaldoExistencias"))
                        If Existencia < SiEsNulo(dtResultado.Rows(h).Item("Cantidad"), 0) And Not ceDevolucion.EditValue Then
                            msj = "El producto --> " & SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), "") & ", no tiene existencia suficiente a la fecha " + Format(deFecha.EditValue, "dd/MM/yyyy")
                            Exit For
                        End If
                        If deFecha.EditValue < Today Then
                            dtExistencia = blInve.inv_ObtieneExistenciaProducto(SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), ""), leBodega.EditValue, Today)
                            Existencia = 0.0
                            If dtExistencia.Rows.Count > 0 Then
                                Existencia = dtExistencia.Rows(0).Item("SaldoExistencias")
                            End If
                            If Existencia < SiEsNulo(dtResultado.Rows(h).Item("Cantidad"), 0) And Not ceDevolucion.EditValue Then
                                msj = "El producto --> " & SiEsNulo(dtResultado.Rows(h).Item("IdProducto"), "") & ", no tiene existencia suficiente a la fecha " + Format(Today, "dd/MM/yyyy")
                                Exit For
                            End If
                        End If
                    Else
                        Dim dtExistenciaKit As DataTable = blInve.inv_ObtieneExistenciaKit(dtResultado.Rows(h).Item("IdProducto"), leBodega.EditValue, deFecha.EditValue)
                        If SiEsNulo(dtExistenciaKit.Rows(0).Item("CantidadDisponible"), 0.0) < SiEsNulo(dtResultado.Rows(h).Item("Cantidad"), 0) And TipoAplicacion = 1 And Not ceDevolucion.EditValue Then
                            msj = "Código de producto no registrado o sin existencia suficiente en la composición del KIT"
                            Exit For
                        End If
                    End If
                Else

                End If
            End If
        Next

        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "PrecioUnitario") = 0.0 Then
                msj = "El producto " & gv.GetRowCellValue(i, "IdProducto") & ",  No tiene precio"
                Exit For
            End If
            If gv.GetRowCellValue(i, "PrecioUnitario") < SiEsNulo(gv.GetRowCellValue(i, "PrecioCosto"), 0) And gv.GetRowCellValue(i, "TipoProducto") = 1 And SiEsNulo(dtParametros.Rows(0).Item("FacturarMenosCosto"), False) = False Then
                msj = "El producto " & gv.GetRowCellValue(i, "IdProducto") & ", tiene precio de venta menor al precio de costo"
                Exit For
            End If
        Next
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.None

        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If

        ' solo cuando es credito se valida todo lo demas , Colocar aqui toda Validacion con Creditos
        If leFormaPago.EditValue = 2 Then
            Dim AbonoHeader As cpc_Abonos
            IdComprobante = blCxc.cpc_ObtenerUltAbono(beCodCliente.EditValue)
            AbonoHeader = objTablas.cpc_AbonosSelectByPK(IdComprobante)
            SaldoCliente = blCxc.SaldoCliente(beCodCliente.EditValue, deFecha.EditValue, piIdSucursalUsuario)
            If leTipoComprobante.EditValue = 8 And leFormaPago.EditValue = 2 Then
                If deFecha.EditValue >= AbonoHeader.Fecha Then
                    If teTotal.EditValue > SaldoCliente Then
                        MsgBox("El Valor de la Nota de Credito excede el Saldo del cliente", MsgBoxStyle.Critical, "Nota")
                        Return False
                    End If
                    'Else
                    '    MsgBox("La Fecha de la Nota de Credito, No puede ser inferior al Ultimo Abono", MsgBoxStyle.Critical, "Nota")
                    '    Return False
                End If
            End If
            ' se dejo de llamar la entidad del cliente y se paso desde que se selecciona el cliente a cargar un control 
            SaldoCliente = SaldoCliente + teTotal.EditValue
            If leTipoComprobante.EditValue <> 8 Then
                If SiEsNulo(TelimiteSaldo.EditValue, 0) > 0 Then
                    If SaldoCliente > SiEsNulo(TelimiteSaldo.EditValue, 0) Then
                        MsgBox("No puede crear el documento" & _
                        " El saldo por cobrar al cliente excede su limite", MsgBoxStyle.Critical, "Nota")
                        Return False
                    End If
                End If
            End If
        End If
        Serie = bl.fac_ObtenerSerieDocumento(IdSucUser, cboPuntoVenta.EditValue, leTipoComprobante.EditValue)
        Dim IdComp As Integer = bl.getIdComprobante(IdSucUser, cboPuntoVenta.EditValue, leTipoComprobante.EditValue, Serie, teNumero.EditValue)
        If IdComp > 0 Then
            MsgBox("El numero de comprobante ya existe", 64, "Nota")
            NumeroComprobante()
            Return False
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            MsgBox("No puede crear el documento" & _
                   "La fecha corresponde a un período ya cerrado", MsgBoxStyle.Critical, "Nota")
            Return False
        End If
        Return True
    End Function
    Private Function ImprimeFactura() As Boolean
        Dim dt As DataTable = bl.fac_ObtenerDetalleDocumento("fac_VentasDetalle", FacturaHeader.IdComprobante)
        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)

        'DESARROLLO EXCLUSIVO PARA ASOCIACION S1
        If gsNombre_Empresa.ToUpper().Contains("S1 EL SALVADOR") Then 'cambio exclusivo para GS1(CLIENTE DE AE INFORMATICA)
            fac_frmConsultaFacturacion.ReImprimeDocumento(FacturaHeader.IdComprobante)
            Exit Function
        End If

        Select Case FacturaHeader.IdTipoComprobante
            Case 5 'crédito fiscal


                Dim Template = Application.StartupPath & "\Plantillas\cfNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""

                With FacturaHeader
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlNumRemision.Text = teNotaRemision.EditValue
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = teGiro.EditValue
                    rpt.xrlDireccion.Text = teDireccion.EditValue
                    rpt.xrlMunic.Text = leMunicipio.Text
                    rpt.xrlDepto.Text = leDepartamento.Text
                    rpt.xrlVendedor.Text = leVendedor.Text
                    rpt.xrlFormaPago.Text = leFormaPago.Text
                    rpt.xrlTelefono.Text = ""
                    rpt.xrlDiasCredito.Text = .DiasCredito
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlVentaAfecta.Text = Format(FacturaHeader.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(FacturaHeader.TotalIva, "###,##0.00")
                    rpt.xrlCESC.Text = Format(FacturaHeader.TotalImpuesto2, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(FacturaHeader.TotalIva + FacturaHeader.TotalAfecto, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(FacturaHeader.TotalImpuesto1, "##,###.00")
                    rpt.xrlVentaExenta.Text = Format(FacturaHeader.TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(FacturaHeader.TotalComprobante, "###,##0.00")

                    Dim Decimales = String.Format("{0:c}", FacturaHeader.TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                    rpt.xrlCantLetras.Text = Num2Text(Int(FacturaHeader.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = meComentario.EditValue

                    If dtParametros.Rows(0).Item("VistaPreviaFacturar") Then
                        rpt.ShowPreviewDialog()
                    Else
                        rpt.PrinterName = SiEsNulo(entComprobantes.NombreImpresor, "")
                        rpt.Print()
                    End If

                End With
            Case 6 'consumidor final
                Dim Template = Application.StartupPath & "\Plantillas\faNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFactura
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""
                rpt.XrSubreport1.ReportSource.DataSource = dt
                rpt.XrSubreport1.ReportSource.DataMember = ""
                rpt.XrSubreport2.ReportSource.DataSource = dt
                rpt.XrSubreport2.ReportSource.DataMember = ""

                With FacturaHeader
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNumRemision.Text = teNotaRemision.EditValue
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlGiro.Text = teGiro.EditValue
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = leMunicipio.Text
                    rpt.xrlDepto.Text = leDepartamento.Text
                    rpt.xrlFormaPago.Text = leFormaPago.Text
                    rpt.xrlVendedor.Text = leVendedor.Text
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlDiasCredito.Text = .DiasCredito
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "##,###.00")
                    rpt.xrlCESC.Text = Format(.TotalImpuesto2, "##,###.00")
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    Dim CantidaLetras As String = Num2Text(Int(IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))) & Decimales & " DÓLARES"
                    rpt.xrlCantLetras.Text = CantidaLetras
                    rpt.xrlComentario.Text = meComentario.EditValue
                    '2 Formato
                    rpt.xrlCodigo2.Text = .IdCliente
                    rpt.xrlNombre2.Text = .Nombre
                    rpt.xrlNumero2.Text = .Numero
                    rpt.xrlNumFormulario2.Text = .NumFormularioUnico
                    rpt.xrlNumRemision2.Text = teNotaRemision.EditValue
                    rpt.xrlFecha2.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion2.Text = Now
                    rpt.xrlSucursal2.Text = Sucursales.Nombre
                    rpt.xrlNit2.Text = .Nit
                    rpt.xrlPedido2.Text = .OrdenCompra
                    rpt.xrlNrc2.Text = .Nrc
                    rpt.xrlGiro2.Text = teGiro.EditValue
                    rpt.xrlDireccion2.Text = .Direccion
                    rpt.xrlTelefono2.Text = .Telefono
                    rpt.xrlMunic2.Text = leMunicipio.Text
                    rpt.xrlDepto2.Text = leDepartamento.Text
                    rpt.xrlFormaPago2.Text = leFormaPago.Text
                    rpt.xrlVendedor2.Text = leVendedor.Text
                    rpt.xrlVentaACuenta2.Text = .VentaAcuentaDe
                    rpt.xrlDiasCredito2.Text = .DiasCredito
                    rpt.xrlVentaAfecta2.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal2.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta2.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlIvaRetenido2.Text = Format(.TotalImpuesto1, "##,###.00")
                    rpt.xrlCESC2.Text = Format(.TotalImpuesto2, "##,###.00")
                    rpt.xrlTotal2.Text = Format(.TotalComprobante, "###,##0.00")
                    rpt.xrlCantLetras2.Text = CantidaLetras
                    rpt.xrlComentario2.Text = meComentario.EditValue

                    rpt.ShowPrintMarginsWarning = False
                    If dtParametros.Rows(0).Item("VistaPreviaFacturar") Then
                        rpt.ShowPreviewDialog()
                    Else
                        rpt.PrinterName = SiEsNulo(entComprobantes.NombreImpresor, "")
                        rpt.Print()
                    End If
                End With
            Case 7 'factura de exportación
                Dim Template = Application.StartupPath & "\Plantillas\feNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFactura
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""
                With FacturaHeader
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumRemision.Text = teNotaRemision.EditValue
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = leMunicipio.Text
                    rpt.xrlDepto.Text = leDepartamento.Text
                    rpt.xrlFormaPago.Text = leFormaPago.Text
                    rpt.xrlVendedor.Text = leVendedor.Text
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")

                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    Dim CantidaLetras As String = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlCantLetras.Text = CantidaLetras
                    rpt.xrlComentario.Text = meComentario.EditValue
                    rpt.ShowPrintMarginsWarning = False
                    If dtParametros.Rows(0).Item("VistaPreviaFacturar") Then
                        rpt.ShowPreviewDialog()
                    Else
                        rpt.PrinterName = SiEsNulo(entComprobantes.NombreImpresor, "")
                        rpt.Print()
                    End If
                End With
            Case 8 'nota de crédito
                Dim dtAfectaNc As DataTable = bl.fac_ObtenerAfectaNC(FacturaHeader.IdComprobante) ' para saber a que documentos afecto
                Dim Template = Application.StartupPath & "\Plantillas\ncNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)

                For i = 0 To dt.Rows.Count - 1
                    dt.Rows(i)("Cantidad") = 0 - dt.Rows(i)("Cantidad")
                    dt.Rows(i)("PrecioUnitario") = 0 - dt.Rows(i)("PrecioUnitario")
                    dt.Rows(i)("VentaExenta") = 0 - dt.Rows(i)("VentaExenta")
                    dt.Rows(i)("VentaNoSujeta") = 0 - dt.Rows(i)("VentaNoSujeta")
                    dt.Rows(i)("VentaNeta") = 0 - dt.Rows(i)("VentaNeta")
                    dt.Rows(i)("VentaAfecta") = 0 - dt.Rows(i)("VentaAfecta")
                Next
                Dim DocumentosAfectados As String = ""
                For j = 0 To dtAfectaNc.Rows.Count - 1
                    DocumentosAfectados += dtAfectaNc.Rows(j)("NumComprobanteVenta") + ", "
                Next

                rpt.DataSource = dt
                rpt.DataMember = ""

                With FacturaHeader
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumRemision.Text = teNotaRemision.EditValue
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlGiro.Text = teGiro.EditValue
                    rpt.xrlDireccion.Text = teDireccion.EditValue
                    rpt.xrlMunic.Text = leMunicipio.Text
                    rpt.xrlVendedor.Text = leVendedor.Text
                    rpt.xrlFormaPago.Text = leFormaPago.Text
                    rpt.xrlTelefono.Text = ""
                    rpt.xrlDocAfectados.Text = DocumentosAfectados
                    rpt.xrlVentaAfecta.Text = Format(0 - FacturaHeader.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(0 - FacturaHeader.TotalIva, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(0 - FacturaHeader.TotalIva + 0 - FacturaHeader.TotalAfecto, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(0 - FacturaHeader.TotalImpuesto1, "##,###.00")
                    rpt.xrlVentaExenta.Text = Format(0 - FacturaHeader.TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(0 - FacturaHeader.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", 0 - FacturaHeader.TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                    rpt.xrlCantLetras.Text = Num2Text(Int(0 - FacturaHeader.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = meComentario.EditValue
                    If dtParametros.Rows(0).Item("VistaPreviaFacturar") Then
                        rpt.ShowPreviewDialog()
                    Else
                        rpt.PrinterName = SiEsNulo(entComprobantes.NombreImpresor, "")
                        rpt.Print()
                    End If
                End With
            Case 9 'nota de débito
                Dim Template = Application.StartupPath & "\Plantillas\ndNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""

                With FacturaHeader
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumRemision.Text = teNotaRemision.EditValue
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = teGiro.EditValue
                    rpt.xrlDireccion.Text = teDireccion.EditValue
                    rpt.xrlMunic.Text = leMunicipio.Text
                    rpt.xrlVendedor.Text = leVendedor.Text
                    rpt.xrlFormaPago.Text = leFormaPago.Text
                    rpt.xrlTelefono.Text = ""

                    rpt.xrlVentaAfecta.Text = Format(FacturaHeader.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(FacturaHeader.TotalIva, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(FacturaHeader.TotalIva + FacturaHeader.TotalAfecto, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(FacturaHeader.TotalImpuesto1, "##,###.00")
                    rpt.xrlVentaExenta.Text = Format(FacturaHeader.TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(FacturaHeader.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", FacturaHeader.TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                    rpt.xrlCantLetras.Text = Num2Text(Int(FacturaHeader.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = meComentario.EditValue
                    If dtParametros.Rows(0).Item("VistaPreviaFacturar") Then
                        rpt.ShowPreviewDialog()
                    Else
                        rpt.PrinterName = SiEsNulo(entComprobantes.NombreImpresor, "")
                        rpt.Print()
                    End If
                End With
            Case 10 'tickette de caja se imprime a puro código
                ImprimirTikect()
            Case 19 'NOTA DE REMISION O NOTA DE ENVÍO
                Dim Template = Application.StartupPath & "\Plantillas\neNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptOrdenEnvio
                Dim entSucursalRecibe = objTablas.adm_SucursalesSelectByPK(leSucursalEnvio.EditValue)
                Dim entSucursalEnvia = objTablas.adm_SucursalesSelectByPK(piIdSucursalUsuario)
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""
                With FacturaHeader
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlDia.Text = CDate(.Fecha).Day.ToString()
                    rpt.xrlMes.Text = ObtieneMesString(CDate(.Fecha).Month)
                    rpt.xrlAnio.Text = CDate(.Fecha).Year.ToString()
                    rpt.xrlTotal2.Text = Format(.TotalComprobante, "###,##0.00")
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlSucursalEnvia.Text = entSucursalEnvia.Nombre
                    rpt.xrlSucursalRecibe.Text = entSucursalRecibe.Nombre
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro

                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlFormaPago.Text = leFormaPago.Text
                    rpt.xrlVendedor.Text = leVendedor.Text
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(.TotalIva, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    rpt.xrlCantLetras.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = meComentario.EditValue
                    If dtParametros.Rows(0).Item("VistaPreviaFacturar") Then
                        rpt.ShowPreviewDialog()
                    Else
                        rpt.PrinterName = SiEsNulo(entComprobantes.NombreImpresor, "")
                        rpt.Print()
                    End If
                End With
            Case 18
                Dim Template = Application.StartupPath & "\Plantillas\neNexus" & NumFormato & ".repx"

                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""
                With FacturaHeader
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNumRemision.Text = .Telefono
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = teGiro.EditValue
                    rpt.xrlDireccion.Text = teDireccion.EditValue
                    rpt.xrlMunic.Text = leMunicipio.Text
                    rpt.xrlDepto.Text = leDepartamento.Text
                    rpt.xrlVendedor.Text = leVendedor.Text
                    rpt.xrlFormaPago.Text = leFormaPago.Text
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlFormaPago.Text = leFormaPago.Text
                    rpt.xrlDiasCredito.Text = .DiasCredito
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(.TotalIva, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlCESC.Text = Format(.TotalImpuesto2, "##,###.00")
                    rpt.xrlSubTotal.Text = Format(.TotalIva + FacturaHeader.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")

                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPrintMarginsWarning = False
                    rpt.ShowPreviewDialog()
                End With
            Case Else
                MsgBox("NO EXISTE CONFIGURACION PARA IMPRESION DE ESTE TIPO DE DOCUMENTO", MsgBoxStyle.Critical, "ALERTA")
        End Select

        Return True
    End Function

    Private Sub sePjeDescuento_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles sePjeDescuento.LostFocus
        If gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
        End If
    End Sub

    Private Sub leTipoComprobante_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leTipoComprobante.EditValueChanged

        bl.fac_TipoAplicacionComprobante(leTipoComprobante.EditValue, DocumentoDetallaIva, TipoAplicacion, LimiteLineas)
        If leTipoComprobante.EditValue = 7 Then
            leFormaPago.EditValue = 4
            leTipoImpuesto.EditValue = 3 'Tasa Cero, Solicitado por Edwin y Don Elmer 18-02-2020, mediante correo.
        End If
        leTipoImpuesto.Enabled = Not leTipoComprobante.EditValue = 7

        If EntUsuario.CambiarPrecios = True Then
            ceDevolucion.Visible = leTipoComprobante.EditValue = 10 Or leTipoComprobante.EditValue = 6
        Else
            ceDevolucion.Visible = False
        End If
        If DocumentoDetallaIva Then
            lblNRC_DUI.Text = "NRC:"
        Else
            lblNRC_DUI.Text = "DUI:"
            teNRC.Properties.Mask.EditMask = "00000000-0"
        End If

        teLimiteLineas.EditValue = LimiteLineas

        ceDevolucion.Checked = leTipoComprobante.EditValue = 8

        If leTipoComprobante.EditValue = 6 Then
            ceDevolucion.Text = "Marcar como Factura de Devolución"
        Else
            ceDevolucion.Text = "Marcar como Ticket de Devolución"
        End If

        'no se mostrara si no es nota de remision
        lblSucursalEnvio.Visible = leTipoComprobante.EditValue = 19
        leSucursalEnvio.Visible = leTipoComprobante.EditValue = 19
        leSucursalEnvio.EditValue = piIdSucursalUsuario
        ' se Carga desde aqui para la imprecion del comprobante , asi no se llama varias veces esta entidad 
        entComprobantes = objTablas.adm_TiposComprobanteSelectByPK(leTipoComprobante.EditValue)
        NumeroComprobante()
    End Sub

    Private Sub ceDevolucion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ceDevolucion.CheckedChanged, ceRetencionPercepcion.CheckedChanged
        If ceDevolucion.Checked = True Then
            frmObtienePassword.Text = "AUTORIZACIÓN PARA GENERAR DEVOLUCION"
            frmObtienePassword.Usuario = objMenu.User
            frmObtienePassword.TipoAcceso = 1
            frmObtienePassword.ShowDialog()
            If frmObtienePassword.Acceso = False Then
                ceDevolucion.Checked = False
                If leTipoComprobante.EditValue = 8 Then
                    leTipoComprobante.EditValue = 5
                End If
                Exit Sub
            End If
        End If
        TipoAplicacion = IIf(ceDevolucion.EditValue, -1, 1)
        CalcularTotales()
    End Sub

    Private Sub ImprimirTikect()
        Dim strLinea As String = ""
        Dim strDescripcion As String = ""

        Dim str As String = "", Ancho As Integer = 0, s As String = ""
        Dim dt As DataTable = bl.fac_ObtenerDetalleDocumento("fac_ventasDetalle", FacturaHeader.IdComprobante)

        str = Chr(27) & Chr(99) & Chr(48) & Chr(3) 'para que imprima en la cinta de auditoría
        str += Chr(27) + "z" + Chr(1) 'activación del modo de impresión en auditoría
        str += Chr(27) & "p" & Chr(0) 'para abrir la gaveta del cash drawer
        str += DatosEncabezado

        str += "CONTROL No. " & Trim(FacturaHeader.IdComprobante)
        s = "TICKET No. " & FacturaHeader.Numero

        Ancho = 40 - Len(s)
        str += s.PadLeft(Ancho, " ") & Chr(10)

        str += "FECHA: " & Format(FacturaHeader.Fecha, "dd/MM/yyyy")
        s = "HORA: " & Now.ToString("HH:mm:ss")

        str += s.PadLeft(23, " ") & Chr(10)
        str += "CAJA No. " & FacturaHeader.IdPunto & Chr(10)
        s = "CLIENTE: " & teNombre.Text + Space(30)
        str += s.Substring(0, 39) & Chr(10)
        s = "VENDEDOR: " & leVendedor.Text + Space(30)
        str += s.Substring(0, 39) & Chr(10) & Chr(10)
        If FacturaHeader.EsDevolucion Then
            str += "    TICKET DE DEVOLUCION" & Chr(10) & Chr(10)
        End If
        Dim TotalExento As Decimal = 0.0, TotalAfecto As Decimal = 0.0, Vta As String = "G"
        For Each Fila As DataRow In dt.Rows
            TotalExento += CDec(Fila.Item("VentaExenta"))
            TotalAfecto += CDec(Fila.Item("VentaAfecta"))
            Vta = " G"
            If CDec(Fila.Item("VentaExenta")) > 0.0 Then
                Vta = " E"
            End If
            ''.Substring(1, 30)
            ''strPrueba += (Fila.Item("Descripcion").ToString()).Substring(0, 25) + Vta & Chr(10)

            '' str += Fila.Item("Descripcion").ToString() + Vta & Chr(10)
            strLinea = ""
            strDescripcion = (Fila.Item("Descripcion").ToString())

            If Len(strDescripcion) > 16 Then
                '' str += (Fila.Item("Descripcion").ToString()).Substring(0, 13) + Vta & " "
                strLinea += (Fila.Item("Descripcion").ToString()).Substring(0, 16) & " "
            Else
                '' str += (Fila.Item("Descripcion").ToString()) + Vta & " "
                strLinea += (Fila.Item("Descripcion").ToString()) & " "
            End If

            ''& Chr(10)
            s = Format(Fila.Item("Cantidad"), "#,##0.00") + "X" + Format(Fila.Item("PrecioUnitario"), "#,##0.00") & " "
            ''str += s
            strLinea += s
            If CDec(Fila.Item("VentaExenta")) > 0.0 Then
                s = Format(Fila.Item("VentaExenta"), "##,##0.00") & Vta
            Else
                s = Format(Fila.Item("VentaAfecta"), "##,##0.00") & Vta
            End If
            Ancho = 40 - Len(strLinea)
            If Ancho > 0 And Ancho > Len(s) Then
                strLinea += s.PadLeft(Ancho, " ")
            Else
                strLinea = strLinea & " " & s
            End If

            str += strLinea
            ''str += s.PadLeft(Ancho, " ")
            ''str += s
            str += Chr(10)
        Next
        str += Chr(10)
        s = Format(TotalExento, "$##,##0.00")
        str += "Sub-Total Exento:"
        str += s.PadLeft(23, " ") & Chr(10)

        s = Format(0, "$##,##0.00")
        str += "Sub-Total No Sujeto:"
        str += s.PadLeft(20, " ") & Chr(10)

        s = Format(TotalAfecto, "$##,##0.00")
        str += "Sub-Total Gravado:"
        str += s.PadLeft(22, " ") & Chr(10)
        str += "TOTAL:"
        s = Format(TotalExento + TotalAfecto, "$##,##0.00")
        str += s.PadLeft(34, " ") & Chr(10)

        str += "EFECTIVO RECIBIDO:"   '18 caracteres
        s = Format(FacturaHeader.TotalPagado, "$##,##0.00")
        str += s.PadLeft(22, " ") & Chr(10)

        str += "VUELTO A ENTREGAR:"   '18 caracteres
        s = Format(FacturaHeader.TotalPagado - TotalExento - TotalAfecto, "$##,##0.00")
        str += s.PadLeft(22, " ") & Chr(10) & Chr(10)

        str += "G= VENTA GRAVADA, E= VENTA EXENTA" & Chr(10)
        str += "N= VENTA NO SUJETA" & Chr(10) & Chr(10)
        If FacturaHeader.TotalComprobante > 200 Or FacturaHeader.EsDevolucion Then 'SI LA VENTA ES MAYOR A $200.00 O ES UNA DEVOLUCION
            If FacturaHeader.EsDevolucion Then
                str += "TICKETE DE DEVOLUCION. FAVOR FIRMAR" & Chr(10)
            Else
                str += "VENTA MAYOR O IGUAL A $200. FAVOR FIRMAR" & Chr(10)
            End If
            str += "NIT: " & teNit.EditValue & Chr(10)
            str += "DUI: " & teNRC.EditValue & Chr(10)
            str += "Nombre: " & teNombre.EditValue & Chr(10) & Chr(10)
            str += "Firma: _____________________________" & Chr(10)
        End If
        str += Chr(10) & Chr(10)
        str += DatosPie
        str += Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10)
        str += Chr(27) + "z" + Chr(0) 'para desactivar el modo de impresión en la cinta
        str += Chr(27) + Chr(109) 'para cortar el ticket con la cuchilla 
        str += Chr(27) + Chr(109) 'para cortar el ticket con la cuchilla 
        RawPrinterHelper.SendStringToPrinter(entidadTicket.NombreImpresor, str)
    End Sub

    Private Sub sbObtenerPedido_Click(ByVal sender As Object, ByVal e As EventArgs) Handles sbObtenerPedido.Click
        If teNumPedido.Properties.ReadOnly = True Then
            Exit Sub
        End If
        Dim IdOrden As Integer = bl.fac_ObtenerIdPedidoPorNumero(teNumPedido.EditValue)
        If IdOrden = 0 Then
            MsgBox("No se encontró ninguna orden o pedido con ése número", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        Dim entPed As fac_Pedidos = objTablas.fac_PedidosSelectByPK(IdOrden)
        If entPed.Reservado Then
            MsgBox("El pedido seleccionado es solicitud de mercadería" + Chr(13) + "no aplica para facturación", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        If entPed.Facturado Then
            MsgBox("El pedido ya fue facturado", MsgBoxStyle.Critical, "Error")
            teNumPedido.EditValue = ""
            Exit Sub
        End If
        With entPed
            beCodCliente.EditValue = .IdCliente
            teNombre.EditValue = .Nombre
            teNRC.EditValue = .Nrc
            teNit.EditValue = .Nit
            teDireccion.EditValue = .Direccion
            leTipoComprobante.EditValue = .IdTipoComprobante
            teGiro.EditValue = .Giro
            leBodega.EditValue = .IdBodega
            leVendedor.EditValue = .IdVendedor
            leFormaPago.EditValue = .IdFormaPago
            meComentario.EditValue = .Notas & "PEDIDO CREADO POR: " + .CreadoPor
        End With

        beIdCliente_Validated(beCodCliente, New EventArgs)

        sbImportarBoletos.DataSource = bl.fac_ObtenerPedidoDetalle(IdOrden, pnIVA, DocumentoDetallaIva, TipoVenta)
        teCESC.EditValue = 0.0
        CalcularTotales()
        teDescuento.EditValue = 0.0
    End Sub

    Private Sub LlenarFormaPago()
        FacturaDetallePago = New List(Of fac_VentasDetallePago)
        For i = 0 To _dtDetallePago.Rows.Count - 1
            If SiEsNulo(_dtDetallePago.Rows(i).Item("Total"), 0.0) > 0.0 Then
                Dim entDetalle As New fac_VentasDetallePago
                With entDetalle
                    .IdComprobVenta = 0
                    .IdFormaPago = _dtDetallePago.Rows(i).Item("IdFormaPago")
                    .Total = SiEsNulo(_dtDetallePago.Rows(i).Item("Total"), 0)
                End With
                FacturaDetallePago.Add(entDetalle)
            End If
        Next
    End Sub
    Private Sub NumeroComprobante()
        Dim NumeroComprobante As Integer = bl.GetObtieneCorrelativoFacturacion(cboPuntoVenta.EditValue, leTipoComprobante.EditValue)
        teNumero.EditValue = NumeroComprobante.ToString.PadLeft(6, "0")
        Dim entTipos As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(leTipoComprobante.EditValue)

        If dtParametros.Rows(0).Item("UtilizarFormularioUnico") Then
            Dim NumeroUnico As Integer = bl.GetObtieneCorrelativoFacturacionUnico(cboPuntoVenta.EditValue, piIdSucursalUsuario)
            teNumeroUnico.EditValue = IIf(entTipos.FormularioUnico, NumeroUnico.ToString.PadLeft(6, "0"), "")
        Else
            teNumeroUnico.EditValue = ""
        End If
    End Sub

    Private Sub leFormaPago_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles leFormaPago.EditValueChanged
        ValidaSaldoCliente()
    End Sub

    Private Sub sbEditarCESC_Click(sender As Object, e As EventArgs) Handles sbEditarCESC.Click
        Dim CescAct As String = teCESC.EditValue.ToString()
        CescAct = InputBox("VALOR DEL CESC:", "CESC", CescAct)
        Try
            If CDec(CescAct) <> teCESC.EditValue Then
                teCESC.EditValue = CDec(CescAct)
                CalcularTotales()
                CescEditado = True
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnCentroCosto_ButtonClick(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles btnCentroCosto.ButtonClick
        Dim IdCentro As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdCentro")), "")
        Dim frmSeleccionaCC As New frmDetallarCentroCosto
        Me.AddOwnedForm(frmSeleccionaCC)
        frmSeleccionaCC.IdCentro = IdCentro
        frmSeleccionaCC.ShowDialog()

        IdCentro = SiEsNulo(frmSeleccionaCC.IdCentro, "")
        gv.SetFocusedRowCellValue("IdCentro", IdCentro)
        'frmSeleccionaCC.Dispose()

    End Sub

    Private Sub gv_ShownEditor(sender As Object, e As EventArgs) Handles gv.ShownEditor
        'Try
        '    Dim view As GridView = CType(sender, GridView)
        '    If View.FocusedColumn.FieldName = "IdCentro" Then
        '        Dim ed As ButtonEdit = CType(View.ActiveEditor, ButtonEdit)
        '        ed.Properties.Buttons(0).Caption = View.GetFocusedDisplayText()
        '    End If
        'Catch ex As Exception

        'End Try

    End Sub

    Private Sub gv_CustomDrawCell(sender As Object, e As DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs) Handles gv.CustomDrawCell



        If e.Column.FieldName = "IdCentro" Then
            'Dim cellInfo As GridCellInfo
            'Dim buttonEditViewInfo As DevExpress.XtraEditors.ViewInfo.ButtonEditViewInfo
            'Dim sValue As String = "-"
            'Dim entCC As con_CentrosCosto
            'Try
            '    sValue = SiEsNulo(e.CellValue.ToString, "")

            '    cellInfo = CType(e.Cell, GridCellInfo)
            '    entCC = objTablas.con_CentrosCostoSelectByPK(sValue)
            '    buttonEditViewInfo = CType(cellInfo.ViewInfo, DevExpress.XtraEditors.ViewInfo.ButtonEditViewInfo)
            '    buttonEditViewInfo.RightButtons(0).Button.Caption = entCC.Nombre ' sValue
            'Catch ex As Exception

            'End Try
            'Dim entCC As con_CentrosCosto
            'Dim cellInfo As GridCellInfo = CType(e.Cell, GridCellInfo)
            'Dim buttonEditViewInfo As ButtonEditViewInfo = CType(cellInfo.ViewInfo, ButtonEditViewInfo)
            'Dim CC As String = e.CellValue
            ''entCC = objTablas.con_CentrosCostoSelectByPK(e.CellValue)
            'buttonEditViewInfo.RightButtons(0).Button.Caption = CC
            '  ButtonEdit ed = (ButtonEdit)view.ActiveEditor;
            'ButtonEditViewInfo editInfo = (ButtonEditViewInfo)((DevExpress.XtraGrid.Views.Grid.ViewInfo.GridCellInfo)e.Cell).ViewInfo;
            'editInfo.RightButtons[0].Button.Caption = e.DisplayText;

            'Try
            '    Dim editInfo As ButtonEditViewInfo = CType((CType(e.Cell, DevExpress.XtraGrid.Views.Grid.ViewInfo.GridCellInfo)).ViewInfo, ButtonEditViewInfo)
            '    editInfo.RightButtons(0).Button.Caption = e.DisplayText
            'Catch ex As Exception

            'End Try


        End If
    End Sub
    Private Sub gv_DoubleClick(sender As Object, e As EventArgs) Handles gv.DoubleClick
        Dim IdCentro As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdCentro")), "")
        Dim frmSeleccionaCC As New frmDetallarCentroCosto
        Me.AddOwnedForm(frmSeleccionaCC)
        frmSeleccionaCC.IdCentro = IdCentro
        frmSeleccionaCC.ShowDialog()

        IdCentro = SiEsNulo(frmSeleccionaCC.IdCentro, "")
        gv.SetFocusedRowCellValue("IdCentro", IdCentro)
        frmSeleccionaCC.Dispose()
    End Sub

    Private Sub cboPuntoVenta_EditValueChanged(sender As Object, e As EventArgs) Handles cboPuntoVenta.EditValueChanged
        deFecha.EditValue = SiEsNulo(objFunciones.GetFechaContableCaja(piIdSucursalUsuario, cboPuntoVenta.EditValue), Today)
    End Sub


    Private Sub sbImportar_Click(sender As Object, e As EventArgs) Handles sbImportar.Click
        Dim frmObtenerfacturacionNR As New fac_frmObtenerFacturacionBoletos
        Me.AddOwnedForm(frmObtenerfacturacionNR)

        frmObtenerfacturacionNR.IdCliente = beCodCliente.EditValue
        frmObtenerfacturacionNR.ShowDialog()


        If Not frmObtenerfacturacionNR.Aceptar Then
            frmObtenerfacturacionNR.Dispose()
            Exit Sub
        End If

        'lFacturaPedio = True
        'RefrescaGrid()
        frmObtenerfacturacionNR.Dispose()
        'CalcularTotales()
    End Sub

    Private Sub sbMarcarBoletos_Click(sender As Object, e As EventArgs) Handles sbMarcarBoletos.Click
        Dim frmObtenerfacturacionNR As New fac_frmMarcacionBoletos
        Me.AddOwnedForm(frmObtenerfacturacionNR)

        frmObtenerfacturacionNR.IdCliente = beCodCliente.EditValue
        frmObtenerfacturacionNR.ShowDialog()


        If Not frmObtenerfacturacionNR.Aceptar Then
            frmObtenerfacturacionNR.Dispose()
            Exit Sub
        End If

        'lFacturaPedio = True
        'RefrescaGrid()
        frmObtenerfacturacionNR.Dispose()
        'CalcularTotales()
    End Sub
End Class