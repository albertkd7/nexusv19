﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class fac_frmAplicacionNotaCredito
    Dim dtDetalle As New DataTable
    Dim bl As New CuentasPCBLL(g_ConnectionString)
    Dim NotaDetalle As List(Of fac_NotasCredito)

    Private _IdComprob As Integer

    Public Property IdComprob() As Integer
        Get
            Return _IdComprob
        End Get
        Set(ByVal value As Integer)
            _IdComprob = value
        End Set
    End Property

    Private _NumeroNota As System.String
    Public Property NumeroNota() As System.String
        Get
            Return _NumeroNota
        End Get
        Set(ByVal value As System.String)
            _NumeroNota = value
        End Set
    End Property

    Private _FechaNota As System.DateTime
    Public Property FechaNota() As System.DateTime
        Get
            Return _FechaNota
        End Get
        Set(ByVal value As System.DateTime)
            _FechaNota = value
        End Set
    End Property

    Private _IdCliente As System.String
    Public Property IdCliente() As System.String
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As System.String)
            _IdCliente = value
        End Set
    End Property

    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Private _MontoAbonado As System.Decimal
    Public Property MontoAbonado() As System.Decimal
        Get
            Return _MontoAbonado
        End Get
        Set(ByVal value As System.Decimal)
            _MontoAbonado = value
        End Set
    End Property


    Private Sub sbGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles sbGuardar.Click
        If teMontoAbonar.EditValue <> Me.MontoAbonar.SummaryItem.SummaryValue() Then
            MsgBox("No puede aplicar el abono, hay saldo pendiente de liquidar", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If MsgBox("Está seguro(a) de aplicar ésta devolución?" & Chr(13) & "Ya no podrá editar los datos", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        LlenarEntidades()
        Dim msjOk As String = bl.cpc_AplicarNotaCredito(NotaDetalle, deFecha.EditValue)
        If msjOk = "" Then
            MsgBox("La devolución ha sido aplicado con éxito", MsgBoxStyle.Information, "Nota")
        Else
            MsgBox("NO FUE POSIBLE APLICAR LA DEVOLUCIÓN" + Chr(13) + msjOk, MsgBoxStyle.Critical, "Nota")
        End If
        Dispose()
    End Sub

    Private Sub LlenarEntidades()
        NotaDetalle = New List(Of fac_NotasCredito)
        For i = 0 To gvNC.DataRowCount - 1
            If gvNC.GetRowCellValue(i, "MontoAbonar") > 0 Then  'inserto solo los que aplicaron abono
                Dim entDetalle As New fac_NotasCredito
                With entDetalle
                    .IdComprobante = teIdComprobante.EditValue
                    .IdDetalle = i + 1
                    .IdComprobVenta = gvNC.GetRowCellValue(i, "IdComprobante")
                    .MontoAbonado = gvNC.GetRowCellValue(i, "MontoAbonar")
                    .SaldoComprobante = gvNC.GetRowCellValue(i, "Saldo")
                    .NumComprobanteVenta = gvNC.GetRowCellValue(i, "Numero")
                End With
                NotaDetalle.Add(entDetalle)
            End If
        Next
    End Sub
    Private Sub fac_frmAplicacionNotaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '' gcNC.DataSource = bl.GetSaldosFacturas(beCliente.EditValue, deFecha.EditValue, deFecha.EditValue)
        Dim msj As String = ""
        teNumeroComprobante.EditValue = NumeroNota
        teIdComprobante.EditValue = IdComprob
        deFecha.EditValue = FechaNota
        beCliente.EditValue = IdCliente
        teNombre.EditValue = Nombre
        teMontoAbonar.EditValue = MontoAbonado
        gcNC.DataSource = bl.GetSaldosFacturas(IdCliente, FechaNota, FechaNota, piIdSucursalUsuario)
    End Sub


    Private Sub riteMonto_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles riteMonto.Validating
        If gvNC.EditingValue > gvNC.GetRowCellValue(gvNC.FocusedRowHandle, "Saldo") Then
            gvNC.EditingValue = gvNC.GetRowCellValue(gvNC.FocusedRowHandle, "Saldo")
        End If
        CalculaDiferencia()
    End Sub

    Private Sub CalculaDiferencia()
        gvNC.UpdateTotalSummary()
        'teDiferencia.EditValue = txtMontoAbonar.EditValue - MontoAbonar.SummaryItem.SummaryValue
    End Sub

    Private Sub fac_frmAplicacionNotaCredito_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        e.Cancel = True
        MsgBox("No puede salir si terminar el proceso completo de devolución", MsgBoxStyle.Information, "Nota")
    End Sub
End Class