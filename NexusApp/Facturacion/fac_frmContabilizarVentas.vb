﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class fac_frmContabilizarVentas
    Dim bl As New FacturaBLL(g_ConnectionString), blConta As New ContabilidadBLL(g_ConnectionString)
    'Dim myBL As New ComprasBLL(g_ConnectionString)
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    
    Private Sub facContabilizarVentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDet, objMenu.User, "")
        objCombos.conTiposPartida(leTipoPartida)
        objCombos.conTiposPartida(leTipo)
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        gc.DataSource = blConta.con_ObtenerPeriodoContabilizado(4, piIdSucursalUsuario, objMenu.User)
    End Sub
    Private Sub sbContabilizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbContabilizar.Click
        If deDesde.DateTime.Month <> deHasta.DateTime.Month Then
            MsgBox("Los meses deben ser iguales", MsgBoxStyle.Exclamation, "Nota")
            Return
        End If

        If deDesde.EditValue > deHasta.EditValue Then
            MsgBox("La fecha inicial no puede ser mayor a la fecha final", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(deDesde.EditValue)
        If Not EsOk Then
            MsgBox("La fecha inicial está fuera del período permitido", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If blConta.con_ValidaContabilizacion(leSucursal.EditValue, 4, deDesde.EditValue, deHasta.EditValue) > 0 Then
            MsgBox("Ya existen documentos de venta contabilizados en este período", MsgBoxStyle.Critical, "Error de Usuario")
            Exit Sub
        End If

        If MsgBox("Está seguro(a) de contabilizar estas ventas?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        Dim msj As String = bl.ContabilizarVentas(deDesde.DateTime, deHasta.DateTime, leTipoPartida.EditValue, objMenu.User, meConcepto.EditValue, leSucursal.EditValue)

        If msj = "Ok" Then
            MsgBox("La contabilización se ha realizado con éxito", MsgBoxStyle.Information, "Nota")
        Else
            MsgBox("La contabilización NO se pudo realizar" + Chr(13) + msj, MsgBoxStyle.Critical, "Error de datos")
        End If
        gc.DataSource = blConta.con_ObtenerPeriodoContabilizado(4, piIdSucursalUsuario, objMenu.User)

    End Sub

    Private Sub sbReverir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbReverir.Click
        If Not AllowDelete Then
            MsgBox("No le está permitido eliminar información" + Chr(13) + "Verifique sus permisos con el administrador del sistema", MsgBoxStyle.Exclamation, Me.Text)
            Exit Sub
        End If

        Dim Desde As Date = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "DesdeFecha"), Today)
        Dim Hasta As Date = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "HastaFecha"), Today)
        Dim TipoPartida As String = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipoPartida"), "")

        If MsgBox("Está seguro(a) de revertir el período contabilizado de: " + Chr(13) + Desde + " al " + Hasta + " de tipo de partida: " + TipoPartida + " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        If MsgBox("Confirme Nuevamente.", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(Desde)
        If Not EsOk Then
            MsgBox("Fecha de la partida corresponde a un período ya cerrado", MsgBoxStyle.Critical, "Imposible eliminar")
            Exit Sub
        End If

        Dim msj As Integer = blConta.con_EliminaPartidas(4, Desde, Hasta, TipoPartida, SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdSucursal"), 0))

        If msj >= 1 Then
            MsgBox("La eliminación se ha realizado con éxito", 64, "Nota")
        Else
            MsgBox("La eliminación NO se pudo realizar", MsgBoxStyle.Critical, "Error de base de datos")
        End If
        gc.DataSource = blConta.con_ObtenerPeriodoContabilizado(4, piIdSucursalUsuario, objMenu.User)
    End Sub

End Class
