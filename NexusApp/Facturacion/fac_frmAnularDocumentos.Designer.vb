﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmAnularDocumentos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
        Me.teMotivo = New DevExpress.XtraEditors.TextEdit
        Me.seTotal = New DevExpress.XtraEditors.SpinEdit
        Me.deFecha = New DevExpress.XtraEditors.DateEdit
        Me.sbObtener = New DevExpress.XtraEditors.SimpleButton
        Me.sbAnular = New DevExpress.XtraEditors.SimpleButton
        Me.teNombre = New DevExpress.XtraEditors.TextEdit
        Me.teNumero = New DevExpress.XtraEditors.TextEdit
        Me.teSerie = New DevExpress.XtraEditors.TextEdit
        Me.leTipo = New DevExpress.XtraEditors.LookUpEdit
        Me.lePunto = New DevExpress.XtraEditors.LookUpEdit
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.teMotivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.teMotivo)
        Me.GroupControl1.Controls.Add(Me.seTotal)
        Me.GroupControl1.Controls.Add(Me.deFecha)
        Me.GroupControl1.Controls.Add(Me.sbObtener)
        Me.GroupControl1.Controls.Add(Me.sbAnular)
        Me.GroupControl1.Controls.Add(Me.teNombre)
        Me.GroupControl1.Controls.Add(Me.teNumero)
        Me.GroupControl1.Controls.Add(Me.teSerie)
        Me.GroupControl1.Controls.Add(Me.leTipo)
        Me.GroupControl1.Controls.Add(Me.lePunto)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(680, 345)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Datos del documento que se desea anular"
        '
        'teMotivo
        '
        Me.teMotivo.Location = New System.Drawing.Point(140, 220)
        Me.teMotivo.Name = "teMotivo"
        Me.teMotivo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teMotivo.Size = New System.Drawing.Size(516, 20)
        Me.teMotivo.TabIndex = 8
        '
        'seTotal
        '
        Me.seTotal.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seTotal.Location = New System.Drawing.Point(140, 185)
        Me.seTotal.Name = "seTotal"
        Me.seTotal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seTotal.Properties.Mask.EditMask = "c"
        Me.seTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seTotal.Properties.ReadOnly = True
        Me.seTotal.Size = New System.Drawing.Size(100, 20)
        Me.seTotal.TabIndex = 7
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.Location = New System.Drawing.Point(140, 143)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.ReadOnly = True
        Me.deFecha.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 5
        '
        'sbObtener
        '
        Me.sbObtener.Location = New System.Drawing.Point(246, 114)
        Me.sbObtener.Name = "sbObtener"
        Me.sbObtener.Size = New System.Drawing.Size(75, 23)
        Me.sbObtener.TabIndex = 5
        Me.sbObtener.Text = "Obtener..."
        '
        'sbAnular
        '
        Me.sbAnular.Location = New System.Drawing.Point(140, 246)
        Me.sbAnular.Name = "sbAnular"
        Me.sbAnular.Size = New System.Drawing.Size(150, 32)
        Me.sbAnular.TabIndex = 9
        Me.sbAnular.Text = "&Anular documento..."
        '
        'teNombre
        '
        Me.teNombre.Location = New System.Drawing.Point(140, 164)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.ReadOnly = True
        Me.teNombre.Size = New System.Drawing.Size(516, 20)
        Me.teNombre.TabIndex = 6
        '
        'teNumero
        '
        Me.teNumero.Location = New System.Drawing.Point(140, 115)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(100, 20)
        Me.teNumero.TabIndex = 4
        '
        'teSerie
        '
        Me.teSerie.Location = New System.Drawing.Point(140, 94)
        Me.teSerie.Name = "teSerie"
        Me.teSerie.Size = New System.Drawing.Size(100, 20)
        Me.teSerie.TabIndex = 3
        '
        'leTipo
        '
        Me.leTipo.Location = New System.Drawing.Point(140, 71)
        Me.leTipo.Name = "leTipo"
        Me.leTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipo.Size = New System.Drawing.Size(315, 20)
        Me.leTipo.TabIndex = 2
        '
        'lePunto
        '
        Me.lePunto.Location = New System.Drawing.Point(140, 50)
        Me.lePunto.Name = "lePunto"
        Me.lePunto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePunto.Size = New System.Drawing.Size(315, 20)
        Me.lePunto.TabIndex = 1
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(140, 29)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(315, 20)
        Me.leSucursal.TabIndex = 0
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(26, 223)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(111, 13)
        Me.LabelControl9.TabIndex = 4
        Me.LabelControl9.Text = "Motivo de la Anulación:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(101, 188)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl8.TabIndex = 4
        Me.LabelControl8.Text = "TOTAL:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(71, 167)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl7.TabIndex = 4
        Me.LabelControl7.Text = "A Nombre de:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(104, 146)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl6.TabIndex = 4
        Me.LabelControl6.Text = "Fecha:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(96, 118)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl5.TabIndex = 4
        Me.LabelControl5.Text = "Número:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(109, 97)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl4.TabIndex = 3
        Me.LabelControl4.Text = "Serie:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(113, 74)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Tipo:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(59, 54)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Punto de Venta:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(93, 32)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Sucursal:"
        '
        'fac_frmAnularDocumentos
        '
        Me.ClientSize = New System.Drawing.Size(680, 370)
        Me.Controls.Add(Me.GroupControl1)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmAnularDocumentos"
        Me.OptionId = "002005"
        Me.Text = "Anulación de documentos fiscales"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.teMotivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leTipo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lePunto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbAnular As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbObtener As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents seTotal As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teMotivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl

End Class
