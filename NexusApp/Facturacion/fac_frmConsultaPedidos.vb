Imports NexusBLL
Public Class fac_frmConsultaPedidos
    Dim blCon As New FacturaBLL(g_ConnectionString)


    Private Sub fac_frmConsultaPedidos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gcConsultaPed.DataSource = New DataView(dtPedidos)
        deDesde.EditValue = Today
        deHasta.EditValue = Today
    End Sub

    Private _dtPedidos As DataTable
    Public Property dtPedidos() As DataTable
        Get
            Return _dtPedidos
        End Get
        Set(ByVal value As DataTable)
            _dtPedidos = value
        End Set
    End Property
    Private _IdPedido As System.Int32
    Public Property IdPedido() As System.Int32
        Get
            Return _IdPedido
        End Get
        Set(ByVal value As System.Int32)
            _IdPedido = value
        End Set
    End Property

    Private Sub sbConsultaPed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbConsultaPed.Click
        gcConsultaPed.DataSource = blCon.fac_ConsultaPedidos(piIdSucursalUsuario)
    End Sub

    Private Sub sbSalida_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbSalida.Click
        Me.Close()
    End Sub
    Private Sub fac_frmConsultaPedidos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub gvConsultaPed_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvConsultaPed.DoubleClick
        IdPedido = gvConsultaPed.GetRowCellValue(gvConsultaPed.FocusedRowHandle, "IdComprobante")
        Me.Close()
    End Sub

    Private Sub gvConsultaPed_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvConsultaPed.KeyDown
        If e.KeyCode = Keys.Enter Then
            IdPedido = gvConsultaPed.GetRowCellValue(gvConsultaPed.FocusedRowHandle, "IdComprobante")
            Me.Close()
        End If
    End Sub
End Class