Imports NexusELL.TableEntities
Imports NexusBLL
Imports System.IO
Public Class fac_frmMarcacionBoletos
    Dim dtDetalle As New DataTable
    'Dim bl As New CuentasPCBLL(g_ConnectionString)
    Dim blfac As New FacturaBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParametros As DataTable = blAdmon.ObtieneParametros()
    Dim FacturaHeader As fac_Ventas, FacturaDetalle As List(Of fac_VentasDetalle)
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim IdSucUser As Integer = SiEsNulo(EntUsuario.IdSucursal, 1)
    Dim bl As New FacturaBLL(g_ConnectionString), fd As New FuncionesBLL(g_ConnectionString)

    Private _IdCliente As System.String
    Public Property IdCliente() As System.String
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As System.String)
            _IdCliente = value
        End Set
    End Property

    Private _TotalAnticipos As System.Decimal
    Public Property TotalAnticipos() As System.Decimal
        Get
            Return _TotalAnticipos
        End Get
        Set(ByVal value As System.Decimal)
            _TotalAnticipos = value
        End Set
    End Property

    Private _TipoVenta As System.Int32
    Public Property TipoVenta() As System.Int32
        Get
            Return _TipoVenta
        End Get
        Set(ByVal value As System.Int32)
            _TipoVenta = value
        End Set
    End Property


    Private _Aceptar As Boolean = False

    Public Property Aceptar() As Boolean
        Get
            Return _Aceptar
        End Get
        Set(ByVal value As Boolean)
            _Aceptar = value
        End Set
    End Property

    Private _DocumentoDetallaIva As Boolean = False

    Public Property DocumentoDetallaIva() As Boolean
        Get
            Return _DocumentoDetallaIva
        End Get
        Set(ByVal value As Boolean)
            _DocumentoDetallaIva = value
        End Set
    End Property


    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        LlenarGrid()
        Aceptar = True
        Close()
    End Sub


    Private Sub sbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCancel.Click
        TotalAnticipos = 0.0
        Aceptar = False
        Close()
    End Sub


    Private Sub fac_frmMostrarCambio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        InicializaGrid()
        deFecha.EditValue = Today()
        teValorNoSujeto.EditValue = 0.0
        teExento.EditValue = 0.0
        teAfecto.EditValue = 0.0
    End Sub


    Private Sub LlenarGrid()

        Dim frmFacturacion As fac_frmFacturacion = Me.Owner
        For i = 0 To gvFP.RowCount - 1
            frmFacturacion.gv.AddNewRow()
            frmFacturacion.gv.SetRowCellValue(frmFacturacion.gv.FocusedRowHandle, "IdProducto", "01")
            frmFacturacion.gv.SetRowCellValue(frmFacturacion.gv.FocusedRowHandle, "Cantidad", 1)
            frmFacturacion.gv.SetRowCellValue(frmFacturacion.gv.FocusedRowHandle, "IdPrecio", 1)
            frmFacturacion.gv.SetRowCellValue(frmFacturacion.gv.FocusedRowHandle, "Descripcion", "FACTURACI�N DE BOLETO N� " + gvFP.GetRowCellValue(i, "Numero"))
            frmFacturacion.gv.SetRowCellValue(frmFacturacion.gv.FocusedRowHandle, "PrecioUnitario", gvFP.GetRowCellValue(i, "TotalAfecto"))
            frmFacturacion.gv.SetRowCellValue(frmFacturacion.gv.FocusedRowHandle, "VentaNoSujeta", gvFP.GetRowCellValue(i, "TotalNoSujeto"))
            frmFacturacion.gv.SetRowCellValue(frmFacturacion.gv.FocusedRowHandle, "VentaExenta", gvFP.GetRowCellValue(i, "TotalExento"))
            frmFacturacion.gv.SetRowCellValue(frmFacturacion.gv.FocusedRowHandle, "VentaAfecta", gvFP.GetRowCellValue(i, "TotalAfecto"))
            frmFacturacion.gv.SetRowCellValue(frmFacturacion.gv.FocusedRowHandle, "IdComprobante", gvFP.GetRowCellValue(i, "IdComprobante"))
            frmFacturacion.gv.UpdateCurrentRow()
        Next

    End Sub

    Private Sub sbImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            Return
        End If
        Dim csv_file As StreamReader = File.OpenText(ofd.FileName)
        Try
            While csv_file.Peek() > 0
                Dim sLine As String = csv_file.ReadLine()
                Dim aData As Array = sLine.Split(",")

                gvFP.AddNewRow()
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Fecha", aData(0))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "IdPunto", aData(1))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalNoSujeto", aData(2))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalExento", SiEsNulo(aData(3), 0))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalAfecto", SiEsNulo(aData(4), 0))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalIva", SiEsNulo(aData(5), 0))
                gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Nit", SiEsNulo(aData(6), ""))
                gvFP.UpdateCurrentRow()

            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Return
        End Try

        csv_file.Close()

    End Sub

    Private Sub InicializaGrid()
        dtDetalle = New DataTable
        dtDetalle.Columns.Add(New DataColumn("Fecha", GetType(Date)))
        dtDetalle.Columns.Add(New DataColumn("IdComprobante", GetType(Integer)))
        dtDetalle.Columns.Add(New DataColumn("TotalNoSujeto", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("TotalExento", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("TotalAfecto", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("TotalIva", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("Numero", GetType(String)))
        'dtDetalle.Columns.Add(New DataColumn("IdComprobante", GetType(Integer)))
        gcFP.DataSource = dtDetalle
    End Sub

    Private Sub deFecha_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles deFecha.Validated
        'Dim dtFactura As DataTable = blfac.fac_ConsultaFacturacionBoletos(teNumero.EditValue, deFecha.EditValue) 'objConsultas.cnsClientes(fac_frmConsultaClientes, beCodCliente.EditValue)
        'If dtFactura.Rows.Count = 0 Then
        '    MsgBox("No ser encontraron registros para ese n�mero de Boleto", MsgBoxStyle.Exclamation, "Nota")
        'Else
        '    deFecha.EditValue = dtFactura.Rows(0).Item("Fecha")
        '    teValorNoSujeto.EditValue = dtFactura.Rows(0).Item("TotalNoSujeto")
        '    teExento.EditValue = dtFactura.Rows(0).Item("TotalExento")
        '    teAfecto.EditValue = dtFactura.Rows(0).Item("TotalAfecto")

        '    gvFP.AddNewRow()
        '    gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Numero", dtFactura.Rows(0).Item("Numero"))
        '    gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Fecha", dtFactura.Rows(0).Item("Fecha"))
        '    gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalNoSujeto", dtFactura.Rows(0).Item("TotalNoSujeto"))
        '    gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalExento", dtFactura.Rows(0).Item("TotalExento"))
        '    gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalAfecto", dtFactura.Rows(0).Item("TotalAfecto"))
        '    gvFP.UpdateCurrentRow()
        'End If
    End Sub

    Private Sub sbAgregar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles sbAgregar.Click
        Dim dtFactura As DataTable = blfac.fac_ConsultaFacturacionBoletos(teNumero.EditValue, deFecha.EditValue) 'objConsultas.cnsClientes(fac_frmConsultaClientes, beCodCliente.EditValue)
        If dtFactura.Rows.Count = 0 Then
            MsgBox("No ser encontraron registros para ese n�mero de Boleto", MsgBoxStyle.Exclamation, "Nota")
        Else
            deFecha.EditValue = dtFactura.Rows(0).Item("Fecha")
            teValorNoSujeto.EditValue = dtFactura.Rows(0).Item("TotalNoSujeto")
            teExento.EditValue = dtFactura.Rows(0).Item("TotalExento")
            teAfecto.EditValue = dtFactura.Rows(0).Item("TotalAfecto")

            gvFP.AddNewRow()
            gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Numero", dtFactura.Rows(0).Item("Numero"))
            gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "Fecha", dtFactura.Rows(0).Item("Fecha"))
            gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalNoSujeto", dtFactura.Rows(0).Item("TotalNoSujeto"))
            gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalExento", dtFactura.Rows(0).Item("TotalExento"))
            gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "TotalAfecto", dtFactura.Rows(0).Item("TotalAfecto"))
            gvFP.SetRowCellValue(gvFP.FocusedRowHandle, "IdComprobante", dtFactura.Rows(0).Item("IdComprobante"))
            gvFP.UpdateCurrentRow()
        End If
    End Sub
End Class