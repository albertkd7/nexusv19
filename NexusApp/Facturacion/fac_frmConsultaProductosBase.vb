﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class fac_frmConsultaProductosBase
    Dim blFac As New FacturaBLL(g_ConnectionString)
    Dim blInventa As New InventarioBLL(g_ConnectionString)
    Dim Prod As DataTable, ProdVista As DataTable
    Dim IdProducto As String
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)


    Private Sub fac_frmConsultaProductosBase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        deDesde.EditValue = Today
        deHasta.EditValue = Today
        CargaCombos()
        leTipoDocumento.EditValue = -1
        'txtCodigo.EditValue = ""
        'txtNombre.EditValue = ""

        gvProd1.Columns.Clear()
        gcProductos.DataSource = blFac.fac_ConsultaProductos("select IdProducto, Nombre from inv_productos where EstadoProducto = 1 and permitefacturar=1")

        IdProducto = ""
        ProdVista = gcProductos.DataSource

        gvProd1.BestFitColumns()
        'gvProd1.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle

        gcPed.DataSource = blFac.fac_ConsultaGerencial(Today, Today, -1)
        gvPed.BestFitColumns()

        'para que no vean costos, e informacion adicional
        XtraTabPage2.PageVisible = entUsuario.CambiarPrecios
        XtraTabPage3.PageVisible = entUsuario.CambiarPrecios


    End Sub

    Private Sub CargaCombos()
        objCombos.fac_TiposComprobante(leTipoDocumento, "-- TODOS --")
        objCombos.inv_Precios(rileIdPrecio)
        objCombos.inv_Precios(rileIdPrecio1)

    End Sub

#Region "Consulta Productos"

    Private Sub gvProd1_FocusedRowChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gvProd1.FocusedRowChanged
        IdProducto = gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "IdProducto")
        Prod = blInventa.inv_GeneralesProducto(IdProducto)

        gcPr1.DataSource = blInventa.inv_ObtienePrecios(IdProducto, objMenu.User, pnIVA)
        gcEx1.DataSource = blInventa.inv_ObtieneSaldosPorProducto(-1, IdProducto)
        If Prod.Rows.Count = 0 Then
            Exit Sub
        End If

        teGrupo1.EditValue = Prod.Rows(0).Item("Grupo")
        teSubGrupo1.EditValue = Prod.Rows(0).Item("SubGrupo")
        teUnidad1.EditValue = Prod.Rows(0).Item("UnidadMedida")
        teMarca.EditValue = Prod.Rows(0).Item("Marca")
        teProveedor1.EditValue = Prod.Rows(0).Item("Proveedor")
        teColor.EditValue = Prod.Rows(0).Item("Color")
        teTalla.EditValue = Prod.Rows(0).Item("Talla")
        teEstilo.EditValue = Prod.Rows(0).Item("Estilo")
        teUnidadesPre.EditValue = Prod.Rows(0).Item("UnidadesPresentacion")
        meInfo.EditValue = Prod.Rows(0).Item("InformacionAdicional")

        Dim ArchivoImagen As String = Prod.Rows(0).Item("ArchivoImagen")
        If Not FileIO.FileSystem.FileExists(ArchivoImagen) Or ArchivoImagen = "" Then
            peFoto1.Image = Nothing
        Else
            Dim bm As New Bitmap(ArchivoImagen)
            peFoto1.Image = bm
        End If
    End Sub
#End Region

#Region "Consulta Historial"

    Private Sub btnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        gcProd.DataSource = blInventa.inv_BuscaProductosByLike(teCodigo.EditValue, teNombre.EditValue)
        gvProd.BestFitColumns()
        gcVentas.DataSource = Nothing
    End Sub

    Private Sub gvProd_FocusedRowChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gvProd.FocusedRowChanged
        IdProducto = ""
        IdProducto = gvProd.GetRowCellValue(gvProd.FocusedRowHandle, "IdProducto")

        Dim ds As DataSet = blFac.fac_ConsultaHistorialPorCodigoProducto(IdProducto)

        'lleno los data sources
        gcPr.DataSource = ds.Tables(0)
        gcEx.DataSource = ds.Tables(1)
        gcVentas.DataSource = ds.Tables(2)

        If ds.Tables(3).Rows.Count = 0 Then
            Exit Sub
        End If

        With ds.Tables(3).Rows(0)
            Dim ArchivoImagen As String = .Item("ArchivoImagen")
            If Not FileIO.FileSystem.FileExists(ArchivoImagen) Or ArchivoImagen = "" Then
                peFoto.Image = Nothing
            Else
                Dim bm As New Bitmap(ArchivoImagen)
                peFoto.Image = bm
            End If
            teGrupo.EditValue = .Item("Grupo")
            teSubGrupo.EditValue = .Item("SubGrupo")
            teUnidad.EditValue = .Item("UnidadMedida")
            tePrecioCosto.EditValue = Format(.Item("PrecioCosto"), "###,##0.00")
            teFechaUltCompra.EditValue = .Item("FechaUltCompra")
            teProveedor.EditValue = .Item("Proveedor")
            teFacturaUltCompra.EditValue = .Item("FacturaUltCompra")
            teCantidadUltCompra.EditValue = Format(.Item("CantidadUltCompra"), "###,##0.00")
        End With
    End Sub

#End Region


#Region "ConsultaGerencial"
    Private Sub btGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGenerar.Click
        gcPed.DataSource = blFac.fac_ConsultaGerencial(deDesde.EditValue, deHasta.EditValue, leTipoDocumento.EditValue)
        gvPed.BestFitColumns()
    End Sub
    Private Sub gvPed_FocusedRowChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gvPed.FocusedRowChanged
        gcDetalle.DataSource = blFac.fac_ConsultaPedidosDetalleGerencial(gvPed.GetFocusedRowCellValue("IdComprobante"))
        gvDetalle.BestFitColumns()
    End Sub

    Private Sub gvDetalle_FocusedRowChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gvDetalle.FocusedRowChanged
        Dim Prod As DataTable = blInventa.inv_GeneralesProducto(gvDetalle.GetFocusedRowCellValue("Referencia"))
        If Prod.Rows.Count = 0 Then
            Exit Sub
        End If
        Dim ArchivoImagen As String = Prod.Rows(0).Item("ArchivoImagen")
        If Not FileIO.FileSystem.FileExists(ArchivoImagen) Or ArchivoImagen = "" Then
            peFoto.Image = Nothing
        Else
            Dim bm As New Bitmap(ArchivoImagen)
            peFoto.Image = bm
        End If
    End Sub
#End Region

#Region "Tipo Busqueda"

    Private Sub txtNombre_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombre.EditValueChanged
        'If rgTipoBusqueda.SelectedIndex = 0 Then
        '    ProdVista.DefaultView.RowFilter = ("Nombre like '%" + txtNombre.EditValue + "%'")
        'Else
        '    ProdVista.DefaultView.RowFilter = ("Nombre like '" + txtNombre.EditValue + "%'")
        'End If
        If rgTipoBusqueda.SelectedIndex = 0 Then
            Dim Transaccion As String
            Dim CadenaPrincipal As String = txtNombre.EditValue

            Transaccion = txtNombre.EditValue
            If txtNombre.EditValue <> "" And Transaccion.IndexOf("%") > 0 Then
                Dim V() As String
                Dim Cadena As String
                Dim SentenciaBusqueda As String = "Nombre like '%"
                Dim x As Integer

                'Cadena a tratar 
                Cadena = txtNombre.EditValue

                'Remplazando todos los espaciadores incorrectos por ";" 
                For x = 0 To 1
                    Cadena = Replace(Cadena, "%", ";")
                Next x

                'Separo la cadena gracias al delimitador ";" 
                V = Split(Cadena, ";")

                'Muestro la cadena 
                For x = 0 To UBound(V)
                    If x = 0 Then
                        SentenciaBusqueda += Trim(V(x)) + "%'"
                    Else
                        SentenciaBusqueda += IIf(Trim(V(x)) <> "", " and Nombre like '%" + Trim(V(x)) + "%'", Trim(V(x)))
                    End If
                Next x
                ProdVista.DefaultView.RowFilter = (SentenciaBusqueda)
            Else
                ProdVista.DefaultView.RowFilter = ("Nombre like '%" + txtNombre.EditValue + "%'")
            End If
        Else
            ProdVista.DefaultView.RowFilter = ("Nombre like '" + txtNombre.EditValue + "%'")
        End If

        IdProducto = gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "IdProducto")
        gcPr1.DataSource = blInventa.inv_ObtienePrecios(IdProducto, objMenu.User, pnIVA)
        gcEx1.DataSource = blInventa.inv_ObtieneSaldosPorProducto(-1, IdProducto)
        gcProductos.DataSource = ProdVista.DefaultView
    End Sub

    Private Sub txtCodigo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigo.EditValueChanged
        If rgTipoBusqueda.SelectedIndex = 0 Then
            ProdVista.DefaultView.RowFilter = ("IdProducto like '%" + txtCodigo.EditValue + "%'")
        Else
            ProdVista.DefaultView.RowFilter = ("IdProducto like '" + txtCodigo.EditValue + "%'")
        End If

        IdProducto = gvProd1.GetRowCellValue(gvProd1.FocusedRowHandle, "IdProducto")
        gcPr1.DataSource = blInventa.inv_ObtienePrecios(IdProducto, objMenu.User, pnIVA)
        gcEx1.DataSource = blInventa.inv_ObtieneSaldosPorProducto(-1, IdProducto)
        gcProductos.DataSource = ProdVista.DefaultView
    End Sub

    Private Sub rgTipoBusqueda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgTipoBusqueda.SelectedIndexChanged
        If txtCodigo.EditValue <> "" Then
            txtCodigo_EditValueChanged("", New EventArgs)
        Else
            If txtNombre.EditValue <> "" Then
                txtNombre_EditValueChanged("", New EventArgs)
            End If
        End If
    End Sub
#End Region



End Class
