﻿Imports NexusELL.TableEntities
Imports NexusDLL
Public Class fac_frmRutas
    Dim entidad As fac_Rutas
    Dim entCuentas As con_Cuentas

    Private Sub ban_frmTiposTransaccion_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub ban_frmTiposTransaccion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.fac_RutasSelectAll
        entidad = objTablas.fac_RutasSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdRuta"))

        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub cban_frmTiposTransaccion_Nuevo_Click() Handles Me.Nuevo
        entidad = New fac_Rutas
        entidad.IdRuta = objFunciones.ObtenerUltimoId("FAC_RUTAS", "IdRuta") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub ban_frmTiposTransaccion_Save_Click() Handles Me.Guardar
        If teNombre.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [ Nombre]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.fac_RutasInsert(entidad)
        Else
            objTablas.fac_RutasUpdate(entidad)
        End If
        gc.DataSource = objTablas.fac_RutasSelectAll
        entidad = objTablas.fac_RutasSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdRuta"))
        CargaPantalla()
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub ban_frmTiposTransaccion_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.fac_RutasDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.fac_RutasSelectAll
                entidad = objTablas.fac_RutasSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdRuta"))
                CargaPantalla()
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL REGISTRO:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub ban_frmTiposTransaccion_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entidad
            teId.EditValue = .IdRuta
            teNombre.EditValue = .Nombre
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entidad
            .IdRuta = teId.EditValue
            .Nombre = teNombre.EditValue
        End With
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entidad = objTablas.fac_RutasSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdRuta"))
        CargaPantalla()
    End Sub

    Private Sub cban_frmTiposTransaccion_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teId.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub
End Class
