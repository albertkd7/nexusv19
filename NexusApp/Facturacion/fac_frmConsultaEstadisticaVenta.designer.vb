﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmConsultaEstadisticaVenta
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
        Me.btExportar = New DevExpress.XtraEditors.SimpleButton
        Me.deHasta = New DevExpress.XtraEditors.DateEdit
        Me.deDesde = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.btGenerar = New DevExpress.XtraEditors.SimpleButton
        Me.leGrupo = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.BeProducto1 = New Nexus.beProducto
        Me.lePuntoVenta = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.gcProductos = New DevExpress.XtraGrid.GridControl
        Me.gvProd1 = New DevExpress.XtraGrid.Views.Grid.GridView
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.deHasta.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvProd1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btExportar)
        Me.GroupControl1.Controls.Add(Me.deHasta)
        Me.GroupControl1.Controls.Add(Me.deDesde)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.btGenerar)
        Me.GroupControl1.Controls.Add(Me.leGrupo)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.BeProducto1)
        Me.GroupControl1.Controls.Add(Me.lePuntoVenta)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1087, 145)
        Me.GroupControl1.TabIndex = 65
        Me.GroupControl1.Text = "Parametros de la Consulta"
        '
        'btExportar
        '
        Me.btExportar.Location = New System.Drawing.Point(436, 101)
        Me.btExportar.Name = "btExportar"
        Me.btExportar.Size = New System.Drawing.Size(160, 23)
        Me.btExportar.TabIndex = 81
        Me.btExportar.Text = "Exportar"
        '
        'deHasta
        '
        Me.deHasta.EditValue = Nothing
        Me.deHasta.Location = New System.Drawing.Point(274, 25)
        Me.deHasta.Name = "deHasta"
        Me.deHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deHasta.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deHasta.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deHasta.Size = New System.Drawing.Size(100, 20)
        Me.deHasta.TabIndex = 66
        '
        'deDesde
        '
        Me.deDesde.EditValue = Nothing
        Me.deDesde.Location = New System.Drawing.Point(86, 25)
        Me.deDesde.Name = "deDesde"
        Me.deDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deDesde.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deDesde.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deDesde.Size = New System.Drawing.Size(100, 20)
        Me.deDesde.TabIndex = 65
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(207, 28)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 80
        Me.LabelControl2.Text = "Hasta Fecha:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(17, 28)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl1.TabIndex = 79
        Me.LabelControl1.Text = "Desde Fecha:"
        '
        'btGenerar
        '
        Me.btGenerar.Location = New System.Drawing.Point(436, 72)
        Me.btGenerar.Name = "btGenerar"
        Me.btGenerar.Size = New System.Drawing.Size(160, 23)
        Me.btGenerar.TabIndex = 71
        Me.btGenerar.Text = "Generar la Consulta"
        '
        'leGrupo
        '
        Me.leGrupo.Location = New System.Drawing.Point(86, 114)
        Me.leGrupo.Name = "leGrupo"
        Me.leGrupo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leGrupo.Size = New System.Drawing.Size(335, 20)
        Me.leGrupo.TabIndex = 70
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(48, 118)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl3.TabIndex = 78
        Me.LabelControl3.Text = "Grupo:"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(86, 69)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(335, 20)
        Me.leSucursal.TabIndex = 68
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(37, 73)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl4.TabIndex = 77
        Me.LabelControl4.Text = "Sucursal:"
        '
        'BeProducto1
        '
        Me.BeProducto1.Location = New System.Drawing.Point(30, 44)
        Me.BeProducto1.Name = "BeProducto1"
        Me.BeProducto1.Size = New System.Drawing.Size(612, 25)
        Me.BeProducto1.TabIndex = 67
        '
        'lePuntoVenta
        '
        Me.lePuntoVenta.Location = New System.Drawing.Point(86, 92)
        Me.lePuntoVenta.Name = "lePuntoVenta"
        Me.lePuntoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePuntoVenta.Size = New System.Drawing.Size(335, 20)
        Me.lePuntoVenta.TabIndex = 69
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(3, 95)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl5.TabIndex = 76
        Me.LabelControl5.Text = "Punto de Venta:"
        '
        'gcProductos
        '
        Me.gcProductos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcProductos.Location = New System.Drawing.Point(0, 145)
        Me.gcProductos.MainView = Me.gvProd1
        Me.gcProductos.Name = "gcProductos"
        Me.gcProductos.Size = New System.Drawing.Size(1087, 259)
        Me.gcProductos.TabIndex = 66
        Me.gcProductos.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvProd1})
        '
        'gvProd1
        '
        Me.gvProd1.GridControl = Me.gcProductos
        Me.gvProd1.Name = "gvProd1"
        Me.gvProd1.OptionsBehavior.Editable = False
        Me.gvProd1.OptionsView.ShowGroupPanel = False
        '
        'fac_frmConsultaEstadisticaVenta
        '
        Me.ClientSize = New System.Drawing.Size(1087, 429)
        Me.Controls.Add(Me.gcProductos)
        Me.Controls.Add(Me.GroupControl1)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmConsultaEstadisticaVenta"
        Me.OptionId = ""
        Me.Text = "Consulta de Estadistica de Ventas"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.gcProductos, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.deHasta.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvProd1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents deHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents deDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btGenerar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents leGrupo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BeProducto1 As Nexus.beProducto
    Friend WithEvents lePuntoVenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcProductos As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvProd1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btExportar As DevExpress.XtraEditors.SimpleButton

End Class
