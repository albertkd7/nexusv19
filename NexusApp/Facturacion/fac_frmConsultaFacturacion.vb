﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmConsultaFacturacion
    Dim blFac As New FacturaBLL(g_ConnectionString), DatosEncabezado As String, DatosPie As String, entidadTicket As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(10)
    Private Sub fac_frmConsultaFacturacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue)
        objCombos.fac_TiposComprobante(leTipoDoc, "-- TODOS --")
        gc.DataSource = blFac.fac_ConsultaFacturacion(Today, Today, -1, objMenu.User)
        gv.BestFitColumns()
        blFac.fac_ObtenerDatosEncabezadoPieTicket(DatosEncabezado, DatosPie, lePuntoVenta.EditValue)
    End Sub

    Private Sub btGenerar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btGenerar.Click
        gc.DataSource = blFac.fac_ConsultaFacturacion(deDesde.EditValue, deHasta.EditValue, leTipoDoc.EditValue, objMenu.User).Tables(0)
        gv.BestFitColumns()
    End Sub

    Private Sub btReImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btReImprimir.Click
        Dim IdDoc As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
        Dim IdFormaPago As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdFormaPago"), 0)
        If IdDoc = 0 Then
            MsgBox("Es necesario selecionar un documento", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        '   Se coloca la Validacion para que solo en Juro se Valide 

        If gsNombre_Empresa = "JURO, S. A. DE C. V." Then
            If IdFormaPago = 2 Then
                Dim N As Integer
                N = 0
                N = blFac.fac_VerificaVentaAprobada(IdDoc)
                If N = 0 Then
                    MsgBox("VENTA NO AUTORIZADA PARA SU IMPRECION", MsgBoxStyle.Exclamation, "Nota")
                    Exit Sub
                End If
            End If
        End If
        ReImprimeDocumento(IdDoc)
    End Sub
    Public Function ReImprimeDocumento(ByVal IdDoc As Integer) As Boolean
        Dim Ventas As fac_Ventas, Vendedores As fac_Vendedores, Departamentos As adm_Departamentos, Municipios As adm_Municipios, FormaPago As fac_FormasPago, Sucursales As adm_Sucursales, EntTipoDoc As adm_TiposComprobante
        Ventas = objTablas.fac_VentasSelectByPK(IdDoc)
        Vendedores = objTablas.fac_VendedoresSelectByPK(Ventas.IdVendedor)
        Departamentos = objTablas.adm_DepartamentosSelectByPK(Ventas.IdDepartamento)
        Municipios = objTablas.adm_MunicipiosSelectByPK(Ventas.IdMunicipio)
        FormaPago = objTablas.fac_FormasPagoSelectByPK(Ventas.IdFormaPago)
        Sucursales = objTablas.adm_SucursalesSelectByPK(Ventas.IdSucursal)
        EntTipoDoc = objTablas.adm_TiposComprobanteSelectByPK(Ventas.IdTipoComprobante)
        Dim dt As DataTable = blFac.fac_ObtenerDetalleDocumento("fac_VentasDetalle", IdDoc)
        Dim dtAfectaNc As DataTable = blFac.fac_ObtenerAfectaNC(IdDoc) ' para saber a que documentos afecto
        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)

        Select Case Ventas.IdTipoComprobante
            Case 5 'crédito fiscal
                If gsNombre_Empresa.ToUpper().Contains("S1 EL SALVADOR") Then 'cambio exclusivo para GS1(CLIENTE DE AE INFORMATICA)

                    Dim rpt1 As New fac_rptCCFGS1
                    'rpt1.XrSubreport1.ReportSource.DataSource = dt
                    'rpt1.XrSubreport1.ReportSource.DataMember = ""

                    'rpt1.XrSubreport2.ReportSource.DataSource = dt
                    'rpt1.XrSubreport2.ReportSource.DataMember = ""

                    'rpt1.XrSubreport3.ReportSource.DataSource = dt
                    'rpt1.XrSubreport3.ReportSource.DataMember = ""
                    rpt1.DataSource = dt
                    rpt1.DataMember = ""
                    With Ventas

                        rpt1.xrlNumero.Text = .Numero

                        rpt1.xrlNombre1.Text = .Nombre
                        rpt1.xrlFecha1.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlNit1.Text = .Nit
                        rpt1.xrlNrc1.Text = .Nrc

                        rpt1.xrlGiro1.Text = .Giro
                        rpt1.xrlFormaPago1.Text = dt.Rows(0).Item("FormaPago")
                        rpt1.xrlDirec1.Text = .Direccion & " " & Municipios.Nombre & " " & Departamentos.Nombre

                        'DETALLE 1
                        If dt.Rows.Count > 0 Then
                            rpt1.xrlCant11.Text = dt.Rows(0).Item("Cantidad")
                            rpt1.xrlDesc11.Text = dt.Rows(0).Item("Descripcion")
                            rpt1.xrlPrecio11.Text = dt.Rows(0).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta11.Text = dt.Rows(0).Item("VentaExenta")
                            rpt1.xrlTotal11.Text = dt.Rows(0).Item("VentaAfecta")
                        End If
                        'EL DETALLE 2
                        If dt.Rows.Count > 1 Then
                            rpt1.xrlCant12.Text = dt.Rows(1).Item("Cantidad")
                            rpt1.xrlDesc12.Text = dt.Rows(1).Item("Descripcion")
                            rpt1.xrlPrecio12.Text = dt.Rows(1).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta12.Text = dt.Rows(1).Item("VentaExenta")
                            rpt1.xrlTotal12.Text = dt.Rows(1).Item("VentaAfecta")
                        End If
                        'EL DETALLE 3
                        If dt.Rows.Count > 2 Then
                            rpt1.xrlCant13.Text = dt.Rows(2).Item("Cantidad")
                            rpt1.xrlDesc13.Text = dt.Rows(2).Item("Descripcion")
                            rpt1.xrlPrecio13.Text = dt.Rows(2).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta13.Text = dt.Rows(2).Item("VentaExenta")
                            rpt1.xrlTotal13.Text = dt.Rows(2).Item("VentaAfecta")
                        End If
                        'EL DETALLE 4
                        If dt.Rows.Count > 3 Then
                            rpt1.xrlCant14.Text = dt.Rows(3).Item("Cantidad")
                            rpt1.xrlDesc14.Text = dt.Rows(3).Item("Descripcion")
                            rpt1.xrlPrecio14.Text = dt.Rows(3).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta14.Text = dt.Rows(3).Item("VentaExenta")
                            rpt1.xrlTotal14.Text = dt.Rows(3).Item("VentaAfecta")
                        End If
                        'EL DETALLE 5
                        If dt.Rows.Count > 4 Then
                            rpt1.xrlCant15.Text = dt.Rows(4).Item("Cantidad")
                            rpt1.xrlDesc15.Text = dt.Rows(4).Item("Descripcion")
                            rpt1.xrlPrecio15.Text = dt.Rows(4).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta15.Text = dt.Rows(4).Item("VentaExenta")
                            rpt1.xrlTotal15.Text = dt.Rows(4).Item("VentaAfecta")
                        End If
                        'EL DETALLE 6
                        If dt.Rows.Count > 5 Then
                            rpt1.xrlCant16.Text = dt.Rows(5).Item("Cantidad")
                            rpt1.xrlDesc16.Text = dt.Rows(5).Item("Descripcion")
                            rpt1.xrlPrecio16.Text = dt.Rows(5).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta16.Text = dt.Rows(5).Item("VentaExenta")
                            rpt1.xrlTotal16.Text = dt.Rows(5).Item("VentaAfecta")
                        End If
                        'EL DETALLE 7
                        If dt.Rows.Count > 6 Then
                            rpt1.xrlCant17.Text = dt.Rows(6).Item("Cantidad")
                            rpt1.xrlDesc17.Text = dt.Rows(6).Item("Descripcion")
                            rpt1.xrlPrecio17.Text = dt.Rows(6).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta17.Text = dt.Rows(6).Item("VentaExenta")
                            rpt1.xrlTotal17.Text = dt.Rows(6).Item("VentaAfecta")
                        End If
                        'EL TOTAL 1
                        rpt1.xrlTotalAfecto1.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlIva1.Text = Format(.TotalIva, "###,##0.00")
                        If .TotalImpuesto1 = 0 Then
                            rpt1.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                        Else
                            rpt1.xrlIvaRetenido.Text = "(" & Format(.TotalImpuesto1, "###,##0.00") & ")"
                        End If
                        'rpt1.xrlCesc1.Text = Format(.TotalImpuesto2, "##,###.00")
                        rpt1.xrlSubTotal1.Text = Format(.TotalIva + .TotalAfecto, "###,##0.00")
                        rpt1.xrlTotalExento1.Text = Format(.TotalExento, "###,##0.00")
                        rpt1.xrlTotal1.Text = Format(.TotalComprobante, "###,##0.00")
                        Dim Decimales = String.Format("{0:c}", .TotalComprobante)
                        Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                        rpt1.xrlCantLetras1.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"


                        'LA COPIA DEL CRÉDITO FISCAL 2
                        rpt1.xrlNombre2.Text = .Nombre
                        rpt1.xrlFecha2.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlNit2.Text = .Nit
                        rpt1.xrlNrc2.Text = .Nrc
                        rpt1.xrlGiro2.Text = .Giro
                        rpt1.xrlFormaPago2.Text = dt.Rows(0).Item("FormaPago")
                        rpt1.xrlDirec2.Text = .Direccion & " " & Municipios.Nombre & " " & Departamentos.Nombre

                        'DETALLE 1
                        If dt.Rows.Count > 0 Then
                            rpt1.xrlCant21.Text = dt.Rows(0).Item("Cantidad")
                            rpt1.xrlDesc21.Text = dt.Rows(0).Item("Descripcion")
                            rpt1.xrlPrecio21.Text = dt.Rows(0).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta21.Text = dt.Rows(0).Item("VentaExenta")
                            rpt1.xrlTotal21.Text = dt.Rows(0).Item("VentaAfecta")
                        End If
                        'EL DETALLE 2
                        If dt.Rows.Count > 1 Then
                            rpt1.xrlCant22.Text = dt.Rows(1).Item("Cantidad")
                            rpt1.xrlDesc22.Text = dt.Rows(1).Item("Descripcion")
                            rpt1.xrlPrecio22.Text = dt.Rows(1).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta22.Text = dt.Rows(1).Item("VentaExenta")
                            rpt1.xrlTotal22.Text = dt.Rows(1).Item("VentaAfecta")
                        End If
                        If dt.Rows.Count > 2 Then
                            rpt1.xrlCant23.Text = dt.Rows(2).Item("Cantidad")
                            rpt1.xrlDesc23.Text = dt.Rows(2).Item("Descripcion")
                            rpt1.xrlPrecio23.Text = dt.Rows(2).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta23.Text = dt.Rows(2).Item("VentaExenta")
                            rpt1.xrlTotal23.Text = dt.Rows(2).Item("VentaAfecta")
                        End If
                        If dt.Rows.Count > 3 Then
                            rpt1.xrlCant24.Text = dt.Rows(3).Item("Cantidad")
                            rpt1.xrlDesc24.Text = dt.Rows(3).Item("Descripcion")
                            rpt1.xrlPrecio24.Text = dt.Rows(3).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta24.Text = dt.Rows(3).Item("VentaExenta")
                            rpt1.xrlTotal24.Text = dt.Rows(3).Item("VentaAfecta")
                        End If
                        'EL DETALLE 5
                        If dt.Rows.Count > 4 Then
                            rpt1.xrlCant25.Text = dt.Rows(4).Item("Cantidad")
                            rpt1.xrlDesc25.Text = dt.Rows(4).Item("Descripcion")
                            rpt1.xrlPrecio25.Text = dt.Rows(4).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta25.Text = dt.Rows(4).Item("VentaExenta")
                            rpt1.xrlTotal25.Text = dt.Rows(4).Item("VentaAfecta")
                        End If
                        'EL DETALLE 6
                        If dt.Rows.Count > 5 Then
                            rpt1.xrlCant26.Text = dt.Rows(5).Item("Cantidad")
                            rpt1.xrlDesc26.Text = dt.Rows(5).Item("Descripcion")
                            rpt1.xrlPrecio26.Text = dt.Rows(5).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta26.Text = dt.Rows(5).Item("VentaExenta")
                            rpt1.xrlTotal26.Text = dt.Rows(5).Item("VentaAfecta")
                        End If
                        'EL DETALLE 7
                        If dt.Rows.Count > 6 Then
                            rpt1.xrlCant27.Text = dt.Rows(6).Item("Cantidad")
                            rpt1.xrlDesc27.Text = dt.Rows(6).Item("Descripcion")
                            rpt1.xrlPrecio27.Text = dt.Rows(6).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta27.Text = dt.Rows(6).Item("VentaExenta")
                            rpt1.xrlTotal27.Text = dt.Rows(6).Item("VentaAfecta")
                        End If
                        'EL TOTAL 2
                        rpt1.xrlTotalAfecto2.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlIva2.Text = Format(.TotalIva, "###,##0.00")
                        If .TotalImpuesto1 = 0 Then
                            rpt1.xrlIvaRetenido2.Text = Format(.TotalImpuesto1, "###,##0.00")
                        Else
                            rpt1.xrlIvaRetenido2.Text = "(" & Format(.TotalImpuesto1, "###,##0.00") & ")"
                        End If
                        'rpt1.xrlCesc2.Text = Format(.TotalImpuesto2, "##,###.00")
                        rpt1.xrlSubTotal2.Text = Format(.TotalIva + .TotalAfecto, "###,##0.00")
                        rpt1.xrlTotalExento2.Text = Format(.TotalExento, "###,##0.00")
                        rpt1.xrlTotal2.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlCantLetras2.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"


                        'LA COPIA 3 DEL CCF
                        rpt1.xrlNombre3.Text = .Nombre
                        rpt1.xrlFecha3.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlNit3.Text = .Nit
                        rpt1.xrlNrc3.Text = .Nrc
                        rpt1.xrlGiro3.Text = .Giro
                        rpt1.xrlFormaPago3.Text = dt.Rows(0).Item("FormaPago")
                        rpt1.xrlDirec3.Text = .Direccion & " " & Municipios.Nombre & " " & Departamentos.Nombre

                        'DETALLE 1
                        If dt.Rows.Count > 0 Then
                            rpt1.xrlCant31.Text = dt.Rows(0).Item("Cantidad")
                            rpt1.xrlDesc31.Text = dt.Rows(0).Item("Descripcion")
                            rpt1.xrlPrecio31.Text = dt.Rows(0).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta31.Text = dt.Rows(0).Item("VentaExenta")
                            rpt1.xrlTotal31.Text = dt.Rows(0).Item("VentaAfecta")
                        End If
                        'EL DETALLE 2
                        If dt.Rows.Count > 1 Then
                            rpt1.xrlCant32.Text = dt.Rows(1).Item("Cantidad")
                            rpt1.xrlDesc32.Text = dt.Rows(1).Item("Descripcion")
                            rpt1.xrlPrecio32.Text = dt.Rows(1).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta32.Text = dt.Rows(1).Item("VentaExenta")
                            rpt1.xrlTotal32.Text = dt.Rows(1).Item("VentaAfecta")
                        End If
                        If dt.Rows.Count > 2 Then
                            rpt1.xrlCant33.Text = dt.Rows(2).Item("Cantidad")
                            rpt1.xrlDesc33.Text = dt.Rows(2).Item("Descripcion")
                            rpt1.xrlPrecio33.Text = dt.Rows(2).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta33.Text = dt.Rows(2).Item("VentaExenta")
                            rpt1.xrlTotal33.Text = dt.Rows(2).Item("VentaAfecta")
                        End If
                        If dt.Rows.Count > 3 Then
                            rpt1.xrlCant34.Text = dt.Rows(3).Item("Cantidad")
                            rpt1.xrlDesc34.Text = dt.Rows(3).Item("Descripcion")
                            rpt1.xrlPrecio34.Text = dt.Rows(3).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta34.Text = dt.Rows(3).Item("VentaExenta")
                            rpt1.xrlTotal34.Text = dt.Rows(3).Item("VentaAfecta")
                        End If
                        'EL DETALLE 5
                        If dt.Rows.Count > 4 Then
                            rpt1.xrlCant35.Text = dt.Rows(4).Item("Cantidad")
                            rpt1.xrlDesc35.Text = dt.Rows(4).Item("Descripcion")
                            rpt1.xrlPrecio35.Text = dt.Rows(4).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta35.Text = dt.Rows(4).Item("VentaExenta")
                            rpt1.xrlTotal35.Text = dt.Rows(4).Item("VentaAfecta")
                        End If
                        'EL DETALLE 6
                        If dt.Rows.Count > 5 Then
                            rpt1.xrlCant36.Text = dt.Rows(5).Item("Cantidad")
                            rpt1.xrlDesc36.Text = dt.Rows(5).Item("Descripcion")
                            rpt1.xrlPrecio36.Text = dt.Rows(5).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta36.Text = dt.Rows(5).Item("VentaExenta")
                            rpt1.xrlTotal36.Text = dt.Rows(5).Item("VentaAfecta")
                        End If
                        'EL DETALLE 7
                        If dt.Rows.Count > 6 Then
                            rpt1.xrlCant37.Text = dt.Rows(6).Item("Cantidad")
                            rpt1.xrlDesc37.Text = dt.Rows(6).Item("Descripcion")
                            rpt1.xrlPrecio37.Text = dt.Rows(6).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta37.Text = dt.Rows(6).Item("VentaExenta")
                            rpt1.xrlTotal37.Text = dt.Rows(6).Item("VentaAfecta")
                        End If
                        'EL TOTAL 3
                        rpt1.xrlTotalAfecto3.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlIva3.Text = Format(.TotalIva, "###,##0.00")
                        If .TotalImpuesto1 = 0 Then
                            rpt1.xrlIvaRetenido3.Text = Format(.TotalImpuesto1, "###,##0.00")
                        Else
                            rpt1.xrlIvaRetenido3.Text = "(" & Format(.TotalImpuesto1, "###,##0.00") & ")"
                        End If
                        ' rpt1.xrlCesc3.Text = Format(.TotalImpuesto2, "##,###.00")
                        rpt1.xrlSubTotal3.Text = Format(.TotalIva + .TotalAfecto, "###,##0.00")
                        rpt1.xrlTotalExento3.Text = Format(.TotalExento, "###,##0.00")
                        rpt1.xrlTotal3.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlCantLetras3.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"

                        'LAS COLITAS
                        rpt1.xrlClient1.Text = .IdCliente & "   " & .Nombre
                        rpt1.xrlDoc1.Text = EntTipoDoc.Abreviatura & "-" & .Numero
                        rpt1.xrlFec1.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlSub1.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlImp1.Text = Format(.TotalIva, "###,##0.00")
                        rpt1.xrlTot1.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlArt1.Text = dt.Rows(0).Item("Descripcion")

                        rpt1.xrlClient2.Text = .IdCliente & "   " & .Nombre
                        rpt1.xrlDoc2.Text = EntTipoDoc.Abreviatura & "-" & .Numero
                        rpt1.xrlFec2.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlSub2.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlImp2.Text = Format(.TotalIva, "###,##0.00")
                        rpt1.xrlTot2.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlArt2.Text = dt.Rows(0).Item("Descripcion")
                    End With
                    rpt1.ShowPrintMarginsWarning = False
                    rpt1.ShowPreviewDialog()
                    Exit Function
                End If

                Dim Template = Application.StartupPath & "\Plantillas\cfNexus" & NumFormato & ".repx"

                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNumRemision.Text = .Telefono
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlDiasCredito.Text = .DiasCredito
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(.TotalIva, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlCESC.Text = Format(.TotalImpuesto2, "##,###.00")
                    rpt.xrlSubTotal.Text = Format(.TotalIva + Ventas.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                    rpt.xrlCantLetras.Text = Num2Text(Int(Ventas.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPrintMarginsWarning = False

                    rpt.ShowPreviewDialog()
                End With
            Case 6 'consumidor final
                If gsNombre_Empresa.ToUpper().Contains("S1 EL SALVADOR") Then 'cambio exclusivo para GS1(CLIENTE DE AE INFORMATICA)
                    Dim rpt1 As New fac_rptFCFGS1
                    rpt1.DataSource = dt
                    rpt1.DataMember = ""
                    With Ventas

                        rpt1.xrlNumero.Text = .Numero

                        rpt1.xrlNombre1.Text = .Nombre
                        rpt1.xrlFecha1.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlNit1.Text = .Nit
                        rpt1.xrlNrc1.Text = .Nrc

                        rpt1.xrlGiro1.Text = .Giro
                        rpt1.xrlFormaPago1.Text = dt.Rows(0).Item("FormaPago")
                        rpt1.xrlDirec1.Text = .Direccion & " " & Municipios.Nombre & " " & Departamentos.Nombre

                        'DETALLE 1
                        If dt.Rows.Count > 0 Then
                            rpt1.xrlCant11.Text = dt.Rows(0).Item("Cantidad")
                            rpt1.xrlDesc11.Text = dt.Rows(0).Item("Descripcion")
                            rpt1.xrlPrecio11.Text = dt.Rows(0).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta11.Text = dt.Rows(0).Item("VentaExenta")
                            rpt1.xrlTotal11.Text = dt.Rows(0).Item("VentaAfecta")
                        End If
                        'EL DETALLE 2
                        If dt.Rows.Count > 1 Then
                            rpt1.xrlCant12.Text = dt.Rows(1).Item("Cantidad")
                            rpt1.xrlDesc12.Text = dt.Rows(1).Item("Descripcion")
                            rpt1.xrlPrecio12.Text = dt.Rows(1).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta12.Text = dt.Rows(1).Item("VentaExenta")
                            rpt1.xrlTotal12.Text = dt.Rows(1).Item("VentaAfecta")
                        End If
                        'EL DETALLE 3
                        If dt.Rows.Count > 2 Then
                            rpt1.xrlCant13.Text = dt.Rows(2).Item("Cantidad")
                            rpt1.xrlDesc13.Text = dt.Rows(2).Item("Descripcion")
                            rpt1.xrlPrecio13.Text = dt.Rows(2).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta13.Text = dt.Rows(2).Item("VentaExenta")
                            rpt1.xrlTotal13.Text = dt.Rows(2).Item("VentaAfecta")
                        End If
                        'EL DETALLE 4
                        If dt.Rows.Count > 3 Then
                            rpt1.xrlCant14.Text = dt.Rows(3).Item("Cantidad")
                            rpt1.xrlDesc14.Text = dt.Rows(3).Item("Descripcion")
                            rpt1.xrlPrecio14.Text = dt.Rows(3).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta14.Text = dt.Rows(3).Item("VentaExenta")
                            rpt1.xrlTotal14.Text = dt.Rows(3).Item("VentaAfecta")
                        End If
                        'EL DETALLE 5
                        If dt.Rows.Count > 4 Then
                            rpt1.xrlCant15.Text = dt.Rows(4).Item("Cantidad")
                            rpt1.xrlDesc15.Text = dt.Rows(4).Item("Descripcion")
                            rpt1.xrlPrecio15.Text = dt.Rows(4).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta15.Text = dt.Rows(4).Item("VentaExenta")
                            rpt1.xrlTotal15.Text = dt.Rows(4).Item("VentaAfecta")
                        End If
                        'EL DETALLE 6
                        If dt.Rows.Count > 5 Then
                            rpt1.xrlCant16.Text = dt.Rows(5).Item("Cantidad")
                            rpt1.xrlDesc16.Text = dt.Rows(5).Item("Descripcion")
                            rpt1.xrlPrecio16.Text = dt.Rows(5).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta16.Text = dt.Rows(5).Item("VentaExenta")
                            rpt1.xrlTotal16.Text = dt.Rows(5).Item("VentaAfecta")
                        End If
                        'EL DETALLE 7
                        If dt.Rows.Count > 6 Then
                            rpt1.xrlCant17.Text = dt.Rows(6).Item("Cantidad")
                            rpt1.xrlDesc17.Text = dt.Rows(6).Item("Descripcion")
                            rpt1.xrlPrecio17.Text = dt.Rows(6).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta17.Text = dt.Rows(6).Item("VentaExenta")
                            rpt1.xrlTotal17.Text = dt.Rows(6).Item("VentaAfecta")
                        End If
                        'EL TOTAL 1
                        rpt1.xrlTotalAfecto1.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlIva1.Text = Format(.TotalIva, "###,##0.00")
                        If .TotalImpuesto1 = 0 Then
                            rpt1.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                        Else
                            rpt1.xrlIvaRetenido.Text = "(" & Format(.TotalImpuesto1, "###,##0.00") & ")"
                        End If
                        'rpt1.xrlCesc1.Text = Format(.TotalImpuesto2, "##,###.00")
                        rpt1.xrlSubTotal1.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlTotalExento1.Text = Format(.TotalExento, "###,##0.00")
                        rpt1.xrlTotal1.Text = Format(.TotalComprobante, "###,##0.00")
                        Dim Decimales = String.Format("{0:c}", .TotalComprobante)
                        Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                        rpt1.xrlCantLetras1.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"


                        'LA COPIA DEL CRÉDITO FISCAL 2
                        rpt1.xrlNombre2.Text = .Nombre
                        rpt1.xrlFecha2.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlNit2.Text = .Nit
                        rpt1.xrlNrc2.Text = .Nrc
                        rpt1.xrlGiro2.Text = .Giro
                        rpt1.xrlFormaPago2.Text = dt.Rows(0).Item("FormaPago")
                        rpt1.xrlDirec2.Text = .Direccion & " " & Municipios.Nombre & " " & Departamentos.Nombre

                        'DETALLE 1
                        If dt.Rows.Count > 0 Then
                            rpt1.xrlCant21.Text = dt.Rows(0).Item("Cantidad")
                            rpt1.xrlDesc21.Text = dt.Rows(0).Item("Descripcion")
                            rpt1.xrlPrecio21.Text = dt.Rows(0).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta21.Text = dt.Rows(0).Item("VentaExenta")
                            rpt1.xrlTotal21.Text = dt.Rows(0).Item("VentaAfecta")
                        End If
                        'EL DETALLE 2
                        If dt.Rows.Count > 1 Then
                            rpt1.xrlCant22.Text = dt.Rows(1).Item("Cantidad")
                            rpt1.xrlDesc22.Text = dt.Rows(1).Item("Descripcion")
                            rpt1.xrlPrecio22.Text = dt.Rows(1).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta22.Text = dt.Rows(1).Item("VentaExenta")
                            rpt1.xrlTotal22.Text = dt.Rows(1).Item("VentaAfecta")
                        End If
                        If dt.Rows.Count > 2 Then
                            rpt1.xrlCant23.Text = dt.Rows(2).Item("Cantidad")
                            rpt1.xrlDesc23.Text = dt.Rows(2).Item("Descripcion")
                            rpt1.xrlPrecio23.Text = dt.Rows(2).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta23.Text = dt.Rows(2).Item("VentaExenta")
                            rpt1.xrlTotal23.Text = dt.Rows(2).Item("VentaAfecta")
                        End If
                        If dt.Rows.Count > 3 Then
                            rpt1.xrlCant24.Text = dt.Rows(3).Item("Cantidad")
                            rpt1.xrlDesc24.Text = dt.Rows(3).Item("Descripcion")
                            rpt1.xrlPrecio24.Text = dt.Rows(3).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta24.Text = dt.Rows(3).Item("VentaExenta")
                            rpt1.xrlTotal24.Text = dt.Rows(3).Item("VentaAfecta")
                        End If
                        'EL DETALLE 5
                        If dt.Rows.Count > 4 Then
                            rpt1.xrlCant25.Text = dt.Rows(4).Item("Cantidad")
                            rpt1.xrlDesc25.Text = dt.Rows(4).Item("Descripcion")
                            rpt1.xrlPrecio25.Text = dt.Rows(4).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta25.Text = dt.Rows(4).Item("VentaExenta")
                            rpt1.xrlTotal25.Text = dt.Rows(4).Item("VentaAfecta")
                        End If
                        'EL DETALLE 6
                        If dt.Rows.Count > 5 Then
                            rpt1.xrlCant26.Text = dt.Rows(5).Item("Cantidad")
                            rpt1.xrlDesc26.Text = dt.Rows(5).Item("Descripcion")
                            rpt1.xrlPrecio26.Text = dt.Rows(5).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta26.Text = dt.Rows(5).Item("VentaExenta")
                            rpt1.xrlTotal26.Text = dt.Rows(5).Item("VentaAfecta")
                        End If
                        'EL DETALLE 7
                        If dt.Rows.Count > 6 Then
                            rpt1.xrlCant27.Text = dt.Rows(6).Item("Cantidad")
                            rpt1.xrlDesc27.Text = dt.Rows(6).Item("Descripcion")
                            rpt1.xrlPrecio27.Text = dt.Rows(6).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta27.Text = dt.Rows(6).Item("VentaExenta")
                            rpt1.xrlTotal27.Text = dt.Rows(6).Item("VentaAfecta")
                        End If
                        'EL TOTAL 2
                        rpt1.xrlTotalAfecto2.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlIva2.Text = Format(.TotalIva, "###,##0.00")
                        If .TotalImpuesto1 = 0 Then
                            rpt1.xrlIvaRetenido2.Text = Format(.TotalImpuesto1, "###,##0.00")
                        Else
                            rpt1.xrlIvaRetenido2.Text = "(" & Format(.TotalImpuesto1, "###,##0.00") & ")"
                        End If
                        'rpt1.xrlCesc2.Text = Format(.TotalImpuesto2, "##,###.00")
                        rpt1.xrlSubTotal2.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlTotalExento2.Text = Format(.TotalExento, "###,##0.00")
                        rpt1.xrlTotal2.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlCantLetras2.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"


                        'LA COPIA 3 DEL CCF
                        rpt1.xrlNombre3.Text = .Nombre
                        rpt1.xrlFecha3.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlNit3.Text = .Nit
                        rpt1.xrlNrc3.Text = .Nrc
                        rpt1.xrlGiro3.Text = .Giro
                        rpt1.xrlFormaPago3.Text = dt.Rows(0).Item("FormaPago")
                        rpt1.xrlDirec3.Text = .Direccion & " " & Municipios.Nombre & " " & Departamentos.Nombre

                        'DETALLE 1
                        If dt.Rows.Count > 0 Then
                            rpt1.xrlCant31.Text = dt.Rows(0).Item("Cantidad")
                            rpt1.xrlDesc31.Text = dt.Rows(0).Item("Descripcion")
                            rpt1.xrlPrecio31.Text = dt.Rows(0).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta31.Text = dt.Rows(0).Item("VentaExenta")
                            rpt1.xrlTotal31.Text = dt.Rows(0).Item("VentaAfecta")
                        End If
                        'EL DETALLE 2
                        If dt.Rows.Count > 1 Then
                            rpt1.xrlCant32.Text = dt.Rows(1).Item("Cantidad")
                            rpt1.xrlDesc32.Text = dt.Rows(1).Item("Descripcion")
                            rpt1.xrlPrecio32.Text = dt.Rows(1).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta32.Text = dt.Rows(1).Item("VentaExenta")
                            rpt1.xrlTotal32.Text = dt.Rows(1).Item("VentaAfecta")
                        End If
                        If dt.Rows.Count > 2 Then
                            rpt1.xrlCant33.Text = dt.Rows(2).Item("Cantidad")
                            rpt1.xrlDesc33.Text = dt.Rows(2).Item("Descripcion")
                            rpt1.xrlPrecio33.Text = dt.Rows(2).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta33.Text = dt.Rows(2).Item("VentaExenta")
                            rpt1.xrlTotal33.Text = dt.Rows(2).Item("VentaAfecta")
                        End If
                        If dt.Rows.Count > 3 Then
                            rpt1.xrlCant34.Text = dt.Rows(3).Item("Cantidad")
                            rpt1.xrlDesc34.Text = dt.Rows(3).Item("Descripcion")
                            rpt1.xrlPrecio34.Text = dt.Rows(3).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta34.Text = dt.Rows(3).Item("VentaExenta")
                            rpt1.xrlTotal34.Text = dt.Rows(3).Item("VentaAfecta")
                        End If
                        'EL DETALLE 5
                        If dt.Rows.Count > 4 Then
                            rpt1.xrlCant35.Text = dt.Rows(4).Item("Cantidad")
                            rpt1.xrlDesc35.Text = dt.Rows(4).Item("Descripcion")
                            rpt1.xrlPrecio35.Text = dt.Rows(4).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta35.Text = dt.Rows(4).Item("VentaExenta")
                            rpt1.xrlTotal35.Text = dt.Rows(4).Item("VentaAfecta")
                        End If
                        'EL DETALLE 6
                        If dt.Rows.Count > 5 Then
                            rpt1.xrlCant36.Text = dt.Rows(5).Item("Cantidad")
                            rpt1.xrlDesc36.Text = dt.Rows(5).Item("Descripcion")
                            rpt1.xrlPrecio36.Text = dt.Rows(5).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta36.Text = dt.Rows(5).Item("VentaExenta")
                            rpt1.xrlTotal36.Text = dt.Rows(5).Item("VentaAfecta")
                        End If
                        'EL DETALLE 7
                        If dt.Rows.Count > 6 Then
                            rpt1.xrlCant37.Text = dt.Rows(6).Item("Cantidad")
                            rpt1.xrlDesc37.Text = dt.Rows(6).Item("Descripcion")
                            rpt1.xrlPrecio37.Text = dt.Rows(6).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta37.Text = dt.Rows(6).Item("VentaExenta")
                            rpt1.xrlTotal37.Text = dt.Rows(6).Item("VentaAfecta")
                        End If
                        'EL TOTAL 3
                        rpt1.xrlTotalAfecto3.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlIva3.Text = Format(.TotalIva, "###,##0.00")
                        If .TotalImpuesto1 = 0 Then
                            rpt1.xrlIvaRetenido3.Text = Format(.TotalImpuesto1, "###,##0.00")
                        Else
                            rpt1.xrlIvaRetenido3.Text = "(" & Format(.TotalImpuesto1, "###,##0.00") & ")"
                        End If
                        ' rpt1.xrlCesc3.Text = Format(.TotalImpuesto2, "##,###.00")
                        rpt1.xrlSubTotal3.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlTotalExento3.Text = Format(.TotalExento, "###,##0.00")
                        rpt1.xrlTotal3.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlCantLetras3.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"

                        'LAS COLITAS
                        rpt1.xrlClient1.Text = .IdCliente & "   " & .Nombre
                        rpt1.xrlDoc1.Text = EntTipoDoc.Abreviatura & "-" & .Numero
                        rpt1.xrlFec1.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlSub1.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlImp1.Text = Format(.TotalIva, "###,##0.00")
                        rpt1.xrlTot1.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlArt1.Text = dt.Rows(0).Item("Descripcion")

                        rpt1.xrlClient2.Text = .IdCliente & "   " & .Nombre
                        rpt1.xrlDoc2.Text = EntTipoDoc.Abreviatura & "-" & .Numero
                        rpt1.xrlFec2.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlSub2.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlImp2.Text = Format(.TotalIva, "###,##0.00")
                        rpt1.xrlTot2.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlArt2.Text = dt.Rows(0).Item("Descripcion")
                    End With
                    rpt1.ShowPrintMarginsWarning = False
                    rpt1.ShowPreviewDialog()
                    Exit Function
                End If

                Dim Template = Application.StartupPath & "\Plantillas\faNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFactura
                rpt.LoadLayout(Template)

                rpt.DataSource = dt
                rpt.DataMember = ""
                rpt.XrSubreport1.ReportSource.DataSource = dt
                rpt.XrSubreport1.ReportSource.DataMember = ""

                rpt.XrSubreport2.ReportSource.DataSource = dt
                rpt.XrSubreport2.ReportSource.DataMember = ""

                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNumRemision.Text = .Telefono
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDiasCredito.Text = .DiasCredito
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlCESC.Text = Format(.TotalImpuesto2, "##,###.00")
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalAfecto - .TotalImpuesto1, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    Dim CantidaLetras As String = Num2Text(Int(IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))) & Decimales & " DÓLARES"
                    rpt.xrlCantLetras.Text = CantidaLetras
                    rpt.xrlComentario.Text = .Comentario
                    '2 Formato
                    rpt.xrlCodigo2.Text = .IdCliente
                    rpt.xrlNombre2.Text = .Nombre
                    rpt.xrlNumero2.Text = .Numero
                    rpt.xrlNumFormulario2.Text = .NumFormularioUnico
                    rpt.xrlNumRemision2.Text = .Telefono
                    rpt.xrlFecha2.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion2.Text = Now
                    rpt.xrlSucursal2.Text = Sucursales.Nombre
                    rpt.xrlNit2.Text = .Nit
                    rpt.xrlPedido2.Text = .OrdenCompra
                    rpt.xrlNrc2.Text = .Nrc
                    rpt.xrlGiro2.Text = .Giro
                    rpt.xrlDireccion2.Text = .Direccion
                    rpt.xrlTelefono2.Text = .Telefono
                    rpt.xrlMunic2.Text = Municipios.Nombre
                    rpt.xrlDepto2.Text = Departamentos.Nombre
                    rpt.xrlFormaPago2.Text = FormaPago.Nombre
                    rpt.xrlVendedor2.Text = Vendedores.Nombre
                    rpt.xrlVentaACuenta2.Text = .VentaAcuentaDe
                    rpt.xrlDiasCredito2.Text = .DiasCredito
                    rpt.xrlVentaAfecta2.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal2.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta2.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlIvaRetenido2.Text = Format(.TotalImpuesto1, "##,###.00")
                    rpt.xrlCESC2.Text = Format(.TotalImpuesto2, "##,###.00")
                    rpt.xrlTotal2.Text = Format(.TotalComprobante, "###,##0.00")
                    rpt.xrlCantLetras2.Text = CantidaLetras
                    rpt.xrlComentario2.Text = .Comentario

                    rpt.ShowPrintMarginsWarning = False
                    rpt.ShowPreviewDialog()
                End With
            Case 7 'factura de exportación
                If gsNombre_Empresa.ToUpper().Contains("S1 EL SALVADOR") Then 'cambio exclusivo para GS1(CLIENTE DE AE INFORMATICA)
                    Dim rpt1 As New fac_rptFEXGS1
                    rpt1.DataSource = dt
                    rpt1.DataMember = ""
                    With Ventas
                        rpt1.xrlNumero.Text = .Numero
                        rpt1.xrlNombre1.Text = .Nombre
                        rpt1.xrlFecha1.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlNit1.Text = .Nit
                        rpt1.xrlNrc1.Text = .Nrc
                        rpt1.xrlGiro1.Text = .Giro
                        rpt1.xrlFormaPago1.Text = dt.Rows(0).Item("FormaPago")
                        rpt1.xrlDirec1.Text = .Direccion & " " & Municipios.Nombre & " " & Departamentos.Nombre
                        'DETALLE 1
                        If dt.Rows.Count > 0 Then
                            rpt1.xrlCant11.Text = dt.Rows(0).Item("Cantidad")
                            rpt1.xrlDesc11.Text = dt.Rows(0).Item("Descripcion")
                            rpt1.xrlPrecio11.Text = dt.Rows(0).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta11.Text = dt.Rows(0).Item("VentaExenta")
                            rpt1.xrlTotal11.Text = dt.Rows(0).Item("VentaAfecta")
                        End If
                        'EL DETALLE 2
                        If dt.Rows.Count > 1 Then
                            rpt1.xrlCant12.Text = dt.Rows(1).Item("Cantidad")
                            rpt1.xrlDesc12.Text = dt.Rows(1).Item("Descripcion")
                            rpt1.xrlPrecio12.Text = dt.Rows(1).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta12.Text = dt.Rows(1).Item("VentaExenta")
                            rpt1.xrlTotal12.Text = dt.Rows(1).Item("VentaAfecta")
                        End If
                        'EL DETALLE 3
                        If dt.Rows.Count > 2 Then
                            rpt1.xrlCant13.Text = dt.Rows(2).Item("Cantidad")
                            rpt1.xrlDesc13.Text = dt.Rows(2).Item("Descripcion")
                            rpt1.xrlPrecio13.Text = dt.Rows(2).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta13.Text = dt.Rows(2).Item("VentaExenta")
                            rpt1.xrlTotal13.Text = dt.Rows(2).Item("VentaAfecta")
                        End If
                        'EL DETALLE 4
                        If dt.Rows.Count > 3 Then
                            rpt1.xrlCant14.Text = dt.Rows(3).Item("Cantidad")
                            rpt1.xrlDesc14.Text = dt.Rows(3).Item("Descripcion")
                            rpt1.xrlPrecio14.Text = dt.Rows(3).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta14.Text = dt.Rows(3).Item("VentaExenta")
                            rpt1.xrlTotal14.Text = dt.Rows(3).Item("VentaAfecta")
                        End If
                        'EL DETALLE 5
                        If dt.Rows.Count > 4 Then
                            rpt1.xrlCant15.Text = dt.Rows(4).Item("Cantidad")
                            rpt1.xrlDesc15.Text = dt.Rows(4).Item("Descripcion")
                            rpt1.xrlPrecio15.Text = dt.Rows(4).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta15.Text = dt.Rows(4).Item("VentaExenta")
                            rpt1.xrlTotal15.Text = dt.Rows(4).Item("VentaAfecta")
                        End If
                        'EL DETALLE 6
                        If dt.Rows.Count > 5 Then
                            rpt1.xrlCant16.Text = dt.Rows(5).Item("Cantidad")
                            rpt1.xrlDesc16.Text = dt.Rows(5).Item("Descripcion")
                            rpt1.xrlPrecio16.Text = dt.Rows(5).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta16.Text = dt.Rows(5).Item("VentaExenta")
                            rpt1.xrlTotal16.Text = dt.Rows(5).Item("VentaAfecta")
                        End If
                        'EL DETALLE 7
                        If dt.Rows.Count > 6 Then
                            rpt1.xrlCant17.Text = dt.Rows(6).Item("Cantidad")
                            rpt1.xrlDesc17.Text = dt.Rows(6).Item("Descripcion")
                            rpt1.xrlPrecio17.Text = dt.Rows(6).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta17.Text = dt.Rows(6).Item("VentaExenta")
                            rpt1.xrlTotal17.Text = dt.Rows(6).Item("VentaAfecta")
                        End If
                        'EL TOTAL 1
                        rpt1.xrlTotalAfecto1.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlIva1.Text = Format(.TotalIva, "###,##0.00")
                        If .TotalImpuesto1 = 0 Then
                            rpt1.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                        Else
                            rpt1.xrlIvaRetenido.Text = "(" & Format(.TotalImpuesto1, "###,##0.00") & ")"
                        End If
                        rpt1.xrlSubTotal1.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlTotalExento1.Text = Format(.TotalExento, "###,##0.00")
                        rpt1.xrlTotal1.Text = Format(.TotalComprobante, "###,##0.00")
                        Dim Decimales = String.Format("{0:c}", .TotalComprobante)
                        Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                        rpt1.xrlCantLetras1.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"

                        'LA COPIA DEL CRÉDITO FISCAL 2
                        rpt1.xrlNombre2.Text = .Nombre
                        rpt1.xrlFecha2.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlNit2.Text = .Nit
                        rpt1.xrlNrc2.Text = .Nrc
                        rpt1.xrlGiro2.Text = .Giro
                        rpt1.xrlFormaPago2.Text = dt.Rows(0).Item("FormaPago")
                        rpt1.xrlDirec2.Text = .Direccion & " " & Municipios.Nombre & " " & Departamentos.Nombre

                        'DETALLE 1
                        If dt.Rows.Count > 0 Then
                            rpt1.xrlCant21.Text = dt.Rows(0).Item("Cantidad")
                            rpt1.xrlDesc21.Text = dt.Rows(0).Item("Descripcion")
                            rpt1.xrlPrecio21.Text = dt.Rows(0).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta21.Text = dt.Rows(0).Item("VentaExenta")
                            rpt1.xrlTotal21.Text = dt.Rows(0).Item("VentaAfecta")
                        End If
                        'EL DETALLE 2
                        If dt.Rows.Count > 1 Then
                            rpt1.xrlCant22.Text = dt.Rows(1).Item("Cantidad")
                            rpt1.xrlDesc22.Text = dt.Rows(1).Item("Descripcion")
                            rpt1.xrlPrecio22.Text = dt.Rows(1).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta22.Text = dt.Rows(1).Item("VentaExenta")
                            rpt1.xrlTotal22.Text = dt.Rows(1).Item("VentaAfecta")
                        End If
                        If dt.Rows.Count > 2 Then
                            rpt1.xrlCant23.Text = dt.Rows(2).Item("Cantidad")
                            rpt1.xrlDesc23.Text = dt.Rows(2).Item("Descripcion")
                            rpt1.xrlPrecio23.Text = dt.Rows(2).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta23.Text = dt.Rows(2).Item("VentaExenta")
                            rpt1.xrlTotal23.Text = dt.Rows(2).Item("VentaAfecta")
                        End If
                        If dt.Rows.Count > 3 Then
                            rpt1.xrlCant24.Text = dt.Rows(3).Item("Cantidad")
                            rpt1.xrlDesc24.Text = dt.Rows(3).Item("Descripcion")
                            rpt1.xrlPrecio24.Text = dt.Rows(3).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta24.Text = dt.Rows(3).Item("VentaExenta")
                            rpt1.xrlTotal24.Text = dt.Rows(3).Item("VentaAfecta")
                        End If
                        'EL DETALLE 5
                        If dt.Rows.Count > 4 Then
                            rpt1.xrlCant25.Text = dt.Rows(4).Item("Cantidad")
                            rpt1.xrlDesc25.Text = dt.Rows(4).Item("Descripcion")
                            rpt1.xrlPrecio25.Text = dt.Rows(4).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta25.Text = dt.Rows(4).Item("VentaExenta")
                            rpt1.xrlTotal25.Text = dt.Rows(4).Item("VentaAfecta")
                        End If
                        'EL DETALLE 6
                        If dt.Rows.Count > 5 Then
                            rpt1.xrlCant26.Text = dt.Rows(5).Item("Cantidad")
                            rpt1.xrlDesc26.Text = dt.Rows(5).Item("Descripcion")
                            rpt1.xrlPrecio26.Text = dt.Rows(5).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta26.Text = dt.Rows(5).Item("VentaExenta")
                            rpt1.xrlTotal26.Text = dt.Rows(5).Item("VentaAfecta")
                        End If
                        'EL DETALLE 7
                        If dt.Rows.Count > 6 Then
                            rpt1.xrlCant27.Text = dt.Rows(6).Item("Cantidad")
                            rpt1.xrlDesc27.Text = dt.Rows(6).Item("Descripcion")
                            rpt1.xrlPrecio27.Text = dt.Rows(6).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta27.Text = dt.Rows(6).Item("VentaExenta")
                            rpt1.xrlTotal27.Text = dt.Rows(6).Item("VentaAfecta")
                        End If
                        'EL TOTAL 2
                        rpt1.xrlTotalAfecto2.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlIva2.Text = Format(.TotalIva, "###,##0.00")
                        If .TotalImpuesto1 = 0 Then
                            rpt1.xrlIvaRetenido2.Text = Format(.TotalImpuesto1, "###,##0.00")
                        Else
                            rpt1.xrlIvaRetenido2.Text = "(" & Format(.TotalImpuesto1, "###,##0.00") & ")"
                        End If
                        'rpt1.xrlCesc2.Text = Format(.TotalImpuesto2, "##,###.00")
                        rpt1.xrlSubTotal2.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlTotalExento2.Text = Format(.TotalExento, "###,##0.00")
                        rpt1.xrlTotal2.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlCantLetras2.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"


                        'LA COPIA 3 DEL CCF
                        rpt1.xrlNombre3.Text = .Nombre
                        rpt1.xrlFecha3.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlNit3.Text = .Nit
                        rpt1.xrlNrc3.Text = .Nrc
                        rpt1.xrlGiro3.Text = .Giro
                        rpt1.xrlFormaPago3.Text = dt.Rows(0).Item("FormaPago")
                        rpt1.xrlDirec3.Text = .Direccion & " " & Municipios.Nombre & " " & Departamentos.Nombre

                        'DETALLE 1
                        If dt.Rows.Count > 0 Then
                            rpt1.xrlCant31.Text = dt.Rows(0).Item("Cantidad")
                            rpt1.xrlDesc31.Text = dt.Rows(0).Item("Descripcion")
                            rpt1.xrlPrecio31.Text = dt.Rows(0).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta31.Text = dt.Rows(0).Item("VentaExenta")
                            rpt1.xrlTotal31.Text = dt.Rows(0).Item("VentaAfecta")
                        End If
                        'EL DETALLE 2
                        If dt.Rows.Count > 1 Then
                            rpt1.xrlCant32.Text = dt.Rows(1).Item("Cantidad")
                            rpt1.xrlDesc32.Text = dt.Rows(1).Item("Descripcion")
                            rpt1.xrlPrecio32.Text = dt.Rows(1).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta32.Text = dt.Rows(1).Item("VentaExenta")
                            rpt1.xrlTotal32.Text = dt.Rows(1).Item("VentaAfecta")
                        End If
                        If dt.Rows.Count > 2 Then
                            rpt1.xrlCant33.Text = dt.Rows(2).Item("Cantidad")
                            rpt1.xrlDesc33.Text = dt.Rows(2).Item("Descripcion")
                            rpt1.xrlPrecio33.Text = dt.Rows(2).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta33.Text = dt.Rows(2).Item("VentaExenta")
                            rpt1.xrlTotal33.Text = dt.Rows(2).Item("VentaAfecta")
                        End If
                        If dt.Rows.Count > 3 Then
                            rpt1.xrlCant34.Text = dt.Rows(3).Item("Cantidad")
                            rpt1.xrlDesc34.Text = dt.Rows(3).Item("Descripcion")
                            rpt1.xrlPrecio34.Text = dt.Rows(3).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta34.Text = dt.Rows(3).Item("VentaExenta")
                            rpt1.xrlTotal34.Text = dt.Rows(3).Item("VentaAfecta")
                        End If
                        'EL DETALLE 5
                        If dt.Rows.Count > 4 Then
                            rpt1.xrlCant35.Text = dt.Rows(4).Item("Cantidad")
                            rpt1.xrlDesc35.Text = dt.Rows(4).Item("Descripcion")
                            rpt1.xrlPrecio35.Text = dt.Rows(4).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta35.Text = dt.Rows(4).Item("VentaExenta")
                            rpt1.xrlTotal35.Text = dt.Rows(4).Item("VentaAfecta")
                        End If
                        'EL DETALLE 6
                        If dt.Rows.Count > 5 Then
                            rpt1.xrlCant36.Text = dt.Rows(5).Item("Cantidad")
                            rpt1.xrlDesc36.Text = dt.Rows(5).Item("Descripcion")
                            rpt1.xrlPrecio36.Text = dt.Rows(5).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta36.Text = dt.Rows(5).Item("VentaExenta")
                            rpt1.xrlTotal36.Text = dt.Rows(5).Item("VentaAfecta")
                        End If
                        'EL DETALLE 7
                        If dt.Rows.Count > 6 Then
                            rpt1.xrlCant37.Text = dt.Rows(6).Item("Cantidad")
                            rpt1.xrlDesc37.Text = dt.Rows(6).Item("Descripcion")
                            rpt1.xrlPrecio37.Text = dt.Rows(6).Item("PrecioUnitario")
                            rpt1.xrlVentaExenta37.Text = dt.Rows(6).Item("VentaExenta")
                            rpt1.xrlTotal37.Text = dt.Rows(6).Item("VentaAfecta")
                        End If
                        'EL TOTAL 3
                        rpt1.xrlTotalAfecto3.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlIva3.Text = Format(.TotalIva, "###,##0.00")
                        If .TotalImpuesto1 = 0 Then
                            rpt1.xrlIvaRetenido3.Text = Format(.TotalImpuesto1, "###,##0.00")
                        Else
                            rpt1.xrlIvaRetenido3.Text = "(" & Format(.TotalImpuesto1, "###,##0.00") & ")"
                        End If
                        ' rpt1.xrlCesc3.Text = Format(.TotalImpuesto2, "##,###.00")
                        rpt1.xrlSubTotal3.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlTotalExento3.Text = Format(.TotalExento, "###,##0.00")
                        rpt1.xrlTotal3.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlCantLetras3.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"

                        'LAS COLITAS
                        rpt1.xrlClient1.Text = .IdCliente & "   " & .Nombre
                        rpt1.xrlDoc1.Text = EntTipoDoc.Abreviatura & "-" & .Numero
                        rpt1.xrlFec1.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlSub1.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlImp1.Text = Format(.TotalIva, "###,##0.00")
                        rpt1.xrlTot1.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlArt1.Text = dt.Rows(0).Item("Descripcion")

                        rpt1.xrlClient2.Text = .IdCliente & "   " & .Nombre
                        rpt1.xrlDoc2.Text = EntTipoDoc.Abreviatura & "-" & .Numero
                        rpt1.xrlFec2.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlSub2.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlImp2.Text = Format(.TotalIva, "###,##0.00")
                        rpt1.xrlTot2.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlArt2.Text = dt.Rows(0).Item("Descripcion")
                    End With
                    rpt1.ShowPrintMarginsWarning = False
                    rpt1.ShowPreviewDialog()
                    Exit Function
                End If
                Dim Template = Application.StartupPath & "\Plantillas\feNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFactura
                rpt.LoadLayout(Template)

                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumRemision.Text = .Telefono
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    Dim CantidaLetras As String = Num2Text(Int(IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))) & Decimales & " DÓLARES"
                    rpt.xrlCantLetras.Text = CantidaLetras
                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPrintMarginsWarning = False
                    rpt.ShowPreviewDialog()
                End With
            Case 8 'nota de crédito
                If gsNombre_Empresa.ToUpper().Contains("S1 EL SALVADOR") Then 'cambio exclusivo para GS1(CLIENTE DE AE INFORMATICA)
                    Dim rpt1 As New fac_rptCCFGS1
                    For i = 0 To dt.Rows.Count - 1
                        dt.Rows(i)("Cantidad") = 0 - dt.Rows(i)("Cantidad")
                        dt.Rows(i)("PrecioUnitario") = 0 - dt.Rows(i)("PrecioUnitario")
                        dt.Rows(i)("VentaExenta") = 0 - dt.Rows(i)("VentaExenta")
                        dt.Rows(i)("VentaNoSujeta") = 0 - dt.Rows(i)("VentaNoSujeta")
                        dt.Rows(i)("VentaNeta") = 0 - dt.Rows(i)("VentaNeta")
                        dt.Rows(i)("VentaAfecta") = 0 - dt.Rows(i)("VentaAfecta")
                    Next
                    rpt1.DataSource = dt
                    rpt1.DataMember = ""
                    With Ventas

                        rpt1.xrlNumero.Text = .Numero

                        rpt1.xrlNombre1.Text = .Nombre
                        rpt1.xrlFecha1.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlNit1.Text = .Nit
                        rpt1.xrlNrc1.Text = .Nrc

                        rpt1.xrlGiro1.Text = .Giro
                        rpt1.xrlFormaPago1.Text = dt.Rows(0).Item("FormaPago")
                        rpt1.xrlDirec1.Text = .Direccion & " " & Municipios.Nombre & " " & Departamentos.Nombre

                        'DETALLE 1
                        If dt.Rows.Count > 0 Then
                            rpt1.xrlCant11.Text = Format(dt.Rows(0).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc11.Text = dt.Rows(0).Item("Descripcion")
                            rpt1.xrlPrecio11.Text = Format(dt.Rows(0).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta11.Text = dt.Rows(0).Item("VentaExenta")
                            rpt1.xrlTotal11.Text = Format(dt.Rows(0).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 2
                        If dt.Rows.Count > 1 Then
                            rpt1.xrlCant12.Text = Format(dt.Rows(1).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc12.Text = dt.Rows(1).Item("Descripcion")
                            rpt1.xrlPrecio12.Text = Format(dt.Rows(1).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta12.Text = dt.Rows(1).Item("VentaExenta")
                            rpt1.xrlTotal12.Text = Format(dt.Rows(1).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 3
                        If dt.Rows.Count > 2 Then
                            rpt1.xrlCant13.Text = Format(dt.Rows(2).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc13.Text = dt.Rows(2).Item("Descripcion")
                            rpt1.xrlPrecio13.Text = Format(dt.Rows(2).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta13.Text = dt.Rows(2).Item("VentaExenta")
                            rpt1.xrlTotal13.Text = Format(dt.Rows(2).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 4
                        If dt.Rows.Count > 3 Then
                            rpt1.xrlCant14.Text = Format(dt.Rows(3).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc14.Text = dt.Rows(3).Item("Descripcion")
                            rpt1.xrlPrecio14.Text = Format(dt.Rows(3).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta14.Text = dt.Rows(3).Item("VentaExenta")
                            rpt1.xrlTotal14.Text = Format(dt.Rows(3).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 5
                        If dt.Rows.Count > 4 Then
                            rpt1.xrlCant15.Text = Format(dt.Rows(4).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc15.Text = dt.Rows(4).Item("Descripcion")
                            rpt1.xrlPrecio15.Text = Format(dt.Rows(4).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta15.Text = dt.Rows(4).Item("VentaExenta")
                            rpt1.xrlTotal15.Text = Format(dt.Rows(4).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 6
                        If dt.Rows.Count > 5 Then
                            rpt1.xrlCant16.Text = Format(dt.Rows(5).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc16.Text = dt.Rows(5).Item("Descripcion")
                            rpt1.xrlPrecio16.Text = Format(dt.Rows(5).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta16.Text = dt.Rows(5).Item("VentaExenta")
                            rpt1.xrlTotal16.Text = Format(dt.Rows(5).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 7
                        If dt.Rows.Count > 6 Then
                            rpt1.xrlCant17.Text = Format(dt.Rows(6).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc17.Text = dt.Rows(6).Item("Descripcion")
                            rpt1.xrlPrecio17.Text = Format(dt.Rows(6).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta17.Text = dt.Rows(6).Item("VentaExenta")
                            rpt1.xrlTotal17.Text = Format(dt.Rows(6).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL TOTAL 1
                        rpt1.xrlTotalAfecto1.Text = Format(0 - .TotalAfecto, "###,##0.00")
                        rpt1.xrlIva1.Text = Format(0 - .TotalIva, "###,##0.00")
                        rpt1.xrlIvaRetenido.Text = Format(0 - .TotalImpuesto1, "###,##0.00")
                        'rpt1.xrlCesc1.Text = Format(.TotalImpuesto2, "##,###.00")
                        rpt1.xrlSubTotal1.Text = Format(0 - .TotalIva + 0 - .TotalAfecto, "###,##0.00")
                        rpt1.xrlTotalExento1.Text = Format(0 - .TotalExento, "###,##0.00")
                        rpt1.xrlTotal1.Text = Format(0 - .TotalComprobante, "###,##0.00")
                        Dim Decimales = String.Format("{0:c}", 0 - .TotalComprobante)
                        Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                        rpt1.xrlCantLetras1.Text = Num2Text(Int(0 - .TotalComprobante)) & Decimales & " DÓLARES"


                        'LA COPIA DEL CRÉDITO FISCAL 2
                        rpt1.xrlNombre2.Text = .Nombre
                        rpt1.xrlFecha2.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlNit2.Text = .Nit
                        rpt1.xrlNrc2.Text = .Nrc
                        rpt1.xrlGiro2.Text = .Giro
                        rpt1.xrlFormaPago2.Text = dt.Rows(0).Item("FormaPago")
                        rpt1.xrlDirec2.Text = .Direccion & " " & Municipios.Nombre & " " & Departamentos.Nombre

                        'DETALLE 1
                        If dt.Rows.Count > 0 Then
                            rpt1.xrlCant21.Text = Format(dt.Rows(0).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc21.Text = dt.Rows(0).Item("Descripcion")
                            rpt1.xrlPrecio21.Text = Format(dt.Rows(0).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta21.Text = dt.Rows(0).Item("VentaExenta")
                            rpt1.xrlTotal21.Text = Format(dt.Rows(0).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 2
                        If dt.Rows.Count > 1 Then
                            rpt1.xrlCant22.Text = Format(dt.Rows(1).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc22.Text = dt.Rows(1).Item("Descripcion")
                            rpt1.xrlPrecio22.Text = Format(dt.Rows(1).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta22.Text = dt.Rows(1).Item("VentaExenta")
                            rpt1.xrlTotal22.Text = Format(dt.Rows(1).Item("VentaAfecta"), "###,##0.00")
                        End If
                        If dt.Rows.Count > 2 Then
                            rpt1.xrlCant23.Text = Format(dt.Rows(2).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc23.Text = dt.Rows(2).Item("Descripcion")
                            rpt1.xrlPrecio23.Text = Format(dt.Rows(2).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta23.Text = dt.Rows(2).Item("VentaExenta")
                            rpt1.xrlTotal23.Text = Format(dt.Rows(2).Item("VentaAfecta"), "###,##0.00")
                        End If
                        If dt.Rows.Count > 3 Then
                            rpt1.xrlCant24.Text = Format(dt.Rows(3).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc24.Text = dt.Rows(3).Item("Descripcion")
                            rpt1.xrlPrecio24.Text = Format(dt.Rows(3).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta24.Text = dt.Rows(3).Item("VentaExenta")
                            rpt1.xrlTotal24.Text = Format(dt.Rows(3).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 5
                        If dt.Rows.Count > 4 Then
                            rpt1.xrlCant25.Text = Format(dt.Rows(4).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc25.Text = dt.Rows(4).Item("Descripcion")
                            rpt1.xrlPrecio25.Text = Format(dt.Rows(4).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta25.Text = dt.Rows(4).Item("VentaExenta")
                            rpt1.xrlTotal25.Text = Format(dt.Rows(4).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 6
                        If dt.Rows.Count > 5 Then
                            rpt1.xrlCant26.Text = Format(dt.Rows(5).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc26.Text = dt.Rows(5).Item("Descripcion")
                            rpt1.xrlPrecio26.Text = Format(dt.Rows(5).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta26.Text = dt.Rows(5).Item("VentaExenta")
                            rpt1.xrlTotal26.Text = Format(dt.Rows(5).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 7
                        If dt.Rows.Count > 6 Then
                            rpt1.xrlCant27.Text = Format(dt.Rows(6).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc27.Text = dt.Rows(6).Item("Descripcion")
                            rpt1.xrlPrecio27.Text = Format(dt.Rows(6).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta27.Text = dt.Rows(6).Item("VentaExenta")
                            rpt1.xrlTotal27.Text = Format(dt.Rows(6).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL TOTAL 2
                        rpt1.xrlTotalAfecto2.Text = Format(0 - .TotalAfecto, "###,##0.00")
                        rpt1.xrlIva2.Text = Format(0 - .TotalIva, "###,##0.00")
                        rpt1.xrlIvaRetenido2.Text = Format(0 - .TotalImpuesto1, "###,##0.00")
                        'rpt1.xrlCesc2.Text = Format(.TotalImpuesto2, "##,###.00")
                        rpt1.xrlSubTotal2.Text = Format(0 - .TotalIva + 0 - .TotalAfecto, "###,##0.00")
                        rpt1.xrlTotalExento2.Text = Format(0 - .TotalExento, "###,##0.00")
                        rpt1.xrlTotal2.Text = Format(0 - .TotalComprobante, "###,##0.00")
                        rpt1.xrlCantLetras2.Text = Num2Text(Int(0 - .TotalComprobante)) & Decimales & " DÓLARES"


                        'LA COPIA 3 DEL CCF
                        rpt1.xrlNombre3.Text = .Nombre
                        rpt1.xrlFecha3.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlNit3.Text = .Nit
                        rpt1.xrlNrc3.Text = .Nrc
                        rpt1.xrlGiro3.Text = .Giro
                        rpt1.xrlFormaPago3.Text = dt.Rows(0).Item("FormaPago")
                        rpt1.xrlDirec3.Text = .Direccion & " " & Municipios.Nombre & " " & Departamentos.Nombre

                        'DETALLE 1
                        If dt.Rows.Count > 0 Then
                            rpt1.xrlCant31.Text = Format(dt.Rows(0).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc31.Text = dt.Rows(0).Item("Descripcion")
                            rpt1.xrlPrecio31.Text = Format(dt.Rows(0).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta31.Text = dt.Rows(0).Item("VentaExenta")
                            rpt1.xrlTotal31.Text = Format(dt.Rows(0).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 2
                        If dt.Rows.Count > 1 Then
                            rpt1.xrlCant32.Text = Format(dt.Rows(1).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc32.Text = dt.Rows(1).Item("Descripcion")
                            rpt1.xrlPrecio32.Text = Format(dt.Rows(1).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta32.Text = dt.Rows(1).Item("VentaExenta")
                            rpt1.xrlTotal32.Text = Format(dt.Rows(1).Item("VentaAfecta"), "###,##0.00")
                        End If
                        If dt.Rows.Count > 2 Then
                            rpt1.xrlCant33.Text = Format(dt.Rows(2).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc33.Text = dt.Rows(2).Item("Descripcion")
                            rpt1.xrlPrecio33.Text = Format(dt.Rows(2).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta33.Text = dt.Rows(2).Item("VentaExenta")
                            rpt1.xrlTotal33.Text = Format(dt.Rows(2).Item("VentaAfecta"), "###,##0.00")
                        End If
                        If dt.Rows.Count > 3 Then
                            rpt1.xrlCant34.Text = Format(dt.Rows(3).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc34.Text = dt.Rows(3).Item("Descripcion")
                            rpt1.xrlPrecio34.Text = Format(dt.Rows(3).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta34.Text = dt.Rows(3).Item("VentaExenta")
                            rpt1.xrlTotal34.Text = Format(dt.Rows(3).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 5
                        If dt.Rows.Count > 4 Then
                            rpt1.xrlCant35.Text = Format(dt.Rows(4).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc35.Text = dt.Rows(4).Item("Descripcion")
                            rpt1.xrlPrecio35.Text = Format(dt.Rows(4).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta35.Text = dt.Rows(4).Item("VentaExenta")
                            rpt1.xrlTotal35.Text = Format(dt.Rows(4).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 6
                        If dt.Rows.Count > 5 Then
                            rpt1.xrlCant36.Text = Format(dt.Rows(5).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc36.Text = dt.Rows(5).Item("Descripcion")
                            rpt1.xrlPrecio36.Text = Format(dt.Rows(5).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta36.Text = dt.Rows(5).Item("VentaExenta")
                            rpt1.xrlTotal36.Text = Format(dt.Rows(5).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL DETALLE 7
                        If dt.Rows.Count > 6 Then
                            rpt1.xrlCant37.Text = Format(dt.Rows(6).Item("Cantidad"), "##0.00")
                            rpt1.xrlDesc37.Text = dt.Rows(6).Item("Descripcion")
                            rpt1.xrlPrecio37.Text = Format(dt.Rows(6).Item("PrecioUnitario"), "###,##0.0000")
                            rpt1.xrlVentaExenta37.Text = dt.Rows(6).Item("VentaExenta")
                            rpt1.xrlTotal37.Text = Format(dt.Rows(6).Item("VentaAfecta"), "###,##0.00")
                        End If
                        'EL TOTAL 3
                        rpt1.xrlTotalAfecto3.Text = Format(0 - .TotalAfecto, "###,##0.00")
                        rpt1.xrlIva3.Text = Format(0 - .TotalIva, "###,##0.00")
                        rpt1.xrlIvaRetenido3.Text = Format(0 - .TotalImpuesto1, "###,##0.00")
                        ' rpt1.xrlCesc3.Text = Format(.TotalImpuesto2, "##,###.00")
                        rpt1.xrlSubTotal3.Text = Format(0 - .TotalIva + 0 - .TotalAfecto, "###,##0.00")
                        rpt1.xrlTotalExento3.Text = Format(0 - .TotalExento, "###,##0.00")
                        rpt1.xrlTotal3.Text = Format(0 - .TotalComprobante, "###,##0.00")
                        rpt1.xrlCantLetras3.Text = Num2Text(Int(0 - .TotalComprobante)) & Decimales & " DÓLARES"

                        'LAS COLITAS
                        rpt1.xrlClient1.Text = .IdCliente & "   " & .Nombre
                        rpt1.xrlDoc1.Text = EntTipoDoc.Abreviatura & "-" & .Numero
                        rpt1.xrlFec1.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlSub1.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlImp1.Text = Format(.TotalIva, "###,##0.00")
                        rpt1.xrlTot1.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlArt1.Text = dt.Rows(0).Item("Descripcion")

                        rpt1.xrlClient2.Text = .IdCliente & "   " & .Nombre
                        rpt1.xrlDoc2.Text = EntTipoDoc.Abreviatura & "-" & .Numero
                        rpt1.xrlFec2.Text = Format(.Fecha, "dd/MM/yyyy")
                        rpt1.xrlSub2.Text = Format(.TotalAfecto, "###,##0.00")
                        rpt1.xrlImp2.Text = Format(.TotalIva, "###,##0.00")
                        rpt1.xrlTot2.Text = Format(.TotalComprobante, "###,##0.00")
                        rpt1.xrlArt2.Text = dt.Rows(0).Item("Descripcion")
                    End With
                    rpt1.ShowPrintMarginsWarning = False
                    rpt1.ShowPreviewDialog()
                    Exit Function
                End If
                Dim Template = Application.StartupPath & "\Plantillas\ncNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)

                For i = 0 To dt.Rows.Count - 1
                    dt.Rows(i)("Cantidad") = 0 - dt.Rows(i)("Cantidad")
                    dt.Rows(i)("PrecioUnitario") = 0 - dt.Rows(i)("PrecioUnitario")
                    dt.Rows(i)("VentaExenta") = 0 - dt.Rows(i)("VentaExenta")
                    dt.Rows(i)("VentaNoSujeta") = 0 - dt.Rows(i)("VentaNoSujeta")
                    dt.Rows(i)("VentaNeta") = 0 - dt.Rows(i)("VentaNeta")
                    dt.Rows(i)("VentaAfecta") = 0 - dt.Rows(i)("VentaAfecta")
                Next

                Dim DocumentosAfectados As String = ""
                For j = 0 To dtAfectaNc.Rows.Count - 1
                    DocumentosAfectados += dtAfectaNc.Rows(j)("NumComprobanteVenta") + ", "
                Next

                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlNumRemision.Text = .Telefono
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlDocAfectados.Text = DocumentosAfectados
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVentaAfecta.Text = Format(0 - .TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(0 - .TotalIva, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(0 - .TotalIva + 0 - Ventas.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(0 - .TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(0 - .TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", 0 - .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                    rpt.xrlCantLetras.Text = Num2Text(Int(0 - Ventas.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPreviewDialog()
                End With
            Case 9 'nota de débito
                Dim Template = Application.StartupPath & "\Plantillas\ndNexus" & NumFormato & ".repx"

                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumRemision.Text = .Telefono
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(.TotalIva, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalIva + Ventas.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                    rpt.xrlCantLetras.Text = Num2Text(Int(Ventas.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPreviewDialog()
                End With
            Case 10 'tickette de caja se imprime a puro código
                'MsgBox("No es posible re-imprimir tickete", MsgBoxStyle.Information, "Nota")
                Dim strLinea As String = ""
                Dim strDescripcion As String = ""

                Dim str As String = "", Ancho As Integer = 0, s As String = ""
                Dim dtv As DataTable = blFac.fac_ObtenerDetalleDocumento("fac_ventasDetalle", Ventas.IdComprobante)

                str = Chr(27) & Chr(99) & Chr(48) & Chr(3) 'para que imprima en la cinta de auditoría
                str += Chr(27) + "z" + Chr(1) 'activación del modo de impresión en auditoría
                str += Chr(27) & "p" & Chr(0) 'para abrir la gaveta del cash drawer
                str += DatosEncabezado

                str += "CONTROL No. " & Trim(Ventas.IdComprobante)
                s = "TICKET No. " & Ventas.Numero

                Ancho = 40 - Len(s)
                str += s.PadLeft(Ancho, " ") & Chr(10)

                str += "FECHA: " & Format(Ventas.Fecha, "dd/MM/yyyy")
                s = "HORA: " & Now.ToString("HH:mm:ss")

                str += s.PadLeft(23, " ") & Chr(10)
                str += "CAJA No. " & Ventas.IdPunto & Chr(10)
                s = "CLIENTE: " & Ventas.Nombre + Space(30)
                str += s.Substring(0, 39) & Chr(10)
                s = "VENDEDOR: " & Vendedores.Nombre.ToString + Space(30)
                str += s.Substring(0, 39) & Chr(10) & Chr(10)
                If Ventas.EsDevolucion Then
                    str += "    TICKET DE DEVOLUCION" & Chr(10) & Chr(10)
                End If
                Dim TotalExento As Decimal = 0.0, TotalAfecto As Decimal = 0.0, Vta As String = "G"
                For Each Fila As DataRow In dtv.Rows
                    TotalExento += CDec(Fila.Item("VentaExenta"))
                    TotalAfecto += CDec(Fila.Item("VentaAfecta"))
                    Vta = " G"
                    If CDec(Fila.Item("VentaExenta")) > 0.0 Then
                        Vta = " E"
                    End If
                    ''.Substring(1, 30)
                    ''strPrueba += (Fila.Item("Descripcion").ToString()).Substring(0, 25) + Vta & Chr(10)

                    '' str += Fila.Item("Descripcion").ToString() + Vta & Chr(10)
                    strLinea = ""
                    strDescripcion = (Fila.Item("Descripcion").ToString())

                    If Len(strDescripcion) > 16 Then
                        '' str += (Fila.Item("Descripcion").ToString()).Substring(0, 13) + Vta & " "
                        strLinea += (Fila.Item("Descripcion").ToString()).Substring(0, 16) & " "
                    Else
                        '' str += (Fila.Item("Descripcion").ToString()) + Vta & " "
                        strLinea += (Fila.Item("Descripcion").ToString()) & " "
                    End If

                    ''& Chr(10)
                    s = Format(Fila.Item("Cantidad"), "#,##0.00") + "X" + Format(Fila.Item("PrecioUnitario"), "#,##0.00") & " "
                    ''str += s
                    strLinea += s
                    If CDec(Fila.Item("VentaExenta")) > 0.0 Then
                        s = Format(Fila.Item("VentaExenta"), "##,##0.00") & Vta
                    Else
                        s = Format(Fila.Item("VentaAfecta"), "##,##0.00") & Vta
                    End If
                    Ancho = 40 - Len(strLinea)
                    If Ancho > 0 And Ancho > Len(s) Then
                        strLinea += s.PadLeft(Ancho, " ")
                    Else
                        strLinea = strLinea & " " & s
                    End If

                    str += strLinea
                    ''str += s.PadLeft(Ancho, " ")
                    ''str += s
                    str += Chr(10)
                Next
                str += Chr(10)
                s = Format(TotalExento, "$##,##0.00")
                str += "Sub-Total Exento:"
                str += s.PadLeft(23, " ") & Chr(10)

                s = Format(0, "$##,##0.00")
                str += "Sub-Total No Sujeto:"
                str += s.PadLeft(20, " ") & Chr(10)

                s = Format(TotalAfecto, "$##,##0.00")
                str += "Sub-Total Gravado:"
                str += s.PadLeft(22, " ") & Chr(10)
                str += "TOTAL:"
                s = Format(TotalExento + TotalAfecto, "$##,##0.00")
                str += s.PadLeft(34, " ") & Chr(10)

                str += "EFECTIVO RECIBIDO:"   '18 caracteres
                s = Format(Ventas.TotalPagado, "$##,##0.00")
                str += s.PadLeft(22, " ") & Chr(10)

                str += "VUELTO A ENTREGAR:"   '18 caracteres
                s = Format(Ventas.TotalPagado - TotalExento - TotalAfecto, "$##,##0.00")
                str += s.PadLeft(22, " ") & Chr(10) & Chr(10)

                str += "G= VENTA GRAVADA, E= VENTA EXENTA" & Chr(10)
                str += "N= VENTA NO SUJETA" & Chr(10) & Chr(10)
                If Ventas.TotalComprobante > 200 Or Ventas.EsDevolucion Then 'SI LA VENTA ES MAYOR A $200.00 O ES UNA DEVOLUCION
                    If Ventas.EsDevolucion Then
                        str += "TICKETE DE DEVOLUCION. FAVOR FIRMAR" & Chr(10)
                    Else
                        str += "VENTA MAYOR O IGUAL A $200. FAVOR FIRMAR" & Chr(10)
                    End If
                    str += "NIT: " & Ventas.Nit.ToString & Chr(10)
                    str += "DUI: " & Ventas.Nrc & Chr(10)
                    str += "Nombre: " & Ventas.Nombre & Chr(10) & Chr(10)
                    str += "Firma: _____________________________" & Chr(10)
                End If
                str += Chr(10) & Chr(10)
                str += DatosPie
                str += Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10)
                str += Chr(27) + "z" + Chr(0) 'para desactivar el modo de impresión en la cinta
                str += Chr(27) + Chr(109) 'para cortar el ticket con la cuchilla 
                str += Chr(27) + Chr(109) 'para cortar el ticket con la cuchilla 
                RawPrinterHelper.SendStringToPrinter(entidadTicket.NombreImpresor, str)
            Case 19 'tickette de caja se imprime a puro código
                Dim Template = Application.StartupPath & "\Plantillas\neNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptOrdenEnvio
                Dim entSucursalRecibe = objTablas.adm_SucursalesSelectByPK(Ventas.IdSucursalEnvio)
                Dim entSucursalEnvia = objTablas.adm_SucursalesSelectByPK(piIdSucursalUsuario)
                rpt.LoadLayout(Template)

                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlDia.Text = CDate(.Fecha).Day.ToString()
                    rpt.xrlMes.Text = ObtieneMesString(CDate(.Fecha).Month)
                    rpt.xrlAnio.Text = CDate(.Fecha).Year.ToString()
                    rpt.xrlTotal2.Text = Format(.TotalComprobante, "###,##0.00")
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlSucursalEnvia.Text = entSucursalEnvia.Nombre
                    rpt.xrlSucursalRecibe.Text = entSucursalRecibe.Nombre
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(.TotalIva, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalIva + .TotalAfecto, "###,##0.00") 'Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    rpt.xrlCantLetras.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = .Comentario

                    'rpt.ShowPreview()
                    rpt.ShowPreviewDialog()
                End With
            Case 24 'consumidor final formato pequeño
                Dim Template = Application.StartupPath & "\Plantillas\fpNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFactura
                rpt.LoadLayout(Template)

                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDiasCredito.Text = .DiasCredito
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    Dim CantidaLetras As String = Num2Text(Int(IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))) & Decimales & " DÓLARES"
                    rpt.xrlCantLetras.Text = CantidaLetras
                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPrintMarginsWarning = False
                    rpt.ShowPreviewDialog()
                End With
            Case 18
                Dim Template = Application.StartupPath & "\Plantillas\neNexus" & NumFormato & ".repx"

                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNumRemision.Text = .Telefono
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlDiasCredito.Text = .DiasCredito
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(.TotalIva, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlCESC.Text = Format(.TotalImpuesto2, "##,###.00")
                    rpt.xrlSubTotal.Text = Format(.TotalIva + Ventas.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")

                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPrintMarginsWarning = False
                    rpt.ShowPreviewDialog()
                End With
            Case Else
                MsgBox("NO EXISTE CONFIGURACION PARA IMPRESION DE ESTE TIPO DE DOCUMENTO", MsgBoxStyle.Critical, "ALERTA")
        End Select

        Return True
    End Function

    Private Sub sbAjuste_Click(sender As Object, e As EventArgs) Handles sbAjuste.Click
        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            MsgBox("No seleccionó ningun archivo", MsgBoxStyle.Exclamation, "Nota")
            Return
        End If
        Dim rpt As New XtraReport
        Try
            rpt.LoadLayout(ofd.FileName)
            Dim dt As New ReportDesignTool(rpt)
            dt.ShowDesigner()
        Catch ex As Exception
            MsgBox("Se generó un error al cargar el archivo." & Chr(13) & "Asegurese que tenga un formato válido" & Chr(13) & "Normalmente debe ser con extensión .REPX" & ex.Message, MsgBoxStyle.Exclamation, "Nota")
        End Try
    End Sub
    Private Sub ImprimirTikect()
     
    End Sub

    Private Sub leSucursal_EditValueChanged(sender As Object, e As EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue)
    End Sub
End Class
