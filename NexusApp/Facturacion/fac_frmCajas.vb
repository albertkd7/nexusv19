﻿Imports NexusELL.TableEntities
Public Class fac_frmCajas

    Private Sub fac_frmCajas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        gc.DataSource = objTablas.fac_CajasSelectAll
    End Sub
    Private Sub acf_frmCajas_Delete_Click() Handles MyBase.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Return
        End If

        Dim Id As Integer = gv.GetFocusedRowCellValue(gv.Columns(0))
        objTablas.fac_CajasDeleteByPK(Id)
        gc.DataSource = objTablas.fac_CajasSelectAll
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated

        Dim entidad As New fac_Cajas
        Dim numFila As Integer

        If e.RowHandle < 0 Then
            DbMode = DbModeType.insert
            numFila = gv.RowCount - 1
        Else
            DbMode = DbModeType.update
            numFila = e.RowHandle
        End If

        If Not AllowInsert Or Not AllowEdit Then
            MsgBox("No le está permitido ingresar o editar información", MsgBoxStyle.Exclamation, Me.Text)
            gc.DataSource = objTablas.fac_CajasSelectAll
            Exit Sub
        End If

        With entidad
            .NumeroCaja = gv.GetRowCellValue(numFila, gv.Columns(0).FieldName)
            .NombreCaja = gv.GetRowCellValue(numFila, gv.Columns(1).FieldName)
            .Responsable = gv.GetRowCellValue(numFila, gv.Columns(2).FieldName)
            .UltimoCorte = gv.GetRowCellValue(numFila, gv.Columns(3).FieldName)
            .FechaUltimoCorte = gv.GetRowCellValue(numFila, gv.Columns(4).FieldName)
        End With
        If DbMode = DbModeType.insert Then
            objTablas.fac_CajasInsert(entidad)
        Else
            objTablas.fac_CajasUpdate(entidad)
        End If

    End Sub

    Private Sub acf_frmTipos_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub


End Class
