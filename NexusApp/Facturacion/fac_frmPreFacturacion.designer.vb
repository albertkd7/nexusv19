﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmPreFacturacion
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.teNit = New DevExpress.XtraEditors.TextEdit()
        Me.teNrc = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.sePjeDescuento = New DevExpress.XtraEditors.SpinEdit()
        Me.ceTipo = New DevExpress.XtraEditors.CheckEdit()
        Me.btnImprimir = New DevExpress.XtraEditors.SimpleButton()
        Me.btnFacturar = New DevExpress.XtraEditors.SimpleButton()
        Me.sbConsultaPed = New DevExpress.XtraEditors.SimpleButton()
        Me.deHasta = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.deDesde = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.gcConsultaPed = New DevExpress.XtraGrid.GridControl()
        Me.gvConsultaPed = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcVendedor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdBodega = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcTipo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFormaPago = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFacturado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCreadoPor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdCliente = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNrc = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPorcDescuento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcUtilidadPerdida = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leTipoDoc = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.leFormaPago = New DevExpress.XtraEditors.LookUpEdit()
        Me.sbCambiaFormaPago = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumeroUnico = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaFacturacion = New DevExpress.XtraEditors.DateEdit()
        Me.sbAplicarInventario = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.teNit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNrc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sePjeDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcConsultaPed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvConsultaPed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoDoc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumeroUnico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaFacturacion.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaFacturacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.sbAplicarInventario)
        Me.GroupControl1.Controls.Add(Me.deFechaFacturacion)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.teNumeroUnico)
        Me.GroupControl1.Controls.Add(Me.LabelControl30)
        Me.GroupControl1.Controls.Add(Me.LabelControl23)
        Me.GroupControl1.Controls.Add(Me.teNumero)
        Me.GroupControl1.Controls.Add(Me.sbCambiaFormaPago)
        Me.GroupControl1.Controls.Add(Me.leFormaPago)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.leTipoDoc)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.teNit)
        Me.GroupControl1.Controls.Add(Me.teNrc)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.teNombre)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl26)
        Me.GroupControl1.Controls.Add(Me.sePjeDescuento)
        Me.GroupControl1.Controls.Add(Me.ceTipo)
        Me.GroupControl1.Controls.Add(Me.btnImprimir)
        Me.GroupControl1.Controls.Add(Me.btnFacturar)
        Me.GroupControl1.Controls.Add(Me.sbConsultaPed)
        Me.GroupControl1.Controls.Add(Me.deHasta)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.deDesde)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Size = New System.Drawing.Size(1137, 133)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Parámetros de la consulta"
        '
        'teNit
        '
        Me.teNit.EditValue = ""
        Me.teNit.EnterMoveNextControl = True
        Me.teNit.Location = New System.Drawing.Point(574, 67)
        Me.teNit.Name = "teNit"
        Me.teNit.Size = New System.Drawing.Size(131, 20)
        Me.teNit.TabIndex = 7
        '
        'teNrc
        '
        Me.teNrc.EditValue = ""
        Me.teNrc.EnterMoveNextControl = True
        Me.teNrc.Location = New System.Drawing.Point(379, 67)
        Me.teNrc.Name = "teNrc"
        Me.teNrc.Size = New System.Drawing.Size(131, 20)
        Me.teNrc.TabIndex = 6
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(528, 70)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(43, 13)
        Me.LabelControl5.TabIndex = 101
        Me.LabelControl5.Text = "NIT/DUI:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(379, 46)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teNombre.Size = New System.Drawing.Size(326, 20)
        Me.teNombre.TabIndex = 5
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(351, 70)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl4.TabIndex = 103
        Me.LabelControl4.Text = "NRC:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(266, 49)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(110, 13)
        Me.LabelControl3.TabIndex = 102
        Me.LabelControl3.Text = "Facturar a Nombre De:"
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(179, 24)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl26.TabIndex = 100
        Me.LabelControl26.Text = "% de Descuento:"
        Me.LabelControl26.Visible = False
        '
        'sePjeDescuento
        '
        Me.sePjeDescuento.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.sePjeDescuento.EnterMoveNextControl = True
        Me.sePjeDescuento.Location = New System.Drawing.Point(179, 39)
        Me.sePjeDescuento.Name = "sePjeDescuento"
        Me.sePjeDescuento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.sePjeDescuento.Properties.Mask.EditMask = "P2"
        Me.sePjeDescuento.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.sePjeDescuento.Size = New System.Drawing.Size(71, 20)
        Me.sePjeDescuento.TabIndex = 4
        Me.sePjeDescuento.Visible = False
        '
        'ceTipo
        '
        Me.ceTipo.EditValue = True
        Me.ceTipo.Location = New System.Drawing.Point(7, 64)
        Me.ceTipo.Name = "ceTipo"
        Me.ceTipo.Properties.Caption = "Ver solamente Hojas pendientes de facturación"
        Me.ceTipo.Size = New System.Drawing.Size(268, 19)
        Me.ceTipo.TabIndex = 2
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(597, 23)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(108, 21)
        Me.btnImprimir.TabIndex = 9
        Me.btnImprimir.Text = "Re-Imprimir Orden"
        '
        'btnFacturar
        '
        Me.btnFacturar.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnFacturar.Appearance.Options.UseFont = True
        Me.btnFacturar.Location = New System.Drawing.Point(754, 23)
        Me.btnFacturar.Name = "btnFacturar"
        Me.btnFacturar.Size = New System.Drawing.Size(111, 23)
        Me.btnFacturar.TabIndex = 8
        Me.btnFacturar.Text = "Generar Factura"
        '
        'sbConsultaPed
        '
        Me.sbConsultaPed.Location = New System.Drawing.Point(76, 83)
        Me.sbConsultaPed.Name = "sbConsultaPed"
        Me.sbConsultaPed.Size = New System.Drawing.Size(100, 23)
        Me.sbConsultaPed.TabIndex = 3
        Me.sbConsultaPed.Text = "Obtener Datos"
        '
        'deHasta
        '
        Me.deHasta.EditValue = Nothing
        Me.deHasta.EnterMoveNextControl = True
        Me.deHasta.Location = New System.Drawing.Point(76, 43)
        Me.deHasta.Name = "deHasta"
        Me.deHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deHasta.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deHasta.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deHasta.Size = New System.Drawing.Size(100, 20)
        Me.deHasta.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(9, 46)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 92
        Me.LabelControl2.Text = "Hasta Fecha:"
        '
        'deDesde
        '
        Me.deDesde.EditValue = Nothing
        Me.deDesde.EnterMoveNextControl = True
        Me.deDesde.Location = New System.Drawing.Point(76, 22)
        Me.deDesde.Name = "deDesde"
        Me.deDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deDesde.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deDesde.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deDesde.Size = New System.Drawing.Size(100, 20)
        Me.deDesde.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(7, 25)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl1.TabIndex = 89
        Me.LabelControl1.Text = "Desde Fecha:"
        '
        'gcConsultaPed
        '
        Me.gcConsultaPed.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcConsultaPed.Location = New System.Drawing.Point(0, 133)
        Me.gcConsultaPed.MainView = Me.gvConsultaPed
        Me.gcConsultaPed.Name = "gcConsultaPed"
        Me.gcConsultaPed.Size = New System.Drawing.Size(1137, 385)
        Me.gcConsultaPed.TabIndex = 1
        Me.gcConsultaPed.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvConsultaPed})
        '
        'gvConsultaPed
        '
        Me.gvConsultaPed.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdComprobante, Me.gcNumero, Me.gcFecha, Me.gcNombre, Me.gcVendedor, Me.gcIdBodega, Me.gcTotal, Me.gcTipo, Me.gcFormaPago, Me.gcFacturado, Me.gcCreadoPor, Me.gcIdCliente, Me.gcNrc, Me.gcNit, Me.gcPorcDescuento, Me.gcUtilidadPerdida, Me.GridColumn1, Me.GridColumn2})
        Me.gvConsultaPed.GridControl = Me.gcConsultaPed
        Me.gvConsultaPed.Name = "gvConsultaPed"
        Me.gvConsultaPed.OptionsBehavior.Editable = False
        Me.gvConsultaPed.OptionsView.ShowAutoFilterRow = True
        Me.gvConsultaPed.OptionsView.ShowGroupPanel = False
        '
        'gcIdComprobante
        '
        Me.gcIdComprobante.Caption = "Correlativo"
        Me.gcIdComprobante.FieldName = "IdComprobante"
        Me.gcIdComprobante.Name = "gcIdComprobante"
        Me.gcIdComprobante.Visible = True
        Me.gcIdComprobante.VisibleIndex = 0
        Me.gcIdComprobante.Width = 93
        '
        'gcNumero
        '
        Me.gcNumero.Caption = "Numero"
        Me.gcNumero.FieldName = "Numero"
        Me.gcNumero.Name = "gcNumero"
        Me.gcNumero.Visible = True
        Me.gcNumero.VisibleIndex = 1
        Me.gcNumero.Width = 77
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha"
        Me.gcFecha.FieldName = "Fecha"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 2
        Me.gcFecha.Width = 84
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre del Cliente"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 3
        Me.gcNombre.Width = 188
        '
        'gcVendedor
        '
        Me.gcVendedor.Caption = "Vendedor"
        Me.gcVendedor.FieldName = "Vendedor"
        Me.gcVendedor.Name = "gcVendedor"
        Me.gcVendedor.Visible = True
        Me.gcVendedor.VisibleIndex = 4
        Me.gcVendedor.Width = 101
        '
        'gcIdBodega
        '
        Me.gcIdBodega.Caption = "IdBodega"
        Me.gcIdBodega.FieldName = "IdBodega"
        Me.gcIdBodega.Name = "gcIdBodega"
        Me.gcIdBodega.Visible = True
        Me.gcIdBodega.VisibleIndex = 5
        Me.gcIdBodega.Width = 84
        '
        'gcTotal
        '
        Me.gcTotal.Caption = "Total"
        Me.gcTotal.FieldName = "Total"
        Me.gcTotal.Name = "gcTotal"
        Me.gcTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)})
        Me.gcTotal.Visible = True
        Me.gcTotal.VisibleIndex = 6
        Me.gcTotal.Width = 101
        '
        'gcTipo
        '
        Me.gcTipo.Caption = "Tipo"
        Me.gcTipo.FieldName = "Tipo"
        Me.gcTipo.Name = "gcTipo"
        Me.gcTipo.Visible = True
        Me.gcTipo.VisibleIndex = 7
        Me.gcTipo.Width = 88
        '
        'gcFormaPago
        '
        Me.gcFormaPago.Caption = "Forma/Pago"
        Me.gcFormaPago.FieldName = "FormaPago"
        Me.gcFormaPago.Name = "gcFormaPago"
        Me.gcFormaPago.Visible = True
        Me.gcFormaPago.VisibleIndex = 8
        '
        'gcFacturado
        '
        Me.gcFacturado.Caption = "Facturado"
        Me.gcFacturado.FieldName = "Facturado"
        Me.gcFacturado.Name = "gcFacturado"
        Me.gcFacturado.Visible = True
        Me.gcFacturado.VisibleIndex = 9
        Me.gcFacturado.Width = 94
        '
        'gcCreadoPor
        '
        Me.gcCreadoPor.Caption = "Creado Por"
        Me.gcCreadoPor.FieldName = "CreadoPor"
        Me.gcCreadoPor.Name = "gcCreadoPor"
        Me.gcCreadoPor.Visible = True
        Me.gcCreadoPor.VisibleIndex = 10
        '
        'gcIdCliente
        '
        Me.gcIdCliente.Caption = "GridColumn1"
        Me.gcIdCliente.FieldName = "IdCliente"
        Me.gcIdCliente.Name = "gcIdCliente"
        '
        'gcNrc
        '
        Me.gcNrc.Caption = "GridColumn2"
        Me.gcNrc.FieldName = "Nrc"
        Me.gcNrc.Name = "gcNrc"
        '
        'gcNit
        '
        Me.gcNit.Caption = "GridColumn3"
        Me.gcNit.FieldName = "Nit"
        Me.gcNit.Name = "gcNit"
        '
        'gcPorcDescuento
        '
        Me.gcPorcDescuento.Caption = "GridColumn1"
        Me.gcPorcDescuento.FieldName = "PorcDescuento"
        Me.gcPorcDescuento.Name = "gcPorcDescuento"
        '
        'gcUtilidadPerdida
        '
        Me.gcUtilidadPerdida.Caption = "UtilidadPerdida"
        Me.gcUtilidadPerdida.FieldName = "UtilidadPerdida"
        Me.gcUtilidadPerdida.Name = "gcUtilidadPerdida"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "GridColumn1"
        Me.GridColumn1.FieldName = "IdTipoComprobante"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "GridColumn2"
        Me.GridColumn2.FieldName = "IdFormaPago"
        Me.GridColumn2.Name = "GridColumn2"
        '
        'leTipoDoc
        '
        Me.leTipoDoc.Enabled = False
        Me.leTipoDoc.Location = New System.Drawing.Point(379, 89)
        Me.leTipoDoc.Name = "leTipoDoc"
        Me.leTipoDoc.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.leTipoDoc.Properties.Appearance.Options.UseFont = True
        Me.leTipoDoc.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Red
        Me.leTipoDoc.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.leTipoDoc.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoDoc.Size = New System.Drawing.Size(326, 20)
        Me.leTipoDoc.TabIndex = 105
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(280, 92)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(96, 13)
        Me.LabelControl6.TabIndex = 104
        Me.LabelControl6.Text = "Tipo de Documento:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(300, 113)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl7.TabIndex = 106
        Me.LabelControl7.Text = "Forma de Pago:"
        '
        'leFormaPago
        '
        Me.leFormaPago.Location = New System.Drawing.Point(379, 110)
        Me.leFormaPago.Name = "leFormaPago"
        Me.leFormaPago.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leFormaPago.Size = New System.Drawing.Size(326, 20)
        Me.leFormaPago.TabIndex = 107
        '
        'sbCambiaFormaPago
        '
        Me.sbCambiaFormaPago.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sbCambiaFormaPago.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.sbCambiaFormaPago.Appearance.Options.UseFont = True
        Me.sbCambiaFormaPago.Appearance.Options.UseForeColor = True
        Me.sbCambiaFormaPago.Location = New System.Drawing.Point(379, 23)
        Me.sbCambiaFormaPago.Name = "sbCambiaFormaPago"
        Me.sbCambiaFormaPago.Size = New System.Drawing.Size(152, 23)
        Me.sbCambiaFormaPago.TabIndex = 108
        Me.sbCambiaFormaPago.Text = "Actualizar Forma de Pago"
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(726, 49)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(172, 13)
        Me.LabelControl23.TabIndex = 110
        Me.LabelControl23.Text = "No. DE DOCUMENTO A GENERAR"
        '
        'teNumero
        '
        Me.teNumero.Enabled = False
        Me.teNumero.Location = New System.Drawing.Point(763, 63)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Properties.AllowFocused = False
        Me.teNumero.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.teNumero.Properties.Appearance.ForeColor = System.Drawing.Color.DarkRed
        Me.teNumero.Properties.Appearance.Options.UseFont = True
        Me.teNumero.Properties.Appearance.Options.UseForeColor = True
        Me.teNumero.Properties.ReadOnly = True
        Me.teNumero.Size = New System.Drawing.Size(103, 26)
        Me.teNumero.TabIndex = 109
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Location = New System.Drawing.Point(737, 90)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(150, 13)
        Me.LabelControl30.TabIndex = 117
        Me.LabelControl30.Text = "No. DE FORMULARIO UNICO"
        '
        'teNumeroUnico
        '
        Me.teNumeroUnico.Enabled = False
        Me.teNumeroUnico.Location = New System.Drawing.Point(763, 104)
        Me.teNumeroUnico.Name = "teNumeroUnico"
        Me.teNumeroUnico.Properties.AllowFocused = False
        Me.teNumeroUnico.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.teNumeroUnico.Properties.Appearance.ForeColor = System.Drawing.Color.DarkRed
        Me.teNumeroUnico.Properties.Appearance.Options.UseFont = True
        Me.teNumeroUnico.Properties.Appearance.Options.UseForeColor = True
        Me.teNumeroUnico.Properties.ReadOnly = True
        Me.teNumeroUnico.Size = New System.Drawing.Size(103, 26)
        Me.teNumeroUnico.TabIndex = 118
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl8.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Appearance.Options.UseForeColor = True
        Me.LabelControl8.Location = New System.Drawing.Point(5, 112)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(161, 13)
        Me.LabelControl8.TabIndex = 119
        Me.LabelControl8.Text = "Fecha Activa de Facturación:"
        '
        'deFechaFacturacion
        '
        Me.deFechaFacturacion.EditValue = Nothing
        Me.deFechaFacturacion.Enabled = False
        Me.deFechaFacturacion.EnterMoveNextControl = True
        Me.deFechaFacturacion.Location = New System.Drawing.Point(172, 109)
        Me.deFechaFacturacion.Name = "deFechaFacturacion"
        Me.deFechaFacturacion.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.deFechaFacturacion.Properties.Appearance.ForeColor = System.Drawing.Color.Red
        Me.deFechaFacturacion.Properties.Appearance.Options.UseFont = True
        Me.deFechaFacturacion.Properties.Appearance.Options.UseForeColor = True
        Me.deFechaFacturacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaFacturacion.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaFacturacion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaFacturacion.Size = New System.Drawing.Size(91, 20)
        Me.deFechaFacturacion.TabIndex = 120
        '
        'sbAplicarInventario
        '
        Me.sbAplicarInventario.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.sbAplicarInventario.Appearance.Options.UseFont = True
        Me.sbAplicarInventario.Location = New System.Drawing.Point(925, 25)
        Me.sbAplicarInventario.Name = "sbAplicarInventario"
        Me.sbAplicarInventario.Size = New System.Drawing.Size(130, 23)
        Me.sbAplicarInventario.TabIndex = 121
        Me.sbAplicarInventario.Text = "Aplicar al Inventario"
        '
        'fac_frmPreFacturacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(1137, 543)
        Me.Controls.Add(Me.gcConsultaPed)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmPreFacturacion"
        Me.Text = "Pre-Facturación"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.gcConsultaPed, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.teNit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNrc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sePjeDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcConsultaPed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvConsultaPed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoDoc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumeroUnico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaFacturacion.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaFacturacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents teNit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNrc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sePjeDescuento As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents ceTipo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents btnImprimir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFacturar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbConsultaPed As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents deHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcConsultaPed As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvConsultaPed As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcVendedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFormaPago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFacturado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCreadoPor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNrc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPorcDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcUtilidadPerdida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leTipoDoc As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leFormaPago As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents sbCambiaFormaPago As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNumeroUnico As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaFacturacion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbAplicarInventario As DevExpress.XtraEditors.SimpleButton
End Class
