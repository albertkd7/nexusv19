﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmAsignacionDocumentos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmAsignacionDocumentos))
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.rileSucursal = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.rilePunto = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.rileTipo = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkActivo = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.gvPuntos = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.TeResolucion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoDoc = New DevExpress.XtraEditors.LookUpEdit()
        Me.lePunto = New DevExpress.XtraEditors.LookUpEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.ceActivo = New DevExpress.XtraEditors.CheckEdit()
        Me.seHasta = New DevExpress.XtraEditors.SpinEdit()
        Me.seDesde = New DevExpress.XtraEditors.SpinEdit()
        Me.seUltimo = New DevExpress.XtraEditors.SpinEdit()
        Me.teSerie = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rileSucursal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rilePunto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rileTipo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActivo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPuntos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.TeResolucion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoDoc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seUltimo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(990, 363)
        Me.XtraTabControl1.TabIndex = 2
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.gc)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(984, 335)
        Me.XtraTabPage1.Text = "Documentos asignados"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rileSucursal, Me.rilePunto, Me.rileTipo, Me.chkActivo})
        Me.gc.Size = New System.Drawing.Size(983, 335)
        Me.gc.TabIndex = 1
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv, Me.gvPuntos})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn6, Me.GridColumn5})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Sucursal"
        Me.GridColumn1.ColumnEdit = Me.rileSucursal
        Me.GridColumn1.FieldName = "IdSucursal"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 182
        '
        'rileSucursal
        '
        Me.rileSucursal.AutoHeight = False
        Me.rileSucursal.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rileSucursal.Name = "rileSucursal"
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Punto de Venta"
        Me.GridColumn2.ColumnEdit = Me.rilePunto
        Me.GridColumn2.FieldName = "IdPunto"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 189
        '
        'rilePunto
        '
        Me.rilePunto.AutoHeight = False
        Me.rilePunto.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rilePunto.Name = "rilePunto"
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Tipo de Documento"
        Me.GridColumn3.ColumnEdit = Me.rileTipo
        Me.GridColumn3.FieldName = "IdTipoComprobante"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 176
        '
        'rileTipo
        '
        Me.rileTipo.AutoHeight = False
        Me.rileTipo.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rileTipo.Name = "rileTipo"
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Serie"
        Me.GridColumn4.FieldName = "Serie"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 138
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Serie Activa?"
        Me.GridColumn5.ColumnEdit = Me.chkActivo
        Me.GridColumn5.FieldName = "EsActivo"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 5
        Me.GridColumn5.Width = 102
        '
        'chkActivo
        '
        Me.chkActivo.AutoHeight = False
        Me.chkActivo.Name = "chkActivo"
        '
        'gvPuntos
        '
        Me.gvPuntos.GridControl = Me.gc
        Me.gvPuntos.Name = "gvPuntos"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.TeResolucion)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl9)
        Me.XtraTabPage2.Controls.Add(Me.leTipoDoc)
        Me.XtraTabPage2.Controls.Add(Me.lePunto)
        Me.XtraTabPage2.Controls.Add(Me.leSucursal)
        Me.XtraTabPage2.Controls.Add(Me.deFecha)
        Me.XtraTabPage2.Controls.Add(Me.ceActivo)
        Me.XtraTabPage2.Controls.Add(Me.seHasta)
        Me.XtraTabPage2.Controls.Add(Me.seDesde)
        Me.XtraTabPage2.Controls.Add(Me.seUltimo)
        Me.XtraTabPage2.Controls.Add(Me.teSerie)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl8)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl7)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl6)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl5)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl4)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl3)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl2)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl1)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(984, 335)
        Me.XtraTabPage2.Text = "Información de la asignación"
        '
        'TeResolucion
        '
        Me.TeResolucion.Location = New System.Drawing.Point(139, 121)
        Me.TeResolucion.Name = "TeResolucion"
        Me.TeResolucion.Size = New System.Drawing.Size(236, 20)
        Me.TeResolucion.TabIndex = 13
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(63, 124)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(70, 13)
        Me.LabelControl9.TabIndex = 12
        Me.LabelControl9.Text = "N° Resolución:"
        '
        'leTipoDoc
        '
        Me.leTipoDoc.Location = New System.Drawing.Point(139, 71)
        Me.leTipoDoc.Name = "leTipoDoc"
        Me.leTipoDoc.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoDoc.Size = New System.Drawing.Size(354, 20)
        Me.leTipoDoc.TabIndex = 2
        '
        'lePunto
        '
        Me.lePunto.Location = New System.Drawing.Point(139, 49)
        Me.lePunto.Name = "lePunto"
        Me.lePunto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePunto.Size = New System.Drawing.Size(354, 20)
        Me.lePunto.TabIndex = 1
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(139, 27)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(354, 20)
        Me.leSucursal.TabIndex = 0
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.Location = New System.Drawing.Point(139, 248)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 11
        '
        'ceActivo
        '
        Me.ceActivo.Location = New System.Drawing.Point(139, 219)
        Me.ceActivo.Name = "ceActivo"
        Me.ceActivo.Properties.Caption = "Activo?"
        Me.ceActivo.Size = New System.Drawing.Size(75, 19)
        Me.ceActivo.TabIndex = 10
        '
        'seHasta
        '
        Me.seHasta.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seHasta.Location = New System.Drawing.Point(139, 192)
        Me.seHasta.Name = "seHasta"
        Me.seHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seHasta.Size = New System.Drawing.Size(100, 20)
        Me.seHasta.TabIndex = 6
        '
        'seDesde
        '
        Me.seDesde.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDesde.Location = New System.Drawing.Point(139, 170)
        Me.seDesde.Name = "seDesde"
        Me.seDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDesde.Size = New System.Drawing.Size(100, 20)
        Me.seDesde.TabIndex = 5
        '
        'seUltimo
        '
        Me.seUltimo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seUltimo.Location = New System.Drawing.Point(139, 148)
        Me.seUltimo.Name = "seUltimo"
        Me.seUltimo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seUltimo.Size = New System.Drawing.Size(100, 20)
        Me.seUltimo.TabIndex = 4
        '
        'teSerie
        '
        Me.teSerie.Location = New System.Drawing.Point(139, 95)
        Me.teSerie.Name = "teSerie"
        Me.teSerie.Size = New System.Drawing.Size(236, 20)
        Me.teSerie.TabIndex = 3
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(34, 252)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl8.TabIndex = 0
        Me.LabelControl8.Text = "Fecha de Asignación:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(64, 195)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl7.TabIndex = 0
        Me.LabelControl7.Text = "Hasta Número:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(62, 173)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(74, 13)
        Me.LabelControl6.TabIndex = 0
        Me.LabelControl6.Text = "Desde Número:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(38, 151)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(98, 13)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Ult. Número Emitido:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(53, 98)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Serie Autorizada:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(40, 74)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(96, 13)
        Me.LabelControl3.TabIndex = 0
        Me.LabelControl3.Text = "Tipo de Documento:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(58, 52)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Punto de Venta:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(92, 31)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Sucursal:"
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "No. Resolucion"
        Me.GridColumn6.FieldName = "Resolucion"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 4
        Me.GridColumn6.Width = 178
        '
        'fac_frmAsignacionDocumentos
        '
        Me.ClientSize = New System.Drawing.Size(990, 388)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmAsignacionDocumentos"
        Me.OptionId = "001004"
        Me.Text = "Asignación de documentos"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.XtraTabControl1, 0)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rileSucursal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rilePunto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rileTipo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActivo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPuntos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        Me.XtraTabPage2.PerformLayout()
        CType(Me.TeResolucion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoDoc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seUltimo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rileSucursal As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rilePunto As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rileTipo As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gvPuntos As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seHasta As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seDesde As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seUltimo As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents ceActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoDoc As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lePunto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkActivo As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents TeResolucion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
End Class
