﻿Imports NexusBLL
Public Class fac_frmConsultaEstadisticaVenta
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blInventa As New InventarioBLL(g_ConnectionString)
    Dim Prod As DataTable, ProdVista As DataTable
    Dim dt As New DataTable


    Private Sub fac_frmConsultaProductosBase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        deDesde.EditValue = Today
        deHasta.EditValue = Today
        BeProducto1.beCodigo.EditValue = ""
        CargaCombos()


        gvProd1.Columns.Clear()
        gcProductos.DataSource = bl.fac_VentasProductosAnual2(deDesde.EditValue, deHasta.EditValue, BeProducto1.beCodigo.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, leGrupo.EditValue)
        ProdVista = gcProductos.DataSource

        ' gvProd1.BestFitColumns()


    End Sub

    Private Sub CargaCombos()
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS --")
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub


    Private Sub btGenerar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGenerar.Click
        gvProd1.Columns.Clear()
        gcProductos.DataSource = bl.fac_VentasProductosAnual2(deDesde.EditValue, deHasta.EditValue, BeProducto1.beCodigo.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, leGrupo.EditValue)
        ProdVista = gcProductos.DataSource
    End Sub

    Private Sub btExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btExportar.Click
        Dim NombreArchivo As String = ""
        NombreArchivo = ObtieneNombreArchivo() & ".xls"
        gcProductos.ExportToXls(NombreArchivo)

        MsgBox("El documento ha sido exportado con éxito en formato Excel", 64, "Nota")

    End Sub
    Private Function ObtieneNombreArchivo() As String
        Dim NombreArchivo As String = "Ventas Anuales " & Today.Year.ToString.PadLeft(4, "0") & Today.Month.ToString.PadLeft(2, "0") & Today.Day.ToString.PadLeft(2, "0")
        NombreArchivo = InputBox("Nombre del archivo", "Defina el nombre del archivo", NombreArchivo)

        Dim myFolderBrowserDialog As New FolderBrowserDialog

        With myFolderBrowserDialog

            .RootFolder = Environment.SpecialFolder.Desktop
            .SelectedPath = "c:\"
            .Description = "Seleccione la carpeta destino"
            If .ShowDialog = DialogResult.OK Then
                NombreArchivo = .SelectedPath & "\" & NombreArchivo
            End If
        End With
        Return NombreArchivo
    End Function
End Class
