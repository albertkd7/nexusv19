﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class fac_frmVendedores
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim entVendedor As fac_Vendedores
    Dim entComision As List(Of fac_VendedoresDetalle)
    Dim entClientesVen As List(Of fac_VendedoresClientes)
    Dim ExtensionRCV As Boolean = True


    Private Sub fac_frmVendedores_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub facVendedores_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.fac_VendedoresSelectAll
        entVendedor = objTablas.fac_VendedoresSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdVendedor"))
        gcComision.DataSource = bl.fac_ObtenerDetalleVendedores(gv.GetRowCellValue(gv.FocusedRowHandle, "IdVendedor"))
        ExtensionRCV = bl.fac_ExisteRCV()
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")

        If ExtensionRCV Then
            gcVenClientes.DataSource = bl.fac_ObtenerClientesVendedor(gv.GetRowCellValue(gv.FocusedRowHandle, "IdVendedor"))
        Else
            gcVenClientes.DataSource = Nothing
            gcVenClientes.Visible = False
            lbInfoRCV.Visible = False
            entClientesVen = New List(Of fac_VendedoresClientes)
            sbtnImprimirCRV.Visible = False
            ceTodosClientes.Visible = False
        End If

        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub facVendedores_Nuevo_Click() Handles Me.Nuevo
        entVendedor = New fac_Vendedores
        entVendedor.IdVendedor = objFunciones.ObtenerUltimoId("FAC_VENDEDORES", "IdVendedor") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub facVendedores_Save_Click() Handles Me.Guardar
        If teTelefonos.EditValue = "" Or teDireccion.EditValue = "" Or teNombre.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Nombre, Dirección y Teléfono]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        Dim Msg As String = ""

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            Msg = bl.InsertVendedores(entVendedor, entComision, entClientesVen)
        Else
            Msg = bl.UpdateVendedores(entVendedor, entComision, entClientesVen)
        End If

        If String.IsNullOrEmpty(Msg) Then
            gc.DataSource = objTablas.fac_VendedoresSelectAll
            gcComision.DataSource = bl.fac_ObtenerDetalleVendedores(teIdVendedor.EditValue)
            If ExtensionRCV Then
                gcVenClientes.DataSource = bl.fac_ObtenerClientesVendedor(teIdVendedor.EditValue)
            End If
            ActivaControles(False)
            MostrarModoInicial()
        Else
            MsgBox(Msg, MsgBoxStyle.OkOnly, "ERROR! =(")
        End If

    End Sub
    Private Sub fac_frmVendedores_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar al vendedor seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.fac_VendedoresDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.fac_VendedoresSelectAll
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR AL VENDEDOR:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub facVendedores_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entVendedor
            teIdVendedor.EditValue = .IdVendedor
            teNombre.EditValue = .Nombre
            teTelefonos.EditValue = .Telefonos
            teDireccion.EditValue = .Direccion
            teLimite.EditValue = .LimiteVenta
            tePorcComision.EditValue = .PorcComision
            beIdEmpleado.EditValue = .IdEmpleado
            ceActivo.EditValue = .Activo
            leSucursal.EditValue = .IdSucursal
            ceTodosClientes.EditValue = .AllRCV
        End With
        gcComision.DataSource = bl.fac_ObtenerDetalleVendedores(entVendedor.IdVendedor)
        'MessageBox.Show("" & ExtensionRCV, "RCV", MessageBoxButtons.OK)
        If ExtensionRCV Then
            gcVenClientes.DataSource = bl.fac_ObtenerClientesVendedor(entVendedor.IdVendedor)
        End If
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entVendedor
            .IdVendedor = teIdVendedor.EditValue
            .Activo = ceActivo.EditValue
            .Nombre = teNombre.EditValue
            .Telefonos = teTelefonos.EditValue
            .Direccion = teDireccion.EditValue
            .LimiteVenta = teLimite.EditValue
            .PorcComision = tePorcComision.EditValue
            .IdEmpleado = beIdEmpleado.EditValue
            .IdSucursal = leSucursal.EditValue
            .AllRCV = ceTodosClientes.EditValue
        End With

        entComision = New List(Of fac_VendedoresDetalle)
        For i = 0 To gvComision.DataRowCount - 1
            Dim Comision As New fac_VendedoresDetalle
            With Comision
                .IdDetalle = i + 1
                .RangoDesde = gvComision.GetRowCellValue(i, "RangoDesde")
                .RangoHasta = gvComision.GetRowCellValue(i, "RangoHasta")
                .Porcentaje = gvComision.GetRowCellValue(i, "Porcentaje")
                .IdVendedor = teIdVendedor.EditValue
            End With
            entComision.Add(Comision)
        Next

        entClientesVen = New List(Of fac_VendedoresClientes)
        For i = 0 To gvClientesVendedor.DataRowCount - 1
            Dim ClientesVen As New fac_VendedoresClientes
            With ClientesVen
                .IdVendedor = teIdVendedor.EditValue
                .IdCliente = gvClientesVendedor.GetRowCellValue(i, "IdCliente")
            End With
            entClientesVen.Add(ClientesVen)
        Next
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entVendedor = objTablas.fac_VendedoresSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdVendedor"))
        CargaPantalla()
    End Sub

    Private Sub facVendedores_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gcVenClientes.Enabled = Tipo
        gcComision.Enabled = Tipo
        ceTodosClientes.Enabled = Tipo
        teIdVendedor.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub

    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gvComision.DeleteSelectedRows()
    End Sub

    Private Sub gcVenClientes_KeyUp(sender As Object, e As KeyEventArgs) Handles gcVenClientes.KeyUp
        If e.KeyCode = Keys.Delete Then
            gvClientesVendedor.DeleteSelectedRows()
        End If
    End Sub

    Private Sub gvClientesVendedor_ValidateRow(sender As Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gvClientesVendedor.ValidateRow
        Dim CodCliente As String = SiEsNulo(gvClientesVendedor.GetFocusedRowCellValue("IdCliente"), "")
        If String.IsNullOrEmpty(CodCliente) = False Then
            Dim _Nombre As String = bl.fac_ClientesObtieneNombre(CodCliente)
            If String.IsNullOrEmpty(_Nombre) Or ValidaNoRepetir(CodCliente) = False Then
                e.Valid = False
                gvClientesVendedor.SetColumnError(gv.Columns("IdCliente"), "Código de Cliente no válido, esta vacio, se repite, o no existe.")
            End If
        End If
    End Sub

    Private Sub gvClientesVendedor_KeyDown(sender As Object, e As KeyEventArgs) Handles gvClientesVendedor.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            If gvClientesVendedor.FocusedColumn.FieldName = "IdCliente" Then
                If String.IsNullOrEmpty(gvClientesVendedor.EditingValue) Then
                    Dim entCliente As fac_Clientes = objConsultas.cnsClientes(fac_frmConsultaClientes, "", " IdCliente NOT IN(SELECT IdCliente FROM fac_VendedoresClientes )")
                    If String.IsNullOrEmpty(entCliente.IdCliente) = False Then
                        gvClientesVendedor.EditingValue = entCliente.IdCliente
                        gvClientesVendedor.SetFocusedRowCellValue("Nombre", entCliente.Nombre)
                        Exit Sub
                    End If
                Else
                    Dim _Nombre As String = bl.fac_ClientesObtieneNombre(gvClientesVendedor.EditingValue)
                    If String.IsNullOrEmpty(_Nombre) = False Then
                        gvClientesVendedor.SetFocusedRowCellValue("Nombre", _Nombre)
                    End If
                End If
            End If
        End If
    End Sub

    Public Function ValidaNoRepetir(ByVal _IdCli As String)
        For i = 0 To gvClientesVendedor.DataRowCount - 1
            If gvClientesVendedor.GetRowCellValue(i, "IdCliente").ToString() = (_IdCli) Then
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub sbtnImprimirCRV_Click(sender As Object, e As EventArgs) Handles sbtnImprimirCRV.Click
        Dim _IdVen As Integer = -1
        If MessageBox.Show("Imprimir Actual Vendedor? / NO (Todos)", "Impresion RCV", MessageBoxButtons.YesNo) = DialogResult.Yes Then
            _IdVen = teIdVendedor.EditValue
        End If
        Dim dt As DataTable = bl.fac_rpt_RCV(_IdVen)
        Dim rpt As New fac_rptClientesPorVendedor() With {.DataSource = dt, .DataMember = ""}
        rpt.ShowPreviewDialog()
    End Sub
End Class
