﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmNotaRemision
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gcHeader = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.txtDireccion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.cboDepartamento = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cboMunicipio = New DevExpress.XtraEditors.LookUpEdit()
        Me.txtGiro = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNRC = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNIT = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.cboVendedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.leBodega = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.teContacto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.txtTelefono = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.lePunto = New DevExpress.XtraEditors.LookUpEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.beIdCliente = New DevExpress.XtraEditors.ButtonEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdProducto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdPrecio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.teCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPjeDescuento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.tePjeDescuento = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.tePrecioUnitario = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colPrecioTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcHeader.SuspendLayout()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboDepartamento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboMunicipio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGiro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNIT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teContacto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beIdCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePjeDescuento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePrecioUnitario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcHeader
        '
        Me.gcHeader.Controls.Add(Me.LabelControl6)
        Me.gcHeader.Controls.Add(Me.LabelControl1)
        Me.gcHeader.Controls.Add(Me.teCorrelativo)
        Me.gcHeader.Controls.Add(Me.teNumero)
        Me.gcHeader.Controls.Add(Me.LabelControl2)
        Me.gcHeader.Controls.Add(Me.deFecha)
        Me.gcHeader.Controls.Add(Me.LabelControl3)
        Me.gcHeader.Controls.Add(Me.txtDireccion)
        Me.gcHeader.Controls.Add(Me.LabelControl16)
        Me.gcHeader.Controls.Add(Me.LabelControl4)
        Me.gcHeader.Controls.Add(Me.cboDepartamento)
        Me.gcHeader.Controls.Add(Me.LabelControl5)
        Me.gcHeader.Controls.Add(Me.cboMunicipio)
        Me.gcHeader.Controls.Add(Me.txtGiro)
        Me.gcHeader.Controls.Add(Me.LabelControl7)
        Me.gcHeader.Controls.Add(Me.txtNRC)
        Me.gcHeader.Controls.Add(Me.LabelControl8)
        Me.gcHeader.Controls.Add(Me.txtNIT)
        Me.gcHeader.Controls.Add(Me.LabelControl9)
        Me.gcHeader.Controls.Add(Me.cboVendedor)
        Me.gcHeader.Controls.Add(Me.LabelControl10)
        Me.gcHeader.Controls.Add(Me.leBodega)
        Me.gcHeader.Controls.Add(Me.LabelControl11)
        Me.gcHeader.Controls.Add(Me.teContacto)
        Me.gcHeader.Controls.Add(Me.LabelControl12)
        Me.gcHeader.Controls.Add(Me.txtTelefono)
        Me.gcHeader.Controls.Add(Me.LabelControl13)
        Me.gcHeader.Controls.Add(Me.txtNombre)
        Me.gcHeader.Controls.Add(Me.LabelControl17)
        Me.gcHeader.Controls.Add(Me.LabelControl14)
        Me.gcHeader.Controls.Add(Me.lePunto)
        Me.gcHeader.Controls.Add(Me.leSucursal)
        Me.gcHeader.Controls.Add(Me.LabelControl15)
        Me.gcHeader.Controls.Add(Me.beIdCliente)
        Me.gcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.gcHeader.Location = New System.Drawing.Point(0, 0)
        Me.gcHeader.Name = "gcHeader"
        Me.gcHeader.Size = New System.Drawing.Size(988, 190)
        Me.gcHeader.TabIndex = 0
        Me.gcHeader.Text = "Datos del cliente"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(41, 24)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl6.TabIndex = 32
        Me.LabelControl6.Text = "Correlativo:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(287, 24)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(122, 13)
        Me.LabelControl1.TabIndex = 32
        Me.LabelControl1.Text = "No. de Nota de Remisión:"
        '
        'teCorrelativo
        '
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(100, 21)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Properties.ReadOnly = True
        Me.teCorrelativo.Size = New System.Drawing.Size(112, 20)
        Me.teCorrelativo.TabIndex = 0
        '
        'teNumero
        '
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(412, 21)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(112, 20)
        Me.teNumero.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(607, 25)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl2.TabIndex = 35
        Me.LabelControl2.Text = "Fecha:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(643, 21)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(87, 20)
        Me.deFecha.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(51, 87)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl3.TabIndex = 36
        Me.LabelControl3.Text = "Dirección:"
        '
        'txtDireccion
        '
        Me.txtDireccion.EnterMoveNextControl = True
        Me.txtDireccion.Location = New System.Drawing.Point(100, 84)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(630, 20)
        Me.txtDireccion.TabIndex = 8
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(617, 67)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(23, 13)
        Me.LabelControl16.TabIndex = 39
        Me.LabelControl16.Text = "Giro:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(25, 109)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl4.TabIndex = 39
        Me.LabelControl4.Text = "Departamento:"
        '
        'cboDepartamento
        '
        Me.cboDepartamento.EnterMoveNextControl = True
        Me.cboDepartamento.Location = New System.Drawing.Point(100, 105)
        Me.cboDepartamento.Name = "cboDepartamento"
        Me.cboDepartamento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboDepartamento.Size = New System.Drawing.Size(247, 20)
        Me.cboDepartamento.TabIndex = 8
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(362, 109)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl5.TabIndex = 41
        Me.LabelControl5.Text = "Municipio:"
        '
        'cboMunicipio
        '
        Me.cboMunicipio.EnterMoveNextControl = True
        Me.cboMunicipio.Location = New System.Drawing.Point(412, 105)
        Me.cboMunicipio.Name = "cboMunicipio"
        Me.cboMunicipio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboMunicipio.Size = New System.Drawing.Size(318, 20)
        Me.cboMunicipio.TabIndex = 9
        '
        'txtGiro
        '
        Me.txtGiro.EnterMoveNextControl = True
        Me.txtGiro.Location = New System.Drawing.Point(643, 63)
        Me.txtGiro.Name = "txtGiro"
        Me.txtGiro.Size = New System.Drawing.Size(260, 20)
        Me.txtGiro.TabIndex = 7
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(73, 67)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl7.TabIndex = 44
        Me.LabelControl7.Text = "NRC:"
        '
        'txtNRC
        '
        Me.txtNRC.EnterMoveNextControl = True
        Me.txtNRC.Location = New System.Drawing.Point(100, 63)
        Me.txtNRC.Name = "txtNRC"
        Me.txtNRC.Size = New System.Drawing.Size(112, 20)
        Me.txtNRC.TabIndex = 5
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(388, 67)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl8.TabIndex = 46
        Me.LabelControl8.Text = "NIT:"
        '
        'txtNIT
        '
        Me.txtNIT.EnterMoveNextControl = True
        Me.txtNIT.Location = New System.Drawing.Point(412, 63)
        Me.txtNIT.Name = "txtNIT"
        Me.txtNIT.Size = New System.Drawing.Size(112, 20)
        Me.txtNIT.TabIndex = 6
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(48, 130)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl9.TabIndex = 48
        Me.LabelControl9.Text = "Vendedor:"
        '
        'cboVendedor
        '
        Me.cboVendedor.EnterMoveNextControl = True
        Me.cboVendedor.Location = New System.Drawing.Point(100, 126)
        Me.cboVendedor.Name = "cboVendedor"
        Me.cboVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboVendedor.Size = New System.Drawing.Size(247, 20)
        Me.cboVendedor.TabIndex = 10
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(369, 151)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl10.TabIndex = 50
        Me.LabelControl10.Text = "Bodega:"
        '
        'leBodega
        '
        Me.leBodega.EnterMoveNextControl = True
        Me.leBodega.Location = New System.Drawing.Point(412, 147)
        Me.leBodega.Name = "leBodega"
        Me.leBodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leBodega.Size = New System.Drawing.Size(318, 20)
        Me.leBodega.TabIndex = 13
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(361, 171)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl11.TabIndex = 52
        Me.LabelControl11.Text = "Contacto:"
        '
        'teContacto
        '
        Me.teContacto.EnterMoveNextControl = True
        Me.teContacto.Location = New System.Drawing.Point(412, 168)
        Me.teContacto.Name = "teContacto"
        Me.teContacto.Size = New System.Drawing.Size(318, 20)
        Me.teContacto.TabIndex = 15
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(358, 129)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl12.TabIndex = 54
        Me.LabelControl12.Text = "Teléfonos:"
        '
        'txtTelefono
        '
        Me.txtTelefono.EnterMoveNextControl = True
        Me.txtTelefono.Location = New System.Drawing.Point(412, 126)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(318, 20)
        Me.txtTelefono.TabIndex = 11
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(368, 45)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl13.TabIndex = 56
        Me.LabelControl13.Text = "Nombre:"
        '
        'txtNombre
        '
        Me.txtNombre.EnterMoveNextControl = True
        Me.txtNombre.Location = New System.Drawing.Point(412, 42)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(491, 20)
        Me.txtNombre.TabIndex = 4
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(20, 171)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl17.TabIndex = 58
        Me.LabelControl17.Text = "Punto de Venta:"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(54, 151)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl14.TabIndex = 58
        Me.LabelControl14.Text = "Sucursal:"
        '
        'lePunto
        '
        Me.lePunto.EnterMoveNextControl = True
        Me.lePunto.Location = New System.Drawing.Point(100, 168)
        Me.lePunto.Name = "lePunto"
        Me.lePunto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePunto.Size = New System.Drawing.Size(247, 20)
        Me.lePunto.TabIndex = 14
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(100, 147)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(247, 20)
        Me.leSucursal.TabIndex = 12
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(20, 43)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl15.TabIndex = 60
        Me.LabelControl15.Text = "Cód. de Cliente:"
        '
        'beIdCliente
        '
        Me.beIdCliente.EnterMoveNextControl = True
        Me.beIdCliente.Location = New System.Drawing.Point(100, 42)
        Me.beIdCliente.Name = "beIdCliente"
        Me.beIdCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beIdCliente.Size = New System.Drawing.Size(112, 20)
        Me.beIdCliente.TabIndex = 3
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cmdUp)
        Me.PanelControl1.Controls.Add(Me.cmdDown)
        Me.PanelControl1.Controls.Add(Me.cmdBorrar)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelControl1.Location = New System.Drawing.Point(955, 190)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(33, 173)
        Me.PanelControl1.TabIndex = 1
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(3, 7)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(27, 24)
        Me.cmdUp.TabIndex = 28
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(3, 42)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(27, 24)
        Me.cmdDown.TabIndex = 29
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(3, 77)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 24)
        Me.cmdBorrar.TabIndex = 30
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 190)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.teCantidad, Me.tePjeDescuento, Me.tePrecioUnitario})
        Me.gc.Size = New System.Drawing.Size(955, 173)
        Me.gc.TabIndex = 1
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.gv.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gv.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.gv.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gv.Appearance.TopNewRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.gv.Appearance.TopNewRow.Options.UseBackColor = True
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdProducto, Me.gcIdPrecio, Me.gcCantidad, Me.gcDescripcion, Me.colPjeDescuento, Me.colPrecioUnitario, Me.colPrecioTotal})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsSelection.MultiSelect = True
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdProducto
        '
        Me.gcIdProducto.Caption = "Cód.Producto"
        Me.gcIdProducto.FieldName = "IdProducto"
        Me.gcIdProducto.Name = "gcIdProducto"
        Me.gcIdProducto.Visible = True
        Me.gcIdProducto.VisibleIndex = 0
        Me.gcIdProducto.Width = 89
        '
        'gcIdPrecio
        '
        Me.gcIdPrecio.Caption = "Tipo Precio"
        Me.gcIdPrecio.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcIdPrecio.FieldName = "TipoPrecio"
        Me.gcIdPrecio.Name = "gcIdPrecio"
        Me.gcIdPrecio.Visible = True
        Me.gcIdPrecio.VisibleIndex = 1
        Me.gcIdPrecio.Width = 81
        '
        'gcCantidad
        '
        Me.gcCantidad.Caption = "Cantidad"
        Me.gcCantidad.ColumnEdit = Me.teCantidad
        Me.gcCantidad.FieldName = "Cantidad"
        Me.gcCantidad.Name = "gcCantidad"
        Me.gcCantidad.Visible = True
        Me.gcCantidad.VisibleIndex = 2
        Me.gcCantidad.Width = 74
        '
        'teCantidad
        '
        Me.teCantidad.AutoHeight = False
        Me.teCantidad.Mask.EditMask = "n5"
        Me.teCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.teCantidad.Name = "teCantidad"
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Descripción / Concepto de la venta"
        Me.gcDescripcion.FieldName = "Descripcion"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 3
        Me.gcDescripcion.Width = 248
        '
        'colPjeDescuento
        '
        Me.colPjeDescuento.Caption = "% de Descto"
        Me.colPjeDescuento.ColumnEdit = Me.tePjeDescuento
        Me.colPjeDescuento.FieldName = "PjeDescuento"
        Me.colPjeDescuento.Name = "colPjeDescuento"
        Me.colPjeDescuento.Visible = True
        Me.colPjeDescuento.VisibleIndex = 4
        Me.colPjeDescuento.Width = 112
        '
        'tePjeDescuento
        '
        Me.tePjeDescuento.AutoHeight = False
        Me.tePjeDescuento.Mask.EditMask = "n2"
        Me.tePjeDescuento.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tePjeDescuento.Mask.UseMaskAsDisplayFormat = True
        Me.tePjeDescuento.Name = "tePjeDescuento"
        '
        'colPrecioUnitario
        '
        Me.colPrecioUnitario.Caption = "Precio Unitario"
        Me.colPrecioUnitario.ColumnEdit = Me.tePrecioUnitario
        Me.colPrecioUnitario.FieldName = "PrecioUnitario"
        Me.colPrecioUnitario.Name = "colPrecioUnitario"
        Me.colPrecioUnitario.Visible = True
        Me.colPrecioUnitario.VisibleIndex = 5
        Me.colPrecioUnitario.Width = 104
        '
        'tePrecioUnitario
        '
        Me.tePrecioUnitario.AutoHeight = False
        Me.tePrecioUnitario.Mask.EditMask = "n6"
        Me.tePrecioUnitario.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tePrecioUnitario.Mask.UseMaskAsDisplayFormat = True
        Me.tePrecioUnitario.Name = "tePrecioUnitario"
        '
        'colPrecioTotal
        '
        Me.colPrecioTotal.Caption = "Precio Total"
        Me.colPrecioTotal.DisplayFormat.FormatString = "n2"
        Me.colPrecioTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colPrecioTotal.FieldName = "PrecioTotal"
        Me.colPrecioTotal.Name = "colPrecioTotal"
        Me.colPrecioTotal.OptionsColumn.AllowEdit = False
        Me.colPrecioTotal.OptionsColumn.AllowFocus = False
        Me.colPrecioTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PrecioTotal", "{0:n2}")})
        Me.colPrecioTotal.Visible = True
        Me.colPrecioTotal.VisibleIndex = 6
        Me.colPrecioTotal.Width = 103
        '
        'fac_frmNotaRemision
        '
        Me.ClientSize = New System.Drawing.Size(988, 388)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.gcHeader)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmNotaRemision"
        Me.OptionId = "002002"
        Me.Text = "Notas de Remisión"
        Me.Controls.SetChildIndex(Me.gcHeader, 0)
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcHeader.ResumeLayout(False)
        Me.gcHeader.PerformLayout()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboDepartamento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboMunicipio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGiro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNIT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teContacto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beIdCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePjeDescuento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePrecioUnitario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gcHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cboDepartamento As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cboMunicipio As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents txtGiro As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNIT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cboVendedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leBodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teContacto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtTelefono As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beIdCliente As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPjeDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tePjeDescuento As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tePrecioUnitario As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colPrecioTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lePunto As DevExpress.XtraEditors.LookUpEdit

End Class
