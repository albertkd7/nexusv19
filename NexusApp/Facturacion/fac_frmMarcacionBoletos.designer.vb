<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmMarcacionBoletos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmMarcacionBoletos))
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.gcFP = New DevExpress.XtraGrid.GridControl()
        Me.gvFP = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcCorrel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcVentaNoSujeta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcVentaExenta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcVentaAfecta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIVA = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.riteFacturar = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.teValorNoSujeto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teExento = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teAfecto = New DevExpress.XtraEditors.TextEdit()
        Me.sbAgregar = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.gcFP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvFP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteFacturar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teValorNoSujeto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teExento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teAfecto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(78, 371)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(165, 32)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Agregar a Facturación"
        '
        'sbCancel
        '
        Me.sbCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sbCancel.Appearance.Options.UseFont = True
        Me.sbCancel.Location = New System.Drawing.Point(256, 371)
        Me.sbCancel.Name = "sbCancel"
        Me.sbCancel.Size = New System.Drawing.Size(95, 32)
        Me.sbCancel.TabIndex = 2
        Me.sbCancel.Text = "Cancelar"
        '
        'gcFP
        '
        Me.gcFP.Location = New System.Drawing.Point(12, 125)
        Me.gcFP.MainView = Me.gvFP
        Me.gcFP.Name = "gcFP"
        Me.gcFP.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar, Me.riteFacturar})
        Me.gcFP.Size = New System.Drawing.Size(434, 240)
        Me.gcFP.TabIndex = 6
        Me.gcFP.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvFP})
        '
        'gvFP
        '
        Me.gvFP.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcCorrel, Me.gcFecha, Me.gcNumero, Me.gcVentaNoSujeta, Me.gcVentaExenta, Me.gcVentaAfecta, Me.gcIVA, Me.gcNit, Me.gcIdComprobante})
        Me.gvFP.GridControl = Me.gcFP
        Me.gvFP.Name = "gvFP"
        Me.gvFP.OptionsView.ShowFooter = True
        Me.gvFP.OptionsView.ShowGroupPanel = False
        '
        'gcCorrel
        '
        Me.gcCorrel.Caption = "No. Boleto"
        Me.gcCorrel.FieldName = "IdComprobante"
        Me.gcCorrel.Name = "gcCorrel"
        Me.gcCorrel.OptionsColumn.AllowEdit = False
        Me.gcCorrel.OptionsColumn.AllowFocus = False
        Me.gcCorrel.OptionsColumn.AllowMove = False
        Me.gcCorrel.Width = 47
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha"
        Me.gcFecha.FieldName = "Fecha"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.OptionsColumn.AllowEdit = False
        Me.gcFecha.OptionsColumn.AllowFocus = False
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 1
        Me.gcFecha.Width = 68
        '
        'gcNumero
        '
        Me.gcNumero.Caption = "No. Boleto"
        Me.gcNumero.FieldName = "Numero"
        Me.gcNumero.Name = "gcNumero"
        Me.gcNumero.OptionsColumn.AllowEdit = False
        Me.gcNumero.OptionsColumn.AllowFocus = False
        Me.gcNumero.Visible = True
        Me.gcNumero.VisibleIndex = 0
        Me.gcNumero.Width = 66
        '
        'gcVentaNoSujeta
        '
        Me.gcVentaNoSujeta.Caption = "No Sujeta"
        Me.gcVentaNoSujeta.FieldName = "TotalNoSujeto"
        Me.gcVentaNoSujeta.Name = "gcVentaNoSujeta"
        Me.gcVentaNoSujeta.Visible = True
        Me.gcVentaNoSujeta.VisibleIndex = 2
        Me.gcVentaNoSujeta.Width = 78
        '
        'gcVentaExenta
        '
        Me.gcVentaExenta.Caption = "Exenta"
        Me.gcVentaExenta.FieldName = "TotalExento"
        Me.gcVentaExenta.Name = "gcVentaExenta"
        Me.gcVentaExenta.Visible = True
        Me.gcVentaExenta.VisibleIndex = 3
        Me.gcVentaExenta.Width = 83
        '
        'gcVentaAfecta
        '
        Me.gcVentaAfecta.Caption = "Afecta"
        Me.gcVentaAfecta.FieldName = "TotalAfecto"
        Me.gcVentaAfecta.Name = "gcVentaAfecta"
        Me.gcVentaAfecta.Visible = True
        Me.gcVentaAfecta.VisibleIndex = 4
        Me.gcVentaAfecta.Width = 84
        '
        'gcIVA
        '
        Me.gcIVA.Caption = "IVA"
        Me.gcIVA.FieldName = "TotalIva"
        Me.gcIVA.Name = "gcIVA"
        '
        'gcNit
        '
        Me.gcNit.Caption = "Nit"
        Me.gcNit.FieldName = "Nit"
        Me.gcNit.Name = "gcNit"
        '
        'gcIdComprobante
        '
        Me.gcIdComprobante.Caption = "Id Comp"
        Me.gcIdComprobante.FieldName = "IdComprobante"
        Me.gcIdComprobante.Name = "gcIdComprobante"
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'riteFacturar
        '
        Me.riteFacturar.AutoHeight = False
        Me.riteFacturar.EditFormat.FormatString = "n2"
        Me.riteFacturar.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteFacturar.Mask.EditMask = "n2"
        Me.riteFacturar.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteFacturar.Mask.UseMaskAsDisplayFormat = True
        Me.riteFacturar.Name = "riteFacturar"
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(130, 37)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl19.TabIndex = 79
        Me.LabelControl19.Text = "Fecha:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(170, 34)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.deFecha.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.deFecha.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.deFecha.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(123, 26)
        Me.deFecha.TabIndex = 1
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(94, 15)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl3.TabIndex = 81
        Me.LabelControl3.Text = "No. de Boleto:"
        '
        'teNumero
        '
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(170, 12)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(73, 26)
        Me.teNumero.TabIndex = 0
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(89, 61)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl20.TabIndex = 83
        Me.LabelControl20.Text = "Valor No Sujeto:"
        '
        'teValorNoSujeto
        '
        Me.teValorNoSujeto.EnterMoveNextControl = True
        Me.teValorNoSujeto.Location = New System.Drawing.Point(170, 56)
        Me.teValorNoSujeto.Name = "teValorNoSujeto"
        Me.teValorNoSujeto.Properties.ReadOnly = True
        Me.teValorNoSujeto.Size = New System.Drawing.Size(123, 26)
        Me.teValorNoSujeto.TabIndex = 82
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(102, 80)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl1.TabIndex = 85
        Me.LabelControl1.Text = "Valor Exento:"
        '
        'teExento
        '
        Me.teExento.EnterMoveNextControl = True
        Me.teExento.Location = New System.Drawing.Point(170, 77)
        Me.teExento.Name = "teExento"
        Me.teExento.Properties.ReadOnly = True
        Me.teExento.Size = New System.Drawing.Size(123, 26)
        Me.teExento.TabIndex = 84
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(104, 102)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(63, 13)
        Me.LabelControl2.TabIndex = 87
        Me.LabelControl2.Text = "Valor Afecto:"
        '
        'teAfecto
        '
        Me.teAfecto.EnterMoveNextControl = True
        Me.teAfecto.Location = New System.Drawing.Point(170, 99)
        Me.teAfecto.Name = "teAfecto"
        Me.teAfecto.Properties.ReadOnly = True
        Me.teAfecto.Size = New System.Drawing.Size(123, 26)
        Me.teAfecto.TabIndex = 86
        '
        'sbAgregar
        '
        Me.sbAgregar.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sbAgregar.Appearance.Options.UseFont = True
        Me.sbAgregar.Location = New System.Drawing.Point(299, 34)
        Me.sbAgregar.Name = "sbAgregar"
        Me.sbAgregar.Size = New System.Drawing.Size(67, 20)
        Me.sbAgregar.TabIndex = 2
        Me.sbAgregar.Text = "Agregar..."
        '
        'fac_frmMarcacionBoletos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(458, 415)
        Me.ControlBox = False
        Me.Controls.Add(Me.sbAgregar)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.teAfecto)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.teExento)
        Me.Controls.Add(Me.LabelControl20)
        Me.Controls.Add(Me.teValorNoSujeto)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.teNumero)
        Me.Controls.Add(Me.LabelControl19)
        Me.Controls.Add(Me.deFecha)
        Me.Controls.Add(Me.gcFP)
        Me.Controls.Add(Me.sbCancel)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fac_frmMarcacionBoletos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Obtener Facturación de Boletos"
        CType(Me.gcFP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvFP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteFacturar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teValorNoSujeto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teExento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teAfecto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcFP As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvFP As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcCorrel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents riteFacturar As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcVentaNoSujeta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcVentaExenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcVentaAfecta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIVA As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teValorNoSujeto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teExento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teAfecto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents sbAgregar As DevExpress.XtraEditors.SimpleButton
End Class
