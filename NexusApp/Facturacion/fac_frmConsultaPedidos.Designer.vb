<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmConsultaPedidos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gcConsultaPed = New DevExpress.XtraGrid.GridControl()
        Me.gvConsultaPed = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdVendedor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdBodega = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCreadoPor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFacturado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.sbSalida = New DevExpress.XtraEditors.SimpleButton()
        Me.sbConsultaPed = New DevExpress.XtraEditors.SimpleButton()
        Me.deHasta = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.deDesde = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.gcConsultaPed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvConsultaPed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcConsultaPed
        '
        Me.gcConsultaPed.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcConsultaPed.Location = New System.Drawing.Point(0, 68)
        Me.gcConsultaPed.MainView = Me.gvConsultaPed
        Me.gcConsultaPed.Name = "gcConsultaPed"
        Me.gcConsultaPed.Size = New System.Drawing.Size(844, 441)
        Me.gcConsultaPed.TabIndex = 0
        Me.gcConsultaPed.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvConsultaPed, Me.GridView2})
        '
        'gvConsultaPed
        '
        Me.gvConsultaPed.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdComprobante, Me.gcNumero, Me.gcNombre, Me.gcFecha, Me.gcIdVendedor, Me.gcIdBodega, Me.gcCreadoPor, Me.gcTotal, Me.gcFacturado})
        Me.gvConsultaPed.GridControl = Me.gcConsultaPed
        Me.gvConsultaPed.Name = "gvConsultaPed"
        Me.gvConsultaPed.OptionsBehavior.Editable = False
        Me.gvConsultaPed.OptionsView.ShowAutoFilterRow = True
        '
        'gcIdComprobante
        '
        Me.gcIdComprobante.Caption = "Correlativo"
        Me.gcIdComprobante.FieldName = "IdComprobante"
        Me.gcIdComprobante.Name = "gcIdComprobante"
        Me.gcIdComprobante.Visible = True
        Me.gcIdComprobante.VisibleIndex = 0
        Me.gcIdComprobante.Width = 83
        '
        'gcNumero
        '
        Me.gcNumero.Caption = "Numero"
        Me.gcNumero.FieldName = "Numero"
        Me.gcNumero.Name = "gcNumero"
        Me.gcNumero.Visible = True
        Me.gcNumero.VisibleIndex = 1
        Me.gcNumero.Width = 69
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 2
        Me.gcNombre.Width = 259
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha"
        Me.gcFecha.FieldName = "Fecha"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 3
        Me.gcFecha.Width = 70
        '
        'gcIdVendedor
        '
        Me.gcIdVendedor.Caption = "IdVendedor"
        Me.gcIdVendedor.FieldName = "IdVendedor"
        Me.gcIdVendedor.Name = "gcIdVendedor"
        Me.gcIdVendedor.Visible = True
        Me.gcIdVendedor.VisibleIndex = 4
        Me.gcIdVendedor.Width = 72
        '
        'gcIdBodega
        '
        Me.gcIdBodega.Caption = "IdBodega"
        Me.gcIdBodega.FieldName = "IdBodega"
        Me.gcIdBodega.Name = "gcIdBodega"
        Me.gcIdBodega.Visible = True
        Me.gcIdBodega.VisibleIndex = 5
        Me.gcIdBodega.Width = 60
        '
        'gcCreadoPor
        '
        Me.gcCreadoPor.Caption = "Creado Por"
        Me.gcCreadoPor.FieldName = "CreadoPor"
        Me.gcCreadoPor.Name = "gcCreadoPor"
        Me.gcCreadoPor.Visible = True
        Me.gcCreadoPor.VisibleIndex = 6
        Me.gcCreadoPor.Width = 72
        '
        'gcTotal
        '
        Me.gcTotal.Caption = "Total"
        Me.gcTotal.FieldName = "Total"
        Me.gcTotal.Name = "gcTotal"
        Me.gcTotal.Visible = True
        Me.gcTotal.VisibleIndex = 7
        Me.gcTotal.Width = 63
        '
        'gcFacturado
        '
        Me.gcFacturado.Caption = "Facturado"
        Me.gcFacturado.FieldName = "Facturado"
        Me.gcFacturado.Name = "gcFacturado"
        Me.gcFacturado.Visible = True
        Me.gcFacturado.VisibleIndex = 8
        Me.gcFacturado.Width = 59
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.gcConsultaPed
        Me.GridView2.Name = "GridView2"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.sbSalida)
        Me.GroupControl1.Controls.Add(Me.sbConsultaPed)
        Me.GroupControl1.Controls.Add(Me.deHasta)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.deDesde)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(844, 68)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Consulta de pedidos"
        '
        'sbSalida
        '
        Me.sbSalida.Location = New System.Drawing.Point(362, 26)
        Me.sbSalida.Name = "sbSalida"
        Me.sbSalida.Size = New System.Drawing.Size(122, 23)
        Me.sbSalida.TabIndex = 4
        Me.sbSalida.Text = "&Salir"
        '
        'sbConsultaPed
        '
        Me.sbConsultaPed.Location = New System.Drawing.Point(234, 26)
        Me.sbConsultaPed.Name = "sbConsultaPed"
        Me.sbConsultaPed.Size = New System.Drawing.Size(122, 23)
        Me.sbConsultaPed.TabIndex = 4
        Me.sbConsultaPed.Text = "Generar consulta"
        '
        'deHasta
        '
        Me.deHasta.EditValue = Nothing
        Me.deHasta.EnterMoveNextControl = True
        Me.deHasta.Location = New System.Drawing.Point(114, 43)
        Me.deHasta.Name = "deHasta"
        Me.deHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deHasta.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deHasta.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deHasta.Size = New System.Drawing.Size(100, 20)
        Me.deHasta.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(47, 46)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Hasta Fecha:"
        '
        'deDesde
        '
        Me.deDesde.EditValue = Nothing
        Me.deDesde.EnterMoveNextControl = True
        Me.deDesde.Location = New System.Drawing.Point(114, 22)
        Me.deDesde.Name = "deDesde"
        Me.deDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deDesde.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deDesde.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deDesde.Size = New System.Drawing.Size(100, 20)
        Me.deDesde.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(45, 26)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Desde Fecha:"
        '
        'fac_frmConsultaPedidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(844, 509)
        Me.Controls.Add(Me.gcConsultaPed)
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fac_frmConsultaPedidos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Consulta de pedidos"
        CType(Me.gcConsultaPed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvConsultaPed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gcConsultaPed As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvConsultaPed As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdVendedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCreadoPor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFacturado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents sbConsultaPed As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents deHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbSalida As DevExpress.XtraEditors.SimpleButton
End Class
