<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmProductosConDescuento
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmProductosConDescuento))
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.btnRegresar = New DevExpress.XtraEditors.SimpleButton()
        Me.btnImprimir = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAceptar = New DevExpress.XtraEditors.SimpleButton()
        Me.tePassWord = New DevExpress.XtraEditors.TextEdit()
        Me.teUsuario = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.gcProdDesc = New DevExpress.XtraGrid.GridControl()
        Me.gvProdDesc = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcReferencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPrecioCosto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCostoTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcTotalFacturado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcTotalPerdida = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.tePassWord.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teUsuario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcProdDesc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvProdDesc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.btnRegresar)
        Me.PanelControl1.Controls.Add(Me.btnImprimir)
        Me.PanelControl1.Controls.Add(Me.btnAceptar)
        Me.PanelControl1.Controls.Add(Me.tePassWord)
        Me.PanelControl1.Controls.Add(Me.teUsuario)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(878, 48)
        Me.PanelControl1.TabIndex = 0
        '
        'btnRegresar
        '
        Me.btnRegresar.Location = New System.Drawing.Point(446, 23)
        Me.btnRegresar.Name = "btnRegresar"
        Me.btnRegresar.Size = New System.Drawing.Size(75, 23)
        Me.btnRegresar.TabIndex = 3
        Me.btnRegresar.Text = "&Regresar"
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(367, 23)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 3
        Me.btnImprimir.Text = "&Imprimir"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(290, 23)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "&Proceder"
        '
        'tePassWord
        '
        Me.tePassWord.EnterMoveNextControl = True
        Me.tePassWord.Location = New System.Drawing.Point(110, 26)
        Me.tePassWord.Name = "tePassWord"
        Me.tePassWord.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(63)
        Me.tePassWord.Size = New System.Drawing.Size(171, 20)
        Me.tePassWord.TabIndex = 1
        '
        'teUsuario
        '
        Me.teUsuario.EnterMoveNextControl = True
        Me.teUsuario.Location = New System.Drawing.Point(110, 5)
        Me.teUsuario.Name = "teUsuario"
        Me.teUsuario.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teUsuario.Size = New System.Drawing.Size(171, 20)
        Me.teUsuario.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(48, 30)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Contrase�a:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(4, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(104, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Usuario que Autoriza:"
        '
        'gcProdDesc
        '
        Me.gcProdDesc.Location = New System.Drawing.Point(0, 48)
        Me.gcProdDesc.MainView = Me.gvProdDesc
        Me.gcProdDesc.Name = "gcProdDesc"
        Me.gcProdDesc.Size = New System.Drawing.Size(872, 307)
        Me.gcProdDesc.TabIndex = 1
        Me.gcProdDesc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvProdDesc})
        '
        'gvProdDesc
        '
        Me.gvProdDesc.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcReferencia, Me.gcNombre, Me.gcCantidad, Me.gcPrecioCosto, Me.gcCostoTotal, Me.gcPrecioUnitario, Me.gcTotalFacturado, Me.gcTotalPerdida})
        Me.gvProdDesc.GridControl = Me.gcProdDesc
        Me.gvProdDesc.Name = "gvProdDesc"
        Me.gvProdDesc.OptionsBehavior.Editable = False
        Me.gvProdDesc.OptionsView.ShowFooter = True
        Me.gvProdDesc.OptionsView.ShowGroupPanel = False
        '
        'gcReferencia
        '
        Me.gcReferencia.Caption = "C�d.Producto"
        Me.gcReferencia.FieldName = "IdProducto"
        Me.gcReferencia.Name = "gcReferencia"
        Me.gcReferencia.Visible = True
        Me.gcReferencia.VisibleIndex = 0
        Me.gcReferencia.Width = 103
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 1
        Me.gcNombre.Width = 169
        '
        'gcCantidad
        '
        Me.gcCantidad.Caption = "Cantidad"
        Me.gcCantidad.DisplayFormat.FormatString = "n2"
        Me.gcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcCantidad.FieldName = "Cantidad"
        Me.gcCantidad.Name = "gcCantidad"
        Me.gcCantidad.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Cantidad", "{0:n2}")})
        Me.gcCantidad.Visible = True
        Me.gcCantidad.VisibleIndex = 2
        Me.gcCantidad.Width = 84
        '
        'gcPrecioCosto
        '
        Me.gcPrecioCosto.Caption = "Precio de Lista"
        Me.gcPrecioCosto.DisplayFormat.FormatString = "n2"
        Me.gcPrecioCosto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioCosto.FieldName = "PrecioVenta"
        Me.gcPrecioCosto.Name = "gcPrecioCosto"
        Me.gcPrecioCosto.Visible = True
        Me.gcPrecioCosto.VisibleIndex = 3
        Me.gcPrecioCosto.Width = 100
        '
        'gcCostoTotal
        '
        Me.gcCostoTotal.Caption = "Total"
        Me.gcCostoTotal.DisplayFormat.FormatString = "n2"
        Me.gcCostoTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcCostoTotal.FieldName = "TotalVenta"
        Me.gcCostoTotal.Name = "gcCostoTotal"
        Me.gcCostoTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCosto", "{0:n2}")})
        Me.gcCostoTotal.Visible = True
        Me.gcCostoTotal.VisibleIndex = 4
        Me.gcCostoTotal.Width = 91
        '
        'gcPrecioUnitario
        '
        Me.gcPrecioUnitario.Caption = "Precio c/Descuento"
        Me.gcPrecioUnitario.DisplayFormat.FormatString = "n2"
        Me.gcPrecioUnitario.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioUnitario.FieldName = "PrecioUnitario"
        Me.gcPrecioUnitario.Name = "gcPrecioUnitario"
        Me.gcPrecioUnitario.Visible = True
        Me.gcPrecioUnitario.VisibleIndex = 5
        Me.gcPrecioUnitario.Width = 104
        '
        'gcTotalFacturado
        '
        Me.gcTotalFacturado.Caption = "Total Facturado"
        Me.gcTotalFacturado.DisplayFormat.FormatString = "n2"
        Me.gcTotalFacturado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcTotalFacturado.FieldName = "TotalFacturado"
        Me.gcTotalFacturado.Name = "gcTotalFacturado"
        Me.gcTotalFacturado.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalFacturado", "{0:n2}")})
        Me.gcTotalFacturado.Visible = True
        Me.gcTotalFacturado.VisibleIndex = 6
        Me.gcTotalFacturado.Width = 93
        '
        'gcTotalPerdida
        '
        Me.gcTotalPerdida.Caption = "Diferencia"
        Me.gcTotalPerdida.FieldName = "Diferencia"
        Me.gcTotalPerdida.Name = "gcTotalPerdida"
        Me.gcTotalPerdida.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalPerdida", "{0:n2}")})
        Me.gcTotalPerdida.Visible = True
        Me.gcTotalPerdida.VisibleIndex = 7
        Me.gcTotalPerdida.Width = 110
        '
        'fac_frmProductosConDescuento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(878, 361)
        Me.Controls.Add(Me.gcProdDesc)
        Me.Controls.Add(Me.PanelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fac_frmProductosConDescuento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Productos ingresados con diferencia de precios"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.tePassWord.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teUsuario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcProdDesc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvProdDesc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gvProdDesc As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcReferencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCostoTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTotalFacturado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnAceptar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tePassWord As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teUsuario As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnImprimir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRegresar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcProdDesc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gcTotalPerdida As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
End Class
