﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmClientes
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xtcClientes = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdCliente = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNrc = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcTelefonos = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDireccion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.teOtroDocumento = New DevExpress.XtraEditors.TextEdit()
        Me.sbDeclaracionJuradaPersonaNatural = New DevExpress.XtraEditors.SimpleButton()
        Me.sbInfoAdicional = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.rgTipo = New DevExpress.XtraEditors.RadioGroup()
        Me.ceBloquear = New DevExpress.XtraEditors.CheckEdit()
        Me.sbImprimeLista = New DevExpress.XtraEditors.SimpleButton()
        Me.sePorcDescuento = New DevExpress.XtraEditors.SpinEdit()
        Me.seDiasCredito = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.seLimite = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.ceAplicaPer_Ret = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombreCuenta = New DevExpress.XtraEditors.TextEdit()
        Me.beIdCuenta = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.leRuta = New DevExpress.XtraEditors.LookUpEdit()
        Me.leMunicipio = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoPrecio = New DevExpress.XtraEditors.LookUpEdit()
        Me.leFormaPago = New DevExpress.XtraEditors.LookUpEdit()
        Me.leTipoDocto = New DevExpress.XtraEditors.LookUpEdit()
        Me.leTipoImpuesto = New DevExpress.XtraEditors.LookUpEdit()
        Me.leVendedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.leDepartamento = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teNIT = New DevExpress.XtraEditors.TextEdit()
        Me.teFax = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.teCorreoElectronico = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.teTelefonos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.teNRC = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.teDireccion = New DevExpress.XtraEditors.TextEdit()
        Me.teGiro = New DevExpress.XtraEditors.TextEdit()
        Me.teRazonSocial = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teCodigo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.xtpDatos2 = New DevExpress.XtraTab.XtraTabPage()
        Me.gcPre = New DevExpress.XtraGrid.GridControl()
        Me.gvPre = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.teCod = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.xtcClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcClientes.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.teOtroDocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceBloquear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sePorcDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seLimite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceAplicaPer_Ret.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombreCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beIdCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leRuta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leMunicipio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPrecio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoDocto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoImpuesto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leDepartamento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teFax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorreoElectronico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teGiro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teRazonSocial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos2.SuspendLayout()
        CType(Me.gcPre, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPre, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCod, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        Me.SuspendLayout()
        '
        'xtcClientes
        '
        Me.xtcClientes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcClientes.Location = New System.Drawing.Point(0, 0)
        Me.xtcClientes.Name = "xtcClientes"
        Me.xtcClientes.SelectedTabPage = Me.xtpLista
        Me.xtcClientes.Size = New System.Drawing.Size(856, 473)
        Me.xtcClientes.TabIndex = 0
        Me.xtcClientes.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos, Me.xtpDatos2})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(850, 445)
        Me.xtpLista.Text = "Lista (Doble clic para Editar)"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(850, 445)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdCliente, Me.gcNombre, Me.gcNrc, Me.gcNit, Me.gcTelefonos, Me.gcDireccion})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowAutoFilterRow = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdCliente
        '
        Me.gcIdCliente.Caption = "Cód.Cliente"
        Me.gcIdCliente.FieldName = "IdCliente"
        Me.gcIdCliente.Name = "gcIdCliente"
        Me.gcIdCliente.Visible = True
        Me.gcIdCliente.VisibleIndex = 0
        Me.gcIdCliente.Width = 116
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 1
        Me.gcNombre.Width = 419
        '
        'gcNrc
        '
        Me.gcNrc.Caption = "Nrc"
        Me.gcNrc.FieldName = "Nrc"
        Me.gcNrc.Name = "gcNrc"
        Me.gcNrc.Visible = True
        Me.gcNrc.VisibleIndex = 2
        Me.gcNrc.Width = 143
        '
        'gcNit
        '
        Me.gcNit.Caption = "Nit"
        Me.gcNit.FieldName = "Nit"
        Me.gcNit.Name = "gcNit"
        Me.gcNit.Visible = True
        Me.gcNit.VisibleIndex = 3
        Me.gcNit.Width = 148
        '
        'gcTelefonos
        '
        Me.gcTelefonos.Caption = "Teléfonos"
        Me.gcTelefonos.FieldName = "Telefonos"
        Me.gcTelefonos.Name = "gcTelefonos"
        Me.gcTelefonos.Visible = True
        Me.gcTelefonos.VisibleIndex = 4
        Me.gcTelefonos.Width = 101
        '
        'gcDireccion
        '
        Me.gcDireccion.Caption = "Dirección"
        Me.gcDireccion.FieldName = "Direccion"
        Me.gcDireccion.Name = "gcDireccion"
        Me.gcDireccion.Visible = True
        Me.gcDireccion.VisibleIndex = 5
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.LabelControl17)
        Me.xtpDatos.Controls.Add(Me.teOtroDocumento)
        Me.xtpDatos.Controls.Add(Me.sbDeclaracionJuradaPersonaNatural)
        Me.xtpDatos.Controls.Add(Me.sbInfoAdicional)
        Me.xtpDatos.Controls.Add(Me.LabelControl25)
        Me.xtpDatos.Controls.Add(Me.rgTipo)
        Me.xtpDatos.Controls.Add(Me.ceBloquear)
        Me.xtpDatos.Controls.Add(Me.sbImprimeLista)
        Me.xtpDatos.Controls.Add(Me.sePorcDescuento)
        Me.xtpDatos.Controls.Add(Me.seDiasCredito)
        Me.xtpDatos.Controls.Add(Me.LabelControl18)
        Me.xtpDatos.Controls.Add(Me.seLimite)
        Me.xtpDatos.Controls.Add(Me.LabelControl16)
        Me.xtpDatos.Controls.Add(Me.ceAplicaPer_Ret)
        Me.xtpDatos.Controls.Add(Me.LabelControl15)
        Me.xtpDatos.Controls.Add(Me.LabelControl23)
        Me.xtpDatos.Controls.Add(Me.LabelControl22)
        Me.xtpDatos.Controls.Add(Me.LabelControl20)
        Me.xtpDatos.Controls.Add(Me.LabelControl19)
        Me.xtpDatos.Controls.Add(Me.LabelControl14)
        Me.xtpDatos.Controls.Add(Me.teNombreCuenta)
        Me.xtpDatos.Controls.Add(Me.beIdCuenta)
        Me.xtpDatos.Controls.Add(Me.LabelControl13)
        Me.xtpDatos.Controls.Add(Me.leRuta)
        Me.xtpDatos.Controls.Add(Me.leMunicipio)
        Me.xtpDatos.Controls.Add(Me.LabelControl24)
        Me.xtpDatos.Controls.Add(Me.LabelControl21)
        Me.xtpDatos.Controls.Add(Me.LabelControl12)
        Me.xtpDatos.Controls.Add(Me.leTipoPrecio)
        Me.xtpDatos.Controls.Add(Me.leFormaPago)
        Me.xtpDatos.Controls.Add(Me.leTipoDocto)
        Me.xtpDatos.Controls.Add(Me.leTipoImpuesto)
        Me.xtpDatos.Controls.Add(Me.leVendedor)
        Me.xtpDatos.Controls.Add(Me.leDepartamento)
        Me.xtpDatos.Controls.Add(Me.LabelControl11)
        Me.xtpDatos.Controls.Add(Me.LabelControl7)
        Me.xtpDatos.Controls.Add(Me.LabelControl6)
        Me.xtpDatos.Controls.Add(Me.LabelControl5)
        Me.xtpDatos.Controls.Add(Me.teNIT)
        Me.xtpDatos.Controls.Add(Me.teFax)
        Me.xtpDatos.Controls.Add(Me.LabelControl9)
        Me.xtpDatos.Controls.Add(Me.teCorreoElectronico)
        Me.xtpDatos.Controls.Add(Me.LabelControl10)
        Me.xtpDatos.Controls.Add(Me.teTelefonos)
        Me.xtpDatos.Controls.Add(Me.LabelControl8)
        Me.xtpDatos.Controls.Add(Me.teNRC)
        Me.xtpDatos.Controls.Add(Me.LabelControl4)
        Me.xtpDatos.Controls.Add(Me.teDireccion)
        Me.xtpDatos.Controls.Add(Me.teGiro)
        Me.xtpDatos.Controls.Add(Me.teRazonSocial)
        Me.xtpDatos.Controls.Add(Me.LabelControl3)
        Me.xtpDatos.Controls.Add(Me.teNombre)
        Me.xtpDatos.Controls.Add(Me.LabelControl2)
        Me.xtpDatos.Controls.Add(Me.teCodigo)
        Me.xtpDatos.Controls.Add(Me.LabelControl1)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(850, 445)
        Me.xtpDatos.Text = "Datos del Cliente"
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(652, 84)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl17.TabIndex = 95
        Me.LabelControl17.Text = "DUI:"
        '
        'teOtroDocumento
        '
        Me.teOtroDocumento.EnterMoveNextControl = True
        Me.teOtroDocumento.Location = New System.Drawing.Point(675, 80)
        Me.teOtroDocumento.Name = "teOtroDocumento"
        Me.teOtroDocumento.Size = New System.Drawing.Size(170, 20)
        Me.teOtroDocumento.TabIndex = 94
        '
        'sbDeclaracionJuradaPersonaNatural
        '
        Me.sbDeclaracionJuradaPersonaNatural.Location = New System.Drawing.Point(175, 406)
        Me.sbDeclaracionJuradaPersonaNatural.Name = "sbDeclaracionJuradaPersonaNatural"
        Me.sbDeclaracionJuradaPersonaNatural.Size = New System.Drawing.Size(159, 23)
        Me.sbDeclaracionJuradaPersonaNatural.TabIndex = 93
        Me.sbDeclaracionJuradaPersonaNatural.Text = "Imprimir Declaración Jurada"
        '
        'sbInfoAdicional
        '
        Me.sbInfoAdicional.Image = Global.Nexus.My.Resources.Resources.Modify
        Me.sbInfoAdicional.Location = New System.Drawing.Point(698, 1)
        Me.sbInfoAdicional.Name = "sbInfoAdicional"
        Me.sbInfoAdicional.Size = New System.Drawing.Size(43, 37)
        Me.sbInfoAdicional.TabIndex = 91
        Me.sbInfoAdicional.ToolTip = "Información Adicional del Cliente"
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(78, 322)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl25.TabIndex = 90
        Me.LabelControl25.Text = "Impuesto Adicional:"
        '
        'rgTipo
        '
        Me.rgTipo.EditValue = 1
        Me.rgTipo.Location = New System.Drawing.Point(175, 318)
        Me.rgTipo.Name = "rgTipo"
        Me.rgTipo.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Ninguno"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Retención"), New DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Percepción")})
        Me.rgTipo.Size = New System.Drawing.Size(271, 21)
        Me.rgTipo.TabIndex = 22
        '
        'ceBloquear
        '
        Me.ceBloquear.Location = New System.Drawing.Point(689, 230)
        Me.ceBloquear.Name = "ceBloquear"
        Me.ceBloquear.Properties.Caption = "Bloquear Cliente en Pedidos"
        Me.ceBloquear.Size = New System.Drawing.Size(155, 19)
        Me.ceBloquear.TabIndex = 16
        '
        'sbImprimeLista
        '
        Me.sbImprimeLista.Image = Global.Nexus.My.Resources.Resources.Printer
        Me.sbImprimeLista.Location = New System.Drawing.Point(786, 0)
        Me.sbImprimeLista.Name = "sbImprimeLista"
        Me.sbImprimeLista.Size = New System.Drawing.Size(58, 37)
        Me.sbImprimeLista.TabIndex = 25
        Me.sbImprimeLista.ToolTip = "Imprimir Lista de Precios por Cliente"
        '
        'sePorcDescuento
        '
        Me.sePorcDescuento.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.sePorcDescuento.EnterMoveNextControl = True
        Me.sePorcDescuento.Location = New System.Drawing.Point(461, 273)
        Me.sePorcDescuento.Name = "sePorcDescuento"
        Me.sePorcDescuento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.sePorcDescuento.Properties.Mask.EditMask = "n2"
        Me.sePorcDescuento.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.sePorcDescuento.Size = New System.Drawing.Size(69, 20)
        Me.sePorcDescuento.TabIndex = 20
        '
        'seDiasCredito
        '
        Me.seDiasCredito.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDiasCredito.EnterMoveNextControl = True
        Me.seDiasCredito.Location = New System.Drawing.Point(461, 251)
        Me.seDiasCredito.Name = "seDiasCredito"
        Me.seDiasCredito.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDiasCredito.Properties.Mask.BeepOnError = True
        Me.seDiasCredito.Properties.Mask.EditMask = "f0"
        Me.seDiasCredito.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seDiasCredito.Size = New System.Drawing.Size(69, 20)
        Me.seDiasCredito.TabIndex = 18
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(333, 276)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(125, 13)
        Me.LabelControl18.TabIndex = 17
        Me.LabelControl18.Text = "Porcentaje de Descuento:"
        '
        'seLimite
        '
        Me.seLimite.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seLimite.EnterMoveNextControl = True
        Me.seLimite.Location = New System.Drawing.Point(175, 252)
        Me.seLimite.Name = "seLimite"
        Me.seLimite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seLimite.Properties.Mask.EditMask = "n2"
        Me.seLimite.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seLimite.Size = New System.Drawing.Size(130, 20)
        Me.seLimite.TabIndex = 17
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(379, 254)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl16.TabIndex = 17
        Me.LabelControl16.Text = "Días de Crédito:"
        '
        'ceAplicaPer_Ret
        '
        Me.ceAplicaPer_Ret.Location = New System.Drawing.Point(459, 229)
        Me.ceAplicaPer_Ret.Name = "ceAplicaPer_Ret"
        Me.ceAplicaPer_Ret.Properties.Caption = "Aplica retención al Facturar"
        Me.ceAplicaPer_Ret.Size = New System.Drawing.Size(155, 19)
        Me.ceAplicaPer_Ret.TabIndex = 15
        Me.ceAplicaPer_Ret.Visible = False
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(89, 255)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl15.TabIndex = 17
        Me.LabelControl15.Text = "Límite de Crédito:"
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(55, 388)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(118, 13)
        Me.LabelControl23.TabIndex = 16
        Me.LabelControl23.Text = "Tipo de Precio Asignado:"
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(18, 366)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(155, 13)
        Me.LabelControl22.TabIndex = 16
        Me.LabelControl22.Text = "Forma de Pago Predeterminada:"
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(2, 344)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(171, 13)
        Me.LabelControl20.TabIndex = 16
        Me.LabelControl20.Text = "Tipo de Documento que se le emite:"
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(35, 279)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(138, 13)
        Me.LabelControl19.TabIndex = 16
        Me.LabelControl19.Text = "Tipo de Impuesto que aplica:"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(51, 232)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(122, 13)
        Me.LabelControl14.TabIndex = 16
        Me.LabelControl14.Text = "Vendedor que lo Atiende:"
        '
        'teNombreCuenta
        '
        Me.teNombreCuenta.Location = New System.Drawing.Point(461, 207)
        Me.teNombreCuenta.Name = "teNombreCuenta"
        Me.teNombreCuenta.Properties.AllowFocused = False
        Me.teNombreCuenta.Properties.ReadOnly = True
        Me.teNombreCuenta.Size = New System.Drawing.Size(384, 20)
        Me.teNombreCuenta.TabIndex = 13
        '
        'beIdCuenta
        '
        Me.beIdCuenta.EnterMoveNextControl = True
        Me.beIdCuenta.Location = New System.Drawing.Point(175, 207)
        Me.beIdCuenta.Name = "beIdCuenta"
        Me.beIdCuenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beIdCuenta.Size = New System.Drawing.Size(209, 20)
        Me.beIdCuenta.TabIndex = 12
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(88, 211)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl13.TabIndex = 13
        Me.LabelControl13.Text = "Cuenta Contable:"
        '
        'leRuta
        '
        Me.leRuta.EnterMoveNextControl = True
        Me.leRuta.Location = New System.Drawing.Point(175, 296)
        Me.leRuta.Name = "leRuta"
        Me.leRuta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leRuta.Size = New System.Drawing.Size(271, 20)
        Me.leRuta.TabIndex = 21
        '
        'leMunicipio
        '
        Me.leMunicipio.EnterMoveNextControl = True
        Me.leMunicipio.Location = New System.Drawing.Point(461, 185)
        Me.leMunicipio.Name = "leMunicipio"
        Me.leMunicipio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leMunicipio.Size = New System.Drawing.Size(190, 20)
        Me.leMunicipio.TabIndex = 11
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(109, 300)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(63, 13)
        Me.LabelControl24.TabIndex = 11
        Me.LabelControl24.Text = "Ruta o Zona:"
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(393, 211)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl21.TabIndex = 11
        Me.LabelControl21.Text = "Nombre Cta.:"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(411, 189)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl12.TabIndex = 11
        Me.LabelControl12.Text = "Municipio:"
        '
        'leTipoPrecio
        '
        Me.leTipoPrecio.EnterMoveNextControl = True
        Me.leTipoPrecio.Location = New System.Drawing.Point(175, 385)
        Me.leTipoPrecio.Name = "leTipoPrecio"
        Me.leTipoPrecio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPrecio.Size = New System.Drawing.Size(271, 20)
        Me.leTipoPrecio.TabIndex = 25
        '
        'leFormaPago
        '
        Me.leFormaPago.EnterMoveNextControl = True
        Me.leFormaPago.Location = New System.Drawing.Point(175, 363)
        Me.leFormaPago.Name = "leFormaPago"
        Me.leFormaPago.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leFormaPago.Size = New System.Drawing.Size(271, 20)
        Me.leFormaPago.TabIndex = 24
        '
        'leTipoDocto
        '
        Me.leTipoDocto.EnterMoveNextControl = True
        Me.leTipoDocto.Location = New System.Drawing.Point(175, 341)
        Me.leTipoDocto.Name = "leTipoDocto"
        Me.leTipoDocto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoDocto.Size = New System.Drawing.Size(271, 20)
        Me.leTipoDocto.TabIndex = 23
        '
        'leTipoImpuesto
        '
        Me.leTipoImpuesto.EnterMoveNextControl = True
        Me.leTipoImpuesto.Location = New System.Drawing.Point(175, 275)
        Me.leTipoImpuesto.Name = "leTipoImpuesto"
        Me.leTipoImpuesto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoImpuesto.Size = New System.Drawing.Size(130, 20)
        Me.leTipoImpuesto.TabIndex = 19
        '
        'leVendedor
        '
        Me.leVendedor.EnterMoveNextControl = True
        Me.leVendedor.Location = New System.Drawing.Point(175, 229)
        Me.leVendedor.Name = "leVendedor"
        Me.leVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leVendedor.Size = New System.Drawing.Size(209, 20)
        Me.leVendedor.TabIndex = 14
        '
        'leDepartamento
        '
        Me.leDepartamento.EnterMoveNextControl = True
        Me.leDepartamento.Location = New System.Drawing.Point(175, 185)
        Me.leDepartamento.Name = "leDepartamento"
        Me.leDepartamento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leDepartamento.Size = New System.Drawing.Size(209, 20)
        Me.leDepartamento.TabIndex = 10
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(100, 189)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl11.TabIndex = 11
        Me.LabelControl11.Text = "Departamento:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(126, 125)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl7.TabIndex = 10
        Me.LabelControl7.Text = "Dirección:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(150, 104)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(23, 13)
        Me.LabelControl6.TabIndex = 10
        Me.LabelControl6.Text = "Giro:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(437, 82)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl5.TabIndex = 9
        Me.LabelControl5.Text = "NIT:"
        '
        'teNIT
        '
        Me.teNIT.EnterMoveNextControl = True
        Me.teNIT.Location = New System.Drawing.Point(461, 80)
        Me.teNIT.Name = "teNIT"
        Me.teNIT.Size = New System.Drawing.Size(190, 20)
        Me.teNIT.TabIndex = 4
        '
        'teFax
        '
        Me.teFax.EnterMoveNextControl = True
        Me.teFax.Location = New System.Drawing.Point(461, 143)
        Me.teFax.Name = "teFax"
        Me.teFax.Size = New System.Drawing.Size(190, 20)
        Me.teFax.TabIndex = 8
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(436, 146)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl9.TabIndex = 6
        Me.LabelControl9.Text = "Fax:"
        '
        'teCorreoElectronico
        '
        Me.teCorreoElectronico.EnterMoveNextControl = True
        Me.teCorreoElectronico.Location = New System.Drawing.Point(175, 164)
        Me.teCorreoElectronico.Name = "teCorreoElectronico"
        Me.teCorreoElectronico.Size = New System.Drawing.Size(476, 20)
        Me.teCorreoElectronico.TabIndex = 9
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(26, 167)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(147, 13)
        Me.LabelControl10.TabIndex = 6
        Me.LabelControl10.Text = "Correo Electrónico / Sitio Web:"
        '
        'teTelefonos
        '
        Me.teTelefonos.EnterMoveNextControl = True
        Me.teTelefonos.Location = New System.Drawing.Point(175, 143)
        Me.teTelefonos.Name = "teTelefonos"
        Me.teTelefonos.Size = New System.Drawing.Size(209, 20)
        Me.teTelefonos.TabIndex = 7
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(122, 146)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl8.TabIndex = 6
        Me.LabelControl8.Text = "Teléfonos:"
        '
        'teNRC
        '
        Me.teNRC.EnterMoveNextControl = True
        Me.teNRC.Location = New System.Drawing.Point(175, 80)
        Me.teNRC.Name = "teNRC"
        Me.teNRC.Size = New System.Drawing.Size(100, 20)
        Me.teNRC.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(148, 84)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl4.TabIndex = 6
        Me.LabelControl4.Text = "NRC:"
        '
        'teDireccion
        '
        Me.teDireccion.EnterMoveNextControl = True
        Me.teDireccion.Location = New System.Drawing.Point(175, 122)
        Me.teDireccion.Name = "teDireccion"
        Me.teDireccion.Size = New System.Drawing.Size(670, 20)
        Me.teDireccion.TabIndex = 6
        '
        'teGiro
        '
        Me.teGiro.EnterMoveNextControl = True
        Me.teGiro.Location = New System.Drawing.Point(175, 101)
        Me.teGiro.Name = "teGiro"
        Me.teGiro.Size = New System.Drawing.Size(670, 20)
        Me.teGiro.TabIndex = 5
        '
        'teRazonSocial
        '
        Me.teRazonSocial.EnterMoveNextControl = True
        Me.teRazonSocial.Location = New System.Drawing.Point(175, 59)
        Me.teRazonSocial.Name = "teRazonSocial"
        Me.teRazonSocial.Size = New System.Drawing.Size(670, 20)
        Me.teRazonSocial.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(109, 62)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "Razón Social:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(175, 38)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(670, 20)
        Me.teNombre.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(132, 43)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Nombre:"
        '
        'teCodigo
        '
        Me.teCodigo.EnterMoveNextControl = True
        Me.teCodigo.Location = New System.Drawing.Point(175, 17)
        Me.teCodigo.Name = "teCodigo"
        Me.teCodigo.Size = New System.Drawing.Size(100, 20)
        Me.teCodigo.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(95, 22)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Cód. de Cliente:"
        '
        'xtpDatos2
        '
        Me.xtpDatos2.Controls.Add(Me.gcPre)
        Me.xtpDatos2.Controls.Add(Me.pcBotones)
        Me.xtpDatos2.Name = "xtpDatos2"
        Me.xtpDatos2.Size = New System.Drawing.Size(850, 445)
        Me.xtpDatos2.Text = "Lista de Precios por Cliente"
        '
        'gcPre
        '
        Me.gcPre.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcPre.Location = New System.Drawing.Point(0, 0)
        Me.gcPre.MainView = Me.gvPre
        Me.gcPre.Name = "gcPre"
        Me.gcPre.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.teCod})
        Me.gcPre.Size = New System.Drawing.Size(809, 445)
        Me.gcPre.TabIndex = 4
        Me.gcPre.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvPre})
        '
        'gvPre
        '
        Me.gvPre.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn7, Me.GridColumn8})
        Me.gvPre.GridControl = Me.gcPre
        Me.gvPre.Name = "gvPre"
        Me.gvPre.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gvPre.OptionsView.ShowGroupPanel = False
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Id. Producto"
        Me.GridColumn6.ColumnEdit = Me.teCod
        Me.GridColumn6.FieldName = "IdProducto"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 0
        Me.GridColumn6.Width = 112
        '
        'teCod
        '
        Me.teCod.AutoHeight = False
        Me.teCod.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teCod.Name = "teCod"
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Descripción"
        Me.GridColumn7.FieldName = "Descripcion"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 1
        Me.GridColumn7.Width = 546
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Precio Venta"
        Me.GridColumn8.DisplayFormat.FormatString = "n6"
        Me.GridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn8.FieldName = "PrecioUnitario"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 2
        Me.GridColumn8.Width = 127
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdUp)
        Me.pcBotones.Controls.Add(Me.cmdDown)
        Me.pcBotones.Controls.Add(Me.cmdBorrar)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(809, 0)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(41, 445)
        Me.pcBotones.TabIndex = 3
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(7, 8)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(27, 24)
        Me.cmdUp.TabIndex = 0
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(7, 43)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(27, 24)
        Me.cmdDown.TabIndex = 1
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(7, 78)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 24)
        Me.cmdBorrar.TabIndex = 2
        '
        'fac_frmClientes
        '
        Me.ClientSize = New System.Drawing.Size(856, 498)
        Me.Controls.Add(Me.xtcClientes)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmClientes"
        Me.OptionId = "001003"
        Me.Text = "Clientes"
        Me.Controls.SetChildIndex(Me.xtcClientes, 0)
        CType(Me.xtcClientes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcClientes.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        Me.xtpDatos.PerformLayout()
        CType(Me.teOtroDocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceBloquear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sePorcDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seLimite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceAplicaPer_Ret.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombreCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beIdCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leRuta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leMunicipio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPrecio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoDocto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoImpuesto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leDepartamento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teFax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorreoElectronico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teGiro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teRazonSocial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos2.ResumeLayout(False)
        CType(Me.gcPre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCod, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents xtcClientes As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNIT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teGiro As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teRazonSocial As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teFax As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTelefonos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCorreoElectronico As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beIdCuenta As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leMunicipio As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leDepartamento As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceAplicaPer_Ret As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombreCuenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leVendedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents seDiasCredito As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seLimite As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents sePorcDescuento As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcIdCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNrc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTelefonos As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoDocto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leTipoImpuesto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leFormaPago As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoPrecio As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leRuta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents xtpDatos2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcPre As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvPre As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teCod As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents sbImprimeLista As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ceBloquear As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents gcDireccion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rgTipo As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbInfoAdicional As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbDeclaracionJuradaPersonaNatural As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teOtroDocumento As DevExpress.XtraEditors.TextEdit

End Class
