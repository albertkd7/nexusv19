﻿Imports NexusBLL
Public Class fac_frmEmiteDocumentoDevolucion
    Dim bl As New FacturaBLL(g_ConnectionString)
    Private Sub facEmiteDocumentoDevolucion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue)
        objCombos.adm_TiposComprobante(leTipoComprobante)
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue)
    End Sub

    Private Sub sbOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbOk.Click
        Dim iIdVenta As Integer
        iIdVenta = bl.getIdComprobante(leSucursal.EditValue, lePuntoVenta.EditValue, leTipoComprobante.EditValue, teSerie.Text, teNumero.Text)
        If iIdVenta = 0 Then
            MsgBox("No se encuentra registrado ningún comprobante con éstos datos", MsgBoxStyle.Information, "Nota")

        End If

    End Sub
End Class
