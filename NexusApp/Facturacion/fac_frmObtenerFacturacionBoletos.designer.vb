<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmObtenerFacturacionBoletos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmObtenerFacturacionBoletos))
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.gcFP = New DevExpress.XtraGrid.GridControl()
        Me.gvFP = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcCorrel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNumero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkSeleccionar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.gcVentaNoSujeta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcVentaExenta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcVentaAfecta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIVA = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdPunto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteFacturar = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.sbImportar = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.gcFP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvFP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteFacturar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(776, 380)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(95, 23)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Aceptar"
        '
        'sbCancel
        '
        Me.sbCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.sbCancel.Appearance.Options.UseFont = True
        Me.sbCancel.Location = New System.Drawing.Point(877, 380)
        Me.sbCancel.Name = "sbCancel"
        Me.sbCancel.Size = New System.Drawing.Size(95, 23)
        Me.sbCancel.TabIndex = 2
        Me.sbCancel.Text = "Cancelar"
        '
        'gcFP
        '
        Me.gcFP.Location = New System.Drawing.Point(12, 2)
        Me.gcFP.MainView = Me.gvFP
        Me.gcFP.Name = "gcFP"
        Me.gcFP.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.chkSeleccionar, Me.riteFacturar})
        Me.gcFP.Size = New System.Drawing.Size(1009, 372)
        Me.gcFP.TabIndex = 6
        Me.gcFP.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvFP})
        '
        'gvFP
        '
        Me.gvFP.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcCorrel, Me.GridColumn3, Me.gcFecha, Me.gcNumero, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn4, Me.gcVentaNoSujeta, Me.gcVentaExenta, Me.gcVentaAfecta, Me.gcIVA, Me.gcNit, Me.gcIdPunto})
        Me.gvFP.GridControl = Me.gcFP
        Me.gvFP.Name = "gvFP"
        Me.gvFP.OptionsView.ShowFooter = True
        Me.gvFP.OptionsView.ShowGroupPanel = False
        '
        'gcCorrel
        '
        Me.gcCorrel.Caption = "No."
        Me.gcCorrel.FieldName = "IdComprobante"
        Me.gcCorrel.Name = "gcCorrel"
        Me.gcCorrel.OptionsColumn.AllowEdit = False
        Me.gcCorrel.OptionsColumn.AllowFocus = False
        Me.gcCorrel.OptionsColumn.AllowMove = False
        Me.gcCorrel.Width = 47
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "No. Pedido"
        Me.GridColumn3.FieldName = "Numero"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Width = 101
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha"
        Me.gcFecha.FieldName = "Fecha"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.OptionsColumn.AllowEdit = False
        Me.gcFecha.OptionsColumn.AllowFocus = False
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 0
        Me.gcFecha.Width = 90
        '
        'gcNumero
        '
        Me.gcNumero.Caption = "N�mero"
        Me.gcNumero.FieldName = "Numero"
        Me.gcNumero.Name = "gcNumero"
        Me.gcNumero.OptionsColumn.AllowEdit = False
        Me.gcNumero.OptionsColumn.AllowFocus = False
        Me.gcNumero.Visible = True
        Me.gcNumero.VisibleIndex = 2
        Me.gcNumero.Width = 131
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "IdProducto"
        Me.GridColumn5.FieldName = "IdProducto"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.OptionsColumn.AllowEdit = False
        Me.GridColumn5.Width = 142
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Producto"
        Me.GridColumn6.FieldName = "Producto"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.OptionsColumn.AllowEdit = False
        Me.GridColumn6.Width = 202
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Cant. Pendiente Entrega"
        Me.GridColumn7.FieldName = "Cantidad"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.OptionsColumn.AllowEdit = False
        Me.GridColumn7.Width = 124
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Seleccionar?"
        Me.GridColumn4.ColumnEdit = Me.chkSeleccionar
        Me.GridColumn4.FieldName = "Seleccionar"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Width = 71
        '
        'chkSeleccionar
        '
        Me.chkSeleccionar.AutoHeight = False
        Me.chkSeleccionar.Name = "chkSeleccionar"
        '
        'gcVentaNoSujeta
        '
        Me.gcVentaNoSujeta.Caption = "VentaNoSujeta"
        Me.gcVentaNoSujeta.FieldName = "TotalNoSujeto"
        Me.gcVentaNoSujeta.Name = "gcVentaNoSujeta"
        Me.gcVentaNoSujeta.Visible = True
        Me.gcVentaNoSujeta.VisibleIndex = 3
        Me.gcVentaNoSujeta.Width = 133
        '
        'gcVentaExenta
        '
        Me.gcVentaExenta.Caption = "Venta Exenta"
        Me.gcVentaExenta.FieldName = "TotalExento"
        Me.gcVentaExenta.Name = "gcVentaExenta"
        Me.gcVentaExenta.Visible = True
        Me.gcVentaExenta.VisibleIndex = 4
        Me.gcVentaExenta.Width = 133
        '
        'gcVentaAfecta
        '
        Me.gcVentaAfecta.Caption = "Venta Afecta"
        Me.gcVentaAfecta.FieldName = "TotalAfecto"
        Me.gcVentaAfecta.Name = "gcVentaAfecta"
        Me.gcVentaAfecta.Visible = True
        Me.gcVentaAfecta.VisibleIndex = 5
        Me.gcVentaAfecta.Width = 133
        '
        'gcIVA
        '
        Me.gcIVA.Caption = "IVA"
        Me.gcIVA.FieldName = "TotalIva"
        Me.gcIVA.Name = "gcIVA"
        Me.gcIVA.Visible = True
        Me.gcIVA.VisibleIndex = 6
        Me.gcIVA.Width = 133
        '
        'gcNit
        '
        Me.gcNit.Caption = "Nit"
        Me.gcNit.FieldName = "Nit"
        Me.gcNit.Name = "gcNit"
        Me.gcNit.Visible = True
        Me.gcNit.VisibleIndex = 7
        Me.gcNit.Width = 140
        '
        'gcIdPunto
        '
        Me.gcIdPunto.Caption = "Id Punto"
        Me.gcIdPunto.FieldName = "IdPunto"
        Me.gcIdPunto.Name = "gcIdPunto"
        Me.gcIdPunto.Visible = True
        Me.gcIdPunto.VisibleIndex = 1
        Me.gcIdPunto.Width = 98
        '
        'riteFacturar
        '
        Me.riteFacturar.AutoHeight = False
        Me.riteFacturar.EditFormat.FormatString = "n2"
        Me.riteFacturar.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteFacturar.Mask.EditMask = "n2"
        Me.riteFacturar.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteFacturar.Mask.UseMaskAsDisplayFormat = True
        Me.riteFacturar.Name = "riteFacturar"
        '
        'sbImportar
        '
        Me.sbImportar.AllowFocus = False
        Me.sbImportar.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.sbImportar.Appearance.Options.UseFont = True
        Me.sbImportar.Location = New System.Drawing.Point(667, 380)
        Me.sbImportar.Name = "sbImportar"
        Me.sbImportar.Size = New System.Drawing.Size(103, 23)
        Me.sbImportar.TabIndex = 7
        Me.sbImportar.Text = "Importar CSV"
        '
        'fac_frmObtenerFacturacionBoletos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1011, 415)
        Me.ControlBox = False
        Me.Controls.Add(Me.sbImportar)
        Me.Controls.Add(Me.gcFP)
        Me.Controls.Add(Me.sbCancel)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fac_frmObtenerFacturacionBoletos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Obtener Facturaci�n de Boletos"
        CType(Me.gcFP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvFP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSeleccionar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteFacturar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcFP As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvFP As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcCorrel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkSeleccionar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents riteFacturar As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcVentaNoSujeta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcVentaExenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcVentaAfecta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIVA As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdPunto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents sbImportar As DevExpress.XtraEditors.SimpleButton
End Class
