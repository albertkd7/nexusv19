<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmMostrarCambio
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmMostrarCambio))
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.teTotalFactura = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.teEfectivo = New DevExpress.XtraEditors.TextEdit
        Me.teCambio = New DevExpress.XtraEditors.TextEdit
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
        Me.sbCancel = New DevExpress.XtraEditors.SimpleButton
        CType(Me.teTotalFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teEfectivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCambio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(46, 18)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Total Documento:"
        '
        'teTotalFactura
        '
        Me.teTotalFactura.EditValue = 0
        Me.teTotalFactura.Enabled = False
        Me.teTotalFactura.EnterMoveNextControl = True
        Me.teTotalFactura.Location = New System.Drawing.Point(134, 14)
        Me.teTotalFactura.Name = "teTotalFactura"
        Me.teTotalFactura.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotalFactura.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotalFactura.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.teTotalFactura.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.teTotalFactura.Properties.Mask.EditMask = "n2"
        Me.teTotalFactura.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotalFactura.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTotalFactura.Properties.ReadOnly = True
        Me.teTotalFactura.Size = New System.Drawing.Size(109, 20)
        Me.teTotalFactura.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(45, 52)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Efectivo Recibido:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LabelControl3.Location = New System.Drawing.Point(71, 95)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(60, 19)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Cambio:"
        '
        'teEfectivo
        '
        Me.teEfectivo.EditValue = 0
        Me.teEfectivo.EnterMoveNextControl = True
        Me.teEfectivo.Location = New System.Drawing.Point(134, 49)
        Me.teEfectivo.Name = "teEfectivo"
        Me.teEfectivo.Properties.Appearance.Options.UseTextOptions = True
        Me.teEfectivo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teEfectivo.Properties.Mask.EditMask = "n2"
        Me.teEfectivo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teEfectivo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teEfectivo.Size = New System.Drawing.Size(109, 20)
        Me.teEfectivo.TabIndex = 0
        '
        'teCambio
        '
        Me.teCambio.EditValue = 0
        Me.teCambio.Enabled = False
        Me.teCambio.EnterMoveNextControl = True
        Me.teCambio.Location = New System.Drawing.Point(134, 92)
        Me.teCambio.Name = "teCambio"
        Me.teCambio.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.teCambio.Properties.Appearance.Options.UseFont = True
        Me.teCambio.Properties.Appearance.Options.UseTextOptions = True
        Me.teCambio.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teCambio.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.teCambio.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.teCambio.Properties.Mask.EditMask = "n2"
        Me.teCambio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teCambio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teCambio.Properties.ReadOnly = True
        Me.teCambio.Size = New System.Drawing.Size(109, 26)
        Me.teCambio.TabIndex = 2
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(68, 133)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(95, 29)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "&Imprimir"
        '
        'sbCancel
        '
        Me.sbCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sbCancel.Appearance.Options.UseFont = True
        Me.sbCancel.Location = New System.Drawing.Point(168, 133)
        Me.sbCancel.Name = "sbCancel"
        Me.sbCancel.Size = New System.Drawing.Size(94, 29)
        Me.sbCancel.TabIndex = 2
        Me.sbCancel.Text = "Continuar"
        '
        'fac_frmMostrarCambio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(289, 168)
        Me.Controls.Add(Me.sbCancel)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.teCambio)
        Me.Controls.Add(Me.teEfectivo)
        Me.Controls.Add(Me.teTotalFactura)
        Me.Controls.Add(Me.LabelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fac_frmMostrarCambio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cerrar Venta"
        CType(Me.teTotalFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teEfectivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCambio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTotalFactura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teEfectivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teCambio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbCancel As DevExpress.XtraEditors.SimpleButton
End Class
