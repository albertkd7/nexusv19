﻿Imports NexusELL.TableEntities
Imports NexusBLL

Public Class fac_frmAperturaCaja
    Dim entApertura As fac_AperturaCaja
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blAmon As New AdmonBLL(g_ConnectionString)
    Dim fd As New FuncionesBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAmon.ObtieneParametros()
    Dim IdDoc As Integer



    Private Sub fac_frmAperturaCaja_Editar() Handles Me.Editar
        ActivarControles(True)
    End Sub

    Private Sub fac_frmAperturaCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "")
        IdDoc = objFunciones.ObtenerUltimoId("FAC_APERTURACAJA", "IdComprobante")
        ActivarControles(False)
        gc2.DataSource = bl.fac_ConsultaAperturasCaja(objMenu.User)
    End Sub

    Private Sub fac_frmAperturaCaja_Nuevo_Click() Handles Me.Nuevo

        entApertura = New fac_AperturaCaja
        leSucursal.EditValue = piIdSucursalUsuario
        leSucursal_EditValueChanged("", New EventArgs)
        deFecha.EditValue = SiEsNulo(objFunciones.GetFechaContable(leSucursal.EditValue), Nothing)
        teValor.EditValue = 0.0
        meConcepto.EditValue = ""
        teNombre.EditValue = ""
        ActivarControles(True)
        xtcVales.SelectedTabPage = xtpDatos
    End Sub
    Private Sub fac_frmAperturaCaja_Edit_Click() Handles Me.Editar
        ActivarControles(True)
        deFecha.Focus()
    End Sub
    Private Sub fac_frmAperturaCaja_Guardar() Handles Me.Guardar
        If SiEsNulo(deFecha.EditValue, Nothing) = Nothing Then
            MsgBox("No existe fecha activa, para realizar la Apertura", MsgBoxStyle.Critical, "Error de Usuario")
            Return
        End If

        Dim IdExiste As Integer = SiEsNulo(bl.fac_ExisteAperturaCaja(deFecha.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue), 0)
        If IdExiste <> 0 Then
            MsgBox("Ya existe una apertura con esta fecha", MsgBoxStyle.Critical, "Error de Usuario")
            Return
        End If


        CargaEntidad()
        If DbMode = DbModeType.insert Then
            entApertura.IdComprobante = objFunciones.ObtenerUltimoId("FAC_APERTURACAJA", "IdComprobante") + 1
            objTablas.fac_AperturaCajaInsert(entApertura)
            teCorrelativo.EditValue = entApertura.IdComprobante
        Else
            objTablas.fac_AperturaCajaUpdate(entApertura)
        End If

        xtcVales.SelectedTabPage = xtpLista
        gc2.DataSource = bl.fac_ConsultaAperturasCaja(objMenu.User)
        gc2.Focus()

        MostrarModoInicial()
        ActivarControles(False)
    End Sub

    Private Sub fac_frmAperturaCaja_Eliminar() Handles Me.Eliminar
        entApertura = objTablas.fac_AperturaCajaSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Return
        End If

        objTablas.fac_AperturaCajaDeleteByPK(entApertura.IdComprobante)

        xtcVales.SelectedTabPage = xtpLista
        gc2.DataSource = bl.fac_ConsultaAperturasCaja(objMenu.User)
        gc2.Focus()
    End Sub
    Private Sub fac_frmAperturaCaja_Reporte() Handles Me.Reporte

    End Sub

    Private Sub CargaEntidad()
        With entApertura
            .IdComprobante = teCorrelativo.EditValue
            .IdSucursal = leSucursal.EditValue
            .IdPunto = lePuntoVenta.EditValue
            .Fecha = deFecha.EditValue
            .Responsable = teNombre.EditValue
            .Comentario = meConcepto.EditValue
            .FondoApertura = teValor.EditValue

            If DbMode = DbModeType.insert Then
                .FechaHoraCreacion = Now
                .CreadoPor = objMenu.User
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If
        End With
    End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        deFecha.Properties.ReadOnly = True
        deFecha.Enabled = False
    End Sub

    Private Sub fac_frmVales_Revertir() Handles Me.Revertir
        xtcVales.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        deFecha.EditValue = SiEsNulo(objFunciones.GetFechaContable(leSucursal.EditValue), Nothing)
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "")
    End Sub

    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        entApertura = objTablas.fac_AperturaCajaSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        DbMode = DbModeType.update
        deFecha.Focus()
    End Sub
    Private Sub CargaPantalla()
        xtcVales.SelectedTabPage = xtpDatos
        deFecha.Focus()

        With entApertura
            teCorrelativo.EditValue = .IdComprobante
            leSucursal.EditValue = .IdSucursal
            deFecha.EditValue = SiEsNulo(.Fecha, Nothing)
            teValor.EditValue = .FondoApertura
            meConcepto.EditValue = .Comentario
            teNombre.EditValue = .Responsable
            lePuntoVenta.EditValue = .IdPunto
        End With
    End Sub
End Class