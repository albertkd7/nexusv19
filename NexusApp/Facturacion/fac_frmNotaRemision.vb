﻿
Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class fac_frmNotaRemision
    Dim bl As New FacturaBLL(g_ConnectionString)
    'Dim blInve As New InventarioBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim entProducto As inv_Productos
    Dim Header As fac_NotasRemision
    Dim Detalle As List(Of fac_NotasRemisionDetalle)
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim dtParametros As DataTable = blAdmon.ObtieneParametros()
    Private Sub fac_frmNotaRemision_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        CargaCombos()
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("fac_NotasRemision", "IdComprobante")
        CargaControles(0)
        ActivarControles(False)
        ' para el caso de los clientes que no tenia este cambio, estara nulo este campo
        Me.gcDescripcion.OptionsColumn.ReadOnly = Not SiEsNulo(EntUsuario.AccesoProductos, True)
    End Sub


    Private Sub fac_frmNotaRemision_Nuevo() Handles Me.Nuevo

        ActivarControles(True)

        Header = New fac_NotasRemision
        gc.DataSource = bl.fac_ObtenerDetalleDocumento("fac_NotasRemisionDetalle", -1)
        gv.CancelUpdateCurrentRow()
        gv.AddNewRow()

        teNumero.EditValue = ""
        deFecha.EditValue = Today
        beIdCliente.EditValue = ""
        txtNombre.EditValue = ""
        txtNRC.EditValue = ""
        txtGiro.EditValue = ""
        txtDireccion.EditValue = ""
        txtTelefono.EditValue = ""
        cboVendedor.ItemIndex = 0
        leBodega.ItemIndex = 0
        leSucursal.ItemIndex = 0
        txtNIT.EditValue = ""
        cboDepartamento.ItemIndex = 0
        cboMunicipio.ItemIndex = 0
        teNumero.Focus()
    End Sub
    Private Sub fac_frmNotaRemision_Guardar() Handles Me.Guardar
        CargarEntidades()
        Dim msj As String = ""
        If DbMode = DbModeType.insert Then
            msj = bl.InsertRemision(Header, Detalle)

            If msj = "" Then
                MsgBox("La nota de remisión ha sido registrada con éxito", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox("NO SE PUDO CREAR LA NOTA DE REMISIÓN" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        Else
            msj = bl.UpdateRemision(Header, Detalle)
            If msj = "" Then
                MsgBox("La nota de remisión ha sido actualizada con éxito", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox(String.Format("NO FUE POSIBLE ACTUALIZAR LA NOTA DE REMISIÓN{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If
        teCorrelativo.EditValue = Header.IdComprobante
        MostrarModoInicial()
        teCorrelativo.Focus()
        ActivarControles(False)
    End Sub
    Private Sub fac_frmNotaRemision_Editar() Handles Me.Editar
        ActivarControles(True)
        teNumero.Focus()
    End Sub
    Private Sub fac_frmNotaRemision_Consulta() Handles Me.Consulta
        teCorrelativo.EditValue = objConsultas.ConsultaRemision(frmConsultaDetalle)
        CargaControles(0)
    End Sub
    Private Sub fac_frmNotaRemision_Revertir() Handles Me.Revertir
        ActivarControles(False)
    End Sub
    Private Sub fac_frmNotaRemision_Delete() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar la nota remisión?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            objTablas.fac_NotasRemisionDeleteByPK(Header.IdComprobante)
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("fac_NotasRemision", "IdComprobante")
            CargaControles(0)
            ActivarControles(False)
        End If
    End Sub
    
    Private Sub CargarEntidades()
        With Header
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
            Else
                .ModificadoPor = objMenu.User
                .FechaHoraModificacion = Nothing
            End If
            .IdComprobante = teCorrelativo.EditValue  'el id se asignará en la capa de datos
            .Numero = teNumero.EditValue
            .Fecha = deFecha.EditValue
            .IdCliente = beIdCliente.EditValue
            .Nombre = txtNombre.EditValue
            .Nrc = txtNRC.EditValue
            .Giro = txtGiro.EditValue
            .Direccion = txtDireccion.EditValue
            .Telefonos = txtTelefono.EditValue
            .IdVendedor = cboVendedor.EditValue
            .IdBodega = leBodega.EditValue
            .IdSucursal = leSucursal.EditValue
            .IdPunto = lePunto.EditValue
            .Nit = txtNIT.EditValue
            .IdDepartamento = cboDepartamento.EditValue
            .IdMunicipio = cboMunicipio.EditValue
        End With

        Detalle = New List(Of fac_NotasRemisionDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New fac_NotasRemisionDetalle
            With entDetalle
                .IdDetalle = i + 1
                .IdComprobante = Header.IdComprobante
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .IdPrecio = gv.GetRowCellValue(i, "TipoPrecio")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .PjeDescuento = gv.GetRowCellValue(i, "PjeDescuento")
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .PrecioTotal = gv.GetRowCellValue(i, "PrecioTotal")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            Detalle.Add(entDetalle)
        Next

    End Sub
    Public Sub CargaControles(ByVal TipoAvance As Integer)

        If TipoAvance = 0 Then 'no es necesario obtener el Id de la orden
        Else
            teCorrelativo.EditValue = bl.fac_ObtenerIdNotaRemision(teCorrelativo.EditValue, TipoAvance)
        End If

        Header = objTablas.fac_NotasRemisionSelectByPK(teCorrelativo.EditValue)
        If Header Is Nothing Then
            Exit Sub
        End If
        With Header
            teCorrelativo.EditValue = .IdComprobante
            teNumero.EditValue = .Numero
            deFecha.EditValue = .Fecha
            beIdCliente.EditValue = .IdCliente
            txtNombre.EditValue = .Nombre
            txtNRC.EditValue = .Nrc
            txtGiro.EditValue = .Giro
            txtDireccion.EditValue = .Direccion
            txtTelefono.EditValue = .Telefonos
            cboVendedor.EditValue = .IdVendedor
            leBodega.EditValue = .IdBodega
            leSucursal.EditValue = .IdPunto
            txtNIT.EditValue = .Nit
            cboDepartamento.EditValue = .IdDepartamento
            cboMunicipio.EditValue = .IdMunicipio

            gc.DataSource = bl.fac_ObtenerDetalleDocumento("fac_NotasRemisionDetalle", .IdComprobante)
        End With

    End Sub
    Private Sub CargaCombos()
        With objCombos
            .adm_Sucursales(leSucursal, objMenu.User, "")
            .fac_PuntosVenta(lePunto, leSucursal.EditValue)
            .admDepartamentos(cboDepartamento)
            .fac_Vendedores(cboVendedor)
            .inv_Bodegas(leBodega)
        End With
    End Sub

#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "TipoPrecio", dtParametros.Rows(0).Item("IdTipoPrecio"))
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "PjeDescuento", 0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", 0)
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), "") = "" Then
            e.Valid = False
        End If
        'Dim IdProduc As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdProducto")), "")
        'Dim IdPrecio As Integer = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdPrecio")), 0)
        'If IdProduc = "" OrElse Not blInve.inv_VerificaCodigoProducto(IdProduc) Then
        '    e.Valid = False
        '    gv.SetColumnError(gv.Columns("IdProducto"), "Código de producto no válido o no existe")
        'End If
        If SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("TipoPrecio")), 0) = 0 Then
            e.Valid = False
            gv.SetColumnError(gv.Columns("TipoPrecio"), "Debe de especificar el precio")
        End If

    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Dim cantidad As Integer = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
        Dim PjeDescuento As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PjeDescuento")
        Dim PrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")

        Select Case gv.FocusedColumn.FieldName
            Case "Cantidad"
                cantidad = e.Value
            Case "PjeDescuento"
                PjeDescuento = e.Value
            Case "PrecioUnitario"
                PrecioUnitario = e.Value
        End Select

        Dim total As Decimal = Decimal.Round((cantidad * PrecioUnitario) * (100 - PjeDescuento) / 100, 2)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", total)
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gv.EditingValue = IdProd
                End If
                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                End If
            End If
        End If

    End Sub
#End Region

    Private Sub cboDepartamento_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepartamento.EditValueChanged
        objCombos.admMunicipios(cboMunicipio, cboDepartamento.EditValue)
    End Sub
    Private Sub beIdCliente_Click(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beIdCliente.ButtonClick
        beIdCliente.EditValue = ""
        beIdCliente_Validated(sender, New System.EventArgs)
    End Sub
    Private Sub beIdCliente_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beIdCliente.Validated
        Dim entCliente As New fac_Clientes
        entCliente = objConsultas.cnsClientes(fac_frmConsultaClientes, beIdCliente.EditValue)
        beIdCliente.EditValue = entCliente.IdCliente
        txtNombre.EditValue = entCliente.Nombre
        txtDireccion.EditValue = entCliente.Direccion
        txtGiro.EditValue = entCliente.Giro
        txtNRC.EditValue = entCliente.Nrc
        txtNIT.EditValue = entCliente.Nit
        txtTelefono.EditValue = entCliente.Telefonos
        cboDepartamento.EditValue = entCliente.IdDepartamento
        cboMunicipio.EditValue = entCliente.IdMunicipio
        cboVendedor.EditValue = entCliente.IdVendedor
    End Sub
    Private Sub txtTelefono_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles teContacto.LostFocus
        If gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
        End If
        gv.FocusedColumn = gv.Columns(0)
    End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In gcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
    End Sub
    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePunto, leSucursal.EditValue)
    End Sub

    Private Sub fac_frmNotaRemision_Reporte() Handles Me.Reporte
        Dim dt As DataTable = bl.fac_ObtenerNotaRemision(teCorrelativo.EditValue)
        Dim rpt As New fac_rptNotaRemision() With {.DataSource = dt, .DataMember = ""}

        Dim Template = Application.StartupPath & "\Plantillas\nr01.repx"
        If Not FileIO.FileSystem.FileExists(Template) Then
            MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        'Dim rpt As New fac_rptOrdenEnvio
        
        rpt.LoadLayout(Template)
        rpt.DataSource = dt
        rpt.DataMember = ""
        With Header
            'rpt.xrlCodigo.Text = .IdCliente
            'rpt.xrlNombre.Text = .Nombre
            'rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
            'rpt.xrlDia.Text = CDate(.Fecha).Day.ToString()
            'rpt.xrlMes.Text = ObtieneMesString(CDate(.Fecha).Month)
            'rpt.xrlAnio.Text = CDate(.Fecha).Year.ToString()
            'rpt.xrlTotal2.Text = Format(.TotalComprobante, "###,##0.00")
            'rpt.xrlCodigo.Text = .IdCliente
            'rpt.xrlDireccion.Text = .Direccion
            'rpt.xrlNumero.Text = .Numero

            'rpt.xrlNit.Text = .Nit
            'rpt.xrlGiro.Text = .Giro
            'rpt.xrlPedido.Text = .OrdenCompra
            'rpt.xrlFormaPago.Text = ""
            'rpt.xrlVendedor.Text = leVendedor.Text
            'rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
            'rpt.xrlTelefono.Text = .Telefono
            'rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
            'rpt.xrlIva.Text = Format(.TotalIva, "###,##0.00")
            'rpt.xrlSubTotal.Text = Format(.TotalAfecto, "###,##0.00")
            'rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
            'rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
            'rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
            'Dim Decimales = String.Format("{0:c}", .TotalComprobante)

            'Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
            'rpt.xrlCantLetras.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"

            'If dtParametros.Rows(0).Item("VistaPreviaFacturar") Then
            '    rpt.ShowPreviewDialog()
            'Else
            '    rpt.PrinterName = SiEsNulo(entComprobantes.NombreImpresor, "")
            '    rpt.Print()
            'End If
        End With

        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.ShowPreviewDialog()
    End Sub
End Class
