Imports NexusBLL
Public Class fac_frmConsultaClientes
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim Prod As DataTable
    Private Sub inv_frmConsultaProductos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gvClientes.Columns.Clear()
        gcClientes.DataSource = New DataView(cnsDataTable)
        IdCliente = ""
        txtCodigo.EditValue = ""
        txtNombre.EditValue = ""
        txtDir.EditValue = ""
        gvClientes.BestFitColumns()
        txtCodigo.Focus()
    End Sub

    Private _cnsDatatable As DataTable
    Public Property cnsDataTable() As DataTable
        Get
            Return _cnsDatatable
        End Get
        Set(ByVal value As DataTable)
            _cnsDatatable = value
        End Set
    End Property
    Private _IdCliente As String = ""
    Public Property IdCliente() As String
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As String)
            _IdCliente = value
        End Set
    End Property


    Private Sub gvClientes_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvClientes.KeyDown
        If e.KeyCode = Keys.Enter Then
            IdCliente = gvClientes.GetRowCellValue(gvClientes.FocusedRowHandle, "IdCliente")
            Me.Close()
        End If
    End Sub

    Private Sub gcClientes_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gcClientes.DoubleClick
        IdCliente = gvClientes.GetRowCellValue(gvClientes.FocusedRowHandle, "IdCliente")
        Me.Close()
    End Sub
    Private Sub fac_frmConsultaClientes_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub


#Region "Tipo de Busqueda"
    Private Sub txtNombre_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombre.EditValueChanged
        If rgTipoBusqueda.SelectedIndex = 0 Then
            cnsDataTable.DefaultView.RowFilter = ("Nombre like '%" + txtNombre.EditValue + "%'")
        Else
            cnsDataTable.DefaultView.RowFilter = ("Nombre like '" + txtNombre.EditValue + "%'")
        End If

        gcClientes.DataSource = cnsDataTable.DefaultView
    End Sub

    Private Sub txtCodigo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigo.EditValueChanged
        If rgTipoBusqueda.SelectedIndex = 0 Then
            cnsDataTable.DefaultView.RowFilter = ("IdCliente like '%" + txtCodigo.EditValue + "%'")
        Else
            cnsDataTable.DefaultView.RowFilter = ("IdCliente like '" + txtCodigo.EditValue + "%'")
        End If

        gcClientes.DataSource = cnsDataTable.DefaultView
    End Sub
    Private Sub txtDir_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDir.EditValueChanged
        If rgTipoBusqueda.SelectedIndex = 0 Then
            cnsDataTable.DefaultView.RowFilter = ("Direccion like '%" + txtDir.EditValue + "%'")
        Else
            cnsDataTable.DefaultView.RowFilter = ("Direccion like '" + txtDir.EditValue + "%'")
        End If

        gcClientes.DataSource = cnsDataTable.DefaultView
    End Sub
    Private Sub rgTipoBusqueda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgTipoBusqueda.SelectedIndexChanged
        If txtCodigo.EditValue <> "" Then
            txtCodigo_EditValueChanged("", New EventArgs)
        Else
            If txtNombre.EditValue <> "" Then
                txtNombre_EditValueChanged("", New EventArgs)
            End If
        End If
    End Sub
#End Region


End Class