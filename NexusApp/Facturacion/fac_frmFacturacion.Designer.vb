﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmFacturacion
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmFacturacion))
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.sbMarcarBoletos = New DevExpress.XtraEditors.SimpleButton()
        Me.sbImportar = New DevExpress.XtraEditors.SimpleButton()
        Me.TelimiteSaldo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.teTelefonos = New DevExpress.XtraEditors.TextEdit()
        Me.teNumeroUnico = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.rgTipo = New DevExpress.XtraEditors.RadioGroup()
        Me.PictureEdit2 = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.teSaldo = New DevExpress.XtraEditors.TextEdit()
        Me.sbObtenerPedido = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teDireccion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoComprobante = New DevExpress.XtraEditors.LookUpEdit()
        Me.lblNRC_DUI = New DevExpress.XtraEditors.LabelControl()
        Me.teNRC = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.teNit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teGiro = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.teNotaRemision = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.leFormaPago = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumPedido = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.leVendedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.leBodega = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.teVentaCuentaDe = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.leDepartamento = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.seDiasCredito = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.cboPuntoVenta = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.beCodCliente = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.sePjeDescuento = New DevExpress.XtraEditors.SpinEdit()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoImpuesto = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.leMunicipio = New DevExpress.XtraEditors.LookUpEdit()
        Me.chkMargen = New DevExpress.XtraEditors.CheckEdit()
        Me.pcDetalle = New DevExpress.XtraEditors.PanelControl()
        Me.sbImportarBoletos = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.IdProducto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCodigo = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.Cantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.IdPrecio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteTipoPrecio = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.Descripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCentroCosto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCentroCosto = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.PrecioVenta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ritePrecioVenta = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.PorcDescuento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ritePorcDescto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.PrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ritePrecioUnitario = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcVentaNoSujeta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcVentaExenta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcVentaAfecta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcValorDescuento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcValorIVA = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcVentaNeta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCantidad2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.btnCentroCosto = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.pcTotales = New DevExpress.XtraEditors.PanelControl()
        Me.lblUltimoCambio = New DevExpress.XtraEditors.LabelControl()
        Me.sbEditarCESC = New DevExpress.XtraEditors.SimpleButton()
        Me.lblSucursalEnvio = New DevExpress.XtraEditors.LabelControl()
        Me.leSucursalEnvio = New DevExpress.XtraEditors.LookUpEdit()
        Me.teLimiteLineas = New DevExpress.XtraEditors.TextEdit()
        Me.ceCESC = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.teLineas = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.btGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.meComentario = New DevExpress.XtraEditors.MemoEdit()
        Me.ceRetencionPercepcion = New DevExpress.XtraEditors.CheckEdit()
        Me.ceDevolucion = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.teCESC = New DevExpress.XtraEditors.TextEdit()
        Me.teDescuento = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.teIVA = New DevExpress.XtraEditors.TextEdit()
        Me.lblPercReten = New DevExpress.XtraEditors.LabelControl()
        Me.teRetencionPercepcion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.teTotal = New DevExpress.XtraEditors.TextEdit()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.TelimiteSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumeroUnico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoComprobante.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teGiro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNotaRemision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumPedido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teVentaCuentaDe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leDepartamento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboPuntoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCodCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sePjeDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoImpuesto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leMunicipio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMargen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcDetalle.SuspendLayout()
        CType(Me.sbImportarBoletos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteTipoPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCentroCosto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePrecioVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePorcDescto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePrecioUnitario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCantidad2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCentroCosto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcTotales.SuspendLayout()
        CType(Me.leSucursalEnvio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teLimiteLineas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceCESC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teLineas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meComentario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceRetencionPercepcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceDevolucion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCESC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIVA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teRetencionPercepcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.sbMarcarBoletos)
        Me.pcHeader.Controls.Add(Me.sbImportar)
        Me.pcHeader.Controls.Add(Me.TelimiteSaldo)
        Me.pcHeader.Controls.Add(Me.LabelControl31)
        Me.pcHeader.Controls.Add(Me.teTelefonos)
        Me.pcHeader.Controls.Add(Me.teNumeroUnico)
        Me.pcHeader.Controls.Add(Me.LabelControl30)
        Me.pcHeader.Controls.Add(Me.LabelControl12)
        Me.pcHeader.Controls.Add(Me.rgTipo)
        Me.pcHeader.Controls.Add(Me.PictureEdit2)
        Me.pcHeader.Controls.Add(Me.LabelControl27)
        Me.pcHeader.Controls.Add(Me.teSaldo)
        Me.pcHeader.Controls.Add(Me.sbObtenerPedido)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Controls.Add(Me.teDireccion)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.leTipoComprobante)
        Me.pcHeader.Controls.Add(Me.lblNRC_DUI)
        Me.pcHeader.Controls.Add(Me.teNRC)
        Me.pcHeader.Controls.Add(Me.LabelControl4)
        Me.pcHeader.Controls.Add(Me.teNit)
        Me.pcHeader.Controls.Add(Me.LabelControl5)
        Me.pcHeader.Controls.Add(Me.teGiro)
        Me.pcHeader.Controls.Add(Me.LabelControl6)
        Me.pcHeader.Controls.Add(Me.teNotaRemision)
        Me.pcHeader.Controls.Add(Me.LabelControl7)
        Me.pcHeader.Controls.Add(Me.leFormaPago)
        Me.pcHeader.Controls.Add(Me.LabelControl8)
        Me.pcHeader.Controls.Add(Me.teNumPedido)
        Me.pcHeader.Controls.Add(Me.LabelControl9)
        Me.pcHeader.Controls.Add(Me.leVendedor)
        Me.pcHeader.Controls.Add(Me.LabelControl10)
        Me.pcHeader.Controls.Add(Me.leBodega)
        Me.pcHeader.Controls.Add(Me.LabelControl11)
        Me.pcHeader.Controls.Add(Me.teVentaCuentaDe)
        Me.pcHeader.Controls.Add(Me.LabelControl13)
        Me.pcHeader.Controls.Add(Me.leDepartamento)
        Me.pcHeader.Controls.Add(Me.LabelControl14)
        Me.pcHeader.Controls.Add(Me.seDiasCredito)
        Me.pcHeader.Controls.Add(Me.LabelControl19)
        Me.pcHeader.Controls.Add(Me.deFecha)
        Me.pcHeader.Controls.Add(Me.LabelControl20)
        Me.pcHeader.Controls.Add(Me.teNombre)
        Me.pcHeader.Controls.Add(Me.LabelControl21)
        Me.pcHeader.Controls.Add(Me.cboPuntoVenta)
        Me.pcHeader.Controls.Add(Me.LabelControl22)
        Me.pcHeader.Controls.Add(Me.beCodCliente)
        Me.pcHeader.Controls.Add(Me.LabelControl26)
        Me.pcHeader.Controls.Add(Me.LabelControl23)
        Me.pcHeader.Controls.Add(Me.sePjeDescuento)
        Me.pcHeader.Controls.Add(Me.teNumero)
        Me.pcHeader.Controls.Add(Me.LabelControl24)
        Me.pcHeader.Controls.Add(Me.leTipoImpuesto)
        Me.pcHeader.Controls.Add(Me.LabelControl25)
        Me.pcHeader.Controls.Add(Me.leMunicipio)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(1212, 179)
        Me.pcHeader.TabIndex = 0
        '
        'sbMarcarBoletos
        '
        Me.sbMarcarBoletos.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.sbMarcarBoletos.Appearance.Options.UseFont = True
        Me.sbMarcarBoletos.Location = New System.Drawing.Point(1047, 138)
        Me.sbMarcarBoletos.Name = "sbMarcarBoletos"
        Me.sbMarcarBoletos.Size = New System.Drawing.Size(128, 37)
        Me.sbMarcarBoletos.TabIndex = 123
        Me.sbMarcarBoletos.Text = "Marcar Boletos"
        '
        'sbImportar
        '
        Me.sbImportar.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.sbImportar.Appearance.Options.UseFont = True
        Me.sbImportar.Location = New System.Drawing.Point(1047, 96)
        Me.sbImportar.Name = "sbImportar"
        Me.sbImportar.Size = New System.Drawing.Size(128, 37)
        Me.sbImportar.TabIndex = 122
        Me.sbImportar.Text = "Importar Boletos"
        '
        'TelimiteSaldo
        '
        Me.TelimiteSaldo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.TelimiteSaldo.Enabled = False
        Me.TelimiteSaldo.Location = New System.Drawing.Point(1173, 7)
        Me.TelimiteSaldo.Name = "TelimiteSaldo"
        Me.TelimiteSaldo.Properties.AllowFocused = False
        Me.TelimiteSaldo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.TelimiteSaldo.Properties.Appearance.ForeColor = System.Drawing.Color.OrangeRed
        Me.TelimiteSaldo.Properties.Appearance.Options.UseFont = True
        Me.TelimiteSaldo.Properties.Appearance.Options.UseForeColor = True
        Me.TelimiteSaldo.Properties.Appearance.Options.UseTextOptions = True
        Me.TelimiteSaldo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.TelimiteSaldo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TelimiteSaldo.Properties.ReadOnly = True
        Me.TelimiteSaldo.Size = New System.Drawing.Size(33, 26)
        Me.TelimiteSaldo.TabIndex = 121
        Me.TelimiteSaldo.Visible = False
        '
        'LabelControl31
        '
        Me.LabelControl31.Location = New System.Drawing.Point(388, 94)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl31.TabIndex = 120
        Me.LabelControl31.Text = "Teléfonos:"
        '
        'teTelefonos
        '
        Me.teTelefonos.EnterMoveNextControl = True
        Me.teTelefonos.Location = New System.Drawing.Point(442, 91)
        Me.teTelefonos.Name = "teTelefonos"
        Me.teTelefonos.Size = New System.Drawing.Size(148, 20)
        Me.teTelefonos.TabIndex = 119
        '
        'teNumeroUnico
        '
        Me.teNumeroUnico.Location = New System.Drawing.Point(911, 150)
        Me.teNumeroUnico.Name = "teNumeroUnico"
        Me.teNumeroUnico.Properties.AllowFocused = False
        Me.teNumeroUnico.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.teNumeroUnico.Properties.Appearance.ForeColor = System.Drawing.Color.DarkRed
        Me.teNumeroUnico.Properties.Appearance.Options.UseFont = True
        Me.teNumeroUnico.Properties.Appearance.Options.UseForeColor = True
        Me.teNumeroUnico.Size = New System.Drawing.Size(103, 26)
        Me.teNumeroUnico.TabIndex = 116
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Location = New System.Drawing.Point(891, 137)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(150, 13)
        Me.LabelControl30.TabIndex = 115
        Me.LabelControl30.Text = "No. DE FORMULARIO UNICO"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(397, 158)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl12.TabIndex = 97
        Me.LabelControl12.Text = "Impuesto Adicional:"
        '
        'rgTipo
        '
        Me.rgTipo.EditValue = 1
        Me.rgTipo.Enabled = False
        Me.rgTipo.Location = New System.Drawing.Point(493, 154)
        Me.rgTipo.Name = "rgTipo"
        Me.rgTipo.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Ninguno"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Retención"), New DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Percepción")})
        Me.rgTipo.Size = New System.Drawing.Size(218, 21)
        Me.rgTipo.TabIndex = 96
        '
        'PictureEdit2
        '
        Me.PictureEdit2.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit2.EditValue = CType(resources.GetObject("PictureEdit2.EditValue"), Object)
        Me.PictureEdit2.Location = New System.Drawing.Point(832, 7)
        Me.PictureEdit2.Name = "PictureEdit2"
        Me.PictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch
        Me.PictureEdit2.Properties.ZoomAccelerationFactor = 1.0R
        Me.PictureEdit2.Size = New System.Drawing.Size(290, 87)
        Me.PictureEdit2.TabIndex = 99
        '
        'LabelControl27
        '
        Me.LabelControl27.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl27.Appearance.Options.UseFont = True
        Me.LabelControl27.Location = New System.Drawing.Point(766, 132)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl27.TabIndex = 93
        Me.LabelControl27.Text = "SALDO"
        '
        'teSaldo
        '
        Me.teSaldo.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.teSaldo.Enabled = False
        Me.teSaldo.Location = New System.Drawing.Point(746, 147)
        Me.teSaldo.Name = "teSaldo"
        Me.teSaldo.Properties.AllowFocused = False
        Me.teSaldo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.teSaldo.Properties.Appearance.ForeColor = System.Drawing.Color.OrangeRed
        Me.teSaldo.Properties.Appearance.Options.UseFont = True
        Me.teSaldo.Properties.Appearance.Options.UseForeColor = True
        Me.teSaldo.Properties.Appearance.Options.UseTextOptions = True
        Me.teSaldo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teSaldo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teSaldo.Properties.ReadOnly = True
        Me.teSaldo.Size = New System.Drawing.Size(85, 26)
        Me.teSaldo.TabIndex = 92
        '
        'sbObtenerPedido
        '
        Me.sbObtenerPedido.Location = New System.Drawing.Point(832, 112)
        Me.sbObtenerPedido.Name = "sbObtenerPedido"
        Me.sbObtenerPedido.Size = New System.Drawing.Size(60, 21)
        Me.sbObtenerPedido.TabIndex = 91
        Me.sbObtenerPedido.Text = "Obtener..."
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(49, 52)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl1.TabIndex = 46
        Me.LabelControl1.Text = "Dirección:"
        '
        'teDireccion
        '
        Me.teDireccion.EnterMoveNextControl = True
        Me.teDireccion.Location = New System.Drawing.Point(101, 49)
        Me.teDireccion.Name = "teDireccion"
        Me.teDireccion.Size = New System.Drawing.Size(489, 20)
        Me.teDireccion.TabIndex = 5
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(276, 10)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(107, 13)
        Me.LabelControl2.TabIndex = 50
        Me.LabelControl2.Text = "Tipo de Comprobante:"
        '
        'leTipoComprobante
        '
        Me.leTipoComprobante.EnterMoveNextControl = True
        Me.leTipoComprobante.Location = New System.Drawing.Point(385, 7)
        Me.leTipoComprobante.Name = "leTipoComprobante"
        Me.leTipoComprobante.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoComprobante.Properties.NullText = ""
        Me.leTipoComprobante.Size = New System.Drawing.Size(205, 20)
        Me.leTipoComprobante.TabIndex = 1
        '
        'lblNRC_DUI
        '
        Me.lblNRC_DUI.Location = New System.Drawing.Point(681, 53)
        Me.lblNRC_DUI.Name = "lblNRC_DUI"
        Me.lblNRC_DUI.Size = New System.Drawing.Size(25, 13)
        Me.lblNRC_DUI.TabIndex = 52
        Me.lblNRC_DUI.Text = "NRC:"
        '
        'teNRC
        '
        Me.teNRC.EnterMoveNextControl = True
        Me.teNRC.Location = New System.Drawing.Point(708, 49)
        Me.teNRC.Name = "teNRC"
        Me.teNRC.Properties.Mask.EditMask = "000000000-0"
        Me.teNRC.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teNRC.Size = New System.Drawing.Size(123, 20)
        Me.teNRC.TabIndex = 6
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(685, 74)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl4.TabIndex = 53
        Me.LabelControl4.Text = "NIT:"
        '
        'teNit
        '
        Me.teNit.EnterMoveNextControl = True
        Me.teNit.Location = New System.Drawing.Point(708, 70)
        Me.teNit.Name = "teNit"
        Me.teNit.Size = New System.Drawing.Size(123, 20)
        Me.teNit.TabIndex = 9
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(73, 94)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(23, 13)
        Me.LabelControl5.TabIndex = 55
        Me.LabelControl5.Text = "Giro:"
        '
        'teGiro
        '
        Me.teGiro.EnterMoveNextControl = True
        Me.teGiro.Location = New System.Drawing.Point(101, 91)
        Me.teGiro.Name = "teGiro"
        Me.teGiro.Size = New System.Drawing.Size(282, 20)
        Me.teGiro.TabIndex = 10
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(602, 95)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(104, 13)
        Me.LabelControl6.TabIndex = 58
        Me.LabelControl6.Text = "No. Nota de remisión:"
        '
        'teNotaRemision
        '
        Me.teNotaRemision.EnterMoveNextControl = True
        Me.teNotaRemision.Location = New System.Drawing.Point(708, 91)
        Me.teNotaRemision.Name = "teNotaRemision"
        Me.teNotaRemision.Size = New System.Drawing.Size(123, 20)
        Me.teNotaRemision.TabIndex = 11
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(20, 114)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl7.TabIndex = 59
        Me.LabelControl7.Text = "Forma de Pago:"
        '
        'leFormaPago
        '
        Me.leFormaPago.EnterMoveNextControl = True
        Me.leFormaPago.Location = New System.Drawing.Point(101, 112)
        Me.leFormaPago.Name = "leFormaPago"
        Me.leFormaPago.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leFormaPago.Properties.NullText = ""
        Me.leFormaPago.Size = New System.Drawing.Size(139, 20)
        Me.leFormaPago.TabIndex = 12
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(615, 116)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl8.TabIndex = 61
        Me.LabelControl8.Text = "Número de Pedido:"
        '
        'teNumPedido
        '
        Me.teNumPedido.EnterMoveNextControl = True
        Me.teNumPedido.Location = New System.Drawing.Point(708, 112)
        Me.teNumPedido.Name = "teNumPedido"
        Me.teNumPedido.Size = New System.Drawing.Size(123, 20)
        Me.teNumPedido.TabIndex = 14
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(46, 136)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl9.TabIndex = 64
        Me.LabelControl9.Text = "Vendedor:"
        '
        'leVendedor
        '
        Me.leVendedor.EnterMoveNextControl = True
        Me.leVendedor.Location = New System.Drawing.Point(101, 133)
        Me.leVendedor.Name = "leVendedor"
        Me.leVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leVendedor.Properties.NullText = ""
        Me.leVendedor.Size = New System.Drawing.Size(282, 20)
        Me.leVendedor.TabIndex = 15
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(449, 136)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl10.TabIndex = 65
        Me.LabelControl10.Text = "Bodega:"
        '
        'leBodega
        '
        Me.leBodega.EnterMoveNextControl = True
        Me.leBodega.Location = New System.Drawing.Point(493, 133)
        Me.leBodega.Name = "leBodega"
        Me.leBodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leBodega.Properties.NullText = ""
        Me.leBodega.Size = New System.Drawing.Size(218, 20)
        Me.leBodega.TabIndex = 16
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(4, 158)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(92, 13)
        Me.LabelControl11.TabIndex = 67
        Me.LabelControl11.Text = "Venta a cuenta de:"
        '
        'teVentaCuentaDe
        '
        Me.teVentaCuentaDe.EnterMoveNextControl = True
        Me.teVentaCuentaDe.Location = New System.Drawing.Point(101, 154)
        Me.teVentaCuentaDe.Name = "teVentaCuentaDe"
        Me.teVentaCuentaDe.Size = New System.Drawing.Size(140, 20)
        Me.teVentaCuentaDe.TabIndex = 17
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(23, 72)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl13.TabIndex = 71
        Me.LabelControl13.Text = "Departamento:"
        '
        'leDepartamento
        '
        Me.leDepartamento.EnterMoveNextControl = True
        Me.leDepartamento.Location = New System.Drawing.Point(101, 70)
        Me.leDepartamento.Name = "leDepartamento"
        Me.leDepartamento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leDepartamento.Properties.NullText = ""
        Me.leDepartamento.Size = New System.Drawing.Size(218, 20)
        Me.leDepartamento.TabIndex = 7
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(246, 116)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl14.TabIndex = 74
        Me.LabelControl14.Text = "Días de Crédito:"
        '
        'seDiasCredito
        '
        Me.seDiasCredito.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDiasCredito.EnterMoveNextControl = True
        Me.seDiasCredito.Location = New System.Drawing.Point(327, 112)
        Me.seDiasCredito.Name = "seDiasCredito"
        Me.seDiasCredito.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDiasCredito.Properties.Mask.EditMask = "n0"
        Me.seDiasCredito.Size = New System.Drawing.Size(56, 20)
        Me.seDiasCredito.TabIndex = 13
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(605, 10)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(101, 13)
        Me.LabelControl19.TabIndex = 77
        Me.LabelControl19.Text = "Fecha Comprobante:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(708, 7)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.deFecha.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.deFecha.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.deFecha.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(123, 20)
        Me.deFecha.TabIndex = 2
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(342, 32)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl20.TabIndex = 80
        Me.LabelControl20.Text = "Nombre:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(385, 28)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(446, 20)
        Me.teNombre.TabIndex = 4
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(18, 10)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl21.TabIndex = 81
        Me.LabelControl21.Text = "Punto de Venta:"
        '
        'cboPuntoVenta
        '
        Me.cboPuntoVenta.Enabled = False
        Me.cboPuntoVenta.EnterMoveNextControl = True
        Me.cboPuntoVenta.Location = New System.Drawing.Point(101, 7)
        Me.cboPuntoVenta.Name = "cboPuntoVenta"
        Me.cboPuntoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboPuntoVenta.Properties.NullText = ""
        Me.cboPuntoVenta.Size = New System.Drawing.Size(157, 20)
        Me.cboPuntoVenta.TabIndex = 0
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(33, 30)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(63, 13)
        Me.LabelControl22.TabIndex = 82
        Me.LabelControl22.Text = "Cód. Cliente:"
        '
        'beCodCliente
        '
        Me.beCodCliente.EnterMoveNextControl = True
        Me.beCodCliente.Location = New System.Drawing.Point(101, 28)
        Me.beCodCliente.Name = "beCodCliente"
        Me.beCodCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCodCliente.Size = New System.Drawing.Size(115, 20)
        Me.beCodCliente.TabIndex = 3
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(243, 158)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl26.TabIndex = 85
        Me.LabelControl26.Text = "% de Descto:"
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(907, 94)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl23.TabIndex = 87
        Me.LabelControl23.Text = "No. DE DOCUMENTO"
        '
        'sePjeDescuento
        '
        Me.sePjeDescuento.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.sePjeDescuento.EnterMoveNextControl = True
        Me.sePjeDescuento.Location = New System.Drawing.Point(315, 154)
        Me.sePjeDescuento.Name = "sePjeDescuento"
        Me.sePjeDescuento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.sePjeDescuento.Properties.Mask.EditMask = "P2"
        Me.sePjeDescuento.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.sePjeDescuento.Size = New System.Drawing.Size(68, 20)
        Me.sePjeDescuento.TabIndex = 20
        '
        'teNumero
        '
        Me.teNumero.Location = New System.Drawing.Point(911, 108)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Properties.AllowFocused = False
        Me.teNumero.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.teNumero.Properties.Appearance.ForeColor = System.Drawing.Color.DarkRed
        Me.teNumero.Properties.Appearance.Options.UseFont = True
        Me.teNumero.Properties.Appearance.Options.UseForeColor = True
        Me.teNumero.Size = New System.Drawing.Size(103, 26)
        Me.teNumero.TabIndex = 86
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(402, 115)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl24.TabIndex = 88
        Me.LabelControl24.Text = "Tipo de Impuesto:"
        '
        'leTipoImpuesto
        '
        Me.leTipoImpuesto.EnterMoveNextControl = True
        Me.leTipoImpuesto.Location = New System.Drawing.Point(493, 112)
        Me.leTipoImpuesto.Name = "leTipoImpuesto"
        Me.leTipoImpuesto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoImpuesto.Size = New System.Drawing.Size(97, 20)
        Me.leTipoImpuesto.TabIndex = 18
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(336, 74)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl25.TabIndex = 90
        Me.LabelControl25.Text = "Municipio:"
        '
        'leMunicipio
        '
        Me.leMunicipio.EnterMoveNextControl = True
        Me.leMunicipio.Location = New System.Drawing.Point(385, 70)
        Me.leMunicipio.Name = "leMunicipio"
        Me.leMunicipio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leMunicipio.Properties.NullText = ""
        Me.leMunicipio.Size = New System.Drawing.Size(205, 20)
        Me.leMunicipio.TabIndex = 8
        '
        'chkMargen
        '
        Me.chkMargen.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.chkMargen.Location = New System.Drawing.Point(272, 28)
        Me.chkMargen.Name = "chkMargen"
        Me.chkMargen.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMargen.Properties.Appearance.Options.UseFont = True
        Me.chkMargen.Properties.Caption = "Aplicar Descuento Margen"
        Me.chkMargen.Size = New System.Drawing.Size(222, 20)
        Me.chkMargen.TabIndex = 100
        Me.chkMargen.Visible = False
        '
        'pcDetalle
        '
        Me.pcDetalle.Controls.Add(Me.sbImportarBoletos)
        Me.pcDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pcDetalle.Location = New System.Drawing.Point(0, 179)
        Me.pcDetalle.Name = "pcDetalle"
        Me.pcDetalle.Size = New System.Drawing.Size(1175, 212)
        Me.pcDetalle.TabIndex = 1
        '
        'sbImportarBoletos
        '
        Me.sbImportarBoletos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.sbImportarBoletos.Location = New System.Drawing.Point(3, 3)
        Me.sbImportarBoletos.MainView = Me.gv
        Me.sbImportarBoletos.Name = "sbImportarBoletos"
        Me.sbImportarBoletos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.ritePrecioUnitario, Me.ritePrecioVenta, Me.riteCantidad, Me.ritePorcDescto, Me.riteCodigo, Me.riteTipoPrecio, Me.riteCantidad2, Me.btnCentroCosto, Me.riteCentroCosto})
        Me.sbImportarBoletos.Size = New System.Drawing.Size(1169, 206)
        Me.sbImportarBoletos.TabIndex = 0
        Me.sbImportarBoletos.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.IdProducto, Me.Cantidad, Me.IdPrecio, Me.Descripcion, Me.gcCentroCosto, Me.PrecioVenta, Me.PorcDescuento, Me.PrecioUnitario, Me.gcVentaNoSujeta, Me.gcVentaExenta, Me.gcVentaAfecta, Me.gcValorDescuento, Me.gcValorIVA, Me.gcVentaNeta, Me.GridColumn1, Me.GridColumn3, Me.GridColumn4, Me.GridColumn2, Me.gcIdComprobante})
        Me.gv.GridControl = Me.sbImportarBoletos
        Me.gv.Name = "gv"
        Me.gv.NewItemRowText = "Click aqui para agregar nuevo registro... Esc para salir"
        Me.gv.OptionsNavigation.AutoFocusNewRow = True
        Me.gv.OptionsNavigation.EnterMoveNextColumn = True
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'IdProducto
        '
        Me.IdProducto.Caption = "Cód. Producto"
        Me.IdProducto.ColumnEdit = Me.riteCodigo
        Me.IdProducto.FieldName = "IdProducto"
        Me.IdProducto.Name = "IdProducto"
        Me.IdProducto.ToolTip = "Código de producto. <Enter> para consultar"
        Me.IdProducto.Visible = True
        Me.IdProducto.VisibleIndex = 0
        Me.IdProducto.Width = 91
        '
        'riteCodigo
        '
        Me.riteCodigo.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCodigo.AutoHeight = False
        Me.riteCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.riteCodigo.Name = "riteCodigo"
        '
        'Cantidad
        '
        Me.Cantidad.Caption = "Cantidad"
        Me.Cantidad.ColumnEdit = Me.riteCantidad
        Me.Cantidad.FieldName = "Cantidad"
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.Visible = True
        Me.Cantidad.VisibleIndex = 1
        Me.Cantidad.Width = 64
        '
        'riteCantidad
        '
        Me.riteCantidad.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCantidad.AutoHeight = False
        Me.riteCantidad.Mask.EditMask = "n3"
        Me.riteCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.riteCantidad.Name = "riteCantidad"
        '
        'IdPrecio
        '
        Me.IdPrecio.Caption = "T/P"
        Me.IdPrecio.ColumnEdit = Me.riteTipoPrecio
        Me.IdPrecio.FieldName = "IdPrecio"
        Me.IdPrecio.Name = "IdPrecio"
        Me.IdPrecio.Visible = True
        Me.IdPrecio.VisibleIndex = 2
        Me.IdPrecio.Width = 41
        '
        'riteTipoPrecio
        '
        Me.riteTipoPrecio.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteTipoPrecio.AutoHeight = False
        Me.riteTipoPrecio.Mask.EditMask = "###"
        Me.riteTipoPrecio.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteTipoPrecio.Mask.UseMaskAsDisplayFormat = True
        Me.riteTipoPrecio.Name = "riteTipoPrecio"
        Me.riteTipoPrecio.NullText = "1"
        '
        'Descripcion
        '
        Me.Descripcion.Caption = "Descripción"
        Me.Descripcion.FieldName = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.Visible = True
        Me.Descripcion.VisibleIndex = 3
        Me.Descripcion.Width = 313
        '
        'gcCentroCosto
        '
        Me.gcCentroCosto.Caption = "Centro Costo"
        Me.gcCentroCosto.ColumnEdit = Me.riteCentroCosto
        Me.gcCentroCosto.FieldName = "IdCentro"
        Me.gcCentroCosto.Name = "gcCentroCosto"
        Me.gcCentroCosto.OptionsColumn.AllowEdit = False
        Me.gcCentroCosto.OptionsColumn.AllowFocus = False
        Me.gcCentroCosto.Visible = True
        Me.gcCentroCosto.VisibleIndex = 4
        '
        'riteCentroCosto
        '
        Me.riteCentroCosto.AutoHeight = False
        Me.riteCentroCosto.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteCentroCosto.Name = "riteCentroCosto"
        '
        'PrecioVenta
        '
        Me.PrecioVenta.Caption = "Precio de Lista"
        Me.PrecioVenta.ColumnEdit = Me.ritePrecioVenta
        Me.PrecioVenta.FieldName = "PrecioVenta"
        Me.PrecioVenta.Name = "PrecioVenta"
        Me.PrecioVenta.OptionsColumn.AllowEdit = False
        Me.PrecioVenta.OptionsColumn.AllowFocus = False
        Me.PrecioVenta.Visible = True
        Me.PrecioVenta.VisibleIndex = 5
        Me.PrecioVenta.Width = 98
        '
        'ritePrecioVenta
        '
        Me.ritePrecioVenta.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.ritePrecioVenta.AutoHeight = False
        Me.ritePrecioVenta.Mask.EditMask = "n6"
        Me.ritePrecioVenta.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePrecioVenta.Mask.UseMaskAsDisplayFormat = True
        Me.ritePrecioVenta.Name = "ritePrecioVenta"
        '
        'PorcDescuento
        '
        Me.PorcDescuento.Caption = "% Descto."
        Me.PorcDescuento.ColumnEdit = Me.ritePorcDescto
        Me.PorcDescuento.FieldName = "PorcDescuento"
        Me.PorcDescuento.Name = "PorcDescuento"
        Me.PorcDescuento.Visible = True
        Me.PorcDescuento.VisibleIndex = 6
        Me.PorcDescuento.Width = 77
        '
        'ritePorcDescto
        '
        Me.ritePorcDescto.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.ritePorcDescto.AutoHeight = False
        Me.ritePorcDescto.Mask.EditMask = "##0.00"
        Me.ritePorcDescto.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePorcDescto.Mask.UseMaskAsDisplayFormat = True
        Me.ritePorcDescto.Name = "ritePorcDescto"
        '
        'PrecioUnitario
        '
        Me.PrecioUnitario.Caption = "Precio Unitario"
        Me.PrecioUnitario.ColumnEdit = Me.ritePrecioUnitario
        Me.PrecioUnitario.FieldName = "PrecioUnitario"
        Me.PrecioUnitario.Name = "PrecioUnitario"
        Me.PrecioUnitario.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "precio_unitario", "SUBTOTALES")})
        Me.PrecioUnitario.Visible = True
        Me.PrecioUnitario.VisibleIndex = 7
        Me.PrecioUnitario.Width = 101
        '
        'ritePrecioUnitario
        '
        Me.ritePrecioUnitario.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.ritePrecioUnitario.AutoHeight = False
        Me.ritePrecioUnitario.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.ritePrecioUnitario.Mask.EditMask = "n6"
        Me.ritePrecioUnitario.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePrecioUnitario.Mask.UseMaskAsDisplayFormat = True
        Me.ritePrecioUnitario.Name = "ritePrecioUnitario"
        '
        'gcVentaNoSujeta
        '
        Me.gcVentaNoSujeta.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.gcVentaNoSujeta.AppearanceCell.Options.UseBackColor = True
        Me.gcVentaNoSujeta.Caption = "Venta No Sujeta"
        Me.gcVentaNoSujeta.DisplayFormat.FormatString = "{0:c}"
        Me.gcVentaNoSujeta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcVentaNoSujeta.FieldName = "VentaNoSujeta"
        Me.gcVentaNoSujeta.Name = "gcVentaNoSujeta"
        Me.gcVentaNoSujeta.OptionsColumn.AllowEdit = False
        Me.gcVentaNoSujeta.OptionsColumn.AllowFocus = False
        Me.gcVentaNoSujeta.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VentaNoSujeta", "{0:c}")})
        Me.gcVentaNoSujeta.Visible = True
        Me.gcVentaNoSujeta.VisibleIndex = 8
        Me.gcVentaNoSujeta.Width = 112
        '
        'gcVentaExenta
        '
        Me.gcVentaExenta.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.gcVentaExenta.AppearanceCell.Options.UseBackColor = True
        Me.gcVentaExenta.Caption = "Venta Exenta"
        Me.gcVentaExenta.DisplayFormat.FormatString = "{0:c}"
        Me.gcVentaExenta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcVentaExenta.FieldName = "VentaExenta"
        Me.gcVentaExenta.Name = "gcVentaExenta"
        Me.gcVentaExenta.OptionsColumn.AllowEdit = False
        Me.gcVentaExenta.OptionsColumn.AllowFocus = False
        Me.gcVentaExenta.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VentaExenta", "{0:c}")})
        Me.gcVentaExenta.Visible = True
        Me.gcVentaExenta.VisibleIndex = 9
        Me.gcVentaExenta.Width = 102
        '
        'gcVentaAfecta
        '
        Me.gcVentaAfecta.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.gcVentaAfecta.AppearanceCell.Options.UseBackColor = True
        Me.gcVentaAfecta.Caption = "Venta Afecta"
        Me.gcVentaAfecta.DisplayFormat.FormatString = "{0:c}"
        Me.gcVentaAfecta.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcVentaAfecta.FieldName = "VentaAfecta"
        Me.gcVentaAfecta.Name = "gcVentaAfecta"
        Me.gcVentaAfecta.OptionsColumn.AllowEdit = False
        Me.gcVentaAfecta.OptionsColumn.AllowFocus = False
        Me.gcVentaAfecta.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VentaAfecta", "{0:c}")})
        Me.gcVentaAfecta.Visible = True
        Me.gcVentaAfecta.VisibleIndex = 10
        Me.gcVentaAfecta.Width = 102
        '
        'gcValorDescuento
        '
        Me.gcValorDescuento.Caption = "Descuento"
        Me.gcValorDescuento.FieldName = "ValorDescuento"
        Me.gcValorDescuento.Name = "gcValorDescuento"
        Me.gcValorDescuento.OptionsColumn.AllowEdit = False
        Me.gcValorDescuento.OptionsColumn.AllowFocus = False
        Me.gcValorDescuento.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)})
        '
        'gcValorIVA
        '
        Me.gcValorIVA.Caption = "IVA"
        Me.gcValorIVA.FieldName = "ValorIVA"
        Me.gcValorIVA.Name = "gcValorIVA"
        Me.gcValorIVA.OptionsColumn.AllowEdit = False
        Me.gcValorIVA.OptionsColumn.AllowFocus = False
        Me.gcValorIVA.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)})
        '
        'gcVentaNeta
        '
        Me.gcVentaNeta.Caption = "VentaNeta"
        Me.gcVentaNeta.FieldName = "VentaNeta"
        Me.gcVentaNeta.Name = "gcVentaNeta"
        Me.gcVentaNeta.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)})
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "DescuentoAutorizadoPor"
        Me.GridColumn1.FieldName = "DescuentoAutorizadoPor"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "PrecioCosto"
        Me.GridColumn3.FieldName = "PrecioCosto"
        Me.GridColumn3.Name = "GridColumn3"
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "TipoProducto"
        Me.GridColumn4.FieldName = "TipoProducto"
        Me.GridColumn4.Name = "GridColumn4"
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "UltPrecio"
        Me.GridColumn2.FieldName = "UltPrecio"
        Me.GridColumn2.Name = "GridColumn2"
        '
        'gcIdComprobante
        '
        Me.gcIdComprobante.Caption = "IdComprobante"
        Me.gcIdComprobante.FieldName = "IdComprobante"
        Me.gcIdComprobante.Name = "gcIdComprobante"
        '
        'riteCantidad2
        '
        Me.riteCantidad2.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCantidad2.AutoHeight = False
        Me.riteCantidad2.Mask.EditMask = "n3"
        Me.riteCantidad2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteCantidad2.Mask.UseMaskAsDisplayFormat = True
        Me.riteCantidad2.Name = "riteCantidad2"
        '
        'btnCentroCosto
        '
        Me.btnCentroCosto.AutoHeight = False
        Me.btnCentroCosto.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Seleccionar Centro de Costo", Nothing, Nothing, True)})
        Me.btnCentroCosto.Name = "btnCentroCosto"
        Me.btnCentroCosto.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdBorrar)
        Me.pcBotones.Controls.Add(Me.cmdDown)
        Me.pcBotones.Controls.Add(Me.cmdUp)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(1175, 179)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(37, 307)
        Me.pcBotones.TabIndex = 3
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(4, 76)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 23)
        Me.cmdBorrar.TabIndex = 34
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(4, 32)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(27, 24)
        Me.cmdDown.TabIndex = 33
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(4, 6)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(27, 24)
        Me.cmdUp.TabIndex = 32
        '
        'pcTotales
        '
        Me.pcTotales.Controls.Add(Me.lblUltimoCambio)
        Me.pcTotales.Controls.Add(Me.sbEditarCESC)
        Me.pcTotales.Controls.Add(Me.lblSucursalEnvio)
        Me.pcTotales.Controls.Add(Me.leSucursalEnvio)
        Me.pcTotales.Controls.Add(Me.teLimiteLineas)
        Me.pcTotales.Controls.Add(Me.ceCESC)
        Me.pcTotales.Controls.Add(Me.chkMargen)
        Me.pcTotales.Controls.Add(Me.LabelControl29)
        Me.pcTotales.Controls.Add(Me.teLineas)
        Me.pcTotales.Controls.Add(Me.LabelControl28)
        Me.pcTotales.Controls.Add(Me.btGuardar)
        Me.pcTotales.Controls.Add(Me.meComentario)
        Me.pcTotales.Controls.Add(Me.ceRetencionPercepcion)
        Me.pcTotales.Controls.Add(Me.ceDevolucion)
        Me.pcTotales.Controls.Add(Me.LabelControl15)
        Me.pcTotales.Controls.Add(Me.teCESC)
        Me.pcTotales.Controls.Add(Me.teDescuento)
        Me.pcTotales.Controls.Add(Me.LabelControl17)
        Me.pcTotales.Controls.Add(Me.LabelControl16)
        Me.pcTotales.Controls.Add(Me.teIVA)
        Me.pcTotales.Controls.Add(Me.lblPercReten)
        Me.pcTotales.Controls.Add(Me.teRetencionPercepcion)
        Me.pcTotales.Controls.Add(Me.LabelControl18)
        Me.pcTotales.Controls.Add(Me.teTotal)
        Me.pcTotales.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pcTotales.Location = New System.Drawing.Point(0, 391)
        Me.pcTotales.Name = "pcTotales"
        Me.pcTotales.Size = New System.Drawing.Size(1175, 95)
        Me.pcTotales.TabIndex = 2
        '
        'lblUltimoCambio
        '
        Me.lblUltimoCambio.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblUltimoCambio.Appearance.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUltimoCambio.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblUltimoCambio.Appearance.Options.UseFont = True
        Me.lblUltimoCambio.Appearance.Options.UseForeColor = True
        Me.lblUltimoCambio.Location = New System.Drawing.Point(9, 64)
        Me.lblUltimoCambio.Name = "lblUltimoCambio"
        Me.lblUltimoCambio.Size = New System.Drawing.Size(252, 25)
        Me.lblUltimoCambio.TabIndex = 122
        Me.lblUltimoCambio.Text = "ULTIMO CAMBIO: $0.00"
        '
        'sbEditarCESC
        '
        Me.sbEditarCESC.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.sbEditarCESC.Location = New System.Drawing.Point(887, 48)
        Me.sbEditarCESC.Name = "sbEditarCESC"
        Me.sbEditarCESC.Size = New System.Drawing.Size(60, 18)
        Me.sbEditarCESC.TabIndex = 121
        Me.sbEditarCESC.Text = "Editar..."
        '
        'lblSucursalEnvio
        '
        Me.lblSucursalEnvio.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblSucursalEnvio.Appearance.Options.UseFont = True
        Me.lblSucursalEnvio.Location = New System.Drawing.Point(285, 6)
        Me.lblSucursalEnvio.Name = "lblSucursalEnvio"
        Me.lblSucursalEnvio.Size = New System.Drawing.Size(125, 13)
        Me.lblSucursalEnvio.TabIndex = 102
        Me.lblSucursalEnvio.Text = "Sucursal Recibe Envío:"
        Me.lblSucursalEnvio.Visible = False
        '
        'leSucursalEnvio
        '
        Me.leSucursalEnvio.EnterMoveNextControl = True
        Me.leSucursalEnvio.Location = New System.Drawing.Point(412, 3)
        Me.leSucursalEnvio.Name = "leSucursalEnvio"
        Me.leSucursalEnvio.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.leSucursalEnvio.Properties.Appearance.Options.UseFont = True
        Me.leSucursalEnvio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalEnvio.Properties.NullText = ""
        Me.leSucursalEnvio.Size = New System.Drawing.Size(218, 20)
        Me.leSucursalEnvio.TabIndex = 101
        Me.leSucursalEnvio.Visible = False
        '
        'teLimiteLineas
        '
        Me.teLimiteLineas.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teLimiteLineas.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.teLimiteLineas.Enabled = False
        Me.teLimiteLineas.Location = New System.Drawing.Point(820, 10)
        Me.teLimiteLineas.Name = "teLimiteLineas"
        Me.teLimiteLineas.Properties.AllowFocused = False
        Me.teLimiteLineas.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.teLimiteLineas.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.teLimiteLineas.Properties.Appearance.Options.UseFont = True
        Me.teLimiteLineas.Properties.Appearance.Options.UseForeColor = True
        Me.teLimiteLineas.Properties.Appearance.Options.UseTextOptions = True
        Me.teLimiteLineas.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teLimiteLineas.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teLimiteLineas.Properties.ReadOnly = True
        Me.teLimiteLineas.Size = New System.Drawing.Size(61, 26)
        Me.teLimiteLineas.TabIndex = 95
        '
        'ceCESC
        '
        Me.ceCESC.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.ceCESC.Location = New System.Drawing.Point(889, 7)
        Me.ceCESC.Name = "ceCESC"
        Me.ceCESC.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ceCESC.Properties.Appearance.Options.UseFont = True
        Me.ceCESC.Properties.Caption = "CESC"
        Me.ceCESC.Size = New System.Drawing.Size(63, 20)
        Me.ceCESC.TabIndex = 100
        '
        'LabelControl29
        '
        Me.LabelControl29.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl29.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl29.Appearance.ForeColor = System.Drawing.Color.Black
        Me.LabelControl29.Appearance.Options.UseFont = True
        Me.LabelControl29.Appearance.Options.UseForeColor = True
        Me.LabelControl29.Location = New System.Drawing.Point(759, 15)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(57, 19)
        Me.LabelControl29.TabIndex = 94
        Me.LabelControl29.Text = "Limite:"
        '
        'teLineas
        '
        Me.teLineas.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teLineas.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.teLineas.Enabled = False
        Me.teLineas.Location = New System.Drawing.Point(820, 37)
        Me.teLineas.Name = "teLineas"
        Me.teLineas.Properties.AllowFocused = False
        Me.teLineas.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.teLineas.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.teLineas.Properties.Appearance.Options.UseFont = True
        Me.teLineas.Properties.Appearance.Options.UseForeColor = True
        Me.teLineas.Properties.Appearance.Options.UseTextOptions = True
        Me.teLineas.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teLineas.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teLineas.Properties.ReadOnly = True
        Me.teLineas.Size = New System.Drawing.Size(61, 26)
        Me.teLineas.TabIndex = 93
        '
        'LabelControl28
        '
        Me.LabelControl28.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl28.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl28.Appearance.ForeColor = System.Drawing.Color.Black
        Me.LabelControl28.Appearance.Options.UseFont = True
        Me.LabelControl28.Appearance.Options.UseForeColor = True
        Me.LabelControl28.Location = New System.Drawing.Point(726, 40)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(90, 19)
        Me.LabelControl28.TabIndex = 48
        Me.LabelControl28.Text = "No. Lineas:"
        '
        'btGuardar
        '
        Me.btGuardar.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btGuardar.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btGuardar.Appearance.Options.UseFont = True
        Me.btGuardar.Image = Global.Nexus.My.Resources.Resources.Save
        Me.btGuardar.Location = New System.Drawing.Point(572, 30)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(143, 31)
        Me.btGuardar.TabIndex = 47
        Me.btGuardar.Text = "&Guardar (Alt-G)"
        '
        'meComentario
        '
        Me.meComentario.Location = New System.Drawing.Point(9, 21)
        Me.meComentario.Name = "meComentario"
        Me.meComentario.Size = New System.Drawing.Size(257, 37)
        Me.meComentario.TabIndex = 46
        '
        'ceRetencionPercepcion
        '
        Me.ceRetencionPercepcion.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.ceRetencionPercepcion.Location = New System.Drawing.Point(272, 45)
        Me.ceRetencionPercepcion.Name = "ceRetencionPercepcion"
        Me.ceRetencionPercepcion.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ceRetencionPercepcion.Properties.Appearance.Options.UseFont = True
        Me.ceRetencionPercepcion.Properties.Caption = "APLICAR RETENCIÓN DEL 1 %"
        Me.ceRetencionPercepcion.Size = New System.Drawing.Size(248, 20)
        Me.ceRetencionPercepcion.TabIndex = 44
        '
        'ceDevolucion
        '
        Me.ceDevolucion.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.ceDevolucion.Location = New System.Drawing.Point(569, 65)
        Me.ceDevolucion.Name = "ceDevolucion"
        Me.ceDevolucion.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ceDevolucion.Properties.Appearance.Options.UseFont = True
        Me.ceDevolucion.Properties.Caption = "Marcar como Ticket de Devolución"
        Me.ceDevolucion.Size = New System.Drawing.Size(325, 23)
        Me.ceDevolucion.TabIndex = 44
        '
        'LabelControl15
        '
        Me.LabelControl15.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl15.Location = New System.Drawing.Point(970, 2)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl15.TabIndex = 36
        Me.LabelControl15.Text = "Descuento:"
        '
        'teCESC
        '
        Me.teCESC.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teCESC.EditValue = New Decimal(New Integer() {0, 0, 0, 65536})
        Me.teCESC.EnterMoveNextControl = True
        Me.teCESC.Location = New System.Drawing.Point(887, 28)
        Me.teCESC.Name = "teCESC"
        Me.teCESC.Properties.Appearance.Options.UseTextOptions = True
        Me.teCESC.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teCESC.Properties.DisplayFormat.FormatString = "c"
        Me.teCESC.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teCESC.Properties.Mask.EditMask = "n2"
        Me.teCESC.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teCESC.Size = New System.Drawing.Size(68, 20)
        ToolTipTitleItem1.Text = "CESC"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Debe presionar <Enter> para que el valor editado por el usuario sea considerado."
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.teCESC.SuperTip = SuperToolTip1
        Me.teCESC.TabIndex = 37
        '
        'teDescuento
        '
        Me.teDescuento.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teDescuento.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.teDescuento.Location = New System.Drawing.Point(1071, 2)
        Me.teDescuento.Name = "teDescuento"
        Me.teDescuento.Properties.Appearance.Options.UseTextOptions = True
        Me.teDescuento.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teDescuento.Properties.DisplayFormat.FormatString = "c"
        Me.teDescuento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teDescuento.Properties.ReadOnly = True
        Me.teDescuento.Size = New System.Drawing.Size(98, 20)
        Me.teDescuento.TabIndex = 37
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(9, 6)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl17.TabIndex = 39
        Me.LabelControl17.Text = "Comentarios:"
        '
        'LabelControl16
        '
        Me.LabelControl16.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl16.Location = New System.Drawing.Point(970, 23)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl16.TabIndex = 39
        Me.LabelControl16.Text = "IVA:"
        '
        'teIVA
        '
        Me.teIVA.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teIVA.Location = New System.Drawing.Point(1071, 23)
        Me.teIVA.Name = "teIVA"
        Me.teIVA.Properties.Appearance.Options.UseTextOptions = True
        Me.teIVA.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teIVA.Properties.DisplayFormat.FormatString = "c"
        Me.teIVA.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teIVA.Properties.Mask.EditMask = "c"
        Me.teIVA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teIVA.Properties.ReadOnly = True
        Me.teIVA.Size = New System.Drawing.Size(98, 20)
        Me.teIVA.TabIndex = 38
        '
        'lblPercReten
        '
        Me.lblPercReten.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblPercReten.Location = New System.Drawing.Point(970, 44)
        Me.lblPercReten.Name = "lblPercReten"
        Me.lblPercReten.Size = New System.Drawing.Size(82, 13)
        Me.lblPercReten.TabIndex = 41
        Me.lblPercReten.Text = "(-) IVA Retenido:"
        '
        'teRetencionPercepcion
        '
        Me.teRetencionPercepcion.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teRetencionPercepcion.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.teRetencionPercepcion.Location = New System.Drawing.Point(1071, 44)
        Me.teRetencionPercepcion.Name = "teRetencionPercepcion"
        Me.teRetencionPercepcion.Properties.Appearance.Options.UseTextOptions = True
        Me.teRetencionPercepcion.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teRetencionPercepcion.Properties.DisplayFormat.FormatString = "c"
        Me.teRetencionPercepcion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teRetencionPercepcion.Properties.Mask.EditMask = "c"
        Me.teRetencionPercepcion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teRetencionPercepcion.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teRetencionPercepcion.Properties.ReadOnly = True
        Me.teRetencionPercepcion.Size = New System.Drawing.Size(98, 20)
        Me.teRetencionPercepcion.TabIndex = 40
        '
        'LabelControl18
        '
        Me.LabelControl18.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(970, 71)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(61, 19)
        Me.LabelControl18.TabIndex = 43
        Me.LabelControl18.Text = "TOTAL:"
        '
        'teTotal
        '
        Me.teTotal.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teTotal.EditValue = New Decimal(New Integer() {0, 0, 0, 65536})
        Me.teTotal.Location = New System.Drawing.Point(1047, 66)
        Me.teTotal.Name = "teTotal"
        Me.teTotal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.teTotal.Properties.Appearance.Options.UseFont = True
        Me.teTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotal.Properties.DisplayFormat.FormatString = "c"
        Me.teTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teTotal.Properties.ReadOnly = True
        Me.teTotal.Size = New System.Drawing.Size(122, 26)
        Me.teTotal.TabIndex = 42
        '
        'fac_frmFacturacion
        '
        Me.ClientSize = New System.Drawing.Size(1212, 514)
        Me.Controls.Add(Me.pcDetalle)
        Me.Controls.Add(Me.pcTotales)
        Me.Controls.Add(Me.pcBotones)
        Me.Controls.Add(Me.pcHeader)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmFacturacion"
        Me.OcultarBotonGuardar = True
        Me.OptionId = "002004"
        Me.Text = "Ventas y Facturación"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.pcHeader, 0)
        Me.Controls.SetChildIndex(Me.pcBotones, 0)
        Me.Controls.SetChildIndex(Me.pcTotales, 0)
        Me.Controls.SetChildIndex(Me.pcDetalle, 0)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.TelimiteSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumeroUnico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoComprobante.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teGiro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNotaRemision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumPedido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teVentaCuentaDe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leDepartamento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboPuntoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCodCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sePjeDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoImpuesto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leMunicipio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMargen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcDetalle.ResumeLayout(False)
        CType(Me.sbImportarBoletos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteTipoPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCentroCosto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePrecioVenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePorcDescto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePrecioUnitario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCantidad2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCentroCosto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcTotales.ResumeLayout(False)
        Me.pcTotales.PerformLayout()
        CType(Me.leSucursalEnvio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teLimiteLineas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceCESC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teLineas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meComentario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceRetencionPercepcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceDevolucion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCESC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIVA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teRetencionPercepcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoComprobante As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lblNRC_DUI As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teGiro As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNotaRemision As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leFormaPago As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumPedido As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leVendedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leBodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teVentaCuentaDe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leDepartamento As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seDiasCredito As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cboPuntoVenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCodCliente As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sePjeDescuento As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoImpuesto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leMunicipio As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents pcDetalle As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pcTotales As DevExpress.XtraEditors.PanelControl
    Friend WithEvents sbImportarBoletos As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents IdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Cantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents IdPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Descripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PrecioVenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ritePrecioVenta As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents PorcDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ritePorcDescto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents PrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ritePrecioUnitario As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcVentaNoSujeta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcVentaExenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorDescuento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorIVA As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDescuento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIVA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPercReten As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teRetencionPercepcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gcVentaNeta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ceDevolucion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents gcVentaAfecta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents sbObtenerPedido As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ceRetencionPercepcion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meComentario As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teSaldo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents riteCodigo As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents riteTipoPrecio As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents btGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teLimiteLineas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teLineas As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PictureEdit2 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents chkMargen As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgTipo As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents riteCantidad2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents teNumeroUnico As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTelefonos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblSucursalEnvio As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursalEnvio As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teCESC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ceCESC As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents sbEditarCESC As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcCentroCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnCentroCosto As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents riteCentroCosto As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lblUltimoCambio As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TelimiteSaldo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents sbImportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbMarcarBoletos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcIdComprobante As DevExpress.XtraGrid.Columns.GridColumn

End Class
