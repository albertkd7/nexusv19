﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports System.IO

Public Class fac_frmPreFacturacion
    Dim blFac As New FacturaBLL(g_ConnectionString)
    Dim blAdm As New AdmonBLL(g_ConnectionString), dtPar As DataTable = blAdm.ObtieneParametros()

    Private Sub fac_frmPreFacturacion_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        Dim FechaActiva As DateTime = objFunciones.GetFechaContable(dtPar.Rows(0).Item("IdSucursal"))
        teNit.EditValue = ""
        teNrc.EditValue = ""
        teNombre.EditValue = ""
        objCombos.fac_TiposComprobante(leTipoDoc, "")
        objCombos.fac_FormasPago(leFormaPago, "")
        gcConsultaPed.DataSource = blFac.fac_ConsultaPedidos(objMenu.User, Today, Today, True)
        deDesde.EditValue = FechaActiva 'Today
        deHasta.EditValue = FechaActiva 'Today

        deFechaFacturacion.EditValue = objFunciones.GetFechaContable(dtPar.Rows(0).Item("IdSucursal"))

        sbAplicarInventario.Visible = gsNombre_Empresa.StartsWith("KEIRAN, SOCIEDAD ANONIMA DE CAPITAL VARIABLE")
    End Sub

    Private Sub sbConsultaPed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbConsultaPed.Click
        gcConsultaPed.DataSource = blFac.fac_ConsultaPedidos(objMenu.User, deDesde.EditValue, deHasta.EditValue, ceTipo.Checked)
        gvConsultaPed.BestFitColumns()
        deFechaFacturacion.EditValue = objFunciones.GetFechaContable(dtPar.Rows(0).Item("IdSucursal"))
    End Sub

    Private Sub fac_frmConsultaPedidos_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Close()
        End If
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnImprimir.Click
        Dim IdComprobante As Integer = gvConsultaPed.GetFocusedRowCellValue("IdComprobante")

        Dim dt As DataTable = blFac.fac_ObtenerPedidoPreHoja(IdComprobante, 0)
        Dim rpt As New fac_rptPedido With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        'rpt.xrlTipoDoc.Text = leTipoDoc.Text
        'rpt.xrlTotalPedido.Text = Format(CDec(gvConsultaPed.GetFocusedRowCellValue("Total")) * 100, "#########")
        'rpt.xrlUtilidadPerdida.Text = Format(CDec(gvConsultaPed.GetFocusedRowCellValue("UtilidadPerdida")), "##,###,##0.00")
        'If InStr(gsNombre_Empresa, "1") = 0 Then
        '    rpt.xrlCantA.Visible = False
        '    rpt.xrlCantB.Visible = False
        '    rpt.xrlCantAD.Visible = False
        '    rpt.xrlCantBD.Visible = False
        'End If
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub btnFacturar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFacturar.Click
        ObtieneCorrelativo() 'actualizo el correlativo en pantalla para que vean el numero a asignar

        Dim IdPedido As Integer = gvConsultaPed.GetFocusedRowCellValue("IdComprobante")
        Dim entPe As fac_Pedidos = objTablas.fac_PedidosSelectByPK(IdPedido)
        Dim entTiposDoc As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(entPe.IdTipoComprobante)
        Dim Tipo As String = entTiposDoc.Abreviatura

        If IdPedido = 0 Then
            MsgBox("Debe de seleccionar el pedido que requiere facturar", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If teNombre.EditValue = "" Then
            MsgBox("Debe especificar el nombre del cliente", MsgBoxStyle.Information, "Nota")
            teNombre.Focus()
            Exit Sub
        End If

        If gvConsultaPed.GetFocusedRowCellValue("Facturado") Then
            MsgBox(String.Format("Este pedido ya fué facturado{0}Debe anular el documento si desea volver a facturarla{1}", Chr(13), Chr(13)), MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        'Dim TotalPedido As Decimal = IIf(entTiposDoc.DetallaIVA, ((entPe.TotalComprobante), entPe.TotalAfecto - entPe.TotalImpuesto1)
        'Dim FechaServer As Date = objFunciones.GetFechaContable(dtPar.Rows(0).Item("IdSucursal")) 'blFac.fac_FechaServer()
        'If FechaServer <> gvConsultaPed.GetFocusedRowCellValue("Fecha") Then
        '    MsgBox(String.Format("Este pedido corresponde a una fecha no valida", Chr(13), Chr(13)), MsgBoxStyle.Critical, "Nota")
        '    Exit Sub
        'End If

        Dim NumDocumento As String = teNit.EditValue

        If entPe.IdTipoComprobante = 5 Or entPe.IdTipoComprobante = 8 Then 'leTipoDoc.EditValue
            If teNrc.EditValue = "" And teNit.EditValue = "" Then
                MsgBox("Debe colocar número de Registro ó NIT ", MsgBoxStyle.Information, "Nota")
                Exit Sub
            End If

            If teNit.EditValue <> "" And NumDocumento.Length <> 17 Then
                MsgBox("Debe colocar un número de NIT Correcto", MsgBoxStyle.Information, "Nota")
                Exit Sub
            End If
        Else
            If teNit.EditValue = "" And entPe.TotalComprobante >= 200.0 Then
                MsgBox("Debe colocar número de DUI ó NIT", MsgBoxStyle.Information, "Nota")
                Exit Sub
            End If

            If teNit.EditValue <> "" And (NumDocumento.Length <> 10 And NumDocumento.Length <> 17) And entPe.TotalComprobante >= 200.0 Then
                MsgBox("Debe colocar un número de DUI o NIT Correcto", MsgBoxStyle.Information, "Nota")
                Exit Sub
            End If
        End If

        Dim FechaActiva As Date = objFunciones.GetFechaContable(dtPar.Rows(0).Item("IdSucursal"))

        If Tipo = "NCI" Or Tipo = "FDV" Or entPe.Devolucion = True Then 'se debe tener cuidado con éstas iniciales, son las devoluciones" Then
        Else
            Dim dt As DataTable = blFac.fac_VerificaExistenciasPorIdPedido(IdPedido, gvConsultaPed.GetFocusedRowCellValue("IdBodega"), True)  'solamente falta obtener la bodega de donde se factura
            If dt.Rows.Count > 0 And dtPar.Rows(0).Item("ValidarExistencias") Then
                MsgBox(String.Format("Existen articulos que sobregiran las existencias{0}Vea el siguiente informe para corregir", Chr(13)), MsgBoxStyle.Information, "Imposible continuar")
                Dim rpt As New fac_rptReferenciasSobregiran
                rpt.DataSource = dt
                rpt.DataMember = ""
                rpt.xrlEmpresa.Text = gsNombre_Empresa
                rpt.xrlPedido.Text = "CORRELATIVO DE PEDIDO: " + CStr(IdPedido)

                rpt.ShowPreview()
                Exit Sub
            End If
        End If

        If MsgBox("¿Está seguro(a) de facturar éste pedido con los siguientes datos?" _
        + Chr(13) + "" _
        + Chr(13) + "Nombre: " + teNombre.EditValue + Chr(13) + "TipoComprobante: " + entTiposDoc.Nombre _
        + Chr(13) + "Forma de Pago: " + leFormaPago.Text _
        + Chr(13) + "TotalComprobante: $ " + Format(entPe.TotalComprobante, "##,##0.00"), MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim nf As Integer = 1

        'PARA PROBAR, SE HA REALIZADO ESTE CAMBIO CON EL CLIENTE PITUTAS 08/03/2016
        'LE HEMOS DADO VUELTA PARA QUE NO SE RECALCULE EL IVA Y QUE SE OBTENGA POR DIFERENCIA, A MODO QUE CUADRE CON EL PEDIDO
        'POR EL MOMENTO SE ESTÁ PROBANDO CON PITUTAS, LUEGO SE TIENE QUE VERIFICAR QUE SEA MEJOR UN PARAMETRO
        Dim RecalcularIva As Boolean = If(gsNombre_Empresa.StartsWith("PITUTA"), False, True)

        Dim msj As String = blFac.fac_GenerarFacturaPedido(IdPedido, teNombre.EditValue, teNrc.EditValue, teNit.EditValue, objMenu.User, pnIVA, sePjeDescuento.EditValue, dtPar.Rows(0).Item("TipoContribuyente") = 3, nf, entPe.IdTipoComprobante, leFormaPago.EditValue, FechaActiva, RecalcularIva)

        If msj = "" Then
            If nf = 1 Then
                msj = "Se generó un solo documento"
            Else
                msj = String.Format("Se generaron {0} documentos", nf)
            End If

            MsgBox(String.Format("El pedido ha sido facturado con éxito{0}{1}", Chr(13), msj), MsgBoxStyle.Information, "Nota")

            Dim dtFacturasPedido As DataTable = blFac.fac_ObtenerVentasPedido(IdPedido)
            Dim entPed As fac_Pedidos = objTablas.fac_PedidosSelectByPK(IdPedido)
            Dim entForma As fac_FormasPago = objTablas.fac_FormasPagoSelectByPK(leFormaPago.EditValue)

            'ACTIVACION DE LA APLICACION DE LA NOTA DE CREDITO O FACTURA DE DEVOLUCION
            If entForma.DiasCredito > 0 And (entTiposDoc.Abreviatura = "NCI" Or entTiposDoc.Abreviatura = "FDV" Or entPed.Devolucion = True) Then
                For l = 0 To dtFacturasPedido.Rows.Count - 1
                    Try
                        Dim frmNc As New fac_frmAplicacionNotaCredito
                        frmNc.IdComprob = dtFacturasPedido.Rows(l).Item("IdComprobante")
                        frmNc.NumeroNota = dtFacturasPedido.Rows(l).Item("Numero")
                        frmNc.FechaNota = dtFacturasPedido.Rows(l).Item("Fecha")
                        frmNc.IdCliente = dtFacturasPedido.Rows(l).Item("IdCliente")
                        frmNc.Nombre = dtFacturasPedido.Rows(l).Item("Nombre")

                        If dtFacturasPedido.Rows(l).Item("TotalComprobante") < 0.0 Then
                            frmNc.MontoAbonado = dtFacturasPedido.Rows(l).Item("TotalComprobante") * -1
                        Else
                            frmNc.MontoAbonado = dtFacturasPedido.Rows(l).Item("TotalComprobante")
                        End If

                        frmNc.ShowDialog()
                    Catch ex As Exception
                    End Try
                Next
            End If


            If MsgBox("¿Desea imprimir el comprobante de facturación?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
                For h = 0 To dtFacturasPedido.Rows.Count - 1
                    MsgBox("Inserte el documento de facturación", MsgBoxStyle.Information, "Nota")
                    ReImprimeDocumento(dtFacturasPedido.Rows(h).Item("IdComprobante"))
                Next
            End If

            gvConsultaPed.DeleteRow(gvConsultaPed.FocusedRowHandle)
            gvConsultaPed_Click("", New EventArgs)
        Else
            MsgBox(String.Format("Se detectó un error al generar el documento{0}{1}", Chr(13), msj), MsgBoxStyle.Critical, "Error")
        End If
        ObtieneCorrelativo() ' refresco el correlativo en pantalla para que vean el numero a asignar
    End Sub

    Private Sub gvConsultaPed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvConsultaPed.Click
        teNombre.EditValue = gvConsultaPed.GetFocusedRowCellValue("Nombre")
        teNrc.EditValue = gvConsultaPed.GetFocusedRowCellValue("Nrc")
        teNit.EditValue = gvConsultaPed.GetFocusedRowCellValue("Nit")
        sePjeDescuento.EditValue = 0.0 ' gvConsultaPed.GetFocusedRowCellValue("PorcDescuento")
        leTipoDoc.EditValue = gvConsultaPed.GetFocusedRowCellValue("IdTipoComprobante")
        leFormaPago.EditValue = gvConsultaPed.GetFocusedRowCellValue("IdFormaPago")
        ObtieneCorrelativo()
    End Sub

    Private Sub gvConsultaPed_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gvConsultaPed.FocusedRowChanged
        teNombre.EditValue = gvConsultaPed.GetFocusedRowCellValue("Nombre")
        teNrc.EditValue = gvConsultaPed.GetFocusedRowCellValue("Nrc")
        teNit.EditValue = gvConsultaPed.GetFocusedRowCellValue("Nit")
        sePjeDescuento.EditValue = 0.0 ' gvConsultaPed.GetFocusedRowCellValue("PorcDescuento")
        leTipoDoc.EditValue = gvConsultaPed.GetFocusedRowCellValue("IdTipoComprobante")
        leFormaPago.EditValue = gvConsultaPed.GetFocusedRowCellValue("IdFormaPago")

        ObtieneCorrelativo()
    End Sub

    Private Sub fac_frmPreFacturacion_Reporte() Handles Me.Reporte
        gcConsultaPed.ShowPrintPreview()
    End Sub

    Private Sub sbGenerarArchivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim dt As DataTable = blFac.fac_ObtenerDetallePedidoParaArchivo(gvConsultaPed.GetFocusedRowCellValue("IdComprobante"))
        If dt.Rows.Count = 0 Then
            MessageBox.Show("El pedido no tiene ninguna referencia", "Nota", MessageBoxButtons.OK)
            Exit Sub
        End If

        Dim fbd As New FolderBrowserDialog

        fbd.ShowDialog()
        If fbd.SelectedPath = "" Then
            Return
        End If

        Dim Archivo As String = String.Format("{0}\Transfer{1}{2}.txt", fbd.SelectedPath, gvConsultaPed.GetFocusedRowCellValue("IdComprobante"), Format(gvConsultaPed.GetFocusedRowCellValue("Fecha"), "yyyyMMdd"))

        Try
            Using Arc As StreamWriter = New StreamWriter(Archivo)

                Dim Linea As String = String.Empty

                ' Recorrer las filas del data set
                For Fila As Integer = 0 To dt.Rows.Count - 1
                    Linea = String.Empty

                    ' Recorrer la cantidad de columnas que contiene el DataSet
                    For Col As Integer = 0 To dt.Columns.Count - 1
                        Linea &= dt.Rows(Fila).Item(Col).ToString() & "|"
                    Next

                    With Arc
                        Linea = Linea.Remove(Linea.Length - 1).ToString
                        .WriteLine(Linea.ToString)
                    End With
                Next
            End Using

        Catch ex As Exception
            MsgBox(ex.Message.ToString, MsgBoxStyle.Critical, "Nota")
        Finally
            MessageBox.Show("El archivo fue generado con éxito" + Chr(13) + "Archivo: " & Archivo & Chr(13) & "Ubicado en: " & fbd.SelectedPath, "Nota", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Function ReImprimeDocumento(ByVal IdDoc As Integer) As Boolean
        Dim Ventas As fac_Ventas, Vendedores As fac_Vendedores, Departamentos As adm_Departamentos, Municipios As adm_Municipios, FormaPago As fac_FormasPago, Comprobantes As adm_TiposComprobante
        Ventas = objTablas.fac_VentasSelectByPK(IdDoc)
        Vendedores = objTablas.fac_VendedoresSelectByPK(Ventas.IdVendedor)
        Departamentos = objTablas.adm_DepartamentosSelectByPK(Ventas.IdDepartamento)
        Municipios = objTablas.adm_MunicipiosSelectByPK(Ventas.IdMunicipio)
        FormaPago = objTablas.fac_FormasPagoSelectByPK(Ventas.IdFormaPago)
        Comprobantes = objTablas.adm_TiposComprobanteSelectByPK(Ventas.IdTipoComprobante)
        Dim dt As DataTable = blFac.fac_ObtenerDetalleDocumento("fac_VentasDetalle", IdDoc)
        Dim dtAfectaNc As DataTable = blFac.fac_ObtenerAfectaNC(IdDoc) ' para saber a que documentos afecto
        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)

        Select Case Ventas.IdTipoComprobante
            Case 5 'crédito fiscal
                Dim Template = Application.StartupPath & "\Plantillas\cfNexus" & NumFormato & ".repx"

                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlDiasCredito.Text = .DiasCredito
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(.TotalIva, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalIva + Ventas.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                    rpt.xrlCantLetras.Text = Num2Text(Int(Ventas.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = .Comentario
                    rpt.PrinterName = Comprobantes.NombreImpresor

                    ' rpt.PrintDialog()
                    rpt.Print()
                End With
            Case 6 'consumidor final
                Dim Template = Application.StartupPath & "\Plantillas\faNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFactura
                rpt.LoadLayout(Template)

                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDiasCredito.Text = .DiasCredito
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalAfecto - .TotalImpuesto1, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    Dim CantidaLetras As String = Num2Text(Int(IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))) & Decimales & " DÓLARES"
                    rpt.xrlCantLetras.Text = CantidaLetras
                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPrintMarginsWarning = False
                    rpt.PrinterName = Comprobantes.NombreImpresor
                    'rpt.PrintDialog
                    rpt.Print()

                End With
            Case 7 'factura de exportación
                Dim Template = Application.StartupPath & "\Plantillas\feNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFactura
                rpt.LoadLayout(Template)

                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    Dim CantidaLetras As String = Num2Text(Int(IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))) & Decimales & " DÓLARES"
                    rpt.xrlCantLetras.Text = CantidaLetras
                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPrintMarginsWarning = False
                    rpt.PrinterName = Comprobantes.NombreImpresor
                    'rpt.PrintDialog()
                    rpt.Print()
                End With
            Case 8 'nota de crédito
                Dim Template = Application.StartupPath & "\Plantillas\ncNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)

                For i = 0 To dt.Rows.Count - 1
                    dt.Rows(i)("Cantidad") = 0 - dt.Rows(i)("Cantidad")
                    dt.Rows(i)("PrecioUnitario") = 0 - dt.Rows(i)("PrecioUnitario")
                    dt.Rows(i)("VentaExenta") = 0 - dt.Rows(i)("VentaExenta")
                    dt.Rows(i)("VentaNoSujeta") = 0 - dt.Rows(i)("VentaNoSujeta")
                    dt.Rows(i)("VentaNeta") = 0 - dt.Rows(i)("VentaNeta")
                    dt.Rows(i)("VentaAfecta") = 0 - dt.Rows(i)("VentaAfecta")
                Next

                Dim DocumentosAfectados As String = ""
                For j = 0 To dtAfectaNc.Rows.Count - 1
                    DocumentosAfectados += dtAfectaNc.Rows(j)("NumComprobanteVenta") + ", "
                Next

                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlDocAfectados.Text = DocumentosAfectados
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVentaAfecta.Text = Format(0 - .TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(0 - .TotalIva, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(0 - .TotalIva + 0 - Ventas.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(0 - .TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(0 - .TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", 0 - .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                    rpt.xrlCantLetras.Text = Num2Text(Int(0 - Ventas.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = .Comentario
                    rpt.PrinterName = Comprobantes.NombreImpresor
                    ' rpt.PrintDialog()
                    rpt.Print()
                End With
            Case 9 'nota de débito
                Dim Template = Application.StartupPath & "\Plantillas\ndNexus" & NumFormato & ".repx"

                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVentaAfecta.Text = Format(0 - .TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(0 - .TotalIva, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(0 - .TotalImpuesto1, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(0 - .TotalIva + Ventas.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(0 - .TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(0 - .TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", 0 - .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                    rpt.xrlCantLetras.Text = Num2Text(Int(0 - Ventas.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = .Comentario
                    rpt.PrinterName = Comprobantes.NombreImpresor
                    ' rpt.PrintDialog()
                    rpt.Print()
                End With
            Case 10 'tickette de caja se imprime a puro código
                MsgBox("No es posible re-imprimir tickete", MsgBoxStyle.Information, "Nota")
            Case 19 'NOTAS DE REMISION
                Dim Template = Application.StartupPath & "\Plantillas\neNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptOrdenEnvio
                rpt.LoadLayout(Template)

                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(.TotalIva, "###,##0.00")
                    rpt.xrlPedido.Text = .OrdenCompra
                    'rpt.xrlSubTotal.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalIva + .TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    rpt.xrlCantLetras.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = .Comentario

                    'rpt.ShowPreview()
                    rpt.PrinterName = Comprobantes.NombreImpresor
                    'rpt.PrintDialog()
                    rpt.Print()
                End With

        End Select

        Return True
    End Function

    Private Sub sbCambiaFormaPago_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCambiaFormaPago.Click
        Dim IdPed As Integer = gvConsultaPed.GetFocusedRowCellValue("IdComprobante")

        If IdPed = 0 Then
            MsgBox("Debe de seleccionar el pedido que requiere actualizar", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If Not gvConsultaPed.GetFocusedRowCellValue("Facturado") Then
            MsgBox(String.Format("Este pedido aún no se ha facturado"), MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        If MsgBox("¿Está seguro(a) de actualizar la forma de pago?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim dtFacturasPedidoAct As DataTable = blFac.fac_ObtenerVentasPedido(IdPed)
        Dim msj As String = ""

        'ACTUALIZO LAS FACTURAS Y EL PEDIDO CON LA NUEVA FORMA DE PAGO QUE SELECCIONEN
        For j = 0 To dtFacturasPedidoAct.Rows.Count - 1
            '*-- verifico si no hay abonos aplicados
            Dim sFecha = SiEsNulo(blFac.Verificar_Abonos(dtFacturasPedidoAct.Rows(j).Item("IdComprobante")), "")

            If sFecha <> "" Then
                MsgBox("Imposible anular éste documento. Ya tiene abonos aplicados" + Chr(13) + _
                "Fecha del abono: " + sFecha, 16, "Imposible Anular")
                msj = "error"
                Exit For
            End If

            '*-- verifico si no hay alguna nota de credito o debito que afecte al documento
            Dim sNumero_Nota = SiEsNulo(blFac.Verificar_Nota(dtFacturasPedidoAct.Rows(j).Item("IdComprobante")), "")
            If sNumero_Nota <> "" Then
                MsgBox("Debe de anular la nota de crédito que afecta a éste documento" & Chr(13) & _
                 "Número de la Nota de Crédito: " + sNumero_Nota, 16, "Imposible Anular")
                Exit For
            End If

            blFac.fac_ActualizaFormaPago(dtFacturasPedidoAct.Rows(j).Item("IdComprobante"), IdPed, leFormaPago.EditValue)
        Next
        If msj = "" Then
            MsgBox(String.Format("El pedido y factura han sido actualizados con  éxito"), MsgBoxStyle.Information, "Nota")
        End If
    End Sub

    Private Sub ObtieneCorrelativo()
        If leTipoDoc.EditValue <> 0 Then
            Dim NumeroComprobante As Integer = blFac.GetObtieneCorrelativoFacturacion(1, leTipoDoc.EditValue)
            teNumero.EditValue = NumeroComprobante.ToString.PadLeft(6, "0")

            Dim entTipos As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(leTipoDoc.EditValue)
            If dtPar.Rows(0).Item("UtilizarFormularioUnico") Then
                Dim NumeroUnico As Integer = blFac.GetObtieneCorrelativoFacturacionUnico(piIdSucursalUsuario, piIdSucursalUsuario)
                teNumeroUnico.EditValue = IIf(entTipos.FormularioUnico, NumeroUnico.ToString.PadLeft(6, "0"), "")
            Else
                teNumeroUnico.EditValue = ""
            End If

        End If
    End Sub

    Private Sub sbAplicarInventario_Click(sender As Object, e As EventArgs) Handles sbAplicarInventario.Click
        If MsgBox("¿Esta seguro(a) de aplicar al inventario el pedido, no se aplicara factura por el pedido?" + Chr(13) + "FAVOR CONFIRMAR", MsgBoxStyle.YesNo + MsgBoxStyle.Question) = MsgBoxResult.No Then
            Exit Sub
        End If


        Dim IdPedido As Integer = gvConsultaPed.GetFocusedRowCellValue("IdComprobante")

        If IdPedido = 0 Then
            MsgBox("Debe de seleccionar el pedido que requiere descargar del inventario", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim msj As String = ""
        msj = blFac.fac_InsertarKardexPedido(IdPedido)

        If msj = "Ok" Then
            MsgBox("EL pedido aplicado al inventario exitosamente", MsgBoxStyle.Information, "Nota")

            gcConsultaPed.DataSource = blFac.fac_ConsultaPedidos(objMenu.User, deDesde.EditValue, deHasta.EditValue, ceTipo.Checked)
            gvConsultaPed.BestFitColumns()
            gvConsultaPed_Click("", New EventArgs)
            Exit Sub
        Else
            MsgBox("No se pudo aplicar el pedido del kardex: " & msj, MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If


    End Sub

    Private Sub leTipoDoc_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leTipoDoc.EditValueChanged
        ObtieneCorrelativo()
    End Sub


End Class
