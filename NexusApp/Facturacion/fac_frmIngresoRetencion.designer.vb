﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmIngresoRetencion
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.seRetencion = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaContable = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.teComprobanteRetencion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaRetencion = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.teMotivo = New DevExpress.XtraEditors.TextEdit()
        Me.seTotal = New DevExpress.XtraEditors.SpinEdit()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.sbObtener = New DevExpress.XtraEditors.SimpleButton()
        Me.sbRetencion = New DevExpress.XtraEditors.SimpleButton()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.teSerie = New DevExpress.XtraEditors.TextEdit()
        Me.leTipo = New DevExpress.XtraEditors.LookUpEdit()
        Me.lePunto = New DevExpress.XtraEditors.LookUpEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.seRetencion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaContable.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaContable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teComprobanteRetencion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaRetencion.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaRetencion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teMotivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.SimpleButton1)
        Me.GroupControl1.Controls.Add(Me.seRetencion)
        Me.GroupControl1.Controls.Add(Me.LabelControl13)
        Me.GroupControl1.Controls.Add(Me.deFechaContable)
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Controls.Add(Me.teComprobanteRetencion)
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.deFechaRetencion)
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.teMotivo)
        Me.GroupControl1.Controls.Add(Me.seTotal)
        Me.GroupControl1.Controls.Add(Me.deFecha)
        Me.GroupControl1.Controls.Add(Me.sbObtener)
        Me.GroupControl1.Controls.Add(Me.sbRetencion)
        Me.GroupControl1.Controls.Add(Me.teNombre)
        Me.GroupControl1.Controls.Add(Me.teNumero)
        Me.GroupControl1.Controls.Add(Me.teSerie)
        Me.GroupControl1.Controls.Add(Me.leTipo)
        Me.GroupControl1.Controls.Add(Me.lePunto)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(680, 392)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Datos del documento que se desea anular"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(241, 92)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(28, 23)
        Me.SimpleButton1.TabIndex = 71
        Me.SimpleButton1.Text = "..."
        '
        'seRetencion
        '
        Me.seRetencion.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seRetencion.Location = New System.Drawing.Point(140, 207)
        Me.seRetencion.Name = "seRetencion"
        Me.seRetencion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seRetencion.Properties.Mask.EditMask = "c"
        Me.seRetencion.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seRetencion.Properties.ReadOnly = True
        Me.seRetencion.Size = New System.Drawing.Size(100, 20)
        Me.seRetencion.TabIndex = 8
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(85, 210)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl13.TabIndex = 18
        Me.LabelControl13.Text = "Retencion:"
        '
        'deFechaContable
        '
        Me.deFechaContable.EditValue = Nothing
        Me.deFechaContable.Location = New System.Drawing.Point(140, 279)
        Me.deFechaContable.Name = "deFechaContable"
        Me.deFechaContable.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaContable.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaContable.Size = New System.Drawing.Size(100, 20)
        Me.deFechaContable.TabIndex = 11
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(58, 282)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl11.TabIndex = 16
        Me.LabelControl11.Text = "Fecha Contable:"
        '
        'teComprobanteRetencion
        '
        Me.teComprobanteRetencion.Location = New System.Drawing.Point(140, 230)
        Me.teComprobanteRetencion.Name = "teComprobanteRetencion"
        Me.teComprobanteRetencion.Size = New System.Drawing.Size(100, 20)
        Me.teComprobanteRetencion.TabIndex = 9
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(17, 233)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl12.TabIndex = 14
        Me.LabelControl12.Text = "Comprobante Retención:"
        '
        'deFechaRetencion
        '
        Me.deFechaRetencion.EditValue = Nothing
        Me.deFechaRetencion.Location = New System.Drawing.Point(140, 254)
        Me.deFechaRetencion.Name = "deFechaRetencion"
        Me.deFechaRetencion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaRetencion.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaRetencion.Size = New System.Drawing.Size(100, 20)
        Me.deFechaRetencion.TabIndex = 10
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(36, 257)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(101, 13)
        Me.LabelControl10.TabIndex = 10
        Me.LabelControl10.Text = "Fecha Comprobante:"
        '
        'teMotivo
        '
        Me.teMotivo.Location = New System.Drawing.Point(140, 304)
        Me.teMotivo.Name = "teMotivo"
        Me.teMotivo.Size = New System.Drawing.Size(516, 20)
        Me.teMotivo.TabIndex = 12
        '
        'seTotal
        '
        Me.seTotal.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seTotal.Location = New System.Drawing.Point(140, 183)
        Me.seTotal.Name = "seTotal"
        Me.seTotal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seTotal.Properties.Mask.EditMask = "c"
        Me.seTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seTotal.Properties.ReadOnly = True
        Me.seTotal.Size = New System.Drawing.Size(100, 20)
        Me.seTotal.TabIndex = 7
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.Location = New System.Drawing.Point(140, 141)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.ReadOnly = True
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 6
        '
        'sbObtener
        '
        Me.sbObtener.Location = New System.Drawing.Point(241, 115)
        Me.sbObtener.Name = "sbObtener"
        Me.sbObtener.Size = New System.Drawing.Size(75, 23)
        Me.sbObtener.TabIndex = 5
        Me.sbObtener.Text = "Obtener..."
        '
        'sbRetencion
        '
        Me.sbRetencion.Location = New System.Drawing.Point(140, 339)
        Me.sbRetencion.Name = "sbRetencion"
        Me.sbRetencion.Size = New System.Drawing.Size(196, 32)
        Me.sbRetencion.TabIndex = 13
        Me.sbRetencion.Text = "&Ingreso Comprobante Retención..."
        '
        'teNombre
        '
        Me.teNombre.Location = New System.Drawing.Point(140, 162)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.ReadOnly = True
        Me.teNombre.Size = New System.Drawing.Size(516, 20)
        Me.teNombre.TabIndex = 6
        '
        'teNumero
        '
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(140, 115)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(100, 20)
        Me.teNumero.TabIndex = 4
        '
        'teSerie
        '
        Me.teSerie.Location = New System.Drawing.Point(140, 94)
        Me.teSerie.Name = "teSerie"
        Me.teSerie.Size = New System.Drawing.Size(100, 20)
        Me.teSerie.TabIndex = 3
        '
        'leTipo
        '
        Me.leTipo.Location = New System.Drawing.Point(140, 71)
        Me.leTipo.Name = "leTipo"
        Me.leTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipo.Size = New System.Drawing.Size(315, 20)
        Me.leTipo.TabIndex = 2
        '
        'lePunto
        '
        Me.lePunto.Location = New System.Drawing.Point(140, 50)
        Me.lePunto.Name = "lePunto"
        Me.lePunto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePunto.Size = New System.Drawing.Size(315, 20)
        Me.lePunto.TabIndex = 1
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(140, 29)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(315, 20)
        Me.leSucursal.TabIndex = 0
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(73, 307)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl9.TabIndex = 4
        Me.LabelControl9.Text = "Observación:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(101, 186)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl8.TabIndex = 4
        Me.LabelControl8.Text = "TOTAL:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(71, 165)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl7.TabIndex = 4
        Me.LabelControl7.Text = "A Nombre de:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(104, 144)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl6.TabIndex = 4
        Me.LabelControl6.Text = "Fecha:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(96, 118)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl5.TabIndex = 4
        Me.LabelControl5.Text = "Número:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(109, 97)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl4.TabIndex = 3
        Me.LabelControl4.Text = "Serie:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(113, 74)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Tipo:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(59, 54)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Punto de Venta:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(93, 32)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Sucursal:"
        '
        'fac_frmIngresoRetencion
        '
        Me.ClientSize = New System.Drawing.Size(680, 420)
        Me.Controls.Add(Me.GroupControl1)
        Me.Modulo = "Cuentas por Cobrar"
        Me.Name = "fac_frmIngresoRetencion"
        Me.OptionId = "002007"
        Me.Text = "Ingreso de Comprobante Retención"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.seRetencion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaContable.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaContable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teComprobanteRetencion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaRetencion.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaRetencion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teMotivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leTipo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lePunto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbRetencion As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbObtener As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents seTotal As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teMotivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teComprobanteRetencion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaRetencion As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaContable As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seRetencion As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton

End Class
