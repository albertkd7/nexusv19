﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmEmiteDocumentoDevolucion
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmEmiteDocumentoDevolucion))
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
        Me.sbOk = New DevExpress.XtraEditors.SimpleButton
        Me.teSaldo = New DevExpress.XtraEditors.TextEdit
        Me.lePuntoVenta = New DevExpress.XtraEditors.LookUpEdit
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.teTotal = New DevExpress.XtraEditors.TextEdit
        Me.teFecha = New DevExpress.XtraEditors.TextEdit
        Me.teSerie = New DevExpress.XtraEditors.TextEdit
        Me.leTipoComprobante = New DevExpress.XtraEditors.LookUpEdit
        Me.teNumero = New DevExpress.XtraEditors.TextEdit
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
        Me.layout00 = New DevExpress.XtraLayout.LayoutControlItem
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.teSaldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoComprobante.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layout00, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Appearance.DisabledLayoutGroupCaption.ForeColor = System.Drawing.SystemColors.GrayText
        Me.LayoutControl1.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = True
        Me.LayoutControl1.Appearance.DisabledLayoutItem.ForeColor = System.Drawing.SystemColors.GrayText
        Me.LayoutControl1.Appearance.DisabledLayoutItem.Options.UseForeColor = True
        Me.LayoutControl1.Controls.Add(Me.sbOk)
        Me.LayoutControl1.Controls.Add(Me.teSaldo)
        Me.LayoutControl1.Controls.Add(Me.lePuntoVenta)
        Me.LayoutControl1.Controls.Add(Me.leSucursal)
        Me.LayoutControl1.Controls.Add(Me.teTotal)
        Me.LayoutControl1.Controls.Add(Me.teFecha)
        Me.LayoutControl1.Controls.Add(Me.teSerie)
        Me.LayoutControl1.Controls.Add(Me.leTipoComprobante)
        Me.LayoutControl1.Controls.Add(Me.teNumero)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(605, 361)
        Me.LayoutControl1.TabIndex = 2
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'sbOk
        '
        Me.sbOk.Location = New System.Drawing.Point(567, 100)
        Me.sbOk.Name = "sbOk"
        Me.sbOk.Size = New System.Drawing.Size(32, 22)
        Me.sbOk.StyleController = Me.LayoutControl1
        Me.sbOk.TabIndex = 12
        Me.sbOk.Text = "..."
        Me.sbOk.ToolTip = "Buscar el documento seleccionado"
        Me.sbOk.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        Me.sbOk.ToolTipTitle = "Nota"
        '
        'teSaldo
        '
        Me.teSaldo.EditValue = ""
        Me.teSaldo.Location = New System.Drawing.Point(201, 195)
        Me.teSaldo.Name = "teSaldo"
        Me.teSaldo.Properties.Mask.EditMask = "n2"
        Me.teSaldo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teSaldo.Size = New System.Drawing.Size(398, 20)
        Me.teSaldo.StyleController = Me.LayoutControl1
        Me.teSaldo.TabIndex = 11
        '
        'lePuntoVenta
        '
        Me.lePuntoVenta.Location = New System.Drawing.Point(201, 38)
        Me.lePuntoVenta.Name = "lePuntoVenta"
        Me.lePuntoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePuntoVenta.Size = New System.Drawing.Size(398, 20)
        Me.lePuntoVenta.StyleController = Me.LayoutControl1
        Me.lePuntoVenta.TabIndex = 10
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(201, 7)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(398, 20)
        Me.leSucursal.StyleController = Me.LayoutControl1
        Me.leSucursal.TabIndex = 9
        '
        'teTotal
        '
        Me.teTotal.Location = New System.Drawing.Point(201, 164)
        Me.teTotal.Name = "teTotal"
        Me.teTotal.Properties.Mask.EditMask = "n2"
        Me.teTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotal.Size = New System.Drawing.Size(398, 20)
        Me.teTotal.StyleController = Me.LayoutControl1
        Me.teTotal.TabIndex = 8
        '
        'teFecha
        '
        Me.teFecha.Location = New System.Drawing.Point(201, 133)
        Me.teFecha.Name = "teFecha"
        Me.teFecha.Properties.Mask.EditMask = "d"
        Me.teFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.teFecha.Size = New System.Drawing.Size(398, 20)
        Me.teFecha.StyleController = Me.LayoutControl1
        Me.teFecha.TabIndex = 7
        '
        'teSerie
        '
        Me.teSerie.Location = New System.Drawing.Point(201, 100)
        Me.teSerie.Name = "teSerie"
        Me.teSerie.Size = New System.Drawing.Size(199, 20)
        Me.teSerie.StyleController = Me.LayoutControl1
        Me.teSerie.TabIndex = 5
        '
        'leTipoComprobante
        '
        Me.leTipoComprobante.Location = New System.Drawing.Point(201, 69)
        Me.leTipoComprobante.Name = "leTipoComprobante"
        Me.leTipoComprobante.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoComprobante.Size = New System.Drawing.Size(398, 20)
        Me.leTipoComprobante.StyleController = Me.LayoutControl1
        Me.leTipoComprobante.TabIndex = 4
        '
        'teNumero
        '
        Me.teNumero.Location = New System.Drawing.Point(411, 100)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(145, 20)
        Me.teNumero.StyleController = Me.LayoutControl1
        Me.teNumero.TabIndex = 6
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.layout00, Me.LayoutControlItem1, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem2, Me.LayoutControlItem7, Me.LayoutControlItem8})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(605, 361)
        Me.LayoutControlGroup1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'layout00
        '
        Me.layout00.Control = Me.leTipoComprobante
        Me.layout00.CustomizationFormText = "Tipo de Comprobante que se Afecta:"
        Me.layout00.Location = New System.Drawing.Point(0, 62)
        Me.layout00.Name = "layout00"
        Me.layout00.Size = New System.Drawing.Size(603, 31)
        Me.layout00.Text = "Tipo de Comprobante que se Afecta:"
        Me.layout00.TextLocation = DevExpress.Utils.Locations.Left
        Me.layout00.TextSize = New System.Drawing.Size(189, 13)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.teSerie
        Me.LayoutControlItem1.CustomizationFormText = "Serie y Número del documento:"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 93)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(404, 33)
        Me.LayoutControlItem1.Text = "Serie y Número del documento:"
        Me.LayoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(189, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.teFecha
        Me.LayoutControlItem3.CustomizationFormText = "Fecha del Documento:"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 126)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(603, 31)
        Me.LayoutControlItem3.Text = "Fecha del Documento:"
        Me.LayoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(189, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.teTotal
        Me.LayoutControlItem4.CustomizationFormText = "Total del Documento:"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 157)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(603, 31)
        Me.LayoutControlItem4.Text = "Total del Documento:"
        Me.LayoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(189, 13)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.leSucursal
        Me.LayoutControlItem5.CustomizationFormText = "Sucursal donde se emitió el documento:"
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(603, 31)
        Me.LayoutControlItem5.Text = "Sucursal donde se emitió el documento:"
        Me.LayoutControlItem5.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(189, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.lePuntoVenta
        Me.LayoutControlItem6.CustomizationFormText = "Punto de Venta:"
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 31)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(603, 31)
        Me.LayoutControlItem6.Text = "Punto de Venta:"
        Me.LayoutControlItem6.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(189, 13)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.teNumero
        Me.LayoutControlItem2.CustomizationFormText = "Número del Documento:"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(404, 93)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(156, 33)
        Me.LayoutControlItem2.Text = "Número del Documento:"
        Me.LayoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextToControlDistance = 0
        Me.LayoutControlItem2.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.teSaldo
        Me.LayoutControlItem7.CustomizationFormText = "Saldo Actual:"
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 188)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(603, 171)
        Me.LayoutControlItem7.Text = "Saldo Actual:"
        Me.LayoutControlItem7.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(189, 13)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.sbOk
        Me.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8"
        Me.LayoutControlItem8.Location = New System.Drawing.Point(560, 93)
        Me.LayoutControlItem8.MinSize = New System.Drawing.Size(36, 33)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(43, 33)
        Me.LayoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem8.Text = "LayoutControlItem8"
        Me.LayoutControlItem8.TextLocation = DevExpress.Utils.Locations.Left
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextToControlDistance = 0
        Me.LayoutControlItem8.TextVisible = False
        '
        'fac_frmEmiteDocumentoDevolucion
        '
        Me.ClientSize = New System.Drawing.Size(605, 361)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "fac_frmEmiteDocumentoDevolucion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Definir documento que afecta la devolución"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.teSaldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoComprobante.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layout00, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents leTipoComprobante As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents layout00 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents teTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teFecha As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents teSaldo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lePuntoVenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents sbOk As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem

End Class
