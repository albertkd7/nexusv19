﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmGestionClientes
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
		Me.deFecha = New DevExpress.XtraEditors.DateEdit()
		Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
		Me.teNombre = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
		Me.beCodCliente = New DevExpress.XtraEditors.ButtonEdit()
		Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
		Me.teGestion = New DevExpress.XtraEditors.TimeEdit()
		Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
		Me.teMotivoGestion = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
		Me.meComentarioGeneral = New DevExpress.XtraEditors.MemoEdit()
		Me.meComentarioCliente = New DevExpress.XtraEditors.MemoEdit()
		Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
		Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
		Me.deFechaProxima = New DevExpress.XtraEditors.DateEdit()
		Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
		Me.teVisita = New DevExpress.XtraEditors.TimeEdit()
		Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
		Me.deFechaVisita = New DevExpress.XtraEditors.DateEdit()
		Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
		Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton()
		Me.sbObtenerGestion = New DevExpress.XtraEditors.SimpleButton()
		Me.teIdGestion = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
		Me.teTelefonos = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
		Me.RepositoryItemLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
		Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
		Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
		Me.gc = New DevExpress.XtraGrid.GridControl()
		Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
		Me.gcCorrelativo = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
		Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
		Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
		Me.sbLlamaCotizacion = New DevExpress.XtraEditors.SimpleButton()
		Me.teNumCotizacion = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
		Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
		Me.teIdCotizacion = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
		Me.teNombreProspecto = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
		Me.beCodProspecto = New DevExpress.XtraEditors.ButtonEdit()
		CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupControl1.SuspendLayout()
		CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.beCodCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teGestion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teMotivoGestion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.meComentarioGeneral.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.meComentarioCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupControl2.SuspendLayout()
		CType(Me.deFechaProxima.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaProxima.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teVisita.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaVisita.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deFechaVisita.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teIdGestion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teNumCotizacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teIdCotizacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teNombreProspecto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.beCodProspecto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'GroupControl1
		'
		Me.GroupControl1.Controls.Add(Me.LabelControl9)
		Me.GroupControl1.Controls.Add(Me.teNombreProspecto)
		Me.GroupControl1.Controls.Add(Me.LabelControl10)
		Me.GroupControl1.Controls.Add(Me.beCodProspecto)
		Me.GroupControl1.Controls.Add(Me.sbLlamaCotizacion)
		Me.GroupControl1.Controls.Add(Me.teNumCotizacion)
		Me.GroupControl1.Controls.Add(Me.LabelControl15)
		Me.GroupControl1.Controls.Add(Me.LabelControl16)
		Me.GroupControl1.Controls.Add(Me.teIdCotizacion)
		Me.GroupControl1.Controls.Add(Me.teTelefonos)
		Me.GroupControl1.Controls.Add(Me.LabelControl12)
		Me.GroupControl1.Controls.Add(Me.LabelControl11)
		Me.GroupControl1.Controls.Add(Me.teIdGestion)
		Me.GroupControl1.Controls.Add(Me.sbObtenerGestion)
		Me.GroupControl1.Controls.Add(Me.sbGuardar)
		Me.GroupControl1.Controls.Add(Me.GroupControl2)
		Me.GroupControl1.Controls.Add(Me.LabelControl5)
		Me.GroupControl1.Controls.Add(Me.meComentarioCliente)
		Me.GroupControl1.Controls.Add(Me.meComentarioGeneral)
		Me.GroupControl1.Controls.Add(Me.LabelControl3)
		Me.GroupControl1.Controls.Add(Me.teMotivoGestion)
		Me.GroupControl1.Controls.Add(Me.LabelControl2)
		Me.GroupControl1.Controls.Add(Me.teGestion)
		Me.GroupControl1.Controls.Add(Me.LabelControl4)
		Me.GroupControl1.Controls.Add(Me.LabelControl20)
		Me.GroupControl1.Controls.Add(Me.teNombre)
		Me.GroupControl1.Controls.Add(Me.LabelControl22)
		Me.GroupControl1.Controls.Add(Me.beCodCliente)
		Me.GroupControl1.Controls.Add(Me.deFecha)
		Me.GroupControl1.Controls.Add(Me.LabelControl1)
		Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
		Me.GroupControl1.Size = New System.Drawing.Size(1379, 188)
		Me.GroupControl1.Text = "Parámetros de la consulta"
		'
		'LabelControl1
		'
		Me.LabelControl1.Location = New System.Drawing.Point(30, 69)
		Me.LabelControl1.Name = "LabelControl1"
		Me.LabelControl1.Size = New System.Drawing.Size(72, 13)
		Me.LabelControl1.TabIndex = 0
		Me.LabelControl1.Text = "Fecha Gestión:"
		'
		'deFecha
		'
		Me.deFecha.EditValue = Nothing
		Me.deFecha.EnterMoveNextControl = True
		Me.deFecha.Location = New System.Drawing.Point(106, 66)
		Me.deFecha.Name = "deFecha"
		Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
		Me.deFecha.Properties.ReadOnly = True
		Me.deFecha.Size = New System.Drawing.Size(115, 20)
		Me.deFecha.TabIndex = 2
		'
		'LabelControl20
		'
		Me.LabelControl20.Location = New System.Drawing.Point(250, 27)
		Me.LabelControl20.Name = "LabelControl20"
		Me.LabelControl20.Size = New System.Drawing.Size(41, 13)
		Me.LabelControl20.TabIndex = 85
		Me.LabelControl20.Text = "Nombre:"
		'
		'teNombre
		'
		Me.teNombre.EnterMoveNextControl = True
		Me.teNombre.Location = New System.Drawing.Point(292, 23)
		Me.teNombre.Name = "teNombre"
		Me.teNombre.Properties.ReadOnly = True
		Me.teNombre.Size = New System.Drawing.Size(570, 20)
		Me.teNombre.TabIndex = 1
		'
		'LabelControl22
		'
		Me.LabelControl22.Location = New System.Drawing.Point(39, 25)
		Me.LabelControl22.Name = "LabelControl22"
		Me.LabelControl22.Size = New System.Drawing.Size(63, 13)
		Me.LabelControl22.TabIndex = 86
		Me.LabelControl22.Text = "Cód. Cliente:"
		'
		'beCodCliente
		'
		Me.beCodCliente.EnterMoveNextControl = True
		Me.beCodCliente.Location = New System.Drawing.Point(106, 23)
		Me.beCodCliente.Name = "beCodCliente"
		Me.beCodCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.beCodCliente.Size = New System.Drawing.Size(115, 20)
		Me.beCodCliente.TabIndex = 0
		'
		'LabelControl4
		'
		Me.LabelControl4.Location = New System.Drawing.Point(225, 69)
		Me.LabelControl4.Name = "LabelControl4"
		Me.LabelControl4.Size = New System.Drawing.Size(66, 13)
		Me.LabelControl4.TabIndex = 87
		Me.LabelControl4.Text = "Hora Gestión:"
		'
		'teGestion
		'
		Me.teGestion.EditValue = New Date(2016, 7, 21, 0, 0, 0, 0)
		Me.teGestion.Location = New System.Drawing.Point(292, 66)
		Me.teGestion.Name = "teGestion"
		Me.teGestion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.teGestion.Properties.Mask.EditMask = "HH:mm tt"
		Me.teGestion.Properties.ReadOnly = True
		Me.teGestion.Size = New System.Drawing.Size(80, 20)
		Me.teGestion.TabIndex = 88
		'
		'LabelControl2
		'
		Me.LabelControl2.Location = New System.Drawing.Point(377, 69)
		Me.LabelControl2.Name = "LabelControl2"
		Me.LabelControl2.Size = New System.Drawing.Size(75, 13)
		Me.LabelControl2.TabIndex = 89
		Me.LabelControl2.Text = "Motivo Gestión:"
		'
		'teMotivoGestion
		'
		Me.teMotivoGestion.EnterMoveNextControl = True
		Me.teMotivoGestion.Location = New System.Drawing.Point(453, 66)
		Me.teMotivoGestion.Name = "teMotivoGestion"
		Me.teMotivoGestion.Size = New System.Drawing.Size(409, 20)
		Me.teMotivoGestion.TabIndex = 3
		'
		'LabelControl3
		'
		Me.LabelControl3.Location = New System.Drawing.Point(3, 113)
		Me.LabelControl3.Name = "LabelControl3"
		Me.LabelControl3.Size = New System.Drawing.Size(99, 13)
		Me.LabelControl3.TabIndex = 91
		Me.LabelControl3.Text = "Comentario General:"
		'
		'meComentarioGeneral
		'
		Me.meComentarioGeneral.EnterMoveNextControl = True
		Me.meComentarioGeneral.Location = New System.Drawing.Point(106, 108)
		Me.meComentarioGeneral.Name = "meComentarioGeneral"
		Me.meComentarioGeneral.Size = New System.Drawing.Size(756, 36)
		Me.meComentarioGeneral.TabIndex = 4
		'
		'meComentarioCliente
		'
		Me.meComentarioCliente.EnterMoveNextControl = True
		Me.meComentarioCliente.Location = New System.Drawing.Point(106, 146)
		Me.meComentarioCliente.Name = "meComentarioCliente"
		Me.meComentarioCliente.Size = New System.Drawing.Size(756, 36)
		Me.meComentarioCliente.TabIndex = 5
		'
		'LabelControl5
		'
		Me.LabelControl5.Location = New System.Drawing.Point(7, 147)
		Me.LabelControl5.Name = "LabelControl5"
		Me.LabelControl5.Size = New System.Drawing.Size(95, 13)
		Me.LabelControl5.TabIndex = 102
		Me.LabelControl5.Text = "Comentario Cliente:"
		'
		'GroupControl2
		'
		Me.GroupControl2.Controls.Add(Me.deFechaProxima)
		Me.GroupControl2.Controls.Add(Me.LabelControl8)
		Me.GroupControl2.Controls.Add(Me.teVisita)
		Me.GroupControl2.Controls.Add(Me.LabelControl7)
		Me.GroupControl2.Controls.Add(Me.deFechaVisita)
		Me.GroupControl2.Controls.Add(Me.LabelControl6)
		Me.GroupControl2.Location = New System.Drawing.Point(864, 23)
		Me.GroupControl2.Name = "GroupControl2"
		Me.GroupControl2.Size = New System.Drawing.Size(184, 87)
		Me.GroupControl2.TabIndex = 103
		Me.GroupControl2.Text = "Próximos Eventos"
		'
		'deFechaProxima
		'
		Me.deFechaProxima.EditValue = Nothing
		Me.deFechaProxima.EnterMoveNextControl = True
		Me.deFechaProxima.Location = New System.Drawing.Point(85, 23)
		Me.deFechaProxima.Name = "deFechaProxima"
		Me.deFechaProxima.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.deFechaProxima.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.deFechaProxima.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
		Me.deFechaProxima.Size = New System.Drawing.Size(97, 20)
		Me.deFechaProxima.TabIndex = 0
		'
		'LabelControl8
		'
		Me.LabelControl8.Location = New System.Drawing.Point(4, 26)
		Me.LabelControl8.Name = "LabelControl8"
		Me.LabelControl8.Size = New System.Drawing.Size(81, 13)
		Me.LabelControl8.TabIndex = 92
		Me.LabelControl8.Text = "Próxima Gestión:"
		'
		'teVisita
		'
		Me.teVisita.EditValue = New Date(2016, 7, 21, 0, 0, 0, 0)
		Me.teVisita.Location = New System.Drawing.Point(85, 67)
		Me.teVisita.Name = "teVisita"
		Me.teVisita.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.teVisita.Properties.Mask.EditMask = "HH:mm tt"
		Me.teVisita.Size = New System.Drawing.Size(97, 20)
		Me.teVisita.TabIndex = 2
		'
		'LabelControl7
		'
		Me.LabelControl7.Location = New System.Drawing.Point(15, 70)
		Me.LabelControl7.Name = "LabelControl7"
		Me.LabelControl7.Size = New System.Drawing.Size(70, 13)
		Me.LabelControl7.TabIndex = 89
		Me.LabelControl7.Text = "Hora de Visita:"
		'
		'deFechaVisita
		'
		Me.deFechaVisita.EditValue = Nothing
		Me.deFechaVisita.EnterMoveNextControl = True
		Me.deFechaVisita.Location = New System.Drawing.Point(85, 44)
		Me.deFechaVisita.Name = "deFechaVisita"
		Me.deFechaVisita.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.deFechaVisita.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.deFechaVisita.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
		Me.deFechaVisita.Size = New System.Drawing.Size(97, 20)
		Me.deFechaVisita.TabIndex = 1
		'
		'LabelControl6
		'
		Me.LabelControl6.Location = New System.Drawing.Point(9, 47)
		Me.LabelControl6.Name = "LabelControl6"
		Me.LabelControl6.Size = New System.Drawing.Size(76, 13)
		Me.LabelControl6.TabIndex = 2
		Me.LabelControl6.Text = "Fecha de Visita:"
		'
		'sbGuardar
		'
		Me.sbGuardar.Image = Global.Nexus.My.Resources.Resources.Save
		Me.sbGuardar.Location = New System.Drawing.Point(864, 132)
		Me.sbGuardar.Name = "sbGuardar"
		Me.sbGuardar.Size = New System.Drawing.Size(182, 27)
		Me.sbGuardar.TabIndex = 104
		Me.sbGuardar.Text = "Guardar"
		'
		'sbObtenerGestion
		'
		Me.sbObtenerGestion.Location = New System.Drawing.Point(864, 112)
		Me.sbObtenerGestion.Name = "sbObtenerGestion"
		Me.sbObtenerGestion.Size = New System.Drawing.Size(182, 18)
		Me.sbObtenerGestion.TabIndex = 110
		Me.sbObtenerGestion.Text = "Obtener Gestión Programada"
		'
		'teIdGestion
		'
		Me.teIdGestion.Enabled = False
		Me.teIdGestion.EnterMoveNextControl = True
		Me.teIdGestion.Location = New System.Drawing.Point(1133, 136)
		Me.teIdGestion.Name = "teIdGestion"
		Me.teIdGestion.Properties.ReadOnly = True
		Me.teIdGestion.Size = New System.Drawing.Size(116, 20)
		Me.teIdGestion.TabIndex = 111
		'
		'LabelControl11
		'
		Me.LabelControl11.Location = New System.Drawing.Point(1081, 142)
		Me.LabelControl11.Name = "LabelControl11"
		Me.LabelControl11.Size = New System.Drawing.Size(50, 13)
		Me.LabelControl11.TabIndex = 112
		Me.LabelControl11.Text = "IdGestión:"
		'
		'teTelefonos
		'
		Me.teTelefonos.EnterMoveNextControl = True
		Me.teTelefonos.Location = New System.Drawing.Point(106, 87)
		Me.teTelefonos.Name = "teTelefonos"
		Me.teTelefonos.Properties.ReadOnly = True
		Me.teTelefonos.Size = New System.Drawing.Size(266, 20)
		Me.teTelefonos.TabIndex = 113
		'
		'LabelControl12
		'
		Me.LabelControl12.Location = New System.Drawing.Point(51, 90)
		Me.LabelControl12.Name = "LabelControl12"
		Me.LabelControl12.Size = New System.Drawing.Size(51, 13)
		Me.LabelControl12.TabIndex = 114
		Me.LabelControl12.Text = "Teléfonos:"
		'
		'RepositoryItemLookUpEdit1
		'
		Me.RepositoryItemLookUpEdit1.AutoHeight = False
		Me.RepositoryItemLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.RepositoryItemLookUpEdit1.Name = "RepositoryItemLookUpEdit1"
		'
		'GridView3
		'
		Me.GridView3.Name = "GridView3"
		'
		'RepositoryItemTextEdit1
		'
		Me.RepositoryItemTextEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
		Me.RepositoryItemTextEdit1.AutoHeight = False
		Me.RepositoryItemTextEdit1.Mask.EditMask = "n6"
		Me.RepositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
		Me.RepositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = True
		Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
		'
		'gc
		'
		Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
		Me.gc.Location = New System.Drawing.Point(0, 188)
		Me.gc.MainView = Me.gv
		Me.gc.Name = "gc"
		Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit1, Me.RepositoryItemTimeEdit2})
		Me.gc.Size = New System.Drawing.Size(1379, 330)
		Me.gc.TabIndex = 7
		Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv, Me.GridView2})
		'
		'gv
		'
		Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcCorrelativo, Me.GridColumn12, Me.GridColumn11, Me.GridColumn2, Me.GridColumn3, Me.GridColumn9, Me.GridColumn4, Me.GridColumn5, Me.GridColumn13, Me.GridColumn14, Me.GridColumn6, Me.GridColumn7, Me.GridColumn1, Me.GridColumn8})
		Me.gv.GridControl = Me.gc
		Me.gv.Name = "gv"
		Me.gv.OptionsBehavior.Editable = False
		Me.gv.OptionsView.ShowAutoFilterRow = True
		Me.gv.OptionsView.ShowGroupPanel = False
		'
		'gcCorrelativo
		'
		Me.gcCorrelativo.Caption = "Correlativo"
		Me.gcCorrelativo.FieldName = "IdGestion"
		Me.gcCorrelativo.Name = "gcCorrelativo"
		Me.gcCorrelativo.Visible = True
		Me.gcCorrelativo.VisibleIndex = 0
		'
		'GridColumn12
		'
		Me.GridColumn12.Caption = "Gestión Progra."
		Me.GridColumn12.FieldName = "GestionProgramada"
		Me.GridColumn12.Name = "GridColumn12"
		Me.GridColumn12.Visible = True
		Me.GridColumn12.VisibleIndex = 1
		'
		'GridColumn11
		'
		Me.GridColumn11.Caption = "Cliente"
		Me.GridColumn11.FieldName = "NombreCliente"
		Me.GridColumn11.Name = "GridColumn11"
		Me.GridColumn11.Visible = True
		Me.GridColumn11.VisibleIndex = 2
		'
		'GridColumn2
		'
		Me.GridColumn2.Caption = "Fecha"
		Me.GridColumn2.FieldName = "Fecha"
		Me.GridColumn2.Name = "GridColumn2"
		Me.GridColumn2.Visible = True
		Me.GridColumn2.VisibleIndex = 3
		'
		'GridColumn3
		'
		Me.GridColumn3.Caption = "Hora de Llamada"
		Me.GridColumn3.ColumnEdit = Me.RepositoryItemTimeEdit1
		Me.GridColumn3.FieldName = "HoraLlamada"
		Me.GridColumn3.Name = "GridColumn3"
		Me.GridColumn3.Visible = True
		Me.GridColumn3.VisibleIndex = 4
		'
		'RepositoryItemTimeEdit1
		'
		Me.RepositoryItemTimeEdit1.AutoHeight = False
		Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.RepositoryItemTimeEdit1.EditFormat.FormatString = "HH:mm tt"
		Me.RepositoryItemTimeEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
		Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm tt"
		Me.RepositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = True
		Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
		'
		'GridColumn9
		'
		Me.GridColumn9.Caption = "Motivo"
		Me.GridColumn9.FieldName = "Motivo"
		Me.GridColumn9.Name = "GridColumn9"
		Me.GridColumn9.Visible = True
		Me.GridColumn9.VisibleIndex = 5
		'
		'GridColumn4
		'
		Me.GridColumn4.Caption = "Comentario Gral."
		Me.GridColumn4.FieldName = "ComentarioGeneral"
		Me.GridColumn4.Name = "GridColumn4"
		Me.GridColumn4.Visible = True
		Me.GridColumn4.VisibleIndex = 6
		'
		'GridColumn5
		'
		Me.GridColumn5.Caption = "Próxima Gestion"
		Me.GridColumn5.FieldName = "ProximaGestion"
		Me.GridColumn5.Name = "GridColumn5"
		Me.GridColumn5.Visible = True
		Me.GridColumn5.VisibleIndex = 7
		'
		'GridColumn13
		'
		Me.GridColumn13.Caption = "IdCotizacion"
		Me.GridColumn13.FieldName = "IdCotizacion"
		Me.GridColumn13.Name = "GridColumn13"
		Me.GridColumn13.Visible = True
		Me.GridColumn13.VisibleIndex = 8
		'
		'GridColumn14
		'
		Me.GridColumn14.Caption = "No. Cotización"
		Me.GridColumn14.FieldName = "NumCotizacion"
		Me.GridColumn14.Name = "GridColumn14"
		Me.GridColumn14.Visible = True
		Me.GridColumn14.VisibleIndex = 9
		'
		'GridColumn6
		'
		Me.GridColumn6.Caption = "FechaVisita"
		Me.GridColumn6.ColumnEdit = Me.RepositoryItemTimeEdit2
		Me.GridColumn6.FieldName = "FechaVisita"
		Me.GridColumn6.Name = "GridColumn6"
		Me.GridColumn6.Visible = True
		Me.GridColumn6.VisibleIndex = 10
		'
		'RepositoryItemTimeEdit2
		'
		Me.RepositoryItemTimeEdit2.AutoHeight = False
		Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.RepositoryItemTimeEdit2.EditFormat.FormatString = " dd/MM/yyyy hh:mm:ss"
		Me.RepositoryItemTimeEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
		Me.RepositoryItemTimeEdit2.Mask.EditMask = " dd/MM/yyyy"
		Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
		Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
		'
		'GridColumn7
		'
		Me.GridColumn7.Caption = "Hora Visita"
		Me.GridColumn7.ColumnEdit = Me.RepositoryItemTimeEdit1
		Me.GridColumn7.FieldName = "HoraVisita"
		Me.GridColumn7.Name = "GridColumn7"
		Me.GridColumn7.Visible = True
		Me.GridColumn7.VisibleIndex = 11
		'
		'GridColumn1
		'
		Me.GridColumn1.Caption = "CreadoPor"
		Me.GridColumn1.FieldName = "CreadoPor"
		Me.GridColumn1.Name = "GridColumn1"
		Me.GridColumn1.Visible = True
		Me.GridColumn1.VisibleIndex = 12
		'
		'GridColumn8
		'
		Me.GridColumn8.Caption = "Fecha Creación"
		Me.GridColumn8.ColumnEdit = Me.RepositoryItemTimeEdit2
		Me.GridColumn8.FieldName = "FechaHoraCreacion"
		Me.GridColumn8.Name = "GridColumn8"
		Me.GridColumn8.Visible = True
		Me.GridColumn8.VisibleIndex = 13
		'
		'GridView2
		'
		Me.GridView2.GridControl = Me.gc
		Me.GridView2.Name = "GridView2"
		'
		'sbLlamaCotizacion
		'
		Me.sbLlamaCotizacion.AllowFocus = False
		Me.sbLlamaCotizacion.Location = New System.Drawing.Point(1256, 36)
		Me.sbLlamaCotizacion.Name = "sbLlamaCotizacion"
		Me.sbLlamaCotizacion.Size = New System.Drawing.Size(25, 22)
		Me.sbLlamaCotizacion.TabIndex = 121
		Me.sbLlamaCotizacion.Text = "..."
		'
		'teNumCotizacion
		'
		Me.teNumCotizacion.EnterMoveNextControl = True
		Me.teNumCotizacion.Location = New System.Drawing.Point(1133, 58)
		Me.teNumCotizacion.Name = "teNumCotizacion"
		Me.teNumCotizacion.Properties.ReadOnly = True
		Me.teNumCotizacion.Size = New System.Drawing.Size(116, 20)
		Me.teNumCotizacion.TabIndex = 118
		'
		'LabelControl15
		'
		Me.LabelControl15.Location = New System.Drawing.Point(1057, 63)
		Me.LabelControl15.Name = "LabelControl15"
		Me.LabelControl15.Size = New System.Drawing.Size(73, 13)
		Me.LabelControl15.TabIndex = 120
		Me.LabelControl15.Text = "No. Cotización:"
		'
		'LabelControl16
		'
		Me.LabelControl16.Location = New System.Drawing.Point(1064, 41)
		Me.LabelControl16.Name = "LabelControl16"
		Me.LabelControl16.Size = New System.Drawing.Size(66, 13)
		Me.LabelControl16.TabIndex = 119
		Me.LabelControl16.Text = "Id Cotización:"
		'
		'teIdCotizacion
		'
		Me.teIdCotizacion.EnterMoveNextControl = True
		Me.teIdCotizacion.Location = New System.Drawing.Point(1133, 37)
		Me.teIdCotizacion.Name = "teIdCotizacion"
		Me.teIdCotizacion.Properties.ReadOnly = True
		Me.teIdCotizacion.Size = New System.Drawing.Size(116, 20)
		Me.teIdCotizacion.TabIndex = 117
		'
		'LabelControl9
		'
		Me.LabelControl9.Location = New System.Drawing.Point(250, 48)
		Me.LabelControl9.Name = "LabelControl9"
		Me.LabelControl9.Size = New System.Drawing.Size(41, 13)
		Me.LabelControl9.TabIndex = 124
		Me.LabelControl9.Text = "Nombre:"
		'
		'teNombreProspecto
		'
		Me.teNombreProspecto.EnterMoveNextControl = True
		Me.teNombreProspecto.Location = New System.Drawing.Point(292, 44)
		Me.teNombreProspecto.Name = "teNombreProspecto"
		Me.teNombreProspecto.Properties.ReadOnly = True
		Me.teNombreProspecto.Size = New System.Drawing.Size(570, 20)
		Me.teNombreProspecto.TabIndex = 123
		'
		'LabelControl10
		'
		Me.LabelControl10.Location = New System.Drawing.Point(24, 46)
		Me.LabelControl10.Name = "LabelControl10"
		Me.LabelControl10.Size = New System.Drawing.Size(78, 13)
		Me.LabelControl10.TabIndex = 125
		Me.LabelControl10.Text = "Cód. Prospecto:"
		'
		'beCodProspecto
		'
		Me.beCodProspecto.EnterMoveNextControl = True
		Me.beCodProspecto.Location = New System.Drawing.Point(106, 44)
		Me.beCodProspecto.Name = "beCodProspecto"
		Me.beCodProspecto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.beCodProspecto.Properties.Mask.EditMask = "n0"
		Me.beCodProspecto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
		Me.beCodProspecto.Size = New System.Drawing.Size(115, 20)
		Me.beCodProspecto.TabIndex = 122
		'
		'fac_frmGestionClientes
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.ClientSize = New System.Drawing.Size(1379, 543)
		Me.Controls.Add(Me.gc)
		Me.Modulo = "Facturación"
		Me.Name = "fac_frmGestionClientes"
		Me.Text = "Gestiones de Clientes"
		Me.Controls.SetChildIndex(Me.GroupControl1, 0)
		Me.Controls.SetChildIndex(Me.gc, 0)
		CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupControl1.ResumeLayout(False)
		Me.GroupControl1.PerformLayout()
		CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.beCodCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teGestion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teMotivoGestion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.meComentarioGeneral.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.meComentarioCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupControl2.ResumeLayout(False)
		Me.GroupControl2.PerformLayout()
		CType(Me.deFechaProxima.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaProxima.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teVisita.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaVisita.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deFechaVisita.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teIdGestion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teNumCotizacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teIdCotizacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teNombreProspecto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.beCodProspecto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCodCliente As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teMotivoGestion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teGestion As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meComentarioCliente As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents meComentarioGeneral As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents deFechaProxima As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teVisita As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaVisita As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbObtenerGestion As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdGestion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teTelefonos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RepositoryItemLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcCorrelativo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents sbLlamaCotizacion As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teNumCotizacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdCotizacion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents teNombreProspecto As DevExpress.XtraEditors.TextEdit
	Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents beCodProspecto As DevExpress.XtraEditors.ButtonEdit
End Class
