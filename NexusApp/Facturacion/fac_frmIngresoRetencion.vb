﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmIngresoRetencion
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim entFactura As New fac_Ventas
    Dim CreaDocumento As Integer = 0
    Dim entRetencion As New fac_VentasRetencion


    Private Sub fac_frmIngresoRetencion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePunto, leSucursal.EditValue, "")
        leSucursal.EditValue = piIdSucursal
        objCombos.fac_TiposComprobante(leTipo, "")
        sbRetencion.Enabled = False
    End Sub

    Private Sub sbObtener_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbObtener.Click
        CreaDocumento = 0
        Dim IdComprobante As Integer = bl.getIdComprobante(leSucursal.EditValue, lePunto.EditValue, leTipo.EditValue, teSerie.EditValue, teNumero.EditValue)
        If IdComprobante = 0 Then
            MsgBox("No se ha emitido ningún documento con ese número", 64, "Nota")
        End If

        If CreaDocumento = 0 Then
            deFecha.Properties.ReadOnly = True
            entFactura = objTablas.fac_VentasSelectByPK(IdComprobante)
            deFecha.EditValue = entFactura.Fecha
            teNombre.EditValue = entFactura.Nombre
            seTotal.EditValue = entFactura.TotalComprobante
            seRetencion.EditValue = entFactura.TotalImpuesto1

            Dim entR As New fac_VentasRetencion
            entR = objTablas.fac_VentasRetencionSelectByPK(entFactura.IdComprobante)
            If entR.Numero = "" Then
                teComprobanteRetencion.EditValue = ""
                deFechaRetencion.EditValue = Today
                deFechaContable.EditValue = Today
                teMotivo.EditValue = ""
            Else
                teComprobanteRetencion.EditValue = entR.Numero
                deFechaRetencion.EditValue = entR.Fecha
                teMotivo.EditValue = entR.Observacion
                deFechaContable.EditValue = entR.FechaContable
            End If

            sbRetencion.Enabled = True
        Else
            deFecha.Properties.ReadOnly = False
            deFecha.EditValue = entFactura.Fecha
            teNombre.EditValue = entFactura.Nombre
            seTotal.EditValue = entFactura.TotalComprobante
            sbRetencion.Enabled = True
        End If

    End Sub

    Private Sub sbRetencion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbRetencion.Click
        sbRetencion.Enabled = False
      
        'If teComprobanteRetencion.EditValue = "" Then
        '    MsgBox("Debe de especificar el numero de Comprobante Retención", 16, "Nota")
        '    Exit Sub
        'End If

        Dim EsOk As Boolean = ValidarFechaCierre(deFechaContable.EditValue)
        If Not EsOk Then
            MsgBox("No puede crear el documento" & _
                   "La fecha corresponde a un período ya cerrado", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        '*-- Mensaje de confirmación para ingresar comprobante
        If entFactura.TotalImpuesto1 = 0.0 Then
            MsgBox("Comprobante no posee retención", 16, "Imposible ingresar")
            sbRetencion.Enabled = False
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de Ingresar el Comprobante de Retención?  " + teNumero.EditValue, 292, "Confirme") = MsgBoxResult.No Then
            sbRetencion.Enabled = True
            Exit Sub
        End If

        Dim msj As String = ""
        Dim entR As New fac_VentasRetencion
        entFactura.Fecha = deFecha.EditValue

        entR = objTablas.fac_VentasRetencionSelectByPK(entFactura.IdComprobante)

        entRetencion = New fac_VentasRetencion

        entRetencion.IdComprobante = entFactura.IdComprobante
        entRetencion.Numero = teComprobanteRetencion.EditValue
        entRetencion.Fecha = deFechaRetencion.EditValue
        entRetencion.FechaContable = deFechaContable.EditValue
        entRetencion.Observacion = teMotivo.Text
        entRetencion.CreadoPor = objMenu.User
        entRetencion.FechaHoraCreacion = Today


        If entR.IdComprobante > 0 Then
            objTablas.fac_VentasRetencionUpdate(entRetencion)
        Else
            objTablas.fac_VentasRetencionInsert(entRetencion)
        End If

        MsgBox("El Comprobante retención ha sido Aplicado", MsgBoxStyle.Information, "Nota")


        teNumero.EditValue = ""
        teNombre.EditValue = ""
        seTotal.EditValue = 0.0
        teMotivo.EditValue = ""
        deFechaRetencion.EditValue = Today
        teComprobanteRetencion.EditValue = ""
        deFecha.Properties.ReadOnly = True
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePunto, leSucursal.EditValue)
        CargaSerie()
    End Sub
    Private Sub CargaSerie()
        teSerie.EditValue = bl.fac_ObtenerSerieDocumento(leSucursal.EditValue, lePunto.EditValue, leTipo.EditValue)
    End Sub

    Private Sub lePunto_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lePunto.EditValueChanged
        CargaSerie()
    End Sub

    Private Sub leTipo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leTipo.EditValueChanged
        CargaSerie()
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        teSerie.EditValue = objConsultas.cnsSeries(frmConsultas, leTipo.EditValue)
    End Sub
End Class
