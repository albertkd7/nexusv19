﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class fac_frmAprobacionVentaCredito
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Dim entUsu As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub fac_frmAprobacionCotizacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        gc.DataSource = bl.fac_ConsultaFacturasCredito(Today, Today)
        gv.BestFitColumns()
    End Sub


    Private Sub btAprobar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAprobar.Click
        Dim IdDoc As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
        If IdDoc = 0 Then
            MsgBox("Es necesario selecionar una Venta ", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If MsgBox("¿Está seguro(a) de aprobar la Venta al Credito?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            bl.fac_AprobarFacturaCredito(IdDoc, 1, Now, objMenu.User)
            gc.DataSource = bl.fac_ConsultaFacturasCredito(deDesde.EditValue, deHasta.EditValue)
            gv.BestFitColumns()
        End If

    End Sub

    'Private Sub sbReprobar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbReprobar.Click
    '    Dim IdDoc As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
    '    If IdDoc = 0 Then
    '        MsgBox("Es necesario selecionar una Cotización", MsgBoxStyle.Information, "Nota")
    '        Exit Sub
    '    End If
    '    blFac.fac_ActualizaEstadoCotizacion(IdDoc, 3)
    '    gc.DataSource = blFac.fac_ConsultaCotizaciones(deDesde.EditValue, deHasta.EditValue, leEstado.EditValue)
    '    gv.BestFitColumns()
    'End Sub

    Private Sub btDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDetalle.Click
        Dim IdDoc As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
        ReImprimeDocumento(IdDoc)
    End Sub
    Private Function ReImprimeDocumento(ByVal IdDoc As Integer) As Boolean
        Dim Ventas As fac_Ventas, Vendedores As fac_Vendedores, Departamentos As adm_Departamentos, Municipios As adm_Municipios, FormaPago As fac_FormasPago, Sucursales As adm_Sucursales
        Ventas = objTablas.fac_VentasSelectByPK(IdDoc)
        Vendedores = objTablas.fac_VendedoresSelectByPK(Ventas.IdVendedor)
        Departamentos = objTablas.adm_DepartamentosSelectByPK(Ventas.IdDepartamento)
        Municipios = objTablas.adm_MunicipiosSelectByPK(Ventas.IdMunicipio)
        FormaPago = objTablas.fac_FormasPagoSelectByPK(Ventas.IdFormaPago)
        Sucursales = objTablas.adm_SucursalesSelectByPK(Ventas.IdSucursal)
        Dim dt As DataTable = bl.fac_ObtenerDetalleDocumento("fac_VentasDetalle", IdDoc)
        Dim dtAfectaNc As DataTable = bl.fac_ObtenerAfectaNC(IdDoc) ' para saber a que documentos afecto
        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)

        Select Case Ventas.IdTipoComprobante
            Case 5 'crédito fiscal
                Dim Template = Application.StartupPath & "\Plantillas\cfNexus" & NumFormato & ".repx"

                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNumRemision.Text = .Telefono
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlDiasCredito.Text = .DiasCredito
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(.TotalIva, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlCESC.Text = Format(.TotalImpuesto2, "##,###.00")
                    rpt.xrlSubTotal.Text = Format(.TotalIva + Ventas.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                    rpt.xrlCantLetras.Text = Num2Text(Int(Ventas.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPreviewDialog()
                End With
            Case 6 'consumidor final
                Dim Template = Application.StartupPath & "\Plantillas\faNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFactura
                rpt.LoadLayout(Template)

                rpt.DataSource = dt
                rpt.DataMember = ""
                rpt.XrSubreport1.ReportSource.DataSource = dt
                rpt.XrSubreport1.ReportSource.DataMember = ""

                rpt.XrSubreport2.ReportSource.DataSource = dt
                rpt.XrSubreport2.ReportSource.DataMember = ""

                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNumRemision.Text = .Telefono
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDiasCredito.Text = .DiasCredito
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlCESC.Text = Format(.TotalImpuesto2, "##,###.00")
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalAfecto - .TotalImpuesto1, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    Dim CantidaLetras As String = Num2Text(Int(IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))) & Decimales & " DÓLARES"
                    rpt.xrlCantLetras.Text = CantidaLetras
                    rpt.xrlComentario.Text = .Comentario
                    '2 Formato
                    rpt.xrlCodigo2.Text = .IdCliente
                    rpt.xrlNombre2.Text = .Nombre
                    rpt.xrlNumero2.Text = .Numero
                    rpt.xrlNumFormulario2.Text = .NumFormularioUnico
                    rpt.xrlNumRemision2.Text = .Telefono
                    rpt.xrlFecha2.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion2.Text = Now
                    rpt.xrlSucursal2.Text = Sucursales.Nombre
                    rpt.xrlNit2.Text = .Nit
                    rpt.xrlPedido2.Text = .OrdenCompra
                    rpt.xrlNrc2.Text = .Nrc
                    rpt.xrlGiro2.Text = .Giro
                    rpt.xrlDireccion2.Text = .Direccion
                    rpt.xrlTelefono2.Text = .Telefono
                    rpt.xrlMunic2.Text = Municipios.Nombre
                    rpt.xrlDepto2.Text = Departamentos.Nombre
                    rpt.xrlFormaPago2.Text = FormaPago.Nombre
                    rpt.xrlVendedor2.Text = Vendedores.Nombre
                    rpt.xrlVentaACuenta2.Text = .VentaAcuentaDe
                    rpt.xrlDiasCredito2.Text = .DiasCredito
                    rpt.xrlVentaAfecta2.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal2.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta2.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlIvaRetenido2.Text = Format(.TotalImpuesto1, "##,###.00")
                    rpt.xrlCESC2.Text = Format(.TotalImpuesto2, "##,###.00")
                    rpt.xrlTotal2.Text = Format(.TotalComprobante, "###,##0.00")
                    rpt.xrlCantLetras2.Text = CantidaLetras
                    rpt.xrlComentario2.Text = .Comentario

                    rpt.ShowPrintMarginsWarning = False
                    rpt.ShowPreviewDialog()
                End With
            Case 7 'factura de exportación
                Dim Template = Application.StartupPath & "\Plantillas\feNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFactura
                rpt.LoadLayout(Template)

                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumRemision.Text = .Telefono
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    Dim CantidaLetras As String = Num2Text(Int(IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))) & Decimales & " DÓLARES"
                    rpt.xrlCantLetras.Text = CantidaLetras
                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPrintMarginsWarning = False
                    rpt.ShowPreviewDialog()
                End With
            Case 8 'nota de crédito
                Dim Template = Application.StartupPath & "\Plantillas\ncNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)

                For i = 0 To dt.Rows.Count - 1
                    dt.Rows(i)("Cantidad") = 0 - dt.Rows(i)("Cantidad")
                    dt.Rows(i)("PrecioUnitario") = 0 - dt.Rows(i)("PrecioUnitario")
                    dt.Rows(i)("VentaExenta") = 0 - dt.Rows(i)("VentaExenta")
                    dt.Rows(i)("VentaNoSujeta") = 0 - dt.Rows(i)("VentaNoSujeta")
                    dt.Rows(i)("VentaNeta") = 0 - dt.Rows(i)("VentaNeta")
                    dt.Rows(i)("VentaAfecta") = 0 - dt.Rows(i)("VentaAfecta")
                Next

                Dim DocumentosAfectados As String = ""
                For j = 0 To dtAfectaNc.Rows.Count - 1
                    DocumentosAfectados += dtAfectaNc.Rows(j)("NumComprobanteVenta") + ", "
                Next

                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlNumRemision.Text = .Telefono
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlDocAfectados.Text = DocumentosAfectados
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVentaAfecta.Text = Format(0 - .TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(0 - .TotalIva, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(0 - .TotalIva + 0 - Ventas.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(0 - .TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(0 - .TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", 0 - .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                    rpt.xrlCantLetras.Text = Num2Text(Int(0 - Ventas.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPreviewDialog()
                End With
            Case 9 'nota de débito
                Dim Template = Application.StartupPath & "\Plantillas\ndNexus" & NumFormato & ".repx"

                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFiscal
                rpt.LoadLayout(Template)
                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumRemision.Text = .Telefono
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlFechaImpresion.Text = Now
                    rpt.xrlSucursal.Text = Sucursales.Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(.TotalIva, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalIva + Ventas.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

                    rpt.xrlCantLetras.Text = Num2Text(Int(Ventas.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPreviewDialog()
                End With
            Case 10 'tickette de caja se imprime a puro código
                MsgBox("No es posible re-imprimir tickete", MsgBoxStyle.Information, "Nota")
            Case 19 'tickette de caja se imprime a puro código
                Dim Template = Application.StartupPath & "\Plantillas\neNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptOrdenEnvio
                Dim entSucursalRecibe = objTablas.adm_SucursalesSelectByPK(Ventas.IdSucursalEnvio)
                Dim entSucursalEnvia = objTablas.adm_SucursalesSelectByPK(piIdSucursalUsuario)
                rpt.LoadLayout(Template)

                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlDia.Text = CDate(.Fecha).Day.ToString()
                    rpt.xrlMes.Text = ObtieneMesString(CDate(.Fecha).Month)
                    rpt.xrlAnio.Text = CDate(.Fecha).Year.ToString()
                    rpt.xrlTotal2.Text = Format(.TotalComprobante, "###,##0.00")
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlNumFormulario.Text = .NumFormularioUnico
                    rpt.xrlSucursalEnvia.Text = entSucursalEnvia.Nombre
                    rpt.xrlSucursalRecibe.Text = entSucursalRecibe.Nombre
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlIva.Text = Format(.TotalIva, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalIva + .TotalAfecto, "###,##0.00") 'Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", .TotalComprobante)

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    rpt.xrlCantLetras.Text = Num2Text(Int(.TotalComprobante)) & Decimales & " DÓLARES"
                    rpt.xrlComentario.Text = .Comentario

                    'rpt.ShowPreview()
                    rpt.ShowPreviewDialog()
                End With
            Case 24 'consumidor final formato pequeño
                Dim Template = Application.StartupPath & "\Plantillas\fpNexus" & NumFormato & ".repx"
                If Not FileIO.FileSystem.FileExists(Template) Then
                    MsgBox("No existe la plantilla necesaria para el documento", MsgBoxStyle.Critical, "Nota")
                    Exit Function
                End If
                Dim rpt As New fac_rptFactura
                rpt.LoadLayout(Template)

                rpt.DataSource = dt
                rpt.DataMember = ""
                With Ventas
                    rpt.xrlCodigo.Text = .IdCliente
                    rpt.xrlNombre.Text = .Nombre
                    rpt.xrlNumero.Text = .Numero
                    rpt.xrlFecha.Text = Format(.Fecha, "dd/MM/yyyy")
                    rpt.xrlNit.Text = .Nit
                    rpt.xrlNrc.Text = .Nrc
                    rpt.xrlGiro.Text = .Giro
                    rpt.xrlDireccion.Text = .Direccion
                    rpt.xrlTelefono.Text = .Telefono
                    rpt.xrlMunic.Text = Municipios.Nombre
                    rpt.xrlDiasCredito.Text = .DiasCredito
                    rpt.xrlDepto.Text = Departamentos.Nombre
                    rpt.xrlFormaPago.Text = FormaPago.Nombre
                    rpt.xrlVendedor.Text = Vendedores.Nombre
                    rpt.xrlPedido.Text = .OrdenCompra
                    rpt.xrlIvaRetenido.Text = Format(.TotalImpuesto1, "###,##0.00")
                    rpt.xrlVentaAfecta.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlSubTotal.Text = Format(.TotalAfecto, "###,##0.00")
                    rpt.xrlVentaExenta.Text = Format(.TotalExento, "###,##0.00")
                    rpt.xrlVentaACuenta.Text = .VentaAcuentaDe
                    rpt.xrlTotal.Text = Format(.TotalComprobante, "###,##0.00")
                    Dim Decimales = String.Format("{0:c}", IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))

                    Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
                    Dim CantidaLetras As String = Num2Text(Int(IIf(.TotalComprobante < 0.0, 0 - .TotalComprobante, .TotalComprobante))) & Decimales & " DÓLARES"
                    rpt.xrlCantLetras.Text = CantidaLetras
                    rpt.xrlComentario.Text = .Comentario
                    rpt.ShowPrintMarginsWarning = False
                    rpt.ShowPreviewDialog()
                End With
        End Select

        Return True
    End Function

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbReprobar.Click
        Dim IdDoc As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
        If IdDoc = 0 Then
            MsgBox("Es necesario selecionar una Orden de Compra", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de reprobar la Orden de Compra?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Dim Comentario As String = InputBox("Comentario:", "Reprobar Orden Compra", "")

            bl.fac_AprobarFacturaCredito(IdDoc, 0, Comentario, objMenu.User)
            gc.DataSource = bl.fac_ConsultaFacturasCredito(deDesde.EditValue, deHasta.EditValue)
            gv.BestFitColumns()
        End If

    End Sub

    Private Sub sbObtener_Click(sender As Object, e As EventArgs) Handles sbObtener.Click
        gc.DataSource = bl.fac_ConsultaFacturasCredito(deDesde.EditValue, deHasta.EditValue)
        gv.BestFitColumns()
    End Sub
End Class
