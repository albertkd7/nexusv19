Imports NexusBLL, NexusELL.TableEntities
Public Class fac_frmProductosConDescuento
    Private _Aceptado As Boolean
    Private _IdClientePed As String
    Private _NombreCliente As String

    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim blInventa As New InventarioBLL(g_ConnectionString), blFact As New FacturaBLL(g_ConnectionString)
    Dim DesdeFechaEstado As Date, FechaIniEstado As String = ""
    Private Sub fac_frmProductosConDescuento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
    End Sub

    Public Property Aceptado() As Boolean
        Get
            Return _Aceptado
        End Get
        Set(ByVal value As Boolean)
            _Aceptado = value
        End Set
    End Property
    Public Property IdClientePed() As String
        Get
            Return _IdClientePed
        End Get
        Set(ByVal value As String)
            _IdClientePed = value
        End Set
    End Property
    Public Property NombreCliente() As String
        Get
            Return _NombreCliente
        End Get
        Set(ByVal value As String)
            _NombreCliente = value
        End Set
    End Property

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim provider As New Security.Cryptography.SHA1CryptoServiceProvider
        Dim bytes As Byte() = System.Text.Encoding.UTF8.GetBytes(tePassword.Text)
        Dim inArray As Byte() = provider.ComputeHash(bytes)
        provider.Clear()
        'encripto el password
        Dim PassWord As String = Convert.ToBase64String(inArray)

        Aceptado = blAdmon.ValidarUsuario(teUsuario.EditValue, PassWord) = "Ok"
        If Not Aceptado Then
            MsgBox("No fue posible obtener la autorizaci�n" & "No coinciden las credenciales del usuario", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        Dim entUsuarioAutoriza As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(teUsuario.EditValue)
        'PENDIENTE DE CONFIGURAR QUE CADA LINEA CUMPLA CON EL RANGO AUTORIZADO
        'If porcDescuento > entUsuarioAutoriza.HastaDescuentos Then
        '    MsgBox("El Usuario no puede autorizar descuento mayor al establecido ", MsgBoxStyle.Critical, "Nota")
        '    Aceptado = False
        '    Exit Sub
        'End If
        If Not entUsuarioAutoriza.CambiarPrecios Then
            MsgBox("El Usuario ingresado no est� configurado para autorizar descuentos", MsgBoxStyle.Critical, "Nota")
            Aceptado = False
            Exit Sub
        End If
        fac_frmPedidos.AutorizadoPor = teUsuario.EditValue
        Close()
    End Sub
    
    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        gcProdDesc.ShowPrintPreview()
    End Sub
    
    Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
        _Aceptado = False
        Close()
    End Sub

End Class