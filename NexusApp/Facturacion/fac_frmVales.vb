﻿Imports NexusELL.TableEntities
Imports NexusBLL

Public Class fac_frmVales
    Dim entVale As fac_Vales
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blAmon As New AdmonBLL(g_ConnectionString)
    Dim fd As New FuncionesBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAmon.ObtieneParametros()
    Dim IdDoc As Integer



    Private Sub ban_frmCuentasBancarias_Editar() Handles Me.Editar
        ActivarControles(True)
    End Sub

    Private Sub ban_frmCuentasBancarias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "")
        IdDoc = objFunciones.ObtenerUltimoId("FAC_VALES", "IdComprobante")
        ActivarControles(False)
        gc2.DataSource = bl.fac_ConsultaVales(objMenu.User)
    End Sub

    Private Sub ban_frmCuentasBancarias_Nuevo_Click() Handles Me.Nuevo

        entVale = New fac_Vales
        teNumero.EditValue = ""
        leSucursal.EditValue = piIdSucursalUsuario

        deFecha.EditValue = Today
        teValor.EditValue = 0.0
        meConcepto.EditValue = ""
        teNombre.EditValue = ""
        ActivarControles(True)
        xtcVales.SelectedTabPage = xtpDatos
    End Sub
    Private Sub fac_frmVales_Edit_Click() Handles Me.Editar
        ActivarControles(True)
        teNumero.Focus()
    End Sub
    Private Sub ban_frmCuentasBancarias_Guardar() Handles Me.Guardar
        CargaEntidad()
        If DbMode = DbModeType.insert Then
            entVale.IdComprobante = objFunciones.ObtenerUltimoId("FAC_VALES", "IdComprobante") + 1
            Dim UltimoNumero As Integer = 0
            UltimoNumero = fd.CorrelativosInv("FAC_VALES")
            entVale.NumeroComprobante = UltimoNumero.ToString.PadLeft(6, "0")

            objTablas.fac_ValesInsert(entVale)
            teNumero.EditValue = entVale.NumeroComprobante
            teCorrelativo.EditValue = entVale.IdComprobante
        Else
            objTablas.fac_ValesUpdate(entVale)
        End If

        xtcVales.SelectedTabPage = xtpLista
        gc2.DataSource = bl.fac_ConsultaVales(objMenu.User)
        gc2.Focus()

        MostrarModoInicial()
        ActivarControles(False)
    End Sub

    Private Sub ban_frmCuentasBancarias_Eliminar() Handles Me.Eliminar
        entVale = objTablas.fac_ValesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Return
        End If

        objTablas.fac_ValesDeleteByPK(entVale.IdComprobante)

        xtcVales.SelectedTabPage = xtpLista
        gc2.DataSource = bl.fac_ConsultaVales(objMenu.User)
        gc2.Focus()
    End Sub
    Private Sub ban_frmCuentasBancarias_Reporte() Handles Me.Reporte
        entVale = objTablas.fac_ValesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))

        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)
        Dim rpt As New fac_rptVales

        Dim Template = Application.StartupPath & "\Plantillas\ValeNexus" & NumFormato & ".repx"
        If FileIO.FileSystem.FileExists(Template) Then
            'SI EXISTE PLANTILLA CARGARLA AL REPORTE
            rpt.LoadLayout(Template)
        End If
        rpt.DataSource = bl.fac_DetalleVale(entVale.IdComprobante)
        rpt.DataMember = ""

        Dim sDecimal As String = ""
        sDecimal = String.Format("{0:c}", entVale.Valor)
        sDecimal = sDecimal.Substring(sDecimal.Length - 2) & "/100"
        Dim Valor As String = "********************" + Format(entVale.Valor, "###,###,##0.00")
        Valor = Valor.Substring(Valor.Length - 15, 15)

        Dim Sucursales As adm_Sucursales = objTablas.adm_SucursalesSelectByPK(entVale.IdSucursal)
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlNumero.Text = entVale.NumeroComprobante
        teCorrelativo.Text = entVale.IdComprobante
        rpt.xrlNombre.Text = entVale.Nombre
        rpt.xrlSucursal.Text = Sucursales.Nombre
        rpt.xrlConcepto.Text = entVale.Concepto
        rpt.xrlCantidadLetras.Text = Num2Text(Int(entVale.Valor)) + " " + sDecimal
        rpt.xrlFecha.Text = dtParam.Rows(0).Item("Domicilio") + ", " + FechaToString(entVale.Fecha, entVale.Fecha)
        rpt.xrlValor.Text = Format(entVale.Valor, "##,###,###.00")
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub CargaEntidad()
        With entVale
            .IdComprobante = teCorrelativo.EditValue
            .IdSucursal = leSucursal.EditValue
            .IdPunto = lePuntoVenta.EditValue
            .Fecha = deFecha.EditValue
            .Nombre = teNombre.EditValue
            .Concepto = meConcepto.EditValue
            .NumeroComprobante = teNumero.EditValue
            .Valor = teValor.EditValue

            If DbMode = DbModeType.insert Then
                .FechaHoraCreacion = Now
                .CreadoPor = objMenu.User
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If
        End With
    End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next

    End Sub

    Private Sub fac_frmVales_Revertir() Handles Me.Revertir
        xtcVales.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "")
    End Sub

    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        entVale = objTablas.fac_ValesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        DbMode = DbModeType.update
        teNumero.Focus()
    End Sub
    Private Sub CargaPantalla()
        xtcVales.SelectedTabPage = xtpDatos
        teNumero.Focus()

        With entVale
            teCorrelativo.EditValue = .IdComprobante
            teNumero.EditValue = .NumeroComprobante
            leSucursal.EditValue = .IdSucursal
            deFecha.EditValue = SiEsNulo(.Fecha, Today)
            teValor.EditValue = .Valor
            meConcepto.EditValue = .Concepto
            teNombre.EditValue = .Nombre
            lePuntoVenta.EditValue = .IdPunto
        End With
    End Sub
End Class