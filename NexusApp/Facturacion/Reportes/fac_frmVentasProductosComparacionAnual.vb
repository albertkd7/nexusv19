﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmVentasProductosComparacionAnual
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim dt As New DataTable


    Private Sub fac_frmVentasProductosComparacionAnual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargaCombos()
        seEjercicio.EditValue = Today.Year
        seEjercicio2.EditValue = Today.Year
    End Sub
    Private Sub CargaCombos()
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS --")
        objCombos.fac_PuntosVenta(lePuntoVenta, 1, "-- TODOS --")
    End Sub

    Private Sub sbAceptar_Click() Handles Me.Reporte
        dt = bl.fac_VentasProductosComparacionAnual(seEjercicio.EditValue, seEjercicio2.EditValue, BeProducto1.beCodigo.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)
        Dim rpt As New fac_rptVentasProductosComparacionAnual() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, SUCURSAL: {1}, PUNTO DE VENTA: {2}", teTitulo.EditValue, leSucursal.Text, lePuntoVenta.Text)
        rpt.xrlPeriodo.Text = "AÑOS COMPARADOS:  " + seEjercicio.EditValue.ToString + " CON " + seEjercicio2.EditValue.ToString
        rpt.xrlValor1.Text = "AÑO:  " + seEjercicio.EditValue.ToString
        rpt.xrlValor2.Text = "AÑO:  " + seEjercicio2.EditValue.ToString
        rpt.xrlDiferencia.Text = "DIFERENCIAS  (+/-)"
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub

End Class
