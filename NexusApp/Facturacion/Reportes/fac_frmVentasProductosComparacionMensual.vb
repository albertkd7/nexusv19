﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmVentasProductosComparacionMensual
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim dt As New DataTable


    Private Sub fac_frmVentasProductosComparacionAnual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargaCombos()
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        meMes1.Month = Mes
        seAnio1.EditValue = Ejercicio

        meMes2.Month = Mes
        seAnio2.EditValue = Ejercicio - 1
    End Sub
    Private Sub CargaCombos()
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS --")
        objCombos.fac_PuntosVenta(lePuntoVenta, 1, "-- TODOS --")
    End Sub

    Private Sub sbAceptar_Click() Handles Me.Reporte
        dt = bl.fac_VentasProductosComparacionMensual(seAnio1.EditValue, meMes1.EditValue, seAnio2.EditValue, meMes2.EditValue, BeProducto1.beCodigo.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)
        Dim rpt As New fac_rptVentasProductosComparacionMensual() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, SUCURSAL: {1}, PUNTO DE VENTA: {2}", teTitulo.EditValue, leSucursal.Text, lePuntoVenta.Text)
        rpt.xrlPeriodo.Text = "AÑOS COMPARADOS:  " + ObtieneMesString(meMes1.EditValue) + " " + seAnio1.EditValue.ToString + " CON " + ObtieneMesString(meMes2.EditValue) + " " + seAnio2.EditValue.ToString
        rpt.xrlValor1.Text = "AÑO:  " + seAnio1.EditValue.ToString
        rpt.xrlMes1.Text = ObtieneMesString(meMes1.EditValue)
        rpt.xrlValor2.Text = "AÑO:  " + seAnio2.EditValue.ToString
        rpt.xrlMes2.Text = ObtieneMesString(meMes2.EditValue)
        rpt.xrlDiferencia.Text = "DIFERENCIAS  (+/-)"
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub

End Class
