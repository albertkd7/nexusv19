﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class fac_rptPlanFacturaDetalle
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Descripcion = New DevExpress.XtraReports.UI.XRLabel()
        Me.VentaExenta = New DevExpress.XtraReports.UI.XRLabel()
        Me.PrecioUnitario = New DevExpress.XtraReports.UI.XRLabel()
        Me.Cantidad = New DevExpress.XtraReports.UI.XRLabel()
        Me.VentaAfecta = New DevExpress.XtraReports.UI.XRLabel()
        Me.IdProducto = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.DsFactura1 = New Nexus.dsFactura()
        CType(Me.DsFactura1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel1, Me.Descripcion, Me.VentaExenta, Me.PrecioUnitario, Me.Cantidad, Me.VentaAfecta, Me.IdProducto})
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 12.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "VentasDetalle.VentaNoSujeta", "{0:n2}")})
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(560.6516!, 0.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(58.8902!, 12.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "VentasDetalle.PorcDescuento", "{0:n2}")})
        Me.XrLabel1.Dpi = 100.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(720.3333!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(42.70831!, 12.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        Me.XrLabel1.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "VentasDetalle.Descripcion")})
        Me.Descripcion.Dpi = 100.0!
        Me.Descripcion.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripcion.LocationFloat = New DevExpress.Utils.PointFloat(99.08337!, 0.0!)
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.Descripcion.SizeF = New System.Drawing.SizeF(394.8183!, 12.0!)
        Me.Descripcion.StylePriority.UseFont = False
        Me.Descripcion.StylePriority.UseTextAlignment = False
        Me.Descripcion.Text = "Descripcion"
        Me.Descripcion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'VentaExenta
        '
        Me.VentaExenta.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "VentasDetalle.VentaExenta", "{0:n2}")})
        Me.VentaExenta.Dpi = 100.0!
        Me.VentaExenta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VentaExenta.LocationFloat = New DevExpress.Utils.PointFloat(620.4585!, 0.0!)
        Me.VentaExenta.Name = "VentaExenta"
        Me.VentaExenta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.VentaExenta.SizeF = New System.Drawing.SizeF(63.74994!, 12.0!)
        Me.VentaExenta.StylePriority.UseFont = False
        Me.VentaExenta.StylePriority.UseTextAlignment = False
        Me.VentaExenta.Text = "VentaExenta"
        Me.VentaExenta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'PrecioUnitario
        '
        Me.PrecioUnitario.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "VentasDetalle.PrecioUnitario", "{0:n2}")})
        Me.PrecioUnitario.Dpi = 100.0!
        Me.PrecioUnitario.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrecioUnitario.LocationFloat = New DevExpress.Utils.PointFloat(494.9017!, 0.0!)
        Me.PrecioUnitario.Name = "PrecioUnitario"
        Me.PrecioUnitario.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.PrecioUnitario.SizeF = New System.Drawing.SizeF(68.74991!, 12.0!)
        Me.PrecioUnitario.StylePriority.UseFont = False
        Me.PrecioUnitario.StylePriority.UseTextAlignment = False
        Me.PrecioUnitario.Text = "PrecioUnitario"
        Me.PrecioUnitario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'Cantidad
        '
        Me.Cantidad.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "VentasDetalle.Cantidad", "{0:n2}")})
        Me.Cantidad.Dpi = 100.0!
        Me.Cantidad.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cantidad.LocationFloat = New DevExpress.Utils.PointFloat(5.0!, 0.0!)
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.Cantidad.SizeF = New System.Drawing.SizeF(64.04166!, 12.0!)
        Me.Cantidad.StylePriority.UseFont = False
        Me.Cantidad.StylePriority.UseTextAlignment = False
        Me.Cantidad.Text = "Cantidad"
        Me.Cantidad.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'VentaAfecta
        '
        Me.VentaAfecta.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "VentasDetalle.VentaAfecta", "{0:n2}")})
        Me.VentaAfecta.Dpi = 100.0!
        Me.VentaAfecta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VentaAfecta.LocationFloat = New DevExpress.Utils.PointFloat(696.2084!, 0.0!)
        Me.VentaAfecta.Name = "VentaAfecta"
        Me.VentaAfecta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.VentaAfecta.SizeF = New System.Drawing.SizeF(63.79156!, 12.0!)
        Me.VentaAfecta.StylePriority.UseFont = False
        Me.VentaAfecta.StylePriority.UseTextAlignment = False
        Me.VentaAfecta.Text = "VentaAfecta"
        Me.VentaAfecta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'IdProducto
        '
        Me.IdProducto.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "VentasDetalle.IdProducto")})
        Me.IdProducto.Dpi = 100.0!
        Me.IdProducto.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IdProducto.LocationFloat = New DevExpress.Utils.PointFloat(78.70832!, 0.0!)
        Me.IdProducto.Name = "IdProducto"
        Me.IdProducto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.IdProducto.SizeF = New System.Drawing.SizeF(121.125!, 12.0!)
        Me.IdProducto.StylePriority.UseFont = False
        Me.IdProducto.StylePriority.UseTextAlignment = False
        Me.IdProducto.Text = "IdProducto"
        Me.IdProducto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        Me.IdProducto.Visible = False
        '
        'TopMargin
        '
        Me.TopMargin.Dpi = 100.0!
        Me.TopMargin.HeightF = 0.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 100.0!
        Me.BottomMargin.HeightF = 0.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'DsFactura1
        '
        Me.DsFactura1.DataSetName = "dsFactura"
        Me.DsFactura1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'fac_rptPlanFacturaDetalle
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin})
        Me.DataMember = "VentasDetalle"
        Me.DataSource = Me.DsFactura1
        Me.Margins = New System.Drawing.Printing.Margins(40, 40, 0, 0)
        Me.SnapToGrid = False
        Me.Version = "16.1"
        CType(Me.DsFactura1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents DsFactura1 As Nexus.dsFactura
    Friend WithEvents PrecioUnitario As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Cantidad As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Descripcion As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents IdProducto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents VentaAfecta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents VentaExenta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
End Class
