Public Class fac_rptPedido
    Private nTotal As Decimal
    Private Sub xrlTotal_SummaryRowChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles xrlTotal.SummaryRowChanged
        nTotal += GetCurrentColumnValue("PrecioTotal")
    End Sub

    Private Sub xrlTotal_SummaryGetResult(ByVal sender As System.Object, ByVal e As DevExpress.XtraReports.UI.SummaryGetResultEventArgs) Handles xrlTotal.SummaryGetResult
        e.Result = nTotal
        e.Handled = True
    End Sub
End Class