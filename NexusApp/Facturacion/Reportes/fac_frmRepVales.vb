﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class fac_frmRepVales
    Dim bl As New FacturaBLL(g_ConnectionString)
    Private Sub fac_rptVentasPorComprobante_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CargaCombos()
        deDesde.EditValue = CDate(String.Format("1/{0}/{1}", Month(Today), Year(Today)))
        deHasta.EditValue = Today
    End Sub


    Private Sub CargaCombos()
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        objCombos.fac_PuntosVenta(lePuntoVenta, 1, "-- TODOS --")
    End Sub

    Private Sub fac_rptVentasPorComprobante_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.fac_RepVales(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)

        Dim rpt As New fac_rptRepVales With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, PUNTO DE VENTA: {1}", teTitulo.EditValue, lePuntoVenta.Text)
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub
End Class
