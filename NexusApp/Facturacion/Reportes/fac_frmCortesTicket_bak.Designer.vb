﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmCortesTicket_bak
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup
        Me.lblCorteX = New DevExpress.XtraEditors.LabelControl
        Me.deCorteX = New DevExpress.XtraEditors.DateEdit
        Me.lblCorteZ = New DevExpress.XtraEditors.LabelControl
        Me.lblCorteTotal = New DevExpress.XtraEditors.LabelControl
        Me.deCorteZ = New DevExpress.XtraEditors.DateEdit
        Me.deGranTotal = New DevExpress.XtraEditors.DateEdit
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit
        Me.lePuntoVenta = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.cmdImprimirCinta = New DevExpress.XtraEditors.SimpleButton
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deCorteX.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deCorteX.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deCorteZ.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deCorteZ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deGranTotal.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deGranTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.cmdImprimirCinta)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.deCorteZ)
        Me.GroupControl1.Controls.Add(Me.lePuntoVenta)
        Me.GroupControl1.Controls.Add(Me.deGranTotal)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.deCorteX)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.RadioGroup1)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.lblCorteTotal)
        Me.GroupControl1.Controls.Add(Me.lblCorteZ)
        Me.GroupControl1.Controls.Add(Me.lblCorteX)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Size = New System.Drawing.Size(617, 321)
        Me.GroupControl1.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(14, 38)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Tipo de Corte a Generar:"
        '
        'RadioGroup1
        '
        Me.RadioGroup1.EditValue = 0
        Me.RadioGroup1.Location = New System.Drawing.Point(137, 30)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Corte X"), New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Corte Z"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Gran Total Z")})
        Me.RadioGroup1.Size = New System.Drawing.Size(101, 99)
        Me.RadioGroup1.TabIndex = 0
        '
        'lblCorteX
        '
        Me.lblCorteX.Location = New System.Drawing.Point(244, 41)
        Me.lblCorteX.Name = "lblCorteX"
        Me.lblCorteX.Size = New System.Drawing.Size(107, 13)
        Me.lblCorteX.TabIndex = 0
        Me.lblCorteX.Text = "Fecha/Hora del Corte:"
        '
        'deCorteX
        '
        Me.deCorteX.EditValue = Nothing
        Me.deCorteX.Location = New System.Drawing.Point(354, 38)
        Me.deCorteX.Name = "deCorteX"
        Me.deCorteX.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deCorteX.Properties.Mask.EditMask = "G"
        Me.deCorteX.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.deCorteX.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deCorteX.Size = New System.Drawing.Size(205, 20)
        Me.deCorteX.TabIndex = 1
        '
        'lblCorteZ
        '
        Me.lblCorteZ.Location = New System.Drawing.Point(281, 73)
        Me.lblCorteZ.Name = "lblCorteZ"
        Me.lblCorteZ.Size = New System.Drawing.Size(70, 13)
        Me.lblCorteZ.TabIndex = 0
        Me.lblCorteZ.Text = "Día a Generar:"
        Me.lblCorteZ.Visible = False
        '
        'lblCorteTotal
        '
        Me.lblCorteTotal.Location = New System.Drawing.Point(276, 107)
        Me.lblCorteTotal.Name = "lblCorteTotal"
        Me.lblCorteTotal.Size = New System.Drawing.Size(75, 13)
        Me.lblCorteTotal.TabIndex = 0
        Me.lblCorteTotal.Text = "Mes A Generar:"
        Me.lblCorteTotal.Visible = False
        '
        'deCorteZ
        '
        Me.deCorteZ.EditValue = Nothing
        Me.deCorteZ.Location = New System.Drawing.Point(354, 70)
        Me.deCorteZ.Name = "deCorteZ"
        Me.deCorteZ.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deCorteZ.Properties.Mask.EditMask = "G"
        Me.deCorteZ.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deCorteZ.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.deCorteZ.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deCorteZ.Size = New System.Drawing.Size(205, 20)
        Me.deCorteZ.TabIndex = 2
        Me.deCorteZ.Visible = False
        '
        'deGranTotal
        '
        Me.deGranTotal.EditValue = Nothing
        Me.deGranTotal.Location = New System.Drawing.Point(354, 104)
        Me.deGranTotal.Name = "deGranTotal"
        Me.deGranTotal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deGranTotal.Properties.Mask.EditMask = "y"
        Me.deGranTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.deGranTotal.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deGranTotal.Size = New System.Drawing.Size(125, 20)
        Me.deGranTotal.TabIndex = 3
        Me.deGranTotal.Visible = False
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "CORTE X"
        Me.teTitulo.Location = New System.Drawing.Point(137, 184)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(422, 20)
        Me.teTitulo.TabIndex = 6
        '
        'lePuntoVenta
        '
        Me.lePuntoVenta.Location = New System.Drawing.Point(137, 158)
        Me.lePuntoVenta.Name = "lePuntoVenta"
        Me.lePuntoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePuntoVenta.Size = New System.Drawing.Size(422, 20)
        Me.lePuntoVenta.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(48, 187)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl5.TabIndex = 21
        Me.LabelControl5.Text = "Título del informe:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(56, 161)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl6.TabIndex = 22
        Me.LabelControl6.Text = "Punto de Venta:"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(137, 135)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(422, 20)
        Me.leSucursal.TabIndex = 4
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(90, 138)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl7.TabIndex = 20
        Me.LabelControl7.Text = "Sucursal:"
        '
        'cmdImprimirCinta
        '
        Me.cmdImprimirCinta.Location = New System.Drawing.Point(137, 225)
        Me.cmdImprimirCinta.Name = "cmdImprimirCinta"
        Me.cmdImprimirCinta.Size = New System.Drawing.Size(321, 34)
        Me.cmdImprimirCinta.TabIndex = 23
        Me.cmdImprimirCinta.Text = "Imprimir datos fiscales para terminación de cinta de auditoría"
        '
        'fac_frmCortesTicket
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(617, 346)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmCortesTicket"
        Me.Text = "Imprimir Corte X, Corte Z y Gran Total Z"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deCorteX.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deCorteX.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deCorteZ.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deCorteZ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deGranTotal.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deGranTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deCorteX As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCorteX As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCorteZ As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deCorteZ As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lblCorteTotal As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deGranTotal As DevExpress.XtraEditors.DateEdit
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lePuntoVenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cmdImprimirCinta As DevExpress.XtraEditors.SimpleButton

End Class
