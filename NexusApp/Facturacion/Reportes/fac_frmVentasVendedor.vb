﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmVentasVendedor
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)


    Private Sub frmFacReportes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = CDate("1/" & Month(Today) & "/" & Year(Today))
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
        objCombos.fac_Vendedores(leVendedor, "-- TODOS --")

        If (EntUsuario.IdVendedor > 0) Then
            leVendedor.EditValue = EntUsuario.IdVendedor
            leVendedor.Enabled = False
        End If
    End Sub

    Private Sub sbAceptar_Click() Handles Me.Reporte

        Dim dt As DataTable = bl.fac_VentasVendedor(deDesde.EditValue, deHasta.EditValue, leVendedor.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, ceDetallado.EditValue)

        If ceDetallado.EditValue Then
            Dim rpt As New fac_rptVentasVendedor
            rpt.DataSource = dt
            rpt.DataMember = ""
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = String.Format("{0}, PUNTO DE VENTA: {1}", teTitulo.EditValue, lePuntoVenta.Text)
            rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
            rpt.ShowPreviewDialog()
        Else
            Dim rpt As New fac_rptVentasVendedorConsolidado
            rpt.DataSource = dt
            rpt.DataMember = ""
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = String.Format("{0}, PUNTO DE VENTA: {1}", teTitulo.EditValue, lePuntoVenta.Text)
            rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
            rpt.ShowPreviewDialog()
        End If
    End Sub



End Class
