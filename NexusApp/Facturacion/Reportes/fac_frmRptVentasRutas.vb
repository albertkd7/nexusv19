﻿Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraSplashScreen
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmRptVentasRutas
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blInventa As New InventarioBLL(g_ConnectionString)
    Dim Prod As DataTable, ProdVista As DataTable
    Dim dt As New DataTable


    Private Sub fac_frmConsultaProductosBase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        gvFormaPagoXYZ.Columns.Clear()
    End Sub


    Private Sub btGenerar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGenerar.Click

        SplashScreenManager.ShowForm(Me, WaitForm.GetType(), True, True, False)
        SplashScreenManager.Default.SetWaitFormCaption("Procesando")
        SplashScreenManager.Default.SetWaitFormDescription("Consultado Formas de Pago...")

        Dim dt As DataTable
        dt = bl.fac_RptConsusltaFormasPago(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        gvFormaPagoXYZ.Columns.Clear()
        gcFormaPago.DataSource = dt
        ' DE QUI HACIA ABAJO FUNCIONA PARA GENERAR LOS TOTALES EN EL GRID DE MANERA DINAMICA
        Dim Colum As String, Columnas As Integer = dt.Columns.Count()
        For i = 0 To Columnas - 1
            Colum = gvFormaPagoXYZ.Columns(i).FieldName()
            gvFormaPagoXYZ.Columns(i).Summary.Add(DevExpress.Data.SummaryItemType.Sum, Colum)
        Next
        ProdVista = gcFormaPago.DataSource

        SplashScreenManager.Default.SetWaitFormCaption("Completado!")
        SplashScreenManager.Default.SetWaitFormDescription("Valores obtenidos con exito")
        SplashScreenManager.CloseForm()
    End Sub

    Private Sub btExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btExportar.Click
        Dim NombreArchivo As String = ""
        NombreArchivo = ObtieneNombreArchivo()
        gcFormaPago.ExportToXls(NombreArchivo)

        MsgBox("El documento ha sido exportado con éxito en formato Excel", 64, "Nota")

    End Sub
    Private Function ObtieneNombreArchivo() As String
        Dim NombreArchivo As String = "Ventas Forma Pago " & Today.Year.ToString.PadLeft(4, "0") & Today.Month.ToString.PadLeft(2, "0") & Today.Day.ToString.PadLeft(2, "0")
        NombreArchivo = InputBox("Nombre del archivo", "Defina el nombre del archivo", NombreArchivo)

        Dim myFolderBrowserDialog As New FolderBrowserDialog

        With myFolderBrowserDialog

            .RootFolder = Environment.SpecialFolder.Desktop
            .SelectedPath = "c:\"
            .Description = "Seleccione la carpeta destino"
            If .ShowDialog = DialogResult.OK Then
                NombreArchivo = .SelectedPath & "\" & NombreArchivo & ".xls"
            End If
        End With
        Return NombreArchivo
    End Function
End Class
