﻿Imports DevExpress.XtraReports.UI
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmVentasSucursal
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blInventa As New InventarioBLL(g_ConnectionString)
    Dim Prod As DataTable, ProdVista As DataTable
    Dim dt As New DataTable


    Private Sub fac_frmConsultaProductosBase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        CargaCombos()
        gvProd1.Columns.Clear()
        gcProductos.DataSource = bl.fac_VentasSucursal(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        ProdVista = gcProductos.DataSource
        ' gvProd1.BestFitColumns()
    End Sub

    Private Sub CargaCombos()
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS --")
        objCombos.fac_TipoReporteVentas(leTipoReporte)
    End Sub


    Private Sub btGenerar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGenerar.Click
        Dim dt As DataTable
        If leTipoReporte.EditValue = 1 Then
            dt = bl.fac_VentasSucursal(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        ElseIf leTipoReporte.EditValue = 2 Then
            dt = bl.fac_VentasSucursalGrupos(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        ElseIf leTipoReporte.EditValue = 3 Then
            dt = bl.fac_VentasSucursalSubGrupos(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        ElseIf leTipoReporte.EditValue = 4 Then
            dt = bl.fac_VentasSucursalMarca(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        End If
        gvProd1.Columns.Clear()
        gcProductos.DataSource = dt
        Dim algo As String, Columnas As Integer = dt.Columns.Count()
        For i = 0 To Columnas - 1
            algo = gvProd1.Columns(i).FieldName()
            gvProd1.Columns(i).Summary.Add(DevExpress.Data.SummaryItemType.Sum, algo, "{0:c}")
        Next
        ProdVista = gcProductos.DataSource
    End Sub

    Private Sub btExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btExportar.Click
        Dim NombreArchivo As String = ""
        NombreArchivo = ObtieneNombreArchivo()
        gcProductos.ExportToXls(NombreArchivo)

        MsgBox("El documento ha sido exportado con éxito en formato Excel", 64, "Nota")

    End Sub
    Private Function ObtieneNombreArchivo() As String
        Dim NombreArchivo As String = "Ventas Anuales " & Today.Year.ToString.PadLeft(4, "0") & Today.Month.ToString.PadLeft(2, "0") & Today.Day.ToString.PadLeft(2, "0")
        NombreArchivo = InputBox("Nombre del archivo", "Defina el nombre del archivo", NombreArchivo)

        Dim myFolderBrowserDialog As New FolderBrowserDialog

        With myFolderBrowserDialog

            .RootFolder = Environment.SpecialFolder.Desktop
            .SelectedPath = "c:\"
            .Description = "Seleccione la carpeta destino"
            If .ShowDialog = DialogResult.OK Then
                NombreArchivo = .SelectedPath & "\" & NombreArchivo & ".xls"
            End If
        End With
        Return NombreArchivo
    End Function

End Class
