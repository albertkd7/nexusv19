﻿Imports NexusBLL


Public Class fac_frmListadoPedidos
    Dim bl As New FacturaBLL(g_ConnectionString)
    Private Sub fac_frmListadoPedidos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS --")
        objCombos.fac_PuntosVenta(lePuntoVenta, 1, "-- TODOS --")
        deDesde.EditValue = CDate("1/" & Month(Today) & "/" & Year(Today))
        deHasta.EditValue = Today
    End Sub

    Private Sub fac_frmListadoPedidos_Reporte() Handles Me.Reporte

        Dim dt As DataTable = bl.fac_PedidosPeriodo(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)
        Dim rpt As New fac_rptListadoPedidos() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.Text & ", PUNTO DE VENTA: " & lePuntoVenta.Text
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt.ShowPreviewDialog()
    End Sub


    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub
End Class
