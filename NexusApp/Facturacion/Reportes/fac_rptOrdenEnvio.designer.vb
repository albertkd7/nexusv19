﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class fac_rptOrdenEnvio
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xrlTotal = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCon = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaExenta = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCre = New DevExpress.XtraReports.UI.XRLabel
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
        Me.FechaVence3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNombre = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlComentario = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.xrlSucursalRecibe = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlSucursalEnvia = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTotal2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlAnio = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlMes = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDia = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaACuenta = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlPedido = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCodigo = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDiasCredito = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDepto = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlMunic = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNit = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDireccion = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFecha = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNrc = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFormaPago = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlGiro = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNumero = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVendedor = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTelefono = New DevExpress.XtraReports.UI.XRLabel
        Me.FechaCargo2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaNoSujeta = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlSubTotal = New DevExpress.XtraReports.UI.XRLabel
        Me.Factura2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCantLetras = New DevExpress.XtraReports.UI.XRLabel
        Me.FechaVence4 = New DevExpress.XtraReports.UI.XRLabel
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.xrlIvaRetenido = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlIva = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaAfecta = New DevExpress.XtraReports.UI.XRLabel
        Me.LecturaActual2 = New DevExpress.XtraReports.UI.XRLabel
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.LecturaAnterior2 = New DevExpress.XtraReports.UI.XRLabel
        Me.ConsumoM32 = New DevExpress.XtraReports.UI.XRLabel
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
        Me.xrlNumFormulario = New DevExpress.XtraReports.UI.XRLabel
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xrlTotal
        '
        Me.xrlTotal.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlTotal.LocationFloat = New DevExpress.Utils.PointFloat(612.4999!, 114.0832!)
        Me.xrlTotal.Name = "xrlTotal"
        Me.xrlTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal.SizeF = New System.Drawing.SizeF(94.0!, 15.99998!)
        Me.xrlTotal.StylePriority.UseBorders = False
        Me.xrlTotal.StylePriority.UseFont = False
        Me.xrlTotal.StylePriority.UseTextAlignment = False
        Me.xrlTotal.Text = "0.00"
        Me.xrlTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCon
        '
        Me.xrlCon.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.Contado")})
        Me.xrlCon.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlCon.LocationFloat = New DevExpress.Utils.PointFloat(482.1246!, 116.0001!)
        Me.xrlCon.Name = "xrlCon"
        Me.xrlCon.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCon.SizeF = New System.Drawing.SizeF(15.66644!, 14.00002!)
        Me.xrlCon.StylePriority.UseFont = False
        Me.xrlCon.StylePriority.UseTextAlignment = False
        Me.xrlCon.Text = "xrlCon"
        Me.xrlCon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlVentaExenta
        '
        Me.xrlVentaExenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaExenta.LocationFloat = New DevExpress.Utils.PointFloat(612.4999!, 69.08318!)
        Me.xrlVentaExenta.Name = "xrlVentaExenta"
        Me.xrlVentaExenta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta.SizeF = New System.Drawing.SizeF(94.0!, 16.0!)
        Me.xrlVentaExenta.StylePriority.UseFont = False
        Me.xrlVentaExenta.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta.Text = "0.00"
        Me.xrlVentaExenta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel1
        '
        Me.XrLabel1.CanGrow = False
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.UnidadMedida")})
        Me.XrLabel1.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(443.7079!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(55.16666!, 14.37481!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.Text = "XrLabel1"
        '
        'xrlCre
        '
        Me.xrlCre.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.Credito")})
        Me.xrlCre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlCre.LocationFloat = New DevExpress.Utils.PointFloat(555.4999!, 116.0001!)
        Me.xrlCre.Name = "xrlCre"
        Me.xrlCre.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCre.SizeF = New System.Drawing.SizeF(15.66644!, 14.00002!)
        Me.xrlCre.StylePriority.UseFont = False
        Me.xrlCre.StylePriority.UseTextAlignment = False
        Me.xrlCre.Text = "xrlCre"
        Me.xrlCre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.HeightF = 35.0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'FechaVence3
        '
        Me.FechaVence3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.VentaNoSujeta", "{0:n2}")})
        Me.FechaVence3.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaVence3.LocationFloat = New DevExpress.Utils.PointFloat(581.1246!, 0.0!)
        Me.FechaVence3.Name = "FechaVence3"
        Me.FechaVence3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.FechaVence3.SizeF = New System.Drawing.SizeF(25.41998!, 14.37482!)
        Me.FechaVence3.StylePriority.UseFont = False
        Me.FechaVence3.StylePriority.UseTextAlignment = False
        Me.FechaVence3.Text = "FechaVence3"
        Me.FechaVence3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.FechaVence3.Visible = False
        '
        'xrlNombre
        '
        Me.xrlNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlNombre.LocationFloat = New DevExpress.Utils.PointFloat(51.75003!, 67.0!)
        Me.xrlNombre.Name = "xrlNombre"
        Me.xrlNombre.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNombre.SizeF = New System.Drawing.SizeF(521.3744!, 16.0!)
        Me.xrlNombre.StylePriority.UseFont = False
        Me.xrlNombre.StylePriority.UseTextAlignment = False
        Me.xrlNombre.Text = "IT OUTSOURCING, S.A. DE C.V."
        Me.xrlNombre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlComentario
        '
        Me.xrlComentario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlComentario.LocationFloat = New DevExpress.Utils.PointFloat(27.83327!, 50.87506!)
        Me.xrlComentario.Name = "xrlComentario"
        Me.xrlComentario.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlComentario.SizeF = New System.Drawing.SizeF(430.2912!, 17.00001!)
        Me.xrlComentario.StylePriority.UseFont = False
        Me.xrlComentario.StylePriority.UseTextAlignment = False
        Me.xrlComentario.Text = "Comentario"
        Me.xrlComentario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlNumFormulario, Me.xrlSucursalRecibe, Me.xrlSucursalEnvia, Me.xrlTotal2, Me.xrlAnio, Me.xrlMes, Me.xrlDia, Me.xrlVentaACuenta, Me.xrlCre, Me.xrlCon, Me.xrlPedido, Me.xrlCodigo, Me.xrlDiasCredito, Me.xrlDepto, Me.xrlMunic, Me.xrlNit, Me.xrlDireccion, Me.xrlFecha, Me.xrlNrc, Me.xrlNombre, Me.xrlFormaPago, Me.xrlGiro, Me.xrlNumero, Me.xrlVendedor, Me.xrlTelefono})
        Me.PageHeader.HeightF = 203.0832!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlSucursalRecibe
        '
        Me.xrlSucursalRecibe.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlSucursalRecibe.LocationFloat = New DevExpress.Utils.PointFloat(605.1245!, 142.3749!)
        Me.xrlSucursalRecibe.Name = "xrlSucursalRecibe"
        Me.xrlSucursalRecibe.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSucursalRecibe.SizeF = New System.Drawing.SizeF(181.1667!, 15.00001!)
        Me.xrlSucursalRecibe.StylePriority.UseFont = False
        Me.xrlSucursalRecibe.StylePriority.UseTextAlignment = False
        Me.xrlSucursalRecibe.Text = "xrlVendedor"
        Me.xrlSucursalRecibe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlSucursalEnvia
        '
        Me.xrlSucursalEnvia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlSucursalEnvia.LocationFloat = New DevExpress.Utils.PointFloat(604.4999!, 100.0!)
        Me.xrlSucursalEnvia.Name = "xrlSucursalEnvia"
        Me.xrlSucursalEnvia.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSucursalEnvia.SizeF = New System.Drawing.SizeF(181.1667!, 15.00001!)
        Me.xrlSucursalEnvia.StylePriority.UseFont = False
        Me.xrlSucursalEnvia.StylePriority.UseTextAlignment = False
        Me.xrlSucursalEnvia.Text = "xrlVendedor"
        Me.xrlSucursalEnvia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTotal2
        '
        Me.xrlTotal2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlTotal2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlTotal2.LocationFloat = New DevExpress.Utils.PointFloat(659.5829!, 67.00001!)
        Me.xrlTotal2.Name = "xrlTotal2"
        Me.xrlTotal2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal2.SizeF = New System.Drawing.SizeF(94.0!, 15.99998!)
        Me.xrlTotal2.StylePriority.UseBorders = False
        Me.xrlTotal2.StylePriority.UseFont = False
        Me.xrlTotal2.StylePriority.UseTextAlignment = False
        Me.xrlTotal2.Text = "0.00"
        Me.xrlTotal2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlAnio
        '
        Me.xrlAnio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlAnio.LocationFloat = New DevExpress.Utils.PointFloat(681.5414!, 45.99997!)
        Me.xrlAnio.Name = "xrlAnio"
        Me.xrlAnio.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlAnio.SizeF = New System.Drawing.SizeF(72.0415!, 14.99999!)
        Me.xrlAnio.StylePriority.UseFont = False
        Me.xrlAnio.StylePriority.UseTextAlignment = False
        Me.xrlAnio.Text = "xrlAnio"
        Me.xrlAnio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlMes
        '
        Me.xrlMes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlMes.LocationFloat = New DevExpress.Utils.PointFloat(604.4999!, 45.99997!)
        Me.xrlMes.Name = "xrlMes"
        Me.xrlMes.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlMes.SizeF = New System.Drawing.SizeF(72.0415!, 14.99999!)
        Me.xrlMes.StylePriority.UseFont = False
        Me.xrlMes.StylePriority.UseTextAlignment = False
        Me.xrlMes.Text = "xrlMes"
        Me.xrlMes.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDia
        '
        Me.xrlDia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlDia.LocationFloat = New DevExpress.Utils.PointFloat(555.4999!, 45.99997!)
        Me.xrlDia.Name = "xrlDia"
        Me.xrlDia.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDia.SizeF = New System.Drawing.SizeF(46.62463!, 14.99999!)
        Me.xrlDia.StylePriority.UseFont = False
        Me.xrlDia.StylePriority.UseTextAlignment = False
        Me.xrlDia.Text = "xrlDia"
        Me.xrlDia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaACuenta
        '
        Me.xrlVentaACuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaACuenta.LocationFloat = New DevExpress.Utils.PointFloat(60.74993!, 116.0001!)
        Me.xrlVentaACuenta.Name = "xrlVentaACuenta"
        Me.xrlVentaACuenta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaACuenta.SizeF = New System.Drawing.SizeF(319.7084!, 15.00001!)
        Me.xrlVentaACuenta.StylePriority.UseFont = False
        Me.xrlVentaACuenta.StylePriority.UseTextAlignment = False
        Me.xrlVentaACuenta.Text = "VENTA A CUENTA DE"
        Me.xrlVentaACuenta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlPedido
        '
        Me.xrlPedido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlPedido.LocationFloat = New DevExpress.Utils.PointFloat(318.0625!, 159.3749!)
        Me.xrlPedido.Name = "xrlPedido"
        Me.xrlPedido.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPedido.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlPedido.StylePriority.UseFont = False
        Me.xrlPedido.StylePriority.UseTextAlignment = False
        Me.xrlPedido.Text = "1945"
        Me.xrlPedido.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCodigo
        '
        Me.xrlCodigo.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlCodigo.LocationFloat = New DevExpress.Utils.PointFloat(113.0!, 13.00001!)
        Me.xrlCodigo.Name = "xrlCodigo"
        Me.xrlCodigo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCodigo.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlCodigo.StylePriority.UseFont = False
        Me.xrlCodigo.StylePriority.UseTextAlignment = False
        Me.xrlCodigo.Text = "xrlCodigo"
        Me.xrlCodigo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlCodigo.Visible = False
        '
        'xrlDiasCredito
        '
        Me.xrlDiasCredito.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDiasCredito.LocationFloat = New DevExpress.Utils.PointFloat(190.0!, 13.00001!)
        Me.xrlDiasCredito.Name = "xrlDiasCredito"
        Me.xrlDiasCredito.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDiasCredito.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlDiasCredito.StylePriority.UseFont = False
        Me.xrlDiasCredito.StylePriority.UseTextAlignment = False
        Me.xrlDiasCredito.Text = "xrlDiasCredito"
        Me.xrlDiasCredito.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlDiasCredito.Visible = False
        '
        'xrlDepto
        '
        Me.xrlDepto.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDepto.LocationFloat = New DevExpress.Utils.PointFloat(168.0001!, 0.0!)
        Me.xrlDepto.Name = "xrlDepto"
        Me.xrlDepto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDepto.SizeF = New System.Drawing.SizeF(132.0001!, 15.00002!)
        Me.xrlDepto.StylePriority.UseFont = False
        Me.xrlDepto.StylePriority.UseTextAlignment = False
        Me.xrlDepto.Text = "SAN SALVADOR"
        Me.xrlDepto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlDepto.Visible = False
        '
        'xrlMunic
        '
        Me.xrlMunic.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlMunic.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 0.0!)
        Me.xrlMunic.Name = "xrlMunic"
        Me.xrlMunic.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlMunic.SizeF = New System.Drawing.SizeF(132.0001!, 15.00002!)
        Me.xrlMunic.StylePriority.UseFont = False
        Me.xrlMunic.StylePriority.UseTextAlignment = False
        Me.xrlMunic.Text = "SAN SALVADOR"
        Me.xrlMunic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlMunic.Visible = False
        '
        'xrlNit
        '
        Me.xrlNit.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlNit.LocationFloat = New DevExpress.Utils.PointFloat(469.1245!, 131.0!)
        Me.xrlNit.Name = "xrlNit"
        Me.xrlNit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNit.SizeF = New System.Drawing.SizeF(133.0!, 12.0!)
        Me.xrlNit.StylePriority.UseBorders = False
        Me.xrlNit.StylePriority.UseFont = False
        Me.xrlNit.StylePriority.UseTextAlignment = False
        Me.xrlNit.Text = "0614-251107-107-7"
        Me.xrlNit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDireccion
        '
        Me.xrlDireccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlDireccion.LocationFloat = New DevExpress.Utils.PointFloat(60.74992!, 100.0!)
        Me.xrlDireccion.Name = "xrlDireccion"
        Me.xrlDireccion.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDireccion.SizeF = New System.Drawing.SizeF(445.3746!, 15.00003!)
        Me.xrlDireccion.StylePriority.UseFont = False
        Me.xrlDireccion.StylePriority.UseTextAlignment = False
        Me.xrlDireccion.Text = "RESIDENCIAL CLAUDIA PASAJE AIDA No.19"
        Me.xrlDireccion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFecha
        '
        Me.xrlFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlFecha.LocationFloat = New DevExpress.Utils.PointFloat(425.1245!, 45.99998!)
        Me.xrlFecha.Name = "xrlFecha"
        Me.xrlFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlFecha.StylePriority.UseFont = False
        Me.xrlFecha.StylePriority.UseTextAlignment = False
        Me.xrlFecha.Text = "25/12/2012"
        Me.xrlFecha.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNrc
        '
        Me.xrlNrc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlNrc.LocationFloat = New DevExpress.Utils.PointFloat(469.1245!, 145.3749!)
        Me.xrlNrc.Name = "xrlNrc"
        Me.xrlNrc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNrc.SizeF = New System.Drawing.SizeF(136.0!, 13.0!)
        Me.xrlNrc.StylePriority.UseFont = False
        Me.xrlNrc.StylePriority.UseTextAlignment = False
        Me.xrlNrc.Text = "194534-3"
        Me.xrlNrc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFormaPago
        '
        Me.xrlFormaPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlFormaPago.LocationFloat = New DevExpress.Utils.PointFloat(300.0002!, 0.0!)
        Me.xrlFormaPago.Name = "xrlFormaPago"
        Me.xrlFormaPago.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFormaPago.SizeF = New System.Drawing.SizeF(104.2081!, 17.00001!)
        Me.xrlFormaPago.StylePriority.UseFont = False
        Me.xrlFormaPago.StylePriority.UseTextAlignment = False
        Me.xrlFormaPago.Text = "CONTADO"
        Me.xrlFormaPago.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlFormaPago.Visible = False
        '
        'xrlGiro
        '
        Me.xrlGiro.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlGiro.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlGiro.LocationFloat = New DevExpress.Utils.PointFloat(35.8747!, 144.3749!)
        Me.xrlGiro.Name = "xrlGiro"
        Me.xrlGiro.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlGiro.SizeF = New System.Drawing.SizeF(405.2498!, 13.0!)
        Me.xrlGiro.StylePriority.UseBorders = False
        Me.xrlGiro.StylePriority.UseFont = False
        Me.xrlGiro.StylePriority.UseTextAlignment = False
        Me.xrlGiro.Text = "CONSULTORES DE EQUIPO Y PROGRAMAS DE INFORMATICA"
        Me.xrlGiro.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNumero
        '
        Me.xrlNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlNumero.LocationFloat = New DevExpress.Utils.PointFloat(423.0828!, 28.0!)
        Me.xrlNumero.Name = "xrlNumero"
        Me.xrlNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlNumero.StylePriority.UseFont = False
        Me.xrlNumero.StylePriority.UseTextAlignment = False
        Me.xrlNumero.Text = "1945"
        Me.xrlNumero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVendedor
        '
        Me.xrlVendedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVendedor.LocationFloat = New DevExpress.Utils.PointFloat(64.75003!, 159.3749!)
        Me.xrlVendedor.Name = "xrlVendedor"
        Me.xrlVendedor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVendedor.SizeF = New System.Drawing.SizeF(181.1667!, 15.00001!)
        Me.xrlVendedor.StylePriority.UseFont = False
        Me.xrlVendedor.StylePriority.UseTextAlignment = False
        Me.xrlVendedor.Text = "xrlVendedor"
        Me.xrlVendedor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTelefono
        '
        Me.xrlTelefono.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlTelefono.LocationFloat = New DevExpress.Utils.PointFloat(36.0!, 13.0!)
        Me.xrlTelefono.Name = "xrlTelefono"
        Me.xrlTelefono.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTelefono.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlTelefono.StylePriority.UseFont = False
        Me.xrlTelefono.StylePriority.UseTextAlignment = False
        Me.xrlTelefono.Text = "25/12/2012"
        Me.xrlTelefono.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlTelefono.Visible = False
        '
        'FechaCargo2
        '
        Me.FechaCargo2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.IdProducto")})
        Me.FechaCargo2.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaCargo2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.FechaCargo2.Name = "FechaCargo2"
        Me.FechaCargo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.FechaCargo2.SizeF = New System.Drawing.SizeF(91.16668!, 14.37481!)
        Me.FechaCargo2.StylePriority.UseFont = False
        Me.FechaCargo2.Text = "FechaCargo2"
        '
        'xrlVentaNoSujeta
        '
        Me.xrlVentaNoSujeta.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlVentaNoSujeta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaNoSujeta.LocationFloat = New DevExpress.Utils.PointFloat(612.4999!, 85.875!)
        Me.xrlVentaNoSujeta.Name = "xrlVentaNoSujeta"
        Me.xrlVentaNoSujeta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaNoSujeta.SizeF = New System.Drawing.SizeF(94.0!, 15.99998!)
        Me.xrlVentaNoSujeta.StylePriority.UseBorders = False
        Me.xrlVentaNoSujeta.StylePriority.UseFont = False
        Me.xrlVentaNoSujeta.StylePriority.UseTextAlignment = False
        Me.xrlVentaNoSujeta.Text = "0.00"
        Me.xrlVentaNoSujeta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlSubTotal
        '
        Me.xrlSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlSubTotal.LocationFloat = New DevExpress.Utils.PointFloat(612.4999!, 33.87506!)
        Me.xrlSubTotal.Name = "xrlSubTotal"
        Me.xrlSubTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSubTotal.SizeF = New System.Drawing.SizeF(94.0!, 19.0!)
        Me.xrlSubTotal.StylePriority.UseFont = False
        Me.xrlSubTotal.StylePriority.UseTextAlignment = False
        Me.xrlSubTotal.Text = "0.00"
        Me.xrlSubTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'Factura2
        '
        Me.Factura2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.VentaAfecta", "{0:n2}")})
        Me.Factura2.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Factura2.LocationFloat = New DevExpress.Utils.PointFloat(634.4584!, 0.0!)
        Me.Factura2.Name = "Factura2"
        Me.Factura2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.Factura2.SizeF = New System.Drawing.SizeF(73.74994!, 14.37482!)
        Me.Factura2.StylePriority.UseFont = False
        Me.Factura2.StylePriority.UseTextAlignment = False
        Me.Factura2.Text = "Factura2"
        Me.Factura2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCantLetras
        '
        Me.xrlCantLetras.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlCantLetras.LocationFloat = New DevExpress.Utils.PointFloat(27.83327!, 69.875!)
        Me.xrlCantLetras.Name = "xrlCantLetras"
        Me.xrlCantLetras.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCantLetras.SizeF = New System.Drawing.SizeF(449.4583!, 14.00001!)
        Me.xrlCantLetras.StylePriority.UseFont = False
        Me.xrlCantLetras.StylePriority.UseTextAlignment = False
        Me.xrlCantLetras.Text = "DIEZ"
        Me.xrlCantLetras.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'FechaVence4
        '
        Me.FechaVence4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.VentaExenta", "{0:n2}")})
        Me.FechaVence4.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaVence4.LocationFloat = New DevExpress.Utils.PointFloat(609.4999!, 0.0!)
        Me.FechaVence4.Name = "FechaVence4"
        Me.FechaVence4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.FechaVence4.SizeF = New System.Drawing.SizeF(23.41998!, 14.37482!)
        Me.FechaVence4.StylePriority.UseFont = False
        Me.FechaVence4.StylePriority.UseTextAlignment = False
        Me.FechaVence4.Text = "FechaVence4"
        Me.FechaVence4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.FechaVence4.Visible = False
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlIvaRetenido, Me.xrlIva, Me.xrlComentario, Me.xrlCantLetras, Me.xrlTotal, Me.xrlVentaAfecta, Me.xrlSubTotal, Me.xrlVentaExenta, Me.xrlVentaNoSujeta})
        Me.PageFooter.HeightF = 154.1665!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlIvaRetenido
        '
        Me.xrlIvaRetenido.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlIvaRetenido.LocationFloat = New DevExpress.Utils.PointFloat(612.4999!, 52.87508!)
        Me.xrlIvaRetenido.Name = "xrlIvaRetenido"
        Me.xrlIvaRetenido.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIvaRetenido.SizeF = New System.Drawing.SizeF(94.0!, 16.875!)
        Me.xrlIvaRetenido.StylePriority.UseFont = False
        Me.xrlIvaRetenido.StylePriority.UseTextAlignment = False
        Me.xrlIvaRetenido.Text = "0.00"
        Me.xrlIvaRetenido.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlIva
        '
        Me.xrlIva.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlIva.LocationFloat = New DevExpress.Utils.PointFloat(612.4999!, 16.87505!)
        Me.xrlIva.Name = "xrlIva"
        Me.xrlIva.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIva.SizeF = New System.Drawing.SizeF(94.0!, 19.0!)
        Me.xrlIva.StylePriority.UseFont = False
        Me.xrlIva.StylePriority.UseTextAlignment = False
        Me.xrlIva.Text = "0.00"
        Me.xrlIva.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaAfecta
        '
        Me.xrlVentaAfecta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaAfecta.LocationFloat = New DevExpress.Utils.PointFloat(612.4999!, 0.0!)
        Me.xrlVentaAfecta.Name = "xrlVentaAfecta"
        Me.xrlVentaAfecta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaAfecta.SizeF = New System.Drawing.SizeF(94.0!, 16.875!)
        Me.xrlVentaAfecta.StylePriority.UseFont = False
        Me.xrlVentaAfecta.StylePriority.UseTextAlignment = False
        Me.xrlVentaAfecta.Text = "0.00"
        Me.xrlVentaAfecta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'LecturaActual2
        '
        Me.LecturaActual2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.Descripcion")})
        Me.LecturaActual2.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LecturaActual2.LocationFloat = New DevExpress.Utils.PointFloat(90.83328!, 0.0!)
        Me.LecturaActual2.Name = "LecturaActual2"
        Me.LecturaActual2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LecturaActual2.SizeF = New System.Drawing.SizeF(279.4999!, 14.37481!)
        Me.LecturaActual2.StylePriority.UseFont = False
        Me.LecturaActual2.StylePriority.UseTextAlignment = False
        Me.LecturaActual2.Text = "LecturaActual2"
        Me.LecturaActual2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.Factura2, Me.FechaVence3, Me.FechaCargo2, Me.LecturaAnterior2, Me.ConsumoM32, Me.LecturaActual2, Me.FechaVence4, Me.XrLabel1})
        Me.Detail.HeightF = 15.33314!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'LecturaAnterior2
        '
        Me.LecturaAnterior2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.Cantidad", "{0:n3}")})
        Me.LecturaAnterior2.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LecturaAnterior2.LocationFloat = New DevExpress.Utils.PointFloat(367.6665!, 0.0!)
        Me.LecturaAnterior2.Name = "LecturaAnterior2"
        Me.LecturaAnterior2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LecturaAnterior2.SizeF = New System.Drawing.SizeF(74.04132!, 14.37481!)
        Me.LecturaAnterior2.StylePriority.UseFont = False
        Me.LecturaAnterior2.StylePriority.UseTextAlignment = False
        Me.LecturaAnterior2.Text = "LecturaAnterior2"
        Me.LecturaAnterior2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'ConsumoM32
        '
        Me.ConsumoM32.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.PrecioUnitario", "{0:n4}")})
        Me.ConsumoM32.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsumoM32.LocationFloat = New DevExpress.Utils.PointFloat(497.8745!, 0.0!)
        Me.ConsumoM32.Name = "ConsumoM32"
        Me.ConsumoM32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.ConsumoM32.SizeF = New System.Drawing.SizeF(63.41669!, 14.37482!)
        Me.ConsumoM32.StylePriority.UseFont = False
        Me.ConsumoM32.StylePriority.UseTextAlignment = False
        Me.ConsumoM32.Text = "ConsumoM32"
        Me.ConsumoM32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.HeightF = 30.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        Me.BottomMarginBand1.SnapLinePadding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        '
        'xrlNumFormulario
        '
        Me.xrlNumFormulario.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.xrlNumFormulario.LocationFloat = New DevExpress.Utils.PointFloat(581.1246!, 10.00001!)
        Me.xrlNumFormulario.Name = "xrlNumFormulario"
        Me.xrlNumFormulario.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumFormulario.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlNumFormulario.StylePriority.UseFont = False
        Me.xrlNumFormulario.StylePriority.UseTextAlignment = False
        Me.xrlNumFormulario.Text = "1945"
        Me.xrlNumFormulario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'fac_rptOrdenEnvio
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.DataMember = "Factura"
        Me.DesignerOptions.ShowExportWarnings = False
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(15, 39, 35, 30)
        Me.PageHeight = 850
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PaperName = "CCF"
        Me.PrinterName = "EPSON FX-890 Ver 2.0"
        Me.SnapGridSize = 2.083333!
        Me.SnapToGrid = False
        Me.Version = "11.1"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents xrlTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCon As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCre As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents FechaVence3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNombre As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlComentario As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents xrlPedido As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCodigo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDiasCredito As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDepto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlMunic As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNit As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDireccion As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFecha As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNrc As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFormaPago As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlGiro As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumero As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVendedor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTelefono As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents FechaCargo2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaNoSujeta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSubTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Factura2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCantLetras As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents FechaVence4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents xrlIvaRetenido As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIva As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaAfecta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents LecturaActual2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents LecturaAnterior2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ConsumoM32 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents xrlVentaACuenta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlAnio As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlMes As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDia As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSucursalRecibe As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSucursalEnvia As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumFormulario As DevExpress.XtraReports.UI.XRLabel
End Class
