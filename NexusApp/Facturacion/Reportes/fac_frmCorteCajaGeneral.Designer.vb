﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmCorteCajaGeneral
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit()
        Me.lePuntoVenta = New DevExpress.XtraEditors.LookUpEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.deDesde = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.sbCerrar = New DevExpress.XtraEditors.SimpleButton()
        Me.leVendedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teTotal = New DevExpress.XtraEditors.TextEdit()
        Me.gcArqueo = New DevExpress.XtraGrid.GridControl()
        Me.gvArqueo = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcValor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcNumCheque = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcBanco = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteBancoCheque = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcCliente = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcValorCheque = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcReserva = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcBancoRem = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteBancoRM = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.gc2 = New DevExpress.XtraGrid.GridControl()
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcNumRemesa = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcBancoRemesa = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteBancoRemesa = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcValorRemesa = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.deFechaDet = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdBorrar2 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.gc3 = New DevExpress.XtraGrid.GridControl()
        Me.gv3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcNumVaucher = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcBancoVaucher = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteBancoVaucher = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcValorVaucher = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.cmdBorrar3 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbRevierteCierre = New DevExpress.XtraEditors.SimpleButton()
        Me.sbObtener = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcArqueo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvArqueo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteBancoCheque, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteBancoRM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteBancoRemesa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaDet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaDet.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteBancoVaucher, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.sbObtener)
        Me.GroupControl1.Controls.Add(Me.sbRevierteCierre)
        Me.GroupControl1.Controls.Add(Me.cmdBorrar3)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Controls.Add(Me.gc3)
        Me.GroupControl1.Controls.Add(Me.cmdBorrar2)
        Me.GroupControl1.Controls.Add(Me.cmdBorrar)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.gc2)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.gc)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.teTotal)
        Me.GroupControl1.Controls.Add(Me.gcArqueo)
        Me.GroupControl1.Controls.Add(Me.leVendedor)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.sbCerrar)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.lePuntoVenta)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.deDesde)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Size = New System.Drawing.Size(1204, 490)
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "INFORME DE CORTE DE CAJA DEL DIA"
        Me.teTitulo.Location = New System.Drawing.Point(90, 123)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(342, 20)
        Me.teTitulo.TabIndex = 4
        '
        'lePuntoVenta
        '
        Me.lePuntoVenta.EnterMoveNextControl = True
        Me.lePuntoVenta.Location = New System.Drawing.Point(90, 78)
        Me.lePuntoVenta.Name = "lePuntoVenta"
        Me.lePuntoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePuntoVenta.Size = New System.Drawing.Size(342, 20)
        Me.lePuntoVenta.TabIndex = 2
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(90, 55)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(342, 20)
        Me.leSucursal.TabIndex = 1
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(1, 126)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl7.TabIndex = 20
        Me.LabelControl7.Text = "Título del informe:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(9, 79)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl6.TabIndex = 21
        Me.LabelControl6.Text = "Punto de Venta:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(43, 58)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl5.TabIndex = 19
        Me.LabelControl5.Text = "Sucursal:"
        '
        'deDesde
        '
        Me.deDesde.EditValue = Nothing
        Me.deDesde.EnterMoveNextControl = True
        Me.deDesde.Location = New System.Drawing.Point(90, 29)
        Me.deDesde.Name = "deDesde"
        Me.deDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deDesde.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deDesde.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deDesde.Size = New System.Drawing.Size(93, 20)
        Me.deDesde.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(54, 32)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl1.TabIndex = 22
        Me.LabelControl1.Text = "Fecha:"
        '
        'sbCerrar
        '
        Me.sbCerrar.Image = Global.Nexus.My.Resources.Resources.autoriza3
        Me.sbCerrar.Location = New System.Drawing.Point(257, 27)
        Me.sbCerrar.Name = "sbCerrar"
        Me.sbCerrar.Size = New System.Drawing.Size(95, 23)
        Me.sbCerrar.TabIndex = 6
        Me.sbCerrar.Text = "Cerrar Corte"
        '
        'leVendedor
        '
        Me.leVendedor.EnterMoveNextControl = True
        Me.leVendedor.Location = New System.Drawing.Point(90, 100)
        Me.leVendedor.Name = "leVendedor"
        Me.leVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leVendedor.Size = New System.Drawing.Size(342, 20)
        Me.leVendedor.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(36, 103)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl3.TabIndex = 81
        Me.LabelControl3.Text = "Vendedor:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(280, 452)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl2.TabIndex = 83
        Me.LabelControl2.Text = "Total:"
        '
        'teTotal
        '
        Me.teTotal.EditValue = 0
        Me.teTotal.EnterMoveNextControl = True
        Me.teTotal.Location = New System.Drawing.Point(310, 448)
        Me.teTotal.Name = "teTotal"
        Me.teTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotal.Properties.Mask.EditMask = "n2"
        Me.teTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTotal.Properties.ReadOnly = True
        Me.teTotal.Size = New System.Drawing.Size(100, 20)
        Me.teTotal.TabIndex = 84
        '
        'gcArqueo
        '
        Me.gcArqueo.Location = New System.Drawing.Point(90, 149)
        Me.gcArqueo.MainView = Me.gvArqueo
        Me.gcArqueo.Name = "gcArqueo"
        Me.gcArqueo.Size = New System.Drawing.Size(343, 293)
        Me.gcArqueo.TabIndex = 82
        Me.gcArqueo.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvArqueo, Me.GridView2})
        '
        'gvArqueo
        '
        Me.gvArqueo.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcValor, Me.gcCantidad, Me.gcTotal, Me.gcNombre})
        Me.gvArqueo.GridControl = Me.gcArqueo
        Me.gvArqueo.Name = "gvArqueo"
        Me.gvArqueo.OptionsView.ShowGroupPanel = False
        '
        'gcValor
        '
        Me.gcValor.Caption = "Denominación"
        Me.gcValor.FieldName = "Valor"
        Me.gcValor.Name = "gcValor"
        Me.gcValor.OptionsColumn.AllowFocus = False
        Me.gcValor.OptionsColumn.AllowMove = False
        Me.gcValor.OptionsColumn.ReadOnly = True
        '
        'gcCantidad
        '
        Me.gcCantidad.Caption = "Cantidad"
        Me.gcCantidad.FieldName = "Cantidad"
        Me.gcCantidad.Name = "gcCantidad"
        Me.gcCantidad.OptionsColumn.AllowMove = False
        Me.gcCantidad.Visible = True
        Me.gcCantidad.VisibleIndex = 1
        '
        'gcTotal
        '
        Me.gcTotal.Caption = "Total"
        Me.gcTotal.FieldName = "Total"
        Me.gcTotal.Name = "gcTotal"
        Me.gcTotal.OptionsColumn.AllowFocus = False
        Me.gcTotal.OptionsColumn.AllowMove = False
        Me.gcTotal.OptionsColumn.ReadOnly = True
        Me.gcTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)})
        Me.gcTotal.Visible = True
        Me.gcTotal.VisibleIndex = 2
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Denominacion"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.OptionsColumn.AllowMove = False
        Me.gcNombre.OptionsColumn.ReadOnly = True
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 0
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.gcArqueo
        Me.GridView2.Name = "GridView2"
        '
        'gc
        '
        Me.gc.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.First.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.Last.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.Next.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.NextPage.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.Prev.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.PrevPage.Visible = False
        Me.gc.Location = New System.Drawing.Point(439, 59)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteBancoCheque, Me.riteBancoRM})
        Me.gc.Size = New System.Drawing.Size(580, 114)
        Me.gc.TabIndex = 85
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcNumCheque, Me.gcBanco, Me.gcCliente, Me.gcValorCheque, Me.gcReserva, Me.gcBancoRem, Me.GridColumn1, Me.GridColumn2})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsNavigation.AutoFocusNewRow = True
        Me.gv.OptionsNavigation.EnterMoveNextColumn = True
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcNumCheque
        '
        Me.gcNumCheque.Caption = "Número"
        Me.gcNumCheque.FieldName = "NumCheque"
        Me.gcNumCheque.Name = "gcNumCheque"
        Me.gcNumCheque.Visible = True
        Me.gcNumCheque.VisibleIndex = 0
        Me.gcNumCheque.Width = 87
        '
        'gcBanco
        '
        Me.gcBanco.Caption = "Banco"
        Me.gcBanco.ColumnEdit = Me.riteBancoCheque
        Me.gcBanco.FieldName = "IdBanco"
        Me.gcBanco.Name = "gcBanco"
        Me.gcBanco.Visible = True
        Me.gcBanco.VisibleIndex = 3
        Me.gcBanco.Width = 134
        '
        'riteBancoCheque
        '
        Me.riteBancoCheque.AutoHeight = False
        Me.riteBancoCheque.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteBancoCheque.Name = "riteBancoCheque"
        '
        'gcCliente
        '
        Me.gcCliente.Caption = "Cliente"
        Me.gcCliente.FieldName = "Cliente"
        Me.gcCliente.Name = "gcCliente"
        Me.gcCliente.Visible = True
        Me.gcCliente.VisibleIndex = 2
        Me.gcCliente.Width = 119
        '
        'gcValorCheque
        '
        Me.gcValorCheque.Caption = "Valor"
        Me.gcValorCheque.FieldName = "Valor"
        Me.gcValorCheque.Name = "gcValorCheque"
        Me.gcValorCheque.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor", "{0:c}")})
        Me.gcValorCheque.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.gcValorCheque.Visible = True
        Me.gcValorCheque.VisibleIndex = 1
        Me.gcValorCheque.Width = 71
        '
        'gcReserva
        '
        Me.gcReserva.Caption = "No. Reserva"
        Me.gcReserva.FieldName = "NumReserva"
        Me.gcReserva.Name = "gcReserva"
        Me.gcReserva.Visible = True
        Me.gcReserva.VisibleIndex = 4
        Me.gcReserva.Width = 66
        '
        'gcBancoRem
        '
        Me.gcBancoRem.Caption = "Banco RM"
        Me.gcBancoRem.ColumnEdit = Me.riteBancoRM
        Me.gcBancoRem.FieldName = "IdBancoRemesa"
        Me.gcBancoRem.Name = "gcBancoRem"
        Me.gcBancoRem.Visible = True
        Me.gcBancoRem.VisibleIndex = 5
        Me.gcBancoRem.Width = 87
        '
        'riteBancoRM
        '
        Me.riteBancoRM.AutoHeight = False
        Me.riteBancoRM.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteBancoRM.Name = "riteBancoRM"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "GridColumn1"
        Me.GridColumn1.FieldName = "Banco"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "GridColumn2"
        Me.GridColumn2.FieldName = "BancoRM"
        Me.GridColumn2.Name = "GridColumn2"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(446, 42)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(108, 13)
        Me.LabelControl4.TabIndex = 86
        Me.LabelControl4.Text = "Cheques Recibidos:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(439, 185)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(111, 13)
        Me.LabelControl8.TabIndex = 88
        Me.LabelControl8.Text = "Remesas a Realizar"
        '
        'gc2
        '
        Me.gc2.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.gc2.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.gc2.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.gc2.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.gc2.EmbeddedNavigator.Buttons.First.Visible = False
        Me.gc2.EmbeddedNavigator.Buttons.Last.Visible = False
        Me.gc2.EmbeddedNavigator.Buttons.Next.Visible = False
        Me.gc2.EmbeddedNavigator.Buttons.NextPage.Visible = False
        Me.gc2.EmbeddedNavigator.Buttons.Prev.Visible = False
        Me.gc2.EmbeddedNavigator.Buttons.PrevPage.Visible = False
        Me.gc2.Location = New System.Drawing.Point(439, 204)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteBancoRemesa, Me.deFechaDet})
        Me.gc2.Size = New System.Drawing.Size(580, 114)
        Me.gc2.TabIndex = 87
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcNumRemesa, Me.gcBancoRemesa, Me.gcValorRemesa, Me.GridColumn3, Me.GridColumn4})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsNavigation.AutoFocusNewRow = True
        Me.gv2.OptionsNavigation.EnterMoveNextColumn = True
        Me.gv2.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv2.OptionsView.ShowFooter = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'gcNumRemesa
        '
        Me.gcNumRemesa.Caption = "Número"
        Me.gcNumRemesa.FieldName = "Numero"
        Me.gcNumRemesa.Name = "gcNumRemesa"
        Me.gcNumRemesa.Visible = True
        Me.gcNumRemesa.VisibleIndex = 1
        Me.gcNumRemesa.Width = 87
        '
        'gcBancoRemesa
        '
        Me.gcBancoRemesa.Caption = "Banco"
        Me.gcBancoRemesa.ColumnEdit = Me.riteBancoRemesa
        Me.gcBancoRemesa.FieldName = "IdBanco"
        Me.gcBancoRemesa.Name = "gcBancoRemesa"
        Me.gcBancoRemesa.Visible = True
        Me.gcBancoRemesa.VisibleIndex = 2
        Me.gcBancoRemesa.Width = 305
        '
        'riteBancoRemesa
        '
        Me.riteBancoRemesa.AutoHeight = False
        Me.riteBancoRemesa.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteBancoRemesa.Name = "riteBancoRemesa"
        '
        'gcValorRemesa
        '
        Me.gcValorRemesa.Caption = "Valor"
        Me.gcValorRemesa.FieldName = "Valor"
        Me.gcValorRemesa.Name = "gcValorRemesa"
        Me.gcValorRemesa.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor", "{0:c}")})
        Me.gcValorRemesa.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.gcValorRemesa.Visible = True
        Me.gcValorRemesa.VisibleIndex = 3
        Me.gcValorRemesa.Width = 71
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "GridColumn3"
        Me.GridColumn3.FieldName = "Banco"
        Me.GridColumn3.Name = "GridColumn3"
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Fecha Remesar"
        Me.GridColumn4.ColumnEdit = Me.deFechaDet
        Me.GridColumn4.FieldName = "FechaRemesar"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 0
        Me.GridColumn4.Width = 88
        '
        'deFechaDet
        '
        Me.deFechaDet.AutoHeight = False
        Me.deFechaDet.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaDet.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaDet.Name = "deFechaDet"
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(1022, 80)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 23)
        Me.cmdBorrar.TabIndex = 89
        '
        'cmdBorrar2
        '
        Me.cmdBorrar2.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar2.Location = New System.Drawing.Point(1022, 243)
        Me.cmdBorrar2.Name = "cmdBorrar2"
        Me.cmdBorrar2.Size = New System.Drawing.Size(27, 23)
        Me.cmdBorrar2.TabIndex = 90
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(443, 322)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(132, 13)
        Me.LabelControl9.TabIndex = 92
        Me.LabelControl9.Text = "FACTURACIÓN CON POS:"
        '
        'gc3
        '
        Me.gc3.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.gc3.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.gc3.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.gc3.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.gc3.EmbeddedNavigator.Buttons.First.Visible = False
        Me.gc3.EmbeddedNavigator.Buttons.Last.Visible = False
        Me.gc3.EmbeddedNavigator.Buttons.Next.Visible = False
        Me.gc3.EmbeddedNavigator.Buttons.NextPage.Visible = False
        Me.gc3.EmbeddedNavigator.Buttons.Prev.Visible = False
        Me.gc3.EmbeddedNavigator.Buttons.PrevPage.Visible = False
        Me.gc3.Location = New System.Drawing.Point(439, 341)
        Me.gc3.MainView = Me.gv3
        Me.gc3.Name = "gc3"
        Me.gc3.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteBancoVaucher})
        Me.gc3.Size = New System.Drawing.Size(580, 125)
        Me.gc3.TabIndex = 91
        Me.gc3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv3})
        '
        'gv3
        '
        Me.gv3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcNumVaucher, Me.gcBancoVaucher, Me.gcValorVaucher, Me.GridColumn7})
        Me.gv3.GridControl = Me.gc3
        Me.gv3.Name = "gv3"
        Me.gv3.OptionsNavigation.AutoFocusNewRow = True
        Me.gv3.OptionsNavigation.EnterMoveNextColumn = True
        Me.gv3.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv3.OptionsView.ShowFooter = True
        Me.gv3.OptionsView.ShowGroupPanel = False
        '
        'gcNumVaucher
        '
        Me.gcNumVaucher.Caption = "Número"
        Me.gcNumVaucher.FieldName = "Numero"
        Me.gcNumVaucher.Name = "gcNumVaucher"
        Me.gcNumVaucher.Visible = True
        Me.gcNumVaucher.VisibleIndex = 0
        Me.gcNumVaucher.Width = 87
        '
        'gcBancoVaucher
        '
        Me.gcBancoVaucher.Caption = "Banco"
        Me.gcBancoVaucher.ColumnEdit = Me.riteBancoVaucher
        Me.gcBancoVaucher.FieldName = "IdBanco"
        Me.gcBancoVaucher.Name = "gcBancoVaucher"
        Me.gcBancoVaucher.Visible = True
        Me.gcBancoVaucher.VisibleIndex = 2
        Me.gcBancoVaucher.Width = 322
        '
        'riteBancoVaucher
        '
        Me.riteBancoVaucher.AutoHeight = False
        Me.riteBancoVaucher.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteBancoVaucher.Name = "riteBancoVaucher"
        '
        'gcValorVaucher
        '
        Me.gcValorVaucher.Caption = "Valor"
        Me.gcValorVaucher.FieldName = "Valor"
        Me.gcValorVaucher.Name = "gcValorVaucher"
        Me.gcValorVaucher.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor", "{0:c}")})
        Me.gcValorVaucher.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.gcValorVaucher.Visible = True
        Me.gcValorVaucher.VisibleIndex = 1
        Me.gcValorVaucher.Width = 71
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "GridColumn3"
        Me.GridColumn7.FieldName = "Banco"
        Me.GridColumn7.Name = "GridColumn7"
        '
        'cmdBorrar3
        '
        Me.cmdBorrar3.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar3.Location = New System.Drawing.Point(1022, 373)
        Me.cmdBorrar3.Name = "cmdBorrar3"
        Me.cmdBorrar3.Size = New System.Drawing.Size(27, 23)
        Me.cmdBorrar3.TabIndex = 93
        '
        'sbRevierteCierre
        '
        Me.sbRevierteCierre.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.sbRevierteCierre.Location = New System.Drawing.Point(357, 27)
        Me.sbRevierteCierre.Name = "sbRevierteCierre"
        Me.sbRevierteCierre.Size = New System.Drawing.Size(75, 23)
        Me.sbRevierteCierre.TabIndex = 7
        Me.sbRevierteCierre.Text = "Revertir"
        '
        'sbObtener
        '
        Me.sbObtener.Location = New System.Drawing.Point(188, 27)
        Me.sbObtener.Name = "sbObtener"
        Me.sbObtener.Size = New System.Drawing.Size(65, 23)
        Me.sbObtener.TabIndex = 5
        Me.sbObtener.Text = "Obtener"
        '
        'fac_frmCorteCajaGeneral
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(1204, 515)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmCorteCajaGeneral"
        Me.Text = "Corte Caja"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcArqueo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvArqueo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteBancoCheque, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteBancoRM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteBancoRemesa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaDet.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaDet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteBancoVaucher, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lePuntoVenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbCerrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents leVendedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gcArqueo As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvArqueo As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcValor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcNumCheque As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcBanco As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorCheque As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcReserva As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcBancoRem As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteBancoCheque As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents riteBancoRM As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcNumRemesa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcBancoRemesa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteBancoRemesa As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents gcValorRemesa As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmdBorrar2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmdBorrar3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcNumVaucher As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcBancoVaucher As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteBancoVaucher As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents gcValorVaucher As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents sbRevierteCierre As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbObtener As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents deFechaDet As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
End Class
