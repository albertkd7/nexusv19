﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmLiquidacionImpuestos
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.spAnio = New DevExpress.XtraEditors.SpinEdit
        Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.btExportXLS = New DevExpress.XtraEditors.SimpleButton
        Me.teProgreso = New DevExpress.XtraEditors.TextEdit
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.spAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teProgreso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.teProgreso)
        Me.GroupControl1.Controls.Add(Me.btExportXLS)
        Me.GroupControl1.Controls.Add(Me.spAnio)
        Me.GroupControl1.Controls.Add(Me.meMes)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Size = New System.Drawing.Size(826, 345)
        '
        'spAnio
        '
        Me.spAnio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spAnio.Location = New System.Drawing.Point(287, 47)
        Me.spAnio.Name = "spAnio"
        Me.spAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.spAnio.Properties.Mask.EditMask = "####"
        Me.spAnio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.spAnio.Size = New System.Drawing.Size(61, 20)
        Me.spAnio.TabIndex = 1
        '
        'meMes
        '
        Me.meMes.Location = New System.Drawing.Point(181, 47)
        Me.meMes.Name = "meMes"
        Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes.Size = New System.Drawing.Size(100, 20)
        Me.meMes.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(66, 50)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(105, 13)
        Me.LabelControl1.TabIndex = 28
        Me.LabelControl1.Text = "Mes y Año a Generar:"
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "LIQUIDACION DE IMPUESTOS"
        Me.teTitulo.Location = New System.Drawing.Point(181, 73)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(365, 20)
        Me.teTitulo.TabIndex = 2
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(181, 106)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(301, 20)
        Me.leSucursal.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(85, 76)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl2.TabIndex = 25
        Me.LabelControl2.Text = "Título del reporte:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(127, 109)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl3.TabIndex = 27
        Me.LabelControl3.Text = "Sucursal:"
        '
        'btExportXLS
        '
        Me.btExportXLS.Location = New System.Drawing.Point(181, 132)
        Me.btExportXLS.Name = "btExportXLS"
        Me.btExportXLS.Size = New System.Drawing.Size(92, 23)
        Me.btExportXLS.TabIndex = 63
        Me.btExportXLS.Text = "&Exportar a Excel"
        Me.btExportXLS.Visible = False
        '
        'teProgreso
        '
        Me.teProgreso.EditValue = ""
        Me.teProgreso.Location = New System.Drawing.Point(279, 135)
        Me.teProgreso.Name = "teProgreso"
        Me.teProgreso.Size = New System.Drawing.Size(265, 20)
        Me.teProgreso.TabIndex = 64
        Me.teProgreso.Visible = False
        '
        'fac_frmLiquidacionImpuestos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(826, 370)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmLiquidacionImpuestos"
        Me.Text = "Liquidación de Impuestos"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.spAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teProgreso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents spAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents btExportXLS As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teProgreso As DevExpress.XtraEditors.TextEdit

End Class
