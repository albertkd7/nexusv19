﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmVentasTrimestrales

    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim dt As New DataTable

    Private Sub fac_frmVentasProductosComparacionAnual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim Mes As Integer = Today.Month, Ejercicio As Integer = Today.Year
        CargaCombos()
        seEjercicio1.EditValue = Ejercicio
        seEjercicio2.EditValue = Ejercicio
    End Sub
    Private Sub CargaCombos()
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS --")
        objCombos.fac_PuntosVenta(lePuntoVenta, 1, "-- TODOS --")
    End Sub

    Private Sub sbAceptar_Click() Handles Me.Reporte
        dt = bl.fac_VentasProductosComparacionAnual(seEjercicio1.EditValue, seEjercicio2.EditValue, BeProducto1.beCodigo.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)
        Dim rpt As New fac_rptVentasProductosComparacionAnual() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, PUNTO DE VENTA: {1}", teTitulo.EditValue, lePuntoVenta.Text)
        rpt.xrlPeriodo.Text = "AÑOS COMPARADOS:  " + seEjercicio1.EditValue.ToString + " CON " + seEjercicio2.EditValue.ToString
        rpt.xrlValor1.Text = "AÑO:  " + seEjercicio1.EditValue.ToString
        rpt.xrlValor2.Text = "AÑO:  " + seEjercicio2.EditValue.ToString
        rpt.xrlDiferencia.Text = "DIFERENCIAS  (+/-)"
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub

End Class
