﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraReports
Imports DevExpress.XtraEditors
Imports DevExpress.LookAndFeel

Public Class fac_frmCorteCajaGeneral
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Dim ds As New DataSet

    Private Sub fac_frmCorteCajaGeneral_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.banCuentasBancarias(riteBancoCheque)
        objCombos.banCuentasBancarias(riteBancoRemesa)
        objCombos.banCuentasBancarias(riteBancoRM)
        objCombos.banCuentasBancarias(riteBancoVaucher)
        deDesde.EditValue = Today

        leSucursal.EditValue = piIdSucursalUsuario
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "")
        objCombos.fac_Vendedores(leVendedor, "-- TODOS LOS VENDEDORES --")

        ActualizaGrid()
        VerificaFechaActiva()
    End Sub


    Private Sub sbAceptar_Click() Handles Me.Reporte

        'ActualizaGrid()
        'VerificaFechaActiva()

        'If teTotal.EditValue = 0 Then
        '    MsgBox("DEBE GUARDAR EL CORTE ANTES DE PROCEDER A IMPRIMIR", MsgBoxStyle.Critical, "INVALIDO!")
        '    Exit Sub
        'End If

        Dim TotalCheques As Decimal = 0.0, TotalRemesas As Decimal = 0.0, TotalVaucher As Decimal = 0.0
        gv.UpdateTotalSummary()
        TotalCheques = Me.gcValorCheque.SummaryItem.SummaryValue

        gv2.UpdateTotalSummary()
        TotalRemesas = Me.gcValorRemesa.SummaryItem.SummaryValue

        gv3.UpdateTotalSummary()
        TotalVaucher = Me.gcValorVaucher.SummaryItem.SummaryValue

        ''verifico si el ultimo corte esta abierto o no
        'Dim Cerrado As Integer = bl.fac_VerificaCorteCajaActivo(deDesde.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)

        'If Cerrado = 0 Then
        '    MsgBox("Existe un corte de caja con fecha anterior activo", 16, "Imposible Continuar")
        '    Exit Sub
        'End If

        ds = bl.fac_CorteCaja(deDesde.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, leVendedor.EditValue, teTotal.EditValue, TotalCheques, TotalRemesas, TotalVaucher)

        Dim rpt As New fac_rptCorteCajaGeneral() With {.DataSource = Nothing, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, PUNTO DE VENTA: {1}", teTitulo.EditValue, lePuntoVenta.Text)
        rpt.xrlVendedor.Text = "Vendedor: " & leVendedor.Text
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deDesde.EditValue)

        rpt.XrSubreport1.ReportSource.DataSource = ds.Tables(0)
        rpt.XrSubreport1.ReportSource.DataMember = ""

        rpt.XrSubreport2.ReportSource.DataSource = ds.Tables(1)
        rpt.XrSubreport2.ReportSource.DataMember = ""

        rpt.XrSubreport3.ReportSource.DataSource = ds.Tables(2)
        rpt.XrSubreport3.ReportSource.DataMember = ""

        rpt.XrSubreport4.ReportSource.DataSource = ds.Tables(3)
        rpt.XrSubreport4.ReportSource.DataMember = ""

        rpt.XrSubreport6.ReportSource.DataSource = ds.Tables(4)
        rpt.XrSubreport6.ReportSource.DataMember = ""

        'productos
        'rpt.XrSubreport5.ReportSource.DataSource = ds.Tables(5)
        'rpt.XrSubreport5.ReportSource.DataMember = ""

        rpt.XrSubreport7.ReportSource.DataSource = ds.Tables(6)
        rpt.XrSubreport7.ReportSource.DataMember = ""

        rpt.XrSubreport8.ReportSource.DataSource = gcArqueo.DataSource   'ds.Tables(7)
        rpt.XrSubreport8.ReportSource.DataMember = ""

        If gv.RowCount > 0 Then
            rpt.XrSubreport9.ReportSource.DataSource = gc.DataSource
            rpt.XrSubreport9.ReportSource.DataMember = ""
        End If

        If gv2.RowCount > 0 Then
            rpt.XrSubreport10.ReportSource.DataSource = gc2.DataSource
            rpt.XrSubreport10.ReportSource.DataMember = ""
        End If
        If gv3.RowCount > 0 Then
            rpt.XrSubreport5.ReportSource.DataSource = gc3.DataSource
            rpt.XrSubreport5.ReportSource.DataMember = ""
        End If

        rpt.ShowPreviewDialog()
        If MessageBox.Show("Desea guardar el corte de Caja mostrado?" & vbCr & "Advertencia: Porfavor no olvide guardar sus datos, de lo contrario el dia quedara en blanco.", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = MsgBoxResult.Yes Then
            sbCerrar_Click(Nothing, New EventArgs)
        End If
    End Sub

    Private Sub sbCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCerrar.Click

        Dim Fecha As Date = objFunciones.GetFechaContableCaja(leSucursal.EditValue, lePuntoVenta.EditValue)
        If Fecha <> deDesde.EditValue Then
            MsgBox("Esta fecha ya fue cerrada anteriormente ó aún no esta activa", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If

        If MsgBox("¿Está seguro(a) de cerrar definitivamente el corte de caja?", 292, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        If MsgBox("Confirme nuevamente", 292, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        ds = bl.fac_CorteCaja(deDesde.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, leVendedor.EditValue, teTotal.EditValue, 0, 0, 0)
        If teTotal.EditValue <> SiEsNulo(ds.Tables(2).Rows(0).Item("Remesas"), 0.0) Then
            If MsgBox("El Monto del Arqueo es diferente al monto a remesar del corte de Caja" + Chr(13) + "¿Está seguro(a) de cerrar definitivamente el corte de caja?", 292, "Confirme") = MsgBoxResult.No Then
                Exit Sub
            End If
        End If

        Try
            Dim entArqueo = New List(Of fac_CorteCajaArqueo)
            For i = 0 To gvArqueo.DataRowCount - 1
                Dim entDetalleA As New fac_CorteCajaArqueo
                With entDetalleA
                    .IdSucursal = leSucursal.EditValue
                    .IdPunto = lePuntoVenta.EditValue
                    .Fecha = deDesde.EditValue
                    .IdDenominacion = gvArqueo.GetRowCellValue(i, "IdDenominacion")
                    .Cantidad = IsNULL(gvArqueo.GetRowCellValue(i, "Cantidad"), 0)
                    .Total = IsNULL(gvArqueo.GetRowCellValue(i, "Total"), 0)
                End With
                entArqueo.Add(entDetalleA)
            Next

            Dim entArqueoCheques = New List(Of fac_CorteCajaCheques)
            For i = 0 To gv.DataRowCount - 1
                Dim entDetalleA As New fac_CorteCajaCheques
                With entDetalleA
                    .IdSucursal = leSucursal.EditValue
                    .IdPunto = lePuntoVenta.EditValue
                    .Fecha = deDesde.EditValue
                    .NumCheque = IsNULL(gv.GetRowCellValue(i, "NumCheque"), " ")
                    .Cliente = IsNULL(gv.GetRowCellValue(i, "Cliente"), " ")
                    .Valor = IsNULL(gv.GetRowCellValue(i, "Valor"), 0)
                    .IdBanco = IsNULL(gv.GetRowCellValue(i, "IdBanco"), -1)
                    .NumReserva = IsNULL(gv.GetRowCellValue(i, "NumReserva"), " ")
                    .IdBancoRemesa = IsNULL(gv.GetRowCellValue(i, "IdBancoRemesa"), -1)
                End With
                entArqueoCheques.Add(entDetalleA)
            Next

            Dim entArqueoRemesas = New List(Of fac_CorteCajaRemesas)
            For i = 0 To gv2.DataRowCount - 1
                Dim entDetalleA As New fac_CorteCajaRemesas
                With entDetalleA
                    .IdSucursal = leSucursal.EditValue
                    .IdPunto = lePuntoVenta.EditValue
                    .Fecha = deDesde.EditValue
                    .Numero = IsNULL(gv2.GetRowCellValue(i, "Numero"), " ")
                    .Valor = IsNULL(gv2.GetRowCellValue(i, "Valor"), 0)
                    .IdBanco = IsNULL(gv2.GetRowCellValue(i, "IdBanco"), -1)
                    .FechaRemesar = gv2.GetRowCellValue(i, "FechaRemesar")
                End With
                entArqueoRemesas.Add(entDetalleA)
            Next

            Dim entArqueoVaucher = New List(Of fac_CorteCajaPOS)
            For i = 0 To gv3.DataRowCount - 1
                Dim entDetalleA As New fac_CorteCajaPOS
                With entDetalleA
                    .IdSucursal = leSucursal.EditValue
                    .IdPunto = lePuntoVenta.EditValue
                    .Fecha = deDesde.EditValue
                    .Numero = IsNULL(gv3.GetRowCellValue(i, "Numero"), " ")
                    .Valor = IsNULL(gv3.GetRowCellValue(i, "Valor"), 0)
                    .IdBanco = IsNULL(gv3.GetRowCellValue(i, "IdBanco"), -1)
                End With
                entArqueoVaucher.Add(entDetalleA)
            Next

            Dim CountError As Integer = entArqueoVaucher.Where(Function(x) x.IdBanco < 0).Count
            If CountError > 0 Then
                MsgBox("Es obligatorio seleccionar Banco, en Facturación con POS", MsgBoxStyle.Exclamation, "Nota")
                Exit Sub
            End If

            Dim msgOK = blAdmon.adm_CierreSucursal(leSucursal.EditValue, deDesde.EditValue, entArqueo, entArqueoCheques, entArqueoRemesas, entArqueoVaucher, objMenu.User, lePuntoVenta.EditValue)
            If msgOK = "Ok" Then
                MsgBox("El corte fue guardado con éxito", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox("No fue posible guardar el corte de caja" + Chr(13) + msgOK, 16, "Verifique el error")
            End If

        Catch ex As Exception
            MsgBox("No fue posible guardar el corte de caja" + Chr(13) +
            ex.Message(), 16, "Verifique el error")
            Exit Sub
        End Try
        VerificaFechaActiva()
    End Sub

    Public Function IsNULL(ByVal val As Object, ByVal def As Object) As Object
        If val = Nothing Then
            Return def
        Else
            Return val
        End If
    End Function

    Private Sub gvArqueo_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gvArqueo.RowUpdated
        CalcularTotales()
    End Sub
    Private Sub CalcularTotales()
        If deDesde.EditValue = Nothing Then
            Exit Sub
        End If
        gvArqueo.SetRowCellValue(gvArqueo.FocusedRowHandle, "Total", gvArqueo.GetRowCellValue(gvArqueo.FocusedRowHandle, "Cantidad") * gvArqueo.GetRowCellValue(gvArqueo.FocusedRowHandle, "Valor"))
        gvArqueo.UpdateTotalSummary()
        teTotal.EditValue = Me.gcTotal.SummaryItem.SummaryValue
    End Sub
    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "")     
    End Sub

    Private Sub ActualizaGrid()
        Dim ds As DataSet = bl.GetArqueoCaja(leSucursal.EditValue, lePuntoVenta.EditValue, deDesde.EditValue)
        gcArqueo.DataSource = ds.Tables(0)
        gc.DataSource = ds.Tables(1)
        gc2.DataSource = ds.Tables(2)
        gc3.DataSource = ds.Tables(3)
    End Sub
    Private Sub VerificaFechaActiva()
        Dim Fecha As Date = SiEsNulo(objFunciones.GetFechaContableCaja(leSucursal.EditValue, lePuntoVenta.EditValue), Today)
        Dim _Enable As Boolean = False

        If Fecha <> deDesde.EditValue Then
            _Enable = True
            gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None
            gv2.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None
            gv3.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None
        Else
            gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
            gv2.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
            gv3.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        End If

        Me.gcCantidad.OptionsColumn.ReadOnly = _Enable
        Me.gcValorCheque.OptionsColumn.ReadOnly = _Enable
        Me.gcNumCheque.OptionsColumn.ReadOnly = _Enable
        Me.gcBanco.OptionsColumn.ReadOnly = _Enable
        Me.gcBancoRem.OptionsColumn.ReadOnly = _Enable
        Me.gcReserva.OptionsColumn.ReadOnly = _Enable
        Me.gcValorRemesa.OptionsColumn.ReadOnly = _Enable
        Me.gcValorRemesa.OptionsColumn.ReadOnly = _Enable
        Me.gcBancoRemesa.OptionsColumn.ReadOnly = _Enable
        Me.gcNumRemesa.OptionsColumn.ReadOnly = _Enable
        Me.gcCliente.OptionsColumn.ReadOnly = _Enable

        Me.gcNumVaucher.OptionsColumn.ReadOnly = _Enable
        Me.gcValorVaucher.OptionsColumn.ReadOnly = _Enable
        Me.gcBancoVaucher.OptionsColumn.ReadOnly = _Enable

        CalcularTotales()
    End Sub

    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "NumCheque", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdBanco", 0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Valor", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cliente", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "NumReserva", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdBancoRemesa", 0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Banco", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "BancoRM", "")
    End Sub

    Private Sub gv2_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv2.InitNewRow
        gv2.SetRowCellValue(gv2.FocusedRowHandle, "Numero", "")
        gv2.SetRowCellValue(gv2.FocusedRowHandle, "IdBanco", 0)
        gv2.SetRowCellValue(gv2.FocusedRowHandle, "Valor", 0.0)
        gv2.SetRowCellValue(gv2.FocusedRowHandle, "Banco", "")
    End Sub
    Private Sub gv3_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv3.InitNewRow
        gv3.SetRowCellValue(gv3.FocusedRowHandle, "Numero", "")
        gv3.SetRowCellValue(gv3.FocusedRowHandle, "IdBanco", 0)
        gv3.SetRowCellValue(gv3.FocusedRowHandle, "Valor", 0.0)
        gv3.SetRowCellValue(gv3.FocusedRowHandle, "Banco", "")
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        Dim Fecha As Date = SiEsNulo(objFunciones.GetFechaContableCaja(leSucursal.EditValue, lePuntoVenta.EditValue), Today)
        If Fecha <> deDesde.EditValue Then
            Exit Sub
        End If
        gv.DeleteRow(gv.FocusedRowHandle)
        'CalcularTotales()
    End Sub

    Private Sub cmdBorrar2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar2.Click
        Dim Fecha As Date = SiEsNulo(objFunciones.GetFechaContableCaja(leSucursal.EditValue, lePuntoVenta.EditValue), Today)
        If Fecha <> deDesde.EditValue Then
            Exit Sub
        End If
        gv2.DeleteRow(gv2.FocusedRowHandle)
        'CalcularTotales()
    End Sub
    Private Sub cmdBorrar3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar3.Click
        Dim Fecha As Date = SiEsNulo(objFunciones.GetFechaContableCaja(leSucursal.EditValue, lePuntoVenta.EditValue), Today)
        If Fecha <> deDesde.EditValue Then
            Exit Sub
        End If
        gv3.DeleteRow(gv3.FocusedRowHandle)
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Select Case gv.FocusedColumn.FieldName
            Case "IdBanco"
                Dim Banco As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(e.Value)
                gv.SetFocusedRowCellValue("Banco", SiEsNulo(Banco.Nombre, ""))
            Case "IdBancoRemesa"
                Dim Banco As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(e.Value)
                gv.SetFocusedRowCellValue("BancoRM", SiEsNulo(Banco.Nombre, ""))
        End Select

    End Sub

    Private Sub gv2_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv2.ValidatingEditor
        Select Case gv2.FocusedColumn.FieldName
            Case "IdBanco"
                Dim Banco As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(e.Value)
                gv2.SetFocusedRowCellValue("Banco", SiEsNulo(Banco.Nombre, ""))
        End Select
    End Sub

    Private Sub gv3_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv3.ValidatingEditor
        Select Case gv3.FocusedColumn.FieldName
            Case "IdBanco"
                Dim Banco As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(e.Value)
                gv3.SetFocusedRowCellValue("Banco", SiEsNulo(Banco.Nombre, ""))
        End Select
    End Sub

    Private Sub sbRevierteCierre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbRevierteCierre.Click
        Dim FechaNueva As Date = Today
        fac_frmPideFechaRevierteCierre.deFechaNew.EditValue = objFunciones.GetFechaContableCaja(leSucursal.EditValue, lePuntoVenta.EditValue)
        fac_frmPideFechaRevierteCierre.ShowDialog()
        FechaNueva = fac_frmPideFechaRevierteCierre.deFechaNew.EditValue

        If MsgBox("Está seguro de revertir el corte de caja de fecha: " + Format(FechaNueva, "dd/MM/yyyy"), MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        blAdmon.adm_RevierteCierreSucursal(leSucursal.EditValue, FechaNueva, objMenu.User, lePuntoVenta.EditValue)
        MsgBox("El corte fue revertido con éxito", MsgBoxStyle.Information, "Nota")
    End Sub

    Private Sub sbObtener_Click(sender As Object, e As EventArgs) Handles sbObtener.Click
        ActualizaGrid()
        VerificaFechaActiva()
    End Sub
End Class
