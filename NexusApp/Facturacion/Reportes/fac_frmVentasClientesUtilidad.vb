﻿Imports NexusBLL

Public Class fac_frmVentasClientesUtilidad
    Dim bl As New FacturaBLL(g_ConnectionString)

    Private Sub fac_frmVentasVendedorUtilidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        leSucursal.EditValue = piIdSucursal
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "")

        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        BeCliente1.beCodigo.EditValue = ""
        deDesde.EditValue = Today
        deHasta.EditValue = Today
    End Sub

    Private Sub fac_frmVentasVendedorUtilidad_Reporte() Handles Me.Reporte
        Dim dt As DataTable = bl.fac_VentasClientesUtilidad(deDesde.EditValue, deHasta.EditValue, leBodega.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, BeCliente1.beCodigo.EditValue)

        Dim rpt3 As New fac_rptClientesUtilidadConsolidado() With {.DataSource = dt, .DataMember = ""}
        rpt3.xrlEmpresa.Text = gsNombre_Empresa
        rpt3.xrlMoneda.Text = gsDesc_Moneda
        rpt3.xrlBodegas.Text = leBodega.Text
        rpt3.xrlTitulo.Text = "VENTAS POR CLIENTES Y UTILIDAD, SUCURSAL: " + leSucursal.Text + ", PUNTO DE VENTA: " + lePuntoVenta.Text
        rpt3.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt3.ShowPreviewDialog()

    End Sub
    Private Sub leSucursal_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub
End Class
