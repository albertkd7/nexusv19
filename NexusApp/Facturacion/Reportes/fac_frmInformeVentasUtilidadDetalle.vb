﻿Imports NexusBLL
Public Class fac_frmInformeVentasUtilidadDetalle
    Dim bl As New InventarioBLL(g_ConnectionString)

    Private Sub inv_frmInformeVentasUtilidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
        objCombos.invSubGrupos(leSubGrupo, -1, "-- TODOS LOS SUB-GRUPOS --")
    End Sub

    Private Sub inv_frmInformeVentasUtilidad_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable
        If chkPorGrupo.Checked = False Then
            dt = bl.inv_UtilidadesVentasdetalle(deDesde.EditValue, deHasta.EditValue, leBodega.EditValue, BeCliente1.beCodigo.EditValue, leGrupo.EditValue, leSubGrupo.EditValue, chkPorGrupo.Checked, seUtilidad.EditValue)
            Dim rpt As New fac_rptUtilidadesVentasDetalle() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
            rpt.xrlTitulo.Text = "INFORME DE VENTAS Y UTILIDAD"
            rpt.ShowPreview()
        Else
            dt = bl.inv_UtilidadesVentasdetalle(deDesde.EditValue, deHasta.EditValue, leBodega.EditValue, BeCliente1.beCodigo.EditValue, leGrupo.EditValue, leSubGrupo.EditValue, chkPorGrupo.Checked, seUtilidad.EditValue)
            Dim rpt As New inv_rptUtilidadesVentas() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.XrLabel16.Visible = False
            rpt.XrLabel18.Visible = False
            rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
            rpt.xrlTitulo.Text = "INFORME DE VENTAS Y UTILIDAD POR GRUPO"
            rpt.ShowPreview()
        End If

    End Sub

    Private Sub ChUtilidadMenor_CheckedChanged(sender As Object, e As EventArgs) Handles ChUtilidadMenor.CheckedChanged
        If ChUtilidadMenor.Checked = False Then
            seUtilidad.Visible = False
            seUtilidad.EditValue = 0
        Else
            seUtilidad.Visible = True
        End If
    End Sub
End Class
