﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmVentasProductosComparacionMensual
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmVentasProductosComparacionMensual))
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.lePuntoVenta = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.BeProducto1 = New Nexus.beProducto()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.seAnio2 = New DevExpress.XtraEditors.SpinEdit()
        Me.seAnio1 = New DevExpress.XtraEditors.SpinEdit()
        Me.meMes2 = New DevExpress.XtraScheduler.UI.MonthEdit()
        Me.meMes1 = New DevExpress.XtraScheduler.UI.MonthEdit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seAnio2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seAnio1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.seAnio2)
        Me.GroupControl1.Controls.Add(Me.seAnio1)
        Me.GroupControl1.Controls.Add(Me.meMes2)
        Me.GroupControl1.Controls.Add(Me.meMes1)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.BeProducto1)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.lePuntoVenta)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Size = New System.Drawing.Size(668, 339)
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "COMPARACION DE VENTAS MENSUALES"
        Me.teTitulo.Location = New System.Drawing.Point(106, 196)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(335, 20)
        Me.teTitulo.TabIndex = 6
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(16, 199)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl6.TabIndex = 24
        Me.LabelControl6.Text = "Título del informe:"
        '
        'lePuntoVenta
        '
        Me.lePuntoVenta.Location = New System.Drawing.Point(106, 149)
        Me.lePuntoVenta.Name = "lePuntoVenta"
        Me.lePuntoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePuntoVenta.Size = New System.Drawing.Size(335, 20)
        Me.lePuntoVenta.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(25, 152)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl5.TabIndex = 23
        Me.LabelControl5.Text = "Punto de Venta:"
        '
        'BeProducto1
        '
        Me.BeProducto1.Location = New System.Drawing.Point(49, 92)
        Me.BeProducto1.Name = "BeProducto1"
        Me.BeProducto1.Size = New System.Drawing.Size(612, 25)
        Me.BeProducto1.TabIndex = 3
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(106, 126)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(335, 20)
        Me.leSucursal.TabIndex = 4
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(59, 130)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl4.TabIndex = 28
        Me.LabelControl4.Text = "Sucursal:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(32, 69)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl1.TabIndex = 36
        Me.LabelControl1.Text = "Comparar con:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(21, 43)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl2.TabIndex = 35
        Me.LabelControl2.Text = "Mes a Comparar:"
        '
        'seAnio2
        '
        Me.seAnio2.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seAnio2.EnterMoveNextControl = True
        Me.seAnio2.Location = New System.Drawing.Point(208, 66)
        Me.seAnio2.Name = "seAnio2"
        Me.seAnio2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seAnio2.Properties.Mask.EditMask = "n0"
        Me.seAnio2.Size = New System.Drawing.Size(69, 20)
        Me.seAnio2.TabIndex = 34
        '
        'seAnio1
        '
        Me.seAnio1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seAnio1.EnterMoveNextControl = True
        Me.seAnio1.Location = New System.Drawing.Point(208, 40)
        Me.seAnio1.Name = "seAnio1"
        Me.seAnio1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seAnio1.Properties.Mask.EditMask = "n0"
        Me.seAnio1.Properties.Mask.IgnoreMaskBlank = False
        Me.seAnio1.Size = New System.Drawing.Size(69, 20)
        Me.seAnio1.TabIndex = 32
        '
        'meMes2
        '
        Me.meMes2.EnterMoveNextControl = True
        Me.meMes2.Location = New System.Drawing.Point(106, 66)
        Me.meMes2.Name = "meMes2"
        Me.meMes2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes2.Size = New System.Drawing.Size(100, 20)
        Me.meMes2.TabIndex = 33
        '
        'meMes1
        '
        Me.meMes1.EnterMoveNextControl = True
        Me.meMes1.Location = New System.Drawing.Point(106, 40)
        Me.meMes1.Name = "meMes1"
        Me.meMes1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes1.Size = New System.Drawing.Size(100, 20)
        Me.meMes1.TabIndex = 31
        '
        'fac_frmVentasProductosComparacionMensual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(668, 367)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmVentasProductosComparacionMensual"
        Me.Text = "Comparación de Ventas Mensual"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seAnio2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seAnio1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lePuntoVenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BeProducto1 As Nexus.beProducto
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seAnio2 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seAnio1 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents meMes2 As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents meMes1 As DevExpress.XtraScheduler.UI.MonthEdit

End Class
