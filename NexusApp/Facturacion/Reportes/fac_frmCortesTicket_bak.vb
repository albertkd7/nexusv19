﻿Imports NexusBLL
Imports System.Drawing.Printing
Imports NexusELL.TableEntities
Public Class fac_frmCortesTicket_bak
    Dim bl As New FacturaBLL(g_ConnectionString), blAdmon As New AdmonBLL(g_ConnectionString)
    Dim DatosEncabezado As String = "", DatosPie As String = "", entidadTicket As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(10)

    Private Sub fac_frmCorteCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deCorteX.EditValue = Now
        deCorteZ.EditValue = Now
        deGranTotal.EditValue = Today
        bl.fac_ObtenerDatosEncabezadoPieTicket(DatosEncabezado, DatosPie, lePuntoVenta.EditValue)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue)
    End Sub

    Private Sub fac_frmCorteCaja_Report_Click() Handles Me.Reporte
        Dim str As String = "", Ancho As Integer = 0, s As String = ""

        Dim dt As New DataTable
        If RadioGroup1.EditValue = 0 Then
            dt = bl.fac_CorteX(deCorteX.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)
            s = "CORTE X --> " & deCorteX.EditValue.ToString()
        End If
        If RadioGroup1.EditValue = 1 Then
            dt = bl.fac_CorteZ(deCorteZ.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)
            s = "CORTE Z --> " & deCorteZ.EditValue.ToString()
        End If
        If RadioGroup1.EditValue = 2 Then
            dt = bl.fac_CorteGranTotalZ(deGranTotal.DateTime.Month, deGranTotal.DateTime.Year, leSucursal.EditValue, lePuntoVenta.EditValue)
            s = "GRAN TOTAL Z --> " & deGranTotal.DateTime.Month & "/" & deGranTotal.DateTime.Year '& "    " & Format(Now, "HH:mm:SS")
        End If
        str = Chr(27) & Chr(99) & Chr(48) & Chr(3) 'para que imprima en la cinta de auditoría
        str += Chr(27) + "z" + Chr(1)  'para activar el modo de impresión de la cinta
        str += DatosEncabezado & Chr(10) & Chr(10)
        str += s & Chr(10)
        For Each Fila As DataRow In dt.Rows
            s = Fila.Item("Concepto").ToString()
            str += s
            If Fila.Item("Imprimir") = 1 Then
                Ancho = 40 - Len(s)
                s = Format(Fila.Item("Total"), "$##,##0.00")
                str += s.PadLeft(Ancho, " ")
            End If
            str += Chr(10)
        Next
        str += Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10)
        str += Chr(27) + "z" + Chr(0)  'para desactivar el modo de impresión en la cinta
        str += Chr(27) + Chr(105)
        RawPrinterHelper.SendStringToPrinter(entidadTicket.NombreImpresor, str)
    End Sub

    Private Sub RadioGroup1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioGroup1.SelectedIndexChanged
        lblCorteX.Visible = RadioGroup1.EditValue = 0
        deCorteX.Visible = RadioGroup1.EditValue = 0
        lblCorteZ.Visible = RadioGroup1.EditValue = 1
        deCorteZ.Visible = RadioGroup1.EditValue = 1
        lblCorteTotal.Visible = RadioGroup1.EditValue = 2
        deGranTotal.Visible = RadioGroup1.EditValue = 2
        If RadioGroup1.EditValue = 0 Then
            teTitulo.EditValue = "CORTE X"
        End If
        If RadioGroup1.EditValue = 1 Then
            teTitulo.EditValue = "CORTE Z"
        End If
        If RadioGroup1.EditValue = 2 Then
            teTitulo.EditValue = "CORTE GRAN TOTAL Z"
        End If
    End Sub

    Private Sub cmdImprimirCinta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimirCinta.Click
        Dim str As String = ""
        str = Chr(27) & Chr(99) & Chr(48) & Chr(3)
        str += Chr(27) + "z" + Chr(1)  'para activar el modo de impresión de la cinta
        str += DatosEncabezado & Chr(10) & Chr(10)
        RawPrinterHelper.SendStringToPrinter(entidadTicket.NombreImpresor, str)
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue)
    End Sub
End Class
