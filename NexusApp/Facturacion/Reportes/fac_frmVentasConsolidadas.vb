﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class fac_frmVentasConsolidadas

    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim dt As New DataTable


    Private Sub frmFacReportes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        leSucursal.EditValue = piIdSucursal
        objCombos.fac_PuntosVenta(lePuntoVenta, 1, "-- TODOS --")
        deDesde.EditValue = CDate(String.Format("1/{0}/{1}", Month(Today), Year(Today)))
        deHasta.EditValue = Today

    End Sub
   

    Private Sub sbAceptar_Click() Handles Me.Reporte
        If rgTipo.EditValue = 1 Then
            Dim dt As DataTable = bl.fac_VentasClienteConsolidado(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, chkTop10.Checked)
            Dim rpt As New fac_rptVentasClienteConsolidado() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = String.Format("{0}, SUCURSAL: {1}, PUNTO DE VENTA: {2}", teTitulo.EditValue, leSucursal.Text, lePuntoVenta.Text)
            rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
            rpt.ShowPreviewDialog()
        Else
            Dim dt As DataTable = bl.fac_VentasProductoConsolidado(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, chkTop10.Checked)
            Dim rpt As New fac_rptVentasProductoConsolidado() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = String.Format("{0}, SUCURSAL: {1}, PUNTO DE VENTA: {2}", teTitulo.EditValue, leSucursal.EditValue, lePuntoVenta.Text)
            rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
            rpt.ShowPreviewDialog()
        End If

    End Sub


    Private Sub rgTipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgTipo.SelectedIndexChanged
        If rgTipo.EditValue = 1 Then
            teTitulo.EditValue = "INFORME DE VENTAS CONSOLIDADO POR CLIENTE"
        Else
            teTitulo.EditValue = "INFORME DE VENTAS CONSOLIDADO POR PRODUCTO"
        End If
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub
End Class
