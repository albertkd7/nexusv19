﻿Imports DevExpress.XtraReports.UI
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmListadoGestiones
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim dt As New DataTable


    Private Sub facVentasProducto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargaCombos()
        deDesde.EditValue = CDate("1/" & Month(Today) & "/" & Year(Today))
        deHasta.EditValue = Today
        teIdCotizacion.EditValue = 0
    End Sub
    Private Sub CargaCombos()
        objCombos.fac_Vendedores(leVendedor, "--TODOS LOS VENDEDORES--")
    End Sub

    Private Sub sbAceptar_Click() Handles Me.Reporte
        dt = bl.fac_GestionesPeriodos(deDesde.EditValue, deHasta.EditValue, BeCliente1.beCodigo.EditValue, leVendedor.EditValue, teIdCotizacion.EditValue)
        Dim rpt As New fac_rptGestionesPeriodos() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt.ShowPreviewDialog()
    End Sub


    Private Sub sbLlamaCotizacion_Click(sender As Object, e As EventArgs) Handles sbLlamaCotizacion.Click
        ValidaCotizacion()
    End Sub

    Private Sub teIdCotizacion_EditValueChanged(sender As Object, e As EventArgs) Handles teIdCotizacion.EditValueChanged
        Dim entCotizacion As fac_Cotizaciones
        entCotizacion = objTablas.fac_CotizacionesSelectByPK(teIdCotizacion.EditValue)

        If entCotizacion.IdComprobante = 0 Then
            Exit Sub
        End If
    End Sub
    Private Sub ValidaCotizacion()
        Dim entCotizacion As fac_Cotizaciones
        teIdCotizacion.EditValue = 0
        teIdCotizacion.EditValue = objConsultas.ConsultaCotizaciones(frmConsultaDetalle, piIdVendedor)
        entCotizacion = objTablas.fac_CotizacionesSelectByPK(teIdCotizacion.EditValue)

        If entCotizacion.IdComprobante = 0 Then
            MsgBox("No ha Selecciono Ninguna Cotización", MsgBoxStyle.Exclamation, "Nota")
            teIdCotizacion.EditValue = 0
            Exit Sub
        End If
    End Sub
End Class
