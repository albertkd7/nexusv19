﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmVentasTrimestrales
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmVentasTrimestrales))
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.lePuntoVenta = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.seEjercicio1 = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.seEjercicio2 = New DevExpress.XtraEditors.SpinEdit()
        Me.BeProducto1 = New Nexus.beProducto()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seEjercicio1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seEjercicio2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.BeProducto1)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.seEjercicio2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.seEjercicio1)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.lePuntoVenta)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Size = New System.Drawing.Size(683, 342)
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "COMPARACION DE VENTAS ANUAL"
        Me.teTitulo.Location = New System.Drawing.Point(106, 166)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(335, 20)
        Me.teTitulo.TabIndex = 6
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(17, 169)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl6.TabIndex = 24
        Me.LabelControl6.Text = "Título del informe:"
        '
        'lePuntoVenta
        '
        Me.lePuntoVenta.Location = New System.Drawing.Point(106, 140)
        Me.lePuntoVenta.Name = "lePuntoVenta"
        Me.lePuntoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePuntoVenta.Size = New System.Drawing.Size(335, 20)
        Me.lePuntoVenta.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(25, 143)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl5.TabIndex = 23
        Me.LabelControl5.Text = "Punto de Venta:"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(106, 117)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(335, 20)
        Me.leSucursal.TabIndex = 4
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(59, 121)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl4.TabIndex = 28
        Me.LabelControl4.Text = "Sucursal:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(51, 65)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl1.TabIndex = 47
        Me.LabelControl1.Text = "Ejercicio 1:"
        '
        'seEjercicio1
        '
        Me.seEjercicio1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seEjercicio1.Location = New System.Drawing.Point(106, 62)
        Me.seEjercicio1.Name = "seEjercicio1"
        Me.seEjercicio1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seEjercicio1.Properties.DisplayFormat.FormatString = "n0"
        Me.seEjercicio1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seEjercicio1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seEjercicio1.Properties.Mask.EditMask = "n0"
        Me.seEjercicio1.Size = New System.Drawing.Size(104, 20)
        Me.seEjercicio1.TabIndex = 46
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(282, 65)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl2.TabIndex = 50
        Me.LabelControl2.Text = "Ejercicio 2:"
        '
        'seEjercicio2
        '
        Me.seEjercicio2.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seEjercicio2.Location = New System.Drawing.Point(336, 62)
        Me.seEjercicio2.Name = "seEjercicio2"
        Me.seEjercicio2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seEjercicio2.Properties.DisplayFormat.FormatString = "n0"
        Me.seEjercicio2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seEjercicio2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.seEjercicio2.Properties.Mask.EditMask = "n0"
        Me.seEjercicio2.Size = New System.Drawing.Size(105, 20)
        Me.seEjercicio2.TabIndex = 49
        '
        'BeProducto1
        '
        Me.BeProducto1.Location = New System.Drawing.Point(50, 86)
        Me.BeProducto1.Name = "BeProducto1"
        Me.BeProducto1.Size = New System.Drawing.Size(612, 25)
        Me.BeProducto1.TabIndex = 51
        '
        'fac_frmVentasTrimestrales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(683, 367)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmVentasTrimestrales"
        Me.Text = "Comparación de Ventas Anual"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seEjercicio1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seEjercicio2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lePuntoVenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seEjercicio2 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seEjercicio1 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents BeProducto1 As beProducto
End Class
