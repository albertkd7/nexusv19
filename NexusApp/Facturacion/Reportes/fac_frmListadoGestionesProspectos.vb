﻿Imports DevExpress.XtraReports.UI
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmListadoGestionesProspectos
	Dim bl As New FacturaBLL(g_ConnectionString)
	Dim dt As New DataTable
	Dim entProspecto As New fac_ProspectacionClientes
	Dim UsuarioDioClicProspecto As Boolean = False

	Private Sub facVentasProducto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		CargaCombos()
		deDesde.EditValue = CDate("1/" & Month(Today) & "/" & Year(Today))
		deHasta.EditValue = Today
		beCodProspecto.EditValue = 0
		chkGestiones.EditValue = False
	End Sub

	Private Sub CargaCombos()
		objCombos.fac_Vendedores(leVendedor, "--TODOS LOS VENDEDORES--")
	End Sub

	Private Sub sbAceptar_Click() Handles Me.Reporte
		dt = bl.fac_GestionesProspectosPeriodos(deDesde.EditValue, deHasta.EditValue, beCodProspecto.EditValue, leVendedor.EditValue, chkGestiones.EditValue)
		If chkGestiones.EditValue Then
			Dim rpt As New fac_rptGestionesProspectos() With {.DataSource = dt, .DataMember = ""}
			rpt.xrlEmpresa.Text = gsNombre_Empresa
			rpt.xrlTitulo.Text = teTitulo.EditValue
			rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
			rpt.ShowPreviewDialog()
		Else
			Dim rpt As New fac_rptListadoProspectos() With {.DataSource = dt, .DataMember = ""}
			rpt.xrlEmpresa.Text = gsNombre_Empresa
			rpt.xrlTitulo.Text = "LISTADO DE PROSPECTOS DE CLIENTES"
			rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
			rpt.ShowPreviewDialog()
		End If
	End Sub


	Private Sub beCodProspecto_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCodProspecto.ButtonClick
		beCodProspecto.EditValue = 0
		UsuarioDioClicProspecto = True
		beCodProspecto_Validated(beCodProspecto, New EventArgs)
	End Sub

	Private Sub beCodProspecto_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCodProspecto.Validated

		If Not UsuarioDioClicProspecto And beCodProspecto.EditValue = 0 Then
			teNombreProspecto.EditValue = ""
			Exit Sub
		End If

		UsuarioDioClicProspecto = False
		entProspecto = objConsultas.cnsProspectosClientes(frmConsultas, beCodProspecto.EditValue)

		If entProspecto.IdComprobante = 0 Then
			beCodProspecto.EditValue = 0
			teNombreProspecto.EditValue = ""
			beCodProspecto.Focus()
			Exit Sub
		End If


		beCodProspecto.EditValue = entProspecto.IdComprobante
		teNombreProspecto.EditValue = entProspecto.NombreCliente

	End Sub

End Class
