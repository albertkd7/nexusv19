﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmRptCotizaciones

    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim dt As New DataTable

    Private Sub frmFacReportes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = CDate(String.Format("1/{0}/{1}", Month(Today), Year(Today)))
        deHasta.EditValue = Today
    End Sub

    Private Sub sbAceptar_Click() Handles Me.Reporte
        dt = bl.fac_rptCotizaciones(deDesde.EditValue, deHasta.EditValue)

        Dim rpt As New fac_rptCotizaciones() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue).ToUpper()
        rpt.ShowPreviewDialog()
    End Sub

End Class
