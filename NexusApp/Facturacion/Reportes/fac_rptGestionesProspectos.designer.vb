<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class fac_rptGestionesProspectos
	Inherits DevExpress.XtraReports.UI.XtraReport

	'XtraReport overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing AndAlso components IsNot Nothing Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Designer
	'It can be modified using the Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
		Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
		Me.NumeroPartida = New DevExpress.XtraReports.UI.CalculatedField()
		Me.DsFactura1 = New Nexus.dsFactura()
		Me.XrLine5 = New DevExpress.XtraReports.UI.XRLine()
		Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
		Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
		Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
		Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
		Me.xrlPeriodo = New DevExpress.XtraReports.UI.XRLabel()
		Me.xrlTitulo = New DevExpress.XtraReports.UI.XRLabel()
		Me.xrlEmpresa = New DevExpress.XtraReports.UI.XRLabel()
		Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
		Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
		Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
		Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
		Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
		Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
		Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
		Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
		Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
		Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
		CType(Me.DsFactura1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
		'
		'Detail
		'
		Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel9, Me.XrLabel11, Me.XrLabel10, Me.XrLabel8})
		Me.Detail.Dpi = 100.0!
		Me.Detail.HeightF = 17.04167!
		Me.Detail.Name = "Detail"
		Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
		Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		'
		'XrLabel9
		'
		Me.XrLabel9.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ListadoGestiones.ComentarioCliente", "{0}")})
		Me.XrLabel9.Dpi = 100.0!
		Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(821.2499!, 0!)
		Me.XrLabel9.Name = "XrLabel9"
		Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel9.SizeF = New System.Drawing.SizeF(207.6667!, 16.0!)
		'
		'XrLabel11
		'
		Me.XrLabel11.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ListadoGestiones.Motivo", "{0}")})
		Me.XrLabel11.Dpi = 100.0!
		Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(305.1667!, 0!)
		Me.XrLabel11.Name = "XrLabel11"
		Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel11.SizeF = New System.Drawing.SizeF(245.1667!, 16.0!)
		'
		'XrLabel10
		'
		Me.XrLabel10.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ListadoGestiones.NombreCliente", "{0}")})
		Me.XrLabel10.Dpi = 100.0!
		Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(69.00002!, 0!)
		Me.XrLabel10.Name = "XrLabel10"
		Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel10.SizeF = New System.Drawing.SizeF(229.1667!, 16.0!)
		'
		'XrLabel8
		'
		Me.XrLabel8.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ListadoGestiones.Fecha", "{0:dd/MM/yyyy}")})
		Me.XrLabel8.Dpi = 100.0!
		Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
		Me.XrLabel8.Name = "XrLabel8"
		Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel8.SizeF = New System.Drawing.SizeF(69.0!, 16.0!)
		'
		'NumeroPartida
		'
		Me.NumeroPartida.DataMember = "Partidas"
		Me.NumeroPartida.Expression = "'Partida No. ' & [Numero] & '    Fecha de la partida: ' & [Fecha]"
		Me.NumeroPartida.Name = "NumeroPartida"
		'
		'DsFactura1
		'
		Me.DsFactura1.DataSetName = "dsFactura"
		Me.DsFactura1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'XrLine5
		'
		Me.XrLine5.Dpi = 100.0!
		Me.XrLine5.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 0!)
		Me.XrLine5.Name = "XrLine5"
		Me.XrLine5.SizeF = New System.Drawing.SizeF(1026.0!, 2.0!)
		'
		'PageHeader
		'
		Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrPageInfo2, Me.XrLabel1, Me.XrPageInfo1, Me.xrlPeriodo, Me.xrlTitulo, Me.xrlEmpresa})
		Me.PageHeader.Dpi = 100.0!
		Me.PageHeader.HeightF = 133.9167!
		Me.PageHeader.Name = "PageHeader"
		Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
		Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		'
		'XrPanel1
		'
		Me.XrPanel1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
			Or DevExpress.XtraPrinting.BorderSide.Right) _
			Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
		Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel6, Me.XrLabel7, Me.XrLabel2, Me.XrLabel5})
		Me.XrPanel1.Dpi = 100.0!
		Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 103.0!)
		Me.XrPanel1.Name = "XrPanel1"
		Me.XrPanel1.SizeF = New System.Drawing.SizeF(1030.0!, 30.91665!)
		Me.XrPanel1.StylePriority.UseBorders = False
		'
		'XrLabel6
		'
		Me.XrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None
		Me.XrLabel6.Dpi = 100.0!
		Me.XrLabel6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
		Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(71.0!, 13.0!)
		Me.XrLabel6.Name = "XrLabel6"
		Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel6.SizeF = New System.Drawing.SizeF(249.0!, 16.0!)
		Me.XrLabel6.StylePriority.UseBorders = False
		Me.XrLabel6.StylePriority.UseFont = False
		Me.XrLabel6.Text = "Nombre del Cliente"
		'
		'XrLabel7
		'
		Me.XrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None
		Me.XrLabel7.Dpi = 100.0!
		Me.XrLabel7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
		Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(351.6667!, 13.00001!)
		Me.XrLabel7.Name = "XrLabel7"
		Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel7.SizeF = New System.Drawing.SizeF(142.3333!, 16.0!)
		Me.XrLabel7.StylePriority.UseBorders = False
		Me.XrLabel7.StylePriority.UseFont = False
		Me.XrLabel7.StylePriority.UseTextAlignment = False
		Me.XrLabel7.Text = "Motivo de Gesti�n"
		Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'XrLabel2
		'
		Me.XrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None
		Me.XrLabel2.Dpi = 100.0!
		Me.XrLabel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
		Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(10.0!, 13.0!)
		Me.XrLabel2.Name = "XrLabel2"
		Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel2.SizeF = New System.Drawing.SizeF(43.0!, 16.0!)
		Me.XrLabel2.StylePriority.UseBorders = False
		Me.XrLabel2.StylePriority.UseFont = False
		Me.XrLabel2.Text = "Fecha"
		'
		'XrLabel5
		'
		Me.XrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None
		Me.XrLabel5.Dpi = 100.0!
		Me.XrLabel5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
		Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(843.75!, 2.000014!)
		Me.XrLabel5.Multiline = True
		Me.XrLabel5.Name = "XrLabel5"
		Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel5.SizeF = New System.Drawing.SizeF(129.0!, 27.0!)
		Me.XrLabel5.StylePriority.UseBorders = False
		Me.XrLabel5.StylePriority.UseFont = False
		Me.XrLabel5.StylePriority.UseTextAlignment = False
		Me.XrLabel5.Text = "Comentario Cliente"
		Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
		'
		'XrPageInfo2
		'
		Me.XrPageInfo2.Dpi = 100.0!
		Me.XrPageInfo2.Format = "{0:dd/MM/yyyy hh:mm tt}"
		Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(784.0!, 0!)
		Me.XrPageInfo2.Name = "XrPageInfo2"
		Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
		Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(131.0!, 16.0!)
		Me.XrPageInfo2.StylePriority.UseTextAlignment = False
		Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		'
		'XrLabel1
		'
		Me.XrLabel1.Dpi = 100.0!
		Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(665.0!, 0!)
		Me.XrLabel1.Name = "XrLabel1"
		Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel1.SizeF = New System.Drawing.SizeF(59.0!, 16.0!)
		Me.XrLabel1.Text = "P�g. No."
		'
		'XrPageInfo1
		'
		Me.XrPageInfo1.Dpi = 100.0!
		Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(727.0!, 0!)
		Me.XrPageInfo1.Name = "XrPageInfo1"
		Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(52.0!, 16.0!)
		Me.XrPageInfo1.StylePriority.UseTextAlignment = False
		Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
		'
		'xrlPeriodo
		'
		Me.xrlPeriodo.Dpi = 100.0!
		Me.xrlPeriodo.Font = New System.Drawing.Font("Arial", 11.0!)
		Me.xrlPeriodo.LocationFloat = New DevExpress.Utils.PointFloat(143.0!, 59.0!)
		Me.xrlPeriodo.Name = "xrlPeriodo"
		Me.xrlPeriodo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrlPeriodo.SizeF = New System.Drawing.SizeF(769.0!, 21.0!)
		Me.xrlPeriodo.StylePriority.UseFont = False
		Me.xrlPeriodo.StylePriority.UseTextAlignment = False
		Me.xrlPeriodo.Text = "Per�odo del reporte"
		Me.xrlPeriodo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'xrlTitulo
		'
		Me.xrlTitulo.Dpi = 100.0!
		Me.xrlTitulo.Font = New System.Drawing.Font("Arial", 11.0!)
		Me.xrlTitulo.LocationFloat = New DevExpress.Utils.PointFloat(143.0!, 38.0!)
		Me.xrlTitulo.Name = "xrlTitulo"
		Me.xrlTitulo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrlTitulo.SizeF = New System.Drawing.SizeF(769.0!, 21.0!)
		Me.xrlTitulo.StylePriority.UseFont = False
		Me.xrlTitulo.StylePriority.UseTextAlignment = False
		Me.xrlTitulo.Text = "T�tulo del reporte"
		Me.xrlTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'xrlEmpresa
		'
		Me.xrlEmpresa.Dpi = 100.0!
		Me.xrlEmpresa.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
		Me.xrlEmpresa.LocationFloat = New DevExpress.Utils.PointFloat(143.0!, 16.0!)
		Me.xrlEmpresa.Name = "xrlEmpresa"
		Me.xrlEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrlEmpresa.SizeF = New System.Drawing.SizeF(769.0!, 21.0!)
		Me.xrlEmpresa.StylePriority.UseFont = False
		Me.xrlEmpresa.StylePriority.UseTextAlignment = False
		Me.xrlEmpresa.Text = "IT OUTSOURCING, S. A. DE C. V."
		Me.xrlEmpresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'ReportFooter
		'
		Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine5})
		Me.ReportFooter.Dpi = 100.0!
		Me.ReportFooter.HeightF = 13.83333!
		Me.ReportFooter.Name = "ReportFooter"
		'
		'PageFooter
		'
		Me.PageFooter.Dpi = 100.0!
		Me.PageFooter.HeightF = 2.208328!
		Me.PageFooter.Name = "PageFooter"
		Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
		Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		'
		'TopMarginBand1
		'
		Me.TopMarginBand1.Dpi = 100.0!
		Me.TopMarginBand1.HeightF = 35.0!
		Me.TopMarginBand1.Name = "TopMarginBand1"
		'
		'BottomMarginBand1
		'
		Me.BottomMarginBand1.Dpi = 100.0!
		Me.BottomMarginBand1.HeightF = 35.0!
		Me.BottomMarginBand1.Name = "BottomMarginBand1"
		'
		'XrLabel16
		'
		Me.XrLabel16.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ListadoGestiones.Vendedor", "{0}")})
		Me.XrLabel16.Dpi = 100.0!
		Me.XrLabel16.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
		Me.XrLabel16.Name = "XrLabel16"
		Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel16.SizeF = New System.Drawing.SizeF(503.375!, 17.0!)
		Me.XrLabel16.StylePriority.UseFont = False
		'
		'GroupHeader2
		'
		Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel16})
		Me.GroupHeader2.Dpi = 100.0!
		Me.GroupHeader2.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("IdVendedor", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
		Me.GroupHeader2.HeightF = 18.75!
		Me.GroupHeader2.Name = "GroupHeader2"
		'
		'XrLabel3
		'
		Me.XrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ListadoGestiones.ComentarioGeneral", "{0}")})
		Me.XrLabel3.Dpi = 100.0!
		Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(550.5833!, 0!)
		Me.XrLabel3.Name = "XrLabel3"
		Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel3.SizeF = New System.Drawing.SizeF(267.1667!, 16.0!)
		'
		'XrLabel4
		'
		Me.XrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None
		Me.XrLabel4.Dpi = 100.0!
		Me.XrLabel4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
		Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(584.3334!, 2.0!)
		Me.XrLabel4.Multiline = True
		Me.XrLabel4.Name = "XrLabel4"
		Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel4.SizeF = New System.Drawing.SizeF(129.0!, 27.0!)
		Me.XrLabel4.StylePriority.UseBorders = False
		Me.XrLabel4.StylePriority.UseFont = False
		Me.XrLabel4.StylePriority.UseTextAlignment = False
		Me.XrLabel4.Text = "Comentario General"
		Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter
		'
		'GroupFooter1
		'
		Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1})
		Me.GroupFooter1.Dpi = 100.0!
		Me.GroupFooter1.HeightF = 12.5!
		Me.GroupFooter1.Name = "GroupFooter1"
		'
		'XrLine1
		'
		Me.XrLine1.Dpi = 100.0!
		Me.XrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash
		Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 5.250009!)
		Me.XrLine1.Name = "XrLine1"
		Me.XrLine1.SizeF = New System.Drawing.SizeF(1026.0!, 2.0!)
		'
		'fac_rptGestionesProspectos
		'
		Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader2, Me.GroupFooter1})
		Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.NumeroPartida})
		Me.DataMember = "ListadoGestiones"
		Me.DataSource = Me.DsFactura1
		Me.DrawGrid = False
		Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Landscape = True
		Me.Margins = New System.Drawing.Printing.Margins(35, 35, 35, 35)
		Me.PageHeight = 850
		Me.PageWidth = 1100
		Me.SnapGridSize = 3.125!
		Me.SnappingMode = DevExpress.XtraReports.UI.SnappingMode.None
		Me.Version = "16.2"
		CType(Me.DsFactura1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

	End Sub
	Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents NumeroPartida As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents DsFactura1 As Nexus.dsFactura
    Friend WithEvents XrLine5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
	Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
	Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
	Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
	Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
	Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
	Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
	Friend WithEvents xrlPeriodo As DevExpress.XtraReports.UI.XRLabel
	Friend WithEvents xrlTitulo As DevExpress.XtraReports.UI.XRLabel
	Friend WithEvents xrlEmpresa As DevExpress.XtraReports.UI.XRLabel
	Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
	Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
	Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
	Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
	Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
	Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
	Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
	Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
	Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
	Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
	Friend WithEvents XrLabel3 As XRLabel
	Friend WithEvents XrLabel4 As XRLabel
	Friend WithEvents GroupFooter1 As GroupFooterBand
	Friend WithEvents XrLine1 As XRLine
End Class
