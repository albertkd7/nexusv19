﻿Imports NexusBLL

Public Class fac_frmLibroContribuyentes
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()

    Private Sub frmLibroContribuyentes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        meMes.Month = Now.Month
        spAnio.EditValue = Now.Year
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub


    Private Sub fac_frmLibroContribuentes_Reporte() Handles Me.Reporte
        Dim ds As DataSet = bl.getLibroContribuyentes(meMes.Month, spAnio.EditValue, leSucursal.EditValue)

        Dim rpt As New fac_rptLibroContribuyente() With {.DataSource = ds.Tables(0), .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlNRC.Text = "NRC: " & dtParam.Rows(0).Item("NrcEmpresa")
        rpt.xrlNIT.Text = "NIT: " & dtParam.Rows(0).Item("NitEmpresa")
        rpt.xrlEmpresa.Visible = CheckEdit1.Checked
        rpt.xrlNRC.Visible = CheckEdit1.Checked
        rpt.xrlNIT.Visible = CheckEdit1.Checked
        rpt.xrlTitulo.Visible = CheckEdit1.Checked
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlMes.Text = "MES: " + (ObtieneMesString(meMes.EditValue)).ToUpper
        rpt.xrlEjercicio.Text = "AÑO: " + (spAnio.EditValue).ToString
        rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text

        rpt.xrpFolio.StartPageNumber = seNumFolio.EditValue
        rpt.xrpFolio.Visible = CheckEdit2.Checked
        If ds.Tables(1).Rows.Count > 0 Then
            With ds.Tables(1).Rows(0)

                Dim VentaNoSujetasConsum As Decimal = 0
                Dim VentaNoSujetaContrib As Decimal = 0

                Dim colms = ds.Tables(1).Columns
                If colms.Contains("VentaNoSujetasConsum") And
                    colms.Contains("VentaNoSujetaContrib") Then

                    VentaNoSujetasConsum = .Item("VentaNoSujetasConsum")
                    VentaNoSujetaContrib = .Item("VentaNoSujetaContrib")
                End If

    
                    rpt.xrlExport.Text = Format(.Item("Exportaciones"), "###,###,##0.00")
                    rpt.xrlTotalExporta.Text = Format(.Item("Exportaciones"), "###,###,##0.00")
                    rpt.xrlVtaNsCF.Text = Format(VentaNoSujetasConsum, "###,###,##0.00")
                    rpt.xrlVetaNsCo.Text = Format(VentaNoSujetaContrib, "###,###,##0.00")
                    rpt.xrlVtaExCF.Text = Format(.Item("VentaExentaConsum"), "###,###,##0.00")
                    rpt.xrlVtaGrCF.Text = Format(.Item("VentaNetaConsum"), "###,###,##0.00")
                    rpt.xrlTotalExporta.Text = Format(.Item("Exportaciones"), "###,###,##0.00")
                    rpt.xrlIvaCF.Text = Format(.Item("IvaConsum"), "###,###,##0.00")
                    rpt.xrlVtaExCo.Text = Format(.Item("VentaExentaContrib"), "###,###,##0.00")
                    rpt.xrlVtaGrCo.Text = Format(.Item("VentaNetaContrib"), "###,###,##0.00")
                    rpt.xrlIvaCo.Text = Format(.Item("IvaContrib"), "###,###,##0.00")
                rpt.xrlTotalCF.Text = Format(.Item("VentaExentaConsum") + .Item("VentaNetaConsum") + .Item("IvaConsum") + .Item("Exportaciones") + VentaNoSujetasConsum, "###,###,##0.00")
                    rpt.xrlTotalCo.Text = Format(.Item("VentaExentaContrib") + .Item("VentaNetaContrib") + .Item("IvaContrib") + VentaNoSujetaContrib, "###,###,##0.00")
                    rpt.xrlTotalEx.Text = Format(.Item("VentaExentaConsum") + .Item("VentaExentaContrib"), "###,###,##0.00")
                    rpt.xrlTotalGr.Text = Format(.Item("VentaNetaConsum") + .Item("VentaNetaContrib"), "###,###,##0.00")
                    rpt.xrlTotalIva.Text = Format(.Item("IvaConsum") + .Item("IvaContrib"), "###,###,##0.00")
                rpt.xrlGranTotal.Text = Format(.Item("VentaExentaConsum") + .Item("VentaNetaConsum") + .Item("IvaConsum") + .Item("VentaExentaContrib") + .Item("Exportaciones") + .Item("VentaNetaContrib") + .Item("IvaContrib") + VentaNoSujetasConsum + VentaNoSujetaContrib, "###,###,##0.00")
                    rpt.xrlIvaRetCo.Text = Format(.Item("RetenPercep"), "###,###,##0.00")
                    rpt.xrlIvaRetCF.Text = Format(.Item("RetenPercepConsum"), "###,###,##0.00")
                    rpt.xrlTotalIvaRet.Text = Format(.Item("RetenPercep") + .Item("RetenPercepConsum"), "###,###,##0.00")

            End With
        End If
        rpt.ShowPreviewDialog()
    End Sub


    Private Sub btExportXLS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btExportXLS.Click
        ExportarLibroExcel()
    End Sub

    Private Sub ExportarLibroExcel()
        If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_Contribuyentes.XLSX") Then
            If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_Contribuyentes.XLS") Then
                MsgBox("No existe la plantilla necesaria para poder exportar", MsgBoxStyle.Critical, "Nota")
                Exit Sub
            End If
        End If

        Dim Template As String
        teProgreso.EditValue = "PREPARANDO LOS DATOS. ESPERE..."
        teProgreso.Visible = True
        If FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_Contribuyentes.XLSX") Then
            Template = Application.StartupPath & "\Plantillas\Libro_ContribuyentesTmp.XLSX"
            FileCopy(Application.StartupPath & "\Plantillas\Libro_Contribuyentes.XLSX", Template)
        Else
            Template = Application.StartupPath & "\Plantillas\Libro_ContribuyentesTmp.XLS"
            FileCopy(Application.StartupPath & "\Plantillas\Libro_Contribuyentes.XLS", Template)
        End If

        Dim oApp As Object = CreateObject("Excel.Application")

        'verifico si no está en una cultura diferente, por el momento de Inglés a Español, pendiente de verificar cuando sea a la inversa
        Dim oldCI As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        Try
            oApp.DisplayAlerts = False
        Catch ex As Exception
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")
            oApp.DisplayAlerts = False
        End Try

        Dim Range As String

        oApp.workbooks.Open(Template)
        oApp.Cells(3, 1).Value = "NOMBRE DEL CONTRIBUYENTE: " + gsNombre_Empresa
        oApp.Cells(3, 8).Value = "NRC: " & dtParam.Rows(0).Item("NrcEmpresa")
        oApp.Cells(3, 10).Value = "NIT: " & dtParam.Rows(0).Item("NitEmpresa")

        oApp.Cells(4, 1).Value = "MES: " + (ObtieneMesString(meMes.EditValue)).ToUpper
        oApp.Cells(4, 4).Value = "AÑO: " + (spAnio.EditValue).ToString
        oApp.Cells(4, 10).Value = "FOLIO No. " & seNumFolio.EditValue
        oApp.Cells(5, 1).Value = "SUCURSAL: " & leSucursal.Text
        oApp.Cells(5, 1).font.bold = True

        Dim TotExento As Decimal = 0.0, TotAfecto As Decimal = 0.0, TotIVA1 As Decimal = 0.0, TotIVA2 As Decimal = 0.0
        Dim TotGeneral As Decimal = 0.0, TotNoSujeto As Decimal = 0.0
        Dim Linea As Integer = 8, i As Integer = 0
        Dim ds As DataSet = bl.getLibroContribuyentes(meMes.Month, spAnio.EditValue, leSucursal.EditValue)

        While i <= ds.Tables(0).Rows.Count - 1
            oApp.Cells(Linea, 1).Value = i + 1
            oApp.Cells(Linea, 2).Value = ds.Tables(0).Rows(i).Item("Fecha")
            oApp.Cells(Linea, 3).Value = ds.Tables(0).Rows(i).Item("Numero")
            oApp.Cells(Linea, 4).Value = ds.Tables(0).Rows(i).Item("NumFormularioUnico")
            oApp.Cells(Linea, 5).Value = ds.Tables(0).Rows(i).Item("Nombre")
            oApp.Cells(Linea, 6).Value = ds.Tables(0).Rows(i).Item("Nrc")
            oApp.Cells(Linea, 7).Value = ds.Tables(0).Rows(i).Item("Nit")
            oApp.Cells(Linea, 8).Value = ds.Tables(0).Rows(i).Item("TotalExento")
            oApp.Cells(Linea, 9).Value = ds.Tables(0).Rows(i).Item("TotalAfecto")
            oApp.Cells(Linea, 10).Value = ds.Tables(0).Rows(i).Item("IVA1")
            oApp.Cells(Linea, 11).Value = ds.Tables(0).Rows(i).Item("IVA2")
            oApp.Cells(Linea, 12).Value = ds.Tables(0).Rows(i).Item("Total")
            oApp.Cells(Linea, 13).Value = ds.Tables(0).Rows(i).Item("NoSujeto")

            TotExento += SiEsNulo(ds.Tables(0).Rows(i).Item("TotalExento"), 0)
            TotAfecto += SiEsNulo(ds.Tables(0).Rows(i).Item("TotalAfecto"), 0)
            TotIVA1 += SiEsNulo(ds.Tables(0).Rows(i).Item("IVA1"), 0)
            TotIVA2 += SiEsNulo(ds.Tables(0).Rows(i).Item("IVA2"), 0)
            TotGeneral += SiEsNulo(ds.Tables(0).Rows(i).Item("Total"), 0)
            TotNoSujeto += SiEsNulo(ds.Tables(0).Rows(i).Item("NoSujeto"), 0)

            i += 1
            Linea += 1
        End While
        PonerLineaExcel(oApp, Linea)

        Linea += 1
        oApp.Cells(Linea, 7).Value = "TOTALES: "
        oApp.Cells(Linea, 8).Value = TotExento
        oApp.Cells(Linea, 9).Value = TotAfecto
        oApp.Cells(Linea, 10).Value = TotIVA1
        oApp.Cells(Linea, 11).Value = TotIVA2
        oApp.Cells(Linea, 12).Value = TotGeneral
        oApp.Cells(Linea, 13).Value = TotNoSujeto

        If ds.Tables(1).Rows.Count > 0 Then
            Linea += 2
            With ds.Tables(1).Rows(0)
                oApp.Cells(Linea, 5).Value = "RESUMEN DE OPERACIONES: "
                oApp.Cells(Linea, 5).font.bold = True
                oApp.Cells(Linea, 5).font.size = 8
                oApp.Cells(Linea, 7).Value = "EXENTAS"
                oApp.Cells(Linea, 7).font.size = 7
                oApp.Cells(Linea, 8).Value = "GRVADAS"
                oApp.Cells(Linea, 8).font.size = 7
                oApp.Cells(Linea, 9).Value = "EXPORTAC."
                oApp.Cells(Linea, 9).font.size = 7
                oApp.Cells(Linea, 10).Value = "DEBITO FISCAL"
                oApp.Cells(Linea, 10).font.size = 7
                oApp.Cells(Linea, 11).Value = "TOTAL"
                oApp.Cells(Linea, 11).font.size = 7
                oApp.Cells(Linea, 12).Value = "IVA RETENIDO"
                oApp.Cells(Linea, 12).font.size = 7

                Linea += 1
                oApp.Cells(Linea, 5).Value = "VENTAS A CONSUMIDORES FINALES:"
                oApp.Cells(Linea, 7).Value = .Item("VentaExentaConsum")
                oApp.Cells(Linea, 8).Value = .Item("VentaNetaConsum")
                oApp.Cells(Linea, 9).Value = .Item("Exportaciones")
                oApp.Cells(Linea, 10).Value = .Item("IvaConsum")
                oApp.Cells(Linea, 11).Value = .Item("VentaExentaConsum") + .Item("VentaNetaConsum") + .Item("IvaConsum")
                oApp.Cells(Linea, 12).Value = 0.0

                Linea += 1
                oApp.Cells(Linea, 5).Value = "VENTAS A CONTRIBUYENTES:"
                oApp.Cells(Linea, 7).Value = .Item("VentaExentaContrib")
                oApp.Cells(Linea, 8).Value = .Item("VentaNetaContrib")
                oApp.Cells(Linea, 9).Value = 0.0
                oApp.Cells(Linea, 10).Value = .Item("IvaContrib")
                oApp.Cells(Linea, 11).Value = .Item("VentaExentaContrib") + .Item("VentaNetaContrib") + .Item("IvaContrib")
                oApp.Cells(Linea, 12).Value = .Item("RetenPercep")

                Linea += 1

                oApp.Cells(Linea, 5).Value = "TOTAL DE OPERACIONES:"
                oApp.Cells(Linea, 5).font.bold = True
                'oApp.Cells(Linea, 5).WrapText = False
                PonerLineaExcel2(oApp, Linea)
                oApp.Cells(Linea, 7).Value = .Item("VentaExentaConsum") + .Item("VentaExentaContrib")
                oApp.Cells(Linea, 7).font.bold = True
                oApp.Cells(Linea, 8).Value = .Item("VentaNetaConsum") + .Item("VentaNetaContrib")
                oApp.Cells(Linea, 8).font.bold = True
                oApp.Cells(Linea, 9).Value = .Item("Exportaciones")
                oApp.Cells(Linea, 9).font.bold = True
                oApp.Cells(Linea, 10).Value = .Item("IvaConsum") + .Item("IvaContrib")
                oApp.Cells(Linea, 10).font.bold = True
                oApp.Cells(Linea, 11).Value = .Item("VentaExentaConsum") + .Item("VentaNetaConsum") + .Item("IvaConsum") + .Item("VentaExentaContrib") + .Item("VentaNetaContrib") + .Item("IvaContrib")
                oApp.Cells(Linea, 11).font.bold = True
                oApp.Cells(Linea, 12).Value = .Item("RetenPercep")
                oApp.Cells(Linea, 12).font.bold = True
            End With
        End If
        Linea += 3
        PonerLineaExcel3(oApp, Linea)
        oApp.Cells(Linea, 5).Value = "                            NOMBRE Y FIRMA DEL CONTADOR"
        oApp.Cells(Linea, 5).font.bold = True
        oApp.Cells(Linea, 5).font.size = 8

        Range = "$A$1:$M$" + CStr(Linea)
        oApp.ActiveSheet.PageSetup.PrintArea = Range
        oApp.Range("A1").Select()

        'oApp.ActiveSheet.Protect("DrawingObjects:=False, Contents:=True, Scenarios:= False")
        'oApp.Save()
        teProgreso.Visible = False
        oApp.Visible = True
        System.Threading.Thread.CurrentThread.CurrentCulture = oldCI

    End Sub
    Private Sub PonerLineaExcel(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("A{0}:M{0}", CStr(Linea))
        oApp.Cells(5, 33).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
    Private Sub PonerLineaExcel2(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("E{0}:L{0}", CStr(Linea))
        oApp.Cells(5, 33).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
    Private Sub PonerLineaExcel3(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("E{0}:G{0}", CStr(Linea))
        oApp.Cells(5, 33).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
End Class
