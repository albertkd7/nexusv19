﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmVentasPeriodo
    Dim bl As New FacturaBLL(g_ConnectionString)

    Private Sub frmFacReportes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS --")
        objCombos.fac_PuntosVenta(lePuntoVenta, 1, "-- TODOS --")
        objCombos.con_CentrosCosto(leCentroCosto, "", "-- TODOS LOS CENTROS --")
        deDesde.EditValue = CDate(String.Format("1/{0}/{1}", Month(Today), Year(Today)))
        deHasta.EditValue = Today
        leCentroCosto.EditValue = "-1"
    End Sub

    Private Sub SimpleButton1_Click() Handles Me.Reporte

        Dim dt As DataTable = bl.fac_VentasPeriodo(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, leCentroCosto.EditValue)
        Dim rpt As New fac_rptVentasPeriodo() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0},SUCURSAL:{1}, PUNTO DE VENTA: {2}", teTitulo.Text, leSucursal.Text, lePuntoVenta.Text)
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub
End Class
