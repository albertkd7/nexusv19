﻿Imports DevExpress.XtraReports.UI
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmListDetalleDocumentosLegales
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim thread As Threading.Thread
    Private Sub fac_frmListDetalleDocumentosLegales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = CDate("1/" & Month(Today) & "/" & Year(Today))
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
        LabelControl5.Visible = False
    End Sub

    Private Sub fac_frmListAnulados_Report_Click() Handles Me.Reporte
        LabelControl5.Visible = True
        Dim dt As DataTable = bl.fac_DocumentosLegales(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)
        Dim rpt As New fac_rptDocumentosLegales() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, PUNTO DE VENTA: {1}", teTitulo.EditValue, lePuntoVenta.Text)
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        LabelControl5.Visible = False
        rpt.ShowPreviewDialog()
    End Sub


    Private Sub sbGeneraArchivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub Generar()
        LabelControl5.Visible = True
        Try
            Dim msg As String = ""

        Catch ex As Exception
            MsgBox("Error " + Chr(13) + ex.Message, MsgBoxStyle.Critical)
        End Try
        LabelControl5.Visible = False
    End Sub
    Private Sub GenerarArchivo(ByVal Archivo As String, ByVal dt As DataTable)
        FileOpen(1, Archivo, OpenMode.Output)
        Dim sb As String = ""
        Dim dc As DataColumn
        Dim i As Integer = 0
        Dim dr As DataRow
        For Each dr In dt.Rows
            i = 0 : sb = ""
            For Each dc In dt.Columns
                If Not IsDBNull(dr(i)) Then
                    sb &= CStr(dr(i))
                    If i <> 8 Then
                        sb &= ";"
                    End If

                End If
                i += 1
            Next
            PrintLine(1, sb)
        Next
        FileClose(1)
    End Sub
    Private Sub btArchivo_Click(sender As Object, e As EventArgs) Handles btArchivo.Click
        Dim fbd As New FolderBrowserDialog
        fbd.ShowDialog()
        If fbd.SelectedPath = "" Then
            MsgBox("No se seleccionó ninguna carpeta", MsgBoxStyle.Critical, "Nota")
            Return
        End If
        Dim dt As DataTable = bl.fac_DocumentosLegales(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)
        Try
            GenerarArchivo(String.Format("{0}\{1}.csv", fbd.SelectedPath, "F-07"), dt)
            MsgBox("Los archivos han sido generado con éxito." + Chr(13) + "Y fueron colocados en " + fbd.SelectedPath, MsgBoxStyle.Information, "Nota")
        Catch ex As Exception
            MsgBox("NO FUE POSIBLE GENERAR LOS ARCHIVOS" + Chr(13) + ex.Message.ToString, MsgBoxStyle.Critical, "Nota")
        End Try
    End Sub
End Class
