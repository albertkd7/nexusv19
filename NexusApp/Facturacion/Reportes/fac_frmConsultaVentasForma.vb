﻿Imports DevExpress.XtraReports.UI
Imports NexusBLL
Imports NexusELL.TableEntities

Imports DevExpress.XtraGrid.Menu
Imports DevExpress.Utils.Menu
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraSplashScreen

Public Class fac_frmConsultaVentasForma
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blInventa As New InventarioBLL(g_ConnectionString)
    Dim Prod As DataTable, ProdVista As DataTable
    Dim dt As New DataTable

    Public Sub New()
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        AddHandler gv.PopupMenuShowing, AddressOf GridPopupMenuShowing
    End Sub

    Private Sub fac_frmConsultaProductosBase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.fac_Rutas(leRutas, "-- TODAS LAS RUTAS --")
        objCombos.fac_FormasPago(leFormasPago, "-- TODAS LAS FORMAS --")
        objCombos.fac_TiposComprobante(leTipoDoc, "-- TODOS LOS DOCUMENTOS --")
        gv.Columns.Clear()
    End Sub

    Private Sub btGenerar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGenerar.Click
        SplashScreenManager.ShowForm(Me, WaitForm.GetType(), True, True, False)
        SplashScreenManager.Default.SetWaitFormCaption("Procesando")
        SplashScreenManager.Default.SetWaitFormDescription("Consultado Datos...")

        Dim dt As DataTable
        dt = bl.fac_spRptVentasRutas(deDesde.EditValue, deHasta.EditValue, leTipoDoc.EditValue, leFormasPago.EditValue, leRutas.EditValue)
        gv.Columns.Clear()
        gc.DataSource = dt
        ' DE QUI HACIA ABAJO FUNCIONA PARA GENERAR LOS TOTALES EN EL GRID DE MANERA DINAMICA
        Dim Colum As String, Columnas As Integer = dt.Columns.Count()
        For i = 0 To Columnas - 1
            Colum = gv.Columns(i).FieldName()
            gv.Columns(i).Summary.Add(DevExpress.Data.SummaryItemType.Sum, Colum)
        Next
        ProdVista = gc.DataSource

        SplashScreenManager.Default.SetWaitFormCaption("Completado!")
        SplashScreenManager.Default.SetWaitFormDescription("Datos obtenidos con exito")
        SplashScreenManager.CloseForm()
    End Sub

#Region "StripOptions/XtraGrid"

    Private Sub GridPopupMenuShowing(sender As Object, e As PopupMenuShowingEventArgs)
        If e.MenuType = GridMenuType.Column Then
            Dim menu As GridViewColumnMenu = TryCast(e.Menu, GridViewColumnMenu)
            menu.Items.Clear()
            If menu.Column IsNot Nothing Then
                Dim item1 As New DXMenuItem("Exportar a CSV", New EventHandler(AddressOf OnExportOptionCSV_Click), Global.Nexus.My.Resources.Resources.fvcsv)
                item1.Tag = menu.Column
                menu.Items.Add(item1)

                Dim item2 As New DXMenuItem("Exportar a Excel", New EventHandler(AddressOf OnExportOptionExcel_Click), Global.Nexus.My.Resources.Resources.fvexcel)
                item2.Tag = menu.Column
                menu.Items.Add(item2)
            End If
        End If
    End Sub

    ' Menu item click handler - columns Header. 
    Private Sub OnExportOptionCSV_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim Sfd As New SaveFileDialog()
        Sfd.Filter = "CSV Files (*.csv)|*.csv"
        Sfd.Title = "Por favor indique ubicacion para guardar exportacion..."
        Sfd.FileName = "Datos-Inv-Export"

        If Sfd.ShowDialog() = DialogResult.OK Then
            gc.ExportToCsv(Sfd.FileName)
        End If
    End Sub

    Private Sub OnExportOptionExcel_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim Sfd As New SaveFileDialog()
        Sfd.Filter = "Excel Files (*.xlsx)|*.xlsx"
        Sfd.Title = "Por favor indique ubicacion para guardar exportacion..."
        Sfd.FileName = "Datos-Inv-Export"

        If Sfd.ShowDialog() = DialogResult.OK Then
            gc.ExportToXlsx(Sfd.FileName)
        End If
    End Sub
#End Region
End Class
