﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmLibroConsumidor
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.spAnio = New DevExpress.XtraEditors.SpinEdit
        Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit
        Me.ceLibroDetalle = New DevExpress.XtraEditors.CheckEdit
        Me.ceFoliarLibro = New DevExpress.XtraEditors.CheckEdit
        Me.ceMostrarTitulos = New DevExpress.XtraEditors.CheckEdit
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.seNumFolio = New DevExpress.XtraEditors.SpinEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.teProgreso = New DevExpress.XtraEditors.TextEdit
        Me.btExportXLS = New DevExpress.XtraEditors.SimpleButton
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.spAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceLibroDetalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceFoliarLibro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceMostrarTitulos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seNumFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teProgreso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.teProgreso)
        Me.GroupControl1.Controls.Add(Me.btExportXLS)
        Me.GroupControl1.Controls.Add(Me.spAnio)
        Me.GroupControl1.Controls.Add(Me.meMes)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.ceLibroDetalle)
        Me.GroupControl1.Controls.Add(Me.ceFoliarLibro)
        Me.GroupControl1.Controls.Add(Me.ceMostrarTitulos)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.seNumFolio)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Size = New System.Drawing.Size(836, 319)
        '
        'spAnio
        '
        Me.spAnio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spAnio.Location = New System.Drawing.Point(253, 32)
        Me.spAnio.Name = "spAnio"
        Me.spAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.spAnio.Properties.Mask.EditMask = "####"
        Me.spAnio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.spAnio.Size = New System.Drawing.Size(61, 20)
        Me.spAnio.TabIndex = 1
        '
        'meMes
        '
        Me.meMes.Location = New System.Drawing.Point(150, 32)
        Me.meMes.Name = "meMes"
        Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes.Size = New System.Drawing.Size(100, 20)
        Me.meMes.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(35, 35)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(105, 13)
        Me.LabelControl1.TabIndex = 33
        Me.LabelControl1.Text = "Mes y Año a Generar:"
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "LIBRO DE VENTAS A CONSUMIDOR FINAL"
        Me.teTitulo.Location = New System.Drawing.Point(150, 73)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(365, 20)
        Me.teTitulo.TabIndex = 2
        '
        'ceLibroDetalle
        '
        Me.ceLibroDetalle.Location = New System.Drawing.Point(148, 226)
        Me.ceLibroDetalle.Name = "ceLibroDetalle"
        Me.ceLibroDetalle.Properties.Caption = "Imprimir libro detallado"
        Me.ceLibroDetalle.Size = New System.Drawing.Size(211, 19)
        Me.ceLibroDetalle.TabIndex = 7
        '
        'ceFoliarLibro
        '
        Me.ceFoliarLibro.Location = New System.Drawing.Point(148, 158)
        Me.ceFoliarLibro.Name = "ceFoliarLibro"
        Me.ceFoliarLibro.Properties.Caption = "Foliar hojas del libro"
        Me.ceFoliarLibro.Size = New System.Drawing.Size(211, 19)
        Me.ceFoliarLibro.TabIndex = 5
        '
        'ceMostrarTitulos
        '
        Me.ceMostrarTitulos.Location = New System.Drawing.Point(148, 132)
        Me.ceMostrarTitulos.Name = "ceMostrarTitulos"
        Me.ceMostrarTitulos.Properties.Caption = "Incluír encabezados en el libro"
        Me.ceMostrarTitulos.Size = New System.Drawing.Size(178, 19)
        Me.ceMostrarTitulos.TabIndex = 4
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(150, 106)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(281, 20)
        Me.leSucursal.TabIndex = 3
        '
        'seNumFolio
        '
        Me.seNumFolio.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.seNumFolio.Location = New System.Drawing.Point(150, 183)
        Me.seNumFolio.Name = "seNumFolio"
        Me.seNumFolio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.seNumFolio.Properties.Mask.EditMask = "####"
        Me.seNumFolio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seNumFolio.Size = New System.Drawing.Size(83, 20)
        Me.seNumFolio.TabIndex = 6
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(54, 76)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl2.TabIndex = 30
        Me.LabelControl2.Text = "Título del reporte:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(27, 186)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(113, 13)
        Me.LabelControl4.TabIndex = 31
        Me.LabelControl4.Text = "Número Inicial del Folio:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(96, 109)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl3.TabIndex = 32
        Me.LabelControl3.Text = "Sucursal:"
        '
        'teProgreso
        '
        Me.teProgreso.EditValue = ""
        Me.teProgreso.Location = New System.Drawing.Point(248, 254)
        Me.teProgreso.Name = "teProgreso"
        Me.teProgreso.Size = New System.Drawing.Size(265, 20)
        Me.teProgreso.TabIndex = 66
        Me.teProgreso.Visible = False
        '
        'btExportXLS
        '
        Me.btExportXLS.Location = New System.Drawing.Point(150, 251)
        Me.btExportXLS.Name = "btExportXLS"
        Me.btExportXLS.Size = New System.Drawing.Size(92, 23)
        Me.btExportXLS.TabIndex = 65
        Me.btExportXLS.Text = "&Exportar a Excel"
        '
        'fac_frmLibroConsumidor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(836, 344)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmLibroConsumidor"
        Me.Text = "Libro de Ventas a Consumidor"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.spAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceLibroDetalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceFoliarLibro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceMostrarTitulos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seNumFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teProgreso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents spAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ceLibroDetalle As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceFoliarLibro As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceMostrarTitulos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents seNumFolio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teProgreso As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btExportXLS As DevExpress.XtraEditors.SimpleButton

End Class
