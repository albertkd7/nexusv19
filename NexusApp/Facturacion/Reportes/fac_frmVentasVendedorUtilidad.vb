﻿Imports NexusBLL

Public Class fac_frmVentasVendedorUtilidad
    Dim bl As New FacturaBLL(g_ConnectionString)

    Private Sub fac_frmVentasVendedorUtilidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        leSucursal.EditValue = piIdSucursal
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "")

        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        objCombos.fac_Vendedores(leVendedor, "-- TODOS --")
        deDesde.EditValue = Today
        deHasta.EditValue = Today
    End Sub
    Private Sub leSucursal_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub

    Private Sub fac_frmVentasVendedorUtilidad_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.fac_VentasVendedorUtilidad(deDesde.EditValue, deHasta.EditValue, leBodega.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, leVendedor.EditValue, rg1.SelectedIndex)
        If rg1.SelectedIndex = 0 Then
            Dim rpt As New fac_rptVentasVendedorUtilidad() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlMoneda.Text = gsDesc_Moneda
            rpt.xrlTitulo.Text = "VENTAS POR VENDEDOR Y UTILIDAD (DETALLADO), SUCURSAL: " + leSucursal.Text + ", PUNTO DE VENTA: " + lePuntoVenta.Text
            rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
            rpt.ShowPreviewDialog()
        Else
            If rg1.SelectedIndex = 1 Then
                Dim rpt2 As New fac_rptVendedoresUtilidadConsolidado() With {.DataSource = dt, .DataMember = ""}
                rpt2.xrlEmpresa.Text = gsNombre_Empresa
                rpt2.xrlMoneda.Text = gsDesc_Moneda
                rpt2.xrlTitulo.Text = "VENTAS POR VENDEDOR Y UTILIDAD (CONSOLIDADO POR LINEA), SUCURSAL: " + leSucursal.Text + ", PUNTO DE VENTA: " + lePuntoVenta.Text
                rpt2.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
                rpt2.ShowPreviewDialog()
            Else
                Dim rpt3 As New fac_rptVendedoresUtilidadConsolidadoVen() With {.DataSource = dt, .DataMember = ""}
                rpt3.xrlEmpresa.Text = gsNombre_Empresa
                rpt3.xrlMoneda.Text = gsDesc_Moneda
                rpt3.xrlBodegas.Text = leBodega.Text
                rpt3.xrlTitulo.Text = "VENTAS POR VENDEDOR Y UTILIDAD (CONSOLIDADO POR VENDEDOR), SUCURSAL: " + leSucursal.Text + ", PUNTO DE VENTA: " + lePuntoVenta.Text
                rpt3.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
                rpt3.ShowPreviewDialog()
            End If
        End If
    End Sub
End Class
