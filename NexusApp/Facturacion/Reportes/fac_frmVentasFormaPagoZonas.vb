﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmVentasFormaPagoZonas
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim Periodo As String

    Dim dt As New DataTable


    Private Sub frmFacReportes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.admDepartamentos(leDepartamento, "-- TODOS LOS DEPARTAMENTOS --")
        objCombos.admMunicipios(leMunicipio, leDepartamento.EditValue)
        leDepto_EditValueChanged("", New EventArgs())

        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        leSucursal.EditValue = piIdSucursal
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "")
        objCombos.fac_FormasPago(leFormaPago, "-- TODAS --")
        deDesde.EditValue = CDate("1/" & Month(Today) & "/" & Year(Today))
        objCombos.fac_Vendedores(leVendedor, "-- TODOS LOS VENDEDORES --")
        deHasta.EditValue = Today
    End Sub

    Private Sub SimpleButton1_Click() Handles Me.Reporte
        dt = bl.fac_VentasFormaPagoZonas(deDesde.EditValue, deHasta.EditValue, leFormaPago.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, leVendedor.EditValue, leDepartamento.EditValue, leMunicipio.EditValue)
        Dim rpt As New fac_rptVentasFormaPagoZonas() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, SUCURSAL: {1}, PUNTO DE VENTA: {2}", teTitulo.Text, leSucursal.Text, lePuntoVenta.Text)
        rpt.xrlVendedor.Text = "Vendedor: " & leVendedor.Text
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue).ToUpper()
        rpt.ShowPreviewDialog()
    End Sub


    Private Sub leSucursal_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub

    Private Sub leDepto_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leDepartamento.EditValueChanged
        objCombos.admMunicipios(leMunicipio, leDepartamento.EditValue, "-- TODOS LOS MUNICIPIOS --")
    End Sub
End Class
