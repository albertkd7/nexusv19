﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class fac_rptDetalleUnico
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.Factura2 = New DevExpress.XtraReports.UI.XRLabel
        Me.LecturaAnterior2 = New DevExpress.XtraReports.UI.XRLabel
        Me.ConsumoM32 = New DevExpress.XtraReports.UI.XRLabel
        Me.LecturaActual2 = New DevExpress.XtraReports.UI.XRLabel
        Me.FechaVence4 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.HeightF = 0.0!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.HeightF = 26.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        Me.BottomMarginBand1.SnapLinePadding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.Factura2, Me.LecturaAnterior2, Me.ConsumoM32, Me.LecturaActual2, Me.FechaVence4, Me.XrLabel1})
        Me.Detail.HeightF = 18.70824!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'Factura2
        '
        Me.Factura2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.VentaAfecta", "{0:n2}")})
        Me.Factura2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Factura2.LocationFloat = New DevExpress.Utils.PointFloat(680.2084!, 0.0!)
        Me.Factura2.Name = "Factura2"
        Me.Factura2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.Factura2.SizeF = New System.Drawing.SizeF(81.99997!, 16.37481!)
        Me.Factura2.StylePriority.UseFont = False
        Me.Factura2.StylePriority.UseTextAlignment = False
        Me.Factura2.Text = "Factura2"
        Me.Factura2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'LecturaAnterior2
        '
        Me.LecturaAnterior2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.Cantidad", "{0:n0}")})
        Me.LecturaAnterior2.Font = New System.Drawing.Font("Trebuchet MS", 8.0!)
        Me.LecturaAnterior2.LocationFloat = New DevExpress.Utils.PointFloat(30.83327!, 0.0!)
        Me.LecturaAnterior2.Name = "LecturaAnterior2"
        Me.LecturaAnterior2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LecturaAnterior2.SizeF = New System.Drawing.SizeF(54.79133!, 16.37481!)
        Me.LecturaAnterior2.StylePriority.UseFont = False
        Me.LecturaAnterior2.StylePriority.UseTextAlignment = False
        Me.LecturaAnterior2.Text = "LecturaAnterior2"
        Me.LecturaAnterior2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ConsumoM32
        '
        Me.ConsumoM32.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.PrecioUnitario", "{0:n2}")})
        Me.ConsumoM32.Font = New System.Drawing.Font("Trebuchet MS", 8.0!)
        Me.ConsumoM32.LocationFloat = New DevExpress.Utils.PointFloat(505.7079!, 0.0!)
        Me.ConsumoM32.Name = "ConsumoM32"
        Me.ConsumoM32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.ConsumoM32.SizeF = New System.Drawing.SizeF(66.41669!, 16.37481!)
        Me.ConsumoM32.StylePriority.UseFont = False
        Me.ConsumoM32.StylePriority.UseTextAlignment = False
        Me.ConsumoM32.Text = "ConsumoM32"
        Me.ConsumoM32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'LecturaActual2
        '
        Me.LecturaActual2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.Descripcion")})
        Me.LecturaActual2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LecturaActual2.LocationFloat = New DevExpress.Utils.PointFloat(86.2496!, 0.0!)
        Me.LecturaActual2.Name = "LecturaActual2"
        Me.LecturaActual2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LecturaActual2.SizeF = New System.Drawing.SizeF(416.4582!, 16.37481!)
        Me.LecturaActual2.StylePriority.UseFont = False
        Me.LecturaActual2.StylePriority.UseTextAlignment = False
        Me.LecturaActual2.Text = "LecturaActual2"
        Me.LecturaActual2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'FechaVence4
        '
        Me.FechaVence4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.VentaExenta", "{0:n2}")})
        Me.FechaVence4.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaVence4.LocationFloat = New DevExpress.Utils.PointFloat(620.4999!, 0.0!)
        Me.FechaVence4.Name = "FechaVence4"
        Me.FechaVence4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.FechaVence4.SizeF = New System.Drawing.SizeF(61.7085!, 16.37481!)
        Me.FechaVence4.StylePriority.UseFont = False
        Me.FechaVence4.StylePriority.UseTextAlignment = False
        Me.FechaVence4.Text = "0.00"
        Me.FechaVence4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Factura.VentaNoSujeta", "{0:n2}")})
        Me.XrLabel1.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(572.1246!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(59.7085!, 16.37481!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "XrLabel1"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.HeightF = 5.375004!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'PageFooter
        '
        Me.PageFooter.HeightF = 0.624911!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'fac_rptDetalleUnico
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.DataMember = "Factura"
        Me.DesignerOptions.ShowExportWarnings = False
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(23, 40, 5, 26)
        Me.PageHeight = 630
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.SnapGridSize = 2.083333!
        Me.SnapToGrid = False
        Me.Version = "11.1"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents Factura2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents LecturaAnterior2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ConsumoM32 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents LecturaActual2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents FechaVence4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    'Friend WithEvents DsGas1 As Nexus.dsGas
End Class
