<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class fac_rptCotizacion
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_rptCotizacion))
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary4 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary5 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.XrLabel28 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlAtencionA = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlForma = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrpFecha = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPict6 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPict5 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPict4 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPict3 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPict2 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPict1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrLabel33 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotalFinal = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDetallaIVA = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlIva = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel29 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlConcepto = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel25 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlGarantia = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlRetencion = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel41 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDias = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel54 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTiempo = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlVendedor = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.xrlDireccion = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLogo = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.xrpNumero = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.xrlNumero = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFecha = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNombre = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel38 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlSucursal = New DevExpress.XtraReports.UI.XRLabel()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.xrlProyecto = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrLabel30 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlTelefono = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlEmpresa = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel49 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlPage = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNrc = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlNit = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCodigo = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel52 = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'XrLabel28
        '
        Me.XrLabel28.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Cotizaciones.PrecioTotal", "{0:n2}")})
        Me.XrLabel28.Dpi = 100.0!
        Me.XrLabel28.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel28.LocationFloat = New DevExpress.Utils.PointFloat(691.0!, 0!)
        Me.XrLabel28.Name = "XrLabel28"
        Me.XrLabel28.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel28.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel28.SizeF = New System.Drawing.SizeF(91.0!, 13.0!)
        Me.XrLabel28.StylePriority.UseFont = False
        Me.XrLabel28.StylePriority.UseTextAlignment = False
        XrSummary1.FormatString = "{0:n2}"
        Me.XrLabel28.Summary = XrSummary1
        Me.XrLabel28.Text = "XrLabel28"
        Me.XrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel10
        '
        Me.XrLabel10.Dpi = 100.0!
        Me.XrLabel10.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(578.2911!, 358.5833!)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(145.7088!, 13.99998!)
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.StylePriority.UseTextAlignment = False
        Me.XrLabel10.Text = "ACEPTADO CLIENTE"
        Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlAtencionA
        '
        Me.xrlAtencionA.Dpi = 100.0!
        Me.xrlAtencionA.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlAtencionA.LocationFloat = New DevExpress.Utils.PointFloat(107.0!, 187.0!)
        Me.xrlAtencionA.Name = "xrlAtencionA"
        Me.xrlAtencionA.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlAtencionA.SizeF = New System.Drawing.SizeF(484.0!, 16.0!)
        Me.xrlAtencionA.StylePriority.UseFont = False
        Me.xrlAtencionA.StylePriority.UseTextAlignment = False
        Me.xrlAtencionA.Text = "xrlAtencionA"
        Me.xrlAtencionA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlForma
        '
        Me.xrlForma.Dpi = 100.0!
        Me.xrlForma.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlForma.LocationFloat = New DevExpress.Utils.PointFloat(97.0!, 145.8333!)
        Me.xrlForma.Name = "xrlForma"
        Me.xrlForma.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlForma.SizeF = New System.Drawing.SizeF(193.0!, 18.99999!)
        Me.xrlForma.StylePriority.UseFont = False
        Me.xrlForma.StylePriority.UseTextAlignment = False
        Me.xrlForma.Text = "[FormaPago]"
        Me.xrlForma.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrpFecha
        '
        Me.xrpFecha.Dpi = 100.0!
        Me.xrpFecha.Format = "{0:dd/MM/yyyy hh:mm tt}"
        Me.xrpFecha.LocationFloat = New DevExpress.Utils.PointFloat(672.0!, 60.125!)
        Me.xrpFecha.Name = "xrpFecha"
        Me.xrpFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpFecha.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.xrpFecha.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        '
        'XrLabel7
        '
        Me.XrLabel7.Dpi = 100.0!
        Me.XrLabel7.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(12.0!, 207.0!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(47.0!, 15.0!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "Cliente:"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel9
        '
        Me.XrLabel9.Dpi = 100.0!
        Me.XrLabel9.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(489.0!, 207.0!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(41.0!, 15.0!)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.Text = "Fecha:"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox1, Me.XrPict6, Me.XrPict5, Me.XrPict4, Me.XrPict3, Me.XrPict2, Me.XrPict1, Me.XrLabel33, Me.xrlTotalFinal, Me.xrlDetallaIVA, Me.xrlIva, Me.XrLabel29, Me.XrLabel18, Me.XrLabel10, Me.XrLabel5, Me.XrLabel4, Me.XrLine3, Me.xrlConcepto, Me.XrLabel25, Me.xrlGarantia, Me.XrLabel20, Me.xrlTotal, Me.xrlRetencion, Me.XrLabel12, Me.XrLabel8, Me.XrLine2, Me.XrLabel41, Me.xrlForma, Me.xrlDias, Me.XrLabel54, Me.XrLabel19, Me.xrlTiempo, Me.xrlVendedor, Me.XrLine1})
        Me.GroupFooter1.Dpi = 100.0!
        Me.GroupFooter1.HeightF = 386.5833!
        Me.GroupFooter1.Name = "GroupFooter1"
        Me.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Dpi = 100.0!
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(17.3544!, 277.8334!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(748.6455!, 67.29166!)
        Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'XrPict6
        '
        Me.XrPict6.Dpi = 100.0!
        Me.XrPict6.Image = CType(resources.GetObject("XrPict6.Image"), System.Drawing.Image)
        Me.XrPict6.LocationFloat = New DevExpress.Utils.PointFloat(668.0!, 0!)
        Me.XrPict6.Name = "XrPict6"
        Me.XrPict6.SizeF = New System.Drawing.SizeF(129.0!, 94.37498!)
        Me.XrPict6.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'XrPict5
        '
        Me.XrPict5.Dpi = 100.0!
        Me.XrPict5.Image = CType(resources.GetObject("XrPict5.Image"), System.Drawing.Image)
        Me.XrPict5.LocationFloat = New DevExpress.Utils.PointFloat(533.9169!, 0!)
        Me.XrPict5.Name = "XrPict5"
        Me.XrPict5.SizeF = New System.Drawing.SizeF(133.9375!, 94.37498!)
        Me.XrPict5.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'XrPict4
        '
        Me.XrPict4.Dpi = 100.0!
        Me.XrPict4.Image = CType(resources.GetObject("XrPict4.Image"), System.Drawing.Image)
        Me.XrPict4.LocationFloat = New DevExpress.Utils.PointFloat(404.6838!, 0!)
        Me.XrPict4.Name = "XrPict4"
        Me.XrPict4.SizeF = New System.Drawing.SizeF(129.2331!, 94.37498!)
        Me.XrPict4.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'XrPict3
        '
        Me.XrPict3.Dpi = 100.0!
        Me.XrPict3.Image = CType(resources.GetObject("XrPict3.Image"), System.Drawing.Image)
        Me.XrPict3.LocationFloat = New DevExpress.Utils.PointFloat(273.521!, 0!)
        Me.XrPict3.Name = "XrPict3"
        Me.XrPict3.SizeF = New System.Drawing.SizeF(131.1628!, 94.37498!)
        Me.XrPict3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'XrPict2
        '
        Me.XrPict2.Dpi = 100.0!
        Me.XrPict2.Image = CType(resources.GetObject("XrPict2.Image"), System.Drawing.Image)
        Me.XrPict2.LocationFloat = New DevExpress.Utils.PointFloat(138.8544!, 0!)
        Me.XrPict2.Name = "XrPict2"
        Me.XrPict2.SizeF = New System.Drawing.SizeF(134.6667!, 94.37498!)
        Me.XrPict2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'XrPict1
        '
        Me.XrPict1.Dpi = 100.0!
        Me.XrPict1.Image = CType(resources.GetObject("XrPict1.Image"), System.Drawing.Image)
        Me.XrPict1.LocationFloat = New DevExpress.Utils.PointFloat(3.854378!, 0!)
        Me.XrPict1.Name = "XrPict1"
        Me.XrPict1.SizeF = New System.Drawing.SizeF(135.0!, 94.37498!)
        Me.XrPict1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'XrLabel33
        '
        Me.XrLabel33.Dpi = 100.0!
        Me.XrLabel33.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel33.LocationFloat = New DevExpress.Utils.PointFloat(594.0001!, 196.9584!)
        Me.XrLabel33.Name = "XrLabel33"
        Me.XrLabel33.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel33.SizeF = New System.Drawing.SizeF(93.00006!, 15.99998!)
        Me.XrLabel33.StylePriority.UseFont = False
        Me.XrLabel33.StylePriority.UseTextAlignment = False
        Me.XrLabel33.Text = "Total:"
        Me.XrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotalFinal
        '
        Me.xrlTotalFinal.Dpi = 100.0!
        Me.xrlTotalFinal.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrlTotalFinal.LocationFloat = New DevExpress.Utils.PointFloat(691.0!, 196.9584!)
        Me.xrlTotalFinal.Name = "xrlTotalFinal"
        Me.xrlTotalFinal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotalFinal.SizeF = New System.Drawing.SizeF(89.0!, 16.0!)
        Me.xrlTotalFinal.StylePriority.UseFont = False
        Me.xrlTotalFinal.StylePriority.UseTextAlignment = False
        XrSummary2.FormatString = "{0:n2}"
        XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom
        Me.xrlTotalFinal.Summary = XrSummary2
        Me.xrlTotalFinal.Text = "xrlTotalFinal"
        Me.xrlTotalFinal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.xrlTotalFinal.Visible = False
        '
        'xrlDetallaIVA
        '
        Me.xrlDetallaIVA.Dpi = 100.0!
        Me.xrlDetallaIVA.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.xrlDetallaIVA.LocationFloat = New DevExpress.Utils.PointFloat(594.0002!, 179.9584!)
        Me.xrlDetallaIVA.Name = "xrlDetallaIVA"
        Me.xrlDetallaIVA.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDetallaIVA.SizeF = New System.Drawing.SizeF(93.00006!, 15.99998!)
        Me.xrlDetallaIVA.StylePriority.UseFont = False
        Me.xrlDetallaIVA.StylePriority.UseTextAlignment = False
        Me.xrlDetallaIVA.Text = "IVA:"
        Me.xrlDetallaIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlIva
        '
        Me.xrlIva.Dpi = 100.0!
        Me.xrlIva.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrlIva.LocationFloat = New DevExpress.Utils.PointFloat(691.0!, 179.9584!)
        Me.xrlIva.Name = "xrlIva"
        Me.xrlIva.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIva.SizeF = New System.Drawing.SizeF(89.0!, 16.0!)
        Me.xrlIva.StylePriority.UseFont = False
        Me.xrlIva.StylePriority.UseTextAlignment = False
        XrSummary3.FormatString = "{0:n2}"
        XrSummary3.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom
        Me.xrlIva.Summary = XrSummary3
        Me.xrlIva.Text = "xrlTotalFinal"
        Me.xrlIva.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.xrlIva.Visible = False
        '
        'XrLabel29
        '
        Me.XrLabel29.Dpi = 100.0!
        Me.XrLabel29.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel29.LocationFloat = New DevExpress.Utils.PointFloat(594.0!, 161.9584!)
        Me.XrLabel29.Name = "XrLabel29"
        Me.XrLabel29.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel29.SizeF = New System.Drawing.SizeF(93.00006!, 15.99998!)
        Me.XrLabel29.StylePriority.UseFont = False
        Me.XrLabel29.StylePriority.UseTextAlignment = False
        Me.XrLabel29.Text = "Retenci�n:"
        Me.XrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel18
        '
        Me.XrLabel18.Dpi = 100.0!
        Me.XrLabel18.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(18.0!, 264.8334!)
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(134.3544!, 12.99997!)
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.StylePriority.UseTextAlignment = False
        Me.XrLabel18.Text = "GRACIAS POR COTIZAR"
        Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel5
        '
        Me.XrLabel5.Dpi = 100.0!
        Me.XrLabel5.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(62.0!, 220.8334!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(316.8332!, 12.99998!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "PRECIOS Y EXISTENCIAS SUJETOS A PREVIA VENTA"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel4
        '
        Me.XrLabel4.Dpi = 100.0!
        Me.XrLabel4.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(62.0!, 207.8334!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(316.8332!, 12.99998!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "VALIDEZ DE LA OFERTA 8 DIAS DESPUES DE SU EMISI�N"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLine3
        '
        Me.XrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine3.Dpi = 100.0!
        Me.XrLine3.LocationFloat = New DevExpress.Utils.PointFloat(3.854378!, 142.8333!)
        Me.XrLine3.Name = "XrLine3"
        Me.XrLine3.SizeF = New System.Drawing.SizeF(793.1456!, 2.0!)
        Me.XrLine3.StylePriority.UseBorders = False
        '
        'xrlConcepto
        '
        Me.xrlConcepto.Dpi = 100.0!
        Me.xrlConcepto.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlConcepto.LocationFloat = New DevExpress.Utils.PointFloat(62.0!, 236.8334!)
        Me.xrlConcepto.Name = "xrlConcepto"
        Me.xrlConcepto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlConcepto.SizeF = New System.Drawing.SizeF(645.9999!, 14.99998!)
        Me.xrlConcepto.StylePriority.UseFont = False
        Me.xrlConcepto.StylePriority.UseTextAlignment = False
        Me.xrlConcepto.Text = "xrlConcepto"
        Me.xrlConcepto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel25
        '
        Me.XrLabel25.Dpi = 100.0!
        Me.XrLabel25.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel25.LocationFloat = New DevExpress.Utils.PointFloat(18.0!, 205.8334!)
        Me.XrLabel25.Name = "XrLabel25"
        Me.XrLabel25.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel25.SizeF = New System.Drawing.SizeF(42.00001!, 18.99998!)
        Me.XrLabel25.StylePriority.UseFont = False
        Me.XrLabel25.StylePriority.UseTextAlignment = False
        Me.XrLabel25.Text = "Notas:"
        Me.XrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlGarantia
        '
        Me.xrlGarantia.Dpi = 100.0!
        Me.xrlGarantia.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlGarantia.LocationFloat = New DevExpress.Utils.PointFloat(75.99999!, 184.8334!)
        Me.xrlGarantia.Name = "xrlGarantia"
        Me.xrlGarantia.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlGarantia.SizeF = New System.Drawing.SizeF(515.0001!, 18.99998!)
        Me.xrlGarantia.StylePriority.UseFont = False
        Me.xrlGarantia.StylePriority.UseTextAlignment = False
        Me.xrlGarantia.Text = "xrlGarantia"
        Me.xrlGarantia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel20
        '
        Me.XrLabel20.Dpi = 100.0!
        Me.XrLabel20.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(18.0!, 184.8334!)
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.SizeF = New System.Drawing.SizeF(55.0!, 19.0!)
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.StylePriority.UseTextAlignment = False
        Me.XrLabel20.Text = "Garant�a:"
        Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTotal
        '
        Me.xrlTotal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Cotizaciones.PrecioTotal")})
        Me.xrlTotal.Dpi = 100.0!
        Me.xrlTotal.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlTotal.LocationFloat = New DevExpress.Utils.PointFloat(691.0!, 144.8333!)
        Me.xrlTotal.Name = "xrlTotal"
        Me.xrlTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.xrlTotal.SizeF = New System.Drawing.SizeF(89.0!, 15.0!)
        Me.xrlTotal.StylePriority.UseFont = False
        Me.xrlTotal.StylePriority.UseTextAlignment = False
        XrSummary4.FormatString = "{0:c2}"
        XrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
        Me.xrlTotal.Summary = XrSummary4
        Me.xrlTotal.Text = "xrlTotal"
        Me.xrlTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlRetencion
        '
        Me.xrlRetencion.Dpi = 100.0!
        Me.xrlRetencion.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.xrlRetencion.LocationFloat = New DevExpress.Utils.PointFloat(691.0!, 161.9584!)
        Me.xrlRetencion.Name = "xrlRetencion"
        Me.xrlRetencion.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlRetencion.SizeF = New System.Drawing.SizeF(89.0!, 16.0!)
        Me.xrlRetencion.StylePriority.UseFont = False
        Me.xrlRetencion.StylePriority.UseTextAlignment = False
        XrSummary5.FormatString = "{0:n2}"
        XrSummary5.Func = DevExpress.XtraReports.UI.SummaryFunc.Custom
        Me.xrlRetencion.Summary = XrSummary5
        Me.xrlRetencion.Text = "xrlRetencion"
        Me.xrlRetencion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.xrlRetencion.Visible = False
        '
        'XrLabel12
        '
        Me.XrLabel12.Dpi = 100.0!
        Me.XrLabel12.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(594.0001!, 144.8333!)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(93.00006!, 15.99998!)
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "Sub-Total:"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel8
        '
        Me.XrLabel8.Dpi = 100.0!
        Me.XrLabel8.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(55.0!, 372.5833!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(145.7088!, 13.99998!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "VENDEDOR"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine2
        '
        Me.XrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine2.Dpi = 100.0!
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(19.0!, 355.5833!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(219.0!, 2.0!)
        Me.XrLine2.StylePriority.UseBorders = False
        '
        'XrLabel41
        '
        Me.XrLabel41.Dpi = 100.0!
        Me.XrLabel41.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel41.LocationFloat = New DevExpress.Utils.PointFloat(18.0!, 144.8333!)
        Me.XrLabel41.Name = "XrLabel41"
        Me.XrLabel41.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel41.SizeF = New System.Drawing.SizeF(75.0!, 19.0!)
        Me.XrLabel41.StylePriority.UseFont = False
        Me.XrLabel41.StylePriority.UseTextAlignment = False
        Me.XrLabel41.Text = "Forma Pago :"
        Me.XrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDias
        '
        Me.xrlDias.Dpi = 100.0!
        Me.xrlDias.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlDias.LocationFloat = New DevExpress.Utils.PointFloat(367.0!, 145.8333!)
        Me.xrlDias.Name = "xrlDias"
        Me.xrlDias.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDias.SizeF = New System.Drawing.SizeF(41.0!, 19.0!)
        Me.xrlDias.StylePriority.UseFont = False
        Me.xrlDias.StylePriority.UseTextAlignment = False
        Me.xrlDias.Text = "[DiasCredito]"
        Me.xrlDias.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel54
        '
        Me.XrLabel54.Dpi = 100.0!
        Me.XrLabel54.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel54.LocationFloat = New DevExpress.Utils.PointFloat(291.0!, 145.8333!)
        Me.XrLabel54.Name = "XrLabel54"
        Me.XrLabel54.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel54.SizeF = New System.Drawing.SizeF(75.0!, 19.0!)
        Me.XrLabel54.StylePriority.UseFont = False
        Me.XrLabel54.StylePriority.UseTextAlignment = False
        Me.XrLabel54.Text = "Dias Credito:"
        Me.XrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel19
        '
        Me.XrLabel19.Dpi = 100.0!
        Me.XrLabel19.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(18.0!, 165.8333!)
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.SizeF = New System.Drawing.SizeF(112.0!, 19.0!)
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        Me.XrLabel19.Text = "Tiempo de Entrega:"
        Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTiempo
        '
        Me.xrlTiempo.Dpi = 100.0!
        Me.xrlTiempo.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlTiempo.LocationFloat = New DevExpress.Utils.PointFloat(130.0!, 165.8333!)
        Me.xrlTiempo.Name = "xrlTiempo"
        Me.xrlTiempo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTiempo.SizeF = New System.Drawing.SizeF(319.0!, 19.0!)
        Me.xrlTiempo.StylePriority.UseFont = False
        Me.xrlTiempo.StylePriority.UseTextAlignment = False
        Me.xrlTiempo.Text = "xrlTiempo"
        Me.xrlTiempo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVendedor
        '
        Me.xrlVendedor.Dpi = 100.0!
        Me.xrlVendedor.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlVendedor.LocationFloat = New DevExpress.Utils.PointFloat(20.0!, 358.5833!)
        Me.xrlVendedor.Name = "xrlVendedor"
        Me.xrlVendedor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVendedor.SizeF = New System.Drawing.SizeF(220.0!, 12.99998!)
        Me.xrlVendedor.StylePriority.UseFont = False
        Me.xrlVendedor.StylePriority.UseTextAlignment = False
        Me.xrlVendedor.Text = "[Vendedor]"
        Me.xrlVendedor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine1
        '
        Me.XrLine1.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrLine1.Dpi = 100.0!
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(547.0!, 355.5833!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(219.0!, 2.0!)
        Me.XrLine1.StylePriority.UseBorders = False
        '
        'xrlDireccion
        '
        Me.xrlDireccion.Dpi = 100.0!
        Me.xrlDireccion.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlDireccion.LocationFloat = New DevExpress.Utils.PointFloat(74.00001!, 245.0!)
        Me.xrlDireccion.Name = "xrlDireccion"
        Me.xrlDireccion.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDireccion.SizeF = New System.Drawing.SizeF(716.0!, 15.99998!)
        Me.xrlDireccion.StylePriority.UseFont = False
        Me.xrlDireccion.StylePriority.UseTextAlignment = False
        Me.xrlDireccion.Text = "[Direccion]"
        Me.xrlDireccion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Dpi = 100.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(265.6458!, 81.12495!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(396.0!, 20.00001!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "8a Avenida Sur entre  5� y 7� Calle Poniente  # 7"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrLogo
        '
        Me.xrLogo.Dpi = 100.0!
        Me.xrLogo.Image = CType(resources.GetObject("xrLogo.Image"), System.Drawing.Image)
        Me.xrLogo.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 1.000007!)
        Me.xrLogo.Name = "xrLogo"
        Me.xrLogo.SizeF = New System.Drawing.SizeF(239.6458!, 168.5833!)
        Me.xrLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'xrpNumero
        '
        Me.xrpNumero.Dpi = 100.0!
        Me.xrpNumero.LocationFloat = New DevExpress.Utils.PointFloat(731.0001!, 44.12501!)
        Me.xrpNumero.Name = "xrpNumero"
        Me.xrpNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpNumero.SizeF = New System.Drawing.SizeF(66.0!, 16.0!)
        '
        'xrlNumero
        '
        Me.xrlNumero.Dpi = 100.0!
        Me.xrlNumero.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.xrlNumero.LocationFloat = New DevExpress.Utils.PointFloat(705.0!, 101.0!)
        Me.xrlNumero.Name = "xrlNumero"
        Me.xrlNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero.SizeF = New System.Drawing.SizeF(76.0!, 19.0!)
        Me.xrlNumero.StylePriority.UseFont = False
        Me.xrlNumero.StylePriority.UseTextAlignment = False
        Me.xrlNumero.Text = "xrlNumero"
        Me.xrlNumero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlFecha
        '
        Me.xrlFecha.Dpi = 100.0!
        Me.xrlFecha.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlFecha.LocationFloat = New DevExpress.Utils.PointFloat(535.0!, 207.0!)
        Me.xrlFecha.Name = "xrlFecha"
        Me.xrlFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha.SizeF = New System.Drawing.SizeF(163.0!, 15.0!)
        Me.xrlFecha.StylePriority.UseFont = False
        Me.xrlFecha.StylePriority.UseTextAlignment = False
        Me.xrlFecha.Text = "xrlFecha"
        Me.xrlFecha.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNombre
        '
        Me.xrlNombre.Dpi = 100.0!
        Me.xrlNombre.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlNombre.LocationFloat = New DevExpress.Utils.PointFloat(127.0!, 207.0!)
        Me.xrlNombre.Name = "xrlNombre"
        Me.xrlNombre.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNombre.SizeF = New System.Drawing.SizeF(319.0!, 15.0!)
        Me.xrlNombre.StylePriority.UseFont = False
        Me.xrlNombre.StylePriority.UseTextAlignment = False
        Me.xrlNombre.Text = "xrlNombre"
        Me.xrlNombre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel28, Me.XrLabel26, Me.XrLabel11, Me.XrLabel3, Me.XrLabel2})
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 14.75004!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel26
        '
        Me.XrLabel26.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Cotizaciones.Cantidad")})
        Me.XrLabel26.Dpi = 100.0!
        Me.XrLabel26.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 0!)
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.SizeF = New System.Drawing.SizeF(69.0!, 13.0!)
        Me.XrLabel26.StylePriority.UseFont = False
        Me.XrLabel26.StylePriority.UseTextAlignment = False
        Me.XrLabel26.Text = "XrLabel26"
        Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel11
        '
        Me.XrLabel11.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Cotizaciones.Descripcion", "{0:n2}")})
        Me.XrLabel11.Dpi = 100.0!
        Me.XrLabel11.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(176.0!, 0!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(405.9189!, 13.00001!)
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "XrLabel11"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel3
        '
        Me.XrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Cotizaciones.PrecioUnitario", "{0:n2}")})
        Me.XrLabel3.Dpi = 100.0!
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(594.0!, 0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(81.0!, 13.0!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "XrLabel3"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Cotizaciones.IdProducto")})
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(82.0!, 13.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.Text = "XrLabel2"
        '
        'XrLabel14
        '
        Me.XrLabel14.Dpi = 100.0!
        Me.XrLabel14.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(231.0!, 226.0!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(34.0!, 16.0!)
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        Me.XrLabel14.Text = "N I T:"
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel38
        '
        Me.XrLabel38.Dpi = 100.0!
        Me.XrLabel38.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel38.LocationFloat = New DevExpress.Utils.PointFloat(687.9999!, 81.0!)
        Me.XrLabel38.Name = "XrLabel38"
        Me.XrLabel38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel38.SizeF = New System.Drawing.SizeF(101.0001!, 19.0!)
        Me.XrLabel38.StylePriority.UseFont = False
        Me.XrLabel38.StylePriority.UseTextAlignment = False
        Me.XrLabel38.Text = "No. Cotizaci�n"
        Me.XrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlSucursal
        '
        Me.xrlSucursal.Dpi = 100.0!
        Me.xrlSucursal.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlSucursal.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 227.0!)
        Me.xrlSucursal.Name = "xrlSucursal"
        Me.xrlSucursal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSucursal.SizeF = New System.Drawing.SizeF(222.0!, 15.00002!)
        Me.xrlSucursal.StylePriority.UseFont = False
        Me.xrlSucursal.StylePriority.UseTextAlignment = False
        Me.xrlSucursal.Text = "[Sucursal]"
        Me.xrlSucursal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlSucursal.Visible = False
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.Dpi = 100.0!
        Me.BottomMarginBand1.HeightF = 35.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlProyecto, Me.XrTable2, Me.XrLabel30, Me.xrlTelefono, Me.XrLabel6, Me.XrLabel16, Me.XrLabel15, Me.XrLabel1, Me.xrlEmpresa, Me.xrLogo, Me.xrlDireccion, Me.xrlAtencionA, Me.XrLabel49, Me.xrpFecha, Me.xrlPage, Me.xrpNumero, Me.XrLabel7, Me.xrlNombre, Me.XrLabel9, Me.xrlFecha, Me.xrlNrc, Me.XrLabel13, Me.XrLabel14, Me.xrlNit, Me.XrLabel17, Me.XrLabel38, Me.xrlNumero, Me.xrlCodigo, Me.XrLabel52, Me.xrlSucursal})
        Me.ReportHeader.Dpi = 100.0!
        Me.ReportHeader.HeightF = 304.625!
        Me.ReportHeader.Name = "ReportHeader"
        Me.ReportHeader.StyleName = "XrControlStyle1"
        '
        'xrlProyecto
        '
        Me.xrlProyecto.Dpi = 100.0!
        Me.xrlProyecto.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlProyecto.LocationFloat = New DevExpress.Utils.PointFloat(506.0!, 171.0!)
        Me.xrlProyecto.Name = "xrlProyecto"
        Me.xrlProyecto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlProyecto.SizeF = New System.Drawing.SizeF(222.0!, 15.00002!)
        Me.xrlProyecto.StylePriority.UseFont = False
        Me.xrlProyecto.StylePriority.UseTextAlignment = False
        Me.xrlProyecto.Text = "[Sucursal]"
        Me.xrlProyecto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlProyecto.Visible = False
        '
        'XrTable2
        '
        Me.XrTable2.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable2.Dpi = 100.0!
        Me.XrTable2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
        Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(2.000006!, 284.625!)
        Me.XrTable2.Name = "XrTable2"
        Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
        Me.XrTable2.SizeF = New System.Drawing.SizeF(782.4219!, 20.0!)
        Me.XrTable2.StylePriority.UseBorders = False
        Me.XrTable2.StylePriority.UseFont = False
        Me.XrTable2.StylePriority.UseTextAlignment = False
        Me.XrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell7, Me.XrTableCell8, Me.XrTableCell9, Me.XrTableCell10})
        Me.XrTableRow2.Dpi = 100.0!
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 1.0R
        '
        'XrTableCell2
        '
        Me.XrTableCell2.Dpi = 100.0!
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.Text = "C�d.Producto"
        Me.XrTableCell2.Weight = 0.34339629116581694R
        '
        'XrTableCell7
        '
        Me.XrTableCell7.Dpi = 100.0!
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.Text = "Cantidad"
        Me.XrTableCell7.Weight = 0.30124310580986391R
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Dpi = 100.0!
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.Text = "Descripci�n / Concepto"
        Me.XrTableCell8.Weight = 1.5437340111403295R
        '
        'XrTableCell9
        '
        Me.XrTableCell9.Dpi = 100.0!
        Me.XrTableCell9.Name = "XrTableCell9"
        Me.XrTableCell9.Text = "Precio Unitario"
        Me.XrTableCell9.Weight = 0.38208124087506334R
        '
        'XrTableCell10
        '
        Me.XrTableCell10.Dpi = 100.0!
        Me.XrTableCell10.Name = "XrTableCell10"
        Me.XrTableCell10.Text = "Precio Total"
        Me.XrTableCell10.Weight = 0.38208124087506334R
        '
        'XrLabel30
        '
        Me.XrLabel30.Dpi = 100.0!
        Me.XrLabel30.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel30.LocationFloat = New DevExpress.Utils.PointFloat(638.9102!, 186.0!)
        Me.XrLabel30.Name = "XrLabel30"
        Me.XrLabel30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel30.SizeF = New System.Drawing.SizeF(41.0!, 15.0!)
        Me.XrLabel30.StylePriority.UseFont = False
        Me.XrLabel30.StylePriority.UseTextAlignment = False
        Me.XrLabel30.Text = "Fecha:"
        Me.XrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTelefono
        '
        Me.xrlTelefono.Dpi = 100.0!
        Me.xrlTelefono.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlTelefono.LocationFloat = New DevExpress.Utils.PointFloat(695.0!, 186.0!)
        Me.xrlTelefono.Name = "xrlTelefono"
        Me.xrlTelefono.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTelefono.SizeF = New System.Drawing.SizeF(91.0!, 15.0!)
        Me.xrlTelefono.StylePriority.UseFont = False
        Me.xrlTelefono.StylePriority.UseTextAlignment = False
        Me.xrlTelefono.Text = "TELEFONO"
        Me.xrlTelefono.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel6
        '
        Me.XrLabel6.Dpi = 100.0!
        Me.XrLabel6.Font = New System.Drawing.Font("Arial", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(389.2292!, 31.12495!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(148.8333!, 24.00002!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.Text = "COTIZACI�N"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel16
        '
        Me.XrLabel16.Dpi = 100.0!
        Me.XrLabel16.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(265.6458!, 122.125!)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(396.0!, 21.00002!)
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.StylePriority.UseTextAlignment = False
        Me.XrLabel16.Text = "PBX: 2486-6500"
        Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel15
        '
        Me.XrLabel15.Dpi = 100.0!
        Me.XrLabel15.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(265.6458!, 103.125!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(396.0!, 18.0!)
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        Me.XrLabel15.Text = "Santa Ana, El Salvador C.A."
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlEmpresa
        '
        Me.xrlEmpresa.Dpi = 100.0!
        Me.xrlEmpresa.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlEmpresa.LocationFloat = New DevExpress.Utils.PointFloat(672.0!, 10.00001!)
        Me.xrlEmpresa.Name = "xrlEmpresa"
        Me.xrlEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlEmpresa.SizeF = New System.Drawing.SizeF(125.0001!, 19.0!)
        Me.xrlEmpresa.StylePriority.UseFont = False
        Me.xrlEmpresa.StylePriority.UseTextAlignment = False
        Me.xrlEmpresa.Text = "xrlEmpresa"
        Me.xrlEmpresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        Me.xrlEmpresa.Visible = False
        '
        'XrLabel49
        '
        Me.XrLabel49.Dpi = 100.0!
        Me.XrLabel49.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel49.LocationFloat = New DevExpress.Utils.PointFloat(12.0!, 187.0!)
        Me.XrLabel49.Name = "XrLabel49"
        Me.XrLabel49.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel49.SizeF = New System.Drawing.SizeF(93.0!, 16.0!)
        Me.XrLabel49.StylePriority.UseFont = False
        Me.XrLabel49.StylePriority.UseTextAlignment = False
        Me.XrLabel49.Text = "Con Atenci�n a:"
        Me.XrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlPage
        '
        Me.xrlPage.Dpi = 100.0!
        Me.xrlPage.LocationFloat = New DevExpress.Utils.PointFloat(672.0!, 44.12501!)
        Me.xrlPage.Name = "xrlPage"
        Me.xrlPage.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPage.SizeF = New System.Drawing.SizeF(56.0!, 16.0!)
        Me.xrlPage.Text = "P�g. No."
        '
        'xrlNrc
        '
        Me.xrlNrc.Dpi = 100.0!
        Me.xrlNrc.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlNrc.LocationFloat = New DevExpress.Utils.PointFloat(62.0!, 226.0!)
        Me.xrlNrc.Name = "xrlNrc"
        Me.xrlNrc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNrc.SizeF = New System.Drawing.SizeF(81.0!, 16.0!)
        Me.xrlNrc.StylePriority.UseFont = False
        Me.xrlNrc.StylePriority.UseTextAlignment = False
        Me.xrlNrc.Text = "xrlNrc"
        Me.xrlNrc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel13
        '
        Me.XrLabel13.Dpi = 100.0!
        Me.XrLabel13.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(12.0!, 226.0!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(33.0!, 16.0!)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "NRC:"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNit
        '
        Me.xrlNit.Dpi = 100.0!
        Me.xrlNit.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlNit.LocationFloat = New DevExpress.Utils.PointFloat(265.0!, 226.0!)
        Me.xrlNit.Name = "xrlNit"
        Me.xrlNit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNit.SizeF = New System.Drawing.SizeF(153.0!, 16.0!)
        Me.xrlNit.StylePriority.UseFont = False
        Me.xrlNit.StylePriority.UseTextAlignment = False
        Me.xrlNit.Text = "[Nit]"
        Me.xrlNit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel17
        '
        Me.XrLabel17.Dpi = 100.0!
        Me.XrLabel17.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(12.0!, 245.0!)
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(61.0!, 16.0!)
        Me.XrLabel17.StylePriority.UseFont = False
        Me.XrLabel17.StylePriority.UseTextAlignment = False
        Me.XrLabel17.Text = "Direcci�n:"
        Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCodigo
        '
        Me.xrlCodigo.Dpi = 100.0!
        Me.xrlCodigo.Font = New System.Drawing.Font("Arial", 8.0!)
        Me.xrlCodigo.LocationFloat = New DevExpress.Utils.PointFloat(60.0!, 207.0!)
        Me.xrlCodigo.Name = "xrlCodigo"
        Me.xrlCodigo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCodigo.SizeF = New System.Drawing.SizeF(66.0!, 15.0!)
        Me.xrlCodigo.StylePriority.UseFont = False
        Me.xrlCodigo.StylePriority.UseTextAlignment = False
        Me.xrlCodigo.Text = "xrlCodigo"
        Me.xrlCodigo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel52
        '
        Me.XrLabel52.Dpi = 100.0!
        Me.XrLabel52.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel52.LocationFloat = New DevExpress.Utils.PointFloat(474.0!, 227.0!)
        Me.XrLabel52.Name = "XrLabel52"
        Me.XrLabel52.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel52.SizeF = New System.Drawing.SizeF(56.0!, 15.00002!)
        Me.XrLabel52.StylePriority.UseFont = False
        Me.XrLabel52.StylePriority.UseTextAlignment = False
        Me.XrLabel52.Text = "Sucursal:"
        Me.XrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.XrLabel52.Visible = False
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.Dpi = 100.0!
        Me.TopMarginBand1.HeightF = 40.20834!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'XrControlStyle1
        '
        Me.XrControlStyle1.Name = "XrControlStyle1"
        Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'fac_rptCotizacion
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.GroupFooter1, Me.ReportHeader, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 40, 35)
        Me.SnapGridSize = 5.0!
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
        Me.Version = "16.2"
        CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrLabel28 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlAtencionA As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlForma As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrpFecha As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLabel33 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotalFinal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDetallaIVA As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIva As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel29 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlConcepto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel25 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlGarantia As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlRetencion As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel41 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDias As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel54 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTiempo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVendedor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlDireccion As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLogo As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents xrpNumero As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents xrlNumero As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFecha As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNombre As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel38 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSucursal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlEmpresa As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel49 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPage As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNrc As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNit As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCodigo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel52 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents XrLabel30 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTelefono As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents xrlProyecto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPict6 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPict5 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPict4 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPict3 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPict2 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPict1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
End Class
