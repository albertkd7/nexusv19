﻿Imports NexusBLL
Imports System.Math

Public Class fac_frmLibroConsumidor
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blEmp As New AdmonBLL(g_ConnectionString)

    Private Sub frmLibroConsumidor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        meMes.Month = Now.Month
        spAnio.EditValue = Now.Year
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS --")

    End Sub


    Private Sub Rep_Click() Handles Me.Reporte
        Dim dt As New DataTable, dte As New DataTable
        dte = blEmp.ObtieneParametros()

        If ceLibroDetalle.EditValue Then
            dt = bl.getLibroConsumidorDetalle(meMes.Month, spAnio.EditValue, leSucursal.EditValue)
            Dim rpt As New fac_rptLibroConsumidorDetalle() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = "NOMBRE DEL CONTRIBUYENTE: " & gsNombre_Empresa

            rpt.xrlNrc.Text = "NRC:  " & dte.Rows(0).Item("NrcEmpresa")
            rpt.xrlNit.Text = "NIT:  " & dte.Rows(0).Item("NitEmpresa")
            rpt.xrlMes.Text = "MES:  " & ObtieneMesString(meMes.EditValue)
            rpt.xrlAnio.Text = "AÑO:  " & spAnio.EditValue
            rpt.xrlFolio.Visible = ceFoliarLibro.EditValue
            rpt.XrPageInfo.Visible = ceFoliarLibro.EditValue
            rpt.XrPageInfo.StartPageNumber = seNumFolio.EditValue
            rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
            rpt.ShowPreviewDialog()
        Else
            dt = bl.getLibroConsumidor(meMes.Month, spAnio.EditValue, leSucursal.EditValue)
            Dim rpt As New fac_rptLibroConsumidor() With {.DataSource = dt, .DataMember = ""}

            rpt.xrlEmpresa.Text = "NOMBRE DEL CONTRIBUYENTE: " & gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.EditValue
            rpt.xrlNrc.Text = "NRC:  " & dte.Rows(0).Item("NrcEmpresa")
            rpt.xrlNit.Text = "NIT:  " & dte.Rows(0).Item("NitEmpresa")
            rpt.xrlMes.Text = "MES:  " & ObtieneMesString(meMes.EditValue)
            rpt.xrlAnio.Text = "AÑO:  " & spAnio.EditValue
            rpt.xrlEmpresa.Visible = ceMostrarTitulos.EditValue
            rpt.xrlTitulo.Visible = ceMostrarTitulos.EditValue
            rpt.xrlNrc.Visible = ceMostrarTitulos.EditValue
            rpt.xrlNit.Visible = ceMostrarTitulos.EditValue
            rpt.xrlFolio.Visible = ceFoliarLibro.EditValue
            rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
            rpt.XrPageInfo.Visible = ceFoliarLibro.EditValue
            rpt.XrPageInfo.StartPageNumber = seNumFolio.EditValue

            rpt.ShowPreviewDialog()
        End If
    End Sub


    Private Sub btExportXLS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btExportXLS.Click
        ExportarLibroExcel()
    End Sub

    Private Sub ExportarLibroExcel()
        If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_ConsumidorFinal.XLSX") Then
            If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_ConsumidorFinal.XLS") Then
                MsgBox("No existe la plantilla necesaria para poder exportar", MsgBoxStyle.Critical, "Nota")
                Exit Sub
            End If
        End If

        Dim Template As String
        teProgreso.EditValue = "PREPARANDO LOS DATOS. ESPERE..."
        teProgreso.Visible = True
        If FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_ConsumidorFinal.XLSX") Then
            Template = Application.StartupPath & "\Plantillas\Libro_ConsumidorFinalTmp.XLSX"
            FileCopy(Application.StartupPath & "\Plantillas\Libro_ConsumidorFinal.XLSX", Template)
        Else
            Template = Application.StartupPath & "\Plantillas\Libro_ConsumidorFinalTmp.XLS"
            FileCopy(Application.StartupPath & "\Plantillas\Libro_ConsumidorFinal.XLS", Template)
        End If

        Dim oApp As Object = CreateObject("Excel.Application")

        'verifico si no está en una cultura diferente, por el momento de Inglés a Español, pendiente de verificar cuando sea a la inversa
        Dim oldCI As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        Try
            oApp.DisplayAlerts = False
        Catch ex As Exception
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")
            oApp.DisplayAlerts = False
        End Try

        Dim Range As String

        Dim dtParam As New DataTable
        dtParam = blEmp.ObtieneParametros()

        oApp.workbooks.Open(Template)
        oApp.Cells(2, 1).Value = "NOMBRE DEL CONTRIBUYENTE: " + gsNombre_Empresa
        oApp.Cells(4, 1).Value = "NRC: " & dtParam.Rows(0).Item("NrcEmpresa")
        oApp.Cells(4, 6).Value = "NIT: " & dtParam.Rows(0).Item("NitEmpresa")

        oApp.Cells(5, 1).Value = "MES: " + (ObtieneMesString(meMes.EditValue)).ToUpper
        oApp.Cells(5, 6).Value = "AÑO: " + (spAnio.EditValue).ToString
        oApp.Cells(5, 9).Value = "FOLIO No. " & seNumFolio.EditValue
        oApp.Cells(6, 1).Value = "SUCURSAL: " & leSucursal.Text
        oApp.Cells(6, 1).font.bold = True

        Dim TotExento As Decimal = 0.0, TotAfecto As Decimal = 0.0, TotExportacion As Decimal = 0.0, TotTerceros As Decimal = 0.0
        Dim TotGeneral As Decimal = 0.0, Total As Decimal = 0.0, TotalIVA As Decimal = 0.0, TotalRetencion As Decimal = 0.0
        Dim Linea As Integer = 9, i As Integer = 0
        Dim dt As New DataTable
        dt = bl.getLibroConsumidor(meMes.Month, spAnio.EditValue, leSucursal.EditValue)

        While i <= dt.Rows.Count - 1
            oApp.Cells(Linea, 1).Value = dt.Rows(i).Item("Dia")
            oApp.Cells(Linea, 2).Value = dt.Rows(i).Item("Desde")
            oApp.Cells(Linea, 3).Value = dt.Rows(i).Item("Hasta")
            oApp.Cells(Linea, 4).Value = 0.0
            oApp.Cells(Linea, 5).Value = dt.Rows(i).Item("Exento")
            oApp.Cells(Linea, 6).Value = dt.Rows(i).Item("Afecto")
            oApp.Cells(Linea, 7).Value = dt.Rows(i).Item("Exportacion")
            oApp.Cells(Linea, 8).Value = dt.Rows(i).Item("Retencion")
            oApp.Cells(Linea, 9).Value = dt.Rows(i).Item("Terceros")
            oApp.Cells(Linea, 10).Value = dt.Rows(i).Item("Total")

            Total += dt.Rows(i).Item("Total")
            TotExento += dt.Rows(i).Item("Exento")
            TotAfecto += dt.Rows(i).Item("Afecto")
            TotExportacion += dt.Rows(i).Item("Exportacion")
            TotTerceros += dt.Rows(i).Item("Terceros")
            TotGeneral += dt.Rows(i).Item("Total")
            TotalIVA += dt.Rows(i).Item("TotalIVA")
            TotalRetencion += dt.Rows(i).Item("Retencion")
            i += 1
            Linea += 1
        End While
        PonerLineaExcel(oApp, Linea)

        oApp.Cells(Linea, 2).Value = "TOTAL GENERAL:"
        oApp.Cells(Linea, 2).font.bold = True
        oApp.Cells(Linea, 4).Value = 0.0
        oApp.Cells(Linea, 4).font.bold = True
        oApp.Cells(Linea, 5).Value = TotExento
        oApp.Cells(Linea, 5).font.bold = True
        oApp.Cells(Linea, 6).Value = TotAfecto
        oApp.Cells(Linea, 6).font.bold = True
        oApp.Cells(Linea, 7).Value = TotExportacion
        oApp.Cells(Linea, 7).font.bold = True
        oApp.Cells(Linea, 8).Value = TotalRetencion
        oApp.Cells(Linea, 8).font.bold = True
        oApp.Cells(Linea, 9).Value = TotTerceros
        oApp.Cells(Linea, 9).font.bold = True
        oApp.Cells(Linea, 10).Value = TotGeneral
        oApp.Cells(Linea, 10).font.bold = True

        Linea += 2
        oApp.Cells(Linea, 2).Value = "RESUMEN DE OPERACIONES: "
        oApp.Cells(Linea, 2).font.bold = True
        Linea += 1

        oApp.Cells(Linea, 2).Value = "Ventas No Sujetas:"
        oApp.Cells(Linea, 2).font.size = 8
        oApp.Cells(Linea, 5).Value = 0.0
        Linea += 1

        oApp.Cells(Linea, 2).Value = "Ventas Exentas:"
        oApp.Cells(Linea, 2).font.size = 8
        oApp.Cells(Linea, 5).Value = TotExento
        Linea += 1

        oApp.Cells(Linea, 2).Value = "Ventas Afectas Netas:"
        oApp.Cells(Linea, 2).font.size = 8
        oApp.Cells(Linea, 5).Value = TotAfecto - TotalIVA 'Round(TotAfecto / (pnIVA + 1), 2)
        Linea += 1

        oApp.Cells(Linea, 2).Value = "IVA 13%:"
        oApp.Cells(Linea, 2).font.size = 8
        oApp.Cells(Linea, 5).Value = TotalIVA 'TotAfecto - Round(TotAfecto / (pnIVA + 1), 2)
        Linea += 1

        oApp.Cells(Linea, 2).Value = "Exportaciones:"
        oApp.Cells(Linea, 2).font.size = 8
        oApp.Cells(Linea, 5).Value = TotExportacion
        Linea += 1

        oApp.Cells(Linea, 2).Value = "Ventas a Terceros:"
        oApp.Cells(Linea, 2).font.size = 8
        oApp.Cells(Linea, 5).Value = TotTerceros
        Linea += 1

        oApp.Cells(Linea, 2).Value = "(-) Retención:"
        oApp.Cells(Linea, 2).font.size = 8
        oApp.Cells(Linea, 5).Value = TotalRetencion
        Linea += 1

        PonerLineaExcel3(oApp, Linea)

        oApp.Cells(Linea, 2).Value = "TOTAL DE VENTAS:"
        oApp.Cells(Linea, 2).font.bold = True
        oApp.Cells(Linea, 2).font.size = 8
        oApp.Cells(Linea, 5).Value = Total
        oApp.Cells(Linea, 5).font.bold = True

        PonerLineaExcel4(oApp, Linea)
        oApp.Cells(Linea, 6).Value = "                            NOMBRE Y FIRMA DEL CONTADOR"
        oApp.Cells(Linea, 6).font.bold = True
        oApp.Cells(Linea, 6).font.size = 8

        Range = "$A$1:$I$" + CStr(Linea)
        oApp.ActiveSheet.PageSetup.PrintArea = Range
        oApp.Range("A1").Select()

        'oApp.ActiveSheet.Protect("DrawingObjects:=False, Contents:=True, Scenarios:= False")
        'oApp.Save()
        teProgreso.Visible = False
        oApp.Visible = True
        System.Threading.Thread.CurrentThread.CurrentCulture = oldCI

    End Sub
    Private Sub PonerLineaExcel(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("A{0}:I{0}", CStr(Linea))
        oApp.Cells(6, 29).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
    Private Sub PonerLineaExcel2(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("B{0}:D{0}", CStr(Linea))
        oApp.Cells(6, 29).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
    Private Sub PonerLineaExcel3(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("B{0}:E{0}", CStr(Linea))
        oApp.Cells(6, 29).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
    Private Sub PonerLineaExcel4(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("G{0}:I{0}", CStr(Linea))
        oApp.Cells(6, 29).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
End Class
