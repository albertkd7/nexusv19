﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmVentasPorComprobante
    Dim bl As New FacturaBLL(g_ConnectionString)
    Private Sub fac_rptVentasPorComprobante_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CargaCombos()
        deDesde.EditValue = CDate(String.Format("1/{0}/{1}", Month(Today), Year(Today)))
        deHasta.EditValue = Today
    End Sub
    

    Private Sub CargaCombos()
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        objCombos.fac_PuntosVenta(lePuntoVenta, 1, "-- TODOS --")
        objCombos.fac_TiposComprobante(leTipoComprobante, "-- TODOS --")
    End Sub

    Private Sub fac_rptVentasPorComprobante_Reporte() Handles Me.Reporte
        Dim dt As DataTable = bl.fac_VentasComprobante(deDesde.EditValue, deHasta.EditValue, leTipoComprobante.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)

        Dim rpt As New fac_rptVentasComprobante() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, PUNTO DE VENTA: {1}", teTitulo.EditValue, lePuntoVenta.Text)
        If leTipoComprobante.EditValue = 7 Then
            rpt.xrlTitulo.Text = teTitulo.EditValue
        End If
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub

    Private Sub leTipoComprobante_EditValueChanged(sender As Object, e As EventArgs) Handles leTipoComprobante.EditValueChanged
        teTitulo.EditValue = "DETALLE DE DOCUMENTOS EMITIDOS"
        If leTipoComprobante.EditValue = 5 Then
            teTitulo.EditValue = "DETALLE DE CRÉDITOS FISCALES EMITIDOS"
        End If
        If leTipoComprobante.EditValue = 6 Then
            teTitulo.EditValue = "DETALLE DE FACTURAS DE CONSUMIDOR FINAL"
        End If
        If leTipoComprobante.EditValue = 7 Then
            teTitulo.EditValue = "DETALLE DE DOCUMENTOS DE EXPORTACIÓN"
        End If
        If leTipoComprobante.EditValue = 8 Then
            teTitulo.EditValue = "DETALLE DE NOTAS DE CRÉDITO EMITIDAS"
        End If
    End Sub
End Class
