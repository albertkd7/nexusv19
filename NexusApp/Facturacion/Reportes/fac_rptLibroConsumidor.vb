Public Class fac_rptLibroConsumidor
    Dim Total As Decimal
    Private Sub Detail_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Detail.BeforePrint
        Total += GetCurrentColumnValue("Afecto")
        'TotalIva += GetCurrentColumnValue("TotalIVA")
    End Sub

    Private Sub xrlResumen_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlResumen.BeforePrint
        Dim VentaNeta As Decimal = Decimal.Round(Total / (pnIVA + 1), 2)
        xrlVentaNeta.Text = Format(VentaNeta, "##,###,##0.00")
        xrlIva.Text = Format(Total - VentaNeta, "##,###,##0.00")
    End Sub
End Class