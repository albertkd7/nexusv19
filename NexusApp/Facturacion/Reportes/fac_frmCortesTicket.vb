﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports System.IO

Public Class fac_frmCortesTicket
    Dim bl As New FacturaBLL(g_ConnectionString), blAdmon As New AdmonBLL(g_ConnectionString)
    Dim DatosEncabezado As String = "", DatosPie As String = "", entidadTicket As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(10)

    Private Sub fac_frmCorteCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deCorteX.EditValue = Now
        deCorteZ.EditValue = Now
        deGranTotal.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue)

        bl.fac_ObtenerDatosEncabezadoPieTicket(DatosEncabezado, DatosPie, lePuntoVenta.EditValue)

    End Sub

    Private Sub fac_frmCorteCaja_Report_Click() Handles Me.Reporte
        Dim str As String = "", Ancho As Integer = 0, s As String = ""

        Dim dt As New DataTable
        If RadioGroup1.EditValue = 0 Then
            dt = bl.fac_CorteX(deCorteX.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)
            s = "CORTE X --> " & deCorteX.EditValue.ToString()
        End If
        If RadioGroup1.EditValue = 1 Then
            dt = bl.fac_CorteZ(deCorteZ.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)
            s = "CORTE Z --> " & deCorteZ.EditValue.ToString()
        End If
        If RadioGroup1.EditValue = 2 Then
            dt = bl.fac_CorteGranTotalZ(deGranTotal.DateTime.Month, deGranTotal.DateTime.Year, leSucursal.EditValue, lePuntoVenta.EditValue)
            s = "GRAN TOTAL Z --> " & deGranTotal.DateTime.Month & "/" & deGranTotal.DateTime.Year '& "    " & Format(Now, "HH:mm:SS")
        End If
        str = Chr(27) & Chr(99) & Chr(48) & Chr(3) 'para que imprima en la cinta de auditoría
        str += Chr(27) + "z" + Chr(1)  'para activar el modo de impresión de la cintadeCorteX
        str += DatosEncabezado & Chr(10) & Chr(10)
        str += s & Chr(10)
        For Each Fila As DataRow In dt.Rows
            s = Fila.Item("Concepto").ToString()
            str += s
            If Fila.Item("Imprimir") = 1 Then
                Ancho = 40 - Len(s)
                s = Format(Fila.Item("Total"), "$##,##0.00")
                str += s.PadLeft(Ancho, " ")
            End If
            str += Chr(10)
        Next
        str += Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10) & Chr(10)
        str += Chr(27) + "z" + Chr(0)  'para desactivar el modo de impresión en la cinta
        str += Chr(27) + Chr(105)
        RawPrinterHelper.SendStringToPrinter(entidadTicket.NombreImpresor, str)
    End Sub

    Private Sub RadioGroup1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioGroup1.SelectedIndexChanged
        lblCorteX.Visible = RadioGroup1.EditValue = 0
        deCorteX.Visible = RadioGroup1.EditValue = 0
        lblCorteZ.Visible = RadioGroup1.EditValue = 1
        deCorteZ.Visible = RadioGroup1.EditValue = 1
        lblCorteTotal.Visible = RadioGroup1.EditValue = 2
        deGranTotal.Visible = RadioGroup1.EditValue = 2
        If RadioGroup1.EditValue = 0 Then
            teTitulo.EditValue = "CORTE X"
        End If
        If RadioGroup1.EditValue = 1 Then
            teTitulo.EditValue = "CORTE Z"
        End If
        If RadioGroup1.EditValue = 2 Then
            teTitulo.EditValue = "CORTE GRAN TOTAL Z"
        End If
    End Sub

    Private Sub cmdImprimirCinta_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles cmdImprimirCinta.Click
        Dim str As String = ""
        str = Chr(27) & Chr(99) & Chr(48) & Chr(3)
        str += Chr(27) + "z" + Chr(1)  'para activar el modo de impresión de la cinta
        str += DatosEncabezado & Chr(10) & Chr(10)
        RawPrinterHelper.SendStringToPrinter(entidadTicket.NombreImpresor, str)
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue)
    End Sub

    Private Sub btCintaAuditoria_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btCintaAuditoria.Click
        fac_frmPideFechas.ShowDialog()
        If fac_frmPideFechas.deDesde.Text = "" Or fac_frmPideFechas.deHasta.Text = "" Then
            Exit Sub
        End If

        Dim Fec1 As Date, Fec2 As Date
        Fec1 = fac_frmPideFechas.deDesde.EditValue
        Fec2 = fac_frmPideFechas.deHasta.EditValue

        Dim dt As DataTable = bl.fac_ObtenerTicketsParaCinta(Fec1, Fec2, lePuntoVenta.EditValue)
        If dt.Rows.Count = 0 Then
            MsgBox("No se han emitido ticketes en el período selecionado", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim fbd As New FolderBrowserDialog

        fbd.ShowDialog()
        If fbd.SelectedPath = "" Then
            Return
        End If

        Dim archivo As String = String.Format("{0}\CINTA{1}{2}.txt", fbd.SelectedPath, Format(Fec1, "yyyyMMdd"), Format(Fec2, "yyyyMMdd"))

        Dim str As String, ancho As Integer
        Dim dtInfo As DataTable = bl.fac_ObtenerDatosEncabezadoPieTicket(lePuntoVenta.EditValue)

        Try
            Using arc As StreamWriter = New StreamWriter(archivo)
                'PONER ACÁ UNA BARRA DE ESTADO PARA VER QUE TICKETE SE ESTÁ GENERANDO, IGUAL QUE LAS BOLETAS EN PLANILLA

                For Each fila As DataRow In dt.Rows
                    For Each rowHeaderFoot As DataRow In dtInfo.Rows 'ACA GENERA EL ENCABEZADO
                        If rowHeaderFoot.Item("Tipo") = "E" Then
                            arc.WriteLine(rowHeaderFoot.Item("Linea").ToString())
                        End If
                    Next
                    str = "CONTROL No. " & fila.Item("IdComprobante")
                    ancho = 40 - Len(str)
                    str += ("TICKET No. " & fila.Item("Numero")).PadLeft(ancho, " ")
                    arc.WriteLine(str)
                    'LA FECHA Y HORA
                    str = "FECHA: " & Format(fila.Item("Fecha"), "dd/MM/yyyy")
                    ancho = 40 - Len(str)
                    str &= ("HORA: " & CDate(fila.Item("FechaHoraCreacion")).ToString("HH:mm:ss")).PadLeft(ancho, " ")
                    arc.WriteLine(str)

                    arc.WriteLine("CAJA No. " & fila.Item("IdPunto"))
                    arc.WriteLine("VENDEDOR: " & fila.Item("Vendedor"))
                    arc.WriteLine("")
                    arc.WriteLine("")
                    If fila.Item("EsDevolucion") Then
                        arc.WriteLine("    TICKET DE DEVOLUCION")
                        arc.WriteLine("")
                        arc.WriteLine("")
                    End If
                    Dim totalExento As Decimal = 0.0, totalAfecto As Decimal = 0.0, vta As String = "G"
                    Dim dtVta As DataTable = bl.fac_ObtenerDetalleDocumento("fac_VentasDetalle", fila.Item("IdComprobante"))
                    For Each det As DataRow In dtVta.Rows
                        totalExento += CDec(det.Item("VentaExenta"))
                        totalAfecto += CDec(det.Item("VentaAfecta"))
                        vta = "(G)"
                        If CDec(det.Item("VentaExenta")) > 0.0 Then
                            vta = "(E)"
                        End If
                        str = det.Item("Descripcion") & Space(40)
                        arc.WriteLine(str.Substring(0, 37) + vta)
                        str = Format(det.Item("Cantidad"), "#,##0.00") + " X " + Format(det.Item("PrecioUnitario"), "#,##0.00")
                        ancho = 40 - Len(str)
                        If CDec(det.Item("VentaExenta")) > 0.0 Then
                            str &= (Format(det.Item("VentaExenta"), "$##,##0.00")).PadLeft(ancho, " ")
                        Else
                            str &= (Format(det.Item("VentaAfecta"), "$##,##0.00")).PadLeft(ancho, " ")
                        End If
                        arc.WriteLine(str)
                    Next
                    arc.WriteLine("")
                    str = "Sub-Total Exento:" & (Format(totalExento, "$##,##0.00")).PadLeft(23, " ")
                    arc.WriteLine(str)

                    str = "Sub-Total No Sujeto:" & (Format(0, "$##,##0.00")).PadLeft(20, " ")
                    arc.WriteLine(str)

                    str = "Sub-Total Gravado:" & (Format(totalAfecto, "$##,##0.00")).PadLeft(22, " ")
                    arc.WriteLine(str)

                    str = "TOTAL:" & (Format(totalExento + totalAfecto, "$##,##0.00")).PadLeft(34, " ")
                    arc.WriteLine(str)

                    str = "EFECTIVO RECIBIDO:" & (Format(fila.Item("TotalPagado"), "$##,##0.00")).PadLeft(22, " ")
                    arc.WriteLine(str)

                    str = "VUELTO A ENTREGAR:" & (Format(fila.Item("TotalPagado") - fila.Item("TotalExento") - fila.Item("TotalAfecto"), "$##,##0.00")).PadLeft(22, " ")
                    arc.WriteLine(str)
                    arc.WriteLine("")
                    arc.WriteLine("G= VENTA GRAVADA, E= VENTA EXENTA")
                    arc.WriteLine("N= VENTA NO SUJETA")
                    If fila.Item("TotalComprobante") > 200 Or fila.Item("EsDevolucion") Then 'SI LA VENTA ES MAYOR A $200.00 O ES UNA DEVOLUCION
                        arc.WriteLine("")
                        If fila.Item("EsDevolucion") Then
                            arc.WriteLine("TICKETE DE DEVOLUCION. FAVOR FIRMAR")
                        Else
                            arc.WriteLine("VENTA MAYOR O IGUAL A $200. FAVOR FIRMAR")
                        End If
                        arc.WriteLine("NIT: " & fila.Item("Nit"))
                        arc.WriteLine("DUI: " & fila.Item("Nrc"))
                        arc.WriteLine("Nombre: " & fila.Item("Nombre"))
                        arc.WriteLine("")
                        arc.WriteLine("Firma: _____________________________")
                    End If
                    arc.WriteLine("")
                    arc.WriteLine("")

                    For Each rowHeaderFoot As DataRow In dtInfo.Rows 'ACA GENERA EL PIE DE CADA TICKETE
                        If rowHeaderFoot.Item("Tipo") = "P" Then
                            arc.WriteLine(rowHeaderFoot.Item("Linea").ToString())
                        End If
                    Next

                    For i = 0 To 10
                        arc.WriteLine("")
                    Next
                Next
            End Using
            Process.Start(archivo)
        Catch ex As Exception
            MsgBox("No fue posible generar la cinta" & Chr(13) & "ERROR GENERADO: " & Chr(13) & ex.ToString, MsgBoxStyle.Critical, "Error")

        End Try
    End Sub

    Private Sub lePuntoVenta_EditValueChanged(sender As Object, e As EventArgs) Handles lePuntoVenta.EditValueChanged
        bl.fac_ObtenerDatosEncabezadoPieTicket(DatosEncabezado, DatosPie, lePuntoVenta.EditValue)
    End Sub
End Class
