﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class fac_rptUnico
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand
        Me.xrlComentario = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCantLetras = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlIvaRetenido = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDiasCredito = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlIva = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTotal = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaNoSujeta = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVendedor = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaExenta = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaAfecta = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlSubTotal = New DevExpress.XtraReports.UI.XRLabel
        Me.XrSubreport1 = New DevExpress.XtraReports.UI.XRSubreport
        Me.xrlGiro = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTelefono = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaACuenta = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFormaPago = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDocAfectados = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlPedido = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCodigo = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDepto = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNrc = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNombre = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNumero = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFecha = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlMunic = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNit = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDireccion = New DevExpress.XtraReports.UI.XRLabel
        Me.Fac_rptDetalleUnico1 = New Nexus.fac_rptDetalleUnico
        Me.xrlDireccion2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNit2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlMunic2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFecha2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNumero2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNombre2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNrc2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDepto2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCodigo2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlPedido2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDocAfectados2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFormaPago2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaACuenta2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTelefono2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlGiro2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrSubreport2 = New DevExpress.XtraReports.UI.XRSubreport
        Me.Fac_rptDetalleUnico2 = New Nexus.fac_rptDetalleUnico
        Me.xrlSubTotal2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaAfecta2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaExenta2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVendedor2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaNoSujeta2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTotal2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlIva2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDiasCredito2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlIvaRetenido2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCantLetras2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlComentario2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDireccion3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNit3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlMunic3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFecha3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNumero3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNombre3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNrc3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDepto3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCodigo3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlPedido3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDocAfectados3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFormaPago3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaACuenta3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTelefono3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlGiro3 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrSubreport3 = New DevExpress.XtraReports.UI.XRSubreport
        Me.Fac_rptDetalleUnico3 = New Nexus.fac_rptDetalleUnico
        Me.xrlSubTotal3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaAfecta3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaExenta3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVendedor3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlVentaNoSujeta3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTotal3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlIva3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDiasCredito3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlIvaRetenido3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCantLetras3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlComentario3 = New DevExpress.XtraReports.UI.XRLabel
        CType(Me.Fac_rptDetalleUnico1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Fac_rptDetalleUnico2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Fac_rptDetalleUnico3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.HeightF = 26.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        Me.BottomMarginBand1.SnapLinePadding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        '
        'Detail
        '
        Me.Detail.HeightF = 18.70824!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.HeightF = 28.29167!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlVentaAfecta3, Me.xrlVentaExenta3, Me.xrlVendedor3, Me.xrlGiro3, Me.XrSubreport3, Me.xrlSubTotal3, Me.xrlVentaNoSujeta3, Me.xrlIvaRetenido3, Me.xrlCantLetras3, Me.xrlComentario3, Me.xrlTotal3, Me.xrlIva3, Me.xrlDiasCredito3, Me.xrlTelefono3, Me.xrlFecha3, Me.xrlNumero3, Me.xrlNombre3, Me.xrlDireccion3, Me.xrlNit3, Me.xrlMunic3, Me.xrlNrc3, Me.xrlDocAfectados3, Me.xrlFormaPago3, Me.xrlVentaACuenta3, Me.xrlDepto3, Me.xrlCodigo3, Me.xrlPedido3, Me.xrlVentaAfecta2, Me.xrlVentaExenta2, Me.xrlVendedor2, Me.xrlGiro2, Me.XrSubreport2, Me.xrlSubTotal2, Me.xrlVentaNoSujeta2, Me.xrlIvaRetenido2, Me.xrlCantLetras2, Me.xrlComentario2, Me.xrlTotal2, Me.xrlIva2, Me.xrlDiasCredito2, Me.xrlTelefono2, Me.xrlFecha2, Me.xrlNumero2, Me.xrlNombre2, Me.xrlDireccion2, Me.xrlNit2, Me.xrlMunic2, Me.xrlNrc2, Me.xrlDocAfectados2, Me.xrlFormaPago2, Me.xrlVentaACuenta2, Me.xrlDepto2, Me.xrlCodigo2, Me.xrlPedido2, Me.xrlComentario, Me.xrlCantLetras, Me.xrlIvaRetenido, Me.xrlDiasCredito, Me.xrlIva, Me.xrlTotal, Me.xrlVentaNoSujeta, Me.xrlVendedor, Me.xrlVentaExenta, Me.xrlVentaAfecta, Me.xrlSubTotal, Me.XrSubreport1, Me.xrlGiro, Me.xrlTelefono, Me.xrlVentaACuenta, Me.xrlFormaPago, Me.xrlDocAfectados, Me.xrlPedido, Me.xrlCodigo, Me.xrlDepto, Me.xrlNrc, Me.xrlNombre, Me.xrlNumero, Me.xrlFecha, Me.xrlMunic, Me.xrlNit, Me.xrlDireccion})
        Me.ReportHeader.HeightF = 1072.917!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'xrlComentario
        '
        Me.xrlComentario.CanShrink = True
        Me.xrlComentario.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlComentario.LocationFloat = New DevExpress.Utils.PointFloat(10.62469!, 235.2292!)
        Me.xrlComentario.Name = "xrlComentario"
        Me.xrlComentario.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlComentario.SizeF = New System.Drawing.SizeF(359.2084!, 17.00001!)
        Me.xrlComentario.StylePriority.UseFont = False
        Me.xrlComentario.StylePriority.UseTextAlignment = False
        Me.xrlComentario.Text = "Comentario"
        Me.xrlComentario.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlComentario.Visible = False
        '
        'xrlCantLetras
        '
        Me.xrlCantLetras.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlCantLetras.LocationFloat = New DevExpress.Utils.PointFloat(10.62469!, 194.2292!)
        Me.xrlCantLetras.Name = "xrlCantLetras"
        Me.xrlCantLetras.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCantLetras.SizeF = New System.Drawing.SizeF(515.3745!, 17.00001!)
        Me.xrlCantLetras.StylePriority.UseFont = False
        Me.xrlCantLetras.StylePriority.UseTextAlignment = False
        Me.xrlCantLetras.Text = "DIEZ"
        Me.xrlCantLetras.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlIvaRetenido
        '
        Me.xrlIvaRetenido.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIvaRetenido.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlIvaRetenido.LocationFloat = New DevExpress.Utils.PointFloat(652.083!, 255.6042!)
        Me.xrlIvaRetenido.Name = "xrlIvaRetenido"
        Me.xrlIvaRetenido.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIvaRetenido.SizeF = New System.Drawing.SizeF(81.99997!, 15.99998!)
        Me.xrlIvaRetenido.StylePriority.UseBorders = False
        Me.xrlIvaRetenido.StylePriority.UseFont = False
        Me.xrlIvaRetenido.StylePriority.UseTextAlignment = False
        Me.xrlIvaRetenido.Text = "0.00"
        Me.xrlIvaRetenido.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDiasCredito
        '
        Me.xrlDiasCredito.CanGrow = False
        Me.xrlDiasCredito.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDiasCredito.LocationFloat = New DevExpress.Utils.PointFloat(372.6673!, 10.85412!)
        Me.xrlDiasCredito.Name = "xrlDiasCredito"
        Me.xrlDiasCredito.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDiasCredito.SizeF = New System.Drawing.SizeF(94.04163!, 17.0!)
        Me.xrlDiasCredito.StylePriority.UseFont = False
        Me.xrlDiasCredito.StylePriority.UseTextAlignment = False
        Me.xrlDiasCredito.Text = "xrlDiasCredito"
        Me.xrlDiasCredito.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlDiasCredito.Visible = False
        '
        'xrlIva
        '
        Me.xrlIva.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIva.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlIva.LocationFloat = New DevExpress.Utils.PointFloat(652.0831!, 217.6042!)
        Me.xrlIva.Name = "xrlIva"
        Me.xrlIva.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIva.SizeF = New System.Drawing.SizeF(81.99997!, 15.99998!)
        Me.xrlIva.StylePriority.UseBorders = False
        Me.xrlIva.StylePriority.UseFont = False
        Me.xrlIva.StylePriority.UseTextAlignment = False
        Me.xrlIva.Text = "0.00"
        Me.xrlIva.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal
        '
        Me.xrlTotal.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlTotal.LocationFloat = New DevExpress.Utils.PointFloat(652.0831!, 314.2292!)
        Me.xrlTotal.Name = "xrlTotal"
        Me.xrlTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal.SizeF = New System.Drawing.SizeF(81.99997!, 15.99998!)
        Me.xrlTotal.StylePriority.UseBorders = False
        Me.xrlTotal.StylePriority.UseFont = False
        Me.xrlTotal.StylePriority.UseTextAlignment = False
        Me.xrlTotal.Text = "0.00"
        Me.xrlTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaNoSujeta
        '
        Me.xrlVentaNoSujeta.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlVentaNoSujeta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaNoSujeta.LocationFloat = New DevExpress.Utils.PointFloat(652.0831!, 276.2292!)
        Me.xrlVentaNoSujeta.Name = "xrlVentaNoSujeta"
        Me.xrlVentaNoSujeta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaNoSujeta.SizeF = New System.Drawing.SizeF(81.99997!, 15.99998!)
        Me.xrlVentaNoSujeta.StylePriority.UseBorders = False
        Me.xrlVentaNoSujeta.StylePriority.UseFont = False
        Me.xrlVentaNoSujeta.StylePriority.UseTextAlignment = False
        Me.xrlVentaNoSujeta.Text = "0.00"
        Me.xrlVentaNoSujeta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVendedor
        '
        Me.xrlVendedor.CanGrow = False
        Me.xrlVendedor.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlVendedor.LocationFloat = New DevExpress.Utils.PointFloat(10.62469!, 253.2292!)
        Me.xrlVendedor.Name = "xrlVendedor"
        Me.xrlVendedor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVendedor.SizeF = New System.Drawing.SizeF(204.4583!, 17.00001!)
        Me.xrlVendedor.StylePriority.UseFont = False
        Me.xrlVendedor.StylePriority.UseTextAlignment = False
        Me.xrlVendedor.Text = "VENDEDOR"
        Me.xrlVendedor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlVendedor.Visible = False
        '
        'xrlVentaExenta
        '
        Me.xrlVentaExenta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaExenta.LocationFloat = New DevExpress.Utils.PointFloat(652.0831!, 295.2292!)
        Me.xrlVentaExenta.Name = "xrlVentaExenta"
        Me.xrlVentaExenta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta.SizeF = New System.Drawing.SizeF(81.99997!, 16.0!)
        Me.xrlVentaExenta.StylePriority.UseFont = False
        Me.xrlVentaExenta.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta.Text = "0.00"
        Me.xrlVentaExenta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaAfecta
        '
        Me.xrlVentaAfecta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaAfecta.LocationFloat = New DevExpress.Utils.PointFloat(652.0831!, 194.3542!)
        Me.xrlVentaAfecta.Name = "xrlVentaAfecta"
        Me.xrlVentaAfecta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaAfecta.SizeF = New System.Drawing.SizeF(81.99997!, 16.875!)
        Me.xrlVentaAfecta.StylePriority.UseFont = False
        Me.xrlVentaAfecta.StylePriority.UseTextAlignment = False
        Me.xrlVentaAfecta.Text = "0.00"
        Me.xrlVentaAfecta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlSubTotal
        '
        Me.xrlSubTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlSubTotal.LocationFloat = New DevExpress.Utils.PointFloat(652.0831!, 236.6041!)
        Me.xrlSubTotal.Name = "xrlSubTotal"
        Me.xrlSubTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSubTotal.SizeF = New System.Drawing.SizeF(81.99997!, 16.0!)
        Me.xrlSubTotal.StylePriority.UseFont = False
        Me.xrlSubTotal.StylePriority.UseTextAlignment = False
        Me.xrlSubTotal.Text = "0.00"
        Me.xrlSubTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrSubreport1
        '
        Me.XrSubreport1.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 157.25!)
        Me.XrSubreport1.Name = "XrSubreport1"
        Me.XrSubreport1.ReportSource = Me.Fac_rptDetalleUnico1
        Me.XrSubreport1.SizeF = New System.Drawing.SizeF(756.25!, 22.99998!)
        '
        'xrlGiro
        '
        Me.xrlGiro.CanGrow = False
        Me.xrlGiro.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlGiro.LocationFloat = New DevExpress.Utils.PointFloat(535.9583!, 92.85408!)
        Me.xrlGiro.Name = "xrlGiro"
        Me.xrlGiro.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlGiro.SizeF = New System.Drawing.SizeF(181.1662!, 15.00006!)
        Me.xrlGiro.StylePriority.UseFont = False
        Me.xrlGiro.StylePriority.UseTextAlignment = False
        Me.xrlGiro.Text = "GIRO"
        Me.xrlGiro.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTelefono
        '
        Me.xrlTelefono.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlTelefono.LocationFloat = New DevExpress.Utils.PointFloat(5.708464!, 10.85412!)
        Me.xrlTelefono.Name = "xrlTelefono"
        Me.xrlTelefono.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTelefono.SizeF = New System.Drawing.SizeF(112.0417!, 16.0!)
        Me.xrlTelefono.StylePriority.UseFont = False
        Me.xrlTelefono.StylePriority.UseTextAlignment = False
        Me.xrlTelefono.Text = "TELEFONO"
        Me.xrlTelefono.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlTelefono.Visible = False
        '
        'xrlVentaACuenta
        '
        Me.xrlVentaACuenta.CanGrow = False
        Me.xrlVentaACuenta.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlVentaACuenta.LocationFloat = New DevExpress.Utils.PointFloat(558.3719!, 29.85413!)
        Me.xrlVentaACuenta.Name = "xrlVentaACuenta"
        Me.xrlVentaACuenta.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaACuenta.SizeF = New System.Drawing.SizeF(175.7112!, 16.00001!)
        Me.xrlVentaACuenta.StylePriority.UseFont = False
        Me.xrlVentaACuenta.StylePriority.UseTextAlignment = False
        Me.xrlVentaACuenta.Text = "VENTA A CUENTA DE"
        Me.xrlVentaACuenta.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlVentaACuenta.Visible = False
        '
        'xrlFormaPago
        '
        Me.xrlFormaPago.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlFormaPago.LocationFloat = New DevExpress.Utils.PointFloat(604.9583!, 113.8542!)
        Me.xrlFormaPago.Name = "xrlFormaPago"
        Me.xrlFormaPago.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFormaPago.SizeF = New System.Drawing.SizeF(145.0833!, 14.99999!)
        Me.xrlFormaPago.StylePriority.UseFont = False
        Me.xrlFormaPago.StylePriority.UseTextAlignment = False
        Me.xrlFormaPago.Text = "CONTADO"
        Me.xrlFormaPago.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDocAfectados
        '
        Me.xrlDocAfectados.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDocAfectados.LocationFloat = New DevExpress.Utils.PointFloat(260.6256!, 10.85411!)
        Me.xrlDocAfectados.Name = "xrlDocAfectados"
        Me.xrlDocAfectados.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDocAfectados.SizeF = New System.Drawing.SizeF(112.0417!, 16.0!)
        Me.xrlDocAfectados.StylePriority.UseFont = False
        Me.xrlDocAfectados.StylePriority.UseTextAlignment = False
        Me.xrlDocAfectados.Text = "No. Afectados"
        Me.xrlDocAfectados.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlPedido
        '
        Me.xrlPedido.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlPedido.LocationFloat = New DevExpress.Utils.PointFloat(125.2879!, 10.85411!)
        Me.xrlPedido.Name = "xrlPedido"
        Me.xrlPedido.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPedido.SizeF = New System.Drawing.SizeF(112.0417!, 16.0!)
        Me.xrlPedido.StylePriority.UseFont = False
        Me.xrlPedido.StylePriority.UseTextAlignment = False
        Me.xrlPedido.Text = "123456"
        Me.xrlPedido.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCodigo
        '
        Me.xrlCodigo.CanGrow = False
        Me.xrlCodigo.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlCodigo.LocationFloat = New DevExpress.Utils.PointFloat(558.3719!, 13.85412!)
        Me.xrlCodigo.Name = "xrlCodigo"
        Me.xrlCodigo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCodigo.SizeF = New System.Drawing.SizeF(124.6696!, 16.00001!)
        Me.xrlCodigo.StylePriority.UseFont = False
        Me.xrlCodigo.StylePriority.UseTextAlignment = False
        Me.xrlCodigo.Text = "CODIGO: 001"
        Me.xrlCodigo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlCodigo.Visible = False
        '
        'xrlDepto
        '
        Me.xrlDepto.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDepto.LocationFloat = New DevExpress.Utils.PointFloat(95.99972!, 113.8541!)
        Me.xrlDepto.Name = "xrlDepto"
        Me.xrlDepto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDepto.SizeF = New System.Drawing.SizeF(192.3716!, 15.00002!)
        Me.xrlDepto.StylePriority.UseFont = False
        Me.xrlDepto.StylePriority.UseTextAlignment = False
        Me.xrlDepto.Text = "SAN SALVADOR"
        Me.xrlDepto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNrc
        '
        Me.xrlNrc.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNrc.LocationFloat = New DevExpress.Utils.PointFloat(588.9583!, 72.10416!)
        Me.xrlNrc.Name = "xrlNrc"
        Me.xrlNrc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNrc.SizeF = New System.Drawing.SizeF(127.0833!, 14.99999!)
        Me.xrlNrc.StylePriority.UseFont = False
        Me.xrlNrc.StylePriority.UseTextAlignment = False
        Me.xrlNrc.Text = "194534-3"
        Me.xrlNrc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNombre
        '
        Me.xrlNombre.CanGrow = False
        Me.xrlNombre.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNombre.LocationFloat = New DevExpress.Utils.PointFloat(57.70845!, 53.10411!)
        Me.xrlNombre.Name = "xrlNombre"
        Me.xrlNombre.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNombre.SizeF = New System.Drawing.SizeF(314.9588!, 16.0!)
        Me.xrlNombre.StylePriority.UseFont = False
        Me.xrlNombre.StylePriority.UseTextAlignment = False
        Me.xrlNombre.Text = "IT OUTSOURCING, S.A. DE C.V."
        Me.xrlNombre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNumero
        '
        Me.xrlNumero.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNumero.LocationFloat = New DevExpress.Utils.PointFloat(480.2084!, 15.10413!)
        Me.xrlNumero.Name = "xrlNumero"
        Me.xrlNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlNumero.StylePriority.UseFont = False
        Me.xrlNumero.StylePriority.UseTextAlignment = False
        Me.xrlNumero.Text = "1945"
        Me.xrlNumero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFecha
        '
        Me.xrlFecha.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlFecha.LocationFloat = New DevExpress.Utils.PointFloat(559.9583!, 54.1041!)
        Me.xrlFecha.Name = "xrlFecha"
        Me.xrlFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha.SizeF = New System.Drawing.SizeF(89.50046!, 15.0!)
        Me.xrlFecha.StylePriority.UseFont = False
        Me.xrlFecha.StylePriority.UseTextAlignment = False
        Me.xrlFecha.Text = "25/12/2012"
        Me.xrlFecha.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlMunic
        '
        Me.xrlMunic.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlMunic.LocationFloat = New DevExpress.Utils.PointFloat(70.99972!, 92.85414!)
        Me.xrlMunic.Name = "xrlMunic"
        Me.xrlMunic.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlMunic.SizeF = New System.Drawing.SizeF(166.3299!, 15.00003!)
        Me.xrlMunic.StylePriority.UseFont = False
        Me.xrlMunic.StylePriority.UseTextAlignment = False
        Me.xrlMunic.Text = "SAN SALVADOR"
        Me.xrlMunic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNit
        '
        Me.xrlNit.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNit.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNit.LocationFloat = New DevExpress.Utils.PointFloat(535.9583!, 135.8542!)
        Me.xrlNit.Name = "xrlNit"
        Me.xrlNit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNit.SizeF = New System.Drawing.SizeF(136.0!, 16.0!)
        Me.xrlNit.StylePriority.UseBorders = False
        Me.xrlNit.StylePriority.UseFont = False
        Me.xrlNit.StylePriority.UseTextAlignment = False
        Me.xrlNit.Text = "0614-251107-107-7"
        Me.xrlNit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDireccion
        '
        Me.xrlDireccion.CanGrow = False
        Me.xrlDireccion.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDireccion.LocationFloat = New DevExpress.Utils.PointFloat(63.99972!, 72.1041!)
        Me.xrlDireccion.Name = "xrlDireccion"
        Me.xrlDireccion.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDireccion.SizeF = New System.Drawing.SizeF(428.0828!, 15.00006!)
        Me.xrlDireccion.StylePriority.UseFont = False
        Me.xrlDireccion.StylePriority.UseTextAlignment = False
        Me.xrlDireccion.Text = "RESIDENCIAL CLAUDIA PASAJE AIDA No.19"
        Me.xrlDireccion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDireccion2
        '
        Me.xrlDireccion2.CanGrow = False
        Me.xrlDireccion2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDireccion2.LocationFloat = New DevExpress.Utils.PointFloat(68.91595!, 431.25!)
        Me.xrlDireccion2.Name = "xrlDireccion2"
        Me.xrlDireccion2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDireccion2.SizeF = New System.Drawing.SizeF(428.0828!, 15.00006!)
        Me.xrlDireccion2.StylePriority.UseFont = False
        Me.xrlDireccion2.StylePriority.UseTextAlignment = False
        Me.xrlDireccion2.Text = "RESIDENCIAL CLAUDIA PASAJE AIDA No.19"
        Me.xrlDireccion2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNit2
        '
        Me.xrlNit2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNit2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNit2.LocationFloat = New DevExpress.Utils.PointFloat(540.8746!, 495.0!)
        Me.xrlNit2.Name = "xrlNit2"
        Me.xrlNit2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNit2.SizeF = New System.Drawing.SizeF(136.0!, 16.0!)
        Me.xrlNit2.StylePriority.UseBorders = False
        Me.xrlNit2.StylePriority.UseFont = False
        Me.xrlNit2.StylePriority.UseTextAlignment = False
        Me.xrlNit2.Text = "0614-251107-107-7"
        Me.xrlNit2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlMunic2
        '
        Me.xrlMunic2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlMunic2.LocationFloat = New DevExpress.Utils.PointFloat(75.91594!, 452.0!)
        Me.xrlMunic2.Name = "xrlMunic2"
        Me.xrlMunic2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlMunic2.SizeF = New System.Drawing.SizeF(166.3299!, 15.00003!)
        Me.xrlMunic2.StylePriority.UseFont = False
        Me.xrlMunic2.StylePriority.UseTextAlignment = False
        Me.xrlMunic2.Text = "SAN SALVADOR"
        Me.xrlMunic2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFecha2
        '
        Me.xrlFecha2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlFecha2.LocationFloat = New DevExpress.Utils.PointFloat(564.8745!, 413.25!)
        Me.xrlFecha2.Name = "xrlFecha2"
        Me.xrlFecha2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha2.SizeF = New System.Drawing.SizeF(89.50046!, 15.0!)
        Me.xrlFecha2.StylePriority.UseFont = False
        Me.xrlFecha2.StylePriority.UseTextAlignment = False
        Me.xrlFecha2.Text = "25/12/2012"
        Me.xrlFecha2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNumero2
        '
        Me.xrlNumero2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNumero2.LocationFloat = New DevExpress.Utils.PointFloat(485.1246!, 374.25!)
        Me.xrlNumero2.Name = "xrlNumero2"
        Me.xrlNumero2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero2.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlNumero2.StylePriority.UseFont = False
        Me.xrlNumero2.StylePriority.UseTextAlignment = False
        Me.xrlNumero2.Text = "1945"
        Me.xrlNumero2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNombre2
        '
        Me.xrlNombre2.CanGrow = False
        Me.xrlNombre2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNombre2.LocationFloat = New DevExpress.Utils.PointFloat(62.62466!, 412.2499!)
        Me.xrlNombre2.Name = "xrlNombre2"
        Me.xrlNombre2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNombre2.SizeF = New System.Drawing.SizeF(314.9588!, 16.0!)
        Me.xrlNombre2.StylePriority.UseFont = False
        Me.xrlNombre2.StylePriority.UseTextAlignment = False
        Me.xrlNombre2.Text = "IT OUTSOURCING, S.A. DE C.V."
        Me.xrlNombre2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNrc2
        '
        Me.xrlNrc2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNrc2.LocationFloat = New DevExpress.Utils.PointFloat(593.8745!, 431.25!)
        Me.xrlNrc2.Name = "xrlNrc2"
        Me.xrlNrc2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNrc2.SizeF = New System.Drawing.SizeF(127.0833!, 14.99999!)
        Me.xrlNrc2.StylePriority.UseFont = False
        Me.xrlNrc2.StylePriority.UseTextAlignment = False
        Me.xrlNrc2.Text = "194534-3"
        Me.xrlNrc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDepto2
        '
        Me.xrlDepto2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDepto2.LocationFloat = New DevExpress.Utils.PointFloat(100.9159!, 473.0!)
        Me.xrlDepto2.Name = "xrlDepto2"
        Me.xrlDepto2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDepto2.SizeF = New System.Drawing.SizeF(192.3716!, 15.00002!)
        Me.xrlDepto2.StylePriority.UseFont = False
        Me.xrlDepto2.StylePriority.UseTextAlignment = False
        Me.xrlDepto2.Text = "SAN SALVADOR"
        Me.xrlDepto2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCodigo2
        '
        Me.xrlCodigo2.CanGrow = False
        Me.xrlCodigo2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlCodigo2.LocationFloat = New DevExpress.Utils.PointFloat(563.2881!, 372.9999!)
        Me.xrlCodigo2.Name = "xrlCodigo2"
        Me.xrlCodigo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCodigo2.SizeF = New System.Drawing.SizeF(124.6696!, 16.00001!)
        Me.xrlCodigo2.StylePriority.UseFont = False
        Me.xrlCodigo2.StylePriority.UseTextAlignment = False
        Me.xrlCodigo2.Text = "CODIGO: 001"
        Me.xrlCodigo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlCodigo2.Visible = False
        '
        'xrlPedido2
        '
        Me.xrlPedido2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlPedido2.LocationFloat = New DevExpress.Utils.PointFloat(130.2041!, 369.9999!)
        Me.xrlPedido2.Name = "xrlPedido2"
        Me.xrlPedido2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPedido2.SizeF = New System.Drawing.SizeF(112.0417!, 16.0!)
        Me.xrlPedido2.StylePriority.UseFont = False
        Me.xrlPedido2.StylePriority.UseTextAlignment = False
        Me.xrlPedido2.Text = "123456"
        Me.xrlPedido2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDocAfectados2
        '
        Me.xrlDocAfectados2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDocAfectados2.LocationFloat = New DevExpress.Utils.PointFloat(265.5418!, 369.9999!)
        Me.xrlDocAfectados2.Name = "xrlDocAfectados2"
        Me.xrlDocAfectados2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDocAfectados2.SizeF = New System.Drawing.SizeF(112.0417!, 16.0!)
        Me.xrlDocAfectados2.StylePriority.UseFont = False
        Me.xrlDocAfectados2.StylePriority.UseTextAlignment = False
        Me.xrlDocAfectados2.Text = "No. Afectados"
        Me.xrlDocAfectados2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFormaPago2
        '
        Me.xrlFormaPago2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlFormaPago2.LocationFloat = New DevExpress.Utils.PointFloat(609.8745!, 473.0001!)
        Me.xrlFormaPago2.Name = "xrlFormaPago2"
        Me.xrlFormaPago2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFormaPago2.SizeF = New System.Drawing.SizeF(145.0833!, 14.99999!)
        Me.xrlFormaPago2.StylePriority.UseFont = False
        Me.xrlFormaPago2.StylePriority.UseTextAlignment = False
        Me.xrlFormaPago2.Text = "CONTADO"
        Me.xrlFormaPago2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaACuenta2
        '
        Me.xrlVentaACuenta2.CanGrow = False
        Me.xrlVentaACuenta2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlVentaACuenta2.LocationFloat = New DevExpress.Utils.PointFloat(563.2881!, 389.0!)
        Me.xrlVentaACuenta2.Name = "xrlVentaACuenta2"
        Me.xrlVentaACuenta2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaACuenta2.SizeF = New System.Drawing.SizeF(175.7112!, 16.00001!)
        Me.xrlVentaACuenta2.StylePriority.UseFont = False
        Me.xrlVentaACuenta2.StylePriority.UseTextAlignment = False
        Me.xrlVentaACuenta2.Text = "VENTA A CUENTA DE"
        Me.xrlVentaACuenta2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlVentaACuenta2.Visible = False
        '
        'xrlTelefono2
        '
        Me.xrlTelefono2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlTelefono2.LocationFloat = New DevExpress.Utils.PointFloat(10.62469!, 369.9999!)
        Me.xrlTelefono2.Name = "xrlTelefono2"
        Me.xrlTelefono2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTelefono2.SizeF = New System.Drawing.SizeF(112.0417!, 16.0!)
        Me.xrlTelefono2.StylePriority.UseFont = False
        Me.xrlTelefono2.StylePriority.UseTextAlignment = False
        Me.xrlTelefono2.Text = "TELEFONO"
        Me.xrlTelefono2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlTelefono2.Visible = False
        '
        'xrlGiro2
        '
        Me.xrlGiro2.CanGrow = False
        Me.xrlGiro2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlGiro2.LocationFloat = New DevExpress.Utils.PointFloat(540.8746!, 451.9999!)
        Me.xrlGiro2.Name = "xrlGiro2"
        Me.xrlGiro2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlGiro2.SizeF = New System.Drawing.SizeF(181.1662!, 15.00006!)
        Me.xrlGiro2.StylePriority.UseFont = False
        Me.xrlGiro2.StylePriority.UseTextAlignment = False
        Me.xrlGiro2.Text = "GIRO"
        Me.xrlGiro2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrSubreport2
        '
        Me.XrSubreport2.LocationFloat = New DevExpress.Utils.PointFloat(14.91622!, 516.3958!)
        Me.XrSubreport2.Name = "XrSubreport2"
        Me.XrSubreport2.ReportSource = Me.Fac_rptDetalleUnico2
        Me.XrSubreport2.SizeF = New System.Drawing.SizeF(756.25!, 22.99998!)
        '
        'xrlSubTotal2
        '
        Me.xrlSubTotal2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlSubTotal2.LocationFloat = New DevExpress.Utils.PointFloat(656.9993!, 595.75!)
        Me.xrlSubTotal2.Name = "xrlSubTotal2"
        Me.xrlSubTotal2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSubTotal2.SizeF = New System.Drawing.SizeF(81.99997!, 16.0!)
        Me.xrlSubTotal2.StylePriority.UseFont = False
        Me.xrlSubTotal2.StylePriority.UseTextAlignment = False
        Me.xrlSubTotal2.Text = "0.00"
        Me.xrlSubTotal2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaAfecta2
        '
        Me.xrlVentaAfecta2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaAfecta2.LocationFloat = New DevExpress.Utils.PointFloat(656.9993!, 553.5001!)
        Me.xrlVentaAfecta2.Name = "xrlVentaAfecta2"
        Me.xrlVentaAfecta2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaAfecta2.SizeF = New System.Drawing.SizeF(81.99997!, 16.875!)
        Me.xrlVentaAfecta2.StylePriority.UseFont = False
        Me.xrlVentaAfecta2.StylePriority.UseTextAlignment = False
        Me.xrlVentaAfecta2.Text = "0.00"
        Me.xrlVentaAfecta2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta2
        '
        Me.xrlVentaExenta2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaExenta2.LocationFloat = New DevExpress.Utils.PointFloat(656.9993!, 654.3751!)
        Me.xrlVentaExenta2.Name = "xrlVentaExenta2"
        Me.xrlVentaExenta2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta2.SizeF = New System.Drawing.SizeF(81.99997!, 16.0!)
        Me.xrlVentaExenta2.StylePriority.UseFont = False
        Me.xrlVentaExenta2.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta2.Text = "0.00"
        Me.xrlVentaExenta2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVendedor2
        '
        Me.xrlVendedor2.CanGrow = False
        Me.xrlVendedor2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlVendedor2.LocationFloat = New DevExpress.Utils.PointFloat(15.54091!, 612.3751!)
        Me.xrlVendedor2.Name = "xrlVendedor2"
        Me.xrlVendedor2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVendedor2.SizeF = New System.Drawing.SizeF(204.4583!, 17.00001!)
        Me.xrlVendedor2.StylePriority.UseFont = False
        Me.xrlVendedor2.StylePriority.UseTextAlignment = False
        Me.xrlVendedor2.Text = "VENDEDOR"
        Me.xrlVendedor2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlVendedor2.Visible = False
        '
        'xrlVentaNoSujeta2
        '
        Me.xrlVentaNoSujeta2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlVentaNoSujeta2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaNoSujeta2.LocationFloat = New DevExpress.Utils.PointFloat(656.9993!, 635.3751!)
        Me.xrlVentaNoSujeta2.Name = "xrlVentaNoSujeta2"
        Me.xrlVentaNoSujeta2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaNoSujeta2.SizeF = New System.Drawing.SizeF(81.99997!, 15.99998!)
        Me.xrlVentaNoSujeta2.StylePriority.UseBorders = False
        Me.xrlVentaNoSujeta2.StylePriority.UseFont = False
        Me.xrlVentaNoSujeta2.StylePriority.UseTextAlignment = False
        Me.xrlVentaNoSujeta2.Text = "0.00"
        Me.xrlVentaNoSujeta2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal2
        '
        Me.xrlTotal2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlTotal2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlTotal2.LocationFloat = New DevExpress.Utils.PointFloat(656.9993!, 673.3751!)
        Me.xrlTotal2.Name = "xrlTotal2"
        Me.xrlTotal2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal2.SizeF = New System.Drawing.SizeF(81.99997!, 15.99998!)
        Me.xrlTotal2.StylePriority.UseBorders = False
        Me.xrlTotal2.StylePriority.UseFont = False
        Me.xrlTotal2.StylePriority.UseTextAlignment = False
        Me.xrlTotal2.Text = "0.00"
        Me.xrlTotal2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlIva2
        '
        Me.xrlIva2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIva2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlIva2.LocationFloat = New DevExpress.Utils.PointFloat(656.9993!, 576.7501!)
        Me.xrlIva2.Name = "xrlIva2"
        Me.xrlIva2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIva2.SizeF = New System.Drawing.SizeF(81.99997!, 15.99998!)
        Me.xrlIva2.StylePriority.UseBorders = False
        Me.xrlIva2.StylePriority.UseFont = False
        Me.xrlIva2.StylePriority.UseTextAlignment = False
        Me.xrlIva2.Text = "0.00"
        Me.xrlIva2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDiasCredito2
        '
        Me.xrlDiasCredito2.CanGrow = False
        Me.xrlDiasCredito2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDiasCredito2.LocationFloat = New DevExpress.Utils.PointFloat(377.5835!, 369.9999!)
        Me.xrlDiasCredito2.Name = "xrlDiasCredito2"
        Me.xrlDiasCredito2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDiasCredito2.SizeF = New System.Drawing.SizeF(94.04163!, 17.0!)
        Me.xrlDiasCredito2.StylePriority.UseFont = False
        Me.xrlDiasCredito2.StylePriority.UseTextAlignment = False
        Me.xrlDiasCredito2.Text = "xrlDiasCredito"
        Me.xrlDiasCredito2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlDiasCredito2.Visible = False
        '
        'xrlIvaRetenido2
        '
        Me.xrlIvaRetenido2.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIvaRetenido2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlIvaRetenido2.LocationFloat = New DevExpress.Utils.PointFloat(656.9992!, 614.7501!)
        Me.xrlIvaRetenido2.Name = "xrlIvaRetenido2"
        Me.xrlIvaRetenido2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIvaRetenido2.SizeF = New System.Drawing.SizeF(81.99997!, 15.99998!)
        Me.xrlIvaRetenido2.StylePriority.UseBorders = False
        Me.xrlIvaRetenido2.StylePriority.UseFont = False
        Me.xrlIvaRetenido2.StylePriority.UseTextAlignment = False
        Me.xrlIvaRetenido2.Text = "0.00"
        Me.xrlIvaRetenido2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCantLetras2
        '
        Me.xrlCantLetras2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlCantLetras2.LocationFloat = New DevExpress.Utils.PointFloat(15.54091!, 553.3751!)
        Me.xrlCantLetras2.Name = "xrlCantLetras2"
        Me.xrlCantLetras2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCantLetras2.SizeF = New System.Drawing.SizeF(515.3745!, 17.00001!)
        Me.xrlCantLetras2.StylePriority.UseFont = False
        Me.xrlCantLetras2.StylePriority.UseTextAlignment = False
        Me.xrlCantLetras2.Text = "DIEZ"
        Me.xrlCantLetras2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlComentario2
        '
        Me.xrlComentario2.CanShrink = True
        Me.xrlComentario2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlComentario2.LocationFloat = New DevExpress.Utils.PointFloat(15.54091!, 594.3751!)
        Me.xrlComentario2.Name = "xrlComentario2"
        Me.xrlComentario2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlComentario2.SizeF = New System.Drawing.SizeF(359.2084!, 17.00001!)
        Me.xrlComentario2.StylePriority.UseFont = False
        Me.xrlComentario2.StylePriority.UseTextAlignment = False
        Me.xrlComentario2.Text = "Comentario"
        Me.xrlComentario2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlComentario2.Visible = False
        '
        'xrlDireccion3
        '
        Me.xrlDireccion3.CanGrow = False
        Me.xrlDireccion3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDireccion3.LocationFloat = New DevExpress.Utils.PointFloat(73.83218!, 802.6041!)
        Me.xrlDireccion3.Name = "xrlDireccion3"
        Me.xrlDireccion3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDireccion3.SizeF = New System.Drawing.SizeF(428.0828!, 15.00006!)
        Me.xrlDireccion3.StylePriority.UseFont = False
        Me.xrlDireccion3.StylePriority.UseTextAlignment = False
        Me.xrlDireccion3.Text = "RESIDENCIAL CLAUDIA PASAJE AIDA No.19"
        Me.xrlDireccion3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNit3
        '
        Me.xrlNit3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNit3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNit3.LocationFloat = New DevExpress.Utils.PointFloat(545.7908!, 866.3542!)
        Me.xrlNit3.Name = "xrlNit3"
        Me.xrlNit3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNit3.SizeF = New System.Drawing.SizeF(136.0!, 16.0!)
        Me.xrlNit3.StylePriority.UseBorders = False
        Me.xrlNit3.StylePriority.UseFont = False
        Me.xrlNit3.StylePriority.UseTextAlignment = False
        Me.xrlNit3.Text = "0614-251107-107-7"
        Me.xrlNit3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlMunic3
        '
        Me.xrlMunic3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlMunic3.LocationFloat = New DevExpress.Utils.PointFloat(80.83216!, 823.3541!)
        Me.xrlMunic3.Name = "xrlMunic3"
        Me.xrlMunic3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlMunic3.SizeF = New System.Drawing.SizeF(166.3299!, 15.00003!)
        Me.xrlMunic3.StylePriority.UseFont = False
        Me.xrlMunic3.StylePriority.UseTextAlignment = False
        Me.xrlMunic3.Text = "SAN SALVADOR"
        Me.xrlMunic3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFecha3
        '
        Me.xrlFecha3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlFecha3.LocationFloat = New DevExpress.Utils.PointFloat(569.7907!, 784.6041!)
        Me.xrlFecha3.Name = "xrlFecha3"
        Me.xrlFecha3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha3.SizeF = New System.Drawing.SizeF(89.50046!, 15.0!)
        Me.xrlFecha3.StylePriority.UseFont = False
        Me.xrlFecha3.StylePriority.UseTextAlignment = False
        Me.xrlFecha3.Text = "25/12/2012"
        Me.xrlFecha3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNumero3
        '
        Me.xrlNumero3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNumero3.LocationFloat = New DevExpress.Utils.PointFloat(490.0408!, 745.6041!)
        Me.xrlNumero3.Name = "xrlNumero3"
        Me.xrlNumero3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero3.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlNumero3.StylePriority.UseFont = False
        Me.xrlNumero3.StylePriority.UseTextAlignment = False
        Me.xrlNumero3.Text = "1945"
        Me.xrlNumero3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNombre3
        '
        Me.xrlNombre3.CanGrow = False
        Me.xrlNombre3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNombre3.LocationFloat = New DevExpress.Utils.PointFloat(67.54089!, 783.6041!)
        Me.xrlNombre3.Name = "xrlNombre3"
        Me.xrlNombre3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNombre3.SizeF = New System.Drawing.SizeF(314.9588!, 16.0!)
        Me.xrlNombre3.StylePriority.UseFont = False
        Me.xrlNombre3.StylePriority.UseTextAlignment = False
        Me.xrlNombre3.Text = "IT OUTSOURCING, S.A. DE C.V."
        Me.xrlNombre3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNrc3
        '
        Me.xrlNrc3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNrc3.LocationFloat = New DevExpress.Utils.PointFloat(598.7906!, 802.6042!)
        Me.xrlNrc3.Name = "xrlNrc3"
        Me.xrlNrc3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNrc3.SizeF = New System.Drawing.SizeF(127.0833!, 14.99999!)
        Me.xrlNrc3.StylePriority.UseFont = False
        Me.xrlNrc3.StylePriority.UseTextAlignment = False
        Me.xrlNrc3.Text = "194534-3"
        Me.xrlNrc3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDepto3
        '
        Me.xrlDepto3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDepto3.LocationFloat = New DevExpress.Utils.PointFloat(105.8322!, 844.3541!)
        Me.xrlDepto3.Name = "xrlDepto3"
        Me.xrlDepto3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDepto3.SizeF = New System.Drawing.SizeF(192.3716!, 15.00002!)
        Me.xrlDepto3.StylePriority.UseFont = False
        Me.xrlDepto3.StylePriority.UseTextAlignment = False
        Me.xrlDepto3.Text = "SAN SALVADOR"
        Me.xrlDepto3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCodigo3
        '
        Me.xrlCodigo3.CanGrow = False
        Me.xrlCodigo3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlCodigo3.LocationFloat = New DevExpress.Utils.PointFloat(568.2043!, 744.3541!)
        Me.xrlCodigo3.Name = "xrlCodigo3"
        Me.xrlCodigo3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCodigo3.SizeF = New System.Drawing.SizeF(124.6696!, 16.00001!)
        Me.xrlCodigo3.StylePriority.UseFont = False
        Me.xrlCodigo3.StylePriority.UseTextAlignment = False
        Me.xrlCodigo3.Text = "CODIGO: 001"
        Me.xrlCodigo3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlCodigo3.Visible = False
        '
        'xrlPedido3
        '
        Me.xrlPedido3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlPedido3.LocationFloat = New DevExpress.Utils.PointFloat(135.1203!, 741.3541!)
        Me.xrlPedido3.Name = "xrlPedido3"
        Me.xrlPedido3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPedido3.SizeF = New System.Drawing.SizeF(112.0417!, 16.0!)
        Me.xrlPedido3.StylePriority.UseFont = False
        Me.xrlPedido3.StylePriority.UseTextAlignment = False
        Me.xrlPedido3.Text = "123456"
        Me.xrlPedido3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDocAfectados3
        '
        Me.xrlDocAfectados3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDocAfectados3.LocationFloat = New DevExpress.Utils.PointFloat(270.4581!, 741.3541!)
        Me.xrlDocAfectados3.Name = "xrlDocAfectados3"
        Me.xrlDocAfectados3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDocAfectados3.SizeF = New System.Drawing.SizeF(112.0417!, 16.0!)
        Me.xrlDocAfectados3.StylePriority.UseFont = False
        Me.xrlDocAfectados3.StylePriority.UseTextAlignment = False
        Me.xrlDocAfectados3.Text = "No. Afectados"
        Me.xrlDocAfectados3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFormaPago3
        '
        Me.xrlFormaPago3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlFormaPago3.LocationFloat = New DevExpress.Utils.PointFloat(614.7907!, 844.3542!)
        Me.xrlFormaPago3.Name = "xrlFormaPago3"
        Me.xrlFormaPago3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFormaPago3.SizeF = New System.Drawing.SizeF(145.0833!, 14.99999!)
        Me.xrlFormaPago3.StylePriority.UseFont = False
        Me.xrlFormaPago3.StylePriority.UseTextAlignment = False
        Me.xrlFormaPago3.Text = "CONTADO"
        Me.xrlFormaPago3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVentaACuenta3
        '
        Me.xrlVentaACuenta3.CanGrow = False
        Me.xrlVentaACuenta3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlVentaACuenta3.LocationFloat = New DevExpress.Utils.PointFloat(568.2043!, 760.3541!)
        Me.xrlVentaACuenta3.Name = "xrlVentaACuenta3"
        Me.xrlVentaACuenta3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaACuenta3.SizeF = New System.Drawing.SizeF(175.7112!, 16.00001!)
        Me.xrlVentaACuenta3.StylePriority.UseFont = False
        Me.xrlVentaACuenta3.StylePriority.UseTextAlignment = False
        Me.xrlVentaACuenta3.Text = "VENTA A CUENTA DE"
        Me.xrlVentaACuenta3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlVentaACuenta3.Visible = False
        '
        'xrlTelefono3
        '
        Me.xrlTelefono3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlTelefono3.LocationFloat = New DevExpress.Utils.PointFloat(15.54091!, 741.3541!)
        Me.xrlTelefono3.Name = "xrlTelefono3"
        Me.xrlTelefono3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTelefono3.SizeF = New System.Drawing.SizeF(112.0417!, 16.0!)
        Me.xrlTelefono3.StylePriority.UseFont = False
        Me.xrlTelefono3.StylePriority.UseTextAlignment = False
        Me.xrlTelefono3.Text = "TELEFONO"
        Me.xrlTelefono3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlTelefono3.Visible = False
        '
        'xrlGiro3
        '
        Me.xrlGiro3.CanGrow = False
        Me.xrlGiro3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlGiro3.LocationFloat = New DevExpress.Utils.PointFloat(545.7908!, 823.3541!)
        Me.xrlGiro3.Name = "xrlGiro3"
        Me.xrlGiro3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlGiro3.SizeF = New System.Drawing.SizeF(181.1662!, 15.00006!)
        Me.xrlGiro3.StylePriority.UseFont = False
        Me.xrlGiro3.StylePriority.UseTextAlignment = False
        Me.xrlGiro3.Text = "GIRO"
        Me.xrlGiro3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrSubreport3
        '
        Me.XrSubreport3.LocationFloat = New DevExpress.Utils.PointFloat(19.83244!, 887.75!)
        Me.XrSubreport3.Name = "XrSubreport3"
        Me.XrSubreport3.ReportSource = Me.Fac_rptDetalleUnico3
        Me.XrSubreport3.SizeF = New System.Drawing.SizeF(756.25!, 22.99998!)
        '
        'xrlSubTotal3
        '
        Me.xrlSubTotal3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlSubTotal3.LocationFloat = New DevExpress.Utils.PointFloat(661.9155!, 967.1041!)
        Me.xrlSubTotal3.Name = "xrlSubTotal3"
        Me.xrlSubTotal3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSubTotal3.SizeF = New System.Drawing.SizeF(81.99997!, 16.0!)
        Me.xrlSubTotal3.StylePriority.UseFont = False
        Me.xrlSubTotal3.StylePriority.UseTextAlignment = False
        Me.xrlSubTotal3.Text = "0.00"
        Me.xrlSubTotal3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaAfecta3
        '
        Me.xrlVentaAfecta3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaAfecta3.LocationFloat = New DevExpress.Utils.PointFloat(661.9155!, 924.8542!)
        Me.xrlVentaAfecta3.Name = "xrlVentaAfecta3"
        Me.xrlVentaAfecta3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaAfecta3.SizeF = New System.Drawing.SizeF(81.99997!, 16.875!)
        Me.xrlVentaAfecta3.StylePriority.UseFont = False
        Me.xrlVentaAfecta3.StylePriority.UseTextAlignment = False
        Me.xrlVentaAfecta3.Text = "0.00"
        Me.xrlVentaAfecta3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVentaExenta3
        '
        Me.xrlVentaExenta3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaExenta3.LocationFloat = New DevExpress.Utils.PointFloat(661.9155!, 1025.729!)
        Me.xrlVentaExenta3.Name = "xrlVentaExenta3"
        Me.xrlVentaExenta3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaExenta3.SizeF = New System.Drawing.SizeF(81.99997!, 16.0!)
        Me.xrlVentaExenta3.StylePriority.UseFont = False
        Me.xrlVentaExenta3.StylePriority.UseTextAlignment = False
        Me.xrlVentaExenta3.Text = "0.00"
        Me.xrlVentaExenta3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlVendedor3
        '
        Me.xrlVendedor3.CanGrow = False
        Me.xrlVendedor3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlVendedor3.LocationFloat = New DevExpress.Utils.PointFloat(20.45713!, 983.7292!)
        Me.xrlVendedor3.Name = "xrlVendedor3"
        Me.xrlVendedor3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVendedor3.SizeF = New System.Drawing.SizeF(204.4583!, 17.00001!)
        Me.xrlVendedor3.StylePriority.UseFont = False
        Me.xrlVendedor3.StylePriority.UseTextAlignment = False
        Me.xrlVendedor3.Text = "VENDEDOR"
        Me.xrlVendedor3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlVendedor3.Visible = False
        '
        'xrlVentaNoSujeta3
        '
        Me.xrlVentaNoSujeta3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlVentaNoSujeta3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlVentaNoSujeta3.LocationFloat = New DevExpress.Utils.PointFloat(661.9155!, 1006.729!)
        Me.xrlVentaNoSujeta3.Name = "xrlVentaNoSujeta3"
        Me.xrlVentaNoSujeta3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVentaNoSujeta3.SizeF = New System.Drawing.SizeF(81.99997!, 15.99998!)
        Me.xrlVentaNoSujeta3.StylePriority.UseBorders = False
        Me.xrlVentaNoSujeta3.StylePriority.UseFont = False
        Me.xrlVentaNoSujeta3.StylePriority.UseTextAlignment = False
        Me.xrlVentaNoSujeta3.Text = "0.00"
        Me.xrlVentaNoSujeta3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlTotal3
        '
        Me.xrlTotal3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlTotal3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlTotal3.LocationFloat = New DevExpress.Utils.PointFloat(661.9155!, 1044.729!)
        Me.xrlTotal3.Name = "xrlTotal3"
        Me.xrlTotal3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal3.SizeF = New System.Drawing.SizeF(81.99997!, 15.99998!)
        Me.xrlTotal3.StylePriority.UseBorders = False
        Me.xrlTotal3.StylePriority.UseFont = False
        Me.xrlTotal3.StylePriority.UseTextAlignment = False
        Me.xrlTotal3.Text = "0.00"
        Me.xrlTotal3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlIva3
        '
        Me.xrlIva3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIva3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlIva3.LocationFloat = New DevExpress.Utils.PointFloat(661.9155!, 948.1042!)
        Me.xrlIva3.Name = "xrlIva3"
        Me.xrlIva3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIva3.SizeF = New System.Drawing.SizeF(81.99997!, 15.99998!)
        Me.xrlIva3.StylePriority.UseBorders = False
        Me.xrlIva3.StylePriority.UseFont = False
        Me.xrlIva3.StylePriority.UseTextAlignment = False
        Me.xrlIva3.Text = "0.00"
        Me.xrlIva3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlDiasCredito3
        '
        Me.xrlDiasCredito3.CanGrow = False
        Me.xrlDiasCredito3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDiasCredito3.LocationFloat = New DevExpress.Utils.PointFloat(382.4998!, 741.3541!)
        Me.xrlDiasCredito3.Name = "xrlDiasCredito3"
        Me.xrlDiasCredito3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDiasCredito3.SizeF = New System.Drawing.SizeF(94.04163!, 17.0!)
        Me.xrlDiasCredito3.StylePriority.UseFont = False
        Me.xrlDiasCredito3.StylePriority.UseTextAlignment = False
        Me.xrlDiasCredito3.Text = "xrlDiasCredito"
        Me.xrlDiasCredito3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlDiasCredito3.Visible = False
        '
        'xrlIvaRetenido3
        '
        Me.xrlIvaRetenido3.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlIvaRetenido3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlIvaRetenido3.LocationFloat = New DevExpress.Utils.PointFloat(661.9154!, 986.1042!)
        Me.xrlIvaRetenido3.Name = "xrlIvaRetenido3"
        Me.xrlIvaRetenido3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlIvaRetenido3.SizeF = New System.Drawing.SizeF(81.99997!, 15.99998!)
        Me.xrlIvaRetenido3.StylePriority.UseBorders = False
        Me.xrlIvaRetenido3.StylePriority.UseFont = False
        Me.xrlIvaRetenido3.StylePriority.UseTextAlignment = False
        Me.xrlIvaRetenido3.Text = "0.00"
        Me.xrlIvaRetenido3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlCantLetras3
        '
        Me.xrlCantLetras3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlCantLetras3.LocationFloat = New DevExpress.Utils.PointFloat(20.45713!, 924.7292!)
        Me.xrlCantLetras3.Name = "xrlCantLetras3"
        Me.xrlCantLetras3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCantLetras3.SizeF = New System.Drawing.SizeF(515.3745!, 17.00001!)
        Me.xrlCantLetras3.StylePriority.UseFont = False
        Me.xrlCantLetras3.StylePriority.UseTextAlignment = False
        Me.xrlCantLetras3.Text = "DIEZ"
        Me.xrlCantLetras3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlComentario3
        '
        Me.xrlComentario3.CanShrink = True
        Me.xrlComentario3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlComentario3.LocationFloat = New DevExpress.Utils.PointFloat(20.45713!, 965.7292!)
        Me.xrlComentario3.Name = "xrlComentario3"
        Me.xrlComentario3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlComentario3.SizeF = New System.Drawing.SizeF(359.2084!, 17.00001!)
        Me.xrlComentario3.StylePriority.UseFont = False
        Me.xrlComentario3.StylePriority.UseTextAlignment = False
        Me.xrlComentario3.Text = "Comentario"
        Me.xrlComentario3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.xrlComentario3.Visible = False
        '
        'fac_rptUnico
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportHeader})
        Me.DataMember = "Factura"
        Me.DesignerOptions.ShowExportWarnings = False
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Arial Narrow", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(23, 40, 28, 26)
        Me.PageHeight = 1400
        Me.PaperKind = System.Drawing.Printing.PaperKind.Legal
        Me.SnapGridSize = 2.083333!
        Me.SnapToGrid = False
        Me.Version = "11.1"
        CType(Me.Fac_rptDetalleUnico1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Fac_rptDetalleUnico2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Fac_rptDetalleUnico3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents xrlGiro As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTelefono As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaACuenta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFormaPago As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDocAfectados As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPedido As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCodigo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDepto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNrc As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNombre As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumero As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFecha As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlMunic As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNit As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDireccion As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrSubreport1 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents xrlComentario As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCantLetras As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIvaRetenido As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDiasCredito As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIva As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaNoSujeta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVendedor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaAfecta As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlSubTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaAfecta3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVendedor3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlGiro3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrSubreport3 As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Fac_rptDetalleUnico3 As Nexus.fac_rptDetalleUnico
    Friend WithEvents xrlSubTotal3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaNoSujeta3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIvaRetenido3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCantLetras3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlComentario3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIva3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDiasCredito3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTelefono3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFecha3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumero3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNombre3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDireccion3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNit3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlMunic3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNrc3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDocAfectados3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFormaPago3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaACuenta3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDepto3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCodigo3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPedido3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaAfecta2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaExenta2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVendedor2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlGiro2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrSubreport2 As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Fac_rptDetalleUnico2 As Nexus.fac_rptDetalleUnico
    Friend WithEvents xrlSubTotal2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaNoSujeta2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIvaRetenido2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCantLetras2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlComentario2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlIva2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDiasCredito2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTelefono2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFecha2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumero2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNombre2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDireccion2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNit2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlMunic2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNrc2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDocAfectados2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFormaPago2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlVentaACuenta2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDepto2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCodigo2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlPedido2 As DevExpress.XtraReports.UI.XRLabel
    Private WithEvents Fac_rptDetalleUnico1 As Nexus.fac_rptDetalleUnico
    'Friend WithEvents DsGas1 As Nexus.dsGas
End Class
