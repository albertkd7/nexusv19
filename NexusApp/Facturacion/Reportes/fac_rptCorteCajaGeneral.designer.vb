<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class fac_rptCorteCajaGeneral
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrSubreport1 = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport5 = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport9 = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport8 = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport7 = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport6 = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport4 = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport3 = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport2 = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport10 = New DevExpress.XtraReports.UI.XRSubreport()
        Me.xrlTitulo = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.xrlPeriodo = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlEmpresa = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.xrlVendedor = New DevExpress.XtraReports.UI.XRLabel()
        Me.NumeroPartida = New DevExpress.XtraReports.UI.CalculatedField()
        Me.DsFactura1 = New Nexus.dsFactura()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.FormattingRule1 = New DevExpress.XtraReports.UI.FormattingRule()
        CType(Me.DsFactura1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.Dpi = 100.0!
        Me.XrPageInfo2.Format = "{0:dd/MM/yyyy hh:mm tt}"
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(646.0001!, 0.0!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(131.0!, 16.0!)
        Me.XrPageInfo2.StylePriority.UseTextAlignment = False
        Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel1
        '
        Me.XrLabel1.Dpi = 100.0!
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(527.0001!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(59.0!, 16.0!)
        Me.XrLabel1.Text = "P�g. No."
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport1, Me.XrSubreport5, Me.XrSubreport9, Me.XrSubreport8, Me.XrSubreport7, Me.XrSubreport6, Me.XrSubreport4, Me.XrSubreport3, Me.XrSubreport2, Me.XrSubreport10})
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 191.8333!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrSubreport1
        '
        Me.XrSubreport1.Dpi = 100.0!
        Me.XrSubreport1.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 0.0!)
        Me.XrSubreport1.Name = "XrSubreport1"
        Me.XrSubreport1.ReportSource = New Nexus.fac_rptCorteCaja1()
        Me.XrSubreport1.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'XrSubreport5
        '
        Me.XrSubreport5.Dpi = 100.0!
        Me.XrSubreport5.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 71.99999!)
        Me.XrSubreport5.Name = "XrSubreport5"
        Me.XrSubreport5.ReportSource = New Nexus.fac_rptCortePOS()
        Me.XrSubreport5.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'XrSubreport9
        '
        Me.XrSubreport9.Dpi = 100.0!
        Me.XrSubreport9.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 48.00002!)
        Me.XrSubreport9.Name = "XrSubreport9"
        Me.XrSubreport9.ReportSource = New Nexus.fac_rptCorteCheques()
        Me.XrSubreport9.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'XrSubreport8
        '
        Me.XrSubreport8.Dpi = 100.0!
        Me.XrSubreport8.LocationFloat = New DevExpress.Utils.PointFloat(470.3335!, 0.0!)
        Me.XrSubreport8.Name = "XrSubreport8"
        Me.XrSubreport8.ReportSource = New Nexus.fac_rptCorteCajaArqueo()
        Me.XrSubreport8.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'XrSubreport7
        '
        Me.XrSubreport7.Dpi = 100.0!
        Me.XrSubreport7.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 144.8333!)
        Me.XrSubreport7.Name = "XrSubreport7"
        Me.XrSubreport7.ReportSource = New Nexus.fac_rptCorteDocAnulados()
        Me.XrSubreport7.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'XrSubreport6
        '
        Me.XrSubreport6.Dpi = 100.0!
        Me.XrSubreport6.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 168.8333!)
        Me.XrSubreport6.Name = "XrSubreport6"
        Me.XrSubreport6.ReportSource = New Nexus.fac_rptCorteCaja6()
        Me.XrSubreport6.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'XrSubreport4
        '
        Me.XrSubreport4.Dpi = 100.0!
        Me.XrSubreport4.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 120.8333!)
        Me.XrSubreport4.Name = "XrSubreport4"
        Me.XrSubreport4.ReportSource = New Nexus.fac_rptCorteCaja5()
        Me.XrSubreport4.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'XrSubreport3
        '
        Me.XrSubreport3.Dpi = 100.0!
        Me.XrSubreport3.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 97.5834!)
        Me.XrSubreport3.Name = "XrSubreport3"
        Me.XrSubreport3.ReportSource = New Nexus.fac_rptCorteCaja3()
        Me.XrSubreport3.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'XrSubreport2
        '
        Me.XrSubreport2.Dpi = 100.0!
        Me.XrSubreport2.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 24.00004!)
        Me.XrSubreport2.Name = "XrSubreport2"
        Me.XrSubreport2.ReportSource = New Nexus.fac_rptCorteCaja2()
        Me.XrSubreport2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'XrSubreport10
        '
        Me.XrSubreport10.Dpi = 100.0!
        Me.XrSubreport10.LocationFloat = New DevExpress.Utils.PointFloat(389.0!, 71.99999!)
        Me.XrSubreport10.Name = "XrSubreport10"
        Me.XrSubreport10.ReportSource = New Nexus.fac_rptCorteRemesas()
        Me.XrSubreport10.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'xrlTitulo
        '
        Me.xrlTitulo.Dpi = 100.0!
        Me.xrlTitulo.Font = New System.Drawing.Font("Arial", 11.0!)
        Me.xrlTitulo.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 38.0!)
        Me.xrlTitulo.Name = "xrlTitulo"
        Me.xrlTitulo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTitulo.SizeF = New System.Drawing.SizeF(769.0!, 21.0!)
        Me.xrlTitulo.StylePriority.UseFont = False
        Me.xrlTitulo.StylePriority.UseTextAlignment = False
        Me.xrlTitulo.Text = "T�tulo del reporte"
        Me.xrlTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Dpi = 100.0!
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(590.0!, 0.0!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(52.0!, 16.0!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlPeriodo
        '
        Me.xrlPeriodo.Dpi = 100.0!
        Me.xrlPeriodo.Font = New System.Drawing.Font("Arial", 11.0!)
        Me.xrlPeriodo.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 82.0!)
        Me.xrlPeriodo.Name = "xrlPeriodo"
        Me.xrlPeriodo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPeriodo.SizeF = New System.Drawing.SizeF(769.0!, 21.0!)
        Me.xrlPeriodo.StylePriority.UseFont = False
        Me.xrlPeriodo.StylePriority.UseTextAlignment = False
        Me.xrlPeriodo.Text = "Per�odo del reporte"
        Me.xrlPeriodo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'xrlEmpresa
        '
        Me.xrlEmpresa.Dpi = 100.0!
        Me.xrlEmpresa.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.xrlEmpresa.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 16.0!)
        Me.xrlEmpresa.Name = "xrlEmpresa"
        Me.xrlEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlEmpresa.SizeF = New System.Drawing.SizeF(769.0!, 21.0!)
        Me.xrlEmpresa.StylePriority.UseFont = False
        Me.xrlEmpresa.StylePriority.UseTextAlignment = False
        Me.xrlEmpresa.Text = "IT OUTSOURCING, S. A. DE C. V."
        Me.xrlEmpresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlVendedor, Me.XrPageInfo2, Me.XrLabel1, Me.XrPageInfo1, Me.xrlPeriodo, Me.xrlTitulo, Me.xrlEmpresa})
        Me.PageHeader.Dpi = 100.0!
        Me.PageHeader.HeightF = 105.4583!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlVendedor
        '
        Me.xrlVendedor.Dpi = 100.0!
        Me.xrlVendedor.Font = New System.Drawing.Font("Arial", 11.0!)
        Me.xrlVendedor.LocationFloat = New DevExpress.Utils.PointFloat(7.5!, 59.60416!)
        Me.xrlVendedor.Name = "xrlVendedor"
        Me.xrlVendedor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlVendedor.SizeF = New System.Drawing.SizeF(769.0!, 21.0!)
        Me.xrlVendedor.StylePriority.UseFont = False
        Me.xrlVendedor.StylePriority.UseTextAlignment = False
        Me.xrlVendedor.Text = "Vendedor"
        Me.xrlVendedor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'NumeroPartida
        '
        Me.NumeroPartida.DataMember = "Partidas"
        Me.NumeroPartida.Expression = "'Partida No. ' & [Numero] & '    Fecha de la partida: ' & [Fecha]"
        Me.NumeroPartida.Name = "NumeroPartida"
        '
        'DsFactura1
        '
        Me.DsFactura1.DataSetName = "dsFactura"
        Me.DsFactura1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine3, Me.XrLine2, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLine1})
        Me.ReportFooter.Dpi = 100.0!
        Me.ReportFooter.HeightF = 66.99999!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'XrLine3
        '
        Me.XrLine3.Dpi = 100.0!
        Me.XrLine3.LocationFloat = New DevExpress.Utils.PointFloat(12.0!, 47.99997!)
        Me.XrLine3.Name = "XrLine3"
        Me.XrLine3.SizeF = New System.Drawing.SizeF(180.0001!, 2.0!)
        '
        'XrLine2
        '
        Me.XrLine2.Dpi = 100.0!
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(279.9583!, 47.99997!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(180.0001!, 2.0!)
        '
        'XrLabel4
        '
        Me.XrLabel4.Dpi = 100.0!
        Me.XrLabel4.Font = New System.Drawing.Font("Arial", 11.0!)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(611.1042!, 49.99998!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(113.2083!, 17.00001!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "Autorizado Por"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel3
        '
        Me.XrLabel3.Dpi = 100.0!
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 11.0!)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(321.5209!, 49.99998!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(105.2083!, 17.00001!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "Revisado Por"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel2
        '
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 11.0!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(43.79166!, 49.99998!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(105.2083!, 17.00001!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "Elaborado Por"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine1
        '
        Me.XrLine1.Dpi = 100.0!
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(574.9999!, 47.99997!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(180.0001!, 2.0!)
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.Dpi = 100.0!
        Me.TopMarginBand1.HeightF = 19.37501!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.Dpi = 100.0!
        Me.BottomMarginBand1.HeightF = 23.2083!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'FormattingRule1
        '
        Me.FormattingRule1.Name = "FormattingRule1"
        '
        'fac_rptCorteCajaGeneral
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.NumeroPartida})
        Me.DataMember = "fac_VentasFormaPago"
        Me.DataSource = Me.DsFactura1
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormattingRuleSheet.AddRange(New DevExpress.XtraReports.UI.FormattingRule() {Me.FormattingRule1})
        Me.Margins = New System.Drawing.Printing.Margins(35, 35, 19, 23)
        Me.SnapGridSize = 3.125!
        Me.SnapToGrid = False
        Me.Version = "16.1"
        CType(Me.DsFactura1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents xrlTitulo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents xrlPeriodo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlEmpresa As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents NumeroPartida As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents DsFactura1 As Nexus.dsFactura
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents XrSubreport4 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents XrSubreport3 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents XrSubreport2 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents XrSubreport1 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents XrLine3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents xrlVendedor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrSubreport6 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents XrSubreport7 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents XrSubreport8 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents XrSubreport9 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents XrSubreport5 As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents FormattingRule1 As DevExpress.XtraReports.UI.FormattingRule
    Friend WithEvents XrSubreport10 As DevExpress.XtraReports.UI.XRSubreport
End Class
