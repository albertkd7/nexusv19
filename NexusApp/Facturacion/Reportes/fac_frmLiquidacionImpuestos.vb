﻿Imports NexusBLL

Public Class fac_frmLiquidacionImpuestos
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()


    Private Sub frmLibroContribuyentes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        meMes.Month = Now.Month
        spAnio.EditValue = Now.Year
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub


    Private Sub fac_frmLibroContribuentes_Reporte() Handles Me.Reporte
        Dim ds As DataSet = bl.getLiquidacionImpuestos(meMes.Month, spAnio.EditValue, leSucursal.EditValue)
        Dim rpt As New fac_rptLiquidacionImpuestos() With {.DataSource = ds.Tables(6), .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa

        If ds.Tables(0).Rows.Count > 0 Then
            rpt.XrSubreport1.ReportSource.DataSource = ds.Tables(0)
            rpt.XrSubreport1.ReportSource.DataMember = ""
        Else
            rpt.XrSubreport1.ReportSource.DataSource = Nothing
            rpt.XrSubreport1.ReportSource.DataMember = ""
        End If
        If ds.Tables(1).Rows.Count > 0 Then
            rpt.XrSubreport2.ReportSource.DataSource = ds.Tables(1)
            rpt.XrSubreport2.ReportSource.DataMember = ""
        Else
            rpt.XrSubreport2.ReportSource.DataSource = Nothing
            rpt.XrSubreport2.ReportSource.DataMember = ""
        End If
        If ds.Tables(2).Rows.Count > 0 Then
            rpt.XrSubreport3.ReportSource.DataSource = ds.Tables(2)
            rpt.XrSubreport3.ReportSource.DataMember = ""
        Else
            rpt.XrSubreport3.ReportSource.DataSource = Nothing
            rpt.XrSubreport3.ReportSource.DataMember = ""
        End If
        If ds.Tables(3).Rows.Count > 0 Then
            rpt.XrSubreport4.ReportSource.DataSource = ds.Tables(3)
            rpt.XrSubreport4.ReportSource.DataMember = ""
        Else
            rpt.XrSubreport4.ReportSource.DataSource = Nothing
            rpt.XrSubreport4.ReportSource.DataMember = ""
        End If
        If ds.Tables(4).Rows.Count > 0 Then
            rpt.XrSubreport5.ReportSource.DataSource = ds.Tables(4)
            rpt.XrSubreport5.ReportSource.DataMember = ""
        Else
            rpt.XrSubreport5.ReportSource.DataSource = Nothing
            rpt.XrSubreport5.ReportSource.DataMember = ""
        End If
        If ds.Tables(5).Rows.Count > 0 Then
            rpt.XrSubreport6.ReportSource.DataSource = ds.Tables(5)
            rpt.XrSubreport6.ReportSource.DataMember = ""
        Else
            rpt.XrSubreport6.ReportSource.DataSource = Nothing
            rpt.XrSubreport6.ReportSource.DataMember = ""
        End If
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlMes.Text = "MES: " + (ObtieneMesString(meMes.EditValue)).ToUpper
        rpt.xrlAnio.Text = "AÑO: " + spAnio.EditValue.ToString()
        rpt.ShowPreviewDialog()
    End Sub


    Private Sub btExportXLS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btExportXLS.Click
        ExportarLibroExcel()
    End Sub

    Private Sub ExportarLibroExcel()
        If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_Contribuyentes.XLSX") Then
            If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_Contribuyentes.XLS") Then
                MsgBox("No existe la plantilla necesaria para poder exportar", MsgBoxStyle.Critical, "Nota")
                Exit Sub
            End If
        End If

        Dim Template As String
        teProgreso.EditValue = "PREPARANDO LOS DATOS. ESPERE..."
        teProgreso.Visible = True
        If FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_Contribuyentes.XLSX") Then
            Template = Application.StartupPath & "\Plantillas\Libro_Contribuyentes.XLSX"
            FileCopy(Application.StartupPath & "\Plantillas\Libro_Auxiliar.XLSX", Template)
        Else
            Template = Application.StartupPath & "\Plantillas\Libro_Contribuyentes.XLS"
            ' FileCopy(Application.StartupPath & "\Plantillas\Libro_Contribuyentes.XLS", Template)
        End If

        Dim oApp As Object = CreateObject("Excel.Application")

        'verifico si no está en una cultura diferente, por el momento de Inglés a Español, pendiente de verificar cuando sea a la inversa
        Dim oldCI As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        Try
            oApp.DisplayAlerts = False
        Catch ex As Exception
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")
            oApp.DisplayAlerts = False
        End Try

        Dim Range As String

        oApp.workbooks.Open(Template)
        oApp.Cells(3, 1).Value = "NOMBRE DEL CONTRIBUYENTE: " + gsNombre_Empresa
        oApp.Cells(3, 8).Value = "NRC: " & dtParam.Rows(0).Item("NrcEmpresa")
        oApp.Cells(3, 10).Value = "NIT: " & dtParam.Rows(0).Item("NitEmpresa")

        oApp.Cells(4, 1).Value = "MES: " + (ObtieneMesString(meMes.EditValue)).ToUpper
        oApp.Cells(4, 4).Value = "AÑO: " + (spAnio.EditValue).ToString
        'oApp.Cells(4, 10).Value = "FOLIO No. " & seNumFolio.EditValue
        oApp.Cells(5, 1).Value = "SUCURSAL: " & leSucursal.Text
        oApp.Cells(5, 1).font.bold = True

        Dim TotExento As Decimal = 0.0, TotAfecto As Decimal = 0.0, TotIVA1 As Decimal = 0.0, TotIVA2 As Decimal = 0.0
        Dim TotGeneral As Decimal = 0.0, TotNoSujeto As Decimal = 0.0
        Dim Linea As Integer = 8, i As Integer = 0
        Dim ds As DataSet = bl.getLibroContribuyentes(meMes.Month, spAnio.EditValue, leSucursal.EditValue)

        While i <= ds.Tables(0).Rows.Count - 1
            oApp.Cells(Linea, 1).Value = i + 1
            oApp.Cells(Linea, 2).Value = ds.Tables(0).Rows(i).Item("Fecha")
            oApp.Cells(Linea, 3).Value = ds.Tables(0).Rows(i).Item("Numero")
            oApp.Cells(Linea, 4).Value = ds.Tables(0).Rows(i).Item("NumFormularioUnico")
            oApp.Cells(Linea, 5).Value = ds.Tables(0).Rows(i).Item("Nombre")
            oApp.Cells(Linea, 6).Value = ds.Tables(0).Rows(i).Item("Nrc")
            oApp.Cells(Linea, 7).Value = ds.Tables(0).Rows(i).Item("Nit")
            oApp.Cells(Linea, 8).Value = ds.Tables(0).Rows(i).Item("TotalExento")
            oApp.Cells(Linea, 9).Value = ds.Tables(0).Rows(i).Item("TotalAfecto")
            oApp.Cells(Linea, 10).Value = ds.Tables(0).Rows(i).Item("IVA1")
            oApp.Cells(Linea, 11).Value = ds.Tables(0).Rows(i).Item("IVA2")
            oApp.Cells(Linea, 12).Value = ds.Tables(0).Rows(i).Item("Total")
            oApp.Cells(Linea, 13).Value = ds.Tables(0).Rows(i).Item("NoSujeto")

            TotExento += ds.Tables(0).Rows(i).Item("TotalExento")
            TotAfecto += ds.Tables(0).Rows(i).Item("TotalAfecto")
            TotIVA1 += ds.Tables(0).Rows(i).Item("IVA1")
            TotIVA2 += ds.Tables(0).Rows(i).Item("IVA2")
            TotGeneral += ds.Tables(0).Rows(i).Item("Total")
            TotNoSujeto += ds.Tables(0).Rows(i).Item("NoSujeto")

            i += 1
            Linea += 1
        End While
        PonerLineaExcel(oApp, Linea)

        Linea += 1
        oApp.Cells(Linea, 7).Value = "TOTALES: "
        oApp.Cells(Linea, 8).Value = TotExento
        oApp.Cells(Linea, 9).Value = TotAfecto
        oApp.Cells(Linea, 10).Value = TotIVA1
        oApp.Cells(Linea, 11).Value = TotIVA2
        oApp.Cells(Linea, 12).Value = TotGeneral
        oApp.Cells(Linea, 13).Value = TotNoSujeto

        If ds.Tables(1).Rows.Count > 0 Then
            Linea += 2
            With ds.Tables(1).Rows(0)
                oApp.Cells(Linea, 5).Value = "RESUMEN DE OPERACIONES: "
                oApp.Cells(Linea, 5).font.bold = True
                oApp.Cells(Linea, 5).font.size = 8
                oApp.Cells(Linea, 7).Value = "EXENTAS"
                oApp.Cells(Linea, 7).font.size = 7
                oApp.Cells(Linea, 8).Value = "GRVADAS"
                oApp.Cells(Linea, 8).font.size = 7
                oApp.Cells(Linea, 9).Value = "EXPORTAC."
                oApp.Cells(Linea, 9).font.size = 7
                oApp.Cells(Linea, 10).Value = "DEBITO FISCAL"
                oApp.Cells(Linea, 10).font.size = 7
                oApp.Cells(Linea, 11).Value = "TOTAL"
                oApp.Cells(Linea, 11).font.size = 7
                oApp.Cells(Linea, 12).Value = "IVA RETENIDO"
                oApp.Cells(Linea, 12).font.size = 7

                Linea += 1
                oApp.Cells(Linea, 5).Value = "VENTAS A CONSUMIDORES FINALES:"
                oApp.Cells(Linea, 7).Value = .Item("VentaExentaConsum")
                oApp.Cells(Linea, 8).Value = .Item("VentaNetaConsum")
                oApp.Cells(Linea, 9).Value = .Item("Exportaciones")
                oApp.Cells(Linea, 10).Value = .Item("IvaConsum")
                oApp.Cells(Linea, 11).Value = .Item("VentaExentaConsum") + .Item("VentaNetaConsum") + .Item("IvaConsum")
                oApp.Cells(Linea, 12).Value = 0.0

                Linea += 1
                oApp.Cells(Linea, 5).Value = "VENTAS A CONTRIBUYENTES:"
                oApp.Cells(Linea, 7).Value = .Item("VentaExentaContrib")
                oApp.Cells(Linea, 8).Value = .Item("VentaNetaContrib")
                oApp.Cells(Linea, 9).Value = 0.0
                oApp.Cells(Linea, 10).Value = .Item("IvaContrib")
                oApp.Cells(Linea, 11).Value = .Item("VentaExentaContrib") + .Item("VentaNetaContrib") + .Item("IvaContrib")
                oApp.Cells(Linea, 12).Value = .Item("RetenPercep")

                Linea += 1

                oApp.Cells(Linea, 5).Value = "TOTAL DE OPERACIONES:"
                oApp.Cells(Linea, 5).font.bold = True
                'oApp.Cells(Linea, 5).WrapText = False
                PonerLineaExcel2(oApp, Linea)
                oApp.Cells(Linea, 7).Value = .Item("VentaExentaConsum") + .Item("VentaExentaContrib")
                oApp.Cells(Linea, 7).font.bold = True
                oApp.Cells(Linea, 8).Value = .Item("VentaNetaConsum") + .Item("VentaNetaContrib")
                oApp.Cells(Linea, 8).font.bold = True
                oApp.Cells(Linea, 9).Value = .Item("Exportaciones")
                oApp.Cells(Linea, 9).font.bold = True
                oApp.Cells(Linea, 10).Value = .Item("IvaConsum") + .Item("IvaContrib")
                oApp.Cells(Linea, 10).font.bold = True
                oApp.Cells(Linea, 11).Value = .Item("VentaExentaConsum") + .Item("VentaNetaConsum") + .Item("IvaConsum") + .Item("VentaExentaContrib") + .Item("VentaNetaContrib") + .Item("IvaContrib")
                oApp.Cells(Linea, 11).font.bold = True
                oApp.Cells(Linea, 12).Value = .Item("RetenPercep")
                oApp.Cells(Linea, 12).font.bold = True
            End With
        End If
        Linea += 3
        PonerLineaExcel3(oApp, Linea)
        oApp.Cells(Linea, 5).Value = "                            NOMBRE Y FIRMA DEL CONTADOR"
        oApp.Cells(Linea, 5).font.bold = True
        oApp.Cells(Linea, 5).font.size = 8

        Range = "$A$1:$M$" + CStr(Linea)
        oApp.ActiveSheet.PageSetup.PrintArea = Range
        oApp.Range("A1").Select()

        'oApp.ActiveSheet.Protect("DrawingObjects:=False, Contents:=True, Scenarios:= False")
        'oApp.Save()
        teProgreso.Visible = False
        oApp.Visible = True
        System.Threading.Thread.CurrentThread.CurrentCulture = oldCI

    End Sub
    Private Sub PonerLineaExcel(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("A{0}:M{0}", CStr(Linea))
        oApp.Cells(5, 33).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
    Private Sub PonerLineaExcel2(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("E{0}:L{0}", CStr(Linea))
        oApp.Cells(5, 33).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
    Private Sub PonerLineaExcel3(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("E{0}:G{0}", CStr(Linea))
        oApp.Cells(5, 33).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
End Class
