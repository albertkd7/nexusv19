﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmVentasProductosAnual
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim dt As New DataTable


    Private Sub fac_frmVentasProductosAnual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargaCombos()
        seEjercicio.EditValue = Today.Year
        rgTipo.SelectedIndex = 0
    End Sub

    Private Sub CargaCombos()
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS --")
        objCombos.invGrupos(leGrupo, "-- TODOS LOS GRUPOS --")
        objCombos.fac_PuntosVenta(lePuntoVenta, 1, "-- TODOS --")
        objCombos.inv_Bodegas(lebodega, "-- TODAS LAS BODEGAS --")
    End Sub

    Private Sub sbAceptar_Click() Handles Me.Reporte
        dt = bl.fac_VentasProductosAnual(seEjercicio.EditValue, BeProducto1.beCodigo.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, leGrupo.EditValue, rgTipo.SelectedIndex, lebodega.EditValue)
        Dim rpt As New fac_rptVentasProductosAnual() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, PUNTO DE VENTA: {1}", teTitulo.EditValue, lePuntoVenta.Text)
        rpt.xrlBodega.Text = "Bodega:  " + lebodega.Text
        If rgTipo.SelectedIndex = 0 Then
            rpt.xrlPeriodo.Text = "REPORTE EXPRESADO EN VALORES - AÑO: " + seEjercicio.EditValue.ToString
        Else
            rpt.xrlPeriodo.Text = "REPORTE EXPRESADO EN UNIDADES - AÑO: " + seEjercicio.EditValue.ToString
        End If
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub
End Class
