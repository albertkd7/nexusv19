﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class fac_frmListadoGestionesProspectos
	Inherits Nexus.gen_frmBaseRpt

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing AndAlso components IsNot Nothing Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmListadoGestionesProspectos))
		Me.teTitulo = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
		Me.deHasta = New DevExpress.XtraEditors.DateEdit()
		Me.deDesde = New DevExpress.XtraEditors.DateEdit()
		Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
		Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
		Me.leVendedor = New DevExpress.XtraEditors.LookUpEdit()
		Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
		Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
		Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
		Me.teNombreProspecto = New DevExpress.XtraEditors.TextEdit()
		Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
		Me.beCodProspecto = New DevExpress.XtraEditors.ButtonEdit()
		Me.chkGestiones = New DevExpress.XtraEditors.CheckEdit()
		CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupControl1.SuspendLayout()
		CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.teNombreProspecto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.beCodProspecto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkGestiones.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'GroupControl1
		'
		Me.GroupControl1.Controls.Add(Me.chkGestiones)
		Me.GroupControl1.Controls.Add(Me.LabelControl9)
		Me.GroupControl1.Controls.Add(Me.teNombreProspecto)
		Me.GroupControl1.Controls.Add(Me.LabelControl10)
		Me.GroupControl1.Controls.Add(Me.beCodProspecto)
		Me.GroupControl1.Controls.Add(Me.LabelControl4)
		Me.GroupControl1.Controls.Add(Me.leVendedor)
		Me.GroupControl1.Controls.Add(Me.LabelControl3)
		Me.GroupControl1.Controls.Add(Me.teTitulo)
		Me.GroupControl1.Controls.Add(Me.LabelControl6)
		Me.GroupControl1.Controls.Add(Me.deHasta)
		Me.GroupControl1.Controls.Add(Me.deDesde)
		Me.GroupControl1.Controls.Add(Me.LabelControl2)
		Me.GroupControl1.Controls.Add(Me.LabelControl1)
		Me.GroupControl1.Size = New System.Drawing.Size(881, 342)
		Me.GroupControl1.TabIndex = 0
		'
		'teTitulo
		'
		Me.teTitulo.EditValue = "INFORME DE GESTIONES - PROSPECTOS DE CLIENTES"
		Me.teTitulo.Location = New System.Drawing.Point(106, 153)
		Me.teTitulo.Name = "teTitulo"
		Me.teTitulo.Size = New System.Drawing.Size(335, 20)
		Me.teTitulo.TabIndex = 6
		'
		'LabelControl6
		'
		Me.LabelControl6.Location = New System.Drawing.Point(17, 156)
		Me.LabelControl6.Name = "LabelControl6"
		Me.LabelControl6.Size = New System.Drawing.Size(86, 13)
		Me.LabelControl6.TabIndex = 24
		Me.LabelControl6.Text = "Título del informe:"
		'
		'deHasta
		'
		Me.deHasta.EditValue = Nothing
		Me.deHasta.Location = New System.Drawing.Point(106, 57)
		Me.deHasta.Name = "deHasta"
		Me.deHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.deHasta.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.deHasta.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
		Me.deHasta.Size = New System.Drawing.Size(100, 20)
		Me.deHasta.TabIndex = 1
		'
		'deDesde
		'
		Me.deDesde.EditValue = Nothing
		Me.deDesde.Location = New System.Drawing.Point(106, 35)
		Me.deDesde.Name = "deDesde"
		Me.deDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.deDesde.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.deDesde.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
		Me.deDesde.Size = New System.Drawing.Size(100, 20)
		Me.deDesde.TabIndex = 0
		'
		'LabelControl2
		'
		Me.LabelControl2.Location = New System.Drawing.Point(39, 61)
		Me.LabelControl2.Name = "LabelControl2"
		Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
		Me.LabelControl2.TabIndex = 15
		Me.LabelControl2.Text = "Hasta Fecha:"
		'
		'LabelControl1
		'
		Me.LabelControl1.Location = New System.Drawing.Point(37, 35)
		Me.LabelControl1.Name = "LabelControl1"
		Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
		Me.LabelControl1.TabIndex = 13
		Me.LabelControl1.Text = "Desde Fecha:"
		'
		'leVendedor
		'
		Me.leVendedor.EnterMoveNextControl = True
		Me.leVendedor.Location = New System.Drawing.Point(106, 129)
		Me.leVendedor.Name = "leVendedor"
		Me.leVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
		Me.leVendedor.Size = New System.Drawing.Size(335, 20)
		Me.leVendedor.TabIndex = 4
		'
		'LabelControl3
		'
		Me.LabelControl3.Location = New System.Drawing.Point(52, 132)
		Me.LabelControl3.Name = "LabelControl3"
		Me.LabelControl3.Size = New System.Drawing.Size(50, 13)
		Me.LabelControl3.TabIndex = 29
		Me.LabelControl3.Text = "Vendedor:"
		'
		'LabelControl4
		'
		Me.LabelControl4.Location = New System.Drawing.Point(212, 87)
		Me.LabelControl4.Name = "LabelControl4"
		Me.LabelControl4.Size = New System.Drawing.Size(169, 13)
		Me.LabelControl4.TabIndex = 30
		Me.LabelControl4.Text = "<-- Cero para todos los prospectos"
		'
		'LabelControl9
		'
		Me.LabelControl9.Location = New System.Drawing.Point(64, 110)
		Me.LabelControl9.Name = "LabelControl9"
		Me.LabelControl9.Size = New System.Drawing.Size(41, 13)
		Me.LabelControl9.TabIndex = 128
		Me.LabelControl9.Text = "Nombre:"
		'
		'teNombreProspecto
		'
		Me.teNombreProspecto.EnterMoveNextControl = True
		Me.teNombreProspecto.Location = New System.Drawing.Point(106, 106)
		Me.teNombreProspecto.Name = "teNombreProspecto"
		Me.teNombreProspecto.Properties.ReadOnly = True
		Me.teNombreProspecto.Size = New System.Drawing.Size(335, 20)
		Me.teNombreProspecto.TabIndex = 127
		'
		'LabelControl10
		'
		Me.LabelControl10.Location = New System.Drawing.Point(24, 85)
		Me.LabelControl10.Name = "LabelControl10"
		Me.LabelControl10.Size = New System.Drawing.Size(78, 13)
		Me.LabelControl10.TabIndex = 129
		Me.LabelControl10.Text = "Cód. Prospecto:"
		'
		'beCodProspecto
		'
		Me.beCodProspecto.EnterMoveNextControl = True
		Me.beCodProspecto.Location = New System.Drawing.Point(106, 83)
		Me.beCodProspecto.Name = "beCodProspecto"
		Me.beCodProspecto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
		Me.beCodProspecto.Properties.Mask.EditMask = "n0"
		Me.beCodProspecto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
		Me.beCodProspecto.Size = New System.Drawing.Size(100, 20)
		Me.beCodProspecto.TabIndex = 126
		'
		'chkGestiones
		'
		Me.chkGestiones.Location = New System.Drawing.Point(106, 180)
		Me.chkGestiones.Name = "chkGestiones"
		Me.chkGestiones.Properties.Caption = "Reporte de Gestiones?"
		Me.chkGestiones.Size = New System.Drawing.Size(150, 19)
		Me.chkGestiones.TabIndex = 130
		'
		'fac_frmListadoGestionesProspectos
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.ClientSize = New System.Drawing.Size(881, 367)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Modulo = "Facturación"
		Me.Name = "fac_frmListadoGestionesProspectos"
		Me.Text = "Informe de Gestiones - Prospectos de Clientes"
		CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupControl1.ResumeLayout(False)
		Me.GroupControl1.PerformLayout()
		CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.teNombreProspecto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.beCodProspecto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkGestiones.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents deDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents leVendedor As DevExpress.XtraEditors.LookUpEdit
	Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents teNombreProspecto As DevExpress.XtraEditors.TextEdit
	Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
	Friend WithEvents beCodProspecto As DevExpress.XtraEditors.ButtonEdit
	Friend WithEvents chkGestiones As DevExpress.XtraEditors.CheckEdit
End Class
