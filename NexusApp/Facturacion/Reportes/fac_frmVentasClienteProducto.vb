﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class fac_frmVentasClienteProducto
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim dt As New DataTable
    
    Private Sub frmVentasCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        leSucursal.EditValue = piIdSucursal
        objCombos.fac_PuntosVenta(lePuntoVenta, 1, "-- TODOS --")
        deDesde.EditValue = CDate("1/" & Month(Today) & "/" & Year(Today))
        deHasta.EditValue = Today
    End Sub
    Private Sub sbAceptar_Click() Handles Me.Reporte

        dt = bl.fac_VentasClienteProducto _
                        (deDesde.EditValue, deHasta.EditValue, _
                         BeCliente1.beCodigo.EditValue, BeProducto1.beCodigo.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)
        Dim rpt As New fac_rptVentasClienteProducto() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, SUCURSAL: {1}, PUNTO DE VENTA: {2}", teTitulo.Text, leSucursal.Text, lePuntoVenta.Text)
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt.ShowPreviewDialog()
    End Sub


    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub
    
End Class
