﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class fac_rptListadoProspectos
	Inherits DevExpress.XtraReports.UI.XtraReport

	'XtraReport overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing AndAlso components IsNot Nothing Then
			components.Dispose()
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Designer
	'It can be modified using the Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
		Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
		Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
		Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
		Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.xrlEmpresa = New DevExpress.XtraReports.UI.XRLabel()
		Me.xrlTitulo = New DevExpress.XtraReports.UI.XRLabel()
		Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
		Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
		Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
		Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
		Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.XrTableCell11 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.XrTableCell12 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.XrTableCell13 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
		Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
		Me.xrlPeriodo = New DevExpress.XtraReports.UI.XRLabel()
		Me.DsCPC1 = New Nexus.dsCPC()
		Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
		Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
		Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
		Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
		CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.DsCPC1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
		'
		'TopMarginBand1
		'
		Me.TopMarginBand1.Dpi = 100.0!
		Me.TopMarginBand1.HeightF = 35.0!
		Me.TopMarginBand1.Name = "TopMarginBand1"
		'
		'BottomMarginBand1
		'
		Me.BottomMarginBand1.Dpi = 100.0!
		Me.BottomMarginBand1.HeightF = 35.0!
		Me.BottomMarginBand1.Name = "BottomMarginBand1"
		'
		'XrTable1
		'
		Me.XrTable1.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
			Or DevExpress.XtraPrinting.BorderSide.Right) _
			Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
		Me.XrTable1.Dpi = 100.0!
		Me.XrTable1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold)
		Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(1.291748!, 75.66666!)
		Me.XrTable1.Name = "XrTable1"
		Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
		Me.XrTable1.SizeF = New System.Drawing.SizeF(1027.417!, 18.0!)
		Me.XrTable1.StylePriority.UseBorders = False
		Me.XrTable1.StylePriority.UseFont = False
		Me.XrTable1.StylePriority.UseTextAlignment = False
		Me.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'XrTableRow1
		'
		Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell3, Me.XrTableCell4, Me.XrTableCell5, Me.XrTableCell6, Me.XrTableCell8})
		Me.XrTableRow1.Dpi = 100.0!
		Me.XrTableRow1.Name = "XrTableRow1"
		Me.XrTableRow1.Weight = 1.0R
		'
		'XrTableCell1
		'
		Me.XrTableCell1.Dpi = 100.0!
		Me.XrTableCell1.Name = "XrTableCell1"
		Me.XrTableCell1.Text = "Correl"
		Me.XrTableCell1.Weight = 0.5100000747824921R
		'
		'XrTableCell2
		'
		Me.XrTableCell2.Dpi = 100.0!
		Me.XrTableCell2.Name = "XrTableCell2"
		Me.XrTableCell2.Text = "Nombre"
		Me.XrTableCell2.Weight = 2.7299997004789258R
		'
		'XrTableCell3
		'
		Me.XrTableCell3.Dpi = 100.0!
		Me.XrTableCell3.Name = "XrTableCell3"
		Me.XrTableCell3.Text = "DUI"
		Me.XrTableCell3.Weight = 1.0174995610354316R
		'
		'XrTableCell4
		'
		Me.XrTableCell4.Dpi = 100.0!
		Me.XrTableCell4.Name = "XrTableCell4"
		Me.XrTableCell4.Text = "NIT"
		Me.XrTableCell4.Weight = 1.0308330377563395R
		'
		'XrTableCell5
		'
		Me.XrTableCell5.Dpi = 100.0!
		Me.XrTableCell5.Name = "XrTableCell5"
		Me.XrTableCell5.Text = "Teléfono"
		Me.XrTableCell5.Weight = 1.1116670980457106R
		'
		'XrTableCell6
		'
		Me.XrTableCell6.Dpi = 100.0!
		Me.XrTableCell6.Name = "XrTableCell6"
		Me.XrTableCell6.Text = "Dirección"
		Me.XrTableCell6.Weight = 2.9600005872087314R
		'
		'XrTableCell8
		'
		Me.XrTableCell8.Dpi = 100.0!
		Me.XrTableCell8.Name = "XrTableCell8"
		Me.XrTableCell8.Text = "Fecha Contacto"
		Me.XrTableCell8.Weight = 0.91416680084635371R
		'
		'xrlEmpresa
		'
		Me.xrlEmpresa.Dpi = 100.0!
		Me.xrlEmpresa.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
		Me.xrlEmpresa.LocationFloat = New DevExpress.Utils.PointFloat(0!, 10.0!)
		Me.xrlEmpresa.Name = "xrlEmpresa"
		Me.xrlEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrlEmpresa.SizeF = New System.Drawing.SizeF(621.0!, 20.0!)
		Me.xrlEmpresa.StylePriority.UseFont = False
		Me.xrlEmpresa.StylePriority.UseTextAlignment = False
		Me.xrlEmpresa.Text = "xrlEmpresa"
		Me.xrlEmpresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		'
		'xrlTitulo
		'
		Me.xrlTitulo.Dpi = 100.0!
		Me.xrlTitulo.Font = New System.Drawing.Font("Arial", 11.0!)
		Me.xrlTitulo.LocationFloat = New DevExpress.Utils.PointFloat(0!, 31.0!)
		Me.xrlTitulo.Name = "xrlTitulo"
		Me.xrlTitulo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrlTitulo.SizeF = New System.Drawing.SizeF(621.0!, 16.0!)
		Me.xrlTitulo.StylePriority.UseFont = False
		Me.xrlTitulo.StylePriority.UseTextAlignment = False
		Me.xrlTitulo.Text = "xrlTitulo"
		Me.xrlTitulo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		'
		'PageFooter
		'
		Me.PageFooter.Dpi = 100.0!
		Me.PageFooter.HeightF = 30.0!
		Me.PageFooter.Name = "PageFooter"
		Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
		Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		'
		'Detail
		'
		Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2})
		Me.Detail.Dpi = 100.0!
		Me.Detail.HeightF = 23.29165!
		Me.Detail.Name = "Detail"
		Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
		Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		'
		'XrTable2
		'
		Me.XrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None
		Me.XrTable2.Dpi = 100.0!
		Me.XrTable2.Font = New System.Drawing.Font("Arial", 8.25!)
		Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(1.291748!, 2.083324!)
		Me.XrTable2.Name = "XrTable2"
		Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
		Me.XrTable2.SizeF = New System.Drawing.SizeF(1027.417!, 18.0!)
		Me.XrTable2.StylePriority.UseBorders = False
		Me.XrTable2.StylePriority.UseFont = False
		Me.XrTable2.StylePriority.UseTextAlignment = False
		Me.XrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
		'
		'XrTableRow2
		'
		Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell9, Me.XrTableCell10, Me.XrTableCell11, Me.XrTableCell12, Me.XrTableCell13, Me.XrTableCell14})
		Me.XrTableRow2.Dpi = 100.0!
		Me.XrTableRow2.Name = "XrTableRow2"
		Me.XrTableRow2.Weight = 1.0R
		'
		'XrTableCell7
		'
		Me.XrTableCell7.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Clientes.IdComprobante")})
		Me.XrTableCell7.Dpi = 100.0!
		Me.XrTableCell7.Name = "XrTableCell7"
		Me.XrTableCell7.Weight = 0.5100000747824921R
		'
		'XrTableCell9
		'
		Me.XrTableCell9.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Clientes.Nombre")})
		Me.XrTableCell9.Dpi = 100.0!
		Me.XrTableCell9.Name = "XrTableCell9"
		Me.XrTableCell9.StylePriority.UseTextAlignment = False
		Me.XrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		Me.XrTableCell9.Weight = 2.7299997004789258R
		'
		'XrTableCell10
		'
		Me.XrTableCell10.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Clientes.Dui")})
		Me.XrTableCell10.Dpi = 100.0!
		Me.XrTableCell10.Name = "XrTableCell10"
		Me.XrTableCell10.StylePriority.UseTextAlignment = False
		Me.XrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		Me.XrTableCell10.Weight = 1.0174995610354316R
		'
		'XrTableCell11
		'
		Me.XrTableCell11.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Clientes.Nit")})
		Me.XrTableCell11.Dpi = 100.0!
		Me.XrTableCell11.Name = "XrTableCell11"
		Me.XrTableCell11.StylePriority.UseTextAlignment = False
		Me.XrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		Me.XrTableCell11.Weight = 1.0308330377563395R
		'
		'XrTableCell12
		'
		Me.XrTableCell12.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Clientes.Telefonos")})
		Me.XrTableCell12.Dpi = 100.0!
		Me.XrTableCell12.Name = "XrTableCell12"
		Me.XrTableCell12.StylePriority.UseTextAlignment = False
		Me.XrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		Me.XrTableCell12.Weight = 1.1116670980457106R
		'
		'XrTableCell13
		'
		Me.XrTableCell13.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Clientes.Direccion")})
		Me.XrTableCell13.Dpi = 100.0!
		Me.XrTableCell13.Name = "XrTableCell13"
		Me.XrTableCell13.StylePriority.UseTextAlignment = False
		Me.XrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		Me.XrTableCell13.Weight = 2.9600005872087314R
		'
		'XrTableCell14
		'
		Me.XrTableCell14.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Clientes.FechaContacto", "{0:dd/MM/yyyy}")})
		Me.XrTableCell14.Dpi = 100.0!
		Me.XrTableCell14.Name = "XrTableCell14"
		Me.XrTableCell14.Weight = 0.91416680084635371R
		'
		'PageHeader
		'
		Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlPeriodo, Me.XrTable1, Me.xrlTitulo, Me.xrlEmpresa})
		Me.PageHeader.Dpi = 100.0!
		Me.PageHeader.HeightF = 94.04166!
		Me.PageHeader.Name = "PageHeader"
		Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
		Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		'
		'xrlPeriodo
		'
		Me.xrlPeriodo.Dpi = 100.0!
		Me.xrlPeriodo.Font = New System.Drawing.Font("Arial", 11.0!)
		Me.xrlPeriodo.LocationFloat = New DevExpress.Utils.PointFloat(0!, 49.43749!)
		Me.xrlPeriodo.Name = "xrlPeriodo"
		Me.xrlPeriodo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.xrlPeriodo.SizeF = New System.Drawing.SizeF(621.0!, 16.0!)
		Me.xrlPeriodo.StylePriority.UseFont = False
		Me.xrlPeriodo.StylePriority.UseTextAlignment = False
		Me.xrlPeriodo.Text = "xrlPeriodo"
		Me.xrlPeriodo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
		'
		'DsCPC1
		'
		Me.DsCPC1.DataSetName = "dsCPC"
		Me.DsCPC1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'GroupHeader1
		'
		Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9})
		Me.GroupHeader1.Dpi = 100.0!
		Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("IdVendedor", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
		Me.GroupHeader1.HeightF = 18.75!
		Me.GroupHeader1.Name = "GroupHeader1"
		'
		'XrLabel9
		'
		Me.XrLabel9.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Clientes.Vendedor")})
		Me.XrLabel9.Dpi = 100.0!
		Me.XrLabel9.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(5.0!, 0!)
		Me.XrLabel9.Name = "XrLabel9"
		Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
		Me.XrLabel9.SizeF = New System.Drawing.SizeF(510.9999!, 16.0!)
		Me.XrLabel9.StylePriority.UseFont = False
		'
		'GroupFooter1
		'
		Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1})
		Me.GroupFooter1.Dpi = 100.0!
		Me.GroupFooter1.HeightF = 8.333333!
		Me.GroupFooter1.Name = "GroupFooter1"
		'
		'XrLine1
		'
		Me.XrLine1.Dpi = 100.0!
		Me.XrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dash
		Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(2.999997!, 3.0!)
		Me.XrLine1.Name = "XrLine1"
		Me.XrLine1.SizeF = New System.Drawing.SizeF(1025.0!, 2.0!)
		'
		'fac_rptListadoProspectos
		'
		Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1})
		Me.DataMember = "Clientes"
		Me.DataSource = Me.DsCPC1
		Me.DrawGrid = False
		Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Landscape = True
		Me.Margins = New System.Drawing.Printing.Margins(35, 35, 35, 35)
		Me.PageHeight = 850
		Me.PageWidth = 1100
		Me.SnapGridSize = 4.166667!
		Me.SnappingMode = DevExpress.XtraReports.UI.SnappingMode.None
		Me.Version = "16.2"
		CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.DsCPC1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

	End Sub
	Friend WithEvents TopMarginBand1 As TopMarginBand
	Friend WithEvents BottomMarginBand1 As BottomMarginBand
	Friend WithEvents XrTable1 As XRTable
	Friend WithEvents XrTableRow1 As XRTableRow
	Friend WithEvents XrTableCell1 As XRTableCell
	Friend WithEvents XrTableCell2 As XRTableCell
	Friend WithEvents XrTableCell3 As XRTableCell
	Friend WithEvents XrTableCell4 As XRTableCell
	Friend WithEvents XrTableCell5 As XRTableCell
	Friend WithEvents XrTableCell6 As XRTableCell
	Friend WithEvents XrTableCell8 As XRTableCell
	Friend WithEvents xrlEmpresa As XRLabel
	Friend WithEvents xrlTitulo As XRLabel
	Friend WithEvents PageFooter As PageFooterBand
	Friend WithEvents Detail As DetailBand
	Friend WithEvents PageHeader As PageHeaderBand
	Friend WithEvents XrTable2 As XRTable
	Friend WithEvents XrTableRow2 As XRTableRow
	Friend WithEvents XrTableCell7 As XRTableCell
	Friend WithEvents XrTableCell9 As XRTableCell
	Friend WithEvents XrTableCell10 As XRTableCell
	Friend WithEvents XrTableCell11 As XRTableCell
	Friend WithEvents XrTableCell12 As XRTableCell
	Friend WithEvents XrTableCell13 As XRTableCell
	Friend WithEvents XrTableCell14 As XRTableCell
	Friend WithEvents DsCPC1 As dsCPC
	Friend WithEvents GroupHeader1 As GroupHeaderBand
	Friend WithEvents XrLabel9 As XRLabel
	Friend WithEvents GroupFooter1 As GroupFooterBand
	Friend WithEvents XrLine1 As XRLine
	Friend WithEvents xrlPeriodo As XRLabel
End Class
