﻿Imports NexusBLL

Public Class fac_frmReportesLiquidacion
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim dt As New DataTable
    Private Sub fac_frmReportesLiquidacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.fac_Rutas(leRuta, "-- TODAS LAS RUTAS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS --")
        objCombos.fac_PuntosVenta(lePuntoVenta, 1, "-- TODOS --")
        deDesde.EditValue = CDate("01/" & Month(Today) & "/" & Year(Today))
        deHasta.EditValue = Today
    End Sub
    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub

    Private Sub fac_frmReportesLiquidacion_Reporte() Handles Me.Reporte
        Dim dt As DataTable = bl.fac_ProductoDespachado(leRuta.EditValue, deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)
        If rgTipo.EditValue = 1 Then
            Dim rpt As New fac_rptProductoDespachado() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = String.Format("{0}, SUCURSAL: {1}, PUNTO DE VENTA: {2}, RUTA: {2}", teTitulo.Text, leSucursal.Text, lePuntoVenta.Text, leRuta.Text)
            rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
            rpt.ShowPreviewDialog()
        Else
            Dim rpt As New fac_rptLiquidacionEntregas() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = String.Format("{0}, SUCURSAL: {1}, PUNTO DE VENTA: {2}, RUTA: {3}", teTitulo.Text, leSucursal.Text, lePuntoVenta.Text, leRuta.Text)
            rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
            rpt.ShowPreviewDialog()
        End If
        
    End Sub

    Private Sub rgTipo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgTipo.EditValueChanged
        If rgTipo.EditValue = 1 Then
            teTitulo.EditValue = "DETALLE DE PRODUCTO DESPACHADO"
        Else
            teTitulo.EditValue = "INFORME DE LIQUIDACION DE PRODUCTOS"
        End If
    End Sub
End Class
