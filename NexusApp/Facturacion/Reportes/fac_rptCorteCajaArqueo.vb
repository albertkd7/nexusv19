Public Class fac_rptCorteCajaArqueo

    Private Sub XrTableCell11_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrTableCell11.BeforePrint
        If SiEsNulo(GetCurrentColumnValue("Cantidad"), 0.0) = 0.0 Then
            XrTableCell11.Text = ""
        End If
    End Sub

    Private Sub XrTableCell4_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrTableCell4.BeforePrint
        If SiEsNulo(GetCurrentColumnValue("Total"), 0.0) = 0.0 Then
            XrTableCell4.Text = ""
        End If
    End Sub
End Class