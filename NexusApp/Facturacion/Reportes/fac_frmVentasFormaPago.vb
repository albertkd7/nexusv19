﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmVentasFormaPago
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim Periodo As String

    Dim dt As New DataTable


    Private Sub frmFacReportes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ''Dim usu As New adm_Usuarios
        ''  usu = objTablas.adm_UsuariosSelectByPK(objMenu.User)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        leSucursal.EditValue = piIdSucursal
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "")
        objCombos.fac_FormasPago(leFormaPago, "-- TODAS --")
        deDesde.EditValue = CDate("1/" & Month(Today) & "/" & Year(Today))
        objCombos.fac_Vendedores(leVendedor, "-- TODOS LOS VENDEDORES --")
        ''lePuntoVenta.EditValue = usu.id
        deHasta.EditValue = Today
    End Sub
    
    Private Sub SimpleButton1_Click() Handles Me.Reporte
        dt = bl.fac_VentasFormaPago(deDesde.EditValue, deHasta.EditValue, leFormaPago.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue, leVendedor.EditValue)
        Dim rpt As New fac_rptVentasFormaPago() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, SUCURSAL: {1}, PUNTO DE VENTA: {2}", teTitulo.Text, leSucursal.Text, lePuntoVenta.Text)
        rpt.xrlVendedor.Text = "Vendedor: " & leVendedor.Text
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue).ToUpper()
        rpt.ShowPreviewDialog()
    End Sub


    Private Sub leSucursal_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub
End Class
