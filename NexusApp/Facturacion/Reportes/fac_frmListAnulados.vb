﻿Imports NexusBLL

Imports NexusELL.TableEntities
Public Class fac_frmListAnulados
    Dim bl As New FacturaBLL(g_ConnectionString)

    Private Sub fac_frmListAnulados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        deDesde.EditValue = CDate("1/" & Month(Today) & "/" & Year(Today))
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
        objCombos.fac_TiposComprobante(leTipoComprobante, "-- TODOS --")
    End Sub

    Private Sub fac_frmListAnulados_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.fac_DocumentosAnulados(deDesde.EditValue, deHasta.EditValue, leTipoComprobante.EditValue, leSucursal.EditValue, lePuntoVenta.EditValue)

        Dim rpt As New fac_rptDocumentosAnulados() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, PUNTO DE VENTA: {1}", teTitulo.EditValue, lePuntoVenta.Text)
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt.ShowPreviewDialog()
    End Sub


End Class
