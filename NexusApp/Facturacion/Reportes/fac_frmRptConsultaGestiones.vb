﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmRptConsultaGestiones
    Dim bl As New AdmonBLL(g_ConnectionString)
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)


    Private Sub frmFacReportes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.fac_Vendedores(leVendedor, "-- TODOS --")

        If (EntUsuario.IdVendedor > 0) Then
            leVendedor.EditValue = EntUsuario.IdVendedor
            'leVendedor.Enabled = False
        End If
    End Sub

    Private Sub sbAceptar_Click() Handles Me.Reporte

        Dim dtGestiones As DataTable = bl.GestionesProgramadas(deDesde.EditValue, deHasta.EditValue, leVendedor.EditValue)

        Dim rpt As New fac_rptGestionProgramada
        rpt.DataSource = dtGestiones
        rpt.DataMember = ""
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt.ShowPreviewDialog()

    End Sub
End Class
