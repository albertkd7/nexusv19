﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmVentasDevoluciones
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim Periodo As String

    Dim dt As New DataTable


    Private Sub frmFacReportes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        leSucursal.EditValue = piIdSucursal
        objCombos.fac_PuntosVenta(lePuntoVenta, 1, "-- TODOS --")
        deDesde.EditValue = CDate("1/" & Month(Today) & "/" & Year(Today))
        deHasta.EditValue = Today
    End Sub

    Private Sub SimpleButton1_Click() Handles Me.Reporte

        dt = bl.fac_VentasDevoluciones(deDesde.EditValue, deHasta.EditValue, lePuntoVenta.EditValue)

        Dim rpt As New fac_rptVentasFormaPago() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = String.Format("{0}, PUNTO DE VENTA: {1}", teTitulo.Text, lePuntoVenta.Text)
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue).ToUpper()
        rpt.ShowPreviewDialog()
    End Sub


    Private Sub leSucursal_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePuntoVenta, leSucursal.EditValue, "-- TODOS --")
    End Sub
End Class
