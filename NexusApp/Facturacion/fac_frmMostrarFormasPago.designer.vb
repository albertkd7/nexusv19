<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmMostrarFormasPago
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmMostrarFormasPago))
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.sbCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.gcFP = New DevExpress.XtraGrid.GridControl()
        Me.gvFP = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcForma = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leFormaPago = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteMonto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.BtRegresar = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teCambio = New DevExpress.XtraEditors.TextEdit()
        Me.teEfectivo = New DevExpress.XtraEditors.TextEdit()
        Me.teTotalFactura = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.gcFP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvFP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leFormaPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteMonto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCambio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teEfectivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotalFactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(434, 218)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(95, 33)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Imprimir"
        '
        'sbCancel
        '
        Me.sbCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.sbCancel.Appearance.Options.UseFont = True
        Me.sbCancel.Location = New System.Drawing.Point(578, 218)
        Me.sbCancel.Name = "sbCancel"
        Me.sbCancel.Size = New System.Drawing.Size(95, 33)
        Me.sbCancel.TabIndex = 2
        Me.sbCancel.Text = "Continuar"
        '
        'gcFP
        '
        Me.gcFP.Location = New System.Drawing.Point(1, 4)
        Me.gcFP.MainView = Me.gvFP
        Me.gcFP.Name = "gcFP"
        Me.gcFP.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteMonto, Me.leFormaPago})
        Me.gcFP.Size = New System.Drawing.Size(402, 252)
        Me.gcFP.TabIndex = 6
        Me.gcFP.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvFP})
        '
        'gvFP
        '
        Me.gvFP.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcForma, Me.gcTotal})
        Me.gvFP.GridControl = Me.gcFP
        Me.gvFP.Name = "gvFP"
        Me.gvFP.OptionsView.ShowFooter = True
        Me.gvFP.OptionsView.ShowGroupPanel = False
        '
        'gcForma
        '
        Me.gcForma.Caption = "Forma de Pago"
        Me.gcForma.ColumnEdit = Me.leFormaPago
        Me.gcForma.FieldName = "IdFormaPago"
        Me.gcForma.Name = "gcForma"
        Me.gcForma.OptionsColumn.AllowEdit = False
        Me.gcForma.OptionsColumn.AllowFocus = False
        Me.gcForma.OptionsColumn.AllowMove = False
        Me.gcForma.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "IdFormaPago", "TOTAL PAGO:")})
        Me.gcForma.Visible = True
        Me.gcForma.VisibleIndex = 0
        Me.gcForma.Width = 388
        '
        'leFormaPago
        '
        Me.leFormaPago.AutoHeight = False
        Me.leFormaPago.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leFormaPago.Name = "leFormaPago"
        '
        'gcTotal
        '
        Me.gcTotal.Caption = "Total "
        Me.gcTotal.DisplayFormat.FormatString = "{0:c}"
        Me.gcTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcTotal.FieldName = "Total"
        Me.gcTotal.Name = "gcTotal"
        Me.gcTotal.OptionsColumn.AllowMove = False
        Me.gcTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", "{0:c}")})
        Me.gcTotal.Visible = True
        Me.gcTotal.VisibleIndex = 1
        Me.gcTotal.Width = 143
        '
        'riteMonto
        '
        Me.riteMonto.AutoHeight = False
        Me.riteMonto.Mask.EditMask = "n2"
        Me.riteMonto.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteMonto.Mask.UseMaskAsDisplayFormat = True
        Me.riteMonto.Name = "riteMonto"
        '
        'BtRegresar
        '
        Me.BtRegresar.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.BtRegresar.Appearance.Options.UseFont = True
        Me.BtRegresar.Image = Global.Nexus.My.Resources.Resources.Undo32x32
        Me.BtRegresar.Location = New System.Drawing.Point(627, 4)
        Me.BtRegresar.Name = "BtRegresar"
        Me.BtRegresar.Size = New System.Drawing.Size(47, 33)
        Me.BtRegresar.TabIndex = 15
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(493, 164)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(65, 20)
        Me.LabelControl3.TabIndex = 18
        Me.LabelControl3.Text = "Cambio:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(413, 123)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(145, 20)
        Me.LabelControl2.TabIndex = 19
        Me.LabelControl2.Text = "Efectivo Recibido:"
        '
        'teCambio
        '
        Me.teCambio.EditValue = 0
        Me.teCambio.Enabled = False
        Me.teCambio.EnterMoveNextControl = True
        Me.teCambio.Location = New System.Drawing.Point(564, 161)
        Me.teCambio.Name = "teCambio"
        Me.teCambio.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.teCambio.Properties.Appearance.Options.UseFont = True
        Me.teCambio.Properties.Appearance.Options.UseTextOptions = True
        Me.teCambio.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teCambio.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.teCambio.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.teCambio.Properties.Mask.EditMask = "n2"
        Me.teCambio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teCambio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teCambio.Properties.ReadOnly = True
        Me.teCambio.Size = New System.Drawing.Size(109, 26)
        Me.teCambio.TabIndex = 20
        '
        'teEfectivo
        '
        Me.teEfectivo.EditValue = 0
        Me.teEfectivo.EnterMoveNextControl = True
        Me.teEfectivo.Location = New System.Drawing.Point(564, 120)
        Me.teEfectivo.Name = "teEfectivo"
        Me.teEfectivo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.teEfectivo.Properties.Appearance.Options.UseFont = True
        Me.teEfectivo.Properties.Appearance.Options.UseTextOptions = True
        Me.teEfectivo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teEfectivo.Properties.Mask.EditMask = "n2"
        Me.teEfectivo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teEfectivo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teEfectivo.Size = New System.Drawing.Size(109, 26)
        Me.teEfectivo.TabIndex = 16
        '
        'teTotalFactura
        '
        Me.teTotalFactura.EditValue = 0
        Me.teTotalFactura.Enabled = False
        Me.teTotalFactura.EnterMoveNextControl = True
        Me.teTotalFactura.Location = New System.Drawing.Point(564, 88)
        Me.teTotalFactura.Name = "teTotalFactura"
        Me.teTotalFactura.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotalFactura.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotalFactura.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.teTotalFactura.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.teTotalFactura.Properties.Mask.EditMask = "n2"
        Me.teTotalFactura.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotalFactura.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTotalFactura.Properties.ReadOnly = True
        Me.teTotalFactura.Size = New System.Drawing.Size(109, 20)
        Me.teTotalFactura.TabIndex = 21
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(416, 88)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(142, 20)
        Me.LabelControl1.TabIndex = 17
        Me.LabelControl1.Text = "Total Documento:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(413, 23)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(196, 26)
        Me.LabelControl4.TabIndex = 17
        Me.LabelControl4.Text = "*La suma total de las formas de pago " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "debe ser igua al valor final del documento" &
    ""
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(409, 7)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(183, 13)
        Me.LabelControl5.TabIndex = 17
        Me.LabelControl5.Text = "<-------------------------------------------------- nota:"
        '
        'fac_frmMostrarFormasPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(688, 258)
        Me.ControlBox = False
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.teCambio)
        Me.Controls.Add(Me.teEfectivo)
        Me.Controls.Add(Me.teTotalFactura)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.BtRegresar)
        Me.Controls.Add(Me.gcFP)
        Me.Controls.Add(Me.sbCancel)
        Me.Controls.Add(Me.SimpleButton1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fac_frmMostrarFormasPago"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Forma de Pago"
        CType(Me.gcFP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvFP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leFormaPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteMonto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCambio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teEfectivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotalFactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcFP As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvFP As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcForma As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteMonto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents leFormaPago As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents BtRegresar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCambio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teEfectivo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teTotalFactura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
End Class
