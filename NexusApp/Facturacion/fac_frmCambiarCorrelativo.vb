﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class fac_frmCambiarCorrelativo
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim entFactura As New fac_Ventas
    Dim FacturaDetallePago As List(Of fac_VentasDetallePago)
    Dim CreaDocumento As Integer = 0

    Private Sub fac_frmAnularDocumentos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePunto, leSucursal.EditValue, "")
        objCombos.fac_Vendedores(leVendedor, "")
        leSucursal.EditValue = piIdSucursal
        objCombos.fac_TiposComprobante(leTipo, "")
        objCombos.fac_FormasPago(leFormaPago, "")
        sbCambia.Enabled = False
    End Sub

    Private Sub sbObtener_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbObtener.Click
        CreaDocumento = 0
        Dim IdComprobante As Integer = bl.getIdComprobante(leSucursal.EditValue, lePunto.EditValue, leTipo.EditValue, teSerie.EditValue, teNumero.EditValue)
        If IdComprobante = 0 Then
            MsgBox("No se ha emitido ningún documento con ese número", 64, "Nota")
        End If

        entFactura = objTablas.fac_VentasSelectByPK(IdComprobante)
        gcFP.DataSource = bl.fac_ObtenerFormasPagoDetalle(entFactura.IdComprobante)

        deFecha.EditValue = entFactura.Fecha
        teNombre.EditValue = entFactura.Nombre
        seTotal.EditValue = entFactura.TotalComprobante

        'Datos Nuevos
        teNuevoNumero.EditValue = entFactura.Numero
        DeFechaNueva.EditValue = entFactura.Fecha
        leVendedor.EditValue = entFactura.IdVendedor

        sbCambia.Enabled = True
    End Sub

    Private Sub sbCambia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCambia.Click
        sbCambia.Enabled = False

        Dim msj As String = ""
        'entFactura.Fecha = DeFechaNueva.EditValue
        'entFactura.Numero = teNuevoNumero.EditValue
        entFactura.IdVendedor = leVendedor.EditValue
        entFactura.TotalPagado = gcTotal.SummaryItem.SummaryValue

        Try
            LlenarFormaPago()
            'objTablas.fac_VentasUpdate(entFactura)
            MsgBox("El documento ha sido actualizado", MsgBoxStyle.Information, "Nota")
        Catch ex As Exception
            msj = ex.Message()
            MsgBox("No fue posible actualizar el documento" + Chr(13) + msj, 16, "Verifique el error")
            Exit Sub
        End Try

        teNumero.EditValue = ""
        teNombre.EditValue = ""
        seTotal.EditValue = 0.0
        teNuevoNumero.EditValue = ""
        gcFP.DataSource = bl.fac_ObtenerFormasPagoDetalle(-1)

    End Sub

    Private Sub LlenarFormaPago()
        FacturaDetallePago = New List(Of fac_VentasDetallePago)
        objTablas.fac_VentasDetallePagoDeleteByPK(1, entFactura.IdComprobante)
        For i = 0 To gvFP.DataRowCount - 1
            If gvFP.GetRowCellValue(i, "Total") <> 0.0 Then
                Dim entDetalle As New fac_VentasDetallePago
                With entDetalle
                    .IdComprobVenta = entFactura.IdComprobante
                    .IdFormaPago = gvFP.GetRowCellValue(i, "IdFormaPago") ' _dtDetallePago.Rows(i).Item("IdFormaPago")
                    .Total = SiEsNulo(gvFP.GetRowCellValue(i, "Total"), 0) 'SiEsNulo(_dtDetallePago.Rows(i).Item("Total"), 0)
                End With

                objTablas.fac_VentasDetallePagoInsert(entDetalle)
            End If
        Next
    End Sub
    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePunto, leSucursal.EditValue)
        CargaSerie()
    End Sub
    Private Sub CargaSerie()
        teSerie.EditValue = bl.fac_ObtenerSerieDocumento(leSucursal.EditValue, lePunto.EditValue, leTipo.EditValue)
    End Sub

    Private Sub lePunto_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lePunto.EditValueChanged
        CargaSerie()
    End Sub

    Private Sub leTipo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leTipo.EditValueChanged
        CargaSerie()
    End Sub

End Class
