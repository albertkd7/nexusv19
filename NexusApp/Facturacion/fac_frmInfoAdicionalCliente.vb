﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class fac_frmInfoAdicionalCliente
    Dim dtDetalle As New DataTable
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim blFac As New FacturaBLL(g_ConnectionString)
    Dim entCentro As con_CentrosCosto

    Private _IdCliente As String
    Public Property IdCliente() As String
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As String)
            _IdCliente = value
        End Set
    End Property


    Private Sub fac_frmInfoAdicionalCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargaListas()
        Dim frmClie As fac_frmClientes = Me.Owner
        Dim dtInfo As DataTable = frmClie.dtAnexoCliente

        For i = 0 To dtInfo.Rows.Count - 1
            leProfesion.EditValue = dtInfo.Rows(i).Item("IdProfesion")
            leRubro.EditValue = dtInfo.Rows(i).Item("IdRubro")
            leActividad.EditValue = dtInfo.Rows(i).Item("IdActividad")
            teIngresosBrutos.EditValue = dtInfo.Rows(i).Item("IngresosBruto")
            chkApnfd.EditValue = dtInfo.Rows(i).Item("APNFD")
            teEstimadoCompras.EditValue = dtInfo.Rows(i).Item("MontoEstimadoCompras")
            meObservaciones.EditValue = dtInfo.Rows(i).Item("Comentario")
            chkPersonaJuridica.EditValue = dtInfo.Rows(i).Item("PersonaJuridica")

            teNombreRepLegal.EditValue = dtInfo.Rows(i).Item("RepresentanteLegal")
            leTipoDocumento.EditValue = dtInfo.Rows(i).Item("TipoDocumento")
            teNumDocRL.EditValue = dtInfo.Rows(i).Item("NumDocumentoRepLegal")
            teNITRL.EditValue = dtInfo.Rows(i).Item("NITRepLegal")
            leDepartamentoRL.EditValue = dtInfo.Rows(i).Item("IdDepartamentoRepLegal")
            leMunicipioRL.EditValue = dtInfo.Rows(i).Item("IdMunicipioRepLegal")
            leProfesionRL.EditValue = dtInfo.Rows(i).Item("IdProfesionRepLegal")
            teDireccionRL.EditValue = dtInfo.Rows(i).Item("DireccionRepLegal")
            teTelefonoMovilRL.EditValue = dtInfo.Rows(i).Item("TelMovilRepLegal")
            teTelefonoRL.EditValue = dtInfo.Rows(i).Item("TelefonosRepLegal")
            teCorreoElectronicoRL.EditValue = dtInfo.Rows(i).Item("CorreoElectronicoRepLegal")
        Next

    End Sub


    Private Sub sbSalir_Click(sender As Object, e As EventArgs) Handles sbSalir.Click
        Me.Close()
    End Sub

    Private Sub leRubro_EditValueChanged(sender As Object, e As EventArgs)
        objCombos.fac_Actividades(leActividad, String.Format("IdRubro={0}", leRubro.EditValue))
    End Sub

    Private Sub CargaListas()
        objCombos.fac_Profesiones(leProfesion)
        objCombos.fac_Profesiones(leProfesionRL)
        objCombos.fac_RubroActividades(leRubro)
        objCombos.fac_Actividades(leActividad, "")
        objCombos.fac_TiposDocumento(leTipoDocumento)

        objCombos.admDepartamentos(leDepartamentoRL)
        objCombos.admMunicipios(leMunicipioRL, leDepartamentoRL.EditValue)
    End Sub

    Private Sub leDepartamentoRL_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leDepartamentoRL.EditValueChanged
        objCombos.admMunicipios(leMunicipioRL, leDepartamentoRL.EditValue)
    End Sub

    Private Sub sbGuardarCambios_Click(sender As Object, e As EventArgs) Handles sbGuardarCambios.Click
        If MsgBox("¿Esta seguro(a) de guardar los cambios?", MsgBoxStyle.YesNo, "CONFIRMACION") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim frmCli As fac_frmClientes = Me.Owner
        Dim dt As DataTable = frmCli.dtAnexoCliente


        Dim filas As DataRow() = frmCli.dtAnexoCliente.Select("CreadoPor <> '' ")

        For Each filaActual As DataRow In filas
            filaActual.Delete()
        Next

        frmCli.dtAnexoCliente.AcceptChanges()


        Dim dr As DataRow = frmCli.dtAnexoCliente.NewRow()
        dr("IdCliente") = ""
        dr("IdProfesion") = leProfesion.EditValue
        dr("IdRubro") = leRubro.EditValue
        dr("IdActividad") = leActividad.EditValue
        dr("IngresosBruto") = teIngresosBrutos.EditValue
        dr("APNFD") = chkApnfd.EditValue
        dr("Comentario") = meObservaciones.EditValue
        dr("MontoEstimadoCompras") = teEstimadoCompras.EditValue

        dr("RepresentanteLegal") = teNombreRepLegal.EditValue
        dr("TipoDocumento") = leTipoDocumento.EditValue
        dr("NumDocumentoRepLegal") = teNumDocRL.EditValue
        dr("NITRepLegal") = teNITRL.EditValue
        dr("IdDepartamentoRepLegal") = leDepartamentoRL.EditValue
        dr("IdMunicipioRepLegal") = leMunicipioRL.EditValue
        dr("IdProfesionRepLegal") = leProfesionRL.EditValue
        dr("DireccionRepLegal") = teDireccionRL.EditValue
        dr("TelMovilRepLegal") = teTelefonoMovilRL.EditValue
        dr("TelefonosRepLegal") = teTelefonoRL.EditValue
        dr("CorreoElectronicoRepLegal") = teCorreoElectronicoRL.EditValue
        dr("PersonaJuridica") = chkPersonaJuridica.EditValue

        dr("CreadoPor") = objMenu.User
        dr("FechaHoraCreacion") = Now


        frmCli.dtAnexoCliente.Rows.Add(dr)
        Me.Close()


    End Sub
End Class
