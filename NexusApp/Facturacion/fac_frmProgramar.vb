﻿Imports DevExpress.XtraReports.UI
Imports NexusBLL
Imports DevExpress.Utils
Imports NexusELL.TableEntities
Imports DevExpress.XtraScheduler
Imports DevExpress.XtraScheduler.Drawing
Imports System.Drawing
Imports System.IO
Imports DevExpress.XtraScheduler.Services

Public Class fac_frmProgramar
    Dim blFac As New FacturaBLL(g_ConnectionString)
    Dim fd As New FuncionesBLL(g_ConnectionString)

    ' Define the variables to hold the default service and our custom service. 
    Private prevTimeRulerFormatStringService As ITimeRulerFormatStringService
    Private customTimeRulerFormatStringService As ITimeRulerFormatStringService
    Private Sub CRM_frmCitas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        'Me.Fac_VendedoresTableAdapter.FillBy(Me.TecniinoxDataSet.fac_Vendedores, piIdSucursalUsuario)

        ''TODO: This line of code loads data into the 'TecniinoxDataSet.AppointmentsInstalacion' table. You can move, or remove it, as needed.
        'Me.AppointmentsInstalacionTableAdapter.Fill(Me.TecniinoxDataSet.AppointmentsInstalacion)

        SchedulerControl1.Start = Date.Today

        '   comentariado , se dejo fijo en el control
        'SchedulerControl1.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Day
        'SchedulerControl1.GroupType = DevExpress.XtraScheduler.SchedulerGroupType.Resource
        'SchedulerControl1.DayView.TimeIndicatorDisplayOptions.ShowOverAppointment = True
        'ToolTipController1.ShowBeak = True
        'ToolTipController1.ToolTipType = ToolTipType.SuperTip

        SchedulerControl1.OptionsView.ToolTipVisibility = ToolTipVisibility.Always
        ObtenerCitas(SchedulerControl1.Start) ' obtengo el detalle de todas las citas procesadas en el sistema
        SchedulerControl1.Storage.Resources.DataSource = blFac.fac_GetVendedores()
        SchedulerControl1.Storage.Resources.DataMember = ""



        SchedulerControl1.ToolTipController = ToolTipController1

        'domingo comenzara desde 10 am
        If Weekday(Today) = 1 Then
            Dim span2 As TimeSpan = New TimeSpan(0, 19, 45, 0, 0)
            Dim span1 As TimeSpan = TimeSpan.FromHours(10)


            SchedulerControl1.Views.DayView.VisibleTime.Start = span1
            SchedulerControl1.Views.DayView.VisibleTime.End = span2
        End If

        AddHandler Me.SchedulerStorage1.AppointmentsChanged, AddressOf OnAppointmentChangedInsertedDeleted
        AddHandler Me.SchedulerStorage1.AppointmentsInserted, AddressOf OnAppointmentChangedInsertedDeleted
        AddHandler Me.SchedulerStorage1.AppointmentsDeleted, AddressOf OnAppointmentChangedInsertedDeleted

        '///////////////////////////////////////////CAMBIO DE FORMATO DE HORAS EN REGLA//////////////////////////////////////////////////////////////
        CreateTimeRulerFormatStringService()
        sb12H_Click(e, New EventArgs())
        '/////////////////////////////////////////////////////////////////////////////////////////////////////////


    End Sub

    Private Sub ObtenerCitas(ByVal Fecha As Date)
        SchedulerControl1.Storage.Appointments.DataSource = blFac.fac_GetCitasAll(Fecha)
        SchedulerControl1.Storage.Appointments.DataMember = ""
    End Sub
    '///////////////////////////////////////////CAMBIO DE FORMATO DE HORAS EN REGLA//////////////////////////////////////////////////////////////
    Public Sub CreateTimeRulerFormatStringService()
        Me.prevTimeRulerFormatStringService = CType(schedulerControl1.GetService( _
            GetType(ITimeRulerFormatStringService)), ITimeRulerFormatStringService)
        Me.customTimeRulerFormatStringService = _
            New CustomTimeRulerFormatStringService(prevTimeRulerFormatStringService)
    End Sub
    Private Sub sb12H_Click(sender As Object, e As EventArgs) Handles sb12H.Click
        ' Substitute the service. 
        SchedulerControl1.RemoveService(GetType(ITimeRulerFormatStringService))
        SchedulerControl1.AddService(GetType(ITimeRulerFormatStringService), _
            customTimeRulerFormatStringService)

        ' Initiate a control refresh. 
        SchedulerControl1.ActiveView.LayoutChanged()
    End Sub

    Private Sub sb24H_Click(sender As Object, e As EventArgs) Handles sb24H.Click
        ' Substitute the service. 
        SchedulerControl1.RemoveService(GetType(ITimeRulerFormatStringService))
        SchedulerControl1.AddService(GetType(ITimeRulerFormatStringService), _
            prevTimeRulerFormatStringService)

        ' Initiate a control refresh. 
        SchedulerControl1.ActiveView.LayoutChanged()
    End Sub
    '/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    Private Sub OnAppointmentChangedInsertedDeleted(ByVal sender As Object, ByVal e As PersistentObjectsEventArgs)
        'PENDIENTE DE REVISAR**************************************************************************************************************+
        'AppointmentsInstalacionTableAdapter.Update(TecniinoxDataSet)
        'TecniinoxDataSet.AcceptChanges()
    End Sub

    Private Sub SchedulerControl1_AppointmentViewInfoCustomizing(sender As Object, e As AppointmentViewInfoCustomizingEventArgs) Handles SchedulerControl1.AppointmentViewInfoCustomizing
        If e.ViewInfo.Appointment.CustomFields("Pagado") = True Then
            e.ViewInfo.Appearance.BackColor = Color.Gray
        Else
            e.ViewInfo.Appearance.BackColor = Color.LightBlue
        End If
    End Sub

    Private Sub SchedulerControl1_EditAppointmentFormShowing(sender As Object, e As AppointmentFormEventArgs) Handles SchedulerControl1.EditAppointmentFormShowing
        Dim scheduler As DevExpress.XtraScheduler.SchedulerControl = CType(sender, DevExpress.XtraScheduler.SchedulerControl)


        'OBTENGO EL ID DEL APPOINT
        Dim id As Int32 = 0
        Try
            Dim apt As Appointment = scheduler.SelectedAppointments(0)
            Dim dr As DataRowView = CType(apt.GetSourceObject(SchedulerStorage1), DataRowView)
            id = Convert.ToInt32(dr("UniqueID"))
        Catch ex As Exception
            id = 0 ' fd.ObtenerUltimoIdAppointment("AppointmentsInstalacion") + 1 ' fd.ObtenerUltimoId("AppointmentsInstalacion", "UniqueID") + 1
        End Try



        Dim form As Nexus.CustomAppointmentForm = New Nexus.CustomAppointmentForm(scheduler, e.Appointment, e.OpenRecurrenceForm, id)
        Try
            e.DialogResult = form.ShowDialog
            e.Handled = True
        Finally
            form.Dispose()
        End Try

        'comentariado 02-01-2018
        'Me.AppointmentsInstalacionTableAdapter.Fill(Me.TecniinoxDataSet.AppointmentsInstalacion)

        ObtenerCitas(SchedulerControl1.Start) ' obtengo el detalle de todas las citas procesadas en el sistema
        SchedulerControl1.RefreshData()
    End Sub

    Private Sub SchedulerControl1_InitAppointmentDisplayText1(sender As Object, e As AppointmentDisplayTextEventArgs) Handles SchedulerControl1.InitAppointmentDisplayText
        'Dim entClientes As New fac_Clientes
        'entClientes = objTablas.fac_ClientesSelectByPK(SiEsNulo(e.Appointment.CustomFields("IdCliente"), 0))
        'e.Text = SiEsNulo(entClientes.Nombre, "") + " " + SiEsNulo(e.Appointment.CustomFields("Descripcion"), "")

        e.Text = SiEsNulo(e.Appointment.CustomFields("NombreCliente"), "") + " " + SiEsNulo(e.Appointment.CustomFields("Descripcion"), "")
    End Sub


    Private Sub OnFilterAppointment(sender As Object, e As PersistentObjectCancelEventArgs) Handles SchedulerStorage1.FilterAppointment

        If txtNombre.EditValue <> "" Then
            Dim Bandera As Boolean = False
            Bandera = CType(e.Object, Appointment).CustomFields("NombreCliente").ToString().Contains(txtNombre.EditValue)
            If Not Bandera Then
                e.Cancel = True
            End If
            'e.Cancel = CType(e.Object, Appointment).CustomFields("NombreCliente") <> txtNombre.EditValue
        End If

        If txtCodigo.EditValue <> "" Then
            e.Cancel = CType(e.Object, Appointment).CustomFields("IdCliente") <> txtCodigo.EditValue
        End If

    End Sub

    Private Sub txtNombre_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombre.EditValueChanged
        SchedulerControl1.RefreshData()
    End Sub

    Private Sub txtCodigo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigo.EditValueChanged
        SchedulerControl1.RefreshData()
    End Sub


#Region "#ToolTipControllerBeforeShow"
    Private Sub ToolTipController1_BeforeShow(sender As Object, e As DevExpress.Utils.ToolTipControllerShowEventArgs) Handles ToolTipController1.BeforeShow
        Dim aptViewInfo As AppointmentViewInfo
        Dim controller As ToolTipController = DirectCast(sender, ToolTipController)
        Try
            aptViewInfo = CType(controller.ActiveObject, AppointmentViewInfo)
        Catch
            Return
        End Try

        If aptViewInfo Is Nothing Then
            Return
        End If

        If ToolTipController1.ToolTipType = ToolTipType.Standard Then
            e.IconType = ToolTipIconType.Information
            e.ToolTip = "adasdasdasda" ' aptViewInfo.Description
        End If

        Dim Descripcion As String = aptViewInfo.Appointment.Start.TimeOfDay.ToString() & "-" & aptViewInfo.Appointment.End.TimeOfDay.ToString() & Chr(13) & " "
        Dim entCliente As fac_Clientes = objTablas.fac_ClientesSelectByPK(SiEsNulo(aptViewInfo.Appointment.CustomFields("IdCliente"), ""))
        Descripcion += entCliente.Nombre & Chr(13) & Chr(13) & " "
        ' Descripcion += SiEsNulo(aptViewInfo.Appointment.CustomFields("Descripcion"), "") & Chr(13) & " "

        If ToolTipController1.ToolTipType = ToolTipType.SuperTip Then
            Dim SuperTip As New SuperToolTip()
            Dim args As New SuperToolTipSetupArgs()
            args.Title.Text = "Detalle..."
            args.Title.Font = New Font("Arial", 9)
            args.Contents.Text = Descripcion '& Chr(13)  & aptViewInfo.Description
            args.Contents.Appearance.TextOptions.WordWrap = WordWrap.Wrap

            'args.Contents.Image = resImage
            args.ShowFooterSeparator = False
            args.Footer.Font = New Font("Arial", 9)
            args.Footer.Text = SiEsNulo(aptViewInfo.Appointment.CustomFields("Descripcion"), "").ToString().ToUpper()
            SuperTip.Setup(args)
            e.SuperTip = SuperTip
            e.SuperTip.MaxWidth = 1000
            e.SuperTip.FixedTooltipWidth = True
        End If
    End Sub
#End Region ' #ToolTipControllerBeforeShow




    Private Sub DateNavigator1_EditValueChanged(sender As Object, e As EventArgs) Handles DateNavigator1.EditValueChanged
        ObtenerCitas(DateNavigator1.EditValue) ' obtengo el detalle de todas las citas procesadas en el sistema
    End Sub
End Class

Public Class CustomTimeRulerFormatStringService
    Inherits TimeRulerFormatStringServiceWrapper
    Public Sub New(ByVal service As ITimeRulerFormatStringService)
        MyBase.New(service)
    End Sub

#Region "ITimeRulerFormatStringService Members"
    ' Override methods so they return 12 hr time format for the time ruler with local time. 

    Public Overrides Function GetHalfDayHourFormat(ByVal ruler As TimeRuler) As String
        If ruler.UseClientTimeZone Then
            Return "htt:mm"
        Else
            Return "HH:mm"
        End If
    End Function
    Public Overrides Function GetHourFormat(ByVal ruler As TimeRuler) As String
        If ruler.UseClientTimeZone Then
            Return "htt:mm"
        Else
            Return "HH:mm"
        End If
    End Function
    Public Overrides Function GetHourOnlyFormat(ByVal ruler As TimeRuler) As String
        If ruler.UseClientTimeZone Then
            Return "htt"
        Else
            Return "HH"
        End If
    End Function
    Public Overrides Function GetMinutesOnlyFormat(ByVal ruler As TimeRuler) As String
        Return "mm"
    End Function
    Public Overrides Function GetTimeDesignatorOnlyFormat(ByVal ruler As TimeRuler) _
As String
        If ruler.UseClientTimeZone Then
            Return "tt"
        Else
            Return "mm"
        End If
    End Function
#End Region

End Class
