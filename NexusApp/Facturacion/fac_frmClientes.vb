﻿Imports NexusBLL 
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns


Public Class fac_frmClientes
    Dim entCliente As fac_Clientes
    Dim FlagIdCuenta As Boolean = False
    Dim entProducto As inv_Productos
    Dim bl As New InventarioBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim blCli As New FacturaBLL(g_ConnectionString)
    Dim Detalle As List(Of fac_ClientesPrecios)
    Dim DetalleInfoAdicional As fac_ClientesAnexo
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Dim _dtAnexoCliente As DataTable

    Property dtAnexoCliente() As DataTable
        Get
            Return _dtAnexoCliente
        End Get
        Set(ByVal value As DataTable)
            _dtAnexoCliente = value
        End Set
    End Property

    Private Sub fac_frmClientes_Eliminar() Handles Me.Eliminar
        Try
            If MsgBox("Está seguro(a) de eliminar a éste cliente?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
                objTablas.fac_ClientesDeleteByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCliente"))
            End If
        Catch ex As Exception

            MsgBox("SE DETECTÓ UN ERROR AL TRATAR DE ELIMINAR:" + Chr(13) + ex.Message(), MsgBoxStyle.Critical, "Error")
        End Try


    End Sub

    Private Sub acf_frmActivos_RefreshConsulta() Handles Me.RefreshConsulta
        gc.DataSource = objTablas.fac_ClientesSelectAll
        gv.BestFitColumns()
    End Sub

    Private Sub facClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.admDepartamentos(leDepartamento)
        objCombos.admMunicipios(leMunicipio, leDepartamento.EditValue)
        objCombos.fac_Vendedores(leVendedor)
        objCombos.fac_TiposComprobante(leTipoDocto)
        objCombos.fac_TiposImpuesto(leTipoImpuesto)
        objCombos.fac_FormasPago(leFormaPago, "")
        objCombos.fac_Rutas(leRuta, "")
        objCombos.inv_Precios(leTipoPrecio, "-- SIN PRECIO --")
        gc.DataSource = objTablas.fac_ClientesSelectAll
        ActivaControles(False)
    End Sub

    Private Sub fac_frmClientes_Nuevo() Handles Me.Nuevo
        dtParam = blAdmon.ObtieneParametros()

        entCliente = New fac_Clientes
        entCliente.IdVendedor = leVendedor.EditValue
        entCliente.IdTipoComprobante = 5
        entCliente.IdFormaPago = 2
        entCliente.IdTipoImpuesto = 1
        entCliente.IdMunicipio = "0614"
        entCliente.IdDepartamento = "06"
        entCliente.IdVendedor = 1
        entCliente.DiasCredito = 30
        entCliente.IdPrecio = 1
        CargaPantalla()

        beIdCuenta.EditValue = SiEsNulo(dtParam.Rows(0).Item("IdCuentaPorCobrar"), "")
        If SiEsNulo(dtParam.Rows(0).Item("IdCuentaPorCobrar"), "") <> "" Then
            beIdCuenta_Validated("", New EventArgs())
        End If

        ActivaControles(True)
    End Sub
    Private Sub fac_frmClientes_Guardar() Handles Me.Guardar
        If Not DatosValidos() Then
            Exit Sub
        End If

        CargaEntidad()
        LlenarInfoAdicionalCliente()

        Dim msj As String = ""

        If DbMode = DbModeType.insert Then
            msj = blCli.fac_InsertarCliente(entCliente, Detalle, DetalleInfoAdicional)
            If msj <> "" Then
                MsgBox("Ocurrio un problema al crear la ficha del cliente" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If
        Else
            msj = blCli.fac_ActualizarCliente(entCliente, Detalle, DetalleInfoAdicional)
            If msj <> "" Then
                MsgBox("Ocurrio un problema al actualizar la ficha del cliente" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If
        End If

        xtcClientes.SelectedTabPage = xtpLista
        gc.DataSource = objTablas.fac_ClientesSelectAll
        gc.Focus()
        MostrarModoInicial()
        ActivaControles(False)
    End Sub
    Private Sub fac_frmClientes_Revertir() Handles Me.Revertir
        xtcClientes.SelectedTabPage = xtpLista
        gc.Focus()
        ActivaControles(False)
    End Sub
    Private Sub fac_frmClientes_Edit_Click() Handles Me.Editar
        entCliente = objTablas.fac_ClientesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCliente"))
        CargaPantalla()
        Me.Text = "Cliente: " & entCliente.Nombre
        DbMode = DbModeType.update
        ActivaControles(True)
        teCodigo.Focus()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In xtpDatos.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
        teNombreCuenta.Properties.ReadOnly = True
    End Sub
    Private Sub CargaPantalla()

        xtcClientes.SelectedTabPage = xtpDatos
        teCodigo.Focus()
        With entCliente
            teCodigo.EditValue = .IdCliente
            teNombre.EditValue = .Nombre
            teRazonSocial.EditValue = .RazonSocial
            teNRC.EditValue = .Nrc
            teNIT.EditValue = .Nit
            teGiro.EditValue = .Giro
            teTelefonos.EditValue = .Telefonos
            teDireccion.EditValue = .Direccion
            teCorreoElectronico.EditValue = .CorreoElectronico
            ceAplicaPer_Ret.Checked = .AplicaRetencion
            beIdCuenta.EditValue = .IdCuentaContable
            leDepartamento.EditValue = .IdDepartamento
            leMunicipio.EditValue = .IdMunicipio
            leVendedor.EditValue = .IdVendedor
            seLimite.EditValue = .LimiteCredito
            seDiasCredito.EditValue = .DiasCredito
            sePorcDescuento.EditValue = .PorcDescuento
            leTipoDocto.EditValue = .IdTipoComprobante
            leTipoImpuesto.EditValue = .IdTipoImpuesto
            leFormaPago.EditValue = .IdFormaPago
            leTipoPrecio.EditValue = .IdPrecio
            leRuta.EditValue = .IdRuta
            teFax.EditValue = .Fax
            teOtroDocumento.EditValue = .OtroDocumento
            ceBloquear.EditValue = .BloquearFacturacion
            rgTipo.EditValue = .TipoImpuestoAdicional
            gcPre.DataSource = blCli.fac_ObtenerClientesPrecios(.IdCliente)

            _dtAnexoCliente = blCli.fac_ClientesInfoAnexos(.IdCliente)
            LlenarInfoAdicionalCliente()
        End With
    End Sub
    Private Sub CargaEntidad()
        With entCliente
            .IdCliente = teCodigo.EditValue
            .Nombre = teNombre.EditValue
            .RazonSocial = teRazonSocial.EditValue
            .Nrc = teNRC.EditValue
            .Nit = teNIT.EditValue
            .Giro = teGiro.EditValue
            .Telefonos = teTelefonos.EditValue
            .Fax = teFax.EditValue
            .Direccion = teDireccion.EditValue
            .CorreoElectronico = teCorreoElectronico.EditValue
            .AplicaRetencion = ceAplicaPer_Ret.Checked
            .IdCuentaContable = beIdCuenta.EditValue
            .IdDepartamento = leDepartamento.EditValue
            .IdMunicipio = leMunicipio.EditValue
            .IdVendedor = leVendedor.EditValue
            .SaldoActual = 0.0
            .DiasCredito = seDiasCredito.EditValue
            .FechaUltPago = Today
            .LimiteCredito = seLimite.EditValue
            .IdTipoImpuesto = leTipoImpuesto.EditValue
            .IdTipoComprobante = leTipoDocto.EditValue
            .PorcDescuento = sePorcDescuento.EditValue
            .IdFormaPago = leFormaPago.EditValue
            .IdPrecio = leTipoPrecio.EditValue
            .IdRuta = leRuta.EditValue
            .BloquearFacturacion = ceBloquear.EditValue
            .TipoImpuestoAdicional = rgTipo.EditValue
            .OtroDocumento = teOtroDocumento.EditValue

            If DbMode = DbModeType.insert Then
                .FechaHoraCreacion = Now
                .CreadoPor = objMenu.User
                .ModificadoPor = ""
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If

            Detalle = New List(Of fac_ClientesPrecios)
            For i = 0 To gvPre.DataRowCount - 1
                Dim entDetalle As New fac_ClientesPrecios
                With entDetalle
                    .IdCliente = entCliente.IdCliente
                    .IdDetalle = i + 1
                    .IdProducto = gvPre.GetRowCellValue(i, "IdProducto")
                    .Descripcion = gvPre.GetRowCellValue(i, "Descripcion")
                    .PrecioUnitario = gvPre.GetRowCellValue(i, "PrecioUnitario")
                End With
                Detalle.Add(entDetalle)
            Next

        End With
    End Sub
    Private Sub leDepto_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leDepartamento.EditValueChanged
        objCombos.admMunicipios(leMunicipio, leDepartamento.EditValue)
    End Sub
    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc.DoubleClick
        entCliente = objTablas.fac_ClientesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCliente"))
        CargaPantalla()
        Me.Text = "Cliente: " & entCliente.Nombre
        DbMode = DbModeType.update
        teCodigo.Focus()
    End Sub
    Function DatosValidos() As Boolean
        If teNombre.EditValue = "" Or teNIT.EditValue = "" OrElse leVendedor.EditValue = 0 OrElse beIdCuenta.EditValue = "" OrElse leTipoDocto.EditValue = 0 And leTipoImpuesto.EditValue = 0 Then
            MsgBox("Existen datos que no pueden quedar en blanco" & Chr(13) & "Verifique [Nombre, NIT, Vendedor, Cuenta Contable, Tipo de Docto.]", MsgBoxStyle.Critical, "Nota")
            Return False
        End If
        If teCodigo.EditValue = "" Then
            MsgBox("El codigo de Cliente no puede quedar Vacio", MsgBoxStyle.Critical, "Nota")
            Return False
        End If
        If DbMode = DbModeType.insert Then
            Dim Entc As fac_Clientes
            Entc = objTablas.fac_ClientesSelectByPK(teCodigo.EditValue)
            If teCodigo.EditValue = Entc.IdCliente Then
                MsgBox("El Codigo de Cliente ya existe ", MsgBoxStyle.Critical, "Nota")
                Return False
            End If
        End If
        Dim msj As String = ""
        gvPre.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        For i = 0 To gvPre.DataRowCount - 1
            If gvPre.GetRowCellValue(i, "IdProducto") = gvPre.GetRowCellValue(i + 1, "IdProducto") Then
                msj = "El producto " & gvPre.GetRowCellValue(i, "IdProducto") & ",  está repetido en la lista de precios del cliente"
                Exit For
            End If
        Next
        gvPre.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.None
        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If

        Return True
    End Function
    Private Sub fac_frmClientes_Report_Click() Handles Me.Reporte
        Dim rpt As New cpc_rptListClientes
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Listado de clientes"
        rpt.DataSource = objTablas.fac_ClientesSelectAll
        rpt.DataMember = ""
        rpt.ShowPreview()
    End Sub
    Private Sub beIdCuenta_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beIdCuenta.ButtonClick
        beIdCuenta.EditValue = ""
        FlagIdCuenta = False
        beIdCuenta_Validated(sender, New System.EventArgs)
    End Sub
    Private Sub beIdCuenta_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beIdCuenta.Validated
        If FlagIdCuenta And beIdCuenta.EditValue = "" Then
            Exit Sub
        End If
        Dim ent As con_Cuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beIdCuenta.EditValue)

        If Not ent.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beIdCuenta.EditValue = ""
            FlagIdCuenta = False
            Exit Sub
        End If
        If ent.Naturaleza <> 1 Then
            MsgBox("La cuenta ingresada no es de tipo [Cuenta por cobrar]", MsgBoxStyle.Critical, "Nota")
        End If
        beIdCuenta.EditValue = ent.IdCuenta
        teNombreCuenta.EditValue = ent.Nombre
        FlagIdCuenta = True
    End Sub



#Region "Grid1"

    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gvPre.DeleteSelectedRows()
    End Sub
    Private Sub GridView1_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs)
        gvPre.SetRowCellValue(gvPre.FocusedRowHandle, "Descripcion", "")
        gvPre.SetRowCellValue(gvPre.FocusedRowHandle, "PrecioUnitario", 0.0)
    End Sub



    Private Sub GridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gvPre.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            If gvPre.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gvPre.EditingValue, "") = "" Then
                    Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gvPre.EditingValue = IdProd
                End If

                entProducto = objTablas.inv_ProductosSelectByPK(gvPre.EditingValue)

                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gvPre.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
                Else
                    gvPre.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                    gvPre.SetFocusedRowCellValue("PrecioUnitario", 0.0)
                End If
            End If
        End If
    End Sub


#End Region


    Private Sub sbImprimeLista_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbImprimeLista.Click
        Dim dt As DataTable = blCli.fac_ObtenerClientesPrecios(entCliente.IdCliente)
        Dim rpt As New fac_rptListaPrecios
        rpt.DataSource = dt
        rpt.DataMember = ""

        rpt.xrlTitulo.Text = "LISTA DE PRECIOS POR CLIENTE"
        rpt.xrlPeriodo.Text = "CLIENTE: " & entCliente.Nombre
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub LlenarInfoAdicionalCliente()
        'LLENO LA LISTA DE PRODUCTO PARA QUE LE COLOQUEN LOS GASTOS QUE APLICARAN
        DetalleInfoAdicional = New fac_ClientesAnexo

        If _dtAnexoCliente.Rows.Count > 0 Then
            With DetalleInfoAdicional
                .IdCliente = ""
                .IdProfesion = SiEsNulo(_dtAnexoCliente.Rows(0).Item("IdProfesion"), 0)
                .IdRubro = SiEsNulo(_dtAnexoCliente.Rows(0).Item("IdRubro"), 0)
                .IdActividad = SiEsNulo(_dtAnexoCliente.Rows(0).Item("IdActividad"), 0)
                .IngresosBruto = SiEsNulo(_dtAnexoCliente.Rows(0).Item("IngresosBruto"), 0)
                .APNFD = SiEsNulo(_dtAnexoCliente.Rows(0).Item("APNFD"), False)
                .Comentario = SiEsNulo(_dtAnexoCliente.Rows(0).Item("Comentario"), "")
                .MontoEstimadoCompras = SiEsNulo(_dtAnexoCliente.Rows(0).Item("MontoEstimadoCompras"), 0)

                .RepresentanteLegal = SiEsNulo(_dtAnexoCliente.Rows(0).Item("RepresentanteLegal"), "")
                .TipoDocumento = SiEsNulo(_dtAnexoCliente.Rows(0).Item("TipoDocumento"), 0)
                .NumDocumentoRepLegal = SiEsNulo(_dtAnexoCliente.Rows(0).Item("NumDocumentoRepLegal"), "")
                .NITRepLegal = SiEsNulo(_dtAnexoCliente.Rows(0).Item("NITRepLegal"), "")
                .IdDepartamentoRepLegal = SiEsNulo(_dtAnexoCliente.Rows(0).Item("IdDepartamentoRepLegal"), "")
                .IdMunicipioRepLegal = SiEsNulo(_dtAnexoCliente.Rows(0).Item("IdMunicipioRepLegal"), "")
                .IdProfesionRepLegal = SiEsNulo(_dtAnexoCliente.Rows(0).Item("IdProfesionRepLegal"), 0)
                .DireccionRepLegal = SiEsNulo(_dtAnexoCliente.Rows(0).Item("DireccionRepLegal"), "")
                .TelMovilRepLegal = SiEsNulo(_dtAnexoCliente.Rows(0).Item("TelMovilRepLegal"), "")
                .TelefonosRepLegal = SiEsNulo(_dtAnexoCliente.Rows(0).Item("TelefonosRepLegal"), "")
                .CorreoElectronicoRepLegal = SiEsNulo(_dtAnexoCliente.Rows(0).Item("CorreoElectronicoRepLegal"), "")
                .PersonaJuridica = SiEsNulo(_dtAnexoCliente.Rows(0).Item("PersonaJuridica"), False)

                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
        End If

    End Sub

    Private Sub sbInfoAdicional_Click(sender As Object, e As EventArgs) Handles sbInfoAdicional.Click
        If teNombre.Properties.ReadOnly Then
            Exit Sub
        End If

        Dim frmDetalleCli As New fac_frmInfoAdicionalCliente
        Me.AddOwnedForm(frmDetalleCli)
        frmDetalleCli.IdCliente = teCodigo.EditValue
        frmDetalleCli.ShowDialog()

        LlenarInfoAdicionalCliente()
    End Sub



    Private Sub DeclaracionJuradaPersonaNatural()
        If Not teNombre.Properties.ReadOnly Then
            MsgBox("Debe guardar el registro para imprimir la declaración jurada", MsgBoxStyle.Information, "NOTA")
            Exit Sub
        End If

        Dim entClienteAnexo As fac_ClientesAnexo = objTablas.fac_ClientesAnexoSelectByPK(teCodigo.EditValue)

        If entClienteAnexo.PersonaJuridica Then
            MsgBox("El cliente esta catalogado como persona jurídica" + Chr(13) + "Imposible continuar", MsgBoxStyle.Information, "NOTA")
            Exit Sub
        End If

        Dim dt As DataTable = blCli.rptDatosDeclaracionJuradaCliente(teCodigo.EditValue)

        Dim entProfesion As fac_Profesiones = objTablas.fac_ProfesionesSelectByPK(entClienteAnexo.IdProfesion)

        Dim rpt As New fac_rptEntrevistaDeclaracion
        rpt.DataSource = dt
        rpt.DataMember = ""

        Dim Texto As String = "Yo, " + entCliente.Nombre + ", con D.U.I. No. " + teOtroDocumento.EditValue
        Texto += " y NIT No. " + teNIT.EditValue + ", del domicilio de " + leMunicipio.Text
        Texto += " y de profesión " + entProfesion.Nombre + ", actuando en lo personal, por este medio manifiesto que:"
        rpt.xrlTexto.Text = Texto

        Texto = "La información proporcionada es fidedigna y ha sido solicitada por"
        Texto += gsNombre_Empresa + " en cumplimiento de los dispuestos en el Instructivo de la Unidad de Investigación Financiera (UIF) de la Fiscalia General de la Republica,"
        Texto += " para la prevención de Lavado de Dinero y de Activos en las Instituciones de Intermediación Financiera , por lo que autorizo a " + gsNombre_Empresa + " para que pueda"
        Texto += " investigar la información proporcionada , apertura de cuentás, otorgamiento de creditos y otras transacciones."

        rpt.xrlTexto2.Text = Texto

        rpt.xrlLugarFecha.Text = dtParam.Rows(0).Item("Municipio") + ", " + FechaToString(CDate(entCliente.FechaHoraCreacion), CDate(entCliente.FechaHoraCreacion))
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.XrPictureBox1.Image = ByteToImagen(dtParam.Rows(0).Item("LogoEmpresa"))
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub DeclaracionJuradaPersonaJuridica()
        If Not teNombre.Properties.ReadOnly Then
            MsgBox("Debe guardar el registro para imprimir la declaración jurada", MsgBoxStyle.Information, "NOTA")
            Exit Sub
        End If

        Dim dt As DataTable = blCli.rptDatosDeclaracionJuradaCliente(teCodigo.EditValue)
        Dim entClienteAnexo As fac_ClientesAnexo = objTablas.fac_ClientesAnexoSelectByPK(teCodigo.EditValue)
        Dim entMunicipios As adm_Municipios = objTablas.adm_MunicipiosSelectByPK(entClienteAnexo.IdMunicipioRepLegal)
        Dim entProfesionRL As fac_Profesiones = objTablas.fac_ProfesionesSelectByPK(entClienteAnexo.IdProfesionRepLegal)

        Dim rpt As New fac_rptEntrevistaDeclaracionPJ
        rpt.DataSource = dt
        rpt.DataMember = ""

        Dim Texto As String = "Yo, " + entClienteAnexo.RepresentanteLegal + ", con " + IIf(entClienteAnexo.TipoDocumento <> 5, dt.Rows(0).Item("TipoDocumento"), teOtroDocumento.EditValue) + " No. " + entClienteAnexo.NumDocumentoRepLegal
        Texto += " y NIT No. " + entClienteAnexo.NITRepLegal + ", del domicilio de " + SiEsNulo(entMunicipios.Nombre, "PENDIENTE DE DEFINIR")
        Texto += " y de profesión " + entProfesionRL.Nombre + ", actuando en representación de " + teNombre.EditValue + ", por este medio manifiesto que:"

        rpt.xrlTexto.Text = Texto

        Texto = "La información proporcionada es fidedigna y ha sido solicitada por "
        Texto += gsNombre_Empresa + " en cumplimiento de los dispuestos en el Instructivo de la Unidad de Investigación Financiera (UIF) de la Fiscalia General de la Republica,"
        Texto += " para la prevención de Lavado de Dinero y de Activos en las Instituciones de Intermediación Financiera , por lo que autorizo a "
        Texto += gsNombre_Empresa + " para que pueda"
        Texto += " investigar la información proporcionada, apertura de cuentás, otorgamiento de creditos y otras transacciones."

        rpt.xrlTexto2.Text = Texto


        rpt.xrlLugarFecha.Text = dtParam.Rows(0).Item("Municipio") + ", " + FechaToString(CDate(entCliente.FechaHoraCreacion), CDate(entCliente.FechaHoraCreacion))
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.XrPictureBox1.Image = ByteToImagen(dtParam.Rows(0).Item("LogoEmpresa"))
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub sbDeclaracionJuradaPersonaNatural_Click(sender As Object, e As EventArgs) Handles sbDeclaracionJuradaPersonaNatural.Click

        Dim entClienteAnexo As fac_ClientesAnexo = objTablas.fac_ClientesAnexoSelectByPK(teCodigo.EditValue)

        If entClienteAnexo.PersonaJuridica Then
            DeclaracionJuradaPersonaJuridica()
        Else
            DeclaracionJuradaPersonaNatural()
        End If

    End Sub

    Private Sub leTipoDocto_EditValueChanged(sender As Object, e As EventArgs) Handles leTipoDocto.EditValueChanged
        If leTipoDocto.EditValue = 7 Then
            leFormaPago.EditValue = 4
            leTipoImpuesto.EditValue = 3 'Tasa Cero, Solicitado por Edwin y Don Elmer 18-02-2020, mediante correo.
        End If
        leTipoImpuesto.Enabled = Not leTipoDocto.EditValue = 7
    End Sub
End Class
