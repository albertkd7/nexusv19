<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmTipoFacturaEvento
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmTipoFacturaEvento))
        Me.sbCancel = New DevExpress.XtraEditors.SimpleButton
        Me.rgOpcion = New DevExpress.XtraEditors.RadioGroup
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.lblCuota = New DevExpress.XtraEditors.LabelControl
        Me.teNumCuota = New DevExpress.XtraEditors.TextEdit
        CType(Me.rgOpcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumCuota.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sbCancel
        '
        Me.sbCancel.Location = New System.Drawing.Point(117, 105)
        Me.sbCancel.Name = "sbCancel"
        Me.sbCancel.Size = New System.Drawing.Size(75, 23)
        Me.sbCancel.TabIndex = 2
        Me.sbCancel.Text = "Continuar"
        '
        'rgOpcion
        '
        Me.rgOpcion.EditValue = 1
        Me.rgOpcion.Location = New System.Drawing.Point(106, 15)
        Me.rgOpcion.Name = "rgOpcion"
        Me.rgOpcion.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Facturación Horas"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Facturación Seminario/Evento")})
        Me.rgOpcion.Size = New System.Drawing.Size(179, 47)
        Me.rgOpcion.TabIndex = 3
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(21, 16)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl1.TabIndex = 4
        Me.LabelControl1.Text = "Tipo Facturación:"
        '
        'lblCuota
        '
        Me.lblCuota.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblCuota.Location = New System.Drawing.Point(3, 68)
        Me.lblCuota.Name = "lblCuota"
        Me.lblCuota.Size = New System.Drawing.Size(101, 19)
        Me.lblCuota.TabIndex = 5
        Me.lblCuota.Text = "Num. Cuota:"
        Me.lblCuota.Visible = False
        '
        'teNumCuota
        '
        Me.teNumCuota.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teNumCuota.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.teNumCuota.Location = New System.Drawing.Point(106, 65)
        Me.teNumCuota.Name = "teNumCuota"
        Me.teNumCuota.Properties.AllowFocused = False
        Me.teNumCuota.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.teNumCuota.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.teNumCuota.Properties.Appearance.Options.UseFont = True
        Me.teNumCuota.Properties.Appearance.Options.UseForeColor = True
        Me.teNumCuota.Properties.Appearance.Options.UseTextOptions = True
        Me.teNumCuota.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teNumCuota.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teNumCuota.Size = New System.Drawing.Size(61, 26)
        Me.teNumCuota.TabIndex = 96
        Me.teNumCuota.Visible = False
        '
        'fac_frmTipoFacturaEvento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(289, 133)
        Me.Controls.Add(Me.teNumCuota)
        Me.Controls.Add(Me.lblCuota)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.rgOpcion)
        Me.Controls.Add(Me.sbCancel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fac_frmTipoFacturaEvento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cerrar Venta"
        CType(Me.rgOpcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumCuota.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents sbCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents rgOpcion As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCuota As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumCuota As DevExpress.XtraEditors.TextEdit
End Class
