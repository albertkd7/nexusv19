<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmConsultaClientesPorPedidos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmConsultaClientesPorPedidos))
        Me.gcClientes = New DevExpress.XtraGrid.GridControl
        Me.gvClientes = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl
        Me.rgTipoBusqueda = New DevExpress.XtraEditors.RadioGroup
        Me.txtCodigo = New DevExpress.XtraEditors.TextEdit
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.txtNrc = New DevExpress.XtraEditors.TextEdit
        CType(Me.gcClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipoBusqueda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNrc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcClientes
        '
        Me.gcClientes.Location = New System.Drawing.Point(2, 66)
        Me.gcClientes.MainView = Me.gvClientes
        Me.gcClientes.Name = "gcClientes"
        Me.gcClientes.Size = New System.Drawing.Size(829, 462)
        Me.gcClientes.TabIndex = 5
        Me.gcClientes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvClientes})
        '
        'gvClientes
        '
        Me.gvClientes.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn6, Me.GridColumn5})
        Me.gvClientes.GridControl = Me.gcClientes
        Me.gvClientes.Name = "gvClientes"
        Me.gvClientes.OptionsBehavior.Editable = False
        Me.gvClientes.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "C�d. Cliente"
        Me.GridColumn1.FieldName = "IdCliente"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        Me.GridColumn1.Width = 114
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombre"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 311
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Nrc"
        Me.GridColumn3.FieldName = "Nrc"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 3
        Me.GridColumn3.Width = 94
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "NIT"
        Me.GridColumn4.FieldName = "Nit"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 4
        Me.GridColumn4.Width = 99
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "OtroDocumento"
        Me.GridColumn6.FieldName = "OtroDocumento"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 5
        Me.GridColumn6.Width = 90
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Tel�fonos"
        Me.GridColumn5.FieldName = "Telefonos"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 6
        Me.GridColumn5.Width = 103
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(5, 5)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl24.TabIndex = 49
        Me.LabelControl24.Text = "Tipo de Busqueda:"
        '
        'rgTipoBusqueda
        '
        Me.rgTipoBusqueda.Location = New System.Drawing.Point(96, 2)
        Me.rgTipoBusqueda.Name = "rgTipoBusqueda"
        Me.rgTipoBusqueda.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Contiene"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Inicia Con")})
        Me.rgTipoBusqueda.Size = New System.Drawing.Size(153, 21)
        Me.rgTipoBusqueda.TabIndex = 6
        '
        'txtCodigo
        '
        Me.txtCodigo.Location = New System.Drawing.Point(96, 24)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(117, 20)
        Me.txtCodigo.TabIndex = 0
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(96, 44)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(349, 20)
        Me.txtNombre.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(57, 25)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl1.TabIndex = 50
        Me.LabelControl1.Text = "C�digo:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(49, 49)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 51
        Me.LabelControl2.Text = "Nombre:"
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Id Comprobante"
        Me.GridColumn7.FieldName = "IdComprobante"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 0
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(279, 25)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl3.TabIndex = 52
        Me.LabelControl3.Text = "Registro:"
        '
        'txtNrc
        '
        Me.txtNrc.Location = New System.Drawing.Point(328, 22)
        Me.txtNrc.Name = "txtNrc"
        Me.txtNrc.Size = New System.Drawing.Size(117, 20)
        Me.txtNrc.TabIndex = 53
        '
        'fac_frmConsultaClientesPorPedidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(832, 537)
        Me.Controls.Add(Me.txtNrc)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.LabelControl24)
        Me.Controls.Add(Me.rgTipoBusqueda)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.gcClientes)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fac_frmConsultaClientesPorPedidos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Consulta de Clientes por Pedidos"
        CType(Me.gcClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipoBusqueda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNrc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gcClientes As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvClientes As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgTipoBusqueda As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents txtCodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNrc As DevExpress.XtraEditors.TextEdit
End Class
