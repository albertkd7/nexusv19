﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmConsultaProductosBase
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
        Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
        Me.xtlConsultas = New DevExpress.XtraTab.XtraTabControl
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl
        Me.rgTipoBusqueda = New DevExpress.XtraEditors.RadioGroup
        Me.txtCodigo = New DevExpress.XtraEditors.TextEdit
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit
        Me.gcEx1 = New DevExpress.XtraGrid.GridControl
        Me.gvEx1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcPr1 = New DevExpress.XtraGrid.GridControl
        Me.gvPr1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcIdPrecio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.rileIdPrecio1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcProductos = New DevExpress.XtraGrid.GridControl
        Me.gvProd1 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.meInfo = New DevExpress.XtraEditors.MemoEdit
        Me.peFoto1 = New DevExpress.XtraEditors.PictureEdit
        Me.teUnidad1 = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
        Me.teUnidadesPre = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.teColor = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl
        Me.teProveedor1 = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl
        Me.teEstilo = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl
        Me.teTalla = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl
        Me.teMarca = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl
        Me.teSubGrupo1 = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl
        Me.teGrupo1 = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage
        Me.teCantidadUltCompra = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl
        Me.teFacturaUltCompra = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl
        Me.teUnidad = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.teFechaUltCompra = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.tePrecioCosto = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl
        Me.teProveedor = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl
        Me.teSubGrupo = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.teGrupo = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
        Me.gcEx = New DevExpress.XtraGrid.GridControl
        Me.gvEx = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcBodega = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcSaldo = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.gcPr = New DevExpress.XtraGrid.GridControl
        Me.gvPr = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcPrecio = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcValor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcVentas = New DevExpress.XtraGrid.GridControl
        Me.gvVentas = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.rileIdPrecio = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.peFoto = New DevExpress.XtraEditors.PictureEdit
        Me.gcProd = New DevExpress.XtraGrid.GridControl
        Me.gvProd = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
        Me.btnGenerar = New DevExpress.XtraEditors.SimpleButton
        Me.teNombre = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.teCodigo = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage
        Me.gcDetalle = New DevExpress.XtraGrid.GridControl
        Me.gvDetalle = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcRef = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcDesc = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcCant = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcPrecioU = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcPrecioT = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcUtilidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcPorcUtilidad = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcCosto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemLookUpEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
        Me.peFoto2 = New DevExpress.XtraEditors.PictureEdit
        Me.gcPed = New DevExpress.XtraGrid.GridControl
        Me.gvPed = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcPedido = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcNumero = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcNombre = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcVendedor = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcTotal = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcIdComprobante = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl
        Me.leTipoDocumento = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl
        Me.btGenerar = New DevExpress.XtraEditors.SimpleButton
        Me.deHasta = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl
        Me.deDesde = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl
        CType(Me.xtlConsultas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtlConsultas.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.rgTipoBusqueda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcEx1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvEx1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcPr1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPr1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rileIdPrecio1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvProd1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meInfo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peFoto1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teUnidad1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teUnidadesPre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teColor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teProveedor1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teEstilo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTalla.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teMarca.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSubGrupo1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teGrupo1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.teCantidadUltCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teFacturaUltCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teUnidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teFechaUltCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePrecioCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSubGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teGrupo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcEx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvEx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcPr, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPr, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rileIdPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peFoto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.gcDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemLookUpEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.peFoto2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcPed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.leTipoDocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtlConsultas
        '
        Me.xtlConsultas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtlConsultas.Location = New System.Drawing.Point(0, 0)
        Me.xtlConsultas.Name = "xtlConsultas"
        Me.xtlConsultas.SelectedTabPage = Me.XtraTabPage1
        Me.xtlConsultas.Size = New System.Drawing.Size(1140, 556)
        Me.xtlConsultas.TabIndex = 4
        Me.xtlConsultas.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.LabelControl24)
        Me.XtraTabPage1.Controls.Add(Me.rgTipoBusqueda)
        Me.XtraTabPage1.Controls.Add(Me.txtCodigo)
        Me.XtraTabPage1.Controls.Add(Me.txtNombre)
        Me.XtraTabPage1.Controls.Add(Me.gcEx1)
        Me.XtraTabPage1.Controls.Add(Me.gcPr1)
        Me.XtraTabPage1.Controls.Add(Me.gcProductos)
        Me.XtraTabPage1.Controls.Add(Me.meInfo)
        Me.XtraTabPage1.Controls.Add(Me.peFoto1)
        Me.XtraTabPage1.Controls.Add(Me.teUnidad1)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl7)
        Me.XtraTabPage1.Controls.Add(Me.teUnidadesPre)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl10)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl8)
        Me.XtraTabPage1.Controls.Add(Me.teColor)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl11)
        Me.XtraTabPage1.Controls.Add(Me.teProveedor1)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl12)
        Me.XtraTabPage1.Controls.Add(Me.teEstilo)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl14)
        Me.XtraTabPage1.Controls.Add(Me.teTalla)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl17)
        Me.XtraTabPage1.Controls.Add(Me.teMarca)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl18)
        Me.XtraTabPage1.Controls.Add(Me.teSubGrupo1)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl19)
        Me.XtraTabPage1.Controls.Add(Me.teGrupo1)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl20)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1134, 530)
        Me.XtraTabPage1.Text = "Consulta Productos"
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(117, 2)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl24.TabIndex = 45
        Me.LabelControl24.Text = "Tipo de Busqueda:"
        '
        'rgTipoBusqueda
        '
        Me.rgTipoBusqueda.Location = New System.Drawing.Point(209, 0)
        Me.rgTipoBusqueda.Name = "rgTipoBusqueda"
        Me.rgTipoBusqueda.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Contiene"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Inicia Con")})
        Me.rgTipoBusqueda.Size = New System.Drawing.Size(153, 21)
        Me.rgTipoBusqueda.TabIndex = 44
        '
        'txtCodigo
        '
        Me.txtCodigo.Location = New System.Drawing.Point(21, 22)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(186, 20)
        Me.txtCodigo.TabIndex = 42
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(209, 22)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(388, 20)
        Me.txtNombre.TabIndex = 43
        '
        'gcEx1
        '
        Me.gcEx1.Location = New System.Drawing.Point(698, 130)
        Me.gcEx1.MainView = Me.gvEx1
        Me.gcEx1.Name = "gcEx1"
        Me.gcEx1.Size = New System.Drawing.Size(300, 120)
        Me.gcEx1.TabIndex = 37
        Me.gcEx1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvEx1})
        '
        'gvEx1
        '
        Me.gvEx1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn8})
        Me.gvEx1.GridControl = Me.gcEx1
        Me.gvEx1.Name = "gvEx1"
        Me.gvEx1.OptionsBehavior.Editable = False
        Me.gvEx1.OptionsView.ShowFooter = True
        Me.gvEx1.OptionsView.ShowGroupPanel = False
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Bodega"
        Me.GridColumn7.FieldName = "Bodega"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.OptionsColumn.AllowEdit = False
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 0
        Me.GridColumn7.Width = 167
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Saldo"
        Me.GridColumn8.DisplayFormat.FormatString = "###,##0.00"
        Me.GridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn8.FieldName = "Saldo"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)})
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 1
        Me.GridColumn8.Width = 112
        '
        'gcPr1
        '
        Me.gcPr1.Location = New System.Drawing.Point(698, 13)
        Me.gcPr1.MainView = Me.gvPr1
        Me.gcPr1.Name = "gcPr1"
        Me.gcPr1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rileIdPrecio1})
        Me.gcPr1.Size = New System.Drawing.Size(300, 114)
        Me.gcPr1.TabIndex = 35
        Me.gcPr1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvPr1})
        '
        'gvPr1
        '
        Me.gvPr1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdPrecio, Me.GridColumn9})
        Me.gvPr1.GridControl = Me.gcPr1
        Me.gvPr1.Name = "gvPr1"
        Me.gvPr1.OptionsBehavior.Editable = False
        Me.gvPr1.OptionsView.ShowGroupPanel = False
        '
        'gcIdPrecio
        '
        Me.gcIdPrecio.Caption = "Precio"
        Me.gcIdPrecio.ColumnEdit = Me.rileIdPrecio1
        Me.gcIdPrecio.FieldName = "IdPrecio"
        Me.gcIdPrecio.Name = "gcIdPrecio"
        Me.gcIdPrecio.OptionsColumn.AllowEdit = False
        Me.gcIdPrecio.Visible = True
        Me.gcIdPrecio.VisibleIndex = 0
        Me.gcIdPrecio.Width = 167
        '
        'rileIdPrecio1
        '
        Me.rileIdPrecio1.AutoHeight = False
        Me.rileIdPrecio1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rileIdPrecio1.Name = "rileIdPrecio1"
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Valor"
        Me.GridColumn9.DisplayFormat.FormatString = "###,##0.0000"
        Me.GridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn9.FieldName = "Precio"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 1
        Me.GridColumn9.Width = 112
        '
        'gcProductos
        '
        Me.gcProductos.Location = New System.Drawing.Point(6, 42)
        Me.gcProductos.MainView = Me.gvProd1
        Me.gcProductos.Name = "gcProductos"
        Me.gcProductos.Size = New System.Drawing.Size(591, 208)
        Me.gcProductos.TabIndex = 14
        Me.gcProductos.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvProd1})
        '
        'gvProd1
        '
        Me.gvProd1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn10, Me.GridColumn11})
        Me.gvProd1.GridControl = Me.gcProductos
        Me.gvProd1.Name = "gvProd1"
        Me.gvProd1.OptionsBehavior.Editable = False
        Me.gvProd1.OptionsView.ShowGroupPanel = False
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Cód.Producto"
        Me.GridColumn10.FieldName = "IdProducto"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 0
        Me.GridColumn10.Width = 184
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Nombre"
        Me.GridColumn11.FieldName = "Nombre"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 1
        Me.GridColumn11.Width = 377
        '
        'meInfo
        '
        Me.meInfo.Location = New System.Drawing.Point(378, 273)
        Me.meInfo.Name = "meInfo"
        Me.meInfo.Properties.ReadOnly = True
        Me.meInfo.Size = New System.Drawing.Size(219, 170)
        Me.meInfo.TabIndex = 34
        '
        'peFoto1
        '
        Me.peFoto1.Location = New System.Drawing.Point(698, 254)
        Me.peFoto1.Name = "peFoto1"
        Me.peFoto1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch
        Me.peFoto1.Size = New System.Drawing.Size(300, 191)
        ToolTipTitleItem1.Text = "Imagen del producto"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        Me.peFoto1.SuperTip = SuperToolTip1
        Me.peFoto1.TabIndex = 36
        Me.peFoto1.ToolTipTitle = "Doble clic para cambiar o cargar imagen"
        '
        'teUnidad1
        '
        Me.teUnidad1.Location = New System.Drawing.Point(102, 297)
        Me.teUnidad1.Name = "teUnidad1"
        Me.teUnidad1.Properties.ReadOnly = True
        Me.teUnidad1.Size = New System.Drawing.Size(270, 20)
        Me.teUnidad1.TabIndex = 27
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(11, 301)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl7.TabIndex = 23
        Me.LabelControl7.Text = "Unidad de Medida:"
        '
        'teUnidadesPre
        '
        Me.teUnidadesPre.Location = New System.Drawing.Point(102, 382)
        Me.teUnidadesPre.Name = "teUnidadesPre"
        Me.teUnidadesPre.Properties.ReadOnly = True
        Me.teUnidadesPre.Size = New System.Drawing.Size(270, 20)
        Me.teUnidadesPre.TabIndex = 32
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(381, 257)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl10.TabIndex = 25
        Me.LabelControl10.Text = "Información Adicional:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(46, 406)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl8.TabIndex = 18
        Me.LabelControl8.Text = "Proveedor:"
        '
        'teColor
        '
        Me.teColor.Location = New System.Drawing.Point(102, 361)
        Me.teColor.Name = "teColor"
        Me.teColor.Properties.ReadOnly = True
        Me.teColor.Size = New System.Drawing.Size(270, 20)
        Me.teColor.TabIndex = 31
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(71, 364)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl11.TabIndex = 24
        Me.LabelControl11.Text = "Color:"
        '
        'teProveedor1
        '
        Me.teProveedor1.Location = New System.Drawing.Point(102, 403)
        Me.teProveedor1.Name = "teProveedor1"
        Me.teProveedor1.Properties.ReadOnly = True
        Me.teProveedor1.Size = New System.Drawing.Size(270, 20)
        Me.teProveedor1.TabIndex = 33
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(9, 385)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl12.TabIndex = 15
        Me.LabelControl12.Text = "Uni.  Presentación:"
        '
        'teEstilo
        '
        Me.teEstilo.Location = New System.Drawing.Point(102, 340)
        Me.teEstilo.Name = "teEstilo"
        Me.teEstilo.Properties.ReadOnly = True
        Me.teEstilo.Size = New System.Drawing.Size(270, 20)
        Me.teEstilo.TabIndex = 30
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(71, 344)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl14.TabIndex = 16
        Me.LabelControl14.Text = "Estilo:"
        '
        'teTalla
        '
        Me.teTalla.Location = New System.Drawing.Point(102, 319)
        Me.teTalla.Name = "teTalla"
        Me.teTalla.Properties.ReadOnly = True
        Me.teTalla.Size = New System.Drawing.Size(270, 20)
        Me.teTalla.TabIndex = 29
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(74, 323)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl17.TabIndex = 21
        Me.LabelControl17.Text = "Talla:"
        '
        'teMarca
        '
        Me.teMarca.Location = New System.Drawing.Point(102, 424)
        Me.teMarca.Name = "teMarca"
        Me.teMarca.Properties.ReadOnly = True
        Me.teMarca.Size = New System.Drawing.Size(270, 20)
        Me.teMarca.TabIndex = 28
        Me.teMarca.Visible = False
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(67, 427)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl18.TabIndex = 22
        Me.LabelControl18.Text = "Marca:"
        Me.LabelControl18.Visible = False
        '
        'teSubGrupo1
        '
        Me.teSubGrupo1.Location = New System.Drawing.Point(102, 276)
        Me.teSubGrupo1.Name = "teSubGrupo1"
        Me.teSubGrupo1.Properties.ReadOnly = True
        Me.teSubGrupo1.Size = New System.Drawing.Size(270, 20)
        Me.teSubGrupo1.TabIndex = 26
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(45, 279)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl19.TabIndex = 19
        Me.LabelControl19.Text = "Sub-Grupo:"
        '
        'teGrupo1
        '
        Me.teGrupo1.Location = New System.Drawing.Point(102, 255)
        Me.teGrupo1.Name = "teGrupo1"
        Me.teGrupo1.Properties.ReadOnly = True
        Me.teGrupo1.Size = New System.Drawing.Size(270, 20)
        Me.teGrupo1.TabIndex = 20
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(67, 259)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl20.TabIndex = 17
        Me.LabelControl20.Text = "Grupo:"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.teCantidadUltCompra)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl16)
        Me.XtraTabPage2.Controls.Add(Me.teFacturaUltCompra)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl15)
        Me.XtraTabPage2.Controls.Add(Me.teUnidad)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl3)
        Me.XtraTabPage2.Controls.Add(Me.teFechaUltCompra)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl5)
        Me.XtraTabPage2.Controls.Add(Me.tePrecioCosto)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl13)
        Me.XtraTabPage2.Controls.Add(Me.teProveedor)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl9)
        Me.XtraTabPage2.Controls.Add(Me.teSubGrupo)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl4)
        Me.XtraTabPage2.Controls.Add(Me.teGrupo)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl6)
        Me.XtraTabPage2.Controls.Add(Me.gcEx)
        Me.XtraTabPage2.Controls.Add(Me.gcPr)
        Me.XtraTabPage2.Controls.Add(Me.gcVentas)
        Me.XtraTabPage2.Controls.Add(Me.peFoto)
        Me.XtraTabPage2.Controls.Add(Me.gcProd)
        Me.XtraTabPage2.Controls.Add(Me.GroupControl1)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1134, 530)
        Me.XtraTabPage2.Text = "Historico de Ventas por Productos"
        '
        'teCantidadUltCompra
        '
        Me.teCantidadUltCompra.Location = New System.Drawing.Point(852, 476)
        Me.teCantidadUltCompra.Name = "teCantidadUltCompra"
        Me.teCantidadUltCompra.Properties.ReadOnly = True
        Me.teCantidadUltCompra.Size = New System.Drawing.Size(111, 20)
        Me.teCantidadUltCompra.TabIndex = 74
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(760, 479)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(90, 13)
        Me.LabelControl16.TabIndex = 73
        Me.LabelControl16.Text = "Ulti.Cant. Compra:"
        '
        'teFacturaUltCompra
        '
        Me.teFacturaUltCompra.Location = New System.Drawing.Point(852, 455)
        Me.teFacturaUltCompra.Name = "teFacturaUltCompra"
        Me.teFacturaUltCompra.Properties.ReadOnly = True
        Me.teFacturaUltCompra.Size = New System.Drawing.Size(111, 20)
        Me.teFacturaUltCompra.TabIndex = 75
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(761, 459)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl15.TabIndex = 72
        Me.LabelControl15.Text = "Ult. Fact. Compra:"
        '
        'teUnidad
        '
        Me.teUnidad.Location = New System.Drawing.Point(852, 371)
        Me.teUnidad.Name = "teUnidad"
        Me.teUnidad.Properties.ReadOnly = True
        Me.teUnidad.Size = New System.Drawing.Size(258, 20)
        Me.teUnidad.TabIndex = 68
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(761, 375)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl3.TabIndex = 65
        Me.LabelControl3.Text = "Unidad de Medida:"
        '
        'teFechaUltCompra
        '
        Me.teFechaUltCompra.Location = New System.Drawing.Point(852, 413)
        Me.teFechaUltCompra.Name = "teFechaUltCompra"
        Me.teFechaUltCompra.Properties.ReadOnly = True
        Me.teFechaUltCompra.Size = New System.Drawing.Size(111, 20)
        Me.teFechaUltCompra.TabIndex = 70
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(796, 437)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl5.TabIndex = 63
        Me.LabelControl5.Text = "Proveedor:"
        '
        'tePrecioCosto
        '
        Me.tePrecioCosto.Location = New System.Drawing.Point(852, 392)
        Me.tePrecioCosto.Name = "tePrecioCosto"
        Me.tePrecioCosto.Properties.ReadOnly = True
        Me.tePrecioCosto.Size = New System.Drawing.Size(111, 20)
        Me.tePrecioCosto.TabIndex = 69
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(786, 398)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl13.TabIndex = 64
        Me.LabelControl13.Text = "Precio Costo:"
        '
        'teProveedor
        '
        Me.teProveedor.Location = New System.Drawing.Point(852, 434)
        Me.teProveedor.Name = "teProveedor"
        Me.teProveedor.Properties.ReadOnly = True
        Me.teProveedor.Size = New System.Drawing.Size(258, 20)
        Me.teProveedor.TabIndex = 71
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(757, 417)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(93, 13)
        Me.LabelControl9.TabIndex = 61
        Me.LabelControl9.Text = "Fecha Ult. Compra:"
        '
        'teSubGrupo
        '
        Me.teSubGrupo.Location = New System.Drawing.Point(852, 350)
        Me.teSubGrupo.Name = "teSubGrupo"
        Me.teSubGrupo.Properties.ReadOnly = True
        Me.teSubGrupo.Size = New System.Drawing.Size(258, 20)
        Me.teSubGrupo.TabIndex = 67
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(795, 353)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl4.TabIndex = 62
        Me.LabelControl4.Text = "Sub-Grupo:"
        '
        'teGrupo
        '
        Me.teGrupo.Location = New System.Drawing.Point(852, 329)
        Me.teGrupo.Name = "teGrupo"
        Me.teGrupo.Properties.ReadOnly = True
        Me.teGrupo.Size = New System.Drawing.Size(258, 20)
        Me.teGrupo.TabIndex = 66
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(817, 333)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl6.TabIndex = 60
        Me.LabelControl6.Text = "Grupo:"
        '
        'gcEx
        '
        Me.gcEx.Location = New System.Drawing.Point(759, 208)
        Me.gcEx.MainView = Me.gvEx
        Me.gcEx.Name = "gcEx"
        Me.gcEx.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemLookUpEdit1})
        Me.gcEx.Size = New System.Drawing.Size(300, 119)
        Me.gcEx.TabIndex = 59
        Me.gcEx.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvEx})
        '
        'gvEx
        '
        Me.gvEx.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcBodega, Me.gcSaldo})
        Me.gvEx.GridControl = Me.gcEx
        Me.gvEx.Name = "gvEx"
        Me.gvEx.OptionsBehavior.Editable = False
        Me.gvEx.OptionsView.ShowGroupPanel = False
        '
        'gcBodega
        '
        Me.gcBodega.Caption = "Bodega"
        Me.gcBodega.FieldName = "Bodega"
        Me.gcBodega.Name = "gcBodega"
        Me.gcBodega.OptionsColumn.AllowEdit = False
        Me.gcBodega.Visible = True
        Me.gcBodega.VisibleIndex = 0
        Me.gcBodega.Width = 167
        '
        'gcSaldo
        '
        Me.gcSaldo.Caption = "Saldo"
        Me.gcSaldo.DisplayFormat.FormatString = "###,##0.00"
        Me.gcSaldo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcSaldo.FieldName = "Saldo"
        Me.gcSaldo.Name = "gcSaldo"
        Me.gcSaldo.Visible = True
        Me.gcSaldo.VisibleIndex = 1
        Me.gcSaldo.Width = 112
        '
        'RepositoryItemLookUpEdit1
        '
        Me.RepositoryItemLookUpEdit1.AutoHeight = False
        Me.RepositoryItemLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit1.Name = "RepositoryItemLookUpEdit1"
        '
        'gcPr
        '
        Me.gcPr.Location = New System.Drawing.Point(759, 82)
        Me.gcPr.MainView = Me.gvPr
        Me.gcPr.Name = "gcPr"
        Me.gcPr.Size = New System.Drawing.Size(300, 123)
        Me.gcPr.TabIndex = 58
        Me.gcPr.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvPr})
        '
        'gvPr
        '
        Me.gvPr.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcPrecio, Me.gcValor})
        Me.gvPr.GridControl = Me.gcPr
        Me.gvPr.Name = "gvPr"
        Me.gvPr.OptionsBehavior.Editable = False
        Me.gvPr.OptionsView.ShowGroupPanel = False
        '
        'gcPrecio
        '
        Me.gcPrecio.Caption = "Precio"
        Me.gcPrecio.FieldName = "Precio"
        Me.gcPrecio.Name = "gcPrecio"
        Me.gcPrecio.OptionsColumn.AllowEdit = False
        Me.gcPrecio.Visible = True
        Me.gcPrecio.VisibleIndex = 0
        Me.gcPrecio.Width = 167
        '
        'gcValor
        '
        Me.gcValor.Caption = "Valor"
        Me.gcValor.DisplayFormat.FormatString = "n2"
        Me.gcValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcValor.FieldName = "Valor"
        Me.gcValor.Name = "gcValor"
        Me.gcValor.Visible = True
        Me.gcValor.VisibleIndex = 1
        Me.gcValor.Width = 112
        '
        'gcVentas
        '
        Me.gcVentas.Location = New System.Drawing.Point(2, 283)
        Me.gcVentas.MainView = Me.gvVentas
        Me.gcVentas.Name = "gcVentas"
        Me.gcVentas.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.rileIdPrecio})
        Me.gcVentas.Size = New System.Drawing.Size(751, 214)
        Me.gcVentas.TabIndex = 56
        Me.gcVentas.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvVentas})
        '
        'gvVentas
        '
        Me.gvVentas.GridControl = Me.gcVentas
        Me.gvVentas.Name = "gvVentas"
        Me.gvVentas.OptionsBehavior.Editable = False
        Me.gvVentas.OptionsView.ShowGroupPanel = False
        '
        'rileIdPrecio
        '
        Me.rileIdPrecio.AutoHeight = False
        Me.rileIdPrecio.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.rileIdPrecio.Name = "rileIdPrecio"
        '
        'peFoto
        '
        Me.peFoto.Location = New System.Drawing.Point(521, 82)
        Me.peFoto.Name = "peFoto"
        Me.peFoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch
        Me.peFoto.Size = New System.Drawing.Size(232, 195)
        ToolTipTitleItem2.Text = "Imagen del producto"
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        Me.peFoto.SuperTip = SuperToolTip2
        Me.peFoto.TabIndex = 57
        Me.peFoto.ToolTipTitle = "Doble clic para cambiar o cargar imagen"
        '
        'gcProd
        '
        Me.gcProd.Location = New System.Drawing.Point(2, 82)
        Me.gcProd.MainView = Me.gvProd
        Me.gcProd.Name = "gcProd"
        Me.gcProd.Size = New System.Drawing.Size(513, 195)
        Me.gcProd.TabIndex = 55
        Me.gcProd.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvProd})
        '
        'gvProd
        '
        Me.gvProd.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6})
        Me.gvProd.GridControl = Me.gcProd
        Me.gvProd.Name = "gvProd"
        Me.gvProd.OptionsBehavior.Editable = False
        Me.gvProd.OptionsView.ShowAutoFilterRow = True
        Me.gvProd.OptionsView.ShowFooter = True
        Me.gvProd.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Código"
        Me.GridColumn1.FieldName = "IdProducto"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Nombre"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Proveedor"
        Me.GridColumn3.FieldName = "Proveedor"
        Me.GridColumn3.Name = "GridColumn3"
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Existencia"
        Me.GridColumn4.FieldName = "Precio RBD"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Existencia", "{n:0}")})
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Precio KG"
        Me.GridColumn5.FieldName = "PrecioKG"
        Me.GridColumn5.Name = "GridColumn5"
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Precio Autorizado"
        Me.GridColumn6.FieldName = "PrecioAutorizado"
        Me.GridColumn6.Name = "GridColumn6"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btnGenerar)
        Me.GroupControl1.Controls.Add(Me.teNombre)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.teCodigo)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1134, 72)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Parametros de la Consulta"
        '
        'btnGenerar
        '
        Me.btnGenerar.Location = New System.Drawing.Point(283, 44)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(108, 23)
        Me.btnGenerar.TabIndex = 7
        Me.btnGenerar.Text = "Generar consulta"
        '
        'teNombre
        '
        Me.teNombre.EditValue = ""
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(49, 46)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(231, 20)
        Me.teNombre.TabIndex = 6
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(5, 48)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "Nombre:"
        '
        'teCodigo
        '
        Me.teCodigo.EditValue = ""
        Me.teCodigo.EnterMoveNextControl = True
        Me.teCodigo.Location = New System.Drawing.Point(49, 25)
        Me.teCodigo.Name = "teCodigo"
        Me.teCodigo.Size = New System.Drawing.Size(231, 20)
        Me.teCodigo.TabIndex = 4
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(9, 28)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl1.TabIndex = 3
        Me.LabelControl1.Text = "Código:"
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.gcDetalle)
        Me.XtraTabPage3.Controls.Add(Me.PanelControl1)
        Me.XtraTabPage3.Controls.Add(Me.GroupControl2)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(1134, 530)
        Me.XtraTabPage3.Text = "Consulta Gerencial"
        '
        'gcDetalle
        '
        Me.gcDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcDetalle.Location = New System.Drawing.Point(0, 281)
        Me.gcDetalle.MainView = Me.gvDetalle
        Me.gcDetalle.Name = "gcDetalle"
        Me.gcDetalle.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemLookUpEdit2})
        Me.gcDetalle.Size = New System.Drawing.Size(1134, 249)
        Me.gcDetalle.TabIndex = 14
        Me.gcDetalle.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvDetalle})
        '
        'gvDetalle
        '
        Me.gvDetalle.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcRef, Me.gcDesc, Me.gcCant, Me.gcPrecioU, Me.gcPrecioT, Me.gcUtilidad, Me.gcPorcUtilidad, Me.gcCosto})
        Me.gvDetalle.GridControl = Me.gcDetalle
        Me.gvDetalle.Name = "gvDetalle"
        Me.gvDetalle.OptionsBehavior.Editable = False
        Me.gvDetalle.OptionsView.ShowFooter = True
        Me.gvDetalle.OptionsView.ShowGroupPanel = False
        '
        'gcRef
        '
        Me.gcRef.Caption = "Referencia"
        Me.gcRef.FieldName = "Referencia"
        Me.gcRef.Name = "gcRef"
        Me.gcRef.Visible = True
        Me.gcRef.VisibleIndex = 0
        '
        'gcDesc
        '
        Me.gcDesc.Caption = "Descripcion"
        Me.gcDesc.FieldName = "Descripcion"
        Me.gcDesc.Name = "gcDesc"
        Me.gcDesc.Visible = True
        Me.gcDesc.VisibleIndex = 1
        '
        'gcCant
        '
        Me.gcCant.Caption = "Cantidad"
        Me.gcCant.DisplayFormat.FormatString = "n2"
        Me.gcCant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcCant.FieldName = "Cantidad"
        Me.gcCant.Name = "gcCant"
        Me.gcCant.Visible = True
        Me.gcCant.VisibleIndex = 2
        '
        'gcPrecioU
        '
        Me.gcPrecioU.Caption = "Precio Unit."
        Me.gcPrecioU.DisplayFormat.FormatString = "n2"
        Me.gcPrecioU.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioU.FieldName = "PrecioUnitario"
        Me.gcPrecioU.Name = "gcPrecioU"
        Me.gcPrecioU.Visible = True
        Me.gcPrecioU.VisibleIndex = 3
        '
        'gcPrecioT
        '
        Me.gcPrecioT.Caption = "Precio Total"
        Me.gcPrecioT.DisplayFormat.FormatString = "n2"
        Me.gcPrecioT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioT.FieldName = "PrecioTotal"
        Me.gcPrecioT.Name = "gcPrecioT"
        Me.gcPrecioT.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PrecioTotal", "{0:c2}")})
        Me.gcPrecioT.Visible = True
        Me.gcPrecioT.VisibleIndex = 4
        '
        'gcUtilidad
        '
        Me.gcUtilidad.Caption = "Utilidad"
        Me.gcUtilidad.DisplayFormat.FormatString = "n2"
        Me.gcUtilidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcUtilidad.FieldName = "Utilidad"
        Me.gcUtilidad.Name = "gcUtilidad"
        Me.gcUtilidad.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Utilidad", "{0:c2}")})
        Me.gcUtilidad.Visible = True
        Me.gcUtilidad.VisibleIndex = 6
        '
        'gcPorcUtilidad
        '
        Me.gcPorcUtilidad.Caption = "% Utilidad"
        Me.gcPorcUtilidad.DisplayFormat.FormatString = "n2"
        Me.gcPorcUtilidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPorcUtilidad.FieldName = "PorcUtilidad"
        Me.gcPorcUtilidad.Name = "gcPorcUtilidad"
        Me.gcPorcUtilidad.Visible = True
        Me.gcPorcUtilidad.VisibleIndex = 7
        '
        'gcCosto
        '
        Me.gcCosto.Caption = "Costo"
        Me.gcCosto.DisplayFormat.FormatString = "n2"
        Me.gcCosto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcCosto.FieldName = "Costo"
        Me.gcCosto.Name = "gcCosto"
        Me.gcCosto.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Costo", "{0:n2}")})
        Me.gcCosto.Visible = True
        Me.gcCosto.VisibleIndex = 5
        '
        'RepositoryItemLookUpEdit2
        '
        Me.RepositoryItemLookUpEdit2.AutoHeight = False
        Me.RepositoryItemLookUpEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit2.Name = "RepositoryItemLookUpEdit2"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.peFoto2)
        Me.PanelControl1.Controls.Add(Me.gcPed)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 56)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1134, 225)
        Me.PanelControl1.TabIndex = 9
        '
        'peFoto2
        '
        Me.peFoto2.Location = New System.Drawing.Point(816, 0)
        Me.peFoto2.Name = "peFoto2"
        Me.peFoto2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch
        Me.peFoto2.Size = New System.Drawing.Size(304, 222)
        ToolTipTitleItem3.Text = "Imagen del producto"
        SuperToolTip3.Items.Add(ToolTipTitleItem3)
        Me.peFoto2.SuperTip = SuperToolTip3
        Me.peFoto2.TabIndex = 15
        Me.peFoto2.ToolTipTitle = "Doble clic para cambiar o cargar imagen"
        '
        'gcPed
        '
        Me.gcPed.Dock = System.Windows.Forms.DockStyle.Left
        Me.gcPed.Location = New System.Drawing.Point(2, 2)
        Me.gcPed.MainView = Me.gvPed
        Me.gcPed.Name = "gcPed"
        Me.gcPed.Size = New System.Drawing.Size(811, 221)
        Me.gcPed.TabIndex = 0
        Me.gcPed.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvPed})
        '
        'gvPed
        '
        Me.gvPed.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcPedido, Me.gcNumero, Me.gcNombre, Me.gcFecha, Me.gcVendedor, Me.gcTotal, Me.gcIdComprobante})
        Me.gvPed.GridControl = Me.gcPed
        Me.gvPed.Name = "gvPed"
        Me.gvPed.OptionsBehavior.Editable = False
        Me.gvPed.OptionsView.ShowGroupPanel = False
        '
        'gcPedido
        '
        Me.gcPedido.Caption = "No. Pedido"
        Me.gcPedido.FieldName = "Pedido"
        Me.gcPedido.Name = "gcPedido"
        Me.gcPedido.Visible = True
        Me.gcPedido.VisibleIndex = 0
        '
        'gcNumero
        '
        Me.gcNumero.Caption = "No.Factura"
        Me.gcNumero.FieldName = "Numero"
        Me.gcNumero.Name = "gcNumero"
        Me.gcNumero.Visible = True
        Me.gcNumero.VisibleIndex = 1
        '
        'gcNombre
        '
        Me.gcNombre.Caption = "Nombre"
        Me.gcNombre.FieldName = "Nombre"
        Me.gcNombre.Name = "gcNombre"
        Me.gcNombre.Visible = True
        Me.gcNombre.VisibleIndex = 2
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha"
        Me.gcFecha.FieldName = "Fecha"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 3
        '
        'gcVendedor
        '
        Me.gcVendedor.Caption = "Vendedor"
        Me.gcVendedor.FieldName = "Vendedor"
        Me.gcVendedor.Name = "gcVendedor"
        Me.gcVendedor.Visible = True
        Me.gcVendedor.VisibleIndex = 4
        '
        'gcTotal
        '
        Me.gcTotal.Caption = "Total"
        Me.gcTotal.FieldName = "Total"
        Me.gcTotal.Name = "gcTotal"
        Me.gcTotal.Visible = True
        Me.gcTotal.VisibleIndex = 5
        '
        'gcIdComprobante
        '
        Me.gcIdComprobante.Caption = "IdComprobante"
        Me.gcIdComprobante.FieldName = "IdComprobante"
        Me.gcIdComprobante.Name = "gcIdComprobante"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.leTipoDocumento)
        Me.GroupControl2.Controls.Add(Me.LabelControl21)
        Me.GroupControl2.Controls.Add(Me.btGenerar)
        Me.GroupControl2.Controls.Add(Me.deHasta)
        Me.GroupControl2.Controls.Add(Me.LabelControl22)
        Me.GroupControl2.Controls.Add(Me.deDesde)
        Me.GroupControl2.Controls.Add(Me.LabelControl23)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(1134, 56)
        Me.GroupControl2.TabIndex = 0
        Me.GroupControl2.Text = "Parametros de la Consulta"
        '
        'leTipoDocumento
        '
        Me.leTipoDocumento.Location = New System.Drawing.Point(416, 28)
        Me.leTipoDocumento.Name = "leTipoDocumento"
        Me.leTipoDocumento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoDocumento.Size = New System.Drawing.Size(227, 20)
        Me.leTipoDocumento.TabIndex = 17
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(317, 31)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(96, 13)
        Me.LabelControl21.TabIndex = 16
        Me.LabelControl21.Text = "Tipo de Documento:"
        '
        'btGenerar
        '
        Me.btGenerar.Location = New System.Drawing.Point(692, 27)
        Me.btGenerar.Name = "btGenerar"
        Me.btGenerar.Size = New System.Drawing.Size(121, 23)
        Me.btGenerar.TabIndex = 13
        Me.btGenerar.Text = "Generar la Consulta"
        '
        'deHasta
        '
        Me.deHasta.EditValue = Nothing
        Me.deHasta.EnterMoveNextControl = True
        Me.deHasta.Location = New System.Drawing.Point(200, 27)
        Me.deHasta.Name = "deHasta"
        Me.deHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deHasta.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deHasta.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deHasta.Size = New System.Drawing.Size(92, 20)
        Me.deHasta.TabIndex = 11
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(163, 30)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl22.TabIndex = 14
        Me.LabelControl22.Text = "Desde:"
        '
        'deDesde
        '
        Me.deDesde.EditValue = Nothing
        Me.deDesde.EnterMoveNextControl = True
        Me.deDesde.Location = New System.Drawing.Point(42, 27)
        Me.deDesde.Name = "deDesde"
        Me.deDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deDesde.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deDesde.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deDesde.Size = New System.Drawing.Size(92, 20)
        Me.deDesde.TabIndex = 10
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(5, 30)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl23.TabIndex = 15
        Me.LabelControl23.Text = "Desde:"
        '
        'fac_frmConsultaProductosBase
        '
        Me.ClientSize = New System.Drawing.Size(1140, 581)
        Me.Controls.Add(Me.xtlConsultas)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmConsultaProductosBase"
        Me.OptionId = ""
        Me.Text = "Consulta de productos"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.xtlConsultas, 0)
        CType(Me.xtlConsultas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtlConsultas.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.XtraTabPage1.PerformLayout()
        CType(Me.rgTipoBusqueda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcEx1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvEx1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcPr1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPr1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rileIdPrecio1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvProd1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meInfo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peFoto1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teUnidad1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teUnidadesPre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teColor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teProveedor1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teEstilo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTalla.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teMarca.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSubGrupo1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teGrupo1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        Me.XtraTabPage2.PerformLayout()
        CType(Me.teCantidadUltCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teFacturaUltCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teUnidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teFechaUltCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePrecioCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSubGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teGrupo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcEx, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvEx, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcPr, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPr, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rileIdPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peFoto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.gcDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemLookUpEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.peFoto2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcPed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.leTipoDocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents xtlConsultas As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents teCantidadUltCompra As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teFacturaUltCompra As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teUnidad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teFechaUltCompra As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tePrecioCosto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teProveedor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teSubGrupo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teGrupo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcEx As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvEx As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcBodega As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcSaldo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents gcPr As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvPr As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcVentas As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvVentas As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents rileIdPrecio As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents peFoto As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents gcProd As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvProd As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnGenerar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcEx1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvEx1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPr1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvPr1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents rileIdPrecio1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcProductos As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvProd1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents meInfo As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents peFoto1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents teUnidad1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teUnidadesPre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teColor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teProveedor1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teEstilo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTalla As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teMarca As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teSubGrupo1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teGrupo1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcDetalle As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvDetalle As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcRef As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDesc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioU As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcUtilidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPorcUtilidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemLookUpEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents peFoto2 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents gcPed As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvPed As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcPedido As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcVendedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btGenerar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents deHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoDocumento As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcCosto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgTipoBusqueda As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents txtCodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit

End Class
