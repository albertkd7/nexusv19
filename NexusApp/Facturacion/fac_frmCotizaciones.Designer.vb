﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmCotizaciones
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xtcCotizaciones = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage()
        Me.gc2 = New DevExpress.XtraGrid.GridControl()
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcCodProducto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdPrecio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ritePrecioVenta = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteDescuento = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ritePrecioUnitario = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcPrecioTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcVentaNeta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcValorIva = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.ceRetencionPercepcion = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.teIVA = New DevExpress.XtraEditors.TextEdit()
        Me.lblPercReten = New DevExpress.XtraEditors.LabelControl()
        Me.teRetencionPercepcion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.teTotal = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoDoc = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoVenta = New DevExpress.XtraEditors.LookUpEdit()
        Me.ceImprimePrecios = New DevExpress.XtraEditors.CheckEdit()
        Me.meConcepto = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.teProyecto = New DevExpress.XtraEditors.TextEdit()
        Me.teGarantia = New DevExpress.XtraEditors.TextEdit()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.leVendedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.seDiasCredito = New DevExpress.XtraEditors.SpinEdit()
        Me.leFormaPago = New DevExpress.XtraEditors.LookUpEdit()
        Me.meNombre = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.beCliente = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.lblNRC_DUI = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teNRC = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teTiempo = New DevExpress.XtraEditors.TextEdit()
        Me.teDireccion = New DevExpress.XtraEditors.TextEdit()
        Me.teAtencion = New DevExpress.XtraEditors.TextEdit()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.xtcCotizaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcCotizaciones.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePrecioVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteDescuento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePrecioUnitario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.ceRetencionPercepcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIVA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teRetencionPercepcion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoDoc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceImprimePrecios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teProyecto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teGarantia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTiempo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teAtencion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcCotizaciones
        '
        Me.xtcCotizaciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcCotizaciones.Location = New System.Drawing.Point(0, 0)
        Me.xtcCotizaciones.Name = "xtcCotizaciones"
        Me.xtcCotizaciones.SelectedTabPage = Me.xtpLista
        Me.xtcCotizaciones.Size = New System.Drawing.Size(872, 461)
        Me.xtcCotizaciones.TabIndex = 4
        Me.xtcCotizaciones.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc2)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(866, 433)
        Me.xtpLista.Text = "Consulta Cotizaciones"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(866, 433)
        Me.gc2.TabIndex = 3
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn3, Me.GridColumn4, Me.GridColumn8, Me.GridColumn5, Me.GridColumn2, Me.GridColumn9, Me.GridColumn6, Me.GridColumn7})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdComprobante"
        Me.GridColumn1.FieldName = "IdComprobante"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 59
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Numero"
        Me.GridColumn3.FieldName = "Numero"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        Me.GridColumn3.Width = 85
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Fecha"
        Me.GridColumn4.FieldName = "Fecha"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 2
        Me.GridColumn4.Width = 81
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Sucursal"
        Me.GridColumn8.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn8.FieldName = "IdSucursal"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 3
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Cliente"
        Me.GridColumn5.FieldName = "Nombre"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 4
        Me.GridColumn5.Width = 440
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Atencion A"
        Me.GridColumn2.FieldName = "AtencionA"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 5
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Total Cotización"
        Me.GridColumn9.FieldName = "TotalComprobante"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 6
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Creado Por"
        Me.GridColumn6.FieldName = "CreadoPor"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 7
        Me.GridColumn6.Width = 98
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Fecha Hora Creacion"
        Me.GridColumn7.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.GridColumn7.FieldName = "FechaHoraCreacion"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 8
        Me.GridColumn7.Width = 114
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemDateEdit1.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.gc)
        Me.xtpDatos.Controls.Add(Me.PanelControl2)
        Me.xtpDatos.Controls.Add(Me.PanelControl1)
        Me.xtpDatos.Controls.Add(Me.pcHeader)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(866, 433)
        Me.xtpDatos.Text = "Cotizaciones"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.First.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.Last.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.Next.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.NextPage.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.Prev.Visible = False
        Me.gc.EmbeddedNavigator.Buttons.PrevPage.Visible = False
        Me.gc.Location = New System.Drawing.Point(0, 225)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.ritePrecioVenta, Me.riteDescuento, Me.ritePrecioUnitario})
        Me.gc.Size = New System.Drawing.Size(836, 145)
        Me.gc.TabIndex = 10
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcCodProducto, Me.gcIdPrecio, Me.gcCantidad, Me.gcDescripcion, Me.GridColumn10, Me.GridColumn11, Me.gcPrecioUnitario, Me.gcPrecioTotal, Me.gcVentaNeta, Me.gcValorIva, Me.GridColumn12, Me.GridColumn13, Me.GridColumn14})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click
        Me.gv.OptionsNavigation.AutoFocusNewRow = True
        Me.gv.OptionsNavigation.EnterMoveNextColumn = True
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcCodProducto
        '
        Me.gcCodProducto.Caption = "Cód.Producto"
        Me.gcCodProducto.FieldName = "IdProducto"
        Me.gcCodProducto.Name = "gcCodProducto"
        Me.gcCodProducto.Visible = True
        Me.gcCodProducto.VisibleIndex = 0
        Me.gcCodProducto.Width = 106
        '
        'gcIdPrecio
        '
        Me.gcIdPrecio.Caption = "T/P"
        Me.gcIdPrecio.FieldName = "IdPrecio"
        Me.gcIdPrecio.Name = "gcIdPrecio"
        Me.gcIdPrecio.Visible = True
        Me.gcIdPrecio.VisibleIndex = 2
        Me.gcIdPrecio.Width = 38
        '
        'gcCantidad
        '
        Me.gcCantidad.Caption = "Cantidad"
        Me.gcCantidad.FieldName = "Cantidad"
        Me.gcCantidad.Name = "gcCantidad"
        Me.gcCantidad.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.gcCantidad.Visible = True
        Me.gcCantidad.VisibleIndex = 1
        Me.gcCantidad.Width = 61
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Descripción"
        Me.gcDescripcion.FieldName = "Descripcion"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 3
        Me.gcDescripcion.Width = 319
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Precio Venta"
        Me.GridColumn10.ColumnEdit = Me.ritePrecioVenta
        Me.GridColumn10.FieldName = "PrecioVenta"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 5
        Me.GridColumn10.Width = 64
        '
        'ritePrecioVenta
        '
        Me.ritePrecioVenta.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.ritePrecioVenta.AutoHeight = False
        Me.ritePrecioVenta.Mask.EditMask = "n6"
        Me.ritePrecioVenta.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePrecioVenta.Mask.UseMaskAsDisplayFormat = True
        Me.ritePrecioVenta.Name = "ritePrecioVenta"
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "% Descuento"
        Me.GridColumn11.ColumnEdit = Me.riteDescuento
        Me.GridColumn11.FieldName = "PorcDescuento"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 6
        Me.GridColumn11.Width = 64
        '
        'riteDescuento
        '
        Me.riteDescuento.AutoHeight = False
        Me.riteDescuento.Mask.EditMask = "n2"
        Me.riteDescuento.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteDescuento.Mask.UseMaskAsDisplayFormat = True
        Me.riteDescuento.Name = "riteDescuento"
        '
        'gcPrecioUnitario
        '
        Me.gcPrecioUnitario.Caption = "Precio Unitario"
        Me.gcPrecioUnitario.ColumnEdit = Me.ritePrecioUnitario
        Me.gcPrecioUnitario.DisplayFormat.FormatString = "n2"
        Me.gcPrecioUnitario.FieldName = "PrecioUnitario"
        Me.gcPrecioUnitario.Name = "gcPrecioUnitario"
        Me.gcPrecioUnitario.Visible = True
        Me.gcPrecioUnitario.VisibleIndex = 7
        Me.gcPrecioUnitario.Width = 73
        '
        'ritePrecioUnitario
        '
        Me.ritePrecioUnitario.AutoHeight = False
        Me.ritePrecioUnitario.Mask.EditMask = "n6"
        Me.ritePrecioUnitario.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePrecioUnitario.Mask.UseMaskAsDisplayFormat = True
        Me.ritePrecioUnitario.Name = "ritePrecioUnitario"
        '
        'gcPrecioTotal
        '
        Me.gcPrecioTotal.Caption = "Precio Total"
        Me.gcPrecioTotal.DisplayFormat.FormatString = "n2"
        Me.gcPrecioTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioTotal.FieldName = "PrecioTotal"
        Me.gcPrecioTotal.Name = "gcPrecioTotal"
        Me.gcPrecioTotal.OptionsColumn.ReadOnly = True
        Me.gcPrecioTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PrecioTotal", "{0:c2}")})
        Me.gcPrecioTotal.Visible = True
        Me.gcPrecioTotal.VisibleIndex = 8
        Me.gcPrecioTotal.Width = 93
        '
        'gcVentaNeta
        '
        Me.gcVentaNeta.Caption = "Venta Neta"
        Me.gcVentaNeta.FieldName = "VentaNeta"
        Me.gcVentaNeta.Name = "gcVentaNeta"
        Me.gcVentaNeta.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "VentaNeta", "{0:c}")})
        '
        'gcValorIva
        '
        Me.gcValorIva.Caption = "Valor IVA"
        Me.gcValorIva.FieldName = "ValorIva"
        Me.gcValorIva.Name = "gcValorIva"
        Me.gcValorIva.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorIva", "{0:c}")})
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "DescuentoAutorizadoPor"
        Me.GridColumn12.FieldName = "DescuentoAutorizadoPor"
        Me.GridColumn12.Name = "GridColumn12"
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Tiempo de Entrega"
        Me.GridColumn13.FieldName = "Especificaciones"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 4
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "ArchivoImagen"
        Me.GridColumn14.FieldName = "ArchivoImagen"
        Me.GridColumn14.Name = "GridColumn14"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.ceRetencionPercepcion)
        Me.PanelControl2.Controls.Add(Me.LabelControl20)
        Me.PanelControl2.Controls.Add(Me.teIVA)
        Me.PanelControl2.Controls.Add(Me.lblPercReten)
        Me.PanelControl2.Controls.Add(Me.teRetencionPercepcion)
        Me.PanelControl2.Controls.Add(Me.LabelControl21)
        Me.PanelControl2.Controls.Add(Me.teTotal)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl2.Location = New System.Drawing.Point(0, 370)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(836, 63)
        Me.PanelControl2.TabIndex = 9
        '
        'ceRetencionPercepcion
        '
        Me.ceRetencionPercepcion.Location = New System.Drawing.Point(6, 6)
        Me.ceRetencionPercepcion.Name = "ceRetencionPercepcion"
        Me.ceRetencionPercepcion.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ceRetencionPercepcion.Properties.Appearance.Options.UseFont = True
        Me.ceRetencionPercepcion.Properties.Caption = "APLICAR RETENCIÓN DEL 1 %"
        Me.ceRetencionPercepcion.Size = New System.Drawing.Size(206, 19)
        Me.ceRetencionPercepcion.TabIndex = 104
        '
        'LabelControl20
        '
        Me.LabelControl20.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl20.Location = New System.Drawing.Point(630, 3)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl20.TabIndex = 45
        Me.LabelControl20.Text = "IVA:"
        '
        'teIVA
        '
        Me.teIVA.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teIVA.Location = New System.Drawing.Point(731, 3)
        Me.teIVA.Name = "teIVA"
        Me.teIVA.Properties.Appearance.Options.UseTextOptions = True
        Me.teIVA.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teIVA.Properties.DisplayFormat.FormatString = "c"
        Me.teIVA.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teIVA.Properties.Mask.EditMask = "c"
        Me.teIVA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teIVA.Properties.ReadOnly = True
        Me.teIVA.Size = New System.Drawing.Size(98, 20)
        Me.teIVA.TabIndex = 44
        '
        'lblPercReten
        '
        Me.lblPercReten.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblPercReten.Location = New System.Drawing.Point(630, 24)
        Me.lblPercReten.Name = "lblPercReten"
        Me.lblPercReten.Size = New System.Drawing.Size(90, 13)
        Me.lblPercReten.TabIndex = 47
        Me.lblPercReten.Text = "(-) IVA RETENIDO:"
        '
        'teRetencionPercepcion
        '
        Me.teRetencionPercepcion.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teRetencionPercepcion.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.teRetencionPercepcion.Location = New System.Drawing.Point(731, 24)
        Me.teRetencionPercepcion.Name = "teRetencionPercepcion"
        Me.teRetencionPercepcion.Properties.Appearance.Options.UseTextOptions = True
        Me.teRetencionPercepcion.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teRetencionPercepcion.Properties.DisplayFormat.FormatString = "c"
        Me.teRetencionPercepcion.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teRetencionPercepcion.Properties.Mask.EditMask = "c"
        Me.teRetencionPercepcion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teRetencionPercepcion.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teRetencionPercepcion.Properties.ReadOnly = True
        Me.teRetencionPercepcion.Size = New System.Drawing.Size(98, 20)
        Me.teRetencionPercepcion.TabIndex = 46
        '
        'LabelControl21
        '
        Me.LabelControl21.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(630, 46)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl21.TabIndex = 49
        Me.LabelControl21.Text = "TOTAL:"
        '
        'teTotal
        '
        Me.teTotal.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teTotal.Location = New System.Drawing.Point(731, 45)
        Me.teTotal.Name = "teTotal"
        Me.teTotal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.teTotal.Properties.Appearance.Options.UseFont = True
        Me.teTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotal.Properties.DisplayFormat.FormatString = "c"
        Me.teTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teTotal.Properties.ReadOnly = True
        Me.teTotal.Size = New System.Drawing.Size(98, 20)
        Me.teTotal.TabIndex = 48
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cmdUp)
        Me.PanelControl1.Controls.Add(Me.cmdDown)
        Me.PanelControl1.Controls.Add(Me.cmdBorrar)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelControl1.Location = New System.Drawing.Point(836, 225)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(30, 208)
        Me.PanelControl1.TabIndex = 8
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(3, 7)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(27, 24)
        Me.cmdUp.TabIndex = 28
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(3, 42)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(27, 24)
        Me.cmdDown.TabIndex = 29
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(3, 77)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 24)
        Me.cmdBorrar.TabIndex = 30
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.LabelControl16)
        Me.pcHeader.Controls.Add(Me.leTipoDoc)
        Me.pcHeader.Controls.Add(Me.LabelControl15)
        Me.pcHeader.Controls.Add(Me.LabelControl24)
        Me.pcHeader.Controls.Add(Me.leTipoVenta)
        Me.pcHeader.Controls.Add(Me.ceImprimePrecios)
        Me.pcHeader.Controls.Add(Me.meConcepto)
        Me.pcHeader.Controls.Add(Me.LabelControl14)
        Me.pcHeader.Controls.Add(Me.LabelControl13)
        Me.pcHeader.Controls.Add(Me.teProyecto)
        Me.pcHeader.Controls.Add(Me.teGarantia)
        Me.pcHeader.Controls.Add(Me.deFecha)
        Me.pcHeader.Controls.Add(Me.leVendedor)
        Me.pcHeader.Controls.Add(Me.LabelControl17)
        Me.pcHeader.Controls.Add(Me.LabelControl11)
        Me.pcHeader.Controls.Add(Me.seDiasCredito)
        Me.pcHeader.Controls.Add(Me.leFormaPago)
        Me.pcHeader.Controls.Add(Me.meNombre)
        Me.pcHeader.Controls.Add(Me.LabelControl4)
        Me.pcHeader.Controls.Add(Me.beCliente)
        Me.pcHeader.Controls.Add(Me.LabelControl10)
        Me.pcHeader.Controls.Add(Me.LabelControl9)
        Me.pcHeader.Controls.Add(Me.LabelControl8)
        Me.pcHeader.Controls.Add(Me.LabelControl7)
        Me.pcHeader.Controls.Add(Me.LabelControl6)
        Me.pcHeader.Controls.Add(Me.lblNRC_DUI)
        Me.pcHeader.Controls.Add(Me.LabelControl3)
        Me.pcHeader.Controls.Add(Me.teNRC)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.teTiempo)
        Me.pcHeader.Controls.Add(Me.teDireccion)
        Me.pcHeader.Controls.Add(Me.teAtencion)
        Me.pcHeader.Controls.Add(Me.teNumero)
        Me.pcHeader.Controls.Add(Me.LabelControl12)
        Me.pcHeader.Controls.Add(Me.teCorrelativo)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(866, 225)
        Me.pcHeader.TabIndex = 7
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(668, 2)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(162, 20)
        Me.leSucursal.TabIndex = 3
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(623, 5)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl16.TabIndex = 94
        Me.LabelControl16.Text = "Sucursal:"
        '
        'leTipoDoc
        '
        Me.leTipoDoc.EnterMoveNextControl = True
        Me.leTipoDoc.Location = New System.Drawing.Point(520, 65)
        Me.leTipoDoc.Name = "leTipoDoc"
        Me.leTipoDoc.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoDoc.Size = New System.Drawing.Size(310, 20)
        Me.leTipoDoc.TabIndex = 8
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(459, 68)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl15.TabIndex = 92
        Me.LabelControl15.Text = "Documento:"
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(500, 154)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl24.TabIndex = 90
        Me.LabelControl24.Text = "Tipo de Impuesto:"
        '
        'leTipoVenta
        '
        Me.leTipoVenta.EnterMoveNextControl = True
        Me.leTipoVenta.Location = New System.Drawing.Point(591, 151)
        Me.leTipoVenta.Name = "leTipoVenta"
        Me.leTipoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoVenta.Size = New System.Drawing.Size(110, 20)
        Me.leTipoVenta.TabIndex = 15
        '
        'ceImprimePrecios
        '
        Me.ceImprimePrecios.Location = New System.Drawing.Point(703, 150)
        Me.ceImprimePrecios.Name = "ceImprimePrecios"
        Me.ceImprimePrecios.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ceImprimePrecios.Properties.Appearance.Options.UseFont = True
        Me.ceImprimePrecios.Properties.Caption = "Imprimir Precios"
        Me.ceImprimePrecios.Size = New System.Drawing.Size(127, 20)
        Me.ceImprimePrecios.TabIndex = 45
        '
        'meConcepto
        '
        Me.meConcepto.EnterMoveNextControl = True
        Me.meConcepto.Location = New System.Drawing.Point(105, 173)
        Me.meConcepto.Name = "meConcepto"
        Me.meConcepto.Size = New System.Drawing.Size(392, 48)
        Me.meConcepto.TabIndex = 16
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(27, 176)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl14.TabIndex = 35
        Me.LabelControl14.Text = "Observaciones:"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(472, 132)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(45, 13)
        Me.LabelControl13.TabIndex = 34
        Me.LabelControl13.Text = "Garantía:"
        '
        'teProyecto
        '
        Me.teProyecto.EnterMoveNextControl = True
        Me.teProyecto.Location = New System.Drawing.Point(105, 129)
        Me.teProyecto.Name = "teProyecto"
        Me.teProyecto.Size = New System.Drawing.Size(320, 20)
        Me.teProyecto.TabIndex = 12
        '
        'teGarantia
        '
        Me.teGarantia.EnterMoveNextControl = True
        Me.teGarantia.Location = New System.Drawing.Point(520, 129)
        Me.teGarantia.Name = "teGarantia"
        Me.teGarantia.Size = New System.Drawing.Size(310, 20)
        Me.teGarantia.TabIndex = 12
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(520, 2)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 2
        '
        'leVendedor
        '
        Me.leVendedor.EnterMoveNextControl = True
        Me.leVendedor.Location = New System.Drawing.Point(520, 108)
        Me.leVendedor.Name = "leVendedor"
        Me.leVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leVendedor.Size = New System.Drawing.Size(310, 20)
        Me.leVendedor.TabIndex = 11
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(55, 133)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl17.TabIndex = 32
        Me.LabelControl17.Text = "Proyecto:"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(467, 111)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl11.TabIndex = 32
        Me.LabelControl11.Text = "Vendedor:"
        '
        'seDiasCredito
        '
        Me.seDiasCredito.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDiasCredito.EnterMoveNextControl = True
        Me.seDiasCredito.Location = New System.Drawing.Point(431, 151)
        Me.seDiasCredito.Name = "seDiasCredito"
        Me.seDiasCredito.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDiasCredito.Size = New System.Drawing.Size(66, 20)
        Me.seDiasCredito.TabIndex = 14
        '
        'leFormaPago
        '
        Me.leFormaPago.EnterMoveNextControl = True
        Me.leFormaPago.Location = New System.Drawing.Point(105, 151)
        Me.leFormaPago.Name = "leFormaPago"
        Me.leFormaPago.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leFormaPago.Size = New System.Drawing.Size(240, 20)
        Me.leFormaPago.TabIndex = 13
        '
        'meNombre
        '
        Me.meNombre.EnterMoveNextControl = True
        Me.meNombre.Location = New System.Drawing.Point(325, 23)
        Me.meNombre.Name = "meNombre"
        Me.meNombre.Size = New System.Drawing.Size(505, 40)
        Me.meNombre.TabIndex = 5
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(250, 25)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl4.TabIndex = 27
        Me.LabelControl4.Text = "Nombre (Sres):"
        '
        'beCliente
        '
        Me.beCliente.EnterMoveNextControl = True
        Me.beCliente.Location = New System.Drawing.Point(105, 23)
        Me.beCliente.Name = "beCliente"
        Me.beCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCliente.Size = New System.Drawing.Size(100, 20)
        Me.beCliente.TabIndex = 4
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(351, 155)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl10.TabIndex = 22
        Me.LabelControl10.Text = "Días de Crédito:"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(26, 155)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl9.TabIndex = 23
        Me.LabelControl9.Text = "Forma de Pago:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(8, 111)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl8.TabIndex = 18
        Me.LabelControl8.Text = "Tiempo de Entrega:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(55, 91)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl7.TabIndex = 17
        Me.LabelControl7.Text = "Dirección:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(25, 69)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl6.TabIndex = 24
        Me.LabelControl6.Text = "Con atención A:"
        '
        'lblNRC_DUI
        '
        Me.lblNRC_DUI.Location = New System.Drawing.Point(77, 46)
        Me.lblNRC_DUI.Name = "lblNRC_DUI"
        Me.lblNRC_DUI.Size = New System.Drawing.Size(25, 13)
        Me.lblNRC_DUI.TabIndex = 19
        Me.lblNRC_DUI.Text = "NRC:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(24, 25)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl3.TabIndex = 21
        Me.LabelControl3.Text = "Cód. de Cliente:"
        '
        'teNRC
        '
        Me.teNRC.EnterMoveNextControl = True
        Me.teNRC.Location = New System.Drawing.Point(105, 44)
        Me.teNRC.Name = "teNRC"
        Me.teNRC.Size = New System.Drawing.Size(100, 20)
        Me.teNRC.TabIndex = 6
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(484, 5)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl2.TabIndex = 15
        Me.LabelControl2.Text = "Fecha:"
        '
        'teTiempo
        '
        Me.teTiempo.EnterMoveNextControl = True
        Me.teTiempo.Location = New System.Drawing.Point(105, 108)
        Me.teTiempo.Name = "teTiempo"
        Me.teTiempo.Size = New System.Drawing.Size(320, 20)
        Me.teTiempo.TabIndex = 10
        '
        'teDireccion
        '
        Me.teDireccion.EnterMoveNextControl = True
        Me.teDireccion.Location = New System.Drawing.Point(105, 86)
        Me.teDireccion.Name = "teDireccion"
        Me.teDireccion.Size = New System.Drawing.Size(725, 20)
        Me.teDireccion.TabIndex = 9
        '
        'teAtencion
        '
        Me.teAtencion.EnterMoveNextControl = True
        Me.teAtencion.Location = New System.Drawing.Point(105, 65)
        Me.teAtencion.Name = "teAtencion"
        Me.teAtencion.Size = New System.Drawing.Size(320, 20)
        Me.teAtencion.TabIndex = 7
        '
        'teNumero
        '
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(325, 2)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(100, 20)
        Me.teNumero.TabIndex = 1
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(45, 5)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl12.TabIndex = 12
        Me.LabelControl12.Text = "Correlativo:"
        '
        'teCorrelativo
        '
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(105, 2)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Properties.ReadOnly = True
        Me.teCorrelativo.Size = New System.Drawing.Size(100, 20)
        Me.teCorrelativo.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(235, 5)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(88, 13)
        Me.LabelControl1.TabIndex = 12
        Me.LabelControl1.Text = "No. de Cotización:"
        '
        'fac_frmCotizaciones
        '
        Me.ClientSize = New System.Drawing.Size(872, 486)
        Me.Controls.Add(Me.xtcCotizaciones)
        Me.DbMode = 1
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmCotizaciones"
        Me.OptionId = "002003"
        Me.Text = "Cotizaciones"
        Me.Controls.SetChildIndex(Me.xtcCotizaciones, 0)
        CType(Me.xtcCotizaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcCotizaciones.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePrecioVenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteDescuento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePrecioUnitario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.ceRetencionPercepcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIVA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teRetencionPercepcion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoDoc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceImprimePrecios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teProyecto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teGarantia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTiempo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teAtencion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents xtcCotizaciones As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcCodProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdPrecio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcVentaNeta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorIva As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ceRetencionPercepcion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIVA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPercReten As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teRetencionPercepcion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents leTipoDoc As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoVenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents ceImprimePrecios As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents meConcepto As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teGarantia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents leVendedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seDiasCredito As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents leFormaPago As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents meNombre As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCliente As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblNRC_DUI As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTiempo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teAtencion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ritePrecioVenta As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteDescuento As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents ritePrecioUnitario As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teProyecto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn

End Class
