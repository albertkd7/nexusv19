﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmAplicacionNotaCredito
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fac_frmAplicacionNotaCredito))
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdComprobante = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumeroComprobante = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.teMontoAbonar = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.beCliente = New DevExpress.XtraEditors.ButtonEdit()
        Me.gcNC = New DevExpress.XtraGrid.GridControl()
        Me.gvNC = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.NroComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.FechaComp = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.FechaVencto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TotalComprob = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SaldoComprob = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.MontoAbonar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteMonto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.IdComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.teIdComprobante.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumeroComprobante.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teMontoAbonar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcNC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvNC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteMonto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.sbGuardar)
        Me.pcHeader.Controls.Add(Me.LabelControl3)
        Me.pcHeader.Controls.Add(Me.teIdComprobante)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Controls.Add(Me.teNumeroComprobante)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.teNombre)
        Me.pcHeader.Controls.Add(Me.LabelControl8)
        Me.pcHeader.Controls.Add(Me.teMontoAbonar)
        Me.pcHeader.Controls.Add(Me.LabelControl12)
        Me.pcHeader.Controls.Add(Me.deFecha)
        Me.pcHeader.Controls.Add(Me.LabelControl13)
        Me.pcHeader.Controls.Add(Me.beCliente)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(726, 87)
        Me.pcHeader.TabIndex = 2
        '
        'sbGuardar
        '
        Me.sbGuardar.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbGuardar.Appearance.Options.UseFont = True
        Me.sbGuardar.Location = New System.Drawing.Point(524, 54)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(119, 28)
        Me.sbGuardar.TabIndex = 56
        Me.sbGuardar.Text = "&Aplicar Nota"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(231, 11)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl3.TabIndex = 55
        Me.LabelControl3.Text = "Id. Comprobante:"
        '
        'teIdComprobante
        '
        Me.teIdComprobante.Enabled = False
        Me.teIdComprobante.EnterMoveNextControl = True
        Me.teIdComprobante.Location = New System.Drawing.Point(321, 8)
        Me.teIdComprobante.Name = "teIdComprobante"
        Me.teIdComprobante.Properties.ReadOnly = True
        Me.teIdComprobante.Size = New System.Drawing.Size(108, 20)
        Me.teIdComprobante.TabIndex = 54
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(9, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(100, 13)
        Me.LabelControl1.TabIndex = 27
        Me.LabelControl1.Text = "No. Nota de Crédito:"
        '
        'teNumeroComprobante
        '
        Me.teNumeroComprobante.EnterMoveNextControl = True
        Me.teNumeroComprobante.Location = New System.Drawing.Point(112, 9)
        Me.teNumeroComprobante.Name = "teNumeroComprobante"
        Me.teNumeroComprobante.Properties.ReadOnly = True
        Me.teNumeroComprobante.Size = New System.Drawing.Size(111, 20)
        Me.teNumeroComprobante.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(277, 33)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 30
        Me.LabelControl2.Text = "Nombre:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(321, 30)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.ReadOnly = True
        Me.teNombre.Size = New System.Drawing.Size(321, 20)
        Me.teNombre.TabIndex = 3
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(28, 55)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(81, 13)
        Me.LabelControl8.TabIndex = 44
        Me.LabelControl8.Text = "Monto a Abonar:"
        '
        'teMontoAbonar
        '
        Me.teMontoAbonar.EditValue = "0"
        Me.teMontoAbonar.EnterMoveNextControl = True
        Me.teMontoAbonar.Location = New System.Drawing.Point(112, 52)
        Me.teMontoAbonar.Name = "teMontoAbonar"
        Me.teMontoAbonar.Properties.Appearance.Options.UseTextOptions = True
        Me.teMontoAbonar.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teMontoAbonar.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teMontoAbonar.Properties.Mask.EditMask = "n2"
        Me.teMontoAbonar.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teMontoAbonar.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teMontoAbonar.Properties.ReadOnly = True
        Me.teMontoAbonar.Size = New System.Drawing.Size(112, 20)
        Me.teMontoAbonar.TabIndex = 10
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(449, 11)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl12.TabIndex = 52
        Me.LabelControl12.Text = "Fecha:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(484, 9)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Properties.ReadOnly = True
        Me.deFecha.Size = New System.Drawing.Size(86, 20)
        Me.deFecha.TabIndex = 1
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(72, 34)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl13.TabIndex = 53
        Me.LabelControl13.Text = "Cliente:"
        '
        'beCliente
        '
        Me.beCliente.Enabled = False
        Me.beCliente.EnterMoveNextControl = True
        Me.beCliente.Location = New System.Drawing.Point(112, 30)
        Me.beCliente.Name = "beCliente"
        Me.beCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCliente.Properties.ReadOnly = True
        Me.beCliente.Size = New System.Drawing.Size(111, 20)
        Me.beCliente.TabIndex = 2
        '
        'gcNC
        '
        Me.gcNC.Dock = System.Windows.Forms.DockStyle.Left
        Me.gcNC.Location = New System.Drawing.Point(0, 87)
        Me.gcNC.MainView = Me.gvNC
        Me.gcNC.Name = "gcNC"
        Me.gcNC.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteMonto})
        Me.gcNC.Size = New System.Drawing.Size(721, 262)
        Me.gcNC.TabIndex = 3
        Me.gcNC.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvNC})
        '
        'gvNC
        '
        Me.gvNC.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.NroComprobante, Me.FechaComp, Me.FechaVencto, Me.TotalComprob, Me.SaldoComprob, Me.MontoAbonar, Me.IdComprobante})
        Me.gvNC.GridControl = Me.gcNC
        Me.gvNC.Name = "gvNC"
        Me.gvNC.OptionsView.ShowFooter = True
        Me.gvNC.OptionsView.ShowGroupPanel = False
        '
        'NroComprobante
        '
        Me.NroComprobante.Caption = "No. Comprobante"
        Me.NroComprobante.FieldName = "Numero"
        Me.NroComprobante.Name = "NroComprobante"
        Me.NroComprobante.OptionsColumn.AllowEdit = False
        Me.NroComprobante.OptionsColumn.AllowFocus = False
        Me.NroComprobante.Visible = True
        Me.NroComprobante.VisibleIndex = 0
        Me.NroComprobante.Width = 122
        '
        'FechaComp
        '
        Me.FechaComp.Caption = "Fecha Comprob."
        Me.FechaComp.FieldName = "Fecha"
        Me.FechaComp.Name = "FechaComp"
        Me.FechaComp.OptionsColumn.AllowEdit = False
        Me.FechaComp.OptionsColumn.AllowFocus = False
        Me.FechaComp.Visible = True
        Me.FechaComp.VisibleIndex = 1
        Me.FechaComp.Width = 122
        '
        'FechaVencto
        '
        Me.FechaVencto.Caption = "Vencimiento"
        Me.FechaVencto.FieldName = "FechaVence"
        Me.FechaVencto.Name = "FechaVencto"
        Me.FechaVencto.OptionsColumn.AllowEdit = False
        Me.FechaVencto.OptionsColumn.AllowFocus = False
        Me.FechaVencto.Visible = True
        Me.FechaVencto.VisibleIndex = 2
        Me.FechaVencto.Width = 104
        '
        'TotalComprob
        '
        Me.TotalComprob.Caption = "Valor Documento"
        Me.TotalComprob.FieldName = "TotalComprobante"
        Me.TotalComprob.Name = "TotalComprob"
        Me.TotalComprob.OptionsColumn.AllowEdit = False
        Me.TotalComprob.OptionsColumn.AllowFocus = False
        Me.TotalComprob.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalComprobante", "{0:n2}")})
        Me.TotalComprob.Visible = True
        Me.TotalComprob.VisibleIndex = 3
        Me.TotalComprob.Width = 90
        '
        'SaldoComprob
        '
        Me.SaldoComprob.Caption = "Saldo"
        Me.SaldoComprob.FieldName = "Saldo"
        Me.SaldoComprob.Name = "SaldoComprob"
        Me.SaldoComprob.OptionsColumn.AllowEdit = False
        Me.SaldoComprob.OptionsColumn.AllowFocus = False
        Me.SaldoComprob.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Saldo", "{0:n2}")})
        Me.SaldoComprob.Visible = True
        Me.SaldoComprob.VisibleIndex = 4
        Me.SaldoComprob.Width = 88
        '
        'MontoAbonar
        '
        Me.MontoAbonar.Caption = "Monto Abonar"
        Me.MontoAbonar.ColumnEdit = Me.riteMonto
        Me.MontoAbonar.FieldName = "MontoAbonar"
        Me.MontoAbonar.Name = "MontoAbonar"
        Me.MontoAbonar.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontoAbonar", "{0:n2}")})
        Me.MontoAbonar.Visible = True
        Me.MontoAbonar.VisibleIndex = 5
        Me.MontoAbonar.Width = 94
        '
        'riteMonto
        '
        Me.riteMonto.AutoHeight = False
        Me.riteMonto.Mask.EditMask = "n2"
        Me.riteMonto.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteMonto.Mask.UseMaskAsDisplayFormat = True
        Me.riteMonto.Name = "riteMonto"
        '
        'IdComprobante
        '
        Me.IdComprobante.Caption = "Id"
        Me.IdComprobante.FieldName = "IdComprobante"
        Me.IdComprobante.Name = "IdComprobante"
        '
        'fac_frmAplicacionNotaCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(726, 349)
        Me.Controls.Add(Me.gcNC)
        Me.Controls.Add(Me.pcHeader)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "fac_frmAplicacionNotaCredito"
        Me.Text = "Aplicación de Notas de Crédito y Devoluciones"
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.teIdComprobante.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumeroComprobante.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teMontoAbonar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcNC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvNC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteMonto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumeroComprobante As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teMontoAbonar As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCliente As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents gcNC As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvNC As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents NroComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents FechaComp As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents FechaVencto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TotalComprob As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SaldoComprob As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents MontoAbonar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteMonto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents IdComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdComprobante As DevExpress.XtraEditors.TextEdit
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
End Class
