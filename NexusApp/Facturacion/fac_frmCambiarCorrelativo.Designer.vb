﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fac_frmCambiarCorrelativo
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.leVendedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.DeFechaNueva = New DevExpress.XtraEditors.DateEdit()
        Me.gcFP = New DevExpress.XtraGrid.GridControl()
        Me.gvFP = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcForma = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leFormaPago = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteMonto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.teNuevoNumero = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.seTotal = New DevExpress.XtraEditors.SpinEdit()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.sbObtener = New DevExpress.XtraEditors.SimpleButton()
        Me.sbCambia = New DevExpress.XtraEditors.SimpleButton()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.teSerie = New DevExpress.XtraEditors.TextEdit()
        Me.leTipo = New DevExpress.XtraEditors.LookUpEdit()
        Me.lePunto = New DevExpress.XtraEditors.LookUpEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DeFechaNueva.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DeFechaNueva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcFP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvFP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leFormaPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteMonto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNuevoNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Controls.Add(Me.leVendedor)
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.DeFechaNueva)
        Me.GroupControl1.Controls.Add(Me.gcFP)
        Me.GroupControl1.Controls.Add(Me.teNuevoNumero)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Controls.Add(Me.seTotal)
        Me.GroupControl1.Controls.Add(Me.deFecha)
        Me.GroupControl1.Controls.Add(Me.sbObtener)
        Me.GroupControl1.Controls.Add(Me.sbCambia)
        Me.GroupControl1.Controls.Add(Me.teNombre)
        Me.GroupControl1.Controls.Add(Me.teNumero)
        Me.GroupControl1.Controls.Add(Me.teSerie)
        Me.GroupControl1.Controls.Add(Me.leTipo)
        Me.GroupControl1.Controls.Add(Me.lePunto)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.LabelControl13)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(651, 432)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Datos del documento a cambiar"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(510, 80)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(76, 18)
        Me.LabelControl11.TabIndex = 81
        Me.LabelControl11.Text = "Vendedor:"
        Me.LabelControl11.Visible = False
        '
        'leVendedor
        '
        Me.leVendedor.EnterMoveNextControl = True
        Me.leVendedor.Location = New System.Drawing.Point(591, 79)
        Me.leVendedor.Name = "leVendedor"
        Me.leVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leVendedor.Properties.NullText = ""
        Me.leVendedor.Size = New System.Drawing.Size(282, 20)
        Me.leVendedor.TabIndex = 80
        Me.leVendedor.Visible = False
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(536, 56)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(49, 18)
        Me.LabelControl10.TabIndex = 79
        Me.LabelControl10.Text = "Fecha:"
        Me.LabelControl10.Visible = False
        '
        'DeFechaNueva
        '
        Me.DeFechaNueva.EditValue = Nothing
        Me.DeFechaNueva.EnterMoveNextControl = True
        Me.DeFechaNueva.Location = New System.Drawing.Point(591, 53)
        Me.DeFechaNueva.Name = "DeFechaNueva"
        Me.DeFechaNueva.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
        Me.DeFechaNueva.Properties.AppearanceDisabled.Options.UseForeColor = True
        Me.DeFechaNueva.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.DeFechaNueva.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.DeFechaNueva.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DeFechaNueva.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DeFechaNueva.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.DeFechaNueva.Size = New System.Drawing.Size(123, 20)
        Me.DeFechaNueva.TabIndex = 78
        Me.DeFechaNueva.Visible = False
        '
        'gcFP
        '
        Me.gcFP.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.gcFP.Location = New System.Drawing.Point(91, 242)
        Me.gcFP.MainView = Me.gvFP
        Me.gcFP.Name = "gcFP"
        Me.gcFP.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteMonto, Me.leFormaPago})
        Me.gcFP.Size = New System.Drawing.Size(422, 173)
        Me.gcFP.TabIndex = 12
        Me.gcFP.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvFP})
        '
        'gvFP
        '
        Me.gvFP.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcForma, Me.gcTotal})
        Me.gvFP.GridControl = Me.gcFP
        Me.gvFP.Name = "gvFP"
        Me.gvFP.OptionsView.ShowFooter = True
        Me.gvFP.OptionsView.ShowGroupPanel = False
        '
        'gcForma
        '
        Me.gcForma.Caption = "Forma de Pago"
        Me.gcForma.ColumnEdit = Me.leFormaPago
        Me.gcForma.FieldName = "IdFormaPago"
        Me.gcForma.Name = "gcForma"
        Me.gcForma.OptionsColumn.AllowEdit = False
        Me.gcForma.OptionsColumn.AllowFocus = False
        Me.gcForma.OptionsColumn.AllowMove = False
        Me.gcForma.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "IdFormaPago", "TOTAL PAGO:")})
        Me.gcForma.Visible = True
        Me.gcForma.VisibleIndex = 0
        Me.gcForma.Width = 251
        '
        'leFormaPago
        '
        Me.leFormaPago.AutoHeight = False
        Me.leFormaPago.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leFormaPago.Name = "leFormaPago"
        '
        'gcTotal
        '
        Me.gcTotal.Caption = "Total "
        Me.gcTotal.DisplayFormat.FormatString = "{0:c}"
        Me.gcTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcTotal.FieldName = "Total"
        Me.gcTotal.Name = "gcTotal"
        Me.gcTotal.OptionsColumn.AllowMove = False
        Me.gcTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Total", "{0:c}")})
        Me.gcTotal.Visible = True
        Me.gcTotal.VisibleIndex = 1
        Me.gcTotal.Width = 143
        '
        'riteMonto
        '
        Me.riteMonto.AutoHeight = False
        Me.riteMonto.Mask.EditMask = "n2"
        Me.riteMonto.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteMonto.Mask.UseMaskAsDisplayFormat = True
        Me.riteMonto.Name = "riteMonto"
        '
        'teNuevoNumero
        '
        Me.teNuevoNumero.Location = New System.Drawing.Point(591, 23)
        Me.teNuevoNumero.Name = "teNuevoNumero"
        Me.teNuevoNumero.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold)
        Me.teNuevoNumero.Properties.Appearance.Options.UseFont = True
        Me.teNuevoNumero.Size = New System.Drawing.Size(123, 24)
        Me.teNuevoNumero.TabIndex = 11
        Me.teNuevoNumero.Visible = False
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(472, 26)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(116, 18)
        Me.LabelControl9.TabIndex = 10
        Me.LabelControl9.Text = "Nuevo Número:"
        Me.LabelControl9.Visible = False
        '
        'seTotal
        '
        Me.seTotal.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seTotal.Location = New System.Drawing.Point(94, 192)
        Me.seTotal.Name = "seTotal"
        Me.seTotal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seTotal.Properties.Mask.EditMask = "c"
        Me.seTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seTotal.Properties.ReadOnly = True
        Me.seTotal.Size = New System.Drawing.Size(100, 20)
        Me.seTotal.TabIndex = 7
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.Location = New System.Drawing.Point(94, 150)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.ReadOnly = True
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 5
        '
        'sbObtener
        '
        Me.sbObtener.Location = New System.Drawing.Point(200, 114)
        Me.sbObtener.Name = "sbObtener"
        Me.sbObtener.Size = New System.Drawing.Size(75, 23)
        Me.sbObtener.TabIndex = 5
        Me.sbObtener.Text = "Obtener..."
        '
        'sbCambia
        '
        Me.sbCambia.Location = New System.Drawing.Point(363, 214)
        Me.sbCambia.Name = "sbCambia"
        Me.sbCambia.Size = New System.Drawing.Size(150, 24)
        Me.sbCambia.TabIndex = 9
        Me.sbCambia.Text = "&Actualizar documento..."
        '
        'teNombre
        '
        Me.teNombre.Location = New System.Drawing.Point(94, 171)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.ReadOnly = True
        Me.teNombre.Size = New System.Drawing.Size(377, 20)
        Me.teNombre.TabIndex = 6
        '
        'teNumero
        '
        Me.teNumero.Location = New System.Drawing.Point(94, 115)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(100, 20)
        Me.teNumero.TabIndex = 4
        '
        'teSerie
        '
        Me.teSerie.Location = New System.Drawing.Point(94, 94)
        Me.teSerie.Name = "teSerie"
        Me.teSerie.Size = New System.Drawing.Size(100, 20)
        Me.teSerie.TabIndex = 3
        '
        'leTipo
        '
        Me.leTipo.Location = New System.Drawing.Point(94, 71)
        Me.leTipo.Name = "leTipo"
        Me.leTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipo.Size = New System.Drawing.Size(315, 20)
        Me.leTipo.TabIndex = 2
        '
        'lePunto
        '
        Me.lePunto.Location = New System.Drawing.Point(94, 50)
        Me.lePunto.Name = "lePunto"
        Me.lePunto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePunto.Size = New System.Drawing.Size(315, 20)
        Me.lePunto.TabIndex = 1
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(94, 29)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(315, 20)
        Me.leSucursal.TabIndex = 0
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(94, 220)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(249, 13)
        Me.LabelControl8.TabIndex = 4
        Me.LabelControl8.Text = "DATOS A ACTUALIZAR -------------------------------->"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(63, 195)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl12.TabIndex = 4
        Me.LabelControl12.Text = "Total:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(25, 174)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(66, 13)
        Me.LabelControl7.TabIndex = 4
        Me.LabelControl7.Text = "A Nombre de:"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(200, 152)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(109, 13)
        Me.LabelControl13.TabIndex = 4
        Me.LabelControl13.Text = "<------- ENCONTRADO"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(58, 153)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl6.TabIndex = 4
        Me.LabelControl6.Text = "Fecha:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(50, 118)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl5.TabIndex = 4
        Me.LabelControl5.Text = "Número:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(63, 97)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl4.TabIndex = 3
        Me.LabelControl4.Text = "Serie:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(67, 74)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Tipo:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(13, 54)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Punto de Venta:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(47, 32)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Sucursal:"
        '
        'fac_frmCambiarCorrelativo
        '
        Me.ClientSize = New System.Drawing.Size(651, 457)
        Me.Controls.Add(Me.GroupControl1)
        Me.Modulo = "Facturación"
        Me.Name = "fac_frmCambiarCorrelativo"
        Me.OptionId = "002013"
        Me.Text = "Actualizar Documento"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DeFechaNueva.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DeFechaNueva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcFP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvFP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leFormaPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteMonto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNuevoNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leTipo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lePunto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbCambia As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbObtener As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents seTotal As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNuevoNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcFP As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvFP As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcForma As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leFormaPago As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents gcTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteMonto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DeFechaNueva As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leVendedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
End Class
