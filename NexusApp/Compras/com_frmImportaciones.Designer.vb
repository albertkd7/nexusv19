﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class com_frmImportaciones
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xtcImportacion = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage()
        Me.gc2 = New DevExpress.XtraGrid.GridControl()
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcCorrelativo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcAplicada = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.IdProducto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riCodProd = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.Descripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Referencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ValorDai = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtValorDai = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.Cantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtPrecio1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.PrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtPrecioUnitario = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtPrecio2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtPrecio3 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PrecioTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leCentroCosto = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.txtIdProducto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.txtPrecioTotal = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdDetalla = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.leClaseDocumento = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.leAduana = New DevExpress.XtraEditors.LookUpEdit()
        Me.beOrden = New DevExpress.XtraEditors.SimpleButton()
        Me.teOrdenCompra = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdPartida = New DevExpress.XtraEditors.TextEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.leOrigenCompra = New DevExpress.XtraEditors.LookUpEdit()
        Me.btCPP = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btRevertir = New DevExpress.XtraEditors.SimpleButton()
        Me.btAplicar = New DevExpress.XtraEditors.SimpleButton()
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit()
        Me.sePorcDescto = New DevExpress.XtraEditors.SpinEdit()
        Me.leBodega = New DevExpress.XtraEditors.LookUpEdit()
        Me.deFechaCon = New DevExpress.XtraEditors.DateEdit()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teTotalPP = New DevExpress.XtraEditors.TextEdit()
        Me.teTotalIva = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.teValorExento = New DevExpress.XtraEditors.TextEdit()
        Me.teValorSujeto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumPoliza = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.xtcImportacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcImportacion.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riCodProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtValorDai, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrecio1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrecioUnitario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrecio2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrecio3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCentroCosto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtIdProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrecioTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.leClaseDocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leAduana.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teOrdenCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leOrigenCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sePorcDescto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaCon.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaCon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotalPP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotalIva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teValorExento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teValorSujeto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumPoliza.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcImportacion
        '
        Me.xtcImportacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcImportacion.Location = New System.Drawing.Point(0, 0)
        Me.xtcImportacion.Name = "xtcImportacion"
        Me.xtcImportacion.SelectedTabPage = Me.xtpLista
        Me.xtcImportacion.Size = New System.Drawing.Size(1148, 488)
        Me.xtcImportacion.TabIndex = 4
        Me.xtcImportacion.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc2)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(1142, 460)
        Me.xtpLista.Text = "Consulta Importaciones"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(1142, 460)
        Me.gc2.TabIndex = 3
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcCorrelativo, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn8, Me.GridColumn5, Me.GridColumn9, Me.gcAplicada, Me.GridColumn6, Me.GridColumn7})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'gcCorrelativo
        '
        Me.gcCorrelativo.Caption = "Correlativo"
        Me.gcCorrelativo.FieldName = "IdComprobante"
        Me.gcCorrelativo.Name = "gcCorrelativo"
        Me.gcCorrelativo.Visible = True
        Me.gcCorrelativo.VisibleIndex = 0
        Me.gcCorrelativo.Width = 47
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "No. de Poliza"
        Me.GridColumn2.FieldName = "NumeroPoliza"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 92
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Fecha"
        Me.GridColumn3.FieldName = "Fecha"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 64
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Fecha Contable"
        Me.GridColumn4.FieldName = "FechaContable"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 80
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Sucursal"
        Me.GridColumn8.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn8.FieldName = "IdSucursal"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 4
        Me.GridColumn8.Width = 155
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Total Poliza"
        Me.GridColumn5.FieldName = "TotalPoliza"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 5
        Me.GridColumn5.Width = 117
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Total Por Pagar"
        Me.GridColumn9.FieldName = "TotalPorPagar"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 6
        Me.GridColumn9.Width = 76
        '
        'gcAplicada
        '
        Me.gcAplicada.Caption = "Aplicada"
        Me.gcAplicada.FieldName = "AplicadaInventario"
        Me.gcAplicada.Name = "gcAplicada"
        Me.gcAplicada.Visible = True
        Me.gcAplicada.VisibleIndex = 7
        Me.gcAplicada.Width = 69
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Creado Por"
        Me.GridColumn6.FieldName = "CreadoPor"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 8
        Me.GridColumn6.Width = 100
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Fecha Hora Creacion"
        Me.GridColumn7.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.GridColumn7.FieldName = "FechaHoraCreacion"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 9
        Me.GridColumn7.Width = 154
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemDateEdit1.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.gc)
        Me.xtpDatos.Controls.Add(Me.PanelControl2)
        Me.xtpDatos.Controls.Add(Me.pcHeader)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(1142, 460)
        Me.xtpDatos.Text = "Importaciones"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 132)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.txtIdProducto, Me.txtCantidad, Me.txtPrecioUnitario, Me.txtValorDai, Me.riCodProd, Me.txtPrecio1, Me.txtPrecio2, Me.txtPrecio3, Me.txtPrecioTotal, Me.leCentroCosto})
        Me.gc.Size = New System.Drawing.Size(1101, 328)
        Me.gc.TabIndex = 25
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.IdProducto, Me.Descripcion, Me.Referencia, Me.ValorDai, Me.Cantidad, Me.GridColumn10, Me.PrecioUnitario, Me.GridColumn11, Me.GridColumn12, Me.GridColumn13, Me.PrecioTotal, Me.GridColumn1})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsSelection.MultiSelect = True
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'IdProducto
        '
        Me.IdProducto.Caption = "Cód. Producto"
        Me.IdProducto.ColumnEdit = Me.riCodProd
        Me.IdProducto.FieldName = "IdProducto"
        Me.IdProducto.Name = "IdProducto"
        Me.IdProducto.Visible = True
        Me.IdProducto.VisibleIndex = 0
        Me.IdProducto.Width = 72
        '
        'riCodProd
        '
        Me.riCodProd.AutoHeight = False
        Me.riCodProd.Name = "riCodProd"
        '
        'Descripcion
        '
        Me.Descripcion.Caption = "Nombre del producto"
        Me.Descripcion.FieldName = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.Visible = True
        Me.Descripcion.VisibleIndex = 1
        Me.Descripcion.Width = 267
        '
        'Referencia
        '
        Me.Referencia.Caption = "Referencia"
        Me.Referencia.FieldName = "Referencia"
        Me.Referencia.Name = "Referencia"
        Me.Referencia.Visible = True
        Me.Referencia.VisibleIndex = 2
        Me.Referencia.Width = 60
        '
        'ValorDai
        '
        Me.ValorDai.Caption = "DAI"
        Me.ValorDai.ColumnEdit = Me.txtValorDai
        Me.ValorDai.FieldName = "ValorDai"
        Me.ValorDai.Name = "ValorDai"
        Me.ValorDai.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorDai", "{0:n2} %")})
        Me.ValorDai.Visible = True
        Me.ValorDai.VisibleIndex = 3
        Me.ValorDai.Width = 52
        '
        'txtValorDai
        '
        Me.txtValorDai.AutoHeight = False
        Me.txtValorDai.Mask.EditMask = "n2"
        Me.txtValorDai.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtValorDai.Mask.UseMaskAsDisplayFormat = True
        Me.txtValorDai.Name = "txtValorDai"
        '
        'Cantidad
        '
        Me.Cantidad.Caption = "Cantidad"
        Me.Cantidad.ColumnEdit = Me.txtCantidad
        Me.Cantidad.FieldName = "Cantidad"
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Cantidad", "{0:n3} n")})
        Me.Cantidad.Visible = True
        Me.Cantidad.VisibleIndex = 4
        Me.Cantidad.Width = 79
        '
        'txtCantidad
        '
        Me.txtCantidad.AutoHeight = False
        Me.txtCantidad.Mask.EditMask = "n3"
        Me.txtCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.txtCantidad.Name = "txtCantidad"
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Precio Vta. Actual"
        Me.GridColumn10.ColumnEdit = Me.txtPrecio1
        Me.GridColumn10.FieldName = "PrecioVentaAct"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.OptionsColumn.AllowEdit = False
        Me.GridColumn10.OptionsColumn.AllowFocus = False
        Me.GridColumn10.Width = 97
        '
        'txtPrecio1
        '
        Me.txtPrecio1.AutoHeight = False
        Me.txtPrecio1.Mask.EditMask = "n4"
        Me.txtPrecio1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPrecio1.Mask.UseMaskAsDisplayFormat = True
        Me.txtPrecio1.Name = "txtPrecio1"
        '
        'PrecioUnitario
        '
        Me.PrecioUnitario.Caption = "Costo Unitario"
        Me.PrecioUnitario.ColumnEdit = Me.txtPrecioUnitario
        Me.PrecioUnitario.FieldName = "PrecioUnitario"
        Me.PrecioUnitario.Name = "PrecioUnitario"
        Me.PrecioUnitario.Visible = True
        Me.PrecioUnitario.VisibleIndex = 5
        Me.PrecioUnitario.Width = 78
        '
        'txtPrecioUnitario
        '
        Me.txtPrecioUnitario.AutoHeight = False
        Me.txtPrecioUnitario.Mask.EditMask = "n6"
        Me.txtPrecioUnitario.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPrecioUnitario.Mask.UseMaskAsDisplayFormat = True
        Me.txtPrecioUnitario.Name = "txtPrecioUnitario"
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Precios Vta.Calculado"
        Me.GridColumn11.ColumnEdit = Me.txtPrecio2
        Me.GridColumn11.FieldName = "PrecioVentaCal"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.OptionsColumn.AllowEdit = False
        Me.GridColumn11.OptionsColumn.AllowFocus = False
        Me.GridColumn11.Width = 115
        '
        'txtPrecio2
        '
        Me.txtPrecio2.AutoHeight = False
        Me.txtPrecio2.Mask.EditMask = "n4"
        Me.txtPrecio2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPrecio2.Mask.UseMaskAsDisplayFormat = True
        Me.txtPrecio2.Name = "txtPrecio2"
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Nuevo Precio Vta."
        Me.GridColumn12.ColumnEdit = Me.txtPrecio3
        Me.GridColumn12.FieldName = "PrecioVentaNuevo"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Width = 71
        '
        'txtPrecio3
        '
        Me.txtPrecio3.AutoHeight = False
        Me.txtPrecio3.Mask.EditMask = "n4"
        Me.txtPrecio3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPrecio3.Mask.UseMaskAsDisplayFormat = True
        Me.txtPrecio3.Name = "txtPrecio3"
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "% Comisión"
        Me.GridColumn13.FieldName = "PorcComision"
        Me.GridColumn13.Name = "GridColumn13"
        '
        'PrecioTotal
        '
        Me.PrecioTotal.Caption = "Costo Total"
        Me.PrecioTotal.DisplayFormat.FormatString = "n2"
        Me.PrecioTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.PrecioTotal.FieldName = "PrecioTotal"
        Me.PrecioTotal.Name = "PrecioTotal"
        Me.PrecioTotal.OptionsColumn.AllowEdit = False
        Me.PrecioTotal.OptionsColumn.AllowFocus = False
        Me.PrecioTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PrecioTotal", "{0:n2}")})
        Me.PrecioTotal.Visible = True
        Me.PrecioTotal.VisibleIndex = 6
        Me.PrecioTotal.Width = 74
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Centro Costo"
        Me.GridColumn1.ColumnEdit = Me.leCentroCosto
        Me.GridColumn1.FieldName = "IdCentro"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.AllowFocus = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 7
        '
        'leCentroCosto
        '
        Me.leCentroCosto.AutoHeight = False
        Me.leCentroCosto.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCentroCosto.Name = "leCentroCosto"
        '
        'txtIdProducto
        '
        Me.txtIdProducto.AutoHeight = False
        Me.txtIdProducto.Mask.UseMaskAsDisplayFormat = True
        Me.txtIdProducto.Name = "txtIdProducto"
        '
        'txtPrecioTotal
        '
        Me.txtPrecioTotal.AutoHeight = False
        Me.txtPrecioTotal.Mask.EditMask = "n2"
        Me.txtPrecioTotal.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPrecioTotal.Mask.UseMaskAsDisplayFormat = True
        Me.txtPrecioTotal.Name = "txtPrecioTotal"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.cmdDetalla)
        Me.PanelControl2.Controls.Add(Me.cmdBorrar)
        Me.PanelControl2.Controls.Add(Me.cmdUp)
        Me.PanelControl2.Controls.Add(Me.cmdDown)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelControl2.Location = New System.Drawing.Point(1101, 132)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(41, 328)
        Me.PanelControl2.TabIndex = 26
        '
        'cmdDetalla
        '
        Me.cmdDetalla.Image = Global.Nexus.My.Resources.Resources.Edit
        Me.cmdDetalla.Location = New System.Drawing.Point(6, 87)
        Me.cmdDetalla.Name = "cmdDetalla"
        Me.cmdDetalla.Size = New System.Drawing.Size(28, 24)
        Me.cmdDetalla.TabIndex = 32
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(7, 53)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(28, 24)
        Me.cmdBorrar.TabIndex = 31
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(7, 3)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(28, 24)
        Me.cmdUp.TabIndex = 29
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(7, 28)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(28, 24)
        Me.cmdDown.TabIndex = 30
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.LabelControl28)
        Me.pcHeader.Controls.Add(Me.leClaseDocumento)
        Me.pcHeader.Controls.Add(Me.LabelControl7)
        Me.pcHeader.Controls.Add(Me.leAduana)
        Me.pcHeader.Controls.Add(Me.beOrden)
        Me.pcHeader.Controls.Add(Me.teOrdenCompra)
        Me.pcHeader.Controls.Add(Me.LabelControl6)
        Me.pcHeader.Controls.Add(Me.LabelControl39)
        Me.pcHeader.Controls.Add(Me.teIdPartida)
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.LabelControl24)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.leOrigenCompra)
        Me.pcHeader.Controls.Add(Me.btCPP)
        Me.pcHeader.Controls.Add(Me.SimpleButton1)
        Me.pcHeader.Controls.Add(Me.btRevertir)
        Me.pcHeader.Controls.Add(Me.btAplicar)
        Me.pcHeader.Controls.Add(Me.teCorrelativo)
        Me.pcHeader.Controls.Add(Me.sePorcDescto)
        Me.pcHeader.Controls.Add(Me.leBodega)
        Me.pcHeader.Controls.Add(Me.deFechaCon)
        Me.pcHeader.Controls.Add(Me.deFecha)
        Me.pcHeader.Controls.Add(Me.LabelControl3)
        Me.pcHeader.Controls.Add(Me.teTotalPP)
        Me.pcHeader.Controls.Add(Me.teTotalIva)
        Me.pcHeader.Controls.Add(Me.LabelControl20)
        Me.pcHeader.Controls.Add(Me.teValorExento)
        Me.pcHeader.Controls.Add(Me.teValorSujeto)
        Me.pcHeader.Controls.Add(Me.LabelControl5)
        Me.pcHeader.Controls.Add(Me.LabelControl19)
        Me.pcHeader.Controls.Add(Me.LabelControl18)
        Me.pcHeader.Controls.Add(Me.LabelControl21)
        Me.pcHeader.Controls.Add(Me.LabelControl15)
        Me.pcHeader.Controls.Add(Me.LabelControl16)
        Me.pcHeader.Controls.Add(Me.LabelControl4)
        Me.pcHeader.Controls.Add(Me.teNumPoliza)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(1142, 132)
        Me.pcHeader.TabIndex = 0
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(712, 29)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl28.TabIndex = 161
        Me.LabelControl28.Text = "Clase de Documento:"
        '
        'leClaseDocumento
        '
        Me.leClaseDocumento.Location = New System.Drawing.Point(817, 25)
        Me.leClaseDocumento.Name = "leClaseDocumento"
        Me.leClaseDocumento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leClaseDocumento.Size = New System.Drawing.Size(145, 20)
        Me.leClaseDocumento.TabIndex = 160
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(773, 6)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl7.TabIndex = 159
        Me.LabelControl7.Text = "Aduana:"
        '
        'leAduana
        '
        Me.leAduana.EnterMoveNextControl = True
        Me.leAduana.Location = New System.Drawing.Point(817, 3)
        Me.leAduana.Name = "leAduana"
        Me.leAduana.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leAduana.Size = New System.Drawing.Size(145, 20)
        Me.leAduana.TabIndex = 158
        '
        'beOrden
        '
        Me.beOrden.Image = Global.Nexus.My.Resources.Resources.Preview16x16
        Me.beOrden.Location = New System.Drawing.Point(476, 24)
        Me.beOrden.Name = "beOrden"
        Me.beOrden.Size = New System.Drawing.Size(27, 20)
        Me.beOrden.TabIndex = 141
        '
        'teOrdenCompra
        '
        Me.teOrdenCompra.EnterMoveNextControl = True
        Me.teOrdenCompra.Location = New System.Drawing.Point(374, 25)
        Me.teOrdenCompra.Name = "teOrdenCompra"
        Me.teOrdenCompra.Size = New System.Drawing.Size(100, 20)
        Me.teOrdenCompra.TabIndex = 139
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(247, 29)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(124, 13)
        Me.LabelControl6.TabIndex = 140
        Me.LabelControl6.Text = "No. de Orden de Compra:"
        '
        'LabelControl39
        '
        Me.LabelControl39.Location = New System.Drawing.Point(556, 27)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl39.TabIndex = 138
        Me.LabelControl39.Text = "Id Partida:"
        '
        'teIdPartida
        '
        Me.teIdPartida.Enabled = False
        Me.teIdPartida.EnterMoveNextControl = True
        Me.teIdPartida.Location = New System.Drawing.Point(609, 24)
        Me.teIdPartida.Name = "teIdPartida"
        Me.teIdPartida.Properties.ReadOnly = True
        Me.teIdPartida.Size = New System.Drawing.Size(100, 20)
        Me.teIdPartida.TabIndex = 4
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(143, 110)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(331, 20)
        Me.leSucursal.TabIndex = 11
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(97, 115)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl24.TabIndex = 87
        Me.LabelControl24.Text = "Sucursal:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(19, 70)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(122, 13)
        Me.LabelControl2.TabIndex = 85
        Me.LabelControl2.Text = "Origen de la Importación:"
        '
        'leOrigenCompra
        '
        Me.leOrigenCompra.EnterMoveNextControl = True
        Me.leOrigenCompra.Location = New System.Drawing.Point(143, 67)
        Me.leOrigenCompra.Name = "leOrigenCompra"
        Me.leOrigenCompra.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leOrigenCompra.Size = New System.Drawing.Size(331, 20)
        Me.leOrigenCompra.TabIndex = 7
        '
        'btCPP
        '
        Me.btCPP.AllowFocus = False
        Me.btCPP.Image = Global.Nexus.My.Resources.Resources.Users
        Me.btCPP.Location = New System.Drawing.Point(968, 1)
        Me.btCPP.Name = "btCPP"
        Me.btCPP.Size = New System.Drawing.Size(144, 27)
        Me.btCPP.TabIndex = 83
        Me.btCPP.Text = "Detallar Proveedores"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.AllowFocus = False
        Me.SimpleButton1.Image = Global.Nexus.My.Resources.Resources.Modify
        Me.SimpleButton1.Location = New System.Drawing.Point(968, 30)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(144, 24)
        Me.SimpleButton1.TabIndex = 82
        Me.SimpleButton1.Text = "&Detallar Gastos "
        '
        'btRevertir
        '
        Me.btRevertir.AllowFocus = False
        Me.btRevertir.Location = New System.Drawing.Point(968, 96)
        Me.btRevertir.Name = "btRevertir"
        Me.btRevertir.Size = New System.Drawing.Size(144, 24)
        Me.btRevertir.TabIndex = 81
        Me.btRevertir.Text = "&Revertir Aplicación"
        '
        'btAplicar
        '
        Me.btAplicar.AllowFocus = False
        Me.btAplicar.Location = New System.Drawing.Point(968, 70)
        Me.btAplicar.Name = "btAplicar"
        Me.btAplicar.Size = New System.Drawing.Size(144, 24)
        Me.btAplicar.TabIndex = 80
        Me.btAplicar.Text = "&Aplicar a Inventarios"
        '
        'teCorrelativo
        '
        Me.teCorrelativo.Location = New System.Drawing.Point(609, 3)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Properties.ReadOnly = True
        Me.teCorrelativo.Size = New System.Drawing.Size(100, 20)
        Me.teCorrelativo.TabIndex = 2
        '
        'sePorcDescto
        '
        Me.sePorcDescto.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.sePorcDescto.EnterMoveNextControl = True
        Me.sePorcDescto.Location = New System.Drawing.Point(609, 110)
        Me.sePorcDescto.Name = "sePorcDescto"
        Me.sePorcDescto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.sePorcDescto.Size = New System.Drawing.Size(72, 20)
        Me.sePorcDescto.TabIndex = 12
        '
        'leBodega
        '
        Me.leBodega.EnterMoveNextControl = True
        Me.leBodega.Location = New System.Drawing.Point(143, 88)
        Me.leBodega.Name = "leBodega"
        Me.leBodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leBodega.Size = New System.Drawing.Size(331, 20)
        Me.leBodega.TabIndex = 9
        '
        'deFechaCon
        '
        Me.deFechaCon.EditValue = Nothing
        Me.deFechaCon.EnterMoveNextControl = True
        Me.deFechaCon.Location = New System.Drawing.Point(374, 5)
        Me.deFechaCon.Name = "deFechaCon"
        Me.deFechaCon.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaCon.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaCon.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaCon.Size = New System.Drawing.Size(100, 20)
        Me.deFechaCon.TabIndex = 1
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(143, 28)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(100, 20)
        Me.deFecha.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(63, 31)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "Fecha de Póliza:"
        '
        'teTotalPP
        '
        Me.teTotalPP.EditValue = 0
        Me.teTotalPP.Enabled = False
        Me.teTotalPP.EnterMoveNextControl = True
        Me.teTotalPP.Location = New System.Drawing.Point(374, 46)
        Me.teTotalPP.Name = "teTotalPP"
        Me.teTotalPP.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotalPP.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotalPP.Properties.Mask.EditMask = "n2"
        Me.teTotalPP.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotalPP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTotalPP.Size = New System.Drawing.Size(100, 20)
        Me.teTotalPP.TabIndex = 5
        '
        'teTotalIva
        '
        Me.teTotalIva.EditValue = 0
        Me.teTotalIva.EnterMoveNextControl = True
        Me.teTotalIva.Location = New System.Drawing.Point(609, 67)
        Me.teTotalIva.Name = "teTotalIva"
        Me.teTotalIva.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotalIva.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotalIva.Properties.Mask.EditMask = "n2"
        Me.teTotalIva.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotalIva.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTotalIva.Size = New System.Drawing.Size(100, 20)
        Me.teTotalIva.TabIndex = 8
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(187, 50)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(183, 13)
        Me.LabelControl20.TabIndex = 0
        Me.LabelControl20.Text = "TOTAL POR PAGAR A PROVEEDORES:"
        '
        'teValorExento
        '
        Me.teValorExento.EditValue = 0
        Me.teValorExento.EnterMoveNextControl = True
        Me.teValorExento.Location = New System.Drawing.Point(609, 88)
        Me.teValorExento.Name = "teValorExento"
        Me.teValorExento.Properties.Appearance.Options.UseTextOptions = True
        Me.teValorExento.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teValorExento.Properties.Mask.EditMask = "n2"
        Me.teValorExento.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teValorExento.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teValorExento.Size = New System.Drawing.Size(100, 20)
        Me.teValorExento.TabIndex = 10
        '
        'teValorSujeto
        '
        Me.teValorSujeto.EditValue = 0
        Me.teValorSujeto.EnterMoveNextControl = True
        Me.teValorSujeto.Location = New System.Drawing.Point(609, 46)
        Me.teValorSujeto.Name = "teValorSujeto"
        Me.teValorSujeto.Properties.Appearance.Options.UseTextOptions = True
        Me.teValorSujeto.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teValorSujeto.Properties.Mask.EditMask = "n2"
        Me.teValorSujeto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teValorSujeto.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teValorSujeto.Size = New System.Drawing.Size(100, 20)
        Me.teValorSujeto.TabIndex = 6
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(528, 93)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "VALOR EXENTO:"
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(542, 70)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl19.TabIndex = 0
        Me.LabelControl19.Text = "Valor del IVA:"
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(478, 51)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(129, 13)
        Me.LabelControl18.TabIndex = 0
        Me.LabelControl18.Text = "Valor Sujeto a Declaración:"
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(546, 5)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl21.TabIndex = 0
        Me.LabelControl21.Text = "Correlativo:"
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(46, 94)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(95, 13)
        Me.LabelControl15.TabIndex = 0
        Me.LabelControl15.Text = "Bodega de Ingreso:"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(523, 113)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl16.TabIndex = 0
        Me.LabelControl16.Text = "% de Descuento:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(292, 8)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Fecha Contable:"
        '
        'teNumPoliza
        '
        Me.teNumPoliza.EnterMoveNextControl = True
        Me.teNumPoliza.Location = New System.Drawing.Point(143, 5)
        Me.teNumPoliza.Name = "teNumPoliza"
        Me.teNumPoliza.Size = New System.Drawing.Size(100, 20)
        Me.teNumPoliza.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(6, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(135, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "No. de la Póliza/Documento:"
        '
        'com_frmImportaciones
        '
        Me.ClientSize = New System.Drawing.Size(1148, 513)
        Me.Controls.Add(Me.xtcImportacion)
        Me.Modulo = "Compras"
        Me.Name = "com_frmImportaciones"
        Me.OptionId = "002003"
        Me.Text = "Importaciones y prorrateos"
        Me.Controls.SetChildIndex(Me.xtcImportacion, 0)
        CType(Me.xtcImportacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcImportacion.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riCodProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtValorDai, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrecio1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrecioUnitario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrecio2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrecio3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCentroCosto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtIdProducto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrecioTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.leClaseDocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leAduana.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teOrdenCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leOrigenCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sePorcDescto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaCon.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaCon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotalPP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotalIva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teValorExento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teValorSujeto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumPoliza.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents xtcImportacion As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents IdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riCodProd As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents Descripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Referencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ValorDai As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtValorDai As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents Cantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents PrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtPrecioUnitario As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents PrecioTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtIdProducto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdDetalla As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leOrigenCompra As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents btCPP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btRevertir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btAplicar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents sePorcDescto As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents leBodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents deFechaCon As DevExpress.XtraEditors.DateEdit
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTotalPP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teTotalIva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teValorSujeto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumPoliza As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcCorrelativo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdPartida As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtPrecio1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtPrecio2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtPrecio3 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtPrecioTotal As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcAplicada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teValorExento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leCentroCosto As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents beOrden As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teOrdenCompra As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leAduana As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leClaseDocumento As DevExpress.XtraEditors.LookUpEdit
End Class
