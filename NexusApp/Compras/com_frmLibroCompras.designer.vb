﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class com_frmLibroCompras
    Inherits Nexus.gen_frmBaseRpt
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.spAnio = New DevExpress.XtraEditors.SpinEdit()
        Me.meMes = New DevExpress.XtraScheduler.UI.MonthEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teTitulo = New DevExpress.XtraEditors.TextEdit()
        Me.ceFoliarHojas = New DevExpress.XtraEditors.CheckEdit()
        Me.ceEncabezado = New DevExpress.XtraEditors.CheckEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.seNumFolio = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teProgreso = New DevExpress.XtraEditors.TextEdit()
        Me.btExportXLS = New DevExpress.XtraEditors.SimpleButton()
        Me.ceFormato2 = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.spAnio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceFoliarHojas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceEncabezado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seNumFolio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teProgreso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceFormato2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.teProgreso)
        Me.GroupControl1.Controls.Add(Me.btExportXLS)
        Me.GroupControl1.Controls.Add(Me.spAnio)
        Me.GroupControl1.Controls.Add(Me.meMes)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.teTitulo)
        Me.GroupControl1.Controls.Add(Me.ceFormato2)
        Me.GroupControl1.Controls.Add(Me.ceFoliarHojas)
        Me.GroupControl1.Controls.Add(Me.ceEncabezado)
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.seNumFolio)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Size = New System.Drawing.Size(606, 319)
        '
        'spAnio
        '
        Me.spAnio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.spAnio.Location = New System.Drawing.Point(259, 47)
        Me.spAnio.Name = "spAnio"
        Me.spAnio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.spAnio.Properties.Mask.EditMask = "####"
        Me.spAnio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.spAnio.Size = New System.Drawing.Size(61, 20)
        Me.spAnio.TabIndex = 1
        '
        'meMes
        '
        Me.meMes.Location = New System.Drawing.Point(153, 47)
        Me.meMes.Name = "meMes"
        Me.meMes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMes.Size = New System.Drawing.Size(100, 20)
        Me.meMes.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(45, 49)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(105, 13)
        Me.LabelControl1.TabIndex = 21
        Me.LabelControl1.Text = "Mes y Año a Generar:"
        '
        'teTitulo
        '
        Me.teTitulo.EditValue = "LIBRO O REGISTRO DE COMPRAS"
        Me.teTitulo.Location = New System.Drawing.Point(153, 73)
        Me.teTitulo.Name = "teTitulo"
        Me.teTitulo.Size = New System.Drawing.Size(365, 20)
        Me.teTitulo.TabIndex = 2
        '
        'ceFoliarHojas
        '
        Me.ceFoliarHojas.Location = New System.Drawing.Point(151, 161)
        Me.ceFoliarHojas.Name = "ceFoliarHojas"
        Me.ceFoliarHojas.Properties.Caption = "Foliar hojas del libro"
        Me.ceFoliarHojas.Size = New System.Drawing.Size(211, 19)
        Me.ceFoliarHojas.TabIndex = 5
        '
        'ceEncabezado
        '
        Me.ceEncabezado.Location = New System.Drawing.Point(151, 133)
        Me.ceEncabezado.Name = "ceEncabezado"
        Me.ceEncabezado.Properties.Caption = "Incluír encabezados en el libro"
        Me.ceEncabezado.Size = New System.Drawing.Size(178, 19)
        Me.ceEncabezado.TabIndex = 4
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(153, 106)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(261, 20)
        Me.leSucursal.TabIndex = 3
        '
        'seNumFolio
        '
        Me.seNumFolio.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
        Me.seNumFolio.Location = New System.Drawing.Point(155, 215)
        Me.seNumFolio.Name = "seNumFolio"
        Me.seNumFolio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seNumFolio.Properties.Mask.EditMask = "####"
        Me.seNumFolio.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seNumFolio.Size = New System.Drawing.Size(83, 20)
        Me.seNumFolio.TabIndex = 6
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(64, 75)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl2.TabIndex = 18
        Me.LabelControl2.Text = "Título del reporte:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(37, 218)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(113, 13)
        Me.LabelControl4.TabIndex = 19
        Me.LabelControl4.Text = "Número Inicial del Folio:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(106, 109)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl3.TabIndex = 20
        Me.LabelControl3.Text = "Sucursal:"
        '
        'teProgreso
        '
        Me.teProgreso.EditValue = ""
        Me.teProgreso.Location = New System.Drawing.Point(253, 256)
        Me.teProgreso.Name = "teProgreso"
        Me.teProgreso.Size = New System.Drawing.Size(265, 20)
        Me.teProgreso.TabIndex = 68
        Me.teProgreso.Visible = False
        '
        'btExportXLS
        '
        Me.btExportXLS.Location = New System.Drawing.Point(155, 253)
        Me.btExportXLS.Name = "btExportXLS"
        Me.btExportXLS.Size = New System.Drawing.Size(92, 23)
        Me.btExportXLS.TabIndex = 67
        Me.btExportXLS.Text = "&Exportar a Excel"
        '
        'ceFormato2
        '
        Me.ceFormato2.Location = New System.Drawing.Point(151, 186)
        Me.ceFormato2.Name = "ceFormato2"
        Me.ceFormato2.Properties.Caption = "IMPRIMIR EN FORMATO 2"
        Me.ceFormato2.Size = New System.Drawing.Size(211, 19)
        Me.ceFormato2.TabIndex = 5
        '
        'com_frmLibroCompras
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(606, 344)
        Me.Modulo = "Compras"
        Me.Name = "com_frmLibroCompras"
        Me.Text = "Libro de Compras"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.spAnio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTitulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceFoliarHojas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceEncabezado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seNumFolio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teProgreso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceFormato2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents spAnio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents meMes As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTitulo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ceFoliarHojas As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ceEncabezado As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents seNumFolio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teProgreso As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btExportXLS As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ceFormato2 As DevExpress.XtraEditors.CheckEdit

End Class
