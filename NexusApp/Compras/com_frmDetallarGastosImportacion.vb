﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class com_frmDetallarGastosImportaciones
    Dim dtDetalle As New DataTable
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim frmImp As com_frmImportaciones

    Private _IdImportacion As Integer
    Public Property IdImportacion() As Integer
        Get
            Return _IdImportacion
        End Get
        Set(ByVal value As Integer)
            _IdImportacion = value
        End Set
    End Property

    Private Sub com_frmDetallarGastosImportaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        frmImp = Me.Owner
        Dim HabilitarckGastosImp As Boolean = bl.com_HabilitarCheckGastoImportacion()
        gv.Columns("Contabilizar").Visible = HabilitarckGastosImp
        'gc.DataSource = bl.com_ObtenerGastosImportacion(IdImportacion)
        gc.DataSource = frmImp.dtGastosImp
    End Sub

    Private Sub LlenarEntidades()
        Dim dt As DataTable = frmImp.dtGastosImp
        frmImp.dtGastosImp = gc.DataSource

        'For i = 0 To gv.DataRowCount - 1
        '    Dim dr As DataRow = frmImp.dtGastosImp.NewRow()
        '    dr("IdGasto") = gv.GetRowCellValue(i, "IdGasto")
        '    dr("Concepto") = gv.GetRowCellValue(i, "Concepto")
        '    dr("Valor") = gv.GetRowCellValue(i, "Valor")
        '    dr("IdImportacion") = gv.GetRowCellValue(i, "IdImportacion")
        '    dr("Contabilizar") = gv.GetRowCellValue(i, "Contabilizar")
        '    frmImp.dtGastosImp.Rows.Add(dr)
        'Next
        Me.Close()
    End Sub

#Region "Grilla"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        gv.DeleteSelectedRows()
    End Sub


#End Region

    Private Sub sbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGuardar.Click
        LlenarEntidades()
    End Sub
End Class
