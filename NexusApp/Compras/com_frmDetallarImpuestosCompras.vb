﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class com_frmDetallarImpuestosCompras
    Dim dtDetalle As New DataTable
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim dtImpuestos As DataTable

    Private _IdCompra As Integer
    Public Property IdCompra() As Integer
        Get
            Return _IdCompra
        End Get
        Set(ByVal value As Integer)
            _IdCompra = value
        End Set
    End Property
    Private _TotalAfecto As Decimal
    Public Property TotalAfecto() As Decimal
        Get
            Return _TotalAfecto
        End Get
        Set(ByVal value As Decimal)
            _TotalAfecto = value
        End Set
    End Property


    Private Sub com_frmDetallarGastosImportaciones_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        CargarDatos()
    End Sub

    Private Sub LlenarEntidades()
        Dim frmImp As com_frmCompras = Me.Owner
        frmImp.dtImpuestos = CType(gc.DataSource, DataTable)

        'Dim dt As DataTable = frmImp.dtImpuestos

        'For i = 0 To gv.DataRowCount - 1
        '    Dim dr As DataRow = frmImp.dtImpuestos.NewRow()
        '    dr("IdImpuesto") = gv.GetRowCellValue(i, "IdImpuesto")
        '    dr("Nombre") = gv.GetRowCellValue(i, "Nombre")
        '    dr("Valor") = gv.GetRowCellValue(i, "Valor")
        '    dr("IdCompra") = gv.GetRowCellValue(i, "IdCompra")
        '    frmImp.dtImpuestos.Rows.Add(dr)
        'Next
        Me.Close()
    End Sub

    Private Sub sbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGuardar.Click
        LlenarEntidades()
    End Sub

    Private Sub CargarDatos()
        Dim frmImp As com_frmCompras = Me.Owner
        Dim dt As DataTable = frmImp.dtImpuestos

        If dt.Rows.Count > 0 Then
            gc.DataSource = dt
        Else
            gc.DataSource = bl.com_ObtenerImpuestos(IdCompra, TotalAfecto)
        End If
    End Sub
End Class
