﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class com_frmAplicacionNotaCredito
    Dim dtDetalle As New DataTable
    Dim bl As New CuentasPPBLL(g_ConnectionString)
    Dim NotaDetalle As List(Of com_NotasCredito)

    Private _IdComprobante As Integer
    Public Property IdComprobante() As Integer
        Get
            Return _IdComprobante
        End Get
        Set(ByVal value As Integer)
            _IdComprobante = value
        End Set
    End Property

    Private _NumeroNota As System.String
    Public Property NumeroNota() As System.String
        Get
            Return _NumeroNota
        End Get
        Set(ByVal value As System.String)
            _NumeroNota = value
        End Set
    End Property

    Private _FechaNota As System.DateTime
    Public Property FechaNota() As DateTime
        Get
            Return _FechaNota
        End Get
        Set(ByVal value As DateTime)
            _FechaNota = value
        End Set
    End Property

    Private _IdProveedor As System.String
    Public Property IdProveedor() As String
        Get
            Return _IdProveedor
        End Get
        Set(ByVal value As String)
            _IdProveedor = value
        End Set
    End Property

    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property
    Private _MontoAbonar As System.Decimal
    Public Property MontoAbonar() As Decimal
        Get
            Return _MontoAbonar
        End Get
        Set(ByVal value As Decimal)
            _MontoAbonar = value
        End Set
    End Property
    Private _IdSucursal As Integer
    Public Property IdSucursal() As Integer
        Get
            Return _IdSucursal
        End Get
        Set(ByVal value As Integer)
            _IdSucursal = value
        End Set
    End Property
    Private Sub com_frmAplicacionNotaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LimpiaPantalla()
        btObtenerData.Focus()
    End Sub

    Private Sub LlenarEntidades()
        NotaDetalle = New List(Of com_NotasCredito)
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "MontoAbonar") > 0.0 Then  'inserto solo los que aplicaron abono
                Dim entDetalle As New com_NotasCredito
                With entDetalle
                    .IdComprobante = teIdcomprobante.EditValue
                    .IdDetalle = i
                    .NumComprobanteCompra = gv.GetRowCellValue(i, "Numero")
                    .MontoAbonado = gv.GetRowCellValue(i, "MontoAbonar")
                    .SaldoActual = gv.GetRowCellValue(i, "SaldoCompra")
                    .IdComprobanteCompra = gv.GetRowCellValue(i, "IdComprobante")
                End With
                NotaDetalle.Add(entDetalle)
            End If
        Next

    End Sub
    Private Sub CalculaDiferencia()
        gv.UpdateTotalSummary()
        teDiferencia.EditValue = teMontoAbonar.EditValue - colMontoAbonar.SummaryItem.SummaryValue
    End Sub
    Private Sub txtMontoAbonar_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles teMontoAbonar.Validated
        Dim Acum As Decimal = 0.0
        For i = 0 To gv.RowCount - 1
            gv.SetRowCellValue(i, "Abonar", True)
            Dim mSaldo As Decimal = gv.GetRowCellValue(i, "SaldoCompra")
            gv.SetRowCellValue(i, "MontoAbonar", mSaldo)
            Acum += mSaldo
            If Acum > teMontoAbonar.EditValue Then
                gv.SetRowCellValue(i, "MontoAbonar", mSaldo - (Acum - teMontoAbonar.EditValue))
                Exit For
            End If
        Next
        CalculaDiferencia()
    End Sub
    Private Sub chkAbonar_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkAbonar.CheckedChanged
        If gv.EditingValue Then
            Dim mSaldo As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "SaldoCompra")
            gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", mSaldo)
        Else
            gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", 0.0)
        End If
        CalculaDiferencia()
    End Sub

    Private Sub LimpiaPantalla()
        Dim msj As String = ""
        teNumeroComprobante.EditValue = NumeroNota
        teIdcomprobante.EditValue = IdComprobante
        deFechaCompra.EditValue = FechaNota
        beProveedor.beCodigo.EditValue = IdProveedor
        beProveedor.teNombre.EditValue = Nombre
        teMontoAbonar.EditValue = MontoAbonar
        teDiferencia.EditValue = 0.0
        msj = bl.cpp_EliminaNotaCredito(teIdcomprobante.EditValue)
        gc.DataSource = bl.ObtenerSaldosCompras(beProveedor.beCodigo.EditValue, deFechaCompra.EditValue, IdSucursal)
    End Sub

#Region "Grilla"
    'Private Sub InicializaGridsss()
    '    dtDetalle = New DataTable
    '    dtDetalle.Columns.Add(New DataColumn("Abonar", GetType(Boolean)))
    '    dtDetalle.Columns.Add(New DataColumn("NumeroComprob", GetType(String)))
    '    dtDetalle.Columns.Add(New DataColumn("FechaComprob", GetType(Date)))
    '    dtDetalle.Columns.Add(New DataColumn("FechaVencto", GetType(Date)))
    '    dtDetalle.Columns.Add(New DataColumn("TotalComprob", GetType(Decimal)))
    '    dtDetalle.Columns.Add(New DataColumn("SaldoComprob", GetType(Decimal)))
    '    dtDetalle.Columns.Add(New DataColumn("MontoAbonar", GetType(Decimal)))
    '    gc.DataSource = dtDetalle
    'End Sub


    'Private Sub gv_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.GotFocus
    '    SendKeys.SendWait("{enter}")
    'End Sub
    'Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "Abonar", False)
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "NumeroComprob", "")
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "TotalComprob", 0.0)
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "SaldoComprob", 0.0)
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", 0.0)
    'End Sub
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        gv.DeleteSelectedRows()
    End Sub
    'Private Sub AgregaFila(ByVal fila As DataRow)
    '    gv.AddNewRow()
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "Abonar", False)
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "NumeroComprob", fila.Item("Numero"))
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "FechaComprob", fila.Item("Fecha"))
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "FechaVencto", fila.Item("FechaVencto"))
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "TotalComprob", fila.Item("TotalCompra"))
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "SaldoComprob", fila.Item("Saldo"))
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", 0.0)
    'End Sub

#End Region

    Private Sub btObtenerData_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btObtenerData.Click
        gc.DataSource = bl.ObtenerSaldosCompras(beProveedor.beCodigo.EditValue, deFechaCompra.EditValue, IdSucursal)
    End Sub

    Private Sub sbGuardar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles sbGuardar.Click
        If SiEsNulo(beProveedor.beCodigo.EditValue, "") = "" Then
            MsgBox("Debe de especificar el código de proveedor al que desea aplicar el abono", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If teMontoAbonar.EditValue = 0 Then
            MsgBox("Debe de especificar el monto que desea abonar", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If teDiferencia.EditValue <> 0 Or colMontoAbonar.SummaryItem.SummaryValue = 0 Then
            MsgBox("No puede aplicar el abono, hay saldo disponible", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If MsgBox("Está seguro(a) de aplicar éste abono?" & Chr(13) & "Ya no podrá editar los datos", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        LlenarEntidades()
        Dim msj As String = bl.cpp_AplicarNotaCredito(NotaDetalle)
        If msj = "" Then
            MsgBox("La  Nota de Credito ha sido aplicado con éxito", MsgBoxStyle.Information, "Nota")
            Close()
        Else
            MsgBox("NO FUE POSIBLE APLICAR LA NOTA DE CREDITO" + Chr(13) + msj, MsgBoxStyle.Critical, "Nota")
        End If
    End Sub

    Private Sub gv_ValidatingEditor(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Select Case gv.FocusedColumn.FieldName
            Case "MontoAbonar"
                Dim Monto As Decimal = e.Value
                gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", Monto)
                CalculaDiferencia()
        End Select

    End Sub
End Class
