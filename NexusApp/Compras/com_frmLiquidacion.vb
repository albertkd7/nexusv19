﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Imports System.Math
Public Class com_frmLiquidacion
    Dim Header As New com_Liquidacion
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Dim IdCom As Integer

    Private Sub com_frmRequisiciones_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = bl.com_ConsultaDocsLiquidacion(objMenu.User)
        gv2.BestFitColumns()
    End Sub

    Private Sub com_frmCompras_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("COM_LIQUIDACION", "IdComprobante")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        objCombos.banCuentasBancarias(leCtaBancaria)
        objCombos.com_ClaseDocumento(leClaseDocumento)
        objCombos.com_ClaseDocumento(leClaseDocumentoCCF)

        ActivarControles(False)
        gc2.DataSource = bl.com_ConsultaDocsLiquidacion(objMenu.User)
    End Sub
    Private Sub com_frmCompras_Nuevo() Handles Me.Nuevo
        Header = New com_Liquidacion
        ActivarControles(True)
        teSerie.EditValue = ""
        beProveedor.EditValue = ""
        teNumero.EditValue = ""
        deFechaCompra.EditValue = Today
        deFechaContable.EditValue = Today
        teIva.EditValue = 0.0
        'teTotal.EditValue = 0.0
        teTotal.EditValue = 0.0
        TeExento.EditValue = 0.0
        teComision.EditValue = 0.0
        TeNumeroCF.EditValue = ""
        TeSerieCF.EditValue = ""
        teDireccion.EditValue = ""
        teNombre.EditValue = ""
        teNIT.EditValue = ""
        teNRC.EditValue = ""
        leSucursal.EditValue = piIdSucursalUsuario
        leCtaBancaria.EditValue = 1

        teResolucion.EditValue = ""
        teNumeroControlInternoFormularioUnico.EditValue = ""
        leClaseDocumento.EditValue = 1
        teResolucionCCF.EditValue = ""
        teNumeroControlInternoFormularioUnicoCCF.EditValue = ""
        leClaseDocumentoCCF.EditValue = 1

        teSerie.Focus()
        xtcDocs.SelectedTabPage = xtpDatos
    End Sub
    Private Sub com_frmCompras_Guardar() Handles Me.Guardar
        If Not DatosValidos() Then
            Exit Sub
        End If
        CargaEntidades()
        If DbMode = DbModeType.insert Then
            Dim msj As String = bl.InsertaLiquidacion(Header)
            If msj = "" Then
                MsgBox("El documento ha sido guardado con éxito", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox(String.Format("NO FUE POSIBLE GUARDAR EL DOCUMENTO{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
                Return
            End If
        Else
            Dim msj As String = bl.com_ActualizaLiquidacion(Header)
            If msj = "" Then
                MsgBox("El documento ha sido actualizado con éxito", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox(String.Format("No fue posible actualizar el documento{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
                Return
            End If
        End If

        xtcDocs.SelectedTabPage = xtpLista
        gc2.DataSource = bl.com_ConsultaDocsLiquidacion(objMenu.User)
        gc2.Focus()

        MostrarModoInicial()
        ActivarControles(False)
        teSerie.Focus()
        teCorrelativo.EditValue = Header.IdComprobante
    End Sub
    Private Sub com_frmCompras_Editar() Handles Me.Editar
        ActivarControles(True)
        teSerie.Focus()
    End Sub
    Private Sub com_frmCompras_Consulta() Handles Me.Consulta
        ''teCorrelativo.EditValue = objConsultas.com_ConsultaLiquidacion(frmConsultaDetalle)
        '' CargaControles(0)
        'Header = objConsultas.cnsLiquidacion(frmConsultas, 0)

        'IdCom = Header.IdComprobante
        'teCorrelativo.EditValue = IdCom
        'CargaControles(-1)
    End Sub
    Private Sub com_frmCompras_Revertir() Handles Me.Revertir
        xtcDocs.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub
    Private Sub com_frmCompras_Eliminar() Handles Me.Eliminar

        Header = objTablas.com_LiquidacionSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        If Not ValidarFechaCierre(Header.Fecha) Then
            MsgBox("La fecha del docuemnto no corresponde al período activo. Imposible eliminar", MsgBoxStyle.Critical, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If

        If MsgBox("¿Está seguro(a) de eliminar éste documento ?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                'al eliminar un documento de compra se debe confirmar si no está relacionado con otro tipo de info
                Header.ModificadoPor = objMenu.User
                objTablas.com_LiquidacionUpdate(Header)

                objTablas.com_LiquidacionDeleteByPK(Header.IdComprobante)
                MsgBox("El documento fue eliminado con éxito", MsgBoxStyle.Information, "Nota")
                teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("COM_LIQUIDACION", "IdComprobante")

                gc2.DataSource = bl.com_ConsultaDocsLiquidacion(objMenu.User)
            Catch ex As Exception
                MsgBox(String.Format("El documento no pudo ser eliminado{0}{1}", Chr(13), ex.Message), MsgBoxStyle.Critical, "Nota")
            End Try
        End If
    End Sub


    Function DatosValidos() As Boolean
        Dim msj As String = ""

        If teNumero.EditValue = "" OrElse teSerie.EditValue = "" OrElse teResolucion.EditValue = "" Then
            msj = "Debe de especificar la serie, número de documento y resolución del documento"
        End If

        If TeSerieCF.EditValue <> "" Or TeNumeroCF.EditValue <> "" Then
            If TeNumeroCF.EditValue = "" OrElse TeSerieCF.EditValue = "" OrElse teResolucionCCF.EditValue = "" Then
                msj = "Debe de especificar la serie, número de documento y resolución del documento de crédito fiscal"
            End If
            If leClaseDocumentoCCF.EditValue = 2 And teNumeroControlInternoFormularioUnicoCCF.EditValue = "" Then
                msj = "Debe especificar el número de control interno del documento emitido en formulario único para el crédito fiscal"
            End If
        End If


        If leClaseDocumento.EditValue = 2 And teNumeroControlInternoFormularioUnico.EditValue = "" Then
            msj = "Debe especificar el número de control interno del documento emitido en formulario único"
        End If

        If beProveedor.EditValue = "" Then
            msj = "Debe de especificar el código del proveedor"
        End If

        If leCtaBancaria.Text = "" Then
            msj = "Debe de especificar la Cuenta Bancaria"
        End If

        If teNRC.EditValue = "" Or teNIT.EditValue = "" Then
            msj = "Debe de especificar el NIT y NRC del proveedor"
        End If

        If leSucursal.EditValue = 0 Or leSucursal.EditValue = Nothing Or leSucursal.Text = "" Then
            msj = "Debe seleccionar una sucursal"
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(deFechaContable.EditValue)

        If Not EsOk Then
            msj = "La fecha de la compra no corresponde al período activo"
        End If

        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If
        Return True
    End Function


    Private Sub CargaEntidades()
        With Header
            .Serie = teSerie.Text
            .Numero = teNumero.EditValue
            .IdProveedor = beProveedor.EditValue
            .Nombre = teNombre.Text
            .Nrc = teNRC.EditValue
            .Nit = teNIT.Text
            .Fecha = deFechaCompra.EditValue
            .FechaContable = deFechaContable.DateTime
            .Direccion = teDireccion.Text
            .TotalIva = teIva.EditValue
            .TotalComprobante = teTotal.EditValue
            .Comision = teComision.EditValue
            .IdSucursal = leSucursal.EditValue
            .IdCuentaBancaria = leCtaBancaria.EditValue
            .TotalExento = TeExento.EditValue
            .NumeroCF = TeNumeroCF.EditValue
            .SerieCF = TeSerieCF.EditValue

            .Resolucion = teResolucion.EditValue
            .NumControlInterno = teNumeroControlInternoFormularioUnico.EditValue
            .ClaseDocumento = leClaseDocumento.EditValue

            .ResolucionCF = teResolucionCCF.EditValue
            .NumControlInternoCF = teNumeroControlInternoFormularioUnicoCCF.EditValue
            .ClaseDocumentoCF = leClaseDocumentoCCF.EditValue

            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If
        End With
    End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        For Each ctrl In gcCCF.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next

        teCorrelativo.Properties.ReadOnly = True
    End Sub
    Private Sub beProveedor_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beProveedor.ButtonClick
        beProveedor.EditValue = ""
        beProveedor_Validated(sender, New System.EventArgs)
    End Sub
    Private Sub beProveedor_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beProveedor.Validated
        Dim entProveedor As com_Proveedores = objConsultas.cnsProveedores(frmConsultas, beProveedor.EditValue)
        beProveedor.EditValue = entProveedor.IdProveedor
        teNombre.EditValue = entProveedor.Nombre
        teDireccion.EditValue = entProveedor.Direccion
        teNRC.EditValue = entProveedor.Nrc
        teNIT.EditValue = entProveedor.Nit
    End Sub
    Private Sub com_frmCompras_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Dispose()
    End Sub
    Private Sub deFechaCompra_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deFechaCompra.EditValueChanged
        deFechaCompra.EditValue = deFechaCompra.EditValue
    End Sub

    Private Sub teTotal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles teTotal.EditValueChanged
        teIva.EditValue = Round((teTotal.EditValue / (pnIVA + 1)) * 0.02, 2)
    End Sub

    Private Sub sbAgregarCompra_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If rbMenu.qbSave.Visibility = DevExpress.XtraBars.BarItemVisibility.Never Then
            Exit Sub
        End If
        com_frmCompras.sbGuardar.Visible = True
        com_frmCompras.deFechaCompra.EditValue = deFechaCompra.EditValue
        com_frmCompras.deFechaContable.EditValue = deFechaContable.EditValue
        com_frmCompras.beProveedor.EditValue = beProveedor.EditValue
        com_frmCompras.OrigenCompra = "Liquidacion"
        com_frmCompras.xtcCompras.SelectedTabPage = com_frmCompras.xtpDatos1
        com_frmCompras.xtpLista1.PageVisible = False
        com_frmCompras.ShowDialog()
    End Sub
    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        Header = objTablas.com_LiquidacionSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        DbMode = DbModeType.update
        teNumero.Focus()
    End Sub
    Private Sub CargaPantalla()
        xtcDocs.SelectedTabPage = xtpDatos
        teNumero.Focus()

        With Header
            teCorrelativo.EditValue = .IdComprobante
            teSerie.EditValue = .Serie
            teNumero.EditValue = .Numero
            deFechaCompra.EditValue = .Fecha
            deFechaContable.DateTime = .FechaContable
            beProveedor.EditValue = .IdProveedor
            teNombre.EditValue = .Nombre
            teNRC.EditValue = .Nrc
            teNIT.EditValue = .Nit
            teDireccion.EditValue = .Direccion
            teTotal.EditValue = .TotalComprobante
            teIva.EditValue = .TotalIva
            TeExento.EditValue = .TotalExento
            teComision.EditValue = .Comision
            leSucursal.EditValue = .IdSucursal
            leCtaBancaria.EditValue = .IdCuentaBancaria
            TeSerieCF.EditValue = .SerieCF
            TeNumeroCF.EditValue = .NumeroCF

            teResolucion.EditValue = .Resolucion
            teNumeroControlInternoFormularioUnico.EditValue = .NumControlInterno
            leClaseDocumento.EditValue = .ClaseDocumento

            teResolucionCCF.EditValue = .ResolucionCF
            teNumeroControlInternoFormularioUnicoCCF.EditValue = .NumControlInternoCF
            leClaseDocumentoCCF.EditValue = .ClaseDocumentoCF
        End With
    End Sub

    Private Sub sbImportar_Click(sender As Object, e As EventArgs) Handles sbImportar.Click
        If DbMode = DbModeType.insert Then
            com_frmCargarComprobantesLiquidacion.IdSucursal = leSucursal.EditValue
            com_frmCargarComprobantesLiquidacion.ShowDialog()
            xtcDocs.SelectedTabPage = xtpLista
            gc2.Focus()
            ActivarControles(False)
            MostrarModoInicial()
            gc2.DataSource = bl.com_ConsultaDocsLiquidacion(objMenu.User)
        End If
    End Sub
End Class
