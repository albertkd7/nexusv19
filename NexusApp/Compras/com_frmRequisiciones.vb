﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns
Imports System.IO

Public Class com_frmRequisiciones
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim entProducto As inv_Productos
    Dim Header As New com_Requisiciones
    Dim Detalle As List(Of com_RequisicionesDetalle)
    Dim EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim IdSucUser As Integer = SiEsNulo(EntUsuario.IdSucursal, 0)


    Private Sub com_frmRequisiciones_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = bl.com_ConsultaRequisiciones(objMenu.User).Tables(0)
        gv2.BestFitColumns()
    End Sub


    Private Sub com_frmRequisiciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("COM_REQUISICIONES", "IdComprobante")
        CargaControles(0)
        ActivarControles(False)
        gc2.DataSource = bl.com_ConsultaRequisiciones(objMenu.User).Tables(0)
    End Sub
    Private Sub com_frmRequisiciones_Nuevo() Handles MyBase.Nuevo
        Header = New com_Requisiciones
        ActivarControles(True)

        gc.DataSource = bl.com_ObtenerDetalleRequisicion(-1, "com_RequisicionesDetalle")
        gv.CancelUpdateCurrentRow()
        'gv.AddNewRow()

        teNumero.EditValue = ""
        deFecha.EditValue = Today
        meConcepto.EditValue = ""

        leSucursal.EditValue = piIdSucursalUsuario

        teNumero.Focus()
        xtcRequisiciones.SelectedTabPage = xtpDatos
    End Sub
    Private Sub com_frmRequisiciones_Guardar() Handles MyBase.Guardar
        Dim msj As String = ""
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "IdProducto") = gv.GetRowCellValue(i + 1, "IdProducto") Then
                msj = "El producto " & gv.GetRowCellValue(i, "IdProducto") & ",  está repetido"
                Exit For
            End If
        Next
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.None
        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If
        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            MsgBox("La fecha del documento debe ser de un período válido", 16, "Error de datos")
            Exit Sub
        End If
        CargarEntidades()

        If DbMode = DbModeType.insert Then
            msj = bl.com_InsertaRequisiciones(Header, Detalle)

            If msj = "" Then
                MsgBox("El comprobante ha sido registrado con éxito", MsgBoxStyle.Information, "Nota")
            Else
                MsgBox("SE DETECTÓ UN ERROR AL CREAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
                Exit Sub
            End If
        Else
            msj = bl.com_ActualizaRequisiciones(Header, Detalle)
            If msj = "" Then
                MsgBox("El documento ha sido actualizado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("SE DETECTÓ UN ERROR AL ACTUALIZAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If

        xtcRequisiciones.SelectedTabPage = xtpLista
        gc2.DataSource = bl.com_ConsultaRequisiciones(objMenu.User).Tables(0)
        gc2.Focus()

        teCorrelativo.EditValue = Header.IdComprobante
        MostrarModoInicial()
        teCorrelativo.Focus()
        ActivarControles(False)
    End Sub
    Private Sub com_frmRequisiciones_Editar() Handles MyBase.Editar

        ActivarControles(True)
        teNumero.Focus()
    End Sub
    Private Sub com_frmRequisiciones_Consulta() Handles MyBase.Consulta
        'teCorrelativo.EditValue = objConsultas.com_ConsultaRequisiciones(frmConsultaDetalle)
        'CargaControles(0)
    End Sub
    Private Sub com_frmRequisiciones_Revertir() Handles MyBase.Revertir
        xtcRequisiciones.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub
    Private Sub com_frmRequisiciones_Eliminar() Handles MyBase.Eliminar
        Header = objTablas.com_RequisicionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))

        If MsgBox("¿Está seguro(a) de eliminar la transacción?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            objTablas.com_RequisicionesDeleteByPK(Header.IdComprobante)
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("COM_REQUISICIONES", "IdComprobante")
            CargaControles(0)
            ActivarControles(False)
            gc2.DataSource = bl.com_ConsultaRequisiciones(objMenu.User).Tables(0)
        End If
    End Sub

#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")

    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), "") = "" Then
            e.Valid = False
        End If
    End Sub

    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gv.EditingValue = IdProd
                End If
                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)

                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                End If
            End If
        End If

    End Sub
#End Region

    Public Sub CargaControles(ByVal TipoAvance As Integer)

        'If TipoAvance = 0 Then 'no es necesario obtener el Id del comprobante
        'Else
        '    teCorrelativo.EditValue = bl.com_ObtenerIdRequisicion(teCorrelativo.EditValue, TipoAvance, "com_Requisiciones")
        'End If

        'If teCorrelativo.EditValue = 0 Then
        '    Exit Sub
        'End If

        'Header = objTablas.com_RequisicionesSelectByPK(teCorrelativo.EditValue)

        'If Header Is Nothing Then
        '    Exit Sub
        'End If
        'With Header
        '    teCorrelativo.EditValue = .IdComprobante
        '    deFecha.EditValue = .Fecha
        '    teNumero.EditValue = .Numero
        '    meConcepto.EditValue = .Concepto
        '    leSucursal.EditValue = .IdSucursal
        '    gc.DataSource = bl.com_ObtenerDetalleRequisicion(.IdComprobante, "com_RequisicionesDetalle")
        'End With

    End Sub

    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
    End Sub
    Private Sub CargarEntidades()
        With Header
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
            Else
                .ModificadoPor = objMenu.User
                .FechaHoraModificacion = Now
            End If
            .IdComprobante = teCorrelativo.EditValue  'el id se asignará en la capa de datos

            .Numero = teNumero.EditValue
            .Fecha = deFecha.EditValue
            .Concepto = meConcepto.EditValue

            .IdSucursal = leSucursal.EditValue

        End With

        Detalle = New List(Of com_RequisicionesDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New com_RequisicionesDetalle
            With entDetalle
                .IdComprobante = Header.IdComprobante
                .IdDetalle = i + 1
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .CantidadAutorizada = gv.GetRowCellValue(i, "Cantidad")
            End With
            Detalle.Add(entDetalle)
        Next

    End Sub

    Private Sub inv_frmEntradas_Reporte() Handles Me.Reporte
        Header = objTablas.com_RequisicionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Dim dt As DataTable = bl.com_ObtenerDetalleRequisicion(Header.IdComprobante, "com_RequisicionesDetalle")
        Dim rpt As New com_rptRequisiciones() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlNumero.Text = Header.Numero

        rpt.xrlConcepto.Text = Header.Concepto
        rpt.xrlFecha.Text = Header.Fecha
        rpt.xrlCorrel.Text = Header.IdComprobante

        rpt.ShowPreviewDialog()
    End Sub


    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        Header = objTablas.com_RequisicionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        DbMode = DbModeType.update
        teNumero.Focus()
    End Sub
    Private Sub CargaPantalla()
        xtcRequisiciones.SelectedTabPage = xtpDatos
        teNumero.Focus()

        With Header
            teCorrelativo.EditValue = .IdComprobante
            deFecha.EditValue = .Fecha
            teNumero.EditValue = .Numero
            meConcepto.EditValue = .Concepto
            leSucursal.EditValue = .IdSucursal
            gc.DataSource = bl.com_ObtenerDetalleRequisicion(.IdComprobante, "com_RequisicionesDetalle")
        End With

    End Sub
End Class
