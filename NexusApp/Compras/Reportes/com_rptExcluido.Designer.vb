﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class com_rptExcluido
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LecturaActual2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlTotal = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNombre = New DevExpress.XtraReports.UI.XRLabel
        Me.FechaCargo2 = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
        Me.xrlFecha = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNrc = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCantLetras = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlFormaPago = New DevExpress.XtraReports.UI.XRLabel
        Me.LecturaAnterior2 = New DevExpress.XtraReports.UI.XRLabel
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand
        Me.xrlNumero = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDepto = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlDireccion = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlNit = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
        Me.Factura2 = New DevExpress.XtraReports.UI.XRLabel
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand
        Me.xrlGiro = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlCodigo = New DevExpress.XtraReports.UI.XRLabel
        Me.ConsumoM32 = New DevExpress.XtraReports.UI.XRLabel
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand
        Me.xrlDiasCredito = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlMunic = New DevExpress.XtraReports.UI.XRLabel
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
        Me.xrlSubTotal = New DevExpress.XtraReports.UI.XRLabel
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'LecturaActual2
        '
        Me.LecturaActual2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.Descripcion")})
        Me.LecturaActual2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LecturaActual2.LocationFloat = New DevExpress.Utils.PointFloat(85.83328!, 0.0!)
        Me.LecturaActual2.Name = "LecturaActual2"
        Me.LecturaActual2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LecturaActual2.SizeF = New System.Drawing.SizeF(290.4999!, 16.37481!)
        Me.LecturaActual2.StylePriority.UseFont = False
        Me.LecturaActual2.StylePriority.UseTextAlignment = False
        Me.LecturaActual2.Text = "LecturaActual2"
        Me.LecturaActual2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlTotal
        '
        Me.xrlTotal.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlTotal.LocationFloat = New DevExpress.Utils.PointFloat(608.4999!, 106.0832!)
        Me.xrlTotal.Name = "xrlTotal"
        Me.xrlTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTotal.SizeF = New System.Drawing.SizeF(94.0!, 15.99998!)
        Me.xrlTotal.StylePriority.UseBorders = False
        Me.xrlTotal.StylePriority.UseFont = False
        Me.xrlTotal.StylePriority.UseTextAlignment = False
        Me.xrlTotal.Text = "0.00"
        Me.xrlTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel2
        '
        Me.XrLabel2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(559.2084!, 56.62506!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(67.0!, 16.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "SUB TOTAL"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'xrlNombre
        '
        Me.xrlNombre.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.Nombre")})
        Me.xrlNombre.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlNombre.LocationFloat = New DevExpress.Utils.PointFloat(37.75003!, 30.0!)
        Me.xrlNombre.Name = "xrlNombre"
        Me.xrlNombre.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNombre.SizeF = New System.Drawing.SizeF(394.9579!, 16.0!)
        Me.xrlNombre.StylePriority.UseFont = False
        Me.xrlNombre.StylePriority.UseTextAlignment = False
        Me.xrlNombre.Text = "xrlNombre"
        Me.xrlNombre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'FechaCargo2
        '
        Me.FechaCargo2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.IdProducto")})
        Me.FechaCargo2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaCargo2.LocationFloat = New DevExpress.Utils.PointFloat(5.0!, 0.0!)
        Me.FechaCargo2.Name = "FechaCargo2"
        Me.FechaCargo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.FechaCargo2.SizeF = New System.Drawing.SizeF(75.16668!, 16.37481!)
        Me.FechaCargo2.StylePriority.UseFont = False
        Me.FechaCargo2.Text = "FechaCargo2"
        '
        'XrLabel5
        '
        Me.XrLabel5.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(20.62503!, 47.87503!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(36.00002!, 15.0!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "SON:"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.xrlCodigo, Me.xrlDiasCredito, Me.xrlDepto, Me.xrlMunic, Me.xrlNit, Me.xrlDireccion, Me.xrlFecha, Me.xrlNrc, Me.xrlNombre, Me.xrlFormaPago, Me.xrlGiro, Me.xrlNumero, Me.XrLabel1})
        Me.PageHeader.HeightF = 135.3749!
        Me.PageHeader.Name = "PageHeader"
        Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFecha
        '
        Me.xrlFecha.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.Fecha", "{0:dd/MM/yyyy}")})
        Me.xrlFecha.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlFecha.LocationFloat = New DevExpress.Utils.PointFloat(625.708!, 64.99998!)
        Me.xrlFecha.Name = "xrlFecha"
        Me.xrlFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlFecha.StylePriority.UseFont = False
        Me.xrlFecha.StylePriority.UseTextAlignment = False
        Me.xrlFecha.Text = "xrlFecha"
        Me.xrlFecha.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNrc
        '
        Me.xrlNrc.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.Nrc")})
        Me.xrlNrc.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNrc.LocationFloat = New DevExpress.Utils.PointFloat(97.79164!, 79.24998!)
        Me.xrlNrc.Name = "xrlNrc"
        Me.xrlNrc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNrc.SizeF = New System.Drawing.SizeF(136.0!, 15.0!)
        Me.xrlNrc.StylePriority.UseFont = False
        Me.xrlNrc.StylePriority.UseTextAlignment = False
        Me.xrlNrc.Text = "xrlNrc"
        Me.xrlNrc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCantLetras
        '
        Me.xrlCantLetras.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlCantLetras.LocationFloat = New DevExpress.Utils.PointFloat(57.62504!, 46.875!)
        Me.xrlCantLetras.Name = "xrlCantLetras"
        Me.xrlCantLetras.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCantLetras.SizeF = New System.Drawing.SizeF(443.4583!, 17.00001!)
        Me.xrlCantLetras.StylePriority.UseFont = False
        Me.xrlCantLetras.StylePriority.UseTextAlignment = False
        Me.xrlCantLetras.Text = "DIEZ"
        Me.xrlCantLetras.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'xrlFormaPago
        '
        Me.xrlFormaPago.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.FormaPago")})
        Me.xrlFormaPago.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlFormaPago.LocationFloat = New DevExpress.Utils.PointFloat(649.7917!, 95.24995!)
        Me.xrlFormaPago.Name = "xrlFormaPago"
        Me.xrlFormaPago.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFormaPago.SizeF = New System.Drawing.SizeF(104.2081!, 17.00001!)
        Me.xrlFormaPago.StylePriority.UseFont = False
        Me.xrlFormaPago.StylePriority.UseTextAlignment = False
        Me.xrlFormaPago.Text = "xrlFormaPago"
        Me.xrlFormaPago.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'LecturaAnterior2
        '
        Me.LecturaAnterior2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.Cantidad", "{0:n3}")})
        Me.LecturaAnterior2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LecturaAnterior2.LocationFloat = New DevExpress.Utils.PointFloat(459.3332!, 0.0!)
        Me.LecturaAnterior2.Name = "LecturaAnterior2"
        Me.LecturaAnterior2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LecturaAnterior2.SizeF = New System.Drawing.SizeF(63.37466!, 16.37481!)
        Me.LecturaAnterior2.StylePriority.UseFont = False
        Me.LecturaAnterior2.StylePriority.UseTextAlignment = False
        Me.LecturaAnterior2.Text = "LecturaAnterior2"
        Me.LecturaAnterior2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.HeightF = 30.20884!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        Me.BottomMarginBand1.SnapLinePadding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        '
        'xrlNumero
        '
        Me.xrlNumero.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.Numero")})
        Me.xrlNumero.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNumero.LocationFloat = New DevExpress.Utils.PointFloat(626.6663!, 31.99998!)
        Me.xrlNumero.Name = "xrlNumero"
        Me.xrlNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNumero.SizeF = New System.Drawing.SizeF(76.04175!, 15.0!)
        Me.xrlNumero.StylePriority.UseFont = False
        Me.xrlNumero.StylePriority.UseTextAlignment = False
        Me.xrlNumero.Text = "xrlNumero"
        Me.xrlNumero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel3
        '
        Me.XrLabel3.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(38.70831!, 79.24995!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(55.00001!, 15.0!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "Registro:"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDepto
        '
        Me.xrlDepto.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.Departamento")})
        Me.xrlDepto.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDepto.LocationFloat = New DevExpress.Utils.PointFloat(299.2083!, 64.33327!)
        Me.xrlDepto.Name = "xrlDepto"
        Me.xrlDepto.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDepto.SizeF = New System.Drawing.SizeF(132.0001!, 15.00002!)
        Me.xrlDepto.StylePriority.UseFont = False
        Me.xrlDepto.StylePriority.UseTextAlignment = False
        Me.xrlDepto.Text = "xrlDepto"
        Me.xrlDepto.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDireccion
        '
        Me.xrlDireccion.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.Direccion")})
        Me.xrlDireccion.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlDireccion.LocationFloat = New DevExpress.Utils.PointFloat(37.75003!, 45.99998!)
        Me.xrlDireccion.Name = "xrlDireccion"
        Me.xrlDireccion.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDireccion.SizeF = New System.Drawing.SizeF(394.9579!, 15.00002!)
        Me.xrlDireccion.StylePriority.UseFont = False
        Me.xrlDireccion.StylePriority.UseTextAlignment = False
        Me.xrlDireccion.Text = "xrlDireccion"
        Me.xrlDireccion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlNit
        '
        Me.xrlNit.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlNit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.Nit")})
        Me.xrlNit.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlNit.LocationFloat = New DevExpress.Utils.PointFloat(38.70831!, 94.24994!)
        Me.xrlNit.Name = "xrlNit"
        Me.xrlNit.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlNit.SizeF = New System.Drawing.SizeF(133.0!, 16.0!)
        Me.xrlNit.StylePriority.UseBorders = False
        Me.xrlNit.StylePriority.UseFont = False
        Me.xrlNit.StylePriority.UseTextAlignment = False
        Me.xrlNit.Text = "xrlNit"
        Me.xrlNit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(584.2084!, 106.0832!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(42.0!, 16.0!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "TOTAL"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'Factura2
        '
        Me.Factura2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.ValorAfecto")})
        Me.Factura2.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Factura2.LocationFloat = New DevExpress.Utils.PointFloat(630.4584!, 0.0!)
        Me.Factura2.Name = "Factura2"
        Me.Factura2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.Factura2.SizeF = New System.Drawing.SizeF(73.74994!, 16.37482!)
        Me.Factura2.StylePriority.UseFont = False
        Me.Factura2.StylePriority.UseTextAlignment = False
        Me.Factura2.Text = "Factura2"
        Me.Factura2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.Factura2, Me.FechaCargo2, Me.LecturaAnterior2, Me.ConsumoM32, Me.LecturaActual2})
        Me.Detail.HeightF = 16.37482!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlGiro
        '
        Me.xrlGiro.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlGiro.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.Giro")})
        Me.xrlGiro.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlGiro.LocationFloat = New DevExpress.Utils.PointFloat(212.7916!, 94.24998!)
        Me.xrlGiro.Name = "xrlGiro"
        Me.xrlGiro.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlGiro.SizeF = New System.Drawing.SizeF(331.4585!, 16.0!)
        Me.xrlGiro.StylePriority.UseBorders = False
        Me.xrlGiro.StylePriority.UseFont = False
        Me.xrlGiro.StylePriority.UseTextAlignment = False
        Me.xrlGiro.Text = "xrlGiro"
        Me.xrlGiro.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlCodigo
        '
        Me.xrlCodigo.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.IdProveedor")})
        Me.xrlCodigo.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlCodigo.LocationFloat = New DevExpress.Utils.PointFloat(574.6663!, 96.24997!)
        Me.xrlCodigo.Name = "xrlCodigo"
        Me.xrlCodigo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCodigo.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlCodigo.StylePriority.UseFont = False
        Me.xrlCodigo.StylePriority.UseTextAlignment = False
        Me.xrlCodigo.Text = "xrlCodigo"
        Me.xrlCodigo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ConsumoM32
        '
        Me.ConsumoM32.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.PrecioUnitario", "{0:n4}")})
        Me.ConsumoM32.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsumoM32.LocationFloat = New DevExpress.Utils.PointFloat(511.7079!, 0.0!)
        Me.ConsumoM32.Name = "ConsumoM32"
        Me.ConsumoM32.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.ConsumoM32.SizeF = New System.Drawing.SizeF(76.41669!, 16.37482!)
        Me.ConsumoM32.StylePriority.UseFont = False
        Me.ConsumoM32.StylePriority.UseTextAlignment = False
        Me.ConsumoM32.Text = "ConsumoM32"
        Me.ConsumoM32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel4, Me.XrLabel2, Me.xrlCantLetras, Me.xrlTotal, Me.xrlSubTotal})
        Me.PageFooter.HeightF = 151.0415!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.HeightF = 80.0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'xrlDiasCredito
        '
        Me.xrlDiasCredito.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.DiasCredito")})
        Me.xrlDiasCredito.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlDiasCredito.LocationFloat = New DevExpress.Utils.PointFloat(37.75004!, 10.00001!)
        Me.xrlDiasCredito.Name = "xrlDiasCredito"
        Me.xrlDiasCredito.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDiasCredito.SizeF = New System.Drawing.SizeF(77.0!, 15.0!)
        Me.xrlDiasCredito.StylePriority.UseFont = False
        Me.xrlDiasCredito.StylePriority.UseTextAlignment = False
        Me.xrlDiasCredito.Text = "xrlDiasCredito"
        Me.xrlDiasCredito.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.xrlDiasCredito.Visible = False
        '
        'xrlMunic
        '
        Me.xrlMunic.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Compra.Municipio")})
        Me.xrlMunic.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.xrlMunic.LocationFloat = New DevExpress.Utils.PointFloat(38.7083!, 64.33327!)
        Me.xrlMunic.Name = "xrlMunic"
        Me.xrlMunic.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlMunic.SizeF = New System.Drawing.SizeF(132.0001!, 15.00002!)
        Me.xrlMunic.StylePriority.UseFont = False
        Me.xrlMunic.StylePriority.UseTextAlignment = False
        Me.xrlMunic.Text = "xrlMunic"
        Me.xrlMunic.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Font = New System.Drawing.Font("Trebuchet MS", 9.0!)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(174.7083!, 94.24998!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(36.00002!, 15.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "Giro:"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlSubTotal
        '
        Me.xrlSubTotal.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlSubTotal.LocationFloat = New DevExpress.Utils.PointFloat(608.4999!, 54.87506!)
        Me.xrlSubTotal.Name = "xrlSubTotal"
        Me.xrlSubTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlSubTotal.SizeF = New System.Drawing.SizeF(94.0!, 19.0!)
        Me.xrlSubTotal.StylePriority.UseFont = False
        Me.xrlSubTotal.StylePriority.UseTextAlignment = False
        Me.xrlSubTotal.Text = "0.00"
        Me.xrlSubTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'com_rptExcluido
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
        Me.DataMember = "Compra"
        Me.DesignerOptions.ShowExportWarnings = False
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(21, 40, 80, 30)
        Me.PageHeight = 550
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PaperName = "Facturapeq"
        Me.SnapGridSize = 2.083333!
        Me.SnapToGrid = False
        Me.Version = "11.1"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents LecturaActual2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNombre As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents FechaCargo2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCodigo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDiasCredito As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDepto As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlMunic As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNit As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlDireccion As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFecha As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNrc As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFormaPago As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlGiro As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlNumero As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCantLetras As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents LecturaAnterior2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Factura2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents ConsumoM32 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents xrlSubTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
End Class
