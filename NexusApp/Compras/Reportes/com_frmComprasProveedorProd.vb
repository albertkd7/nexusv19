﻿
Imports NexusBLL
Public Class com_frmComprasProveedorProd
    Dim bl As New ComprasBLL(g_ConnectionString)


    Private Sub SimpleButton1_Click() Handles Me.Reporte
        Dim dt As DataTable = bl.com_ComprasProveedorProducto(deFechaI.DateTime, deFechaF.DateTime, leBodega.EditValue, BeProveedor1.beCodigo.EditValue, leSucursal.EditValue)
        Dim rpt As New com_rptComprasProveedorProducto() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue + " - " & leSucursal.Text
        rpt.xrlPeriodo.Text = (FechaToString(deFechaI.DateTime, deFechaF.DateTime)).ToUpper
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub repComprasProveedor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFechaI.DateTime = Today
        deFechaF.DateTime = Today
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub
End Class
