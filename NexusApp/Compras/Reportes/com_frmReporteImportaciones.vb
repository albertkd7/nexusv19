﻿
Imports NexusBLL
Public Class com_frmReporteImportaciones
    Dim bl As New ComprasBLL(g_ConnectionString), blAdmon As New AdmonBLL(g_ConnectionString), dtParam As DataTable = blAdmon.ObtieneParametros
    Private Sub com_frmReporteImportaciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        deFechaI.EditValue = Today
        deFechaF.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub com_frmReporteImportaciones_Reporte() Handles Me.Reporte
        Dim rpt As New com_rptListadoImportaciones
        'rpt.DataAdapter =
        rpt.DataSource = bl.com_ObtenerListadoImportaciones(leSucursal.EditValue, deFechaI.EditValue, deFechaF.EditValue)
        rpt.DataMember = ""
        rpt.xrlPeriodo.Text = FechaToString(deFechaI.EditValue, deFechaF.EditValue)
        rpt.xrlNIT.Text = "NIT: " & dtParam.Rows(0).Item("NitEmpresa")
        rpt.xrlNRC.Text = "NRC: " & dtParam.Rows(0).Item("NrcEmpresa")
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.Text
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.ShowPreviewDialog()
    End Sub
End Class
