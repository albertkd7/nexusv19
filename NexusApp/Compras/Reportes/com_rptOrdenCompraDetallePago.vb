Public Class com_rptOrdenCompraDetallePago
    Private nValorExento As Decimal
    Private nValorGravado As Decimal

    Private Sub XrLabel15_SummaryRowChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        nValorExento += GetCurrentColumnValue("ValorExento")
    End Sub

    Private Sub XrLabel15_SummaryGetResult(ByVal sender As System.Object, ByVal e As DevExpress.XtraReports.UI.SummaryGetResultEventArgs)
        e.Result = nValorExento
        e.Handled = True
    End Sub

    Private Sub xrlTotal_SummaryRowChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        nValorGravado += GetCurrentColumnValue("ValorAfecto")
    End Sub

    Private Sub xrlTotal_SummaryGetResult(ByVal sender As System.Object, ByVal e As DevExpress.XtraReports.UI.SummaryGetResultEventArgs)
        e.Result = nValorGravado
        e.Handled = True
    End Sub
End Class