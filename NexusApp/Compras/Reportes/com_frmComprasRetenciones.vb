﻿
Imports NexusBLL
Public Class com_frmComprasRetenciones
    Dim bl As New ComprasBLL(g_ConnectionString)


    Private Sub repComprasPeriodo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFechaI.DateTime = Today
        deFechaF.DateTime = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub SimpleButton1_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.com_ComprasRetencion(deFechaI.DateTime, deFechaF.DateTime, BeProveedor1.beCodigo.EditValue, leSucursal.EditValue)
        Dim rpt As New com_rptComprasRetencion() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = txtTitulo.EditValue + " -  SUCURSAL: " & leSucursal.Text
        rpt.xrlPeriodo.Text = (FechaToString(deFechaI.DateTime, deFechaF.DateTime)).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.ShowPreviewDialog()
    End Sub
End Class
