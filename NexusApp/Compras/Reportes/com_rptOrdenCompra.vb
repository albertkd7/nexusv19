Public Class com_rptOrdenCompra
    Private nValorExento As Decimal
    Private nValorGravado As Decimal
    Private nCorre As Integer = 1

    Private Sub XrLabel15_SummaryRowChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        nValorExento += GetCurrentColumnValue("ValorExento")
    End Sub

    Private Sub XrLabel15_SummaryGetResult(ByVal sender As System.Object, ByVal e As DevExpress.XtraReports.UI.SummaryGetResultEventArgs)
        e.Result = nValorExento
        e.Handled = True
    End Sub

    Private Sub xrlTotal_SummaryRowChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles xrlTotal.SummaryRowChanged
        nValorGravado += GetCurrentColumnValue("ValorAfecto")
    End Sub

    Private Sub xrlTotal_SummaryGetResult(ByVal sender As System.Object, ByVal e As DevExpress.XtraReports.UI.SummaryGetResultEventArgs) Handles xrlTotal.SummaryGetResult
        e.Result = nValorGravado
        e.Handled = True
    End Sub

    Private Sub xrlCorre_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlCorre.BeforePrint
        xrlCorre.Text = nCorre
        nCorre += 1
    End Sub
End Class