<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class com_rptCartaAduana
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(com_rptCartaAduana))
        Me.xrlPage = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrpNumero = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.xrlEmpresa = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlCorre = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.xrlFecha = New DevExpress.XtraReports.UI.XRLabel()
        Me.DsBancos1 = New Nexus.dsBancos()
        Me.DsCompras1 = New Nexus.dsCompras()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.xrlTelefonos = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrLogo = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.xrlCartaAduana = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlDireccionEmpresa = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell5 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell9 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.xrlPaisProveedor = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlProveedor = New DevExpress.XtraReports.UI.XRLabel()
        Me.xrlFactura = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me.DsBancos1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsCompras1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'xrlPage
        '
        Me.xrlPage.Dpi = 100.0!
        Me.xrlPage.LocationFloat = New DevExpress.Utils.PointFloat(907.0!, 0.0!)
        Me.xrlPage.Name = "xrlPage"
        Me.xrlPage.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPage.SizeF = New System.Drawing.SizeF(56.0!, 22.0!)
        Me.xrlPage.Text = "P�g. No."
        '
        'XrControlStyle1
        '
        Me.XrControlStyle1.Name = "XrControlStyle1"
        Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'XrLabel11
        '
        Me.XrLabel11.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "OrdenCompra.Descripcion", "{0:n2}")})
        Me.XrLabel11.Dpi = 100.0!
        Me.XrLabel11.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(189.4583!, 0.0!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.Suppress
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(341.5417!, 16.0!)
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "XrLabel11"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrpNumero
        '
        Me.xrpNumero.Dpi = 100.0!
        Me.xrpNumero.LocationFloat = New DevExpress.Utils.PointFloat(966.0!, 0.0!)
        Me.xrpNumero.Name = "xrpNumero"
        Me.xrpNumero.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrpNumero.SizeF = New System.Drawing.SizeF(66.0!, 22.0!)
        '
        'xrlEmpresa
        '
        Me.xrlEmpresa.Dpi = 100.0!
        Me.xrlEmpresa.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.xrlEmpresa.LocationFloat = New DevExpress.Utils.PointFloat(357.5!, 0.0!)
        Me.xrlEmpresa.Name = "xrlEmpresa"
        Me.xrlEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlEmpresa.SizeF = New System.Drawing.SizeF(537.5!, 22.0!)
        Me.xrlEmpresa.StylePriority.UseFont = False
        Me.xrlEmpresa.StylePriority.UseTextAlignment = False
        Me.xrlEmpresa.Text = "xrlEmpresa"
        Me.xrlEmpresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "OrdenCompra.IdProducto")})
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(96.0!, 0.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(91.0!, 16.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.Text = "XrLabel2"
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel20, Me.xrlCorre, Me.XrLabel11, Me.XrLabel1, Me.XrLabel3, Me.XrLabel9})
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 17.83333!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel20
        '
        Me.XrLabel20.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "OrdenCompra.Traduccion")})
        Me.XrLabel20.Dpi = 100.0!
        Me.XrLabel20.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(531.9166!, 0.0!)
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.SizeF = New System.Drawing.SizeF(300.6183!, 16.0!)
        Me.XrLabel20.StylePriority.UseFont = False
        '
        'xrlCorre
        '
        Me.xrlCorre.Dpi = 100.0!
        Me.xrlCorre.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.xrlCorre.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 0.00000500679!)
        Me.xrlCorre.Name = "xrlCorre"
        Me.xrlCorre.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCorre.SizeF = New System.Drawing.SizeF(24.0!, 16.0!)
        Me.xrlCorre.StylePriority.UseFont = False
        XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
        Me.xrlCorre.Summary = XrSummary1
        Me.xrlCorre.Text = "xrlCorre"
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "OrdenCompra.Marca")})
        Me.XrLabel1.Dpi = 100.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(27.0!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(68.0!, 16.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        '
        'XrLabel3
        '
        Me.XrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "OrdenCompra.Origen")})
        Me.XrLabel3.Dpi = 100.0!
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(833.5!, 0.0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(122.0!, 16.0!)
        Me.XrLabel3.StylePriority.UseFont = False
        '
        'XrLabel9
        '
        Me.XrLabel9.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "OrdenCompra.Cantidad", "{0:n2}")})
        Me.XrLabel9.Dpi = 100.0!
        Me.XrLabel9.Font = New System.Drawing.Font("Arial", 7.0!)
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(964.7916!, 0.0!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(47.70831!, 16.0!)
        Me.XrLabel9.StylePriority.UseFont = False
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlFecha})
        Me.GroupFooter1.Dpi = 100.0!
        Me.GroupFooter1.HeightF = 41.66667!
        Me.GroupFooter1.Name = "GroupFooter1"
        Me.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'xrlFecha
        '
        Me.xrlFecha.Dpi = 100.0!
        Me.xrlFecha.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlFecha.LocationFloat = New DevExpress.Utils.PointFloat(0.999999!, 13.83333!)
        Me.xrlFecha.Name = "xrlFecha"
        Me.xrlFecha.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFecha.SizeF = New System.Drawing.SizeF(1011.5!, 16.0!)
        Me.xrlFecha.StylePriority.UseFont = False
        Me.xrlFecha.StylePriority.UseTextAlignment = False
        Me.xrlFecha.Text = "xrlFecha"
        Me.xrlFecha.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'DsBancos1
        '
        Me.DsBancos1.DataSetName = "dsBancos"
        Me.DsBancos1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DsCompras1
        '
        Me.DsCompras1.DataSetName = "dsCompras"
        Me.DsCompras1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.xrlTelefonos, Me.xrLogo, Me.xrlCartaAduana, Me.xrlDireccionEmpresa, Me.xrlEmpresa, Me.xrpNumero, Me.xrlPage})
        Me.PageHeader.Dpi = 100.0!
        Me.PageHeader.HeightF = 129.875!
        Me.PageHeader.Name = "PageHeader"
        '
        'xrlTelefonos
        '
        Me.xrlTelefonos.Dpi = 100.0!
        Me.xrlTelefonos.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlTelefonos.LocationFloat = New DevExpress.Utils.PointFloat(357.5001!, 39.0!)
        Me.xrlTelefonos.Name = "xrlTelefonos"
        Me.xrlTelefonos.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlTelefonos.SizeF = New System.Drawing.SizeF(538.0!, 14.0!)
        Me.xrlTelefonos.StylePriority.UseFont = False
        Me.xrlTelefonos.StylePriority.UseTextAlignment = False
        Me.xrlTelefonos.Text = "xrlTelefonos"
        Me.xrlTelefonos.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrLogo
        '
        Me.xrLogo.Dpi = 100.0!
        Me.xrLogo.Image = CType(resources.GetObject("xrLogo.Image"), System.Drawing.Image)
        Me.xrLogo.LocationFloat = New DevExpress.Utils.PointFloat(23.0!, 0.0!)
        Me.xrLogo.Name = "xrLogo"
        Me.xrLogo.SizeF = New System.Drawing.SizeF(287.0209!, 94.27087!)
        Me.xrLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'xrlCartaAduana
        '
        Me.xrlCartaAduana.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.xrlCartaAduana.Dpi = 100.0!
        Me.xrlCartaAduana.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.xrlCartaAduana.LocationFloat = New DevExpress.Utils.PointFloat(8.999999!, 95.27087!)
        Me.xrlCartaAduana.Multiline = True
        Me.xrlCartaAduana.Name = "xrlCartaAduana"
        Me.xrlCartaAduana.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlCartaAduana.SizeF = New System.Drawing.SizeF(781.0!, 34.60416!)
        Me.xrlCartaAduana.StylePriority.UseBorders = False
        Me.xrlCartaAduana.StylePriority.UseFont = False
        Me.xrlCartaAduana.StylePriority.UseTextAlignment = False
        Me.xrlCartaAduana.Text = "xrlCartaAduana"
        Me.xrlCartaAduana.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlDireccionEmpresa
        '
        Me.xrlDireccionEmpresa.Dpi = 100.0!
        Me.xrlDireccionEmpresa.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlDireccionEmpresa.LocationFloat = New DevExpress.Utils.PointFloat(357.5!, 22.99999!)
        Me.xrlDireccionEmpresa.Name = "xrlDireccionEmpresa"
        Me.xrlDireccionEmpresa.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlDireccionEmpresa.SizeF = New System.Drawing.SizeF(538.0!, 14.0!)
        Me.xrlDireccionEmpresa.StylePriority.UseFont = False
        Me.xrlDireccionEmpresa.StylePriority.UseTextAlignment = False
        Me.xrlDireccionEmpresa.Text = "xrlDireccionEmpresa"
        Me.xrlDireccionEmpresa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.Dpi = 100.0!
        Me.TopMarginBand1.HeightF = 12.0!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.Dpi = 100.0!
        Me.BottomMarginBand1.HeightF = 35.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2, Me.XrTable1, Me.xrlPaisProveedor, Me.xrlProveedor, Me.xrlFactura})
        Me.GroupHeader1.Dpi = 100.0!
        Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("IdProveedorProducto", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader1.HeightF = 59.85419!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'XrTable2
        '
        Me.XrTable2.BackColor = System.Drawing.Color.Black
        Me.XrTable2.Dpi = 100.0!
        Me.XrTable2.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTable2.ForeColor = System.Drawing.Color.White
        Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(1.500058!, 38.77084!)
        Me.XrTable2.Name = "XrTable2"
        Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow2})
        Me.XrTable2.SizeF = New System.Drawing.SizeF(1014.0!, 19.0!)
        Me.XrTable2.StylePriority.UseBackColor = False
        Me.XrTable2.StylePriority.UseFont = False
        Me.XrTable2.StylePriority.UseForeColor = False
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell5, Me.XrTableCell6, Me.XrTableCell7, Me.XrTableCell8, Me.XrTableCell9})
        Me.XrTableRow2.Dpi = 100.0!
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 1.0R
        '
        'XrTableCell4
        '
        Me.XrTableCell4.Dpi = 100.0!
        Me.XrTableCell4.Name = "XrTableCell4"
        Me.XrTableCell4.Text = "   FABRICA"
        Me.XrTableCell4.Weight = 0.38205751649213066R
        '
        'XrTableCell5
        '
        Me.XrTableCell5.Dpi = 100.0!
        Me.XrTableCell5.Name = "XrTableCell5"
        Me.XrTableCell5.Text = "ITEM #"
        Me.XrTableCell5.Weight = 0.32402783502217708R
        '
        'XrTableCell6
        '
        Me.XrTableCell6.Dpi = 100.0!
        Me.XrTableCell6.Name = "XrTableCell6"
        Me.XrTableCell6.Text = "DESCRIPCION"
        Me.XrTableCell6.Weight = 0.89313021656236713R
        '
        'XrTableCell7
        '
        Me.XrTableCell7.Dpi = 100.0!
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.Text = "TRADUCCI�N"
        Me.XrTableCell7.Weight = 0.8752644566879475R
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Dpi = 100.0!
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.Text = "ORIGEN"
        Me.XrTableCell8.Weight = 0.35166861251211751R
        '
        'XrTableCell9
        '
        Me.XrTableCell9.Dpi = 100.0!
        Me.XrTableCell9.Name = "XrTableCell9"
        Me.XrTableCell9.Text = "CUANTIA"
        Me.XrTableCell9.Weight = 0.19312602595770456R
        '
        'XrTable1
        '
        Me.XrTable1.BackColor = System.Drawing.Color.Black
        Me.XrTable1.Dpi = 100.0!
        Me.XrTable1.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTable1.ForeColor = System.Drawing.Color.White
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(1015.5!, 19.0!)
        Me.XrTable1.StylePriority.UseBackColor = False
        Me.XrTable1.StylePriority.UseFont = False
        Me.XrTable1.StylePriority.UseForeColor = False
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell3})
        Me.XrTableRow1.Dpi = 100.0!
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 1.0R
        '
        'XrTableCell1
        '
        Me.XrTableCell1.Dpi = 100.0!
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.Text = "FACTURA"
        Me.XrTableCell1.Weight = 1.0R
        '
        'XrTableCell2
        '
        Me.XrTableCell2.Dpi = 100.0!
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.Text = "PROVEEDOR"
        Me.XrTableCell2.Weight = 1.0R
        '
        'XrTableCell3
        '
        Me.XrTableCell3.Dpi = 100.0!
        Me.XrTableCell3.Name = "XrTableCell3"
        Me.XrTableCell3.Text = "PAIS DEL EXPORTADOR"
        Me.XrTableCell3.Weight = 1.0240714117761744R
        '
        'xrlPaisProveedor
        '
        Me.xrlPaisProveedor.BackColor = System.Drawing.Color.Transparent
        Me.xrlPaisProveedor.BorderColor = System.Drawing.Color.Black
        Me.xrlPaisProveedor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "OrdenCompra.PaisProveedor")})
        Me.xrlPaisProveedor.Dpi = 100.0!
        Me.xrlPaisProveedor.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlPaisProveedor.ForeColor = System.Drawing.Color.Black
        Me.xrlPaisProveedor.LocationFloat = New DevExpress.Utils.PointFloat(678.0!, 19.04168!)
        Me.xrlPaisProveedor.Name = "xrlPaisProveedor"
        Me.xrlPaisProveedor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlPaisProveedor.SizeF = New System.Drawing.SizeF(262.0!, 16.0!)
        Me.xrlPaisProveedor.StylePriority.UseBackColor = False
        Me.xrlPaisProveedor.StylePriority.UseBorderColor = False
        Me.xrlPaisProveedor.StylePriority.UseFont = False
        Me.xrlPaisProveedor.StylePriority.UseForeColor = False
        Me.xrlPaisProveedor.StylePriority.UseTextAlignment = False
        Me.xrlPaisProveedor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlProveedor
        '
        Me.xrlProveedor.BackColor = System.Drawing.Color.Transparent
        Me.xrlProveedor.BorderColor = System.Drawing.Color.Black
        Me.xrlProveedor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "OrdenCompra.ProveedorProducto")})
        Me.xrlProveedor.Dpi = 100.0!
        Me.xrlProveedor.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlProveedor.ForeColor = System.Drawing.Color.Black
        Me.xrlProveedor.LocationFloat = New DevExpress.Utils.PointFloat(212.4583!, 19.0417!)
        Me.xrlProveedor.Name = "xrlProveedor"
        Me.xrlProveedor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlProveedor.SizeF = New System.Drawing.SizeF(435.0417!, 16.0!)
        Me.xrlProveedor.StylePriority.UseBackColor = False
        Me.xrlProveedor.StylePriority.UseBorderColor = False
        Me.xrlProveedor.StylePriority.UseFont = False
        Me.xrlProveedor.StylePriority.UseForeColor = False
        Me.xrlProveedor.StylePriority.UseTextAlignment = False
        Me.xrlProveedor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'xrlFactura
        '
        Me.xrlFactura.BackColor = System.Drawing.Color.Transparent
        Me.xrlFactura.BorderColor = System.Drawing.Color.Black
        Me.xrlFactura.Dpi = 100.0!
        Me.xrlFactura.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.xrlFactura.ForeColor = System.Drawing.Color.Black
        Me.xrlFactura.LocationFloat = New DevExpress.Utils.PointFloat(0.999999!, 19.04167!)
        Me.xrlFactura.Name = "xrlFactura"
        Me.xrlFactura.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.xrlFactura.SizeF = New System.Drawing.SizeF(128.0!, 16.0!)
        Me.xrlFactura.StylePriority.UseBackColor = False
        Me.xrlFactura.StylePriority.UseBorderColor = False
        Me.xrlFactura.StylePriority.UseFont = False
        Me.xrlFactura.StylePriority.UseForeColor = False
        Me.xrlFactura.StylePriority.UseTextAlignment = False
        Me.xrlFactura.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'com_rptCartaAduana
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.GroupFooter1, Me.PageHeader, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1})
        Me.DataMember = "OrdenCompra"
        Me.DataSource = Me.DsCompras1
        Me.DrawGrid = False
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(43, 25, 12, 35)
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.SnapGridSize = 3.0!
        Me.SnapToGrid = False
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
        Me.Version = "16.1"
        CType(Me.DsBancos1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsCompras1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents xrlPage As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrpNumero As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents xrlEmpresa As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents DsBancos1 As Nexus.dsBancos
    Friend WithEvents DsCompras1 As Nexus.dsCompras
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents xrlDireccionEmpresa As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCartaAduana As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlCorre As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrLogo As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrTable2 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell5 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell9 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents xrlPaisProveedor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlProveedor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFactura As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlFecha As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents xrlTelefonos As DevExpress.XtraReports.UI.XRLabel
End Class
