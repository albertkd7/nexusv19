﻿
Imports NexusBLL
Public Class com_frmComprasPeriodoProveedor
    Dim bl As New ComprasBLL(g_ConnectionString)

    Private Sub repComprasPeriodo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFechaI.DateTime = Today
        deFechaF.DateTime = Today
        objCombos.inv_Bodegas(leBodega, "-- TODAS --")
        objCombos.invMarcas(leMarca, "-- TODAS LAS MARCAS --")
        objCombos.con_CentrosCosto(leCentroCosto, "", "-- TODOS LOS CENTROS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        leCentroCosto.EditValue = "-1"
    End Sub

    Private Sub SimpleButton1_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.com_ComprasPeriodoProveedor(deFechaI.DateTime, deFechaF.DateTime, leBodega.EditValue, BeProveedor1.beCodigo.EditValue, leSucursal.EditValue, leMarca.EditValue, leCentroCosto.EditValue)
        Dim rpt As New com_rptComprasPeriodoProveedor() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = txtTitulo.EditValue & " - SUCURSAL: " & leSucursal.Text
        rpt.xrlPeriodo.Text = (FechaToString(deFechaI.DateTime, deFechaF.DateTime)).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.xrlMarca.Text = "MARCA: " + leMarca.Text
        rpt.ShowPreviewDialog()
    End Sub
End Class
