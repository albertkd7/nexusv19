﻿Imports NexusBLL

Public Class com_frmComprasPeriodoProducto
    Dim bl As New ComprasBLL(g_ConnectionString)
    

    Private Sub com_frmComprasPeriodoProducto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        deFechaI.DateTime = Today
        deFechaF.DateTime = Today
        beProducto.beCodigo.EditValue = ""
        objCombos.inv_Bodegas(leBodega, "-- TODAS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        objCombos.con_CentrosCosto(leCentroCosto, "", "-- TODOS LOS CENTROS DE COSTOS --")
        leCentroCosto.EditValue = "-1"
    End Sub

    Private Sub com_frmComprasPeriodoProducto_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.com_ComprasPeriodoProducto(deFechaI.DateTime, deFechaF.DateTime, leBodega.EditValue, beProducto.beCodigo.EditValue, leSucursal.EditValue, leCentroCosto.EditValue)
        Dim rpt As New com_rptComprasPeriodoProducto() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = txtTitulo.EditValue & " - SUCURSAL: " & leSucursal.Text
        rpt.xrlPeriodo.Text = (FechaToString(deFechaI.DateTime, deFechaF.DateTime)).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.ShowPreviewDialog()
    End Sub
End Class
