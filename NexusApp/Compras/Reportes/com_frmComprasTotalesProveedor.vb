﻿
Imports NexusBLL
Public Class com_frmComprasTotalesProveedor
    Dim bl As New ComprasBLL(g_ConnectionString)


    Private Sub com_frmComprasTotalesProveedor_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFechaI.DateTime = Today
        deFechaF.DateTime = Today
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        objCombos.invMarcas(leMarca, "-- TODAS LAS MARCAS --")
    End Sub

    Private Sub SimpleButton1_Click() Handles Me.Reporte
        Dim dt As DataTable = bl.GetComprasTotalesProveedor(deFechaI.DateTime, deFechaF.DateTime, leBodega.EditValue, leSucursal.EditValue, leMarca.EditValue)
        Dim rpt As New com_rptComprasTotalesProv() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue & " - SUCURSAL: " & leSucursal.Text
        rpt.xrlPeriodo.Text = (FechaToString(deFechaI.DateTime, deFechaF.DateTime)).ToUpper
        rpt.xrlMarca.Text = "MARCA: " + leMarca.Text
        rpt.ShowPreviewDialog()
    End Sub
End Class
