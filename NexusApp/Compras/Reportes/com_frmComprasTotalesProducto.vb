﻿
Imports NexusBLL
Public Class com_frmComprasTotalesProducto
    Dim bl As New ComprasBLL(g_ConnectionString)

    Private Sub repComprasTotalesProducto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        deFechaI.EditValue = Today
        deFechaF.EditValue = Today
        objCombos.inv_Bodegas(leBodega, "-- TODAS LAS BODEGAS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub com_frmComprasTotalesProducto_Report_Click() Handles Me.Reporte
        Dim dt As DataTable = bl.GetComprasTotalesProducto(deFechaI.EditValue, deFechaF.EditValue, leBodega.EditValue, leSucursal.EditValue)
        Dim rpt As New com_rptComprasTotalesProd() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = (FechaToString(deFechaI.EditValue, deFechaF.EditValue)).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.ShowPreviewDialog()
    End Sub
End Class
