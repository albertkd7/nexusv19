﻿
Imports NexusBLL
Public Class com_frmComprasExcluidos
    Dim bl As New ComprasBLL(g_ConnectionString)


    Private Sub repComprasPeriodo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFechaI.DateTime = Today
        deFechaF.DateTime = Today
        objCombos.inv_Bodegas(leBodega, "-- TODAS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub SimpleButton1_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.com_ComprasExcluidos(deFechaI.DateTime, deFechaF.DateTime, leBodega.EditValue, BeProveedor1.beCodigo.EditValue, leSucursal.EditValue)
        Dim rpt As New com_rptComprasExcluidos() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = txtTitulo.EditValue + " -  SUCURSAL: " & leSucursal.Text
        rpt.xrlPeriodo.Text = (FechaToString(deFechaI.DateTime, deFechaF.DateTime)).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.ShowPreviewDialog()
    End Sub
End Class
