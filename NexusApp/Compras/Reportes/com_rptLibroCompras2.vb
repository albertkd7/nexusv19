Public Class com_rptLibroCompras2
    Dim Correl = 1
    Private Sub Detail_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Detail.BeforePrint
        xlrNumero.Text = String.Format("{0}", GetCurrentColumnValue("Numero"))
    End Sub

    Private Sub xrFolio_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrFolio.BeforePrint
        If Not Me.Parameters.Item("ImprimirFolio").Value Then
            e.Cancel = True
        End If
    End Sub

    Private Sub xrlCorrel_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles xrlCorrel.BeforePrint
        xrlCorrel.Text = Format(Correl, "##,###")
        Correl += 1
    End Sub
End Class