﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class com_frmGenerador
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.sbToText = New DevExpress.XtraEditors.SimpleButton
        Me.sbToPdf = New DevExpress.XtraEditors.SimpleButton
        Me.sbToExcel = New DevExpress.XtraEditors.SimpleButton
        Me.sbMostrarOcultar = New DevExpress.XtraEditors.SimpleButton
        Me.sbGenerar = New DevExpress.XtraEditors.SimpleButton
        Me.deFecFin = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.deFecIni = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.deFecFin.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecFin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecIni.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecIni.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.leSucursal)
        Me.GroupControl1.Controls.Add(Me.LabelControl32)
        Me.GroupControl1.Controls.Add(Me.sbToText)
        Me.GroupControl1.Controls.Add(Me.sbToPdf)
        Me.GroupControl1.Controls.Add(Me.sbToExcel)
        Me.GroupControl1.Controls.Add(Me.sbMostrarOcultar)
        Me.GroupControl1.Controls.Add(Me.sbGenerar)
        Me.GroupControl1.Controls.Add(Me.deFecFin)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.deFecIni)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Size = New System.Drawing.Size(778, 122)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Parámetros de la consulta"
        '
        'sbToText
        '
        Me.sbToText.Location = New System.Drawing.Point(616, 26)
        Me.sbToText.Name = "sbToText"
        Me.sbToText.Size = New System.Drawing.Size(107, 23)
        Me.sbToText.TabIndex = 4
        Me.sbToText.Text = "Exportar a Texto"
        '
        'sbToPdf
        '
        Me.sbToPdf.Location = New System.Drawing.Point(504, 26)
        Me.sbToPdf.Name = "sbToPdf"
        Me.sbToPdf.Size = New System.Drawing.Size(107, 23)
        Me.sbToPdf.TabIndex = 3
        Me.sbToPdf.Text = "Exportar a PDF"
        '
        'sbToExcel
        '
        Me.sbToExcel.Location = New System.Drawing.Point(390, 26)
        Me.sbToExcel.Name = "sbToExcel"
        Me.sbToExcel.Size = New System.Drawing.Size(107, 23)
        Me.sbToExcel.TabIndex = 2
        Me.sbToExcel.Text = "Exportar a Excel"
        '
        'sbMostrarOcultar
        '
        Me.sbMostrarOcultar.Location = New System.Drawing.Point(195, 83)
        Me.sbMostrarOcultar.Name = "sbMostrarOcultar"
        Me.sbMostrarOcultar.Size = New System.Drawing.Size(170, 23)
        Me.sbMostrarOcultar.TabIndex = 6
        Me.sbMostrarOcultar.Text = "Ocultar Selector de Columnas"
        '
        'sbGenerar
        '
        Me.sbGenerar.Location = New System.Drawing.Point(79, 82)
        Me.sbGenerar.Name = "sbGenerar"
        Me.sbGenerar.Size = New System.Drawing.Size(100, 23)
        Me.sbGenerar.TabIndex = 5
        Me.sbGenerar.Text = "Generar..."
        '
        'deFecFin
        '
        Me.deFecFin.EditValue = Nothing
        Me.deFecFin.Location = New System.Drawing.Point(265, 29)
        Me.deFecFin.Name = "deFecFin"
        Me.deFecFin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecFin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecFin.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFecFin.Size = New System.Drawing.Size(100, 20)
        Me.deFecFin.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(195, 32)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl2.TabIndex = 9
        Me.LabelControl2.Text = "Hasta Fecha:"
        '
        'deFecIni
        '
        Me.deFecIni.EditValue = Nothing
        Me.deFecIni.Location = New System.Drawing.Point(79, 29)
        Me.deFecIni.Name = "deFecIni"
        Me.deFecIni.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecIni.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecIni.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFecIni.Size = New System.Drawing.Size(100, 20)
        Me.deFecIni.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(10, 32)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(63, 13)
        Me.LabelControl1.TabIndex = 7
        Me.LabelControl1.Text = "Fecha Inicial:"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 122)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(778, 189)
        Me.gc.TabIndex = 4
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsFilter.ShowAllTableValuesInFilterPopup = True
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(79, 52)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(286, 20)
        Me.leSucursal.TabIndex = 2
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(31, 56)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl32.TabIndex = 63
        Me.LabelControl32.Text = "Sucursal:"
        '
        'com_frmGenerador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(778, 336)
        Me.Controls.Add(Me.gc)
        Me.Name = "com_frmGenerador"
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.deFecFin.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecFin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecIni.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecIni.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents sbToText As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbToPdf As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbToExcel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbMostrarOcultar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbGenerar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents deFecFin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecIni As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl

End Class
