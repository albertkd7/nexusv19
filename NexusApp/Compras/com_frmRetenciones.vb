﻿
Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class com_frmRetenciones
    Dim bl As New ComprasBLL(g_ConnectionString)
    'Dim blInve As New InventarioBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim Header As com_Retenciones
    Dim Detalle As List(Of com_RetencionesDetalle)

    Dim dtParametros As DataTable

    Private Sub fac_frmRetenciones_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = bl.com_ConsultaRetenciones(objMenu.User).Tables(0)
        gv2.BestFitColumns()
    End Sub

    Private Sub fac_frmRetenciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargaCombos()
        dtParametros = blAdmon.ObtieneParametros()
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("com_Retenciones", "IdComprobante")
        CargaControles(0)
        ActivarControles(False)
        gc2.DataSource = bl.com_ConsultaRetenciones(objMenu.User).Tables(0)
    End Sub
    Private Sub fac_frmRetenciones_Nuevo_Click() Handles Me.Nuevo
        ActivarControles(True)

        Header = New com_Retenciones
        gc.DataSource = bl.GetRetencionesDetalle(-1)
        gv.CancelUpdateCurrentRow()
        gv.AddNewRow()

        teNumero.EditValue = ""
        deFecha.EditValue = Today
        deFechaContable.EditValue = Today
        beIdCliente.EditValue = ""
        txtNombre.EditValue = ""
        txtNRC.EditValue = ""
        txtDireccion.EditValue = ""
        leSucursal.EditValue = piIdSucursalUsuario
        txtNIT.EditValue = ""
        sePorcIVA.EditValue = 13.0
        sePorcReten.EditValue = 10.0
        teNumero.Focus()
        xtcRetencion.SelectedTabPage = xtpDatos
    End Sub
    Private Sub fac_frmRetenciones_Save_Click() Handles Me.Guardar
        CargarEntidades()
        Dim msj As String = ""
        If DbMode = DbModeType.insert Then
            msj = bl.InsertRetencion(Header, Detalle)

            If msj = "" Then
                MsgBox("El Comprobante de Retención ha sido registrado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("NO SE PUDO CREAR EL COMPROBANTE DE RETENCIÓN" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        Else
            msj = bl.UpdateRetencion(Header, Detalle)
            If msj = "" Then
                MsgBox("El Comprobante de Retención ha sido actualizado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox(String.Format("NO FUE POSIBLE ACTUALIZAR EL COMPROBANTE DE RETENCION{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If

        xtcRetencion.SelectedTabPage = xtpLista
        gc2.DataSource = bl.com_ConsultaRetenciones(objMenu.User).Tables(0)
        gc2.Focus()

        teCorrelativo.EditValue = Header.IdComprobante
        MostrarModoInicial()
        teCorrelativo.Focus()
        ActivarControles(False)
    End Sub
    Private Sub fac_frmRetenciones_Edit_Click() Handles Me.Editar
        ActivarControles(True)
        teNumero.Focus()
    End Sub
    Private Sub fac_frmRetenciones_Consulta_Click() Handles Me.Consulta
        'teCorrelativo.EditValue = objConsultas.ConsultaRetencion(frmConsultaDetalle)
        'CargaControles(0)
    End Sub
    Private Sub fac_frmRetenciones_Cancelar_Click() Handles Me.Revertir
        xtcRetencion.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub
    Private Sub fac_frmRetenciones_Delete_Click() Handles Me.Eliminar
        Header = objTablas.com_RetencionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))

        If MsgBox("Está seguro(a) de eliminar el Comprobante de Retención ?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            objTablas.com_RetencionesDeleteByPK(Header.IdComprobante)
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("com_Retenciones", "IdComprobante")
            CargaControles(0)
            ActivarControles(False)
            gc2.DataSource = bl.com_ConsultaRetenciones(objMenu.User).Tables(0)
        End If
    End Sub

    Private Sub CargarEntidades()
        With Header
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
            Else
                .ModificadoPor = objMenu.User
                .FechaHoraModificacion = Nothing
            End If
            .IdComprobante = teCorrelativo.EditValue  'el id se asignará en la capa de datos
            .Numero = teNumero.EditValue
            .Serie = teSerie.EditValue
            .Fecha = deFecha.EditValue
            .FechaContable = deFechaContable.EditValue
            .IdClienteProveedor = beIdCliente.EditValue
            .Nombre = txtNombre.EditValue
            .Nrc = txtNRC.EditValue
            .Direccion = txtDireccion.EditValue
            .IdSucursal = leSucursal.EditValue
            .Nit = txtNIT.EditValue
            .PorcentajeIva = sePorcIVA.EditValue
            .PorcentajeRet = sePorcReten.EditValue
            .TotalIva = teIva.EditValue
            .TotalImpuesto1 = teRetencion.EditValue
            .TotalComprobante = teTotal.EditValue
        End With

        Detalle = New List(Of com_RetencionesDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New com_RetencionesDetalle
            With entDetalle
                .IdDetalle = i + 1
                .IdComprobante = Header.IdComprobante
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .PrecioTotal = gv.GetRowCellValue(i, "PrecioTotal")
            End With
            Detalle.Add(entDetalle)
        Next
    End Sub

    Public Sub CargaControles(ByVal TipoAvance As Integer)

        'If TipoAvance = 0 Then 'no es necesario obtener el Id de la orden
        'Else
        '    teCorrelativo.EditValue = bl.com_ObtenerIdRetencion(teCorrelativo.EditValue, TipoAvance)
        'End If

        'Header = objTablas.com_RetencionesSelectByPK(teCorrelativo.EditValue)
        'If Header Is Nothing Then
        '    Exit Sub
        'End If
        'With Header
        '    teCorrelativo.EditValue = .IdComprobante
        '    gc.DataSource = bl.GetRetencionesDetalle(.IdComprobante)
        '    teNumero.EditValue = .Numero
        '    teSerie.EditValue = .Serie
        '    deFecha.EditValue = .Fecha
        '    deFechaContable.EditValue = .FechaContable
        '    beIdCliente.EditValue = .IdClienteProveedor
        '    txtNombre.EditValue = .Nombre
        '    txtNRC.EditValue = .Nrc
        '    txtDireccion.EditValue = .Direccion
        '    leSucursal.EditValue = .IdSucursal
        '    txtNIT.EditValue = .Nit
        '    sePorcIVA.EditValue = .PorcentajeIva
        '    sePorcReten.EditValue = .PorcentajeRet
        '    teIva.EditValue = .TotalIva
        '    teRetencion.EditValue = .TotalImpuesto1
        '    teTotal.EditValue = .TotalComprobante
        'End With

    End Sub

    Private Sub CargaCombos()
        With objCombos
            .adm_Sucursales(leSucursal, objMenu.User, "")
            .adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        End With
    End Sub

#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
        CalcularTotales()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", 0)
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs)

        'Dim IdProduc As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdProducto")), "")
        'Dim IdPrecio As Integer = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdPrecio")), 0)
        'If IdProduc = "" OrElse Not blInve.inv_VerificaCodigoProducto(IdProduc) Then
        '    e.Valid = False
        '    gv.SetColumnError(gv.Columns("IdProducto"), "Código de producto no válido o no existe")
        'End If

    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Dim cantidad As Integer = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
        Dim PrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")

        Select Case gv.FocusedColumn.FieldName
            Case "Cantidad"
                cantidad = e.Value
            Case "PrecioUnitario"
                PrecioUnitario = e.Value
        End Select

        Dim total As Decimal = Decimal.Round(cantidad * PrecioUnitario, 2)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", total)
        CalcularTotales()
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto

        End If

    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        CalcularTotales()
    End Sub
#End Region

    Private Sub beIdCliente_Click(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beIdCliente.ButtonClick
        beIdCliente.EditValue = ""
        beIdCliente_Validated(sender, New System.EventArgs)
    End Sub
    Private Sub beIdCliente_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beIdCliente.Validated
        Dim entProveedor As New com_Proveedores
        entProveedor = objConsultas.cnsProveedores(frmConsultas, beIdCliente.EditValue)
        beIdCliente.EditValue = entProveedor.IdProveedor
        txtNombre.EditValue = entProveedor.Nombre
        txtDireccion.EditValue = entProveedor.Direccion
        txtNRC.EditValue = entProveedor.Nrc
        txtNIT.EditValue = entProveedor.Nit
    End Sub
    Private Sub leSucursal_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles leSucursal.LostFocus
        If gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
        End If
        gv.FocusedColumn = gv.Columns(0)
    End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In gcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.SpinEdit Then
                CType(ctrl, DevExpress.XtraEditors.SpinEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
    End Sub

    Private Sub fac_frmRetenciones_Reporte() Handles Me.Reporte
        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)
        Dim TemplateGral As String = Environment.CurrentDirectory & "\Plantillas\retNexus.repx"
        Dim Template As String = Environment.CurrentDirectory & "\Plantillas\retNexus" & NumFormato & ".repx"
        Dim rpt As New com_rptRetencion

        If Not FileIO.FileSystem.FileExists(Template) Then
            If FileIO.FileSystem.FileExists(TemplateGral) Then
                rpt.LoadLayout(TemplateGral)
            Else
                MsgBox("No existe la plantilla necesaria para el documento, procedera a imprimir formato estandar", MsgBoxStyle.Critical, "Nota")
            End If
        Else
            rpt.LoadLayout(Template)
        End If
        Header = objTablas.com_RetencionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        gc.DataSource = bl.GetRetencionesDetalle(Header.IdComprobante)

        Dim dt As DataTable = bl.com_ObtenerRetencion(Header.IdComprobante)
        rpt.DataSource = dt
        rpt.DataMember = ""
        rpt.xrlNombre.Text = Header.Nombre
        rpt.xrlFecha.Text = Format(Header.Fecha, "dd/MM/yyyy")
        rpt.xrlNrc.Text = Header.Nrc
        rpt.xrlNit.Text = Header.Nit
        rpt.xrlDireccion.Text = Header.Direccion
        rpt.xrlNumero.Text = Header.Numero
        rpt.xrlVentaAfecta.Text = Format(colPrecioTotal.SummaryItem.SummaryValue, "###,##0.00")
        'rpt.xrlIva.Text = Format(teIVA.EditValue, "###,##0.00")
        rpt.xrlIva.Text = Format(Header.TotalIva, "###,##0.00")
        rpt.xrIvaRetenido.Text = Header.TotalImpuesto1
        rpt.xrlTotal.Text = Format(Header.TotalComprobante, "###,##0.00")
        Dim Decimales = String.Format("{0:c}", Header.TotalComprobante)
        Dim CantidaLetras As String
        Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
        CantidaLetras = Num2Text(Int(Header.TotalImpuesto1)) & Decimales & " DÓLARES" 'teTotal.EditValue
        rpt.xrlCantLetras.Text = CantidaLetras
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub CalcularTotales()
        gv.UpdateTotalSummary()
        teIva.EditValue = Decimal.Round(Me.colPrecioTotal.SummaryItem.SummaryValue * ((sePorcIVA.EditValue / 100)), 2)
        teRetencion.EditValue = 0.0

        teRetencion.EditValue = Decimal.Round((Me.colPrecioTotal.SummaryItem.SummaryValue) * sePorcReten.EditValue / 100, 2)

        teTotal.EditValue = Me.colPrecioTotal.SummaryItem.SummaryValue - (teRetencion.EditValue)

    End Sub

    Private Sub sePorcIVA_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles sePorcIVA.EditValueChanged
        If txtNombre.Properties.ReadOnly = False Then
            CalcularTotales()
        End If
    End Sub

    Private Sub sePorcReten_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles sePorcReten.EditValueChanged
        If txtNombre.Properties.ReadOnly = False Then
            CalcularTotales()
        End If
    End Sub

    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        Header = objTablas.com_RetencionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        DbMode = DbModeType.update
        teNumero.Focus()
    End Sub
    Private Sub CargaPantalla()
        xtcRetencion.SelectedTabPage = xtpDatos
        teNumero.Focus()

        With Header
            teCorrelativo.EditValue = .IdComprobante
            gc.DataSource = bl.GetRetencionesDetalle(.IdComprobante)
            teNumero.EditValue = .Numero
            teSerie.EditValue = .Serie
            deFecha.EditValue = .Fecha
            deFechaContable.EditValue = .FechaContable
            beIdCliente.EditValue = .IdClienteProveedor
            txtNombre.EditValue = .Nombre
            txtNRC.EditValue = .Nrc
            txtDireccion.EditValue = .Direccion
            leSucursal.EditValue = .IdSucursal
            txtNIT.EditValue = .Nit
            sePorcIVA.EditValue = .PorcentajeIva
            sePorcReten.EditValue = .PorcentajeRet
            teIva.EditValue = .TotalIva
            teRetencion.EditValue = .TotalImpuesto1
            teTotal.EditValue = .TotalComprobante
        End With

    End Sub
End Class
