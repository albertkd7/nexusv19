﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class com_frmRetenciones
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xtcRetencion = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage()
        Me.gc2 = New DevExpress.XtraGrid.GridControl()
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.pcTotales = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teTotal = New DevExpress.XtraEditors.TextEdit()
        Me.teRetencion = New DevExpress.XtraEditors.TextEdit()
        Me.teIva = New DevExpress.XtraEditors.TextEdit()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcCantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.teCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.tePrecioUnitario = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colPrecioTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.tePjeDescuento = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.gcHeader = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaContable = New DevExpress.XtraEditors.DateEdit()
        Me.sePorcReten = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.sePorcIVA = New DevExpress.XtraEditors.SpinEdit()
        Me.teSerie = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.txtDireccion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNRC = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNIT = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.beIdCliente = New DevExpress.XtraEditors.ButtonEdit()
        CType(Me.xtcRetencion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcRetencion.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcTotales.SuspendLayout()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teRetencion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePrecioUnitario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePjeDescuento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcHeader.SuspendLayout()
        CType(Me.deFechaContable.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaContable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sePorcReten.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sePorcIVA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNIT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beIdCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcRetencion
        '
        Me.xtcRetencion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcRetencion.Location = New System.Drawing.Point(0, 0)
        Me.xtcRetencion.Name = "xtcRetencion"
        Me.xtcRetencion.SelectedTabPage = Me.xtpLista
        Me.xtcRetencion.Size = New System.Drawing.Size(902, 436)
        Me.xtcRetencion.TabIndex = 59
        Me.xtcRetencion.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc2)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(896, 408)
        Me.xtpLista.Text = "Consulta Retenciones"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit2, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(896, 408)
        Me.gc2.TabIndex = 4
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12, Me.GridColumn13, Me.GridColumn16, Me.GridColumn14, Me.GridColumn15})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "IdComprobante"
        Me.GridColumn7.FieldName = "IdComprobante"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 0
        Me.GridColumn7.Width = 59
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Numero"
        Me.GridColumn10.FieldName = "Numero"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 1
        Me.GridColumn10.Width = 85
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Fecha"
        Me.GridColumn11.FieldName = "Fecha"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 2
        Me.GridColumn11.Width = 81
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Sucursal"
        Me.GridColumn12.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn12.FieldName = "IdSucursal"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 3
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Proveedor"
        Me.GridColumn13.FieldName = "Nombre"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 4
        Me.GridColumn13.Width = 440
        '
        'GridColumn16
        '
        Me.GridColumn16.Caption = "Total Comprobante"
        Me.GridColumn16.FieldName = "TotalComprobante"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = True
        Me.GridColumn16.VisibleIndex = 5
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "Creado Por"
        Me.GridColumn14.FieldName = "CreadoPor"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 6
        Me.GridColumn14.Width = 98
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "Fecha Hora Creacion"
        Me.GridColumn15.ColumnEdit = Me.RepositoryItemDateEdit2
        Me.GridColumn15.FieldName = "FechaHoraCreacion"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 7
        Me.GridColumn15.Width = 114
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemDateEdit2.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.pcTotales)
        Me.xtpDatos.Controls.Add(Me.gc)
        Me.xtpDatos.Controls.Add(Me.PanelControl1)
        Me.xtpDatos.Controls.Add(Me.gcHeader)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(896, 408)
        Me.xtpDatos.Text = "Retención"
        '
        'pcTotales
        '
        Me.pcTotales.Controls.Add(Me.LabelControl20)
        Me.pcTotales.Controls.Add(Me.LabelControl4)
        Me.pcTotales.Controls.Add(Me.LabelControl5)
        Me.pcTotales.Controls.Add(Me.teTotal)
        Me.pcTotales.Controls.Add(Me.teRetencion)
        Me.pcTotales.Controls.Add(Me.teIva)
        Me.pcTotales.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcTotales.Location = New System.Drawing.Point(0, 336)
        Me.pcTotales.Name = "pcTotales"
        Me.pcTotales.Size = New System.Drawing.Size(863, 73)
        Me.pcTotales.TabIndex = 65
        '
        'LabelControl20
        '
        Me.LabelControl20.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl20.Location = New System.Drawing.Point(695, 51)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl20.TabIndex = 3
        Me.LabelControl20.Text = "TOTAL:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl4.Location = New System.Drawing.Point(679, 31)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl4.TabIndex = 3
        Me.LabelControl4.Text = "Retención:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl5.Location = New System.Drawing.Point(710, 9)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl5.TabIndex = 3
        Me.LabelControl5.Text = "IVA:"
        '
        'teTotal
        '
        Me.teTotal.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teTotal.EditValue = 0
        Me.teTotal.Location = New System.Drawing.Point(734, 48)
        Me.teTotal.Name = "teTotal"
        Me.teTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotal.Properties.Mask.EditMask = "n2"
        Me.teTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTotal.Properties.ReadOnly = True
        Me.teTotal.Size = New System.Drawing.Size(129, 20)
        Me.teTotal.TabIndex = 2
        '
        'teRetencion
        '
        Me.teRetencion.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teRetencion.EditValue = 0
        Me.teRetencion.Location = New System.Drawing.Point(734, 27)
        Me.teRetencion.Name = "teRetencion"
        Me.teRetencion.Properties.Appearance.Options.UseTextOptions = True
        Me.teRetencion.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teRetencion.Properties.Mask.EditMask = "n2"
        Me.teRetencion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teRetencion.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teRetencion.Size = New System.Drawing.Size(129, 20)
        Me.teRetencion.TabIndex = 2
        '
        'teIva
        '
        Me.teIva.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teIva.EditValue = 0
        Me.teIva.Location = New System.Drawing.Point(734, 6)
        Me.teIva.Name = "teIva"
        Me.teIva.Properties.Appearance.Options.UseTextOptions = True
        Me.teIva.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teIva.Properties.Mask.EditMask = "n2"
        Me.teIva.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teIva.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teIva.Properties.ReadOnly = True
        Me.teIva.Size = New System.Drawing.Size(129, 20)
        Me.teIva.TabIndex = 2
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Top
        Me.gc.Location = New System.Drawing.Point(0, 133)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.teCantidad, Me.tePjeDescuento, Me.tePrecioUnitario})
        Me.gc.Size = New System.Drawing.Size(863, 203)
        Me.gc.TabIndex = 64
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.gv.Appearance.FocusedRow.Options.UseBackColor = True
        Me.gv.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.gv.Appearance.SelectedRow.Options.UseBackColor = True
        Me.gv.Appearance.TopNewRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.gv.Appearance.TopNewRow.Options.UseBackColor = True
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcCantidad, Me.gcDescripcion, Me.colPrecioUnitario, Me.colPrecioTotal})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsSelection.MultiSelect = True
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcCantidad
        '
        Me.gcCantidad.Caption = "Cantidad"
        Me.gcCantidad.ColumnEdit = Me.teCantidad
        Me.gcCantidad.FieldName = "Cantidad"
        Me.gcCantidad.Name = "gcCantidad"
        Me.gcCantidad.Visible = True
        Me.gcCantidad.VisibleIndex = 0
        Me.gcCantidad.Width = 95
        '
        'teCantidad
        '
        Me.teCantidad.AutoHeight = False
        Me.teCantidad.Mask.EditMask = "n5"
        Me.teCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.teCantidad.Name = "teCantidad"
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Descripción / Concepto de la venta"
        Me.gcDescripcion.FieldName = "Descripcion"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 1
        Me.gcDescripcion.Width = 478
        '
        'colPrecioUnitario
        '
        Me.colPrecioUnitario.Caption = "Precio Unitario"
        Me.colPrecioUnitario.ColumnEdit = Me.tePrecioUnitario
        Me.colPrecioUnitario.FieldName = "PrecioUnitario"
        Me.colPrecioUnitario.Name = "colPrecioUnitario"
        Me.colPrecioUnitario.Visible = True
        Me.colPrecioUnitario.VisibleIndex = 2
        Me.colPrecioUnitario.Width = 117
        '
        'tePrecioUnitario
        '
        Me.tePrecioUnitario.AutoHeight = False
        Me.tePrecioUnitario.Mask.EditMask = "n6"
        Me.tePrecioUnitario.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tePrecioUnitario.Mask.UseMaskAsDisplayFormat = True
        Me.tePrecioUnitario.Name = "tePrecioUnitario"
        '
        'colPrecioTotal
        '
        Me.colPrecioTotal.Caption = "Precio Total"
        Me.colPrecioTotal.DisplayFormat.FormatString = "n2"
        Me.colPrecioTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colPrecioTotal.FieldName = "PrecioTotal"
        Me.colPrecioTotal.Name = "colPrecioTotal"
        Me.colPrecioTotal.OptionsColumn.AllowEdit = False
        Me.colPrecioTotal.OptionsColumn.AllowFocus = False
        Me.colPrecioTotal.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PrecioTotal", "{0:n2}")})
        Me.colPrecioTotal.Visible = True
        Me.colPrecioTotal.VisibleIndex = 3
        Me.colPrecioTotal.Width = 108
        '
        'tePjeDescuento
        '
        Me.tePjeDescuento.AutoHeight = False
        Me.tePjeDescuento.Mask.EditMask = "n2"
        Me.tePjeDescuento.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tePjeDescuento.Mask.UseMaskAsDisplayFormat = True
        Me.tePjeDescuento.Name = "tePjeDescuento"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cmdUp)
        Me.PanelControl1.Controls.Add(Me.cmdDown)
        Me.PanelControl1.Controls.Add(Me.cmdBorrar)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelControl1.Location = New System.Drawing.Point(863, 133)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(33, 275)
        Me.PanelControl1.TabIndex = 63
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(3, 7)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(27, 24)
        Me.cmdUp.TabIndex = 28
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(3, 42)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(27, 24)
        Me.cmdDown.TabIndex = 29
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(3, 77)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 24)
        Me.cmdBorrar.TabIndex = 30
        '
        'gcHeader
        '
        Me.gcHeader.Controls.Add(Me.LabelControl11)
        Me.gcHeader.Controls.Add(Me.deFechaContable)
        Me.gcHeader.Controls.Add(Me.sePorcReten)
        Me.gcHeader.Controls.Add(Me.LabelControl9)
        Me.gcHeader.Controls.Add(Me.LabelControl1)
        Me.gcHeader.Controls.Add(Me.sePorcIVA)
        Me.gcHeader.Controls.Add(Me.teSerie)
        Me.gcHeader.Controls.Add(Me.LabelControl10)
        Me.gcHeader.Controls.Add(Me.LabelControl6)
        Me.gcHeader.Controls.Add(Me.teCorrelativo)
        Me.gcHeader.Controls.Add(Me.teNumero)
        Me.gcHeader.Controls.Add(Me.LabelControl2)
        Me.gcHeader.Controls.Add(Me.deFecha)
        Me.gcHeader.Controls.Add(Me.LabelControl3)
        Me.gcHeader.Controls.Add(Me.txtDireccion)
        Me.gcHeader.Controls.Add(Me.LabelControl7)
        Me.gcHeader.Controls.Add(Me.txtNRC)
        Me.gcHeader.Controls.Add(Me.LabelControl8)
        Me.gcHeader.Controls.Add(Me.txtNIT)
        Me.gcHeader.Controls.Add(Me.LabelControl13)
        Me.gcHeader.Controls.Add(Me.txtNombre)
        Me.gcHeader.Controls.Add(Me.LabelControl14)
        Me.gcHeader.Controls.Add(Me.leSucursal)
        Me.gcHeader.Controls.Add(Me.LabelControl15)
        Me.gcHeader.Controls.Add(Me.beIdCliente)
        Me.gcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.gcHeader.Location = New System.Drawing.Point(0, 0)
        Me.gcHeader.Name = "gcHeader"
        Me.gcHeader.Size = New System.Drawing.Size(896, 133)
        Me.gcHeader.TabIndex = 62
        Me.gcHeader.Text = "Datos del Proveedor"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(692, 26)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(59, 13)
        Me.LabelControl11.TabIndex = 67
        Me.LabelControl11.Text = "Fecha Libro:"
        '
        'deFechaContable
        '
        Me.deFechaContable.EditValue = Nothing
        Me.deFechaContable.EnterMoveNextControl = True
        Me.deFechaContable.Location = New System.Drawing.Point(753, 21)
        Me.deFechaContable.Name = "deFechaContable"
        Me.deFechaContable.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaContable.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaContable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaContable.Size = New System.Drawing.Size(87, 20)
        Me.deFechaContable.TabIndex = 4
        '
        'sePorcReten
        '
        Me.sePorcReten.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.sePorcReten.Location = New System.Drawing.Point(770, 106)
        Me.sePorcReten.Name = "sePorcReten"
        Me.sePorcReten.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.sePorcReten.Properties.Mask.EditMask = "P2"
        Me.sePorcReten.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.sePorcReten.Size = New System.Drawing.Size(69, 20)
        Me.sePorcReten.TabIndex = 12
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(649, 110)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(119, 13)
        Me.LabelControl9.TabIndex = 65
        Me.LabelControl9.Text = "Porcentaje de retención:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(225, 25)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(121, 13)
        Me.LabelControl1.TabIndex = 62
        Me.LabelControl1.Text = "Serie/No. de Documento:"
        '
        'sePorcIVA
        '
        Me.sePorcIVA.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.sePorcIVA.Location = New System.Drawing.Point(510, 107)
        Me.sePorcIVA.Name = "sePorcIVA"
        Me.sePorcIVA.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.sePorcIVA.Properties.Mask.EditMask = "P2"
        Me.sePorcIVA.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.sePorcIVA.Size = New System.Drawing.Size(69, 20)
        Me.sePorcIVA.TabIndex = 11
        '
        'teSerie
        '
        Me.teSerie.EnterMoveNextControl = True
        Me.teSerie.Location = New System.Drawing.Point(349, 22)
        Me.teSerie.Name = "teSerie"
        Me.teSerie.Size = New System.Drawing.Size(87, 20)
        Me.teSerie.TabIndex = 1
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(418, 110)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(91, 13)
        Me.LabelControl10.TabIndex = 64
        Me.LabelControl10.Text = "Porcentaje de IVA:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(41, 24)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl6.TabIndex = 32
        Me.LabelControl6.Text = "Correlativo:"
        '
        'teCorrelativo
        '
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(100, 21)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Properties.ReadOnly = True
        Me.teCorrelativo.Size = New System.Drawing.Size(112, 20)
        Me.teCorrelativo.TabIndex = 0
        '
        'teNumero
        '
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(441, 22)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(112, 20)
        Me.teNumero.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(564, 25)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl2.TabIndex = 35
        Me.LabelControl2.Text = "Fecha:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(601, 21)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(87, 20)
        Me.deFecha.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(51, 87)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl3.TabIndex = 36
        Me.LabelControl3.Text = "Dirección:"
        '
        'txtDireccion
        '
        Me.txtDireccion.EnterMoveNextControl = True
        Me.txtDireccion.Location = New System.Drawing.Point(100, 84)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(630, 20)
        Me.txtDireccion.TabIndex = 9
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(73, 67)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl7.TabIndex = 44
        Me.LabelControl7.Text = "NRC:"
        '
        'txtNRC
        '
        Me.txtNRC.EnterMoveNextControl = True
        Me.txtNRC.Location = New System.Drawing.Point(100, 63)
        Me.txtNRC.Name = "txtNRC"
        Me.txtNRC.Size = New System.Drawing.Size(112, 20)
        Me.txtNRC.TabIndex = 7
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(325, 67)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl8.TabIndex = 46
        Me.LabelControl8.Text = "NIT:"
        '
        'txtNIT
        '
        Me.txtNIT.EnterMoveNextControl = True
        Me.txtNIT.Location = New System.Drawing.Point(349, 63)
        Me.txtNIT.Name = "txtNIT"
        Me.txtNIT.Size = New System.Drawing.Size(112, 20)
        Me.txtNIT.TabIndex = 8
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(305, 45)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl13.TabIndex = 56
        Me.LabelControl13.Text = "Nombre:"
        '
        'txtNombre
        '
        Me.txtNombre.EnterMoveNextControl = True
        Me.txtNombre.Location = New System.Drawing.Point(349, 42)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(491, 20)
        Me.txtNombre.TabIndex = 6
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(54, 109)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl14.TabIndex = 58
        Me.LabelControl14.Text = "Sucursal:"
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(100, 105)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(247, 20)
        Me.leSucursal.TabIndex = 10
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(3, 44)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(95, 13)
        Me.LabelControl15.TabIndex = 60
        Me.LabelControl15.Text = "Cód. de Proveedor:"
        '
        'beIdCliente
        '
        Me.beIdCliente.EnterMoveNextControl = True
        Me.beIdCliente.Location = New System.Drawing.Point(100, 42)
        Me.beIdCliente.Name = "beIdCliente"
        Me.beIdCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beIdCliente.Size = New System.Drawing.Size(112, 20)
        Me.beIdCliente.TabIndex = 5
        '
        'com_frmRetenciones
        '
        Me.ClientSize = New System.Drawing.Size(902, 461)
        Me.Controls.Add(Me.xtcRetencion)
        Me.Modulo = "Compras"
        Me.Name = "com_frmRetenciones"
        Me.OptionId = "002007"
        Me.Text = "Comprobante de Retención"
        Me.Controls.SetChildIndex(Me.xtcRetencion, 0)
        CType(Me.xtcRetencion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcRetencion.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcTotales.ResumeLayout(False)
        Me.pcTotales.PerformLayout()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teRetencion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePrecioUnitario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePjeDescuento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcHeader.ResumeLayout(False)
        Me.gcHeader.PerformLayout()
        CType(Me.deFechaContable.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaContable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sePorcReten.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sePorcIVA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNIT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beIdCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents xtcRetencion As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents pcTotales As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teRetencion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teIva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tePrecioUnitario As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colPrecioTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tePjeDescuento As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaContable As DevExpress.XtraEditors.DateEdit
    Friend WithEvents sePorcReten As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sePorcIVA As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents teSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNIT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beIdCliente As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit

End Class
