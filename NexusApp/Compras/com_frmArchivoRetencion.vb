﻿Imports NexusBLL
Imports System.IO
Public Class com_frmArchivoRetencion
    Dim bl As New ComprasBLL(g_ConnectionString)
    Private Sub btGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGenerar.Click
        Dim fbd As New FolderBrowserDialog

        fbd.ShowDialog()
        If fbd.SelectedPath = "" Then
            MsgBox("No se seleccionó ninguna carpeta", MsgBoxStyle.Critical, "Nota")
            Return
        End If

        Dim dt As DataTable = bl.com_ObtenerDatosf930(deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)

        Try
            GenerarArchivo(String.Format("{0}\{1}.txt", fbd.SelectedPath, "InformeF930"), dt)
            MsgBox("El archivo ha sido generado con éxito." + Chr(13) + "Y fue colocado en " + fbd.SelectedPath, MsgBoxStyle.Information, "Nota")

        Catch ex As Exception
            MsgBox("NO FUE POSIBLE GENERAR LOS ARCHIVOS" + Chr(13) + ex.Message.ToString, MsgBoxStyle.Critical, "Nota")
        End Try
    End Sub
    Private Sub GenerarArchivo(ByVal Archivo As String, ByVal dt As DataTable)
        FileOpen(1, Archivo, OpenMode.Output)
        Dim sb As String = ""
        Dim dc As DataColumn

        Dim i As Integer = 0
        Dim dr As DataRow
        For Each dr In dt.Rows
            i = 0 : sb = ""
            For Each dc In dt.Columns
                If Not IsDBNull(dr(i)) Then
                    sb &= CStr(dr(i))
                End If
                i += 1
            Next
            PrintLine(1, sb)
        Next
        FileClose(1)
    End Sub

    Private Sub con_frmGenerarArchivosICV_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        leSucursal.EditValue = piIdSucursalUsuario
    End Sub
End Class
