﻿Imports NexusELL.TableEntities
Imports NexusBLL

Public Class com_frmContabilizarLiquidacion
    Dim myBL As New ComprasBLL(g_ConnectionString), blConta As New ContabilidadBLL(g_ConnectionString)
    Dim EntCuentas As con_Cuentas
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub com_frmContabilizar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.banTiposTransaccion(leTipoTransaccion, "")
        objCombos.conTiposPartida(leTipo)
        objCombos.adm_Sucursales(leSucursalDet, objMenu.User, "")
        gc.DataSource = blConta.con_ObtenerPeriodoContabilizado(22, entUsuario.IdSucursal, objMenu.User)
    End Sub

    Private Sub sbReverir_Click(sender As Object, e As EventArgs) Handles sbReverir.Click
        If Not AllowDelete Then
            MsgBox("No le está permitido eliminar información" + Chr(13) + "Verifique sus permisos con el administrador del sistema", MsgBoxStyle.Exclamation, Me.Text)
            Exit Sub
        End If

        Dim Desde As Date = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "DesdeFecha"), Today)
        Dim Hasta As Date = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "HastaFecha"), Today)
        Dim TipoPartida As String = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipoPartida"), "")

        If MsgBox("Está seguro(a) de revertir el período contabilizado de: " + Chr(13) + Desde + " al " + Hasta + " de tipo de partida: " + TipoPartida + " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        If MsgBox("Confirme Nuevamente.", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(Desde)
        If Not EsOk Then
            MsgBox("Fecha de la partida corresponde a un período ya cerrado", MsgBoxStyle.Critical, "Imposible eliminar")
            Exit Sub
        End If

        'Dim msj As Integer = myBL.com_EliminaPartidas(3, Desde, Hasta, TipoPartida, SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdSucursal"), 0))
        Dim msj As Integer = myBL.com_EliminaPartidas(22, Desde, Hasta, TipoPartida, SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdSucursal"), 0), 0, "")

        If msj >= 1 Then
            MsgBox("La eliminación se ha realizado con éxito", 64, "Nota")
        Else
            MsgBox("La eliminación NO se pudo realizar", MsgBoxStyle.Critical, "Error de base de datos")
        End If
        gc.DataSource = blConta.con_ObtenerPeriodoContabilizado(22, piIdSucursalUsuario, objMenu.User)
    End Sub

    Private Sub sbAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbAceptar.Click
        If MsgBox("Está seguro(a) de contabilizar éstos documentos?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        If deDesde.EditValue > deHasta.EditValue Then
            MsgBox("La fecha inicial no puede ser mayor a la fecha final", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If deDesde.DateTime.Month <> deHasta.DateTime.Month Then
            MsgBox("La contabilización debe ser del mismo mes", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(deDesde.EditValue)
        If Not EsOk Then
            MsgBox("La fecha inicial está fuera del período permitido", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        If blConta.con_ValidaContabilizacion(leSucursal.EditValue, 22, deDesde.EditValue, deHasta.EditValue) > 0 Then
            MsgBox("Existen documentos contabilizados con este período", MsgBoxStyle.Critical, "Error de Usuario")
            Exit Sub
        End If

        Dim res As String = myBL.com_ContabilizarLiquidacion(deDesde.EditValue, deHasta.EditValue, leTipoTransaccion.EditValue, leSucursal.EditValue, objMenu.User)
        If res = "Ok" Then
            MsgBox("La contabilización se ha realizado con éxito", 64, "Nota")
        Else
            MsgBox("La contabilización NO se pudo realizar" + Chr(13) + res, MsgBoxStyle.Critical, "Error de base de datos")
        End If
        gc.DataSource = blConta.con_ObtenerPeriodoContabilizado(22, piIdSucursalUsuario, objMenu.User)
    End Sub


End Class
