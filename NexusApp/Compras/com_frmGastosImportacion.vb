﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class com_frmGastosImportacion
    Dim entidad As com_GastosImportaciones
    Dim entCuentas As con_Cuentas

    Private Sub ban_frmTiposTransaccion_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub ban_frmTiposTransaccion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        gc.DataSource = objTablas.com_GastosImportacionesSelectAll
        entidad = objTablas.com_GastosImportacionesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdGasto"))

        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub cban_frmTiposTransaccion_Nuevo_Click() Handles Me.Nuevo
        entidad = New com_GastosImportaciones
        entidad.IdGasto = objFunciones.ObtenerUltimoId("COM_GASTOSIMPORTACIONES", "IdGasto") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub ban_frmTiposTransaccion_Save_Click() Handles Me.Guardar
        If teNombre.EditValue = "" Or SiEsNulo(beCta01.EditValue, "") = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Nombre, Cuenta Contable]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.com_GastosImportacionesInsert(entidad)
        Else
            objTablas.com_GastosImportacionesUpdate(entidad)
        End If
        gc.DataSource = objTablas.com_GastosImportacionesSelectAll
        entidad = objTablas.com_GastosImportacionesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdGasto"))
        CargaPantalla()
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub ban_frmTiposTransaccion_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.com_GastosImportacionesDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.com_GastosImportacionesSelectAll
                entidad = objTablas.com_GastosImportacionesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdGasto"))
                CargaPantalla()
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL REGISTRO:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub ban_frmTiposTransaccion_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entidad
            teId.EditValue = .IdGasto
            teNombre.EditValue = .Nombre
            beCta01.EditValue = .IdCuenta
            entCuentas = objTablas.con_CuentasSelectByPK(.IdCuenta)
            teCta01.EditValue = entCuentas.Nombre
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entidad
            .IdGasto = teId.EditValue
            .Nombre = teNombre.EditValue
            .IdCuenta = beCta01.EditValue
        End With
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entidad = objTablas.com_GastosImportacionesSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdGasto"))
        CargaPantalla()
    End Sub

    Private Sub cban_frmTiposTransaccion_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teId.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub
    Private Sub beCta01_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta01.ButtonClick
        beCta01.EditValue = ""
        beCta01_Validated(beCta01, New System.EventArgs)
    End Sub
    Private Sub beCta01_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCta01.Validated
        entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta01.EditValue)
        If Not entCuentas.EsTransaccional Then
            MsgBox("No puede usar ésta cuenta. No acepta transacciones o es de tipo mayor", MsgBoxStyle.Critical, "Nota")
            beCta01.EditValue = ""
            teCta01.EditValue = ""
            Exit Sub
        End If
        beCta01.EditValue = entCuentas.IdCuenta
        teCta01.EditValue = entCuentas.Nombre
    End Sub
End Class
