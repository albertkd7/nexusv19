﻿Imports NexusELL.TableEntities
Imports NexusBLL

Public Class com_frmContabilizarCajaChica

    Dim myBL As New ComprasBLL(g_ConnectionString)
    Dim blConta As New ContabilidadBLL(g_ConnectionString)
    Dim EntCuentas As con_Cuentas
    Dim entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    'Private Sub com_frmContabilizar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress
    '    Close()
    'End Sub

    Private Sub com_frmContabilizar_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.conTiposPartida(leTipoPartida)
        objCombos.conTiposPartida(leTipo)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDet, objMenu.User, "")
        objCombos.Com_CajasChicaComprasContabilizar(leCaja, "")
        objCombos.Com_CajasChicaComprasContabilizar(leCajero, "")
        gc.DataSource = blConta.con_ObtenerPeriodoContabilizado(33, entUsuario.IdSucursal, objMenu.User, True)
    End Sub

    Private Sub sbAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbAceptar.Click
        If MsgBox("Está seguro(a) de contabilizar éstos documentos?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If


        If deDesde.DateTime.Month <> deHasta.DateTime.Month Then
            MsgBox("La contabilización debe ser del mismo mes", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        Dim EsOk As Boolean = ValidarFechaCierre(deDesde.EditValue)
        If Not EsOk Then
            MsgBox("La fecha inicial está fuera del período permitido", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim dtContabilizado As DataTable = myBL.com_ValidaContabilizacion(leSucursal.EditValue, 33, deDesde.EditValue, deHasta.EditValue, leCaja.EditValue)
        If dtContabilizado.Rows.Count > 0 Then
            MsgBox("Ya existen partidas contabilizadas con este período", MsgBoxStyle.Critical, "Error de Usuario")
            Exit Sub
        End If

        Dim res As String = myBL.com_ContabilizarComprasCajaChica(deDesde.EditValue, deHasta.EditValue, leTipoPartida.EditValue, leSucursal.EditValue, objMenu.User, leCaja.EditValue)
        If res = "Ok" Then
            MsgBox("La contabilización se ha realizado con éxito", 64, "Nota")
        Else
            MsgBox("La contabilización NO se pudo realizar" + Chr(13) + res, MsgBoxStyle.Critical, "Error de base de datos")
        End If
        gc.DataSource = blConta.con_ObtenerPeriodoContabilizado(33, entUsuario.IdSucursal, objMenu.User, True)
    End Sub

    Private Sub sbReverir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbReverir.Click
        If Not AllowDelete Then
            MsgBox("No le está permitido eliminar información" + Chr(13) + "Verifique sus permisos con el administrador del sistema", MsgBoxStyle.Exclamation, Me.Text)
            Exit Sub
        End If

        Dim Desde As Date = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "DesdeFecha"), Today)
        Dim Hasta As Date = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "HastaFecha"), Today)
        Dim TipoPartida As String = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipoPartida"), "")

        If MsgBox("Está seguro(a) de revertir el período contabilizado de: " + Chr(13) + Desde + " al " + Hasta + " de tipo de partida: " + TipoPartida + " ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        If MsgBox("Confirme Nuevamente.", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(Desde)
        If Not EsOk Then
            MsgBox("Fecha de la partida corresponde a un período ya cerrado", MsgBoxStyle.Critical, "Imposible eliminar")
            Exit Sub
        End If

        Dim msj As Integer = myBL.com_EliminaPartidas(33, Desde, Hasta, TipoPartida, SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdSucursal"), 0), SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCaja"), 1), SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "Eliminacion"), ""))

        If msj >= 1 Then
            MsgBox("La eliminación se ha realizado con éxito", 64, "Nota")
        Else
            MsgBox("La eliminación NO se pudo realizar", MsgBoxStyle.Critical, "Error de base de datos")
        End If
        gc.DataSource = blConta.con_ObtenerPeriodoContabilizado(33, entUsuario.IdSucursal, objMenu.User, True)
    End Sub
End Class
