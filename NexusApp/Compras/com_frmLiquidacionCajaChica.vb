﻿
Imports NexusBLL
Public Class com_frmLiquidacionCajaChica
    Dim bl As New ComprasBLL(g_ConnectionString)


    Private Sub com_frmLiquidacionCajaChica_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFechaI.DateTime = Today
        deFechaF.DateTime = Today
        objCombos.Com_CajasChicaCompras(leCaja, "-- TODAS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        rgTipoConsulta.EditValue = 3
    End Sub

    Private Sub SimpleButton1_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.com_ComprasCajaChica(deFechaI.DateTime, deFechaF.DateTime, leCaja.EditValue, leSucursal.EditValue, rgTipoConsulta.EditValue)
        Dim rpt As New com_rptComprasCajaChica() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = txtTitulo.EditValue & " - SUCURSAL: " & leSucursal.Text
        rpt.xrlPeriodo.Text = (FechaToString(deFechaI.DateTime, deFechaF.DateTime)).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda
        rpt.ShowPreviewDialog()
    End Sub
End Class
