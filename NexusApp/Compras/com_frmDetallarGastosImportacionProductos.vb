﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class com_frmDetallarGastosImportacionProductos
    Dim dtDetalle As New DataTable
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim dtGastosImpProd As DataTable

    Private _IdImportacion As Integer
    Public Property IdImportacion() As Integer
        Get
            Return _IdImportacion
        End Get
        Set(ByVal value As Integer)
            _IdImportacion = value
        End Set
    End Property

    Private _IdProducto As String
    Public Property IdProducto() As String
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As String)
            _IdProducto = value
        End Set
    End Property
    Private _Referencia As String
    Public Property Referencia() As String
        Get
            Return _Referencia
        End Get
        Set(ByVal value As String)
            _Referencia = value
        End Set
    End Property

    Private Sub com_frmDetallarGastosImportaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.com_GastosImportaciones(leGasto)
        LlenarGastosNuevo()
    End Sub

    Private Sub LlenarEntidades()
        Dim frmImp As com_frmImportaciones = Me.Owner
        Dim dt As DataTable = frmImp.dtGastosImpProd

        'PRIMERO BORRO DEL DATATABLE ORIGINAL EL REGISTRO DE ESTE PRODUCTO SI YA SE ENCUENTRA INGRESADO
        'PARA QUE TOME COMO DATOS VALIDOS LOS QUE A MARCADO AQUI

        'For j = 0 To dt.Rows.Count - 1
        '    If dt.Rows(j).Item("IdProducto") = IdProducto Then
        '        frmImp.dtGastosImpProd.Rows(j).Delete()
        '    End If
        'Next
        'frmImp.dtGastosImpProd.AcceptChanges()

        Dim filas As DataRow() = frmImp.dtGastosImpProd.Select("IdProducto ='" & IdProducto + "'")

        For Each filaActual As DataRow In filas
            filaActual.Delete()
        Next
        frmImp.dtGastosImpProd.AcceptChanges()

        For i = 0 To gv.DataRowCount - 1
            Dim dr As DataRow = frmImp.dtGastosImpProd.NewRow()
            dr("IdImportacion") = gv.GetRowCellValue(i, "IdImportacion")
            dr("Referencia") = gv.GetRowCellValue(i, "Referencia")
            dr("IdProducto") = gv.GetRowCellValue(i, "IdProducto")
            dr("Descripcion") = gv.GetRowCellValue(i, "Descripcion")
            dr("IdGasto") = gv.GetRowCellValue(i, "IdGasto")
            dr("AplicaGasto") = gv.GetRowCellValue(i, "AplicaGasto")
            frmImp.dtGastosImpProd.Rows.Add(dr)
        Next

        Me.Close()
    End Sub

#Region "Grilla"
    'Private Sub InicializaGridsss()
    '    dtDetalle = New DataTable
    '    dtDetalle.Columns.Add(New DataColumn("Abonar", GetType(Boolean)))
    '    dtDetalle.Columns.Add(New DataColumn("NumeroComprob", GetType(String)))
    '    dtDetalle.Columns.Add(New DataColumn("FechaComprob", GetType(Date)))
    '    dtDetalle.Columns.Add(New DataColumn("FechaVencto", GetType(Date)))
    '    dtDetalle.Columns.Add(New DataColumn("TotalComprob", GetType(Decimal)))
    '    dtDetalle.Columns.Add(New DataColumn("SaldoComprob", GetType(Decimal)))
    '    dtDetalle.Columns.Add(New DataColumn("MontoAbonar", GetType(Decimal)))
    '    gc.DataSource = dtDetalle
    'End Sub


    'Private Sub gv_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.GotFocus
    '    SendKeys.SendWait("{enter}")
    'End Sub
    'Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "Abonar", False)
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "NumeroComprob", "")
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "TotalComprob", 0.0)
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "SaldoComprob", 0.0)
    '    gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", 0.0)
    'End Sub
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        gv.DeleteSelectedRows()
    End Sub


#End Region

    Private Sub sbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGuardar.Click
        LlenarEntidades()
    End Sub

    Private Sub LlenarGastosNuevo()
        Dim frmImp As com_frmImportaciones = Me.Owner
        Dim dt As DataTable = frmImp.dtGastosImpProd

        If dt.Rows.Count > 0 Then

            Dim dt2 As New DataTable
            dt2 = bl.com_ObtenerGastosImportacionProdEstructura()


            For i = 0 To dt.Rows.Count - 1
                If SiEsNulo(dt.Rows(i).Item("IdProducto"), "") = IdProducto And SiEsNulo(dt.Rows(i).Item("Referencia"), "") = Referencia Then
                    Dim dr As DataRow = dt2.NewRow()
                    dr("IdImportacion") = dt.Rows(i).Item("IdImportacion")
                    dr("Referencia") = dt.Rows(i).Item("Referencia")
                    dr("IdProducto") = dt.Rows(i).Item("IdProducto")
                    dr("Descripcion") = dt.Rows(i).Item("Descripcion")
                    dr("IdGasto") = dt.Rows(i).Item("IdGasto")
                    dr("AplicaGasto") = dt.Rows(i).Item("AplicaGasto")
                    dt2.Rows.Add(dr)
                End If
            Next

            gc.DataSource = dt2
        Else
            gc.DataSource = dt
        End If

        ' se asigno este valor para el dt quede sin datos para que cuando se guardar aki lo rellene nuevamente
        'frmImp.dtGastosImpProd = bl.com_ObtenerGastosImportacionProd("-999", gv.GetRowCellValue(0, "IdImportacion"), "")

        'For Each drowMyBox As System.Data.DataRow In frmImp.dtGastosImpProd.Rows
        '    If drowMyBox("IdProducto") = gv.GetRowCellValue(0, "IdProducto") Then
        '        drowMyBox.Delete()
        '    End If
        'Next
        'frmImp.dtGastosImpProd.AcceptChanges()

    End Sub
End Class
