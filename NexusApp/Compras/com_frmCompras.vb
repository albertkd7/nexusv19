﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Imports System.Math

Public Class com_frmCompras
    Dim Header As com_Compras
    Dim Detalle As List(Of com_ComprasDetalle)
    Dim ComprasImpuestos As List(Of com_ComprasDetalleImpuestos)
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim blInve As New InventarioBLL(g_ConnectionString)
    Dim blCpp As New CuentasPPBLL(g_ConnectionString), blAdmon As New AdmonBLL(g_ConnectionString), EntUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim MyBL As New AdmonBLL(g_ConnectionString)
    Dim entProducto As inv_Productos, dtParam As DataTable = blAdmon.ObtieneParametros()
    Dim IdSucUser As Integer = SiEsNulo(EntUsuario.IdSucursal, 1)
    Dim dioClick As Boolean = False, CambioIVA As Boolean = False, TotalImpuestos As Decimal = 0.0, CambioPerRet As Boolean
    Dim _dtImpuestos As DataTable, AutorizoReversion As String = "", CopyCompra As Boolean = False

    Property dtImpuestos() As DataTable
        Get
            Return _dtImpuestos
        End Get
        Set(ByVal value As DataTable)
            _dtImpuestos = value
        End Set
    End Property


    Private Sub com_frmCompras_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = bl.com_ConsultaCompras(objMenu.User).Tables(0)
        gv2.BestFitColumns()
    End Sub

    Private Sub com_frmCompras_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        CargaCombos()
       
        If sbGuardar.Visible Then
            com_frmCompras_Nuevo()
            beProveedor_Validated(sender, New System.EventArgs)
            DbMode = DbModeType.insert
            teCorrelativo.Properties.ReadOnly = True
        Else
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("COM_COMPRAS", "IdComprobante")

            ActivarControles(False)
        End If

        dioClick = False

        gc2.DataSource = bl.com_ConsultaCompras(objMenu.User).Tables(0)
    End Sub
    Private Sub com_frmCompras_Nuevo() Handles Me.Nuevo
        Header = New com_Compras
        ActivarControles(True)
        gc.DataSource = bl.com_ObtenerCompraDetalle(-1)
        gv.CancelUpdateCurrentRow()
        '  gv.AddNewRow()

        leFormaPago.EditValue = 2
        teSerie.EditValue = ""
        leTipoCompra.EditValue = 1
        leOrigenCompra.EditValue = 1
        leBodega.EditValue = 1
        If OrigenCompra = "Quedan" Or OrigenCompra = "Liquidacion" Then
            leFormaPago.Properties.ReadOnly = True
        Else
            beProveedor.EditValue = ""
            leFormaPago.Properties.ReadOnly = False
            teNumero.EditValue = ""
            deFechaCompra.EditValue = Today
            deFechaContable.EditValue = Today
        End If

        If OrigenCompra = "Liquidacion" Then
            leFormaPago.EditValue = 1
        End If

        deFechaVto.EditValue = Today
        teOrdenCompra.EditValue = ""
        teIva.EditValue = 0.0
        tePercepcionRetencion.EditValue = 0.0

        teTotal.EditValue = 0.0
        'teDireccion.EditValue = ""
        teNombre.EditValue = ""
        'teGiro.EditValue = ""
        teNIT.EditValue = ""
        teNRC.EditValue = ""
        teDocExcluido.EditValue = ""
        ceExcluido.EditValue = False
        ceRebajaCosto.EditValue = False
        rgTipo.EditValue = 1
        leTipoDocto.EditValue = 1
        teIdPartida.EditValue = 0
        teIdCheque.EditValue = 0
        CambioIVA = False
        dioClick = False
        CopyCompra = False
        leSucursal.EditValue = EntUsuario.IdSucursal

        teNumeroControlInternoFormularioUnico.EditValue = ""
        leClaseDocumento.EditValue = 1
        teResolucion.EditValue = ""

        leCaja.EditValue = 1
        teSerie.Focus()
        _dtImpuestos = bl.com_ObtenerImpuestos(-1, 0)
        teImpuestos.EditValue = 0.0
        CalculaOtrosImpuestos()
        xtcCompras.SelectedTabPage = xtpDatos1
    End Sub
    Private Sub com_frmCompras_Guardar() Handles Me.Guardar, sbGuardar.Click
        If Not DatosValidos() Then
            Exit Sub
        End If
        CargaEntidades()
        Dim msj As String = bl.com_GuardaCompra(Header, Detalle, ComprasImpuestos, DbMode = DbModeType.insert)
        If msj = "Ok" Then
            MsgBox("El documento ha sido guardado con éxito", MsgBoxStyle.Information, "Nota")
        Else
            MsgBox(String.Format("NO FUE POSIBLE GUARDAR EL DOCUMENTO{0}{1}", Chr(13), msj), MsgBoxStyle.Critical)
            Return
        End If
        ' se coloco aqui que refresque el correlativo, porque dejaba pegado el del documento anterior donde se creo

        teCorrelativo.EditValue = Header.IdComprobante
        If leTipoDocto.EditValue = 2 And leFormaPago.EditValue = 2 Then
            com_frmAplicacionNotaCredito.IdComprobante = teCorrelativo.EditValue
            com_frmAplicacionNotaCredito.NumeroNota = teNumero.EditValue
            com_frmAplicacionNotaCredito.FechaNota = deFechaCompra.EditValue
            com_frmAplicacionNotaCredito.IdProveedor = beProveedor.EditValue
            com_frmAplicacionNotaCredito.Nombre = teNombre.EditValue
            com_frmAplicacionNotaCredito.MontoAbonar = teTotal.EditValue
            com_frmAplicacionNotaCredito.IdSucursal = leSucursal.EditValue
            com_frmAplicacionNotaCredito.ShowDialog()
        End If

        If sbGuardar.Visible Then  'EL DOCUMENTO SE ESTÁ INGRESANDO DESDE OTRO MODULO
            Dispose()
            cpp_frmQuedan.DataCompra = Header.IdComprobante & "|" & teNumero.EditValue & "|" & leTipoDocto.EditValue & "|" & deFechaCompra.EditValue & "|" & teTotal.EditValue & "|" & tePercepcionRetencion.EditValue & "|" & teIva.EditValue & "|" & Me.gcValorAfecto.SummaryItem.SummaryValue & "|" & Me.gcValorExento.SummaryItem.SummaryValue
        Else
            MostrarModoInicial()
            ActivarControles(False)
        End If

        CopyCompra = False
        gc2.DataSource = bl.com_ConsultaCompras(objMenu.User).Tables(0)
        teSerie.Focus()
    End Sub
    Private Sub com_frmCompras_Editar() Handles Me.Editar
        If gv2.GetFocusedRowCellValue("IdComprobante") > 0 Then
        Else
            MsgBox("No ha seleccionado ningún documento", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        Header = objTablas.com_ComprasSelectByPK(gv2.GetFocusedRowCellValue("IdComprobante"))
        If Header.Serie.Contains("TRD") Then
            MsgBox("No puede editar ésta compra." + Chr(13) + "Pertenece a un Traslado de Saldos", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        If Header.AplicadaInventario Then
            MsgBox("No puede editar ésta compra." + Chr(13) + "Debe revertirla antes de modificar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If

        ActivarControles(True)
        CargaPantalla()
        _dtImpuestos = bl.com_ObtenerImpuestos(Header.IdComprobante, Header.TotalAfecto)
        CalculaOtrosImpuestos()
        CopyCompra = False
        CambioIVA = False
    End Sub

    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        Header = objTablas.com_ComprasSelectByPK(gv2.GetFocusedRowCellValue("IdComprobante"))
        CargaPantalla()
    End Sub

    Private Sub com_frmCompras_Revertir() Handles Me.Revertir
        xtcCompras.SelectedTabPage = xtpLista1
        gc2.Focus()
        ActivarControles(False)
    End Sub
    Private Sub com_frmCompras_Eliminar() Handles Me.Eliminar
        Header = objTablas.com_ComprasSelectByPK(SiEsNulo(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"), 0))
        If Not ValidarFechaCierre(Header.Fecha) Then
            MsgBox("La fecha de la compra no corresponde al período activo. Imposible eliminar", MsgBoxStyle.Critical, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        If Header.AplicadaInventario Then
            MsgBox("Es necesario que revierta la compra. No se puede eliminar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If

        If bl.com_CompraConAbono(Header.IdComprobante, 1) > 0 Then
            MsgBox("La compra tiene abonos en cuentas por pagar" + Chr(13) + " No se puede eliminar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If

        If MsgBox("¿Está seguro(a) de eliminar éste documento de compra?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                'al eliminar un documento de compra se debe confirmar si no está relacionado con otro tipo de info
                Header.ModificadoPor = objMenu.User
                objTablas.com_ComprasUpdate(Header)

                objTablas.com_ComprasDeleteByPK(Header.IdComprobante)
                MsgBox("El documento fue eliminado con éxito", MsgBoxStyle.Information, "Nota")
                teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("COM_COMPRAS", "IdComprobante")
            Catch ex As Exception
                MsgBox(String.Format("El documento no pudo ser eliminado{0}{1}", Chr(13), ex.Message), MsgBoxStyle.Critical, "Nota")
            End Try
        End If
        Close()
    End Sub
    Private Sub com_frmCompras_Reporte() Handles Me.Reporte
        Header = objTablas.com_ComprasSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Dim entTipo As adm_TiposComprobante = objTablas.adm_TiposComprobanteSelectByPK(Header.IdTipoComprobante)
        Dim entForma As fac_FormasPago = objTablas.fac_FormasPagoSelectByPK(Header.IdFormaPago)
        Dim entBodega As inv_Bodegas = objTablas.inv_BodegasSelectByPK(Header.IdBodega)
        Dim dt As DataTable = bl.com_ObtenerDocumentoCompra(Header.IdComprobante)
        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)

        If Not ceExcluido.EditValue Then
            Dim rpt As New com_rptCompra() With {.DataSource = dt, .DataMember = ""}
            Dim TemplateGral = Application.StartupPath & "\Plantillas\ComNexus.repx"
            Dim Template = Application.StartupPath & "\Plantillas\ComNexus" & NumFormato & ".repx"

            If Not FileIO.FileSystem.FileExists(Template) Then
                If FileIO.FileSystem.FileExists(TemplateGral) Then
                    rpt.LoadLayout(TemplateGral)
                Else
                    MsgBox("No existe la plantilla necesaria para el documento, se procedera a imprimir el formato estandar", MsgBoxStyle.Critical, "Nota")
                End If
            Else
                rpt.LoadLayout(Template)
            End If
            rpt.DataMember = ""
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = String.Format("{0} No. {1}/{2}", entTipo.Nombre, Header.Serie, Header.Numero)
            rpt.xrlTipo.Text = "GRAVADA"
            rpt.xrlForma.Text = entForma.Nombre
            rpt.xrlSucursal.Text = "OFICINA CENTRAL"
            rpt.xrlBodega.Text = entBodega.Nombre
            rpt.xrlOrigen.Text = "LOCAL"
            rpt.ShowPreviewDialog()
        Else
            Dim Decimales = String.Format("{0:c}", Header.TotalComprobante)
            Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"

            Dim rpt As New com_rptExcluido With {.DataSource = dt, .DataMember = ""}
            Dim Template = Application.StartupPath & "\Plantillas\ComExcluido" & NumFormato & ".repx"
            If Not FileIO.FileSystem.FileExists(Template) Then
                MsgBox("No existe la plantilla necesaria para el documento, se procedera a imprimir el formato estandar", MsgBoxStyle.Critical, "Nota")
            Else
                rpt.LoadLayout(Template)
            End If

            rpt.xrlCantLetras.Text = Num2Text(Int(Header.TotalComprobante)) & Decimales & " DÓLARES"
            rpt.xrlSubTotal.Text = Format(Header.TotalAfecto + Header.TotalExento, "###,##0.00")
            rpt.xrlTotal.Text = Format(Header.TotalComprobante, "###,##0.00")
            rpt.ShowPreviewDialog()
        End If

    End Sub

    Function DatosValidos() As Boolean
        CalcularTotales()
        Dim msj As String = ""

        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        For i = 0 To gv.DataRowCount - 1
            If blInve.inv_ObtieneTipoProducto(SiEsNulo(gv.GetRowCellValue(i, "IdProducto"), "")) > 1 Then 'PARA TODOS LOS SERVICIOS NO APLICA
                Exit For
            End If
            'If gv.GetRowCellValue(i, "IdProducto") = gv.GetRowCellValue(i + 1, "IdProducto") Then
            '    msj = "El producto " & gv.GetRowCellValue(i, "IdProducto") & ", está repetido"
            '    Exit For
            'End If
        Next
        gv.Columns("IdProducto").SortOrder = DevExpress.Data.ColumnSortOrder.None

        Dim SaldoProveedor As Decimal = 0.0, IdComprobante As Integer = 0
        Dim AbonoHeader As cpp_Abonos
        If leTipoDocto.EditValue = 2 And leFormaPago.EditValue = 2 Then
            IdComprobante = blCpp.cpp_ObtenerUltAbono(beProveedor.EditValue)
            AbonoHeader = objTablas.cpp_AbonosSelectByPK(IdComprobante)
            SaldoProveedor = blCpp.GetSaldoProveedor(beProveedor.EditValue, deFechaContable.EditValue, leSucursal.EditValue)
            ''   If deFechaContable.EditValue >= AbonoHeader.Fecha Then
            If teTotal.EditValue > SaldoProveedor Then
                    msj = "El Valor de la Nota de Crédito excede el saldo del proveedor"
                End If
            'Else
            '    msj = "La Fecha de la Nota de Crédito, No puede ser inferior al Ultimo Abono"
            '' End If
        End If

        If Not ceExcluido.EditValue Then
            If teNumero.EditValue = "" OrElse teSerie.EditValue = "" OrElse teResolucion.EditValue = "" Then
                msj = "Debe de especificar la serie, número de documento y número de resolución"
            End If
        End If

        If leSucursal.EditValue = 0 Or leSucursal.EditValue = Nothing Or leSucursal.Text = "" Then
            msj = "Debe seleccionar una sucursal"
        End If
        If leClaseDocumento.EditValue = 2 And teNumeroControlInternoFormularioUnico.EditValue = "" Then
            msj = "Debe especificar el número de control interno del documento emitido en formulario único"
        End If
        If beProveedor.EditValue = "" Then
            msj = "Debe de especificar el código del proveedor"
        End If
        If teNRC.EditValue = "" Or teNIT.EditValue = "" Then
            msj = "Debe de especificar el NIT y NRC del proveedor"
        End If
        Dim EsOk As Boolean = ValidarFechaCierre(deFechaContable.EditValue)
        If Not EsOk Then
            msj = "La fecha contable no corresponde al período activo"
        End If
        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If
        Return True
    End Function

    Private Sub CargaEntidades()
        With Header
            .Serie = teSerie.Text
            .Numero = teNumero.EditValue
            .IdProveedor = beProveedor.EditValue
            .Nombre = teNombre.Text
            .Nrc = teNRC.EditValue
            .Nit = teNIT.Text
            .Fecha = deFechaCompra.EditValue
            .FechaContable = deFechaContable.DateTime
            .DiasCredito = seDiasCredito.EditValue
            .FechaVencimiento = deFechaVto.EditValue
            .IdFormaPago = leFormaPago.EditValue
            .IdSucursal = leSucursal.EditValue
            .IdBodega = leBodega.EditValue
            .NumOrdenCompra = teOrdenCompra.Text
            .IdOrigenCompra = leOrigenCompra.EditValue
            .IdTipoCompra = leTipoCompra.EditValue
            .IdTipoComprobante = leTipoDocto.EditValue
            .CompraExcluido = ceExcluido.EditValue
            .NumDocExcluido = teDocExcluido.EditValue
            .AplicadaInventario = False
            .TotalExento = Me.gcValorExento.SummaryItem.SummaryValue
            .TotalAfecto = Me.gcValorAfecto.SummaryItem.SummaryValue
            .TotalIva = teIva.EditValue
            .TipoImpuestoAdicional = rgTipo.EditValue
            .TotalImpuesto1 = tePercepcionRetencion.EditValue
            .TotalImpuesto2 = teImpuestos.EditValue
            .IdPartida = teIdPartida.EditValue
            .IdCheque = teIdCheque.EditValue
            .TotalComprobante = teTotal.EditValue

            .ExcluirLibro = ceExcluirLibroCompras.Checked
            .IdCaja = leCaja.EditValue
            .Contabilizada = False
            .AfectaCosto = ceRebajaCosto.EditValue

            .Correlativo = leClaseDocumento.EditValue
            .Giro = teResolucion.EditValue
            .Direccion = teNumeroControlInternoFormularioUnico.EditValue

            If DbMode = DbModeType.insert Then

                If ceExcluido.EditValue Then
                    Dim UltimoNumero As Integer = MyBL.ObtieneCorrelativos("SUJETO_EXCLUIDO") + 1
                    teNumero.EditValue = UltimoNumero.ToString.PadLeft(6, "0")
                    .Numero = teNumero.EditValue
                End If

                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If
        End With

        Detalle = New List(Of com_ComprasDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New com_ComprasDetalle
            With entDetalle
                .IdComprobante = 0  ' Se asigna en la capa de datos
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .TipoImpuesto = gv.GetRowCellValue(i, "TipoImpuesto")
                .PrecioReferencia = gv.GetRowCellValue(i, "PrecioReferencia")
                .PorcDescto = gv.GetRowCellValue(i, "PorcDescto")
                .ValorDescto = gv.GetRowCellValue(i, "ValorDescto")
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .ValorExento = gv.GetRowCellValue(i, "ValorExento")
                .ValorAfecto = gv.GetRowCellValue(i, "ValorAfecto")
                .IdCuenta = gv.GetRowCellValue(i, "IdCuenta")
                .IdCentro = gv.GetRowCellValue(i, "IdCentro")
                .IdDetalle = i + 1
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            Detalle.Add(entDetalle)
        Next

        If DbMode = DbModeType.insert Then
            If dioClick = False Then
                _dtImpuestos = bl.com_ObtenerImpuestos(Header.IdComprobante, 0)
                dtImpuestos = bl.com_ObtenerImpuestos(Header.IdComprobante, 0)
                LlenarDetalleImpuestos()
            End If
        End If

        If DbMode = DbModeType.update Then
            If dioClick = False Then
                Dim dt As DataTable = bl.com_ObtenerImpuestos(Header.IdComprobante, Header.TotalComprobante)

                Dim IdCompraImp As Decimal = 0.0
                For i = 0 To dt.Rows.Count - 1
                    IdCompraImp += dt.Rows(i).Item("IdCompra") ' para saber si detalle el impuesto
                Next

                If IdCompraImp = 0.0 Then ' si es modificacion y no tiene ningun impuesto
                    _dtImpuestos = bl.com_ObtenerImpuestos(Header.IdComprobante, 0)
                    dtImpuestos = bl.com_ObtenerImpuestos(Header.IdComprobante, 0)
                    LlenarDetalleImpuestos()
                End If
            End If
        End If

    End Sub
    Private Sub CargaCombos()
        objCombos.fac_FormasPago(leFormaPago)
        objCombos.inv_Bodegas(leBodega)
        objCombos.com_TiposCompra(leTipoCompra)
        objCombos.com_ClaseDocumento(leClaseDocumento)
        objCombos.com_OrigenesCompra(leOrigenCompra)
        objCombos.com_TiposComprobante(leTipoDocto)
        objCombos.Com_CajasChicaCompras(leCaja, "")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        objCombos.con_CentrosCosto(leCentroCosto, "", "")
    End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        sbDetallar.Enabled = Not Tipo
        gv.OptionsBehavior.Editable = Tipo
        teCorrelativo.Properties.ReadOnly = True

        teIva.Properties.ReadOnly = Not Tipo
        tePercepcionRetencion.Properties.ReadOnly = Not Tipo

        If Not Header Is Nothing Then
            btAplicar.Enabled = Not Header.AplicadaInventario
            btRevertir.Enabled = Header.AplicadaInventario
        End If

    End Sub

#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteRow(gv.FocusedRowHandle)
        CalcularTotales()
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "CantidadUG", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetFocusedRowCellValue("TipoImpuesto", 0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioReferencia", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PorcDescto", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescto", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorExento", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorAfecto", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuenta", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", "")
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gv.EditingValue = IdProd
                End If
                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("IdProducto", entProducto.IdProducto)
                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                    gv.SetFocusedRowCellValue("IdCuenta", entProducto.IdCuentaInv)
                    gv.SetFocusedRowCellValue("IdCentro", entProducto.IdCentro)
                End If
                gv.SetFocusedRowCellValue("TipoImpuesto", entProducto.EsExento)
            End If
        End If
    End Sub
    Private Sub riteIdCuenta_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles riteIdCuenta.Validating
        gv.EditingValue = SiEsNulo(gv.EditingValue, "")

        Dim entCuenta As con_Cuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, gv.ActiveEditor.EditValue) 'CORREGIR
        If Not entCuenta.EsTransaccional And entCuenta.IdCuenta <> "" Then
            MsgBox("La cuenta no permite aceptar transacciones" & Chr(13) & _
                   "Es de mayor o tiene subcuentas", 64, "Nota")
            e.Cancel = True
        End If

        gv.ActiveEditor.EditValue = entCuenta.IdCuenta
        lblNombreCuenta.Text = entCuenta.Nombre
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        CalcularTotales()
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        'SOLAMENTE SE VALIDAN VALORES OBLIGATORIOS QUE NO PUEDEN QUEDAR VACIOS AL TRATAR DE ABANDONAR LA FILA
        Dim IdProd As String = SiEsNulo(gv.GetFocusedRowCellValue("IdProducto"), "")
        If IdProd = "" Then
            e.Valid = False
            gv.SetColumnError(gv.Columns("IdProducto"), "Código de producto no válido o no existe")
        End If
        If SiEsNulo(gv.GetFocusedRowCellValue("IdCuenta"), "") = "" Then
            MsgBox("Debe de especificar la cuenta contable", MsgBoxStyle.Critical, "Nota")
            e.Valid = False
            gv.SetColumnError(gv.Columns("IdProducto"), "Debe especificar la cuenta contable")
            Exit Sub
        End If
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor

        Dim dCantidad As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
        Dim dPrecioReferencia As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioReferencia")
        Dim dPorcDescto As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PorcDescto")
        Dim dPrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")

        Dim dValorDescto, dValorExento, dValorAfecto As Decimal

        dValorDescto = 0.0
        dValorExento = 0.0
        dValorAfecto = 0.0

        Select Case gv.FocusedColumn.FieldName
            Case "IdProducto"
                entProducto = objTablas.inv_ProductosSelectByPK(SiEsNulo(e.Value, ""))
                gv.SetFocusedRowCellValue("TipoImpuesto", entProducto.EsExento)
            Case "Cantidad"
                dCantidad = e.Value
            Case "PorcDescto"
                dPorcDescto = e.Value
                'dPrecioUnitario = dPrecioReferencia
            Case "PrecioReferencia"
                dPrecioReferencia = e.Value
                If gv.GetFocusedRowCellValue("PrecioUnitario") = 0.0 Then
                    gv.SetFocusedRowCellValue("PrecioUnitario", dPrecioReferencia)
                End If
                'dPrecioUnitario = e.Value
            Case "PrecioUnitario"
                dPrecioUnitario = e.Value
        End Select

        Dim Descto As Decimal = Decimal.Round(dPrecioReferencia * dPorcDescto / 100, 4)
        If dPorcDescto > 0.0 Then
            dPrecioUnitario = dPrecioReferencia - Descto
        End If
        Dim Tipo As Integer = gv.GetFocusedRowCellValue("TipoImpuesto")
        If leTipoCompra.EditValue > 1 Or gv.GetFocusedRowCellValue("TipoImpuesto") = 1 Then 'EsExento = True Then
            dValorAfecto = 0.0
            dValorExento = Decimal.Round(dCantidad * dPrecioUnitario, 2)
            dValorDescto = Decimal.Round(Decimal.Round(dCantidad * dPrecioReferencia, 2) * dPorcDescto / 100, 2)
            dValorExento = dValorExento '- dValorDescto
        Else
            dValorExento = 0.0
            dValorAfecto = Decimal.Round(dCantidad * dPrecioUnitario, 2)
            dValorDescto = Decimal.Round(Decimal.Round(dCantidad * dPrecioReferencia, 2) * dPorcDescto / 100, 2)
            dValorAfecto = dValorAfecto '- dValorDescto
        End If

        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", dPrecioUnitario)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescto", dValorDescto)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorExento", dValorExento)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorAfecto", dValorAfecto)

        CalcularTotales()

    End Sub
    Private Sub CalcularTotales()
        gv.UpdateTotalSummary()
        If leTipoCompra.EditValue = 1 Then
            If ceExcluido.EditValue Then
                teIva.EditValue = 0.0
            Else
                If Not CambioIVA Then
                    teIva.EditValue = Decimal.Round(Me.gcValorAfecto.SummaryItem.SummaryValue * pnIVA, 2)
                End If
            End If
            'tePercepcionRetencion.EditValue = 0.0
            If rgTipo.EditValue > 1 And Me.gcValorAfecto.SummaryItem.SummaryValue > 100 Then
                If Not CambioPerRet Then
                    tePercepcionRetencion.EditValue = Decimal.Round(Me.gcValorAfecto.SummaryItem.SummaryValue * 1 / 100, 2)
                End If
            End If
        End If

        teTotal.EditValue = gcValorAfecto.SummaryItem.SummaryValue + gcValorExento.SummaryItem.SummaryValue + teIva.EditValue - TotalImpuestos
        teImpuestos.EditValue = TotalImpuestos

        If rgTipo.EditValue = 2 Then   'es una retención y el impuesto debe de restarse del documento
            teTotal.EditValue -= tePercepcionRetencion.EditValue
        End If
        If rgTipo.EditValue = 3 Then 'es una percepción y el impuesto debe de sumarse al documento
            teTotal.EditValue += tePercepcionRetencion.EditValue
        End If
    End Sub
#End Region

    Private Sub beProveedor_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beProveedor.ButtonClick
        beProveedor.EditValue = ""
        beProveedor_Validated(sender, New System.EventArgs)
    End Sub
    Private Sub beProveedor_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beProveedor.Validated
        Dim entProveedor As com_Proveedores = objConsultas.cnsProveedores(frmConsultas, beProveedor.EditValue)
        beProveedor.EditValue = entProveedor.IdProveedor
        teNombre.EditValue = entProveedor.Nombre
        'teDireccion.EditValue = entProveedor.Direccion
        'teGiro.EditValue = entProveedor.Giro
        teNRC.EditValue = entProveedor.Nrc
        teNIT.EditValue = entProveedor.Nit
        seDiasCredito.EditValue = entProveedor.DiasCredito
        teDocExcluido.EditValue = SiEsNulo(entProveedor.NumeroExcluido, "")
        If entProveedor.Tipo > 0 Then
            rgTipo.EditValue = entProveedor.Tipo
        Else
            rgTipo.EditValue = 1
        End If

    End Sub
    Private Sub teDocExcluido_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles teDocExcluido.LostFocus
        If gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
        End If
        gv.FocusedColumn = gv.Columns(0)
    End Sub
    Private Sub beOrden_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beOrden.Click
        If teOrdenCompra.Properties.ReadOnly = True Then
            Exit Sub
        End If
        Dim IdOrden As Integer = bl.com_ObtenerIdOrdenCompra(teOrdenCompra.EditValue)
        If IdOrden = 0 Then
            MsgBox("No se encontró ninguna orden con ése número", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If

        Dim entOC As com_OrdenesCompra = objTablas.com_OrdenesCompraSelectByPK(IdOrden)
        If Not entOC.Aprobada Then
            MsgBox("La orden de compra no ha sido aprobada", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If

        With entOC
            beProveedor.EditValue = .IdProveedor
            teNombre.EditValue = .Nombre
            teNRC.EditValue = .Nrc
            teNIT.EditValue = .Nit
            'teDireccion.EditValue = .Direccion
            deFechaCompra.EditValue = .Fecha
            deFechaContable.DateTime = .Fecha
            'teGiro.EditValue = .Giro
            leBodega.EditValue = .IdBodega
            leFormaPago.EditValue = .IdFormaPago
            leOrigenCompra.EditValue = .IdOrigen
            teTotal.EditValue = .Total
            teTotal.EditValue = .Total
            teIva.EditValue = .Iva
            seDiasCredito.EditValue = .DiasCredito
            'deFechaVto.EditValue = DateAdd(DateInterval.Day, Dias, .Fecha)
        End With
        gc.DataSource = bl.com_ObtenerOrdenCompraDetalle(IdOrden)
        CalcularTotales()
    End Sub
    Private Sub com_frmCompras_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        sbGuardar.Visible = False
        Me.Dispose()
    End Sub

    Private Sub sbDetallar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles sbDetallar.Click
        Dim SaldoProveedor As Decimal = 0.0, IdComprobante As Integer = 0
        Dim AbonoHeader As cpp_Abonos
        If leTipoDocto.EditValue = 2 And leFormaPago.EditValue = 2 Then
            IdComprobante = blCpp.cpp_ObtenerUltAbono(beProveedor.EditValue)
            AbonoHeader = objTablas.cpp_AbonosSelectByPK(IdComprobante)
            SaldoProveedor = blCpp.GetSaldoProveedor(beProveedor.EditValue, deFechaCompra.EditValue, leSucursal.EditValue)
            ''     If deFechaCompra.EditValue >= AbonoHeader.Fecha Then
            If teTotal.EditValue < SaldoProveedor Then
                    com_frmAplicacionNotaCredito.IdComprobante = teCorrelativo.EditValue
                    com_frmAplicacionNotaCredito.NumeroNota = teNumero.EditValue
                    com_frmAplicacionNotaCredito.FechaNota = deFechaCompra.EditValue
                    com_frmAplicacionNotaCredito.IdProveedor = beProveedor.EditValue
                    com_frmAplicacionNotaCredito.Nombre = teNombre.EditValue
                    com_frmAplicacionNotaCredito.MontoAbonar = teTotal.EditValue
                    com_frmAplicacionNotaCredito.ShowDialog()
                Else
                    MsgBox("El Valor de la nota de crédito excede el saldo del proveedor", MsgBoxStyle.Critical, "Nota")
                End If
            'Else
            '    MsgBox("La Fecha de la Nota de crédito no puede ser inferior al Ultimo Abono", MsgBoxStyle.Critical, "Nota")
            'End If
        End If
    End Sub
    Private _OrigenCompra As String = "Compras"
    Public Property OrigenCompra() As String
        Get
            Return _OrigenCompra
        End Get
        Set(ByVal value As String)
            _OrigenCompra = value
        End Set
    End Property

    Private Sub deFechaCompra_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deFechaCompra.EditValueChanged
        deFechaCompra.EditValue = deFechaCompra.EditValue
    End Sub

    Private Sub btAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAplicar.Click
        Header = objTablas.com_ComprasSelectByPK(teCorrelativo.EditValue)
        If Header Is Nothing Then
            Exit Sub
        End If
        If Header.AplicadaInventario Then
            MsgBox("Esta compra ya fue aplicada al inventario", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de aplicar al inventario ésta compra?" + Chr(13) + "YA NO SE PODRÁ EDITAR EL DOCUMENTO", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        Dim msj As String = blInve.inv_AplicarTransaccionInventario(Header.IdComprobante, "Compra", "I", Header.IdTipoComprobante, 0)
        If msj = "Ok" Then
            MsgBox("La compra ha sido aplicada al inventario. Ya no puede editarse", MsgBoxStyle.Information, "Nota")
            Header.AplicadaInventario = True
            btAplicar.Enabled = False
            btRevertir.Enabled = True
            If dtParam.Rows(0).Item("ContabilizarLineaCompras") Then
                'contabilizo la compra
                Dim res As String = bl.com_ContabilizarCompras(deFechaContable.EditValue, deFechaContable.EditValue, dtParam.Rows(0).Item("IdTipoPartidaCompras"), leSucursal.EditValue, objMenu.User, Header.IdComprobante, 1)
                If res = "Ok" Then
                    MsgBox("La contabilización se ha realizado con éxito", 64, "Nota")
                Else
                    MsgBox("La contabilización NO se pudo realizar" + Chr(13) + res, MsgBoxStyle.Critical, "Error de base de datos")
                End If
            End If


            Dim entCompras As com_Compras = objTablas.com_ComprasSelectByPK(Header.IdComprobante)
            teIdPartida.EditValue = entCompras.IdPartida

            xtcCompras.SelectedTabPage = xtpLista1
            gc2.DataSource = bl.com_ConsultaCompras(objMenu.User).Tables(0)
            gc2.Focus()
        Else
            MsgBox("Sucedió algún error al aplicar" + Chr(13) + msj + Chr(13) + "Favor reporte éste mensaje a ITO, S.A. DE C.V.", MsgBoxStyle.Critical, "Error")
        End If
    End Sub

    Private Sub btRevertir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRevertir.Click
        If Header Is Nothing Then
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de querer revertir ésta compra?" + Chr(13) + "ESTO PUEDE DESESTABILIZAR SUS INVENTARIOS", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        If Not Header.AplicadaInventario Then
            MsgBox("El documento NO ha sido aplicado aún", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim msj1 As String = "", lDummy As Boolean = False
        AutorizoReversion = ""
        For i = 0 To gv.DataRowCount - 1
            entProducto = objTablas.inv_ProductosSelectByPK(gv.GetRowCellValue(i, "IdProducto"))
            If entProducto.TipoProducto = 1 Then
                If blInve.inv_ObtieneExistenciaProducto(gv.GetRowCellValue(i, "IdProducto"), leBodega.EditValue, deFechaCompra.EditValue).Rows(0).Item("SaldoExistencias") - gv.GetRowCellValue(i, "Cantidad") < 0 Then
                    If msj1 = "" Then
                        msj1 = "Productos que resultaran con Saldo Negativos en Fecha " + Format(deFechaCompra.EditValue, "dd/MM/yyyy") + Chr(13)
                    End If
                    msj1 &= gv.GetRowCellValue(i, "IdProducto") + ","
                End If
            End If
        Next

        For i = 0 To gv.DataRowCount - 1
            entProducto = objTablas.inv_ProductosSelectByPK(gv.GetRowCellValue(i, "IdProducto"))
            If entProducto.TipoProducto = 1 Then
                If blInve.inv_ObtieneExistenciaProducto(gv.GetRowCellValue(i, "IdProducto"), leBodega.EditValue, Today).Rows(0).Item("SaldoExistencias") - gv.GetRowCellValue(i, "Cantidad") < 0 Then
                    If Not lDummy Then
                        msj1 &= Chr(13) + "Productos que resultaran con Saldo Negativos en Fecha " + Format(Today, "dd/MM/yyyy") + Chr(13)
                    End If
                    msj1 &= gv.GetRowCellValue(i, "IdProducto") + ","
                    lDummy = True
                End If
            End If
        Next

        If msj1 <> "" Then
            If MsgBox(msj1 + Chr(13) + "Desea Autorizar para continuar?", 292, "Confirme") = MsgBoxResult.No Then
                Exit Sub
            End If
            frmObtienePassword.Text = "AUTORIZACIÓN PARA REVERTIR INVENTARIOS"
            frmObtienePassword.Usuario = objMenu.User
            frmObtienePassword.TipoAcceso = 4
            frmObtienePassword.ShowDialog()
            If frmObtienePassword.Acceso = False Then
                MsgBox("Usuario no Autorizado para Revertir Inventarios", MsgBoxStyle.Critical, "Nota")
                Exit Sub
            End If
            AutorizoReversion = frmObtienePassword.Usuario
            frmObtienePassword.Dispose()
        End If

        Dim msj As String = blInve.inv_AplicarTransaccionInventario(Header.IdComprobante, "Compra", "D", Header.IdTipoComprobante, 0)
        If msj = "Ok" Then
            MsgBox("El documento de compra ha sido revertido con éxito" + Chr(13) + "YA PUEDE EDITARLA O ELIMINARLA", MsgBoxStyle.Information, "Nota")
            blInve.inv_AutorizoReversionInventario(AutorizoReversion, "Revertir Compra de ", Header.Numero, Header.IdComprobante)
            If dtParam.Rows(0).Item("ContabilizarLineaCompras") Then
                objTablas.con_PartidasDeleteByPK(Header.IdPartida) ' elimino la partida de compras
                teIdPartida.EditValue = 0
            End If
            Header.AplicadaInventario = False
            btAplicar.Enabled = True
            btRevertir.Enabled = False
        Else
            MsgBox("Sucedió algún error al intentar revertir" + Chr(13) + msj + Chr(13) + "Favor reporte éste mensaje a ITO, S.A. DE C.V.", MsgBoxStyle.Critical, "Error")
        End If
    End Sub

    Private Sub seDiasCredito_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles seDiasCredito.EditValueChanged
        deFechaVto.EditValue = DateAdd(DateInterval.Day, seDiasCredito.EditValue, deFechaCompra.EditValue)
    End Sub

    Private Sub deFechaVto_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deFechaVto.EditValueChanged
        seDiasCredito.EditValue = DateDiff(DateInterval.Day, deFechaCompra.EditValue, deFechaVto.EditValue)
    End Sub

    Private Sub sbImpuestos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbImpuestos.Click
        If teOrdenCompra.Properties.ReadOnly = True Then
            Exit Sub
        End If

        Dim frmImpuestos As New com_frmDetallarImpuestosCompras
        Me.AddOwnedForm(frmImpuestos)
        frmImpuestos.IdCompra = Header.IdComprobante
        frmImpuestos.TotalAfecto = Me.gcValorAfecto.SummaryItem.SummaryValue
        frmImpuestos.ShowDialog()
        LlenarDetalleImpuestos()
        dioClick = True
    End Sub

    Private Sub LlenarDetalleImpuestos()
        TotalImpuestos = 0.0
        ComprasImpuestos = New List(Of com_ComprasDetalleImpuestos)
        For i = 0 To _dtImpuestos.Rows.Count - 1
            Dim entDetalle As New com_ComprasDetalleImpuestos
            With entDetalle
                .IdCompra = _dtImpuestos.Rows(i).Item("IdCompra")
                .IdImpuesto = _dtImpuestos.Rows(i).Item("IdImpuesto")
                .Valor = _dtImpuestos.Rows(i).Item("Valor")
                TotalImpuestos += _dtImpuestos.Rows(i).Item("Valor")
                .IdDetalle = i + 1
            End With
            ComprasImpuestos.Add(entDetalle)
        Next
        CalculaOtrosImpuestos()
        CalcularTotales()
    End Sub

    Private Sub CalculaOtrosImpuestos()
        TotalImpuestos = 0.0
        For i = 0 To _dtImpuestos.Rows.Count - 1
            TotalImpuestos += _dtImpuestos.Rows(i).Item("Valor")
        Next
    End Sub

    Private Sub teIva_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles teIva.KeyDown
        If teTotal.EditValue <> 0.0 And e.KeyCode = Keys.Enter Then
            CambioIVA = True
            CalcularTotales()
        End If
    End Sub

    Private Sub tePercepcionRetencion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tePercepcionRetencion.KeyDown
        If teTotal.EditValue <> 0.0 And e.KeyCode = Keys.Enter Then
            CambioPerRet = True
            CalcularTotales()
        End If
    End Sub

    Private Sub leTipoDocto_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leTipoDocto.EditValueChanging
        If leTipoDocto.EditValue = 2 Then
            ceRebajaCosto.Visible = True
        Else
            ceRebajaCosto.Visible = False
        End If
    End Sub

    Private Sub CargaPantalla()
        If Not CopyCompra Then
            If DbMode <> DbModeType.cargar Then
                Exit Sub
            End If
        End If

        xtcCompras.SelectedTabPage = xtpDatos1
        teSerie.Focus()

        With Header
            teCorrelativo.EditValue = .IdComprobante
            leTipoDocto.EditValue = .IdTipoComprobante
            teSerie.EditValue = .Serie
            teNumero.EditValue = .Numero
            deFechaCompra.EditValue = .Fecha
            deFechaContable.DateTime = .FechaContable
            beProveedor.EditValue = .IdProveedor
            teNombre.EditValue = .Nombre
            teNRC.EditValue = .Nrc
            teNIT.EditValue = .Nit
            ceExcluido.Checked = .CompraExcluido
            teDocExcluido.EditValue = .NumDocExcluido

            leBodega.EditValue = .IdBodega
            leFormaPago.EditValue = .IdFormaPago

            teOrdenCompra.Text = .NumOrdenCompra
            leOrigenCompra.EditValue = .IdOrigenCompra
            rgTipo.EditValue = .TipoImpuestoAdicional
            leTipoCompra.EditValue = .IdTipoCompra
            teIva.EditValue = .TotalIva
            rgTipo.EditValue = .TipoImpuestoAdicional
            tePercepcionRetencion.EditValue = .TotalImpuesto1
            teImpuestos.EditValue = .TotalImpuesto2
            teTotal.EditValue = .TotalComprobante
            deFechaVto.EditValue = .FechaVencimiento
            seDiasCredito.EditValue = .DiasCredito
            ceExcluirLibroCompras.Checked = .ExcluirLibro
            leCaja.EditValue = .IdCaja
            ceRebajaCosto.EditValue = .AfectaCosto
            leSucursal.EditValue = .IdSucursal
            teIdPartida.EditValue = .IdPartida
            teIdCheque.EditValue = .IdCheque

            leClaseDocumento.EditValue = .Correlativo
            teResolucion.EditValue = .Giro
            teNumeroControlInternoFormularioUnico.EditValue = .Direccion


            If Not Header Is Nothing Then
                btAplicar.Enabled = Not Header.AplicadaInventario
                btRevertir.Enabled = Header.AplicadaInventario
            End If

            gc.DataSource = bl.com_ObtenerCompraDetalle(.IdComprobante)
            If Not DbMode = DbModeType.insert Then
                CambioIVA = teIva.EditValue <> Decimal.Round(Me.gcValorAfecto.SummaryItem.SummaryValue * pnIVA, 2)

                If rgTipo.EditValue > 1 And Me.gcValorAfecto.SummaryItem.SummaryValue > 100 Then
                    CambioPerRet = tePercepcionRetencion.EditValue <> Decimal.Round(Me.gcValorAfecto.SummaryItem.SummaryValue * 1 / 100, 2)
                End If
            End If

            If EntUsuario.CambiarPrecios = True Then
                deFechaCompra.Properties.ReadOnly = False
                deFechaContable.Properties.ReadOnly = False
            Else
                deFechaCompra.Properties.ReadOnly = True
                deFechaContable.Properties.ReadOnly = True
            End If

            _dtImpuestos = bl.com_ObtenerImpuestos(.IdComprobante, .TotalAfecto)
            LlenarDetalleImpuestos()
            CalculaOtrosImpuestos()
        End With

        CopyCompra = False
    End Sub

    Private Sub rgTipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgTipo.SelectedIndexChanged
        If rgTipo.EditValue = 1 Then
            tePercepcionRetencion.EditValue = 0.0
        End If
        CalcularTotales()
    End Sub


    Private Sub gv_DoubleClick(sender As Object, e As EventArgs) Handles gv.DoubleClick
        If teNombre.Properties.ReadOnly Then
            Exit Sub
        End If

        Dim IdCentro As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdCentro")), "")
        Dim frmSeleccionaCC As New frmDetallarCentroCosto
        Me.AddOwnedForm(frmSeleccionaCC)
        frmSeleccionaCC.IdCentro = IdCentro
        frmSeleccionaCC.ShowDialog()

        IdCentro = SiEsNulo(frmSeleccionaCC.IdCentro, "")
        gv.SetFocusedRowCellValue("IdCentro", IdCentro)
        frmSeleccionaCC.Dispose()
    End Sub

    Private Sub sbCopiaCompras_Click(sender As Object, e As EventArgs) Handles sbCopiaCompras.Click
        If teNombre.Properties.ReadOnly Then
            Exit Sub
        End If

        If DbMode = DbModeType.insert Then
        Else
            MsgBox("Esta función solo aplicara para compras nuevas", MsgBoxStyle.Information, "NOTA")
            Exit Sub
        End If

        Try
            teCorrelativo.EditValue = objConsultas.com_ConsultaCompras(frmConsultaDetalle)
            CopyCompra = True
            Header = objTablas.com_ComprasSelectByPK(teCorrelativo.EditValue)
            CargaPantalla()
            teCorrelativo.EditValue = 0
            teNumero.EditValue = ""
            teSerie.EditValue = ""
        Catch ex As Exception
            teCorrelativo.EditValue = 0
            CopyCompra = False
        End Try

    End Sub
End Class
