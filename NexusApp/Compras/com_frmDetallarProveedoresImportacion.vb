﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class com_frmDetallarProveedoresImportacion
    Dim dtDetalle As New DataTable
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim dtProveedoresImp As DataTable

    Private _IdImportacion As Integer
    Public Property IdImportacion() As Integer
        Get
            Return _IdImportacion
        End Get
        Set(ByVal value As Integer)
            _IdImportacion = value
        End Set
    End Property

    Private Sub com_frmDetallarGastosImportaciones_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.com_Proveedores(leIdProveedor)
        objCombos.fac_FormasPago(leFormaPago)

        LlenarGastosNuevo()

        gv.CancelUpdateCurrentRow()
        gv.AddNewRow()
    End Sub

    Private Sub LlenarEntidades()
        Dim frmImp As com_frmImportaciones = Me.Owner
        Dim dt As DataTable = frmImp.dtProveedoresImp

        For i = 0 To gv.DataRowCount - 1

            If gv.GetRowCellValue(i, "Valor") > 0.0 Then
                Dim dr As DataRow = frmImp.dtProveedoresImp.NewRow()

                dr("IdProveedor") = gv.GetRowCellValue(i, "IdProveedor")
                dr("IdFormaPago") = gv.GetRowCellValue(i, "IdFormaPago")
                dr("FechaContable") = gv.GetRowCellValue(i, "FechaContable")
                dr("Fecha") = gv.GetRowCellValue(i, "Fecha")
                dr("NumeroComprobante") = gv.GetRowCellValue(i, "NumeroComprobante")
                dr("IdComprobante") = gv.GetRowCellValue(i, "IdComprobante")
                dr("DiasCredito") = SiEsNulo(gv.GetRowCellValue(i, "DiasCredito"), 0)
                dr("Valor") = gv.GetRowCellValue(i, "Valor")
                dr("IdImportacion") = gv.GetRowCellValue(i, "IdImportacion")
                dr("Contabilizar") = SiEsNulo(gv.GetRowCellValue(i, "Contabilizar"), False)
                frmImp.dtProveedoresImp.Rows.Add(dr)
            End If
        Next

        Me.Close()
    End Sub

    Private Sub sbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbGuardar.Click
        LlenarEntidades()
    End Sub

    Private Sub LlenarGastosNuevo()
        Dim frmImp As com_frmImportaciones = Me.Owner
        Dim dt As DataTable = frmImp.dtProveedoresImp

        If dt.Rows.Count > 0 Then
            gc.DataSource = dt
        Else
            gc.DataSource = bl.com_ObtenerProveedoresImportacion(IdImportacion)
        End If

        ' se asigno este valor para el dt quede sin datos para que cuando se guardar aki lo rellene nuevamente
        ' solo es para que tenga la estructura de la tabla
        frmImp.dtProveedoresImp = bl.com_ObtenerProveedoresImportacion(-1)
    End Sub

#Region "Grid"

    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Fecha", Today)
        gv.SetRowCellValue(gv.FocusedRowHandle, "FechaContable", Today)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Valor", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "NumeroComprobante", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "DiasCredito", 0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Contabilizar", False)
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        Dim IdProveedor As String = SiEsNulo(gv.GetFocusedRowCellValue("IdProveedor"), "")
        Dim Numero As String = SiEsNulo(gv.GetFocusedRowCellValue("NumeroComprobante"), "")

        If IdProveedor = "" Then
            e.Valid = False
            gv.SetColumnError(gv.Columns("IdProveedor"), "Debe de especificar un proveedor")
        End If

        If Numero = "" Then
            e.Valid = False
            gv.SetColumnError(gv.Columns("NumeroComprobante"), "Debe de especificar el número de Docto.")
        End If
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub

#End Region

    Private Sub sbSalir_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub cmdBorrar_Click(sender As Object, e As EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub
End Class
