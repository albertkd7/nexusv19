<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class com_frmCargarComprobantesLiquidacion
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btAplicar = New DevExpress.XtraEditors.SimpleButton()
        Me.sbImportar = New DevExpress.XtraEditors.SimpleButton()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Numero = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcProveedor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.deFechaDet = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteClaseDocumentoCF = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteClaseDocumentoLQ = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LeCuentaBancaria = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.chkAbonar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.txtMonto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaDet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaDet.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteClaseDocumentoCF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteClaseDocumentoLQ, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LeCuentaBancaria, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAbonar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMonto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.GroupControl1.Controls.Add(Me.SimpleButton2)
        Me.GroupControl1.Controls.Add(Me.SimpleButton1)
        Me.GroupControl1.Controls.Add(Me.btAplicar)
        Me.GroupControl1.Controls.Add(Me.sbImportar)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1109, 54)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Comprobantes Liquidación"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.SimpleButton2.Location = New System.Drawing.Point(774, 25)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(65, 23)
        Me.SimpleButton2.TabIndex = 188
        Me.SimpleButton2.Text = "Salir"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Image = Global.Nexus.My.Resources.Resources.Delete
        Me.SimpleButton1.Location = New System.Drawing.Point(300, 23)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(36, 27)
        Me.SimpleButton1.TabIndex = 187
        '
        'btAplicar
        '
        Me.btAplicar.Image = Global.Nexus.My.Resources.Resources.autoriza3
        Me.btAplicar.Location = New System.Drawing.Point(170, 25)
        Me.btAplicar.Name = "btAplicar"
        Me.btAplicar.Size = New System.Drawing.Size(119, 23)
        Me.btAplicar.TabIndex = 186
        Me.btAplicar.Text = "Aplicar"
        '
        'sbImportar
        '
        Me.sbImportar.Image = Global.Nexus.My.Resources.Resources.bajar
        Me.sbImportar.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.sbImportar.Location = New System.Drawing.Point(21, 25)
        Me.sbImportar.Name = "sbImportar"
        Me.sbImportar.Size = New System.Drawing.Size(119, 23)
        Me.sbImportar.TabIndex = 185
        Me.sbImportar.Text = "Importar Archivo"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 54)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1, Me.chkAbonar, Me.txtMonto, Me.deFechaDet, Me.LeCuentaBancaria, Me.riteClaseDocumentoLQ, Me.riteClaseDocumentoCF})
        Me.gc.Size = New System.Drawing.Size(1109, 455)
        Me.gc.TabIndex = 7
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Numero, Me.gcProveedor, Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn15, Me.GridColumn14, Me.GridColumn13, Me.GridColumn6, Me.GridColumn12, Me.GridColumn11, Me.GridColumn10, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'Numero
        '
        Me.Numero.Caption = "Numero LQ."
        Me.Numero.FieldName = "Numero"
        Me.Numero.Name = "Numero"
        Me.Numero.Visible = True
        Me.Numero.VisibleIndex = 3
        Me.Numero.Width = 102
        '
        'gcProveedor
        '
        Me.gcProveedor.Caption = "Proveedor"
        Me.gcProveedor.FieldName = "Nombre"
        Me.gcProveedor.Name = "gcProveedor"
        Me.gcProveedor.Visible = True
        Me.gcProveedor.VisibleIndex = 2
        Me.gcProveedor.Width = 292
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Total Comprobante"
        Me.GridColumn1.FieldName = "TotalComprobante"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.AllowFocus = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 10
        Me.GridColumn1.Width = 102
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Fecha"
        Me.GridColumn2.ColumnEdit = Me.deFechaDet
        Me.GridColumn2.FieldName = "Fecha"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        Me.GridColumn2.Width = 59
        '
        'deFechaDet
        '
        Me.deFechaDet.AutoHeight = False
        Me.deFechaDet.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaDet.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaDet.Name = "deFechaDet"
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "NRC"
        Me.GridColumn3.FieldName = "Nrc"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        Me.GridColumn3.Width = 69
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Serie C.F."
        Me.GridColumn4.FieldName = "SerieCF"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 6
        Me.GridColumn4.Width = 67
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Numero C.F."
        Me.GridColumn5.FieldName = "NumeroCF"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 5
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "Resolución CF"
        Me.GridColumn15.FieldName = "ResolucionCF"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 7
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "No. Control InternoCF"
        Me.GridColumn14.FieldName = "NumControlInternoCF"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 8
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Clase Documento"
        Me.GridColumn13.ColumnEdit = Me.riteClaseDocumentoCF
        Me.GridColumn13.FieldName = "ClaseDocumentoCF"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 9
        '
        'riteClaseDocumentoCF
        '
        Me.riteClaseDocumentoCF.AutoHeight = False
        Me.riteClaseDocumentoCF.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteClaseDocumentoCF.Name = "riteClaseDocumentoCF"
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Serie L.Q."
        Me.GridColumn6.FieldName = "Serie"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 4
        Me.GridColumn6.Width = 71
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Resolución LQ"
        Me.GridColumn12.FieldName = "Resolucion"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 11
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "No. Control Interno"
        Me.GridColumn11.FieldName = "NumControlInterno"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 12
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Clase Documento"
        Me.GridColumn10.ColumnEdit = Me.riteClaseDocumentoLQ
        Me.GridColumn10.FieldName = "ClaseDocumento"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 13
        '
        'riteClaseDocumentoLQ
        '
        Me.riteClaseDocumentoLQ.AutoHeight = False
        Me.riteClaseDocumentoLQ.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteClaseDocumentoLQ.Name = "riteClaseDocumentoLQ"
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Comision"
        Me.GridColumn7.FieldName = "Comision"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 14
        Me.GridColumn7.Width = 52
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Percepción"
        Me.GridColumn8.DisplayFormat.FormatString = "n2"
        Me.GridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.GridColumn8.FieldName = "TotalIva"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 15
        Me.GridColumn8.Width = 50
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Cuenta Bancaria"
        Me.GridColumn9.ColumnEdit = Me.LeCuentaBancaria
        Me.GridColumn9.FieldName = "IdCuentaBancaria"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 16
        Me.GridColumn9.Width = 114
        '
        'LeCuentaBancaria
        '
        Me.LeCuentaBancaria.AutoHeight = False
        Me.LeCuentaBancaria.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LeCuentaBancaria.Name = "LeCuentaBancaria"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'chkAbonar
        '
        Me.chkAbonar.AutoHeight = False
        Me.chkAbonar.Name = "chkAbonar"
        '
        'txtMonto
        '
        Me.txtMonto.AutoHeight = False
        Me.txtMonto.Mask.EditMask = "n2"
        Me.txtMonto.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtMonto.Mask.UseMaskAsDisplayFormat = True
        Me.txtMonto.Name = "txtMonto"
        '
        'com_frmCargarComprobantesLiquidacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1109, 509)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "com_frmCargarComprobantesLiquidacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Comprobantes Liquidación"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaDet.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaDet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteClaseDocumentoCF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteClaseDocumentoLQ, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LeCuentaBancaria, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAbonar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMonto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents chkAbonar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents Numero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcProveedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtMonto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents deFechaDet As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LeCuentaBancaria As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents sbImportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btAplicar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteClaseDocumentoCF As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteClaseDocumentoLQ As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
End Class
