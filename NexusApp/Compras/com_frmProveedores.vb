﻿
Imports NexusELL.TableEntities
Imports NexusBLL

Public Class com_frmProveedores
    Dim entProveedor As com_Proveedores
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim IdProv As String

    Private Sub acf_frmActivos_RefreshConsulta() Handles Me.RefreshConsulta
        gc.DataSource = objTablas.com_ProveedoresSelectAll
        gv.BestFitColumns()
    End Sub

    Private Sub com_frmProveedores_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.admDepartamentos(leDepto)
        objCombos.admMunicipios(leMunicipio, leDepto.EditValue)
        objCombos.com_OrigenProveedor(leOrigen)
        objCombos.adm_pais(LePaisOrigen)
        gc.DataSource = objTablas.com_ProveedoresSelectAll
      
        If sbGuardar.Visible Then
            com_frmProveedores_Nuevo()
            DbMode = DbModeType.insert
        Else
            ActivaControles(False)
            MostrarModoInicial()
            'CargaControles(dn.Position)
        End If

    End Sub
    Private Sub com_frmProveedores_Nuevo() Handles Me.Nuevo
        IdProv = "-1x" 'código tonto para que limpie la pantalla
        CargaControles(-1)
        ActivaControles(True)

        xtcProveedores.SelectedTabPage = xtpDatos
        teIdProveedor.Focus()
    End Sub
    Private Sub com_frmProveedores_Guardar() Handles Me.Guardar
        CargaEntidad()
        If Not DatosValidos() Then
            Exit Sub
        End If
        Try
            If DbMode = DbModeType.insert Then
                objTablas.com_ProveedoresInsert(entProveedor)
            Else
                objTablas.com_ProveedoresUpdate(entProveedor)
            End If
        Catch ex As Exception
            MsgBox("No fue posible guardar el proveedor" & Chr(13) & "Reporte el siguiente mensaje al personal de Nexus" & Chr(13) & ex.Message)
        End Try

        If sbGuardar.Visible Then  'EL DOCUMENTO SE ESTÁ INGRESANDO DESDE OTRO MODULO
            Dispose()
        Else
            xtcProveedores.SelectedTabPage = xtpLista
            gc.DataSource = objTablas.com_ProveedoresSelectAll()
            gc.Focus()
            MostrarModoInicial()
            ActivaControles(False)
        End If

    End Sub
    Private Sub com_frmProveedores_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub com_frmProveedores_Consulta() Handles Me.Consulta
        'entProveedor = objConsultas.cnsProveedores(frmConsultas, "")
        'IdProv = entProveedor.IdProveedor
        'CargaControles(-1)
    End Sub

    Private Sub com_frmProveedores_Eliminar() Handles Me.Eliminar
        Try
            If MsgBox("Está seguro(a) de eliminar a éste proveedor?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then

                If xtcProveedores.SelectedTabPage.Name = "xtpLista" Then
                    entProveedor = objTablas.com_ProveedoresSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProveedor"))
                Else
                    entProveedor = objTablas.com_ProveedoresSelectByPK(teIdProveedor.EditValue)
                End If
                objTablas.com_ProveedoresDeleteByPK(entProveedor.IdProveedor)

                xtcProveedores.SelectedTabPage = xtpLista
                gc.DataSource = objTablas.com_ProveedoresSelectAll()
                gc.Focus()
            End If

        Catch ex As Exception
            MsgBox("SE DETECTÓ UN ERROR AL TRATAR DE ELIMINAR:" + Chr(13) + ex.Message(), MsgBoxStyle.Critical, "Error")
        End Try
    End Sub
    Private Sub com_frmProveedores_Cancelar() Handles Me.Revertir
        xtcProveedores.SelectedTabPage = xtpLista
        gc.Focus()
        ActivaControles(False)
    End Sub
    Private Sub com_frmProveedores_Reporte() Handles Me.Reporte

        Dim rpt As New cpp_rptListProveedores
        rpt.DataSource = objTablas.com_ProveedoresSelectAll
        rpt.DataMember = ""
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "LISTADO DE PROVEEDORES"
        rpt.ShowPreviewDialog()
    End Sub
    
    Public Sub CargaControles(ByVal Position As Integer)
        entProveedor = objTablas.com_ProveedoresSelectByPK(IdProv)
        CargarEnPatalla()
    End Sub

    Private Sub gc_DoubleClick(sender As Object, e As EventArgs) Handles gc.DoubleClick
        entProveedor = objTablas.com_ProveedoresSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProveedor"))
        xtcProveedores.SelectedTabPage = xtpDatos
        teIdProveedor.Focus()
        CargarEnPatalla()
        ActivaControles(False)
    End Sub

    Private Sub CargarEnPatalla()
        With entProveedor
            teIdProveedor.EditValue = .IdProveedor
            teNombre.EditValue = .Nombre
            teNRC.EditValue = .Nrc
            teNIT.EditValue = .Nit
            teGiro.EditValue = .Giro
            teTelefonos.EditValue = .Telefonos
            leDepto.EditValue = .IdDepartamento
            leMunicipio.EditValue = .IdMunicipio
            teFax.EditValue = .NumeroFax
            teDireccion.EditValue = .Direccion
            teCorreoElectronico.EditValue = .CorreoElectronico
            ceAplicaPerRet.Checked = .AplicaRetencion
            BeCtaContable1.beIdCuenta.EditValue = .IdCuentaContable
            leMunicipio.EditValue = .IdMunicipio
            teContacto1.EditValue = .Contacto1
            teContacto2.EditValue = .Contacto2
            teContacto3.EditValue = .Contacto3
            teNacionalidad.EditValue = .Nacionalidad
            rgTipo.EditValue = .Tipo
            leOrigen.EditValue = .IdOrigen

            teSujeto.EditValue = .NumeroExcluido
            teNumCasa.EditValue = .NumCasa
            teApartamentoLocal.EditValue = .ApartamentoLocal
            teOtrosDatosDomicilio.EditValue = .OtrosDomicilio
            teColoniaBarrio.EditValue = .ColoniaBarrio
            LePaisOrigen.EditValue = .CodPais
            teRepresentante.EditValue = .RepresentanteLegal
        End With
    End Sub

    Private Sub CargaEntidad()
        With entProveedor
            .IdProveedor = teIdProveedor.EditValue
            .Nombre = teNombre.EditValue
            .Nrc = teNRC.EditValue
            .Nit = teNIT.EditValue
            .Giro = teGiro.EditValue
            .Telefonos = teTelefonos.EditValue
            .NumeroFax = teFax.EditValue
            .Direccion = teDireccion.EditValue
            .CorreoElectronico = teCorreoElectronico.EditValue
            .AplicaRetencion = ceAplicaPerRet.Checked
            .IdCuentaContable = BeCtaContable1.beIdCuenta.EditValue
            .LimiteCredito = seLimite.EditValue
            .DiasCredito = seDiasCredito.EditValue
            .IdDepartamento = leDepto.EditValue
            .IdMunicipio = leMunicipio.EditValue
            .Contacto1 = teContacto1.EditValue
            .Contacto2 = teContacto2.EditValue
            .Contacto3 = teContacto3.EditValue
            .Nacionalidad = teNacionalidad.EditValue
            .Tipo = rgTipo.EditValue
            .IdOrigen = leOrigen.EditValue
            .NumeroExcluido = teSujeto.EditValue
            .NumCasa = teNumCasa.EditValue
            .ApartamentoLocal = teApartamentoLocal.EditValue
            .OtrosDomicilio = teOtrosDatosDomicilio.EditValue
            .ColoniaBarrio = teColoniaBarrio.EditValue
            .CodPais = LePaisOrigen.EditValue
            .RepresentanteLegal = teRepresentante.EditValue

            If DbMode = DbModeType.insert Then
                .FechaHoraCreacion = Now
                .CreadoPor = objMenu.User
                .ModificadoPor = ""
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If
        End With
    End Sub
    Private Sub leDepto_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leDepto.EditValueChanged
        objCombos.admMunicipios(leMunicipio, leDepto.EditValue)
    End Sub
    Private Function DatosValidos()
        If BeCtaContable1.beIdCuenta.EditValue = "" Then
            MsgBox("Debe de especificar la cuenta contable", MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If
        If leDepto.EditValue = "" Then
            MsgBox("Debe de especificar el departamento y municipio", MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If

        Return True
    End Function
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In xtpDatos.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
    End Sub


    Private Sub com_frmCompras_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        sbGuardar.Visible = False
        Me.Dispose()
    End Sub

    Private Sub rgTipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rgTipo.SelectedIndexChanged
        If rgTipo.EditValue = 3 Then
            ceAplicaPerRet.Text = "NOS APLICA PERCEPCIÓN AL COMPRARLE"
        Else
            ceAplicaPerRet.Text = "LE APLICAMOS RETENCIÓN AL COMPRARLE"
        End If
    End Sub

End Class