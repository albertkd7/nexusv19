﻿
Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Imports System.IO
Public Class com_frmOrdenesCompra
    Dim Header As com_OrdenesCompra
    Dim Detalle As List(Of com_OrdenesCompraDetalle)
    Dim DetallePago As List(Of com_OrdenesCompraDetallePago)
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim entProducto As inv_Productos
    Dim _dtRequisicion As DataTable, entUsuarios As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()

    Property dtRequisicion() As DataTable
        Get
            Return _dtRequisicion
        End Get
        Set(ByVal value As DataTable)
            _dtRequisicion = value
        End Set
    End Property

    Private Sub com_frmOrdenesCompra_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = bl.com_ConsultaOrdenesCompra(objMenu.User).Tables(0)
        gv2.BestFitColumns()
    End Sub

    Private Sub com_frmOrdenesCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargaCombos()
        If dtParam.Rows(0).Item("TipoContribuyente") = 3 Then   'es una retención y el impuesto debe de restarse del documento
            lblPerRet.Text = "Retención:"
            cePercepcionRetencion.Text = "APLICAR RETENCIÓN"
        End If
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("COM_ORDENESCOMPRA", "IdComprobante")
        ActivarControles(False)
        gc2.DataSource = bl.com_ConsultaOrdenesCompra(objMenu.User).Tables(0)
    End Sub
    Private Sub com_frmOrdenesCompra_Nuevo() Handles Me.Nuevo

        ActivarControles(True)
        Header = New com_OrdenesCompra
        gc.DataSource = bl.com_ObtenerOrdenCompraDetalle(-1)
        gcFormaPago.DataSource = bl.com_ObtenerOrdenCompraFormaPago(-1)

        gv.CancelUpdateCurrentRow()
        gvFormaPago.CancelUpdateCurrentRow()
        gv.AddNewRow()
        gvFormaPago.AddNewRow()

        'gv.FocusedColumn = gv.Columns(0)
        beProveedor.EditValue = ""
        teNumero.EditValue = ""
        deFechaCompra.EditValue = Today
        leFormaPago.EditValue = 1
        seDiasCredito.EditValue = 0
        teDireccion.EditValue = ""
        teNombre.EditValue = ""
        teGiro.EditValue = ""
        teNIT.EditValue = ""
        teNRC.EditValue = ""
        teComentario.EditValue = ""
        tePercepcionRetencion.EditValue = 0.0
        leOrigenCompra.EditValue = 1
        leTipoCompra.EditValue = 1
        leSucursal.EditValue = piIdSucursalUsuario
        meCartaAduana.EditValue = ""
        teFacturaAduana.EditValue = ""

        teEnvioFac.EditValue = dtParam.Rows(0).Item("NombreEmpresa")
        teEmailEnvioFac.EditValue = ""
        teDireccionEnvioFac.EditValue = dtParam.Rows(0).Item("Direccion")
        teDireccionEntrega.EditValue = dtParam.Rows(0).Item("Direccion")
        teLugarEntrega.EditValue = dtParam.Rows(0).Item("NombreEmpresa")
        teSolicitadoPor.EditValue = entUsuarios.Nombre + " " + entUsuarios.Apellidos

        teNumero.Focus()
        xtcOrdenCompra.SelectedTabPage = xtpDatos
    End Sub
    Private Sub com_frmOrdenesCompra_Guardar() Handles Me.Guardar
        If Not DatosValidos() Then
            Exit Sub
        End If
        CargaEntidades()

        If DbMode = DbModeType.insert Then
            If bl.com_InsertaOrdenCompra(Header, Detalle, DetallePago) Then
                MsgBox("El documento ha sido guardado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("No fue posible actualizar el documento", MsgBoxStyle.Critical)
                Return
            End If
        Else
            If bl.com_ActualizaOrdenCompra(Header, Detalle, DetallePago) Then
                MsgBox("El documento ha sido actualizado con éxito", MsgBoxStyle.Information)
            Else
                MsgBox("No fue posible actualizar el documento", MsgBoxStyle.Critical)
                Return
            End If
        End If
        teNumero.EditValue = Header.Numero
        xtcOrdenCompra.SelectedTabPage = xtpLista
        gc2.DataSource = bl.com_ConsultaOrdenesCompra(objMenu.User).Tables(0)
        gc2.Focus()

        teCorrelativo.EditValue = Header.IdComprobante
        MostrarModoInicial()
        teCorrelativo.Focus()
        ActivarControles(False)
    End Sub
    Private Sub com_frmOrdenesCompra_Editar() Handles Me.Editar
        If gv2.GetFocusedRowCellValue("IdComprobante") > 0 Then
        Else
            MsgBox("No ha seleccionado ninguna orden", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        Header = objTablas.com_OrdenesCompraSelectByPK(gv2.GetFocusedRowCellValue("IdComprobante"))
        If Header.Aprobada Then
            MsgBox("No puede editar ésta orden de compra." + Chr(13) + "Ya fue aprobada", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If

        'DbMode = DbModeType.update
        ActivarControles(True)
        CargaPantalla()
    End Sub

    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        Header = objTablas.com_OrdenesCompraSelectByPK(gv2.GetFocusedRowCellValue("IdComprobante"))
        CargaPantalla()
    End Sub
    Private Sub com_frmOrdenesCompra_Consulta() Handles Me.Consulta
        'teCorrelativo.EditValue = objConsultas.com_ConsultaOrdenesCompra(frmConsultaDetalle)
        'CargaControles(0)
    End Sub
    Private Sub com_frmOrdenesCompra_Cancelar() Handles Me.Revertir
        xtcOrdenCompra.SelectedTabPage = xtpLista
        ActivarControles(False)
        gc2.Focus()
    End Sub
    Private Sub com_frmOrdenesCompra_Eliminar() Handles Me.Eliminar
        Header = objTablas.com_OrdenesCompraSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        If Header.Aprobada Then
            MsgBox("No puede eliminar ésta transacción." + Chr(13) + "Ya fue Aprobada", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If

        If MsgBox("Está seguro(a) de eliminar ésta orden de compra?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            objTablas.com_OrdenesCompraDeleteByPK(Header.IdComprobante)
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("COM_ORDENESCOMPRA", "IdComprobante")
            ActivarControles(False)
            gc2.DataSource = bl.com_ConsultaOrdenesCompra(objMenu.User).Tables(0)
        End If
    End Sub
    Private Sub com_frmOrdenesCompra_Reporte() Handles Me.Reporte
        Header = objTablas.com_OrdenesCompraSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Dim dt As DataTable = bl.com_ObtenerOrdenCompra(Header.IdComprobante, 1)
        Dim CountReg As Integer
        Dim entProveedor As com_Proveedores = objTablas.com_ProveedoresSelectByPK(Header.IdProveedor)

        Dim rpt As New com_rptOrdenCompra() 'With {.DataSource = dt, .DataMember = ""}
        Dim Template = Application.StartupPath & "\Plantillas\OrdenCompra.repx"

        If FileIO.FileSystem.FileExists(Template) Then
            rpt.LoadLayout(Template)
        End If

        Dim dtPago As DataTable = bl.com_ObtenerOrdenCompraFormaPago(Header.IdComprobante)
        rpt.DataSource = dt
        rpt.DataMember = ""

        If dtPago.Rows.Count > 0 Then
            rpt.XrSubreport1.Visible = True
            rpt.XrSubreport1.ReportSource.DataSource = dtPago
            rpt.XrSubreport1.ReportSource.DataMember = ""
        Else
            rpt.XrSubreport1.Visible = False
            rpt.XrSubreport1.ReportSource.DataSource = Nothing
            rpt.XrSubreport1.ReportSource.DataMember = ""
        End If

        'rpt.xrLogo.Image = ByteToImagen(dtParam.Rows(0).Item("LogoEmpresa"))
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlDireccionEmpresa.Text = dtParam.Rows(0).Item("Direccion")
        rpt.xrlNitEmpresa.Text = dtParam.Rows(0).Item("NitEmpresa")
        rpt.xrlNrcEmpresa.Text = dtParam.Rows(0).Item("NrcEmpresa")
        rpt.xrlGiroEmpresa.Text = dtParam.Rows(0).Item("ActividadEconomica")

        rpt.xrlEmail.Text = Header.EmailEnvioFactura
        rpt.xrlEnviarFactura.Text = Header.EnviarFacturaA
        rpt.xrlDirecionEnvioFactura.Text = Header.DireccionEnvioFactura
        rpt.xrlSolicitadoPor.Text = Header.SolicitadoPor
        rpt.xrlLugarEntrega.Text = Header.LugarEntrega
        rpt.xrlDireccionEntrega.Text = Header.DireccionEntrega
        rpt.xrlAutorizado.Text = Header.AprobadaPor

        rpt.xrlNotaPiePagina.Text = ""
        If CountReg = 0 And Convert.ToDateTime(entProveedor.FechaHoraCreacion).Date > Date.ParseExact("20180414", "yyyyMMdd", Nothing) Then
            rpt.xrlNotaPiePagina.Text = "<< NUEVO PROVEEDOR >>"
        End If

        rpt.ShowPreviewDialog()
    End Sub

    Function DatosValidos() As Boolean
        Dim msje As String = ""
        If beProveedor.EditValue = "" Then
            msje = "Debe de especificar el código del proveedor"
        End If
        If teNRC.EditValue = "" Or teNIT.EditValue = "" Then
            msje = "Debe de especificar el Nit y NRC del proveedor"
        End If

        gvFormaPago.UpdateTotalSummary()
        If Me.gcValorPago.SummaryItem.SummaryValue <> teTotal.EditValue And Me.gcValorPago.SummaryItem.SummaryValue > 0.0 Then
            msje = "El valor del detalle de pago es diferente al total de la orden"
        End If

        If msje <> "" Then
            MsgBox(msje, MsgBoxStyle.Critical, "Error de usuario")
            Return False
        End If


        Return True
    End Function
    Private Sub CargaEntidades()
        With Header
            .DiasCredito = seDiasCredito.EditValue
            .Direccion = teDireccion.Text
            .Fecha = deFechaCompra.EditValue
            .Giro = teGiro.Text
            .IdBodega = 1
            .IdFormaPago = leFormaPago.EditValue
            .IdProveedor = beProveedor.EditValue
            .Nit = teNIT.Text
            .Nombre = teNombre.Text
            .Nrc = teNRC.EditValue
            .Numero = teNumero.EditValue
            .IdOrigen = leOrigenCompra.EditValue
            .IdTipoCompra = leTipoCompra.EditValue
            .ComentarioAprobacion = teComentario.EditValue
            .TotalImpuesto1 = tePercepcionRetencion.EditValue
            .TotalImpuesto2 = 0.0
            .Iva = teIva.EditValue
            .Total = teTotal.EditValue


            .EnviarFacturaA = teEnvioFac.EditValue
            .EmailEnvioFactura = teEmailEnvioFac.EditValue
            .DireccionEnvioFactura = teDireccionEnvioFac.EditValue
            .DireccionEntrega = teDireccionEntrega.EditValue
            .LugarEntrega = teLugarEntrega.EditValue
            .SolicitadoPor = teSolicitadoPor.EditValue
            .IdSucursal = leSucursal.EditValue
            .FacturaAduana = teFacturaAduana.EditValue
            .CartaAduana = meCartaAduana.EditValue
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
            Else
                .FechaHoraModificacion = Now
                .ModificadoPor = objMenu.User
            End If
        End With

        Detalle = New List(Of com_OrdenesCompraDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New com_OrdenesCompraDetalle
            With entDetalle
                .IdComprobante = 0  ' Se asigna en la capa de datos
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .PrecioReferencia = gv.GetRowCellValue(i, "PrecioReferencia")
                .PorcDescto = gv.GetRowCellValue(i, "PorcDescto")
                .ValorDescto = gv.GetRowCellValue(i, "ValorDescto")
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .ValorExento = gv.GetRowCellValue(i, "ValorExento")
                .ValorAfecto = gv.GetRowCellValue(i, "ValorAfecto")
                .IdDetalle = i + 1
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
            End With
            Detalle.Add(entDetalle)
        Next

        gvFormaPago.UpdateTotalSummary()
        DetallePago = New List(Of com_OrdenesCompraDetallePago)
        For i = 0 To gvFormaPago.DataRowCount - 1
            Dim entDetallePago As New com_OrdenesCompraDetallePago
            With entDetallePago
                .IdComprobante = 0  ' Se asigna en la capa de datos
                .Fecha = gvFormaPago.GetRowCellValue(i, "Fecha")
                .Descripcion = gvFormaPago.GetRowCellValue(i, "Descripcion")
                .Valor = gvFormaPago.GetRowCellValue(i, "Valor")
                .IdDetalle = i + 1
            End With
            DetallePago.Add(entDetallePago)
        Next
    End Sub
    Private Sub CargaCombos()
        objCombos.fac_FormasPago(leFormaPago)
        objCombos.com_TiposCompra(leTipoCompra)
        objCombos.com_OrigenesCompra(leOrigenCompra)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")

    End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        For Each ctrl In GroupControl1.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
        gvFormaPago.OptionsBehavior.Editable = Tipo
    End Sub

#Region "Grid"
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteRow(gv.FocusedRowHandle)
        CalcularTotales()
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdProducto" Then
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                    gv.EditingValue = IdProd
                End If
                entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
                If entProducto.IdProducto = "" Then
                    MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                End If
            End If
        End If

    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioReferencia", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PorcDescto", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescto", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorExento", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorAfecto", 0.0)
    End Sub
    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv.RowUpdated
        CalcularTotales()
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        If SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdProducto"), "") = "" Then
            e.Valid = False
        End If
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Dim dCantidad As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")
        Dim dPrecioReferencia As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioReferencia")
        Dim dPorcDescto As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PorcDescto")
        Dim dPrecioUnitario As Decimal = gv.GetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario")

        Dim dValorDescto, dValorExento, dValorAfecto As Decimal

        dValorDescto = 0.0
        dValorExento = 0.0
        dValorAfecto = 0.0

        Dim iTipoCompra As Integer = leTipoCompra.EditValue

        Select Case gv.FocusedColumn.FieldName
            Case "Cantidad"
                dCantidad = e.Value
            Case "PorcDescto"
                dPorcDescto = e.Value
                'dPrecioUnitario = dPrecioReferencia
            Case "PrecioUnitario"
                dPrecioUnitario = e.Value
                'dPrecioReferencia = dPrecioUnitario
            Case "PrecioReferencia"
                dPrecioReferencia = e.Value
        End Select
        'If entProducto.EsExento Then  'pendiente de revisar éste codigo
        '    iTipoCompra = 2
        'End If
        Dim Descto As Decimal = Decimal.Round(dPrecioReferencia * dPorcDescto / 100, 4)
        If dPorcDescto > 0.0 Then
            dPrecioUnitario = dPrecioReferencia - Descto
        End If

        If iTipoCompra = 1 Then
            dValorExento = 0.0
            dValorAfecto = Decimal.Round(dCantidad * dPrecioUnitario, 2)
            dValorDescto = Decimal.Round(Decimal.Round(dCantidad * dPrecioReferencia, 2) * dPorcDescto / 100, 2)
            dValorAfecto = dValorAfecto '- dValorDescto
        Else
            dValorExento = Decimal.Round(dCantidad * dPrecioUnitario, 2)
            dValorDescto = Decimal.Round(Decimal.Round(dCantidad * dPrecioReferencia, 2) * dPorcDescto / 100, 2)
            dValorExento = dValorExento '- dValorDescto
        End If
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", dPrecioUnitario)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDescto", dValorDescto)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorExento", dValorExento)
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorAfecto", dValorAfecto)

        CalcularTotales()

    End Sub
    Private Sub CalcularTotales()
        tePercepcionRetencion.EditValue = 0.0
        gv.UpdateTotalSummary()
        gvFormaPago.UpdateTotalSummary()
        teSubTotal.EditValue = Me.gcValorAfecto.SummaryItem.SummaryValue + Me.gcValorExento.SummaryItem.SummaryValue
        If cePercepcionRetencion.EditValue And Me.gcValorAfecto.SummaryItem.SummaryValue > 100 Then
            tePercepcionRetencion.EditValue = Decimal.Round(Me.gcValorAfecto.SummaryItem.SummaryValue * 1 / 100, 2)
        End If
        teIva.EditValue = Decimal.Round(Me.gcValorAfecto.SummaryItem.SummaryValue * pnIVA, 2)
        teTotal.EditValue = teSubTotal.EditValue + teIva.EditValue

        If dtParam.Rows(0).Item("TipoContribuyente") = 3 Then   'es una retención y el impuesto debe de restarse del documento
            teTotal.EditValue -= tePercepcionRetencion.EditValue
        Else 'es una percepción y el impuesto debe de sumarse al documento
            teTotal.EditValue += tePercepcionRetencion.EditValue
        End If

    End Sub
#End Region
    'Private Sub cmdCrearProducto_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    inv_frmProductos.ShowDialog()
    'End Sub

    Private Sub beProveedor_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beProveedor.ButtonClick
        beProveedor.EditValue = ""
        beProveedor_Validated(sender, New System.EventArgs)
    End Sub

    Private Sub beProveedor_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beProveedor.Validated
        Dim entProveedor As com_Proveedores = objConsultas.cnsProveedores(frmConsultas, beProveedor.EditValue)
        beProveedor.EditValue = entProveedor.IdProveedor
        teNombre.EditValue = entProveedor.Nombre
        teDireccion.EditValue = entProveedor.Direccion
        teGiro.EditValue = entProveedor.Giro
        teNRC.EditValue = entProveedor.Nrc
        teNIT.EditValue = entProveedor.Nit
        seDiasCredito.EditValue = 30
        cePercepcionRetencion.EditValue = entProveedor.AplicaRetencion

        Dim CountReg As Integer = bl.ConteoOrdenesCompra(entProveedor.IdProveedor)
        If CountReg = 0 And Convert.ToDateTime(entProveedor.FechaHoraCreacion).Date > Date.ParseExact("20180414", "yyyyMMdd", Nothing) Then
            lbMensaje.Text = "<< NUEVO PROVEEDOR >>"
        Else
            lbMensaje.Text = "."
        End If
    End Sub

    Private Sub deFechaVto_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles seDiasCredito.LostFocus
        If gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
        End If
        gv.FocusedColumn = gv.Columns(0)
    End Sub


    Private Sub sbConsolidar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbConsolidar.Click
        _dtRequisicion = bl.com_ObtenerOrdenCompraDetalle(-1)
        Dim frmReq As New com_frmConsolidarRequisiciones
        Me.AddOwnedForm(frmReq)
        frmReq.ShowDialog()
        gc.DataSource = _dtRequisicion
        frmReq.Dispose()
        CalcularTotales()
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbBorrarFP.Click
        gvFormaPago.DeleteRow(gvFormaPago.FocusedRowHandle)
    End Sub

    Private Sub CargaPantalla()
        xtcOrdenCompra.SelectedTabPage = xtpDatos
        teNumero.Focus()

        With Header
            teCorrelativo.EditValue = .IdComprobante
            teNumero.EditValue = .Numero
            deFechaCompra.EditValue = .Fecha
            beProveedor.EditValue = .IdProveedor
            teNombre.EditValue = .Nombre
            teNRC.EditValue = .Nrc
            teDireccion.EditValue = .Direccion
            teGiro.EditValue = .Giro
            leFormaPago.EditValue = .IdFormaPago
            teNIT.EditValue = .Nit
            leOrigenCompra.EditValue = .IdOrigen
            leTipoCompra.EditValue = .IdTipoCompra
            cePercepcionRetencion.Checked = .TotalImpuesto1 > 0
            teEnvioFac.EditValue = .EnviarFacturaA
            teEmailEnvioFac.EditValue = .EmailEnvioFactura
            teDireccionEnvioFac.EditValue = .DireccionEnvioFactura
            teDireccionEntrega.EditValue = .DireccionEntrega
            teLugarEntrega.EditValue = .LugarEntrega
            teSolicitadoPor.EditValue = .SolicitadoPor
            tePercepcionRetencion.EditValue = .TotalImpuesto1
            teComentario.EditValue = .ComentarioAprobacion
            leSucursal.EditValue = .IdSucursal

            teTotal.EditValue = .Total
            teIva.EditValue = .Iva
            teFacturaAduana.EditValue = .FacturaAduana
            meCartaAduana.EditValue = .CartaAduana

            seDiasCredito.EditValue = .DiasCredito
            gc.DataSource = bl.com_ObtenerOrdenCompraDetalle(.IdComprobante)
            gcFormaPago.DataSource = bl.com_ObtenerOrdenCompraFormaPago(.IdComprobante)
        End With
        CalcularTotales()
    End Sub

    Private Sub sbPedido_Click(sender As Object, e As EventArgs) Handles sbPedido.Click
        If beProveedor.EditValue = "" Then
            MsgBox("Debe especificar el proveedor", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        gc.DataSource = bl.com_ObtenerOrdenCompraSugerida(beProveedor.EditValue)
    End Sub

    Private Sub sbCartaAduanera_Click(sender As Object, e As EventArgs) Handles sbCartaAduanera.Click
        If teNombre.Properties.ReadOnly = False Then
            Exit Sub
        End If

        Header = objTablas.com_OrdenesCompraSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Dim dt As DataTable = bl.com_ObtenerOrdenCompra(Header.IdComprobante, 0)

        Dim rpt As New com_rptCartaAduana() With {.DataSource = dt, .DataMember = ""}
        ''rptCartaAduana
        Dim Template = Application.StartupPath & "\Plantillas\rptCartaAduana.repx"
        If Not FileIO.FileSystem.FileExists(Template) Then
            MsgBox("No existe la plantilla necesaria para el documento" & Chr(13) & "Se imprimirá en formato estándar", MsgBoxStyle.Critical, "Nota")
        Else
            rpt.LoadLayout(Template)
        End If


        rpt.xrlFactura.Text = Header.FacturaAduana
        rpt.xrlCartaAduana.Text = Header.CartaAduana
        'rpt.xrLogo.Image = ByteToImagen(dtParam.Rows(0).Item("LogoEmpresa"))
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlDireccionEmpresa.Text = dtParam.Rows(0).Item("Direccion")
        rpt.xrlTelefonos.Text = "Tel.: " + dtParam.Rows(0).Item("Telefonos")
        rpt.xrlFecha.Text = dtParam.Rows(0).Item("Departamento").ToString.ToUpper + ", " + FechaToString(Header.Fecha, Header.Fecha).ToUpper
        rpt.ShowPreviewDialog()

    End Sub
End Class
