﻿Imports NexusBLL
Imports NexusELL.TableEntities
Imports DevExpress.XtraGrid.Columns

Imports DevExpress.XtraGrid.Menu
Imports DevExpress.Utils.Menu
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraSplashScreen

Public Class com_frmImportaciones
    Dim myBL As New ComprasBLL(g_ConnectionString)
    Dim blInve As New InventarioBLL(g_ConnectionString)
    Dim entProducto As inv_Productos
    Dim ImportacionHeader As New com_Importaciones
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Dim ImportacionDetalle As List(Of com_ImportacionesDetalle)
    Dim ImportacionesGastosDet As List(Of com_ImportacionesGastosDetalle)
    Dim ImportacionesGastosProd As List(Of com_ImportacionesGastosProductos)
    Dim ImportacionesProveedoresDet As List(Of com_ImportacionesProveedores)
    Dim _dtGastosImp As DataTable, _dtGastosImpProd As DataTable, _dtProveedoresImp As DataTable
    Dim dioClick As Boolean = False, dioClickProv As Boolean = False, dioClickGasProd As Boolean = False
    Dim TotalGastos As Decimal = 0.0

    Public Sub New()
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        AddHandler gv.PopupMenuShowing, AddressOf GridPopupMenuShowing
    End Sub

    Property dtGastosImp() As DataTable
        Get
            Return _dtGastosImp
        End Get
        Set(ByVal value As DataTable)
            _dtGastosImp = value
        End Set
    End Property
    Property dtGastosImpProd() As DataTable
        Get
            Return _dtGastosImpProd
        End Get
        Set(ByVal value As DataTable)
            _dtGastosImpProd = value
        End Set
    End Property

    Property dtProveedoresImp() As DataTable
        Get
            Return _dtProveedoresImp
        End Get
        Set(ByVal value As DataTable)
            _dtProveedoresImp = value
        End Set
    End Property

    Private Sub com_frmImportaciones_RefreshConsulta() Handles Me.RefreshConsulta
        gc2.DataSource = myBL.com_ConsultaImportaciones(objMenu.User).Tables(0)
        gv2.BestFitColumns()
    End Sub

    Private Sub com_frmImportaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.con_CentrosCosto(leCentroCosto, "", "")
        objCombos.inv_Bodegas(leBodega, "")
        objCombos.com_OrigenesImportacion(leOrigenCompra)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.adm_Sucursales(leSucursalDetalle, objMenu.User, "")
        objCombos.com_Aduana(leAduana)
        objCombos.com_ClaseDocumentoImp(leClaseDocumento)


        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("COM_IMPORTACIONES", "IdComprobante")
        ActivarControles(False)
        gv.BestFitColumns()

        Me.ValorDai.Caption = "% DAI"

        gc2.DataSource = myBL.com_ConsultaImportaciones(objMenu.User).Tables(0)
    End Sub
    Private Sub com_frmImportaciones_Nuevo() Handles MyBase.Nuevo
        ActivarControles(True)
        ImportacionHeader = New com_Importaciones
        ImportacionDetalle = New List(Of com_ImportacionesDetalle)
        gc.DataSource = myBL.com_ObtenerImportacionDetalle(-1)

        _dtGastosImp = myBL.com_ObtenerGastosImportacion(-1)
        _dtProveedoresImp = myBL.com_ObtenerProveedoresImportacion(-1)
        _dtGastosImpProd = myBL.com_ObtenerGastosImportacionProd(ImportacionHeader.IdComprobante, "", "")

        gv.CancelUpdateCurrentRow()
        'gv.AddNewRow()

        teNumPoliza.EditValue = ""
        teTotalIva.EditValue = 0.0
        teValorSujeto.EditValue = 0.0
        teTotalPP.EditValue = 0.0
        sePorcDescto.EditValue = 0.0
        teIdPartida.EditValue = 0
        teOrdenCompra.EditValue = ""
        leBodega.EditValue = 1
        leOrigenCompra.EditValue = 2

        leAduana.EditValue = 1
        leClaseDocumento.EditValue = 1

        leSucursal.EditValue = piIdSucursalUsuario
        deFecha.EditValue = Today
        deFechaCon.EditValue = Today
        teCorrelativo.Properties.ReadOnly = True
        teNumPoliza.Focus()
        xtcImportacion.SelectedTabPage = xtpDatos
    End Sub
    Private Sub com_frmImportaciones_Guardar() Handles MyBase.Guardar
        If Not DatosValidos() Then
            Exit Sub
        End If
        LlenarEntidades()

        If DbMode = DbModeType.insert Then
            ImportacionHeader.IdComprobante = 0
        End If

        Dim msj As String = myBL.com_GuardarImportacion(ImportacionHeader, ImportacionDetalle, ImportacionesGastosDet _
        , ImportacionesProveedoresDet, ImportacionesGastosProd)

        If msj = "Ok" Then
            MsgBox("El documento fue guardado con éxito", MsgBoxStyle.Information, "Nota")
        Else
            MsgBox("SE GENERÓ UN ERROR AL INTENTAR GUARDAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        

        teCorrelativo.EditValue = ImportacionHeader.IdComprobante
        'xtcImportacion.SelectedTabPage = xtpLista
        gc2.DataSource = myBL.com_ConsultaImportaciones(objMenu.User).Tables(0)
        gc2.Focus()

        MostrarModoInicial()
        teNumPoliza.Focus()
        ActivarControles(False)
    End Sub
    Private Sub com_frmImportaciones_Editar() Handles MyBase.Editar
        If ImportacionHeader.AplicadaInventario Then
            MsgBox("No puede editar ésta importación." + Chr(13) + "Debe revertirla antes de modificar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If
        ActivarControles(True)
        teNumPoliza.Focus()
    End Sub
    Private Sub com_frmImportaciones_Revertir() Handles Me.Revertir
        xtcImportacion.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub

    Private Sub com_frmImportaciones_Reporte() Handles MyBase.Reporte
        ImportacionHeader = objTablas.com_ImportacionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Dim entBod As inv_Bodegas = objTablas.inv_BodegasSelectByPK(ImportacionHeader.IdBodega)
        Dim Ds As DataSet = myBL.com_ObtenerProrrateoCalculado(ImportacionHeader.IdComprobante)
        Dim rpt As New com_rptRetaceo() With {.DataSource = Ds.Tables(0), .DataMember = ""}

        If Ds.Tables(1).Rows.Count = 0 Then
            rpt.XrSubreport1.ReportSource = Nothing
        Else
            rpt.XrSubreport1.ReportSource.DataSource = Ds.Tables(1)
            rpt.XrSubreport1.ReportSource.DataMember = ""
            rpt.XrSubreport2.ReportSource.DataSource = Ds.Tables(2)
            rpt.XrSubreport2.ReportSource.DataMember = ""
        End If

        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlFecha.Text = "FECHA: " & ImportacionHeader.Fecha
        rpt.xrlPoliza.Text = "No. DE POLIZA: " & ImportacionHeader.NumeroPoliza
        rpt.xrlNoHoja.Text = "No. DE HOJA: " & ImportacionHeader.IdComprobante
        rpt.xrlBodega.Text = "BODEGA DE INGRESO: " & entBod.Nombre

        rpt.CreateDocument()
        rpt.PaperKind = System.Drawing.Printing.PaperKind.Letter
        rpt.PrintingSystem.Document.AutoFitToPagesWidth = 1

        'Dim AutoSizeRpt As New XtraReport()
        'AutoSizeRpt.CreateDocument()
        'AutoSizeRpt.Pages.AddRange(rpt.Pages)
        'AutoSizeRpt.ShowPreviewDialog()

        rpt.ShowPreviewDialog()
    End Sub

    'Private Sub com_frmImportaciones_Reporte() Handles MyBase.Reporte
    '    ImportacionHeader = objTablas.com_ImportacionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
    '    Dim entBod As inv_Bodegas = objTablas.inv_BodegasSelectByPK(ImportacionHeader.IdBodega)
    '    Dim Ds As DataSet = myBL.com_ObtenerProrrateoCalculado(ImportacionHeader.IdComprobante)
    '    Dim rpt As New com_rptRetaceo() With {.DataSource = Ds.Tables(0), .DataMember = ""}

    '    If Ds.Tables(1).Rows.Count = 0 Then
    '        rpt.XrSubreport1.ReportSource = Nothing
    '    Else
    '        rpt.XrSubreport1.ReportSource.DataSource = Ds.Tables(1)
    '        rpt.XrSubreport1.ReportSource.DataMember = ""
    '    End If

    '    rpt.xrlEmpresa.Text = gsNombre_Empresa
    '    rpt.xrlFecha.Text = "FECHA: " & ImportacionHeader.Fecha
    '    rpt.xrlPoliza.Text = "No. DE POLIZA: " & ImportacionHeader.NumeroPoliza
    '    rpt.xrlNoHoja.Text = "No. DE HOJA: " & ImportacionHeader.IdComprobante
    '    rpt.xrlBodega.Text = "BODEGA DE INGRESO: " & entBod.Nombre
    '    rpt.ShowPreviewDialog()
    'End Sub
    Private Sub com_frmImportaciones_Eliminar() Handles MyBase.Eliminar
        ImportacionHeader = objTablas.com_ImportacionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))

        If ImportacionHeader.AplicadaInventario Then
            MsgBox("Es necesario que revierta la importación. No se puede eliminar", MsgBoxStyle.Exclamation, "Nota")
            MostrarModoInicial()
            Exit Sub
        End If

        If MsgBox("Está seguro(a) de elimnar ésta importación?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(ImportacionHeader.FechaContable)
        If EsOk Then
            ImportacionHeader.ModificadoPor = objMenu.User
            objTablas.com_ImportacionesUpdate(ImportacionHeader)

            objTablas.com_ImportacionesDeleteByPK(ImportacionHeader.IdComprobante)
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("COM_IMPORTACIONES", "IdComprobante")
            gc2.DataSource = myBL.com_ConsultaImportaciones(objMenu.User).Tables(0)
            MostrarModoInicial()
            ActivarControles(False)
        Else
            MsgBox("No puede eliminar éste documento, corresponde a un período ya cerrado", MsgBoxStyle.Critical, "Error")
        End If
    End Sub
   

    Private Sub tetotalpp_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles teTotalPP.LostFocus
        If gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.NewItemRowHandle
        End If
    End Sub

#Region "Grid"
    
    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh > 0 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la fila anterior
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh - 1, col))
            Next
            'actualiza la fila anterior con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh - 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh - 1
            gv.SelectRow(rh - 1)
        End If
    End Sub
    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        Dim rh As Integer = gv.FocusedRowHandle
        If rh < gv.RowCount - 1 Then
            Dim numCol As Integer = gv.Columns.Count
            Dim columna(numCol) As Object
            'Guarda los datos de la fila actual en el temporal columna
            Dim i As Integer = 0
            For Each col As GridColumn In gv.Columns
                columna(i) = gv.GetRowCellValue(rh, col)
                i += 1
            Next
            'actualiza la fila con los registros de la siguiente fila
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh, col, gv.GetRowCellValue(rh + 1, col))
            Next
            'actualiza la fila siguiente con los registros de el temporal
            i = 0
            For Each col As GridColumn In gv.Columns
                gv.SetRowCellValue(rh + 1, col, columna(i))
                i += 1
            Next
            gv.FocusedRowHandle = rh + 1
            gv.SelectRow(rh + 1)
        End If
    End Sub
    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteSelectedRows()
    End Sub

    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDai", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", 1.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioVentaAct", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioVentaCal", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioVentaNuevo", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PorcComision", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", "")
    End Sub
    Private Sub gv_InvalidRowException(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs) Handles gv.InvalidRowException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_InvalidValueException(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.InvalidValueExceptionEventArgs) Handles gv.InvalidValueException
        e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction
    End Sub
    Private Sub gv_ValidateRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles gv.ValidateRow
        'SOLAMENTE SE VALIDAN VALORES OBLIGATORIOS QUE NO PUEDEN QUEDAR VACIOS AL TRATAR DE ABANDONAR LA FILA
        Dim IdProd As String = SiEsNulo(gv.GetFocusedRowCellValue("IdProducto"), "")
        If IdProd = "" OrElse Not blInve.inv_VerificaCodigoProducto(IdProd) Then
            e.Valid = False
            gv.SetColumnError(gv.Columns("IdProducto"), "Código de producto no existe o no permite facturación")
        End If
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Dim Total As Decimal = 0.0

        Select Case gv.FocusedColumn.FieldName

            Case "Cantidad"
                Total = Decimal.Round(e.Value * gv.GetFocusedRowCellValue(gv.Columns("PrecioUnitario")), 2)
                gv.SetFocusedRowCellValue("PrecioTotal", Total)
            Case "PrecioUnitario"
                Total = Decimal.Round(gv.GetFocusedRowCellValue(gv.Columns("Cantidad")) * e.Value, 2)
                gv.SetFocusedRowCellValue("PrecioTotal", Total)
        End Select

    End Sub

    Private Sub gv_RowUpdated(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs)
        'CalcularTotales()
    End Sub

    Private Sub riCodProd_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles riCodProd.KeyDown
        If gv.FocusedColumn.FieldName = "IdProducto" And e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            If SiEsNulo(gv.EditingValue, "") = "" Then
                Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
                gv.EditingValue = IdProd
            End If
        End If

        Dim PrecioVta As Decimal
        Dim cantidad As Integer = gv.GetRowCellValue(gv.FocusedRowHandle, "Cantidad")

        If gv.FocusedColumn.FieldName = "IdProducto" And e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab OrElse e.KeyCode = Keys.Down OrElse e.KeyCode = Keys.Right OrElse e.KeyCode = Keys.Up OrElse e.KeyCode = Keys.Left And gsNombre_Empresa.StartsWith("ELECTRO") Then
            entProducto = objTablas.inv_ProductosSelectByPK(SiEsNulo(gv.EditingValue, ""))
            If entProducto.IdProducto = "" Then
                MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
                gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
            Else
                Dim dtSaldo As DataTable = blInve.inv_ObtieneExistenciaCostoProducto(entProducto.IdProducto, leBodega.EditValue)

                PrecioVta = SiEsNulo(blInve.inv_ObtienePrecioProducto(entProducto.IdProducto, 1), 0.0)
                gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
                gv.SetFocusedRowCellValue("IdCentro", SiEsNulo(entProducto.IdCentro, ""))
                gv.SetFocusedRowCellValue("PrecioVentaAct", PrecioVta)
                Dim Costo As Decimal = 0.0
                If dtSaldo.Rows.Count > 0 Then
                    Costo = SiEsNulo(dtSaldo.Rows(0).Item("PrecioCosto"), 0.0)
                End If

                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", Costo)
                Dim total As Decimal = Decimal.Round(cantidad * Costo, 2)
                gv.SetRowCellValue(gv.FocusedRowHandle, "PorcComision", entProducto.PorcComision)
                gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", total)
            End If
        End If
    End Sub

    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        'If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
        '    'validar columna codigo de producto
        '    If gv.FocusedColumn.FieldName = "IdProducto" Then
        '        If SiEsNulo(gv.EditingValue, "") = "" Then
        '            Dim IdProd = objConsultas.ConsultaProductos(inv_frmConsultaProductos, "", "")
        '            gv.EditingValue = IdProd
        '        End If
        '        entProducto = objTablas.inv_ProductosSelectByPK(gv.EditingValue)
        '        If entProducto.IdProducto = "" Then
        '            MsgBox("Código de producto no registrado", MsgBoxStyle.Critical, "Error")
        '            gv.SetFocusedRowCellValue("Descripcion", "-- PRODUCTO NO REGISTRADO --")
        '        Else
        '            gv.SetFocusedRowCellValue("Descripcion", entProducto.Nombre)
        '        End If
        '    End If
        'End If
    End Sub
    
#End Region


    Private Sub LlenarEntidades()
        With ImportacionHeader
            If DbMode = DbModeType.insert Then
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
            Else
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
            End If


            .NumeroPoliza = teNumPoliza.EditValue
            .Direccion = ""
            .Fecha = deFecha.EditValue
            .FechaContable = deFechaCon.EditValue
            .ValorDai = 0
            ''gv.Columns("ValorDai").SummaryItem.SummaryValue
            .ValorSeguros = 0
            .ValorTransporte = 0
            .ValorGastosImportacion = 0
            .ValorGastosExternos = 0
            .ValorGastosInternos = 0
            .ValorOtrosGastos = 0
            .ValorSujetoDeclaracion = teValorSujeto.EditValue
            .ValorExento = teValorExento.EditValue
            .PorcDescuento = sePorcDescto.EditValue
            .ValorIva = teTotalIva.EditValue
            .ValorImpuesto1 = 0
            .ValorImpuesto2 = 0
            .DiasCredito = 0
            .TotalPoliza = gv.Columns("PrecioTotal").SummaryItem.SummaryValue
            .TotalPorPagar = teTotalPP.EditValue
            .IdBodega = leBodega.EditValue
            .TipoImportacion = leOrigenCompra.EditValue
            .DaiCalculado = True
            .IdSucursal = leSucursal.EditValue
            .IdPartida = teIdPartida.EditValue
            .NumeroFactura = teOrdenCompra.EditValue

            .IdAduana = leAduana.EditValue
            .TipoDocumento = leClaseDocumento.EditValue
        End With

        Dim DaiTotal As Decimal = 0.0

        ImportacionDetalle = New List(Of com_ImportacionesDetalle)
        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New com_ImportacionesDetalle
            With entDetalle
                .IdComprobante = 0
                .IdProducto = gv.GetRowCellValue(i, "IdProducto")
                .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                .Referencia = gv.GetRowCellValue(i, "Referencia")
                .ValorDai = SiEsNulo(gv.GetRowCellValue(i, "ValorDai"), 0.0)
                .PrecioUnitario = gv.GetRowCellValue(i, "PrecioUnitario")
                .PrecioVentaAct = gv.GetRowCellValue(i, "PrecioVentaAct")
                .PrecioVentaCal = gv.GetRowCellValue(i, "PrecioVentaCal")
                .PrecioVentaNuevo = gv.GetRowCellValue(i, "PrecioVentaNuevo")
                .PorcComision = SiEsNulo(gv.GetRowCellValue(i, "PorcComision"), 0.0)
                .PrecioTotal = gv.GetRowCellValue(i, "PrecioTotal")
                .IdCentro = gv.GetRowCellValue(i, "IdCentro")
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .IdDetalle = i + 1

                If gv.GetRowCellValue(i, "ValorDai") > 0.0 Then
                    DaiTotal += gv.GetRowCellValue(i, "PrecioTotal") * (gv.GetRowCellValue(i, "ValorDai") / 100)
                End If

            End With
            ImportacionDetalle.Add(entDetalle)
        Next
        'teDAI.EditValue = DaiTotal
        ImportacionHeader.ValorDai = DaiTotal
    End Sub
    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In pcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        gv.OptionsBehavior.Editable = Tipo
        btAplicar.Enabled = Not ImportacionHeader.AplicadaInventario
        btRevertir.Enabled = ImportacionHeader.AplicadaInventario
    End Sub

    Private Function DatosValidos() As Boolean
        Dim msj As String = ""

        If teNumPoliza.EditValue = "" Then
            msj = "Debe de especificar el número de la póliza de importación"
        End If

        If teTotalPP.EditValue <= 0.0 Then
            msj = "Debe de especificar el valor de compra de cada proveedor"
        End If

        If leSucursal.EditValue = 0 Or leSucursal.EditValue = Nothing Or leSucursal.Text = "" Then
            msj = "Debe seleccionar una sucursal"
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(deFechaCon.EditValue)
        If Not EsOk Then
            msj = "Fecha contable del documento debe ser mayor a la fecha de cierre"
        End If

        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de dato")
            Return False
        End If
        Return True
    End Function

    Private Sub btAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAplicar.Click
        ImportacionHeader = objTablas.com_ImportacionesSelectByPK(teCorrelativo.EditValue)
        If ImportacionHeader.AplicadaInventario Then
            MsgBox("Esta Importación ya fue aplicada al inventario", MsgBoxStyle.Exclamation, "Error")
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de aplicar al inventario ésta Importación?" + Chr(13) + "YA NO SE PODRÁ EDITAR EL DOCUMENTO", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        Dim msj As String = blInve.inv_AplicarTransaccionInventario(ImportacionHeader.IdComprobante, "Importacion", "I", 4, 0)
        If msj = "Ok" Then
            MsgBox("La Importación ha sido aplicada al inventario. Ya no puede editarse", MsgBoxStyle.Information, "Nota")

            If dtParam.Rows(0).Item("ContabilizarLineaCompras") Then
                'contabilizo la compra
                Dim res As String = myBL.com_ContabilizarCompras(ImportacionHeader.FechaContable, ImportacionHeader.FechaContable, dtParam.Rows(0).Item("IdTipoPartidaCompras"), ImportacionHeader.IdSucursal, objMenu.User, ImportacionHeader.IdComprobante, 2)
                If res = "Ok" Then
                    MsgBox("La contabilización se ha realizado con éxito", 64, "Nota")
                Else
                    MsgBox("La contabilización NO se pudo realizar" + Chr(13) + res, MsgBoxStyle.Critical, "Error de base de datos")
                End If
            End If

            'If MsgBox("¿Actualizar precios de venta y % de comisión a productos?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            '    blInve.inv_ActualizaPreciosVentaImportacion(ImportacionHeader.IdComprobante)
            'End If

            ImportacionHeader = objTablas.com_ImportacionesSelectByPK(ImportacionHeader.IdComprobante)
            teIdPartida.EditValue = ImportacionHeader.IdPartida

            ImportacionHeader.AplicadaInventario = True
            btAplicar.Enabled = False
            btRevertir.Enabled = True
        Else
            MsgBox("Sucedió algún error al aplicar" + Chr(13) + msj + Chr(13) + "Favor reporte éste mensaje a ITO, S.A. DE C.V.", MsgBoxStyle.Critical, "Error")
        End If
    End Sub

    Private Sub btRevertir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRevertir.Click
        If MsgBox("¿Está seguro(a) de querer revertir ésta Importación?" + Chr(13) + "ESTO PUEDE DESESTABILIZAR SUS INVENTARIOS", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If
        If Not ImportacionHeader.AplicadaInventario Then
            MsgBox("El documento NO ha sido aplicado aún", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If
        'Dim dt As DataTable = bl.inv_ObtenerProductosAfectados(teCorrelativo.EditValue, "SALIDAS")
        Dim msj As String = blInve.inv_AplicarTransaccionInventario(ImportacionHeader.IdComprobante, "Importacion", "D", 4, 0)
        If msj = "Ok" Then
            MsgBox("El documento de Importación ha sido revertido con éxito" + Chr(13) + "YA PUEDE EDITARLA O ELIMINARLA", MsgBoxStyle.Information, "Nota")
            If dtParam.Rows(0).Item("ContabilizarLineaCompras") Then
                objTablas.con_PartidasDeleteByPK(ImportacionHeader.IdPartida) ' elimino la partida de compras
                teIdPartida.EditValue = 0
            End If
            ImportacionHeader.AplicadaInventario = False
            btAplicar.Enabled = True
            btRevertir.Enabled = False
        Else
            MsgBox("Sucedió algún error al intentar revertir" + Chr(13) + msj + Chr(13) + "Favor reporte éste mensaje a ITO, S.A. DE C.V.", MsgBoxStyle.Critical, "Error")
        End If
    End Sub

    Private Sub LlenarDetalleGastosImp()
        ' LLENO LA LISTA CON LOS GASTOS SELECCIONADOS DEL GRID ANTERIOR
        TotalGastos = 0.0
        ImportacionesGastosDet = New List(Of com_ImportacionesGastosDetalle)
        For i = 0 To _dtGastosImp.Rows.Count - 1
            Dim entDetalle As New com_ImportacionesGastosDetalle
            With entDetalle
                .IdImportacion = _dtGastosImp.Rows(i).Item("IdImportacion")
                .IdGasto = _dtGastosImp.Rows(i).Item("IdGasto")
                .Concepto = _dtGastosImp.Rows(i).Item("Concepto")
                .Valor = _dtGastosImp.Rows(i).Item("Valor")
                .Contabilizar = _dtGastosImp.Rows(i).Item("Contabilizar")
                TotalGastos += _dtGastosImp.Rows(i).Item("Valor")
                .IdDetalle = i + 1
            End With
            ImportacionesGastosDet.Add(entDetalle)
        Next
    End Sub

    Private Sub LlenarDetalleGastosProd()
        'LLENO LA LISTA DE PRODUCTO PARA QUE LE COLOQUEN LOS GASTOS QUE APLICARAN
        ImportacionesGastosProd = New List(Of com_ImportacionesGastosProductos)
        For i = 0 To _dtGastosImpProd.Rows.Count - 1
            If SiEsNulo(_dtGastosImpProd.Rows(i).Item("IdProducto"), "") <> "" Then
                Dim entDetalle As New com_ImportacionesGastosProductos
                With entDetalle
                    .IdImportacion = SiEsNulo(_dtGastosImpProd.Rows(i).Item("IdImportacion"), 0)
                    .Referencia = SiEsNulo(_dtGastosImpProd.Rows(i).Item("Referencia"), "")
                    .IdProducto = SiEsNulo(_dtGastosImpProd.Rows(i).Item("IdProducto"), "")
                    .Descripcion = SiEsNulo(_dtGastosImpProd.Rows(i).Item("Descripcion"), "")
                    .IdGasto = SiEsNulo(_dtGastosImpProd.Rows(i).Item("IdGasto"), 0)
                    .AplicaGasto = SiEsNulo(_dtGastosImpProd.Rows(i).Item("AplicaGasto"), 0)
                End With
                ImportacionesGastosProd.Add(entDetalle)
            End If
        Next
    End Sub

    Private Sub LlenarDetalleProvImp()
        ' LLENO LA LISTA CON LOS PROVEEDORES SELECCIONADOS DEL GRID ANTERIOR
        Dim TotalCompra As Decimal = 0.0

        ImportacionesProveedoresDet = New List(Of com_ImportacionesProveedores)
        For i = 0 To _dtProveedoresImp.Rows.Count - 1

            If _dtProveedoresImp.Rows(i).Item("IdProveedor") <> "" Then
                Dim entDetalle As New com_ImportacionesProveedores
                With entDetalle
                    .IdImportacion = SiEsNulo(_dtProveedoresImp.Rows(i).Item("IdImportacion"), 0)
                    .IdProveedor = _dtProveedoresImp.Rows(i).Item("IdProveedor")
                    .NumeroComprobante = _dtProveedoresImp.Rows(i).Item("NumeroComprobante")
                    .IdComprobante = SiEsNulo(_dtProveedoresImp.Rows(i).Item("IdComprobante"), 0)
                    .IdFormaPago = _dtProveedoresImp.Rows(i).Item("IdFormaPago")
                    .DiasCredito = _dtProveedoresImp.Rows(i).Item("DiasCredito")
                    .Valor = _dtProveedoresImp.Rows(i).Item("Valor")
                    .FechaContable = _dtProveedoresImp.Rows(i).Item("FechaContable")
                    .Fecha = _dtProveedoresImp.Rows(i).Item("Fecha")
                    .Contabilizar = _dtProveedoresImp.Rows(i).Item("Contabilizar")
                    .IdDetalle = i + 1
                    TotalCompra += .Valor
                End With

                ImportacionesProveedoresDet.Add(entDetalle)
            End If
        Next
        teTotalPP.EditValue = TotalCompra
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
        If _dtGastosImp.Rows.Count = 0 Then
            _dtGastosImp = myBL.com_ObtenerGastosImportacion(teCorrelativo.EditValue)
        End If
        Dim frmGastosImp As New com_frmDetallarGastosImportaciones
        Me.AddOwnedForm(frmGastosImp)
        frmGastosImp.IdImportacion = ImportacionHeader.IdComprobante
        frmGastosImp.ShowDialog()
        '_dtGastosImp = frmGastosImp.

        LlenarDetalleGastosImp()
    End Sub

    Private Sub btCPP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCPP.Click

        If dioClickProv = False Then
            _dtProveedoresImp = myBL.com_ObtenerProveedoresImportacion(-1)
        End If

        Dim frmProveedoresImp As New com_frmDetallarProveedoresImportacion
        Me.AddOwnedForm(frmProveedoresImp)
        frmProveedoresImp.IdImportacion = ImportacionHeader.IdComprobante
        frmProveedoresImp.ShowDialog()
        frmProveedoresImp.sbGuardar.Enabled = DbModeType.insert = DbMode
        LlenarDetalleProvImp()
        dioClickProv = True
    End Sub

    Private Sub cmdDetalla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDetalla.Click
        If rbMenu.qbSave.Visibility = DevExpress.XtraBars.BarItemVisibility.Never Then
            Exit Sub
        End If
        If TotalGastos <= 0.0 Then
            MsgBox("No ha definido los gastos de la importación", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If gv.DataRowCount = 0 Then
            MsgBox("No ha definido los productos de la importación", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim IdProdSelecciono As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdProducto")), "")
        Dim ReferenciaSelecciono As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("Referencia")), "")
        If IdProdSelecciono = "" Then
            MsgBox("Debe seleccionar el producto a definir gastos", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim ProductoYaDetallado As Boolean = False

        For i = 0 To _dtGastosImpProd.Rows.Count - 1
            If SiEsNulo(_dtGastosImpProd.Rows(i).Item("IdProducto"), "") = IdProdSelecciono And SiEsNulo(_dtGastosImpProd.Rows(i).Item("Referencia"), "") = ReferenciaSelecciono Then
                ProductoYaDetallado = True
            End If
        Next

        If Not ProductoYaDetallado Then
            Dim dtTemp As New DataTable
            dtTemp = myBL.com_ObtenerGastosImportacionProd(0, ReferenciaSelecciono, IdProdSelecciono)
            For i = 0 To dtTemp.Rows.Count - 1
                Dim dr As DataRow = _dtGastosImpProd.NewRow()
                dr("IdImportacion") = dtTemp.Rows(i).Item("IdImportacion")
                dr("Referencia") = dtTemp.Rows(i).Item("Referencia")
                dr("IdProducto") = dtTemp.Rows(i).Item("IdProducto")
                dr("Descripcion") = dtTemp.Rows(i).Item("Descripcion")
                dr("IdGasto") = dtTemp.Rows(i).Item("IdGasto")
                dr("AplicaGasto") = dtTemp.Rows(i).Item("AplicaGasto")
                _dtGastosImpProd.Rows.Add(dr)
            Next
        End If

        Dim frmGastosImpProd As New com_frmDetallarGastosImportacionProductos
        Me.AddOwnedForm(frmGastosImpProd)
        frmGastosImpProd.IdImportacion = ImportacionHeader.IdComprobante
        frmGastosImpProd.IdProducto = IdProdSelecciono
        frmGastosImpProd.Referencia = ReferenciaSelecciono
        frmGastosImpProd.ShowDialog()

        LlenarDetalleGastosProd()

    End Sub


    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        ImportacionHeader = objTablas.com_ImportacionesSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        DbMode = DbModeType.update
        teNumPoliza.Focus()
    End Sub

    Private Sub CargaPantalla()
        xtcImportacion.SelectedTabPage = xtpDatos
        teNumPoliza.Focus()

        With ImportacionHeader
            teCorrelativo.EditValue = .IdComprobante
            teNumPoliza.EditValue = .NumeroPoliza
            'teDAI.EditValue = .ValorDai
            'teSeguro.EditValue = .ValorSeguros
            'teTransporte.EditValue = .ValorTransporte
            'teGastosImp.EditValue = .ValorGastosImportacion
            'teGastosExt.EditValue = .ValorGastosExternos
            'teGastosInt.EditValue = .ValorGastosInternos
            'teOtrosGastos.EditValue = .ValorOtrosGastos
            teValorSujeto.EditValue = .ValorSujetoDeclaracion
            teValorExento.EditValue = .ValorExento
            sePorcDescto.EditValue = .PorcDescuento
            teTotalIva.EditValue = .ValorIva
            teTotalPP.EditValue = .TotalPorPagar
            leBodega.EditValue = .IdBodega
            deFecha.EditValue = .Fecha
            deFechaCon.EditValue = .FechaContable
            leOrigenCompra.EditValue = .TipoImportacion
            teIdPartida.EditValue = .IdPartida
            leSucursal.EditValue = .IdSucursal
            teOrdenCompra.EditValue = .NumeroFactura

            leAduana.EditValue = .IdAduana
            leClaseDocumento.EditValue = .TipoDocumento
        End With

        gc.DataSource = myBL.com_ObtenerImportacionDetalle(ImportacionHeader.IdComprobante)
        _dtGastosImp = myBL.com_ObtenerGastosImportacion(ImportacionHeader.IdComprobante)
        _dtProveedoresImp = myBL.com_ObtenerProveedoresImportacion(ImportacionHeader.IdComprobante)
        _dtGastosImpProd = myBL.com_ObtenerGastosImportacionProd(ImportacionHeader.IdComprobante, "", "") 'LLAMO TODOS LOS PRODUCTOS PARA VER QUE GASTOS APLICAN A ELLOS

        LlenarDetalleGastosImp()
        LlenarDetalleProvImp()
        LlenarDetalleGastosProd()

        btAplicar.Enabled = Not ImportacionHeader.AplicadaInventario
        btRevertir.Enabled = ImportacionHeader.AplicadaInventario
    End Sub


    Private Sub txtPrecioUnitario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPrecioUnitario.KeyDown
        'AUTOMATICAMENTE AL INGRESAR EL PRODUCTO, LE COLOCA QUE APLICA A TODOS LOS GASTOS QUE TIENE DETALLADO
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            Dim ProductoYaDetallado As Boolean = False
            Dim IdProdSelecciono As String = gv.GetFocusedRowCellValue(gv.Columns("IdProducto"))
            Dim ReferenciaSelecciono As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("Referencia")), "")

            For i = 0 To _dtGastosImpProd.Rows.Count - 1
                If SiEsNulo(_dtGastosImpProd.Rows(i).Item("IdProducto"), "") = IdProdSelecciono And SiEsNulo(_dtGastosImpProd.Rows(i).Item("Referencia"), "") = ReferenciaSelecciono Then
                    ProductoYaDetallado = True
                End If
            Next

            If Not ProductoYaDetallado Then
                Dim dtTemp As New DataTable
                dtTemp = myBL.com_ObtenerGastosImportacionProd(0, ReferenciaSelecciono, IdProdSelecciono)
                For i = 0 To dtTemp.Rows.Count - 1
                    Dim dr As DataRow = _dtGastosImpProd.NewRow()
                    dr("IdImportacion") = dtTemp.Rows(i).Item("IdImportacion")
                    dr("Referencia") = dtTemp.Rows(i).Item("Referencia")
                    dr("IdProducto") = dtTemp.Rows(i).Item("IdProducto")
                    dr("Descripcion") = dtTemp.Rows(i).Item("Descripcion")
                    dr("IdGasto") = dtTemp.Rows(i).Item("IdGasto")
                    dr("AplicaGasto") = dtTemp.Rows(i).Item("AplicaGasto")
                    _dtGastosImpProd.Rows.Add(dr)
                Next
            End If
            LlenarDetalleGastosProd()
        End If
    End Sub

    Private Sub gv_DoubleClick(sender As Object, e As EventArgs) Handles gv.DoubleClick
        If deFechaCon.Properties.ReadOnly Then
            Exit Sub
        End If

        Dim IdCentro As String = SiEsNulo(gv.GetFocusedRowCellValue(gv.Columns("IdCentro")), "")
        Dim frmSeleccionaCC As New frmDetallarCentroCosto
        Me.AddOwnedForm(frmSeleccionaCC)
        frmSeleccionaCC.IdCentro = IdCentro
        frmSeleccionaCC.ShowDialog()

        IdCentro = SiEsNulo(frmSeleccionaCC.IdCentro, "")
        gv.SetFocusedRowCellValue("IdCentro", IdCentro)
        frmSeleccionaCC.Dispose()
    End Sub

    Private Sub beOrden_Click(sender As Object, e As EventArgs) Handles beOrden.Click
        If teOrdenCompra.Properties.ReadOnly = True Then
            Exit Sub
        End If
        Dim IdOrden As Integer = myBL.com_ObtenerIdOrdenCompra(teOrdenCompra.EditValue)
        If IdOrden = 0 Then
            MsgBox("No se encontró ninguna orden con ése número", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If

        Dim entOC As com_OrdenesCompra = objTablas.com_OrdenesCompraSelectByPK(IdOrden)
        If Not entOC.Aprobada Then
            MsgBox("La orden de compra no ha sido aprobada", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If
        gc.DataSource = myBL.com_ObtenerOrdenCompraDetalle(IdOrden, teOrdenCompra.EditValue)

    End Sub

#Region "LoadFileXtraGrids"

    Private Sub GridPopupMenuShowing(sender As Object, e As PopupMenuShowingEventArgs)
        If e.MenuType = GridMenuType.Column Then
            Dim menu As GridViewColumnMenu = TryCast(e.Menu, GridViewColumnMenu)
            menu.Items.Clear()
            If menu.Column IsNot Nothing Then
                Dim item As New DXMenuItem("Import data to this table from a CSV", New EventHandler(AddressOf OnColumHeaderClick), Global.Nexus.My.Resources.Resources.fvupload)
                item.Tag = menu.Column
                menu.Items.Add(item)
            End If
        End If
    End Sub

    ' Menu item click handler - columns Header. 
    Private Sub OnColumHeaderClick(ByVal sender As Object, ByVal e As EventArgs)
        CargarDatosEnTabla()
    End Sub

    Private Sub CargarDatosEnTabla()

        Dim ofd As New OpenFileDialog()
        ofd.Filter = "CSV Files (*.csv)|*.csv|TXT Files (*.txt)|*.txt"
        ofd.Title = "Por Favor indique nombre del archivo a cargar..."

        If ofd.ShowDialog() = DialogResult.OK Then
            Dim DelimitadorCSV As String = InputBox("Ingrese el delimitador del CSV", "Verificacion CSV", ",")

            SplashScreenManager.ShowForm(Me, WaitForm.GetType(), True, True, False)
            SplashScreenManager.Default.SetWaitFormCaption("Procesando")
            SplashScreenManager.Default.SetWaitFormDescription("Extrayendo informacion...")
            Dim lines = IO.File.ReadAllLines(ofd.FileName)
            gc.DataSource = myBL.com_ObtenerImportacionDetalle(-12345)

            Try
                For Each line In lines
                    Dim objFields = From field In line.Split(DelimitadorCSV)
                                    Select CType(field, Object)

                    entProducto = objTablas.inv_ProductosSelectByPK(objFields(0))

                    If Not String.IsNullOrEmpty(entProducto.IdProducto) Then
                        gv.AddNewRow()

                        gv.SetRowCellValue(gv.FocusedRowHandle, "IdProducto", objFields(0))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Descripcion", objFields(1))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Referencia", objFields(2))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "ValorDai", objFields(3))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "Cantidad", objFields(4))
                        Dim PrecioVta = SiEsNulo(blInve.inv_ObtienePrecioProducto(entProducto.IdProducto, 1), 0.0)
                        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCentro", SiEsNulo(entProducto.IdCentro, ""))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioVentaAct", PrecioVta)
                        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioUnitario", objFields(5))
                        gv.SetRowCellValue(gv.FocusedRowHandle, "PorcComision", entProducto.PorcComision)
                        gv.SetRowCellValue(gv.FocusedRowHandle, "PrecioTotal", objFields(6))

                        gv.UpdateCurrentRow()
                    End If
                Next

                SplashScreenManager.Default.SetWaitFormCaption("Completado!")
                SplashScreenManager.Default.SetWaitFormDescription("Extraccion de datos terminado")
                SplashScreenManager.CloseForm()

                MessageBox.Show("Informacion extraida con Exito!", "OPERACION", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                SplashScreenManager.CloseForm()
                MessageBox.Show("Formato incorrecto CSV", "OPERACION", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try
        End If
    End Sub

#End Region
End Class
