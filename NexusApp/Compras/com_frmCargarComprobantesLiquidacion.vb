Imports DevExpress.XtraReports.UI
Imports System.IO
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class com_frmCargarComprobantesLiquidacion
    Dim myBL As New ComprasBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParametros As DataTable = blAdmon.ObtieneParametros
    Dim EntUser As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Dim _dtDatos As DataTable

    Property dtDatos() As DataTable
        Get
            Return _dtDatos
        End Get
        Set(ByVal value As DataTable)
            _dtDatos = value
        End Set
    End Property

    Dim _IdSucursal As Int32
    Property IdSucursal() As Int32
        Get
            Return _IdSucursal
        End Get
        Set(ByVal value As Int32)
            _IdSucursal = value
        End Set
    End Property
    Private Sub fac_frmConsultaPedidos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.banCuentasBancarias(LeCuentaBancaria)
        objCombos.com_ClaseDocumento(riteClaseDocumentoCF)
        objCombos.com_ClaseDocumento(riteClaseDocumentoLQ)

        gc.DataSource = myBL.com_ObtieneCreaLiquidacionEstructura()
    End Sub
    Private Sub CargarDatos()
        For i = 0 To gv.DataRowCount - 1
            Dim msj As String = ""
            Dim msj2 As String = ""
            Dim Liquidacion As New com_Liquidacion
            Dim entProveedor As New com_Proveedores

            entProveedor = objTablas.com_ProveedoresSelectByPK(gv.GetRowCellValue(i, "Nrc"))
            With Liquidacion
                .IdComprobante = 0
                .Serie = gv.GetRowCellValue(i, "Serie")
                .Numero = gv.GetRowCellValue(i, "Numero")
                .Resolucion = gv.GetRowCellValue(i, "Resolucion")
                .NumControlInterno = gv.GetRowCellValue(i, "NumControlInterno")
                .ClaseDocumento = gv.GetRowCellValue(i, "ClaseDocumento")
                .Fecha = gv.GetRowCellValue(i, "Fecha")
                .FechaContable = gv.GetRowCellValue(i, "Fecha")
                .IdProveedor = gv.GetRowCellValue(i, "Nrc")
                .Nombre = gv.GetRowCellValue(i, "Nombre")
                .Nrc = gv.GetRowCellValue(i, "Nrc")
                .Nit = entProveedor.Nit
                .Direccion = entProveedor.Direccion
                .TotalIva = gv.GetRowCellValue(i, "TotalIva")
                .TotalComprobante = gv.GetRowCellValue(i, "TotalComprobante")
                .Comision = gv.GetRowCellValue(i, "Comision")
                .IvaComision = gv.GetRowCellValue(i, "Comision")
                .IdSucursal = IdSucursal
                .CreadoPor = objMenu.User
                .FechaHoraCreacion = Now
                .ModificadoPor = ""
                .FechaHoraModificacion = Nothing
                .NumeroCF = gv.GetRowCellValue(i, "NumeroCF")
                .SerieCF = gv.GetRowCellValue(i, "SerieCF")
                .ResolucionCF = gv.GetRowCellValue(i, "ResolucionCF")
                .NumControlInternoCF = gv.GetRowCellValue(i, "NumControlInternoCF")
                .ClaseDocumentoCF = gv.GetRowCellValue(i, "ClaseDocumentoCF")
                .IdCuentaBancaria = gv.GetRowCellValue(i, "IdCuentaBancaria")
            End With
            msj2 = myBL.InsertaLiquidacion(Liquidacion)
            If msj2 <> "" Then
                MsgBox("LIQUIDACIONES IMPORTADAS CON EXITO", MsgBoxStyle.Information, "Nota")
                Exit Sub
            End If
        Next

    End Sub
    Private Sub btAplicar_Click(sender As Object, e As EventArgs) Handles btAplicar.Click

        For i = 0 To gv.DataRowCount - 1
            Dim Nombre = gv.GetRowCellValue(i, "Nombre")
            If Nombre = "- PROVEEDOR NO REGISTRADO -" Then
                MsgBox("EXISTEN PROVEEDORES NO REGISTRADOS", MsgBoxStyle.Information, "Nota")
            End If
        Next
        CargarDatos()
        Me.Close()
    End Sub

    Private Sub sbImportar_Click(sender As Object, e As EventArgs) Handles sbImportar.Click
        Dim Delimitador As String = InputBox("Especifique el deliminator del archivo, el cual puede ser coma , * |", "Delimitador", "|")
        If Delimitador = "" Then
            Return
        End If
        Dim ofd As New OpenFileDialog()
        ofd.ShowDialog()
        If ofd.FileName = "" Then
            Return
        End If
        Dim csv_file As System.IO.StreamReader = File.OpenText(ofd.FileName)
        Try
            gc.DataSource = myBL.com_ObtieneCreaLiquidacionEstructura()
            While csv_file.Peek() > 0
                Dim sLine As String = csv_file.ReadLine()
                Dim aData As Array = sLine.Split(Delimitador)
                gv.AddNewRow()
                gv.SetRowCellValue(gv.FocusedRowHandle, "Fecha", aData(0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Nrc", aData(1))
                Dim entPro As com_Proveedores
                entPro = objTablas.com_ProveedoresSelectByPK(SiEsNulo(aData(1), ""))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Nombre", SiEsNulo(entPro.Nombre, "- PROVEEDOR NO REGISTRADO -"))
                gv.SetRowCellValue(gv.FocusedRowHandle, "SerieCF", SiEsNulo(aData(2), ""))
                gv.SetRowCellValue(gv.FocusedRowHandle, "NumeroCF", SiEsNulo(aData(3), ""))
                gv.SetRowCellValue(gv.FocusedRowHandle, "ResolucionCF", SiEsNulo(aData(4), ""))
                gv.SetRowCellValue(gv.FocusedRowHandle, "NumControlInternoCF", SiEsNulo(aData(5), ""))

                Try
                    gv.SetRowCellValue(gv.FocusedRowHandle, "ClaseDocumentoCF", SiEsNulo(aData(6), 1))
                Catch ex As Exception
                    gv.SetRowCellValue(gv.FocusedRowHandle, "ClaseDocumentoCF", SiEsNulo(aData(6), 1))
                End Try

                gv.SetRowCellValue(gv.FocusedRowHandle, "Serie", SiEsNulo(aData(7), ""))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Numero", SiEsNulo(aData(8), ""))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Resolucion", SiEsNulo(aData(9), ""))
                gv.SetRowCellValue(gv.FocusedRowHandle, "NumControlInterno", SiEsNulo(aData(10), ""))

                Try
                    gv.SetRowCellValue(gv.FocusedRowHandle, "ClaseDocumento", SiEsNulo(aData(11), 1))
                Catch ex As Exception
                    gv.SetRowCellValue(gv.FocusedRowHandle, "ClaseDocumento", SiEsNulo(aData(11), 1))
                End Try

                gv.SetRowCellValue(gv.FocusedRowHandle, "TotalComprobante", SiEsNulo(aData(12), 0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "Comision", SiEsNulo(aData(13), 0))
                gv.SetRowCellValue(gv.FocusedRowHandle, "IdCuentaBancaria", SiEsNulo(aData(14), 1))
                gv.SetRowCellValue(gv.FocusedRowHandle, "TotalIva", SiEsNulo(aData(15), 0))
                gv.UpdateCurrentRow()
            End While
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error al importar")
            Return
        End Try
        csv_file.Close()
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        gv.DeleteSelectedRows()
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
End Class