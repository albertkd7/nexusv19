﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class com_frmOrdenesCompra
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pcDetalle = New DevExpress.XtraEditors.PanelControl()
        Me.xtcOrdenCompra = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage()
        Me.gc2 = New DevExpress.XtraGrid.GridControl()
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcCorrelativo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdProducto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcCantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcConcepto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPrecioReferencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPorcDescto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcValorDescto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcValorExento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcValorAfecto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.pcTotales = New DevExpress.XtraEditors.PanelControl()
        Me.tePercepcionRetencion = New DevExpress.XtraEditors.TextEdit()
        Me.lblPerRet = New DevExpress.XtraEditors.LabelControl()
        Me.cePercepcionRetencion = New DevExpress.XtraEditors.CheckEdit()
        Me.sbBorrarFP = New DevExpress.XtraEditors.SimpleButton()
        Me.gcFormaPago = New DevExpress.XtraGrid.GridControl()
        Me.gvFormaPago = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcValorPago = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.teTotal = New DevExpress.XtraEditors.TextEdit()
        Me.teIva = New DevExpress.XtraEditors.TextEdit()
        Me.teSubTotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.lbMensaje = New DevExpress.XtraEditors.LabelControl()
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.sbCartaAduanera = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.meCartaAduana = New DevExpress.XtraEditors.MemoEdit()
        Me.teFacturaAduana = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.teComentario = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.teDireccionEntrega = New DevExpress.XtraEditors.TextEdit()
        Me.teLugarEntrega = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.teSolicitadoPor = New DevExpress.XtraEditors.TextEdit()
        Me.teEmailEnvioFac = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.teDireccionEnvioFac = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.teEnvioFac = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.sbPedido = New DevExpress.XtraEditors.SimpleButton()
        Me.sbConsolidar = New DevExpress.XtraEditors.SimpleButton()
        Me.seDiasCredito = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaCompra = New DevExpress.XtraEditors.DateEdit()
        Me.leFormaPago = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.teNIT = New DevExpress.XtraEditors.TextEdit()
        Me.teNRC = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.leOrigenCompra = New DevExpress.XtraEditors.LookUpEdit()
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.beProveedor = New DevExpress.XtraEditors.ButtonEdit()
        Me.teGiro = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.teDireccion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoCompra = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.pcDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcDetalle.SuspendLayout()
        CType(Me.xtcOrdenCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcOrdenCompra.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcTotales.SuspendLayout()
        CType(Me.tePercepcionRetencion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cePercepcionRetencion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcFormaPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvFormaPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSubTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.meCartaAduana.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teFacturaAduana.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teComentario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDireccionEntrega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teLugarEntrega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSolicitadoPor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teEmailEnvioFac.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDireccionEnvioFac.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teEnvioFac.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaCompra.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leOrigenCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teGiro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pcDetalle
        '
        Me.pcDetalle.Controls.Add(Me.xtcOrdenCompra)
        Me.pcDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pcDetalle.Location = New System.Drawing.Point(0, 0)
        Me.pcDetalle.Name = "pcDetalle"
        Me.pcDetalle.Size = New System.Drawing.Size(1370, 573)
        Me.pcDetalle.TabIndex = 3
        '
        'xtcOrdenCompra
        '
        Me.xtcOrdenCompra.Appearance.ForeColor = System.Drawing.Color.Black
        Me.xtcOrdenCompra.Appearance.Options.UseForeColor = True
        Me.xtcOrdenCompra.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcOrdenCompra.Location = New System.Drawing.Point(2, 2)
        Me.xtcOrdenCompra.Name = "xtcOrdenCompra"
        Me.xtcOrdenCompra.SelectedTabPage = Me.xtpLista
        Me.xtcOrdenCompra.Size = New System.Drawing.Size(1366, 569)
        Me.xtcOrdenCompra.TabIndex = 3
        Me.xtcOrdenCompra.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc2)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(1359, 535)
        Me.xtpLista.Text = "Consulta Ordenes de Compra"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit2, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(1359, 535)
        Me.gc2.TabIndex = 3
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcCorrelativo, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12, Me.GridColumn13, Me.GridColumn9, Me.GridColumn16, Me.GridColumn14, Me.GridColumn15})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'gcCorrelativo
        '
        Me.gcCorrelativo.Caption = "Correlativo"
        Me.gcCorrelativo.FieldName = "IdComprobante"
        Me.gcCorrelativo.Name = "gcCorrelativo"
        Me.gcCorrelativo.Visible = True
        Me.gcCorrelativo.VisibleIndex = 0
        Me.gcCorrelativo.Width = 59
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Numero"
        Me.GridColumn10.FieldName = "Numero"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 1
        Me.GridColumn10.Width = 85
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Fecha"
        Me.GridColumn11.FieldName = "Fecha"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 2
        Me.GridColumn11.Width = 81
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Sucursal"
        Me.GridColumn12.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn12.FieldName = "IdSucursal"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 3
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Proveedor"
        Me.GridColumn13.FieldName = "Proveedor"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 4
        Me.GridColumn13.Width = 440
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Total Iva"
        Me.GridColumn9.FieldName = "Iva"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 5
        '
        'GridColumn16
        '
        Me.GridColumn16.Caption = "Total Comprobante"
        Me.GridColumn16.FieldName = "Total"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = True
        Me.GridColumn16.VisibleIndex = 7
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "Creado Por"
        Me.GridColumn14.FieldName = "CreadoPor"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 6
        Me.GridColumn14.Width = 98
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "Fecha Hora Creacion"
        Me.GridColumn15.ColumnEdit = Me.RepositoryItemDateEdit2
        Me.GridColumn15.FieldName = "FechaHoraCreacion"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 8
        Me.GridColumn15.Width = 114
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemDateEdit2.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.gc)
        Me.xtpDatos.Controls.Add(Me.pcTotales)
        Me.xtpDatos.Controls.Add(Me.pcBotones)
        Me.xtpDatos.Controls.Add(Me.pcHeader)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(1359, 535)
        Me.xtpDatos.Text = "Orden Compra"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 192)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.Size = New System.Drawing.Size(1324, 222)
        Me.gc.TabIndex = 8
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdProducto, Me.gcCantidad, Me.gcConcepto, Me.gcPrecioReferencia, Me.gcPorcDescto, Me.gcValorDescto, Me.gcPrecioUnitario, Me.gcValorExento, Me.gcValorAfecto})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.gv.OptionsNavigation.AutoFocusNewRow = True
        Me.gv.OptionsNavigation.EnterMoveNextColumn = True
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdProducto
        '
        Me.gcIdProducto.Caption = "Cód.Producto"
        Me.gcIdProducto.FieldName = "IdProducto"
        Me.gcIdProducto.Name = "gcIdProducto"
        Me.gcIdProducto.Visible = True
        Me.gcIdProducto.VisibleIndex = 0
        Me.gcIdProducto.Width = 78
        '
        'gcCantidad
        '
        Me.gcCantidad.Caption = "Cantidad"
        Me.gcCantidad.DisplayFormat.FormatString = "n3"
        Me.gcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcCantidad.FieldName = "Cantidad"
        Me.gcCantidad.Name = "gcCantidad"
        Me.gcCantidad.Visible = True
        Me.gcCantidad.VisibleIndex = 1
        Me.gcCantidad.Width = 71
        '
        'gcConcepto
        '
        Me.gcConcepto.Caption = "Concepto"
        Me.gcConcepto.FieldName = "Descripcion"
        Me.gcConcepto.Name = "gcConcepto"
        Me.gcConcepto.Visible = True
        Me.gcConcepto.VisibleIndex = 2
        Me.gcConcepto.Width = 242
        '
        'gcPrecioReferencia
        '
        Me.gcPrecioReferencia.Caption = "Precio Referencia"
        Me.gcPrecioReferencia.DisplayFormat.FormatString = "n6"
        Me.gcPrecioReferencia.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioReferencia.FieldName = "PrecioReferencia"
        Me.gcPrecioReferencia.Name = "gcPrecioReferencia"
        Me.gcPrecioReferencia.Visible = True
        Me.gcPrecioReferencia.VisibleIndex = 3
        Me.gcPrecioReferencia.Width = 101
        '
        'gcPorcDescto
        '
        Me.gcPorcDescto.Caption = "Porc.Descto"
        Me.gcPorcDescto.DisplayFormat.FormatString = "n2"
        Me.gcPorcDescto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPorcDescto.FieldName = "PorcDescto"
        Me.gcPorcDescto.Name = "gcPorcDescto"
        Me.gcPorcDescto.Visible = True
        Me.gcPorcDescto.VisibleIndex = 4
        Me.gcPorcDescto.Width = 74
        '
        'gcValorDescto
        '
        Me.gcValorDescto.Caption = "Descto.Total"
        Me.gcValorDescto.FieldName = "ValorDescto"
        Me.gcValorDescto.Name = "gcValorDescto"
        Me.gcValorDescto.OptionsColumn.AllowEdit = False
        Me.gcValorDescto.Visible = True
        Me.gcValorDescto.VisibleIndex = 5
        Me.gcValorDescto.Width = 83
        '
        'gcPrecioUnitario
        '
        Me.gcPrecioUnitario.Caption = "Precio Unitario"
        Me.gcPrecioUnitario.DisplayFormat.FormatString = "n6"
        Me.gcPrecioUnitario.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioUnitario.FieldName = "PrecioUnitario"
        Me.gcPrecioUnitario.Name = "gcPrecioUnitario"
        Me.gcPrecioUnitario.Visible = True
        Me.gcPrecioUnitario.VisibleIndex = 6
        Me.gcPrecioUnitario.Width = 88
        '
        'gcValorExento
        '
        Me.gcValorExento.Caption = "Valor Exento"
        Me.gcValorExento.DisplayFormat.FormatString = "n2"
        Me.gcValorExento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcValorExento.FieldName = "ValorExento"
        Me.gcValorExento.Name = "gcValorExento"
        Me.gcValorExento.OptionsColumn.AllowEdit = False
        Me.gcValorExento.OptionsColumn.AllowFocus = False
        Me.gcValorExento.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorExento", "{0:c}")})
        Me.gcValorExento.Visible = True
        Me.gcValorExento.VisibleIndex = 7
        Me.gcValorExento.Width = 73
        '
        'gcValorAfecto
        '
        Me.gcValorAfecto.Caption = "Valor Afecto"
        Me.gcValorAfecto.DisplayFormat.FormatString = "n2"
        Me.gcValorAfecto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcValorAfecto.FieldName = "ValorAfecto"
        Me.gcValorAfecto.Name = "gcValorAfecto"
        Me.gcValorAfecto.OptionsColumn.AllowEdit = False
        Me.gcValorAfecto.OptionsColumn.AllowFocus = False
        Me.gcValorAfecto.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorAfecto", "{0:c}")})
        Me.gcValorAfecto.Visible = True
        Me.gcValorAfecto.VisibleIndex = 8
        Me.gcValorAfecto.Width = 100
        '
        'pcTotales
        '
        Me.pcTotales.Controls.Add(Me.tePercepcionRetencion)
        Me.pcTotales.Controls.Add(Me.lblPerRet)
        Me.pcTotales.Controls.Add(Me.cePercepcionRetencion)
        Me.pcTotales.Controls.Add(Me.sbBorrarFP)
        Me.pcTotales.Controls.Add(Me.gcFormaPago)
        Me.pcTotales.Controls.Add(Me.LabelControl14)
        Me.pcTotales.Controls.Add(Me.teTotal)
        Me.pcTotales.Controls.Add(Me.teIva)
        Me.pcTotales.Controls.Add(Me.teSubTotal)
        Me.pcTotales.Controls.Add(Me.LabelControl6)
        Me.pcTotales.Controls.Add(Me.LabelControl5)
        Me.pcTotales.Controls.Add(Me.LabelControl4)
        Me.pcTotales.Controls.Add(Me.lbMensaje)
        Me.pcTotales.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pcTotales.Location = New System.Drawing.Point(0, 414)
        Me.pcTotales.Name = "pcTotales"
        Me.pcTotales.Size = New System.Drawing.Size(1324, 121)
        Me.pcTotales.TabIndex = 7
        '
        'tePercepcionRetencion
        '
        Me.tePercepcionRetencion.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.tePercepcionRetencion.EditValue = ""
        Me.tePercepcionRetencion.Location = New System.Drawing.Point(1170, 52)
        Me.tePercepcionRetencion.Name = "tePercepcionRetencion"
        Me.tePercepcionRetencion.Properties.Appearance.Options.UseTextOptions = True
        Me.tePercepcionRetencion.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.tePercepcionRetencion.Properties.Mask.EditMask = "##,##0.00"
        Me.tePercepcionRetencion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tePercepcionRetencion.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tePercepcionRetencion.Properties.ReadOnly = True
        Me.tePercepcionRetencion.Size = New System.Drawing.Size(149, 22)
        Me.tePercepcionRetencion.TabIndex = 70
        '
        'lblPerRet
        '
        Me.lblPerRet.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblPerRet.Location = New System.Drawing.Point(1107, 56)
        Me.lblPerRet.Name = "lblPerRet"
        Me.lblPerRet.Size = New System.Drawing.Size(67, 16)
        Me.lblPerRet.TabIndex = 69
        Me.lblPerRet.Text = "Percepción:"
        '
        'cePercepcionRetencion
        '
        Me.cePercepcionRetencion.Location = New System.Drawing.Point(475, 0)
        Me.cePercepcionRetencion.Name = "cePercepcionRetencion"
        Me.cePercepcionRetencion.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cePercepcionRetencion.Properties.Appearance.Options.UseFont = True
        Me.cePercepcionRetencion.Properties.Caption = "Aplicar Percepción"
        Me.cePercepcionRetencion.Size = New System.Drawing.Size(159, 22)
        Me.cePercepcionRetencion.TabIndex = 67
        '
        'sbBorrarFP
        '
        Me.sbBorrarFP.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.sbBorrarFP.Location = New System.Drawing.Point(609, 35)
        Me.sbBorrarFP.Name = "sbBorrarFP"
        Me.sbBorrarFP.Size = New System.Drawing.Size(23, 23)
        Me.sbBorrarFP.TabIndex = 66
        '
        'gcFormaPago
        '
        Me.gcFormaPago.Location = New System.Drawing.Point(2, 20)
        Me.gcFormaPago.MainView = Me.gvFormaPago
        Me.gcFormaPago.Name = "gcFormaPago"
        Me.gcFormaPago.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1})
        Me.gcFormaPago.Size = New System.Drawing.Size(604, 97)
        Me.gcFormaPago.TabIndex = 65
        Me.gcFormaPago.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvFormaPago})
        '
        'gvFormaPago
        '
        Me.gvFormaPago.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn6, Me.GridColumn8, Me.gcValorPago})
        Me.gvFormaPago.GridControl = Me.gcFormaPago
        Me.gvFormaPago.Name = "gvFormaPago"
        Me.gvFormaPago.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.gvFormaPago.OptionsNavigation.AutoFocusNewRow = True
        Me.gvFormaPago.OptionsNavigation.EnterMoveNextColumn = True
        Me.gvFormaPago.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gvFormaPago.OptionsView.ShowFooter = True
        Me.gvFormaPago.OptionsView.ShowGroupPanel = False
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Fecha Pago"
        Me.GridColumn6.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.GridColumn6.FieldName = "Fecha"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 0
        Me.GridColumn6.Width = 90
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemDateEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Descripcion"
        Me.GridColumn8.FieldName = "Descripcion"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 1
        Me.GridColumn8.Width = 278
        '
        'gcValorPago
        '
        Me.gcValorPago.Caption = "Valor"
        Me.gcValorPago.DisplayFormat.FormatString = "n3"
        Me.gcValorPago.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcValorPago.FieldName = "Valor"
        Me.gcValorPago.Name = "gcValorPago"
        Me.gcValorPago.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor", "{0:c}")})
        Me.gcValorPago.Visible = True
        Me.gcValorPago.VisibleIndex = 2
        Me.gcValorPago.Width = 82
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(5, 0)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(148, 17)
        Me.LabelControl14.TabIndex = 64
        Me.LabelControl14.Text = "Condiciones de Pago:"
        '
        'teTotal
        '
        Me.teTotal.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teTotal.EditValue = ""
        Me.teTotal.Location = New System.Drawing.Point(1170, 73)
        Me.teTotal.Name = "teTotal"
        Me.teTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotal.Properties.EditFormat.FormatString = "{0:n2}"
        Me.teTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teTotal.Properties.Mask.EditMask = "##,##0.00"
        Me.teTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTotal.Properties.ReadOnly = True
        Me.teTotal.Size = New System.Drawing.Size(149, 22)
        Me.teTotal.TabIndex = 1
        '
        'teIva
        '
        Me.teIva.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teIva.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.teIva.Location = New System.Drawing.Point(1170, 31)
        Me.teIva.Name = "teIva"
        Me.teIva.Properties.Appearance.Options.UseTextOptions = True
        Me.teIva.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teIva.Properties.Mask.EditMask = "##,##0.00"
        Me.teIva.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teIva.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teIva.Properties.ReadOnly = True
        Me.teIva.Size = New System.Drawing.Size(149, 22)
        Me.teIva.TabIndex = 1
        '
        'teSubTotal
        '
        Me.teSubTotal.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teSubTotal.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.teSubTotal.Location = New System.Drawing.Point(1170, 10)
        Me.teSubTotal.Name = "teSubTotal"
        Me.teSubTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.teSubTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teSubTotal.Properties.Mask.EditMask = "##,##0.00"
        Me.teSubTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teSubTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teSubTotal.Properties.ReadOnly = True
        Me.teSubTotal.Size = New System.Drawing.Size(149, 22)
        Me.teSubTotal.TabIndex = 1
        '
        'LabelControl6
        '
        Me.LabelControl6.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl6.Location = New System.Drawing.Point(1107, 79)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(44, 16)
        Me.LabelControl6.TabIndex = 0
        Me.LabelControl6.Text = "TOTAL:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl5.Location = New System.Drawing.Point(1107, 35)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(25, 16)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "IVA:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl4.Location = New System.Drawing.Point(1107, 14)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(61, 16)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Sub-Total:"
        '
        'lbMensaje
        '
        Me.lbMensaje.Appearance.Font = New System.Drawing.Font("Cambria", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbMensaje.Appearance.Options.UseFont = True
        Me.lbMensaje.Location = New System.Drawing.Point(728, 20)
        Me.lbMensaje.Name = "lbMensaje"
        Me.lbMensaje.Size = New System.Drawing.Size(6, 28)
        Me.lbMensaje.TabIndex = 86
        Me.lbMensaje.Text = "."
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdBorrar)
        Me.pcBotones.Controls.Add(Me.cmdUp)
        Me.pcBotones.Controls.Add(Me.cmdDown)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(1324, 192)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(35, 343)
        Me.pcBotones.TabIndex = 5
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(4, 67)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(28, 23)
        Me.cmdBorrar.TabIndex = 39
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(4, 37)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(28, 24)
        Me.cmdUp.TabIndex = 38
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(4, 8)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(28, 24)
        Me.cmdDown.TabIndex = 37
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.GroupControl1)
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.LabelControl25)
        Me.pcHeader.Controls.Add(Me.teComentario)
        Me.pcHeader.Controls.Add(Me.LabelControl24)
        Me.pcHeader.Controls.Add(Me.teDireccionEntrega)
        Me.pcHeader.Controls.Add(Me.teLugarEntrega)
        Me.pcHeader.Controls.Add(Me.LabelControl22)
        Me.pcHeader.Controls.Add(Me.LabelControl23)
        Me.pcHeader.Controls.Add(Me.teSolicitadoPor)
        Me.pcHeader.Controls.Add(Me.teEmailEnvioFac)
        Me.pcHeader.Controls.Add(Me.LabelControl21)
        Me.pcHeader.Controls.Add(Me.LabelControl20)
        Me.pcHeader.Controls.Add(Me.teDireccionEnvioFac)
        Me.pcHeader.Controls.Add(Me.LabelControl17)
        Me.pcHeader.Controls.Add(Me.teEnvioFac)
        Me.pcHeader.Controls.Add(Me.LabelControl16)
        Me.pcHeader.Controls.Add(Me.sbPedido)
        Me.pcHeader.Controls.Add(Me.sbConsolidar)
        Me.pcHeader.Controls.Add(Me.seDiasCredito)
        Me.pcHeader.Controls.Add(Me.LabelControl3)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.deFechaCompra)
        Me.pcHeader.Controls.Add(Me.leFormaPago)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Controls.Add(Me.teNumero)
        Me.pcHeader.Controls.Add(Me.LabelControl7)
        Me.pcHeader.Controls.Add(Me.teNombre)
        Me.pcHeader.Controls.Add(Me.teNIT)
        Me.pcHeader.Controls.Add(Me.teNRC)
        Me.pcHeader.Controls.Add(Me.LabelControl9)
        Me.pcHeader.Controls.Add(Me.LabelControl11)
        Me.pcHeader.Controls.Add(Me.LabelControl19)
        Me.pcHeader.Controls.Add(Me.leOrigenCompra)
        Me.pcHeader.Controls.Add(Me.teCorrelativo)
        Me.pcHeader.Controls.Add(Me.LabelControl15)
        Me.pcHeader.Controls.Add(Me.LabelControl10)
        Me.pcHeader.Controls.Add(Me.beProveedor)
        Me.pcHeader.Controls.Add(Me.teGiro)
        Me.pcHeader.Controls.Add(Me.LabelControl18)
        Me.pcHeader.Controls.Add(Me.LabelControl13)
        Me.pcHeader.Controls.Add(Me.teDireccion)
        Me.pcHeader.Controls.Add(Me.LabelControl8)
        Me.pcHeader.Controls.Add(Me.LabelControl12)
        Me.pcHeader.Controls.Add(Me.leTipoCompra)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(1359, 192)
        Me.pcHeader.TabIndex = 3
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.sbCartaAduanera)
        Me.GroupControl1.Controls.Add(Me.LabelControl27)
        Me.GroupControl1.Controls.Add(Me.meCartaAduana)
        Me.GroupControl1.Controls.Add(Me.teFacturaAduana)
        Me.GroupControl1.Controls.Add(Me.LabelControl26)
        Me.GroupControl1.Location = New System.Drawing.Point(832, 26)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(323, 163)
        Me.GroupControl1.TabIndex = 110
        Me.GroupControl1.Text = "Datos Carta Aduanal"
        '
        'sbCartaAduanera
        '
        Me.sbCartaAduanera.Location = New System.Drawing.Point(220, 23)
        Me.sbCartaAduanera.Name = "sbCartaAduanera"
        Me.sbCartaAduanera.Size = New System.Drawing.Size(98, 22)
        Me.sbCartaAduanera.TabIndex = 111
        Me.sbCartaAduanera.Text = "Carta Aduanera"
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(5, 46)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(95, 16)
        Me.LabelControl27.TabIndex = 111
        Me.LabelControl27.Text = "Carta Dirigida A:"
        '
        'meCartaAduana
        '
        Me.meCartaAduana.Location = New System.Drawing.Point(5, 60)
        Me.meCartaAduana.Name = "meCartaAduana"
        Me.meCartaAduana.Size = New System.Drawing.Size(313, 99)
        Me.meCartaAduana.TabIndex = 86
        '
        'teFacturaAduana
        '
        Me.teFacturaAduana.EnterMoveNextControl = True
        Me.teFacturaAduana.Location = New System.Drawing.Point(73, 24)
        Me.teFacturaAduana.Name = "teFacturaAduana"
        Me.teFacturaAduana.Size = New System.Drawing.Size(130, 22)
        Me.teFacturaAduana.TabIndex = 84
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(5, 27)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(71, 16)
        Me.LabelControl26.TabIndex = 85
        Me.LabelControl26.Text = "Factura No.:"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(532, 169)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(293, 22)
        Me.leSucursal.TabIndex = 108
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(486, 174)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(54, 16)
        Me.LabelControl25.TabIndex = 109
        Me.LabelControl25.Text = "Sucursal:"
        '
        'teComentario
        '
        Me.teComentario.Enabled = False
        Me.teComentario.EnterMoveNextControl = True
        Me.teComentario.Location = New System.Drawing.Point(99, 169)
        Me.teComentario.Name = "teComentario"
        Me.teComentario.Size = New System.Drawing.Size(319, 22)
        Me.teComentario.TabIndex = 107
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(37, 171)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(71, 16)
        Me.LabelControl24.TabIndex = 106
        Me.LabelControl24.Text = "Comentario:"
        '
        'teDireccionEntrega
        '
        Me.teDireccionEntrega.EnterMoveNextControl = True
        Me.teDireccionEntrega.Location = New System.Drawing.Point(532, 128)
        Me.teDireccionEntrega.Name = "teDireccionEntrega"
        Me.teDireccionEntrega.Size = New System.Drawing.Size(293, 22)
        Me.teDireccionEntrega.TabIndex = 103
        '
        'teLugarEntrega
        '
        Me.teLugarEntrega.EnterMoveNextControl = True
        Me.teLugarEntrega.Location = New System.Drawing.Point(99, 128)
        Me.teLugarEntrega.Name = "teLugarEntrega"
        Me.teLugarEntrega.Size = New System.Drawing.Size(319, 22)
        Me.teLugarEntrega.TabIndex = 102
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(9, 131)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(103, 16)
        Me.LabelControl22.TabIndex = 101
        Me.LabelControl22.Text = "Lugar de Entrega:"
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(427, 131)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(123, 16)
        Me.LabelControl23.TabIndex = 100
        Me.LabelControl23.Text = "Dirección de Entrega:"
        '
        'teSolicitadoPor
        '
        Me.teSolicitadoPor.EnterMoveNextControl = True
        Me.teSolicitadoPor.Location = New System.Drawing.Point(532, 107)
        Me.teSolicitadoPor.Name = "teSolicitadoPor"
        Me.teSolicitadoPor.Size = New System.Drawing.Size(292, 22)
        Me.teSolicitadoPor.TabIndex = 99
        '
        'teEmailEnvioFac
        '
        Me.teEmailEnvioFac.EnterMoveNextControl = True
        Me.teEmailEnvioFac.Location = New System.Drawing.Point(99, 107)
        Me.teEmailEnvioFac.Name = "teEmailEnvioFac"
        Me.teEmailEnvioFac.Size = New System.Drawing.Size(319, 22)
        Me.teEmailEnvioFac.TabIndex = 98
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(7, 110)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(106, 16)
        Me.LabelControl21.TabIndex = 97
        Me.LabelControl21.Text = "E-Mail Envio Fact.:"
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(445, 110)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(104, 16)
        Me.LabelControl20.TabIndex = 96
        Me.LabelControl20.Text = "Solicitado (a) Por:"
        '
        'teDireccionEnvioFac
        '
        Me.teDireccionEnvioFac.EnterMoveNextControl = True
        Me.teDireccionEnvioFac.Location = New System.Drawing.Point(532, 86)
        Me.teDireccionEnvioFac.Name = "teDireccionEnvioFac"
        Me.teDireccionEnvioFac.Size = New System.Drawing.Size(292, 22)
        Me.teDireccionEnvioFac.TabIndex = 95
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(426, 88)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(123, 16)
        Me.LabelControl17.TabIndex = 94
        Me.LabelControl17.Text = "Dirección Envio Fact.:"
        '
        'teEnvioFac
        '
        Me.teEnvioFac.EnterMoveNextControl = True
        Me.teEnvioFac.Location = New System.Drawing.Point(99, 86)
        Me.teEnvioFac.Name = "teEnvioFac"
        Me.teEnvioFac.Size = New System.Drawing.Size(319, 22)
        Me.teEnvioFac.TabIndex = 93
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(24, 88)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(84, 16)
        Me.LabelControl16.TabIndex = 92
        Me.LabelControl16.Text = "Enviar Fact. A:"
        '
        'sbPedido
        '
        Me.sbPedido.Location = New System.Drawing.Point(934, 2)
        Me.sbPedido.Name = "sbPedido"
        Me.sbPedido.Size = New System.Drawing.Size(98, 22)
        Me.sbPedido.TabIndex = 91
        Me.sbPedido.Text = "Pedido sugerido"
        '
        'sbConsolidar
        '
        Me.sbConsolidar.Location = New System.Drawing.Point(830, 2)
        Me.sbConsolidar.Name = "sbConsolidar"
        Me.sbConsolidar.Size = New System.Drawing.Size(98, 22)
        Me.sbConsolidar.TabIndex = 91
        Me.sbConsolidar.Text = "Ver Requisiciones"
        '
        'seDiasCredito
        '
        Me.seDiasCredito.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDiasCredito.EnterMoveNextControl = True
        Me.seDiasCredito.Location = New System.Drawing.Point(374, 148)
        Me.seDiasCredito.Name = "seDiasCredito"
        Me.seDiasCredito.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDiasCredito.Size = New System.Drawing.Size(44, 22)
        Me.seDiasCredito.TabIndex = 12
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(294, 151)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(92, 16)
        Me.LabelControl3.TabIndex = 74
        Me.LabelControl3.Text = "Días de Crédito:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(20, 150)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(92, 16)
        Me.LabelControl2.TabIndex = 63
        Me.LabelControl2.Text = "Forma de Pago:"
        '
        'deFechaCompra
        '
        Me.deFechaCompra.EditValue = Nothing
        Me.deFechaCompra.EnterMoveNextControl = True
        Me.deFechaCompra.Location = New System.Drawing.Point(728, 2)
        Me.deFechaCompra.Name = "deFechaCompra"
        Me.deFechaCompra.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaCompra.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaCompra.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaCompra.Size = New System.Drawing.Size(96, 22)
        Me.deFechaCompra.TabIndex = 2
        '
        'leFormaPago
        '
        Me.leFormaPago.EnterMoveNextControl = True
        Me.leFormaPago.Location = New System.Drawing.Point(99, 148)
        Me.leFormaPago.Name = "leFormaPago"
        Me.leFormaPago.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leFormaPago.Size = New System.Drawing.Size(179, 22)
        Me.leFormaPago.TabIndex = 11
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(637, 6)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(106, 16)
        Me.LabelControl1.TabIndex = 61
        Me.LabelControl1.Text = "Fecha de Compra:"
        '
        'teNumero
        '
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(374, 2)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(130, 22)
        Me.teNumero.TabIndex = 1
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(330, 26)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(50, 16)
        Me.LabelControl7.TabIndex = 78
        Me.LabelControl7.Text = "Nombre:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(374, 23)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(450, 22)
        Me.teNombre.TabIndex = 4
        '
        'teNIT
        '
        Me.teNIT.EnterMoveNextControl = True
        Me.teNIT.Location = New System.Drawing.Point(694, 65)
        Me.teNIT.Name = "teNIT"
        Me.teNIT.Size = New System.Drawing.Size(130, 22)
        Me.teNIT.TabIndex = 8
        '
        'teNRC
        '
        Me.teNRC.EnterMoveNextControl = True
        Me.teNRC.Location = New System.Drawing.Point(532, 65)
        Me.teNRC.Name = "teNRC"
        Me.teNRC.Size = New System.Drawing.Size(96, 22)
        Me.teNRC.TabIndex = 7
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(505, 68)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(29, 16)
        Me.LabelControl9.TabIndex = 80
        Me.LabelControl9.Text = "NRC:"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(670, 69)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(25, 16)
        Me.LabelControl11.TabIndex = 82
        Me.LabelControl11.Text = "NIT:"
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(428, 153)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(124, 16)
        Me.LabelControl19.TabIndex = 90
        Me.LabelControl19.Text = "Origen de la Compra:"
        '
        'leOrigenCompra
        '
        Me.leOrigenCompra.EnterMoveNextControl = True
        Me.leOrigenCompra.Location = New System.Drawing.Point(532, 149)
        Me.leOrigenCompra.Name = "leOrigenCompra"
        Me.leOrigenCompra.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leOrigenCompra.Size = New System.Drawing.Size(96, 22)
        Me.leOrigenCompra.TabIndex = 10
        '
        'teCorrelativo
        '
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(99, 2)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Size = New System.Drawing.Size(130, 22)
        Me.teCorrelativo.TabIndex = 0
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(39, 6)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(67, 16)
        Me.LabelControl15.TabIndex = 86
        Me.LabelControl15.Text = "Correlativo:"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(25, 69)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(84, 16)
        Me.LabelControl10.TabIndex = 81
        Me.LabelControl10.Text = "Giro/Actividad:"
        '
        'beProveedor
        '
        Me.beProveedor.EditValue = ""
        Me.beProveedor.EnterMoveNextControl = True
        Me.beProveedor.Location = New System.Drawing.Point(99, 23)
        Me.beProveedor.Name = "beProveedor"
        Me.beProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beProveedor.Size = New System.Drawing.Size(130, 22)
        Me.beProveedor.TabIndex = 3
        '
        'teGiro
        '
        Me.teGiro.EnterMoveNextControl = True
        Me.teGiro.Location = New System.Drawing.Point(99, 65)
        Me.teGiro.Name = "teGiro"
        Me.teGiro.Size = New System.Drawing.Size(319, 22)
        Me.teGiro.TabIndex = 6
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(630, 152)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(97, 16)
        Me.LabelControl18.TabIndex = 89
        Me.LabelControl18.Text = "Tipo de Compra:"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(42, 26)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(63, 16)
        Me.LabelControl13.TabIndex = 84
        Me.LabelControl13.Text = "Proveedor:"
        '
        'teDireccion
        '
        Me.teDireccion.EnterMoveNextControl = True
        Me.teDireccion.Location = New System.Drawing.Point(99, 44)
        Me.teDireccion.Name = "teDireccion"
        Me.teDireccion.Size = New System.Drawing.Size(725, 22)
        Me.teDireccion.TabIndex = 5
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(49, 48)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(57, 16)
        Me.LabelControl8.TabIndex = 79
        Me.LabelControl8.Text = "Dirección:"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(302, 6)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(81, 16)
        Me.LabelControl12.TabIndex = 83
        Me.LabelControl12.Text = "No. de Orden:"
        '
        'leTipoCompra
        '
        Me.leTipoCompra.EnterMoveNextControl = True
        Me.leTipoCompra.Location = New System.Drawing.Point(709, 149)
        Me.leTipoCompra.Name = "leTipoCompra"
        Me.leTipoCompra.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoCompra.Size = New System.Drawing.Size(116, 22)
        Me.leTipoCompra.TabIndex = 9
        '
        'com_frmOrdenesCompra
        '
        Me.ClientSize = New System.Drawing.Size(1370, 607)
        Me.Controls.Add(Me.pcDetalle)
        Me.Modulo = "Compras"
        Me.Name = "com_frmOrdenesCompra"
        Me.OptionId = "002001"
        Me.Text = "Ordenes de Compra"
        Me.Controls.SetChildIndex(Me.pcDetalle, 0)
        CType(Me.pcDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcDetalle.ResumeLayout(False)
        CType(Me.xtcOrdenCompra, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcOrdenCompra.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcTotales.ResumeLayout(False)
        Me.pcTotales.PerformLayout()
        CType(Me.tePercepcionRetencion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cePercepcionRetencion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcFormaPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvFormaPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSubTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.meCartaAduana.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teFacturaAduana.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teComentario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDireccionEntrega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teLugarEntrega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSolicitadoPor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teEmailEnvioFac.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDireccionEnvioFac.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teEnvioFac.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaCompra.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leOrigenCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teGiro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pcDetalle As DevExpress.XtraEditors.PanelControl
    Friend WithEvents xtcOrdenCompra As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents teComentario As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDireccionEntrega As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teLugarEntrega As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teSolicitadoPor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teEmailEnvioFac As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDireccionEnvioFac As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teEnvioFac As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbConsolidar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents seDiasCredito As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFechaCompra As DevExpress.XtraEditors.DateEdit
    Friend WithEvents leFormaPago As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNIT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leOrigenCompra As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beProveedor As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents teGiro As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoCompra As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioReferencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPorcDescto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorDescto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorExento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorAfecto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents pcTotales As DevExpress.XtraEditors.PanelControl
    Friend WithEvents tePercepcionRetencion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPerRet As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cePercepcionRetencion As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents sbBorrarFP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcFormaPago As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvFormaPago As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorPago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teIva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teSubTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
   
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcCorrelativo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents sbPedido As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents meCartaAduana As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents teFacturaAduana As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbCartaAduanera As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lbMensaje As DevExpress.XtraEditors.LabelControl
End Class
