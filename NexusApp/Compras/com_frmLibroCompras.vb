﻿
Imports NexusBLL
Public Class com_frmLibroCompras
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim blEmp As New AdmonBLL(g_ConnectionString)

    Private Sub frmLibroCompras_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        meMes.Month = Now.Month
        spAnio.EditValue = Now.Year
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")

    End Sub

    Private Sub SimpleButton1_Click() Handles Me.Reporte
        Dim dt, dtEmp As New DataTable
        dt = bl.GetLibroCompras(meMes.Month, spAnio.EditValue, leSucursal.EditValue)
        dtEmp = blEmp.ObtieneParametros
        If ceFormato2.Checked Then
            Dim rpt As New com_rptLibroCompras2() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlMes.Text = "MES: " + ObtieneMesString(meMes.EditValue)
            rpt.xrlEjercicio.Text = "AÑO: " & spAnio.EditValue

            If ceEncabezado.EditValue Then
                rpt.xrlTitulo.Text = teTitulo.EditValue
                rpt.xrlNombre.Text = "NOMBRE DEL CONTRIBUYENTE: " + gsNombre_Empresa
                rpt.xrlNIT.Text = "NIT: " + dtEmp.Rows(0).Item("NitEmpresa")
                rpt.xrlNRC.Text = "NRC: " + dtEmp.Rows(0).Item("NrcEmpresa")
                rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
            Else
                rpt.xrlTitulo.Text = ""
                rpt.xrlNombre.Text = ""
                rpt.xrlNIT.Text = ""
                rpt.xrlNRC.Text = ""
                rpt.xrlSucursal.Text = ""
            End If

            If ceFoliarHojas.EditValue Then
                rpt.xrFolio.StartPageNumber = seNumFolio.EditValue
            Else
                rpt.Parameters.Item("ImprimirFolio").Value = False
            End If
            rpt.ShowPreviewDialog()
        Else

            Dim rpt As New com_rptLibroCompras() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlMes.Text = "MES: " + ObtieneMesString(meMes.EditValue)
            rpt.xrlEjercicio.Text = "AÑO: " & spAnio.EditValue

            If ceEncabezado.EditValue Then
                rpt.xrlTitulo.Text = teTitulo.EditValue
                rpt.xrlNombre.Text = "NOMBRE DEL CONTRIBUYENTE: " + gsNombre_Empresa
                rpt.xrlNIT.Text = "NIT: " + dtEmp.Rows(0).Item("NitEmpresa")
                rpt.xrlNRC.Text = "NRC: " + dtEmp.Rows(0).Item("NrcEmpresa")
                rpt.xrlSucursal.Text = "SUCURSAL: " + leSucursal.Text
            Else
                rpt.xrlTitulo.Text = ""
                rpt.xrlNombre.Text = ""
                rpt.xrlNIT.Text = ""
                rpt.xrlNRC.Text = ""
                rpt.xrlSucursal.Text = ""
            End If

            If ceFoliarHojas.EditValue Then
                rpt.xrFolio.StartPageNumber = seNumFolio.EditValue
            Else
                rpt.Parameters.Item("ImprimirFolio").Value = False
            End If
            rpt.ShowPreviewDialog()
        End If
    End Sub


    Private Sub btExportXLS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btExportXLS.Click
        ExportarLibroExcel()
    End Sub

    Private Sub ExportarLibroExcel()
        If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_Compras.XLSX") Then
            If Not FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_Compras.XLS") Then
                MsgBox("No existe la plantilla necesaria para poder exportar", MsgBoxStyle.Critical, "Nota")
                Exit Sub
            End If
        End If

        Dim Template As String
        teProgreso.EditValue = "PREPARANDO LOS DATOS. ESPERE..."
        teProgreso.Visible = True
        If FileIO.FileSystem.FileExists(Application.StartupPath & "\Plantillas\Libro_Compras.XLSX") Then
            Template = Application.StartupPath & "\Plantillas\Libro_ComprasTmp.XLSX"
            FileCopy(Application.StartupPath & "\Plantillas\Libro_Compras.XLSX", Template)
        Else
            Template = Application.StartupPath & "\Plantillas\Libro_ComprasTmp.XLS"
            FileCopy(Application.StartupPath & "\Plantillas\Libro_Compras.XLS", Template)
        End If

        Dim oApp As Object = CreateObject("Excel.Application")

        'verifico si no está en una cultura diferente, por el momento de Inglés a Español, pendiente de verificar cuando sea a la inversa
        Dim oldCI As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        Try
            oApp.DisplayAlerts = False
        Catch ex As Exception
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")
            oApp.DisplayAlerts = False
        End Try

        Dim Range As String

        Dim dtParam As New DataTable
        dtParam = blEmp.ObtieneParametros()

        oApp.workbooks.Open(Template)
        oApp.Cells(3, 1).Value = "NOMBRE DEL CONTRIBUYENTE: " + gsNombre_Empresa
        oApp.Cells(3, 1).font.bold = True
        oApp.Cells(3, 8).Value = "NRC: " & dtParam.Rows(0).Item("NrcEmpresa")
        oApp.Cells(3, 8).font.bold = True
        oApp.Cells(3, 10).Value = "NIT: " & dtParam.Rows(0).Item("NitEmpresa")
        oApp.Cells(3, 10).font.bold = True

        oApp.Cells(4, 1).Value = "MES: " + (ObtieneMesString(meMes.EditValue)).ToUpper
        oApp.Cells(4, 1).font.bold = True
        oApp.Cells(4, 4).Value = "AÑO: " + (spAnio.EditValue).ToString
        oApp.Cells(4, 4).font.bold = True
        oApp.Cells(4, 10).Value = "FOLIO No. " & seNumFolio.EditValue
        oApp.Cells(4, 10).font.bold = True
        oApp.Cells(5, 1).Value = "SUCURSAL: " & leSucursal.Text
        oApp.Cells(5, 1).font.bold = True


        Dim TotExentoLocal As Decimal = 0.0, TotalExentoImport As Decimal = 0.0, TotExentoInterna As Decimal = 0.0
        Dim TotAfectoLocal As Decimal = 0.0, TotAfectoImport As Decimal = 0.0, TotAfectoInterna As Decimal = 0.0, TotIva1 As Decimal = 0.0
        Dim TotIva2 As Decimal = 0.0, TotIva3 As Decimal = 0.0, TotIva4 As Decimal = 0.0, TotExcluido As Decimal = 0.0, TotGeneral As Decimal = 0.0
        Dim Linea As Integer = 10
        Dim dt As DataTable = bl.GetLibroCompras(meMes.Month, spAnio.EditValue, leSucursal.EditValue)

        For Each row As DataRow In dt.Rows
            oApp.Cells(Linea, 1).Value = Linea + 1 - 10
            oApp.Cells(Linea, 2).Value = row.Item("Fecha")
            oApp.Cells(Linea, 3).Value = row.Item("Numero")
            oApp.Cells(Linea, 4).Value = row.Item("Nrc")
            oApp.Cells(Linea, 5).Value = row.Item("Nombre")
            oApp.Cells(Linea, 6).Value = row.Item("TotalExentoLocal")
            oApp.Cells(Linea, 7).Value = row.Item("TotalExentoImport")
            oApp.Cells(Linea, 8).Value = row.Item("TotalExentoInterna")
            oApp.Cells(Linea, 9).Value = row.Item("TotalAfectoLocal")
            oApp.Cells(Linea, 10).Value = row.Item("TotalAfectoImport")
            oApp.Cells(Linea, 11).Value = row.Item("TotalAfectoInterna")
            oApp.Cells(Linea, 12).Value = row.Item("TotalIVA")
            oApp.Cells(Linea, 13).Value = row.Item("Iva2")
            oApp.Cells(Linea, 14).Value = row.Item("Iva3")
            oApp.Cells(Linea, 15).Value = row.Item("Iva4")
            oApp.Cells(Linea, 16).Value = row.Item("TotalComprobante")
            oApp.Cells(Linea, 17).Value = row.Item("TotalExcluido")

            TotExentoLocal += SiEsNulo(row.Item("TotalExentoLocal"), 0)
            TotalExentoImport += SiEsNulo(row.Item("TotalExentoImport"), 0)
            TotExentoInterna += SiEsNulo(row.Item("TotalExentoInterna"), 0)
            TotAfectoLocal += SiEsNulo(row.Item("TotalAfectoLocal"), 0)
            TotAfectoImport += SiEsNulo(row.Item("TotalAfectoImport"), 0)
            TotAfectoInterna += SiEsNulo(row.Item("TotalAfectoInterna"), 0)
            TotIva1 += SiEsNulo(row.Item("TotalIVA"), 0)
            TotIva2 += SiEsNulo(row.Item("Iva2"), 0)
            TotIva3 += SiEsNulo(row.Item("Iva3"), 0)
            TotIva4 += SiEsNulo(row.Item("Iva4"), 0)
            TotExcluido += SiEsNulo(row.Item("TotalExcluido"), 0)
            TotGeneral += SiEsNulo(row.Item("TotalComprobante"), 0)

            Linea += 1
        Next
        PonerLineaExcel(oApp, Linea)

        oApp.Cells(Linea, 5).Value = "TOTALES:"
        oApp.Cells(Linea, 5).font.bold = True
        oApp.Cells(Linea, 5).font.Size = 8

        oApp.Cells(Linea, 6).Value = TotExentoLocal
        oApp.Cells(Linea, 6).font.bold = True
        oApp.Cells(Linea, 6).font.Size = 8
        oApp.Cells(Linea, 7).Value = TotalExentoImport
        oApp.Cells(Linea, 7).font.bold = True
        oApp.Cells(Linea, 7).font.Size = 8
        oApp.Cells(Linea, 8).Value = TotExentoInterna
        oApp.Cells(Linea, 8).font.bold = True
        oApp.Cells(Linea, 8).font.Size = 8
        oApp.Cells(Linea, 9).Value = TotAfectoLocal
        oApp.Cells(Linea, 9).font.bold = True
        oApp.Cells(Linea, 9).font.Size = 8
        oApp.Cells(Linea, 10).Value = TotAfectoImport
        oApp.Cells(Linea, 10).font.bold = True
        oApp.Cells(Linea, 10).font.Size = 8
        oApp.Cells(Linea, 11).Value = TotAfectoInterna
        oApp.Cells(Linea, 11).font.bold = True
        oApp.Cells(Linea, 11).font.Size = 8
        oApp.Cells(Linea, 12).Value = TotIva1
        oApp.Cells(Linea, 12).font.bold = True
        oApp.Cells(Linea, 12).font.Size = 8
        oApp.Cells(Linea, 13).Value = TotIva2
        oApp.Cells(Linea, 13).font.bold = True
        oApp.Cells(Linea, 13).font.Size = 8
        oApp.Cells(Linea, 14).Value = TotIva3
        oApp.Cells(Linea, 14).font.bold = True
        oApp.Cells(Linea, 14).font.Size = 8
        oApp.Cells(Linea, 15).Value = TotIva4
        oApp.Cells(Linea, 15).font.bold = True
        oApp.Cells(Linea, 15).font.Size = 8
        oApp.Cells(Linea, 16).Value = TotGeneral
        oApp.Cells(Linea, 16).font.bold = True
        oApp.Cells(Linea, 16).font.Size = 8
        oApp.Cells(Linea, 17).Value = TotExcluido
        oApp.Cells(Linea, 17).font.bold = True
        oApp.Cells(Linea, 17).font.Size = 8

        'Linea += 2
        'oApp.Cells(Linea, 2).Value = "NOMBRE Y FIRMA DEL CONTADOR"
        'oApp.Cells(Linea, 2).font.bold = True
        'Linea += 1


        'Linea += 2
        'oApp.Cells(Linea, 2).Value = "RESUMEN DE OPERACIONES: "
        'oApp.Cells(Linea, 2).font.bold = True
        'Linea += 1

        'oApp.Cells(Linea, 2).Value = "Ventas No Sujetas:"
        'oApp.Cells(Linea, 2).font.size = 8
        'oApp.Cells(Linea, 5).Value = 0.0
        'Linea += 1

        'oApp.Cells(Linea, 2).Value = "Ventas Exentas:"
        'oApp.Cells(Linea, 2).font.size = 8
        'oApp.Cells(Linea, 5).Value = TotExento
        'Linea += 1

        'oApp.Cells(Linea, 2).Value = "Ventas Afectas Netas:"
        'oApp.Cells(Linea, 2).font.size = 8
        'oApp.Cells(Linea, 5).Value = Round(TotAfecto / (pnIVA + 1), 2)
        'Linea += 1

        'oApp.Cells(Linea, 2).Value = "IVA 13%:"
        'oApp.Cells(Linea, 2).font.size = 8
        'oApp.Cells(Linea, 5).Value = TotAfecto - Round(TotAfecto / (pnIVA + 1), 2)
        'Linea += 1

        'oApp.Cells(Linea, 2).Value = "Exportaciones:"
        'oApp.Cells(Linea, 2).font.size = 8
        'oApp.Cells(Linea, 5).Value = TotExportacion
        'Linea += 1

        'oApp.Cells(Linea, 2).Value = "Ventas a Terceros:"
        'oApp.Cells(Linea, 2).font.size = 8
        'oApp.Cells(Linea, 5).Value = TotTerceros
        'Linea += 1

        'PonerLineaExcel3(oApp, Linea)

        'oApp.Cells(Linea, 2).Value = "TOTAL DE VENTAS:"
        'oApp.Cells(Linea, 2).font.bold = True
        'oApp.Cells(Linea, 2).font.size = 8
        'oApp.Cells(Linea, 5).Value = Total
        'oApp.Cells(Linea, 5).font.bold = True

        Linea += 3

        PonerLineaExcel4(oApp, Linea)
        oApp.Cells(Linea, 6).Value = "                            NOMBRE Y FIRMA DEL CONTADOR"
        oApp.Cells(Linea, 6).font.bold = True
        oApp.Cells(Linea, 6).font.size = 8


        Range = "$A$1:$Q$" + CStr(Linea)
        oApp.ActiveSheet.PageSetup.PrintArea = Range
        oApp.Range("A1").Select()

        'oApp.ActiveSheet.Protect("DrawingObjects:=False, Contents:=True, Scenarios:= False")
        'oApp.Save()
        teProgreso.Visible = False
        oApp.Visible = True
        System.Threading.Thread.CurrentThread.CurrentCulture = oldCI

    End Sub
    Private Sub PonerLineaExcel(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("A{0}:M{0}", CStr(Linea))
        oApp.Cells(7, 31).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
    Private Sub PonerLineaExcel2(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("B{0}:D{0}", CStr(Linea))
        oApp.Cells(6, 29).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
    Private Sub PonerLineaExcel3(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("B{0}:E{0}", CStr(Linea))
        oApp.Cells(6, 29).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
    Private Sub PonerLineaExcel4(ByVal oApp As Object, ByVal Linea As Integer)
        Dim Range As String = String.Format("G{0}:I{0}", CStr(Linea))
        oApp.Cells(6, 29).Copy()
        oApp.Range(Range).Select()
        oApp.ActiveSheet.Paste()
    End Sub
End Class
