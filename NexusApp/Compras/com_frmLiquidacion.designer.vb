﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class com_frmLiquidacion
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xtcDocs = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage()
        Me.gc2 = New DevExpress.XtraGrid.GridControl()
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.gcCCF = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.leClaseDocumentoCCF = New DevExpress.XtraEditors.LookUpEdit()
        Me.teNumeroControlInternoFormularioUnicoCCF = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.teResolucionCCF = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.TeSerieCF = New DevExpress.XtraEditors.TextEdit()
        Me.TeNumeroCF = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.leClaseDocumento = New DevExpress.XtraEditors.LookUpEdit()
        Me.teNumeroControlInternoFormularioUnico = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.teResolucion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.sbImportar = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TeExento = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.leCtaBancaria = New DevExpress.XtraEditors.LookUpEdit()
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNRC = New DevExpress.XtraEditors.TextEdit()
        Me.teComision = New DevExpress.XtraEditors.TextEdit()
        Me.deFechaContable = New DevExpress.XtraEditors.DateEdit()
        Me.teNIT = New DevExpress.XtraEditors.TextEdit()
        Me.deFechaCompra = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.teIva = New DevExpress.XtraEditors.TextEdit()
        Me.lblPerRet = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.teSerie = New DevExpress.XtraEditors.TextEdit()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.beProveedor = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.teTotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.teDireccion = New DevExpress.XtraEditors.TextEdit()
        CType(Me.xtcDocs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcDocs.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.gcCCF, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcCCF.SuspendLayout()
        CType(Me.leClaseDocumentoCCF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumeroControlInternoFormularioUnicoCCF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teResolucionCCF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeSerieCF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeNumeroCF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leClaseDocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumeroControlInternoFormularioUnico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teResolucion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TeExento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaContable.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaContable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaCompra.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcDocs
        '
        Me.xtcDocs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcDocs.Location = New System.Drawing.Point(0, 0)
        Me.xtcDocs.Name = "xtcDocs"
        Me.xtcDocs.SelectedTabPage = Me.xtpLista
        Me.xtcDocs.Size = New System.Drawing.Size(855, 522)
        Me.xtcDocs.TabIndex = 52
        Me.xtcDocs.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc2)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(849, 494)
        Me.xtpLista.Text = "Consulta Docs. Liquidación"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(849, 494)
        Me.gc2.TabIndex = 3
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn3, Me.GridColumn4, Me.GridColumn8, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn2, Me.GridColumn9, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12, Me.GridColumn13})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdComprobante"
        Me.GridColumn1.FieldName = "IdComprobante"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 59
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Numero"
        Me.GridColumn3.FieldName = "Numero"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        Me.GridColumn3.Width = 85
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Fecha Contable"
        Me.GridColumn4.FieldName = "FechaContable"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 2
        Me.GridColumn4.Width = 81
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Fecha"
        Me.GridColumn8.FieldName = "Fecha"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 3
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Proveedor"
        Me.GridColumn5.FieldName = "Nombre"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 4
        Me.GridColumn5.Width = 440
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Sucursal"
        Me.GridColumn6.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn6.FieldName = "IdSucursal"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 5
        Me.GridColumn6.Width = 98
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Num. Registro"
        Me.GridColumn7.FieldName = "Nrc"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 6
        Me.GridColumn7.Width = 114
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "NIT"
        Me.GridColumn2.FieldName = "Nit"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 7
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Total Iva"
        Me.GridColumn9.FieldName = "TotalIva"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 8
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "TotalComprobante"
        Me.GridColumn10.FieldName = "TotalComprobante"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 9
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Comisión"
        Me.GridColumn11.FieldName = "TotalComision"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 10
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Creado Por"
        Me.GridColumn12.FieldName = "CreadoPor"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 11
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "FechaHoraCreacion"
        Me.GridColumn13.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.GridColumn13.FieldName = "FechaHoraCreacion"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 12
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemDateEdit1.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.pcHeader)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(849, 494)
        Me.xtpDatos.Text = "Docs. Liquidación"
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.gcCCF)
        Me.pcHeader.Controls.Add(Me.LabelControl28)
        Me.pcHeader.Controls.Add(Me.leClaseDocumento)
        Me.pcHeader.Controls.Add(Me.teNumeroControlInternoFormularioUnico)
        Me.pcHeader.Controls.Add(Me.LabelControl29)
        Me.pcHeader.Controls.Add(Me.teResolucion)
        Me.pcHeader.Controls.Add(Me.LabelControl27)
        Me.pcHeader.Controls.Add(Me.sbImportar)
        Me.pcHeader.Controls.Add(Me.LabelControl4)
        Me.pcHeader.Controls.Add(Me.TeExento)
        Me.pcHeader.Controls.Add(Me.LabelControl3)
        Me.pcHeader.Controls.Add(Me.leCtaBancaria)
        Me.pcHeader.Controls.Add(Me.teCorrelativo)
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.LabelControl9)
        Me.pcHeader.Controls.Add(Me.LabelControl24)
        Me.pcHeader.Controls.Add(Me.LabelControl11)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.teNRC)
        Me.pcHeader.Controls.Add(Me.teComision)
        Me.pcHeader.Controls.Add(Me.deFechaContable)
        Me.pcHeader.Controls.Add(Me.teNIT)
        Me.pcHeader.Controls.Add(Me.deFechaCompra)
        Me.pcHeader.Controls.Add(Me.LabelControl14)
        Me.pcHeader.Controls.Add(Me.teIva)
        Me.pcHeader.Controls.Add(Me.lblPerRet)
        Me.pcHeader.Controls.Add(Me.LabelControl15)
        Me.pcHeader.Controls.Add(Me.teSerie)
        Me.pcHeader.Controls.Add(Me.teNombre)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Controls.Add(Me.beProveedor)
        Me.pcHeader.Controls.Add(Me.LabelControl12)
        Me.pcHeader.Controls.Add(Me.teTotal)
        Me.pcHeader.Controls.Add(Me.LabelControl5)
        Me.pcHeader.Controls.Add(Me.LabelControl13)
        Me.pcHeader.Controls.Add(Me.LabelControl8)
        Me.pcHeader.Controls.Add(Me.LabelControl7)
        Me.pcHeader.Controls.Add(Me.teNumero)
        Me.pcHeader.Controls.Add(Me.teDireccion)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(849, 494)
        Me.pcHeader.TabIndex = 88
        '
        'gcCCF
        '
        Me.gcCCF.Controls.Add(Me.LabelControl17)
        Me.gcCCF.Controls.Add(Me.leClaseDocumentoCCF)
        Me.gcCCF.Controls.Add(Me.teNumeroControlInternoFormularioUnicoCCF)
        Me.gcCCF.Controls.Add(Me.LabelControl10)
        Me.gcCCF.Controls.Add(Me.teResolucionCCF)
        Me.gcCCF.Controls.Add(Me.LabelControl16)
        Me.gcCCF.Controls.Add(Me.TeSerieCF)
        Me.gcCCF.Controls.Add(Me.TeNumeroCF)
        Me.gcCCF.Controls.Add(Me.LabelControl6)
        Me.gcCCF.Location = New System.Drawing.Point(132, 85)
        Me.gcCCF.Name = "gcCCF"
        Me.gcCCF.Size = New System.Drawing.Size(710, 71)
        Me.gcCCF.TabIndex = 156
        Me.gcCCF.Text = "Datos del Comprobante de Crédito Fiscal"
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(164, 48)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl17.TabIndex = 160
        Me.LabelControl17.Text = "Clase de Documento:"
        '
        'leClaseDocumentoCCF
        '
        Me.leClaseDocumentoCCF.Location = New System.Drawing.Point(269, 45)
        Me.leClaseDocumentoCCF.Name = "leClaseDocumentoCCF"
        Me.leClaseDocumentoCCF.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leClaseDocumentoCCF.Size = New System.Drawing.Size(174, 20)
        Me.leClaseDocumentoCCF.TabIndex = 3
        '
        'teNumeroControlInternoFormularioUnicoCCF
        '
        Me.teNumeroControlInternoFormularioUnicoCCF.EnterMoveNextControl = True
        Me.teNumeroControlInternoFormularioUnicoCCF.Location = New System.Drawing.Point(569, 43)
        Me.teNumeroControlInternoFormularioUnicoCCF.Name = "teNumeroControlInternoFormularioUnicoCCF"
        Me.teNumeroControlInternoFormularioUnicoCCF.Size = New System.Drawing.Size(134, 20)
        Me.teNumeroControlInternoFormularioUnicoCCF.TabIndex = 4
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(447, 46)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(122, 13)
        Me.LabelControl10.TabIndex = 158
        Me.LabelControl10.Text = "No. Control Interno F.U.:"
        '
        'teResolucionCCF
        '
        Me.teResolucionCCF.EnterMoveNextControl = True
        Me.teResolucionCCF.Location = New System.Drawing.Point(569, 21)
        Me.teResolucionCCF.Name = "teResolucionCCF"
        Me.teResolucionCCF.Size = New System.Drawing.Size(134, 20)
        Me.teResolucionCCF.TabIndex = 2
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(492, 25)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl16.TabIndex = 157
        Me.LabelControl16.Text = "No. Resolución:"
        '
        'TeSerieCF
        '
        Me.TeSerieCF.EnterMoveNextControl = True
        Me.TeSerieCF.Location = New System.Drawing.Point(269, 23)
        Me.TeSerieCF.Name = "TeSerieCF"
        Me.TeSerieCF.Size = New System.Drawing.Size(87, 20)
        Me.TeSerieCF.TabIndex = 0
        '
        'TeNumeroCF
        '
        Me.TeNumeroCF.EnterMoveNextControl = True
        Me.TeNumeroCF.Location = New System.Drawing.Point(358, 23)
        Me.TeNumeroCF.Name = "TeNumeroCF"
        Me.TeNumeroCF.Size = New System.Drawing.Size(85, 20)
        Me.TeNumeroCF.TabIndex = 1
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(132, 27)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(131, 13)
        Me.LabelControl6.TabIndex = 95
        Me.LabelControl6.Text = "Serie/No. de Credito Fiscal:"
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(296, 43)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl28.TabIndex = 155
        Me.LabelControl28.Text = "Clase de Documento:"
        '
        'leClaseDocumento
        '
        Me.leClaseDocumento.Location = New System.Drawing.Point(401, 40)
        Me.leClaseDocumento.Name = "leClaseDocumento"
        Me.leClaseDocumento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leClaseDocumento.Size = New System.Drawing.Size(174, 20)
        Me.leClaseDocumento.TabIndex = 4
        '
        'teNumeroControlInternoFormularioUnico
        '
        Me.teNumeroControlInternoFormularioUnico.EnterMoveNextControl = True
        Me.teNumeroControlInternoFormularioUnico.Location = New System.Drawing.Point(701, 40)
        Me.teNumeroControlInternoFormularioUnico.Name = "teNumeroControlInternoFormularioUnico"
        Me.teNumeroControlInternoFormularioUnico.Size = New System.Drawing.Size(134, 20)
        Me.teNumeroControlInternoFormularioUnico.TabIndex = 5
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(579, 43)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(122, 13)
        Me.LabelControl29.TabIndex = 154
        Me.LabelControl29.Text = "No. Control Interno F.U.:"
        '
        'teResolucion
        '
        Me.teResolucion.EnterMoveNextControl = True
        Me.teResolucion.Location = New System.Drawing.Point(701, 18)
        Me.teResolucion.Name = "teResolucion"
        Me.teResolucion.Size = New System.Drawing.Size(134, 20)
        Me.teResolucion.TabIndex = 2
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(624, 22)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl27.TabIndex = 153
        Me.LabelControl27.Text = "No. Resolución:"
        '
        'sbImportar
        '
        Me.sbImportar.AllowFocus = False
        Me.sbImportar.Location = New System.Drawing.Point(401, 264)
        Me.sbImportar.Name = "sbImportar"
        Me.sbImportar.Size = New System.Drawing.Size(112, 23)
        Me.sbImportar.TabIndex = 92
        Me.sbImportar.Text = "Importar Archivo"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(64, 339)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl4.TabIndex = 91
        Me.LabelControl4.Text = "Valor Exento:"
        '
        'TeExento
        '
        Me.TeExento.EditValue = 0
        Me.TeExento.Location = New System.Drawing.Point(132, 336)
        Me.TeExento.Name = "TeExento"
        Me.TeExento.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.TeExento.Properties.Appearance.Options.UseBackColor = True
        Me.TeExento.Properties.Appearance.Options.UseTextOptions = True
        Me.TeExento.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.TeExento.Properties.Mask.EditMask = "n2"
        Me.TeExento.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TeExento.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TeExento.Properties.ReadOnly = True
        Me.TeExento.Size = New System.Drawing.Size(129, 20)
        Me.TeExento.TabIndex = 17
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(296, 229)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl3.TabIndex = 89
        Me.LabelControl3.Text = "Banco/Cta. Bancaria:"
        '
        'leCtaBancaria
        '
        Me.leCtaBancaria.EnterMoveNextControl = True
        Me.leCtaBancaria.Location = New System.Drawing.Point(401, 226)
        Me.leCtaBancaria.Name = "leCtaBancaria"
        Me.leCtaBancaria.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCtaBancaria.Size = New System.Drawing.Size(437, 20)
        Me.leCtaBancaria.TabIndex = 13
        '
        'teCorrelativo
        '
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(132, 18)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Properties.ReadOnly = True
        Me.teCorrelativo.Size = New System.Drawing.Size(130, 20)
        Me.teCorrelativo.TabIndex = 0
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(401, 203)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(437, 20)
        Me.leSucursal.TabIndex = 11
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(104, 209)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl9.TabIndex = 69
        Me.LabelControl9.Text = "NRC:"
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(355, 206)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl24.TabIndex = 87
        Me.LabelControl24.Text = "Sucursal:"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(108, 232)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl11.TabIndex = 70
        Me.LabelControl11.Text = "NIT:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(83, 314)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl2.TabIndex = 77
        Me.LabelControl2.Text = "Comisión:"
        '
        'teNRC
        '
        Me.teNRC.EnterMoveNextControl = True
        Me.teNRC.Location = New System.Drawing.Point(132, 208)
        Me.teNRC.Name = "teNRC"
        Me.teNRC.Size = New System.Drawing.Size(129, 20)
        Me.teNRC.TabIndex = 10
        '
        'teComision
        '
        Me.teComision.EditValue = 0
        Me.teComision.Location = New System.Drawing.Point(132, 310)
        Me.teComision.Name = "teComision"
        Me.teComision.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.teComision.Properties.Appearance.Options.UseBackColor = True
        Me.teComision.Properties.Appearance.Options.UseTextOptions = True
        Me.teComision.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teComision.Properties.Mask.EditMask = "n2"
        Me.teComision.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teComision.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teComision.Properties.ReadOnly = True
        Me.teComision.Size = New System.Drawing.Size(129, 20)
        Me.teComision.TabIndex = 16
        '
        'deFechaContable
        '
        Me.deFechaContable.EditValue = Nothing
        Me.deFechaContable.EnterMoveNextControl = True
        Me.deFechaContable.Location = New System.Drawing.Point(132, 62)
        Me.deFechaContable.Name = "deFechaContable"
        Me.deFechaContable.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaContable.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaContable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaContable.Size = New System.Drawing.Size(129, 20)
        Me.deFechaContable.TabIndex = 6
        '
        'teNIT
        '
        Me.teNIT.EnterMoveNextControl = True
        Me.teNIT.Location = New System.Drawing.Point(132, 229)
        Me.teNIT.Name = "teNIT"
        Me.teNIT.Size = New System.Drawing.Size(129, 20)
        Me.teNIT.TabIndex = 12
        '
        'deFechaCompra
        '
        Me.deFechaCompra.EditValue = Nothing
        Me.deFechaCompra.EnterMoveNextControl = True
        Me.deFechaCompra.Location = New System.Drawing.Point(132, 41)
        Me.deFechaCompra.Name = "deFechaCompra"
        Me.deFechaCompra.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaCompra.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaCompra.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaCompra.Size = New System.Drawing.Size(130, 20)
        Me.deFechaCompra.TabIndex = 3
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(50, 64)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl14.TabIndex = 73
        Me.LabelControl14.Text = "Fecha Contable:"
        '
        'teIva
        '
        Me.teIva.EditValue = 0
        Me.teIva.Location = New System.Drawing.Point(132, 284)
        Me.teIva.Name = "teIva"
        Me.teIva.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.teIva.Properties.Appearance.Options.UseBackColor = True
        Me.teIva.Properties.Appearance.Options.UseTextOptions = True
        Me.teIva.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teIva.Properties.Mask.EditMask = "n2"
        Me.teIva.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teIva.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teIva.Properties.ReadOnly = True
        Me.teIva.Size = New System.Drawing.Size(129, 20)
        Me.teIva.TabIndex = 15
        '
        'lblPerRet
        '
        Me.lblPerRet.Location = New System.Drawing.Point(27, 264)
        Me.lblPerRet.Name = "lblPerRet"
        Me.lblPerRet.Size = New System.Drawing.Size(102, 13)
        Me.lblPerRet.TabIndex = 55
        Me.lblPerRet.Text = "Valor del Documento:"
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(72, 21)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl15.TabIndex = 74
        Me.LabelControl15.Text = "Correlativo:"
        '
        'teSerie
        '
        Me.teSerie.EnterMoveNextControl = True
        Me.teSerie.Location = New System.Drawing.Point(401, 18)
        Me.teSerie.Name = "teSerie"
        Me.teSerie.Size = New System.Drawing.Size(87, 20)
        Me.teSerie.TabIndex = 0
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(401, 161)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(437, 20)
        Me.teNombre.TabIndex = 8
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(5, 44)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(125, 13)
        Me.LabelControl1.TabIndex = 60
        Me.LabelControl1.Text = "Fecha de Compra (Libro) :"
        '
        'beProveedor
        '
        Me.beProveedor.EnterMoveNextControl = True
        Me.beProveedor.Location = New System.Drawing.Point(132, 161)
        Me.beProveedor.Name = "beProveedor"
        Me.beProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beProveedor.Size = New System.Drawing.Size(130, 20)
        Me.beProveedor.TabIndex = 7
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(277, 21)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(121, 13)
        Me.LabelControl12.TabIndex = 71
        Me.LabelControl12.Text = "Serie/No. de Documento:"
        '
        'teTotal
        '
        Me.teTotal.EditValue = 0
        Me.teTotal.Location = New System.Drawing.Point(132, 261)
        Me.teTotal.Name = "teTotal"
        Me.teTotal.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.teTotal.Properties.Appearance.Options.UseBackColor = True
        Me.teTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotal.Properties.Mask.EditMask = "n2"
        Me.teTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTotal.Properties.ReadOnly = True
        Me.teTotal.Size = New System.Drawing.Size(129, 20)
        Me.teTotal.TabIndex = 14
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(73, 286)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl5.TabIndex = 56
        Me.LabelControl5.Text = "Percepción:"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(75, 164)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl13.TabIndex = 72
        Me.LabelControl13.Text = "Proveedor:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(82, 186)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl8.TabIndex = 68
        Me.LabelControl8.Text = "Dirección:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(357, 164)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl7.TabIndex = 67
        Me.LabelControl7.Text = "Nombre:"
        '
        'teNumero
        '
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(490, 18)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(85, 20)
        Me.teNumero.TabIndex = 1
        '
        'teDireccion
        '
        Me.teDireccion.EnterMoveNextControl = True
        Me.teDireccion.Location = New System.Drawing.Point(132, 182)
        Me.teDireccion.Name = "teDireccion"
        Me.teDireccion.Size = New System.Drawing.Size(706, 20)
        Me.teDireccion.TabIndex = 9
        '
        'com_frmLiquidacion
        '
        Me.ClientSize = New System.Drawing.Size(855, 547)
        Me.Controls.Add(Me.xtcDocs)
        Me.Modulo = "Compras"
        Me.Name = "com_frmLiquidacion"
        Me.OptionId = "002006"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Documentos de Liquidación"
        Me.Controls.SetChildIndex(Me.xtcDocs, 0)
        CType(Me.xtcDocs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcDocs.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.gcCCF, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcCCF.ResumeLayout(False)
        Me.gcCCF.PerformLayout()
        CType(Me.leClaseDocumentoCCF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumeroControlInternoFormularioUnicoCCF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teResolucionCCF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeSerieCF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeNumeroCF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leClaseDocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumeroControlInternoFormularioUnico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teResolucion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TeExento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCtaBancaria.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaContable.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaContable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaCompra.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents xtcDocs As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teComision As DevExpress.XtraEditors.TextEdit
    Public WithEvents deFechaCompra As DevExpress.XtraEditors.DateEdit
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPerRet As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Public WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTotal As DevExpress.XtraEditors.TextEdit
    Public WithEvents beProveedor As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNIT As DevExpress.XtraEditors.TextEdit
    Public WithEvents deFechaContable As DevExpress.XtraEditors.DateEdit
    Friend WithEvents teNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCtaBancaria As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TeExento As DevExpress.XtraEditors.TextEdit
    Friend WithEvents sbImportar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TeSerieCF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Public WithEvents TeNumeroCF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leClaseDocumento As DevExpress.XtraEditors.LookUpEdit
    Public WithEvents teNumeroControlInternoFormularioUnico As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Public WithEvents teResolucion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcCCF As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leClaseDocumentoCCF As DevExpress.XtraEditors.LookUpEdit
    Public WithEvents teNumeroControlInternoFormularioUnicoCCF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Public WithEvents teResolucionCCF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
End Class
