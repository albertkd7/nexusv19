﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class com_frmAprobacionOrdenCompra
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim dtParam As DataTable = blAdmon.ObtieneParametros()
    Dim entUsu As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub fac_frmAprobacionCotizacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        gc.DataSource = bl.com_ConsultaOrdenCompra(Today, Today)
        gv.BestFitColumns()
    End Sub


    Private Sub btAprobar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAprobar.Click
        Dim IdDoc As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
        If IdDoc = 0 Then
            MsgBox("Es necesario selecionar una Orden de Compra", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        If MsgBox("¿Está seguro(a) de aprobar la Orden de Compra?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            bl.com_AprobarOrdenCompra(IdDoc, 1, "", objMenu.User)
            gc.DataSource = bl.com_ConsultaOrdenCompra(deDesde.EditValue, deHasta.EditValue)
            gv.BestFitColumns()
        End If

    End Sub

    'Private Sub sbReprobar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbReprobar.Click
    '    Dim IdDoc As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
    '    If IdDoc = 0 Then
    '        MsgBox("Es necesario selecionar una Cotización", MsgBoxStyle.Information, "Nota")
    '        Exit Sub
    '    End If
    '    blFac.fac_ActualizaEstadoCotizacion(IdDoc, 3)
    '    gc.DataSource = blFac.fac_ConsultaCotizaciones(deDesde.EditValue, deHasta.EditValue, leEstado.EditValue)
    '    gv.BestFitColumns()
    'End Sub

    Private Sub btDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDetalle.Click
        Dim IdDoc As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
        If IdDoc = 0 Then
            MsgBox("Es necesario selecionar una Orden de Compra", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Dim dt As DataTable = bl.com_ObtenerOrdenCompra(IdDoc, 1)
        Dim entOrden As com_OrdenesCompra = objTablas.com_OrdenesCompraSelectByPK(IdDoc)
        Dim rpt As New com_rptOrdenCompra() With {.DataSource = dt, .DataMember = ""}

        Dim dtPago As DataTable = bl.com_ObtenerOrdenCompraFormaPago(IdDoc)

        If dtPago.Rows.Count > 0 Then
            rpt.XrSubreport1.Visible = True
            rpt.XrSubreport1.ReportSource.DataSource = dtPago
            rpt.XrSubreport1.ReportSource.DataMember = ""
        Else
            rpt.XrSubreport1.Visible = False
            rpt.XrSubreport1.ReportSource.DataSource = Nothing
            rpt.XrSubreport1.ReportSource.DataMember = ""
        End If

        rpt.xrLogo.Image = ByteToImagen(dtParam.Rows(0).Item("LogoEmpresa"))
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlDireccionEmpresa.Text = dtParam.Rows(0).Item("Direccion")
        rpt.xrlNitEmpresa.Text = dtParam.Rows(0).Item("NitEmpresa")
        rpt.xrlNrcEmpresa.Text = dtParam.Rows(0).Item("NrcEmpresa")
        rpt.xrlGiroEmpresa.Text = dtParam.Rows(0).Item("ActividadEconomica")

        rpt.xrlEmail.Text = entOrden.EmailEnvioFactura
        rpt.xrlEnviarFactura.Text = entOrden.EnviarFacturaA
        rpt.xrlDirecionEnvioFactura.Text = entOrden.DireccionEnvioFactura
        rpt.xrlSolicitadoPor.Text = entOrden.SolicitadoPor
        rpt.xrlLugarEntrega.Text = entOrden.LugarEntrega
        rpt.xrlDireccionEntrega.Text = entOrden.DireccionEntrega
        rpt.xrlAutorizado.Text = entOrden.AprobadaPor
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbReprobar.Click
        Dim IdDoc As Integer = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
        If IdDoc = 0 Then
            MsgBox("Es necesario selecionar una Orden de Compra", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If


        If MsgBox("¿Está seguro(a) de reprobar la Orden de Compra?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then

            Dim Comentario As String = InputBox("Comentario:", "Reprobar Orden Compra", "")

            bl.com_AprobarOrdenCompra(IdDoc, 0, Comentario, objMenu.User)
            gc.DataSource = bl.com_ConsultaOrdenCompra(deDesde.EditValue, deHasta.EditValue)
            gv.BestFitColumns()
        End If

    End Sub

    Private Sub sbObtener_Click(sender As Object, e As EventArgs) Handles sbObtener.Click
        gc.DataSource = bl.com_ConsultaOrdenCompra(deDesde.EditValue, deHasta.EditValue)
        gv.BestFitColumns()
    End Sub
End Class
