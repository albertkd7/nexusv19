﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class com_frmCompras
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xtcCompras = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLista1 = New DevExpress.XtraTab.XtraTabPage()
        Me.gc2 = New DevExpress.XtraGrid.GridControl()
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcAplicada = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.xtpDatos1 = New DevExpress.XtraTab.XtraTabPage()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdProducto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCodigo = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcCantidad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcDescripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPrecioReferencia = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ritePrecio = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcPorcDescto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteDecimal = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcValorDescto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcPrecioUnitario = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ritePrecioFinal = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcValorExento = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcValorAfecto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdCuenta = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteIdCuenta = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leCentroCosto = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcTipoImpuesto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteCantidad2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.pcTotales = New DevExpress.XtraEditors.PanelControl()
        Me.rgTipo = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.lblNombreCuenta = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.teImpuestos = New DevExpress.XtraEditors.TextEdit()
        Me.sbImpuestos = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.lblPerRet = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.btRevertir = New DevExpress.XtraEditors.SimpleButton()
        Me.teTotal = New DevExpress.XtraEditors.TextEdit()
        Me.tePercepcionRetencion = New DevExpress.XtraEditors.TextEdit()
        Me.btAplicar = New DevExpress.XtraEditors.SimpleButton()
        Me.teIva = New DevExpress.XtraEditors.TextEdit()
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.sbDetallar = New DevExpress.XtraEditors.SimpleButton()
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.leClaseDocumento = New DevExpress.XtraEditors.LookUpEdit()
        Me.teNumeroControlInternoFormularioUnico = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.teResolucion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.sbCopiaCompras = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdPartida = New DevExpress.XtraEditors.TextEdit()
        Me.teIdCheque = New DevExpress.XtraEditors.TextEdit()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.ceRebajaCosto = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.leCaja = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.seDiasCredito = New DevExpress.XtraEditors.SpinEdit()
        Me.beOrden = New DevExpress.XtraEditors.SimpleButton()
        Me.teOrdenCompra = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaCompra = New DevExpress.XtraEditors.DateEdit()
        Me.ceExcluirLibroCompras = New DevExpress.XtraEditors.CheckEdit()
        Me.leFormaPago = New DevExpress.XtraEditors.LookUpEdit()
        Me.ceExcluido = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teNumero = New DevExpress.XtraEditors.TextEdit()
        Me.deFechaVto = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.teNIT = New DevExpress.XtraEditors.TextEdit()
        Me.teNRC = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.deFechaContable = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.teDocExcluido = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.leOrigenCompra = New DevExpress.XtraEditors.LookUpEdit()
        Me.leBodega = New DevExpress.XtraEditors.LookUpEdit()
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.beProveedor = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoCompra = New DevExpress.XtraEditors.LookUpEdit()
        Me.teSerie = New DevExpress.XtraEditors.TextEdit()
        Me.leTipoDocto = New DevExpress.XtraEditors.LookUpEdit()
        CType(Me.xtcCompras, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcCompras.SuspendLayout()
        Me.xtpLista1.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteDecimal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ritePrecioFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteIdCuenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCentroCosto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCantidad2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcTotales.SuspendLayout()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teImpuestos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tePercepcionRetencion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.leClaseDocumento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumeroControlInternoFormularioUnico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teResolucion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceRebajaCosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCaja.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teOrdenCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaCompra.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceExcluirLibroCompras.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceExcluido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaVto.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaVto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaContable.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFechaContable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDocExcluido.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leOrigenCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoCompra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoDocto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcCompras
        '
        Me.xtcCompras.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcCompras.Location = New System.Drawing.Point(0, 0)
        Me.xtcCompras.Name = "xtcCompras"
        Me.xtcCompras.SelectedTabPage = Me.xtpLista1
        Me.xtcCompras.Size = New System.Drawing.Size(1206, 623)
        Me.xtcCompras.TabIndex = 0
        Me.xtcCompras.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista1, Me.xtpDatos1})
        '
        'xtpLista1
        '
        Me.xtpLista1.Controls.Add(Me.gc2)
        Me.xtpLista1.Name = "xtpLista1"
        Me.xtpLista1.Size = New System.Drawing.Size(1200, 595)
        Me.xtpLista1.Text = "Lista"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit1, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(1200, 595)
        Me.gc2.TabIndex = 2
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn11, Me.GridColumn4, Me.GridColumn8, Me.GridColumn5, Me.gcAplicada, Me.GridColumn10, Me.GridColumn6, Me.GridColumn7})
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Correlativo"
        Me.GridColumn1.FieldName = "IdComprobante"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 58
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Serie"
        Me.GridColumn2.FieldName = "Serie"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 57
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Numero"
        Me.GridColumn3.FieldName = "Numero"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 72
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Resolución"
        Me.GridColumn11.FieldName = "Giro"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 3
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Fecha"
        Me.GridColumn4.FieldName = "Fecha"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 4
        Me.GridColumn4.Width = 68
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Sucursal"
        Me.GridColumn8.ColumnEdit = Me.leSucursalDetalle
        Me.GridColumn8.FieldName = "IdSucursal"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 5
        Me.GridColumn8.Width = 63
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Proveedor"
        Me.GridColumn5.FieldName = "Proveedor"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 6
        Me.GridColumn5.Width = 341
        '
        'gcAplicada
        '
        Me.gcAplicada.Caption = "Aplicada"
        Me.gcAplicada.FieldName = "AplicadaInventario"
        Me.gcAplicada.Name = "gcAplicada"
        Me.gcAplicada.Visible = True
        Me.gcAplicada.VisibleIndex = 7
        Me.gcAplicada.Width = 54
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Total Comprobante"
        Me.GridColumn10.FieldName = "TotalComprobante"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 8
        Me.GridColumn10.Width = 101
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Creado Por"
        Me.GridColumn6.FieldName = "CreadoPor"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 9
        Me.GridColumn6.Width = 86
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Fecha Hora Creacion"
        Me.GridColumn7.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.GridColumn7.FieldName = "FechaHoraCreacion"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 10
        Me.GridColumn7.Width = 115
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemDateEdit1.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'xtpDatos1
        '
        Me.xtpDatos1.Controls.Add(Me.gc)
        Me.xtpDatos1.Controls.Add(Me.pcTotales)
        Me.xtpDatos1.Controls.Add(Me.pcBotones)
        Me.xtpDatos1.Controls.Add(Me.pcHeader)
        Me.xtpDatos1.Name = "xtpDatos1"
        Me.xtpDatos1.Size = New System.Drawing.Size(1200, 595)
        Me.xtpDatos1.Text = "Compras"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 157)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riteCantidad, Me.ritePrecio, Me.riteDecimal, Me.riteCodigo, Me.riteIdCuenta, Me.riteCantidad2, Me.ritePrecioFinal, Me.leCentroCosto})
        Me.gc.Size = New System.Drawing.Size(1166, 351)
        Me.gc.TabIndex = 59
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdProducto, Me.gcCantidad, Me.gcDescripcion, Me.gcPrecioReferencia, Me.gcPorcDescto, Me.gcValorDescto, Me.gcPrecioUnitario, Me.gcValorExento, Me.gcValorAfecto, Me.gcIdCuenta, Me.GridColumn9, Me.gcTipoImpuesto})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.gv.OptionsNavigation.AutoFocusNewRow = True
        Me.gv.OptionsNavigation.EnterMoveNextColumn = True
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdProducto
        '
        Me.gcIdProducto.Caption = "Cód.Producto"
        Me.gcIdProducto.ColumnEdit = Me.riteCodigo
        Me.gcIdProducto.FieldName = "IdProducto"
        Me.gcIdProducto.Name = "gcIdProducto"
        Me.gcIdProducto.Visible = True
        Me.gcIdProducto.VisibleIndex = 0
        Me.gcIdProducto.Width = 78
        '
        'riteCodigo
        '
        Me.riteCodigo.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCodigo.AutoHeight = False
        Me.riteCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.riteCodigo.Name = "riteCodigo"
        '
        'gcCantidad
        '
        Me.gcCantidad.Caption = "Cantidad"
        Me.gcCantidad.ColumnEdit = Me.riteCantidad
        Me.gcCantidad.DisplayFormat.FormatString = "n3"
        Me.gcCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcCantidad.FieldName = "Cantidad"
        Me.gcCantidad.Name = "gcCantidad"
        Me.gcCantidad.Visible = True
        Me.gcCantidad.VisibleIndex = 1
        Me.gcCantidad.Width = 58
        '
        'riteCantidad
        '
        Me.riteCantidad.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCantidad.AutoHeight = False
        Me.riteCantidad.DisplayFormat.FormatString = "n3"
        Me.riteCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteCantidad.EditFormat.FormatString = "n3"
        Me.riteCantidad.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteCantidad.Mask.EditMask = "n3"
        Me.riteCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.riteCantidad.Name = "riteCantidad"
        Me.riteCantidad.NullText = "0"
        '
        'gcDescripcion
        '
        Me.gcDescripcion.Caption = "Concepto"
        Me.gcDescripcion.FieldName = "Descripcion"
        Me.gcDescripcion.Name = "gcDescripcion"
        Me.gcDescripcion.Visible = True
        Me.gcDescripcion.VisibleIndex = 2
        Me.gcDescripcion.Width = 234
        '
        'gcPrecioReferencia
        '
        Me.gcPrecioReferencia.Caption = "Precio Referencia"
        Me.gcPrecioReferencia.ColumnEdit = Me.ritePrecio
        Me.gcPrecioReferencia.DisplayFormat.FormatString = "n6"
        Me.gcPrecioReferencia.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioReferencia.FieldName = "PrecioReferencia"
        Me.gcPrecioReferencia.Name = "gcPrecioReferencia"
        Me.gcPrecioReferencia.Visible = True
        Me.gcPrecioReferencia.VisibleIndex = 3
        Me.gcPrecioReferencia.Width = 103
        '
        'ritePrecio
        '
        Me.ritePrecio.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.ritePrecio.AutoHeight = False
        Me.ritePrecio.Mask.EditMask = "n6"
        Me.ritePrecio.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePrecio.Mask.UseMaskAsDisplayFormat = True
        Me.ritePrecio.Name = "ritePrecio"
        '
        'gcPorcDescto
        '
        Me.gcPorcDescto.Caption = "% Descto"
        Me.gcPorcDescto.ColumnEdit = Me.riteDecimal
        Me.gcPorcDescto.DisplayFormat.FormatString = "n2"
        Me.gcPorcDescto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPorcDescto.FieldName = "PorcDescto"
        Me.gcPorcDescto.Name = "gcPorcDescto"
        Me.gcPorcDescto.Visible = True
        Me.gcPorcDescto.VisibleIndex = 4
        Me.gcPorcDescto.Width = 65
        '
        'riteDecimal
        '
        Me.riteDecimal.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteDecimal.AutoHeight = False
        Me.riteDecimal.Mask.EditMask = "n2"
        Me.riteDecimal.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteDecimal.Mask.UseMaskAsDisplayFormat = True
        Me.riteDecimal.Name = "riteDecimal"
        '
        'gcValorDescto
        '
        Me.gcValorDescto.Caption = "Descto.Total"
        Me.gcValorDescto.ColumnEdit = Me.riteDecimal
        Me.gcValorDescto.FieldName = "ValorDescto"
        Me.gcValorDescto.Name = "gcValorDescto"
        Me.gcValorDescto.OptionsColumn.AllowEdit = False
        Me.gcValorDescto.Visible = True
        Me.gcValorDescto.VisibleIndex = 5
        Me.gcValorDescto.Width = 85
        '
        'gcPrecioUnitario
        '
        Me.gcPrecioUnitario.Caption = "P.Unitario"
        Me.gcPrecioUnitario.ColumnEdit = Me.ritePrecioFinal
        Me.gcPrecioUnitario.DisplayFormat.FormatString = "n6"
        Me.gcPrecioUnitario.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcPrecioUnitario.FieldName = "PrecioUnitario"
        Me.gcPrecioUnitario.Name = "gcPrecioUnitario"
        Me.gcPrecioUnitario.Visible = True
        Me.gcPrecioUnitario.VisibleIndex = 6
        Me.gcPrecioUnitario.Width = 66
        '
        'ritePrecioFinal
        '
        Me.ritePrecioFinal.AutoHeight = False
        Me.ritePrecioFinal.Mask.EditMask = "n6"
        Me.ritePrecioFinal.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.ritePrecioFinal.Mask.UseMaskAsDisplayFormat = True
        Me.ritePrecioFinal.Name = "ritePrecioFinal"
        '
        'gcValorExento
        '
        Me.gcValorExento.Caption = "Valor Exento"
        Me.gcValorExento.ColumnEdit = Me.riteDecimal
        Me.gcValorExento.DisplayFormat.FormatString = "n2"
        Me.gcValorExento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcValorExento.FieldName = "ValorExento"
        Me.gcValorExento.Name = "gcValorExento"
        Me.gcValorExento.OptionsColumn.AllowEdit = False
        Me.gcValorExento.OptionsColumn.AllowFocus = False
        Me.gcValorExento.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorExento", "{0:c}")})
        Me.gcValorExento.Visible = True
        Me.gcValorExento.VisibleIndex = 7
        Me.gcValorExento.Width = 80
        '
        'gcValorAfecto
        '
        Me.gcValorAfecto.Caption = "Valor Afecto"
        Me.gcValorAfecto.ColumnEdit = Me.riteDecimal
        Me.gcValorAfecto.DisplayFormat.FormatString = "n2"
        Me.gcValorAfecto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcValorAfecto.FieldName = "ValorAfecto"
        Me.gcValorAfecto.Name = "gcValorAfecto"
        Me.gcValorAfecto.OptionsColumn.AllowEdit = False
        Me.gcValorAfecto.OptionsColumn.AllowFocus = False
        Me.gcValorAfecto.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ValorAfecto", "{0:c}")})
        Me.gcValorAfecto.Visible = True
        Me.gcValorAfecto.VisibleIndex = 8
        Me.gcValorAfecto.Width = 84
        '
        'gcIdCuenta
        '
        Me.gcIdCuenta.Caption = "Cuenta Contable"
        Me.gcIdCuenta.ColumnEdit = Me.riteIdCuenta
        Me.gcIdCuenta.FieldName = "IdCuenta"
        Me.gcIdCuenta.Name = "gcIdCuenta"
        Me.gcIdCuenta.Visible = True
        Me.gcIdCuenta.VisibleIndex = 9
        Me.gcIdCuenta.Width = 141
        '
        'riteIdCuenta
        '
        Me.riteIdCuenta.AutoHeight = False
        Me.riteIdCuenta.Name = "riteIdCuenta"
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Centro Costo"
        Me.GridColumn9.ColumnEdit = Me.leCentroCosto
        Me.GridColumn9.FieldName = "IdCentro"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.OptionsColumn.AllowEdit = False
        Me.GridColumn9.OptionsColumn.AllowFocus = False
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 10
        '
        'leCentroCosto
        '
        Me.leCentroCosto.AutoHeight = False
        Me.leCentroCosto.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCentroCosto.Name = "leCentroCosto"
        '
        'gcTipoImpuesto
        '
        Me.gcTipoImpuesto.Caption = "TipoImpuesto"
        Me.gcTipoImpuesto.FieldName = "TipoImpuesto"
        Me.gcTipoImpuesto.Name = "gcTipoImpuesto"
        '
        'riteCantidad2
        '
        Me.riteCantidad2.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.riteCantidad2.AutoHeight = False
        Me.riteCantidad2.DisplayFormat.FormatString = "n3"
        Me.riteCantidad2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteCantidad2.EditFormat.FormatString = "n3"
        Me.riteCantidad2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.riteCantidad2.Mask.EditMask = "n3"
        Me.riteCantidad2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteCantidad2.Mask.UseMaskAsDisplayFormat = True
        Me.riteCantidad2.Name = "riteCantidad2"
        Me.riteCantidad2.NullText = "0"
        '
        'pcTotales
        '
        Me.pcTotales.Controls.Add(Me.rgTipo)
        Me.pcTotales.Controls.Add(Me.LabelControl8)
        Me.pcTotales.Controls.Add(Me.lblNombreCuenta)
        Me.pcTotales.Controls.Add(Me.LabelControl23)
        Me.pcTotales.Controls.Add(Me.teImpuestos)
        Me.pcTotales.Controls.Add(Me.sbImpuestos)
        Me.pcTotales.Controls.Add(Me.LabelControl20)
        Me.pcTotales.Controls.Add(Me.lblPerRet)
        Me.pcTotales.Controls.Add(Me.LabelControl5)
        Me.pcTotales.Controls.Add(Me.btRevertir)
        Me.pcTotales.Controls.Add(Me.teTotal)
        Me.pcTotales.Controls.Add(Me.tePercepcionRetencion)
        Me.pcTotales.Controls.Add(Me.btAplicar)
        Me.pcTotales.Controls.Add(Me.teIva)
        Me.pcTotales.Controls.Add(Me.sbGuardar)
        Me.pcTotales.Controls.Add(Me.sbDetallar)
        Me.pcTotales.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pcTotales.Location = New System.Drawing.Point(0, 508)
        Me.pcTotales.Name = "pcTotales"
        Me.pcTotales.Size = New System.Drawing.Size(1166, 87)
        Me.pcTotales.TabIndex = 61
        '
        'rgTipo
        '
        Me.rgTipo.EditValue = 1
        Me.rgTipo.Location = New System.Drawing.Point(457, 44)
        Me.rgTipo.Name = "rgTipo"
        Me.rgTipo.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Ninguno"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Retención"), New DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Percepción")})
        Me.rgTipo.Size = New System.Drawing.Size(271, 21)
        Me.rgTipo.TabIndex = 88
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(359, 48)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl8.TabIndex = 87
        Me.LabelControl8.Text = "Impuesto Adicional:"
        '
        'lblNombreCuenta
        '
        Me.lblNombreCuenta.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreCuenta.Appearance.ForeColor = System.Drawing.Color.Maroon
        Me.lblNombreCuenta.Appearance.Options.UseFont = True
        Me.lblNombreCuenta.Appearance.Options.UseForeColor = True
        Me.lblNombreCuenta.Location = New System.Drawing.Point(336, 15)
        Me.lblNombreCuenta.Name = "lblNombreCuenta"
        Me.lblNombreCuenta.Size = New System.Drawing.Size(142, 14)
        Me.lblNombreCuenta.TabIndex = 86
        Me.lblNombreCuenta.Text = "-- CUENTA CONTABLE --"
        '
        'LabelControl23
        '
        Me.LabelControl23.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl23.Location = New System.Drawing.Point(933, 46)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(93, 13)
        Me.LabelControl23.TabIndex = 85
        Me.LabelControl23.Text = "Otras Retenciones:"
        '
        'teImpuestos
        '
        Me.teImpuestos.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teImpuestos.EditValue = 0
        Me.teImpuestos.Location = New System.Drawing.Point(1031, 43)
        Me.teImpuestos.Name = "teImpuestos"
        Me.teImpuestos.Properties.Appearance.Options.UseTextOptions = True
        Me.teImpuestos.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teImpuestos.Properties.Mask.EditMask = "n2"
        Me.teImpuestos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teImpuestos.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teImpuestos.Properties.ReadOnly = True
        Me.teImpuestos.Size = New System.Drawing.Size(129, 20)
        Me.teImpuestos.TabIndex = 84
        '
        'sbImpuestos
        '
        Me.sbImpuestos.AllowFocus = False
        Me.sbImpuestos.Image = Global.Nexus.My.Resources.Resources.PercentSign16
        Me.sbImpuestos.Location = New System.Drawing.Point(167, 7)
        Me.sbImpuestos.Name = "sbImpuestos"
        Me.sbImpuestos.Size = New System.Drawing.Size(126, 30)
        Me.sbImpuestos.TabIndex = 83
        Me.sbImpuestos.Text = "&Otros Impuestos..."
        '
        'LabelControl20
        '
        Me.LabelControl20.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl20.Location = New System.Drawing.Point(990, 67)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl20.TabIndex = 3
        Me.LabelControl20.Text = "TOTAL:"
        '
        'lblPerRet
        '
        Me.lblPerRet.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblPerRet.Location = New System.Drawing.Point(969, 26)
        Me.lblPerRet.Name = "lblPerRet"
        Me.lblPerRet.Size = New System.Drawing.Size(56, 13)
        Me.lblPerRet.TabIndex = 3
        Me.lblPerRet.Text = "Percepción:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.LabelControl5.Location = New System.Drawing.Point(1005, 4)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl5.TabIndex = 3
        Me.LabelControl5.Text = "IVA:"
        '
        'btRevertir
        '
        Me.btRevertir.AllowFocus = False
        Me.btRevertir.Location = New System.Drawing.Point(167, 46)
        Me.btRevertir.Name = "btRevertir"
        Me.btRevertir.Size = New System.Drawing.Size(126, 24)
        Me.btRevertir.TabIndex = 79
        Me.btRevertir.Text = "&Revertir Aplicación"
        '
        'teTotal
        '
        Me.teTotal.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teTotal.EditValue = 0
        Me.teTotal.Location = New System.Drawing.Point(1031, 64)
        Me.teTotal.Name = "teTotal"
        Me.teTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.teTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teTotal.Properties.Mask.EditMask = "n2"
        Me.teTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teTotal.Properties.ReadOnly = True
        Me.teTotal.Size = New System.Drawing.Size(129, 20)
        Me.teTotal.TabIndex = 2
        '
        'tePercepcionRetencion
        '
        Me.tePercepcionRetencion.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.tePercepcionRetencion.EditValue = 0
        Me.tePercepcionRetencion.Location = New System.Drawing.Point(1031, 22)
        Me.tePercepcionRetencion.Name = "tePercepcionRetencion"
        Me.tePercepcionRetencion.Properties.Appearance.Options.UseTextOptions = True
        Me.tePercepcionRetencion.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.tePercepcionRetencion.Properties.Mask.EditMask = "n2"
        Me.tePercepcionRetencion.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tePercepcionRetencion.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tePercepcionRetencion.Properties.ReadOnly = True
        Me.tePercepcionRetencion.Size = New System.Drawing.Size(129, 20)
        Me.tePercepcionRetencion.TabIndex = 2
        '
        'btAplicar
        '
        Me.btAplicar.AllowFocus = False
        Me.btAplicar.Location = New System.Drawing.Point(9, 48)
        Me.btAplicar.Name = "btAplicar"
        Me.btAplicar.Size = New System.Drawing.Size(148, 24)
        Me.btAplicar.TabIndex = 78
        Me.btAplicar.Text = "&Aplicar Documento"
        '
        'teIva
        '
        Me.teIva.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.teIva.EditValue = 0
        Me.teIva.Location = New System.Drawing.Point(1031, 1)
        Me.teIva.Name = "teIva"
        Me.teIva.Properties.Appearance.Options.UseTextOptions = True
        Me.teIva.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teIva.Properties.Mask.EditMask = "n2"
        Me.teIva.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teIva.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teIva.Size = New System.Drawing.Size(129, 20)
        Me.teIva.TabIndex = 0
        '
        'sbGuardar
        '
        Me.sbGuardar.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sbGuardar.Appearance.Options.UseFont = True
        Me.sbGuardar.Location = New System.Drawing.Point(9, 7)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(148, 30)
        Me.sbGuardar.TabIndex = 0
        Me.sbGuardar.Text = "&Guardar Docto."
        Me.sbGuardar.Visible = False
        '
        'sbDetallar
        '
        Me.sbDetallar.Location = New System.Drawing.Point(539, 10)
        Me.sbDetallar.Name = "sbDetallar"
        Me.sbDetallar.Size = New System.Drawing.Size(139, 23)
        Me.sbDetallar.TabIndex = 17
        Me.sbDetallar.Text = "Detallar Nota de Crédito..."
        Me.sbDetallar.Visible = False
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdBorrar)
        Me.pcBotones.Controls.Add(Me.cmdUp)
        Me.pcBotones.Controls.Add(Me.cmdDown)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(1166, 157)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(34, 438)
        Me.pcBotones.TabIndex = 60
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(3, 65)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(28, 23)
        Me.cmdBorrar.TabIndex = 36
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(3, 35)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(28, 24)
        Me.cmdUp.TabIndex = 35
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(3, 6)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(28, 24)
        Me.cmdDown.TabIndex = 33
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.LabelControl28)
        Me.pcHeader.Controls.Add(Me.leClaseDocumento)
        Me.pcHeader.Controls.Add(Me.teNumeroControlInternoFormularioUnico)
        Me.pcHeader.Controls.Add(Me.LabelControl29)
        Me.pcHeader.Controls.Add(Me.teResolucion)
        Me.pcHeader.Controls.Add(Me.LabelControl27)
        Me.pcHeader.Controls.Add(Me.sbCopiaCompras)
        Me.pcHeader.Controls.Add(Me.LabelControl10)
        Me.pcHeader.Controls.Add(Me.LabelControl39)
        Me.pcHeader.Controls.Add(Me.teIdPartida)
        Me.pcHeader.Controls.Add(Me.teIdCheque)
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.LabelControl24)
        Me.pcHeader.Controls.Add(Me.ceRebajaCosto)
        Me.pcHeader.Controls.Add(Me.LabelControl22)
        Me.pcHeader.Controls.Add(Me.leCaja)
        Me.pcHeader.Controls.Add(Me.LabelControl6)
        Me.pcHeader.Controls.Add(Me.seDiasCredito)
        Me.pcHeader.Controls.Add(Me.beOrden)
        Me.pcHeader.Controls.Add(Me.teOrdenCompra)
        Me.pcHeader.Controls.Add(Me.LabelControl3)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.deFechaCompra)
        Me.pcHeader.Controls.Add(Me.ceExcluirLibroCompras)
        Me.pcHeader.Controls.Add(Me.leFormaPago)
        Me.pcHeader.Controls.Add(Me.ceExcluido)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Controls.Add(Me.teNumero)
        Me.pcHeader.Controls.Add(Me.deFechaVto)
        Me.pcHeader.Controls.Add(Me.LabelControl4)
        Me.pcHeader.Controls.Add(Me.LabelControl7)
        Me.pcHeader.Controls.Add(Me.teNombre)
        Me.pcHeader.Controls.Add(Me.teNIT)
        Me.pcHeader.Controls.Add(Me.teNRC)
        Me.pcHeader.Controls.Add(Me.LabelControl9)
        Me.pcHeader.Controls.Add(Me.LabelControl11)
        Me.pcHeader.Controls.Add(Me.deFechaContable)
        Me.pcHeader.Controls.Add(Me.LabelControl14)
        Me.pcHeader.Controls.Add(Me.LabelControl16)
        Me.pcHeader.Controls.Add(Me.LabelControl17)
        Me.pcHeader.Controls.Add(Me.teDocExcluido)
        Me.pcHeader.Controls.Add(Me.LabelControl19)
        Me.pcHeader.Controls.Add(Me.leOrigenCompra)
        Me.pcHeader.Controls.Add(Me.leBodega)
        Me.pcHeader.Controls.Add(Me.teCorrelativo)
        Me.pcHeader.Controls.Add(Me.LabelControl15)
        Me.pcHeader.Controls.Add(Me.beProveedor)
        Me.pcHeader.Controls.Add(Me.LabelControl18)
        Me.pcHeader.Controls.Add(Me.LabelControl13)
        Me.pcHeader.Controls.Add(Me.LabelControl21)
        Me.pcHeader.Controls.Add(Me.LabelControl12)
        Me.pcHeader.Controls.Add(Me.leTipoCompra)
        Me.pcHeader.Controls.Add(Me.teSerie)
        Me.pcHeader.Controls.Add(Me.leTipoDocto)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(1200, 157)
        Me.pcHeader.TabIndex = 58
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(829, 69)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl28.TabIndex = 149
        Me.LabelControl28.Text = "Clase de Documento:"
        '
        'leClaseDocumento
        '
        Me.leClaseDocumento.Location = New System.Drawing.Point(934, 65)
        Me.leClaseDocumento.Name = "leClaseDocumento"
        Me.leClaseDocumento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leClaseDocumento.Size = New System.Drawing.Size(202, 20)
        Me.leClaseDocumento.TabIndex = 16
        '
        'teNumeroControlInternoFormularioUnico
        '
        Me.teNumeroControlInternoFormularioUnico.EnterMoveNextControl = True
        Me.teNumeroControlInternoFormularioUnico.Location = New System.Drawing.Point(694, 66)
        Me.teNumeroControlInternoFormularioUnico.Name = "teNumeroControlInternoFormularioUnico"
        Me.teNumeroControlInternoFormularioUnico.Size = New System.Drawing.Size(132, 20)
        Me.teNumeroControlInternoFormularioUnico.TabIndex = 15
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(572, 69)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(122, 13)
        Me.LabelControl29.TabIndex = 147
        Me.LabelControl29.Text = "No. Control Interno F.U.:"
        '
        'teResolucion
        '
        Me.teResolucion.EnterMoveNextControl = True
        Me.teResolucion.Location = New System.Drawing.Point(692, 4)
        Me.teResolucion.Name = "teResolucion"
        Me.teResolucion.Size = New System.Drawing.Size(134, 20)
        Me.teResolucion.TabIndex = 3
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(615, 8)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl27.TabIndex = 143
        Me.LabelControl27.Text = "No. Resolución:"
        '
        'sbCopiaCompras
        '
        Me.sbCopiaCompras.Location = New System.Drawing.Point(829, 129)
        Me.sbCopiaCompras.Name = "sbCopiaCompras"
        Me.sbCopiaCompras.Size = New System.Drawing.Size(120, 20)
        Me.sbCopiaCompras.TabIndex = 139
        Me.sbCopiaCompras.Text = "Copiar Compras"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(1012, 111)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl10.TabIndex = 138
        Me.LabelControl10.Text = "IdCheque:"
        '
        'LabelControl39
        '
        Me.LabelControl39.Location = New System.Drawing.Point(1015, 89)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl39.TabIndex = 136
        Me.LabelControl39.Text = "IdPartida:"
        '
        'teIdPartida
        '
        Me.teIdPartida.Enabled = False
        Me.teIdPartida.EnterMoveNextControl = True
        Me.teIdPartida.Location = New System.Drawing.Point(1068, 86)
        Me.teIdPartida.Name = "teIdPartida"
        Me.teIdPartida.Properties.ReadOnly = True
        Me.teIdPartida.Size = New System.Drawing.Size(68, 20)
        Me.teIdPartida.TabIndex = 11
        '
        'teIdCheque
        '
        Me.teIdCheque.Enabled = False
        Me.teIdCheque.EnterMoveNextControl = True
        Me.teIdCheque.Location = New System.Drawing.Point(1068, 108)
        Me.teIdCheque.Name = "teIdCheque"
        Me.teIdCheque.Properties.ReadOnly = True
        Me.teIdCheque.Size = New System.Drawing.Size(68, 20)
        Me.teIdCheque.TabIndex = 137
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(422, 88)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(147, 20)
        Me.leSucursal.TabIndex = 19
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(377, 92)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl24.TabIndex = 85
        Me.LabelControl24.Text = "Sucursal:"
        '
        'ceRebajaCosto
        '
        Me.ceRebajaCosto.Location = New System.Drawing.Point(368, 114)
        Me.ceRebajaCosto.Name = "ceRebajaCosto"
        Me.ceRebajaCosto.Properties.Caption = "Afectar Solo Costo"
        Me.ceRebajaCosto.Size = New System.Drawing.Size(113, 19)
        Me.ceRebajaCosto.TabIndex = 83
        Me.ceRebajaCosto.Visible = False
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(636, 92)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl22.TabIndex = 82
        Me.LabelControl22.Text = "Caja Chica:"
        '
        'leCaja
        '
        Me.leCaja.EnterMoveNextControl = True
        Me.leCaja.Location = New System.Drawing.Point(694, 87)
        Me.leCaja.Name = "leCaja"
        Me.leCaja.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCaja.Size = New System.Drawing.Size(255, 20)
        Me.leCaja.TabIndex = 18
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(435, 71)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl6.TabIndex = 80
        Me.LabelControl6.Text = "Vence:"
        '
        'seDiasCredito
        '
        Me.seDiasCredito.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDiasCredito.Location = New System.Drawing.Point(380, 67)
        Me.seDiasCredito.Name = "seDiasCredito"
        Me.seDiasCredito.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDiasCredito.Size = New System.Drawing.Size(49, 20)
        Me.seDiasCredito.TabIndex = 13
        '
        'beOrden
        '
        Me.beOrden.Location = New System.Drawing.Point(829, 108)
        Me.beOrden.Name = "beOrden"
        Me.beOrden.Size = New System.Drawing.Size(120, 20)
        Me.beOrden.TabIndex = 18
        Me.beOrden.Text = "Obtener Orden de C."
        '
        'teOrdenCompra
        '
        Me.teOrdenCompra.EnterMoveNextControl = True
        Me.teOrdenCompra.Location = New System.Drawing.Point(694, 107)
        Me.teOrdenCompra.Name = "teOrdenCompra"
        Me.teOrdenCompra.Size = New System.Drawing.Size(132, 20)
        Me.teOrdenCompra.TabIndex = 21
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(300, 70)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl3.TabIndex = 16
        Me.LabelControl3.Text = "Días de Crédito:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(26, 70)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl2.TabIndex = 6
        Me.LabelControl2.Text = "Forma de Pago:"
        '
        'deFechaCompra
        '
        Me.deFechaCompra.EditValue = Nothing
        Me.deFechaCompra.EnterMoveNextControl = True
        Me.deFechaCompra.Location = New System.Drawing.Point(934, 4)
        Me.deFechaCompra.Name = "deFechaCompra"
        Me.deFechaCompra.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaCompra.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaCompra.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaCompra.Size = New System.Drawing.Size(113, 20)
        Me.deFechaCompra.TabIndex = 4
        '
        'ceExcluirLibroCompras
        '
        Me.ceExcluirLibroCompras.Location = New System.Drawing.Point(103, 131)
        Me.ceExcluirLibroCompras.Name = "ceExcluirLibroCompras"
        Me.ceExcluirLibroCompras.Properties.Caption = "EXCLUIR DEL LIBRO DE COMPRAS"
        Me.ceExcluirLibroCompras.Size = New System.Drawing.Size(188, 19)
        Me.ceExcluirLibroCompras.TabIndex = 1
        '
        'leFormaPago
        '
        Me.leFormaPago.EnterMoveNextControl = True
        Me.leFormaPago.Location = New System.Drawing.Point(105, 67)
        Me.leFormaPago.Name = "leFormaPago"
        Me.leFormaPago.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leFormaPago.Size = New System.Drawing.Size(130, 20)
        Me.leFormaPago.TabIndex = 12
        '
        'ceExcluido
        '
        Me.ceExcluido.Location = New System.Drawing.Point(368, 133)
        Me.ceExcluido.Name = "ceExcluido"
        Me.ceExcluido.Properties.Caption = "Compra a Sujeto Excluido"
        Me.ceExcluido.Size = New System.Drawing.Size(142, 19)
        Me.ceExcluido.TabIndex = 15
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(845, 7)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "Fecha de Emisión:"
        '
        'teNumero
        '
        Me.teNumero.EnterMoveNextControl = True
        Me.teNumero.Location = New System.Drawing.Point(469, 4)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(100, 20)
        Me.teNumero.TabIndex = 2
        '
        'deFechaVto
        '
        Me.deFechaVto.EditValue = Nothing
        Me.deFechaVto.EnterMoveNextControl = True
        Me.deFechaVto.Location = New System.Drawing.Point(469, 67)
        Me.deFechaVto.Name = "deFechaVto"
        Me.deFechaVto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaVto.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaVto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaVto.Size = New System.Drawing.Size(100, 20)
        Me.deFechaVto.TabIndex = 14
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(569, 111)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(124, 13)
        Me.LabelControl4.TabIndex = 17
        Me.LabelControl4.Text = "No. de Orden de Compra:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(336, 28)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl7.TabIndex = 39
        Me.LabelControl7.Text = "Nombre:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(380, 25)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(446, 20)
        Me.teNombre.TabIndex = 6
        '
        'teNIT
        '
        Me.teNIT.EnterMoveNextControl = True
        Me.teNIT.Location = New System.Drawing.Point(934, 45)
        Me.teNIT.Name = "teNIT"
        Me.teNIT.Size = New System.Drawing.Size(113, 20)
        Me.teNIT.TabIndex = 11
        '
        'teNRC
        '
        Me.teNRC.EnterMoveNextControl = True
        Me.teNRC.Location = New System.Drawing.Point(694, 46)
        Me.teNRC.Name = "teNRC"
        Me.teNRC.Size = New System.Drawing.Size(132, 20)
        Me.teNRC.TabIndex = 10
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(667, 49)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl9.TabIndex = 42
        Me.LabelControl9.Text = "NRC:"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(910, 49)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl11.TabIndex = 44
        Me.LabelControl11.Text = "NIT:"
        '
        'deFechaContable
        '
        Me.deFechaContable.EditValue = Nothing
        Me.deFechaContable.EnterMoveNextControl = True
        Me.deFechaContable.Location = New System.Drawing.Point(934, 25)
        Me.deFechaContable.Name = "deFechaContable"
        Me.deFechaContable.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFechaContable.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFechaContable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFechaContable.Size = New System.Drawing.Size(113, 20)
        Me.deFechaContable.TabIndex = 7
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(852, 28)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl14.TabIndex = 47
        Me.LabelControl14.Text = "Fecha Contable:"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(9, 113)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(93, 13)
        Me.LabelControl16.TabIndex = 49
        Me.LabelControl16.Text = "Bodega de ingreso:"
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(574, 133)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(119, 13)
        Me.LabelControl17.TabIndex = 50
        Me.LabelControl17.Text = "Doc. del Sujeto Excluido:"
        '
        'teDocExcluido
        '
        Me.teDocExcluido.EnterMoveNextControl = True
        Me.teDocExcluido.Location = New System.Drawing.Point(694, 129)
        Me.teDocExcluido.Name = "teDocExcluido"
        Me.teDocExcluido.Size = New System.Drawing.Size(132, 20)
        Me.teDocExcluido.TabIndex = 22
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(275, 50)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(102, 13)
        Me.LabelControl19.TabIndex = 52
        Me.LabelControl19.Text = "Origen de la Compra:"
        '
        'leOrigenCompra
        '
        Me.leOrigenCompra.EnterMoveNextControl = True
        Me.leOrigenCompra.Location = New System.Drawing.Point(380, 46)
        Me.leOrigenCompra.Name = "leOrigenCompra"
        Me.leOrigenCompra.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leOrigenCompra.Size = New System.Drawing.Size(189, 20)
        Me.leOrigenCompra.TabIndex = 9
        '
        'leBodega
        '
        Me.leBodega.EnterMoveNextControl = True
        Me.leBodega.Location = New System.Drawing.Point(105, 109)
        Me.leBodega.Name = "leBodega"
        Me.leBodega.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leBodega.Size = New System.Drawing.Size(262, 20)
        Me.leBodega.TabIndex = 20
        '
        'teCorrelativo
        '
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(105, 4)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Properties.ReadOnly = True
        Me.teCorrelativo.Size = New System.Drawing.Size(130, 20)
        Me.teCorrelativo.TabIndex = 0
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(45, 7)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl15.TabIndex = 48
        Me.LabelControl15.Text = "Correlativo:"
        '
        'beProveedor
        '
        Me.beProveedor.EnterMoveNextControl = True
        Me.beProveedor.Location = New System.Drawing.Point(105, 25)
        Me.beProveedor.Name = "beProveedor"
        Me.beProveedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beProveedor.Size = New System.Drawing.Size(130, 20)
        Me.beProveedor.TabIndex = 5
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(23, 50)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl18.TabIndex = 51
        Me.LabelControl18.Text = "Tipo de Compra:"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(48, 28)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl13.TabIndex = 46
        Me.LabelControl13.Text = "Proveedor:"
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(6, 91)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(96, 13)
        Me.LabelControl21.TabIndex = 54
        Me.LabelControl21.Text = "Tipo de Documento:"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(248, 7)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(129, 13)
        Me.LabelControl12.TabIndex = 45
        Me.LabelControl12.Text = "Serie y No. de Documento:"
        '
        'leTipoCompra
        '
        Me.leTipoCompra.EnterMoveNextControl = True
        Me.leTipoCompra.Location = New System.Drawing.Point(105, 46)
        Me.leTipoCompra.Name = "leTipoCompra"
        Me.leTipoCompra.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoCompra.Size = New System.Drawing.Size(130, 20)
        Me.leTipoCompra.TabIndex = 8
        '
        'teSerie
        '
        Me.teSerie.EnterMoveNextControl = True
        Me.teSerie.Location = New System.Drawing.Point(380, 4)
        Me.teSerie.Name = "teSerie"
        Me.teSerie.Size = New System.Drawing.Size(87, 20)
        Me.teSerie.TabIndex = 1
        '
        'leTipoDocto
        '
        Me.leTipoDocto.EnterMoveNextControl = True
        Me.leTipoDocto.Location = New System.Drawing.Point(105, 88)
        Me.leTipoDocto.Name = "leTipoDocto"
        Me.leTipoDocto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoDocto.Size = New System.Drawing.Size(262, 20)
        Me.leTipoDocto.TabIndex = 17
        '
        'com_frmCompras
        '
        Me.ClientSize = New System.Drawing.Size(1206, 648)
        Me.Controls.Add(Me.xtcCompras)
        Me.Modulo = "Compras"
        Me.Name = "com_frmCompras"
        Me.OptionId = "002002"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Compras, Notas de Crédito y Notas de débito"
        Me.Controls.SetChildIndex(Me.xtcCompras, 0)
        CType(Me.xtcCompras, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcCompras.ResumeLayout(False)
        Me.xtpLista1.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCodigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteDecimal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ritePrecioFinal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteIdCuenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCentroCosto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCantidad2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcTotales.ResumeLayout(False)
        Me.pcTotales.PerformLayout()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teImpuestos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tePercepcionRetencion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.leClaseDocumento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumeroControlInternoFormularioUnico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teResolucion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceRebajaCosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCaja.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teOrdenCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaCompra.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceExcluirLibroCompras.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leFormaPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceExcluido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaVto.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaVto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaContable.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFechaContable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDocExcluido.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leOrigenCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leBodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoCompra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoDocto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents xtcCompras As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdProducto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCodigo As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcCantidad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcDescripcion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcPrecioReferencia As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ritePrecio As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcPorcDescto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteDecimal As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcValorDescto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorExento As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcValorAfecto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdCuenta As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteIdCuenta As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents pcTotales As DevExpress.XtraEditors.PanelControl
    Friend WithEvents lblNombreCuenta As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teImpuestos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents sbImpuestos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblPerRet As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tePercepcionRetencion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teIva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ceExcluirLibroCompras As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceRebajaCosto As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCaja As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seDiasCredito As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents btRevertir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbDetallar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents beOrden As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btAplicar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teOrdenCompra As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Public WithEvents deFechaCompra As DevExpress.XtraEditors.DateEdit
    Friend WithEvents leFormaPago As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents ceExcluido As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Public WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents deFechaVto As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNIT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Public WithEvents deFechaContable As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDocExcluido As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leOrigenCompra As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leBodega As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Public WithEvents beProveedor As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoCompra As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leTipoDocto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents rgTipo As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents riteCantidad2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdPartida As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdCheque As DevExpress.XtraEditors.TextEdit
    Friend WithEvents gcPrecioUnitario As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ritePrecioFinal As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcAplicada As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTipoImpuesto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leCentroCosto As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents sbCopiaCompras As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Public WithEvents teResolucion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leClaseDocumento As DevExpress.XtraEditors.LookUpEdit
    Public WithEvents teNumeroControlInternoFormularioUnico As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
End Class
