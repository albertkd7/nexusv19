﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class com_frmDetallarProveedoresImportacion
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(com_frmDetallarProveedoresImportacion))
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcIdProveedor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leIdProveedor = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcIdFormaPago = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.leFormaPago = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFechaLibro = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcNumeroComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcDiasCredito = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcValor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.teValor = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.gcContabilizar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkContabilizar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutConverter1 = New DevExpress.XtraLayout.Converter.LayoutConverter(Me.components)
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leIdProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leFormaPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teValor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkContabilizar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.leIdProveedor, Me.leFormaPago, Me.teValor, Me.chkContabilizar})
        Me.gc.Size = New System.Drawing.Size(891, 372)
        Me.gc.TabIndex = 2
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdProveedor, Me.gcIdFormaPago, Me.gcFecha, Me.gcFechaLibro, Me.gcNumeroComprobante, Me.gcDiasCredito, Me.gcValor, Me.gcContabilizar, Me.GridColumn1, Me.GridColumn2})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdProveedor
        '
        Me.gcIdProveedor.Caption = "Proveedor"
        Me.gcIdProveedor.ColumnEdit = Me.leIdProveedor
        Me.gcIdProveedor.FieldName = "IdProveedor"
        Me.gcIdProveedor.Name = "gcIdProveedor"
        Me.gcIdProveedor.Visible = True
        Me.gcIdProveedor.VisibleIndex = 0
        Me.gcIdProveedor.Width = 335
        '
        'leIdProveedor
        '
        Me.leIdProveedor.AutoHeight = False
        Me.leIdProveedor.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leIdProveedor.Name = "leIdProveedor"
        '
        'gcIdFormaPago
        '
        Me.gcIdFormaPago.Caption = "Forma de Pago"
        Me.gcIdFormaPago.ColumnEdit = Me.leFormaPago
        Me.gcIdFormaPago.FieldName = "IdFormaPago"
        Me.gcIdFormaPago.Name = "gcIdFormaPago"
        Me.gcIdFormaPago.Visible = True
        Me.gcIdFormaPago.VisibleIndex = 1
        Me.gcIdFormaPago.Width = 104
        '
        'leFormaPago
        '
        Me.leFormaPago.AutoHeight = False
        Me.leFormaPago.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leFormaPago.Name = "leFormaPago"
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha"
        Me.gcFecha.DisplayFormat.FormatString = "d"
        Me.gcFecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.gcFecha.FieldName = "FechaContable"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 2
        Me.gcFecha.Width = 84
        '
        'gcFechaLibro
        '
        Me.gcFechaLibro.Caption = "Fecha IVA"
        Me.gcFechaLibro.DisplayFormat.FormatString = "d"
        Me.gcFechaLibro.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.gcFechaLibro.FieldName = "Fecha"
        Me.gcFechaLibro.Name = "gcFechaLibro"
        Me.gcFechaLibro.Visible = True
        Me.gcFechaLibro.VisibleIndex = 3
        Me.gcFechaLibro.Width = 76
        '
        'gcNumeroComprobante
        '
        Me.gcNumeroComprobante.Caption = "No. Comprobante"
        Me.gcNumeroComprobante.FieldName = "NumeroComprobante"
        Me.gcNumeroComprobante.Name = "gcNumeroComprobante"
        Me.gcNumeroComprobante.Visible = True
        Me.gcNumeroComprobante.VisibleIndex = 4
        Me.gcNumeroComprobante.Width = 101
        '
        'gcDiasCredito
        '
        Me.gcDiasCredito.Caption = "Dias Crédito"
        Me.gcDiasCredito.FieldName = "DiasCredito"
        Me.gcDiasCredito.Name = "gcDiasCredito"
        Me.gcDiasCredito.Visible = True
        Me.gcDiasCredito.VisibleIndex = 5
        '
        'gcValor
        '
        Me.gcValor.Caption = "Total"
        Me.gcValor.ColumnEdit = Me.teValor
        Me.gcValor.DisplayFormat.FormatString = "n2"
        Me.gcValor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.gcValor.FieldName = "Valor"
        Me.gcValor.Name = "gcValor"
        Me.gcValor.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Valor", "{0:c}")})
        Me.gcValor.Visible = True
        Me.gcValor.VisibleIndex = 6
        Me.gcValor.Width = 98
        '
        'teValor
        '
        Me.teValor.AutoHeight = False
        Me.teValor.EditFormat.FormatString = "n2"
        Me.teValor.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teValor.Name = "teValor"
        '
        'gcContabilizar
        '
        Me.gcContabilizar.Caption = "Contabilizar"
        Me.gcContabilizar.ColumnEdit = Me.chkContabilizar
        Me.gcContabilizar.FieldName = "Contabilizar"
        Me.gcContabilizar.Name = "gcContabilizar"
        '
        'chkContabilizar
        '
        Me.chkContabilizar.AutoHeight = False
        Me.chkContabilizar.Name = "chkContabilizar"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "GridColumn1"
        Me.GridColumn1.FieldName = "IdImportacion"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "IdComprobante"
        Me.GridColumn2.FieldName = "IdComprobante"
        Me.GridColumn2.Name = "GridColumn2"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.sbGuardar)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl1.Location = New System.Drawing.Point(0, 372)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(937, 39)
        Me.PanelControl1.TabIndex = 23
        '
        'sbGuardar
        '
        Me.sbGuardar.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.sbGuardar.Appearance.Options.UseFont = True
        Me.sbGuardar.Location = New System.Drawing.Point(791, 8)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(100, 26)
        Me.sbGuardar.TabIndex = 23
        Me.sbGuardar.Text = "&Guardar"
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(897, 48)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(28, 24)
        Me.cmdBorrar.TabIndex = 32
        '
        'com_frmDetallarProveedoresImportacion
        '
        Me.ClientSize = New System.Drawing.Size(937, 411)
        Me.Controls.Add(Me.cmdBorrar)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.PanelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "com_frmDetallarProveedoresImportacion"
        Me.Text = "Detalle de Proveedores de Importación"
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leIdProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leFormaPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teValor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkContabilizar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdProveedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcIdFormaPago As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcDiasCredito As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutConverter1 As DevExpress.XtraLayout.Converter.LayoutConverter
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcValor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents leIdProveedor As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents leFormaPago As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents teValor As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcNumeroComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFechaLibro As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcContabilizar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chkContabilizar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
End Class
