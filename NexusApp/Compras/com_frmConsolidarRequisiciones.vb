﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports System.Math
Public Class com_frmConsolidarRequisiciones
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim RequisicionDetalle As List(Of com_RequisicionesDetalle)



    Private Sub btGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGenerar.Click
        gc.DataSource = bl.com_ObtenerRequisicionesNoAutorizadas(deHasta.EditValue)
        gv.BestFitColumns()
    End Sub
    Private Sub cargarentidades()
        RequisicionDetalle = New List(Of com_RequisicionesDetalle)

        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New com_RequisicionesDetalle

            If gv.GetRowCellValue(i, "Seleccionado") = True Then

                With entDetalle

                    .IdComprobante = gv.GetRowCellValue(i, "IdComprobante")
                    .IdDetalle = gv.GetRowCellValue(i, "IdDetalle")
                    .Cantidad = gv.GetRowCellValue(i, "Cantidad")
                    .Descripcion = gv.GetRowCellValue(i, "Descripcion")
                    .CantidadAutorizada = gv.GetRowCellValue(i, "CantidadAutorizada")
                    .FechaConsolido = deHasta.EditValue
                    .UsuarioConsolido = objMenu.User
                    .Consolidado = gv.GetRowCellValue(i, "Seleccionado")
                End With

                RequisicionDetalle.Add(entDetalle)
            End If
        Next


    End Sub
    Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click


        If MsgBox("¿Está seguro(a) de realizar estas Consolidaciones?", 292, "Confirme") = MsgBoxResult.No Then
            Exit Sub
        End If

        cargarentidades()

        Dim msj As String

        msj = bl.com_MarcarRequisiciones(RequisicionDetalle)

        If msj = "" Then
            InsertaOrdenCompra()
            MsgBox("El documento ha sido actualizado con éxito", MsgBoxStyle.Information)
            Me.Close()
        Else
            MsgBox("SE DETECTÓ UN ERROR AL ACTUALIZAR EL DOCUMENTO" + Chr(13) + msj, MsgBoxStyle.Critical)
            Me.Close()
        End If

    End Sub
    Private Sub InsertaOrdenCompra()

        Dim frmOC As com_frmOrdenesCompra = Me.Owner
        Dim dt As DataTable = frmOC.dtRequisicion

        If gv.RowCount > 0 Then

            For x = 0 To gv.RowCount - 1

                If gv.GetRowCellValue(x, "Seleccionado") = True Then

                    Dim BuscarFila() As DataRow
                    BuscarFila = frmOC.dtRequisicion.Select("IdProducto = '" & gv.GetRowCellValue(x, "IdProducto") & "'")

                    If BuscarFila.Length > 0 Then
                        Dim f As Integer = RetonarFila(gv.GetRowCellValue(x, "IdProducto"))
                        Dim filaEdit As DataRow = dt.Rows.Item(f)
                        Dim CantidadAnt As Decimal = filaEdit("Cantidad")
                        filaEdit("Cantidad") = CantidadAnt + gv.GetRowCellValue(x, "CantidadAutorizada")
                    Else
                        Dim dr As DataRow = frmOC.dtRequisicion.NewRow()


                        dr("IdProducto") = gv.GetRowCellValue(x, "IdProducto")
                        dr("Cantidad") = gv.GetRowCellValue(x, "CantidadAutorizada")
                        dr("Descripcion") = gv.GetRowCellValue(x, "Descripcion")
                        dr("PorcDescto") = 0
                        dr("PrecioReferencia") = 0
                        dr("PrecioUnitario") = 0
                        dr("ValorDescto") = 0
                        dr("ValorExento") = 0
                        dr("ValorAfecto") = 0
                        frmOC.dtRequisicion.Rows.Add(dr)
                    End If

                End If

            Next

        End If
    End Sub
    Function RetonarFila(ByVal IdProducto As String) As Integer
        Dim frmOC As com_frmOrdenesCompra = Me.Owner
        Dim x As Integer = 0
        For Each dr As DataRow In frmOC.dtRequisicion.Rows
            If dr("IdProducto") = IdProducto Then
                Return x
            End If
            x += 1
        Next
        Return 0
    End Function
    Private Sub com_frmConsolidarRemisiones_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        deHasta.EditValue = Today
        gc.DataSource = bl.com_ObtenerRequisicionesNoAutorizadas(deHasta.EditValue)
        gv.BestFitColumns()
    End Sub
End Class