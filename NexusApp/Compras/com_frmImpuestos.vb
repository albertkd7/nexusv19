﻿Imports NexusELL.TableEntities
Public Class com_frmImpuestos
    Dim entImpuestos As com_Impuestos
    Dim entCuentas As con_Cuentas

    Private Sub com_frmImpuestos_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub com_frmImpuestos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.com_ImpuestosSelectAll()
        entImpuestos = objTablas.com_ImpuestosSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdImpuesto"))
        objCombos.coo_TipoCalculoImpCompras(cboTipoCalculo, "")
        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub com_frmImpuestos_Nuevo_Click() Handles Me.Nuevo
        entImpuestos = New com_Impuestos
        entImpuestos.IdImpuesto = objFunciones.ObtenerUltimoId("COM_IMPUESTOS", "IdImpuesto") + 1
        CargaPantalla()
        ActivaControles(True)
        cboTipoCalculo.EditValue = 1
    End Sub
    Private Sub com_frmImpuestos_Save_Click() Handles Me.Guardar
        If beCta01.EditValue = "" Or teNombre.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [Nombre, Cuenta Contable y Teléfono]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.com_ImpuestosInsert(entImpuestos)
        Else
            objTablas.com_ImpuestosUpdate(entImpuestos)
        End If
        gc.DataSource = objTablas.com_ImpuestosSelectAll
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub com_frmImpuestos_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el impuesto seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.com_ImpuestosDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.com_ImpuestosSelectAll()
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL IMPUESTO:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub com_frmImpuestos_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entImpuestos
            teIdImpuesto.EditValue = .IdImpuesto
            teNombre.EditValue = .Nombre
            beCta01.EditValue = .IdCuentaContable
            cboTipoCalculo.EditValue = .TipoCalculo
            teValor.EditValue = .valor
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entImpuestos
            .IdImpuesto = teIdImpuesto.EditValue
            .Nombre = teNombre.EditValue
            .IdCuentaContable = beCta01.EditValue
            .TipoCalculo = cboTipoCalculo.EditValue
            .Valor = teValor.EditValue
        End With
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entImpuestos = objTablas.com_ImpuestosSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdImpuesto"))
        CargaPantalla()
    End Sub

    Private Sub facVendedores_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl2.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teIdImpuesto.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub
    Private Sub beCta01_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCta01.ButtonClick
        If teNombre.Properties.ReadOnly = False Then
            beCta01.EditValue = ""
            entCuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beCta01.EditValue)
            beCta01.EditValue = entCuentas.IdCuenta
            teCta01.EditValue = entCuentas.Nombre
        End If
    End Sub

    Private Sub beCta01_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCta01.Validated
        entCuentas = objTablas.con_CuentasSelectByPK(beCta01.EditValue)
        beCta01.EditValue = entCuentas.IdCuenta
        teCta01.EditValue = entCuentas.Nombre
    End Sub
End Class
