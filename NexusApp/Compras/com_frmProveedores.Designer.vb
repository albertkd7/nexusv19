﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class com_frmProveedores
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xtcProveedores = New DevExpress.XtraTab.XtraTabControl()
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.IdProveedor = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Nombre = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Nrc = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Nit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.AplicaRetencion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riChkAplicaRetencion = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.FechaHoraCreacion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riDepartamentos = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.riMunicipios = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.riPaises = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage()
        Me.LePaisOrigen = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.teRepresentante = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.teColoniaBarrio = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.teOtrosDatosDomicilio = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.teApartamentoLocal = New DevExpress.XtraEditors.TextEdit()
        Me.teNumCasa = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.teSujeto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.rgTipo = New DevExpress.XtraEditors.RadioGroup()
        Me.teNacionalidad = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.sbGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.BeCtaContable1 = New Nexus.beCtaContable()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.seDiasCredito = New DevExpress.XtraEditors.SpinEdit()
        Me.seLimite = New DevExpress.XtraEditors.SpinEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.ceAplicaPerRet = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.leOrigen = New DevExpress.XtraEditors.LookUpEdit()
        Me.leMunicipio = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.leDepto = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teNIT = New DevExpress.XtraEditors.TextEdit()
        Me.teFax = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.teCorreoElectronico = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.teTelefonos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.teNRC = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.teDireccion = New DevExpress.XtraEditors.TextEdit()
        Me.teGiro = New DevExpress.XtraEditors.TextEdit()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.teContacto3 = New DevExpress.XtraEditors.TextEdit()
        Me.teContacto2 = New DevExpress.XtraEditors.TextEdit()
        Me.teContacto1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teIdProveedor = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.xtcProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcProveedores.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riChkAplicaRetencion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riDepartamentos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riMunicipios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riPaises, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.LePaisOrigen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teRepresentante.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teColoniaBarrio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teOtrosDatosDomicilio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teApartamentoLocal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumCasa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSujeto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNacionalidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seLimite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ceAplicaPerRet.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leOrigen.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leMunicipio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leDepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teFax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorreoElectronico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teGiro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teContacto3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teContacto2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teContacto1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teIdProveedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcProveedores
        '
        Me.xtcProveedores.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcProveedores.Location = New System.Drawing.Point(0, 0)
        Me.xtcProveedores.Name = "xtcProveedores"
        Me.xtcProveedores.SelectedTabPage = Me.xtpLista
        Me.xtcProveedores.Size = New System.Drawing.Size(1266, 709)
        Me.xtcProveedores.TabIndex = 65
        Me.xtcProveedores.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(1260, 681)
        Me.xtpLista.Text = "Lista (Doble Clic para Editar)"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(0, 0)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riDepartamentos, Me.riMunicipios, Me.riChkAplicaRetencion, Me.riPaises, Me.RepositoryItemCheckEdit1})
        Me.gc.Size = New System.Drawing.Size(1260, 681)
        Me.gc.TabIndex = 0
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.IdProveedor, Me.Nombre, Me.Nrc, Me.Nit, Me.AplicaRetencion, Me.FechaHoraCreacion})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowAutoFilterRow = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'IdProveedor
        '
        Me.IdProveedor.Caption = "Cod. Proveedor"
        Me.IdProveedor.FieldName = "IdProveedor"
        Me.IdProveedor.Name = "IdProveedor"
        Me.IdProveedor.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.[True]
        Me.IdProveedor.Visible = True
        Me.IdProveedor.VisibleIndex = 0
        Me.IdProveedor.Width = 78
        '
        'Nombre
        '
        Me.Nombre.Caption = "Nombre"
        Me.Nombre.FieldName = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.[True]
        Me.Nombre.Visible = True
        Me.Nombre.VisibleIndex = 1
        Me.Nombre.Width = 264
        '
        'Nrc
        '
        Me.Nrc.Caption = "NRC"
        Me.Nrc.FieldName = "Nrc"
        Me.Nrc.Name = "Nrc"
        Me.Nrc.Visible = True
        Me.Nrc.VisibleIndex = 2
        Me.Nrc.Width = 71
        '
        'Nit
        '
        Me.Nit.Caption = "NIT"
        Me.Nit.FieldName = "Nit"
        Me.Nit.Name = "Nit"
        Me.Nit.Visible = True
        Me.Nit.VisibleIndex = 3
        Me.Nit.Width = 73
        '
        'AplicaRetencion
        '
        Me.AplicaRetencion.Caption = "Retencion"
        Me.AplicaRetencion.ColumnEdit = Me.riChkAplicaRetencion
        Me.AplicaRetencion.FieldName = "AplicaRetencion"
        Me.AplicaRetencion.Name = "AplicaRetencion"
        Me.AplicaRetencion.Visible = True
        Me.AplicaRetencion.VisibleIndex = 4
        Me.AplicaRetencion.Width = 59
        '
        'riChkAplicaRetencion
        '
        Me.riChkAplicaRetencion.AutoHeight = False
        Me.riChkAplicaRetencion.Name = "riChkAplicaRetencion"
        '
        'FechaHoraCreacion
        '
        Me.FechaHoraCreacion.Caption = "Fecha Creación"
        Me.FechaHoraCreacion.FieldName = "FechaHoraCreacion"
        Me.FechaHoraCreacion.Name = "FechaHoraCreacion"
        Me.FechaHoraCreacion.Visible = True
        Me.FechaHoraCreacion.VisibleIndex = 5
        Me.FechaHoraCreacion.Width = 103
        '
        'riDepartamentos
        '
        Me.riDepartamentos.AutoHeight = False
        Me.riDepartamentos.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riDepartamentos.Name = "riDepartamentos"
        '
        'riMunicipios
        '
        Me.riMunicipios.AutoHeight = False
        Me.riMunicipios.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riMunicipios.Name = "riMunicipios"
        '
        'riPaises
        '
        Me.riPaises.AutoHeight = False
        Me.riPaises.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riPaises.Name = "riPaises"
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.LePaisOrigen)
        Me.xtpDatos.Controls.Add(Me.LabelControl27)
        Me.xtpDatos.Controls.Add(Me.teRepresentante)
        Me.xtpDatos.Controls.Add(Me.LabelControl18)
        Me.xtpDatos.Controls.Add(Me.LabelControl26)
        Me.xtpDatos.Controls.Add(Me.teColoniaBarrio)
        Me.xtpDatos.Controls.Add(Me.LabelControl25)
        Me.xtpDatos.Controls.Add(Me.teOtrosDatosDomicilio)
        Me.xtpDatos.Controls.Add(Me.LabelControl24)
        Me.xtpDatos.Controls.Add(Me.teApartamentoLocal)
        Me.xtpDatos.Controls.Add(Me.teNumCasa)
        Me.xtpDatos.Controls.Add(Me.LabelControl23)
        Me.xtpDatos.Controls.Add(Me.teSujeto)
        Me.xtpDatos.Controls.Add(Me.LabelControl22)
        Me.xtpDatos.Controls.Add(Me.rgTipo)
        Me.xtpDatos.Controls.Add(Me.teNacionalidad)
        Me.xtpDatos.Controls.Add(Me.LabelControl20)
        Me.xtpDatos.Controls.Add(Me.sbGuardar)
        Me.xtpDatos.Controls.Add(Me.BeCtaContable1)
        Me.xtpDatos.Controls.Add(Me.LabelControl17)
        Me.xtpDatos.Controls.Add(Me.seDiasCredito)
        Me.xtpDatos.Controls.Add(Me.seLimite)
        Me.xtpDatos.Controls.Add(Me.LabelControl16)
        Me.xtpDatos.Controls.Add(Me.ceAplicaPerRet)
        Me.xtpDatos.Controls.Add(Me.LabelControl15)
        Me.xtpDatos.Controls.Add(Me.LabelControl19)
        Me.xtpDatos.Controls.Add(Me.LabelControl13)
        Me.xtpDatos.Controls.Add(Me.leOrigen)
        Me.xtpDatos.Controls.Add(Me.leMunicipio)
        Me.xtpDatos.Controls.Add(Me.LabelControl12)
        Me.xtpDatos.Controls.Add(Me.leDepto)
        Me.xtpDatos.Controls.Add(Me.LabelControl11)
        Me.xtpDatos.Controls.Add(Me.LabelControl7)
        Me.xtpDatos.Controls.Add(Me.LabelControl21)
        Me.xtpDatos.Controls.Add(Me.LabelControl6)
        Me.xtpDatos.Controls.Add(Me.LabelControl5)
        Me.xtpDatos.Controls.Add(Me.teNIT)
        Me.xtpDatos.Controls.Add(Me.teFax)
        Me.xtpDatos.Controls.Add(Me.LabelControl9)
        Me.xtpDatos.Controls.Add(Me.teCorreoElectronico)
        Me.xtpDatos.Controls.Add(Me.LabelControl10)
        Me.xtpDatos.Controls.Add(Me.teTelefonos)
        Me.xtpDatos.Controls.Add(Me.LabelControl8)
        Me.xtpDatos.Controls.Add(Me.teNRC)
        Me.xtpDatos.Controls.Add(Me.LabelControl4)
        Me.xtpDatos.Controls.Add(Me.teDireccion)
        Me.xtpDatos.Controls.Add(Me.teGiro)
        Me.xtpDatos.Controls.Add(Me.teNombre)
        Me.xtpDatos.Controls.Add(Me.LabelControl3)
        Me.xtpDatos.Controls.Add(Me.teContacto3)
        Me.xtpDatos.Controls.Add(Me.teContacto2)
        Me.xtpDatos.Controls.Add(Me.teContacto1)
        Me.xtpDatos.Controls.Add(Me.LabelControl14)
        Me.xtpDatos.Controls.Add(Me.LabelControl2)
        Me.xtpDatos.Controls.Add(Me.teIdProveedor)
        Me.xtpDatos.Controls.Add(Me.LabelControl1)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(1260, 681)
        Me.xtpDatos.Text = "Proveedor"
        '
        'LePaisOrigen
        '
        Me.LePaisOrigen.EnterMoveNextControl = True
        Me.LePaisOrigen.Location = New System.Drawing.Point(441, 235)
        Me.LePaisOrigen.Name = "LePaisOrigen"
        Me.LePaisOrigen.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LePaisOrigen.Size = New System.Drawing.Size(191, 20)
        Me.LePaisOrigen.TabIndex = 119
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(379, 239)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl27.TabIndex = 120
        Me.LabelControl27.Text = "Pais Origen:"
        '
        'teRepresentante
        '
        Me.teRepresentante.EnterMoveNextControl = True
        Me.teRepresentante.Location = New System.Drawing.Point(155, 388)
        Me.teRepresentante.Name = "teRepresentante"
        Me.teRepresentante.Size = New System.Drawing.Size(670, 20)
        Me.teRepresentante.TabIndex = 117
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(50, 390)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(104, 13)
        Me.LabelControl18.TabIndex = 118
        Me.LabelControl18.Text = "Representante Legal:"
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(367, 151)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl26.TabIndex = 116
        Me.LabelControl26.Text = "Colonia/Barrio:"
        '
        'teColoniaBarrio
        '
        Me.teColoniaBarrio.EnterMoveNextControl = True
        Me.teColoniaBarrio.Location = New System.Drawing.Point(442, 148)
        Me.teColoniaBarrio.Name = "teColoniaBarrio"
        Me.teColoniaBarrio.Size = New System.Drawing.Size(384, 20)
        Me.teColoniaBarrio.TabIndex = 110
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(48, 151)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(105, 13)
        Me.LabelControl25.TabIndex = 115
        Me.LabelControl25.Text = "Otros Datos Domicilio:"
        '
        'teOtrosDatosDomicilio
        '
        Me.teOtrosDatosDomicilio.EnterMoveNextControl = True
        Me.teOtrosDatosDomicilio.Location = New System.Drawing.Point(155, 148)
        Me.teOtrosDatosDomicilio.Name = "teOtrosDatosDomicilio"
        Me.teOtrosDatosDomicilio.Size = New System.Drawing.Size(209, 20)
        Me.teOtrosDatosDomicilio.TabIndex = 109
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(343, 128)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(95, 13)
        Me.LabelControl24.TabIndex = 114
        Me.LabelControl24.Text = "Apartamento/Local:"
        '
        'teApartamentoLocal
        '
        Me.teApartamentoLocal.EnterMoveNextControl = True
        Me.teApartamentoLocal.Location = New System.Drawing.Point(442, 125)
        Me.teApartamentoLocal.Name = "teApartamentoLocal"
        Me.teApartamentoLocal.Size = New System.Drawing.Size(384, 20)
        Me.teApartamentoLocal.TabIndex = 108
        '
        'teNumCasa
        '
        Me.teNumCasa.EnterMoveNextControl = True
        Me.teNumCasa.Location = New System.Drawing.Point(155, 125)
        Me.teNumCasa.Name = "teNumCasa"
        Me.teNumCasa.Size = New System.Drawing.Size(100, 20)
        Me.teNumCasa.TabIndex = 107
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(105, 128)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl23.TabIndex = 113
        Me.LabelControl23.Text = "No. Casa:"
        '
        'teSujeto
        '
        Me.teSujeto.EnterMoveNextControl = True
        Me.teSujeto.Location = New System.Drawing.Point(700, 58)
        Me.teSujeto.Name = "teSujeto"
        Me.teSujeto.Size = New System.Drawing.Size(125, 20)
        Me.teSujeto.TabIndex = 111
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(577, 61)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(117, 13)
        Me.LabelControl22.TabIndex = 112
        Me.LabelControl22.Text = "Número Sujeto Excluído:"
        '
        'rgTipo
        '
        Me.rgTipo.EditValue = 1
        Me.rgTipo.Location = New System.Drawing.Point(155, 279)
        Me.rgTipo.Name = "rgTipo"
        Me.rgTipo.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Pequeño"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Mediano"), New DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Grande")})
        Me.rgTipo.Size = New System.Drawing.Size(271, 21)
        Me.rgTipo.TabIndex = 106
        '
        'teNacionalidad
        '
        Me.teNacionalidad.EnterMoveNextControl = True
        Me.teNacionalidad.Location = New System.Drawing.Point(155, 234)
        Me.teNacionalidad.Name = "teNacionalidad"
        Me.teNacionalidad.Size = New System.Drawing.Size(209, 20)
        Me.teNacionalidad.TabIndex = 76
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(90, 237)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl20.TabIndex = 105
        Me.LabelControl20.Text = "Nacionalidad:"
        '
        'sbGuardar
        '
        Me.sbGuardar.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sbGuardar.Appearance.Options.UseFont = True
        Me.sbGuardar.Location = New System.Drawing.Point(152, 441)
        Me.sbGuardar.Name = "sbGuardar"
        Me.sbGuardar.Size = New System.Drawing.Size(139, 30)
        Me.sbGuardar.TabIndex = 104
        Me.sbGuardar.Text = "&Guardar Proveedor"
        Me.sbGuardar.Visible = False
        '
        'BeCtaContable1
        '
        Me.BeCtaContable1.AutoSize = True
        Me.BeCtaContable1.Location = New System.Drawing.Point(152, 257)
        Me.BeCtaContable1.Name = "BeCtaContable1"
        Me.BeCtaContable1.Size = New System.Drawing.Size(640, 26)
        Me.BeCtaContable1.TabIndex = 77
        Me.BeCtaContable1.ValidarMayor = False
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(511, 306)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl17.TabIndex = 102
        Me.LabelControl17.Text = "Origen del Proveedor:"
        '
        'seDiasCredito
        '
        Me.seDiasCredito.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seDiasCredito.EnterMoveNextControl = True
        Me.seDiasCredito.Location = New System.Drawing.Point(442, 302)
        Me.seDiasCredito.Name = "seDiasCredito"
        Me.seDiasCredito.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seDiasCredito.Properties.Mask.BeepOnError = True
        Me.seDiasCredito.Properties.Mask.EditMask = "f0"
        Me.seDiasCredito.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seDiasCredito.Size = New System.Drawing.Size(69, 20)
        Me.seDiasCredito.TabIndex = 79
        '
        'seLimite
        '
        Me.seLimite.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.seLimite.EnterMoveNextControl = True
        Me.seLimite.Location = New System.Drawing.Point(155, 302)
        Me.seLimite.Name = "seLimite"
        Me.seLimite.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seLimite.Properties.Mask.EditMask = "n2"
        Me.seLimite.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.seLimite.Size = New System.Drawing.Size(130, 20)
        Me.seLimite.TabIndex = 78
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(362, 305)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(77, 13)
        Me.LabelControl16.TabIndex = 100
        Me.LabelControl16.Text = "Días de Crédito:"
        '
        'ceAplicaPerRet
        '
        Me.ceAplicaPerRet.Location = New System.Drawing.Point(440, 280)
        Me.ceAplicaPerRet.Name = "ceAplicaPerRet"
        Me.ceAplicaPerRet.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ceAplicaPerRet.Properties.Appearance.Options.UseFont = True
        Me.ceAplicaPerRet.Properties.Caption = "LE APLICAMOS RETENCIÓN AL COMPRARLE"
        Me.ceAplicaPerRet.Size = New System.Drawing.Size(302, 19)
        Me.ceAplicaPerRet.TabIndex = 97
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(70, 305)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl15.TabIndex = 101
        Me.LabelControl15.Text = "Límite de Crédito:"
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(97, 328)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl19.TabIndex = 99
        Me.LabelControl19.Text = "Contacto 1:"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(69, 260)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl13.TabIndex = 98
        Me.LabelControl13.Text = "Cuenta Contable:"
        '
        'leOrigen
        '
        Me.leOrigen.EnterMoveNextControl = True
        Me.leOrigen.Location = New System.Drawing.Point(618, 302)
        Me.leOrigen.Name = "leOrigen"
        Me.leOrigen.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leOrigen.Size = New System.Drawing.Size(207, 20)
        Me.leOrigen.TabIndex = 74
        '
        'leMunicipio
        '
        Me.leMunicipio.EnterMoveNextControl = True
        Me.leMunicipio.Location = New System.Drawing.Point(442, 213)
        Me.leMunicipio.Name = "leMunicipio"
        Me.leMunicipio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leMunicipio.Size = New System.Drawing.Size(190, 20)
        Me.leMunicipio.TabIndex = 75
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(392, 217)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl12.TabIndex = 96
        Me.LabelControl12.Text = "Municipio:"
        '
        'leDepto
        '
        Me.leDepto.EnterMoveNextControl = True
        Me.leDepto.Location = New System.Drawing.Point(155, 213)
        Me.leDepto.Name = "leDepto"
        Me.leDepto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leDepto.Size = New System.Drawing.Size(209, 20)
        Me.leDepto.TabIndex = 73
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(81, 217)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl11.TabIndex = 95
        Me.LabelControl11.Text = "Departamento:"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(107, 103)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl7.TabIndex = 94
        Me.LabelControl7.Text = "Dirección:"
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(43, 283)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(111, 13)
        Me.LabelControl21.TabIndex = 93
        Me.LabelControl21.Text = "Tipo de Contribuyente:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(24, 82)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(130, 13)
        Me.LabelControl6.TabIndex = 92
        Me.LabelControl6.Text = "Giro / Actividad Económica:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(418, 60)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl5.TabIndex = 91
        Me.LabelControl5.Text = "NIT:"
        '
        'teNIT
        '
        Me.teNIT.EnterMoveNextControl = True
        Me.teNIT.Location = New System.Drawing.Point(442, 58)
        Me.teNIT.Name = "teNIT"
        Me.teNIT.Size = New System.Drawing.Size(131, 20)
        Me.teNIT.TabIndex = 67
        '
        'teFax
        '
        Me.teFax.EnterMoveNextControl = True
        Me.teFax.Location = New System.Drawing.Point(442, 171)
        Me.teFax.Name = "teFax"
        Me.teFax.Size = New System.Drawing.Size(190, 20)
        Me.teFax.TabIndex = 71
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(416, 174)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl9.TabIndex = 88
        Me.LabelControl9.Text = "Fax:"
        '
        'teCorreoElectronico
        '
        Me.teCorreoElectronico.EnterMoveNextControl = True
        Me.teCorreoElectronico.Location = New System.Drawing.Point(155, 192)
        Me.teCorreoElectronico.Name = "teCorreoElectronico"
        Me.teCorreoElectronico.Size = New System.Drawing.Size(477, 20)
        Me.teCorreoElectronico.TabIndex = 72
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(7, 195)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(147, 13)
        Me.LabelControl10.TabIndex = 87
        Me.LabelControl10.Text = "Correo Electrónico / Sitio Web:"
        '
        'teTelefonos
        '
        Me.teTelefonos.EnterMoveNextControl = True
        Me.teTelefonos.Location = New System.Drawing.Point(155, 171)
        Me.teTelefonos.Name = "teTelefonos"
        Me.teTelefonos.Size = New System.Drawing.Size(209, 20)
        Me.teTelefonos.TabIndex = 70
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(103, 174)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(51, 13)
        Me.LabelControl8.TabIndex = 89
        Me.LabelControl8.Text = "Teléfonos:"
        '
        'teNRC
        '
        Me.teNRC.EnterMoveNextControl = True
        Me.teNRC.Location = New System.Drawing.Point(155, 58)
        Me.teNRC.Name = "teNRC"
        Me.teNRC.Size = New System.Drawing.Size(100, 20)
        Me.teNRC.TabIndex = 66
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(129, 62)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl4.TabIndex = 90
        Me.LabelControl4.Text = "NRC:"
        '
        'teDireccion
        '
        Me.teDireccion.EnterMoveNextControl = True
        Me.teDireccion.Location = New System.Drawing.Point(155, 100)
        Me.teDireccion.Name = "teDireccion"
        Me.teDireccion.Size = New System.Drawing.Size(670, 20)
        Me.teDireccion.TabIndex = 69
        '
        'teGiro
        '
        Me.teGiro.EnterMoveNextControl = True
        Me.teGiro.Location = New System.Drawing.Point(155, 79)
        Me.teGiro.Name = "teGiro"
        Me.teGiro.Size = New System.Drawing.Size(670, 20)
        Me.teGiro.TabIndex = 68
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(155, 37)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(670, 20)
        Me.teNombre.TabIndex = 65
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(90, 40)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(64, 13)
        Me.LabelControl3.TabIndex = 86
        Me.LabelControl3.Text = "Razón Social:"
        '
        'teContacto3
        '
        Me.teContacto3.EnterMoveNextControl = True
        Me.teContacto3.Location = New System.Drawing.Point(155, 366)
        Me.teContacto3.Name = "teContacto3"
        Me.teContacto3.Size = New System.Drawing.Size(670, 20)
        Me.teContacto3.TabIndex = 82
        '
        'teContacto2
        '
        Me.teContacto2.EnterMoveNextControl = True
        Me.teContacto2.Location = New System.Drawing.Point(155, 345)
        Me.teContacto2.Name = "teContacto2"
        Me.teContacto2.Size = New System.Drawing.Size(670, 20)
        Me.teContacto2.TabIndex = 81
        '
        'teContacto1
        '
        Me.teContacto1.EnterMoveNextControl = True
        Me.teContacto1.Location = New System.Drawing.Point(155, 324)
        Me.teContacto1.Name = "teContacto1"
        Me.teContacto1.Size = New System.Drawing.Size(670, 20)
        Me.teContacto1.TabIndex = 80
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(97, 371)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl14.TabIndex = 84
        Me.LabelControl14.Text = "Contacto 3:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(97, 350)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl2.TabIndex = 85
        Me.LabelControl2.Text = "Contacto 2:"
        '
        'teIdProveedor
        '
        Me.teIdProveedor.EnterMoveNextControl = True
        Me.teIdProveedor.Location = New System.Drawing.Point(155, 15)
        Me.teIdProveedor.Name = "teIdProveedor"
        Me.teIdProveedor.Size = New System.Drawing.Size(100, 20)
        Me.teIdProveedor.TabIndex = 64
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(59, 18)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(95, 13)
        Me.LabelControl1.TabIndex = 83
        Me.LabelControl1.Text = "Cód. de Proveedor:"
        '
        'com_frmProveedores
        '
        Me.ClientSize = New System.Drawing.Size(1266, 734)
        Me.Controls.Add(Me.xtcProveedores)
        Me.Modulo = "Compras"
        Me.Name = "com_frmProveedores"
        Me.OptionId = "001001"
        Me.Text = "Proveedores"
        Me.TipoFormulario = 1
        Me.Controls.SetChildIndex(Me.xtcProveedores, 0)
        CType(Me.xtcProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcProveedores.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riChkAplicaRetencion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riDepartamentos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riMunicipios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riPaises, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        Me.xtpDatos.PerformLayout()
        CType(Me.LePaisOrigen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teRepresentante.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teColoniaBarrio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teOtrosDatosDomicilio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teApartamentoLocal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumCasa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSujeto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNacionalidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seDiasCredito.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seLimite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ceAplicaPerRet.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leOrigen.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leMunicipio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leDepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNIT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teFax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorreoElectronico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teTelefonos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDireccion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teGiro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teContacto3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teContacto2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teContacto1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teIdProveedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents xtcProveedores As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents IdProveedor As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Nombre As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Nrc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Nit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents AplicaRetencion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riChkAplicaRetencion As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents FechaHoraCreacion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riDepartamentos As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents riMunicipios As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents riPaises As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents rgTipo As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents teNacionalidad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BeCtaContable1 As Nexus.beCtaContable
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents seDiasCredito As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents seLimite As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ceAplicaPerRet As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leOrigen As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leMunicipio As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leDepto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNIT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teFax As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCorreoElectronico As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teTelefonos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDireccion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teGiro As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teContacto3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teContacto2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teContacto1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teIdProveedor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teColoniaBarrio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teOtrosDatosDomicilio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teApartamentoLocal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNumCasa As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teSujeto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teRepresentante As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LePaisOrigen As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
End Class
