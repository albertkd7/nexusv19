﻿
Imports NexusELL.TableEntities
Imports NexusBLL
Public Class cpc_frmAplicaCargos

    Dim bl As New CuentasPCBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim entCargo As New fac_Ventas, ElUsuarioDioClic As Boolean, dtParam As DataTable = blAdmon.ObtieneParametros()

    Private Sub cpc_frmAplicaCargos() Handles Me.Guardar
        If SiEsNulo(beCliente.EditValue, "") = "" Then
            MsgBox("Debe de especificar el código de cliente al que desea aplicar el cargo", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If teMonto.EditValue = 0 Then
            MsgBox("Debe de especificar el monto que desea cargar", MsgBoxStyle.Critical)
            Exit Sub
        End If

        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            MsgBox("La fecha del cargo corresponde a un período ya cerrado", MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If
        If MsgBox("¿Está seguro(a) de aplicar éste cargo?" & Chr(13) & _
                  "Ya no podrá editar los datos", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        With entCargo
            .IdComprobante = 0
            .IdBodega = 1
            .IdSucursal = 1
            .IdPunto = 1
            .Serie = "NDC000"
            .Numero = teReferencia.EditValue
            .Fecha = deFecha.EditValue
            .IdFormaPago = 2
            .IdVendedor = leVendedor.EditValue
            .IdTipoComprobante = dtParam.Rows(0).Item("IdTipoComprobanteCargo")
            .IdCliente = beCliente.EditValue
            .Nombre = teNombre.EditValue
            .Nrc = txtNRC.EditValue
            .Nit = teNit.EditValue
            .SaldoActual = teMonto.EditValue
            .TotalComprobante = teMonto.EditValue
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
        End With
        Dim msj As String = ""
        Try
            entCargo.IdComprobante = objFunciones.ObtenerUltimoId("fac_Ventas", "IdComprobante") + 1
            objTablas.fac_VentasInsert(entCargo)
        Catch ex As Exception
            msj = ex.Message()
        End Try

        If msj = "" Then
            MsgBox("El cargo ha sido aplicado con éxito", MsgBoxStyle.Information, "Nota")
            '            LimpiaPantalla()
        Else
            MsgBox("Error al insertar cargo" + Chr(13) + msj, MsgBoxStyle.Critical, "Nota")
        End If
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("fac_Ventas", "IdComprobante") + 1
        teReferencia.EditValue = ""
        teMonto.EditValue = 0.0
        teConcepto.EditValue = ""
        teReferencia.Focus()
    End Sub

    Private Sub beCliente_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs)
        beCliente.EditValue = ""
        ElUsuarioDioClic = True
        beCliente_Validated(sender, New System.EventArgs)
    End Sub


    Private Sub cpc_frmAplicaCargos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("fac_Ventas", "IdComprobante") + 1
        deFecha.EditValue = Today
        objCombos.fac_Vendedores(leVendedor, "")
        If SiEsNulo(dtParam.Rows(0).Item("IdTipoComprobanteCargo"), 0) = 0 Then
            MsgBox("NO SE HA CONFIGURADO DOCUMENTO DE CARGO !!" + Chr(13), MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
    End Sub

    Private Sub beCliente_ButtonClick_1(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCliente.ButtonClick
        beCliente.EditValue = ""
        ElUsuarioDioClic = True
        beCliente_Validated(sender, New System.EventArgs)
    End Sub
    Private Sub beCliente_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCliente.Validated
        If Not ElUsuarioDioClic And beCliente.EditValue = "" Then
            Exit Sub
        End If
        Dim ent As fac_Clientes = objConsultas.cnsClientes(fac_frmConsultaClientes, beCliente.EditValue)
        ElUsuarioDioClic = False
        beCliente.EditValue = ent.IdCliente
        teNombre.EditValue = ent.Nombre
        txtNRC.EditValue = ent.Nrc
        teNombre.Focus()
    End Sub
End Class
