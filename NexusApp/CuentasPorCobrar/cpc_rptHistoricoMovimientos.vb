Public Class cpc_rptHistoricoMovimientos
    Dim nSaldo As Decimal = 0.0

    Private Sub XrLabel27_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel27.BeforePrint
        XrLabel27.Text = nSaldo
    End Sub

    Private Sub XrLabel20_BeforePrint(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles XrLabel20.BeforePrint
        nSaldo = SiEsNulo(GetCurrentColumnValue("Saldo"), 0.0)
    End Sub
End Class