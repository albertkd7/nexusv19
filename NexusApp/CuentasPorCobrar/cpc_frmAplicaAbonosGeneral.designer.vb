﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpc_frmAplicaAbonosGeneral
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cpc_frmAplicaAbonosGeneral))
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl
        Me.lePunto = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl
        Me.txtNumCheque = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl
        Me.txtBancoProcede = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.txtNroRecibo = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.cboEjecutivo = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.leTipoAbono = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl
        Me.txtConcepto = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl
        Me.leCuentaBanco = New DevExpress.XtraEditors.LookUpEdit
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl
        Me.deFecha = New DevExpress.XtraEditors.DateEdit
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl
        Me.teDiferencia = New DevExpress.XtraEditors.TextEdit
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.colCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
        Me.NroComprobante = New DevExpress.XtraGrid.Columns.GridColumn
        Me.MontoAbonar = New DevExpress.XtraGrid.Columns.GridColumn
        Me.txtMonto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.FechaComp = New DevExpress.XtraGrid.Columns.GridColumn
        Me.FechaVencto = New DevExpress.XtraGrid.Columns.GridColumn
        Me.TotalComprob = New DevExpress.XtraGrid.Columns.GridColumn
        Me.SaldoComprob = New DevExpress.XtraGrid.Columns.GridColumn
        Me.IdComprobante = New DevExpress.XtraGrid.Columns.GridColumn
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.chkAbonar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Me.pcBotones = New DevExpress.XtraEditors.PanelControl
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBancoProcede.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNroRecibo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEjecutivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCuentaBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDiferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMonto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAbonar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcBotones.SuspendLayout()
        Me.SuspendLayout()
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.lePunto)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.LabelControl16)
        Me.pcHeader.Controls.Add(Me.txtNumCheque)
        Me.pcHeader.Controls.Add(Me.LabelControl15)
        Me.pcHeader.Controls.Add(Me.txtBancoProcede)
        Me.pcHeader.Controls.Add(Me.LabelControl14)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Controls.Add(Me.txtNroRecibo)
        Me.pcHeader.Controls.Add(Me.LabelControl4)
        Me.pcHeader.Controls.Add(Me.cboEjecutivo)
        Me.pcHeader.Controls.Add(Me.LabelControl5)
        Me.pcHeader.Controls.Add(Me.leTipoAbono)
        Me.pcHeader.Controls.Add(Me.LabelControl9)
        Me.pcHeader.Controls.Add(Me.txtConcepto)
        Me.pcHeader.Controls.Add(Me.LabelControl11)
        Me.pcHeader.Controls.Add(Me.leCuentaBanco)
        Me.pcHeader.Controls.Add(Me.PictureBox1)
        Me.pcHeader.Controls.Add(Me.LabelControl12)
        Me.pcHeader.Controls.Add(Me.deFecha)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(868, 116)
        Me.pcHeader.TabIndex = 1
        '
        'lePunto
        '
        Me.lePunto.Location = New System.Drawing.Point(471, 27)
        Me.lePunto.Name = "lePunto"
        Me.lePunto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePunto.Size = New System.Drawing.Size(183, 20)
        Me.lePunto.TabIndex = 60
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(391, 30)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl2.TabIndex = 61
        Me.LabelControl2.Text = "Punto de Venta:"
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(471, 8)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(183, 20)
        Me.leSucursal.TabIndex = 2
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(425, 11)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl16.TabIndex = 59
        Me.LabelControl16.Text = "Sucursal:"
        '
        'txtNumCheque
        '
        Me.txtNumCheque.EnterMoveNextControl = True
        Me.txtNumCheque.Location = New System.Drawing.Point(471, 49)
        Me.txtNumCheque.Name = "txtNumCheque"
        Me.txtNumCheque.Size = New System.Drawing.Size(135, 20)
        Me.txtNumCheque.TabIndex = 9
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(408, 52)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl15.TabIndex = 56
        Me.LabelControl15.Text = "No. Cheque:"
        '
        'txtBancoProcede
        '
        Me.txtBancoProcede.EnterMoveNextControl = True
        Me.txtBancoProcede.Location = New System.Drawing.Point(101, 73)
        Me.txtBancoProcede.Name = "txtBancoProcede"
        Me.txtBancoProcede.Size = New System.Drawing.Size(273, 20)
        Me.txtBancoProcede.TabIndex = 8
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(5, 75)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl14.TabIndex = 54
        Me.LabelControl14.Text = "Banco Procedencia:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(27, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl1.TabIndex = 27
        Me.LabelControl1.Text = "No. de Recibo:"
        '
        'txtNroRecibo
        '
        Me.txtNroRecibo.EnterMoveNextControl = True
        Me.txtNroRecibo.Location = New System.Drawing.Point(101, 9)
        Me.txtNroRecibo.Name = "txtNroRecibo"
        Me.txtNroRecibo.Size = New System.Drawing.Size(111, 20)
        Me.txtNroRecibo.TabIndex = 0
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(50, 34)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl4.TabIndex = 36
        Me.LabelControl4.Text = "Ejecutivo:"
        '
        'cboEjecutivo
        '
        Me.cboEjecutivo.EnterMoveNextControl = True
        Me.cboEjecutivo.Location = New System.Drawing.Point(101, 31)
        Me.cboEjecutivo.Name = "cboEjecutivo"
        Me.cboEjecutivo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEjecutivo.Size = New System.Drawing.Size(273, 20)
        Me.cboEjecutivo.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(396, 74)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl5.TabIndex = 37
        Me.LabelControl5.Text = "Tipo de Abono:"
        '
        'leTipoAbono
        '
        Me.leTipoAbono.EnterMoveNextControl = True
        Me.leTipoAbono.Location = New System.Drawing.Point(471, 72)
        Me.leTipoAbono.Name = "leTipoAbono"
        Me.leTipoAbono.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoAbono.Size = New System.Drawing.Size(135, 20)
        Me.leTipoAbono.TabIndex = 12
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(48, 96)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl9.TabIndex = 46
        Me.LabelControl9.Text = "Concepto:"
        '
        'txtConcepto
        '
        Me.txtConcepto.EnterMoveNextControl = True
        Me.txtConcepto.Location = New System.Drawing.Point(101, 93)
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.Size = New System.Drawing.Size(273, 20)
        Me.txtConcepto.TabIndex = 14
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(17, 56)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl11.TabIndex = 49
        Me.LabelControl11.Text = "Banco Depositar:"
        '
        'leCuentaBanco
        '
        Me.leCuentaBanco.EnterMoveNextControl = True
        Me.leCuentaBanco.Location = New System.Drawing.Point(101, 52)
        Me.leCuentaBanco.Name = "leCuentaBanco"
        Me.leCuentaBanco.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCuentaBanco.Size = New System.Drawing.Size(273, 20)
        Me.leCuentaBanco.TabIndex = 7
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.LightGray
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(670, 13)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(147, 100)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 51
        Me.PictureBox1.TabStop = False
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(253, 11)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl12.TabIndex = 52
        Me.LabelControl12.Text = "Fecha:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(288, 9)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFecha.Size = New System.Drawing.Size(86, 20)
        Me.deFecha.TabIndex = 1
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(628, 10)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl10.TabIndex = 47
        Me.LabelControl10.Text = "Diferencia:"
        '
        'teDiferencia
        '
        Me.teDiferencia.EditValue = "0"
        Me.teDiferencia.Location = New System.Drawing.Point(686, 6)
        Me.teDiferencia.Name = "teDiferencia"
        Me.teDiferencia.Properties.Appearance.Options.UseTextOptions = True
        Me.teDiferencia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teDiferencia.Properties.Mask.EditMask = "n2"
        Me.teDiferencia.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teDiferencia.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teDiferencia.Properties.ReadOnly = True
        Me.teDiferencia.Size = New System.Drawing.Size(131, 20)
        Me.teDiferencia.TabIndex = 48
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.teDiferencia)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl1.Location = New System.Drawing.Point(0, 401)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(868, 34)
        Me.PanelControl1.TabIndex = 19
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 116)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1, Me.chkAbonar, Me.txtMonto})
        Me.gc.Size = New System.Drawing.Size(817, 285)
        Me.gc.TabIndex = 2
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCliente, Me.GridColumn2, Me.GridColumn1, Me.NroComprobante, Me.MontoAbonar, Me.FechaComp, Me.FechaVencto, Me.TotalComprob, Me.SaldoComprob, Me.IdComprobante})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'colCliente
        '
        Me.colCliente.Caption = "IdCliente"
        Me.colCliente.FieldName = "IdCliente"
        Me.colCliente.Name = "colCliente"
        Me.colCliente.Visible = True
        Me.colCliente.VisibleIndex = 0
        Me.colCliente.Width = 76
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Cliente"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.OptionsColumn.AllowFocus = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 113
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Seríe Doc."
        Me.GridColumn1.FieldName = "Serie"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 2
        Me.GridColumn1.Width = 68
        '
        'NroComprobante
        '
        Me.NroComprobante.Caption = "No. Comprobante"
        Me.NroComprobante.FieldName = "Numero"
        Me.NroComprobante.Name = "NroComprobante"
        Me.NroComprobante.Visible = True
        Me.NroComprobante.VisibleIndex = 3
        Me.NroComprobante.Width = 93
        '
        'MontoAbonar
        '
        Me.MontoAbonar.Caption = "Monto Abonar"
        Me.MontoAbonar.ColumnEdit = Me.txtMonto
        Me.MontoAbonar.FieldName = "MontoAbonar"
        Me.MontoAbonar.Name = "MontoAbonar"
        Me.MontoAbonar.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontoAbonar", "{0:n2}")})
        Me.MontoAbonar.Visible = True
        Me.MontoAbonar.VisibleIndex = 4
        Me.MontoAbonar.Width = 107
        '
        'txtMonto
        '
        Me.txtMonto.AutoHeight = False
        Me.txtMonto.Mask.EditMask = "n2"
        Me.txtMonto.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtMonto.Mask.UseMaskAsDisplayFormat = True
        Me.txtMonto.Name = "txtMonto"
        '
        'FechaComp
        '
        Me.FechaComp.Caption = "Fecha Comprob."
        Me.FechaComp.FieldName = "Fecha"
        Me.FechaComp.Name = "FechaComp"
        Me.FechaComp.OptionsColumn.AllowEdit = False
        Me.FechaComp.OptionsColumn.AllowFocus = False
        Me.FechaComp.Visible = True
        Me.FechaComp.VisibleIndex = 5
        Me.FechaComp.Width = 83
        '
        'FechaVencto
        '
        Me.FechaVencto.Caption = "Vencimiento"
        Me.FechaVencto.FieldName = "FechaVenc"
        Me.FechaVencto.Name = "FechaVencto"
        Me.FechaVencto.OptionsColumn.AllowEdit = False
        Me.FechaVencto.OptionsColumn.AllowFocus = False
        Me.FechaVencto.Visible = True
        Me.FechaVencto.VisibleIndex = 6
        Me.FechaVencto.Width = 83
        '
        'TotalComprob
        '
        Me.TotalComprob.Caption = "Total"
        Me.TotalComprob.FieldName = "TotalComprobante"
        Me.TotalComprob.Name = "TotalComprob"
        Me.TotalComprob.OptionsColumn.AllowEdit = False
        Me.TotalComprob.OptionsColumn.AllowFocus = False
        Me.TotalComprob.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalComprobante", "{0:n2}")})
        Me.TotalComprob.Visible = True
        Me.TotalComprob.VisibleIndex = 7
        Me.TotalComprob.Width = 83
        '
        'SaldoComprob
        '
        Me.SaldoComprob.Caption = "Saldo"
        Me.SaldoComprob.FieldName = "Saldo"
        Me.SaldoComprob.Name = "SaldoComprob"
        Me.SaldoComprob.OptionsColumn.AllowEdit = False
        Me.SaldoComprob.OptionsColumn.AllowFocus = False
        Me.SaldoComprob.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Saldo", "{0:n2}")})
        Me.SaldoComprob.Visible = True
        Me.SaldoComprob.VisibleIndex = 8
        Me.SaldoComprob.Width = 93
        '
        'IdComprobante
        '
        Me.IdComprobante.Caption = "Id"
        Me.IdComprobante.FieldName = "IdComprobante"
        Me.IdComprobante.Name = "IdComprobante"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'chkAbonar
        '
        Me.chkAbonar.AutoHeight = False
        Me.chkAbonar.Name = "chkAbonar"
        '
        'pcBotones
        '
        Me.pcBotones.Controls.Add(Me.cmdBorrar)
        Me.pcBotones.Dock = System.Windows.Forms.DockStyle.Right
        Me.pcBotones.Location = New System.Drawing.Point(831, 116)
        Me.pcBotones.Name = "pcBotones"
        Me.pcBotones.Size = New System.Drawing.Size(37, 285)
        Me.pcBotones.TabIndex = 20
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(5, 51)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(27, 23)
        Me.cmdBorrar.TabIndex = 34
        '
        'cpc_frmAplicaAbonosGeneral
        '
        Me.ClientSize = New System.Drawing.Size(868, 460)
        Me.Controls.Add(Me.pcBotones)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.pcHeader)
        Me.Modulo = "Cuentas Por Cobrar"
        Me.Name = "cpc_frmAplicaAbonosGeneral"
        Me.OptionId = "002006"
        Me.Text = "Aplicación de abonos"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.pcHeader, 0)
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        Me.Controls.SetChildIndex(Me.pcBotones, 0)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBancoProcede.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNroRecibo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEjecutivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCuentaBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDiferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMonto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAbonar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcBotones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcBotones.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNroRecibo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cboEjecutivo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoAbono As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCuentaBanco As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDiferencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents chkAbonar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents NroComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents FechaComp As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents FechaVencto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TotalComprob As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SaldoComprob As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents MontoAbonar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtMonto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents IdComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtNumCheque As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtBancoProcede As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents colCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents lePunto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents pcBotones As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton

End Class
