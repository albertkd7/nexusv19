﻿Imports DevExpress.XtraReports.UI
Imports NexusBLL
Imports NexusELL.TableEntities
Imports System.Text
Public Class cpc_frmControlComprobanteRetencion
    Dim bl As New FacturaBLL(g_ConnectionString)
    Private Sub cpc_frmControlComprobanteRetencion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deHasta.EditValue = Today
        deDesde.EditValue = Today 'CDate(String.Format("1/{0}/{1}", Month(Today), 2015))
        objCombos.fac_Vendedores(leGerente, "-- TODOS LOS VENDEDORES --")

        If rgTipo.EditValue = 1 Then
            rgOrden.Visible = False
            lblOrdenado.Visible = False
        Else
            rgOrden.Visible = True
            lblOrdenado.Visible = True
        End If

    End Sub


    Private Sub cpc_frmControlComprobanteRetencion_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.fac_InformeComprobantesRetencion(deDesde.EditValue, deHasta.EditValue, rgTipo.EditValue, leGerente.EditValue, beCliente.EditValue, 0)

        If rgTipo.EditValue = 1 Then
            Dim rpt As New fac_rptInformeComprobantesRetencion() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            If rgTipo.EditValue = 1 Then
                rpt.xrlTitulo.Text = "INFORME DE COMPROBANTES DE RETENCION PENDIENTES"
                rpt.xrlPeriodo.Text = "Al " & FechaToString(deHasta.EditValue, deHasta.EditValue)
            Else
                rpt.xrlTitulo.Text = "INFORME DE COMPROBANTES DE RETENCION RECIBIDOS"
                rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
            End If

            rpt.ShowPreviewDialog()
        Else
            If rgOrden.EditValue = 1 Then
                Dim rpt As New fac_rptInformeComprobantesRetencion() With {.DataSource = dt, .DataMember = ""}
                rpt.xrlEmpresa.Text = gsNombre_Empresa
                If rgTipo.EditValue = 1 Then
                    rpt.xrlTitulo.Text = "INFORME DE COMPROBANTES DE RETENCION PENDIENTES"
                    rpt.xrlPeriodo.Text = "Al " & FechaToString(deHasta.EditValue, deHasta.EditValue)
                Else
                    rpt.xrlTitulo.Text = "INFORME DE COMPROBANTES DE RETENCION RECIBIDOS"
                    rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
                End If

                rpt.ShowPreviewDialog()
            Else
                Dim rpt As New fac_rptInformeComprobanteRetencionFecha() With {.DataSource = dt, .DataMember = ""}
                rpt.xrlEmpresa.Text = gsNombre_Empresa
                If rgTipo.EditValue = 1 Then
                    rpt.xrlTitulo.Text = "INFORME DE COMPROBANTES DE RETENCION PENDIENTES"
                    rpt.xrlPeriodo.Text = "Al " & FechaToString(deHasta.EditValue, deHasta.EditValue)
                Else
                    rpt.xrlTitulo.Text = "INFORME DE COMPROBANTES DE RETENCION RECIBIDOS"
                    rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
                End If

                rpt.ShowPreviewDialog()
            End If
        End If

    End Sub

    Private Sub beCliente_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCliente.ButtonClick
        beCliente.EditValue = ""
        beCliente_Validated(sender, New System.EventArgs)
    End Sub
    Private Sub beCliente_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCliente.Validated

        Dim ent As fac_Clientes = objConsultas.cnsClientes(fac_frmConsultaClientes, beCliente.EditValue)


        beCliente.EditValue = ent.IdCliente
        teNombre.EditValue = ent.Nombre
        teNombre.Focus()

    End Sub

    Private Sub sbEnviarClientes_Click(sender As Object, e As EventArgs) Handles sbEnviarClientes.Click
        Dim dtCliente As New DataTable
        Dim dt As New DataTable
        Dim IdCliente As String = ""
        Dim msj As String = ""
        If beCliente.EditValue <> "" Then
            IdCliente = beCliente.EditValue
        End If

        dtCliente = bl.fac_InformeComprobantesRetencion(deDesde.EditValue, deHasta.EditValue, rgTipo.EditValue, leGerente.EditValue, beCliente.EditValue, 2)
        ''   pbcPorcentaje.Visible = True
        pbcPorcentaje.Properties.Minimum = 0
        pbcPorcentaje.Properties.Step = 1
        pbcPorcentaje.Properties.PercentView = True
        pbcPorcentaje.Properties.Maximum = dtCliente.Rows.Count
        For Each row As DataRow In dtCliente.Rows
            Dim mensaje As New StringBuilder()

            dt = bl.fac_InformeComprobantesRetencion(deDesde.EditValue, deHasta.EditValue, rgTipo.EditValue, -1, row("IdCliente"), 0)
            Dim sAsunto As String = "Comprobantes de retención pendientes " & row("NombreCliente")
            mensaje.AppendLine("<left>")
            mensaje.AppendLine("Estimados, buen día:")
            mensaje.AppendLine("<br>")
            mensaje.AppendLine("Deseo que sus actividades se desarrollen con éxito.")
            mensaje.AppendLine("<br>")
            mensaje.AppendLine("Adjunto el detalle de Comprobantes de Retención pendientes a la fecha para enviar por ellos, favor indicarme el lugar en donde serán retirados.")
            mensaje.AppendLine("<br>")
            mensaje.AppendLine("Quedo al pendiente de sus comentarios y gracias por su pronta gestión.")
            mensaje.AppendLine("<br>")
            mensaje.AppendLine("Atentos saludos")
            mensaje.AppendLine("<br>")
            mensaje.AppendLine("<center>")
            mensaje.AppendLine("<h2>" & gsNombre_Empresa & "</h2>")
            mensaje.AppendLine("<h3>COMPROBANTES DE RETENCIÓN PENDIENTES A LA FECHA</h3>")
            mensaje.AppendLine("<h3>" & "A LA FECHA: " & (FechaToString(deHasta.EditValue, deHasta.EditValue)).ToUpper() & "</h3>")
            mensaje.AppendLine("</center>")
            mensaje.AppendLine("<table border='1' style='width: 80%; margin: 0 auto;'>")
            mensaje.AppendLine("<tr><td style='text-align: center;color: #fff;background-color: #00008b;'><h4> FECHA </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> NUM. DOCUMENTO </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> NOMBRE CLIENTE </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> CONTRATO </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> CONCEPTO </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> TOTAL EXENTO </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> TOTAL AFECTO </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> TOTAL IVA </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> 1% RETENIDO </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> TOTAL </h4></td></tr>")
            For Each row2 As DataRow In dt.Rows
                If row2("TotalImpuesto1") > 0.0 Then
                    mensaje.AppendLine("<tr><td style='text-align: left'>" & row2("Fecha") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & row2("Abreviatura") & "-" & row2("Numero") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & row2("NombreCliente") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & row2("NumeroContrato") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & row2("Concepto") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & Format(CDec(row2("TotalExento")), "$##,##0.00") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & Format(CDec(row2("TotalAfecto")), "$##,##0.00") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & Format(CDec(row2("TotalIva")), "$##,##0.00") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & Format(CDec(row2("TotalImpuesto1")), "$##,##0.00") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & Format(CDec(row2("TotalComprobante")), "$##,##0.00") & "</td></tr>")
                End If
            Next
            mensaje.AppendLine("</table>")
            mensaje.AppendLine("<br>")

            mensaje.AppendLine("<left>")
            mensaje.AppendLine("<br>")
            mensaje.AppendLine("Atentos saludos")
            mensaje.AppendLine("<br>")
            mensaje.AppendLine("Licda. Katya Rocío Calderón Navarro")
            mensaje.AppendLine("<br>")
            mensaje.AppendLine("Facturación y Cobros")
            mensaje.AppendLine("<br>")
            mensaje.AppendLine("KPMG, S.A.")
            mensaje.AppendLine("<br>")
            mensaje.AppendLine("Tel.: (503)2213-8422")

            msj = msj & EnviarCorreo(mensaje, row("CorreoElectronico"), sAsunto)
            pbcPorcentaje.PerformStep()
            pbcPorcentaje.Update()
        Next
        If msj = "" Then
            MessageBox.Show("Correos enviados exitosamente", "Exito!", MessageBoxButtons.OK)
        Else
            MessageBox.Show("Error en envio de Correos", "Exito!", MessageBoxButtons.OK)
        End If
        Close()
        ''pbcPorcentaje.Visible = False
    End Sub

    Private Sub sbEnviarCorreos_Click(sender As Object, e As EventArgs) Handles sbEnviarCorreos.Click
        Dim dtGerente As New DataTable
        Dim dt As New DataTable
        Dim IdGerente As Integer = -1
        Dim msj As String = ""
        If leGerente.EditValue > 0 Then
            IdGerente = leGerente.EditValue
        End If



        dtGerente = bl.fac_InformeComprobantesRetencion(deDesde.EditValue, deHasta.EditValue, rgTipo.EditValue, leGerente.EditValue, beCliente.EditValue, 1)
        ''   pbcPorcentaje.Visible = True
        pbcPorcentaje.Properties.Minimum = 0
        pbcPorcentaje.Properties.Step = 1
        pbcPorcentaje.Properties.PercentView = True
        pbcPorcentaje.Properties.Maximum = dtGerente.Rows.Count
        For Each row As DataRow In dtGerente.Rows
            Dim mensaje As New StringBuilder()

            dt = bl.fac_InformeComprobantesRetencion(deDesde.EditValue, deHasta.EditValue, rgTipo.EditValue, row("IdVendedor"), beCliente.EditValue, 0)
            Dim sAsunto As String = "Comprobantes de retención pendientes " & row("Vendedor")
            mensaje.AppendLine("<center>")
            mensaje.AppendLine("<h2>" & gsNombre_Empresa & "</h2>")
            mensaje.AppendLine("<h3>COMPROBANTES DE RETENCIÓN PENDIENTES A LA FECHA</h3>")
            mensaje.AppendLine("<h3>" & "A LA FECHA: " & (FechaToString(deHasta.EditValue, deHasta.EditValue)).ToUpper() & "</h3>")
            mensaje.AppendLine("</center>")
            mensaje.AppendLine("<table border='1' style='width: 80%; margin: 0 auto;'>")
            mensaje.AppendLine("<tr><td style='text-align: center;color: #fff;background-color: #00008b;'><h4> FECHA </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> NUM. DOCUMENTO </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> NOMBRE CLIENTE </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> CONTRATO </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> CONCEPTO </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> TOTAL EXENTO </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> TOTAL AFECTO </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> TOTAL IVA </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> 1% RETENIDO </h4></td>")
            mensaje.AppendLine("<td style='text-align: center;color: #fff;background-color: #00008b;'><h4> TOTAL </h4></td></tr>")
            For Each row2 As DataRow In dt.Rows
                If row2("TotalImpuesto1") > 0.0 Then
                    mensaje.AppendLine("<tr><td style='text-align: left'>" & row2("Fecha") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & row2("Abreviatura") & "-" & row2("Numero") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & row2("NombreCliente") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & row2("NumeroContrato") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & row2("Concepto") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & Format(CDec(row2("TotalExento")), "$##,##0.00") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & Format(CDec(row2("TotalAfecto")), "$##,##0.00") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & Format(CDec(row2("TotalIva")), "$##,##0.00") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & Format(CDec(row2("TotalImpuesto1")), "$##,##0.00") & "</td>")
                    mensaje.AppendLine("<td style='text-align: left'>" & Format(CDec(row2("TotalComprobante")), "$##,##0.00") & "</td></tr>")
                End If
            Next
            mensaje.AppendLine("</table>")
            mensaje.AppendLine("<br>")

            mensaje.AppendLine("<left>")
            mensaje.AppendLine("<br>")
            mensaje.AppendLine("Atentos saludos")
            mensaje.AppendLine("<br>")
            'mensaje.AppendLine("Licda. Katya Rocío Calderón Navarro")
            'mensaje.AppendLine("<br>")
            'mensaje.AppendLine("Facturación y Cobros")
            'mensaje.AppendLine("<br>")
            'mensaje.AppendLine("KPMG, S.A.")
            'mensaje.AppendLine("<br>")
            'mensaje.AppendLine("Tel.: (503)2213-8422")

            msj = msj & EnviarCorreo(mensaje, row("Email"), sAsunto)
            pbcPorcentaje.PerformStep()
            pbcPorcentaje.Update()
        Next
        If msj = "" Then
            MessageBox.Show("Correos enviados exitosamente", "Exito!", MessageBoxButtons.OK)
        Else
            MessageBox.Show("Error en envio de Correos", "Exito!", MessageBoxButtons.OK)
        End If
        Close()
        ''pbcPorcentaje.Visible = False
    End Sub

    Private Sub rgTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgTipo.SelectedIndexChanged
        If rgTipo.EditValue = 1 Then
            rgOrden.Visible = False
            lblOrdenado.Visible = False
        Else
            rgOrden.Visible = True
            lblOrdenado.Visible = True
        End If
    End Sub
End Class
