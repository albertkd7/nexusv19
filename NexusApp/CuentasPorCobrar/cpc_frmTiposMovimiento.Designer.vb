﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpc_frmTiposMovimiento
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.releTipoPartida = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.releCargoAbono = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teId = New DevExpress.XtraEditors.TextEdit()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.leTipoPartida = New DevExpress.XtraEditors.LookUpEdit()
        Me.leTipoAplicacion = New DevExpress.XtraEditors.LookUpEdit()
        Me.BeCtaContable1 = New Nexus.beCtaContable()
        Me.chkAfectaBancos = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.releTipoPartida, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.releCargoAbono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoAplicacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAfectaBancos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.gc)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(483, 331)
        Me.PanelControl1.TabIndex = 4
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc.Location = New System.Drawing.Point(2, 2)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.releTipoPartida, Me.releCargoAbono})
        Me.gc.Size = New System.Drawing.Size(479, 327)
        Me.gc.TabIndex = 3
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.NewItemRowText = "Click aquí para nuevo registro. Esc para cancelar"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsCustomization.AllowGroup = False
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Id."
        Me.GridColumn1.FieldName = "IdTipo"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 49
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Tipo de Movimiento"
        Me.GridColumn2.FieldName = "Nombre"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 210
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Tipo de Partida"
        Me.GridColumn3.ColumnEdit = Me.releTipoPartida
        Me.GridColumn3.FieldName = "IdTipoPartida"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 232
        '
        'releTipoPartida
        '
        Me.releTipoPartida.AutoHeight = False
        Me.releTipoPartida.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.releTipoPartida.Name = "releTipoPartida"
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Tipo de Aplicación Contable"
        Me.GridColumn4.ColumnEdit = Me.releCargoAbono
        Me.GridColumn4.FieldName = "TipoAplicacion"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 3
        Me.GridColumn4.Width = 153
        '
        'releCargoAbono
        '
        Me.releCargoAbono.AutoHeight = False
        Me.releCargoAbono.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.releCargoAbono.Name = "releCargoAbono"
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Código Contable"
        Me.GridColumn5.FieldName = "IdCuentaContable"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 4
        Me.GridColumn5.Width = 168
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(519, 29)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(15, 13)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "ID:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(519, 69)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(96, 13)
        Me.LabelControl2.TabIndex = 6
        Me.LabelControl2.Text = "Tipo de Movimiento:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(519, 111)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(76, 13)
        Me.LabelControl3.TabIndex = 7
        Me.LabelControl3.Text = "Tipo de Partida:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(519, 162)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(135, 13)
        Me.LabelControl4.TabIndex = 8
        Me.LabelControl4.Text = "Tipo de Aplicación Contable:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(519, 208)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(83, 13)
        Me.LabelControl5.TabIndex = 9
        Me.LabelControl5.Text = "Código Contable:"
        '
        'teId
        '
        Me.teId.Location = New System.Drawing.Point(519, 44)
        Me.teId.Name = "teId"
        Me.teId.Size = New System.Drawing.Size(100, 20)
        Me.teId.TabIndex = 10
        '
        'teNombre
        '
        Me.teNombre.Location = New System.Drawing.Point(519, 86)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Size = New System.Drawing.Size(326, 20)
        Me.teNombre.TabIndex = 11
        '
        'leTipoPartida
        '
        Me.leTipoPartida.Location = New System.Drawing.Point(519, 130)
        Me.leTipoPartida.Name = "leTipoPartida"
        Me.leTipoPartida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoPartida.Size = New System.Drawing.Size(326, 20)
        Me.leTipoPartida.TabIndex = 76
        '
        'leTipoAplicacion
        '
        Me.leTipoAplicacion.Location = New System.Drawing.Point(519, 179)
        Me.leTipoAplicacion.Name = "leTipoAplicacion"
        Me.leTipoAplicacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoAplicacion.Size = New System.Drawing.Size(326, 20)
        Me.leTipoAplicacion.TabIndex = 77
        '
        'BeCtaContable1
        '
        Me.BeCtaContable1.Location = New System.Drawing.Point(519, 227)
        Me.BeCtaContable1.Name = "BeCtaContable1"
        Me.BeCtaContable1.Size = New System.Drawing.Size(599, 26)
        Me.BeCtaContable1.TabIndex = 78
        Me.BeCtaContable1.ValidarMayor = False
        '
        'chkAfectaBancos
        '
        Me.chkAfectaBancos.Location = New System.Drawing.Point(519, 260)
        Me.chkAfectaBancos.Name = "chkAfectaBancos"
        Me.chkAfectaBancos.Properties.Caption = "Afecta Bancos?"
        Me.chkAfectaBancos.Size = New System.Drawing.Size(175, 19)
        Me.chkAfectaBancos.TabIndex = 79
        '
        'cpc_frmTiposMovimiento
        '
        Me.ClientSize = New System.Drawing.Size(1127, 356)
        Me.Controls.Add(Me.chkAfectaBancos)
        Me.Controls.Add(Me.BeCtaContable1)
        Me.Controls.Add(Me.leTipoAplicacion)
        Me.Controls.Add(Me.leTipoPartida)
        Me.Controls.Add(Me.teNombre)
        Me.Controls.Add(Me.teId)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Modulo = "Cuentas por Cobrar"
        Me.Name = "cpc_frmTiposMovimiento"
        Me.OptionId = "001002"
        Me.Text = "Tipos de Aplicación"
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        Me.Controls.SetChildIndex(Me.LabelControl3, 0)
        Me.Controls.SetChildIndex(Me.LabelControl4, 0)
        Me.Controls.SetChildIndex(Me.LabelControl5, 0)
        Me.Controls.SetChildIndex(Me.teId, 0)
        Me.Controls.SetChildIndex(Me.teNombre, 0)
        Me.Controls.SetChildIndex(Me.leTipoPartida, 0)
        Me.Controls.SetChildIndex(Me.leTipoAplicacion, 0)
        Me.Controls.SetChildIndex(Me.BeCtaContable1, 0)
        Me.Controls.SetChildIndex(Me.chkAfectaBancos, 0)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.releTipoPartida, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.releCargoAbono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoPartida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoAplicacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAfectaBancos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents releTipoPartida As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents releCargoAbono As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teId As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leTipoPartida As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leTipoAplicacion As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents BeCtaContable1 As Nexus.beCtaContable
    Friend WithEvents chkAfectaBancos As DevExpress.XtraEditors.CheckEdit

End Class
