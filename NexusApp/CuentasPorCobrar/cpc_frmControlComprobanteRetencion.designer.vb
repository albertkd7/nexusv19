﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpc_frmControlComprobanteRetencion
    Inherits Nexus.gen_frmBaseRpt

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.deHasta = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.rgTipo = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.deDesde = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.leGerente = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.beCliente = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.sbEnviarCorreos = New DevExpress.XtraEditors.SimpleButton()
        Me.pbcPorcentaje = New DevExpress.XtraEditors.ProgressBarControl()
        Me.sbEnviarClientes = New DevExpress.XtraEditors.SimpleButton()
        Me.lblOrdenado = New DevExpress.XtraEditors.LabelControl()
        Me.rgOrden = New DevExpress.XtraEditors.RadioGroup()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leGerente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbcPorcentaje.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgOrden.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.lblOrdenado)
        Me.GroupControl1.Controls.Add(Me.rgOrden)
        Me.GroupControl1.Controls.Add(Me.sbEnviarClientes)
        Me.GroupControl1.Controls.Add(Me.pbcPorcentaje)
        Me.GroupControl1.Controls.Add(Me.sbEnviarCorreos)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.teNombre)
        Me.GroupControl1.Controls.Add(Me.LabelControl13)
        Me.GroupControl1.Controls.Add(Me.beCliente)
        Me.GroupControl1.Controls.Add(Me.leGerente)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.deDesde)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.rgTipo)
        Me.GroupControl1.Controls.Add(Me.deHasta)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Size = New System.Drawing.Size(739, 299)
        Me.GroupControl1.TabIndex = 0
        '
        'deHasta
        '
        Me.deHasta.EditValue = Nothing
        Me.deHasta.Location = New System.Drawing.Point(323, 38)
        Me.deHasta.Name = "deHasta"
        Me.deHasta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deHasta.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deHasta.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deHasta.Size = New System.Drawing.Size(100, 20)
        Me.deHasta.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(288, 42)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl2.TabIndex = 14
        Me.LabelControl2.Text = "Hasta:"
        '
        'rgTipo
        '
        Me.rgTipo.EditValue = 1
        Me.rgTipo.Location = New System.Drawing.Point(134, 114)
        Me.rgTipo.Name = "rgTipo"
        Me.rgTipo.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Pendientes"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Recibidos")})
        Me.rgTipo.Size = New System.Drawing.Size(194, 24)
        Me.rgTipo.TabIndex = 23
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(66, 118)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl1.TabIndex = 24
        Me.LabelControl1.Text = "Tipo Informe:"
        '
        'deDesde
        '
        Me.deDesde.EditValue = Nothing
        Me.deDesde.Location = New System.Drawing.Point(134, 38)
        Me.deDesde.Name = "deDesde"
        Me.deDesde.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deDesde.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deDesde.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deDesde.Size = New System.Drawing.Size(100, 20)
        Me.deDesde.TabIndex = 25
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(99, 42)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl3.TabIndex = 26
        Me.LabelControl3.Text = "Desde:"
        '
        'leGerente
        '
        Me.leGerente.EnterMoveNextControl = True
        Me.leGerente.Location = New System.Drawing.Point(135, 62)
        Me.leGerente.Name = "leGerente"
        Me.leGerente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leGerente.Size = New System.Drawing.Size(359, 20)
        Me.leGerente.TabIndex = 30
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(82, 65)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl4.TabIndex = 31
        Me.LabelControl4.Text = "Vendedor:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(240, 88)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.ReadOnly = True
        Me.teNombre.Size = New System.Drawing.Size(254, 20)
        Me.teNombre.TabIndex = 59
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(94, 91)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl13.TabIndex = 61
        Me.LabelControl13.Text = "Cliente:"
        '
        'beCliente
        '
        Me.beCliente.EnterMoveNextControl = True
        Me.beCliente.Location = New System.Drawing.Point(134, 88)
        Me.beCliente.Name = "beCliente"
        Me.beCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCliente.Size = New System.Drawing.Size(100, 20)
        Me.beCliente.TabIndex = 58
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(500, 91)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(132, 13)
        Me.LabelControl5.TabIndex = 62
        Me.LabelControl5.Text = "** En blanco generar todos"
        '
        'sbEnviarCorreos
        '
        Me.sbEnviarCorreos.Location = New System.Drawing.Point(133, 171)
        Me.sbEnviarCorreos.Name = "sbEnviarCorreos"
        Me.sbEnviarCorreos.Size = New System.Drawing.Size(169, 23)
        Me.sbEnviarCorreos.TabIndex = 63
        Me.sbEnviarCorreos.Text = "Enviar Correos Gerente"
        Me.sbEnviarCorreos.Visible = False
        '
        'pbcPorcentaje
        '
        Me.pbcPorcentaje.Location = New System.Drawing.Point(133, 200)
        Me.pbcPorcentaje.Name = "pbcPorcentaje"
        Me.pbcPorcentaje.Size = New System.Drawing.Size(362, 27)
        Me.pbcPorcentaje.TabIndex = 65
        Me.pbcPorcentaje.Visible = False
        '
        'sbEnviarClientes
        '
        Me.sbEnviarClientes.Location = New System.Drawing.Point(308, 171)
        Me.sbEnviarClientes.Name = "sbEnviarClientes"
        Me.sbEnviarClientes.Size = New System.Drawing.Size(187, 23)
        Me.sbEnviarClientes.TabIndex = 66
        Me.sbEnviarClientes.Text = "Enviar Correos Cliente"
        Me.sbEnviarClientes.Visible = False
        '
        'lblOrdenado
        '
        Me.lblOrdenado.Location = New System.Drawing.Point(61, 144)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(71, 13)
        Me.lblOrdenado.TabIndex = 68
        Me.lblOrdenado.Text = "Ordenado por:"
        '
        'rgOrden
        '
        Me.rgOrden.EditValue = 1
        Me.rgOrden.Location = New System.Drawing.Point(135, 140)
        Me.rgOrden.Name = "rgOrden"
        Me.rgOrden.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Vendedor"), New DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Fecha")})
        Me.rgOrden.Size = New System.Drawing.Size(194, 24)
        Me.rgOrden.TabIndex = 67
        '
        'cpc_frmControlComprobanteRetencion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(739, 324)
        Me.Modulo = "Facturación"
        Me.Name = "cpc_frmControlComprobanteRetencion"
        Me.Text = "Informe de Comprobantes de retención"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.deHasta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deHasta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDesde.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leGerente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbcPorcentaje.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgOrden.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents deHasta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgTipo As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents deDesde As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leGerente As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCliente As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbEnviarCorreos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pbcPorcentaje As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents sbEnviarClientes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lblOrdenado As DevExpress.XtraEditors.LabelControl
    Friend WithEvents rgOrden As DevExpress.XtraEditors.RadioGroup

End Class
