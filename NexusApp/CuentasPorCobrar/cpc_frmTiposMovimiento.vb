﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class cpc_frmTiposMovimiento
    Dim entidad As cpc_TiposMov


    Private Sub ban_frmTiposTransaccion_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub

    Private Sub cpc_frmTiposMovimiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.cpc_TiposMovSelectAll
        objCombos.conTiposPartida(releTipoPartida, "")
        objCombos.conTiposPartida(leTipoPartida, "")
        objCombos.Ban_CargoAbono(releCargoAbono)
        objCombos.Ban_CargoAbono(leTipoAplicacion)
    End Sub


    Private Sub ban_frmTiposTransaccion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.cpc_TiposMovSelectAll
        entidad = objTablas.cpc_TiposMovSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipo"))

        CargaPantalla()
        ActivaControles(False)
    End Sub
    Private Sub cban_frmTiposTransaccion_Nuevo_Click() Handles Me.Nuevo
        entidad = New cpc_TiposMov
        entidad.IdTipo = objFunciones.ObtenerUltimoId("cpc_TiposMov", "IdTipo") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub
    Private Sub ban_frmTiposTransaccion_Save_Click() Handles Me.Guardar
        If teNombre.EditValue = "" Or SiEsNulo(leTipoAplicacion.EditValue, 0) = 0 Or SiEsNulo(leTipoPartida.EditValue, "") = "" Or SiEsNulo(BeCtaContable1.beIdCuenta.EditValue, "") = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [ Nombre, TipoAplicacion, Cuenta Contable y Tipo de Partida]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.cpc_TiposMovInsert(entidad)
        Else
            objTablas.cpc_TiposMovUpdate(entidad)
        End If
        gc.DataSource = objTablas.cpc_TiposMovSelectAll
        entidad = objTablas.cpc_TiposMovSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipo"))
        CargaPantalla()
        ActivaControles(False)
        MostrarModoInicial()
    End Sub
    Private Sub ban_frmTiposTransaccion_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el registro seleccionado?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
            Try
                objTablas.cpc_TiposMovDeleteByPK(gv.GetFocusedRowCellValue(gv.Columns(0)))
                gc.DataSource = objTablas.cpc_TiposMovSelectAll
                entidad = objTablas.cpc_TiposMovSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipo"))
                CargaPantalla()
            Catch ex As Exception
                MsgBox("NO SE PUDO ELIMINAR EL REGISTRO:" & Chr(13) & ex.Message(), MsgBoxStyle.Critical, "Error")
            End Try
        End If
    End Sub
    Private Sub ban_frmTiposTransaccion_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub CargaPantalla()
        With entidad
            teId.EditValue = .IdTipo
            teNombre.EditValue = .Nombre
            leTipoAplicacion.EditValue = .IdTipoTransaccion
            leTipoPartida.EditValue = .IdTipoPartida
            BeCtaContable1.beIdCuenta.EditValue = .IdCuentaContable
            Dim entCTA As con_Cuentas = objTablas.con_CuentasSelectByPK(.IdCuentaContable)
            BeCtaContable1.teNombreCuenta.EditValue = entCTA.Nombre
            chkAfectaBancos.EditValue = .AfectaBancos
        End With
        teNombre.Focus()
    End Sub
    Private Sub CargaEntidad()
        With entidad
            .IdTipo = teId.EditValue
            .Nombre = teNombre.EditValue
            .IdTipoTransaccion = leTipoAplicacion.EditValue
            .TipoAplicacion = leTipoAplicacion.EditValue
            .IdTipoPartida = leTipoPartida.EditValue
            .IdCuentaContable = BeCtaContable1.beIdCuenta.EditValue
            .AfectaBancos = chkAfectaBancos.EditValue
        End With
    End Sub

    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entidad = objTablas.cpc_TiposMovSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdTipo"))
        CargaPantalla()
    End Sub

    Private Sub cban_frmTiposTransaccion_Report_Click() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub
    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In Me.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teId.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub
End Class
