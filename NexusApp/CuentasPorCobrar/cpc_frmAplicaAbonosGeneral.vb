﻿Imports NexusELL.TableEntities
Imports NexusBLL

Public Class cpc_frmAplicaAbonosGeneral
    Dim dtDetalle As New DataTable
    Dim bl As New NexusBLL.CuentasPCBLL(g_ConnectionString)
    Dim blFac As New NexusBLL.FacturaBLL(g_ConnectionString)
    Dim fd As New FuncionesBLL(g_ConnectionString)
    Dim AbonoHeader As cpc_Abonos
    Dim AbonoDetalle As List(Of cpc_AbonosDetalle)
    Dim dFechaUltPago As Date, ElUsuarioDioClic As Boolean = False, Saldo As Decimal = 0
    Dim sConcepto As String = "", entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub frmIngresoAbonos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.cpcCobradores(cboEjecutivo)
        objCombos.cpcTiposMov(leTipoAbono, "TipoAplicacion=1")
        objCombos.banCuentasBancarias(leCuentaBanco)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePunto, leSucursal.EditValue, "")
        LimpiaPantalla()
        deFecha.EditValue = Today
        leSucursal.EditValue = piIdSucursalUsuario
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePunto, leSucursal.EditValue)
    End Sub

    Private Sub frmIngresoAbonos_Save_Click() Handles Me.Guardar

        Dim msj2 As String = ""
        gv.UpdateTotalSummary()

        gv.Columns("IdComprobante").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "MontoAbonar") > 0 And (gv.GetRowCellValue(i, "MontoAbonar") > gv.GetRowCellValue(i, "Saldo")) Then
                msj2 = "El monto a abonar es mayor al saldo del comprobante"
                Exit For
            End If
            If gv.GetRowCellValue(i, "MontoAbonar") > 0 And gv.GetRowCellValue(i, "Numero") = "" Then
                msj2 = "Debe especificar el numero de comprobante a abonar"
                Exit For
            End If
            If gv.GetRowCellValue(i, "IdComprobante") = gv.GetRowCellValue(i + 1, "IdComprobante") Then
                msj2 = "El comprobante" & gv.GetRowCellValue(i, "Numero") & ",  está repetido"
                Exit For
            End If
        Next
        gv.Columns("IdComprobante").SortOrder = DevExpress.Data.ColumnSortOrder.None


        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            msj2 = "La fecha del Abono corresponde a un Periodo ya Cerrado"
        End If
        If msj2 <> "" Then
            MsgBox(msj2, MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If
        If MsgBox("Número de recibo a asignar " & txtNroRecibo.EditValue & Chr(13) & _
                  "Está seguro(a) de aplicar éste abono?" & Chr(13) & _
                  "Ya no podrá editar los datos", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If

        sConcepto = "Comprobantes Abonados "
        AbonoDetalle = New List(Of cpc_AbonosDetalle)
        Dim IdCliente As String = ""
        Dim msj As String = ""

        gv.Columns("IdCliente").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "MontoAbonar") > 0 Then  'inserto solo los que aplicaron abono
                If IdCliente <> gv.GetRowCellValue(i, "IdCliente") Then
                    LlenarEntidades(gv.GetRowCellValue(i, "IdCliente"))

                    Dim entTipoAbono As cpc_TiposMov = objTablas.cpc_TiposMovSelectByPK(leTipoAbono.EditValue)
                    Dim entCtaBanco As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(leCuentaBanco.EditValue)
                    Dim endCliente As fac_Clientes = objTablas.fac_ClientesSelectByPK(gv.GetRowCellValue(i, "IdCliente")) ' pendiente de revisar
                  
                    'solo los abonos que afecta bancos, opcion=3, generaran partidas, los demas con el proceso de contabilizar
                    msj = bl.InsertaAbono(AbonoHeader, AbonoDetalle, _
                                           IIf(leTipoAbono.EditValue = 3, True, False) _
                                           , entTipoAbono.IdTipoPartida, _
                                           entTipoAbono.IdCuentaContable, _
                                           8, _
                                           entCtaBanco.IdCuentaBancaria, _
                                           endCliente.IdCuentaContable, _
                                           IIf(leTipoAbono.EditValue = 3, True, False), _
                                           entTipoAbono.IdTipoTransaccion, sConcepto)

                    IdCliente = gv.GetRowCellValue(i, "IdCliente")

                    If msj <> "" Then
                        MsgBox("Error al insertar Abono" + Chr(13) + msj, MsgBoxStyle.Critical, "Nota")
                    End If

                End If

            End If
        Next
        gv.Columns("IdCliente").SortOrder = DevExpress.Data.ColumnSortOrder.None

        If msj = "" Then
            MsgBox("El abono ha sido aplicado con éxito", MsgBoxStyle.Information, "Nota")
            LimpiaPantalla()
        Else
            MsgBox("Error al insertar Abono" + Chr(13) + msj, MsgBoxStyle.Critical, "Nota")
        End If

    End Sub

    Private Sub LlenarEntidades(ByVal IdCliente As String)

        sConcepto = "Comprobantes Abonados "
        AbonoDetalle = New List(Of cpc_AbonosDetalle)

        With AbonoHeader
            .IdComprobante = 0
            .NumeroComprobante = txtNroRecibo.EditValue
            .Fecha = deFecha.EditValue
            .IdTipoMov = leTipoAbono.EditValue
            .IdCuentaBancaria = IIf(leTipoAbono.EditValue = 3, leCuentaBanco.EditValue, 0)
            .IdCobrador = cboEjecutivo.EditValue
            .IdCliente = IdCliente
            .IdSucursal = leSucursal.EditValue
            .Concepto = txtConcepto.EditValue
            .BancoProcedencia = txtBancoProcede.EditValue
            .NumCheque = txtNumCheque.EditValue
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .ModificadoPor = ""
            .FechaHoraModificacion = Nothing
        End With

        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "MontoAbonar") > 0 Then  'inserto solo los que aplicaron abono
                If gv.GetRowCellValue(i, "IdCliente") = IdCliente Then
                    Dim entDetalle As New cpc_AbonosDetalle
                    With entDetalle
                        .IdComprobante = 0
                        .IdDetalle = i + 1
                        .IdComprobVenta = gv.GetRowCellValue(i, "IdComprobante")
                        .MontoAbonado = gv.GetRowCellValue(i, "MontoAbonar")
                        .SaldoComprobante = gv.GetRowCellValue(i, "Saldo")
                        .Concepto = "ABONO COMPROB. " & gv.GetRowCellValue(i, "Numero")
                        .CreadoPor = objMenu.User
                        .FechaHoraCreacion = Now
                        sConcepto = sConcepto & ", # " & gv.GetRowCellValue(i, "Numero")
                    End With
                    AbonoDetalle.Add(entDetalle)
                End If
            End If
        Next
    End Sub


    Private Sub CalculaDiferencia()
        Dim TotalAbono As Decimal = 0.0
        gv.UpdateTotalSummary()

        TotalAbono = Decimal.Round(MontoAbonar.SummaryItem.SummaryValue, 2)
        leSucursal.Properties.ReadOnly = TotalAbono <> 0.0
        lePunto.Properties.ReadOnly = TotalAbono <> 0.0
    End Sub


    Private Sub txtMonto_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtMonto.Validating
        If gv.EditingValue > gv.GetRowCellValue(gv.FocusedRowHandle, "Saldo") Then
            gv.EditingValue = gv.GetRowCellValue(gv.FocusedRowHandle, "Saldo")
        End If
        CalculaDiferencia()
    End Sub
    Private Sub LimpiaPantalla()
        AbonoHeader = New cpc_Abonos
        AbonoDetalle = New List(Of cpc_AbonosDetalle)
        txtNroRecibo.EditValue = fd.ObtenerCorrelativoManual("ABONOS_CPC").ToString.PadLeft(6, "0") '(objFunciones.ObtenerUltimoId("CPC_ABONOS", "IdComprobante") + 1).ToString.PadLeft(6, "0")


        txtConcepto.EditValue = ""
        teDiferencia.EditValue = 0.0
        txtBancoProcede.EditValue = ""
        txtNumCheque.EditValue = ""
        gv.CancelUpdateCurrentRow()
        Dim dt As DataTable = bl.GetSaldosFacturas("", Today, Today, -1)
        gc.DataSource = dt
        CalculaDiferencia()
        txtNroRecibo.Focus()

    End Sub

#Region "Grilla"

    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        gv.DeleteRow(gv.FocusedRowHandle)
        CalculaDiferencia()
    End Sub

    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Nombre", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Serie", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Numero", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "Fecha", Today)
        gv.SetRowCellValue(gv.FocusedRowHandle, "FechaVenc", Today)
        gv.SetRowCellValue(gv.FocusedRowHandle, "TotalComprobante", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Saldo", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdComprobante", 0)
    End Sub
    Private Sub gv_ValidatingEditor(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs) Handles gv.ValidatingEditor
        Select Case gv.FocusedColumn.FieldName
            Case "Numero"
                Dim entCliente As fac_Clientes
                Dim IdCli As Integer = gv.GetFocusedRowCellValue("IdCliente")
                Dim Serie As String = gv.GetFocusedRowCellValue("Serie")
                entCliente = objTablas.fac_ClientesSelectByPK(IdCli)
                Dim IdComprobante As Integer = SiEsNulo(blFac.getIdComprobante(leSucursal.EditValue, lePunto.EditValue, entCliente.IdTipoComprobante, Serie, e.Value), 0)

                If IdComprobante = 0 Then
                    MsgBox("Comprobante no existe", MsgBoxStyle.Critical, "Error")
                    e.Value = ""
                    gv.SetFocusedRowCellValue("IdComprobante", 0)
                Else
                    Dim dt As New DataTable
                    dt = blFac.fac_SaldoComprobante(IdCli, deFecha.EditValue, IdComprobante)
                    gv.SetFocusedRowCellValue("IdComprobante", IdComprobante)

                    If dt.Rows.Count > 0 Then
                        gv.SetFocusedRowCellValue("TotalComprobante", dt.Rows(0).Item("TotalComprobante"))
                        gv.SetFocusedRowCellValue("Saldo", dt.Rows(0).Item("Saldo"))
                    Else
                        gv.SetFocusedRowCellValue("TotalComprobante", 0.0)
                        gv.SetFocusedRowCellValue("Saldo", 0.0)
                    End If
                End If
            Case "Serie"
                Dim entCliente As fac_Clientes
                Dim IdCli As Integer = gv.GetFocusedRowCellValue("IdCliente")
                Dim Serie As String = e.Value
                Dim Numero As String = gv.GetFocusedRowCellValue("Numero")
                entCliente = objTablas.fac_ClientesSelectByPK(IdCli)
                Dim IdComprobante As Integer = SiEsNulo(blFac.getIdComprobante(leSucursal.EditValue, lePunto.EditValue, entCliente.IdTipoComprobante, Serie, Numero), 0)

                If IdComprobante = 0 Then
                    MsgBox("Comprobante no existe", MsgBoxStyle.Critical, "Error")
                    e.Value = ""
                    gv.SetFocusedRowCellValue("IdComprobante", 0)
                Else
                    Dim dt As New DataTable
                    dt = blFac.fac_SaldoComprobante(IdCli, deFecha.EditValue, IdComprobante)
                    gv.SetFocusedRowCellValue("IdComprobante", IdComprobante)

                    If dt.Rows.Count > 0 Then
                        gv.SetFocusedRowCellValue("TotalComprobante", dt.Rows(0).Item("TotalComprobante"))
                        gv.SetFocusedRowCellValue("Saldo", dt.Rows(0).Item("Saldo"))
                    Else
                        gv.SetFocusedRowCellValue("TotalComprobante", 0.0)
                        gv.SetFocusedRowCellValue("Saldo", 0.0)
                    End If
                End If
        End Select
        CalculaDiferencia()
    End Sub
    Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gv.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Tab Then
            'validar columna codigo de producto
            If gv.FocusedColumn.FieldName = "IdCliente" Then
                Dim entCliente As fac_Clientes
                If SiEsNulo(gv.EditingValue, "") = "" Then
                    entCliente = objConsultas.cnsClientes(fac_frmConsultaClientes, SiEsNulo(gv.EditingValue, ""))
                    gv.EditingValue = entCliente.IdCliente
                End If

                entCliente = objTablas.fac_ClientesSelectByPK(gv.EditingValue)
                If entCliente.IdCliente = "" Then
                    MsgBox("Cliente no registrado", MsgBoxStyle.Critical, "Error")
                    gv.SetFocusedRowCellValue("Nombre", "-- CLIENTE NO REGISTRADO --")
                Else
                    gv.SetFocusedRowCellValue("IdCliente", entCliente.IdCliente)
                    gv.SetFocusedRowCellValue("Nombre", entCliente.Nombre)
                    Dim serie As String = blFac.GetObtenerSerieDocumento(lePunto.EditValue, entCliente.IdTipoComprobante)
                    gv.SetFocusedRowCellValue("Serie", serie)
                End If
            End If
        End If
    End Sub


#End Region




End Class
