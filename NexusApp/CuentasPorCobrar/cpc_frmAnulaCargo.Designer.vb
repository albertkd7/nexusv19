﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpc_frmAnulaCargo
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.btEliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.BeCliente1 = New Nexus.beCliente()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.col1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.col7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riIdRecibo = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riIdRecibo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.btEliminar)
        Me.GroupControl1.Controls.Add(Me.BeCliente1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(744, 59)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Datos del cliente"
        '
        'btEliminar
        '
        Me.btEliminar.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.btEliminar.Appearance.Options.UseFont = True
        Me.btEliminar.Location = New System.Drawing.Point(610, 23)
        Me.btEliminar.Name = "btEliminar"
        Me.btEliminar.Size = New System.Drawing.Size(122, 22)
        Me.btEliminar.TabIndex = 73
        Me.btEliminar.Text = "Eliminar"
        '
        'BeCliente1
        '
        Me.BeCliente1.Location = New System.Drawing.Point(12, 25)
        Me.BeCliente1.Name = "BeCliente1"
        Me.BeCliente1.Size = New System.Drawing.Size(600, 20)
        Me.BeCliente1.TabIndex = 0
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 59)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riIdRecibo})
        Me.gc.Size = New System.Drawing.Size(739, 276)
        Me.gc.TabIndex = 8
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col1, Me.col3, Me.col5, Me.col6, Me.col7, Me.GridColumn1})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'col1
        '
        Me.col1.Caption = "Numero Comprobante"
        Me.col1.FieldName = "Numero"
        Me.col1.Name = "col1"
        Me.col1.Visible = True
        Me.col1.VisibleIndex = 1
        Me.col1.Width = 154
        '
        'col3
        '
        Me.col3.Caption = "Fecha Comprobante"
        Me.col3.FieldName = "Fecha"
        Me.col3.Name = "col3"
        Me.col3.Visible = True
        Me.col3.VisibleIndex = 2
        Me.col3.Width = 160
        '
        'col5
        '
        Me.col5.Caption = "Total"
        Me.col5.FieldName = "TotalComprobante"
        Me.col5.Name = "col5"
        Me.col5.Width = 156
        '
        'col6
        '
        Me.col6.Caption = "Saldo"
        Me.col6.FieldName = "Saldo"
        Me.col6.Name = "col6"
        Me.col6.Width = 115
        '
        'col7
        '
        Me.col7.Caption = "Total Cargo"
        Me.col7.FieldName = "TotalComprobante"
        Me.col7.Name = "col7"
        Me.col7.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontoAbonado", "{0:n2}")})
        Me.col7.Visible = True
        Me.col7.VisibleIndex = 3
        Me.col7.Width = 157
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "IdComprobante"
        Me.GridColumn1.FieldName = "IdComprobante"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 94
        '
        'riIdRecibo
        '
        Me.riIdRecibo.AutoHeight = False
        Me.riIdRecibo.Name = "riIdRecibo"
        '
        'cpc_frmAnulaCargo
        '
        Me.ClientSize = New System.Drawing.Size(744, 360)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.GroupControl1)
        Me.Modulo = "Cuentas por Cobrar"
        Me.Name = "cpc_frmAnulaCargo"
        Me.OptionId = "002002"
        Me.Text = "Anula Cargos"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riIdRecibo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents col1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riIdRecibo As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents BeCliente1 As Nexus.beCliente
    Friend WithEvents col6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents col7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btEliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn

End Class
