﻿Imports NexusBLL

Public Class cpc_frmListClientes
    Dim bl As New TableBusiness(g_ConnectionString)

    Private Sub Report1_Click() Handles Me.Reporte

        Dim rpt As New cpc_rptListClientes
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Listado de clientes"
        rpt.DataSource = bl.fac_ClientesSelectAll
        rpt.DataMember = ""
        rpt.ShowPreview()
    End Sub

End Class
