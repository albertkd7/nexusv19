﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.GridControl
Imports System.Math


Public Class cpc_frmEntregaDocumentos
    Dim bl As New FacturaBLL(g_ConnectionString)
    Dim blAdmon As New AdmonBLL(g_ConnectionString)
    Dim blCxC As New CuentasPCBLL(g_ConnectionString)
    Dim Header As cpc_DocumentosCobro
    Dim Detalle As List(Of cpc_DocumentosCobroDetalle)



    Private Sub fac_frmPedidos_Eliminar() Handles Me.Eliminar
        Header = objTablas.cpc_DocumentosCobroSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Try
            If MsgBox("Está seguro(a) de eliminar el registro?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
                Header.ModificadoPor = objMenu.User
                objTablas.cpc_DocumentosCobroUpdate(Header)

                objTablas.cpc_DocumentosCobroDeleteByPK(Header.IdComprobante)
            End If
            teCorrelativo.EditValue = objFunciones.ObtenerUltimoId("CPC_DOCUMENTOSCOBRO", "IdComprobante")
            gc2.DataSource = blCxC.cpc_ConsultaCobros(objMenu.User)
        Catch ex As Exception
            MsgBox("No se pudo eliminar el registro", MsgBoxStyle.Information, "Nota")
        End Try
    End Sub


    Private Sub fac_frmPedidos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(lePunto, 1, "")
        leSucursal.EditValue = piIdSucursal
        objCombos.fac_TiposComprobante(leTipo, "")
        objCombos.fac_TiposComprobante(riteTipoComprobante, "")
        objCombos.fac_TiposComprobante(riteTipoComprobante, "")
        objCombos.adm_TiposComprobante(riteTipoComprobante)
        objCombos.fac_Clientes(riteCliente)
        objCombos.fac_Vendedores(leVendedor)

        ActivarControles(False)
        gc2.DataSource = blCxC.cpc_ConsultaCobros(objMenu.User)
        gv.BestFitColumns()
    End Sub
    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(lePunto, leSucursal.EditValue)
        CargaSerie()
    End Sub
    Private Sub CargaSerie()
        teSerie.EditValue = bl.fac_ObtenerSerieDocumento(leSucursal.EditValue, lePunto.EditValue, leTipo.EditValue)
    End Sub

    Private Sub lePunto_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lePunto.EditValueChanged
        CargaSerie()
    End Sub

    Private Sub fac_frmPedidos_Nuevo() Handles Me.Nuevo

        Header = New cpc_DocumentosCobro
        leSucursal.EditValue = piIdSucursalUsuario
        leSucursal_EditValueChanged("", New EventArgs)
        leTipo.EditValue = 5
        deFecha.EditValue = Today
        leVendedor.EditValue = 1
        gc.DataSource = blCxC.ObtenerDocumentosCobroDetalle(-1)
        gv.CancelUpdateCurrentRow()
        deFecha.EditValue = Today

        ActivarControles(True)
        xtcPedidos.SelectedTabPage = xtpDatos
    End Sub
    Private Sub fac_frmPedidos_Guardar() Handles Me.Guardar

        CargaEntidades()
        Dim msj As String = ""
        If DbMode = DbModeType.insert Then
            msj = blCxC.InsertarDocumentosCobro(Header, Detalle)
            teCorrelativo.EditValue = Header.IdComprobante

            If msj = "" Then
                MsgBox("El pedido fue guardado exitosamente", MsgBoxStyle.Information, "Nexus")
            Else
                MsgBox("SE GENERÓ UN ERROR AL TRATAR DE GUARDAR EL REGISTO" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        Else
            msj = blCxC.ActualizaDocumentosCobro(Header, Detalle)
            If msj = "" Then
                MsgBox("El pedido fue guardado exitosamente", MsgBoxStyle.Information, "Nexus")
            Else
                MsgBox("SE GENERÓ UN ERROR AL TRATAR DE ACTUALIZAR EL REGISTRO" + Chr(13) + msj, MsgBoxStyle.Critical)
                Exit Sub
            End If
        End If

        xtcPedidos.SelectedTabPage = xtpLista
        gc2.DataSource = blCxC.cpc_ConsultaCobros(objMenu.User)
        gc2.Focus()

        MostrarModoInicial()
        teCorrelativo.EditValue = Header.IdComprobante
        ActivarControles(False)

    End Sub

    Private Sub fac_frmPedidos_Edit_Click() Handles Me.Editar
        ActivarControles(True)
    End Sub
    Private Sub fac_frmPedidos_Reporte() Handles Me.Reporte
        Header = objTablas.cpc_DocumentosCobroSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        Dim entVendedores As fac_Vendedores = objTablas.fac_VendedoresSelectByPK(Header.IdVendedor)
        Dim dt As DataTable = blCxC.ObtenerDocumentosCobroDetalle(Header.IdComprobante)
        Dim rpt As New cpc_rptEntregaDocumentos() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlPeriodo.Text = "Documentos Entregados A: " + entVendedores.Nombre + " Fecha: " + Format(Header.Fecha, "dd/MM/yyyy")
        rpt.xrlTitulo.Text = "REPORTE DE DOCUMENTOS ENTREGADOS PARA COBRO"
        rpt.xrlVendedor.Text = entVendedores.Nombre
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.ShowPreviewDialog()
    End Sub

    Private Sub CargaEntidades()
        Header = New cpc_DocumentosCobro
        gv.UpdateTotalSummary()

        With Header
            .IdComprobante = teCorrelativo.EditValue
            .IdSucursal = leSucursal.EditValue
            .IdPunto = lePunto.EditValue
            .Fecha = deFecha.EditValue
            .IdVendedor = leVendedor.EditValue
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .FechaHoraModificacion = Nothing
            .ModificadoPor = ""
        End With


        Detalle = New List(Of cpc_DocumentosCobroDetalle)

        For i = 0 To gv.DataRowCount - 1
            Dim entDetalle As New cpc_DocumentosCobroDetalle
            With entDetalle
                .IdComprobante = 0
                .IdDetalle = i + 1
                .IdComprobVenta = gv.GetRowCellValue(i, "IdComprobVenta")
            End With

            Detalle.Add(entDetalle)
        Next
    End Sub


    Private Sub ActivarControles(ByVal Tipo As Boolean)
        For Each ctrl In gcHeader.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.MemoEdit Then
                CType(ctrl, DevExpress.XtraEditors.MemoEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        For Each ctrl In pcTotales.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.MemoEdit Then
                CType(ctrl, DevExpress.XtraEditors.MemoEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
        Next

        gv.OptionsBehavior.Editable = Tipo
    End Sub


    Private Sub fac_frmPedidos_Revertir() Handles Me.Revertir
        xtcPedidos.SelectedTabPage = xtpLista
        gc2.Focus()
        ActivarControles(False)
    End Sub


    Private Sub gc2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gc2.DoubleClick
        Header = objTablas.cpc_DocumentosCobroSelectByPK(gv2.GetRowCellValue(gv2.FocusedRowHandle, "IdComprobante"))
        CargaPantalla()
        DbMode = DbModeType.update
        teNumero.Focus()
    End Sub
    Private Sub CargaPantalla()
        xtcPedidos.SelectedTabPage = xtpDatos

        With Header
            teCorrelativo.EditValue = .IdComprobante
            leSucursal.EditValue = .IdSucursal
            lePunto.EditValue = .IdPunto
            deFecha.EditValue = .Fecha
            leVendedor.EditValue = .IdVendedor

            gc.DataSource = blCxC.ObtenerDocumentosCobroDetalle(.IdComprobante)
        End With
        leVendedor.Enabled = True


    End Sub


    Private Sub sbObtener_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbObtener.Click
        If deFecha.Properties.ReadOnly Then
            Exit Sub
        End If

        Dim IdComprobante As Integer = bl.getIdComprobante(leSucursal.EditValue, lePunto.EditValue, leTipo.EditValue, teSerie.EditValue, teNumero.EditValue)
        If IdComprobante = 0 Then
            MsgBox("No se ha emitido ningún documento con ese número", 64, "Nota")
            Exit Sub
        End If

        Dim entVentas As fac_Ventas = objTablas.fac_VentasSelectByPK(IdComprobante)
        Dim entFormas As fac_FormasPago = objTablas.fac_FormasPagoSelectByPK(entVentas.IdFormaPago)
        If entFormas.DiasCredito <= 0 Then
            MsgBox("El documento seleccionado no es al crédito", 64, "Nota")
            Exit Sub
        End If

        gv.AddNewRow()
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdComprobVenta", entVentas.IdComprobante)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Numero", entVentas.Numero)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdTipoComprobante", entVentas.IdTipoComprobante)
        gv.SetRowCellValue(gv.FocusedRowHandle, "Fecha", entVentas.Fecha)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdCliente", entVentas.IdCliente)
        gv.SetRowCellValue(gv.FocusedRowHandle, "TotalComprobante", entVentas.TotalComprobante)
        gv.UpdateCurrentRow()
        teNumero.EditValue = ""
    End Sub

    Private Sub leTipo_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leTipo.EditValueChanged
        CargaSerie()
    End Sub

    Private Sub cmdBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBorrar.Click
        If deFecha.Properties.ReadOnly Then
            Exit Sub
        End If
        gv.DeleteSelectedRows()
    End Sub
End Class
