﻿
Imports NexusELL.TableEntities
Imports NexusBLL

Public Class cpc_frmAplicaAbonos
    Dim dtDetalle As New DataTable
    Dim bl As New NexusBLL.CuentasPCBLL(g_ConnectionString)
    Dim blAdmon As New NexusBLL.AdmonBLL(g_ConnectionString)
    Dim fd As New FuncionesBLL(g_ConnectionString)
    Dim AbonoHeader As cpc_Abonos
    Dim AbonoDetalle As List(Of cpc_AbonosDetalle)
    Dim dtParametros As DataTable = blAdmon.ObtieneParametros()
    Dim dFechaUltPago As Date, ElUsuarioDioClic As Boolean = False, Saldo As Decimal = 0.0
    Dim sConcepto As String = "", entUsuario As adm_Usuarios = objTablas.adm_UsuariosSelectByPK(objMenu.User)

    Private Sub frmIngresoAbonos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.cpcCobradores(cboEjecutivo)
        objCombos.cpcTiposMov(leTipoAbono, "TipoAplicacion=1")
        objCombos.banCuentasBancarias(leCuentaBanco)
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "")
        objCombos.fac_PuntosVenta(LePuntoVenta, leSucursal.EditValue)
        InicializaGrid()
        LimpiaPantalla()
        deFecha.EditValue = Today
    End Sub

    Private Sub frmIngresoAbonos_Guardar() Handles Me.Guardar
        ' para que actualice la grilla, por si cambian el valor del abono y no le dan enter
        ' la partida queda descuadrada cuando pasa eso.

        If SiEsNulo(beCliente.EditValue, "") = "" Then
            MsgBox("Debe de especificar el código de cliente al que desea aplicar el abono", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If txtMontoAbonar.EditValue = 0.0 Then
            MsgBox("Debe de especificar el monto que desea abonar", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If leSucursal.EditValue = 0 Or leSucursal.EditValue = Nothing Or leSucursal.Text = "" Then
            MsgBox("Debe de especificar una sucursal", MsgBoxStyle.Critical)
            Exit Sub
        End If

        gv.UpdateTotalSummary()
        If teDiferencia.EditValue <> 0.0 Or Decimal.Round(MontoAbonar.SummaryItem.SummaryValue, 2) = 0.0 Then
            MsgBox("No puede aplicar el abono, hay saldo disponible", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If MontoAbonar.SummaryItem.SummaryValue = 0.0 Or Decimal.Round(MontoAbonar.SummaryItem.SummaryValue, 2) = 0.0 Then
            MsgBox("No puede aplicar el abono, hay saldo disponible", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If Decimal.Round(MontoAbonar.SummaryItem.SummaryValue, 2) <> txtMontoAbonar.EditValue Then
            MsgBox("El monto a abonar no coincide con el abono por Documentos", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If leTipoAbono.EditValue = 3 Then
            If MsgBox("Este abono generará una transacción bancaria" & Chr(13) & "¿Está seguro(a) de continuar?", MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.No Then
                Exit Sub
            End If
        End If
        Dim msj2 As String = ""
        Dim EsOk As Boolean = ValidarFechaCierre(deFecha.EditValue)
        If Not EsOk Then
            msj2 = "La fecha del Abono corresponde a un Periodo ya Cerrado"
        End If
        If msj2 <> "" Then
            MsgBox(msj2, MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If
        If MsgBox("Número de recibo a asignar" & txtNroRecibo.EditValue & Chr(13) & _
                  "¿Está seguro(a) de aplicar éste abono?" & Chr(13) & _
                  "Ya no podrá editar los datos", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        LlenarEntidades()

        Dim entTipoAbono As cpc_TiposMov = objTablas.cpc_TiposMovSelectByPK(leTipoAbono.EditValue)
        Dim entCtaBanco As ban_CuentasBancarias = objTablas.ban_CuentasBancariasSelectByPK(leCuentaBanco.EditValue)
        Dim endCliente As fac_Clientes = objTablas.fac_ClientesSelectByPK(beCliente.EditValue)

        'solo los abonos que afecta bancos, opcion=3, generaran partidas, los demas con el proceso de contabilizar
        'Dim AfectaBancos As Boolean = leTipoAbono.EditValue = 3
        Dim InsertarPartida As Boolean = entTipoAbono.AfectaBancos
        Dim msj As String = bl.InsertaAbono(AbonoHeader, AbonoDetalle, _
                               InsertarPartida, entTipoAbono.IdTipoPartida, _
                               entTipoAbono.IdCuentaContable, _
                               8, entCtaBanco.IdCuentaBancaria, _
                               endCliente.IdCuentaContable, entTipoAbono.AfectaBancos, entTipoAbono.IdTipoTransaccion, sConcepto)
        If msj = "" Then
            If MsgBox(String.Format("El abono ha sido aplicado con éxito{0}Desea imprimirlo?", Chr(13)), MsgBoxStyle.YesNo, "Confirme") = MsgBoxResult.Yes Then
                ImprimirRecibo(AbonoHeader.IdComprobante, AbonoHeader.Fecha)
            End If
            LimpiaPantalla()
        Else
            MsgBox("Error al insertar Abono" + Chr(13) + msj, MsgBoxStyle.Critical, "Nota")
        End If
    End Sub
    Private Sub ImprimirRecibo(ByVal IdComprobante As Integer, ByVal Fecha As DateTime)

        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)
        Dim Template = Application.StartupPath & "\Plantillas\naNexus" & NumFormato & ".repx"
        Dim dt As DataTable = bl.ObtenerAbono(AbonoHeader.IdComprobante, AbonoHeader.Fecha)

        Dim rpt As New cpc_rptAbono

        If Not FileIO.FileSystem.FileExists(Template) Then
        Else
            rpt.LoadLayout(Template)
        End If

        'rpt.DataSource = dts
        'rpt.DataMember = ""

        rpt.DataSource = dt
        rpt.DataMember = ""
        'rpt.xrLogo.Image = ByteToImagen(dtParametros.Rows(0).Item("LogoEmpresa"))
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "COMPROBANTE DE NOTA DE ABONO"
        rpt.xrlPeriodo.Text = "Fecha del Abono: " & FechaToString(AbonoHeader.Fecha, AbonoHeader.Fecha)
        Dim TotalAbono As Decimal = txtMontoAbonar.EditValue
        Dim Decimales = String.Format("{0:c}", TotalAbono)
        Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
        rpt.xrlCantidadLetras.Text = Num2Text(Int(TotalAbono)) & Decimales & " DÓLARES"

        rpt.ShowPreviewDialog()
    End Sub
    Private Sub LlenarEntidades()
        With AbonoHeader
            .IdComprobante = 0
            .NumeroComprobante = txtNroRecibo.EditValue
            .Fecha = deFecha.EditValue
            .IdTipoMov = leTipoAbono.EditValue
            .IdCuentaBancaria = leCuentaBanco.EditValue
            .IdCobrador = cboEjecutivo.EditValue
            .IdCliente = beCliente.EditValue
            .IdSucursal = leSucursal.EditValue
            .IdPuntoVenta = LePuntoVenta.EditValue
            .Concepto = txtConcepto.EditValue
            .BancoProcedencia = txtBancoProcede.EditValue
            .NumCheque = txtNumCheque.EditValue
            .CreadoPor = objMenu.User
            .FechaHoraCreacion = Now
            .ModificadoPor = ""
            .FechaHoraModificacion = Nothing
        End With
        sConcepto = "Comprobantes Abonados "
        AbonoDetalle = New List(Of cpc_AbonosDetalle)
        For i = 0 To gv.DataRowCount - 1
            If gv.GetRowCellValue(i, "MontoAbonar") > 0.0 Then  'inserto solo los que aplicaron abono
                Dim entDetalle As New cpc_AbonosDetalle
                With entDetalle
                    .IdComprobante = AbonoHeader.IdComprobante
                    .IdDetalle = i + 1
                    .IdComprobVenta = gv.GetRowCellValue(i, "IdComprobante")
                    .MontoAbonado = gv.GetRowCellValue(i, "MontoAbonar")
                    .SaldoComprobante = gv.GetRowCellValue(i, "SaldoComprob")
                    .Concepto = "ABONO COMPROB. " & gv.GetRowCellValue(i, "NroComprobante")
                    .CreadoPor = objMenu.User
                    .FechaHoraCreacion = Now
                    sConcepto = sConcepto & ", # " & gv.GetRowCellValue(i, "NroComprobante")
                End With
                AbonoDetalle.Add(entDetalle)
            End If
        Next
    End Sub
    Private Sub beCliente_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCliente.ButtonClick
        beCliente.EditValue = ""
        ElUsuarioDioClic = True
        beCliente_Validated(sender, New System.EventArgs)
    End Sub
    Private Sub beCliente_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCliente.Validated
        If Not ElUsuarioDioClic And beCliente.EditValue = "" Then
            Exit Sub
        End If
        Dim ent As fac_Clientes = objConsultas.cnsClientes(fac_frmConsultaClientes, beCliente.EditValue)
        ElUsuarioDioClic = False

        beCliente.EditValue = ent.IdCliente
        teNombre.EditValue = ent.Nombre
        txtNRC.EditValue = ent.Nrc
        dFechaUltPago = ent.FechaUltPago
        CargarGrilla(ent)
        teNombre.Focus()

    End Sub
    Private Sub CargarGrilla(ByVal entCliente As fac_Clientes)
        InicializaGrid()
        Dim dt As DataTable = bl.GetSaldosFacturas(entCliente.IdCliente, deFecha.EditValue, entCliente.FechaUltPago, leSucursal.EditValue)
        For Each fila As DataRow In dt.Rows
            AgregaFila(fila)
        Next
        gv.UpdateCurrentRow()
    End Sub
    Private Sub CalculaDiferencia()
        gv.UpdateTotalSummary()
        teDiferencia.EditValue = CDec(txtMontoAbonar.EditValue) - Decimal.Round(MontoAbonar.SummaryItem.SummaryValue, 2)
    End Sub
    Private Sub txtMontoAbonar_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMontoAbonar.Validated
        'Inicializa Grid
        beCliente_Validated(sender, e)

        Dim Acum As Decimal = 0.0
        For i = 0 To gv.RowCount - 1
            gv.SetRowCellValue(i, "Abonar", True)
            Saldo = gv.GetRowCellValue(i, "SaldoComprob")
            gv.SetRowCellValue(i, "MontoAbonar", Saldo)
            Acum += Saldo
            If Acum > Val(txtMontoAbonar.EditValue) Then
                gv.SetRowCellValue(i, "MontoAbonar", Saldo - Decimal.Round(Acum - txtMontoAbonar.EditValue, 2, MidpointRounding.AwayFromZero))
                Exit For
            End If
        Next
        CalculaDiferencia()
    End Sub
    Private Sub chkAbonar_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkAbonar.CheckedChanged
        If gv.EditingValue Then
            If gv.GetFocusedRowCellValue("SaldoComprob") > teDiferencia.EditValue Then
                Saldo = teDiferencia.EditValue
            Else
                Saldo = gv.GetFocusedRowCellValue("SaldoComprob")
            End If
            gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", Saldo)
        Else
            gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", 0.0)
        End If
        CalculaDiferencia()
    End Sub
    Private Sub txtMonto_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles riteMonto.Validating
        If gv.EditingValue > gv.GetRowCellValue(gv.FocusedRowHandle, "SaldoComprob") Then
            gv.EditingValue = gv.GetRowCellValue(gv.FocusedRowHandle, "SaldoComprob")
        End If
        CalculaDiferencia()
    End Sub
    Private Sub LimpiaPantalla()
        AbonoHeader = New cpc_Abonos
        AbonoDetalle = New List(Of cpc_AbonosDetalle)
        
        txtNroRecibo.EditValue = fd.ObtenerCorrelativoManual("ABONOS_CPC").ToString.PadLeft(6, "0") '(objFunciones.ObtenerUltimoId("CPC_ABONOS", "IdComprobante") + 1).ToString.PadLeft(6, "0")

        beCliente.EditValue = ""
        teNombre.EditValue = ""
        txtNRC.EditValue = ""
        txtConcepto.EditValue = ""
        teDiferencia.EditValue = 0.0
        txtMontoAbonar.EditValue = 0.0
        txtPjeComis.EditValue = 0.0
        txtRecMora.EditValue = 0.0
        txtBancoProcede.EditValue = ""
        txtNumCheque.EditValue = ""
        gv.CancelUpdateCurrentRow()
        gc.DataSource = Nothing

        txtNroRecibo.Focus()

    End Sub
#Region "Grilla"
    Private Sub InicializaGrid()
        dtDetalle = New DataTable
        dtDetalle.Columns.Add(New DataColumn("Abonar", GetType(Boolean)))
        dtDetalle.Columns.Add(New DataColumn("NroComprobante", GetType(String)))
        dtDetalle.Columns.Add(New DataColumn("FechaComp", GetType(Date)))
        dtDetalle.Columns.Add(New DataColumn("FechaVencto", GetType(Date)))
        dtDetalle.Columns.Add(New DataColumn("TotalComprob", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("SaldoComprob", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("MontoAbonar", GetType(Decimal)))
        dtDetalle.Columns.Add(New DataColumn("IdComprobante", GetType(Integer)))
        gc.DataSource = dtDetalle
    End Sub
    Private Sub gv_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.GotFocus
        SendKeys.SendWait("{enter}")
    End Sub

    Private Sub leSucursal_EditValueChanged(sender As Object, e As EventArgs) Handles leSucursal.EditValueChanged
        objCombos.fac_PuntosVenta(LePuntoVenta, leSucursal.EditValue)
    End Sub

    Private Sub gv_InitNewRow(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles gv.InitNewRow
        gv.SetRowCellValue(gv.FocusedRowHandle, "Abonar", False)
        gv.SetRowCellValue(gv.FocusedRowHandle, "NroComprobante", "")
        gv.SetRowCellValue(gv.FocusedRowHandle, "TotalComprob", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "SaldoComprob", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdComprobante", 0)
    End Sub
    Private Sub AgregaFila(ByVal fila As DataRow)
        gv.AddNewRow()
        gv.SetRowCellValue(gv.FocusedRowHandle, "Abonar", False)
        gv.SetRowCellValue(gv.FocusedRowHandle, "NroComprobante", fila.Item("Numero"))
        gv.SetRowCellValue(gv.FocusedRowHandle, "FechaComp", fila.Item("Fecha"))
        gv.SetRowCellValue(gv.FocusedRowHandle, "FechaVencto", fila.Item("FechaVenc"))
        gv.SetRowCellValue(gv.FocusedRowHandle, "TotalComprob", fila.Item("TotalComprobante"))
        gv.SetRowCellValue(gv.FocusedRowHandle, "SaldoComprob", fila.Item("Saldo"))
        gv.SetRowCellValue(gv.FocusedRowHandle, "MontoAbonar", 0.0)
        gv.SetRowCellValue(gv.FocusedRowHandle, "IdComprobante", fila.Item("IdComprobante"))
    End Sub

#End Region


    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs)
        For i = 0 To gv.DataRowCount - 1
            MsgBox(gv.GetRowCellValue(i, "MontoAbonar").ToString(), MsgBoxStyle.Information, "Nota")
        Next
    End Sub
End Class
