﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpc_frmAplicaAbonos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pcHeader = New DevExpress.XtraEditors.PanelControl()
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNumCheque = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.txtBancoProcede = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNroRecibo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNRC = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.cboEjecutivo = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.leTipoAbono = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.txtRecMora = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.txtPjeComis = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txtMontoAbonar = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.txtConcepto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.leCuentaBanco = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.beCliente = New DevExpress.XtraEditors.ButtonEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.teDiferencia = New DevExpress.XtraEditors.TextEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Abonar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkAbonar = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.NroComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.FechaComp = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.FechaVencto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TotalComprob = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SaldoComprob = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.MontoAbonar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riteMonto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.IdComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.LePuntoVenta = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcHeader.SuspendLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBancoProcede.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNroRecibo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEjecutivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipoAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtRecMora.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPjeComis.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMontoAbonar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leCuentaBanco.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teDiferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAbonar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteMonto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pcHeader
        '
        Me.pcHeader.Controls.Add(Me.LePuntoVenta)
        Me.pcHeader.Controls.Add(Me.LabelControl17)
        Me.pcHeader.Controls.Add(Me.leSucursal)
        Me.pcHeader.Controls.Add(Me.LabelControl16)
        Me.pcHeader.Controls.Add(Me.txtNumCheque)
        Me.pcHeader.Controls.Add(Me.LabelControl15)
        Me.pcHeader.Controls.Add(Me.txtBancoProcede)
        Me.pcHeader.Controls.Add(Me.LabelControl14)
        Me.pcHeader.Controls.Add(Me.LabelControl1)
        Me.pcHeader.Controls.Add(Me.txtNroRecibo)
        Me.pcHeader.Controls.Add(Me.LabelControl2)
        Me.pcHeader.Controls.Add(Me.teNombre)
        Me.pcHeader.Controls.Add(Me.LabelControl3)
        Me.pcHeader.Controls.Add(Me.txtNRC)
        Me.pcHeader.Controls.Add(Me.LabelControl4)
        Me.pcHeader.Controls.Add(Me.cboEjecutivo)
        Me.pcHeader.Controls.Add(Me.LabelControl5)
        Me.pcHeader.Controls.Add(Me.leTipoAbono)
        Me.pcHeader.Controls.Add(Me.LabelControl6)
        Me.pcHeader.Controls.Add(Me.txtRecMora)
        Me.pcHeader.Controls.Add(Me.LabelControl7)
        Me.pcHeader.Controls.Add(Me.txtPjeComis)
        Me.pcHeader.Controls.Add(Me.LabelControl8)
        Me.pcHeader.Controls.Add(Me.txtMontoAbonar)
        Me.pcHeader.Controls.Add(Me.LabelControl9)
        Me.pcHeader.Controls.Add(Me.txtConcepto)
        Me.pcHeader.Controls.Add(Me.LabelControl11)
        Me.pcHeader.Controls.Add(Me.leCuentaBanco)
        Me.pcHeader.Controls.Add(Me.LabelControl12)
        Me.pcHeader.Controls.Add(Me.deFecha)
        Me.pcHeader.Controls.Add(Me.LabelControl13)
        Me.pcHeader.Controls.Add(Me.beCliente)
        Me.pcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pcHeader.Location = New System.Drawing.Point(0, 0)
        Me.pcHeader.Name = "pcHeader"
        Me.pcHeader.Size = New System.Drawing.Size(842, 157)
        Me.pcHeader.TabIndex = 1
        '
        'leSucursal
        '
        Me.leSucursal.EnterMoveNextControl = True
        Me.leSucursal.Location = New System.Drawing.Point(519, 51)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(183, 20)
        Me.leSucursal.TabIndex = 2
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(472, 54)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl16.TabIndex = 59
        Me.LabelControl16.Text = "Sucursal:"
        '
        'txtNumCheque
        '
        Me.txtNumCheque.EnterMoveNextControl = True
        Me.txtNumCheque.Location = New System.Drawing.Point(519, 93)
        Me.txtNumCheque.Name = "txtNumCheque"
        Me.txtNumCheque.Size = New System.Drawing.Size(135, 20)
        Me.txtNumCheque.TabIndex = 7
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(455, 97)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl15.TabIndex = 56
        Me.LabelControl15.Text = "No. Cheque:"
        '
        'txtBancoProcede
        '
        Me.txtBancoProcede.EnterMoveNextControl = True
        Me.txtBancoProcede.Location = New System.Drawing.Point(101, 93)
        Me.txtBancoProcede.Name = "txtBancoProcede"
        Me.txtBancoProcede.Size = New System.Drawing.Size(321, 20)
        Me.txtBancoProcede.TabIndex = 6
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(5, 96)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(94, 13)
        Me.LabelControl14.TabIndex = 54
        Me.LabelControl14.Text = "Banco Procedencia:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(27, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl1.TabIndex = 27
        Me.LabelControl1.Text = "No. de Recibo:"
        '
        'txtNroRecibo
        '
        Me.txtNroRecibo.EnterMoveNextControl = True
        Me.txtNroRecibo.Location = New System.Drawing.Point(101, 9)
        Me.txtNroRecibo.Name = "txtNroRecibo"
        Me.txtNroRecibo.Size = New System.Drawing.Size(156, 20)
        Me.txtNroRecibo.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(290, 33)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 30
        Me.LabelControl2.Text = "Nombre:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(333, 30)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.ReadOnly = True
        Me.teNombre.Size = New System.Drawing.Size(321, 20)
        Me.teNombre.TabIndex = 4
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(491, 7)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl3.TabIndex = 33
        Me.LabelControl3.Text = "NRC:"
        '
        'txtNRC
        '
        Me.txtNRC.EnterMoveNextControl = True
        Me.txtNRC.Location = New System.Drawing.Point(519, 4)
        Me.txtNRC.Name = "txtNRC"
        Me.txtNRC.Properties.ReadOnly = True
        Me.txtNRC.Size = New System.Drawing.Size(135, 20)
        Me.txtNRC.TabIndex = 6
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(50, 54)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl4.TabIndex = 36
        Me.LabelControl4.Text = "Ejecutivo:"
        '
        'cboEjecutivo
        '
        Me.cboEjecutivo.EnterMoveNextControl = True
        Me.cboEjecutivo.Location = New System.Drawing.Point(101, 51)
        Me.cboEjecutivo.Name = "cboEjecutivo"
        Me.cboEjecutivo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cboEjecutivo.Size = New System.Drawing.Size(321, 20)
        Me.cboEjecutivo.TabIndex = 4
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(443, 118)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl5.TabIndex = 37
        Me.LabelControl5.Text = "Tipo de Abono:"
        '
        'leTipoAbono
        '
        Me.leTipoAbono.EnterMoveNextControl = True
        Me.leTipoAbono.Location = New System.Drawing.Point(519, 114)
        Me.leTipoAbono.Name = "leTipoAbono"
        Me.leTipoAbono.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipoAbono.Size = New System.Drawing.Size(135, 20)
        Me.leTipoAbono.TabIndex = 10
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(45, 117)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl6.TabIndex = 39
        Me.LabelControl6.Text = "Rec. Mora:"
        '
        'txtRecMora
        '
        Me.txtRecMora.EditValue = "0"
        Me.txtRecMora.EnterMoveNextControl = True
        Me.txtRecMora.Location = New System.Drawing.Point(101, 114)
        Me.txtRecMora.Name = "txtRecMora"
        Me.txtRecMora.Properties.Appearance.Options.UseTextOptions = True
        Me.txtRecMora.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtRecMora.Properties.Mask.EditMask = "n2"
        Me.txtRecMora.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtRecMora.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtRecMora.Size = New System.Drawing.Size(157, 20)
        Me.txtRecMora.TabIndex = 8
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(271, 118)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl7.TabIndex = 42
        Me.LabelControl7.Text = "% Comisión:"
        '
        'txtPjeComis
        '
        Me.txtPjeComis.EditValue = "0"
        Me.txtPjeComis.EnterMoveNextControl = True
        Me.txtPjeComis.Location = New System.Drawing.Point(334, 114)
        Me.txtPjeComis.Name = "txtPjeComis"
        Me.txtPjeComis.Properties.Appearance.Options.UseTextOptions = True
        Me.txtPjeComis.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtPjeComis.Properties.Mask.EditMask = "n2"
        Me.txtPjeComis.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtPjeComis.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtPjeComis.Size = New System.Drawing.Size(88, 20)
        Me.txtPjeComis.TabIndex = 9
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(17, 138)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(81, 13)
        Me.LabelControl8.TabIndex = 44
        Me.LabelControl8.Text = "Monto a Abonar:"
        '
        'txtMontoAbonar
        '
        Me.txtMontoAbonar.EditValue = New Decimal(New Integer() {0, 0, 0, 65536})
        Me.txtMontoAbonar.EnterMoveNextControl = True
        Me.txtMontoAbonar.Location = New System.Drawing.Point(101, 135)
        Me.txtMontoAbonar.Name = "txtMontoAbonar"
        Me.txtMontoAbonar.Properties.Appearance.Options.UseTextOptions = True
        Me.txtMontoAbonar.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtMontoAbonar.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtMontoAbonar.Properties.Mask.EditMask = "n2"
        Me.txtMontoAbonar.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtMontoAbonar.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtMontoAbonar.Size = New System.Drawing.Size(157, 20)
        Me.txtMontoAbonar.TabIndex = 11
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(281, 138)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl9.TabIndex = 46
        Me.LabelControl9.Text = "Concepto:"
        '
        'txtConcepto
        '
        Me.txtConcepto.EnterMoveNextControl = True
        Me.txtConcepto.Location = New System.Drawing.Point(334, 135)
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.Size = New System.Drawing.Size(320, 20)
        Me.txtConcepto.TabIndex = 12
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(17, 76)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl11.TabIndex = 49
        Me.LabelControl11.Text = "Banco Depositar:"
        '
        'leCuentaBanco
        '
        Me.leCuentaBanco.EnterMoveNextControl = True
        Me.leCuentaBanco.Location = New System.Drawing.Point(101, 72)
        Me.leCuentaBanco.Name = "leCuentaBanco"
        Me.leCuentaBanco.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leCuentaBanco.Size = New System.Drawing.Size(321, 20)
        Me.leCuentaBanco.TabIndex = 5
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(298, 11)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl12.TabIndex = 52
        Me.LabelControl12.Text = "Fecha:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(333, 9)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(86, 20)
        Me.deFecha.TabIndex = 1
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(61, 33)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl13.TabIndex = 53
        Me.LabelControl13.Text = "Cliente:"
        '
        'beCliente
        '
        Me.beCliente.EnterMoveNextControl = True
        Me.beCliente.Location = New System.Drawing.Point(101, 30)
        Me.beCliente.Name = "beCliente"
        Me.beCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCliente.Size = New System.Drawing.Size(156, 20)
        Me.beCliente.TabIndex = 3
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(628, 10)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl10.TabIndex = 47
        Me.LabelControl10.Text = "Diferencia:"
        '
        'teDiferencia
        '
        Me.teDiferencia.EditValue = New Decimal(New Integer() {0, 0, 0, 65536})
        Me.teDiferencia.Location = New System.Drawing.Point(686, 6)
        Me.teDiferencia.Name = "teDiferencia"
        Me.teDiferencia.Properties.Appearance.Options.UseTextOptions = True
        Me.teDiferencia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teDiferencia.Properties.Mask.EditMask = "n2"
        Me.teDiferencia.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teDiferencia.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teDiferencia.Properties.ReadOnly = True
        Me.teDiferencia.Size = New System.Drawing.Size(131, 20)
        Me.teDiferencia.TabIndex = 48
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.teDiferencia)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl1.Location = New System.Drawing.Point(0, 401)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(842, 34)
        Me.PanelControl1.TabIndex = 19
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 157)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1, Me.chkAbonar, Me.riteMonto})
        Me.gc.Size = New System.Drawing.Size(817, 244)
        Me.gc.TabIndex = 2
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Abonar, Me.NroComprobante, Me.FechaComp, Me.FechaVencto, Me.TotalComprob, Me.SaldoComprob, Me.MontoAbonar, Me.IdComprobante})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'Abonar
        '
        Me.Abonar.Caption = "Abonar?"
        Me.Abonar.ColumnEdit = Me.chkAbonar
        Me.Abonar.FieldName = "Abonar"
        Me.Abonar.Name = "Abonar"
        Me.Abonar.Visible = True
        Me.Abonar.VisibleIndex = 0
        Me.Abonar.Width = 60
        '
        'chkAbonar
        '
        Me.chkAbonar.AutoHeight = False
        Me.chkAbonar.Name = "chkAbonar"
        '
        'NroComprobante
        '
        Me.NroComprobante.Caption = "No. Comprobante"
        Me.NroComprobante.FieldName = "NroComprobante"
        Me.NroComprobante.Name = "NroComprobante"
        Me.NroComprobante.OptionsColumn.AllowEdit = False
        Me.NroComprobante.OptionsColumn.AllowFocus = False
        Me.NroComprobante.Visible = True
        Me.NroComprobante.VisibleIndex = 1
        Me.NroComprobante.Width = 119
        '
        'FechaComp
        '
        Me.FechaComp.Caption = "Fecha Comprob."
        Me.FechaComp.FieldName = "FechaComp"
        Me.FechaComp.Name = "FechaComp"
        Me.FechaComp.OptionsColumn.AllowEdit = False
        Me.FechaComp.OptionsColumn.AllowFocus = False
        Me.FechaComp.Visible = True
        Me.FechaComp.VisibleIndex = 2
        Me.FechaComp.Width = 120
        '
        'FechaVencto
        '
        Me.FechaVencto.Caption = "Vencimiento"
        Me.FechaVencto.FieldName = "FechaVencto"
        Me.FechaVencto.Name = "FechaVencto"
        Me.FechaVencto.OptionsColumn.AllowEdit = False
        Me.FechaVencto.OptionsColumn.AllowFocus = False
        Me.FechaVencto.Visible = True
        Me.FechaVencto.VisibleIndex = 3
        Me.FechaVencto.Width = 120
        '
        'TotalComprob
        '
        Me.TotalComprob.Caption = "Total"
        Me.TotalComprob.FieldName = "TotalComprob"
        Me.TotalComprob.Name = "TotalComprob"
        Me.TotalComprob.OptionsColumn.AllowEdit = False
        Me.TotalComprob.OptionsColumn.AllowFocus = False
        Me.TotalComprob.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalComprob", "{0:n2}")})
        Me.TotalComprob.Visible = True
        Me.TotalComprob.VisibleIndex = 4
        Me.TotalComprob.Width = 120
        '
        'SaldoComprob
        '
        Me.SaldoComprob.Caption = "Saldo"
        Me.SaldoComprob.FieldName = "SaldoComprob"
        Me.SaldoComprob.Name = "SaldoComprob"
        Me.SaldoComprob.OptionsColumn.AllowEdit = False
        Me.SaldoComprob.OptionsColumn.AllowFocus = False
        Me.SaldoComprob.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SaldoComprob", "{0:n2}")})
        Me.SaldoComprob.Visible = True
        Me.SaldoComprob.VisibleIndex = 5
        Me.SaldoComprob.Width = 120
        '
        'MontoAbonar
        '
        Me.MontoAbonar.Caption = "Monto Abonar"
        Me.MontoAbonar.ColumnEdit = Me.riteMonto
        Me.MontoAbonar.FieldName = "MontoAbonar"
        Me.MontoAbonar.Name = "MontoAbonar"
        Me.MontoAbonar.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontoAbonar", "{0:n2}")})
        Me.MontoAbonar.Visible = True
        Me.MontoAbonar.VisibleIndex = 6
        Me.MontoAbonar.Width = 137
        '
        'riteMonto
        '
        Me.riteMonto.AutoHeight = False
        Me.riteMonto.Mask.EditMask = "n2"
        Me.riteMonto.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteMonto.Mask.UseMaskAsDisplayFormat = True
        Me.riteMonto.Name = "riteMonto"
        '
        'IdComprobante
        '
        Me.IdComprobante.Caption = "Id"
        Me.IdComprobante.FieldName = "IdComprobante"
        Me.IdComprobante.Name = "IdComprobante"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'LePuntoVenta
        '
        Me.LePuntoVenta.EnterMoveNextControl = True
        Me.LePuntoVenta.Location = New System.Drawing.Point(519, 72)
        Me.LePuntoVenta.Name = "LePuntoVenta"
        Me.LePuntoVenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LePuntoVenta.Size = New System.Drawing.Size(183, 20)
        Me.LePuntoVenta.TabIndex = 60
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(436, 75)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl17.TabIndex = 61
        Me.LabelControl17.Text = "Punto de Venta:"
        '
        'cpc_frmAplicaAbonos
        '
        Me.ClientSize = New System.Drawing.Size(842, 460)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.pcHeader)
        Me.Modulo = "Cuentas Por Cobrar"
        Me.Name = "cpc_frmAplicaAbonos"
        Me.OptionId = "002001"
        Me.Text = "Aplicación de abonos"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.pcHeader, 0)
        Me.Controls.SetChildIndex(Me.PanelControl1, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.pcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcHeader.ResumeLayout(False)
        Me.pcHeader.PerformLayout()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBancoProcede.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNroRecibo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEjecutivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipoAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtRecMora.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPjeComis.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMontoAbonar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leCuentaBanco.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teDiferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAbonar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteMonto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LePuntoVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pcHeader As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNroRecibo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cboEjecutivo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leTipoAbono As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtRecMora As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtPjeComis As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtMontoAbonar As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leCuentaBanco As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCliente As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teDiferencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Abonar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents chkAbonar As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents NroComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents FechaComp As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents FechaVencto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TotalComprob As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SaldoComprob As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents MontoAbonar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riteMonto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents IdComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents txtNumCheque As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtBancoProcede As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LePuntoVenta As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
End Class
