﻿
Imports NexusBLL
Public Class cpc_frmListAbonos
    Dim bl As New CuentasPCBLL(g_ConnectionString)

    Private Sub cpc_frmListAbonos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCombos.fac_Rutas(leRuta, "-- TODAS LAS RUTAS --")
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        deDesde.DateTime = Today
        deHasta.DateTime = Today
        objCombos.fac_Vendedores(leVendedor, "--TODOS LOS VENDEDORES--")
    End Sub

    Private Sub cpc_frmListAbonos_Report_Click() Handles Me.Reporte

        If ceTipo.EditValue = 0 Then
            Dim dt = bl.getAbonosCliente(deDesde.EditValue, deHasta.EditValue, BeCliente1.beCodigo.EditValue, leRuta.EditValue, leSucursal.EditValue, leVendedor.EditValue)
            Dim rpt As New cpc_rptListAbonos() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = "Informe de abonos recibidos de clientes | SUCURSAL: " + leSucursal.Text
            rpt.xrlPeriodo.Text = (FechaToString(deDesde.EditValue, deHasta.EditValue)).ToUpper
            rpt.ShowPreviewDialog()
        Else
            Dim dt = bl.getAbonosClienteConsolidado(deDesde.EditValue, deHasta.EditValue, BeCliente1.beCodigo.EditValue, leRuta.EditValue, leSucursal.EditValue, leVendedor.EditValue)
            Dim rpt As New cpc_rptListAbonosConsolidados() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = "Informe de abonos consolidados recibidos de clientes | SUCURSAL: " + leSucursal.Text
            rpt.xrlPeriodo.Text = (FechaToString(deDesde.EditValue, deHasta.EditValue)).ToUpper
            rpt.ShowPreviewDialog()
        End If
    End Sub
    
End Class
