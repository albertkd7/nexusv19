﻿Imports NexusBLL

Public Class cpc_frmAnalisisAntiguedad
    Dim bl As New CuentasPCBLL(g_ConnectionString)
    
    Private Sub cpc_frmAnalisisAntiguedad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
        objCombos.fac_Vendedores(leVendedor, " TODOS LOS VENDEDORES ")
    End Sub

    Private Sub cpc_frmAnalisisAntiguedad_Reporte() Handles Me.Reporte
        Dim dt As New DataTable
        If ceTipo.EditValue Then
            dt = bl.getAnalisisDetalle(deHasta.EditValue, leSucursal.EditValue, leVendedor.EditValue)
            Dim rpt As New cpc_rptAnalisisDetalle() With {.DataSource = dt, .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.xrlTitulo.Text = teTitulo.EditValue
            rpt.XrLVendedor.Text = leVendedor.Text
            rpt.xrlPeriodo.Text = (FechaToString(deHasta.EditValue, deHasta.EditValue)).ToUpper
            rpt.ShowPreviewDialog()
        Else
            Dim rpt As New cpc_rptAnalisisAntiguedad() With {.DataSource = bl.getAnalisis(deHasta.EditValue, leSucursal.EditValue, leVendedor.EditValue), .DataMember = ""}
            rpt.xrlEmpresa.Text = gsNombre_Empresa
            rpt.XrLVendedor.Text = leVendedor.Text
            rpt.xrlTitulo.Text = teTitulo.EditValue
            rpt.xrlPeriodo.Text = (FechaToString(deHasta.EditValue, deHasta.EditValue)).ToUpper
            rpt.ShowPreviewDialog()
        End If
    End Sub

    Private Sub leVendedor_EditValueChanged(sender As Object, e As EventArgs) Handles leVendedor.EditValueChanged
        teTitulo.EditValue = "ANÁLISIS DE ANTIGÜEDAD DE SALDOS POR COBRAR DE " & leVendedor.Text
    End Sub
    Private Function ObtieneNombreArchivo() As String
        Dim NombreArchivo As String
        If ceTipo.Checked = True Then
            NombreArchivo = "AnalisisCPC_Detalle " & Today.Year.ToString.PadLeft(4, "0") & Today.Month.ToString.PadLeft(2, "0") & Today.Day.ToString.PadLeft(2, "0")
        Else
            NombreArchivo = "AnalisisCPC " & Today.Year.ToString.PadLeft(4, "0") & Today.Month.ToString.PadLeft(2, "0") & Today.Day.ToString.PadLeft(2, "0")
        End If
        NombreArchivo = InputBox("Nombre del archivo", "Defina el nombre del archivo", NombreArchivo)
        Dim myFolderBrowserDialog As New FolderBrowserDialog
        With myFolderBrowserDialog
            .RootFolder = Environment.SpecialFolder.Desktop
            .SelectedPath = "c:\"
            .Description = "Seleccione la carpeta destino"
            If .ShowDialog = DialogResult.OK Then
                NombreArchivo = .SelectedPath & "\" & NombreArchivo
            End If
        End With
        Return NombreArchivo
    End Function

    Private Sub sbObtener_Click(sender As Object, e As EventArgs) Handles sbObtener.Click
        If ceTipo.Checked = True Then
            GcDetalle.DataSource = bl.getAnalisisDetalleExcel(deHasta.EditValue, leSucursal.EditValue, leVendedor.EditValue)
            Dim NombreArchivo As String = ""
            NombreArchivo = ObtieneNombreArchivo() & ".xls"
            GcDetalle.ExportToXls(NombreArchivo)
            MsgBox("El Documento ha sido exportado con Exito a Formato Excel")
        Else
            gc.DataSource = bl.getAnalisisExcel(deHasta.EditValue, leSucursal.EditValue, leVendedor.EditValue)
            Dim NombreArchivo As String = ""
            NombreArchivo = ObtieneNombreArchivo() & ".xls"
            gc.ExportToXls(NombreArchivo)
            MsgBox("El Documento ha sido exportado con Exito a Formato Excel")
        End If

    End Sub
End Class
