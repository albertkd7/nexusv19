﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpc_frmEntregaDocumentos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.xtcPedidos = New DevExpress.XtraTab.XtraTabControl
        Me.xtpLista = New DevExpress.XtraTab.XtraTabPage
        Me.gc2 = New DevExpress.XtraGrid.GridControl
        Me.gv2 = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
        Me.leSucursalDetalle = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.xtpDatos = New DevExpress.XtraTab.XtraTabPage
        Me.pcTotales = New DevExpress.XtraEditors.PanelControl
        Me.gc = New DevExpress.XtraGrid.GridControl
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView
        Me.gcIdComprobante = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcTipoComprobante = New DevExpress.XtraGrid.Columns.GridColumn
        Me.riteTipoComprobante = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.gcNumero = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn
        Me.gcCliente = New DevExpress.XtraGrid.Columns.GridColumn
        Me.riteCliente = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Me.gcTotalComprobante = New DevExpress.XtraGrid.Columns.GridColumn
        Me.riCantidad = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.riPorcDescto = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.riPrecioUnitario = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.riCodProd = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.riteIdPrecio = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
        Me.cmdBorrar = New DevExpress.XtraEditors.SimpleButton
        Me.cmdDown = New DevExpress.XtraEditors.SimpleButton
        Me.cmdUp = New DevExpress.XtraEditors.SimpleButton
        Me.gcHeader = New DevExpress.XtraEditors.GroupControl
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl
        Me.deFecha = New DevExpress.XtraEditors.DateEdit
        Me.sbObtener = New DevExpress.XtraEditors.SimpleButton
        Me.teNumero = New DevExpress.XtraEditors.TextEdit
        Me.teSerie = New DevExpress.XtraEditors.TextEdit
        Me.leTipo = New DevExpress.XtraEditors.LookUpEdit
        Me.lePunto = New DevExpress.XtraEditors.LookUpEdit
        Me.leSucursal = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
        Me.leVendedor = New DevExpress.XtraEditors.LookUpEdit
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl
        CType(Me.xtcPedidos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtcPedidos.SuspendLayout()
        Me.xtpLista.SuspendLayout()
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.xtpDatos.SuspendLayout()
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteTipoComprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riPorcDescto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riPrecioUnitario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riCodProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riteIdPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcHeader.SuspendLayout()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leTipo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'xtcPedidos
        '
        Me.xtcPedidos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.xtcPedidos.Location = New System.Drawing.Point(0, 0)
        Me.xtcPedidos.Name = "xtcPedidos"
        Me.xtcPedidos.SelectedTabPage = Me.xtpLista
        Me.xtcPedidos.Size = New System.Drawing.Size(807, 508)
        Me.xtcPedidos.TabIndex = 6
        Me.xtcPedidos.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtpLista, Me.xtpDatos})
        '
        'xtpLista
        '
        Me.xtpLista.Controls.Add(Me.gc2)
        Me.xtpLista.Name = "xtpLista"
        Me.xtpLista.Size = New System.Drawing.Size(801, 482)
        Me.xtpLista.Text = "Consulta de Entregas"
        '
        'gc2
        '
        Me.gc2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gc2.Location = New System.Drawing.Point(0, 0)
        Me.gc2.MainView = Me.gv2
        Me.gc2.Name = "gc2"
        Me.gc2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemDateEdit2, Me.leSucursalDetalle})
        Me.gc2.Size = New System.Drawing.Size(801, 482)
        Me.gc2.TabIndex = 5
        Me.gc2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv2})
        '
        'gv2
        '
        Me.gv2.GridControl = Me.gc2
        Me.gv2.Name = "gv2"
        Me.gv2.OptionsBehavior.Editable = False
        Me.gv2.OptionsView.ShowAutoFilterRow = True
        Me.gv2.OptionsView.ShowGroupPanel = False
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Mask.EditMask = " dd/MM/yyyy hh:mm:ss"
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        Me.RepositoryItemDateEdit2.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        '
        'leSucursalDetalle
        '
        Me.leSucursalDetalle.AutoHeight = False
        Me.leSucursalDetalle.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursalDetalle.Name = "leSucursalDetalle"
        '
        'xtpDatos
        '
        Me.xtpDatos.Controls.Add(Me.pcTotales)
        Me.xtpDatos.Controls.Add(Me.gc)
        Me.xtpDatos.Controls.Add(Me.PanelControl1)
        Me.xtpDatos.Controls.Add(Me.gcHeader)
        Me.xtpDatos.Name = "xtpDatos"
        Me.xtpDatos.Size = New System.Drawing.Size(801, 482)
        Me.xtpDatos.Text = "Entregas"
        '
        'pcTotales
        '
        Me.pcTotales.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pcTotales.Location = New System.Drawing.Point(0, 393)
        Me.pcTotales.Name = "pcTotales"
        Me.pcTotales.Size = New System.Drawing.Size(763, 89)
        Me.pcTotales.TabIndex = 8
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Top
        Me.gc.Location = New System.Drawing.Point(0, 161)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riCantidad, Me.riPorcDescto, Me.riPrecioUnitario, Me.riCodProd, Me.riteIdPrecio, Me.riteTipoComprobante, Me.riteCliente})
        Me.gc.Size = New System.Drawing.Size(763, 232)
        Me.gc.TabIndex = 6
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcIdComprobante, Me.gcTipoComprobante, Me.gcNumero, Me.gcFecha, Me.gcCliente, Me.gcTotalComprobante})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsNavigation.EnterMoveNextColumn = True
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcIdComprobante
        '
        Me.gcIdComprobante.Caption = "Correlativo"
        Me.gcIdComprobante.FieldName = "IdComprobVenta"
        Me.gcIdComprobante.Name = "gcIdComprobante"
        Me.gcIdComprobante.OptionsColumn.AllowEdit = False
        Me.gcIdComprobante.Visible = True
        Me.gcIdComprobante.VisibleIndex = 0
        Me.gcIdComprobante.Width = 54
        '
        'gcTipoComprobante
        '
        Me.gcTipoComprobante.Caption = "Tipo Doc."
        Me.gcTipoComprobante.ColumnEdit = Me.riteTipoComprobante
        Me.gcTipoComprobante.FieldName = "IdTipoComprobante"
        Me.gcTipoComprobante.Name = "gcTipoComprobante"
        Me.gcTipoComprobante.OptionsColumn.AllowEdit = False
        Me.gcTipoComprobante.Visible = True
        Me.gcTipoComprobante.VisibleIndex = 2
        Me.gcTipoComprobante.Width = 134
        '
        'riteTipoComprobante
        '
        Me.riteTipoComprobante.AutoHeight = False
        Me.riteTipoComprobante.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteTipoComprobante.Name = "riteTipoComprobante"
        '
        'gcNumero
        '
        Me.gcNumero.Caption = "Número"
        Me.gcNumero.FieldName = "Numero"
        Me.gcNumero.Name = "gcNumero"
        Me.gcNumero.OptionsColumn.AllowEdit = False
        Me.gcNumero.Visible = True
        Me.gcNumero.VisibleIndex = 1
        Me.gcNumero.Width = 73
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha"
        Me.gcFecha.FieldName = "Fecha"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.OptionsColumn.AllowEdit = False
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 3
        Me.gcFecha.Width = 64
        '
        'gcCliente
        '
        Me.gcCliente.Caption = "Cliente"
        Me.gcCliente.ColumnEdit = Me.riteCliente
        Me.gcCliente.FieldName = "IdCliente"
        Me.gcCliente.Name = "gcCliente"
        Me.gcCliente.OptionsColumn.AllowEdit = False
        Me.gcCliente.OptionsColumn.AllowFocus = False
        Me.gcCliente.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "IdCliente", "TOTALES:")})
        Me.gcCliente.Visible = True
        Me.gcCliente.VisibleIndex = 4
        Me.gcCliente.Width = 336
        '
        'riteCliente
        '
        Me.riteCliente.AutoHeight = False
        Me.riteCliente.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riteCliente.Name = "riteCliente"
        '
        'gcTotalComprobante
        '
        Me.gcTotalComprobante.Caption = "Valor"
        Me.gcTotalComprobante.FieldName = "TotalComprobante"
        Me.gcTotalComprobante.Name = "gcTotalComprobante"
        Me.gcTotalComprobante.OptionsColumn.AllowEdit = False
        Me.gcTotalComprobante.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalComprobante", "{0:c}")})
        Me.gcTotalComprobante.Visible = True
        Me.gcTotalComprobante.VisibleIndex = 5
        Me.gcTotalComprobante.Width = 84
        '
        'riCantidad
        '
        Me.riCantidad.AutoHeight = False
        Me.riCantidad.Mask.EditMask = "n2"
        Me.riCantidad.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riCantidad.Mask.UseMaskAsDisplayFormat = True
        Me.riCantidad.Name = "riCantidad"
        '
        'riPorcDescto
        '
        Me.riPorcDescto.AutoHeight = False
        Me.riPorcDescto.Mask.EditMask = "n2"
        Me.riPorcDescto.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riPorcDescto.Mask.UseMaskAsDisplayFormat = True
        Me.riPorcDescto.Name = "riPorcDescto"
        '
        'riPrecioUnitario
        '
        Me.riPrecioUnitario.AutoHeight = False
        Me.riPrecioUnitario.Mask.EditMask = "n6"
        Me.riPrecioUnitario.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riPrecioUnitario.Mask.UseMaskAsDisplayFormat = True
        Me.riPrecioUnitario.Name = "riPrecioUnitario"
        '
        'riCodProd
        '
        Me.riCodProd.AutoHeight = False
        Me.riCodProd.Name = "riCodProd"
        '
        'riteIdPrecio
        '
        Me.riteIdPrecio.AutoHeight = False
        Me.riteIdPrecio.Mask.EditMask = "n0"
        Me.riteIdPrecio.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.riteIdPrecio.Mask.UseMaskAsDisplayFormat = True
        Me.riteIdPrecio.Name = "riteIdPrecio"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.cmdBorrar)
        Me.PanelControl1.Controls.Add(Me.cmdDown)
        Me.PanelControl1.Controls.Add(Me.cmdUp)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelControl1.Location = New System.Drawing.Point(763, 161)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(38, 321)
        Me.PanelControl1.TabIndex = 7
        '
        'cmdBorrar
        '
        Me.cmdBorrar.Image = Global.Nexus.My.Resources.Resources.DeleteHS
        Me.cmdBorrar.Location = New System.Drawing.Point(6, 76)
        Me.cmdBorrar.Name = "cmdBorrar"
        Me.cmdBorrar.Size = New System.Drawing.Size(28, 24)
        Me.cmdBorrar.TabIndex = 34
        '
        'cmdDown
        '
        Me.cmdDown.Image = Global.Nexus.My.Resources.Resources.FillDownHS
        Me.cmdDown.Location = New System.Drawing.Point(6, 41)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(28, 24)
        Me.cmdDown.TabIndex = 33
        '
        'cmdUp
        '
        Me.cmdUp.Image = Global.Nexus.My.Resources.Resources.FillUpHS
        Me.cmdUp.Location = New System.Drawing.Point(6, 6)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(28, 24)
        Me.cmdUp.TabIndex = 32
        '
        'gcHeader
        '
        Me.gcHeader.Controls.Add(Me.LabelControl12)
        Me.gcHeader.Controls.Add(Me.deFecha)
        Me.gcHeader.Controls.Add(Me.sbObtener)
        Me.gcHeader.Controls.Add(Me.teNumero)
        Me.gcHeader.Controls.Add(Me.teSerie)
        Me.gcHeader.Controls.Add(Me.leTipo)
        Me.gcHeader.Controls.Add(Me.lePunto)
        Me.gcHeader.Controls.Add(Me.leSucursal)
        Me.gcHeader.Controls.Add(Me.LabelControl5)
        Me.gcHeader.Controls.Add(Me.LabelControl4)
        Me.gcHeader.Controls.Add(Me.LabelControl3)
        Me.gcHeader.Controls.Add(Me.LabelControl2)
        Me.gcHeader.Controls.Add(Me.LabelControl1)
        Me.gcHeader.Controls.Add(Me.leVendedor)
        Me.gcHeader.Controls.Add(Me.LabelControl8)
        Me.gcHeader.Controls.Add(Me.teCorrelativo)
        Me.gcHeader.Controls.Add(Me.LabelControl14)
        Me.gcHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.gcHeader.Location = New System.Drawing.Point(0, 0)
        Me.gcHeader.Name = "gcHeader"
        Me.gcHeader.Size = New System.Drawing.Size(801, 161)
        Me.gcHeader.TabIndex = 1
        Me.gcHeader.Text = "Datos del pedido"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(281, 26)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl12.TabIndex = 54
        Me.LabelControl12.Text = "Fecha:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(316, 24)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.deFecha.Size = New System.Drawing.Size(86, 20)
        Me.deFecha.TabIndex = 53
        '
        'sbObtener
        '
        Me.sbObtener.Location = New System.Drawing.Point(466, 129)
        Me.sbObtener.Name = "sbObtener"
        Me.sbObtener.Size = New System.Drawing.Size(75, 23)
        Me.sbObtener.TabIndex = 49
        Me.sbObtener.Text = "Obtener..."
        '
        'teNumero
        '
        Me.teNumero.Location = New System.Drawing.Point(360, 131)
        Me.teNumero.Name = "teNumero"
        Me.teNumero.Size = New System.Drawing.Size(100, 20)
        Me.teNumero.TabIndex = 47
        '
        'teSerie
        '
        Me.teSerie.Location = New System.Drawing.Point(145, 131)
        Me.teSerie.Name = "teSerie"
        Me.teSerie.Size = New System.Drawing.Size(100, 20)
        Me.teSerie.TabIndex = 46
        '
        'leTipo
        '
        Me.leTipo.Location = New System.Drawing.Point(145, 108)
        Me.leTipo.Name = "leTipo"
        Me.leTipo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leTipo.Size = New System.Drawing.Size(315, 20)
        Me.leTipo.TabIndex = 44
        '
        'lePunto
        '
        Me.lePunto.Location = New System.Drawing.Point(145, 87)
        Me.lePunto.Name = "lePunto"
        Me.lePunto.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.lePunto.Size = New System.Drawing.Size(315, 20)
        Me.lePunto.TabIndex = 41
        '
        'leSucursal
        '
        Me.leSucursal.Location = New System.Drawing.Point(145, 66)
        Me.leSucursal.Name = "leSucursal"
        Me.leSucursal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leSucursal.Size = New System.Drawing.Size(315, 20)
        Me.leSucursal.TabIndex = 40
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(316, 134)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl5.TabIndex = 48
        Me.LabelControl5.Text = "Número:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(114, 134)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(28, 13)
        Me.LabelControl4.TabIndex = 45
        Me.LabelControl4.Text = "Serie:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(118, 111)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl3.TabIndex = 43
        Me.LabelControl3.Text = "Tipo:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(64, 91)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl2.TabIndex = 42
        Me.LabelControl2.Text = "Punto de Venta:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(98, 69)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl1.TabIndex = 39
        Me.LabelControl1.Text = "Sucursal:"
        '
        'leVendedor
        '
        Me.leVendedor.EnterMoveNextControl = True
        Me.leVendedor.Location = New System.Drawing.Point(145, 45)
        Me.leVendedor.Name = "leVendedor"
        Me.leVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leVendedor.Size = New System.Drawing.Size(257, 20)
        Me.leVendedor.TabIndex = 8
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(13, 48)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(129, 13)
        Me.LabelControl8.TabIndex = 28
        Me.LabelControl8.Text = "Entrega de Documentos A:"
        '
        'teCorrelativo
        '
        Me.teCorrelativo.Enabled = False
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(145, 23)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Properties.ReadOnly = True
        Me.teCorrelativo.Size = New System.Drawing.Size(100, 20)
        Me.teCorrelativo.TabIndex = 1
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(85, 26)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl14.TabIndex = 17
        Me.LabelControl14.Text = "Correlativo:"
        '
        'cpc_frmEntregaDocumentos
        '
        Me.ClientSize = New System.Drawing.Size(807, 533)
        Me.Controls.Add(Me.xtcPedidos)
        Me.Modulo = "Cuentas Por Cobrar"
        Me.Name = "cpc_frmEntregaDocumentos"
        Me.OptionId = "002007"
        Me.Text = "Entrega de Documentos para Cobro"
        Me.Controls.SetChildIndex(Me.xtcPedidos, 0)
        CType(Me.xtcPedidos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtcPedidos.ResumeLayout(False)
        Me.xtpLista.ResumeLayout(False)
        CType(Me.gc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursalDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.xtpDatos.ResumeLayout(False)
        CType(Me.pcTotales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteTipoComprobante, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riPorcDescto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riPrecioUnitario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riCodProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riteIdPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.gcHeader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcHeader.ResumeLayout(False)
        Me.gcHeader.PerformLayout()
        CType(Me.deFecha.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNumero.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teSerie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leTipo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lePunto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leSucursal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents xtcPedidos As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents xtpLista As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents xtpDatos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gcHeader As DevExpress.XtraEditors.GroupControl
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents pcTotales As DevExpress.XtraEditors.PanelControl
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcIdComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riCodProd As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcTipoComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcNumero As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riCantidad As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcCliente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcTotalComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riPorcDescto As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents riPrecioUnitario As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cmdBorrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdDown As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdUp As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gc2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents leSucursalDetalle As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents riteIdPrecio As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents leVendedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents sbObtener As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents teNumero As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teSerie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents leTipo As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lePunto As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents leSucursal As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents riteTipoComprobante As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents riteCliente As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit

End Class
