﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class cpc_frmAuxiliar
    Dim bl As New CuentasPCBLL(g_ConnectionString)
    Dim entCliente As fac_Clientes
    Private Sub cpc_frmAuxiliar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        deHasta.DateTime = Today
        BeCliente.beCodigo.EditValue = ""
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub
    
    Private Sub cpc_frmAnalisis_Report_Click() Handles Me.Reporte
        Dim rpt As New cpc_rptAuxiliar() With {.DataSource = bl.getAuxiliar(BeCliente.beCodigo.EditValue, deHasta.EditValue, leSucursal.EditValue), .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = FechaToString(deHasta.EditValue, deHasta.EditValue).ToUpper()
        rpt.ShowPreviewDialog()
    End Sub


    Private Sub deHasta_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deHasta.EditValueChanged
        CargaGrid()
    End Sub

    Private Sub leSucursal_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles leSucursal.EditValueChanged
        CargaGrid()
    End Sub

    Private Sub CargaGrid()
        If SiEsNulo(deHasta.EditValue, Nothing) = Nothing Or SiEsNulo(leSucursal.EditValue, 0) = 0 Then
            Exit Sub
        End If

        gc.DataSource = bl.getAuxiliar(SiEsNulo(BeCliente.beCodigo.EditValue, ""), deHasta.EditValue, leSucursal.EditValue)
    End Sub
End Class
