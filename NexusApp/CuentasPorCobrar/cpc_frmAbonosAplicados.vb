﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class cpc_frmAbonosAplicados
    Dim bl As New CuentasPCBLL(g_ConnectionString)

    Private Sub cpc_frmAbonosAplicados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = CDate(String.Format("01/{0}/{1}", Today.Month, Today.Year))
        deHasta.EditValue = Today
    End Sub
    Private Sub beCliente_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCliente.ButtonClick
        beCliente.EditValue = ""
        beCliente_Validated(sender, New System.EventArgs)
    End Sub
    Private Sub beCliente_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles beCliente.Validated

        Dim ent As fac_Clientes = objConsultas.cnsClientes(fac_frmConsultaClientes, beCliente.EditValue)

        beCliente.EditValue = ent.IdCliente
        teNombre.EditValue = ent.Nombre
        teNombre.Focus()
    End Sub
    Private Sub btObtener_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btObtener.Click
        gc.DataSource = bl.cpc_ConsultaAbonosClientes(beCliente.EditValue, deDesde.EditValue, deHasta.EditValue)
        gv.BestFitColumns()
    End Sub

    Private Sub cpc_frmAbonosAplicados_Reporte() Handles Me.Reporte
        gc.ShowPrintPreview()
    End Sub

    Private Sub sbImprimir_Click(sender As Object, e As EventArgs) Handles sbImprimir.Click
        Dim Fecha As DateTime = gv.GetFocusedRowCellValue("Fecha")
        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)
        Dim Template = Application.StartupPath & "\Plantillas\naNexus" & NumFormato & ".repx"
        Dim IdComprobante As Integer = gv.GetFocusedRowCellValue("IdComprobante")

        Dim dt As DataTable = bl.ObtenerAbono(IdComprobante, Fecha)

        Dim rpt As New cpc_rptAbono
        If Not FileIO.FileSystem.FileExists(Template) Then
        Else
            rpt.LoadLayout(Template)
            rpt.DataSource = dt
            rpt.DataMember = ""
        End If

        rpt.DataSource = dt
        rpt.DataMember = ""
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "COMPROBANTE DE NOTA DE ABONO"
        rpt.xrlPeriodo.Text = "Fecha del Abono: " & FechaToString(Fecha, Fecha)
        Dim TotalAbono As Decimal = gv.GetFocusedRowCellValue("MontoAbonado")
        Dim Decimales = String.Format("{0:c}", TotalAbono)
        Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
        rpt.xrlCantidadLetras.Text = Num2Text(Int(TotalAbono)) & Decimales & " DÓLARES"

        rpt.ShowPreviewDialog()
    End Sub
End Class
