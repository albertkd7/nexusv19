﻿Imports NexusELL.TableEntities
Imports NexusBLL
Public Class cpc_frmAnulaCargo
    Dim dtDetalle As New DataTable
    Dim bl As New NexusBLL.CuentasPCBLL(g_ConnectionString)
    Dim blF As New FacturaBLL(g_ConnectionString)
    Dim AbonoHeader As cpc_Abonos
    Dim AbonoDetalle As List(Of cpc_AbonosDetalle)
    Private Sub sbObtener_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If BeCliente1.beCodigo.EditValue = "" Then
            MsgBox("Debe de especificar el código de cliente", MsgBoxStyle.Information)
            Exit Sub
        End If
        gc.DataSource = bl.ConsultaCargos(BeCliente1.beCodigo.EditValue)
    End Sub

    Private Sub cpc_frmAnulaAbonos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LimpiaPantalla()
    End Sub

    Private Sub LimpiaPantalla()
        gc.DataSource = Nothing
    End Sub

    Private Sub BeCliente1_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles BeCliente1.Validated
        If SiEsNulo(BeCliente1.beCodigo.EditValue, "") = "" Then
            Exit Sub
        End If
        gc.DataSource = bl.ConsultaCargos(BeCliente1.beCodigo.EditValue)
    End Sub

    Private Sub btEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btEliminar.Click
        Dim IdComprobante As Integer = 0
        Dim msj As String = ""
        If SiEsNulo(BeCliente1.beCodigo.EditValue, "") = "" Then
            MsgBox("Debe de especificar el código de Cliente al que desea eliminar el Abono", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If MsgBox("Está seguro(a) de Eliminar éste Cargo?" & Chr(13) & "Ya no podrá revertir la eliminación", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        Dim EsOk As Boolean = ValidarFechaCierre(gv.GetRowCellValue(gv.FocusedRowHandle, "Fecha"))
        If Not EsOk Then
            msj = "La fecha del Abono corresponde a un Periodo ya Cerrado"
        End If
        '*-- verifico si no hay abonos aplicados
        IdComprobante = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, "IdComprobante"), 0)
        Dim sFecha = SiEsNulo(blF.Verificar_Abonos(IdComprobante), "")
        If sFecha <> "" Then
            MsgBox("Imposible anular éste documento. Ya tiene abonos aplicados" + Chr(13) + _
            "Fecha del abono: " + sFecha, 16, "Imposible Anular")
            Exit Sub
        End If
        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If

        Try
            objTablas.fac_VentasDeleteByPK(IdComprobante)
        Catch ex As Exception
            msj = ex.Message()
        End Try
        If msj = "" Then
            MsgBox("El Cargo fue eliminado con éxito", MsgBoxStyle.Information)
        Else
            MsgBox("No fue posible eliminar el Cargo" + Chr(13) + msj, MsgBoxStyle.Critical, "Error al elminar el registro")
        End If
        LimpiaPantalla()
    End Sub
End Class
