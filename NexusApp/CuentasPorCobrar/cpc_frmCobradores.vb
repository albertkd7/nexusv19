﻿Imports DevExpress.XtraEditors
Imports NexusELL.TableEntities
Imports NexusBLL
Public Class cpc_frmCobradores
    Dim entidad As cpc_Cobradores

    Private Sub cpc_frmCobradores_Guardar() Handles Me.Guardar
        If teNombre.EditValue = "" Then
            MsgBox("Existen algunos datos que no pueden quedar en blanco" + Chr(13) + "Verifique [ Nombre]", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If

        CargaEntidad()
        If DbMode = DbModeType.insert Then
            objTablas.cpc_CobradoresInsert(entidad)
        Else
            objTablas.cpc_CobradoresUpdate(entidad)
        End If
        gc.DataSource = objTablas.cpc_CobradoresSelectAll
        entidad = objTablas.cpc_CobradoresSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCobrador"))
        CargaPantalla()
        ActivaControles(False)
        MostrarModoInicial()
    End Sub

    Private Sub cpc_frmCobradores_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gc.DataSource = objTablas.cpc_CobradoresSelectAll

        entidad = objTablas.cpc_CobradoresSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCobrador"))

        CargaPantalla()
        ActivaControles(False)
    End Sub

    Private Sub CargaEntidad()
        With entidad
            .IdCobrador = teID.EditValue
            .Nombre = teNombre.EditValue
            .PorcComision = SpinEdit1.EditValue
        End With
    End Sub

    Private Sub cpc_frmCobradores_Delete_Click() Handles Me.Eliminar
        If MsgBox("Está seguro(a) de eliminar el cobrador?", MsgBoxStyle.YesNo, "Nexus") = MsgBoxResult.No Then
            Return
        End If

        Dim IdGestor As Integer = gv.GetFocusedRowCellValue(gv.Columns(0))
        objTablas.cpc_CobradoresDeleteByPK(IdGestor)
        gc.DataSource = objTablas.cpc_CobradoresSelectAll
    End Sub
    
    Private Sub ban_frmTiposTransaccion_Cancelar_Click() Handles Me.Revertir
        ActivaControles(False)
    End Sub

    Private Sub ban_frmTiposTransaccion_Editar() Handles Me.Editar
        ActivaControles(True)
    End Sub
    Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gv.Click
        entidad = objTablas.cpc_CobradoresSelectByPK(gv.GetRowCellValue(gv.FocusedRowHandle, "IdCobrador"))
        CargaPantalla()
    End Sub

    Private Sub CargaPantalla()
        With entidad
            teID.EditValue = .IdCobrador
            teNombre.EditValue = .Nombre
            SpinEdit1.EditValue = .PorcComision
        End With
        teNombre.Focus()
    End Sub

    Private Sub cpc_frmCobradores_Nuevo() Handles Me.Nuevo
        entidad = New cpc_Cobradores
        entidad.IdCobrador = objFunciones.ObtenerUltimoId("cpc_Cobradores", "IdCobrador") + 1
        CargaPantalla()
        ActivaControles(True)
    End Sub

    Private Sub ActivaControles(ByVal Tipo As Boolean)
        For Each ctrl In PanelControl1.Controls
            If TypeOf ctrl Is DevExpress.XtraEditors.TextEdit Then
                CType(ctrl, DevExpress.XtraEditors.TextEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.ButtonEdit Then
                CType(ctrl, DevExpress.XtraEditors.ButtonEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.CheckEdit Then
                CType(ctrl, DevExpress.XtraEditors.CheckEdit).Properties.ReadOnly = Not Tipo
            End If
            If TypeOf ctrl Is DevExpress.XtraEditors.LookUpEdit Then
                CType(ctrl, DevExpress.XtraEditors.LookUpEdit).Properties.ReadOnly = Not Tipo
            End If
        Next
        teId.Properties.ReadOnly = True
        teNombre.Focus()
    End Sub
End Class
