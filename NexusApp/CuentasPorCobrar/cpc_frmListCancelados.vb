﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class cpc_frmListCancelados
    Dim bl As New CuentasPCBLL(g_ConnectionString)

    Private Sub cpc_frmListCancelados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.DateTime = Today
        deHasta.DateTime = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub cpc_frmListCancelados_Report_Click() Handles Me.Reporte
        Dim dt = bl.getDocsCancelados(deDesde.EditValue, deHasta.EditValue, BeCliente1.beCodigo.EditValue, leSucursal.EditValue)
        Dim rpt As New cpc_rptDocsCancelados() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Informe de documentos cancelados | SUCURSAL: " + leSucursal.Text
        rpt.xrlPeriodo.Text = (FechaToString(deDesde.EditValue, deHasta.EditValue)).ToUpper
        
        rpt.ShowPreviewDialog()
    End Sub

End Class
