﻿Imports NexusBLL
Imports NexusELL.TableEntities

Public Class cpc_frmListAnulados
    Dim bl As New CuentasPCBLL(g_ConnectionString)

    Private Sub cpc_frmListAnulados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        deDesde.EditValue = CDate("1/" & Month(Today) & "/" & Year(Today))
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")

    End Sub

    Private Sub fac_frmListAnulados_Report_Click() Handles Me.Reporte
        Dim dt As New DataTable
        dt = bl.cpc_DocumentosAnulados(deDesde.EditValue, deHasta.EditValue, BeCliente.beCodigo.EditValue, leSucursal.EditValue)

        Dim rpt As New cpc_rptDocumentosAnulados() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue
        rpt.xrlPeriodo.Text = FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt.ShowPreviewDialog()
    End Sub


End Class
