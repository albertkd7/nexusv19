﻿
Imports NexusBLL
Public Class cpc_frmDocumentosCredito
    Dim bl As New CuentasPCBLL(g_ConnectionString)

    Private Sub cpcDocumentosCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deFechaI.EditValue = Today
        deFechaF.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub Report1_Click() Handles Me.Reporte

        Dim rpt As New cpc_rptDocsCredito() With {.DataSource = bl.getDocsCredito(deFechaI.EditValue, deFechaF.EditValue, BeCliente1.beCodigo.EditValue, leSucursal.EditValue), .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "Informe de documentos al crédito"
        rpt.xrlPeriodo.Text = (FechaToString(deFechaI.EditValue, deFechaF.EditValue)).ToUpper
        rpt.xrlMoneda.Text = gsDesc_Moneda
        
        rpt.ShowPreviewDialog()
    End Sub

End Class
