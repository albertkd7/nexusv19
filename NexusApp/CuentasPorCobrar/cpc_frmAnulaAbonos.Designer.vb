﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpc_frmAnulaAbonos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btReimprime = New DevExpress.XtraEditors.SimpleButton()
        Me.btEliminar = New DevExpress.XtraEditors.SimpleButton()
        Me.gc = New DevExpress.XtraGrid.GridControl()
        Me.gv = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.gcNumeroComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcFecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcConcepto = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcTipo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcMontoAbonado = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.gcIdComprobante = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riIdRecibo = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.BeCliente1 = New Nexus.beCliente()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.sb = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.gc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riIdRecibo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btReimprime
        '
        Me.btReimprime.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.btReimprime.Appearance.Options.UseFont = True
        Me.btReimprime.Location = New System.Drawing.Point(692, 147)
        Me.btReimprime.Name = "btReimprime"
        Me.btReimprime.Size = New System.Drawing.Size(112, 29)
        Me.btReimprime.TabIndex = 74
        Me.btReimprime.Text = "&Re-Imprimir"
        '
        'btEliminar
        '
        Me.btEliminar.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.btEliminar.Appearance.Options.UseFont = True
        Me.btEliminar.Location = New System.Drawing.Point(692, 112)
        Me.btEliminar.Name = "btEliminar"
        Me.btEliminar.Size = New System.Drawing.Size(112, 29)
        Me.btEliminar.TabIndex = 73
        Me.btEliminar.Text = "&Anular Abono"
        '
        'gc
        '
        Me.gc.Dock = System.Windows.Forms.DockStyle.Left
        Me.gc.Location = New System.Drawing.Point(0, 86)
        Me.gc.MainView = Me.gv
        Me.gc.Name = "gc"
        Me.gc.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riIdRecibo})
        Me.gc.Size = New System.Drawing.Size(689, 358)
        Me.gc.TabIndex = 8
        Me.gc.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv})
        '
        'gv
        '
        Me.gv.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gcNumeroComprobante, Me.gcFecha, Me.gcConcepto, Me.gcTipo, Me.gcMontoAbonado, Me.gcIdComprobante})
        Me.gv.GridControl = Me.gc
        Me.gv.Name = "gv"
        Me.gv.OptionsBehavior.Editable = False
        Me.gv.OptionsView.ShowFooter = True
        Me.gv.OptionsView.ShowGroupPanel = False
        '
        'gcNumeroComprobante
        '
        Me.gcNumeroComprobante.Caption = "No. de Recibo"
        Me.gcNumeroComprobante.FieldName = "NumeroComprobante"
        Me.gcNumeroComprobante.Name = "gcNumeroComprobante"
        Me.gcNumeroComprobante.Visible = True
        Me.gcNumeroComprobante.VisibleIndex = 0
        Me.gcNumeroComprobante.Width = 87
        '
        'gcFecha
        '
        Me.gcFecha.Caption = "Fecha"
        Me.gcFecha.FieldName = "Fecha"
        Me.gcFecha.Name = "gcFecha"
        Me.gcFecha.Visible = True
        Me.gcFecha.VisibleIndex = 1
        Me.gcFecha.Width = 69
        '
        'gcConcepto
        '
        Me.gcConcepto.Caption = "Concepto"
        Me.gcConcepto.FieldName = "Concepto"
        Me.gcConcepto.Name = "gcConcepto"
        Me.gcConcepto.Visible = True
        Me.gcConcepto.VisibleIndex = 2
        Me.gcConcepto.Width = 259
        '
        'gcTipo
        '
        Me.gcTipo.Caption = "Tipo Abono"
        Me.gcTipo.FieldName = "TipoAbono"
        Me.gcTipo.Name = "gcTipo"
        Me.gcTipo.Visible = True
        Me.gcTipo.VisibleIndex = 3
        Me.gcTipo.Width = 142
        '
        'gcMontoAbonado
        '
        Me.gcMontoAbonado.Caption = "Monto Abonado"
        Me.gcMontoAbonado.FieldName = "MontoAbonado"
        Me.gcMontoAbonado.Name = "gcMontoAbonado"
        Me.gcMontoAbonado.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontoAbonado", "{0:c}")})
        Me.gcMontoAbonado.Visible = True
        Me.gcMontoAbonado.VisibleIndex = 4
        Me.gcMontoAbonado.Width = 98
        '
        'gcIdComprobante
        '
        Me.gcIdComprobante.Caption = "GridColumn1"
        Me.gcIdComprobante.FieldName = "IdComprobante"
        Me.gcIdComprobante.Name = "gcIdComprobante"
        '
        'riIdRecibo
        '
        Me.riIdRecibo.AutoHeight = False
        Me.riIdRecibo.Name = "riIdRecibo"
        '
        'BeCliente1
        '
        Me.BeCliente1.Location = New System.Drawing.Point(69, 27)
        Me.BeCliente1.Name = "BeCliente1"
        Me.BeCliente1.Size = New System.Drawing.Size(600, 20)
        Me.BeCliente1.TabIndex = 0
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.sb)
        Me.GroupControl1.Controls.Add(Me.BeCliente1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(857, 86)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Datos del cliente"
        '
        'sb
        '
        Me.sb.Location = New System.Drawing.Point(112, 53)
        Me.sb.Name = "sb"
        Me.sb.Size = New System.Drawing.Size(121, 23)
        Me.sb.TabIndex = 67
        Me.sb.Text = "Obtener datos..."
        '
        'cpc_frmAnulaAbonos
        '
        Me.ClientSize = New System.Drawing.Size(857, 469)
        Me.Controls.Add(Me.gc)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.btReimprime)
        Me.Controls.Add(Me.btEliminar)
        Me.Modulo = "Cuentas por Cobrar"
        Me.Name = "cpc_frmAnulaAbonos"
        Me.OptionId = "002002"
        Me.Text = "Anula abonos"
        Me.TipoFormulario = 3
        Me.Controls.SetChildIndex(Me.btEliminar, 0)
        Me.Controls.SetChildIndex(Me.btReimprime, 0)
        Me.Controls.SetChildIndex(Me.GroupControl1, 0)
        Me.Controls.SetChildIndex(Me.gc, 0)
        CType(Me.gc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riIdRecibo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gc As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents gcNumeroComprobante As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcFecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcConcepto As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riIdRecibo As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gcTipo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents gcMontoAbonado As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btEliminar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btReimprime As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BeCliente1 As Nexus.beCliente
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents sb As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gcIdComprobante As DevExpress.XtraGrid.Columns.GridColumn

End Class
