﻿
Imports NexusELL.TableEntities
Public Class cpc_frmAnulaAbonos
    Dim dtDetalle As New DataTable
    Dim bl As New NexusBLL.CuentasPCBLL(g_ConnectionString)
    Dim AbonoHeader As cpc_Abonos
    Dim AbonoDetalle As List(Of cpc_AbonosDetalle)

    Private Sub sbObtener_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If BeCliente1.beCodigo.EditValue = "" Then
            MsgBox("Debe de especificar el código de cliente", MsgBoxStyle.Information)
            Exit Sub
        End If
        gc.DataSource = bl.ConsultaAbonos(BeCliente1.beCodigo.EditValue)
    End Sub

    Private Sub BeCliente1_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles BeCliente1.Validated

        If SiEsNulo(BeCliente1.beCodigo.EditValue, "") = "" Then
            Exit Sub
        End If
        'IdComprobante = bl.cpc_ObtenerUltAbono(BeCliente1.beCodigo.EditValue)
        'AbonoHeader = objTablas.cpc_AbonosSelectByPK(IdComprobante)

        'With AbonoHeader
        '    txtNroRecibo.EditValue = .NumeroComprobante
        '    deFecha.EditValue = .Fecha
        '    cboEjecutivo.EditValue = .IdCobrador
        '    leCuentaBanco.EditValue = .IdCuentaBancaria
        '    txtRecMora.EditValue = .RecargoMora
        '    txtPjeComis.EditValue = .PorcComision
        '    leTipoAbono.EditValue = .IdTipoMov
        '    txtConcepto.EditValue = .Concepto
        '    leSucursal.EditValue = .IdSucursal
        'End With

    End Sub

    Private Sub btEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btEliminar.Click
        Dim IdComprobante As Integer = SiEsNulo(gv.GetFocusedRowCellValue("IdComprobante"), 0)
        If IdComprobante = 0 Then
            MsgBox("Debe de especificar el documento de abono que desea eliminar", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
        Dim msj As String = ""
        If SiEsNulo(BeCliente1.beCodigo.EditValue, "") = "" Then
            MsgBox("Debe de especificar el código de cliente al que desea eliminar el abono", MsgBoxStyle.Critical, "Nota")
            Exit Sub
        End If
       
        If MsgBox("Está seguro(a) de Anular éste abono?" & Chr(13) & "Ya no podrá revertir la anulación", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        Dim EsOk As Boolean = ValidarFechaCierre(gv.GetFocusedRowCellValue("Fecha"))
        If Not EsOk Then
            msj = "La fecha del Abono corresponde a un Periodo ya Cerrado"
        End If

        'IdComprobante = bl.cpc_ObtenerUltAbono(BeCliente1.beCodigo.EditValue)
        AbonoHeader = objTablas.cpc_AbonosSelectByPK(IdComprobante)

        If AbonoHeader.IdSucursal <> piIdSucursalUsuario And piIdSucursalUsuario <> 1 Then
            MsgBox("No puede anular un abono de otra sucursal", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If msj <> "" Then
            MsgBox(msj, MsgBoxStyle.Critical, "Error de usuario")
            Exit Sub
        End If

        Try
            AbonoHeader.ModificadoPor = objMenu.User
            objTablas.cpc_AbonosUpdate(AbonoHeader)

            bl.cpc_ActualizaFechaCancelacion(IdComprobante) ' vuelvo nula la fecha de cancelacion
            bl.cpc_AnulaAbono(IdComprobante, objMenu.User)

            If AbonoHeader.IdCuentaBancaria > 0 Then
                objTablas.ban_TransaccionesDeleteByPK(AbonoHeader.IdTransaccion)
                objTablas.con_PartidasDeleteByPK(AbonoHeader.IdPartida)
            Else
                objTablas.con_PartidasDeleteByPK(AbonoHeader.IdPartida)
            End If

        Catch ex As Exception
            msj = ex.Message()
        End Try
        If msj = "" Then
            MsgBox("El Abono fue anulado con éxito", MsgBoxStyle.Information)
        Else
            MsgBox("No fue posible anular el Abono" + Chr(13) + msj, MsgBoxStyle.Critical, "Error al anular el registro")
        End If
        Close()
    End Sub

    Private Sub btReimprime_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btReimprime.Click

        Dim NumFormato As String = g_ConnectionString.Substring(3, 2)
        Dim Template = Application.StartupPath & "\Plantillas\naNexus" & NumFormato & ".repx"

        Dim Fecha As Date = gv.GetFocusedRowCellValue("Fecha")
        Dim dt As DataTable = bl.ObtenerAbono(gv.GetFocusedRowCellValue("IdComprobante"), Fecha)

        Dim rpt As New cpc_rptAbono
        If Not FileIO.FileSystem.FileExists(Template) Then
            MsgBox("No existe la plantilla, se imprimirá en formato estandar", MsgBoxStyle.Information, "Nota")
        Else
            rpt.LoadLayout(Template)
        End If

        rpt.DataSource = dt
        rpt.DataMember = ""
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "COMPROBANTE DE NOTA DE ABONO"
        rpt.xrlPeriodo.Text = "Fecha del Abono: " & FechaToString(Fecha, Fecha)
        Dim TotalAbonado As Decimal = gv.GetFocusedRowCellValue("TotalAbonado")

        Dim Decimales = String.Format("{0:c}", TotalAbonado)
        Decimales = " " & Decimales.Substring(Decimales.Length - 2) & "/100"
        rpt.xrlCantidadLetras.Text = Num2Text(Int(TotalAbonado)) & Decimales & " DÓLARES"

        rpt.ShowPreviewDialog()
    End Sub

    Private Sub sb_Click(sender As Object, e As EventArgs) Handles sb.Click
        gc.DataSource = bl.cpc_ConsultaAbonosClientes(BeCliente1.beCodigo.EditValue, DateAdd(DateInterval.Year, -100, Today()), Today().AddYears(1))
    End Sub
End Class
