﻿
Imports NexusBLL
Imports NexusELL.TableEntities
Public Class cpc_frmHistoricoMovimientos
    Dim bl As New CuentasPCBLL(g_ConnectionString)

    Private Sub cpc_frmEstadoCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        deDesde.EditValue = Today
        deHasta.EditValue = Today
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub cpc_frmEstadoCuenta_Report_Click() Handles Me.Reporte
        If BeCliente1.beCodigo.EditValue = "" Then
            MsgBox("Debe especificar un código de cliente para generar este informe", 64, "Nota")
            Exit Sub
        End If
        Dim dt = bl.cpc_MovimientoHistoricoPorCliente(BeCliente1.beCodigo.EditValue, deDesde.EditValue, deHasta.EditValue, leSucursal.EditValue)
        If dt.Rows.Count = 0 Then
            MsgBox("No se encontraron transacciones para mostrar informe", 64, "Nota")
            Exit Sub
        End If
        Dim rpt As New cpc_rptHistoricoMovimientos() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = teTitulo.EditValue & " " & FechaToString(deDesde.EditValue, deHasta.EditValue)
        rpt.xrlPeriodo.Text = "Cliente: " & BeCliente1.teNombre.EditValue
        rpt.ShowPreviewDialog()
    End Sub

End Class
