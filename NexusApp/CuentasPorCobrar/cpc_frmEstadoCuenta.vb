﻿
Imports NexusBLL
Public Class cpc_frmEstadoCuenta
    Dim bl As New CuentasPCBLL(g_ConnectionString)
    Private Sub cpc_frmDocsPendiente_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        deHasta.EditValue = Today
        BeCliente1.beCodigo.EditValue = ""
        BeCliente2.beCodigo.EditValue = ""
        objCombos.adm_Sucursales(leSucursal, objMenu.User, "-- TODAS LAS SUCURSALES --")
    End Sub

    Private Sub cpc_frmEstadoCuenta_Reporte() Handles Me.Reporte
        Dim dt = bl.cpc_EstadoCuenta(deHasta.EditValue, BeCliente1.beCodigo.EditValue, BeCliente2.beCodigo.EditValue, leSucursal.EditValue)
        Dim rpt As New cpc_rptEstadoCuenta() With {.DataSource = dt, .DataMember = ""}
        rpt.xrlEmpresa.Text = gsNombre_Empresa
        rpt.xrlTitulo.Text = "ESTADO DE CUENTA"
        rpt.xrlCliente.Text = BeCliente1.teNombre.EditValue
        rpt.xrlPeriodo.Text = "AL " + (FechaToString(deHasta.EditValue, deHasta.EditValue)).ToUpper
        rpt.ShowPreviewDialog()
    End Sub


End Class
