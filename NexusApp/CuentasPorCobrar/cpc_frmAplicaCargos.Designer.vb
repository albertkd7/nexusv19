﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cpc_frmAplicaCargos
    Inherits Nexus.gen_frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.leVendedor = New DevExpress.XtraEditors.LookUpEdit()
        Me.teConcepto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.teNit = New DevExpress.XtraEditors.TextEdit()
        Me.txtNRC = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.teMonto = New DevExpress.XtraEditors.TextEdit()
        Me.teReferencia = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.teCorrelativo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.deFecha = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.teNombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.beCliente = New DevExpress.XtraEditors.ButtonEdit()
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teConcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teMonto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teReferencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(58, 161)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl9.TabIndex = 69
        Me.LabelControl9.Text = "Concepto:"
        '
        'leVendedor
        '
        Me.leVendedor.EnterMoveNextControl = True
        Me.leVendedor.Location = New System.Drawing.Point(111, 114)
        Me.leVendedor.Name = "leVendedor"
        Me.leVendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.leVendedor.Size = New System.Drawing.Size(284, 20)
        Me.leVendedor.TabIndex = 61
        '
        'teConcepto
        '
        Me.teConcepto.EditValue = ""
        Me.teConcepto.EnterMoveNextControl = True
        Me.teConcepto.Location = New System.Drawing.Point(111, 158)
        Me.teConcepto.Name = "teConcepto"
        Me.teConcepto.Size = New System.Drawing.Size(547, 20)
        Me.teConcepto.TabIndex = 63
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(58, 117)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl5.TabIndex = 67
        Me.LabelControl5.Text = "Vendedor:"
        '
        'teNit
        '
        Me.teNit.EnterMoveNextControl = True
        Me.teNit.Location = New System.Drawing.Point(111, 92)
        Me.teNit.Name = "teNit"
        Me.teNit.Properties.ReadOnly = True
        Me.teNit.Size = New System.Drawing.Size(135, 20)
        Me.teNit.TabIndex = 59
        '
        'txtNRC
        '
        Me.txtNRC.EnterMoveNextControl = True
        Me.txtNRC.Location = New System.Drawing.Point(332, 92)
        Me.txtNRC.Name = "txtNRC"
        Me.txtNRC.Properties.ReadOnly = True
        Me.txtNRC.Size = New System.Drawing.Size(135, 20)
        Me.txtNRC.TabIndex = 60
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(304, 96)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl3.TabIndex = 65
        Me.LabelControl3.Text = "NRC:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(29, 140)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(79, 13)
        Me.LabelControl8.TabIndex = 68
        Me.LabelControl8.Text = "Monto a Cargar:"
        '
        'teMonto
        '
        Me.teMonto.EditValue = "0"
        Me.teMonto.EnterMoveNextControl = True
        Me.teMonto.Location = New System.Drawing.Point(111, 136)
        Me.teMonto.Name = "teMonto"
        Me.teMonto.Properties.Appearance.Options.UseTextOptions = True
        Me.teMonto.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teMonto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.teMonto.Properties.Mask.EditMask = "n2"
        Me.teMonto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.teMonto.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teMonto.Size = New System.Drawing.Size(157, 20)
        Me.teMonto.TabIndex = 62
        '
        'teReferencia
        '
        Me.teReferencia.EditValue = ""
        Me.teReferencia.EnterMoveNextControl = True
        Me.teReferencia.Location = New System.Drawing.Point(111, 48)
        Me.teReferencia.Name = "teReferencia"
        Me.teReferencia.Properties.Appearance.Options.UseTextOptions = True
        Me.teReferencia.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.teReferencia.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.teReferencia.Size = New System.Drawing.Size(156, 20)
        Me.teReferencia.TabIndex = 56
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(34, 28)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(74, 13)
        Me.LabelControl1.TabIndex = 64
        Me.LabelControl1.Text = "No. de Control:"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(87, 96)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl4.TabIndex = 66
        Me.LabelControl4.Text = "NIT:"
        '
        'teCorrelativo
        '
        Me.teCorrelativo.EditValue = 0
        Me.teCorrelativo.Enabled = False
        Me.teCorrelativo.EnterMoveNextControl = True
        Me.teCorrelativo.Location = New System.Drawing.Point(111, 25)
        Me.teCorrelativo.Name = "teCorrelativo"
        Me.teCorrelativo.Size = New System.Drawing.Size(135, 20)
        Me.teCorrelativo.TabIndex = 54
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(3, 51)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(105, 13)
        Me.LabelControl11.TabIndex = 70
        Me.LabelControl11.Text = "Referencia del Cargo:"
        '
        'deFecha
        '
        Me.deFecha.EditValue = Nothing
        Me.deFecha.EnterMoveNextControl = True
        Me.deFecha.Location = New System.Drawing.Point(332, 25)
        Me.deFecha.Name = "deFecha"
        Me.deFecha.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deFecha.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deFecha.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret
        Me.deFecha.Size = New System.Drawing.Size(86, 20)
        Me.deFecha.TabIndex = 55
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(296, 28)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl12.TabIndex = 72
        Me.LabelControl12.Text = "Fecha:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(300, 72)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(41, 13)
        Me.LabelControl2.TabIndex = 75
        Me.LabelControl2.Text = "Nombre:"
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(343, 69)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.ReadOnly = True
        Me.teNombre.Size = New System.Drawing.Size(321, 20)
        Me.teNombre.TabIndex = 74
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(71, 72)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl13.TabIndex = 76
        Me.LabelControl13.Text = "Cliente:"
        '
        'beCliente
        '
        Me.beCliente.EnterMoveNextControl = True
        Me.beCliente.Location = New System.Drawing.Point(111, 69)
        Me.beCliente.Name = "beCliente"
        Me.beCliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.beCliente.Size = New System.Drawing.Size(156, 20)
        Me.beCliente.TabIndex = 73
        '
        'cpc_frmAplicaCargos
        '
        Me.ClientSize = New System.Drawing.Size(737, 289)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.teNombre)
        Me.Controls.Add(Me.LabelControl13)
        Me.Controls.Add(Me.beCliente)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.leVendedor)
        Me.Controls.Add(Me.teConcepto)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.teNit)
        Me.Controls.Add(Me.txtNRC)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.teMonto)
        Me.Controls.Add(Me.teReferencia)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.teCorrelativo)
        Me.Controls.Add(Me.LabelControl11)
        Me.Controls.Add(Me.deFecha)
        Me.Controls.Add(Me.LabelControl12)
        Me.Modulo = "Cuentas por Cobrar"
        Me.Name = "cpc_frmAplicaCargos"
        Me.OptionId = "002003"
        Me.Text = "Aplicación de cargos"
        Me.Controls.SetChildIndex(Me.LabelControl12, 0)
        Me.Controls.SetChildIndex(Me.deFecha, 0)
        Me.Controls.SetChildIndex(Me.LabelControl11, 0)
        Me.Controls.SetChildIndex(Me.teCorrelativo, 0)
        Me.Controls.SetChildIndex(Me.LabelControl4, 0)
        Me.Controls.SetChildIndex(Me.LabelControl1, 0)
        Me.Controls.SetChildIndex(Me.teReferencia, 0)
        Me.Controls.SetChildIndex(Me.teMonto, 0)
        Me.Controls.SetChildIndex(Me.LabelControl8, 0)
        Me.Controls.SetChildIndex(Me.LabelControl3, 0)
        Me.Controls.SetChildIndex(Me.txtNRC, 0)
        Me.Controls.SetChildIndex(Me.teNit, 0)
        Me.Controls.SetChildIndex(Me.LabelControl5, 0)
        Me.Controls.SetChildIndex(Me.teConcepto, 0)
        Me.Controls.SetChildIndex(Me.leVendedor, 0)
        Me.Controls.SetChildIndex(Me.LabelControl9, 0)
        Me.Controls.SetChildIndex(Me.beCliente, 0)
        Me.Controls.SetChildIndex(Me.LabelControl13, 0)
        Me.Controls.SetChildIndex(Me.teNombre, 0)
        Me.Controls.SetChildIndex(Me.LabelControl2, 0)
        CType(Me.leVendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teConcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teMonto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teReferencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teCorrelativo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deFecha.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents leVendedor As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents teConcepto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teMonto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents teReferencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teCorrelativo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deFecha As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents beCliente As DevExpress.XtraEditors.ButtonEdit

End Class
