Imports NexusELL.TableEntities
Public Class beCtaContable
    Private ElUsuarioDioClic = False
    Private Sub beIdCuenta_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beIdCuenta.ButtonClick
        beIdCuenta.EditValue = ""
        ElUsuarioDioClic = True
        beIdCuenta_Validated(sender, New System.EventArgs)
    End Sub
    Private Sub beIdCuenta_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beIdCuenta.Validated
        If Not ElUsuarioDioClic And beIdCuenta.EditValue = "" Then
            Exit Sub
        End If
        Dim ent As con_Cuentas = objConsultas.cnsCuentas(con_frmConsultaCuentas, beIdCuenta.EditValue)
        ElUsuarioDioClic = False
        If Not ent.EsTransaccional And ValidarMayor Then
            MsgBox("No puede usar �sta cuenta. No acepta transacciones o es de mayor", MsgBoxStyle.Critical, "Nota")
            beIdCuenta.EditValue = ""
            Exit Sub
        End If
        beIdCuenta.EditValue = ent.IdCuenta
        teNombreCuenta.EditValue = ent.Nombre
        teNombreCuenta.Focus()
    End Sub

    Private _ValidarMayor As Boolean
    Public Property ValidarMayor() As Boolean
        Get
            Return _ValidarMayor
        End Get
        Set(ByVal value As Boolean)
            _ValidarMayor = value
        End Set
    End Property


End Class
