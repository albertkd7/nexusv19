﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmValidaToken
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.sbCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.sbOk = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.teToken = New DevExpress.XtraEditors.TextEdit()
        CType(Me.teToken.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sbCancel
        '
        Me.sbCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.sbCancel.Location = New System.Drawing.Point(270, 80)
        Me.sbCancel.Name = "sbCancel"
        Me.sbCancel.Size = New System.Drawing.Size(93, 26)
        Me.sbCancel.TabIndex = 15
        Me.sbCancel.Text = "&Cancelar"
        '
        'sbOk
        '
        Me.sbOk.Location = New System.Drawing.Point(167, 80)
        Me.sbOk.Name = "sbOk"
        Me.sbOk.Size = New System.Drawing.Size(93, 26)
        Me.sbOk.TabIndex = 14
        Me.sbOk.Text = "&Validar"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(5, 15)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(142, 45)
        Me.LabelControl1.TabIndex = 18
        Me.LabelControl1.Text = "TOKEN:"
        '
        'teToken
        '
        Me.teToken.EditValue = ""
        Me.teToken.EnterMoveNextControl = True
        Me.teToken.Location = New System.Drawing.Point(153, 12)
        Me.teToken.Name = "teToken"
        Me.teToken.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.teToken.Properties.Appearance.Options.UseFont = True
        Me.teToken.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.teToken.Size = New System.Drawing.Size(243, 52)
        Me.teToken.TabIndex = 17
        '
        'frmValidaToken
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(406, 112)
        Me.ControlBox = False
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.teToken)
        Me.Controls.Add(Me.sbCancel)
        Me.Controls.Add(Me.sbOk)
        Me.Name = "frmValidaToken"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Validar"
        CType(Me.teToken.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents sbCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents sbOk As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teToken As DevExpress.XtraEditors.TextEdit
End Class
