﻿Module ModuleErrores
    Public Sub TrapError(ByVal ex As Exception)

        If IsNothing(ex) Then
            MsgBox("El registro fue actualizado con exito", MsgBoxStyle.Information, "Nota")
            Exit Sub
        End If

        Select Case ex.GetType.Name
            Case "SqlException"
                MsgBox("ERROR: " + TrapSqlError(ex), MsgBoxStyle.Critical, "Error")
            Case Else
                MsgBox("ERROR: " + ex.Message, MsgBoxStyle.Critical, "Error")
        End Select

    End Sub

    Private Function TrapSqlError(ByVal ex As Exception) As String
        Dim errText As String = ""
        Select Case CType(ex, SqlClient.SqlException).Number
            Case 547
                errText = "No es posible eliminar este registro " & _
                       Chr(13) & "mientras existan dependencias a otros registros"
            Case 2627
                errText = "El registro que pretende agregar ya existe"
            Case Else
                errText = ex.Message
        End Select
        Return errText
    End Function
End Module
