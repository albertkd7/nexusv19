<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class beCtaContable
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.beIdCuenta = New DevExpress.XtraEditors.ButtonEdit
        Me.teNombreCuenta = New DevExpress.XtraEditors.TextEdit
        CType(Me.beIdCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombreCuenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'beIdCuenta
        '
        Me.beIdCuenta.EnterMoveNextControl = True
        Me.beIdCuenta.Location = New System.Drawing.Point(3, 0)
        Me.beIdCuenta.Name = "beIdCuenta"
        Me.beIdCuenta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beIdCuenta.Size = New System.Drawing.Size(170, 20)
        Me.beIdCuenta.TabIndex = 0
        '
        'teNombreCuenta
        '
        Me.teNombreCuenta.EnterMoveNextControl = True
        Me.teNombreCuenta.Location = New System.Drawing.Point(175, 0)
        Me.teNombreCuenta.Name = "teNombreCuenta"
        Me.teNombreCuenta.Properties.AllowFocused = False
        Me.teNombreCuenta.Properties.ReadOnly = True
        Me.teNombreCuenta.Size = New System.Drawing.Size(417, 20)
        Me.teNombreCuenta.TabIndex = 1
        '
        'beCtaContable
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.teNombreCuenta)
        Me.Controls.Add(Me.beIdCuenta)
        Me.Name = "beCtaContable"
        Me.Size = New System.Drawing.Size(594, 20)
        CType(Me.beIdCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombreCuenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents beIdCuenta As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents teNombreCuenta As DevExpress.XtraEditors.TextEdit

End Class
