Imports NexusELL.TableEntities
Public Class beProducto
    Private ElUsuarioDioClic = False
    Private Sub beCodigo_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCodigo.ButtonClick
        beCodigo.EditValue = ""
        ElUsuarioDioClic = True
        beCodigo_Validated(sender, New System.EventArgs)
    End Sub

    Private Sub beCodigo_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCodigo.Validated
        If Not ElUsuarioDioClic And beCodigo.EditValue = "" Then
            Exit Sub
        End If
        Dim ent As inv_Productos = objConsultas.ConsultaProductos2(inv_frmConsultaProductos, "", beCodigo.EditValue)
        ElUsuarioDioClic = False

        If ent.EstadoProducto > 1 Then
            MsgBox("El producto est� inactivo", MsgBoxStyle.Critical, "Nota")
            beCodigo.EditValue = ""
            Exit Sub
        End If
        beCodigo.EditValue = ent.IdProducto
        teNombre.EditValue = ent.Nombre
        teNombre.Focus()
    End Sub
End Class

