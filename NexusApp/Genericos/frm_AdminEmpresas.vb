﻿Imports System.IO
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports DevExpress.XtraSplashScreen

Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports System.Text
Imports System.Security.Cryptography
Imports System.Web.Script.Serialization
Imports System.Configuration

Public Class frm_AdminEmpresas

#Region "[ PASO 1 ]"
    '<< Pagina 1 : Start >>
    Private Sub wAsisEmpPag1_PageValidating(sender As Object, e As DevExpress.XtraWizard.WizardPageValidatingEventArgs) Handles wAsisEmpPag1.PageValidating
        e.Valid = chkAcepto.Checked
        If e.Valid Then
            WizardControlGral.NextText = "Crear"
        End If
    End Sub
    '<< Pagina 1 : End >>
#End Region

#Region "[ PASO 2 ]"
    '<< Pagina 2 >>
    Private Sub chkCatalogo_CheckedChanged(sender As Object, e As EventArgs) Handles chkCatalogo.CheckedChanged
        With chkCatalogo
            'chkFormasPago.Enabled = .Checked
            'chkComprobantes.Enabled = .Checked
            chkProveedores.Enabled = .Checked
        End With
    End Sub

    Dim NewNameSchema As String = Nothing
    Dim NewUserSchema As String = Nothing
    Dim dbf As SqlDatabase = Nothing
    Dim dbs As SqlDatabase = Nothing
    Dim PageContinue As Boolean = True
    Dim LoginDB As Boolean = False

    Private Sub wAsistPag2_PageValidating(sender As Object, e As DevExpress.XtraWizard.WizardPageValidatingEventArgs) Handles wAsistPag2.PageValidating

        SplashScreenManager.ShowForm(Me, WaitForm.GetType(), True, True, False)
        SplashScreenManager.Default.SetWaitFormCaption("Procesando")
        SplashScreenManager.Default.SetWaitFormDescription("Verificando configuracion necesaria...")
        Me.Enabled = False

        If String.IsNullOrEmpty(txtNombreEmpresa.EditValue) _
            Or String.IsNullOrEmpty(txtNit.EditValue) _
            Or String.IsNullOrEmpty(txtNRC.EditValue) _
            Or String.IsNullOrEmpty(txtClave.EditValue) _
            Or String.IsNullOrEmpty(txtClaveConfirmado.EditValue) Then
            e.Valid = False
            SplashScreenManager.CloseForm()
            MessageBox.Show("Por favor completa todo la informacion solicitada **", "DATOS REQUERIDOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Enabled = True
            Exit Sub
        End If

        If txtClave.EditValue <> txtClaveConfirmado.EditValue Then
            e.Valid = False
            SplashScreenManager.CloseForm()
            MessageBox.Show("Las Claves ingresadas no coinciden", "DATOS REQUERIDOS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Enabled = True
            Exit Sub
        End If

        Dim NxSchemas As Boolean = IIf(ConfigurationManager.AppSettings("NxSchemas:Disabled") = "True", True, False)
        If Not NxSchemas Then
            e.Valid = False
            SplashScreenManager.CloseForm()
            MessageBox.Show("Imposible continuar, no tiene activa la KEY necesaria para continuar con la operacion", "FALLO EN CONFIGURACION", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.Enabled = True
            Exit Sub
        End If

        '''Establecer conexion a la base
        Dim dbx As Database = Nothing
        If NxSchemas = True Then
            Dim settings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings
            If Not settings Is Nothing Then
                For Each cs As ConnectionStringSettings In settings
                    If cs.Name = "MultiEmpresa" Then
                        dbx = DatabaseFactory.CreateDatabase(cs.Name)
                    End If
                Next
            End If
        End If

        If dbx Is Nothing Then
            e.Valid = False
            SplashScreenManager.CloseForm()
            MessageBox.Show("Imposible continuar, no se encuentra configurada la conexion bilateral para continuar con la operacion", "FALLO EN CONFIGURACION", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.Enabled = True
            Exit Sub
        End If

        Dim CNSx As String = DesencriptarCNS(dbx.ConnectionString)
        dbf = New SqlDatabase(CNSx)

        '''Verificar Schemas actuales
        Dim Ssql1 As String = " CREATE SCHEMA NX_DEV_CODE; "
        Dim Ssql2 As String = " DECLARE @IdNext INT = 0; " &
                              " SELECT @IdNext = schema_id FROM sys.schemas WHERE [name] = 'NX_DEV_CODE'; " &
                              " DROP SCHEMA NX_DEV_CODE; " &
                              " SELECT @IdNext; "

        dbf.ExecuteScalar(CommandType.Text, Ssql1)
        Dim NexPKidSchema As Integer = dbf.ExecuteScalar(CommandType.Text, Ssql2)
        Dim NameDataBaseT As String = dbf.ExecuteScalar(CommandType.Text, "select db_name()")
        NewNameSchema = "Nexus" & String.Format("{0:00}", NexPKidSchema)

        Try
            SplashScreenManager.Default.SetWaitFormDescription("Trabajando creacion de empresa...")
            '''Creacion de empresa schema...
            Dim ResultClone = (From x In (dbf.ExecuteDataSetWait("CloneLogicDB64", "dbo", NewNameSchema).Tables(0).AsEnumerable)
                               Select New With {
                                        Key .ErrorNumber = x.Field(Of Integer)("ErrorNumber"),
                                        Key .ErrorSeverity = x.Field(Of Integer)("ErrorSeverity"),
                                        Key .ErrorState = x.Field(Of Integer)("ErrorState"),
                                        Key .ErrorProcedure = x.Field(Of String)("ErrorProcedure"),
                                        Key .ErrorLine = x.Field(Of Integer)("ErrorLine"),
                                        Key .ErrorMessage = x.Field(Of String)("ErrorMessage")
                                     }
                                 ).FirstOrDefault()

            If ResultClone.ErrorNumber >= 0 Then
                e.Valid = False
                SplashScreenManager.CloseForm()
                MessageBox.Show("Contacte a su programador, para resolver el siguiente problema de integracion de sus datos y volver a continuar..." _
                                & vbCrLf & "ErrorNumber: " & ResultClone.ErrorNumber _
                                & vbCrLf & "ErrorSeverity: " & ResultClone.ErrorSeverity _
                                & vbCrLf & "ErrorState: " & ResultClone.ErrorState _
                                & vbCrLf & "ErrorProcedure: " & ResultClone.ErrorProcedure _
                                & vbCrLf & "ErrorLine: " & ResultClone.ErrorLine _
                                & vbCrLf & "ErrorMessage: " & ResultClone.ErrorMessage _
                                , "FALLO EN INTEGRIDAD DE DATOS", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Me.Enabled = True
                Exit Sub
            End If

            '''Creacion de Usuario para schema...
            SplashScreenManager.Default.SetWaitFormDescription("Vinculando usuario de acceso DB...")
            Dim StringConexionMaster As String = AccessMaster(dbf.ConnectionString)
            dbs = New SqlDatabase(StringConexionMaster)

            NewUserSchema = "Nx" & String.Format("{0:00}", NexPKidSchema) & "_" & NameDataBaseT
            dbs.ExecuteNonQuery(CommandType.Text, String.Format("IF (SELECT count(*) FROM sys.sql_logins WHERE [Name] = '{0}') > 0 DROP LOGIN {0}", NewUserSchema))
            dbs.ExecuteNonQuery(CommandType.Text, "CREATE LOGIN " & NewUserSchema & " WITH PASSWORD = 'Itosa08$'")

            dbf.ExecuteNonQuery(CommandType.Text, "CREATE USER  " & NewUserSchema & " FOR LOGIN " & NewUserSchema)
            dbf.ExecuteNonQuery(CommandType.Text, "ALTER USER " & NewUserSchema & " WITH DEFAULT_SCHEMA = " & NewNameSchema & "")
            dbf.ExecuteNonQuery(CommandType.Text, "ALTER ROLE db_owner ADD MEMBER " & NewUserSchema & "")

            '''incorporando informacion basica para schema...
            SplashScreenManager.Default.SetWaitFormDescription("Cargando configuracion basica...")
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.adm_parametros SELECT * FROM dbo.adm_parametros", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.adm_usuarios SELECT * FROM dbo.adm_usuarios WHERE idusuario = 'ADMINISTRADOR'", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.adm_modulos SELECT * FROM dbo.adm_modulos", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.adm_opcionesmenu SELECT * FROM dbo.adm_opcionesmenu", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.adm_opcionesusuarios SELECT * FROM dbo.adm_opcionesusuarios WHERE idusuario = 'ADMINISTRADOR'", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("UPDATE {0}.adm_Parametros SET NombreEmpresa = '{1}', NrcEmpresa = '{2}', NitEmpresa = '{3}' ", NewNameSchema, txtNombreEmpresa.EditValue, txtNRC.EditValue, txtNit.EditValue))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.adm_listas SELECT * FROM dbo.adm_listas", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.adm_departamentos SELECT * FROM dbo.adm_departamentos", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.adm_municipios SELECT * FROM dbo.adm_municipios", NewNameSchema))

            'Catalogo planillas
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.pla_EstadoCivil SELECT * FROM dbo.pla_EstadoCivil", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.pla_TiposPlanilla SELECT * FROM dbo.pla_TiposPlanilla", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.pla_Columnas SELECT * FROM dbo.pla_Columnas", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.pla_EstadoEmpleado SELECT * FROM dbo.pla_EstadoEmpleado", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.pla_CatalogoMovimientos SELECT * FROM dbo.pla_CatalogoMovimientos where IdMov <11", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.pla_Sectores SELECT * FROM dbo.pla_Sectores", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.pla_TablasRenta SELECT * FROM dbo.pla_TablasRenta", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.pla_TablasRentaDetalle SELECT * FROM dbo.pla_TablasRentaDetalle", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.pla_TipoAccionPersonal SELECT * FROM dbo.pla_TipoAccionPersonal", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.pla_TipoAplicacion SELECT * FROM dbo.pla_TipoAplicacion", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.pla_TipoContratos SELECT * FROM dbo.pla_TipoContratos", NewNameSchema))
            dbf.ExecuteNonQuery(CommandType.Text, String.Format("INSERT INTO {0}.pla_TablasAguinaldo SELECT * FROM dbo.pla_TablasAguinaldo", NewNameSchema))

            'NOTA: Modificacion por errores del usuario final: Terminos y Condiciones ignorados...
            If chkFormasPago.Checked Then
                dbf.ExecuteNonQueryWait(String.Format("INSERT INTO {0}.fac_formaspago SELECT * FROM dbo.fac_formaspago", NewNameSchema))
            End If
            If chkComprobantes.Checked Then
                dbf.ExecuteNonQueryWait(String.Format("INSERT INTO {0}.adm_TiposComprobante SELECT * FROM dbo.adm_TiposComprobante", NewNameSchema))
            End If

            If chkCatalogo.Checked Then
                dbf.ExecuteNonQueryWait(String.Format("INSERT INTO {0}.con_cuentas SELECT * FROM dbo.con_cuentas", NewNameSchema))
                'If chkFormasPago.Checked Then
                '    dbf.ExecuteNonQueryWait(String.Format("INSERT INTO {0}.fac_formaspago SELECT * FROM dbo.fac_formaspago", NewNameSchema))
                'End If
                'If chkComprobantes.Checked Then
                '    dbf.ExecuteNonQueryWait(String.Format("INSERT INTO {0}.adm_TiposComprobante SELECT * FROM dbo.adm_TiposComprobante", NewNameSchema))
                'End If
                If chkProveedores.Checked Then
                    dbf.ExecuteNonQueryWait(String.Format("INSERT INTO {0}.com_proveedores SELECT * FROM dbo.com_proveedores", NewNameSchema))
                End If
            End If

            If chkSucursales.Checked Then
                dbf.ExecuteNonQueryWait(String.Format("INSERT INTO {0}.adm_sucursales SELECT * FROM dbo.adm_sucursales", NewNameSchema))
            End If
            If chkBodegas.Checked Then
                dbf.ExecuteNonQueryWait(String.Format("INSERT INTO {0}.inv_bodegas SELECT * FROM dbo.inv_bodegas", NewNameSchema))
            End If

            '''Agregando a lista de empresas...
            SplashScreenManager.Default.SetWaitFormDescription("Agregando Empresa creada a lista...")
            dbf.ExecuteNonQuery(CommandType.Text, "INSERT INTO dbo.adm_empresas " &
                                                  "  SELECT Correlativo = (SELECT isnull(max(Correlativo), 0)+1 FROM dbo.adm_empresas) " &
                                                  "        ,Cns = 'cns" & String.Format("{0:00}", NexPKidSchema) & "' " &
                                                  "        ,Servidor = 'MYSERVER' " &
                                                  "        ,NombreDB = 'MYDB' " &
                                                  "        ,Usuario = '" & NewUserSchema & "' " &
                                                  "        ,Clave = 'Itosa08$' " &
                                                  "        ,Nombre = '" & txtNombreEmpresa.EditValue & "' " &
                                                  "        ,CreadoPor = '" & objMenu.User & "' " &
                                                  "        ,FechaHoraCreacion = getdate()")

            '''Configuracion nuevo usuario para login nexus...
            SplashScreenManager.Default.SetWaitFormDescription("Configurando usuario para inicio en Nexus...")
            dbf.ExecuteNonQueryWait(String.Format("update {0}.adm_usuarios set Clave = '{1}' where idusuario = 'ADMINISTRADOR'", NewNameSchema, CType(txtClaveConfirmado.EditValue, String).Encriptar()))

            '''THE END by Josue Diaz.
        Catch ex As Exception
            e.Valid = False
            Try
                If Not dbf Is Nothing And Not String.IsNullOrEmpty(NewNameSchema) Then
                    dbf.ExecuteDataSetWait("DropLogicDB64", NewNameSchema)
                End If
                If Not dbs Is Nothing And Not String.IsNullOrEmpty(NewUserSchema) Then
                    dbs.ExecuteNonQuery(CommandType.Text, String.Format("IF (SELECT count(*) FROM sys.sql_logins WHERE [Name] = '{0}') > 0 DROP LOGIN {0}", NewUserSchema))
                End If
            Catch exx As Exception
                'NULL REVERSION
            End Try
            SplashScreenManager.CloseForm()
            MessageBox.Show("Imposible continuar debido a: " & ex.Message, "FALLO EN OPERACION", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.Enabled = True
            Exit Sub
        End Try

        SplashScreenManager.Default.SetWaitFormCaption("Completado")
        SplashScreenManager.Default.SetWaitFormDescription("Tarea Finalizada con exito!")
        SplashScreenManager.CloseForm()
        Me.Enabled = True
    End Sub

#End Region

#Region "[ FUNCTIONS ]"
    Public Function AccessMaster(ByVal StringConexion As String, Optional namedb As String = "master")
        'Ejemplo de cadena:
        '( Data Source=tcp:jurosa.database.windows.net,1433;Initial Catalog=NEXUS_JURO;User ID=administrador@jurosa;Password=nexus123+ )
        Dim newcnx As String = String.Empty
        Dim sctemp As String = StringConexion.Replace(" ", "") _
                                             .ReplaceIgnoreCase("DataSource=", "") _
                                             .ReplaceIgnoreCase("InitialCatalog=", "") _
                                             .ReplaceIgnoreCase("UserID=", "") _
                                             .ReplaceIgnoreCase("Password=", "") _
                                             .ReplaceIgnoreCase("IntegratedSecurity=True", "")

        Dim RS = sctemp.Split(";")
        newcnx = String.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", RS(0), namedb, RS(2), RS(3))
        Return newcnx
    End Function

    Public Function DesencriptarCNS(ByVal Input As String) As String
        Dim cs As String = Input
        Try
            Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("NexusKey") 'La clave debe ser de 8 caracteres
            Dim EncryptionKey() As Byte = Convert.FromBase64String("rpaSPvIvVLlrcmtzPU9/c67Gkj7yL1S9") 'No se puede alterar la cantidad de caracteres pero si la clave
            Dim buffer() As Byte = Convert.FromBase64String(Input)
            Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
            des.Key = EncryptionKey
            des.IV = IV
            cs = Encoding.UTF8.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length()))
        Catch ex As Exception
        End Try
        Return cs
    End Function

    Private Sub wAsistPag2_Layout(sender As Object, e As LayoutEventArgs) Handles wAsistPag2.Layout
        wAsistPag2.AllowBack = False
    End Sub

    Private Sub CompletionWizardPageEnd_Layout(sender As Object, e As LayoutEventArgs) Handles CompletionWizardPageEnd.Layout
        CompletionWizardPageEnd.AllowBack = False
        CompletionWizardPageEnd.AllowCancel = False
    End Sub
#End Region

End Class