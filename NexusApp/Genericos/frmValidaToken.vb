﻿Imports NexusBLL
Imports NexusELL.TableEntities
Public Class frmValidaToken
    Private NumIntentos As Integer = 0
    Dim bl As AdmonBLL
    Dim blFac As New FacturaBLL(g_ConnectionString)

    Private _Usuario As String
    Public Property Usuario() As String
        Get
            Return _Usuario
        End Get
        Set(ByVal value As String)
            _Usuario = value
        End Set
    End Property

    Private _Accceso As Boolean
    Public Property Acceso() As Boolean
        Get
            Return _Accceso
        End Get
        Set(ByVal value As Boolean)
            _Accceso = value
        End Set
    End Property

    Private _Descuento As System.Decimal
    Public Property Descuento() As System.Decimal
        Get
            Return _Descuento
        End Get
        Set(ByVal value As System.Decimal)
            _Descuento = value
        End Set
    End Property

    Private _DescuentoTK As System.Decimal
    Public Property DescuentoTK() As System.Decimal
        Get
            Return _DescuentoTK
        End Get
        Set(ByVal value As System.Decimal)
            _DescuentoTK = value
        End Set
    End Property

    Private _Solicitud As System.Boolean
    Public Property Solicitud() As System.Boolean
        Get
            Return _Solicitud
        End Get
        Set(ByVal value As System.Boolean)
            _Solicitud = value
        End Set
    End Property

    Private _IdProducto As String
    Public Property IdProducto() As String
        Get
            Return _IdProducto
        End Get
        Set(ByVal value As String)
            _IdProducto = value
        End Set
    End Property

    Private _Fecha As Nullable(Of DateTime)
    Public Property Fecha() As Nullable(Of DateTime)
        Get
            Return _Fecha
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _Fecha = value
        End Set
    End Property

    Private Sub sbOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbOk.Click
        Dim msj As String = blFac.fac_ValidaToken(teToken.EditValue, Descuento, Fecha, piIdSucursalUsuario, IdProducto)
        If msj <> "Ok" Then
            MsgBox(msj, MsgBoxStyle.Critical)
            Acceso = False
            Return
        Else
            Acceso = True
            DescuentoTK = blFac.fac_DescuentoToken(teToken.EditValue)
            Close()
        End If
    End Sub

    Private Sub sbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sbCancel.Click
        Acceso = False
        Close()
    End Sub

    Private Sub frmObtienePassword_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        teToken.Focus()
    End Sub

    Private Sub frmObtienePassword_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        teToken.EditValue = ""
        Acceso = False
    End Sub
End Class