<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class beProducto
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.beCodigo = New DevExpress.XtraEditors.ButtonEdit
        Me.teNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblProducto = New DevExpress.XtraEditors.LabelControl
        CType(Me.beCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'beCodigo
        '
        Me.beCodigo.EnterMoveNextControl = True
        Me.beCodigo.Location = New System.Drawing.Point(56, 3)
        Me.beCodigo.Name = "beCodigo"
        Me.beCodigo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beCodigo.Size = New System.Drawing.Size(155, 20)
        Me.beCodigo.TabIndex = 0
        '
        'teNombre
        '
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(217, 3)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.ReadOnly = True
        Me.teNombre.Size = New System.Drawing.Size(389, 20)
        Me.teNombre.TabIndex = 1
        '
        'lblProducto
        '
        Me.lblProducto.Location = New System.Drawing.Point(6, 6)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(47, 13)
        Me.lblProducto.TabIndex = 2
        Me.lblProducto.Text = "Producto:"
        '
        'beProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblProducto)
        Me.Controls.Add(Me.teNombre)
        Me.Controls.Add(Me.beCodigo)
        Me.Name = "beProducto"
        Me.Size = New System.Drawing.Size(612, 25)
        CType(Me.beCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents beCodigo As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblProducto As DevExpress.XtraEditors.LabelControl

End Class
