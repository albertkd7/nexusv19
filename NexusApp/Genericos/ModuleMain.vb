﻿Imports System.IO
Imports System.Drawing.Printing
Imports System.Runtime.InteropServices
Imports Microsoft.Office.Interop
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Text
Imports System.Net.Mail
Imports System.Text.RegularExpressions


Module ModuleMain

    Public objMenu As NexusBLL.MenusBLL
    Public objTablas As NexusBLL.TableBusiness
    Public objFunciones As NexusBLL.FuncionesBLL
    Public objCombos As NexusBLL.CombosBLL
    Public objConsultas As NexusBLL.ConsultasBLL

    'declaracion de variables globales y constantes
    Public pnIVA As Decimal = 0.0
    Public gsNombre_Empresa As String = "SU EMPRESA, S.A. DE C.V."
    Public gsDesc_Moneda As String = "VALORES EXPRESADOS EN DÓLARES DE LOS ESTADOS UNIDOS DE AMÉRICA"
    Public piIdSucursal As Int32 = 1
    Public piIdSucursalUsuario As Int32 = 1
    Public piIdBodega As Int32 = 1
    Public piIdVendedor As Int32 = 0
    Public g_ConnectionString As String

    Public Enum DbModeType
        insert = 0
        update = 1
    End Enum


    Sub main()


        DevExpress.UserSkins.BonusSkins.Register()

        Application.Run(New adm_frmLogin)

        Application.Run(New NexusMain)

        DestruirObjetos()
    End Sub

    Public Sub CrearObjetos()
        objMenu = New NexusBLL.MenusBLL(g_ConnectionString)
        objTablas = New NexusBLL.TableBusiness(g_ConnectionString)
        objFunciones = New NexusBLL.FuncionesBLL(g_ConnectionString)
        objCombos = New NexusBLL.CombosBLL(g_ConnectionString)
        objConsultas = New NexusBLL.ConsultasBLL(g_ConnectionString)
    End Sub
    Public Function EnviarCorreo(ByVal Mensaje As StringBuilder, ByVal Correo As String, ByVal Asunto As String) As String
        Dim bl As New NexusBLL.AdmonBLL(g_ConnectionString)
        Dim dtParam As DataTable = bl.ObtieneParametros()
        Dim _Message As New MailMessage()
        Dim _SMTP As New System.Net.Mail.SmtpClient

        ''CONFIGURACIÓN DEL SMTP
        _SMTP.Credentials = New System.Net.NetworkCredential(dtParam.Rows(0).Item("EmailNexus").ToString(), dtParam.Rows(0).Item("EmailPasswordNexus").ToString())
        _SMTP.Port = dtParam.Rows(0).Item("PortEmailNexus")
        _SMTP.Host = dtParam.Rows(0).Item("ServerEmailNexus")
        _SMTP.EnableSsl = dtParam.Rows(0).Item("SslNexus")


        ''CONFIGURACIÓN DEL MENSAJE
        _Message.[To].Add(Correo)
        _Message.From = New System.Net.Mail.MailAddress(dtParam.Rows(0).Item("EmailNexus"), "COMPROBANTES DE RETENCIÓN PENDIENTES", System.Text.Encoding.UTF8)
        _Message.Subject = Asunto
        _Message.SubjectEncoding = System.Text.Encoding.UTF8
        _Message.Body = Mensaje.ToString()
        _Message.IsBodyHtml = True
        _Message.BodyEncoding = System.Text.Encoding.UTF8
        _Message.Priority = System.Net.Mail.MailPriority.Normal

        ''ENVIO DE CORREO
        Try
            _SMTP.Send(_Message)
            Return ""
        Catch ex As System.Net.Mail.SmtpException
            MessageBox.Show(ex.ToString, "Error!", MessageBoxButtons.OK)
            Return ex.ToString
        End Try
    End Function

    Private Sub DestruirObjetos()
        objMenu = Nothing
        objTablas = Nothing
        objFunciones = Nothing
        objCombos = Nothing
        objConsultas = Nothing
    End Sub

    Public Sub CrearVariablesGlobales()
        ' Dim bl As New NexusBLL.ContabilidadBLL(g_ConnectionString)
        Dim bl As New NexusBLL.AdmonBLL(g_ConnectionString)

        '-- defino la fecha de cierre 
        Dim dt As DataTable = bl.ObtieneParametros()

        With dt.Rows(0)
            gsNombre_Empresa = .Item("NombreEmpresa")
            pnIVA = .Item("PorcIVA") / 100
            piIdBodega = .Item("IdBodega")
            piIdSucursal = .Item("IdSucursal")
            gsDesc_Moneda = .Item("DescMoneda")
        End With
    End Sub
    Public Function ValidarFechaCierre(ByVal Fecha As Date) As Boolean
        Dim myBL As New NexusBLL.AdmonBLL(g_ConnectionString)
        Dim dt As DataTable = myBL.ObtieneFechaCierre
        If Fecha < dt.Rows(0).Item("FechaCierre") OrElse Fecha > dt.Rows(0).Item("FechaLimite") Then
            Return False
        End If
        Return True
    End Function
    Public Function SiEsNulo(ByVal value As Object, ByVal valueNoNull As Object) As Object
        If value Is Nothing Or IsDBNull(value) Then Return valueNoNull
        Return value
    End Function

    Public Sub ExportToExcel(ByVal objDT As DataTable)
        Dim Excel As Object = CreateObject("Excel.Application")

        If Excel Is Nothing Then
            MsgBox("No se encuentra instalado Excel en éste equipo, es necesario para generar el archivo a Excel", MsgBoxStyle.Critical)
            Return
        End If
        Dim strFilename As String
        Dim intCol, intRow As Integer
        Dim strPath As String = "c:\"

        Try
            With Excel
                .SheetsInNewWorkbook = 1

                .Workbooks.Add()
                .Worksheets(1).Select()

                .cells(1, 1).value = "Encabezado" 'Heading of the excel file
                .cells(1, 1).EntireRow.Font.Bold = True

                Dim intI As Integer = 1
                For intCol = 0 To objDT.Columns.Count - 1
                    .cells(2, intI).value = objDT.Columns(intCol).ColumnName
                    .cells(2, intI).EntireRow.Font.Bold = True
                    intI += 1
                Next
                intI = 3
                Dim intK As Integer = 1
                For intCol = 0 To objDT.Columns.Count - 1
                    intI = 3
                    For intRow = 0 To objDT.Rows.Count - 1
                        .Cells(intI, intK).Value = objDT.Rows(intRow).ItemArray(intCol)
                        intI += 1
                    Next
                    intK += 1
                Next
                If Mid$(strPath, strPath.Length, 1) <> "\" Then
                    strPath = strPath & "\"
                End If
                strFilename = strPath & "Excel.xls"
                .ActiveCell.Worksheet.SaveAs(strFilename)
            End With
            System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            Excel = Nothing
            MsgBox("El archivo ha sido exportado a Excel en '" & strFilename & "'", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        ' The excel is created and opened for insert value. We most close this excel using this system
        Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")
        For Each i As Process In pro
            i.Kill()
        Next
    End Sub


    Public Sub DataTableToExcel(ByVal pDataTable As DataTable, ByVal File As String)
        Dim fbd As New FolderBrowserDialog
        fbd.ShowDialog()
        If fbd.SelectedPath = "" Then
            Return
        End If
        Dim NombreArchivo As String = InputBox("Nombre del archivo", "Caption", File)
        If NombreArchivo = "" Then
            Return
        End If
        Dim vFileName As String = fbd.SelectedPath & "\" & NombreArchivo & ".csv"
        Dim vFileExcel As String = fbd.SelectedPath & "\" & NombreArchivo & ".xls"
        'Path.GetTempFileName()
        FileOpen(1, vFileName, OpenMode.Output)
        Dim sb As String = ""
        Dim dc As DataColumn
        For Each dc In pDataTable.Columns
            sb &= "," 'dc.Caption & Microsoft.VisualBasic.ControlChars.Tab
        Next
        PrintLine(1, sb)
        Dim i As Integer = 0
        Dim dr As DataRow
        For Each dr In pDataTable.Rows
            i = 0 : sb = ""
            For Each dc In pDataTable.Columns
                If IsDBNull(dr(i)) Then
                    sb &= "," 'Microsoft.VisualBasic.ControlChars.Tab
                Else
                    If TypeOf (dr(i)) Is String AndAlso dr(i).IndexOf(",") > 0 Then
                        sb &= Chr(34) & CStr(dr(i)) & Chr(34) & ","
                    Else
                        sb &= CStr(dr(i)) & "," 'Microsoft.VisualBasic.ControlChars.Tab
                    End If
                End If
                i += 1
            Next
            PrintLine(1, sb)
        Next
        FileClose(1)
        'TextToExcel(vFileName, vFileExcel)
        MsgBox("El archivo ha sido guardado con éxito", MsgBoxStyle.Information)
    End Sub

    Public Sub TextToExcel(ByVal pFileName As String, ByVal pFileNameExcel As String)
        Dim Valor As Integer
        Dim vFormato As Excel.XlRangeAutoFormat
        Dim vCultura As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture()
        'Es importante definirle la cultura al sistema
        'ya que podria generar errores
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("es-SV")
        Dim Exc As Excel.Application = New Excel.Application

        Exc.Workbooks.OpenText(pFileName, , , , Excel.XlTextQualifier.xlTextQualifierNone, , True)

        Dim Wb As Excel.Workbook = Exc.ActiveWorkbook
        Dim Ws As Excel.Worksheet = Wb.ActiveSheet
        vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatSimple

        'En el ejemplo vienen otros formatos posibles
        Ws.Range(Ws.Cells(1, 1), Ws.Cells(Ws.UsedRange.Rows.Count, Ws.UsedRange.Columns.Count)).AutoFormat(vFormato)
        Exc.ActiveWorkbook.SaveAs(pFileNameExcel, Excel.XlTextQualifier.xlTextQualifierNone - 1)
        Exc.Quit()
        Ws = Nothing
        Wb = Nothing
        Exc = Nothing
        GC.Collect()
        If Valor > -1 Then
            Dim p As System.Diagnostics.Process = New System.Diagnostics.Process
            p.EnableRaisingEvents = False
        End If
        System.Threading.Thread.CurrentThread.CurrentCulture = vCultura
        File.Delete(pFileName)
    End Sub

    Public Function Num2Text(ByVal value As Double) As String
        Select Case value
            Case 0 : Num2Text = "CERO"
            Case 1 : Num2Text = "UN"
            Case 2 : Num2Text = "DOS"
            Case 3 : Num2Text = "TRES"
            Case 4 : Num2Text = "CUATRO"
            Case 5 : Num2Text = "CINCO"
            Case 6 : Num2Text = "SEIS"
            Case 7 : Num2Text = "SIETE"
            Case 8 : Num2Text = "OCHO"
            Case 9 : Num2Text = "NUEVE"
            Case 10 : Num2Text = "DIEZ"
            Case 11 : Num2Text = "ONCE"
            Case 12 : Num2Text = "DOCE"
            Case 13 : Num2Text = "TRECE"
            Case 14 : Num2Text = "CATORCE"
            Case 15 : Num2Text = "QUINCE"
            Case Is < 20 : Num2Text = "DIECI" & Num2Text(value - 10)
            Case 20 : Num2Text = "VEINTE"
            Case Is < 30 : Num2Text = "VEINTI" & Num2Text(value - 20)
            Case 30 : Num2Text = "TREINTA"
            Case 40 : Num2Text = "CUARENTA"
            Case 50 : Num2Text = "CINCUENTA"
            Case 60 : Num2Text = "SESENTA"
            Case 70 : Num2Text = "SETENTA"
            Case 80 : Num2Text = "OCHENTA"
            Case 90 : Num2Text = "NOVENTA"
            Case Is < 100 : Num2Text = Num2Text(Int(value \ 10) * 10) & " Y " & Num2Text(value Mod 10)
            Case 100 : Num2Text = "CIEN"
            Case Is < 200 : Num2Text = "CIENTO " & Num2Text(value - 100)
            Case 200, 300, 400, 600, 800 : Num2Text = Num2Text(Int(value \ 100)) & "CIENTOS"
            Case 500 : Num2Text = "QUINIENTOS"
            Case 700 : Num2Text = "SETECIENTOS"
            Case 900 : Num2Text = "NOVECIENTOS"
            Case Is < 1000 : Num2Text = Num2Text(Int(value \ 100) * 100) & " " & Num2Text(value Mod 100)
            Case 1000 : Num2Text = "MIL"
            Case Is < 2000 : Num2Text = "UN MIL " & Num2Text(value Mod 1000)
            Case Is < 1000000 : Num2Text = Num2Text(Int(value \ 1000)) & " MIL"
                If value Mod 1000 Then Num2Text = Num2Text & " " & Num2Text(value Mod 1000)
            Case 1000000 : Num2Text = "UN MILLON"
            Case Is < 2000000 : Num2Text = "UN MILLON " & Num2Text(value Mod 1000000)
            Case Is < 1000000000000.0# : Num2Text = Num2Text(Int(value / 1000000)) & " MILLONES "
                If (value - Int(value / 1000000) * 1000000) Then Num2Text = Num2Text & " " & Num2Text(value - Int(value / 1000000) * 1000000)
            Case 1000000000000.0# : Num2Text = "UN BILLON"
            Case Is < 2000000000000.0# : Num2Text = "UN BILLON " & Num2Text(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
            Case Else : Num2Text = Num2Text(Int(value / 1000000000000.0#)) & " BILLONES"
                If (value - Int(value / 1000000000000.0#) * 1000000000000.0#) Then Num2Text = Num2Text & " " & Num2Text(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
        End Select
    End Function

    Public Function FechaToString(ByVal DesdeFecha As Date, ByVal HastaFecha As Date) As String
        If HastaFecha < DesdeFecha Then
            HastaFecha = DesdeFecha
        End If
        Dim sMesI As String = ObtieneMesString(DesdeFecha.Month)
        Dim sMesF As String = ObtieneMesString(HastaFecha.Month)
        Dim sAnioI As String = DesdeFecha.Year
        Dim sAnioF As String = HastaFecha.Year

        Dim sDiaI As String = DesdeFecha.Day
        Dim sDiaF As String = HastaFecha.Day
        Dim sStringDate As String = ""
        If DesdeFecha = HastaFecha Then
            sStringDate = sDiaI + " de " + sMesI + " de " + sAnioI
        Else
            If DesdeFecha.Month = HastaFecha.Month Then
                If DesdeFecha.Year = HastaFecha.Year Then
                    sStringDate = "del " + sDiaI + " al " + sDiaF + " de " + sMesI + " de " + sAnioI
                Else
                    sStringDate = "del " + sDiaI + " de " + sMesI + " de " + sAnioI + " al " + sDiaF + " de " + sMesF + " de " + sAnioF
                End If
            Else
                If sAnioI = sAnioF Then
                    sStringDate = "del " + sDiaI + " de " + sMesI + " al " + sDiaF + " de " + sMesF + " de " + sAnioI
                Else
                    sStringDate = "del " + sDiaI + " de " + sMesI + " de " + sAnioI + " al " + sDiaF + " de " + sMesF + " de " + sAnioF
                End If
            End If
        End If

        Return sStringDate

    End Function

    Public Function ObtieneMesString(ByVal value As Integer) As String
        Dim sMes As String = ""
        Select Case value
            Case 1 : sMes = "Enero"
            Case 2 : sMes = "Febrero"
            Case 3 : sMes = "Marzo"
            Case 4 : sMes = "Abril"
            Case 5 : sMes = "Mayo"
            Case 6 : sMes = "Junio"
            Case 7 : sMes = "Julio"
            Case 8 : sMes = "Agosto"
            Case 9 : sMes = "Septiembre"
            Case 10 : sMes = "Octubre"
            Case 11 : sMes = "Noviembre"
            Case 12 : sMes = "Diciembre"
        End Select
        Return sMes
    End Function

    Public Function ImagenToByte(ByVal img As Image) As Byte()
        Dim ms As New IO.MemoryStream
        img.Save(ms, img.RawFormat)
        ImagenToByte = ms.GetBuffer
    End Function

    Public Function ByteToImagen(ByVal b As Byte()) As Image
        Dim ms As New IO.MemoryStream(b)
        Dim img As Image = Image.FromStream(ms)
        ByteToImagen = img
    End Function

    Public Class RawPrinterHelper
        ' Structure and API declarions:
        <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Unicode)> _
        Structure DOCINFOW
            <MarshalAs(UnmanagedType.LPWStr)> Public pDocName As String
            <MarshalAs(UnmanagedType.LPWStr)> Public pOutputFile As String
            <MarshalAs(UnmanagedType.LPWStr)> Public pDataType As String
        End Structure

        <DllImport("winspool.Drv", EntryPoint:="OpenPrinterW", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function OpenPrinter(ByVal src As String, ByRef hPrinter As IntPtr, ByVal pd As Long) As Boolean
        End Function
        <DllImport("winspool.Drv", EntryPoint:="ClosePrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ClosePrinter(ByVal hPrinter As IntPtr) As Boolean
        End Function
        '[DllImport("winspool.Drv", EntryPoint="ClosePrinter", SetLastError=true, ExactSpelling=true, CallingConvention=CallingConvention.StdCall)]
        ' public static extern bool ClosePrinter(IntPtr hPrinter);

        <DllImport("winspool.Drv", EntryPoint:="StartDocPrinterW", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function StartDocPrinter(ByVal hPrinter As IntPtr, ByVal level As Int32, ByRef pDI As DOCINFOW) As Boolean
        End Function
        <DllImport("winspool.Drv", EntryPoint:="EndDocPrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function EndDocPrinter(ByVal hPrinter As IntPtr) As Boolean
        End Function
        <DllImport("winspool.Drv", EntryPoint:="StartPagePrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function StartPagePrinter(ByVal hPrinter As IntPtr) As Boolean
        End Function
        <DllImport("winspool.Drv", EntryPoint:="EndPagePrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function EndPagePrinter(ByVal hPrinter As IntPtr) As Boolean
        End Function
        <DllImport("winspool.Drv", EntryPoint:="WritePrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function WritePrinter(ByVal hPrinter As IntPtr, ByVal pBytes As IntPtr, ByVal dwCount As Int32, ByRef dwWritten As Int32) As Boolean
        End Function

        ' SendBytesToPrinter()
        ' When the function is given a printer name and an unmanaged array of  
        ' bytes, the function sends those bytes to the print queue.
        ' Returns True on success or False on failure.
        Public Shared Function SendBytesToPrinter(ByVal szPrinterName As String, ByVal pBytes As IntPtr, ByVal dwCount As Int32) As Boolean
            Dim hPrinter As New IntPtr(0)      ' The printer handle.
            Dim dwError As Int32 = 0        ' Last error - in case there was trouble.
            Dim di As New DOCINFOW      ' Describes your document (name, port, data type).
            Dim dwWritten As Int32 = 0     ' The number of bytes written by WritePrinter().
            Dim bSuccess As Boolean     ' Your success code.

            ' Set up the DOCINFO structure.
            'With di
            di.pDocName = "RAW Document"
            di.pDataType = "RAW"

            'End With
            ' Assume failure unless you specifically succeed.
            bSuccess = False
            If OpenPrinter(szPrinterName.Normalize(), hPrinter, IntPtr.Zero) Then
                If StartDocPrinter(hPrinter, 1, di) Then
                    If StartPagePrinter(hPrinter) Then
                        ' Write your printer-specific bytes to the printer.
                        bSuccess = WritePrinter(hPrinter, pBytes, dwCount, dwWritten)
                        EndPagePrinter(hPrinter)
                    End If
                    EndDocPrinter(hPrinter)
                End If
                ClosePrinter(hPrinter)
            End If
            '    if( OpenPrinter( szPrinterName.Normalize(), out hPrinter, IntPtr.Zero ) )
            '{
            '    // Start a document.
            '        If (StartDocPrinter(hPrinter, 1, di)) Then
            '    {
            '        // Start a page.
            '            If (StartPagePrinter(hPrinter)) Then
            '        {
            '            // Write your bytes.
            '            bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
            '            EndPagePrinter(hPrinter);
            '        }
            '        EndDocPrinter(hPrinter);
            '    }
            '    ClosePrinter(hPrinter);
            ' If you did not succeed, GetLastError may give more information
            ' about why not.
            If bSuccess = False Then
                dwError = Marshal.GetLastWin32Error()
            End If
            Return bSuccess
        End Function ' SendBytesToPrinter()

        ' SendFileToPrinter()
        ' When the function is given a file name and a printer name, 
        ' the function reads the contents of the file and sends the
        ' contents to the printer.
        ' Presumes that the file contains printer-ready data.
        ' Shows how to use the SendBytesToPrinter function.
        ' Returns True on success or False on failure.
        Public Shared Function SendFileToPrinter(ByVal szPrinterName As String, ByVal szFileName As String) As Boolean
            ' Open the file.
            Dim fs As New FileStream(szFileName, FileMode.Open)
            ' Create a BinaryReader on the file.
            Dim br As New BinaryReader(fs)
            ' Dim an array of bytes large enough to hold the file's contents.
            Dim bytes(fs.Length) As Byte
            Dim bSuccess As Boolean
            ' Your unmanaged pointer.
            Dim pUnmanagedBytes As IntPtr

            ' Read the contents of the file into the array.
            bytes = br.ReadBytes(fs.Length)
            ' Allocate some unmanaged memory for those bytes.
            pUnmanagedBytes = Marshal.AllocCoTaskMem(fs.Length)
            ' Copy the managed byte array into the unmanaged array.
            Marshal.Copy(bytes, 0, pUnmanagedBytes, fs.Length)
            ' Send the unmanaged bytes to the printer.
            bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, fs.Length)
            ' Free the unmanaged memory that you allocated earlier.
            Marshal.FreeCoTaskMem(pUnmanagedBytes)
            Return bSuccess
        End Function ' SendFileToPrinter()

        ' When the function is given a string and a printer name,
        ' the function sends the string to the printer as raw bytes.
        Public Shared Function SendStringToPrinter(ByVal szPrinterName As String, ByVal szString As String)
            Dim pBytes As IntPtr
            Dim dwCount As Int32
            ' How many characters are in the string?
            dwCount = szString.Length()
            ' Assume that the printer is expecting ANSI text, and then convert
            ' the string to ANSI text.
            pBytes = Marshal.StringToCoTaskMemAnsi(szString)
            ' Send the converted ANSI string to the printer.
            SendBytesToPrinter(szPrinterName, pBytes, dwCount)
            Marshal.FreeCoTaskMem(pBytes)
            Return True
        End Function

    End Class
    <DllImport("Plantillas\Dbd32.dll", EntryPoint:="Initialize", CharSet:=CharSet.Unicode)> _
    Public Function Initialize32(ByVal ask As Boolean) As Integer
    End Function
    <DllImport("Plantillas\Dbd32.dll", EntryPoint:="AddPassword", CharSet:=CharSet.Unicode)> _
    Public Function AddPassword32(ByVal db As String, ByVal Pass As String) As Integer
    End Function
    <DllImport("Plantillas\Dbd64.dll", EntryPoint:="Initialize", CharSet:=CharSet.Unicode)> _
    Public Function Initialize64(ByVal ask As Boolean) As Integer
    End Function
    <DllImport("Plantillas\Dbd64.dll", EntryPoint:="AddPassword", CharSet:=CharSet.Unicode)> _
    Public Function AddPassword64(ByVal db As String, ByVal Pass As String) As Integer
    End Function

#Region "Planilla"
    Public Function PrimeraQuincena(ByVal year As Integer, ByVal month As Integer) As Date
        Return CType("15/" & month & "/" & year, Date)
    End Function

    Public Function UltimoDIaMes(ByVal year As Integer, ByVal month As Integer) As Date
        Return CType("01/" & month & "/" & year, Date).AddMonths(1).AddDays(-1)
    End Function

    Public Sub IniGridNavigator(ByVal gc As DevExpress.XtraGrid.GridControl)
        With gc.EmbeddedNavigator
            .ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
            .Buttons.Append.Visible = True
            .Buttons.CancelEdit.Visible = True
            .Buttons.Edit.Visible = True
            .Buttons.EndEdit.Visible = True
            .Buttons.First.Visible = False
            .Buttons.Last.Visible = False
            .Buttons.Next.Visible = False
            .Buttons.NextPage.Visible = False
            .Buttons.Prev.Visible = False
            .Buttons.PrevPage.Visible = False
            .TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.None
        End With
    End Sub
#End Region
End Module