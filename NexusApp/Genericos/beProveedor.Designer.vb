<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class beProveedor
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.beCodigo = New DevExpress.XtraEditors.ButtonEdit
        Me.teNombre = New DevExpress.XtraEditors.TextEdit
        Me.lblProveedor = New DevExpress.XtraEditors.LabelControl
        CType(Me.beCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'beCodigo
        '
        Me.beCodigo.EnterMoveNextControl = True
        Me.beCodigo.Location = New System.Drawing.Point(61, 0)
        Me.beCodigo.Name = "beCodigo"
        Me.beCodigo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beCodigo.Size = New System.Drawing.Size(124, 20)
        Me.beCodigo.TabIndex = 1
        '
        'teNombre
        '
        Me.teNombre.Enabled = False
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(187, 0)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.ReadOnly = True
        Me.teNombre.Size = New System.Drawing.Size(410, 20)
        Me.teNombre.TabIndex = 2
        '
        'lblProveedor
        '
        Me.lblProveedor.Location = New System.Drawing.Point(4, 3)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(54, 13)
        Me.lblProveedor.TabIndex = 0
        Me.lblProveedor.Text = "Proveedor:"
        '
        'beProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblProveedor)
        Me.Controls.Add(Me.teNombre)
        Me.Controls.Add(Me.beCodigo)
        Me.Name = "beProveedor"
        Me.Size = New System.Drawing.Size(600, 20)
        CType(Me.beCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents beCodigo As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblProveedor As DevExpress.XtraEditors.LabelControl

End Class
