Imports DevExpress.XtraBars.Ribbon
Imports DevExpress.XtraBars
Imports DevExpress.XtraTabbedMdi
Public Class NexusMain
    Private _Timer As Timer
    Dim NxUpTime As Integer = 0
    Dim NxUpActive As Boolean = False
    Dim CuentaRegresiva As Boolean = False
    Dim Notificacion As New DevExpress.XtraBars.Alerter.AlertControl
    Dim NexusControlXml As New System.Xml.XmlDocument
    Dim xmlLocation As String = Application.StartupPath & "\nexus-control.xml"

    Public Sub New()
        ' Esta llamada es exigida por el dise�ador.
        InitializeComponent()
        _Timer = New Timer()
        _Timer.Interval = 1000
        AddHandler _Timer.Tick, AddressOf Timer_tick
        Notificacion.ShowPinButton = False
        Notificacion.FormDisplaySpeed = Alerter.AlertFormDisplaySpeed.Slow
        Notificacion.FormShowingEffect = Alerter.AlertFormShowingEffect.SlideHorizontal
    End Sub

    Private Sub Timer_tick(sender As Object, e As EventArgs)
        Try
            If System.IO.File.Exists(xmlLocation) Then
                NexusControlXml.Load(xmlLocation)
                Boolean.TryParse(NexusControlXml.SelectSingleNode("nexus").Attributes("active").Value, NxUpActive)

                If NxUpActive = True And CuentaRegresiva = False Then
                    Int32.TryParse(NexusControlXml.SelectSingleNode("nexus").Attributes("time").Value, NxUpTime)
                    CuentaRegresiva = True
                    Notificacion.Show(Me, "Nexus ERP: ACTUALIZACION EN PROCESO", NexusControlXml.SelectSingleNode("nexus").Attributes("message").Value, True)
                Else
                    CuentaRegresiva = NxUpActive
                    Me.Text = "Nexus ERP | " + gsNombre_Empresa
                End If

                If CuentaRegresiva = True And NxUpActive = True Then
                    If NxUpTime <= 0 Then
                        Dispose()
                        Close()
                    Else
                        Dim ts As TimeSpan = TimeSpan.FromMilliseconds(NxUpTime)
                        Me.Text = "Nexus ERP (Cerrar y Actualizar en " & String.Format("{0:00}", ts.Minutes) & ":" & String.Format("{0:00}", ts.Seconds) & " min. )"
                        NxUpTime -= 1000
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub NexusMain_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        RibbonControl.SelectedPage = Administracion
        CargaMenu()
        HideQuickBar()
        Me.Text += " | " + gsNombre_Empresa
        _Timer.Start()
    End Sub

    Public Sub CargaMenu()
        Dim Modulo As String, Opcion As String, dtOp As DataTable = objMenu.ObtenerOpcionesPorUsuario(objMenu.User)

        For Each Pagina As RibbonPage In RibbonControl.Pages
            Modulo = Pagina.Text
            Dim query = From q In dtOp.AsEnumerable() Where (q.Field(Of String)("NombreModulo")) = Modulo
            Pagina.Visible = query.Count > 0
            For Each grupo As RibbonPageGroup In Pagina.Groups
                Opcion = Mid(grupo.Name, 2)

                query = From q In dtOp.AsEnumerable() Where (q.Field(Of String)("NombreModulo")) = Modulo And (q.Field(Of String)("IdOpcion")) = Opcion
                grupo.Visible = query.Count > 0
                For i = 0 To grupo.ItemLinks.Count - 1
                    If TypeOf grupo.ItemLinks(i) Is BarSubItemLink Then 'para un sub-Menu
                        Dim Menu As BarSubItemLink = grupo.ItemLinks(i)
                        Opcion = Mid(Menu.Item.Name, 2)
                        query = From q In dtOp.AsEnumerable() Where (q.Field(Of String)("NombreModulo")) = Modulo And (q.Field(Of String)("IdOpcion")) = Opcion
                        If query.Count = 0 Then 'el bot�n del sub-menu
                            Menu.Item.Visibility = BarItemVisibility.Never
                        End If
                        For Each SubMenu As BarButtonItemLink In Menu.VisibleLinks  'las opciones dentro de un sub-menu
                            Opcion = Mid(SubMenu.Item.Name, 2)
                            query = From q In dtOp.AsEnumerable() Where (q.Field(Of String)("NombreModulo")) = Modulo And (q.Field(Of String)("IdOpcion")) = Opcion
                            SubMenu.Visible = query.Count > 0
                        Next
                    ElseIf TypeOf grupo.ItemLinks(i) Is BarButtonItemLink Then 'para los botones
                        Dim Boton As BarButtonItemLink = grupo.ItemLinks(i)
                        Opcion = Mid(Boton.Item.Name, 2)
                        query = From q In dtOp.AsEnumerable() Where (q.Field(Of String)("NombreModulo")) = Modulo And (q.Field(Of String)("IdOpcion")) = Opcion
                        If query.Count = 0 Then
                            Boton.Item.Visibility = BarItemVisibility.Never
                        End If
                    End If
                Next
            Next
        Next
    End Sub
    Public Sub HideQuickBar()
        qbDelete.Visibility = BarItemVisibility.Never
        qbEdit.Visibility = BarItemVisibility.Never
        qbFind.Visibility = BarItemVisibility.Never
        qbNew.Visibility = BarItemVisibility.Never
        qbReport.Visibility = BarItemVisibility.Never
        qbSave.Visibility = BarItemVisibility.Never
        qbUndo.Visibility = BarItemVisibility.Never
        'qbBack.Visibility = BarItemVisibility.Never
        'qbNext.Visibility = BarItemVisibility.Never
        qbRefresh.Visibility = BarItemVisibility.Never
    End Sub
    Private Sub qbNew_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles qbNew.ItemClick
        Dim obj As Object = Me.ActiveMdiChild
        obj.NuevoBase()
    End Sub
    Private Sub qbSave_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles qbSave.ItemClick
        Dim obj As Object = Me.ActiveMdiChild
        obj.GuardarBase()
    End Sub
    Private Sub qbEdit_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles qbEdit.ItemClick
        Dim obj As Object = Me.ActiveMdiChild
        obj.EditarBase()
    End Sub
    Private Sub qbDelete_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles qbDelete.ItemClick
        Dim obj As Object = ActiveMdiChild
        Try
            obj.EliminarBase()
        Catch ex As Exception
            MsgBox("NO FUE POSIBLE ELIMINAR" + ex.Message(), MsgBoxStyle.Critical, "Nota")
        End Try
    End Sub
    Private Sub qbUndo_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles qbUndo.ItemClick
        Dim obj As Object = Me.ActiveMdiChild
        obj.RevertirBase()
    End Sub
    Private Sub qbFind_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles qbFind.ItemClick
        Dim obj As Object = ActiveMdiChild
        obj.ConsultarBase()
    End Sub
    Private Sub qbReport_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles qbReport.ItemClick
        Dim obj As Object = ActiveMdiChild
        obj.ReporteBase()
    End Sub
    Private Sub qbRefresh_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles qbRefresh.ItemClick
        Dim obj As Object = Me.ActiveMdiChild
        obj.RefreshConsultaBase()
    End Sub
    'Private Sub qbBack_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles qbBack.ItemClick
    '    Dim obj As Object = ActiveMdiChild
    '    Try
    '        obj.CargaControles(-1)
    '    Catch ex As Exception
    '        MsgBox("No se ha configurado �ste proceso, consulte con I. T. O.", MsgBoxStyle.Information, "Nota")
    '    End Try
    'End Sub
    'Private Sub qbNext_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles qbNext.ItemClick
    '    Dim obj As Object = Me.ActiveMdiChild
    '    Try
    '        obj.CargaControles(1)
    '    Catch ex As Exception
    '        MsgBox("No se ha configurado �ste proceso, consulte con I. T. O.", MsgBoxStyle.Information, "Nota")
    '    End Try
    'End Sub
    Private Sub CargaForma(ByVal forma As Object)
        '    forma.MdiParent = Me
        '    forma.rbMenu = Me
        '    forma.Show()

        Dim form1 As XtraMdiTabPageCollection = Me.XtraTabbedMdiManager1.Pages
        Dim len As Integer = form1.Count()

        If len <> 0 Then
            For i = 0 To len - 1

                Dim f As Object = form1.Item(i).MdiChild
                If (f.Equals(forma)) Then
                    form1.Item(i).MdiChild.MdiParent = Me
                    form1.Item(i).MdiChild.Activate()
                    Exit Sub
                Else
                    forma.MdiParent = Me
                    forma.rbMenu = Me
                    forma.Show()
                End If

            Next
        Else
            forma.MdiParent = Me
            forma.rbMenu = Me
            forma.Show()
        End If

    End Sub

#Region "Administraci�n"
    Private Sub a001001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a001001.ItemClick
        CargaForma(adm_frmSeguridad)
    End Sub
    Private Sub a001002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a001002.ItemClick
        CargaForma(adm_frmSetup)
    End Sub
    Private Sub a001003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a001003.ItemClick
        CargaForma(adm_frmTiposComprobante)
    End Sub
    Private Sub a001006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a001006.ItemClick
        CargaForma(adm_frmPuntosVenta)
    End Sub
    Private Sub a001007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a001007.ItemClick
        CargaForma(inv_frmUnidadesMedida)
    End Sub
    Private Sub a001008_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a001008.ItemClick
        CargaForma(inv_frmUbicaciones)
    End Sub
    Private Sub a001009_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a001009.ItemClick
        CargaForma(inv_frmMarcas)
    End Sub
    Private Sub a001010_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a001010.ItemClick
        CargaForma(inv_frmBodegas)
    End Sub
    Private Sub a001011_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a001011.ItemClick
        CargaForma(adm_frmCambiarClave)
    End Sub

    Private Sub a001005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a001005.ItemClick
        CargaForma(adm_frmSucursales)
    End Sub
    Private Sub a001004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a001004.ItemClick
        CargaForma(adm_frmFormasPago)
    End Sub
#End Region

#Region "Contabilidad"

    Private Sub b001001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b001001.ItemClick
        CargaForma(con_frmCuentas)
    End Sub
    Private Sub b001002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b001002.ItemClick
        CargaForma(con_frmTiposPartida)
    End Sub
    'Private Sub b001003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b001003.ItemClick
    '    CargaForma(con_frmCentrosCosto)
    'End Sub
    Private Sub b001003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b001003.ItemClick
        CargaForma(con_frmCentrosMayor)
    End Sub
    'Private Sub b001005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
    '    CargaForma(con_frmEditorEstadosFinancierosNIIF)
    'End Sub
    Private Sub b001004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b001004.ItemClick
        CargaForma(con_frmFlujoEfectivo)
    End Sub
    Private Sub b002001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b002001.ItemClick
        CargaForma(con_frmPartidas)
    End Sub
    Private Sub b002002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b002002.ItemClick
        CargaForma(con_frmEliminaPartidas)
    End Sub
    Private Sub b002003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b002003.ItemClick
        CargaForma(con_frmRenumeraPartidas)
    End Sub
    Private Sub b002004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b002004.ItemClick
        CargaForma(con_frmCierreMes)
    End Sub
    Private Sub b002005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b002005.ItemClick
        CargaForma(con_frmRevierteMes)
    End Sub
    Private Sub b002006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b002006.ItemClick
        CargaForma(con_frmCrearPartidaCierre)
    End Sub
    Private Sub b002007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b002007.ItemClick
        CargaForma(con_frmGenerarArchivosICV)
    End Sub
    Private Sub b002009_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b002009.ItemClick
        CargaForma(con_frmRatiosFinancieros)
    End Sub
    Private Sub b003001001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001001.ItemClick
        CargaForma(con_frmPartidasReporte)
    End Sub
    Private Sub b003001002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001002.ItemClick
        CargaForma(con_frmConcentracionPartidas)
    End Sub
    Private Sub b003001003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001003.ItemClick
        CargaForma(con_frmConcentracionMayor)
    End Sub
    Private Sub b003001004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001004.ItemClick
        CargaForma(con_frmMovimientosCuenta)
    End Sub
    Private Sub b003001005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001005.ItemClick
        CargaForma(con_frmTotalesPartida)
    End Sub
    Private Sub b003001006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001006.ItemClick
        CargaForma(con_frmImprimirCatalogo)
    End Sub
    Private Sub b003001007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001007.ItemClick
        CargaForma(con_frmLibroDiario)
    End Sub
    Private Sub b003001008_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001008.ItemClick
        CargaForma(con_frmLibroMayor)
    End Sub
    Private Sub b003001009_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001009.ItemClick
        CargaForma(con_frmLibroDiarioMayor)
    End Sub
    Private Sub b003001010_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001010.ItemClick
        CargaForma(con_frmLibroAuxiliarMayor)
    End Sub
    Private Sub b003001011_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001011.ItemClick
        CargaForma(con_frmAuxiliarCentrosCosto)
    End Sub
    Private Sub b003001012_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001012.ItemClick
        CargaForma(con_frmSaldosCentro)
    End Sub
    Private Sub b003001013_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001013.ItemClick
        CargaForma(con_frmComparacion2Meses)
    End Sub
    Private Sub b003001014_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001014.ItemClick
        CargaForma(con_frmComparacionAnual)
    End Sub
    Private Sub b003001015_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003001015.ItemClick
        CargaForma(con_frmGenerador)
    End Sub

    Private Sub b003002001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003002001.ItemClick
        CargaForma(con_frmBalanceComprobCuenta)
    End Sub
    Private Sub b003002002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003002002.ItemClick
        CargaForma(con_frmBalanceComprobReporte)
    End Sub
    Private Sub b003002003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003002003.ItemClick
        CargaForma(con_frmBalanceGeneral)
    End Sub
    Private Sub b003002004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003002004.ItemClick
        CargaForma(con_frmEstadoResultados)
    End Sub
    Private Sub b003002005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003002005.ItemClick
        CargaForma(con_frmEstadoResultadosCentro)
    End Sub
    Private Sub b003002006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003002006.ItemClick
        CargaForma(con_frmAnexos)
    End Sub
    Private Sub b003002007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003002007.ItemClick
        CargaForma(con_frmBalanceGeneralNIIF)
    End Sub
    Private Sub b003002008_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003002008.ItemClick
        CargaForma(con_frmEstadoResultadoNIIF)
    End Sub
    Private Sub b003002009_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003002009.ItemClick
        CargaForma(con_frmFlujoEfectivoNIIF)
    End Sub
    Private Sub b003002010_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003002010.ItemClick
        CargaForma(con_frmCambioPatrimonio)
    End Sub
    Private Sub b003002011_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003002011.ItemClick
        CargaForma(con_frmBalanceComprobReporteConsulta)
    End Sub
    Private Sub b003002012_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b003002012.ItemClick
        CargaForma(con_frmRazonesFinancieras)
    End Sub
    'PRESUPUESTOS
    Private Sub b004001001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b004001001.ItemClick
        CargaForma(pre_frmDepartamentos)
    End Sub
    Private Sub b004001002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b004001002.ItemClick
        CargaForma(con_frmCentrosCosto)
    End Sub
    Private Sub b004002001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b004002001.ItemClick
        CargaForma(pre_frmPresupuestos)
    End Sub
    Private Sub b004003001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b004003001.ItemClick
        CargaForma(pre_frmPresupuestosGeneral)
    End Sub
    Private Sub b004003002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b004003002.ItemClick
        CargaForma(pre_frmPresupuestoCostosReales)
    End Sub
    Private Sub b004003003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b004003003.ItemClick
        CargaForma(pre_frmPresupuestoAnualSuc)
    End Sub
    Private Sub b004003004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b004003004.ItemClick
        CargaForma(pre_frmPresupuestoAnualDepto)
    End Sub
    Private Sub b004003005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b004003005.ItemClick
        CargaForma(pre_frmPresupuestosVsCuentas)
    End Sub

#End Region

#Region "Bancos"
    Private Sub c001001_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles c001001.ItemClick
        CargaForma(ban_frmTiposTransaccion)
    End Sub
    Private Sub c001002_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles c001002.ItemClick
        CargaForma(ban_frmCuentasBancarias)
    End Sub
    Private Sub c002001_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles c002001.ItemClick
        CargaForma(ban_frmCheques)
    End Sub
    Private Sub c002002_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles c002002.ItemClick
        CargaForma(ban_frmTransacciones)
    End Sub
    Private Sub c002003_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles c002003.ItemClick
        CargaForma(ban_frmAnulacionCheques)
    End Sub
    Private Sub c002004_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles c002004.ItemClick
        CargaForma(ban_frmPrepararConciliacion)
    End Sub
    Private Sub c002005_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles c002005.ItemClick
        CargaForma(ban_frmConciliacion)
    End Sub
    Private Sub c002006_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles c002006.ItemClick
        CargaForma(ban_frmAutorizarCheques)
    End Sub
    Private Sub c002007_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles c002007.ItemClick
        If gsNombre_Empresa.StartsWith("PITUTA") Then
            CargaForma(ban_frmContabilizar)
        Else
            MsgBox("Este proceso no est� activo para su versi�n de Nexus" & Chr(13) & "Consulte con el personal t�cnico de IT OUTSOURING", MsgBoxStyle.Information, "Nota")
        End If
    End Sub
    '--- REPORTES DE BANCOS
    Private Sub c003001_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles c003001.ItemClick
        CargaForma(ban_frmLibroBancos)
    End Sub
    Private Sub c003002_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles c003002.ItemClick
        CargaForma(ban_frmListadoCheques)
    End Sub
    Private Sub c003003_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles c003003.ItemClick
        CargaForma(ban_frmListadoTrans)
    End Sub
    Private Sub c003004_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles c003004.ItemClick
        CargaForma(ban_frmBloqueCheques)
    End Sub
    Private Sub c003005_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles c003005.ItemClick
        CargaForma(ban_frmDisponibilidad)
    End Sub
    Private Sub c003006_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles c003006.ItemClick
        CargaForma(ban_frmGenerador)
    End Sub
#End Region

#Region "Compras"
    Private Sub d001001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d001001.ItemClick
        CargaForma(com_frmProveedores)
    End Sub
    Private Sub d001002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d001002.ItemClick
        CargaForma(com_frmGastosImportacion)
    End Sub
    Private Sub d001003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d001003.ItemClick
        CargaForma(com_frmCajasChica)
    End Sub
    Private Sub d001004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d001004.ItemClick
        CargaForma(com_frmImpuestos)
    End Sub
    Private Sub d002001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d002001.ItemClick
        CargaForma(com_frmOrdenesCompra)
    End Sub
    Private Sub d002002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d002002.ItemClick
        CargaForma(com_frmCompras)
    End Sub
    Private Sub d002003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d002003.ItemClick
        CargaForma(com_frmImportaciones)
    End Sub

    Private Sub d002004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d002004.ItemClick
        CargaForma(com_frmContabilizar)
    End Sub
    Private Sub d002005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d002005.ItemClick
        CargaForma(com_frmRequisiciones)
    End Sub
    Private Sub d002006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d002006.ItemClick
        CargaForma(com_frmLiquidacion)
    End Sub
    Private Sub d002007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d002007.ItemClick
        CargaForma(com_frmRetenciones)
    End Sub
    Private Sub d002008_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d002008.ItemClick
        CargaForma(com_frmContabilizarLiquidacion)
    End Sub
    Private Sub d002009_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d002009.ItemClick
        CargaForma(com_frmAprobacionOrdenCompra)
    End Sub
    Private Sub d002010_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d002010.ItemClick
        CargaForma(com_frmContabilizarCajaChica)
    End Sub

    Private Sub d003001001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d003001001.ItemClick
        CargaForma(com_frmComprasPeriodoProveedor)
    End Sub
    Private Sub d003001002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d003001002.ItemClick
        CargaForma(com_frmComprasPeriodoProducto)
    End Sub
    Private Sub d003001003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d003001003.ItemClick
        CargaForma(com_frmComprasProveedorProd)
    End Sub
    Private Sub d003001004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d003001004.ItemClick
        CargaForma(com_frmComprasProductoProv)
    End Sub
    Private Sub d003001005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d003001005.ItemClick
        CargaForma(com_frmComprasTotalesProveedor)
    End Sub
    Private Sub d003001006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d003001006.ItemClick
        CargaForma(com_frmComprasTotalesProducto)
    End Sub
    Private Sub d003001007_ItemClick(sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d003001007.ItemClick
        CargaForma(com_frmGenerador)
    End Sub
    Private Sub d003001008_ItemClick(sender As Object, e As ItemClickEventArgs) Handles d003001008.ItemClick
        CargaForma(com_frmComprasExcluidos)
    End Sub
    Private Sub d003001009_ItemClick(sender As Object, e As ItemClickEventArgs) Handles d003001009.ItemClick
        CargaForma(com_frmLiquidacionCajaChica)
    End Sub

    Private Sub d003001010_ItemClick(sender As Object, e As ItemClickEventArgs) Handles d003001010.ItemClick
        CargaForma(com_frmReporteImportaciones)
    End Sub
    Private Sub d003001011_ItemClick(sender As Object, e As ItemClickEventArgs) Handles d003001011.ItemClick
        CargaForma(com_frmComprasRetenciones)
    End Sub
    Private Sub d003002001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d003002001.ItemClick
        CargaForma(com_frmLibroCompras)
    End Sub

    Private Sub d003002002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles d003002002.ItemClick
        CargaForma(com_frmArchivoRetencion)
    End Sub
#End Region

#Region "Facturaci�n"
    Private Sub e001001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles e001001.ItemClick
        CargaForma(fac_frmVendedores)
    End Sub
    Private Sub e001003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles e001003.ItemClick
        CargaForma(fac_frmClientes)
    End Sub
    Private Sub e001002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles e001002.ItemClick
        CargaForma(fac_frmRutas)
    End Sub
    Private Sub e001004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles e001004.ItemClick
        CargaForma(fac_frmAsignacionDocumentos)
    End Sub
    Private Sub e001005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles e001005.ItemClick
        CargaForma(fac_frmVales)
    End Sub
    Private Sub e001006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles e001006.ItemClick
        CargaForma(fac_frmAperturaCaja)
    End Sub
    Private Sub e002001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles e002001.ItemClick
        CargaForma(fac_frmPedidos)
    End Sub
    Private Sub e002002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles e002002.ItemClick
        CargaForma(fac_frmNotaRemision)
    End Sub
    Private Sub e002003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles e002003.ItemClick
        CargaForma(fac_frmCotizaciones)
    End Sub
    Private Sub e002004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles e002004.ItemClick
        Dim FechaAct As Date = SiEsNulo(objFunciones.GetFechaContable(piIdSucursalUsuario), Nothing)
        If SiEsNulo(FechaAct, Nothing) = Nothing Then
            MsgBox("No hay fechas activas para esta sucursal", MsgBoxStyle.Critical, "Error de Usuario")
        Else
            CargaForma(fac_frmFacturacion)
        End If
    End Sub
    Private Sub e002005_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e002005.ItemClick
        CargaForma(fac_frmAnularDocumentos)
    End Sub
    Private Sub e002006_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
        'CargaForma(fac_frmVentasCaja)
    End Sub
    Private Sub e002007_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e002006.ItemClick
        CargaForma(fac_frmRetenciones)
    End Sub
    Private Sub e002008_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
        'CargaForma(fac_frmFacturacionBloque)
    End Sub
    Private Sub e002009_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e002007.ItemClick
        CargaForma(fac_frmConsultaFacturacion)
    End Sub
    Private Sub e002010_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e002008.ItemClick
        CargaForma(fac_frmContabilizarVentas)
    End Sub
    Private Sub e002011_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e002009.ItemClick
        CargaForma(fac_frmAutorizarDescuento)
    End Sub
    Private Sub e002012_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e002010.ItemClick
        CargaForma(fac_frmPreFacturacion)
    End Sub
    Private Sub e002013_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e002011.ItemClick
        CargaForma(fac_frmConsultaProductosBase)
    End Sub
    Private Sub e002014_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e002012.ItemClick
        CargaForma(fac_frmConsultaEstadisticaVenta)
    End Sub
    Private Sub e002015_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e002013.ItemClick
        CargaForma(fac_frmCambiarCorrelativo)
    End Sub
    Private Sub e003001001_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001001.ItemClick
        CargaForma(fac_frmVentasPeriodo)
    End Sub
    Private Sub e003001002_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001002.ItemClick
        CargaForma(fac_frmVentasProductoCliente)
    End Sub
    Private Sub e003001003_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001003.ItemClick
        CargaForma(fac_frmVentasClienteProducto)
    End Sub
    Private Sub e003001004_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001004.ItemClick
        CargaForma(fac_frmVentasClienteDocumento)
    End Sub
    Private Sub e003001005_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001005.ItemClick
        CargaForma(fac_frmVentasConsolidadas)
    End Sub
    Private Sub e003001006_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001006.ItemClick
        CargaForma(fac_frmVentasPorComprobante)
    End Sub
    Private Sub e003001007_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001007.ItemClick
        CargaForma(fac_frmVentasFormaPago)
    End Sub
    Private Sub e003001008_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001008.ItemClick
        CargaForma(fac_frmVentasVendedor)
    End Sub
    Private Sub e003001009_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001009.ItemClick
        CargaForma(fac_frmCorteCajaGeneral)
    End Sub
    Private Sub e003001010_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001010.ItemClick
        CargaForma(fac_frmCortesTicket)
    End Sub
    Private Sub e003001011_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001011.ItemClick
        CargaForma(fac_frmListadoPedidos)
    End Sub
    Private Sub e003001012_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001012.ItemClick
        CargaForma(fac_frmReportesLiquidacion)
    End Sub
    Private Sub e003001013_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001013.ItemClick
        CargaForma(fac_frmGenerador)
    End Sub
    Private Sub e003001014_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001014.ItemClick
        CargaForma(fac_frmVentasProductosAnual)
    End Sub
    Private Sub e003001015_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001015.ItemClick
        CargaForma(fac_frmVentasProductosComparacionAnual)
    End Sub
    Private Sub e003001016_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001016.ItemClick
        CargaForma(fac_frmVentasProductoConsolidado)
    End Sub
    Private Sub e003001017_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001017.ItemClick
        CargaForma(fac_frmVentasVendedorUtilidad)
    End Sub
    Private Sub e003001018_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001018.ItemClick
        CargaForma(fac_frmCorteCajaGeneral)
    End Sub
    Private Sub e003001019_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001019.ItemClick
        CargaForma(fac_frmVentasClientesUtilidad)
    End Sub
    Private Sub e003001020_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001020.ItemClick
        CargaForma(fac_frmVentasDevoluciones)
    End Sub
    Private Sub e003001021_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003001021.ItemClick
        CargaForma(fac_frmRepVales)
    End Sub
    Private Sub e003002001_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003002001.ItemClick
        CargaForma(fac_frmLibroContribuyentes)
    End Sub
    Private Sub e003002002_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003002002.ItemClick
        CargaForma(fac_frmLibroConsumidor)
    End Sub
    Private Sub e003002003_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003002003.ItemClick
        CargaForma(fac_frmListAnulados)
    End Sub
    Private Sub e003002004_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003002004.ItemClick
        CargaForma(fac_frmLiquidacionImpuestos)
    End Sub
    Private Sub e003003001_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003003001.ItemClick
        CargaForma(fac_frmVentasProductosComparacionMensual)
    End Sub
    Private Sub e003003002_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003003002.ItemClick
        CargaForma(fac_frmVentasTrimestrales)
    End Sub
    Private Sub e003003004_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e003003004.ItemClick
        CargaForma(fac_frmListadoGestiones)
    End Sub

    Private Sub e004001_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e004001.ItemClick
        CargaForma(fac_frmGestionClientes)
    End Sub
    Private Sub e004002_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e004002.ItemClick
        CargaForma(fac_frmProgramar)
    End Sub
    Private Sub e004003_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles e004003.ItemClick
        CargaForma(fac_frmProspectacionClientes)
    End Sub
#End Region

#Region "Inventario"
    Private Sub f001001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f001001.ItemClick
        CargaForma(inv_frmGrupos)
    End Sub
    Private Sub f001002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f001002.ItemClick
        CargaForma(inv_frmSubGrupos)
    End Sub
    Private Sub f001003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f001003.ItemClick
        CargaForma(inv_frmPrecios)
    End Sub
    Private Sub f001004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f001004.ItemClick
        CargaForma(inv_frmProductos)
    End Sub
    Private Sub f001008_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
        CargaForma(inv_frmBodegas)
    End Sub
    Private Sub f002001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f002001.ItemClick
        CargaForma(inv_frmEntradas)
    End Sub
    Private Sub f002002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f002002.ItemClick
        CargaForma(inv_frmSalidas)
    End Sub
    Private Sub f002003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f002003.ItemClick
        CargaForma(inv_frmTraslados)
    End Sub
    Private Sub f002004_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles f002004.ItemClick
        CargaForma(inv_frmTomaFisica)
    End Sub
    Private Sub f002005_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles f002005.ItemClick
        CargaForma(inv_frmFormulas)
    End Sub
    Private Sub f002006_ItemClick(sender As Object, e As ItemClickEventArgs) Handles f002006.ItemClick
        CargaForma(inv_frmContabilizar)
    End Sub
    Private Sub f002007_ItemClick(sender As Object, e As ItemClickEventArgs) Handles f002007.ItemClick
        CargaForma(inv_frmProducciones)
    End Sub
    Private Sub f002008_ItemClick(sender As Object, e As ItemClickEventArgs) Handles f002008.ItemClick
        CargaForma(inv_frmConsultaProductosPrecios)
    End Sub
    Private Sub f002009_ItemClick(sender As Object, e As ItemClickEventArgs) Handles f002009.ItemClick
        CargaForma(inv_frmRecosteo)
    End Sub
    Private Sub f003001001_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles f003001001.ItemClick
        CargaForma(inv_frmInformeCostos)
    End Sub
    Private Sub f003001002_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles f003001002.ItemClick
        CargaForma(inv_frmInformeVentasUtilidad)
    End Sub
    Private Sub f003001003_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles f003001003.ItemClick
        CargaForma(inv_frmKardex)
    End Sub
    Private Sub f003001004_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles f003001004.ItemClick
        CargaForma(inv_frmKardexUnidades)
    End Sub
    Private Sub f003001005_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles f003001005.ItemClick
        CargaForma(inv_frmInformeInventario)
    End Sub
    Private Sub f003001006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f003001006.ItemClick
        CargaForma(inv_frmInformeMovimientos)
    End Sub
    Private Sub f003001007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f003001007.ItemClick
        CargaForma(inv_frmInformeCargosAbonos)
    End Sub
    Private Sub f003001008_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f003001008.ItemClick
        CargaForma(inv_frmListadoProductos)
    End Sub
    Private Sub f00300109_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f00300109.ItemClick
        'CargaForma(inv_frmListadoLimites)
    End Sub
    Private Sub f003001010_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f003001010.ItemClick
        CargaForma(inv_frmListadoTomaFisica)
    End Sub
    Private Sub f003001011_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f003001011.ItemClick
        CargaForma(inv_frmVineta)
    End Sub
    Private Sub f003001012_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f003001012.ItemClick
        CargaForma(inv_frmGenerador)
    End Sub
    Private Sub f003001014_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f003001014.ItemClick
        CargaForma(inv_frmListadoMaximosMinimos)
    End Sub
    Private Sub f003001015_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f003001015.ItemClick
        CargaForma(inv_frmInformeInventarioSinMovimiento)
    End Sub
    Private Sub f003001016_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f003001016.ItemClick
        CargaForma(inv_frmListadoProductosCategorias)
    End Sub
#End Region
#Region "cpp"
    Private Sub g001001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles g001001.ItemClick
        CargaForma(cpp_frmQuedan)
    End Sub

    Private Sub g001002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles g001002.ItemClick
        CargaForma(cpp_frmCancelarQuedan)
    End Sub

    Private Sub g001003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles g001003.ItemClick
        CargaForma(cpp_frmAbonos)
    End Sub


    Private Sub g001004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles g001004.ItemClick
        CargaForma(cpp_frmAnularAbonos)
    End Sub

    Private Sub g001005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles g001005.ItemClick
        CargaForma(cpp_frmAutorizarAbonos)
    End Sub
    Private Sub g001006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles g001006.ItemClick
        CargaForma(cpp_frmAutorizarAbonos2)
    End Sub

    Private Sub g002001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles g002001.ItemClick
        CargaForma(cpp_frmListQuedan)
    End Sub

    Private Sub g002002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles g002002.ItemClick
        CargaForma(cpp_frmListAbonos)
    End Sub

    Private Sub g002003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles g002003.ItemClick
        CargaForma(cpp_frmAnalisisAntiguedad)
    End Sub

    Private Sub g002004_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles g002004.ItemClick
        CargaForma(cpp_frmAuxiliarAntiguedad)
    End Sub

    Private Sub g002005_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles g002005.ItemClick
        CargaForma(cpp_frmHistoricoMovimientos)
    End Sub

    Private Sub g002006_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles g002006.ItemClick
        CargaForma(cpp_frmAbonosAplicados)
    End Sub
    Private Sub g002007_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) Handles g002007.ItemClick
        CargaForma(cpp_frmEstadoCuenta)
    End Sub
    Private Sub g002008_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles g002008.ItemClick
        CargaForma(cpp_frmListAnulados)
    End Sub
#End Region
#Region "cpc"
    Private Sub h001001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h001001.ItemClick
        CargaForma(cpc_frmCobradores)
    End Sub

    Private Sub h001002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h001002.ItemClick
        CargaForma(cpc_frmTiposMovimiento)
    End Sub

    Private Sub h002001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h002001.ItemClick
        CargaForma(cpc_frmAplicaAbonos)
    End Sub

    Private Sub h002002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h002003.ItemClick
        CargaForma(cpc_frmAplicaCargos)
    End Sub

    Private Sub h002003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h002002.ItemClick
        CargaForma(cpc_frmAnulaAbonos)
    End Sub

    Private Sub h002004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h002004.ItemClick
        CargaForma(cpc_frmAnulaCargo)
    End Sub
    Private Sub h002005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h002005.ItemClick
        CargaForma(cpc_frmContabilizarAbonos)
    End Sub
    Private Sub h002006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h002006.ItemClick
        CargaForma(cpc_frmAplicaAbonosGeneral)
    End Sub
    Private Sub h002007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h002007.ItemClick
        CargaForma(cpc_frmEntregaDocumentos)
    End Sub
    Private Sub h003001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h003001.ItemClick
        CargaForma(cpc_frmListClientes)
    End Sub

    Private Sub h003002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h003002.ItemClick
        CargaForma(cpc_frmDocumentosCredito)
    End Sub

    Private Sub h003003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h003003.ItemClick
        CargaForma(cpc_frmListAbonos)
    End Sub

    Private Sub h003004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h003004.ItemClick
        CargaForma(cpc_frmListCargos)
    End Sub

    Private Sub h003005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h003005.ItemClick
        CargaForma(cpc_frmListCancelados)
    End Sub

    Private Sub h003006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h003006.ItemClick
        CargaForma(cpc_frmEstadoCuenta)
    End Sub

    Private Sub h003007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h003007.ItemClick
        CargaForma(cpc_frmHistoricoMovimientos)
    End Sub

    Private Sub h003008_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h003008.ItemClick
        CargaForma(cpc_frmAnalisisAntiguedad)
    End Sub

    Private Sub h003009_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h003009.ItemClick
        CargaForma(cpc_frmAuxiliar)
    End Sub

    Private Sub h003010_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h003010.ItemClick
        MsgBox("Proceso no disponible para �sta edici�n de Nexus", MsgBoxStyle.Information, "Nota")
    End Sub
    Private Sub h003011_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h003011.ItemClick
        CargaForma(cpc_frmAbonosAplicados)
    End Sub
    Private Sub h003012_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles h003012.ItemClick
        CargaForma(cpc_frmListAnulados)
    End Sub
#End Region


    Private Sub i001001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i001001.ItemClick
        CargaForma(acf_frmClases)
    End Sub

    Private Sub i001002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i001002.ItemClick
        CargaForma(acf_frmUbicaciones)
    End Sub

    Private Sub i001003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i001003.ItemClick
        CargaForma(acf_frmTiposDepreciacion)
    End Sub

    Private Sub i001004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i001004.ItemClick
        CargaForma(acf_frmMarcas)
    End Sub

    Private Sub i001005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i001005.ItemClick
        CargaForma(acf_frmModelos)
    End Sub

    Private Sub i001006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i001006.ItemClick
        CargaForma(acf_frmEstilos)
    End Sub

    Private Sub i001007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i001007.ItemClick
        CargaForma(acf_frmTecnicos)
    End Sub

    Private Sub i001008_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i001008.ItemClick
        CargaForma(acf_frmFallas)
    End Sub

    Private Sub i001009_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i001009.ItemClick
        CargaForma(acf_frmEstados)
    End Sub

    Private Sub i002001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i002001.ItemClick
        CargaForma(acf_frmActivos)
    End Sub

    Private Sub i002002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i002002.ItemClick
        'proceso de revaluaciones
        MsgBox("Proceso no disponible para �sta edici�n de Nexus", MsgBoxStyle.Information, "Nota")
    End Sub

    Private Sub i002003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i002003.ItemClick
        CargaForma(acf_frmMantenimientos)
    End Sub

    Private Sub i002004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i002004.ItemClick
        CargaForma(acf_frmRetiroActivos)
    End Sub

    Private Sub i002005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i002005.ItemClick
        CargaForma(acf_frmVentaActivos)
    End Sub

    Private Sub i002006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i002006.ItemClick
        CargaForma(acf_frmContabilizarActivos)
    End Sub

    Private Sub i002007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i002007.ItemClick
        CargaForma(acf_frmRevierteMes)
    End Sub

    Private Sub i002008_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i002008.ItemClick
        CargaForma(acf_frmListCuadroDiferencias)
    End Sub
    Private Sub i002009_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i002009.ItemClick
        CargaForma(acf_frmTrasladosActivos)
    End Sub

    Private Sub i003001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i003001.ItemClick
        CargaForma(acf_frmListActivos)
    End Sub

    Private Sub i003002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i003002.ItemClick
        CargaForma(acf_frmListActivosCuenta)
    End Sub

    Private Sub i003003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i003003.ItemClick
        CargaForma(acf_frmDepreciacionIndividual)
    End Sub

    Private Sub i003004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i003004.ItemClick
        CargaForma(acf_frmListDepreciacionGeneral)
    End Sub
    Private Sub i003005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i003005.ItemClick
        CargaForma(acf_frmListActivosDepreciados)
    End Sub
    Private Sub i003006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i003006.ItemClick
        CargaForma(acf_frmListActivosRetirados)
    End Sub
    Private Sub i003007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i003007.ItemClick
        CargaForma(acf_frmListActivosVendidos)
    End Sub
    Private Sub i003008_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i003008.ItemClick
        CargaForma(acf_frmListMantenimientos)
    End Sub
    Private Sub i003009_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles i003009.ItemClick
        CargaForma(acf_frmOtrosReportes)
    End Sub
    'Private Sub j001001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles j001001.ItemClick
    '    CargaForma(pro_frmActividades)
    'End Sub
    'Private Sub j001002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles j001002.ItemClick
    '    CargaForma(pro_frmGastosProduccion)
    'End Sub

    'Private Sub j002001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles j002001.ItemClick
    '    CargaForma(pro_frmOrdenesProduccion)
    'End Sub
    'Private Sub j002002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles j002002.ItemClick
    '    CargaForma(pro_frmSalidasProduccion)
    'End Sub
    'Private Sub j002003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles j002003.ItemClick
    '    CargaForma(pro_frmEntradasProduccion)
    'End Sub
    'Private Sub j002004_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles j002004.ItemClick
    '    CargaForma(pro_frmOrdenesEmpaque)
    'End Sub
    Private Sub b001007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b001007.ItemClick
        CargaForma(con_frmExcluirCuentas)
    End Sub
    Private Sub b001008_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b001008.ItemClick
        CargaForma(con_frmConceptos)
    End Sub

    Private Sub b001005_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles b001005.ItemClick
        CargaForma(con_frmCuentasPatrimonio)
    End Sub

    Private Sub f001006_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f001005.ItemClick
        CargaForma(inv_frmProductosMargenes)
    End Sub
    Private Sub f001007_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles f001006.ItemClick
        CargaForma(inv_frmCategorias)
    End Sub

    Private Sub a001013_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a001013.ItemClick
        CargaForma(adm_frmBitacora)
    End Sub
    Private Sub a002001_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a002001.ItemClick
        CargaForma(adm_frmActividades)
    End Sub
    Private Sub a002002_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a002002.ItemClick
        CargaForma(adm_frmProfesiones)
    End Sub
    Private Sub a002003_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles a002003.ItemClick
        CargaForma(adm_frmCorreosAlerta)
    End Sub

    Private Sub RibbonControl_ApplicationButtonClick(sender As Object, e As EventArgs) Handles RibbonControl.ApplicationButtonClick
        'adm_frmAcercaDe.ShowDialog()
    End Sub
    Private Sub e003003005_ItemClick(sender As Object, e As ItemClickEventArgs) Handles e003003005.ItemClick
        CargaForma(fac_frmInformeVentasUtilidadDetalle)
    End Sub
    Private Sub e003003006_ItemClick(sender As Object, e As ItemClickEventArgs) Handles e003003006.ItemClick
        CargaForma(fac_frmVentasSucursal)
    End Sub

    Private Sub f002010_ItemClick(sender As Object, e As ItemClickEventArgs) Handles f002010.ItemClick
        CargaForma(inv_frmAprobacionTraslado)
    End Sub

    Private Sub e003003007_ItemClick(sender As Object, e As ItemClickEventArgs) Handles e003003007.ItemClick
        CargaForma(fac_frmListadoTOKEN)
    End Sub
    Private Sub e003003008_ItemClick(sender As Object, e As ItemClickEventArgs) Handles e003003008.ItemClick
        CargaForma(fac_frmListadoGestionesProspectos)
    End Sub
    Private Sub e003002005_ItemClick(sender As Object, e As ItemClickEventArgs) Handles e003002005.ItemClick
        CargaForma(fac_frmListDetalleDocumentosLegales)
    End Sub
    Private Sub f001007_ItemClick_1(sender As Object, e As ItemClickEventArgs) Handles f001007.ItemClick
        CargaForma(inv_frmCreacionProductos)
    End Sub

    Private Sub f003001017_ItemClick(sender As Object, e As ItemClickEventArgs) Handles f003001017.ItemClick
        CargaForma(inv_frmExistenciaBodegas)
    End Sub

    Private Sub e002014_ItemClick_1(sender As Object, e As ItemClickEventArgs) Handles e002014.ItemClick
        CargaForma(fac_frmAprobacionVentaCredito)
    End Sub

    Private Sub b004003006_ItemClick(sender As Object, e As ItemClickEventArgs) Handles b004003006.ItemClick
        CargaForma(pre_frmPresupuestosVsCuentasCTA)
    End Sub

    Private Sub b004002002_ItemClick(sender As Object, e As ItemClickEventArgs) Handles b004002002.ItemClick
        CargaForma(pre_frmPresupuestosCTA)
    End Sub

    Private Sub h002008_ItemClick(sender As Object, e As ItemClickEventArgs) Handles h002008.ItemClick
        CargaForma(fac_frmIngresoRetencion)
    End Sub

    Private Sub h002009_ItemClick(sender As Object, e As ItemClickEventArgs) Handles h002009.ItemClick
        CargaForma(cpc_frmContabilizarComprobantesRetencion)
    End Sub

    Private Sub h003013_ItemClick(sender As Object, e As ItemClickEventArgs) Handles h003013.ItemClick
        CargaForma(cpc_frmControlComprobanteRetencion)
    End Sub

    Private Sub e003001022_ItemClick(sender As Object, e As ItemClickEventArgs) Handles e003001022.ItemClick
        CargaForma(fac_frmRptCotizaciones)
    End Sub

    Private Sub e003001023_ItemClick(sender As Object, e As ItemClickEventArgs) Handles e003001023.ItemClick
        CargaForma(fac_frmRptRetenciones)
    End Sub

    Private Sub e004004_ItemClick(sender As Object, e As ItemClickEventArgs) Handles e004004.ItemClick
        CargaForma(fac_frmRptConsultaGestiones)
    End Sub

    Private Sub f003001018_ItemClick(sender As Object, e As ItemClickEventArgs) Handles f003001018.ItemClick
        CargaForma(inv_frmInformeGirodelProducto)
    End Sub

    Private Sub f003001019_ItemClick(sender As Object, e As ItemClickEventArgs) Handles f003001019.ItemClick
        CargaForma(inv_frmInformeVentasUtilidadPorDoc)
    End Sub

    Private Sub e003003009_ItemClick(sender As Object, e As ItemClickEventArgs) Handles e003003009.ItemClick
        CargaForma(fac_frmConsultaVentasForma)
    End Sub
    Private Sub e003003010_ItemClick(sender As Object, e As ItemClickEventArgs) Handles e003003010.ItemClick
        CargaForma(fac_frmVentasFormaPagoZonas)
    End Sub

    Private Sub a001012_ItemClick(sender As Object, e As ItemClickEventArgs) Handles a001012.ItemClick
        frm_AdminEmpresas.ShowDialog()
    End Sub

    Private Sub g001007_ItemClick(sender As Object, e As ItemClickEventArgs) Handles g001007.ItemClick
        CargaForma(cpp_frmAutorizarAbonosChequeSeccionados)
    End Sub

    Private Sub e003003003_ItemClick(sender As Object, e As ItemClickEventArgs) Handles e003003003.ItemClick

    End Sub

    Private Sub f003001020_ItemClick(sender As Object, e As ItemClickEventArgs) Handles f003001020.ItemClick
        CargaForma(inv_frmReporteInvExtraible)
    End Sub

    Private Sub g001008_ItemClick(sender As Object, e As ItemClickEventArgs) Handles g001008.ItemClick
        CargaForma(cpp_frmTraslado)
    End Sub
End Class