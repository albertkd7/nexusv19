﻿Public Class frmConsultas

    Private Sub frmConsultas_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Dispose()
        End If
    End Sub

    Private Sub frmConsultas_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        gv.Columns.Clear()
        gc.DataSource = New DataView(cnsDataTable)
        ValCodigo = ""
        ValMovimiento = ""
        ValIdSocio = ""
        gv.BestFitColumns()
        gv.FocusedRowHandle = DevExpress.XtraGrid.GridControl.AutoFilterRowHandle
    End Sub

    Private _cnsDatatable As DataTable
    Public Property cnsDataTable() As DataTable
        Get
            Return _cnsDatatable
        End Get
        Set(ByVal value As DataTable)
            _cnsDatatable = value
        End Set
    End Property

    Private _Descripcion As String
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property


    Private _Codigo As String
    Public Property Codigo() As String
        Get
            Return _Codigo
        End Get
        Set(ByVal value As String)
            _Codigo = value
        End Set
    End Property


    Private _ValCodigo As String
    Public Property ValCodigo() As String
        Get
            Return _ValCodigo
        End Get
        Set(ByVal value As String)
            _ValCodigo = value
        End Set
    End Property
    Private _ValMovimiento As String
    Public Property ValMovimiento() As String
        Get
            Return _ValMovimiento
        End Get
        Set(ByVal value As String)
            _ValMovimiento = value
        End Set
    End Property
    Private _ValIdSocio As String
    Public Property ValIdSocio() As String
        Get
            Return _ValIdSocio
        End Get
        Set(ByVal value As String)
            _ValIdSocio = value
        End Set
    End Property

	Private Sub gc_DoubleClick(ByVal sender As Object, ByVal e As EventArgs) Handles gc.DoubleClick

		If gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(0)).GetType() = Type.GetType("System.Int32") Then
			ValCodigo = Convert.ToString(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(0)))
		Else
			ValCodigo = gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(0))
		End If


		Descripcion = gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(1)) + " " + gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(2))
		Codigo = ""
		ValMovimiento = ""
		ValIdSocio = ""

		'If gv.Columns.Count > 3 Then
		'    Codigo = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(3)), Nothing)
		'End If

		'If gv.Columns.Count > 4 Then
		'    ValMovimiento = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(4)), Nothing)
		'End If

		'If gv.Columns.Count > 5 Then
		'    ValIdSocio = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(5)), Nothing)
		'End If


		Me.Close()
	End Sub

	Private Sub gv_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles gv.KeyDown
		If e.KeyCode = Keys.Enter Then


			If gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(0)).GetType() = Type.GetType("System.Int32") Then
				ValCodigo = Convert.ToString(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(0)))
			Else
				ValCodigo = gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(0))
			End If

			Descripcion = gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(1)) + " " + gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(2))
			Codigo = ""
			ValMovimiento = ""
			ValIdSocio = ""
			'If gv.Columns.Count > 3 Then
			'    Codigo = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(3)), Nothing)
			'End If

			'If gv.Columns.Count > 4 Then
			'    ValMovimiento = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(4)), Nothing)
			'End If

			'If gv.Columns.Count > 5 Then
			'    ValIdSocio = SiEsNulo(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns(5)), Nothing)
			'End If
			Me.Close()
		End If
	End Sub
End Class