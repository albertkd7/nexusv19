﻿Imports NexusELL.TableEntities
Imports NexusBLL
Imports DevExpress.XtraGrid.Columns
Public Class frmDetallarCentroCosto
    Dim dtDetalle As New DataTable
    Dim bl As New ComprasBLL(g_ConnectionString)
    Dim blFac As New FacturaBLL(g_ConnectionString)
    Dim entCentro As con_CentrosCosto

    Private _IdCentro As String
    Public Property IdCentro() As String
        Get
            Return _IdCentro
        End Get
        Set(ByVal value As String)
            _IdCentro = value
        End Set
    End Property


    Private Sub com_frmDetallarGastosImportaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        entCentro = objTablas.con_CentrosCostoSelectByPK(IdCentro)
        gc.DataSource = objTablas.con_CentrosCostoSelectAll
        Me.Text = "CENTRO DE COSTO SELECCIONADO: " & SiEsNulo(entCentro.Nombre, "")

        'SELECCIONO LA FILA DEL CENTRO DE COSTO QUE ESTA SELECCIONADO
        If SiEsNulo(IdCentro, "") <> "" Then
            Dim row As Integer = gv.LocateByValue("IdCentro", IdCentro, Nothing)
            gv.FocusedRowHandle = row
        End If
    End Sub


    Private Sub sbSalir_Click(sender As Object, e As EventArgs) Handles sbSalir.Click
        Me.Close()
    End Sub

    Private Sub gc_DoubleClick(sender As Object, e As EventArgs) Handles gc.DoubleClick
        IdCentro = gv.GetRowCellValue(gv.FocusedRowHandle, "IdCentro")
        Me.Close()
    End Sub
End Class
