﻿Imports System.Text
Imports System.Security.Cryptography
Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common

Public Class ApiFTP

End Class
Public Class ResponseWS
    Private _Completado As Boolean
    Public Property Completado() As Boolean
        Get
            Return Me._Completado
        End Get
        Set(value As Boolean)
            Me._Completado = value
        End Set
    End Property
    Private _Mensaje As String
    Public Property Mensaje() As String
        Get
            Return Me._Mensaje
        End Get
        Set(value As String)
            Me._Mensaje = value
        End Set
    End Property
End Class

Public Module NxExtensions
    <System.Runtime.CompilerServices.Extension()>
    Public Function Encriptar(ByVal s As String) As String
        Dim hashValue As System.Security.Cryptography.HashAlgorithm = New System.Security.Cryptography.SHA1CryptoServiceProvider()
        Dim byteValue As Byte() = System.Text.Encoding.UTF8.GetBytes(s)
        Dim byteHash As Byte() = hashValue.ComputeHash(byteValue)
        hashValue.Clear()
        Return Convert.ToBase64String(byteHash)
    End Function

    <System.Runtime.CompilerServices.Extension()>
    Public Function BuscarImagenEnServidor(ByVal s As String, Optional UseDefaultImage As Boolean = True) As Image
        Dim nxcloud As nxcloudfiles.NxFilesCloud = New nxcloudfiles.NxFilesCloud()
        Dim nxDirectory As String = System.Configuration.ConfigurationManager.AppSettings("NxAPICloudFiles:Directory")
        Dim nxDisabled As Boolean = IIf(System.Configuration.ConfigurationManager.AppSettings("NxAPICloudFiles:Disabled") = "True", True, False)
        Dim _IMAGE As Image

        If String.IsNullOrEmpty(nxDirectory) = False And nxDisabled Then
            Try
                Dim rqt As System.Net.HttpWebRequest = CType(Net.WebRequest.Create(nxcloud.Url.ToLower().Replace("nxcloud.asmx", String.Concat("StorageBox/", nxDirectory, "/", System.IO.Path.GetFileName(s)))), System.Net.HttpWebRequest)
                rqt.Method = "GET"
                rqt.Accept = "*/*" '--> Todo tipo de archivo...
                rqt.Timeout = 3000
                rqt.KeepAlive = False
                rqt.UserAgent = ".NET Framework Example Client"
                rqt.ProtocolVersion = System.Net.HttpVersion.Version10
                Dim rps As System.Net.WebResponse = rqt.GetResponse()
                _IMAGE = IIf(rps.GetResponseStream() Is Nothing = False, Image.FromStream(rps.GetResponseStream), Nothing)
            Catch ex As Exception
                If UseDefaultImage Then
                    _IMAGE = My.Resources.IconSinImagen
                Else
                    _IMAGE = Nothing
                End If
                'MessageBox.Show(ex.Message, "ERROR CONEXION INTERNET", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try
        Else
            _IMAGE = Nothing
        End If
        Return _IMAGE
    End Function

    <System.Runtime.CompilerServices.Extension()>
    Public Function GetLogoEmpresa(ByVal img As String) As Image
        Dim _img As Image = Nothing
        'Dim dbc As DBconfig = New DBconfig(strConexion)

        Return _img
    End Function

    <System.Runtime.CompilerServices.Extension()>
    Public Function EncriptarMNU(ByVal Input As String) As String

        Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("NexusKey") 'La clave debe ser de 8 caracteres
        Dim EncryptionKey() As Byte = Convert.FromBase64String("rpaSPvIvVLlrcmtzPU9/c67Gkj7yL1S9") 'No se puede alterar la cantidad de caracteres pero si la clave
        Dim buffer() As Byte = Encoding.UTF8.GetBytes(Input)
        Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        des.Key = EncryptionKey
        des.IV = IV

        Return Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length()))

    End Function

    <System.Runtime.CompilerServices.Extension()>
    Public Function DesencriptarMNU(ByVal Input As String) As String
        Dim cs As String = "n/a"
        Try
            Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("NexusKey") 'La clave debe ser de 8 caracteres
            Dim EncryptionKey() As Byte = Convert.FromBase64String("rpaSPvIvVLlrcmtzPU9/c67Gkj7yL1S9") 'No se puede alterar la cantidad de caracteres pero si la clave
            Dim buffer() As Byte = Convert.FromBase64String(Input)
            Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
            des.Key = EncryptionKey
            des.IV = IV
            cs = Encoding.UTF8.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length()))

        Catch ex As Exception

        End Try
        Return cs
    End Function

    <System.Runtime.CompilerServices.Extension()>
    Public Function ExecuteDataSetWait(ByVal db As Database, ByVal StoreProcedureName As String, ParamArray parms As Object()) As DataSet
        Dim ds As DataSet = New DataSet()
        Dim cmds As DbCommand = db.GetStoredProcCommand(StoreProcedureName, parms)
        cmds.CommandTimeout = 0
        ds = db.ExecuteDataSet(cmds)
        Return ds
    End Function

    <System.Runtime.CompilerServices.Extension()>
    Public Function ExecuteNonQueryWait(ByVal db As Database, ByVal CmdSQL As String) As Integer
        Dim ds As DataSet = New DataSet()
        Dim cmds As DbCommand = db.GetSqlStringCommand(CmdSQL)
        cmds.CommandTimeout = 0
        Return db.ExecuteNonQuery(cmds)
    End Function

    <System.Runtime.CompilerServices.Extension()>
    Public Function ExecuteScalarWait(ByVal db As Database, ByVal CmdSQL As String) As Object
        Dim ds As DataSet = New DataSet()
        Dim cmds As DbCommand = db.GetSqlStringCommand(CmdSQL)
        cmds.CommandTimeout = 0
        Return db.ExecuteScalar(cmds)
    End Function

    <System.Runtime.CompilerServices.Extension()>
    Public Function ReplaceIgnoreCase(ByVal s As String, ByVal search As String, ByVal replace As String) As String
        ' Reemplazo habitual
        Dim value As String = s.Replace(search, replace)
        ' Reemplazo utilizando expresiones regules ignorando mayúsculas y minúsculas
        Dim value2 As String = System.Text.RegularExpressions.Regex.Replace(s, search, replace, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
        Return value2
    End Function
End Module
