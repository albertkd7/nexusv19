<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NexusMain
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NexusMain))
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip3 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem3 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip4 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem4 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem4 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip5 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem5 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem5 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip6 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem6 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem6 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip7 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem7 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem7 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip8 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem8 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem8 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip9 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem9 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem9 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip10 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem10 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem10 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip11 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem11 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem11 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim SuperToolTip12 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem12 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem12 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim ToolTipSeparatorItem1 As DevExpress.Utils.ToolTipSeparatorItem = New DevExpress.Utils.ToolTipSeparatorItem()
        Dim ToolTipTitleItem13 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim SuperToolTip13 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem14 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem13 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim ToolTipTitleItem15 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim SuperToolTip14 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem16 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem14 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.ImageCollection16x16 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.qbNew = New DevExpress.XtraBars.BarButtonItem()
        Me.qbSave = New DevExpress.XtraBars.BarButtonItem()
        Me.a001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001003 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001004 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001005 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001006 = New DevExpress.XtraBars.BarButtonItem()
        Me.b001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.b001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.b001003 = New DevExpress.XtraBars.BarButtonItem()
        Me.b002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.b002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.b002003 = New DevExpress.XtraBars.BarButtonItem()
        Me.b002004 = New DevExpress.XtraBars.BarButtonItem()
        Me.b002005 = New DevExpress.XtraBars.BarButtonItem()
        Me.b002006 = New DevExpress.XtraBars.BarButtonItem()
        Me.qbEdit = New DevExpress.XtraBars.BarButtonItem()
        Me.qbDelete = New DevExpress.XtraBars.BarButtonItem()
        Me.qbUndo = New DevExpress.XtraBars.BarButtonItem()
        Me.qbFind = New DevExpress.XtraBars.BarButtonItem()
        Me.qbReport = New DevExpress.XtraBars.BarButtonItem()
        Me.qbBack = New DevExpress.XtraBars.BarButtonItem()
        Me.qbNext = New DevExpress.XtraBars.BarButtonItem()
        Me.c001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.c001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.c002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.c002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.c002003 = New DevExpress.XtraBars.BarButtonItem()
        Me.c002004 = New DevExpress.XtraBars.BarButtonItem()
        Me.c002005 = New DevExpress.XtraBars.BarButtonItem()
        Me.c003001 = New DevExpress.XtraBars.BarButtonItem()
        Me.c003002 = New DevExpress.XtraBars.BarButtonItem()
        Me.c003003 = New DevExpress.XtraBars.BarButtonItem()
        Me.c003004 = New DevExpress.XtraBars.BarButtonItem()
        Me.c003005 = New DevExpress.XtraBars.BarButtonItem()
        Me.c003006 = New DevExpress.XtraBars.BarButtonItem()
        Me.d001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.d002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.d002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.d002003 = New DevExpress.XtraBars.BarButtonItem()
        Me.d002004 = New DevExpress.XtraBars.BarButtonItem()
        Me.e001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.e001003 = New DevExpress.XtraBars.BarButtonItem()
        Me.e001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002003 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002004 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001 = New DevExpress.XtraBars.BarSubItem()
        Me.b003001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001003 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001004 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001005 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001006 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001007 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001008 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001009 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001010 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001011 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001012 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001013 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001014 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003001015 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003002 = New DevExpress.XtraBars.BarSubItem()
        Me.b003002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003002003 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003002004 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003002005 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003002006 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003002007 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003002008 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003002009 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003002010 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003002011 = New DevExpress.XtraBars.BarButtonItem()
        Me.b003002012 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002005 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002006 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002007 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001 = New DevExpress.XtraBars.BarSubItem()
        Me.e003001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001003 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001004 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001005 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001006 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001007 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001008 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001009 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001010 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001011 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001012 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001013 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001014 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001015 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001016 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001017 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001019 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001020 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001021 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001022 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001023 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003002 = New DevExpress.XtraBars.BarSubItem()
        Me.e003002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003002003 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003002004 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003002005 = New DevExpress.XtraBars.BarButtonItem()
        Me.f001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.f001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.f001003 = New DevExpress.XtraBars.BarButtonItem()
        Me.f001004 = New DevExpress.XtraBars.BarButtonItem()
        Me.f002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.f002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.f002003 = New DevExpress.XtraBars.BarButtonItem()
        Me.f002004 = New DevExpress.XtraBars.BarButtonItem()
        Me.f002005 = New DevExpress.XtraBars.BarButtonItem()
        Me.f002006 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001 = New DevExpress.XtraBars.BarSubItem()
        Me.f003001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001003 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001004 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001005 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001006 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001007 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001008 = New DevExpress.XtraBars.BarButtonItem()
        Me.f00300109 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001010 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001011 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001012 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001014 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001015 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001016 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001017 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001018 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001019 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001020 = New DevExpress.XtraBars.BarButtonItem()
        Me.g001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.g002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.g001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.g001003 = New DevExpress.XtraBars.BarButtonItem()
        Me.g001004 = New DevExpress.XtraBars.BarButtonItem()
        Me.g002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.g002003 = New DevExpress.XtraBars.BarButtonItem()
        Me.g002004 = New DevExpress.XtraBars.BarButtonItem()
        Me.g002005 = New DevExpress.XtraBars.BarButtonItem()
        Me.g002006 = New DevExpress.XtraBars.BarButtonItem()
        Me.h001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.h001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.h002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.h002003 = New DevExpress.XtraBars.BarButtonItem()
        Me.h002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.h002004 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003001 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003002 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003003 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003004 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003005 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003006 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003007 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003008 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003009 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003010 = New DevExpress.XtraBars.BarButtonItem()
        Me.g001005 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002008 = New DevExpress.XtraBars.BarButtonItem()
        Me.e001004 = New DevExpress.XtraBars.BarButtonItem()
        Me.b002007 = New DevExpress.XtraBars.BarButtonItem()
        Me.i001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.i001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.i001003 = New DevExpress.XtraBars.BarButtonItem()
        Me.i001004 = New DevExpress.XtraBars.BarButtonItem()
        Me.i001005 = New DevExpress.XtraBars.BarButtonItem()
        Me.i001006 = New DevExpress.XtraBars.BarButtonItem()
        Me.i001007 = New DevExpress.XtraBars.BarButtonItem()
        Me.i001008 = New DevExpress.XtraBars.BarButtonItem()
        Me.i001009 = New DevExpress.XtraBars.BarButtonItem()
        Me.i002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.i002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.i002003 = New DevExpress.XtraBars.BarButtonItem()
        Me.i002004 = New DevExpress.XtraBars.BarButtonItem()
        Me.i002005 = New DevExpress.XtraBars.BarButtonItem()
        Me.i002006 = New DevExpress.XtraBars.BarButtonItem()
        Me.i002007 = New DevExpress.XtraBars.BarButtonItem()
        Me.i003001 = New DevExpress.XtraBars.BarButtonItem()
        Me.i002008 = New DevExpress.XtraBars.BarButtonItem()
        Me.i003002 = New DevExpress.XtraBars.BarButtonItem()
        Me.i003003 = New DevExpress.XtraBars.BarButtonItem()
        Me.i003004 = New DevExpress.XtraBars.BarButtonItem()
        Me.i003005 = New DevExpress.XtraBars.BarButtonItem()
        Me.i003006 = New DevExpress.XtraBars.BarButtonItem()
        Me.i003007 = New DevExpress.XtraBars.BarButtonItem()
        Me.i003008 = New DevExpress.XtraBars.BarButtonItem()
        Me.i003009 = New DevExpress.XtraBars.BarButtonItem()
        Me.b001004 = New DevExpress.XtraBars.BarButtonItem()
        Me.b001007 = New DevExpress.XtraBars.BarButtonItem()
        Me.b001008 = New DevExpress.XtraBars.BarButtonItem()
        Me.b001005 = New DevExpress.XtraBars.BarButtonItem()
        Me.d002005 = New DevExpress.XtraBars.BarButtonItem()
        Me.d001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002009 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003011 = New DevExpress.XtraBars.BarButtonItem()
        Me.f003001013 = New DevExpress.XtraBars.BarButtonItem()
        Me.g001006 = New DevExpress.XtraBars.BarButtonItem()
        Me.f002007 = New DevExpress.XtraBars.BarButtonItem()
        Me.d002006 = New DevExpress.XtraBars.BarButtonItem()
        Me.d002007 = New DevExpress.XtraBars.BarButtonItem()
        Me.d001003 = New DevExpress.XtraBars.BarButtonItem()
        Me.d001004 = New DevExpress.XtraBars.BarButtonItem()
        Me.i002009 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002010 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002011 = New DevExpress.XtraBars.BarButtonItem()
        Me.f002008 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003001018 = New DevExpress.XtraBars.BarButtonItem()
        Me.h002005 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002012 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002013 = New DevExpress.XtraBars.BarButtonItem()
        Me.d002008 = New DevExpress.XtraBars.BarButtonItem()
        Me.d002009 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001007 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001008 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001009 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001010 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001011 = New DevExpress.XtraBars.BarButtonItem()
        Me.b004001 = New DevExpress.XtraBars.BarSubItem()
        Me.b004001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.b004001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.b004002 = New DevExpress.XtraBars.BarSubItem()
        Me.b004002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.b004002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.b004003 = New DevExpress.XtraBars.BarSubItem()
        Me.b004003001 = New DevExpress.XtraBars.BarButtonItem()
        Me.b004003002 = New DevExpress.XtraBars.BarButtonItem()
        Me.b004003003 = New DevExpress.XtraBars.BarButtonItem()
        Me.b004003004 = New DevExpress.XtraBars.BarButtonItem()
        Me.b004003005 = New DevExpress.XtraBars.BarButtonItem()
        Me.b004003006 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001012 = New DevExpress.XtraBars.BarButtonItem()
        Me.f001005 = New DevExpress.XtraBars.BarButtonItem()
        Me.g002007 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001013 = New DevExpress.XtraBars.BarButtonItem()
        Me.e001005 = New DevExpress.XtraBars.BarButtonItem()
        Me.j002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.j001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.j002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.j002003 = New DevExpress.XtraBars.BarButtonItem()
        Me.j001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.j002004 = New DevExpress.XtraBars.BarButtonItem()
        Me.h002006 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003003 = New DevExpress.XtraBars.BarSubItem()
        Me.e003003001 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003003002 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003003003 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003003004 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003003005 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003003006 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003003007 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003003008 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003003009 = New DevExpress.XtraBars.BarButtonItem()
        Me.e003003010 = New DevExpress.XtraBars.BarButtonItem()
        Me.f001006 = New DevExpress.XtraBars.BarButtonItem()
        Me.c002006 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003001 = New DevExpress.XtraBars.BarSubItem()
        Me.d003001001 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003001002 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003001003 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003001004 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003001005 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003001006 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003001007 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003001008 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003001009 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003001010 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003001011 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003002 = New DevExpress.XtraBars.BarSubItem()
        Me.d003002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.d003002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.e001006 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003012 = New DevExpress.XtraBars.BarButtonItem()
        Me.g002008 = New DevExpress.XtraBars.BarButtonItem()
        Me.h002007 = New DevExpress.XtraBars.BarButtonItem()
        Me.f002009 = New DevExpress.XtraBars.BarButtonItem()
        Me.c002007 = New DevExpress.XtraBars.BarButtonItem()
        Me.e004001 = New DevExpress.XtraBars.BarButtonItem()
        Me.e004002 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.qbRefresh = New DevExpress.XtraBars.BarButtonItem()
        Me.d002010 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001014 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001015 = New DevExpress.XtraBars.BarButtonItem()
        Me.a001016 = New DevExpress.XtraBars.BarButtonItem()
        Me.a002001 = New DevExpress.XtraBars.BarButtonItem()
        Me.a002002 = New DevExpress.XtraBars.BarButtonItem()
        Me.a002003 = New DevExpress.XtraBars.BarButtonItem()
        Me.a002004 = New DevExpress.XtraBars.BarButtonItem()
        Me.f002010 = New DevExpress.XtraBars.BarButtonItem()
        Me.f001007 = New DevExpress.XtraBars.BarButtonItem()
        Me.e002014 = New DevExpress.XtraBars.BarButtonItem()
        Me.e004003 = New DevExpress.XtraBars.BarButtonItem()
        Me.b002009 = New DevExpress.XtraBars.BarButtonItem()
        Me.h002008 = New DevExpress.XtraBars.BarButtonItem()
        Me.h002009 = New DevExpress.XtraBars.BarButtonItem()
        Me.h003013 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.e004004 = New DevExpress.XtraBars.BarButtonItem()
        Me.g001007 = New DevExpress.XtraBars.BarButtonItem()
        Me.g001008 = New DevExpress.XtraBars.BarButtonItem()
        Me.ImageCollection32x32 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.Administracion = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.a001 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.a002 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.Contabilidad = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.b001 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.b002 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.b003 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.b004 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.Bancos = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.c001 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.c002 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.c003 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.Compras = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.d001 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.d002 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.d003 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.Facturacion = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.e001 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.e002 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.e003 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.e004 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.Inventario = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.f001 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.f002 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.f003 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.CuentasPagar = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.g001 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.g002 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.CuentasCobrar = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.h001 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.h002 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.h003 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.ActivoFijo = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.i001 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.i002 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.i003 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.Producción = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.j001 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.j002 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.j003 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.XtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageCollection16x16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageCollection32x32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl
        '
        Me.RibbonControl.ApplicationButtonText = Nothing
        Me.RibbonControl.ApplicationIcon = CType(resources.GetObject("RibbonControl.ApplicationIcon"), System.Drawing.Bitmap)
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.Images = Me.ImageCollection16x16
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem, Me.qbNew, Me.qbSave, Me.a001001, Me.a001002, Me.a001003, Me.a001004, Me.a001005, Me.a001006, Me.b001001, Me.b001002, Me.b001003, Me.b002001, Me.b002002, Me.b002003, Me.b002004, Me.b002005, Me.b002006, Me.qbEdit, Me.qbDelete, Me.qbUndo, Me.qbFind, Me.qbReport, Me.qbBack, Me.qbNext, Me.c001001, Me.c001002, Me.c002001, Me.c002002, Me.c002003, Me.c002004, Me.c002005, Me.c003001, Me.c003002, Me.c003003, Me.c003004, Me.c003005, Me.c003006, Me.d001001, Me.d002001, Me.d002002, Me.d002003, Me.d002004, Me.e001001, Me.e001003, Me.e001002, Me.e002001, Me.e002002, Me.e002003, Me.e002004, Me.b003001, Me.b003002, Me.b003001001, Me.b003001002, Me.b003001003, Me.b003001004, Me.b003001005, Me.b003001006, Me.b003001007, Me.b003001008, Me.b003001009, Me.b003001010, Me.b003001011, Me.b003001012, Me.b003001013, Me.b003001014, Me.b003001015, Me.b003002001, Me.b003002002, Me.b003002003, Me.b003002004, Me.b003002005, Me.b003002006, Me.e002005, Me.e002006, Me.e002007, Me.e003001, Me.e003001001, Me.e003001002, Me.e003001003, Me.e003001004, Me.e003001005, Me.e003001006, Me.e003001007, Me.e003001008, Me.e003001009, Me.e003001010, Me.e003002, Me.e003002001, Me.e003002002, Me.e003002003, Me.e003001011, Me.e003001012, Me.f001001, Me.f001002, Me.f001003, Me.f001004, Me.f002001, Me.f002002, Me.f002003, Me.f002004, Me.f002005, Me.f002006, Me.f003001, Me.f003001001, Me.f003001002, Me.f003001003, Me.f003001004, Me.f003001005, Me.f003001006, Me.f003001007, Me.f003001008, Me.f00300109, Me.f003001010, Me.f003001011, Me.f003001012, Me.g001001, Me.g002001, Me.g001002, Me.g001003, Me.g001004, Me.g002002, Me.g002003, Me.g002004, Me.g002005, Me.g002006, Me.h001001, Me.h001002, Me.h002001, Me.h002003, Me.h002002, Me.h002004, Me.h003001, Me.h003002, Me.h003003, Me.h003004, Me.h003005, Me.h003006, Me.h003007, Me.h003008, Me.h003009, Me.h003010, Me.g001005, Me.e002008, Me.e001004, Me.b003002007, Me.b003002008, Me.b003002009, Me.b003002010, Me.b002007, Me.i001001, Me.i001002, Me.i001003, Me.i001004, Me.i001005, Me.i001006, Me.i001007, Me.i001008, Me.i001009, Me.i002001, Me.i002002, Me.i002003, Me.i002004, Me.i002005, Me.i002006, Me.i002007, Me.i003001, Me.i002008, Me.i003002, Me.i003003, Me.i003004, Me.i003005, Me.i003006, Me.i003007, Me.i003008, Me.i003009, Me.b001004, Me.e003001013, Me.b001007, Me.b001008, Me.b001005, Me.d002005, Me.d001002, Me.e002009, Me.h003011, Me.f003001013, Me.e003001014, Me.e003001015, Me.g001006, Me.f002007, Me.d002006, Me.d002007, Me.d001003, Me.e003001016, Me.d001004, Me.i002009, Me.e002010, Me.e002011, Me.f002008, Me.e003001017, Me.e003001018, Me.e003001019, Me.e003001020, Me.f003001014, Me.h002005, Me.BarButtonItem1, Me.e002012, Me.e002013, Me.d002008, Me.f003001015, Me.d002009, Me.a001007, Me.a001008, Me.a001009, Me.a001010, Me.a001011, Me.b004001, Me.b004002, Me.b004003, Me.b004001001, Me.b004001002, Me.b004002001, Me.b004003001, Me.b004003002, Me.b004003003, Me.b004003004, Me.b004003005, Me.a001012, Me.f001005, Me.g002007, Me.e003002004, Me.a001013, Me.e001005, Me.e003001021, Me.j002001, Me.j001001, Me.j002002, Me.j002003, Me.j001002, Me.j002004, Me.h002006, Me.e003003, Me.e003003001, Me.f001006, Me.f003001016, Me.b003002011, Me.c002006, Me.d003001, Me.d003001001, Me.d003001002, Me.d003001003, Me.d003001004, Me.d003001005, Me.d003001006, Me.d003001007, Me.d003001008, Me.d003001009, Me.d003002, Me.d003002001, Me.d003002002, Me.e001006, Me.h003012, Me.g002008, Me.h002007, Me.d003001010, Me.f002009, Me.e003003002, Me.e003003003, Me.c002007, Me.e004001, Me.e004002, Me.BarButtonItem4, Me.e003003004, Me.qbRefresh, Me.d002010, Me.a001014, Me.a001015, Me.a001016, Me.a002001, Me.a002002, Me.a002003, Me.a002004, Me.e003003005, Me.e003003006, Me.f002010, Me.e003003007, Me.e003002005, Me.f001007, Me.f003001017, Me.e002014, Me.b004003006, Me.b004002002, Me.e004003, Me.e003003008, Me.b003002012, Me.b002009, Me.h002008, Me.h002009, Me.h003013, Me.BarButtonItem2, Me.e003001022, Me.e003001023, Me.e004004, Me.f003001018, Me.f003001019, Me.e003003009, Me.g001007, Me.f003001020, Me.e003003010, Me.d003001011, Me.g001008})
        Me.RibbonControl.LargeImages = Me.ImageCollection32x32
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 396
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.Administracion, Me.Contabilidad, Me.Bancos, Me.Compras, Me.Facturacion, Me.Inventario, Me.CuentasPagar, Me.CuentasCobrar, Me.ActivoFijo, Me.Producción})
        Me.RibbonControl.QuickToolbarItemLinks.Add(Me.qbNew)
        Me.RibbonControl.QuickToolbarItemLinks.Add(Me.qbSave)
        Me.RibbonControl.QuickToolbarItemLinks.Add(Me.qbEdit)
        Me.RibbonControl.QuickToolbarItemLinks.Add(Me.qbDelete)
        Me.RibbonControl.QuickToolbarItemLinks.Add(Me.qbUndo)
        Me.RibbonControl.QuickToolbarItemLinks.Add(Me.qbFind)
        Me.RibbonControl.QuickToolbarItemLinks.Add(Me.qbReport)
        Me.RibbonControl.QuickToolbarItemLinks.Add(Me.qbRefresh)
        Me.RibbonControl.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.[True]
        Me.RibbonControl.Size = New System.Drawing.Size(1589, 146)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        '
        'ImageCollection16x16
        '
        Me.ImageCollection16x16.ImageStream = CType(resources.GetObject("ImageCollection16x16.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.ImageCollection16x16.Images.SetKeyName(29, "EditInformationHS.png")
        Me.ImageCollection16x16.Images.SetKeyName(30, "sitemap.png")
        Me.ImageCollection16x16.Images.SetKeyName(31, "arrow-left-2.png")
        Me.ImageCollection16x16.Images.SetKeyName(32, "arrow-right-2.png")
        Me.ImageCollection16x16.Images.SetKeyName(33, "address-book-new-3.png")
        Me.ImageCollection16x16.Images.SetKeyName(34, "archive-insert-directory-2.png")
        Me.ImageCollection16x16.Images.SetKeyName(35, "archive-remove.png")
        Me.ImageCollection16x16.Images.SetKeyName(36, "bookmark-new-6.png")
        Me.ImageCollection16x16.Images.SetKeyName(37, "view-sidetree-4.png")
        Me.ImageCollection16x16.Images.SetKeyName(38, "games-config-options.png")
        Me.ImageCollection16x16.Images.SetKeyName(39, "personal.png")
        Me.ImageCollection16x16.Images.SetKeyName(40, "folder-drag-accept-3.png")
        Me.ImageCollection16x16.Images.SetKeyName(41, "wallet-open.png")
        Me.ImageCollection16x16.Images.SetKeyName(42, "applications-office.png")
        Me.ImageCollection16x16.Images.SetKeyName(43, "applications-office.png")
        Me.ImageCollection16x16.Images.SetKeyName(44, "applications-office-2.png")
        Me.ImageCollection16x16.Images.SetKeyName(45, "applications-office-4.png")
        Me.ImageCollection16x16.Images.SetKeyName(46, "applications-office-6.png")
        Me.ImageCollection16x16.Images.SetKeyName(47, "applications-other.png")
        Me.ImageCollection16x16.Images.SetKeyName(48, "applications-other-2.png")
        Me.ImageCollection16x16.Images.SetKeyName(49, "applications-other-4.png")
        Me.ImageCollection16x16.Images.SetKeyName(50, "preferences-system-network.png")
        Me.ImageCollection16x16.Images.SetKeyName(51, "edit-text-frame-update.png")
        Me.ImageCollection16x16.Images.SetKeyName(52, "edit-user.png")
        Me.ImageCollection16x16.Images.SetKeyName(53, "family.png")
        Me.ImageCollection16x16.Images.SetKeyName(54, "folder-new.png")
        Me.ImageCollection16x16.Images.SetKeyName(55, "go-home-4.png")
        Me.ImageCollection16x16.Images.SetKeyName(56, "help-contents-2.png")
        Me.ImageCollection16x16.Images.SetKeyName(57, "help-contents-4.png")
        Me.ImageCollection16x16.Images.SetKeyName(58, "journal-new.png")
        Me.ImageCollection16x16.Images.SetKeyName(59, "computer-3.png")
        Me.ImageCollection16x16.Images.SetKeyName(60, "abiword.png")
        Me.ImageCollection16x16.Images.SetKeyName(61, "accessories-calculator.png")
        Me.ImageCollection16x16.Images.SetKeyName(62, "accessories-calculator-2.png")
        Me.ImageCollection16x16.Images.SetKeyName(63, "accessories-calculator-4.png")
        Me.ImageCollection16x16.Images.SetKeyName(64, "accessories-date.png")
        Me.ImageCollection16x16.Images.SetKeyName(65, "accessories-date-2.png")
        Me.ImageCollection16x16.Images.SetKeyName(66, "accessories-dictionary.png")
        Me.ImageCollection16x16.Images.SetKeyName(67, "accessories-dictionary-2.png")
        Me.ImageCollection16x16.Images.SetKeyName(68, "accessories-text-editor-4.png")
        Me.ImageCollection16x16.Images.SetKeyName(69, "accessories-text-editor-6.png")
        Me.ImageCollection16x16.Images.SetKeyName(70, "accessories-text-editor-7.png")
        Me.ImageCollection16x16.Images.SetKeyName(71, "acroread-3.png")
        Me.ImageCollection16x16.Images.SetKeyName(72, "ams.png")
        Me.ImageCollection16x16.Images.SetKeyName(73, "ark.png")
        Me.ImageCollection16x16.Images.SetKeyName(74, "ark-4.png")
        Me.ImageCollection16x16.Images.SetKeyName(75, "atcalc.png")
        Me.ImageCollection16x16.Images.SetKeyName(76, "beryl.png")
        Me.ImageCollection16x16.Images.SetKeyName(77, "BioCocoa.app.png")
        Me.ImageCollection16x16.Images.SetKeyName(78, "conduit.png")
        Me.ImageCollection16x16.Images.SetKeyName(79, "conduit-2.png")
        Me.ImageCollection16x16.Images.SetKeyName(80, "cups.png")
        Me.ImageCollection16x16.Images.SetKeyName(81, "d3lphin.png")
        Me.ImageCollection16x16.Images.SetKeyName(82, "DictionaryReader.app.png")
        Me.ImageCollection16x16.Images.SetKeyName(83, "MoneyBagUSDollar32.png")
        Me.ImageCollection16x16.Images.SetKeyName(84, "bajar.png")
        Me.ImageCollection16x16.Images.SetKeyName(85, "CheckDelete16.png")
        Me.ImageCollection16x16.Images.SetKeyName(86, "TipCuentas.ico")
        Me.ImageCollection16x16.Images.SetKeyName(87, "ok.png")
        Me.ImageCollection16x16.Images.SetKeyName(88, "Report.png")
        Me.ImageCollection16x16.Images.SetKeyName(89, "Folder 098.ico")
        Me.ImageCollection16x16.Images.SetKeyName(90, "indicator.white.gif")
        Me.ImageCollection16x16.Images.SetKeyName(91, "Tran.png")
        Me.ImageCollection16x16.Images.SetKeyName(92, "Calculator16.png")
        Me.ImageCollection16x16.Images.SetKeyName(93, "CashRegister16.png")
        Me.ImageCollection16x16.Images.SetKeyName(94, "zona.png")
        Me.ImageCollection16x16.Images.SetKeyName(95, "Pedidos.png")
        Me.ImageCollection16x16.Images.SetKeyName(96, "Remision.png")
        Me.ImageCollection16x16.Images.SetKeyName(97, "Cotizacion.ico")
        Me.ImageCollection16x16.Images.SetKeyName(98, "CashRegisterAdd16.png")
        Me.ImageCollection16x16.Images.SetKeyName(99, "Ret.ico")
        Me.ImageCollection16x16.Images.SetKeyName(100, "PercentSign16.png")
        Me.ImageCollection16x16.Images.SetKeyName(101, "1374119924_round_remove.png")
        Me.ImageCollection16x16.Images.SetKeyName(102, "ruler.png")
        Me.ImageCollection16x16.Images.SetKeyName(103, "marca2.png")
        Me.ImageCollection16x16.Images.SetKeyName(104, "Estante.png")
        Me.ImageCollection16x16.Images.SetKeyName(105, "bodega.png")
        Me.ImageCollection16x16.Images.SetKeyName(106, "precios.png")
        Me.ImageCollection16x16.Images.SetKeyName(107, "edit_add.png")
        Me.ImageCollection16x16.Images.SetKeyName(108, "insert-list-bullets.png")
        Me.ImageCollection16x16.Images.SetKeyName(109, "CheckBookDownArrow24.png")
        Me.ImageCollection16x16.Images.SetKeyName(110, "Anula.png")
        Me.ImageCollection16x16.Images.SetKeyName(111, "autoriza2.png")
        Me.ImageCollection16x16.Images.SetKeyName(112, "autoriza3.png")
        Me.ImageCollection16x16.Images.SetKeyName(113, "editcopy.png")
        Me.ImageCollection16x16.Images.SetKeyName(114, "office-chart-line-stacked.png")
        Me.ImageCollection16x16.Images.SetKeyName(115, "timeline-marker.png")
        Me.ImageCollection16x16.Images.SetKeyName(116, "view-calendar-list.png")
        Me.ImageCollection16x16.Images.SetKeyName(117, "view-sidetree-2.png")
        Me.ImageCollection16x16.Images.SetKeyName(118, "script.png")
        Me.ImageCollection16x16.Images.SetKeyName(119, "align-center.png")
        Me.ImageCollection16x16.Images.SetKeyName(120, "modelo.png")
        Me.ImageCollection16x16.Images.SetKeyName(121, "estilo.png")
        Me.ImageCollection16x16.Images.SetKeyName(122, "mantenimiento.png")
        Me.ImageCollection16x16.Images.SetKeyName(123, "refresh.png")
        Me.ImageCollection16x16.Images.SetKeyName(124, "MoneyBagUSDollar32.png")
        Me.ImageCollection16x16.Images.SetKeyName(125, "refresh2.ico")
        Me.ImageCollection16x16.Images.SetKeyName(126, "refresh3.png")
        Me.ImageCollection16x16.Images.SetKeyName(127, "refresh4.png")
        '
        'qbNew
        '
        Me.qbNew.Caption = "Nuevo"
        Me.qbNew.Id = 0
        Me.qbNew.ImageOptions.ImageIndex = 6
        Me.qbNew.ItemShortcut = New DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2)
        Me.qbNew.Name = "qbNew"
        ToolTipTitleItem1.Text = "Nuevo - F2"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Crea un nuevo registro"
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.qbNew.SuperTip = SuperToolTip1
        '
        'qbSave
        '
        Me.qbSave.Caption = "Guardar"
        Me.qbSave.Id = 1
        Me.qbSave.ImageOptions.ImageIndex = 10
        Me.qbSave.ItemShortcut = New DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3)
        Me.qbSave.Name = "qbSave"
        ToolTipTitleItem2.Text = "Guardar - F3"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Guarda el registro en la base de datos."
        SuperToolTip2.Items.Add(ToolTipTitleItem2)
        SuperToolTip2.Items.Add(ToolTipItem2)
        Me.qbSave.SuperTip = SuperToolTip2
        '
        'a001001
        '
        Me.a001001.Caption = "Seguridad"
        Me.a001001.Id = 2
        Me.a001001.ImageOptions.ImageIndex = 39
        Me.a001001.Name = "a001001"
        '
        'a001002
        '
        Me.a001002.Caption = "Configuración"
        Me.a001002.Id = 3
        Me.a001002.ImageOptions.ImageIndex = 38
        Me.a001002.Name = "a001002"
        '
        'a001003
        '
        Me.a001003.Caption = "Tipos de Comprobante"
        Me.a001003.Id = 4
        Me.a001003.ImageOptions.ImageIndex = 40
        Me.a001003.Name = "a001003"
        '
        'a001004
        '
        Me.a001004.Caption = "Formas de Pago"
        Me.a001004.Id = 5
        Me.a001004.ImageOptions.ImageIndex = 41
        Me.a001004.Name = "a001004"
        ToolTipTitleItem3.Text = "Formas de Pago"
        ToolTipItem3.LeftIndent = 6
        ToolTipItem3.Text = "Registro de las diferentes formas de pago al momento de facturar"
        SuperToolTip3.Items.Add(ToolTipTitleItem3)
        SuperToolTip3.Items.Add(ToolTipItem3)
        Me.a001004.SuperTip = SuperToolTip3
        '
        'a001005
        '
        Me.a001005.Caption = "Sucursales"
        Me.a001005.Id = 6
        Me.a001005.ImageOptions.ImageIndex = 55
        Me.a001005.Name = "a001005"
        '
        'a001006
        '
        Me.a001006.Caption = "Puntos de Venta"
        Me.a001006.Id = 7
        Me.a001006.ImageOptions.ImageIndex = 59
        Me.a001006.Name = "a001006"
        '
        'b001001
        '
        Me.b001001.Caption = "Cuentas"
        Me.b001001.Id = 8
        Me.b001001.ImageOptions.ImageIndex = 30
        Me.b001001.Name = "b001001"
        '
        'b001002
        '
        Me.b001002.Caption = "Tipos de Partida"
        Me.b001002.Id = 9
        Me.b001002.ImageOptions.ImageIndex = 0
        Me.b001002.Name = "b001002"
        '
        'b001003
        '
        Me.b001003.Caption = "Relacionar Centros de Costo"
        Me.b001003.Id = 11
        Me.b001003.ImageOptions.ImageIndex = 14
        Me.b001003.Name = "b001003"
        '
        'b002001
        '
        Me.b002001.Caption = "Partidas"
        Me.b002001.Id = 12
        Me.b002001.ImageOptions.ImageIndex = 33
        Me.b002001.ImageOptions.LargeImageIndex = 1
        Me.b002001.Name = "b002001"
        '
        'b002002
        '
        Me.b002002.Caption = "Eliminación"
        Me.b002002.Id = 13
        Me.b002002.ImageOptions.ImageIndex = 35
        Me.b002002.Name = "b002002"
        '
        'b002003
        '
        Me.b002003.Caption = "Re-Numeración"
        Me.b002003.Id = 14
        Me.b002003.ImageOptions.ImageIndex = 51
        Me.b002003.Name = "b002003"
        '
        'b002004
        '
        Me.b002004.Caption = "Cierre Contable"
        Me.b002004.Id = 15
        Me.b002004.ImageOptions.ImageIndex = 65
        Me.b002004.ImageOptions.LargeImageIndex = 10
        Me.b002004.Name = "b002004"
        '
        'b002005
        '
        Me.b002005.Caption = "Revertir Cierre"
        Me.b002005.Id = 16
        Me.b002005.ImageOptions.ImageIndex = 78
        Me.b002005.ImageOptions.LargeImageIndex = 11
        Me.b002005.Name = "b002005"
        '
        'b002006
        '
        Me.b002006.Caption = "Crear Partida de Liquidación"
        Me.b002006.Id = 17
        Me.b002006.ImageOptions.ImageIndex = 75
        Me.b002006.Name = "b002006"
        '
        'qbEdit
        '
        Me.qbEdit.Caption = "Editar"
        Me.qbEdit.Id = 18
        Me.qbEdit.ImageOptions.ImageIndex = 29
        Me.qbEdit.Name = "qbEdit"
        ToolTipTitleItem4.Text = "Editar"
        ToolTipItem4.LeftIndent = 6
        ToolTipItem4.Text = "Editar el registro actual"
        SuperToolTip4.Items.Add(ToolTipTitleItem4)
        SuperToolTip4.Items.Add(ToolTipItem4)
        Me.qbEdit.SuperTip = SuperToolTip4
        '
        'qbDelete
        '
        Me.qbDelete.Caption = "Eliminar"
        Me.qbDelete.Id = 19
        Me.qbDelete.ImageOptions.ImageIndex = 13
        Me.qbDelete.Name = "qbDelete"
        ToolTipTitleItem5.Text = "Eliminar"
        ToolTipItem5.LeftIndent = 6
        ToolTipItem5.Text = "Elimina el registro actual"
        SuperToolTip5.Items.Add(ToolTipTitleItem5)
        SuperToolTip5.Items.Add(ToolTipItem5)
        Me.qbDelete.SuperTip = SuperToolTip5
        '
        'qbUndo
        '
        Me.qbUndo.Caption = "Revertir"
        Me.qbUndo.Id = 20
        Me.qbUndo.ImageOptions.ImageIndex = 11
        Me.qbUndo.Name = "qbUndo"
        ToolTipTitleItem6.Text = "Revertir"
        ToolTipItem6.LeftIndent = 6
        ToolTipItem6.Text = "Revierte los cambios realizados"
        SuperToolTip6.Items.Add(ToolTipTitleItem6)
        SuperToolTip6.Items.Add(ToolTipItem6)
        Me.qbUndo.SuperTip = SuperToolTip6
        '
        'qbFind
        '
        Me.qbFind.Caption = "Consulta"
        Me.qbFind.Id = 21
        Me.qbFind.ImageOptions.ImageIndex = 3
        Me.qbFind.Name = "qbFind"
        ToolTipTitleItem7.Text = "Consulta"
        ToolTipItem7.LeftIndent = 6
        ToolTipItem7.Text = "Relizar una consulta de los datos de éste proceso"
        SuperToolTip7.Items.Add(ToolTipTitleItem7)
        SuperToolTip7.Items.Add(ToolTipItem7)
        Me.qbFind.SuperTip = SuperToolTip7
        '
        'qbReport
        '
        Me.qbReport.Caption = "Reporte"
        Me.qbReport.Id = 22
        Me.qbReport.ImageOptions.ImageIndex = 9
        Me.qbReport.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P))
        Me.qbReport.Name = "qbReport"
        ToolTipTitleItem8.Text = "Reporte - Ctrl-P"
        ToolTipItem8.LeftIndent = 6
        ToolTipItem8.Text = "Genera el reporte en pantalla del proceso actual"
        SuperToolTip8.Items.Add(ToolTipTitleItem8)
        SuperToolTip8.Items.Add(ToolTipItem8)
        Me.qbReport.SuperTip = SuperToolTip8
        '
        'qbBack
        '
        Me.qbBack.Id = 23
        Me.qbBack.ImageOptions.ImageIndex = 31
        Me.qbBack.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A))
        Me.qbBack.Name = "qbBack"
        ToolTipTitleItem9.Text = "Anterior"
        ToolTipItem9.LeftIndent = 6
        ToolTipItem9.Text = "Mostrar el registro anterior"
        SuperToolTip9.Items.Add(ToolTipTitleItem9)
        SuperToolTip9.Items.Add(ToolTipItem9)
        Me.qbBack.SuperTip = SuperToolTip9
        '
        'qbNext
        '
        Me.qbNext.Caption = "Siguiente"
        Me.qbNext.Id = 24
        Me.qbNext.ImageOptions.ImageIndex = 32
        Me.qbNext.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S))
        Me.qbNext.Name = "qbNext"
        ToolTipTitleItem10.Text = "Siguiente"
        ToolTipItem10.LeftIndent = 6
        ToolTipItem10.Text = "Avanzar al registro siguiente"
        SuperToolTip10.Items.Add(ToolTipTitleItem10)
        SuperToolTip10.Items.Add(ToolTipItem10)
        Me.qbNext.SuperTip = SuperToolTip10
        '
        'c001001
        '
        Me.c001001.Caption = "Tipos de Transacciones"
        Me.c001001.Id = 47
        Me.c001001.ImageOptions.ImageIndex = 86
        Me.c001001.Name = "c001001"
        '
        'c001002
        '
        Me.c001002.Caption = "Cuentas Bancarias"
        Me.c001002.Id = 48
        Me.c001002.ImageOptions.LargeImageIndex = 13
        Me.c001002.Name = "c001002"
        '
        'c002001
        '
        Me.c002001.Caption = "Emisión de Cheques"
        Me.c002001.Id = 49
        Me.c002001.ImageOptions.LargeImageIndex = 8
        Me.c002001.Name = "c002001"
        '
        'c002002
        '
        Me.c002002.Caption = "Depósitos, Retiros, Transferencias"
        Me.c002002.Id = 50
        Me.c002002.ImageOptions.ImageIndex = 91
        Me.c002002.Name = "c002002"
        '
        'c002003
        '
        Me.c002003.Caption = "Anulación de cheques"
        Me.c002003.Id = 51
        Me.c002003.ImageOptions.ImageIndex = 85
        Me.c002003.Name = "c002003"
        '
        'c002004
        '
        Me.c002004.Caption = "Preparar Registros p/conciliación"
        Me.c002004.Id = 52
        Me.c002004.ImageOptions.ImageIndex = 87
        Me.c002004.Name = "c002004"
        '
        'c002005
        '
        Me.c002005.Caption = "Conciliación Bancaria"
        Me.c002005.Id = 53
        Me.c002005.ImageOptions.LargeImageIndex = 9
        Me.c002005.Name = "c002005"
        '
        'c003001
        '
        Me.c003001.Caption = "Imprimir Libro de Bancos"
        Me.c003001.Id = 54
        Me.c003001.ImageOptions.ImageIndex = 82
        Me.c003001.ImageOptions.LargeImageIndex = 17
        Me.c003001.Name = "c003001"
        '
        'c003002
        '
        Me.c003002.Caption = "Listado de Cheques"
        Me.c003002.Id = 55
        Me.c003002.ImageOptions.ImageIndex = 88
        Me.c003002.Name = "c003002"
        '
        'c003003
        '
        Me.c003003.Caption = "Listado de Transacciones Bancarias"
        Me.c003003.Id = 56
        Me.c003003.ImageOptions.ImageIndex = 29
        Me.c003003.Name = "c003003"
        '
        'c003004
        '
        Me.c003004.Caption = "Imprimir bloque de cheques"
        Me.c003004.Id = 57
        Me.c003004.ImageOptions.ImageIndex = 46
        Me.c003004.Name = "c003004"
        '
        'c003005
        '
        Me.c003005.Caption = "Disponibilidad bancaria"
        Me.c003005.Id = 58
        Me.c003005.ImageOptions.ImageIndex = 89
        Me.c003005.Name = "c003005"
        '
        'c003006
        '
        Me.c003006.Caption = "Generador de consultas"
        Me.c003006.Id = 59
        Me.c003006.ImageOptions.ImageIndex = 90
        Me.c003006.Name = "c003006"
        '
        'd001001
        '
        Me.d001001.Caption = "Proveedores"
        Me.d001001.Id = 60
        Me.d001001.ImageOptions.ImageIndex = 53
        Me.d001001.ImageOptions.LargeImageIndex = 7
        Me.d001001.Name = "d001001"
        '
        'd002001
        '
        Me.d002001.Caption = "Ordenes de Compra"
        Me.d002001.Id = 61
        Me.d002001.ImageOptions.ImageIndex = 61
        Me.d002001.Name = "d002001"
        '
        'd002002
        '
        Me.d002002.Caption = "Compras"
        Me.d002002.Id = 62
        Me.d002002.ImageOptions.LargeImageIndex = 4
        Me.d002002.Name = "d002002"
        '
        'd002003
        '
        Me.d002003.Caption = "Importaciones"
        Me.d002003.Id = 63
        Me.d002003.ImageOptions.LargeImageIndex = 18
        Me.d002003.Name = "d002003"
        '
        'd002004
        '
        Me.d002004.Caption = "Contabilizar Compras"
        Me.d002004.Id = 65
        Me.d002004.ImageOptions.ImageIndex = 92
        Me.d002004.Name = "d002004"
        '
        'e001001
        '
        Me.e001001.Caption = "Vendedores"
        Me.e001001.Id = 74
        Me.e001001.ImageOptions.ImageIndex = 53
        Me.e001001.Name = "e001001"
        '
        'e001003
        '
        Me.e001003.Caption = "Clientes"
        Me.e001003.Id = 76
        Me.e001003.ImageOptions.LargeImageIndex = 19
        Me.e001003.Name = "e001003"
        '
        'e001002
        '
        Me.e001002.Caption = "Rutas y Zonas"
        Me.e001002.Id = 77
        Me.e001002.ImageOptions.ImageIndex = 94
        Me.e001002.Name = "e001002"
        '
        'e002001
        '
        Me.e002001.Caption = "Pedidos (O.de C.)"
        Me.e002001.Id = 78
        Me.e002001.ImageOptions.ImageIndex = 95
        Me.e002001.Name = "e002001"
        '
        'e002002
        '
        Me.e002002.Caption = "Notas de Remisión"
        Me.e002002.Id = 79
        Me.e002002.ImageOptions.ImageIndex = 96
        Me.e002002.Name = "e002002"
        '
        'e002003
        '
        Me.e002003.Caption = "Cotizaciones"
        Me.e002003.Id = 80
        Me.e002003.ImageOptions.ImageIndex = 97
        Me.e002003.Name = "e002003"
        '
        'e002004
        '
        Me.e002004.Caption = "Ventas y Facturación"
        Me.e002004.Id = 81
        Me.e002004.ImageOptions.LargeImageIndex = 0
        Me.e002004.Name = "e002004"
        ToolTipTitleItem11.Text = "Facturación"
        ToolTipItem11.LeftIndent = 6
        ToolTipItem11.Text = "Registro de los diferentes documentos de venta, permitiendo la impresión del docu" & _
    "mento al momento después de guardarlo"
        SuperToolTip11.Items.Add(ToolTipTitleItem11)
        SuperToolTip11.Items.Add(ToolTipItem11)
        Me.e002004.SuperTip = SuperToolTip11
        '
        'b003001
        '
        Me.b003001.Caption = "Reportes Estándares"
        Me.b003001.Id = 87
        Me.b003001.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.b003001001), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001002), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001003), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001004), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001005), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001006), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001007), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001008), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001009), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001010), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001011), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001012), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001013), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001014), New DevExpress.XtraBars.LinkPersistInfo(Me.b003001015)})
        Me.b003001.Name = "b003001"
        '
        'b003001001
        '
        Me.b003001001.Caption = "Partidas Tipo Reporte"
        Me.b003001001.Id = 89
        Me.b003001001.Name = "b003001001"
        '
        'b003001002
        '
        Me.b003001002.Caption = "Concentración de Partidas"
        Me.b003001002.Id = 90
        Me.b003001002.Name = "b003001002"
        '
        'b003001003
        '
        Me.b003001003.Caption = "Concentración de Cuentas de Mayor"
        Me.b003001003.Id = 91
        Me.b003001003.Name = "b003001003"
        '
        'b003001004
        '
        Me.b003001004.Caption = "Listado de Movimientos de Cuenta"
        Me.b003001004.Id = 92
        Me.b003001004.Name = "b003001004"
        '
        'b003001005
        '
        Me.b003001005.Caption = "Listado de Totales de Partidas"
        Me.b003001005.Id = 93
        Me.b003001005.Name = "b003001005"
        '
        'b003001006
        '
        Me.b003001006.Caption = "Listado de Catálogo de Cuentas"
        Me.b003001006.Id = 94
        Me.b003001006.Name = "b003001006"
        '
        'b003001007
        '
        Me.b003001007.Caption = "Libro Diario"
        Me.b003001007.Id = 95
        Me.b003001007.Name = "b003001007"
        '
        'b003001008
        '
        Me.b003001008.Caption = "Libro Mayor"
        Me.b003001008.Id = 96
        Me.b003001008.Name = "b003001008"
        '
        'b003001009
        '
        Me.b003001009.Caption = "Libro Diario Mayor"
        Me.b003001009.Id = 97
        Me.b003001009.Name = "b003001009"
        '
        'b003001010
        '
        Me.b003001010.Caption = "Libro Auxiliar de Mayor"
        Me.b003001010.Id = 98
        Me.b003001010.Name = "b003001010"
        '
        'b003001011
        '
        Me.b003001011.Caption = "Auxiliar por Centro de Costo"
        Me.b003001011.Id = 99
        Me.b003001011.Name = "b003001011"
        '
        'b003001012
        '
        Me.b003001012.Caption = "Saldos por Centro de Costo"
        Me.b003001012.Id = 100
        Me.b003001012.Name = "b003001012"
        '
        'b003001013
        '
        Me.b003001013.Caption = "Comparación de dos Meses Cualquiera"
        Me.b003001013.Id = 101
        Me.b003001013.Name = "b003001013"
        '
        'b003001014
        '
        Me.b003001014.Caption = "Comparacion Anual de Cuentas"
        Me.b003001014.Id = 102
        Me.b003001014.Name = "b003001014"
        '
        'b003001015
        '
        Me.b003001015.Caption = "Generador de Consultas"
        Me.b003001015.Id = 103
        Me.b003001015.Name = "b003001015"
        '
        'b003002
        '
        Me.b003002.Caption = "Reportes Financieros"
        Me.b003002.Id = 88
        Me.b003002.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.b003002001), New DevExpress.XtraBars.LinkPersistInfo(Me.b003002002), New DevExpress.XtraBars.LinkPersistInfo(Me.b003002003), New DevExpress.XtraBars.LinkPersistInfo(Me.b003002004), New DevExpress.XtraBars.LinkPersistInfo(Me.b003002005), New DevExpress.XtraBars.LinkPersistInfo(Me.b003002006), New DevExpress.XtraBars.LinkPersistInfo(Me.b003002007), New DevExpress.XtraBars.LinkPersistInfo(Me.b003002008), New DevExpress.XtraBars.LinkPersistInfo(Me.b003002009), New DevExpress.XtraBars.LinkPersistInfo(Me.b003002010), New DevExpress.XtraBars.LinkPersistInfo(Me.b003002011), New DevExpress.XtraBars.LinkPersistInfo(Me.b003002012)})
        Me.b003002.Name = "b003002"
        '
        'b003002001
        '
        Me.b003002001.Caption = "Balance de Comprobación Tipo Cuenta"
        Me.b003002001.Id = 104
        Me.b003002001.Name = "b003002001"
        '
        'b003002002
        '
        Me.b003002002.Caption = "Balance de Comprobación Tipo Reporte"
        Me.b003002002.Id = 105
        Me.b003002002.Name = "b003002002"
        '
        'b003002003
        '
        Me.b003002003.Caption = "Balance General"
        Me.b003002003.Id = 106
        Me.b003002003.Name = "b003002003"
        '
        'b003002004
        '
        Me.b003002004.Caption = "Estado de Resultados Consolidado"
        Me.b003002004.Id = 107
        Me.b003002004.Name = "b003002004"
        '
        'b003002005
        '
        Me.b003002005.Caption = "Estado de Resultados por Centro de Costo"
        Me.b003002005.Id = 108
        Me.b003002005.Name = "b003002005"
        '
        'b003002006
        '
        Me.b003002006.Caption = "Anexo a los Estados Financieros"
        Me.b003002006.Id = 109
        Me.b003002006.Name = "b003002006"
        '
        'b003002007
        '
        Me.b003002007.Caption = "Balance General Comparativo"
        Me.b003002007.Id = 191
        Me.b003002007.Name = "b003002007"
        '
        'b003002008
        '
        Me.b003002008.Caption = "Estado de Resultados Comparativo"
        Me.b003002008.Id = 192
        Me.b003002008.Name = "b003002008"
        '
        'b003002009
        '
        Me.b003002009.Caption = "Estado de Flujo de Efectivo"
        Me.b003002009.Id = 193
        Me.b003002009.Name = "b003002009"
        '
        'b003002010
        '
        Me.b003002010.Caption = "Estado de Cambios en el Patrimonio"
        Me.b003002010.Id = 194
        Me.b003002010.Name = "b003002010"
        '
        'b003002011
        '
        Me.b003002011.Caption = "Consulta Balance Comprobación "
        Me.b003002011.Id = 325
        Me.b003002011.Name = "b003002011"
        '
        'b003002012
        '
        Me.b003002012.Caption = "Razones Financieras"
        Me.b003002012.Id = 377
        Me.b003002012.Name = "b003002012"
        '
        'e002005
        '
        Me.e002005.Caption = "Anulación de Documentos"
        Me.e002005.Id = 110
        Me.e002005.ImageOptions.ImageIndex = 101
        Me.e002005.Name = "e002005"
        '
        'e002006
        '
        Me.e002006.Caption = "Comprobantes de Retención"
        Me.e002006.Id = 112
        Me.e002006.ImageOptions.ImageIndex = 99
        Me.e002006.Name = "e002006"
        '
        'e002007
        '
        Me.e002007.Caption = "Consultar Facturación"
        Me.e002007.Id = 114
        Me.e002007.ImageOptions.ImageIndex = 3
        Me.e002007.Name = "e002007"
        '
        'e003001
        '
        Me.e003001.Caption = "Reportes Generales"
        Me.e003001.Id = 115
        Me.e003001.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.e003001001), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001002), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001003), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001004), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001005), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001006), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001007), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001008), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001009), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001010), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001011), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001012), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001013), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001014), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001015), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001016), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001017), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001019), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001020), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001021), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001022), New DevExpress.XtraBars.LinkPersistInfo(Me.e003001023)})
        Me.e003001.Name = "e003001"
        '
        'e003001001
        '
        Me.e003001001.Caption = "Ventas por Períodos"
        Me.e003001001.Id = 116
        Me.e003001001.Name = "e003001001"
        '
        'e003001002
        '
        Me.e003001002.Caption = "Detalle de Ventas por Producto y Cliente"
        Me.e003001002.Id = 117
        Me.e003001002.Name = "e003001002"
        '
        'e003001003
        '
        Me.e003001003.Caption = "Detalle de Ventas por Cliente y Producto"
        Me.e003001003.Id = 118
        Me.e003001003.Name = "e003001003"
        '
        'e003001004
        '
        Me.e003001004.Caption = "Ventas Generales por Cliente y Documento"
        Me.e003001004.Id = 119
        Me.e003001004.Name = "e003001004"
        '
        'e003001005
        '
        Me.e003001005.Caption = "Ventas Consolidadas por Cliente o Producto"
        Me.e003001005.Id = 120
        Me.e003001005.Name = "e003001005"
        '
        'e003001006
        '
        Me.e003001006.Caption = "Ventas por Tipo de Comprobante"
        Me.e003001006.Id = 121
        Me.e003001006.Name = "e003001006"
        '
        'e003001007
        '
        Me.e003001007.Caption = "Ventas por Forma de Pago"
        Me.e003001007.Id = 122
        Me.e003001007.Name = "e003001007"
        '
        'e003001008
        '
        Me.e003001008.Caption = "Informe de Ventas por Vendedor"
        Me.e003001008.Id = 123
        Me.e003001008.Name = "e003001008"
        '
        'e003001009
        '
        Me.e003001009.Caption = "Corte de Caja General"
        Me.e003001009.Id = 124
        Me.e003001009.Name = "e003001009"
        '
        'e003001010
        '
        Me.e003001010.Caption = "Cortes por Ventas con Tickets y Cinta de Auditoría"
        Me.e003001010.Id = 125
        Me.e003001010.Name = "e003001010"
        '
        'e003001011
        '
        Me.e003001011.Caption = "Listado de Pedidos"
        Me.e003001011.Id = 130
        Me.e003001011.Name = "e003001011"
        '
        'e003001012
        '
        Me.e003001012.Caption = "Reportes de Liquidación"
        Me.e003001012.Id = 131
        Me.e003001012.Name = "e003001012"
        '
        'e003001013
        '
        Me.e003001013.Caption = "Generador de Consultas"
        Me.e003001013.Id = 224
        Me.e003001013.Name = "e003001013"
        '
        'e003001014
        '
        Me.e003001014.Caption = "Estadistica de Ventas Anual por Productos"
        Me.e003001014.Id = 235
        Me.e003001014.Name = "e003001014"
        '
        'e003001015
        '
        Me.e003001015.Caption = "Comparación de Ventas Anuales por Producto"
        Me.e003001015.Id = 236
        Me.e003001015.Name = "e003001015"
        '
        'e003001016
        '
        Me.e003001016.Caption = "Ventas Consolidadas por Productos"
        Me.e003001016.Id = 243
        Me.e003001016.Name = "e003001016"
        '
        'e003001017
        '
        Me.e003001017.Caption = "Ventas por Vendedor y Utilidad"
        Me.e003001017.Id = 252
        Me.e003001017.Name = "e003001017"
        '
        'e003001019
        '
        Me.e003001019.Caption = "Ventas por Cliente y Utilidad"
        Me.e003001019.Id = 254
        Me.e003001019.Name = "e003001019"
        '
        'e003001020
        '
        Me.e003001020.Caption = "Devoluciones por Ventas"
        Me.e003001020.Id = 256
        Me.e003001020.Name = "e003001020"
        '
        'e003001021
        '
        Me.e003001021.Caption = "Reporte de Vales"
        Me.e003001021.Id = 308
        Me.e003001021.Name = "e003001021"
        '
        'e003001022
        '
        Me.e003001022.Caption = "Reporte Cotizaciones"
        Me.e003001022.Id = 383
        Me.e003001022.Name = "e003001022"
        '
        'e003001023
        '
        Me.e003001023.Caption = "Reporte Retenciones"
        Me.e003001023.Id = 384
        Me.e003001023.Name = "e003001023"
        '
        'e003002
        '
        Me.e003002.Caption = "Legales"
        Me.e003002.Id = 126
        Me.e003002.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.e003002001), New DevExpress.XtraBars.LinkPersistInfo(Me.e003002002), New DevExpress.XtraBars.LinkPersistInfo(Me.e003002003), New DevExpress.XtraBars.LinkPersistInfo(Me.e003002004), New DevExpress.XtraBars.LinkPersistInfo(Me.e003002005)})
        Me.e003002.Name = "e003002"
        '
        'e003002001
        '
        Me.e003002001.Caption = "Libro de Ventas a Contribuyentes"
        Me.e003002001.Id = 127
        Me.e003002001.Name = "e003002001"
        '
        'e003002002
        '
        Me.e003002002.Caption = "Libro de Ventas a Consumidor Final"
        Me.e003002002.Id = 128
        Me.e003002002.Name = "e003002002"
        '
        'e003002003
        '
        Me.e003002003.Caption = "Listado de Documentos Anulados"
        Me.e003002003.Id = 129
        Me.e003002003.Name = "e003002003"
        '
        'e003002004
        '
        Me.e003002004.Caption = "Liquidación de Impuesto"
        Me.e003002004.Id = 304
        Me.e003002004.Name = "e003002004"
        '
        'e003002005
        '
        Me.e003002005.Caption = "Reporte Ventas Legales"
        Me.e003002005.Id = 369
        Me.e003002005.Name = "e003002005"
        '
        'f001001
        '
        Me.f001001.Caption = "Grupos"
        Me.f001001.Id = 132
        Me.f001001.ImageOptions.ImageIndex = 48
        Me.f001001.Name = "f001001"
        '
        'f001002
        '
        Me.f001002.Caption = "Sub-Grupos"
        Me.f001002.Id = 133
        Me.f001002.ImageOptions.ImageIndex = 73
        Me.f001002.Name = "f001002"
        '
        'f001003
        '
        Me.f001003.Caption = "Precios"
        Me.f001003.Id = 137
        Me.f001003.ImageOptions.ImageIndex = 106
        Me.f001003.Name = "f001003"
        '
        'f001004
        '
        Me.f001004.Caption = "Productos y Servicios"
        Me.f001004.Id = 138
        Me.f001004.ImageOptions.LargeImageIndex = 25
        Me.f001004.Name = "f001004"
        '
        'f002001
        '
        Me.f002001.Caption = "Entradas"
        Me.f002001.Id = 140
        Me.f002001.ImageOptions.LargeImageIndex = 27
        Me.f002001.Name = "f002001"
        '
        'f002002
        '
        Me.f002002.Caption = "Salidas"
        Me.f002002.Id = 141
        Me.f002002.ImageOptions.LargeImageIndex = 28
        Me.f002002.Name = "f002002"
        '
        'f002003
        '
        Me.f002003.Caption = "Traslados"
        Me.f002003.Id = 142
        Me.f002003.ImageOptions.LargeImageIndex = 26
        Me.f002003.Name = "f002003"
        '
        'f002004
        '
        Me.f002004.Caption = "Toma Física de Inventario"
        Me.f002004.Id = 143
        Me.f002004.ImageOptions.ImageIndex = 108
        Me.f002004.Name = "f002004"
        '
        'f002005
        '
        Me.f002005.Caption = "Formulas de Producción"
        Me.f002005.Id = 144
        Me.f002005.ImageOptions.ImageIndex = 91
        Me.f002005.Name = "f002005"
        '
        'f002006
        '
        Me.f002006.Caption = "Contabilizar Transacciones"
        Me.f002006.Id = 145
        Me.f002006.ImageOptions.ImageIndex = 92
        Me.f002006.Name = "f002006"
        '
        'f003001
        '
        Me.f003001.Caption = "Reportes Generales"
        Me.f003001.Id = 146
        Me.f003001.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.f003001001), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001002), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001003), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001004), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001005), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001006), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001007), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001008), New DevExpress.XtraBars.LinkPersistInfo(Me.f00300109), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001010), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001011), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001012), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001014), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001015), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001016), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001017), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001018), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001019), New DevExpress.XtraBars.LinkPersistInfo(Me.f003001020)})
        Me.f003001.Name = "f003001"
        '
        'f003001001
        '
        Me.f003001001.Caption = "Informe de Costos"
        Me.f003001001.Id = 147
        Me.f003001001.Name = "f003001001"
        '
        'f003001002
        '
        Me.f003001002.Caption = "Informe de Ventas y Utilidad"
        Me.f003001002.Id = 148
        Me.f003001002.Name = "f003001002"
        '
        'f003001003
        '
        Me.f003001003.Caption = "Kardex Valuado"
        Me.f003001003.Id = 149
        Me.f003001003.Name = "f003001003"
        '
        'f003001004
        '
        Me.f003001004.Caption = "Kardex sin Valores Monetarios"
        Me.f003001004.Id = 150
        Me.f003001004.Name = "f003001004"
        '
        'f003001005
        '
        Me.f003001005.Caption = "Reporte de Inventario"
        Me.f003001005.Id = 151
        Me.f003001005.Name = "f003001005"
        '
        'f003001006
        '
        Me.f003001006.Caption = "Reporte de Entradas, Salidas y Traslados"
        Me.f003001006.Id = 152
        Me.f003001006.Name = "f003001006"
        '
        'f003001007
        '
        Me.f003001007.Caption = "Cargos y Abonos que Afectan al Inventario"
        Me.f003001007.Id = 153
        Me.f003001007.Name = "f003001007"
        '
        'f003001008
        '
        Me.f003001008.Caption = "Listado de Productos"
        Me.f003001008.Id = 154
        Me.f003001008.Name = "f003001008"
        '
        'f00300109
        '
        Me.f00300109.Caption = "Listado de Productos en Limite de Existencias"
        Me.f00300109.Id = 155
        Me.f00300109.Name = "f00300109"
        '
        'f003001010
        '
        Me.f003001010.Caption = "Listado de Inventario para Toma Física"
        Me.f003001010.Id = 156
        Me.f003001010.Name = "f003001010"
        '
        'f003001011
        '
        Me.f003001011.Caption = "Imprimir Viñetas de Productos"
        Me.f003001011.Id = 157
        Me.f003001011.Name = "f003001011"
        '
        'f003001012
        '
        Me.f003001012.Caption = "Generador de Consultas"
        Me.f003001012.Id = 158
        Me.f003001012.Name = "f003001012"
        '
        'f003001014
        '
        Me.f003001014.Caption = "Minimos y Maximos por Productos"
        Me.f003001014.Id = 257
        Me.f003001014.Name = "f003001014"
        '
        'f003001015
        '
        Me.f003001015.Caption = "Listado de Productos sin Movimientos"
        Me.f003001015.Id = 263
        Me.f003001015.Name = "f003001015"
        '
        'f003001016
        '
        Me.f003001016.Caption = "Listado de Productos por Categorías"
        Me.f003001016.Id = 324
        Me.f003001016.Name = "f003001016"
        '
        'f003001017
        '
        Me.f003001017.Caption = "Existencias por Bodegas"
        Me.f003001017.Id = 371
        Me.f003001017.Name = "f003001017"
        '
        'f003001018
        '
        Me.f003001018.Caption = "Giro del Producto"
        Me.f003001018.Id = 386
        Me.f003001018.Name = "f003001018"
        '
        'f003001019
        '
        Me.f003001019.Caption = "Informe de Ventas y Utillidad Por Documento"
        Me.f003001019.Id = 387
        Me.f003001019.Name = "f003001019"
        '
        'f003001020
        '
        Me.f003001020.Caption = "Reporte de Existencia Extraible"
        Me.f003001020.Id = 391
        Me.f003001020.Name = "f003001020"
        '
        'g001001
        '
        Me.g001001.Caption = "Emitir Quedan"
        Me.g001001.Id = 159
        Me.g001001.ImageOptions.LargeImageIndex = 29
        Me.g001001.Name = "g001001"
        '
        'g002001
        '
        Me.g002001.Caption = "Listado de Quedan"
        Me.g002001.Id = 160
        Me.g002001.ImageOptions.ImageIndex = 113
        Me.g002001.Name = "g002001"
        '
        'g001002
        '
        Me.g001002.Caption = "Cancelaciones de Quedan"
        Me.g001002.Id = 161
        Me.g001002.ImageOptions.ImageIndex = 109
        Me.g001002.Name = "g001002"
        '
        'g001003
        '
        Me.g001003.Caption = "Abonos a proveedores"
        Me.g001003.Id = 163
        Me.g001003.ImageOptions.LargeImageIndex = 16
        Me.g001003.Name = "g001003"
        '
        'g001004
        '
        Me.g001004.Caption = "Anular Abonos"
        Me.g001004.Id = 164
        Me.g001004.ImageOptions.ImageIndex = 110
        Me.g001004.Name = "g001004"
        '
        'g002002
        '
        Me.g002002.Caption = "Listado de Abonos Aplicados"
        Me.g002002.Id = 165
        Me.g002002.ImageOptions.ImageIndex = 77
        Me.g002002.Name = "g002002"
        '
        'g002003
        '
        Me.g002003.Caption = "Análisis de Antigüedad"
        Me.g002003.Id = 166
        Me.g002003.ImageOptions.ImageIndex = 114
        Me.g002003.Name = "g002003"
        '
        'g002004
        '
        Me.g002004.Caption = "Auxiliar de Antigüedad de Saldos"
        Me.g002004.Id = 167
        Me.g002004.ImageOptions.ImageIndex = 115
        Me.g002004.Name = "g002004"
        '
        'g002005
        '
        Me.g002005.Caption = "Historico de Movimientos"
        Me.g002005.Id = 168
        Me.g002005.ImageOptions.LargeImageIndex = 31
        Me.g002005.Name = "g002005"
        '
        'g002006
        '
        Me.g002006.Caption = "Abonos Aplicados por Documento"
        Me.g002006.Id = 169
        Me.g002006.ImageOptions.ImageIndex = 117
        Me.g002006.Name = "g002006"
        '
        'h001001
        '
        Me.h001001.Caption = "Gestores de Cobro"
        Me.h001001.Id = 170
        Me.h001001.ImageOptions.ImageIndex = 53
        Me.h001001.Name = "h001001"
        '
        'h001002
        '
        Me.h001002.Caption = "Tipos de Movimiento"
        Me.h001002.Id = 171
        Me.h001002.ImageOptions.ImageIndex = 75
        Me.h001002.Name = "h001002"
        '
        'h002001
        '
        Me.h002001.Caption = "Aplicar Abonos"
        Me.h002001.Id = 172
        Me.h002001.ImageOptions.LargeImageIndex = 33
        Me.h002001.Name = "h002001"
        '
        'h002003
        '
        Me.h002003.Caption = "Aplicar Cargos"
        Me.h002003.Id = 173
        Me.h002003.ImageOptions.ImageIndex = 83
        Me.h002003.Name = "h002003"
        '
        'h002002
        '
        Me.h002002.Caption = "Anular Abonos"
        Me.h002002.Id = 174
        Me.h002002.ImageOptions.ImageIndex = 110
        Me.h002002.Name = "h002002"
        '
        'h002004
        '
        Me.h002004.Caption = "Anular Cargos"
        Me.h002004.Id = 175
        Me.h002004.ImageOptions.ImageIndex = 101
        Me.h002004.Name = "h002004"
        '
        'h003001
        '
        Me.h003001.Caption = "Listado de Clientes"
        Me.h003001.Id = 176
        Me.h003001.ImageOptions.ImageIndex = 51
        Me.h003001.Name = "h003001"
        '
        'h003002
        '
        Me.h003002.Caption = "Informe de Documentos al Crédito"
        Me.h003002.Id = 177
        Me.h003002.ImageOptions.ImageIndex = 29
        Me.h003002.Name = "h003002"
        '
        'h003003
        '
        Me.h003003.Caption = "Informe de Abonos aplicados"
        Me.h003003.Id = 178
        Me.h003003.ImageOptions.ImageIndex = 54
        Me.h003003.Name = "h003003"
        '
        'h003004
        '
        Me.h003004.Caption = "Informe de Cargos Aplicados"
        Me.h003004.Id = 179
        Me.h003004.ImageOptions.ImageIndex = 62
        Me.h003004.Name = "h003004"
        '
        'h003005
        '
        Me.h003005.Caption = "Informe de Documentos Cancelados"
        Me.h003005.Id = 180
        Me.h003005.ImageOptions.ImageIndex = 86
        Me.h003005.Name = "h003005"
        '
        'h003006
        '
        Me.h003006.Caption = "Estado de Cuenta"
        Me.h003006.Id = 181
        Me.h003006.ImageOptions.ImageIndex = 66
        Me.h003006.Name = "h003006"
        '
        'h003007
        '
        Me.h003007.Caption = "Historico de Movimientos"
        Me.h003007.Id = 182
        Me.h003007.ImageOptions.LargeImageIndex = 31
        Me.h003007.Name = "h003007"
        '
        'h003008
        '
        Me.h003008.Caption = "Análisis de antigüedad de Saldos"
        Me.h003008.Id = 183
        Me.h003008.ImageOptions.ImageIndex = 114
        Me.h003008.Name = "h003008"
        '
        'h003009
        '
        Me.h003009.Caption = "Auxiliar de Antigüedad de Saldos"
        Me.h003009.Id = 184
        Me.h003009.ImageOptions.ImageIndex = 115
        Me.h003009.Name = "h003009"
        '
        'h003010
        '
        Me.h003010.Caption = "Comisiones por Cobranza"
        Me.h003010.Id = 185
        Me.h003010.ImageOptions.ImageIndex = 118
        Me.h003010.Name = "h003010"
        '
        'g001005
        '
        Me.g001005.Caption = "Autorizar Abonos"
        Me.g001005.Id = 186
        Me.g001005.ImageOptions.ImageIndex = 112
        Me.g001005.Name = "g001005"
        '
        'e002008
        '
        Me.e002008.Caption = "Contabilizar Facturación"
        Me.e002008.Id = 188
        Me.e002008.ImageOptions.ImageIndex = 92
        Me.e002008.Name = "e002008"
        '
        'e001004
        '
        Me.e001004.Caption = "Asignación de Documentos"
        Me.e001004.Id = 190
        Me.e001004.ImageOptions.ImageIndex = 1
        Me.e001004.Name = "e001004"
        '
        'b002007
        '
        Me.b002007.Caption = "Generar Archivos ICV"
        Me.b002007.Id = 196
        Me.b002007.ImageOptions.ImageIndex = 84
        Me.b002007.Name = "b002007"
        '
        'i001001
        '
        Me.i001001.Caption = "Clases de Activos"
        Me.i001001.Id = 197
        Me.i001001.ImageOptions.ImageIndex = 119
        Me.i001001.Name = "i001001"
        '
        'i001002
        '
        Me.i001002.Caption = "Ubicaciones"
        Me.i001002.Id = 198
        Me.i001002.ImageOptions.ImageIndex = 94
        Me.i001002.Name = "i001002"
        '
        'i001003
        '
        Me.i001003.Caption = "Tipos de Depreciación"
        Me.i001003.Id = 199
        Me.i001003.ImageOptions.ImageIndex = 75
        Me.i001003.Name = "i001003"
        '
        'i001004
        '
        Me.i001004.Caption = "Marcas"
        Me.i001004.Id = 200
        Me.i001004.ImageOptions.ImageIndex = 103
        Me.i001004.Name = "i001004"
        '
        'i001005
        '
        Me.i001005.Caption = "Modelos"
        Me.i001005.Id = 201
        Me.i001005.ImageOptions.ImageIndex = 120
        Me.i001005.Name = "i001005"
        '
        'i001006
        '
        Me.i001006.Caption = "Estilos"
        Me.i001006.Id = 202
        Me.i001006.ImageOptions.ImageIndex = 121
        Me.i001006.Name = "i001006"
        '
        'i001007
        '
        Me.i001007.Caption = "Técnicos y Empresas de Soporte"
        Me.i001007.Id = 203
        Me.i001007.ImageOptions.ImageIndex = 39
        Me.i001007.Name = "i001007"
        '
        'i001008
        '
        Me.i001008.Caption = "Tipos de Fallas y Reparaciones"
        Me.i001008.Id = 204
        Me.i001008.ImageOptions.ImageIndex = 13
        Me.i001008.Name = "i001008"
        '
        'i001009
        '
        Me.i001009.Caption = "Tipos de Estado de los Activos"
        Me.i001009.Id = 205
        Me.i001009.ImageOptions.ImageIndex = 112
        Me.i001009.Name = "i001009"
        '
        'i002001
        '
        Me.i002001.Caption = "Activos Fijos"
        Me.i002001.Id = 206
        Me.i002001.ImageOptions.LargeImageIndex = 34
        Me.i002001.Name = "i002001"
        '
        'i002002
        '
        Me.i002002.Caption = "Revaluaciones"
        Me.i002002.Id = 207
        Me.i002002.ImageOptions.ImageIndex = 123
        Me.i002002.Name = "i002002"
        '
        'i002003
        '
        Me.i002003.Caption = "Mantenimientos"
        Me.i002003.Id = 208
        Me.i002003.ImageOptions.ImageIndex = 122
        Me.i002003.Name = "i002003"
        '
        'i002004
        '
        Me.i002004.Caption = "Retiro de Activos"
        Me.i002004.Id = 209
        Me.i002004.ImageOptions.LargeImageIndex = 39
        Me.i002004.Name = "i002004"
        '
        'i002005
        '
        Me.i002005.Caption = "Venta de Activos"
        Me.i002005.Id = 210
        Me.i002005.ImageOptions.LargeImageIndex = 40
        Me.i002005.Name = "i002005"
        '
        'i002006
        '
        Me.i002006.Caption = "Contabilizar"
        Me.i002006.Id = 211
        Me.i002006.ImageOptions.ImageIndex = 92
        Me.i002006.Name = "i002006"
        '
        'i002007
        '
        Me.i002007.Caption = "Revertir"
        Me.i002007.Id = 212
        Me.i002007.ImageOptions.ImageIndex = 11
        Me.i002007.Name = "i002007"
        '
        'i003001
        '
        Me.i003001.Caption = "Listado General"
        Me.i003001.Id = 213
        Me.i003001.ImageOptions.ImageIndex = 82
        Me.i003001.Name = "i003001"
        '
        'i002008
        '
        Me.i002008.Caption = "Cuadro de Diferencias"
        Me.i002008.Id = 214
        Me.i002008.ImageOptions.LargeImageIndex = 35
        Me.i002008.Name = "i002008"
        '
        'i003002
        '
        Me.i003002.Caption = "Activos por Cuenta"
        Me.i003002.Id = 215
        Me.i003002.ImageOptions.ImageIndex = 30
        Me.i003002.Name = "i003002"
        '
        'i003003
        '
        Me.i003003.Caption = "Depreciación Individual"
        Me.i003003.Id = 216
        Me.i003003.ImageOptions.ImageIndex = 40
        Me.i003003.Name = "i003003"
        '
        'i003004
        '
        Me.i003004.Caption = "Depreciación General"
        Me.i003004.Id = 217
        Me.i003004.ImageOptions.ImageIndex = 66
        Me.i003004.Name = "i003004"
        '
        'i003005
        '
        Me.i003005.Caption = "Activos Depreciados"
        Me.i003005.Id = 218
        Me.i003005.ImageOptions.ImageIndex = 99
        Me.i003005.Name = "i003005"
        '
        'i003006
        '
        Me.i003006.Caption = "Retirados"
        Me.i003006.Id = 219
        Me.i003006.ImageOptions.ImageIndex = 12
        Me.i003006.Name = "i003006"
        '
        'i003007
        '
        Me.i003007.Caption = "Vendidos"
        Me.i003007.Id = 220
        Me.i003007.ImageOptions.ImageIndex = 41
        Me.i003007.Name = "i003007"
        '
        'i003008
        '
        Me.i003008.Caption = "Mantenimientos"
        Me.i003008.Id = 221
        Me.i003008.ImageOptions.ImageIndex = 38
        Me.i003008.Name = "i003008"
        '
        'i003009
        '
        Me.i003009.Caption = "Otros Reportes"
        Me.i003009.Id = 222
        Me.i003009.ImageOptions.ImageIndex = 48
        Me.i003009.Name = "i003009"
        '
        'b001004
        '
        Me.b001004.Caption = "Rubros del Flujo de Efectivo"
        Me.b001004.Id = 223
        Me.b001004.ImageOptions.ImageIndex = 46
        Me.b001004.Name = "b001004"
        '
        'b001007
        '
        Me.b001007.Caption = "Excluir Cuentas"
        Me.b001007.Id = 225
        Me.b001007.ImageOptions.ImageIndex = 11
        Me.b001007.Name = "b001007"
        '
        'b001008
        '
        Me.b001008.Caption = "Conceptos Corte Caja"
        Me.b001008.Id = 226
        Me.b001008.ImageOptions.ImageIndex = 18
        Me.b001008.Name = "b001008"
        '
        'b001005
        '
        Me.b001005.Caption = "Cuentas de Patrimonio"
        Me.b001005.Id = 228
        Me.b001005.ImageOptions.ImageIndex = 78
        Me.b001005.Name = "b001005"
        '
        'd002005
        '
        Me.d002005.Caption = "Requisiciones OC"
        Me.d002005.Id = 229
        Me.d002005.ImageOptions.ImageIndex = 48
        Me.d002005.Name = "d002005"
        '
        'd001002
        '
        Me.d001002.Caption = "Gastos de Importaciones"
        Me.d001002.Id = 230
        Me.d001002.ImageOptions.ImageIndex = 60
        Me.d001002.Name = "d001002"
        '
        'e002009
        '
        Me.e002009.Caption = "Token de Descuentos"
        Me.e002009.Id = 232
        Me.e002009.ImageOptions.ImageIndex = 100
        Me.e002009.Name = "e002009"
        '
        'h003011
        '
        Me.h003011.Caption = "Abonos por Documento"
        Me.h003011.Id = 233
        Me.h003011.ImageOptions.ImageIndex = 43
        Me.h003011.Name = "h003011"
        '
        'f003001013
        '
        Me.f003001013.Caption = "Estadistica de Ventas Anuales por Producto"
        Me.f003001013.Id = 234
        Me.f003001013.Name = "f003001013"
        '
        'g001006
        '
        Me.g001006.Caption = "Autorizar Abonos Multiples"
        Me.g001006.Id = 238
        Me.g001006.ImageOptions.ImageIndex = 111
        Me.g001006.Name = "g001006"
        '
        'f002007
        '
        Me.f002007.Caption = "Producciones"
        Me.f002007.Id = 239
        Me.f002007.ImageOptions.LargeImageIndex = 42
        Me.f002007.Name = "f002007"
        '
        'd002006
        '
        Me.d002006.Caption = "Doctos. de Liquidación"
        Me.d002006.Id = 240
        Me.d002006.ImageOptions.ImageIndex = 109
        Me.d002006.Name = "d002006"
        '
        'd002007
        '
        Me.d002007.Caption = "Comprob. de Retención"
        Me.d002007.Id = 241
        Me.d002007.ImageOptions.ImageIndex = 83
        Me.d002007.Name = "d002007"
        '
        'd001003
        '
        Me.d001003.Caption = "Cajas Chica"
        Me.d001003.Id = 242
        Me.d001003.ImageOptions.ImageIndex = 47
        Me.d001003.Name = "d001003"
        '
        'd001004
        '
        Me.d001004.Caption = "Tipos de Impuestos"
        Me.d001004.Id = 245
        Me.d001004.ImageOptions.ImageIndex = 86
        Me.d001004.Name = "d001004"
        '
        'i002009
        '
        Me.i002009.Caption = "Traslados de Activo"
        Me.i002009.Id = 248
        Me.i002009.ImageOptions.ImageIndex = 78
        Me.i002009.Name = "i002009"
        '
        'e002010
        '
        Me.e002010.Caption = "Pre Facturacion"
        Me.e002010.Id = 249
        Me.e002010.ImageOptions.ImageIndex = 34
        Me.e002010.Name = "e002010"
        '
        'e002011
        '
        Me.e002011.Caption = "Consulta Productos"
        Me.e002011.Id = 250
        Me.e002011.ImageOptions.ImageIndex = 90
        Me.e002011.Name = "e002011"
        '
        'f002008
        '
        Me.f002008.Caption = "Configurar Precios"
        Me.f002008.Id = 251
        Me.f002008.ImageOptions.ImageIndex = 106
        Me.f002008.Name = "f002008"
        ToolTipTitleItem12.Text = "Precios"
        ToolTipItem12.LeftIndent = 6
        ToolTipItem12.Text = "Permite configurar los precios de venta, tenga en cuenta que estos precios ya deb" & _
    "en incluir los respectivos impuestos."
        ToolTipTitleItem13.LeftIndent = 6
        ToolTipTitleItem13.Text = "Configuración de precios"
        SuperToolTip12.Items.Add(ToolTipTitleItem12)
        SuperToolTip12.Items.Add(ToolTipItem12)
        SuperToolTip12.Items.Add(ToolTipSeparatorItem1)
        SuperToolTip12.Items.Add(ToolTipTitleItem13)
        Me.f002008.SuperTip = SuperToolTip12
        '
        'e003001018
        '
        Me.e003001018.Caption = "Corte de Caja"
        Me.e003001018.Id = 253
        Me.e003001018.Name = "e003001018"
        '
        'h002005
        '
        Me.h002005.Caption = "Contabilizar"
        Me.h002005.Id = 258
        Me.h002005.ImageOptions.ImageIndex = 92
        Me.h002005.Name = "h002005"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Donde está?"
        Me.BarButtonItem1.Id = 259
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'e002012
        '
        Me.e002012.Caption = "Estadistica de Ventas"
        Me.e002012.Id = 260
        Me.e002012.ImageOptions.ImageIndex = 114
        Me.e002012.Name = "e002012"
        '
        'e002013
        '
        Me.e002013.Caption = "Actualizar Info del Comprob."
        Me.e002013.Id = 261
        Me.e002013.ImageOptions.ImageIndex = 95
        Me.e002013.Name = "e002013"
        '
        'd002008
        '
        Me.d002008.Caption = "Contabilizar Doc. Liquidacion"
        Me.d002008.Id = 262
        Me.d002008.ImageOptions.ImageIndex = 96
        Me.d002008.Name = "d002008"
        '
        'd002009
        '
        Me.d002009.Caption = "Autorizar Orden de Compra"
        Me.d002009.Id = 265
        Me.d002009.ImageOptions.ImageIndex = 112
        Me.d002009.Name = "d002009"
        '
        'a001007
        '
        Me.a001007.Caption = "Unidades de Medida"
        Me.a001007.Id = 266
        Me.a001007.ImageOptions.ImageIndex = 102
        Me.a001007.Name = "a001007"
        '
        'a001008
        '
        Me.a001008.Caption = "Ubicaciones y Estantes"
        Me.a001008.Id = 267
        Me.a001008.ImageOptions.ImageIndex = 104
        Me.a001008.Name = "a001008"
        '
        'a001009
        '
        Me.a001009.Caption = "Marcas"
        Me.a001009.Id = 268
        Me.a001009.ImageOptions.ImageIndex = 103
        Me.a001009.Name = "a001009"
        '
        'a001010
        '
        Me.a001010.Caption = "Bodegas"
        Me.a001010.Id = 269
        Me.a001010.ImageOptions.ImageIndex = 105
        Me.a001010.Name = "a001010"
        '
        'a001011
        '
        Me.a001011.Caption = "Cambiar Contraseña"
        Me.a001011.Id = 272
        Me.a001011.ImageOptions.ImageIndex = 123
        Me.a001011.Name = "a001011"
        '
        'b004001
        '
        Me.b004001.Caption = "Archivos"
        Me.b004001.Id = 289
        Me.b004001.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.b004001001), New DevExpress.XtraBars.LinkPersistInfo(Me.b004001002)})
        Me.b004001.Name = "b004001"
        '
        'b004001001
        '
        Me.b004001001.Caption = "Departamentos"
        Me.b004001001.Id = 292
        Me.b004001001.Name = "b004001001"
        '
        'b004001002
        '
        Me.b004001002.Caption = "Centros de Costos"
        Me.b004001002.Id = 293
        Me.b004001002.Name = "b004001002"
        '
        'b004002
        '
        Me.b004002.Caption = "Procesos"
        Me.b004002.Id = 290
        Me.b004002.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.b004002001), New DevExpress.XtraBars.LinkPersistInfo(Me.b004002002)})
        Me.b004002.Name = "b004002"
        '
        'b004002001
        '
        Me.b004002001.Caption = "Configuración de Presupuesto"
        Me.b004002001.Id = 294
        Me.b004002001.Name = "b004002001"
        '
        'b004002002
        '
        Me.b004002002.Caption = "Configuración de Presupuesto por Cta. Contable"
        Me.b004002002.Id = 374
        Me.b004002002.Name = "b004002002"
        '
        'b004003
        '
        Me.b004003.Caption = "Reportes"
        Me.b004003.Id = 291
        Me.b004003.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.b004003001), New DevExpress.XtraBars.LinkPersistInfo(Me.b004003002), New DevExpress.XtraBars.LinkPersistInfo(Me.b004003003), New DevExpress.XtraBars.LinkPersistInfo(Me.b004003004), New DevExpress.XtraBars.LinkPersistInfo(Me.b004003005), New DevExpress.XtraBars.LinkPersistInfo(Me.b004003006)})
        Me.b004003.Name = "b004003"
        '
        'b004003001
        '
        Me.b004003001.Caption = "Presupuesto Géneral"
        Me.b004003001.Id = 295
        Me.b004003001.Name = "b004003001"
        '
        'b004003002
        '
        Me.b004003002.Caption = "Detalle de Presupuesto con Costos Reales"
        Me.b004003002.Id = 296
        Me.b004003002.Name = "b004003002"
        '
        'b004003003
        '
        Me.b004003003.Caption = "Presupuesto Anual por Sucursal"
        Me.b004003003.Id = 297
        Me.b004003003.Name = "b004003003"
        '
        'b004003004
        '
        Me.b004003004.Caption = "Presupuesto Anual por Departamento"
        Me.b004003004.Id = 298
        Me.b004003004.Name = "b004003004"
        '
        'b004003005
        '
        Me.b004003005.Caption = "Presupuestos Vs. Costos Reales"
        Me.b004003005.Id = 299
        Me.b004003005.Name = "b004003005"
        '
        'b004003006
        '
        Me.b004003006.Caption = "Presupuestos vs. Costos Reales por Cuenta"
        Me.b004003006.Id = 373
        Me.b004003006.Name = "b004003006"
        '
        'a001012
        '
        Me.a001012.Caption = " Nueva Empresa"
        Me.a001012.Id = 300
        Me.a001012.ImageOptions.LargeImage = Global.Nexus.My.Resources.Resources.icon_2
        Me.a001012.Name = "a001012"
        '
        'f001005
        '
        Me.f001005.Caption = "Margenes Descuento"
        Me.f001005.Id = 302
        Me.f001005.ImageOptions.ImageIndex = 100
        Me.f001005.Name = "f001005"
        '
        'g002007
        '
        Me.g002007.Caption = "Estado de Cuenta"
        Me.g002007.Id = 303
        Me.g002007.Name = "g002007"
        '
        'a001013
        '
        Me.a001013.Caption = "Bitacora Sucesos"
        Me.a001013.Id = 305
        Me.a001013.ImageOptions.ImageIndex = 114
        Me.a001013.Name = "a001013"
        '
        'e001005
        '
        Me.e001005.Caption = "Vales"
        Me.e001005.Id = 307
        Me.e001005.ImageOptions.ImageIndex = 27
        Me.e001005.Name = "e001005"
        '
        'j002001
        '
        Me.j002001.Caption = "Orden de Producción"
        Me.j002001.Id = 309
        Me.j002001.Name = "j002001"
        '
        'j001001
        '
        Me.j001001.Caption = "Actividades de Producción"
        Me.j001001.Id = 310
        Me.j001001.Name = "j001001"
        '
        'j002002
        '
        Me.j002002.Caption = "Requisición de Producción"
        Me.j002002.Id = 311
        Me.j002002.Name = "j002002"
        '
        'j002003
        '
        Me.j002003.Caption = "Remisión de Producción"
        Me.j002003.Id = 312
        Me.j002003.Name = "j002003"
        '
        'j001002
        '
        Me.j001002.Caption = "Gastos de Producción"
        Me.j001002.Id = 313
        Me.j001002.Name = "j001002"
        '
        'j002004
        '
        Me.j002004.Caption = "Orden de Empaque"
        Me.j002004.Id = 314
        Me.j002004.Name = "j002004"
        '
        'h002006
        '
        Me.h002006.Caption = "Abonos por Liquidación"
        Me.h002006.Id = 316
        Me.h002006.ImageOptions.ImageIndex = 75
        Me.h002006.Name = "h002006"
        '
        'e003003
        '
        Me.e003003.Caption = "Más Reportes"
        Me.e003003.Id = 319
        Me.e003003.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.e003003001), New DevExpress.XtraBars.LinkPersistInfo(Me.e003003002), New DevExpress.XtraBars.LinkPersistInfo(Me.e003003003), New DevExpress.XtraBars.LinkPersistInfo(Me.e003003004), New DevExpress.XtraBars.LinkPersistInfo(Me.e003003005), New DevExpress.XtraBars.LinkPersistInfo(Me.e003003006), New DevExpress.XtraBars.LinkPersistInfo(Me.e003003007), New DevExpress.XtraBars.LinkPersistInfo(Me.e003003008), New DevExpress.XtraBars.LinkPersistInfo(Me.e003003009), New DevExpress.XtraBars.LinkPersistInfo(Me.e003003010)})
        Me.e003003.Name = "e003003"
        '
        'e003003001
        '
        Me.e003003001.Caption = "Comparativo Mensual de Ventas"
        Me.e003003001.Id = 322
        Me.e003003001.Name = "e003003001"
        '
        'e003003002
        '
        Me.e003003002.Caption = "Comparativo Anual de Ventas"
        Me.e003003002.Id = 348
        Me.e003003002.Name = "e003003002"
        '
        'e003003003
        '
        Me.e003003003.Caption = "Generador de Consultas (Export Data)"
        Me.e003003003.Id = 349
        Me.e003003003.Name = "e003003003"
        '
        'e003003004
        '
        Me.e003003004.Caption = "Listado de Gestiones"
        Me.e003003004.Id = 354
        Me.e003003004.Name = "e003003004"
        '
        'e003003005
        '
        Me.e003003005.Caption = "Informe de Ventas y Utilidad Detallado"
        Me.e003003005.Id = 365
        Me.e003003005.Name = "e003003005"
        '
        'e003003006
        '
        Me.e003003006.Caption = "Ventas Por Sucursal"
        Me.e003003006.Id = 366
        Me.e003003006.Name = "e003003006"
        '
        'e003003007
        '
        Me.e003003007.Caption = "Listado de Autorizaciones por TOKEN"
        Me.e003003007.Id = 368
        Me.e003003007.Name = "e003003007"
        '
        'e003003008
        '
        Me.e003003008.Caption = "Listado Gestiones - Prospectos"
        Me.e003003008.Id = 376
        Me.e003003008.Name = "e003003008"
        '
        'e003003009
        '
        Me.e003003009.Caption = "Consulta Formas Pago"
        Me.e003003009.Id = 388
        Me.e003003009.Name = "e003003009"
        '
        'e003003010
        '
        Me.e003003010.Caption = "Ventas por Forma de pago y Zona"
        Me.e003003010.Id = 393
        Me.e003003010.Name = "e003003010"
        '
        'f001006
        '
        Me.f001006.Caption = "Categorías"
        Me.f001006.Id = 323
        Me.f001006.ImageOptions.ImageIndex = 108
        Me.f001006.Name = "f001006"
        '
        'c002006
        '
        Me.c002006.Caption = "Autorizar Cheques"
        Me.c002006.Id = 326
        Me.c002006.ImageOptions.ImageIndex = 109
        Me.c002006.Name = "c002006"
        '
        'd003001
        '
        Me.d003001.Caption = "Reportes Generales"
        Me.d003001.Id = 327
        Me.d003001.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.d003001001), New DevExpress.XtraBars.LinkPersistInfo(Me.d003001002), New DevExpress.XtraBars.LinkPersistInfo(Me.d003001003), New DevExpress.XtraBars.LinkPersistInfo(Me.d003001004), New DevExpress.XtraBars.LinkPersistInfo(Me.d003001005), New DevExpress.XtraBars.LinkPersistInfo(Me.d003001006), New DevExpress.XtraBars.LinkPersistInfo(Me.d003001007), New DevExpress.XtraBars.LinkPersistInfo(Me.d003001008), New DevExpress.XtraBars.LinkPersistInfo(Me.d003001009), New DevExpress.XtraBars.LinkPersistInfo(Me.d003001010), New DevExpress.XtraBars.LinkPersistInfo(Me.d003001011)})
        Me.d003001.Name = "d003001"
        '
        'd003001001
        '
        Me.d003001001.Caption = "Compras por Período y Proveedor"
        Me.d003001001.Id = 328
        Me.d003001001.ImageOptions.ImageIndex = 82
        Me.d003001001.Name = "d003001001"
        '
        'd003001002
        '
        Me.d003001002.Caption = "Compras por Período y Producto"
        Me.d003001002.Id = 329
        Me.d003001002.ImageOptions.ImageIndex = 62
        Me.d003001002.Name = "d003001002"
        '
        'd003001003
        '
        Me.d003001003.Caption = "Detalle por Proveedor y Producto"
        Me.d003001003.Id = 330
        Me.d003001003.ImageOptions.ImageIndex = 66
        Me.d003001003.Name = "d003001003"
        '
        'd003001004
        '
        Me.d003001004.Caption = "Detalle por Producto y Proveedor"
        Me.d003001004.Id = 332
        Me.d003001004.ImageOptions.ImageIndex = 88
        Me.d003001004.Name = "d003001004"
        '
        'd003001005
        '
        Me.d003001005.Caption = "Compras TOP a Proveedores"
        Me.d003001005.Id = 333
        Me.d003001005.ImageOptions.ImageIndex = 111
        Me.d003001005.Name = "d003001005"
        '
        'd003001006
        '
        Me.d003001006.Caption = "Compras TOP de Productos"
        Me.d003001006.Id = 334
        Me.d003001006.ImageOptions.ImageIndex = 96
        Me.d003001006.Name = "d003001006"
        '
        'd003001007
        '
        Me.d003001007.Caption = "Generador de Consultas"
        Me.d003001007.Id = 335
        Me.d003001007.ImageOptions.ImageIndex = 90
        Me.d003001007.Name = "d003001007"
        '
        'd003001008
        '
        Me.d003001008.Caption = "Compras a Sujetos Excluidos"
        Me.d003001008.Id = 336
        Me.d003001008.ImageOptions.ImageIndex = 115
        Me.d003001008.Name = "d003001008"
        '
        'd003001009
        '
        Me.d003001009.Caption = "Compras Caja Chica"
        Me.d003001009.Id = 337
        Me.d003001009.ImageOptions.ImageIndex = 109
        Me.d003001009.Name = "d003001009"
        '
        'd003001010
        '
        Me.d003001010.Caption = "Reporte de Importaciones"
        Me.d003001010.Id = 346
        Me.d003001010.ImageOptions.ImageIndex = 117
        Me.d003001010.Name = "d003001010"
        '
        'd003001011
        '
        Me.d003001011.Caption = "Informe de Retenciones"
        Me.d003001011.Id = 394
        Me.d003001011.ImageOptions.ImageIndex = 71
        Me.d003001011.Name = "d003001011"
        '
        'd003002
        '
        Me.d003002.Caption = "Legales"
        Me.d003002.Id = 338
        Me.d003002.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.d003002001, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.d003002002)})
        Me.d003002.Name = "d003002"
        '
        'd003002001
        '
        Me.d003002001.Caption = "Libro de Compras"
        Me.d003002001.Id = 339
        Me.d003002001.ImageOptions.ImageIndex = 33
        Me.d003002001.Name = "d003002001"
        '
        'd003002002
        '
        Me.d003002002.Caption = "Archivo F930"
        Me.d003002002.Id = 341
        Me.d003002002.ImageOptions.ImageIndex = 116
        Me.d003002002.Name = "d003002002"
        '
        'e001006
        '
        Me.e001006.Caption = "Apertura de Caja"
        Me.e001006.Id = 342
        Me.e001006.ImageOptions.ImageIndex = 124
        Me.e001006.Name = "e001006"
        '
        'h003012
        '
        Me.h003012.Caption = "Informe de Abonos Anulados"
        Me.h003012.Id = 343
        Me.h003012.ImageOptions.ImageIndex = 35
        Me.h003012.Name = "h003012"
        '
        'g002008
        '
        Me.g002008.Caption = "Listado de Abonos Anulados"
        Me.g002008.Id = 344
        Me.g002008.ImageOptions.ImageIndex = 35
        Me.g002008.Name = "g002008"
        '
        'h002007
        '
        Me.h002007.Caption = "Asignar Docs. Cobros"
        Me.h002007.Id = 345
        Me.h002007.ImageOptions.ImageIndex = 1
        Me.h002007.Name = "h002007"
        '
        'f002009
        '
        Me.f002009.Caption = "Re-Costeos/Re-calculos"
        Me.f002009.Id = 347
        Me.f002009.Name = "f002009"
        ToolTipTitleItem14.Text = "Re-Costeo"
        ToolTipItem13.LeftIndent = 6
        ToolTipItem13.Text = "Este proceso le permite ajustar nuevamente o recalcular sus precios de costo, bas" & _
    "ados en sus compras y el metodo de valuación que se ha configurado para esta ver" & _
    "sión de Nexus"
        ToolTipTitleItem15.LeftIndent = 6
        ToolTipTitleItem15.Text = "Re-Calculo de Costos"
        SuperToolTip13.Items.Add(ToolTipTitleItem14)
        SuperToolTip13.Items.Add(ToolTipItem13)
        SuperToolTip13.Items.Add(ToolTipTitleItem15)
        Me.f002009.SuperTip = SuperToolTip13
        Me.f002009.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'c002007
        '
        Me.c002007.Caption = "Contabilizar Registros"
        Me.c002007.Id = 350
        Me.c002007.Name = "c002007"
        '
        'e004001
        '
        Me.e004001.Caption = "Gestiones"
        Me.e004001.Id = 351
        Me.e004001.Name = "e004001"
        '
        'e004002
        '
        Me.e004002.Caption = "Programar Gestiones"
        Me.e004002.Id = 352
        Me.e004002.Name = "e004002"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "BarButtonItem4"
        Me.BarButtonItem4.Id = 353
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'qbRefresh
        '
        Me.qbRefresh.Caption = "Actualizar"
        Me.qbRefresh.Id = 355
        Me.qbRefresh.ImageOptions.ImageIndex = 127
        Me.qbRefresh.Name = "qbRefresh"
        ToolTipTitleItem16.Text = "Actualizar Consulta"
        ToolTipItem14.LeftIndent = 6
        ToolTipItem14.Text = "Actualiza la consulta del formulario actual"
        SuperToolTip14.Items.Add(ToolTipTitleItem16)
        SuperToolTip14.Items.Add(ToolTipItem14)
        Me.qbRefresh.SuperTip = SuperToolTip14
        '
        'd002010
        '
        Me.d002010.Caption = "Contabilizar Compras Caja Chica"
        Me.d002010.Id = 357
        Me.d002010.ImageOptions.ImageIndex = 92
        Me.d002010.Name = "d002010"
        '
        'a001014
        '
        Me.a001014.Caption = "Actividades Económicas"
        Me.a001014.Id = 358
        Me.a001014.Name = "a001014"
        '
        'a001015
        '
        Me.a001015.Caption = "Profesiones"
        Me.a001015.Id = 359
        Me.a001015.Name = "a001015"
        '
        'a001016
        '
        Me.a001016.Caption = "BarButtonItem2"
        Me.a001016.Id = 360
        Me.a001016.Name = "a001016"
        '
        'a002001
        '
        Me.a002001.Caption = "Actividades Económicas"
        Me.a002001.Id = 361
        Me.a002001.Name = "a002001"
        '
        'a002002
        '
        Me.a002002.Caption = "Profesiones"
        Me.a002002.Id = 362
        Me.a002002.Name = "a002002"
        '
        'a002003
        '
        Me.a002003.Caption = "Ctas. de Correos Alertas"
        Me.a002003.Id = 363
        Me.a002003.Name = "a002003"
        '
        'a002004
        '
        Me.a002004.Caption = "BarButtonItem6"
        Me.a002004.Id = 364
        Me.a002004.Name = "a002004"
        '
        'f002010
        '
        Me.f002010.Caption = "Aprobación de Traslados"
        Me.f002010.Id = 367
        Me.f002010.ImageOptions.LargeImageIndex = 22
        Me.f002010.Name = "f002010"
        '
        'f001007
        '
        Me.f001007.Caption = "Creacion de Productos"
        Me.f001007.Id = 370
        Me.f001007.ImageOptions.ImageIndex = 84
        Me.f001007.Name = "f001007"
        '
        'e002014
        '
        Me.e002014.Caption = "Aprobar Venta al Credito"
        Me.e002014.Id = 372
        Me.e002014.ImageOptions.ImageIndex = 87
        Me.e002014.Name = "e002014"
        '
        'e004003
        '
        Me.e004003.Caption = "Prospectación de Clientes"
        Me.e004003.Id = 375
        Me.e004003.Name = "e004003"
        '
        'b002009
        '
        Me.b002009.Caption = "Fórmulas Ratios Financieros"
        Me.b002009.Id = 378
        Me.b002009.ImageOptions.ImageIndex = 95
        Me.b002009.Name = "b002009"
        '
        'h002008
        '
        Me.h002008.Caption = "Ingreso Retención"
        Me.h002008.Id = 379
        Me.h002008.ImageOptions.ImageIndex = 36
        Me.h002008.Name = "h002008"
        '
        'h002009
        '
        Me.h002009.Caption = "Contabilizar Comp. Retencion"
        Me.h002009.Id = 380
        Me.h002009.ImageOptions.ImageIndex = 65
        Me.h002009.Name = "h002009"
        '
        'h003013
        '
        Me.h003013.Caption = "Imforme Comp. retención"
        Me.h003013.Id = 381
        Me.h003013.ImageOptions.ImageIndex = 96
        Me.h003013.Name = "h003013"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "BarButtonItem2"
        Me.BarButtonItem2.Id = 382
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'e004004
        '
        Me.e004004.Caption = "Consulta Tipo Reporte"
        Me.e004004.Id = 385
        Me.e004004.Name = "e004004"
        '
        'g001007
        '
        Me.g001007.Caption = "Autorizar Abonos Seccionados"
        Me.g001007.Id = 389
        Me.g001007.ImageOptions.LargeImage = Global.Nexus.My.Resources.Resources._1666659
        Me.g001007.Name = "g001007"
        '
        'g001008
        '
        Me.g001008.Caption = "Traslado de Saldos"
        Me.g001008.Id = 395
        Me.g001008.ImageOptions.LargeImage = Global.Nexus.My.Resources.Resources.gobby
        Me.g001008.Name = "g001008"
        '
        'ImageCollection32x32
        '
        Me.ImageCollection32x32.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageCollection32x32.ImageStream = CType(resources.GetObject("ImageCollection32x32.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.ImageCollection32x32.Images.SetKeyName(0, "evolution-tasks.png")
        Me.ImageCollection32x32.Images.SetKeyName(1, "address-book-new-3.png")
        Me.ImageCollection32x32.Images.SetKeyName(2, "edit-group.png")
        Me.ImageCollection32x32.Images.SetKeyName(3, "user-new-3.png")
        Me.ImageCollection32x32.Images.SetKeyName(4, "mail-mark-task.png")
        Me.ImageCollection32x32.Images.SetKeyName(5, "folder-new-7.png")
        Me.ImageCollection32x32.Images.SetKeyName(6, "folder-new-2.png")
        Me.ImageCollection32x32.Images.SetKeyName(7, "meeting-attending.png")
        Me.ImageCollection32x32.Images.SetKeyName(8, "Bancos.jpg")
        Me.ImageCollection32x32.Images.SetKeyName(9, "concilia.png")
        Me.ImageCollection32x32.Images.SetKeyName(10, "CalendarCheck32.png")
        Me.ImageCollection32x32.Images.SetKeyName(11, "CalendarDelete32.png")
        Me.ImageCollection32x32.Images.SetKeyName(12, "CheckDelete32.png")
        Me.ImageCollection32x32.Images.SetKeyName(13, "Cuentas.ico")
        Me.ImageCollection32x32.Images.SetKeyName(14, "money.ico")
        Me.ImageCollection32x32.Images.SetKeyName(15, "green dollar1.ico")
        Me.ImageCollection32x32.Images.SetKeyName(16, "payment.ico")
        Me.ImageCollection32x32.Images.SetKeyName(17, "Folder 096.ico")
        Me.ImageCollection32x32.Images.SetKeyName(18, "Poliza.png")
        Me.ImageCollection32x32.Images.SetKeyName(19, "Clientes.png")
        Me.ImageCollection32x32.Images.SetKeyName(20, "AnulaVta.png")
        Me.ImageCollection32x32.Images.SetKeyName(21, "kdict.ico")
        Me.ImageCollection32x32.Images.SetKeyName(22, "descheck.png")
        Me.ImageCollection32x32.Images.SetKeyName(23, "Anula.ico")
        Me.ImageCollection32x32.Images.SetKeyName(24, "bodegas.png")
        Me.ImageCollection32x32.Images.SetKeyName(25, "Productos.png")
        Me.ImageCollection32x32.Images.SetKeyName(26, "imports.png")
        Me.ImageCollection32x32.Images.SetKeyName(27, "folder.png")
        Me.ImageCollection32x32.Images.SetKeyName(28, "folder-locked.png")
        Me.ImageCollection32x32.Images.SetKeyName(29, "CheckBook32.png")
        Me.ImageCollection32x32.Images.SetKeyName(30, "autoriza.png")
        Me.ImageCollection32x32.Images.SetKeyName(31, "stock_contact.png")
        Me.ImageCollection32x32.Images.SetKeyName(32, "Dinero.png")
        Me.ImageCollection32x32.Images.SetKeyName(33, "dinero2.png")
        Me.ImageCollection32x32.Images.SetKeyName(34, "pc.png")
        Me.ImageCollection32x32.Images.SetKeyName(35, "stock_new-spreadsheet - copia.png")
        Me.ImageCollection32x32.Images.SetKeyName(36, "retiro.png")
        Me.ImageCollection32x32.Images.SetKeyName(37, "venta.png")
        Me.ImageCollection32x32.Images.SetKeyName(38, "wiki.gif")
        Me.ImageCollection32x32.Images.SetKeyName(39, "Windows 7 (145).ico")
        Me.ImageCollection32x32.Images.SetKeyName(40, "Money.png")
        Me.ImageCollection32x32.Images.SetKeyName(41, "run-build-2.png")
        Me.ImageCollection32x32.Images.SetKeyName(42, "Utilities.png")
        Me.ImageCollection32x32.Images.SetKeyName(44, "database_add.ico")
        '
        'Administracion
        '
        Me.Administracion.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.a001, Me.a002})
        Me.Administracion.Name = "Administracion"
        Me.Administracion.Text = "Administración"
        '
        'a001
        '
        Me.a001.AllowTextClipping = False
        Me.a001.ItemLinks.Add(Me.a001001)
        Me.a001.ItemLinks.Add(Me.a001002)
        Me.a001.ItemLinks.Add(Me.a001003)
        Me.a001.ItemLinks.Add(Me.a001004, "FORMAS DE PAGO EN CAJA")
        Me.a001.ItemLinks.Add(Me.a001005)
        Me.a001.ItemLinks.Add(Me.a001006)
        Me.a001.ItemLinks.Add(Me.a001007)
        Me.a001.ItemLinks.Add(Me.a001008)
        Me.a001.ItemLinks.Add(Me.a001009)
        Me.a001.ItemLinks.Add(Me.a001010)
        Me.a001.ItemLinks.Add(Me.a001011)
        Me.a001.ItemLinks.Add(Me.a001012)
        Me.a001.ItemLinks.Add(Me.a001013)
        Me.a001.ItemLinks.Add(Me.a001014)
        Me.a001.ItemLinks.Add(Me.a001015)
        Me.a001.Name = "a001"
        Me.a001.Text = "Administración"
        '
        'a002
        '
        Me.a002.ItemLinks.Add(Me.a002001)
        Me.a002.ItemLinks.Add(Me.a002002)
        Me.a002.ItemLinks.Add(Me.a002003)
        Me.a002.ItemLinks.Add(Me.a002004)
        Me.a002.Name = "a002"
        Me.a002.Text = "Operaciones Sospechosas"
        '
        'Contabilidad
        '
        Me.Contabilidad.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.b001, Me.b002, Me.b003, Me.b004})
        Me.Contabilidad.Name = "Contabilidad"
        Me.Contabilidad.Text = "Contabilidad"
        '
        'b001
        '
        Me.b001.ItemLinks.Add(Me.b001001)
        Me.b001.ItemLinks.Add(Me.b001002)
        Me.b001.ItemLinks.Add(Me.b001003)
        Me.b001.ItemLinks.Add(Me.b001004)
        Me.b001.ItemLinks.Add(Me.b001005)
        Me.b001.Name = "b001"
        Me.b001.Text = "Archivos"
        '
        'b002
        '
        Me.b002.ItemLinks.Add(Me.b002001)
        Me.b002.ItemLinks.Add(Me.b002002)
        Me.b002.ItemLinks.Add(Me.b002003)
        Me.b002.ItemLinks.Add(Me.b002004)
        Me.b002.ItemLinks.Add(Me.b002005)
        Me.b002.ItemLinks.Add(Me.b002006)
        Me.b002.ItemLinks.Add(Me.b002007)
        Me.b002.ItemLinks.Add(Me.b002009)
        Me.b002.Name = "b002"
        Me.b002.Text = "Procesos"
        '
        'b003
        '
        Me.b003.AllowTextClipping = False
        Me.b003.ItemLinks.Add(Me.b003001)
        Me.b003.ItemLinks.Add(Me.b003002)
        Me.b003.Name = "b003"
        Me.b003.Text = "Reportes"
        '
        'b004
        '
        Me.b004.ItemLinks.Add(Me.b004001)
        Me.b004.ItemLinks.Add(Me.b004002)
        Me.b004.ItemLinks.Add(Me.b004003)
        Me.b004.Name = "b004"
        Me.b004.Text = "Presupuestos"
        '
        'Bancos
        '
        Me.Bancos.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.c001, Me.c002, Me.c003})
        Me.Bancos.Name = "Bancos"
        Me.Bancos.Text = "Bancos"
        '
        'c001
        '
        Me.c001.ItemLinks.Add(Me.c001001)
        Me.c001.ItemLinks.Add(Me.c001002)
        Me.c001.Name = "c001"
        Me.c001.Text = "Archivos"
        '
        'c002
        '
        Me.c002.ItemLinks.Add(Me.c002001)
        Me.c002.ItemLinks.Add(Me.c002002)
        Me.c002.ItemLinks.Add(Me.c002003)
        Me.c002.ItemLinks.Add(Me.c002004)
        Me.c002.ItemLinks.Add(Me.c002005)
        Me.c002.ItemLinks.Add(Me.c002006)
        Me.c002.ItemLinks.Add(Me.c002007)
        Me.c002.Name = "c002"
        Me.c002.Text = "Procesos"
        '
        'c003
        '
        Me.c003.ItemLinks.Add(Me.c003001)
        Me.c003.ItemLinks.Add(Me.c003002)
        Me.c003.ItemLinks.Add(Me.c003003)
        Me.c003.ItemLinks.Add(Me.c003004)
        Me.c003.ItemLinks.Add(Me.c003005)
        Me.c003.ItemLinks.Add(Me.c003006)
        Me.c003.Name = "c003"
        Me.c003.Text = "Reportes"
        '
        'Compras
        '
        Me.Compras.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.d001, Me.d002, Me.d003})
        Me.Compras.Name = "Compras"
        Me.Compras.Text = "Compras"
        '
        'd001
        '
        Me.d001.ItemLinks.Add(Me.d001001)
        Me.d001.ItemLinks.Add(Me.d001002)
        Me.d001.ItemLinks.Add(Me.d001003)
        Me.d001.ItemLinks.Add(Me.d001004)
        Me.d001.Name = "d001"
        Me.d001.Text = "Archivos"
        '
        'd002
        '
        Me.d002.ItemLinks.Add(Me.d002001)
        Me.d002.ItemLinks.Add(Me.d002002)
        Me.d002.ItemLinks.Add(Me.d002003)
        Me.d002.ItemLinks.Add(Me.d002004)
        Me.d002.ItemLinks.Add(Me.d002005)
        Me.d002.ItemLinks.Add(Me.d002006)
        Me.d002.ItemLinks.Add(Me.d002007)
        Me.d002.ItemLinks.Add(Me.d002008)
        Me.d002.ItemLinks.Add(Me.d002009)
        Me.d002.ItemLinks.Add(Me.d002010)
        Me.d002.Name = "d002"
        Me.d002.Text = "Procesos"
        '
        'd003
        '
        Me.d003.ItemLinks.Add(Me.d003001)
        Me.d003.ItemLinks.Add(Me.d003002)
        Me.d003.Name = "d003"
        Me.d003.Text = "Reportes"
        '
        'Facturacion
        '
        Me.Facturacion.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.e001, Me.e002, Me.e003, Me.e004})
        Me.Facturacion.Name = "Facturacion"
        Me.Facturacion.Text = "Facturación"
        '
        'e001
        '
        Me.e001.ItemLinks.Add(Me.e001001)
        Me.e001.ItemLinks.Add(Me.e001002)
        Me.e001.ItemLinks.Add(Me.e001003)
        Me.e001.ItemLinks.Add(Me.e001004)
        Me.e001.ItemLinks.Add(Me.e001005)
        Me.e001.ItemLinks.Add(Me.e001006)
        Me.e001.Name = "e001"
        Me.e001.Text = "Archivos"
        '
        'e002
        '
        Me.e002.ItemLinks.Add(Me.e002001)
        Me.e002.ItemLinks.Add(Me.e002002)
        Me.e002.ItemLinks.Add(Me.e002003)
        Me.e002.ItemLinks.Add(Me.e002004)
        Me.e002.ItemLinks.Add(Me.e002005)
        Me.e002.ItemLinks.Add(Me.e002006)
        Me.e002.ItemLinks.Add(Me.e002007)
        Me.e002.ItemLinks.Add(Me.e002008)
        Me.e002.ItemLinks.Add(Me.e002009)
        Me.e002.ItemLinks.Add(Me.e002010)
        Me.e002.ItemLinks.Add(Me.e002011)
        Me.e002.ItemLinks.Add(Me.e002012)
        Me.e002.ItemLinks.Add(Me.e002013)
        Me.e002.ItemLinks.Add(Me.e002014)
        Me.e002.Name = "e002"
        Me.e002.Text = "Procesos"
        '
        'e003
        '
        Me.e003.AllowTextClipping = False
        Me.e003.ItemLinks.Add(Me.e003001)
        Me.e003.ItemLinks.Add(Me.e003003)
        Me.e003.ItemLinks.Add(Me.e003002)
        Me.e003.Name = "e003"
        Me.e003.Text = "Reportes y Consultas"
        '
        'e004
        '
        Me.e004.ItemLinks.Add(Me.e004001)
        Me.e004.ItemLinks.Add(Me.e004003)
        Me.e004.ItemLinks.Add(Me.e004002)
        Me.e004.ItemLinks.Add(Me.e004004)
        Me.e004.Name = "e004"
        Me.e004.Text = "Gestiones"
        '
        'Inventario
        '
        Me.Inventario.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.f001, Me.f002, Me.f003})
        Me.Inventario.Name = "Inventario"
        Me.Inventario.Text = "Inventario"
        '
        'f001
        '
        Me.f001.ItemLinks.Add(Me.f001001)
        Me.f001.ItemLinks.Add(Me.f001002)
        Me.f001.ItemLinks.Add(Me.f001003)
        Me.f001.ItemLinks.Add(Me.f001004)
        Me.f001.ItemLinks.Add(Me.f001005)
        Me.f001.ItemLinks.Add(Me.f001006)
        Me.f001.ItemLinks.Add(Me.f001007)
        Me.f001.Name = "f001"
        Me.f001.Text = "Archivos"
        '
        'f002
        '
        Me.f002.ItemLinks.Add(Me.f002001)
        Me.f002.ItemLinks.Add(Me.f002002)
        Me.f002.ItemLinks.Add(Me.f002003)
        Me.f002.ItemLinks.Add(Me.f002004)
        Me.f002.ItemLinks.Add(Me.f002005)
        Me.f002.ItemLinks.Add(Me.f002006)
        Me.f002.ItemLinks.Add(Me.f002007)
        Me.f002.ItemLinks.Add(Me.f002008)
        Me.f002.ItemLinks.Add(Me.f002009)
        Me.f002.ItemLinks.Add(Me.f002010)
        Me.f002.Name = "f002"
        Me.f002.Text = "Procesos"
        '
        'f003
        '
        Me.f003.ItemLinks.Add(Me.f003001)
        Me.f003.Name = "f003"
        Me.f003.Text = "Reportes"
        '
        'CuentasPagar
        '
        Me.CuentasPagar.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.g001, Me.g002})
        Me.CuentasPagar.Name = "CuentasPagar"
        Me.CuentasPagar.Text = "Cuentas por Pagar"
        '
        'g001
        '
        Me.g001.ItemLinks.Add(Me.g001001)
        Me.g001.ItemLinks.Add(Me.g001002)
        Me.g001.ItemLinks.Add(Me.g001003)
        Me.g001.ItemLinks.Add(Me.g001004)
        Me.g001.ItemLinks.Add(Me.g001005)
        Me.g001.ItemLinks.Add(Me.g001006)
        Me.g001.ItemLinks.Add(Me.g001007)
        Me.g001.ItemLinks.Add(Me.g001008)
        Me.g001.Name = "g001"
        Me.g001.Text = "Procesos"
        '
        'g002
        '
        Me.g002.ItemLinks.Add(Me.g002001)
        Me.g002.ItemLinks.Add(Me.g002002)
        Me.g002.ItemLinks.Add(Me.g002003)
        Me.g002.ItemLinks.Add(Me.g002005)
        Me.g002.ItemLinks.Add(Me.g002004)
        Me.g002.ItemLinks.Add(Me.g002006)
        Me.g002.ItemLinks.Add(Me.g002007)
        Me.g002.ItemLinks.Add(Me.g002008)
        Me.g002.Name = "g002"
        Me.g002.Text = "Reportes"
        '
        'CuentasCobrar
        '
        Me.CuentasCobrar.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.h001, Me.h002, Me.h003})
        Me.CuentasCobrar.Name = "CuentasCobrar"
        Me.CuentasCobrar.Text = "Cuentas por Cobrar"
        '
        'h001
        '
        Me.h001.ItemLinks.Add(Me.h001001)
        Me.h001.ItemLinks.Add(Me.h001002)
        Me.h001.Name = "h001"
        Me.h001.Text = "Archivos"
        '
        'h002
        '
        Me.h002.ItemLinks.Add(Me.h002001)
        Me.h002.ItemLinks.Add(Me.h002002)
        Me.h002.ItemLinks.Add(Me.h002003)
        Me.h002.ItemLinks.Add(Me.h002004)
        Me.h002.ItemLinks.Add(Me.h002005)
        Me.h002.ItemLinks.Add(Me.h002006)
        Me.h002.ItemLinks.Add(Me.h002007)
        Me.h002.ItemLinks.Add(Me.h002008)
        Me.h002.ItemLinks.Add(Me.h002009)
        Me.h002.Name = "h002"
        Me.h002.Text = "Procesos"
        '
        'h003
        '
        Me.h003.ItemLinks.Add(Me.h003001)
        Me.h003.ItemLinks.Add(Me.h003002)
        Me.h003.ItemLinks.Add(Me.h003003)
        Me.h003.ItemLinks.Add(Me.h003004)
        Me.h003.ItemLinks.Add(Me.h003005)
        Me.h003.ItemLinks.Add(Me.h003006)
        Me.h003.ItemLinks.Add(Me.h003007)
        Me.h003.ItemLinks.Add(Me.h003008)
        Me.h003.ItemLinks.Add(Me.h003009)
        Me.h003.ItemLinks.Add(Me.h003010)
        Me.h003.ItemLinks.Add(Me.h003011)
        Me.h003.ItemLinks.Add(Me.h003012)
        Me.h003.ItemLinks.Add(Me.h003013)
        Me.h003.Name = "h003"
        Me.h003.Text = "Reportes"
        '
        'ActivoFijo
        '
        Me.ActivoFijo.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.i001, Me.i002, Me.i003})
        Me.ActivoFijo.Name = "ActivoFijo"
        Me.ActivoFijo.Text = "Activo Fijo"
        '
        'i001
        '
        Me.i001.ItemLinks.Add(Me.i001001)
        Me.i001.ItemLinks.Add(Me.i001002)
        Me.i001.ItemLinks.Add(Me.i001003)
        Me.i001.ItemLinks.Add(Me.i001004)
        Me.i001.ItemLinks.Add(Me.i001005)
        Me.i001.ItemLinks.Add(Me.i001006)
        Me.i001.ItemLinks.Add(Me.i001007)
        Me.i001.ItemLinks.Add(Me.i001008)
        Me.i001.ItemLinks.Add(Me.i001009)
        Me.i001.Name = "i001"
        Me.i001.Text = "Archivos"
        '
        'i002
        '
        Me.i002.ItemLinks.Add(Me.i002001)
        Me.i002.ItemLinks.Add(Me.i002002)
        Me.i002.ItemLinks.Add(Me.i002003)
        Me.i002.ItemLinks.Add(Me.i002004)
        Me.i002.ItemLinks.Add(Me.i002005)
        Me.i002.ItemLinks.Add(Me.i002006)
        Me.i002.ItemLinks.Add(Me.i002007)
        Me.i002.ItemLinks.Add(Me.i002008)
        Me.i002.ItemLinks.Add(Me.i002009)
        Me.i002.Name = "i002"
        Me.i002.Text = "Procesos"
        '
        'i003
        '
        Me.i003.ItemLinks.Add(Me.i003001)
        Me.i003.ItemLinks.Add(Me.i003002)
        Me.i003.ItemLinks.Add(Me.i003003)
        Me.i003.ItemLinks.Add(Me.i003004)
        Me.i003.ItemLinks.Add(Me.i003005)
        Me.i003.ItemLinks.Add(Me.i003006)
        Me.i003.ItemLinks.Add(Me.i003007)
        Me.i003.ItemLinks.Add(Me.i003008)
        Me.i003.ItemLinks.Add(Me.i003009)
        Me.i003.Name = "i003"
        Me.i003.Text = "Reportes"
        '
        'Producción
        '
        Me.Producción.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.j001, Me.j002, Me.j003})
        Me.Producción.Name = "Producción"
        Me.Producción.Text = "Producción"
        '
        'j001
        '
        Me.j001.ItemLinks.Add(Me.j001001)
        Me.j001.ItemLinks.Add(Me.j001002)
        Me.j001.Name = "j001"
        Me.j001.Text = "Archivos"
        '
        'j002
        '
        Me.j002.ItemLinks.Add(Me.j002001)
        Me.j002.ItemLinks.Add(Me.j002002)
        Me.j002.ItemLinks.Add(Me.j002003)
        Me.j002.ItemLinks.Add(Me.j002004)
        Me.j002.Name = "j002"
        Me.j002.Text = "Procesos"
        '
        'j003
        '
        Me.j003.Name = "j003"
        Me.j003.Text = "Reportes"
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 426)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(1589, 23)
        '
        'XtraTabbedMdiManager1
        '
        Me.XtraTabbedMdiManager1.MdiParent = Me
        '
        'NexusMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1589, 449)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "NexusMain"
        Me.Ribbon = Me.RibbonControl
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "Nexus ERP"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageCollection16x16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageCollection32x32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Administracion As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents a001 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents Contabilidad As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents Bancos As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents Compras As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents Facturacion As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents Inventario As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents CuentasPagar As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents CuentasCobrar As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents b001 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents b002 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents b003 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents c001 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents c002 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents c003 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents d001 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents qbNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d002 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents d003 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents qbSave As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e001 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents e002 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents e003 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents f001 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents f002 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents f003 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents a001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b001003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b002003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b002004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b002005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b002006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents qbEdit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents qbDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents qbUndo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents qbFind As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents qbReport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents qbBack As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents qbNext As DevExpress.XtraBars.BarButtonItem
    Public WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents XtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Private WithEvents ImageCollection16x16 As DevExpress.Utils.ImageCollection
    Friend WithEvents c001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c002003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c002004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c002005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c003001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c003002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c003003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c003004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c003005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c003006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d002003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d002004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e001001 As DevExpress.XtraBars.BarButtonItem
    'Friend WithEvents e001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e001003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents b003002 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents b003001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001010 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001011 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001012 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001013 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001014 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003001015 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003002003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003002004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003002005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003002006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents e003001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001010 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003002 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents e003002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003002003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001011 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001012 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f001003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f001004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f002003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f002004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f002005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f002006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents f003001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f00300109 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001010 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001011 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001012 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g001 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents g001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g001003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g001004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g002 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents g002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g002003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g002004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g002005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g002006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h001 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents h002 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents h003 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents h002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h002003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h002004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003010 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g001005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ImageCollection32x32 As DevExpress.Utils.ImageCollection
    Friend WithEvents e002008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ActivoFijo As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents e001004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003002007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003002008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003002009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003002010 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b002007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i001 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents i001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i001003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i002 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents i003 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents i001004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i001005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i001006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i001007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i001008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i001009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i002003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i002004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i002005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i002006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i002007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i003001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i002008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i003002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i003003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i003004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i003005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i003006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i003007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i003008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i003009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b001004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001013 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b001007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b001008 As DevExpress.XtraBars.BarButtonItem

    Friend WithEvents b001005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d002005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003011 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001013 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001014 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001015 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g001006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f002007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d002006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d002007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d001003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001016 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d001004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents i002009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002010 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002011 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f002008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001017 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001018 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001019 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001020 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001014 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h002005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002012 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002013 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d002008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001015 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d002009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001010 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001011 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b004 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents b004001 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents b004002 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents b004003 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents b004001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b004001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b004002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b004003001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b004003002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b004003003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b004003004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b004003005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001012 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f001005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g002007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003002004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001013 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e001005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001021 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Producción As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents j001 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents j002 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents j003 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents j002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents j001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents j002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents j002003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents j001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents j002004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h002006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003003 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents e003003001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f001006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001016 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003002011 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c002006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003001 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents d003001001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003001002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003001003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003001004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003001005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003001006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003001007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003001008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003001009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003002 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents d003002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e001006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003012 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g002008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h002007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003001010 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f002009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003003002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003003003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents c002007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e004001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e004002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e004 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents e003003004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents qbRefresh As DevExpress.XtraBars.BarButtonItem

    Friend WithEvents d002010 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001014 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001015 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a001016 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a002001 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a002003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a002004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents a002 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents e003003005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003003006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f002010 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003003007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003002005 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f001007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001017 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e002014 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b004003006 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b004002002 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e004003 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003003008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b003002012 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents b002009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h002008 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h002009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents h003013 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001022 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003001023 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e004004 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001018 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001019 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003003009 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g001007 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents f003001020 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents e003003010 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents d003001011 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents g001008 As DevExpress.XtraBars.BarButtonItem
End Class
