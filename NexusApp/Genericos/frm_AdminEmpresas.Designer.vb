﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_AdminEmpresas
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_AdminEmpresas))
        Me.WizardControlGral = New DevExpress.XtraWizard.WizardControl()
        Me.wAsisEmpPag1 = New DevExpress.XtraWizard.WelcomeWizardPage()
        Me.chkAcepto = New DevExpress.XtraEditors.CheckEdit()
        Me.wAsistPag2 = New DevExpress.XtraWizard.WizardPage()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.chkBodegas = New DevExpress.XtraEditors.CheckEdit()
        Me.chkSucursales = New DevExpress.XtraEditors.CheckEdit()
        Me.chkProveedores = New DevExpress.XtraEditors.CheckEdit()
        Me.chkComprobantes = New DevExpress.XtraEditors.CheckEdit()
        Me.chkFormasPago = New DevExpress.XtraEditors.CheckEdit()
        Me.chkCatalogo = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.txtClaveConfirmado = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.txtClave = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNRC = New DevExpress.XtraEditors.TextEdit()
        Me.txtNit = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtNombreEmpresa = New DevExpress.XtraEditors.TextEdit()
        Me.CompletionWizardPageEnd = New DevExpress.XtraWizard.CompletionWizardPage()
        CType(Me.WizardControlGral, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.WizardControlGral.SuspendLayout()
        Me.wAsisEmpPag1.SuspendLayout()
        CType(Me.chkAcepto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.wAsistPag2.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.chkBodegas.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkSucursales.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkProveedores.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkComprobantes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFormasPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCatalogo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClaveConfirmado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNRC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombreEmpresa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'WizardControlGral
        '
        Me.WizardControlGral.Controls.Add(Me.wAsisEmpPag1)
        Me.WizardControlGral.Controls.Add(Me.wAsistPag2)
        Me.WizardControlGral.Controls.Add(Me.CompletionWizardPageEnd)
        Me.WizardControlGral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WizardControlGral.Image = Global.Nexus.My.Resources.Resources.wpp_01
        Me.WizardControlGral.Location = New System.Drawing.Point(0, 0)
        Me.WizardControlGral.Name = "WizardControlGral"
        Me.WizardControlGral.NextText = "& Siguiente >"
        Me.WizardControlGral.Pages.AddRange(New DevExpress.XtraWizard.BaseWizardPage() {Me.wAsisEmpPag1, Me.wAsistPag2, Me.CompletionWizardPageEnd})
        Me.WizardControlGral.Size = New System.Drawing.Size(677, 432)
        Me.WizardControlGral.Text = "Nueva Empresa"
        '
        'wAsisEmpPag1
        '
        Me.wAsisEmpPag1.Controls.Add(Me.chkAcepto)
        Me.wAsisEmpPag1.IntroductionText = resources.GetString("wAsisEmpPag1.IntroductionText")
        Me.wAsisEmpPag1.Name = "wAsisEmpPag1"
        Me.wAsisEmpPag1.ProceedText = ""
        Me.wAsisEmpPag1.Size = New System.Drawing.Size(460, 299)
        Me.wAsisEmpPag1.Text = "Nueva Empresa / Nexus ERP"
        '
        'chkAcepto
        '
        Me.chkAcepto.Location = New System.Drawing.Point(17, 260)
        Me.chkAcepto.Name = "chkAcepto"
        Me.chkAcepto.Properties.Caption = "He leído, Acepto Terminos y condiciones"
        Me.chkAcepto.Size = New System.Drawing.Size(293, 19)
        Me.chkAcepto.TabIndex = 0
        '
        'wAsistPag2
        '
        Me.wAsistPag2.Controls.Add(Me.GroupControl1)
        Me.wAsistPag2.Controls.Add(Me.LabelControl6)
        Me.wAsistPag2.Controls.Add(Me.txtClaveConfirmado)
        Me.wAsistPag2.Controls.Add(Me.LabelControl5)
        Me.wAsistPag2.Controls.Add(Me.txtClave)
        Me.wAsistPag2.Controls.Add(Me.LabelControl4)
        Me.wAsistPag2.Controls.Add(Me.LabelControl3)
        Me.wAsistPag2.Controls.Add(Me.LabelControl2)
        Me.wAsistPag2.Controls.Add(Me.txtNRC)
        Me.wAsistPag2.Controls.Add(Me.txtNit)
        Me.wAsistPag2.Controls.Add(Me.LabelControl1)
        Me.wAsistPag2.Controls.Add(Me.txtNombreEmpresa)
        Me.wAsistPag2.DescriptionText = "Por favor complete lo siguiente antes de proceder con la operación"
        Me.wAsistPag2.Name = "wAsistPag2"
        Me.wAsistPag2.Size = New System.Drawing.Size(645, 287)
        Me.wAsistPag2.Text = "Informacion Requerida"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.chkBodegas)
        Me.GroupControl1.Controls.Add(Me.chkSucursales)
        Me.GroupControl1.Controls.Add(Me.chkProveedores)
        Me.GroupControl1.Controls.Add(Me.chkComprobantes)
        Me.GroupControl1.Controls.Add(Me.chkFormasPago)
        Me.GroupControl1.Controls.Add(Me.chkCatalogo)
        Me.GroupControl1.Location = New System.Drawing.Point(433, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(200, 263)
        Me.GroupControl1.TabIndex = 5
        Me.GroupControl1.Text = "Importacion de Datos"
        '
        'chkBodegas
        '
        Me.chkBodegas.Location = New System.Drawing.Point(15, 169)
        Me.chkBodegas.Name = "chkBodegas"
        Me.chkBodegas.Properties.Caption = "Bodegas"
        Me.chkBodegas.Size = New System.Drawing.Size(169, 19)
        Me.chkBodegas.TabIndex = 10
        '
        'chkSucursales
        '
        Me.chkSucursales.Location = New System.Drawing.Point(15, 144)
        Me.chkSucursales.Name = "chkSucursales"
        Me.chkSucursales.Properties.Caption = "Sucursales"
        Me.chkSucursales.Size = New System.Drawing.Size(169, 19)
        Me.chkSucursales.TabIndex = 9
        '
        'chkProveedores
        '
        Me.chkProveedores.Enabled = False
        Me.chkProveedores.Location = New System.Drawing.Point(37, 107)
        Me.chkProveedores.Name = "chkProveedores"
        Me.chkProveedores.Properties.Caption = "Proveedores"
        Me.chkProveedores.Size = New System.Drawing.Size(147, 19)
        Me.chkProveedores.TabIndex = 8
        '
        'chkComprobantes
        '
        Me.chkComprobantes.EditValue = True
        Me.chkComprobantes.Enabled = False
        Me.chkComprobantes.Location = New System.Drawing.Point(37, 82)
        Me.chkComprobantes.Name = "chkComprobantes"
        Me.chkComprobantes.Properties.Caption = "Comprobantes I/E"
        Me.chkComprobantes.Size = New System.Drawing.Size(147, 19)
        Me.chkComprobantes.TabIndex = 7
        '
        'chkFormasPago
        '
        Me.chkFormasPago.EditValue = True
        Me.chkFormasPago.Enabled = False
        Me.chkFormasPago.Location = New System.Drawing.Point(37, 57)
        Me.chkFormasPago.Name = "chkFormasPago"
        Me.chkFormasPago.Properties.Caption = "Formas de Pago"
        Me.chkFormasPago.Size = New System.Drawing.Size(147, 19)
        Me.chkFormasPago.TabIndex = 6
        '
        'chkCatalogo
        '
        Me.chkCatalogo.Location = New System.Drawing.Point(15, 32)
        Me.chkCatalogo.Name = "chkCatalogo"
        Me.chkCatalogo.Properties.Caption = "Catalogo de Cuentas"
        Me.chkCatalogo.Size = New System.Drawing.Size(169, 19)
        Me.chkCatalogo.TabIndex = 5
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(176, 230)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(43, 13)
        Me.LabelControl6.TabIndex = 1
        Me.LabelControl6.Text = "Confirme"
        '
        'txtClaveConfirmado
        '
        Me.txtClaveConfirmado.Location = New System.Drawing.Point(176, 249)
        Me.txtClaveConfirmado.Name = "txtClaveConfirmado"
        Me.txtClaveConfirmado.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtClaveConfirmado.Size = New System.Drawing.Size(149, 20)
        Me.txtClaveConfirmado.TabIndex = 4
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(12, 230)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl5.TabIndex = 1
        Me.LabelControl5.Text = "Ingrese"
        '
        'txtClave
        '
        Me.txtClave.Location = New System.Drawing.Point(12, 249)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtClave.Size = New System.Drawing.Size(149, 20)
        Me.txtClave.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(12, 211)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(205, 13)
        Me.LabelControl4.TabIndex = 1
        Me.LabelControl4.Text = "Contraseña usuario ""ADMINISTRADOR"" **"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(224, 57)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "NRC **"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(12, 57)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "NIT **"
        '
        'txtNRC
        '
        Me.txtNRC.Location = New System.Drawing.Point(224, 76)
        Me.txtNRC.Name = "txtNRC"
        Me.txtNRC.Size = New System.Drawing.Size(149, 20)
        Me.txtNRC.TabIndex = 2
        '
        'txtNit
        '
        Me.txtNit.Location = New System.Drawing.Point(12, 76)
        Me.txtNit.Name = "txtNit"
        Me.txtNit.Size = New System.Drawing.Size(149, 20)
        Me.txtNit.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(122, 13)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Nombre de la Empresa **"
        '
        'txtNombreEmpresa
        '
        Me.txtNombreEmpresa.Location = New System.Drawing.Point(12, 31)
        Me.txtNombreEmpresa.Name = "txtNombreEmpresa"
        Me.txtNombreEmpresa.Size = New System.Drawing.Size(361, 20)
        Me.txtNombreEmpresa.TabIndex = 0
        '
        'CompletionWizardPageEnd
        '
        Me.CompletionWizardPageEnd.FinishText = "Su nueva empresa ha sido creada con exito!" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "para acceder a ella, debe salir e rei" &
    "ngresar a " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Nexus ERP."
        Me.CompletionWizardPageEnd.Name = "CompletionWizardPageEnd"
        Me.CompletionWizardPageEnd.Size = New System.Drawing.Size(460, 299)
        Me.CompletionWizardPageEnd.Text = "Completado"
        '
        'frm_AdminEmpresas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(677, 432)
        Me.Controls.Add(Me.WizardControlGral)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_AdminEmpresas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NEXUS ERP / Asistente para Creacion de Empresas"
        CType(Me.WizardControlGral, System.ComponentModel.ISupportInitialize).EndInit()
        Me.WizardControlGral.ResumeLayout(False)
        Me.wAsisEmpPag1.ResumeLayout(False)
        CType(Me.chkAcepto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.wAsistPag2.ResumeLayout(False)
        Me.wAsistPag2.PerformLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.chkBodegas.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkSucursales.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkProveedores.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkComprobantes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFormasPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCatalogo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClaveConfirmado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNRC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombreEmpresa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents WizardControlGral As DevExpress.XtraWizard.WizardControl
    Friend WithEvents wAsisEmpPag1 As DevExpress.XtraWizard.WelcomeWizardPage
    Friend WithEvents wAsistPag2 As DevExpress.XtraWizard.WizardPage
    Friend WithEvents CompletionWizardPageEnd As DevExpress.XtraWizard.CompletionWizardPage
    Friend WithEvents chkAcepto As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkBodegas As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkSucursales As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkProveedores As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkComprobantes As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkFormasPago As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkCatalogo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNombreEmpresa As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtClaveConfirmado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtClave As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNRC As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNit As DevExpress.XtraEditors.TextEdit
End Class
