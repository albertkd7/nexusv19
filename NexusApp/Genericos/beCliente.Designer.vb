<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class beCliente
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblCodigo = New DevExpress.XtraEditors.LabelControl
        Me.teNombre = New DevExpress.XtraEditors.TextEdit
        Me.beCodigo = New DevExpress.XtraEditors.ButtonEdit
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.beCodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblCodigo
        '
        Me.lblCodigo.Location = New System.Drawing.Point(2, 3)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(37, 13)
        Me.lblCodigo.TabIndex = 3
        Me.lblCodigo.Text = "Cliente:"
        '
        'teNombre
        '
        Me.teNombre.Enabled = False
        Me.teNombre.EnterMoveNextControl = True
        Me.teNombre.Location = New System.Drawing.Point(168, 0)
        Me.teNombre.Name = "teNombre"
        Me.teNombre.Properties.ReadOnly = True
        Me.teNombre.Size = New System.Drawing.Size(426, 20)
        Me.teNombre.TabIndex = 5
        '
        'beCodigo
        '
        Me.beCodigo.EnterMoveNextControl = True
        Me.beCodigo.Location = New System.Drawing.Point(42, 0)
        Me.beCodigo.Name = "beCodigo"
        Me.beCodigo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
        Me.beCodigo.Size = New System.Drawing.Size(124, 20)
        Me.beCodigo.TabIndex = 4
        '
        'beCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblCodigo)
        Me.Controls.Add(Me.teNombre)
        Me.Controls.Add(Me.beCodigo)
        Me.Name = "beCliente"
        Me.Size = New System.Drawing.Size(600, 20)
        CType(Me.teNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.beCodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCodigo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents teNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents beCodigo As DevExpress.XtraEditors.ButtonEdit

End Class
