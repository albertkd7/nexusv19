Imports NexusELL.TableEntities
Public Class beCliente
    Private ElUsuarioDioClic = False
    Private Sub beCodigo_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs) Handles beCodigo.ButtonClick
        beCodigo.EditValue = ""
        ElUsuarioDioClic = True
        beCodigo_Validated(sender, New System.EventArgs)
    End Sub

    Private Sub beCodigo_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles beCodigo.Validated
        If Not ElUsuarioDioClic And beCodigo.EditValue = "" Then
            Exit Sub
        End If
        Dim ent As fac_Clientes = objConsultas.cnsClientes(fac_frmConsultaClientes, beCodigo.EditValue)
        ElUsuarioDioClic = False

        beCodigo.EditValue = ent.IdCliente
        teNombre.EditValue = ent.Nombre
        teNombre.Focus()
    End Sub

End Class


